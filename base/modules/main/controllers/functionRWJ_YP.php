<?php
class functionRWJ extends  MX_Controller
{
	public $ErrLoginMsg = '';
	private $dbSQL      = "";
	private $date_now      	= "";
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('bayarigd/M_pembayaran');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model("new/M_produk");
		$this->load->model('new/Tbl_data_transaksi');
		$this->load->model('new/Tbl_data_kunjungan');
		$this->load->model('new/Tbl_data_nginap');
		$this->load->model('new/Tbl_data_pasien_inap');
		$this->load->model('new/Tbl_data_detail_tr_kamar');
		$this->load->model('new/Tbl_data_kamar');
		$this->load->model('new/Tbl_data_dokter');
		$this->load->model('new/Tbl_data_customer');
		$this->load->model('new/Tbl_data_detail_transaksi');
		$this->load->model('new/Tbl_data_detail_component');
		$this->load->model('new/Tbl_data_detail_bayar');
		$this->load->model('new/Tbl_data_detail_tr_bayar');
		$this->load->model('new/Tbl_data_detail_tr_bayar_component');
		$this->load->model('new/Tbl_data_tarif_component');
		$this->load->model('new/Tbl_data_detail_tr_dokter');
		$this->date_now = date("Y-m-d H:i:s");
	}
	public function index()
	{
		$this->load->view('main/index', $data = array('controller' => $this));
	}
	public function insertDataProduk()
	{
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$result 	= array();
		$params = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=> $_POST['no_transaksi'],
			'kd_produk'     => $_POST['kd_produk'],
			'urut' 			=> $_POST['urut'],
			'kd_unit' 		=> $_POST['unit'],
			'kd_tarif' 		=> $_POST['kd_tarif'],
			'tgl_transaksi'	=> date('Y-m-d', strtotime(str_replace('/', '-', $_POST['tgl_transaksi']))),
			'tgl_berlaku'	=> date('Y-m-d', strtotime(str_replace('/', '-', $_POST['tgl_berlaku']))),
		);
		//echo json_encode($params);
		$countDetailTransaksi    = $this->M_produk->cekDetailTransaksi($params)->num_rows();
		//$countDetailTransaksiSQL = $this->M_produk->cekDetailTransaksiSQL($params)->num_rows();
		//echo $countDetailTransaksi .' '.$countDetailTransaksiSQL;
		if ($countDetailTransaksi > 0) {
			$setter['kd_produk']     = $_POST['kd_produk'];
			$setter['qty']           = $_POST['quantity'];
			$qty 		= $this->M_produk->cekDetailTransaksi($params)->row()->qty;
			$success    = $this->M_produk->updateDetailTransaksi($params, $setter);
			//$successSQL = $this->M_produk->updateDetailTransaksiSQL($params, $setter);

			if ($success > 0) {
				$dataubahpasien = $this->db->query("select * from transaksi where 
													no_transaksi='" . $params['no_transaksi'] . "' 
													and 
													kd_kasir='" . $params['kd_kasir'] . "'");
				if (count($dataubahpasien) > 0) {
					$KdPasien = $dataubahpasien->row()->kd_pasien;
					$TglMasuk = $dataubahpasien->row()->tgl_transaksi;
					$KdUnit = $dataubahpasien->row()->kd_unit;
					$UrutMasuk = $dataubahpasien->row()->urut_masuk;
				} else {
					$KdPasien = '';
					$TglMasuk = '';
					$KdUnit = '';
					$UrutMasuk = 0;
				}
				//$result_ubah_status_sinkron= $this->db->query("
				//	UPDATE kunjungan set status_sinkronisasi=2 
				//	WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk=".$UrutMasuk."");

				$this->db->trans_commit();
				//$this->dbSQL->trans_commit();
				$result['status'] = true;
				$result['action'] = "update";
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				$result['status'] = false;
				$result['action'] = "update";
			}
		} else {
			$paramsUrut = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi']
				//'kd_unit' 		=> $_POST['unit'],
			);

			$urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
			//$urutSQL 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;
			if ($urutPG == $urutSQL) {
				$params['urut'] 	= $urutSQL + 1;
			}
			//echo $params['urut'] ;
			$params['folio']         = "A";
			$params['qty']           = $_POST['quantity'];
			$params['harga']         = $_POST['harga'];
			$success         = $this->M_produk->insertDetailTransaksi($params);
			//$successSQL      = $this->M_produk->insertDetailTransaksiSQL($params);
			if ($success > 0) {
				$params_tarif 	= array(
					'kd_unit' 		=> $params['kd_unit'],
					'kd_tarif' 		=> $params['kd_tarif'],
					'tgl_berlaku'	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				//$result_data 	= $this->M_produk->getDataTarifComponentSQL($params_tarif);
				$result_data 	= $this->M_produk->getDataTarifComponent($params_tarif);

				$params_delete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
				);
				$deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
				//$deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);

				$this->db->trans_commit();
				//$this->dbSQL->trans_commit();

				// $this->dbSQL->close();
				foreach ($result_data->result_array() as $data) {
					$params_detail_component = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'tgl_transaksi'	=> $params['tgl_transaksi'],
						'urut'			=> $params['urut'],
						'tarif'			=> $data['TARIF'],
						'kd_component'	=> $data['KD_COMPONENT'],
					);
					$success    = $this->M_produk->insertDetailComponent($params_detail_component);
					//$successSQL = $this->M_produk->insertDetailComponentSQL($params_detail_component);
				}

				if ($success > 0) {
					$dataubahpasien = $this->db->query("select * from transaksi where 
															no_transaksi='" . $params['no_transaksi'] . "' 
															and 
															kd_kasir='" . $params['kd_kasir'] . "'");
					if (count($dataubahpasien) > 0) {
						$KdPasien = $dataubahpasien->row()->kd_pasien;
						$TglMasuk = $dataubahpasien->row()->tgl_transaksi;
						$KdUnit = $dataubahpasien->row()->kd_unit;
						$UrutMasuk = $dataubahpasien->row()->urut_masuk;
					} else {
						$KdPasien = '';
						$TglMasuk = '';
						$KdUnit = '';
						$UrutMasuk = 0;
					}
					//$result_ubah_status_sinkron= $this->db->query("
					//	UPDATE kunjungan set status_sinkronisasi=2 
					//	WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk=".$UrutMasuk."");

					$result['status'] = true;
					$result['action'] = "insert";
					$this->db->trans_commit();
					//$this->dbSQL->trans_commit();
					//$this->dbSQL->close();
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$result['status'] = false;
					$result['action'] = "insert";
				}
			} else {
				$result['status'] = false;
				$result['action'] = "insert";
			}
		}
		echo json_encode($result);
	}
	public function cekdatadetail_bayar()
	{
		$result = $this->db->query("select count(no_transaksi) as jumlah from detail_bayar where no_transaksi='" . $_POST['notrx'] . "'")->result();
		$jsonResult = array();
		$jsonResult['success'] = 'true';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function cekkodepayment()
	{
		$result = $this->db->query("select kd_pay from detail_bayar where no_transaksi='" . $_POST['notrx'] . "' and kd_kasir='01'")->result();
		$jsonResult = array();
		$jsonResult['success'] = 'true';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	//hani 2023-07-18
	public function get_anamnese(){
    	$kd_pasien= $this->input->post('kd_pasien');
    	$kd_unit= $this->input->post('kd_unit');
    	$tgl_masuk= $this->input->post('tgl_masuk');
    	$urut_masuk= $this->input->post('urut_masuk');
		
		$klas_produks = $this->db->query("SELECT anamnese FROM kunjungan where kd_pasien = '".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_masuk."' and urut_masuk=".$urut_masuk."")->row();
		$jsonResult['success']=true;
    	$jsonResult['listData']=$klas_produks->anamnese;
    	echo json_encode($jsonResult);
	}
	//=========================================

	public function upload_foto()
	{
		try {
			if (isset($_FILES)) {

				$temp_file_name = $_FILES['image-upload_lr']['tmp_name'];
				$original_file_name = $_FILES['image-upload_lr']['name'];
				$temp_file_name2 = $_FILES['image-upload_lr']['tmp_name'];
				// Find file extention
				$ext = explode('.', $original_file_name);
				$ext = $ext[count($ext) - 1];

				// Remove the extention from the original file name
				$file_name = str_replace($ext, '', $original_file_name);
				$new_name2 = 'LOGORSBA.' . 'png';
				$new_name = 'LOGO RSBA.' . 'png';
				// 
				if (move_uploaded_file($temp_file_name, 'ui/images/Logo/' . $new_name2)) {
					copy('ui/images/Logo/' . $new_name2, "ui/images/Logo/LOGO RSBA.png");
					// move_uploaded_file ($temp_file_name2,'ui/images/Logo/'. $new_name);
					echo "{success:true}";
				} else {
					echo "{success:false}";
				}
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function get_pemeriksaanmata(){
    	$kd_pasien= $this->input->post('kd_pasien');
    	$kd_unit= $this->input->post('kd_unit');
    	$tgl_masuk= $this->input->post('tgl_masuk');
    	$urut_masuk= $this->input->post('urut_masuk');
		
		$klas_produks = $this->db->query("SELECT test_buta_warna FROM kunjungan where kd_pasien = '".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_masuk."' and urut_masuk=".$urut_masuk."")->row();
		$jsonResult['success']=true;
		if ($klas_produks->test_buta_warna == null) {
			$jsonResult['listData']="-";
		} else {
			$jsonResult['listData']=$klas_produks->test_buta_warna;
		}
		echo json_encode($jsonResult);
	}

	public function get_pemeriksaangigi(){
    	$kd_pasien= $this->input->post('kd_pasien');
    	$kd_unit= $this->input->post('kd_unit');
    	$tgl_masuk= $this->input->post('tgl_masuk');
    	$urut_masuk= $this->input->post('urut_masuk');
		
		$klas_produks = $this->db->query("SELECT test_buta_warna FROM kunjungan where kd_pasien = '".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_masuk."' and urut_masuk=".$urut_masuk."")->row();
		$jsonResult['success']=true;
		if ($klas_produks->test_buta_warna == null) {
			$jsonResult['listData']="-";
		} else {
			$jsonResult['listData']=$klas_produks->test_buta_warna;
		}
		echo json_encode($jsonResult);
	}

	public function print_x()
	{

		$o = shell_exec("lpstat -d -p");
		$res = explode("\n", $o);
		$i = 0;
		foreach ($res as $r) {
			$active = 0;
			if (strpos($r, "printer") !== FALSE) {
				$r = str_replace("printer ", "", $r);
				if (strpos($r, "is idle") !== FALSE)
					$active = 1;

				$r = explode(" ", $r);

				$printers[$i]['name'] = $r[0];
				$printers[$i]['active'] = $active;
				$i++;
			}
		}
		var_dump($printers);
	}
	/*public function cekpassword_rwj(){
    	$result=$this->db->query("select * from sys_setting where key_data='rwj_password_batal_transaksi_kasir' and setting='".$_POST['passDulu']."'");
		echo $result->row()->setting;
		/* if ($result->row()->setting==$_POST['passDulu'])
		{
			$jsonResult=array();
			$jsonResult['success']='true';
			//$jsonResult['listData']=$result;
		}
		else
		{
			$jsonResult=array();
			$jsonResult['success']='false';
		}
    	
		//$jsonResult['kliningan']=$kliningan;
    	echo json_encode($jsonResult);
    }  */
	public function cekpassword_rwj()
	{
		$q = $this->db->query("select * from sys_setting where key_data='rwj_password_batal_transaksi_kasir' and setting=md5('" . $_POST['passDulu'] . "') ")->result();
		if ($q) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}
	public function getPenyakit()
	{
		// echo "aaa"; die;
		$result = $this->db->query("SELECT TOP 10 penyakit, kd_penyakit from penyakit where (upper(penyakit) like upper('%" . $_POST['text'] . "%') or upper(kd_penyakit) like upper('%" . $_POST['text'] . "%')) and kd_penyakit <> ' ' and kd_penyakit not in(select p.kd_penyakit from penyakit_primer p)")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function getNoIcdPenyakit()
	{
		$result = $this->db->query(" select TOP 10 penyakit, kd_penyakit from penyakit where upper(kd_penyakit) like upper('" . $_POST['text'] . "%') and kd_penyakit not in(select p.kd_penyakit from penyakit_primer p)")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}


	public function getProduk()
	{
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$produk 	= $_POST['text'];
		$kd_unit 	= $_POST['kd_unit'];
		$row 		= $this->db->query("SELECT kd_tarif from tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row();
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row()->kd_tarif;
		$sql = "SELECT 
			row_number() OVER (ORDER BY rn.tgl_berlaku DESC) as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				p.kd_klas,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn ,
				CASE WHEN LEFT (p.kd_klas, 1) = '6' THEN '1' ELSE '0' END AS grup
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_bagian = '" . substr($_POST['kd_unit'], 0, 1) . "'
					AND 
					u.kd_unit  = '" . $_POST['kd_unit'] . "'
					AND (c.kd_customer = '" . $_POST['kd_customer'] . "' OR t.kd_tarif='" . $kd_tarif . "') 
					AND (upper(p.deskripsi) like upper('%" . $_POST['text'] . "%') or CAST(p.kp_produk AS TEXT) LIKE '" . $_POST['text'] . "%') 
					AND t.tgl_berlaku <= '" . date('Y-m-d') . "'
			) as rn 
		WHERE rn = 1
		";
		/*
		 
			Where 
			tarif.kd_unit ='".$_POST['kd_unit']."' 
			and lower(tarif.kd_tarif)=LOWER('".$row->kd_tarif."') 
			and (upper(produk.deskripsi) like upper('%".$_POST['text']."%') or CAST(produk.kp_produk AS TEXT) LIKE '%".$_POST['text']."%') 
			and tarif.tgl_berlaku <= '".date('Y-m-d')."'
		  */
		// echo $sql;
		$result = $this->db->query($sql);


		/*" select * from ( 
		select distinct produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, max (tarif.tgl_berlaku) as tglberlaku,max(tarif.tarif) as tarifx,
		 tarif.tgl_berakhir,tr.jumlah from 
		 produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		 inner join tarif on produk.kd_produk = tarif.kd_produk 
		 inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		 inner join unit on tarif.kd_unit = unit.kd_unit 
		 left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		 tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		 group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir order by produk.deskripsi asc
		 ) as resdata WHERE LOWER(kd_tarif)=LOWER('".$row->kd_tarif."') and kd_unit='".$_POST['kd_unit']."' and tgl_berakhir is null 
		 and tglberlaku<= gettanggalberlakufromklas('TU',now()::date, now()::date,'') and upper(deskripsi) like upper('".$_POST['text']."%') limit 10")->result();
		 
//--- Modul Untuk cek apakah produk mempunyai component jasa dokter atau jasa dokter anastesi ---/
		 
//--- Akhir Modul Untuk cek apakah produk mempunyai component jasa dokter atau jasa dokter anastesi ---//	
	 
    	/* $jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult); */
		$data       = array();
		$jsonResult = array();
		$i          = 0;
		foreach ($result->result_array() as $row) {
			$data[$i]['deskripsi'] 		= $row['deskripsi'];
			// $data[$i]['jumlah'] 	 	= $row['jumlah'];
			// $data[$i]['kd_kat'] 	 	= $row['kd_kat'];
			$data[$i]['kd_klas'] 	 	= $row['kd_klas'];
			$data[$i]['kd_produk'] 	 	= $row['kd_produk'];
			$data[$i]['kp_produk'] 	 	= $row['kp_produk'];
			$data[$i]['kd_tarif'] 	 	= $row['kd_tarif'];
			$data[$i]['kd_unit'] 	 	= $row['kd_unit'];
			$data[$i]['nama_unit'] 	 	= $row['nama_unit'];
			$data[$i]['rn'] 	 		= $row['rn'];
			$data[$i]['rnum'] 	 		= $row['rnum'];
			$data[$i]['tarifx'] 	 	= $row['tarif'];
			$data[$i]['group'] 	 	= $row['grup'];
			$data[$i]['tgl_berlaku'] 	= $row['tgl_berlaku'];
			$data[$i]['tgl_berakhir'] 	= $row['tgl_berakhir'];
			// $data[$i]['klasifikasi'] 	= $row['klasifikasi'];
			// $data[$i]['manual'] 	 	= $row['manual'];
			// $data[$i]['parent'] 	 	= $row['parent'];
			// $data[$i]['qty'] 	 		= $row['qty'];

			/*
				Validasi deskripsi seleksi data apabila mengandung  kata konsultasi ataupun konsul
				apabila terdapat kata berikut maka dikasih nilai true sebaliknya
			 */

			$text = strtolower($data[$i]['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data[$i]['status_konsultasi'] 	= true;
			} else {
				$data[$i]['status_konsultasi'] 	= false;
			}
			$i++;
		}
		$jsonResult['processResult'] 	= 'SUCCESS';
		$jsonResult['listData'] 		= $data;
		echo json_encode($jsonResult);
	}

	public function getProdukKey()
	{
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " tarif.kd_unit ='" . $_POST['kd_unit'] . "' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='" . $_POST['kd_customer'] . "' ";
		}
		$q_text 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_text = "  and produk.kp_produk='" . $_POST['text'] . "' ";
		}
		$row = $this->db->query("select kd_tarif from tarif_cust " . $q_customer)->row();
		$sql = "SELECT row_number() OVER (order by rn.deskripsi asc) as rnum,rn.*, 1 as qty 
		from(
		SELECT TOP 10
			--produk_unit.kd_produk,
			produk.kd_produk, 
			produk.deskripsi, 
			tarif.kd_unit, 
			unit.nama_unit, 
			produk.manual,
			produk.kp_produk, 
			produk.kd_kat, 
			produk.kd_klas, 
			klas_produk.klasifikasi, 
			klas_produk.parent, 
			tarif.kd_tarif, 
			max (tarif.tgl_berlaku) as tglberlaku,
			tarif.tgl_berlaku,
			tarif.tarif as tarifx, 
			tarif.tgl_berakhir,
			tr.jumlah,
			row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
			from produk 
				inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
				inner join tarif on produk.kd_produk = tarif.kd_produk 
				--inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
				inner join unit on tarif.kd_unit = unit.kd_unit 
				left join (
					select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah,kd_tarif,kd_produk,kd_unit,tgl_berlaku
					from tarif_component where kd_component = '20' or kd_component = '21' group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
					tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND tr.kd_tarif=tarif.kd_tarif 
					Where " . $q_unit . " lower(tarif.kd_tarif)=LOWER('" . $row->kd_tarif . "') 
				" . $q_text . "
				" . $q_unit . "
				and tarif.tgl_berlaku <= '" . date('Y-m-d') . "'
				group by 
				--produk_unit.kd_produk,
				produk.deskripsi, 
				tarif.kd_unit,
				unit.nama_unit, 
				produk.manual, 
				produk.kp_produk, 
				produk.kd_kat, 
				produk.kd_klas,
				tr.jumlah,
				klas_produk.klasifikasi, 
				klas_produk.parent, 
				tarif.kd_tarif, 
				tarif.tgl_berakhir ,
				tarif.tarif,
				produk.kd_produk,
				tarif.tgl_berlaku   
				 ) 
		as rn where rn = 1 and tgl_berakhir is null order by rn.deskripsi asc ";

		$jsonResult = array();
		$data       = array();
		$result     = $this->db->query($sql)->row();
		if ($result) {
			$data['deskripsi'] 		= $result->deskripsi;
			$data['jumlah'] 	 	= $result->jumlah;
			$data['kd_kat'] 	 	= $result->kd_kat;
			$data['kd_klas'] 	 	= $result->kd_klas;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['klasifikasi'] 	= $result->klasifikasi;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['manual'] 	 	= $result->manual;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['parent'] 	 	= $result->parent;
			$data['qty'] 	 		= $result->qty;
			$data['rn'] 	 		= $result->rn;
			$data['rnum'] 	 		= $result->rnum;
			$data['tarifx'] 	 	= $result->tarifx;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			$text = strtolower($data['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data['status_konsultasi'] 	= true;
			} else {
				$data['status_konsultasi'] 	= false;
			}
			$jsonResult['processResult'] 	= 'SUCCESS';
		} else {
			$jsonResult['processResult'] 	= 'FAILED';
			$data = null;
		}
		$jsonResult['listData'] 		= $data;
		echo json_encode($jsonResult);
	}

	public function cetakLab()
	{
		$kdKasirrwj = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$common     = $this->common;
		$result     = $this->result;
		$title      = 'RESEP';
		$param      = json_decode($_POST['data']);
		$var        = $param->kd_pasien;
		$unit       = $param->kd_unit;
		$tgl        = $param->tgl;
		$notr       = $param->notr;
		$order_lab  = explode(",", $param->order_lab);
		$order_lab  = array_unique($order_lab);
		$html = '';
		$kodekasir = $this->db->query("SELECT * from kasir where deskripsi='Laboratorium'")->row()->KD_KASIR;

		$html .= '
			 <div style="float:right;text-align: right;">
			  ' . date('d M Y') . '<br>
			 Yth. T. S. Bagian Laboratorium<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>';
		$lab = '';
		// foreach($query as $row){
		// 	$lab.='- '.$row->deskripsi.'<br>';
		// }

		foreach ($order_lab as $key => $value) {
			$query 	= $this->db->query("SELECT dbo.GetAllTestLab('" . $value . "', '" . $kodekasir . "') as deskripsi")->row()->deskripsi;
			$query 	= substr($query, 0, -1);
			$query 	= explode(";", $query);
			foreach ($query as $key => $value) {
				$lab .= "-" . $value . "<br>";
			}
		}

		$pasien = $this->db->query("select kd_pasien, nama , jenis_kelamin, convert(CHAR,tgl_lahir) as tahunlahir from pasien Where kd_pasien='" . $var . "'")->row();
		$JK = 'Perempuan';
		if ($pasien->jenis_kelamin == 't') {
			$JK = 'Laki-laki';
		}
		$html .= ' <br>Mohon Pemeriksaan :<br>' . $lab;
		$tahunskarang = date('Y');
		$tahunlahir = $pasien->tahunlahir;
		$umur = $tahunskarang - $tahunlahir;
		$html .= '
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: ' . $var . '</td>
			</tr>
			<tr>
			<td>Nama</td><td>: ' . $pasien->nama . '</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: ' . $JK . '</td>
			</tr>
			<tr>
			<td>Umur</td><td>: ' . $umur . ' th</td>
			</tr>
			</table>';
		$html .= '<div style="align: right; text-align: right;">
			Wasalam,
			<p><p><p>
			(' . $this->session->userdata['user_id']['username'] . ')
			</div>';
		$html .= '</body></html>';
		$prop = array('foot' => true);
		// echo $html; die;
		$this->common->setPdf('P', 'Laboratorium', $html);
	}

	public function cetakRad()
	{
		$common    = $this->common;
		$result    = $this->result;
		$title     = 'RESEP';
		$param     = json_decode($_POST['data']);
		$var       = $param->kd_pasien;
		$unit      = $param->kd_unit;
		$tgl       = $param->tgl;
		$notr      = $param->notr;
		$order_rad = explode(",", $param->order_rad);
		$order_rad = array_unique($order_rad);
		// echo "<pre>".var_export($order_rad, true)."</pre>"; 
		$html = '';
		$kdKasirrwj = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		/* $query = $this->db->query("select C.deskripsi from mr_radkonsuldtl B 
			LEFT JOIN mr_radkonsul A ON B.id_radkonsul=A.id_radkonsul 
			LEFT JOIN produk C ON C.kd_produk=B.kd_produk 
			LEFT JOIN PASIEN E ON E.KD_PASIEN=A.KD_PASIEN 
			where A.KD_PASIEN = '".$var."' AND A.KD_UNIT = '".$unit."' ")->result(); */
		if ($this->db->query("SELECT * from kasir where deskripsi='Rad. Rawat Jalan'")->num_rows() > 0) {
			$kodekasir 			= $this->db->query("SELECT * from kasir where deskripsi='Rad. Rawat Jalan'")->row()->KD_KASIR;
			$querykodeKasir 	= "and kd_kasir='" . $kodekasir . "'";
		} else {
			$querykodeKasir 	= " ";
		}
		// echo $notr."<br>";
		// echo $kdKasirrwj."<br>";
		// echo $querykodeKasir."<br>";
		$query = $this->db->query("SELECT * from unit_asal where no_transaksi_asal = '" . $notr . "' and kd_kasir_asal = '" . $kdKasirrwj . "' " . $querykodeKasir . "")->result();
		$html .= '<html><head></head><body>';
		$html .= '
			 </div>
			 <div style="float:right;text-align: right;">
			  ' . date('d M Y') . '<br>
			 Yth. T. S. Bagian Radiologi<br>
			 di Tempat.<br>
			 </div>
			 Dengan Hormat,<br>';
		$lab = '';
		// foreach($query as $row){
		// 	$query2 	= $this->db->query("select getalltestrad('".$row->no_transaksi."', '".$row->kd_kasir."') as deskripsi")->result();
		// 	foreach ($query2 as $result) {
		// 		$lab.=' - '.$result->deskripsi.'<br>';
		// 	}
		// }
		// 
		foreach ($order_rad as $key => $value) {
			$query 	= $this->db->query("SELECT dbo.GetAllTestLab('" . $value . "', '" . $kodekasir . "') as deskripsi")->row()->deskripsi;
			$query 	= substr($query, 0, -1);
			$query 	= explode(";", $query);
			foreach ($query as $key => $value) {
				$lab .= "-" . $value . "<br>";
			}
		}
		$pasien = $this->db->query("select kd_pasien, nama , jenis_kelamin, CONVERT(CHAR,tgl_lahir) as tahunlahir  from pasien Where kd_pasien='" . $var . "'")->row();
		$JK = 'Perempuan';
		if ($pasien->jenis_kelamin == 't') {
			$JK = 'Laki-laki';
		}
		$html .= ' <br>Mohon Pemeriksaan :<br>' . $lab;
		$tahunskarang = date('Y');
		$tahunlahir = $pasien->tahunlahir;
		$umur = $tahunskarang - $tahunlahir;
		$html .= '
			<br> Untuk Pasien<br>
			<table>
			<tr>
			<td>No. Medrec</td><td>: ' . $var . '</td>
			</tr>
			<tr>
			<td>Nama</td><td>: ' . $pasien->nama . '</td>
			</tr>
			<tr>
			<td>Jenis Kelamin</td><td>: ' . $JK . '</td>
			</tr>
			<tr>
			<td>Umur</td><td>: ' . $umur . ' th</td>
			</tr>
			</table>';
		$html .= '<div style="align: right; text-align: right;">
			Wasalam,
			<p><p><p>
			(' . $this->session->userdata['user_id']['username'] . ')
			</div>';
		$html .= '</body></html>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Radiologi', $html);
	}

	public function UpdateKdCustomer()
	{

		$KdTransaksi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$KdDokter = $_POST['KdDokter'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$KdCustomer = $_POST['KDCustomer'];
		$NoSjp = $_POST['KDNoSJP'];
		$NoAskes = $_POST['KDNoAskes'];
		$KdPasien = $_POST['TrKodePasien'];

		$query = $this->db->query(" updatekdcostumer @kdpasien='" . $KdPasien . "',@unit='" . $KdUnit . "',@tglmasuk='" . $TglTransaksi . "',@noasuransi='" . $NoAskes . "',@nosjp='" . $NoSjp . "',@kdcustomer='" . $KdCustomer . "' ");
		$res = $query->num_rows();



		if ($res) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}

	public function KonsultasiPenataJasa()
	{
		$kdUnit = $_POST['KdUnit'];
		$kdDokter = $_POST['KdDokter'];
		$kdUnitAsal = $_POST['KdUnitAsal'];
		$kdDokterAsal = $_POST['KdDokterAsal'];
		$tglTransaksi = $_POST['TglTransaksi'];
		$kdCostumer = $_POST['KDCustomer'];
		$kdPasien = $_POST['KdPasien'];
		$antrian = $this->GetAntrian($kdPasien, $kdUnit, $tglTransaksi, $kdDokter);
		$kdKasir = 	 $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$kdTransaksi = $this->GetIdTransaksi($kdKasir);
		$_kduser = $this->session->userdata['user_id']['id'];
		if ($tglTransaksi != "" and $tglTransaksi != "undefined") {
			list($tgl, $bln, $thn) = explode('/', $tglTransaksi, 3);
			$ctgl = strtotime($tgl . $bln . $thn);
			$tgl_masuk = date("Y-m-d", $ctgl);
		}

		$this->db->trans_begin();

		$query = $this->db->query(" insertkonsultasitindaklanjut @kdpasien='" . $kdPasien . "',@kdunit='" . $kdUnit . "',@tglmasuk='" . $tgl_masuk . "',@kddokter='" . $kdDokter . "',@kdcustomer='" . $kdCostumer . "',@kdkasir='" . $kdKasir . "',
		@notransaksi='" . $kdTransaksi . "',@kdunitasal='" . $kdUnitAsal . "',@kddokterasal='" . $kdDokterAsal . "',@urutmasuk=" . $antrian . ",@cara_penerimaan=0,@kduser='" . $_kduser . "' "); # 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
		/* $query_sql = _QMS_Query("exec dbo.V5_insert_konsultasi_tindak_lanjut '".$kdKasir."','".$kdDokter."','".$kdUnit."','".$kdPasien."','".$tgl_masuk."','".$kdCostumer."',
				".$antrian.",0,'".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."','".$_kduser."'"); # 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
		 */
		$res = $query->num_rows();


		# Insert antrian_poliklinik
		if ($res) {
			//$db = $this->load->database('otherdb2',TRUE);

			# pertama kali daftar no_antrian belum digenerate, tapi dari tracer setelah berkas ditemukan.
			// $no_antrian = $this->getNoantrian($Params['Poli']);
			$no_antrian = 0;

			# *** Status pasien lama/baru untuk identifikasi tracer ***
			# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
			$kunj = $this->db->query("select * from kunjungan where kd_pasien='" . $kdPasien . "' ");
			if ($kunj->num_rows() == 0) {
				$baru = 1;
			} else {
				$baru = 0;
			}
			date_default_timezone_set('Asia/Makassar');
			$TglAntrian = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
			$conv_tglmasuk = date('Y/m/d');
			$tglmasuk = date('Y-m-d');
			$kdunitprioritas = $this->db->query("select setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'")->row()->setting;
			$sqlTracer_no_antrian_prioritas = $this->db->query("select count(*) as jum_prioritas 
														from kunjungan 
														where tgl_masuk='" . $tglmasuk . "' and kd_unit in(" . $kdunitprioritas . ")")->row()->jum_prioritas;
			if ($sqlTracer_no_antrian_prioritas == 0) {
				$no_prioritas = 1;
			} else {
				$no_prioritas = $sqlTracer_no_antrian_prioritas + 1;
			}

			# No Antrian Cetak
			$cek_dulu_atrian_polinya = $this->db->query("select TOP 1 no_urut from antrian_poliklinik 
			where CAST(tgl_transaksi as date) ='" . $conv_tglmasuk . "' and kd_unit='" . $kdUnit . "' order by no_urut desc ")->result();
			if (count($cek_dulu_atrian_polinya) == 0) {
				$urutCetak = 1;
			} else {
				$sqlTracer_no_antrian_poli = $this->db->query("select TOP 1 no_urut from antrian_poliklinik 
				where CAST(tgl_transaksi as date) ='" . $conv_tglmasuk . "' and kd_unit='" . $kdUnit . "' order by no_urut desc")->row()->no_urut;
				if ($sqlTracer_no_antrian_poli == 0) {
					$urutCetak = 1;
				} else {
					$urutCetak = $sqlTracer_no_antrian_poli + 1;
					//echo $urutCetak;
				}
			}


			# Update antrian poliklinik set no_urut dan no_prioritas


			/* $q_simpan_antrian_poli_ss=$db->query("insert into antrian_poliklinik 
								 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
								 ('".$kdUnit."','".$TglAntrian."',".$urutCetak.",'".$kdPasien."',$_kduser,0,0,".$no_antrian.",".$no_prioritas.")");  */
			$q_simpan_antrian_poli = $this->db->query("insert into antrian_poliklinik 
								 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
								 ('" . $kdUnit . "','" . $tglmasuk . "'," . $urutCetak . ",'" . $kdPasien . "',$_kduser,0,0," . $no_antrian . "," . $no_prioritas . " )");

			/* $no = $this->db->query("select max(no_urut) as no_urut from antrian_poliklinik order by no_urut desc limit 1");
			if(count($no) > 0){
				$no_urut = $no->row()->no_urut + 1;
			} else{
				$no_urut = 1;
			}
			$kd_user =$this->session->userdata['user_id']['id'];
			$dataantrian = array('kd_unit'=>$kdUnit,'tgl_transaksi'=>date('Y-m-d'),
								'kd_pasien'=>$kdPasien,'no_urut'=>$no_urut,
								'kd_user'=>$kd_user,'id_status_antrian'=>1,
								'sts_ditemukan'=>'false');
			$insert = $this->db->insert('antrian_poliklinik',$dataantrian); */

			/*
				AUTO CHARGE OLEH HADAD
			 */
			$paramsCriteria = array(
				'no_transaksi' 	=> $this->input->post('TrKodeTranskasi'),
				'kd_kasir' 		=> '01',
			);

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			unset($paramsCriteria);
			$paramsCriteria = array(
				'kd_pasien' 	=> $query_transaksi->row()->KD_PASIEN,
				'tgl_masuk' 	=> $query_transaksi->row()->TGL_TRANSAKSI,
				'kd_unit' 		=> $query_transaksi->row()->KD_UNIT,
				'urut_masuk' 	=> $query_transaksi->row()->URUT_MASUK,
			);

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("kunjungan");
			$query_kunjungan = $this->db->get();

			/*$this->db->where($paramsCriteria);
				$query_kunjungan = $this->db->select('kunjungan');*/

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("rujukan_kunjungan");
			$query_rujukan = $this->db->get();
			// $query_rujukan   = $this->db->select('rujukan_kunjungan');

			if ($query_rujukan->num_rows() > 0) {
				$pasien_baru_rujukan = '1';
				// $pasien_baru_rujukan = true;
			} else {
				$pasien_baru_rujukan = '0';
				// $pasien_baru_rujukan = false;
			}

			if ($query_kunjungan->row()->CARA_PENERIMAAN != '99') {
				$rujukan_pasien = '1';
				// $rujukan_pasien = true;
			} else {
				$rujukan_pasien = '0';
				// $rujukan_pasien = false;
			}

			$querygetappto = $this->db->query(" getappto @App='0',@PasienBaru='" . $pasien_baru_rujukan . "',@Rujukan='" . $rujukan_pasien . "',@Kontrol='0',@CetakKartu='0',@JenisRujukan='0' ")->row()->getappto;
			$kdtarifcus    = $this->db->query(" getkdtarifcus @KdCus='" . $kdCostumer . "'")->row()->getkdtarifcus;
			$Shiftbagian   = (int)$this->GetShiftBagian();

			$query_tgl_berlaku = $this->db->query(" gettanggalberlakufromklas @KdTarif='$kdtarifcus',@TglBerlaku='" . date('Y-m-d') . "',@TglBerakhir='" . date('Y-m-d') . "',@KdKlas='71' ");

			if ($query_tgl_berlaku->num_rows() > 0) {
				$tgl_berlaku = $query_tgl_berlaku->row()->gettanggalberlakufromklas;
			} else {
				$tgl_berlaku = date('Y-m-d');
			}

			$pg_query = "INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,kd_unit_tr,
							tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							SELECT  
								'$kdKasir',
								'$kdTransaksi',
								row_number() OVER (order by rn.tglberlaku desc) as rnum,
								'" . date('Y-m-d') . "',
								" . $_kduser . ",
								rn.kd_tarif,
								rn.kd_produk,
								rn.kd_unit,
								rn.kd_unit,
								rn.tglberlaku,
								'true',
								'true',
								'A'
								,1,
								rn.tarifx,
								" . $Shiftbagian . ",
								'false',
								''  
							FROM(
								Select 
									AutoCharge.appto,
									tarif.kd_tarif,	
									AutoCharge.kd_produk,
									AutoCharge.kd_unit,
									max (tarif.tgl_berlaku) as tglberlaku,
									tarif.tarif as tarifx,
									row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
								FROM AutoCharge 
								inner join tarif on 
									tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
								inner join produk on produk.kd_produk = tarif.kd_produk  
								Where 
									AutoCharge.kd_unit='" . $kdUnit . "'
									and AutoCharge.appto in $querygetappto
									and LOWER(kd_tarif) = LOWER('$kdtarifcus')
									and tgl_berlaku <= '$tgl_berlaku'
									group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
									tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku 
								
								) as rn
							WHERE rn = 1";
			$q_simpan_antrian_poli = $this->db->query($pg_query);


			$sql_SS = "INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,kd_unit_tr,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							select top 1 
								'$kdKasir',
								'$kdTransaksi',
								1,
								'" . date('Y-m-d') . "',
								" . $_kduser . ",
								rn.kd_tarif,
								rn.kd_produk,
								rn.kd_unit,
								rn.kd_unit,
								rn.tglberlaku,
								1, 
								1, 
								'A'
								,1,
								rn.tarifx,
								" . $Shiftbagian . ",
								0,
								''  
							FROM( 
								SELECT 
									AutoCharge.appto, 
									tarif.kd_tarif, 
									AutoCharge.kd_produk, 
									AutoCharge.kd_unit, 
									max (tarif.tgl_berlaku) as tglberlaku, 
									tarif.tarif as tarifx
								FROM AutoCharge 
								inner join tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit 
								inner join produk on produk.kd_produk = tarif.kd_produk 
								Where 
									AutoCharge.kd_unit='" . $kdUnit . "'
									and AutoCharge.appto in $querygetappto
									and LOWER(kd_tarif) = LOWER('$kdtarifcus')
									and tgl_berlaku <= '$tgl_berlaku'
									group by AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,
									tarif.tgl_berlaku
									)
									as rn order by tglberlaku desc";

			// $q_simpan_antrian_poli_ss = $this->dbSQL->query($sql_SS);


			$query_data = $this->db->query("SELECT * 
                    	from(
							Select 
								row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
								tarif.kd_tarif,
								AutoCharge.kd_produk,
								AutoCharge.kd_unit,
								max (tarif.tgl_berlaku) as tglberlaku,
								max(tarif.tarif) as tarifx
							From AutoCharge 
							inner join tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
							inner join produk on produk.kd_produk = tarif.kd_produk  
							Where 
								AutoCharge.kd_unit='" . $kdUnit . "'
								and AutoCharge.appto in $querygetappto
								and LOWER(kd_tarif) = LOWER('$kdtarifcus')
								and tgl_berlaku <= '$tgl_berlaku'
							group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
							tarif.kd_tarif ) as  rn where rn.rnum=1");

			if ($query_data->num_rows() > 0) {
				$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
				$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
				foreach ($query_data->result() as $row) {
					$kdprodukpasien   = $row->kd_produk;
					$kdTarifpasien    = $row->kd_tarif;
					$tglberlakupasien = $row->tglberlaku;
					$kd_unitpasein    = $row->kd_unit;
					$urutpasein       = $row->rnum;
					$sqlComponent = "INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								Select '$kdKasir','$kdTransaksi',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
								From Tarif_Component 
								Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien";
					//echo $sqlComponent;
					//$q_simpan_antrian_poli_ss 	= $this->dbSQL->query($sqlComponent);
					$q_simpan_antrian_poli 		= $this->db->query("INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								Select '$kdKasir','$kdTransaksi',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
								From Tarif_Component 
								Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien");

					if ($q_simpan_antrian_poli) {

						//echo "LOLOLO";
						/* $data_tarif=_QMS_Query("Select * From Tarif_Component
									Where 
									(KD_Unit='$kd_unitpasein' 
									And Tgl_Berlaku='$tglberlakupasien'
									And KD_Tarif='$kdTarifpasien'
									And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasadok')
									 OR   (KD_Unit='$kd_unitpasein' 
									 And Tgl_Berlaku='$tglberlakupasien'
									 And KD_Tarif='$kdTarifpasien'
									 And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasaanas')");
								$getkodejob=_QMS_Query("select kd_job from dokter_inap_int where kd_component='$kdjasadok'")->row()->kd_job;
								if (count($data_tarif->result())>0){
									$q_simpan_antrian_poli_ss 	= $this->dbSQL->query("insert into detail_trdokter (kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,kd_job,jp,pajak,bayar)
												values
												('$kdKasir','$kdTransaksi',$urutpasein,'".$kdDokter."','".date('Y-m-d')."',0,".$data_tarif->row()->TARIF.",0,0)");
								}else{
									$strError="tidak";
								} */

						$data_tarifpg = $this->db->query("Select * From Tarif_Component
									Where 
									(KD_Unit='$kd_unitpasein' 
									And Tgl_Berlaku='$tglberlakupasien'
									And KD_Tarif='$kdTarifpasien'
									And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasadok')
									 OR   (KD_Unit='$kd_unitpasein' 
									 And Tgl_Berlaku='$tglberlakupasien'
									 And KD_Tarif='$kdTarifpasien'
									 And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasaanas')");
						$getkodejobpg = $this->db->query("select kd_job from dokter_inap_int where kd_component='$kdjasadok'")->row()->kd_job;
						if (count($data_tarifpg->result()) > 0) {
							$q_simpan_antrian_poli = $this->db->query("insert into detail_trdokter (kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,kd_component,jp,pajak,bayar)
												values
												('$kdKasir','$kdTransaksi',$urutpasein,'" . $kdDokter . "','" . date('Y-m-d') . "',$kdjasadok," . $data_tarifpg->row()->tarif . ",0,0)");
						} else {
							$strError = "tidak";
						}
					} else {
						echo "LALALA";
					}
				}
			}
			/*
				AUTO CHARGE OLEH HADAD
			 */


			/*
				AUTO PAID OLEH HADAD
			 */
			$paid = $this->db->query("SELECT auto_paid from zusers where kd_user = '" . $_kduser . "'")->row()->auto_paid;

			if ($paid == 1) {
				# postgre
				$sqlDetailTransaksi = "SELECT * FROM detail_transaksi WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'";
				$resDetailTransaksi = $this->db->query($sqlDetailTransaksi)->result();
				$totBayar = 0;
				for ($i = 0, $iLen = count($resDetailTransaksi); $i < $iLen; $i++) {
					$totBayar += ((int)$resDetailTransaksi[$i]->qty * (int)$resDetailTransaksi[$i]->harga);
				}
				if ($totBayar > 0) {
					$sqlPay = "SELECT kd_pay FROM payment WHERE kd_customer='" . $kdCostumer . "'";
					$resPay = $this->db->query($sqlPay)->row();
					// # sql
					/* $sql="EXEC dbo.v5_inserttrpembayaran '".$kdKasir."','".$kdTransaksi."',1,'".date('Y-m-d')."','".$_kduser."','".$kdUnit."',
							'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",1,'".date('Y-m-d')."',1";
						 $q_simpan_antrian_poli_ss 	= $this->dbSQL->query($sql);
						$sql="EXEC dbo.v5_insertpembayaran '".$kdKasir."','".$kdTransaksi."',1,'".date('Y-m-d')."','".$_kduser."','".$kdUnit."',
							 '".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",1,'".date('Y-m-d')."',1";
							// echo $sql;
						 $q_simpan_antrian_poli_ss 	= $this->dbSQL->query($sql); */

					$sql_pg = " inserttrpembayaran @kdkasir='" . $kdKasir . "',@notransaksi='" . $kdTransaksi . "',@urut=1,@tgltransaksi='" . date('Y-m-d') . "',@kduser='" . $_kduser . "',@kdunit='" . $kdUnit . "',
						@kdpay='" . $resPay->kd_pay . "',@jumlah=" . $totBayar . ",@folio='A',@shift=" . $Shiftbagian . ",@statusbayar='1',@tglbayar='" . date('Y-m-d') . "',@urutbayar=1 ";
					$q_simpan_antrian_poli = $this->db->query($sql_pg);

					$sql_pg = " insertpembayaran @kdkasir='" . $kdKasir . "',@notransaksi='" . $kdTransaksi . "',@urut=1,@tgltransaksi='" . date('Y-m-d') . "',@kduser='" . $_kduser . "',@kdunit='" . $kdUnit . "',
						@kdpay='" . $resPay->kd_pay . "',@jumlah=" . $totBayar . ",@folio='A',@shift=" . $Shiftbagian . ",@statusbayar='1',@tglbayar='" . date('Y-m-d') . "',@urutbayar = 1 ";
					$q_simpan_antrian_poli = $this->db->query($sql_pg);
				}

				# ***Update Auto paid***
				# SQLSERVER
				/* $updatedetailtransaksisql = $this->dbSQL->query("update detail_transaksi set tag='1' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
					$updatedetailbayarsql = $this->dbSQL->query("update detail_bayar set tag='1' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
					 */
				# POSTGRE
				$updatedetailtransaksipg = $this->db->query("update detail_transaksi set tag='t' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
				$updatedetailbayarpg = $this->db->query("update detail_bayar set tag='t' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
			}
			/*
				AUTO PAID OLEH HADAD
			 */
			if ($q_simpan_antrian_poli) {
				$this->db->trans_commit();
				echo "{success: true, notrans:'$kdTransaksi'}";
			} else {
				$this->db->trans_rollback();
				echo '{success: false}';
			}
		} else {
			$this->db->trans_rollback();
			echo '{success: false}';
		}
	}
	public function KonsultasiPenataJasa_LAMA()
	{
		$kdUnit = $_POST['KdUnit'];
		$kdDokter = $_POST['KdDokter'];
		$kdUnitAsal = $_POST['KdUnitAsal'];
		$kdDokterAsal = $_POST['KdDokterAsal'];
		$tglTransaksi = $_POST['TglTransaksi'];
		$kdCostumer = $_POST['KDCustomer'];
		$kdPasien = $_POST['KdPasien'];
		$antrian = $this->GetAntrian($kdPasien, $kdUnit, $tglTransaksi, $kdDokter);
		$kdKasir = 	 $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$kdTransaksi = $this->GetIdTransaksi($kdKasir);
		$_kduser = $this->session->userdata['user_id']['id'];
		if ($tglTransaksi != "" and $tglTransaksi != "undefined") {
			list($tgl, $bln, $thn) = explode('/', $tglTransaksi, 3);
			$ctgl = strtotime($tgl . $bln . $thn);
			$tgl_masuk = date("Y-m-d", $ctgl);
		}

		$this->db->trans_begin();

		$query = $this->db->query("select insertkonsultasitindaklanjut('" . $kdPasien . "','" . $kdUnit . "','" . $tgl_masuk . "','" . $kdDokter . "','" . $kdCostumer . "','" . $kdKasir . "',
				'" . $kdTransaksi . "','" . $kdUnitAsal . "','" . $kdDokterAsal . "'," . $antrian . ",0,'" . $_kduser . "')"); # 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
		/* $query_sql = _QMS_Query("exec dbo.V5_insert_konsultasi_tindak_lanjut '".$kdKasir."','".$kdDokter."','".$kdUnit."','".$kdPasien."','".$tgl_masuk."','".$kdCostumer."',
				".$antrian.",0,'".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."','".$_kduser."'"); # 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
		 */
		$res = $query->result();

		# Insert antrian_poliklinik
		if ($res && $query_sql) {
			//$db = $this->load->database('otherdb2',TRUE);

			# pertama kali daftar no_antrian belum digenerate, tapi dari tracer setelah berkas ditemukan.
			// $no_antrian = $this->getNoantrian($Params['Poli']);
			$no_antrian = 0;

			# *** Status pasien lama/baru untuk identifikasi tracer ***
			# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
			$kunj = $this->db->query("select * from kunjungan where kd_pasien='" . $kdPasien . "' ");
			if ($kunj->num_rows() == 0) {
				$baru = 1;
			} else {
				$baru = 0;
			}
			date_default_timezone_set('Asia/Makassar');
			$TglAntrian = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
			$conv_tglmasuk = date('Y/m/d');
			$tglmasuk = date('Y-m-d');
			$kdunitprioritas = $this->db->query("select setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'")->row()->setting;
			$sqlTracer_no_antrian_prioritas = _QMS_Query("select count(*) as jum_prioritas 
														from kunjungan 
														where tgl_masuk='" . $tglmasuk . "' and kd_unit in(" . $kdunitprioritas . ")")->row()->jum_prioritas;
			if ($sqlTracer_no_antrian_prioritas == 0) {
				$no_prioritas = 1;
			} else {
				$no_prioritas = $sqlTracer_no_antrian_prioritas + 1;
			}

			# No Antrian Cetak
			$cek_dulu_atrian_polinya = _QMS_Query("select top 1 no_urut from antrian_poliklinik 
			where convert(varchar(11),tgl_transaksi,111) ='" . $conv_tglmasuk . "' and kd_unit='" . $kdUnit . "' order by no_urut desc")->result();
			if (count($cek_dulu_atrian_polinya) == 0) {
				$urutCetak = 1;
			} else {
				$sqlTracer_no_antrian_poli = _QMS_Query("select top 1 no_urut from antrian_poliklinik 
				where convert(varchar(11),tgl_transaksi,111) ='" . $conv_tglmasuk . "' and kd_unit='" . $kdUnit . "' order by no_urut desc")->row()->no_urut;
				if ($sqlTracer_no_antrian_poli == 0) {
					$urutCetak = 1;
				} else {
					$urutCetak = $sqlTracer_no_antrian_poli + 1;
					//echo $urutCetak;
				}
			}


			# Update antrian poliklinik set no_urut dan no_prioritas


			$q_simpan_antrian_poli_ss = $db->query("insert into antrian_poliklinik 
								 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
								 ('" . $kdUnit . "','" . $TglAntrian . "'," . $urutCetak . ",'" . $kdPasien . "',$_kduser,0,0," . $no_antrian . "," . $no_prioritas . ")");
			$q_simpan_antrian_poli = $this->db->query("insert into antrian_poliklinik 
								 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
								 ('" . $kdUnit . "','" . $tglmasuk . "'," . $urutCetak . ",'" . $kdPasien . "',$_kduser,0,FALSE," . $no_antrian . "," . $no_prioritas . " )");

			/* $no = $this->db->query("select max(no_urut) as no_urut from antrian_poliklinik order by no_urut desc limit 1");
			if(count($no) > 0){
				$no_urut = $no->row()->no_urut + 1;
			} else{
				$no_urut = 1;
			}
			$kd_user =$this->session->userdata['user_id']['id'];
			$dataantrian = array('kd_unit'=>$kdUnit,'tgl_transaksi'=>date('Y-m-d'),
								'kd_pasien'=>$kdPasien,'no_urut'=>$no_urut,
								'kd_user'=>$kd_user,'id_status_antrian'=>1,
								'sts_ditemukan'=>'false');
			$insert = $this->db->insert('antrian_poliklinik',$dataantrian); */

			/*
				AUTO CHARGE OLEH HADAD
			 */
			$paramsCriteria = array(
				'no_transaksi' 	=> $this->input->post('TrKodeTranskasi'),
				'kd_kasir' 		=> '01',
			);

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("transaksi");
			$query_transaksi = $this->db->get();

			unset($paramsCriteria);
			$paramsCriteria = array(
				'kd_pasien' 	=> $query_transaksi->row()->kd_pasien,
				'tgl_masuk' 	=> $query_transaksi->row()->tgl_transaksi,
				'kd_unit' 		=> $query_transaksi->row()->kd_unit,
				'urut_masuk' 	=> $query_transaksi->row()->urut_masuk,
			);

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("kunjungan");
			$query_kunjungan = $this->db->get();

			/*$this->db->where($paramsCriteria);
				$query_kunjungan = $this->db->select('kunjungan');*/

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("rujukan_kunjungan");
			$query_rujukan = $this->db->get();
			// $query_rujukan   = $this->db->select('rujukan_kunjungan');

			if ($query_rujukan->num_rows() > 0) {
				$pasien_baru_rujukan = '1';
				// $pasien_baru_rujukan = true;
			} else {
				$pasien_baru_rujukan = '0';
				// $pasien_baru_rujukan = false;
			}

			if ($query_kunjungan->row()->cara_penerimaan != '99') {
				$rujukan_pasien = '1';
				// $rujukan_pasien = true;
			} else {
				$rujukan_pasien = '0';
				// $rujukan_pasien = false;
			}

			$querygetappto = $this->db->query("SELECT getappto(FALSE,'" . $pasien_baru_rujukan . "','" . $rujukan_pasien . "',FALSE,FALSE,'0')")->row()->getappto;
			$kdtarifcus    = $this->db->query(" getkdtarifcus @KdCus='" . $kdCostumer . "' ")->row()->getkdtarifcus;
			$Shiftbagian   = (int)$this->GetShiftBagian();

			$query_tgl_berlaku = $this->db->query("SELECT gettanggalberlakufromklas('$kdtarifcus','" . date('Y-m-d') . "','" . date('Y-m-d') . "','71')");

			if ($query_tgl_berlaku->num_rows() > 0) {
				$tgl_berlaku = $query_tgl_berlaku->row()->gettanggalberlakufromklas;
			} else {
				$tgl_berlaku = date('Y-m-d');
			}

			$pg_query = "INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,kd_unit_tr,
							tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							SELECT  
								'$kdKasir',
								'$kdTransaksi',
								row_number() OVER () as rnum,
								'" . date('Y-m-d') . "',
								" . $_kduser . ",
								rn.kd_tarif,
								rn.kd_produk,
								rn.kd_unit,
								rn.kd_unit,
								rn.tglberlaku,
								'true',
								'true',
								'A'
								,1,
								rn.tarifx,
								" . $Shiftbagian . ",
								'false',
								''  
							FROM(
								Select 
									AutoCharge.appto,
									tarif.kd_tarif,	
									AutoCharge.kd_produk,
									AutoCharge.kd_unit,
									max (tarif.tgl_berlaku) as tglberlaku,
									tarif.tarif as tarifx,
									row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
								FROM AutoCharge 
								inner join tarif on 
									tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
								inner join produk on produk.kd_produk = tarif.kd_produk  
								Where 
									AutoCharge.kd_unit='" . $kdUnit . "'
									and AutoCharge.appto in $querygetappto
									and LOWER(kd_tarif) = LOWER('$kdtarifcus')
									and tgl_berlaku <= '$tgl_berlaku'
									group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
									tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku 
								order by tglberlaku desc
								) as rn
							WHERE rn = 1";
			$q_simpan_antrian_poli = $this->db->query($pg_query);


			$sql_SS = "INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,kd_unit_tr,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							select top 1 
								'$kdKasir',
								'$kdTransaksi',
								1,
								'" . date('Y-m-d') . "',
								" . $_kduser . ",
								rn.kd_tarif,
								rn.kd_produk,
								rn.kd_unit,
								rn.kd_unit,
								rn.tglberlaku,
								1, 
								1, 
								'A'
								,1,
								rn.tarifx,
								" . $Shiftbagian . ",
								0,
								''  
							FROM( 
								SELECT 
									AutoCharge.appto, 
									tarif.kd_tarif, 
									AutoCharge.kd_produk, 
									AutoCharge.kd_unit, 
									max (tarif.tgl_berlaku) as tglberlaku, 
									tarif.tarif as tarifx
								FROM AutoCharge 
								inner join tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit 
								inner join produk on produk.kd_produk = tarif.kd_produk 
								Where 
									AutoCharge.kd_unit='" . $kdUnit . "'
									and AutoCharge.appto in $querygetappto
									and LOWER(kd_tarif) = LOWER('$kdtarifcus')
									and tgl_berlaku <= '$tgl_berlaku'
									group by AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,
									tarif.tgl_berlaku
									)
									as rn order by tglberlaku desc";

			$q_simpan_antrian_poli_ss = $this->dbSQL->query($sql_SS);


			$query_data = $this->db->query("SELECT * 
                    	from(
							Select 
								row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
								tarif.kd_tarif,
								AutoCharge.kd_produk,
								AutoCharge.kd_unit,
								max (tarif.tgl_berlaku) as tglberlaku,
								max(tarif.tarif) as tarifx
							From AutoCharge 
							inner join tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
							inner join produk on produk.kd_produk = tarif.kd_produk  
							Where 
								AutoCharge.kd_unit='" . $kdUnit . "'
								and AutoCharge.appto in $querygetappto
								and LOWER(kd_tarif) = LOWER('$kdtarifcus')
								and tgl_berlaku <= '$tgl_berlaku'
							group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
							tarif.kd_tarif order by AutoCharge.kd_produk asc) as  rn where rn.rnum=1");

			if ($query_data->num_rows() > 0) {
				$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
				$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
				foreach ($query_data->result() as $row) {
					$kdprodukpasien   = $row->kd_produk;
					$kdTarifpasien    = $row->kd_tarif;
					$tglberlakupasien = $row->tglberlaku;
					$kd_unitpasein    = $row->kd_unit;
					$urutpasein       = $row->rnum;
					$sqlComponent = "INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								Select '$kdKasir','$kdTransaksi',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
								From Tarif_Component 
								Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien";
					//echo $sqlComponent;
					$q_simpan_antrian_poli_ss 	= $this->dbSQL->query($sqlComponent);
					$q_simpan_antrian_poli 		= $this->db->query("INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								Select '$kdKasir','$kdTransaksi',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
								From Tarif_Component 
								Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien");

					if ($q_simpan_antrian_poli && $q_simpan_antrian_poli_ss) {

						//echo "LOLOLO";
						$data_tarif = _QMS_Query("Select * From Tarif_Component
									Where 
									(KD_Unit='$kd_unitpasein' 
									And Tgl_Berlaku='$tglberlakupasien'
									And KD_Tarif='$kdTarifpasien'
									And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasadok')
									 OR   (KD_Unit='$kd_unitpasein' 
									 And Tgl_Berlaku='$tglberlakupasien'
									 And KD_Tarif='$kdTarifpasien'
									 And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasaanas')");
						$getkodejob = _QMS_Query("select kd_job from dokter_inap_int where kd_component='$kdjasadok'")->row()->kd_job;
						if (count($data_tarif->result()) > 0) {
							$q_simpan_antrian_poli_ss 	= $this->dbSQL->query("insert into detail_trdokter (kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,kd_job,jp,pajak,bayar)
												values
												('$kdKasir','$kdTransaksi',$urutpasein,'" . $kdDokter . "','" . date('Y-m-d') . "',0," . $data_tarif->row()->TARIF . ",0,0)");
						} else {
							$strError = "tidak";
						}

						$data_tarifpg = $this->db->query("Select * From Tarif_Component
									Where 
									(KD_Unit='$kd_unitpasein' 
									And Tgl_Berlaku='$tglberlakupasien'
									And KD_Tarif='$kdTarifpasien'
									And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasadok')
									 OR   (KD_Unit='$kd_unitpasein' 
									 And Tgl_Berlaku='$tglberlakupasien'
									 And KD_Tarif='$kdTarifpasien'
									 And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasaanas')");
						$getkodejobpg = $this->db->query("select kd_job from dokter_inap_int where kd_component='$kdjasadok'")->row()->kd_job;
						if (count($data_tarifpg->result()) > 0) {
							$q_simpan_antrian_poli = $this->db->query("insert into detail_trdokter (kd_kasir,no_transaksi,urut,kd_dokter,tgl_transaksi,kd_component,jp,pajak,bayar)
												values
												('$kdKasir','$kdTransaksi',$urutpasein,'" . $kdDokter . "','" . date('Y-m-d') . "',0," . $data_tarifpg->row()->tarif . ",0,0)");
						} else {
							$strError = "tidak";
						}
					} else {
						echo "LALALA";
					}
				}
			}
			/*
				AUTO CHARGE OLEH HADAD
			 */


			/*
				AUTO PAID OLEH HADAD
			 */
			$paid = $this->db->query("SELECT auto_paid from zusers where kd_user = '" . $_kduser . "'")->row()->auto_paid;

			if ($paid == 1) {
				# postgre
				$sqlDetailTransaksi = "SELECT * FROM detail_transaksi WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'";
				$resDetailTransaksi = $this->db->query($sqlDetailTransaksi)->result();
				$totBayar = 0;
				for ($i = 0, $iLen = count($resDetailTransaksi); $i < $iLen; $i++) {
					$totBayar += ((int)$resDetailTransaksi[$i]->qty * (int)$resDetailTransaksi[$i]->harga);
				}
				if ($totBayar > 0) {
					$sqlPay = "SELECT kd_pay FROM payment WHERE kd_customer='" . $kdCostumer . "'";
					$resPay = $this->db->query($sqlPay)->row();
					// # sql
					$sql = "EXEC dbo.v5_inserttrpembayaran '" . $kdKasir . "','" . $kdTransaksi . "',1,'" . date('Y-m-d') . "','" . $_kduser . "','" . $kdUnit . "',
							'" . $resPay->kd_pay . "'," . $totBayar . ",'A'," . $Shiftbagian . ",1,'" . date('Y-m-d') . "',1";
					$q_simpan_antrian_poli_ss 	= $this->dbSQL->query($sql);
					$sql = "EXEC dbo.v5_insertpembayaran '" . $kdKasir . "','" . $kdTransaksi . "',1,'" . date('Y-m-d') . "','" . $_kduser . "','" . $kdUnit . "',
							 '" . $resPay->kd_pay . "'," . $totBayar . ",'A'," . $Shiftbagian . ",1,'" . date('Y-m-d') . "',1";
					// echo $sql;
					$q_simpan_antrian_poli_ss 	= $this->dbSQL->query($sql);

					$sql_pg = "SELECT inserttrpembayaran('" . $kdKasir . "','" . $kdTransaksi . "',1,'" . date('Y-m-d') . "','" . $_kduser . "','" . $kdUnit . "',
							'" . $resPay->kd_pay . "'," . $totBayar . ",'A'," . $Shiftbagian . ",true,'" . date('Y-m-d') . "',1)";
					$q_simpan_antrian_poli = $this->db->query($sql_pg);
					$sql_pg = "SELECT insertpembayaran('" . $kdKasir . "','" . $kdTransaksi . "',1,'" . date('Y-m-d') . "','" . $_kduser . "','" . $kdUnit . "',
							'" . $resPay->kd_pay . "'," . $totBayar . ",'A'," . $Shiftbagian . ",true,'" . date('Y-m-d') . "',1)";
					$q_simpan_antrian_poli = $this->db->query($sql_pg);
				}

				# ***Update Auto paid***
				# SQLSERVER
				$updatedetailtransaksisql = $this->dbSQL->query("update detail_transaksi set tag='1' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
				$updatedetailbayarsql = $this->dbSQL->query("update detail_bayar set tag='1' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");

				# POSTGRE
				$updatedetailtransaksipg = $this->db->query("update detail_transaksi set tag='t' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
				$updatedetailbayarpg = $this->db->query("update detail_bayar set tag='t' WHERE kd_kasir='$kdKasir' AND no_transaksi='$kdTransaksi'");
			}
			/*
				AUTO PAID OLEH HADAD
			 */
			if ($q_simpan_antrian_poli && $q_simpan_antrian_poli_ss) {
				$this->db->trans_commit();
				echo "{success: true, notrans:'$kdTransaksi'}";
			} else {
				$this->db->trans_rollback();
				echo '{success: false}';
			}
		} else {
			$this->db->trans_rollback();
			echo '{success: false}';
		}
	}


	private function GetAntrian($medrec, $Poli, $Tgl, $Dokter)
	{
		$retVal = 1;
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '" . $medrec . "' and kd_unit = '" . $Poli . "' and tgl_masuk = '" . $Tgl . "'and kd_dokter = '" . $Dokter . "'", null, false);
		$res = $this->tb_getantrian->GetRowList(0, 1, "DESC", "no_transaksi",  "");
		if ($res[1] > 0) {
			$nm = $res[0][0]->URUT_MASUK;
			$retVal = $nm;
		}
		return $retVal;
	}

	private function GetIdTransaksi($kd_kasir)
	{

		$kdKasirrwj = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kdKasirrwj'")->row();
		$no = $counter->counter;
		$retVal2 = $no + 1;
		$strNomor = '';
		$update = $this->db->query("update kasir set counter=$retVal2, lastdate='" . date('Y-m-d') . "' where kd_kasir='$kd_kasir'");
		$retVal = $strNomor . str_pad($retVal2, 7, "000000", STR_PAD_LEFT);
		return $retVal;
	}


	public function savePembayaran()
	{
		// $kd_pay = $this->db->query("select kd_pay from payment where kd_customer='".$_POST['KdCustomer']."'")->row()->kd_pay;
		$kd_pay = $this->input->post('bayar');

		$this->M_pembayaran->savePembayaran(
			'default_kd_kasir_rwj',
			$_POST['TrKodeTranskasi'],
			$_POST['Tgl'],
			$this->GetShiftBagian(),
			$_POST['Flag'],
			//date('Y-m-d'),
			$_POST['TglBayar'],
			$_POST['List'],
			$_POST['JmlList'],
			$_POST['kdUnit'],
			$_POST['Typedata'],
			//$kd_pay,
			$_POST['bayar'],
			$_POST['Totalbayar'],
			$this->session->userdata['user_id']['id']
		);



		/* $kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $this->GetShiftBagian();
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		$this->db->trans_begin();
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		}  else {
			foreach($det_query->result() as $det)
			{
					$urut_detailbayar = $det->urutan+1;
			}
		}
                
		if($jmllist > 1) {
			$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++) {
				$b = explode("@@##$$@@",$a[$i]);
				for($k=0;$k<=count($b)-1;$k++)
				{		
					$_kdproduk = $b[1];
					$_qty = $b[2];
					$_harga = $b[3];
					$_urut = $b[5];
					
					if($_Typedata == 0) {
						$harga = $b[6];
					} else if($_Typedata == 1) {
						$harga = $b[8];	
					} else if ($_Typedata == 3) {
						$harga = $b[7];	
					}
				}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
				
				//echo " urutan bayar : ".$urutanbayar;
                                        
					
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",
									'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
				$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
									".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
									".$urut_detailbayar.")")->result();


			}
		} else {
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++) {
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];

				if($_Typedata == 0) {
					$harga = $b[6];
				}
				
				if($_Typedata == 1) {
					$harga = $b[8];	
				}
				
				if ($_Typedata == 3) {
					$harga = $b[7];	
				}
			}
			$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
			foreach ($urut as $r)
			{
				$urutanbayar = $r->geturutbayar;
			}
			
		
			$pay_query = $this->db->query("select inserttrpembayaran
									('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",
									'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
			$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
									".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
									".$urut_detailbayar.")")->result();
		}


		if($pembayaran) {
			$tmptagihan = $this->db->query("select gettagihan('$kdKasir','$notransaksi')");
			$tagihan1 = $tmptagihan->result();
			foreach ($tagihan1 as $data){
				$tagihan = $data->gettagihan;
			}
			$tmppembayaran = $this->db->query("select getpembayaran('$kdKasir','$notransaksi')");
			$bayar1 = $tmppembayaran->result();
			foreach ($bayar1 as $data){
				$bayar = $data->getpembayaran;
			}
			if($tagihan <> $bayar){
				$queryUpdate = $this->db->query("update transaksi set ispay = 'f' where no_transaksi = '$notransaksi' and kd_kasir = '$kdKasir'");
			}else{
				$queryUpdate = $this->db->query("update transaksi set ispay = 't' where no_transaksi = '$notransaksi' and kd_kasir = '$kdKasir'");
			}			
			$this->db->trans_commit();
			echo '{success:true}';	
		} else {
			$this->db->trans_rollback();
			echo '{success:false}';	
		} */
	}

	public function saveDiagnosa()
	{

		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl = $_POST['Tgl'];
		$list = $_POST['List'];
		$jmlfield = $_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;


		$a = explode("##[[]]##", $list);
		$this->db->trans_begin();
		for ($i = 0; $i <= count($a) - 1; $i++) {

			$b = explode("@@##$$@@", $a[$i]);
			for ($k = 1; $k <= count($b) - 1; $k++) {
				if ($b[$k] == 'Diagnosa Awal') {
					$diagnosa = 0;
				} else if ($b[$k] == 'Diagnosa Utama') {
					$diagnosa = 1;
				} else if ($b[$k] == 'Komplikasi') {
					$diagnosa = 2;
				} else if ($b[$k] == 'Diagnosa Sekunder') {
					$diagnosa = 3;
				} else if ($b[$k] == 'Baru') {
					$kasus = 'FALSE';
					$zkasus = 0;
				} else if ($b[$k] == 'Lama') {
					$kasus = 'TRUE';
					$zkasus = 1;
				}
			}

			$urut = $this->db->query("select getUrutMrPenyakit('" . $kdPasien . "','" . $kdUnit . "','" . $Tgl . "'," . $urut_masuk . ") ");
			$result = $urut->result();
			foreach ($result as $data) {
				$Urutan = $data->geturutmrpenyakit;
			}

			$query = $this->db->query("select insertdatapenyakit('" . $b[1] . "','" . $kdPasien . "','" . $kdUnit . "','" . $Tgl . "'," . $diagnosa . ",
				" . $kasus . "," . $tindakan . "," . $perawatan . "," . $Urutan . "," . $urut_masuk . ")");
		}
		if ($query) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function saveDiagnosaPoliklinik()
	{
		$this->db->trans_begin();
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl = $_POST['Tgl'];
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$kebidanan = 99;
		$inser_batch = array();
		$this->db->query("DELETE FROM mr_penyakit WHERE kd_pasien='" . $kdPasien . "' and kd_unit='" . $kdUnit . "' and tgl_masuk='" . $Tgl . "' and urut_masuk='" . $urut_masuk . "'");

		for ($i = 0; $i < $jmlList; $i++) {
			if ($_POST['STAT_DIAG' . $i] == 'Diagnosa Awal') {
				$diagnosa = 0;
			} else if ($_POST['STAT_DIAG' . $i]  == 'Diagnosa Utama') {
				$diagnosa = 1;
			} else if ($_POST['STAT_DIAG' . $i]  == 'Komplikasi') {
				$diagnosa = 2;
			} else if ($_POST['STAT_DIAG' . $i] == 'Diagnosa Sekunder') {
				$diagnosa = 3;
			}
			/*
				BARU = FALSE
				LAMA = TRUE
			 */
			if ($_POST['KASUS' . $i] == 'Baru') {
				// $kasus = 'TRUE';
				$kasus = 'FALSE';
				$zkasus = 0;
			} else if ($_POST['KASUS' . $i] == 'Lama') {
				// $kasus = 'FALSE';
				$kasus = 'TRUE';
				$zkasus = 1;
			}
			$urut = $this->db->query("getUrutMrPenyakit @kdpasien='" . $kdPasien . "',@kdunit='" . $kdUnit . "',@tglmasuk='" . $Tgl . "',@urutmasuk=" . $urut_masuk . " ");
			$result = $urut->result();
			foreach ($result as $data) {
				$Urutan = $data->getUrutMrPenyakit;
			}

			$query = $this->db->query("insertdatapenyakit @kdpenyakit='" . $_POST['KD_PENYAKIT' . $i] . "', @kdpasien='" . $kdPasien . "', @kdunit = '" . $kdUnit . "',
			@tglmasuk='" . $Tgl . "',@statdiag=" . $diagnosa . ",@kasus=" . $kasus . ",@tindakan=" . $tindakan . ",@perawatan=" . $perawatan . ",@urut=" . $Urutan . ",
			@urutmasuk=" . $urut_masuk . ", @kebidanan=" . $kebidanan . " ");


			if ($_POST['NOTE' . $i] == 1) {
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='" . $_POST['KD_PENYAKIT' . $i] . "' AND kd_pasien='" . $kdPasien . "' AND
				kd_unit='" . $kdUnit . "' AND tgl_masuk='" . $Tgl . "' AND urut_masuk=" . $urut_masuk);
				$query = $this->db->query("select insertneoplasma('" . $_POST['KD_PENYAKIT' . $i] . "','" . $kdPasien . "','" . $kdUnit . "','" . $Tgl . "'," . $urut_masuk . "," . $Urutan . ",
				'" . $_POST['DETAIL' . $i] . "')");
			} else if ($_POST['NOTE' . $i] == 2) {
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='" . $_POST['KD_PENYAKIT' . $i] . "' AND kd_pasien='" . $kdPasien . "' AND
				kd_unit='" . $kdUnit . "' AND tgl_masuk='" . $Tgl . "' AND urut_masuk=" . $urut_masuk);
				$query = $this->db->query("select insertkecelakaan('" . $_POST['KD_PENYAKIT' . $i] . "','" . $kdPasien . "','" . $kdUnit . "','" . $Tgl . "',
				" . $urut_masuk . "," . $Urutan . ",'" . $_POST['DETAIL' . $i] . "')");
			} else if ($_POST['NOTE' . $i] == 0) {
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='" . $_POST['KD_PENYAKIT' . $i] . "' AND kd_pasien='" . $kdPasien . "' AND
				kd_unit='" . $kdUnit . "' AND tgl_masuk='" . $Tgl . "' AND urut_masuk=" . $urut_masuk);
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='" . $_POST['KD_PENYAKIT' . $i] . "' AND kd_pasien='" . $kdPasien . "' AND
				kd_unit='" . $kdUnit . "' AND tgl_masuk='" . $Tgl . "' AND urut_masuk=" . $urut_masuk);
			}
		}

		# Update STATUS ANTRIAN PASIEN antrian_poli -> SEDANG DILAYANI
		$dataupdate = array('id_status_antrian' => 3);
		$criteria = array(
			'tgl_transaksi' => $_POST['Tgl'], 'kd_pasien' => $_POST['KdPasien'],
			'sts_ditemukan' => 'true', 'kd_unit' => $_POST['KdUnit']
		);
		$this->db->where($criteria);
		$update = $this->db->update('antrian_poliklinik', $dataupdate);

		# Update STATUS ANTRIAN PASIEN kunjungan -> SEDANG DILAYANI
		date_default_timezone_set('Asia/Makassar');
		$JamDilayani = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$data = array('id_status_antrian' => 3, 'jam_dilayani' => $JamDilayani, 'status_sinkronisasi' => 2);
		$criteria = array(
			'tgl_masuk' => $_POST['Tgl'], 'kd_pasien' => $_POST['KdPasien'],
			'kd_unit' => $_POST['KdUnit'], 'urut_masuk' => $_POST['UrutMasuk']
		);
		$this->db->where($criteria);
		$updatekunjungan = $this->db->update('kunjungan', $data);

		if ($this->db->trans_status() == true) {
			$this->db->trans_commit();
			echo "{success:true,echo:" . $this->db->trans_status() . "}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}


	/* public function deletedetail_bayar()
	{
		$_kduser = $this->session->userdata['user_id']['id'];
		$kdKasir = $_POST['KDkasir'];	
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$Urut =$_POST['Urut'];
		$TmpTglBayar = explode(' ',$_POST['TrTglbayar']);
		$TglBayar = $TmpTglBayar[0];
		$Kodepay=$_POST['Kodepay'];
		$KodeUnit=$_POST['KodeUnit'];
		$NamaPasien=$_POST['NamaPasien'];
		$Namaunit=$_POST['Namaunit'];
		$Tgltransaksi=explode(' ',$_POST['Tgltransaksi']);
		$Kodepasein=$_POST['Kodepasein'];
		$Uraian=$_POST['Uraian'];
		$jumlahbayar=str_replace('.','',$_POST['Jumlah']);
		$this->db->trans_begin();
		if( $_POST['Uraian']==='transfer' || $_POST['Uraian']==='TRANSFER')
			{
				$queryselect_tranfer=$this->db->query("select * from transfer_bayar where kd_kasir='$kdKasir'
				and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."' ")->result();
						if(count($queryselect_tranfer)===1)
						{
							for($x=0;  $x<count($queryselect_tranfer); $x++)
							{
							
							$kdkasirtujuan=$queryselect_tranfer[$x]->det_kd_kasir;
							$no_transaksitujuan=$queryselect_tranfer[$x]->det_no_transaksi;
							$uruttujuan=$queryselect_tranfer[$x]->det_urut;
							$tgl_transaksitujuan=$queryselect_tranfer[$x]->det_tgl_transaksi;
							}
							//cek ke transaksi RWI 
								$totaldttrx=$this->db->query("select sum(qty * harga) as total from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
								and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
								$totaldtbyr=$this->db->query("select sum(jumlah) as total from detail_bayar where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
								and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
								
								if ($totaldttrx==$totaldtbyr)
								{
									echo "{success:false , pesan:'LUNAS'}";
								}else
								{
									$Querydelete_tujuan=$this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
									and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'");
									if($Querydelete_tujuan)
									{
								
									$Querydelete_tujuan=$this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
									and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
										if($Querydelete_tujuan)
										{
											$query=$this->db->query(
											"select hapusdetailbayar('$kdKasir',
											'".$TrKodeTranskasi."',
										".$Urut.",'".$TglBayar."')");
										if($query)
										{
		
											$this->db->trans_commit();
											echo "{success:true}";
										}
										else{
											$this->db->trans_rollback();
											echo "{success:false}";
										
											}
										}
										else
										{	$this->db->trans_rollback();
											echo "{success:false}";
										}
									}
									else
									{
									$this->db->trans_rollback();
									echo "{success:false}";
									}
								}
							
							
						}else
						{
						$this->db->trans_rollback();
								echo "{success:false}";
						}

			
			}else
			{	$query=$this->db->query(
										"select hapusdetailbayar('$kdKasir',
										'".$TrKodeTranskasi."',
									".$Urut.",'".$TglBayar."')");
									if($query)
									{
										$tmptagihan = $this->db->query("select gettagihan('$kdKasir','$TrKodeTranskasi')");
										$tagihan1 = $tmptagihan->result();
										foreach ($tagihan1 as $data){
											$tagihan = $data->gettagihan;
										}
										$tmppembayaran = $this->db->query("select getpembayaran('$kdKasir','$TrKodeTranskasi')");
										$bayar1 = $tmppembayaran->result();
										foreach ($bayar1 as $data){
											$bayar = $data->getpembayaran;
										}
										if($tagihan <> $bayar){
											$queryUpdate = $this->db->query("update transaksi set ispay = 'f' where no_transaksi = '$TrKodeTranskasi' and kd_kasir = '$kdKasir'");
										}else{
											$queryUpdate = $this->db->query("update transaksi set ispay = 't' where no_transaksi = '$TrKodeTranskasi' and kd_kasir = '$kdKasir'");
										}
	
										$this->db->trans_commit();
										echo "{success:true}";
									}
									else{
										$this->db->trans_rollback();
										echo "{success:false}";
									
										}
									
			}
			
	} */
	public function deletedetail_bayar()
	{
		//$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		//$db->trans_begin();

		$_kduser = $this->session->userdata['user_id']['id'];
		//$kdKasir =  $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;	
		$kdKasir =  $_POST['KDkasir'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$Urut = $_POST['Urut'];
		$TmpTglBayar = explode(' ', $_POST['TrTglbayar']);
		$TglBayar = $TmpTglBayar[0];
		$Kodepay = $_POST['Kodepay'];
		$KodeUnit = $_POST['KodeUnit'];
		$NamaPasien = $_POST['NamaPasien'];
		$Namaunit = $_POST['Namaunit'];
		$Tgltransaksi = explode(' ', $_POST['Tgltransaksi']);
		$Kodepasein = $_POST['Kodepasein'];
		$Uraian = $_POST['Uraian'];
		$Alasan = $_POST['alasan'];
		$jumlahbayar = str_replace('.', '', $_POST['Jumlah']);
		$this->db->trans_begin();

		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
								inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
									and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
								inner join zusers zu on zu.kd_user=t.kd_user
							where kd_kasir='" . $kdKasir . "' 
								and no_transaksi='" . $TrKodeTranskasi . "' 
								and tgl_transaksi='" . $_POST['Tgltransaksi'] . "'
							")->row();
		$param_insert_history_detail_bayar = array(
			'kd_kasir'		=> $kdKasir,
			'no_transaksi'	=> $TrKodeTranskasi,
			'tgl_transaksi'	=> $_POST['Tgltransaksi'],
			'kd_pasien'		=> $Kodepasein,
			'nama'			=> $NamaPasien,
			'kd_unit'		=> $KodeUnit,
			'nama_unit'		=> $Namaunit,
			'kd_pay'		=> $Kodepay,
			'uraian'		=> $Uraian,
			'kd_user'		=> $res->kd_user,
			'kd_user_del'	=> $_kduser,
			'shift'			=> $res->shift,
			'shiftdel'		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
			'user_name'		=> $res->user_names,
			'jumlah'		=> $jumlahbayar,
			'tgl_batal'		=> date('Y-m-d'),
			'ket'			=> $Alasan,
		);
		$insert		= $this->db->insert("history_detail_bayar", $param_insert_history_detail_bayar);
		//$insertSQL		= $db->insert("history_detail_bayar",$param_insert_history_detail_bayar);		
		if ($_POST['Uraian'] === 'transfer' || $_POST['Uraian'] === 'TRANSFER') {
			$queryselect_tranfer = $this->db->query("select det_kd_kasir,det_no_transaksi,det_urut,det_tgl_transaksi from transfer_bayar where kd_kasir='$kdKasir'
						and no_Transaksi='" . $TrKodeTranskasi . "'and urut=" . $Urut . " and tgl_transaksi = '" . $TglBayar . "' ")->result();
			if (count($queryselect_tranfer) === 1) {
				for ($x = 0; $x < count($queryselect_tranfer); $x++) {
					$kdkasirtujuan = $queryselect_tranfer[$x]->det_kd_kasir;
					$no_transaksitujuan = $queryselect_tranfer[$x]->det_no_transaksi;
					$uruttujuan = $queryselect_tranfer[$x]->det_urut;
					$tgl_transaksitujuan = $queryselect_tranfer[$x]->det_tgl_transaksi;
				}
				$totaldttrx = $this->db->query("select sum(qty * harga) as total from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
						and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
				$totaldtbyr = $this->db->query("select sum(jumlah) as total from detail_bayar where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
						and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;

				if ($totaldttrx == $totaldtbyr) {
					echo "{success:false , pesan:'LUNAS'}";
				} else {
					$Querydelete_tujuan = $this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
							and no_Transaksi='" . $TrKodeTranskasi . "'and urut=" . $Urut . " and tgl_transaksi = '" . $TglBayar . "'");
					/* $Querydelete_tujuansql=$db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
							and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'"); */
					if ($Querydelete_tujuan) {
						$Querydelete_tujuan = $this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
							and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
						/* $Querydelete_tujuansql=$db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
							and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'"); */
						if ($Querydelete_tujuan) {
							$query = $this->db->query(" hapusdetailbayar @kdkasir='$kdKasir', @notrans='" . $TrKodeTranskasi . "',
								@urut=" . $Urut . ",@tgltrans='" . $TglBayar . "'");
							/* $querysql=$db->query( "exec dbo.V5_hapus_detail_bayar '$kdKasir', '".$TrKodeTranskasi."',
															".$Urut.",'".$TglBayar."'");
								 */
							if ($query) { // && $querysql) {									
								$updatetrans = $this->db->query("update transaksi set lunas='0', ispay='0' where kd_kasir='$kdKasir'
																and no_Transaksi='" . $TrKodeTranskasi . "'");
								/* $updatetranssql = $db->query("update transaksi set lunas=0, ispay=0 where kd_kasir='$kdKasir'
																and no_Transaksi='".$TrKodeTranskasi."'"); */
								if ($updatetrans) {
									$this->db->trans_commit();
									//$db->trans_commit();
									echo "{success:true}";
								} else {
									$this->db->trans_rollback();
									//$db->trans_rollback();
									echo "{success:false}";
								}
							} else {
								$this->db->trans_rollback();
								//$db->trans_rollback();
								echo "{success:false}";
							}
						} else {
							$this->db->trans_rollback();
							//$db->trans_rollback();
							echo "{success:false}";
						}
					} else {
						$this->db->trans_rollback();
						//$db->trans_rollback();
						echo "{success:false}";
					}
				}
			} else {
				$this->db->trans_rollback();
				//$db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$query = $this->db->query("hapusdetailbayar @kdkasir='$kdKasir', @notrans='" . $TrKodeTranskasi . "',@urut=" . $Urut . ",@tgltrans='" . $TglBayar . "'");
			//$querysql=$db->query( "exec dbo.V5_hapus_detail_bayar '$kdKasir', '".$TrKodeTranskasi."', ".$Urut.",'".$TglBayar."'");
			if ($query) {
				$updatetrans = $this->db->query("update transaksi set lunas='0', ispay='0' where kd_kasir='$kdKasir'
												and no_Transaksi='" . $TrKodeTranskasi . "'");
				/* $updatetranssql = $db->query("update transaksi set lunas=0, ispay=0 where kd_kasir='$kdKasir'
												and no_Transaksi='".$TrKodeTranskasi."'"); */
				if ($updatetrans) {
					$this->db->trans_commit();
					//$db->trans_commit();
					echo "{success:true}";
				} else {
					$this->db->trans_rollback();
					//$db->trans_rollback();
					echo "{success:false}";
				}
			} else {
				$this->db->trans_rollback();
				//$db->trans_rollback();
				echo "{success:false}";
			}
		}
	}
	public function deleteradiologi()
	{
		$query    = false;
		$criteria = array();
		$response = array();
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'urut' 			=> $this->input->post('urut'),
		);

		unset($criteria);
		$criteria['no_transaksi'] = $params['no_transaksi'];
		$criteria['kd_kasir']     = $params['kd_kasir'];
		$criteria['urut']         = $params['urut'];
		$this->db->where($criteria);
		$query = $this->db->delete("detail_trdokter");



		if ($query > 0 || $query === true) {
			unset($criteria);
			$criteria['transaksi.no_transaksi'] = $params['no_transaksi'];
			$criteria['transaksi.kd_kasir']     = $params['kd_kasir'];
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("transaksi");
			$this->db->join("unit_asal", "unit_asal.no_transaksi = transaksi.no_transaksi AND unit_asal.kd_kasir = transaksi.kd_kasir", "INNER");
			$query_transaksi = $this->db->get();

			if ($query_transaksi->num_rows() > 0) {
				$this->db->select("*");
				$this->db->where(
					array(
						'kd_kasir'     => $query_transaksi->row()->KD_KASIR_ASAL,
						'no_transaksi' => $query_transaksi->row()->NO_TRANSAKSI_ASAL,
					)
				);
				$this->db->from("transaksi");
				$unit_asal = $this->db->get();

				unset($criteria);
				$criteria['kd_pasien']  = $query_transaksi->row()->KD_PASIEN;
				$criteria['tgl_masuk']  = $query_transaksi->row()->TGL_TRANSAKSI;
				$criteria['kd_unit']    = $query_transaksi->row()->KD_UNIT;
				$criteria['urut_masuk'] = $query_transaksi->row()->URUT_MASUK;
				$criteria['urut']       = $params['urut'];
				$criteria['kd_test']  = $params['kd_produk'];
				if ($unit_asal->num_rows() > 0) {
					$criteria['kd_unit_asal']  = $unit_asal->row()->KD_UNIT;
					//$criteria['tgl_masuk_asal']  = $unit_asal->row()->tgl_transaksi;
				}
				$this->db->where($criteria);

				$query = $this->db->delete("rad_hasil");

				#hapus transaksi 

				if ($query > 0 || $query === true) {
					$cek_detail_transaksi = $this->db->query(" 
						select * from detail_transaksi 
						where 
							no_transaksi = '" . $params['no_transaksi'] . "' 
							and kd_kasir = '" . $params['kd_kasir'] . "'
					")->result();

					//JIKA DETAIL TRANSAKSI TINGGAL 1 HAPUS KUNJUNGAN DAN TRANSAKSI
					if (count($cek_detail_transaksi) == 1) {

						$criteria_hps_kj['kd_pasien']  = $query_transaksi->row()->KD_PASIEN;
						$criteria_hps_kj['tgl_masuk']  = $query_transaksi->row()->TGL_TRANSAKSI;
						$criteria_hps_kj['kd_unit']    = $query_transaksi->row()->KD_UNIT;
						$criteria_hps_kj['urut_masuk'] = $query_transaksi->row()->URUT_MASUK;
						$this->db->where($criteria_hps_kj);
						$query = $this->db->delete("mr_rad");

						if ($query) {
							$this->db->where($criteria_hps_kj);
							$query = $this->db->delete("kunjungan");
						}
						//echo "detail".$query;
					} else {
						$query = true;
					}

					//HAPUS DETAIL TRANSAKSI DISINI
					if ($query > 0 || $query === true) {
						unset($criteria);
						$criteria['no_transaksi'] = $params['no_transaksi'];
						$criteria['kd_kasir']     = $params['kd_kasir'];
						$criteria['urut']         = $params['urut'];
						$criteria['kd_produk']    = $params['kd_produk'];
						if (!empty($params['no_transaksi']) === true) {
							$this->db->where($criteria);
							$query = $this->db->delete("detail_transaksi");
						}
					} else {
						$query = false;
					}
				} else {
					$query = false;
				}
			} else {
				$query = false;
			}
		} else {
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$this->db->trans_commit();
			$response['success'] = true;
		} else {
			$this->db->trans_rollback();
			$response['success'] = false;
		}

		echo json_encode($response);
	}

	public function deletelaboratorium()
	{
		$query    = false;
		$criteria = array();
		$response = array();
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'urut' 			=> $this->input->post('urut'),
		);

		unset($criteria);
		$criteria['no_transaksi'] = $params['no_transaksi'];
		$criteria['kd_kasir']     = $params['kd_kasir'];
		$criteria['urut']         = $params['urut'];
		$this->db->where($criteria);
		$query = $this->db->delete("detail_trdokter");



		if ($query > 0 || $query === true) {
			unset($criteria);
			$criteria['transaksi.no_transaksi'] = $params['no_transaksi'];
			$criteria['transaksi.kd_kasir']     = $params['kd_kasir'];
			$this->db->select("transaksi.kd_pasien,transaksi.tgl_transaksi,transaksi.kd_unit,transaksi.urut_masuk,unit_asal.no_transaksi_asal,unit_asal.kd_kasir_asal");
			$this->db->where($criteria);
			$this->db->from("transaksi");
			$this->db->join("unit_asal", "unit_asal.no_transaksi = transaksi.no_transaksi AND unit_asal.kd_kasir = transaksi.kd_kasir", "INNER");
			$query_transaksi = $this->db->get();

			if ($query_transaksi->num_rows() > 0) {
				$this->db->select("kd_unit");
				$this->db->where(
					array(
						'kd_kasir'     => $query_transaksi->row()->kd_kasir_asal,
						'no_transaksi' => $query_transaksi->row()->no_transaksi_asal,
					)
				);
				$this->db->from("transaksi");
				$unit_asal = $this->db->get();

				unset($criteria);
				$criteria['kd_pasien']  = $query_transaksi->row()->kd_pasien;
				$criteria['tgl_masuk']  = $query_transaksi->row()->tgl_transaksi;
				$criteria['kd_unit']    = $query_transaksi->row()->kd_unit;
				$criteria['urut_masuk'] = $query_transaksi->row()->urut_masuk;
				$criteria['urut']       = $params['urut'];
				$criteria['kd_produk']  = $params['kd_produk'];
				if ($unit_asal->num_rows() > 0) {
					$criteria['kd_unit_asal']  = $unit_asal->row()->kd_unit;
					//$criteria['tgl_masuk_asal']  = $unit_asal->row()->tgl_transaksi;
				}
				$this->db->where($criteria);

				$query = $this->db->delete("lab_hasil");

				#hapus transaksi 

				if ($query > 0 || $query === true) {
					$cek_detail_transaksi = $this->db->query(" 
						select * from detail_transaksi 
						where 
							no_transaksi = '" . $params['no_transaksi'] . "' 
							and kd_kasir = '" . $params['kd_kasir'] . "'
					")->result();

					//JIKA DETAIL TRANSAKSI TINGGAL 1 HAPUS KUNJUNGAN DAN TRANSAKSI
					if (count($cek_detail_transaksi) == 1) {

						$criteria_hps_kj['kd_pasien']  = $query_transaksi->row()->kd_pasien;
						$criteria_hps_kj['tgl_masuk']  = $query_transaksi->row()->tgl_transaksi;
						$criteria_hps_kj['kd_unit']    = $query_transaksi->row()->kd_unit;
						$criteria_hps_kj['urut_masuk'] = $query_transaksi->row()->urut_masuk;

						$this->db->where($criteria_hps_kj);
						$query = $this->db->delete("mr_lab");
						if ($query) {
							$this->db->where($criteria_hps_kj);
							$query = $this->db->delete("kunjungan");
						}
						//echo "detail".$query;
					} else {
						$query = true;
					}

					//HAPUS DETAIL TRANSAKSI DISINI
					if ($query > 0 || $query === true) {
						unset($criteria);
						$criteria['no_transaksi'] = $params['no_transaksi'];
						$criteria['kd_kasir']     = $params['kd_kasir'];
						$criteria['urut']         = $params['urut'];
						$criteria['kd_produk']    = $params['kd_produk'];
						if (!empty($params['no_transaksi']) === true) {
							$this->db->where($criteria);
							$query = $this->db->delete("detail_transaksi");
						}
					} else {
						$query = false;
					}
				} else {
					$query = false;
				}
			} else {
				$query = false;
			}
		} else {
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$this->db->trans_commit();
			$response['success'] = true;
		} else {
			$this->db->trans_rollback();
			$response['success'] = false;
		}

		echo json_encode($response);
	}

	public function deletelaboratorium_()
	{
		$query    = false;
		$criteria = array();
		$response = array();
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'urut' 			=> $this->input->post('urut'),
		);
		unset($criteria);
		$criteria['no_transaksi'] = $params['no_transaksi'];
		$criteria['kd_kasir']     = $params['kd_kasir'];
		$criteria['urut']         = $params['urut'];
		$criteria['kd_produk']    = $params['kd_produk'];
		if (!empty($params['no_transaksi']) === true) {
			$this->db->where($criteria);
			$query = $this->db->delete("detail_transaksi");
		}

		if ($query > 0 || $query === true) {
			unset($criteria);
			$criteria['no_transaksi'] = $params['no_transaksi'];
			$criteria['kd_kasir']     = $params['kd_kasir'];
			$criteria['urut']         = $params['urut'];
			$this->db->where($criteria);
			$query = $this->db->delete("detail_trdokter");
		} else {
			$query = false;
		}

		if ($query > 0 || $query === true) {
			unset($criteria);
			$criteria['transaksi.no_transaksi'] = $params['no_transaksi'];
			$criteria['transaksi.kd_kasir']     = $params['kd_kasir'];
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("transaksi");
			$this->db->join("unit_asal", "unit_asal.no_transaksi = transaksi.no_transaksi AND unit_asal.kd_kasir = transaksi.kd_kasir", "INNER");
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				$this->db->select("*");
				$this->db->where(
					array(
						'kd_kasir'     => $query->row()->kd_kasir_asal,
						'no_transaksi' => $query->row()->no_transaksi_asal,
					)
				);
				$this->db->from("transaksi");
				$unit_asal = $this->db->get();

				unset($criteria);
				$criteria['kd_pasien']  = $query->row()->kd_pasien;
				$criteria['tgl_masuk']  = $query->row()->tgl_transaksi;
				$criteria['kd_unit']    = $query->row()->kd_unit;
				$criteria['urut_masuk'] = $query->row()->urut_masuk;
				$criteria['urut']       = $params['urut'];
				$criteria['kd_produk']  = $params['kd_produk'];
				if ($unit_asal->num_rows() > 0) {
					$criteria['kd_unit_asal']  = $unit_asal->row()->kd_unit;
					$criteria['tgl_masuk_asal']  = $unit_asal->row()->tgl_transaksi;
				}
				$this->db->where($criteria);
				$query = $this->db->delete("lab_hasil");
				#hapus transaksi 

				if ($query > 0 || $query === true) {
					$criteria_tr['no_transaksi'] = $params['no_transaksi'];
					$criteria_tr['kd_kasir']     = $params['kd_kasir'];
					$this->db->where($criteria_tr);
					$query = $this->db->delete("transaksi");
				} else {
					$query = false;
				}
			} else {
				$query = false;
			}
		} else {
			$query = false;
		}

		if ($query > 0 || $query === true) {
			$this->db->trans_commit();
			$response['success'] = true;
		} else {
			$this->db->trans_rollback();
			$response['success'] = false;
		}

		echo json_encode($response);
	}

	public function getTindakan_()
	{
		$result = $this->db->query("SELECT id_status,catatan from mr_rwi_rujukan WHERE kd_pasien='" . $_POST['kd_pasien'] . "' 
		AND kd_unit='" . $_POST['kd_unit'] . "' AND tgl_masuk='" . $_POST['tgl_masuk'] . "' AND urut_masuk=" . $_POST['urut_masuk'])->result();
		$hasil = array();
		if (count($result) > 0) {
			$hasil['id_status'] = $result[0]->id_status;
			$hasil['catatan'] = $result[0]->catatan;
		} else {
			$hasil['id_status'] = -1;
			$hasil['catatan'] = '';
		}

		echo "{success:true,echo:" . json_encode($hasil) . "}";
	}

	public function getTindakan()
	{
		$result = $this->db->query("SELECT 
			sm.kd_sebab_mati, 
			sm.sebab_mati, 
			sp.kd_status_pulang, 
			sp.status_pulang as keadaan_pasien, 
			msp.status as cara_keluar, 
			msp.id_status as id_cara_keluar, 
			k.catatan from kunjungan k 
		LEFT JOIN sebab_mati sm ON sm.kd_sebab_mati = k.sebabmati
		LEFT JOIN status_pulang sp ON sp.kd_status_pulang = k.keadaan_pasien
		LEFT JOIN mr_status_pulang msp ON msp.id_status = CONVERT(CHAR, k.cara_keluar)
		WHERE kd_pasien='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['kd_unit'] . "' AND tgl_masuk='" . $_POST['tgl_masuk'] . "' AND urut_masuk=" . $_POST['urut_masuk'])->result();
		$hasil = array();
		if (count($result) > 0) {
			$hasil['sebab_mati']       = $result[0]->sebab_mati;
			$hasil['kd_sebab_mati']    = $result[0]->kd_sebab_mati;
			$hasil['keadaan_pasien']   = $result[0]->keadaan_pasien;
			$hasil['kd_status_pulang'] = $result[0]->kd_status_pulang;
			$hasil['cara_keluar']      = $result[0]->cara_keluar;
			$hasil['id_cara_keluar']   = $result[0]->id_cara_keluar;
			$hasil['catatan']          = $result[0]->catatan;
		} else {
			$hasil['kd_status_pulang'] = '';
			$hasil['kd_sebab_mati']    = '';
			$hasil['sebab_mati']       = '';
			$hasil['id_cara_keluar']   = '';
			$hasil['keadaan_pasien']   = '';
			$hasil['cara_keluar']      = '';
			$hasil['catatan']          = '';
		}

		echo "{success:true,data:" . json_encode($hasil) . "}";
	}

	public function saveTindakan()
	{
		$this->db->trans_begin();
		$urut = $this->db->query('select TOP 1 id_rwirujukan AS code FROM mr_rwi_rujukan order by id_rwirujukan desc ')->row();
		$id = '';
		if (isset($urut->code)) {
			$urut = substr($urut->code, 8, 12);
			$sisa = 4 - count(((int)$urut + 1));
			$real = date('Ymd');
			for ($i = 0; $i < $sisa; $i++) {
				$real .= "0";
			}
			$real .= ((int)$urut + 1);
			$id = $real;
		} else {
			$id = date('Ymd') . '0001';
		}

		$count = $this->db->query("select * from mr_rwi_rujukan WHERE kd_pasien='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['kd_unit'] . "' 
		AND tgl_masuk='" . $_POST['tgl_masuk'] . "' AND urut_masuk=" . $_POST['urut_masuk'])->result();
		if (count($count) > 0) {
			$id = $count[0]->id_rwirujukan;
			$this->db->where('id_rwirujukan', $id);
			$mr_rwi_rujukan = array();
			$mr_rwi_rujukan['id_status'] = $_POST['id_status'];
			$mr_rwi_rujukan['catatan'] = $_POST['catatan'];
			if (isset($_POST['kd_unit_tujuan'])) {
				$mr_rwi_rujukan['kd_unit_tujuan'] = $_POST['kd_unit_tujuan'];
			}
			$res = $this->db->update('mr_rwi_rujukan', $mr_rwi_rujukan);
		} else {
			$mr_rwi_rujukan = array();
			$mr_rwi_rujukan['kd_pasien'] = $_POST['kd_pasien'];
			$mr_rwi_rujukan['kd_unit'] = $_POST['kd_unit'];
			$mr_rwi_rujukan['tgl_masuk'] = $_POST['tgl_masuk'];
			$mr_rwi_rujukan['urut_masuk'] = $_POST['urut_masuk'];
			$mr_rwi_rujukan['id_rwirujukan'] = $id;
			$mr_rwi_rujukan['id_status'] = $_POST['id_status'];
			$mr_rwi_rujukan['catatan'] = $_POST['catatan'];
			if (isset($_POST['kd_unit_tujuan'])) {
				$mr_rwi_rujukan['kd_unit_tujuan'] = $_POST['kd_unit_tujuan'];
			}
			$res = $this->db->insert('mr_rwi_rujukan', $mr_rwi_rujukan);
		}

		if ($res) {
			# update STATUS ANTRIAN PASIEN antrian_poliklinik -> TUNGGU PANGGILAN
			$dataupdate = array('id_status_antrian' => 4);
			$criteria = array(
				'tgl_transaksi' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'],
				'sts_ditemukan' => 'true', 'kd_unit' => $_POST['kd_unit']
			);
			$this->db->where($criteria);
			$update = $this->db->update('antrian_poliklinik', $dataupdate);


			if ($update) {
				# update STATUS ANTRIAN PASIEN kunjungan -> TUNGGU PANGGILAN
				date_default_timezone_set('Asia/Makassar');
				$JamKeluar = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
				$data = array(
					'id_status_antrian' => 4,
					'jam_keluar'        => $JamKeluar,
					'cara_keluar'       => $this->input->post('cara_keluar'),
					// 'keadaan_pasien'    => $this->input->post('keadaan_akhir'),
					// 'sebabmati'         => $this->input->post('sebab_mati'),
					'catatan'           => $this->input->post('catatan'),
				);
				$criteriakunjungan = array(
					'tgl_masuk'  => $_POST['tgl_masuk'],
					'kd_pasien'  => $_POST['kd_pasien'],
					'kd_unit'    => $_POST['kd_unit'],
					'urut_masuk' => $_POST['urut_masuk']
				);
				$this->db->where($criteriakunjungan);
				$updatekunjungan = $this->db->update('kunjungan', $data);
				if ($updatekunjungan) {
					$this->db->trans_commit();
					echo "{success:true}";
				} else {
					$this->db->trans_rollback();
					echo "{success:false}";
				}
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			echo "{success:false}";
		}
	}

	public function savelaboratorium()
	{
		//create id
		$urut = $this->db->query('select TOP 1 id_labkonsul AS code FROM mr_labkonsul order by id_labkonsul desc ')->row();
		$id = '';
		if (isset($urut->code)) {
			$urut = substr($urut->code, 8, 12);
			$sisa = 4 - count(((int)$urut + 1));
			$real = date('Ymd');
			for ($i = 0; $i < $sisa; $i++) {
				$real .= "0";
			}
			$real .= ((int)$urut + 1);
			$id = $real;
		} else {
			$id = date('Ymd') . '0001';
		}
		$count = $this->db->query("select * from mr_labkonsul WHERE kd_pasien='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['kd_unit'] . "' AND tgl_masuk='" . $_POST['tgl_masuk'] . "' AND urut_masuk=" . $_POST['urut_masuk'])->result();
		if (count($count) > 0) {
			$id = $count[0]->id_labkonsul;
		} else {
			$mr_labkonsul = array();
			$mr_labkonsul['kd_pasien'] = $_POST['kd_pasien'];
			$mr_labkonsul['kd_unit'] = $_POST['kd_unit'];
			$mr_labkonsul['tgl_masuk'] = $_POST['tgl_masuk'];
			$mr_labkonsul['urut_masuk'] = $_POST['urut_masuk'];
			$mr_labkonsul['id_labkonsul'] = $id;
			$this->db->insert('mr_labkonsul', $mr_labkonsul);
		}
		$count = $this->db->query("delete from mr_labkonsuldtl WHERE id_labkonsul='" . $id . "' ");
		for ($i = 0; $i < $_POST['jum']; $i++) {
			$mr_labkonsuldtl = array();
			$mr_labkonsuldtl['id_labkonsul'] = $id;
			$mr_labkonsuldtl['kd_lab'] = $_POST['kd_lab' . $i];
			$mr_labkonsuldtl['kd_produk'] = $_POST['kd_produk' . $i];
			$r = $this->db->query("select * from dokter where nama='" . $this->session->userdata['user_id']['username'] . "'")->result();
			if (count($r) > 0) {
				$code = $r[0]->kd_dokter;
			} else {
				$code = '0';
			}
			$mr_labkonsuldtl['kd_dokter'] = $code;
			$this->db->insert('mr_labkonsuldtl', $mr_labkonsuldtl);
		}
		echo "{success:true}";
	}
	public function savedetailpenyakit()
	{
		$kdkasirrwj = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$queryTransaksi  = $this->db->query("SELECT * FROM transaksi where no_transaksi = '" . $_POST['TrKodeTranskasi'] . "' and kd_kasir = '01'");
		//$KdUnit        = $_POST['KdUnit'];
		$Tgl             = $queryTransaksi->row()->tgl_transaksi;
		// echo $queryTransaksi->row()->tgl_transaksi;die;
		$Shift           = $this->GetShiftBagian();
		$list            = $_POST['List'];
		$jmlfield        = $_POST['JmlField'];
		$jmlList         = $_POST['JmlList'];
		$kd_dokter       = $_POST['kdDokter'];
		$urut_masuk      = $_POST['urut_masuk'];
		$resep           = $_POST['resep'];
		//$urut_masuk = $_POST['UrutMasuk'];
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;

		$this->db->trans_begin();

		$tmplunas = $this->db->query("select lunas from transaksi where no_Transaksi = '" . $TrKodeTranskasi . "'")->row()->lunas;
		if ($tmplunas === 't') {
			$dataubah1 = array("lunas" => 'f');
			$criteria1 = array("no_transaksi" => $TrKodeTranskasi);
			$this->db->where($criteria1);
			$result = $this->db->update("transaksi", $dataubah1);
		}

		# INSERT DETAIL_TRANSAKSI
		$a 	= explode("##[[]]##", $list);
		//secho (count($a)-1)." ";
		for ($i = 0, $iLen = (count($a)); $i < $iLen; $i++) {
			$b = explode("@@##$$@@", $a[$i]);
			$cek_KdUnit				= $this->db->query("select kd_unit from tarif where kd_produk='" . $b[1] . "' and kd_tarif='" . $b[5] . "' and tgl_berlaku='" . $b[3] . "' and kd_unit='" . $_POST['KdUnit'] . "'"); // KODE UNIT MENGACU LANGSUNG KE TABEL TARIF
			if (count($cek_KdUnit->result()) == 0) {
				$KdUnit				= $this->db->query("select kd_unit from tarif where kd_produk='" . $b[1] . "' and kd_tarif='" . $b[5] . "' and tgl_berlaku='" . $b[3] . "'")->row()->kd_unit; // KODE UNIT MENGACU LANGSUNG KE TABEL TARIF
			} else {
				$KdUnit 			= $_POST['KdUnit'];
			}
			//echo json_encode($b);
			//echo " ".$i;
			if (isset($b[6])) {
				$urut = $this->db->query("SELECT geturutdetailtransaksi('$kdkasirrwj','" . $TrKodeTranskasi . "','" . $Tgl . "') ");
				$result = $urut->result();

				foreach ($result as $data) {
					if ($b[6] == 'x') {
						$Urutan = $data->geturutdetailtransaksi;
					} else {
						$Urutan = $b[6];
					}
				}

				//echo $b[5];
				$query = $this->db->query("SELECT insert_detail_transaksi
					('$kdkasirrwj',
					'" . $TrKodeTranskasi . "',
					" . $Urutan . ",
					'" . $Tgl . "',
					 '" . $this->session->userdata['user_id']['id'] . "',
					'" . $b[5] . "',
					" . $b[1] . ",
					'" . $KdUnit . "',
					'" . $b[3] . "',
					'false',
					'true',
					'',
					" . $b[2] . ",
					" . $b[4] . ",
					" . $Shift . ",
					'false'
					)
				");

				$paramsCriteria = array(
					'kd_kasir' 		=> $kdkasirrwj,
					'no_transaksi' 	=> $TrKodeTranskasi,
					'urut' 			=> $Urutan,
					'tgl_transaksi'	=> $Tgl,
				);
				/* $this->dbSQL->where($paramsCriteria);
				$this->dbSQL->from("detail_transaksi");
				$querySQL = $this->dbSQL->get();
				
				if ($querySQL->num_rows() == 0) {
					$paramsInsert = array(
						'kd_kasir' 		=> $kdkasirrwj,
						'no_transaksi' 	=> $TrKodeTranskasi,
						'urut' 			=> $Urutan,
						'tgl_transaksi'	=> $Tgl,
						'kd_user'		=> $this->session->userdata['user_id']['id'],
						'kd_tarif'		=> $b[5],
						'kd_produk'		=> $b[1],
						'kd_unit'		=> $KdUnit,
						'tgl_berlaku'	=> $b[3],
						'charge'		=> 0,
						'adjust'		=> 1,
						'folio'			=> 'A',
						'qty' 			=> $b[2],
						'harga' 		=> $b[4],
						'shift' 		=> $Shift,
						'tag' 			=> 0,
					);
					$this->dbSQL->insert("detail_transaksi", $paramsInsert);
				}else{
					$paramsUpdate = array(
						'qty' 	=> $b[2],
					);
					$this->dbSQL->where($paramsCriteria);
					$this->dbSQL->update("detail_transaksi", $paramsUpdate);
				} */
				#---------tambahan query untuk insert komopnent tarif dokter--------------#
				/* if($query)
				{
					
					$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
					where 
					(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
					kd_unit ='".$KdUnit."' AND
					kd_produk='".$b[1]."' AND
					tgl_berlaku='".$b[3]."' AND
					kd_tarif='".$b[5]."' group by tarif")->result();
					foreach($ctarif as $ct)
					{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into
						detail_trdokter select '$kdkasirrwj','".$TrKodeTranskasi."','".$Urutan."','".$kd_dokter."','".$b[7]."',0,0,".$ct->tarif.",0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '$kdkasirrwj' AND
									tgl_transaksi='".$b[7]."' AND
									urut='".$Urutan."' AND
									kd_dokter = '".$kd_dokter."' AND
									no_transaksi='".$TrKodeTranskasi."'
							)");
					}
					}
				} */
				#---------Akhir tambahan query untuk insert komopnent tarif dokter--------------#
				#_____________________________________________________________________________________

				$updt = $this->db->query("update detail_transaksi set kd_dokter = '" . $kd_dokter . "' 
				where kd_kasir='$kdkasirrwj' AND
				tgl_transaksi='" . $b[7] . "' AND
				urut='" . $Urutan . "' AND
				no_transaksi='" . $TrKodeTranskasi . "'");
				if ($b[8] == 'false') {
					$cek = $this->db->query("select * from detail_trdokter where kd_kasir='" . $kdkasirrwj . "'
									and no_transaksi='" . $TrKodeTranskasi . "' and urut=" . $Urutan . " and tgl_transaksi='" . $Tgl . "' 
									and kd_component=" . $kdjasadok . "");

					//echo count($cek->result());
					$data_tarifpgnya = $this->db->query("Select tarif as harga From Tarif_Component
									Where 
									KD_Unit='$KdUnit' 
									And Tgl_Berlaku='" . $b[3] . "'
									And KD_Tarif='" . $b[5] . "'
									And Kd_Produk=" . $b[1] . " 
									and kd_component = '$kdjasadok'");
					if (count($data_tarifpgnya->result()) <> 0) {
						if (count($cek->result()) > 0) {
							$dataubah = array("kd_dokter" => $_POST['kdDokter'], "jp" => $data_tarifpgnya->row()->harga);
							$criteria = array(
								"kd_kasir" => $kdkasirrwj, "no_transaksi" => $TrKodeTranskasi, "urut" => $Urutan,
								"tgl_transaksi" => $Tgl, "kd_component" => $kdjasadok
							);
							$this->db->where($criteria);
							$result = $this->db->update("detail_trdokter", $dataubah);
						} else {
							$data = array(
								"kd_kasir" => $kdkasirrwj, "no_transaksi" => $TrKodeTranskasi, "urut" => $Urutan,
								"kd_dokter" => $_POST['kdDokter'], "tgl_transaksi" => $Tgl,
								"kd_component" => $kdjasadok, "jp" => $data_tarifpgnya->row()->harga
							);
							$result = $this->db->insert("detail_trdokter", $data);
						}
					}
				}
				/* $cek = $this->db->query("select * from detail_trdokter where kd_kasir='".$kdkasirrwj."'
									and no_transaksi='".$TrKodeTranskasi."' and urut=".$Urutan." and tgl_transaksi='".$Tgl."' 
									and kd_component=".$kdjasadok."");
				
					//echo count($cek->result());
					if(count($cek->result()) > 0){
						$dataubah = array("kd_dokter"=>$_POST['kdDokter'],"jp"=>$b[4]);
						$criteria = array("kd_kasir"=>$kdkasirrwj,"no_transaksi"=>$TrKodeTranskasi,"urut"=>$Urutan,
										"tgl_transaksi"=>$Tgl,"kd_component"=>$kdjasadok);
						$this->db->where($criteria);			
						$result = $this->db->update("detail_trdokter",$dataubah);
					} else{
						$data = array("kd_kasir"=>$kdkasirrwj,"no_transaksi"=>$TrKodeTranskasi,"urut"=>$Urutan,
								"kd_dokter"=>$_POST['kdDokter'],"tgl_transaksi"=>$Tgl,
								"kd_component"=>$kdjasadok,"jp"=>$b[4]);
						$result = $this->db->insert("detail_trdokter",$data);
					} */
				//---------Akhir tambahan query untuk insert komopnent tarif dokter--------------//

				//--------Query Untuk Insert / Update Ke mr_tindakan ------------------------\\
				/* $query_cek_data_mr =$this->db->query("select * from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' and tgl_masuk='".$_POST['Tgl']."' and urut_masuk=".$_POST['urut_masuk']." and urut = '".$Urutan."'")->result();
				
				$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$b[1]."'")->row()->kd_klas;
				if ($cek_klas<>'9' && $cek_klas<>'1')
				{
					if (count($query_cek_data_mr)==0)
						{
							$query = $this->db->query("INSERT INTO mr_tindakan (kd_produk, kd_pasien, kd_unit, tgl_masuk, urut_masuk, urut, tgl_tindakan) 
													VALUES (".$b[1].", '".$_POST['kd_pasien']."', '".$KdUnit."', '".$_POST['Tgl']."', ".$_POST['urut_masuk'].", '".$Urutan."', '".$Tgl."')");
						} else
						{
							$query = $this->db->query("Update mr_tindakan set kd_produk = ".$b[1].", tgl_tindakan = '".$Tgl."' where kd_pasien = '".$_POST['kd_pasien']."' and  kd_unit = '".$KdUnit."' and tgl_masuk = '".$_POST['Tgl']."' and urut_masuk =  ".$_POST['urut_masuk']." and urut = '".$Urutan."'");
						}
				} */
			}
		}

		if ($resep == 'true') {
			# SAVE RESEP ONLINE / ORDER RESEP KE APOTEK
			$urut = $this->db->query('SELECT TOP 1 id_mrresep from mr_resep order by id_mrresep desc');
			if (count($urut->result()) > 0) {
				$urut = substr($urut->row()->id_mrresep, 8, 12);
				$sisa = 4 - count(((int)$urut + 1));
				$real = date('Ymd');
				for ($i = 0; $i < $sisa; $i++) {
					$real .= "0";
				}
				$real .= ((int)$urut + 1);
				$urut = $real;
			} else {
				$urut = date('Ymd') . '0001';
			}

			$result = $this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['KdUnit'] . "' AND 
									tgl_masuk='" . $_POST['Tgl'] . "' AND urut_masuk='" . $_POST['urut_masuk'] . "'")->row();
			if ($result->jumlah > 0) {
				$result = $this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='" . $_POST['kd_pasien'] . "' AND kd_unit='" . $_POST['KdUnit'] . "' AND 
									tgl_masuk='" . $_POST['Tgl'] . "' AND urut_masuk='" . $_POST['urut_masuk'] . "'")->row();
				$urut = $result->id_mrresep;
			} else {
				/* $kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
				$kd='0';
				if(isset($kd_dokter->kd_dokter)){
					$kd=$kd_dokter->kd_dokter;
				} */
				$mr_resep = array();
				$mr_resep['kd_pasien'] = $_POST['kd_pasien'];
				$mr_resep['kd_unit'] = $_POST['KdUnit'];
				$mr_resep['tgl_masuk'] = $_POST['Tgl'];
				$mr_resep['urut_masuk'] = $_POST['urut_masuk'];
				$mr_resep['kd_dokter'] = $kd_dokter;
				$mr_resep['id_mrresep'] = $urut;
				$mr_resep['cat_racikan'] = '';
				$mr_resep['tgl_order'] = $Tgl;
				$mr_resep['dilayani'] = 0;
				$this->db->insert('mr_resep', $mr_resep);
			}
			$result = $this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=" . $urut)->result();
			for ($i = 0; $i < count($result); $i++) {
				$ada = false;
				for ($j = 0; $j < $_POST['jmlObat']; $j++) {
					if ($result[$i]->urut == ($j + 1) && $result[$i]->kd_prd == $_POST['kd_prd' . $j]) {
						$ada = true;
					}
				}
				/* 	if($ada==false){
					$this->db->query("DELETE FROM mr_resepdtl WHERE
					id_mrresep=".$result[$i]->id_mrresep." AND urut=".$result[$i]->urut." AND kd_prd='".$result[$i]->kd_prd."'");
				} */
			}
			for ($i = 0; $i < $_POST['jmlObat']; $i++) {
				$status = 0;
				if ($_POST['urut' . $i] == 0 || $_POST['urut' . $i] == '' || $_POST['urut' . $i] == 'undefined') {
					$urut_order = ($i + 1);
				} else {

					$urut_order = $_POST['urut' . $i];
				}
				if ($_POST['verified' . $i] == 'Not Verified') {
					$status = 1;
				}
				$result = $this->db->query("SELECT insertmr_resepdtl(" . $urut . "," . $urut_order . ",'" . $_POST['kd_prd' . $i] . "'," . $_POST['jumlah' . $i] . ",'" . $_POST['cara_pakai' . $i] . "'
				,0,'" . $_POST['kd_dokter' . $i] . "'," . $status . "," . $_POST['racikan' . $i] . ",'f','" . $_POST['aturan_pakai' . $i] . "','" . $_POST['aturan_racik' . $i] . "'
				,'" . $_POST['kd_unit_far' . $i] . "'
				," . $_POST['kd_milik' . $i] . "
				," . $_POST['no_racik' . $i] . "
				,'" . $_POST['signa' . $i] . "'
				,'" . $_POST['takaran' . $i] . "'
				," . $_POST['jumlah_racik' . $i] . "
				,'" . $_POST['satuan_racik' . $i] . "'
				,'" . $_POST['catatan_racik' . $i] . "') ");
			}
		} else {
			$urut = '';
		}

		# Update STATUS ANTRIAN PASIEN antrian_poli -> SEDANG DILAYANI
		$dataupdate = array('id_status_antrian' => 3);
		$criteria = array(
			'tgl_transaksi' => $_POST['Tgl'], 'kd_pasien' => $_POST['kd_pasien'],
			'sts_ditemukan' => 'true', 'kd_unit' => $_POST['KdUnit']
		);
		$this->db->where($criteria);
		$update = $this->db->update('antrian_poliklinik', $dataupdate);

		if ($update) {
			# Update STATUS ANTRIAN PASIEN kunjungan -> SEDANG DILAYANI
			date_default_timezone_set('Asia/Makassar');
			$JamDilayani = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
			$data = array('id_status_antrian' => 3, 'jam_dilayani' => $JamDilayani);
			$criteria = array(
				'tgl_masuk' => $_POST['Tgl'], 'kd_pasien' => $_POST['kd_pasien'],
				'kd_unit' => $_POST['KdUnit'], 'urut_masuk' => $_POST['urut_masuk']
			);
			$this->db->where($criteria);
			$updatekunjungan = $this->db->update('kunjungan', $data);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			echo "{success:false}";
		} else {
			$this->db->trans_commit();
			echo "{success:true,id_mrresep:'$urut'}";
			//echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
		}

		/* 	if($query){
			echo "{success:true}";
 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
			}else{
				echo "{success:false}";
			} */
	}

	public function ubah_co_status_transksi()
	{
		$kdunit = $_POST['KDUnit'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$kdKasir = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj'")->row()->setting;

		$query = $this->db->query(" update transaksi set ispay='true',co_status='true' ,  tgl_co='" . date("Y-m-d") . "' where no_transaksi='" . $TrKodeTranskasi . "' and kd_kasir='" . $kdKasir . "'");
		if ($query) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}



	public function GetRecordPenyakit($kd, $nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%" . $kd . "%' OR penyakit ilike '%" . $nama . "%' ");
		$count = $query->num_rows();
		return $count;
	}


	public function saveTransfer()
	{
		$db                 = $this->load->database('otherdb2', TRUE);
		$tglasal            = $_POST['Tglasal'];
		$KDunittujuan       = $_POST['KDunittujuan'];
		$KDkasirIGD         = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
		$KdUnit             = $_POST['KdUnit'];
		$Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting; //$_POST['Kdpay'];
		//$Kdpay            = $_POST['Kdpay'];
		$total              = str_replace('.', '', $_POST['Jumlahtotal']);
		$Shift1             = $this->GetShiftBagian();
		$Kdcustomer         = $_POST['Kdcustomer'];
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$KASIRRWI           = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
		$_kduser            = $this->session->userdata['user_id']['id'];
		$tgltransfer        = $_POST['tgl_transfer'];
		$KDalasan           = $_POST['KDalasan'];
		$tglhariini         = date("Y-m-d");
		$tgl_transfer       = $_POST['tgl_transfer'];
		$this->db->trans_begin();
		$db->trans_begin();
		$det_query          = "SELECT TOP 1 COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc ";
		$resulthasil        = $this->db->query($det_query);
		if ($resulthasil->num_rows() <= 0) {
			$urut_detailbayar = 1;
		} else {
			foreach ($resulthasil->result() as $line) {
				$urut_detailbayar = $line['urutan'];
			}
		}
		$pay_query = $this->db->query(" insert into detail_bayar 
						 (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

						values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");

		/*$pay_query_SQL= _QMS_Query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'A',$Shift1,1)");*/
		if ($pay_query) // && $pay_query_SQL)
		{
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
								SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
								WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
			/*$detailTrbayar_SQL = _QMS_Query("	insert into detail_tr_bayar 
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
								SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
								WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");		*/
			if ($detailTrbayar) // && $detailTrbayar_SQL)
			{
				$statuspembayaran = $this->db->query("updatestatustransaksi '$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'");
				/*$sql="EXEC dbo.updatestatustransaksi '$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'";
									$statuspembayaran_sql=_QMS_Query($sql);*/
				if ($statuspembayaran) // && $statuspembayaran_sql)
				{
					$detailtrcomponet = $this->db->query("INSERT into detail_tr_bayar_component
										    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
											Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
											'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
											FROM Detail_Component dc
											INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
											INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
											and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
											WHERE dc.Kd_Kasir = '$KDkasirIGD'
											AND dc.No_Transaksi ='$TrKodeTranskasi'
											ORDER BY dc.Kd_Component");

					/*$detailtrcomponet_SQL = _QMS_Query("insert into detail_tr_bayar_component
											(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
											Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
											'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
											FROM Detail_Component dc
											INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
											INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
											and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
											WHERE dc.Kd_Kasir = '$KDkasirIGD'
											AND dc.No_Transaksi ='$TrKodeTranskasi'
											ORDER BY dc.Kd_Component");*/
					if ($detailtrcomponet) // && $detailtrcomponet_SQL)
					{	//where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1


						$urutquery = "select TOP 1 urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc ";
						$resulthasilurut = $this->db->query($urutquery);
						if ($resulthasilurut->num_rows() <= 0) {
							$uruttujuan = 1;
						} else {
							foreach ($resulthasilurut->result() as $line) {
								$uruttujuan = $line['urutan'];
							}
						}
						$getkdtarifcus = $this->db->query(" getkdtarifcus @KdCus='$Kdcustomer'")->result();
						foreach ($getkdtarifcus as $xkdtarifcus) {
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
												FROM Produk_Charge pc 
												INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
												WHERE kd_unit='" . substr($KdUnit, 0, 1) . "'")->result();
						foreach ($getkdproduk as $det) {
							$kdproduktranfer = $det->kdproduk;
							$kdUnittranfer = $det->unitproduk;
						}
						$gettanggalberlaku = $this->db->query("gettanggalberlakuunit
													'$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer'")->result();
						foreach ($gettanggalberlaku as $detx) {
							$tanggalberlaku = $detx->gettanggalberlakuunit;
						}
						/*$gettanggalberlaku_SQL=_QMS_Query("
													select distinct top 1  tgl_berlaku 
														from Tarif inner join 
														(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
														on tarif.kd_produk =produk.kd_produk 
														where kd_tarif = '$kdtarifcus'
														and tgl_berlaku <='$tgltransfer'
														and (tgl_berakhir >='$tglhariini'
														or tgl_berakhir is null)
														and kd_unit='$kdUnittranfer'
														and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ")->result();
													//echo json_encode($gettanggalberlaku);
													// echo "
													// select distinct top 1  tgl_berlaku 
														// from Tarif inner join 
														// (produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
														// on tarif.kd_produk =produk.kd_produk 
														// where kd_tarif = '$kdtarifcus'
														// and tgl_berlaku <='$tgltransfer'
														// and (tgl_berakhir >='$tglhariini'
														// or tgl_berakhir is null)
														// and kd_unit='$kdUnittranfer'
														// and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ";
													$tanggalberlaku_SQL='';
													foreach($gettanggalberlaku_SQL as $detx)
													{
														$tanggalberlaku_SQL = $detx->tgl_berlaku;
														
													}*/

						$gettanggalberlaku = $this->db->query("SELECT TOP 1 tgl_berlaku 
														from Tarif inner join 
														(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
														on tarif.kd_produk =produk.kd_produk 
														where kd_tarif = '$kdtarifcus'
														and tgl_berlaku <='$tgltransfer'
														and (tgl_berakhir >='$tglhariini'
														or tgl_berakhir is null)
														and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ");
						$tanggalberlaku = $gettanggalberlaku->row()->tgl_berlaku;
						// echo $tanggalberlaku;die();
						// if($tanggalberlaku=='')// && $tanggalberlaku_SQL=='')
						// {
						// echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
						// exit;
						// }
						$detailtransaksitujuan = $this->db->query("INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur,kd_unit_tr)
													VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
													'$_kduser', '$kdtarifcus','$kdproduktranfer','" . substr($kdUnittranfer, 0, 1) . "','$tanggalberlaku','true','true','E',1,
													$total,$Shift1,'false','$TrKodeTranskasi', '$KDunittujuan')
													");
						/*$detailtransaksitujuan_SQL = _QMS_Query("
													INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
													VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
													'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,1,'',1,
													$total,$Shift1,0,'$TrKodeTranskasi')
													");*/
						if ($detailtransaksitujuan) // && $detailtransaksitujuan_SQL)	
						{
							$detailcomponentujuan = $this->db->query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
													   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
													   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
													   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");

							$detailprsh = $this->db->query("INSERT INTO detail_prsh (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,hak,selisih, Disc)
													   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',sum(jumlah) as jumlah,0,0
													   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
													   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");
							/*$detailcomponentujuan_SQL = _QMS_Query
													("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
													   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
													   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
													   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");*/
							if ($detailcomponentujuan) // && $detailcomponentujuan_SQL)
							{

								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
															(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
															det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
															  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
															  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								/*$tranferbyr_SQL = _QMS_Query("INSERT INTO transfer_bayar
														(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
														det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
														  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
														  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");*/
								if ($tranferbyr) // && $tranferbyr_SQL)
								{
									$trkamar = $this->db->query("
																INSERT INTO detail_tr_kamar VALUES
																('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','" . $_POST['kodeunitkamar'] . "','" . $_POST['nokamar'] . "','" . $_POST['kdspesial'] . "')");
									/*$trkamar_SQL = _QMS_Query("INSERT INTO detail_tr_kamar VALUES
															('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	*/
									if ($trkamar) // && $trkamar_SQL)
									{

										$this->db->trans_commit();
										// $db->trans_commit();
										echo '{success:true}';
									} else {
										$this->db->trans_rollback();
										// $db->trans_rollback();
										echo '{success:false}';
									}
								} else {
									$this->db->trans_rollback();
									// $db->trans_rollback();
									echo '{success:false}';
								}
							} else {
								$this->db->trans_rollback();
								$db->trans_rollback();
								echo '{success:false}';
							}
						} else {
							$this->db->trans_rollback();
							// $db->trans_rollback();
							echo '{success:false}';
						}
					} else {
						$this->db->trans_rollback();
						// $db->trans_rollback();
						echo '{success:false}';
					}
				} else {
					$this->db->trans_rollback();
					// $db->trans_rollback();
					echo '{success:false}';
				}
			} else {
				$this->db->trans_rollback();
				// $db->trans_rollback();
				echo '{success:false}';
			}
		} else {
			$this->db->trans_rollback();
			echo '{success:false}';
		}
	}

	public function savedetailpenyakitonly()
	{
		/*
			PERBARUAN INSERT DAN UPDATE PRODUK DETAIL TRANSAKSI (KD_UNIT)
			OLEH 	: M 
			TANGGAL	: 2017 - 02 - 08
			ALASAN 	: KETIKA ROW PRODUK PADA GRID LIST PRODUK KOSONG TIDAK DAPAT MENYIMPAN PRODUK
		 */

		$this->db->trans_begin();
		/*
			Urutan store Functional Parameter
			- KD Kasir
			- No Transaksi
			- Urut
			- Tgl Transaksi
			- KD User
			- KD Tarif
			- KD Produk
			- KD Unit
			- Tgl Berlaku
			- Charge
			- Adjust
			- Folio 
			- QTY
			- Harga
			- Shift
			- Tag
			- No Faktur 
		 */
		//'kd_unit'       => $_POST['KdUnit'],
		$data 	= array(
			'kd_kasir'      => $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting,
			'no_transaksi'  => $_POST['TrKodeTranskasi'],
			'tgl_transaksi' => $_POST['Tgl'],
			'shift'         => $this->GetShiftBagian(),
			'list'          => $_POST['List'],
			'jmlfield'      => $_POST['JmlField'],
			'jmlList'       => $_POST['JmlList'],
			'kd_dokter'     => $_POST['kdDokter'],
			'urut_masuk'    => $_POST['urut_masuk'],
			'id_user'       => $this->session->userdata['user_id']['id'],
		);

		/*
			PEMBARUAN DEFAULT KODE COMPONENT DETAIL TR DOKTER
			OLEH 	: M
			TANGGAL : 2017-02-08
			ALASAN 	: KD KOMPONENT DI KASIH NILAI TETAP (DI PATOK)
		 */
		$kdjasadok        = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas       = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$KdTrDokterComp   = $this->db->query("SELECT setting FROM sys_setting WHERE key_data='def_kdcomp_dettrdr'")->row()->setting;

		$a 	= explode("##[[]]##", $data['list']);
		for ($i = 0, $iLen = (count($a)); $i < $iLen; $i++) {
			$b = explode("@@##$$@@", $a[$i]);
			if (isset($b[6])) {
				$urut = $this->db->query("select geturutdetailtransaksi('" . $data['kd_kasir'] . "','" . $data['no_transaksi'] . "','" . $data['tgl_transaksi'] . "') ");
				$result = $urut->result();
				foreach ($result as $data_result) {
					if ($b[6] == '') {
						$Urutan = $data_result->geturutdetailtransaksi;
					} else {
						$Urutan = $b[6];
					}
				}

				$data['kd_produk']   = $b[1];
				$data['qty']         = $b[2];
				$data['tgl_berlaku'] = $b[3];
				$data['harga']       = $b[4];
				$data['kd_tarif']    = $b[5];
				$data['kd_unit']     = $b[9];
				$data['urutan']      = $Urutan;

				$tmpTarifComponent   = $this->db->query("SELECT tarif FROM detail_component WHERE 
					no_transaksi = '" . $data['no_transaksi'] . "' 
					and kd_kasir='" . $data['kd_kasir'] . "'
					and tgl_transaksi = '" . $data['tgl_transaksi'] . "' 
					and urut = '" . $data['urutan'] . "' 
					and kd_component='" . $kdjasadok . "'");
				foreach ($tmpTarifComponent->result_array() as $res) {
					$TarifComponentDR = $res['tarif'];
				}

				$queryTransaksi      = $this->M_pembayaran->saveDetailProduk($data);

				$updt = $this->db->query("update detail_transaksi set kd_dokter = '" . $data['kd_dokter'] . "' 
				where kd_kasir ='" . $data['kd_kasir'] . "' AND
				tgl_transaksi  ='" . $data['tgl_transaksi'] . "' AND
				urut           ='" . $data['urutan'] . "' AND
				no_transaksi   ='" . $data['no_transaksi'] . "'");

				if ($b[8] == 'false') {
					$cek = $this->db->query("select * from detail_trdokter where kd_kasir='" . $data['kd_kasir'] . "'and no_transaksi='" . $data['no_transaksi'] . "' and urut=" . $data['urutan'] . " and tgl_transaksi='" . $data['tgl_transaksi'] . "' and kd_component=" . $kdjasadok . "");
					if (count($cek->result()) > 0) {
						$dataubah = array(
							"kd_dokter" => $data['kd_dokter'],
							"jp"        => $TarifComponentDR
						);
						$criteria = array(
							"kd_kasir"      => $data['kd_kasir'],
							"no_transaksi"  => $data['no_transaksi'],
							"urut"          => $data['urutan'],
							"tgl_transaksi" => $data['tgl_transaksi'],
							"kd_component"  => $kdjasadok
						);
						$this->db->where($criteria);
						$result = $this->db->update("detail_trdokter", $dataubah);
						if ($result) {
							_QMS_query(
								"UPDATE detail_trdokter SET 
										kd_dokter = '" . $data['kd_dokter'] . "', 
										jp        = '" . $TarifComponentDR . "' 
									WHERE 
										kd_kasir      = '" . $data['kd_kasir'] . "' AND
										no_transaksi  = '" . $data['no_transaksi'] . "' AND
										urut          = '" . $data['urutan'] . "' AND
										tgl_transaksi = '" . $data['tgl_transaksi'] . "'
									"
							);
						}
					} else {
						$tmpTarifComponentInsert   = $this->db->query("SELECT tarif FROM detail_component WHERE 
								no_transaksi = '" . $data['no_transaksi'] . "' 
								and kd_kasir='" . $data['kd_kasir'] . "'
								and tgl_transaksi = '" . $data['tgl_transaksi'] . "' 
								and urut = '" . $data['urutan'] . "' 
								and kd_component='" . $kdjasadok . "'");
						foreach ($tmpTarifComponentInsert->result_array() as $res) {
							$TarifComponentInsert = $res['tarif'];
						}

						$result_pg 	= $this->db->query("
								SELECT * from detail_trdokter WHERE 
								kd_kasir      = '" . $data['kd_kasir'] . "' AND
								no_transaksi  = '" . $data['no_transaksi'] . "' AND
								urut          = '" . $data['urutan'] . "' AND
								kd_dokter     = '" . $data['kd_dokter'] . "' AND
								tgl_transaksi = '" . $data['tgl_transaksi'] . "'
							");

						if ($result_pg->num_rows() == 0) {
							$dataDetailDokter = array(
								"kd_kasir"      => $data['kd_kasir'],
								"no_transaksi"  => $data['no_transaksi'],
								"urut"          => $data['urutan'],
								"kd_dokter"     => $data['kd_dokter'],
								"tgl_transaksi" => $data['tgl_transaksi'],
								"kd_component"  => $kdjasadok,
								"jp"            => $TarifComponentInsert
							);
							$result = $this->db->insert("detail_trdokter", $dataDetailDokter);
						}

						$result_sql = _QMS_query("
								SELECT * from detail_trdokter WHERE 
								kd_kasir      = '" . $data['kd_kasir'] . "' AND
								no_transaksi  = '" . $data['no_transaksi'] . "' AND
								urut          = '" . $data['urutan'] . "' AND
								kd_dokter     = '" . $data['kd_dokter'] . "' AND
								tgl_transaksi = '" . $data['tgl_transaksi'] . "'
							");
						if ($result_sql->num_rows() == 0) {
							_QMS_query(
								"
									INSERT INTO detail_trdokter 
										(kd_kasir, no_transaksi, urut, kd_dokter, tgl_transaksi, jp)
									VALUES (
										'" . $data['kd_kasir'] . "', 
										'" . $data['no_transaksi'] . "', 
										'" . $data['urutan'] . "', 
										'" . $data['kd_dokter'] . "', 
										'" . $data['tgl_transaksi'] . "', 
										'" . $TarifComponentInsert . "' 
									)
									"
							);
						}
					}
				}
			}
		}
		if ($queryTransaksi && $result) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}


	/* public function savedetailpenyakitonly(){
		$kdkasirrwj=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField']; 
		$jmlList 			= $_POST['JmlList'];
		
		$tmplunas = $this->db->query("select lunas from transaksi where no_Transaksi = '".$TrKodeTranskasi."'")->row()->lunas;
		if ($tmplunas === 't') {
			$dataubah1 = array("lunas"=>'f');
			$criteria1 = array("no_transaksi"=>$TrKodeTranskasi);
			$this->db->where($criteria1);			
			$result = $this->db->update("transaksi",$dataubah1);
		}else{

		}

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		//$urut_masuk = $_POST['UrutMasuk'];
		$a = explode("##[[]]##",$list);
		for($i=0,$iLen=(count($a));$i<$iLen;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			if(isset($b[6])){
				$urut = $this->db->query("select geturutdetailtransaksi('$kdkasirrwj','".$TrKodeTranskasi."','".$Tgl."') ");
				$result = $urut->result();
				foreach ($result as $data){ 
					if($b[6]==0 || $b[6]=='' ){
						$Urutan = $data->geturutdetailtransaksi;
					}else{
						$Urutan=$b[6];
					}
				}
				$query = $this->db->query("select insert_detail_transaksi
					('$kdkasirrwj',
					'".$TrKodeTranskasi."',
					".$Urutan.",
					'".$Tgl."',
					 '".$this->session->userdata['user_id']['id']."',
					'".$b[5]."',
					".$b[1].",
					'".$KdUnit."',
					'".$b[3]."',
					'false',
					'true',
					'',
					".$b[2].",
					".$b[4].",
					".$Shift.",
					'false'
					)
				");	
				if ($b[8]=='false'){
					$cek = $this->db->query("select * from detail_trdokter where kd_kasir='".$kdkasirrwj."'
									and no_transaksi='".$TrKodeTranskasi."' and urut=".$Urutan." and tgl_transaksi='".$Tgl."' 
									and kd_component=".$kdjasadok."");
				
					//echo count($cek->result());
					if(count($cek->result()) > 0){
						$dataubah = array("kd_dokter"=>$_POST['kdDokter'],"jp"=>$b[4]);
						$criteria = array("kd_kasir"=>$kdkasirrwj,"no_transaksi"=>$TrKodeTranskasi,"urut"=>$Urutan,
										"tgl_transaksi"=>$Tgl,"kd_component"=>$kdjasadok);
						$this->db->where($criteria);			
						$result = $this->db->update("detail_trdokter",$dataubah);
					} else{
						$data = array("kd_kasir"=>$kdkasirrwj,"no_transaksi"=>$TrKodeTranskasi,"urut"=>$Urutan,
								"kd_dokter"=>$_POST['kdDokter'],"tgl_transaksi"=>$Tgl,
								"kd_component"=>$kdjasadok,"jp"=>$b[4]);
						$result = $this->db->insert("detail_trdokter",$data);
					}
				}
			}
			
			// $cek = $this->db->query("select * from detail_trdokter where kd_kasir='".$kdkasirrwj."'
								// and no_transaksi='".$TrKodeTranskasi."' and urut=".$Urutan." and tgl_transaksi='".$Tgl."' 
							    // and kd_component=".$kdjasadok."");
			
				// if(count($cek->result()) > 0){
					// $dataubah = array("kd_dokter"=>$_POST['kdDokter'],"jp"=>$b[4]);
					// $criteria = array("kd_kasir"=>$kdkasirrwj,"no_transaksi"=>$TrKodeTranskasi,"urut"=>$Urutan,
									// "tgl_transaksi"=>$Tgl,"kd_component"=>$kdjasadok);
					// $this->db->where($criteria);			
					// $result = $this->db->update("detail_trdokter",$dataubah);
				// } else{
					// $data = array("kd_kasir"=>$kdkasirrwj,"no_transaksi"=>$TrKodeTranskasi,"urut"=>$Urutan,
							// "kd_dokter"=>$_POST['kdDokter'],"tgl_transaksi"=>$Tgl,
							// "kd_component"=>$kdjasadok,"jp"=>$b[4]);
					// $result = $this->db->insert("detail_trdokter",$data);
				// }
		}
		
		if($query && $result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
			echo $kd;
		}
	}  */

	public function deletekunjungan_Revisi()
	{
		$response              = array();
		$result_PG             = false;
		$result_SQL            = false;
		$kd_unit               = $_POST['kd_unit'];
		$tgl_kunjungan         = $_POST['Tglkunjungan'];
		$kd_pasien             = $_POST['Kodepasein'];
		$urut_masuk            = $_POST['urut'];
		$keterangan            = $_POST['alasan'];
		$shift                 = $this->GetShiftBagian();
		$kd_user               = $this->session->userdata['user_id']['id'];
		$response['transaksi'] = false;
		$response['bayar']     = false;
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();

		$resultKunjungan = $this->db->query("SELECT * from kunjungan k
			inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
			inner join pasien p on k.kd_pasien=p.kd_pasien
			inner join unit u on k.kd_unit=u.kd_unit
			where k.kd_pasien='" . $kd_pasien . "' and k.kd_unit='" . $kd_unit . "' and k.tgl_masuk='" . $tgl_kunjungan . "' and k.urut_masuk=" . $urut_masuk . "")->row();

		$resultTransaksi = $this->db->query("SELECT dt.*, produk.deskripsi from detail_transaksi dt 
											inner join produk on produk.kd_produk = dt.kd_produk
											where no_transaksi='" . $resultKunjungan->NO_TRANSAKSI . "' and kd_kasir='" . $resultKunjungan->KD_KASIR . "' and tgl_transaksi='" . $resultKunjungan->TGL_TRANSAKSI . "'");

		$resultDetailBayar = $this->db->query("SELECT detail_bayar.*,payment.uraian from detail_bayar 
												inner join payment on payment.kd_pay=detail_bayar.kd_pay
												where no_transaksi='" . $resultKunjungan->NO_TRANSAKSI . "' and kd_kasir='" . $resultKunjungan->KD_KASIR . "' 
												and tgl_transaksi='" . $resultKunjungan->TGL_TRANSAKSI . "'");
		$jumlah = 0;
		for ($i = 0; $i < $resultTransaksi->num_rows(); $i++) {
			$total = 0;
			$total = $resultTransaksi->row()->QTY * $resultTransaksi->row()->HARGA;
			$jumlah += $total;
		}
		$resultNotaBill = $this->db->query("SELECT * from nota_bill where no_transaksi='" . $resultKunjungan->NO_TRANSAKSI . "' and kd_kasir='" . $resultKunjungan->KD_KASIR . "' ");
		$user = $this->db->query("select * from zusers where kd_user='" . $kd_user . "'")->row();

		if ($resultTransaksi->num_rows() > 0) {
			$response['transaksi'] = true;
		}

		/* 	========================================================= HISTORY KUNJUNGAN ============================================== */
		$response['tahap'] = 'History Kunjungan';
		$paramsInsert = array(
			"tgl_kunjungan" => $tgl_kunjungan,
			"kd_pasien" 	=> $kd_pasien,
			"nama" 			=> $resultKunjungan->NAMA,
			"kd_unit" 		=> $kd_unit,
			"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
			"kd_user_del" 	=> $kd_user,
			"shift" 		=> $resultKunjungan->SHIFT,
			"shiftdel" 		=> $shift,
			"username" 		=> $user->User_names,
			"ket" 			=> $keterangan,
			"tgl_batal" 	=> date('Y-m-d'),
			"jam_batal" 	=> gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7)
		);
		$result_PG = $this->db->insert('history_batal_kunjungan', $paramsInsert);

		/* unset($paramsInsert);
		$paramsInsert = array(
			"kd_pasien" 	=> $kd_pasien,
			"kd_unit" 		=> $kd_unit,
			"tgl_masuk" 	=> $tgl_kunjungan,
			"urut_masuk" 	=> $urut_masuk,
			"urut" 			=> $urut_masuk,
			"kd_user" 		=> $kd_user,
			"tgl_update" 	=> date('Y-m-d'),
			"jam_update" 	=> gmdate("d/M/Y H:i:s", time()+60*61*7),
			"keterangan" 	=> $keterangan);
		$result_SQL = $this->dbSQL->insert('history_kunjungan',$paramsInsert); */

		/* 	========================================================= HISTORY TRANSAKSI ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			if ($resultTransaksi->num_rows() > 0) {
				$response['tahap'] = 'History Transaksi';
				unset($paramsInsert);
				$paramsInsert = array(
					"kd_kasir" 		=> $resultKunjungan->KD_KASIR,
					"no_transaksi" 	=> $resultKunjungan->NO_TRANSAKSI,
					"ispay" 		=> $resultKunjungan->ISPAY,
					"tgl_transaksi" => $tgl_kunjungan,
					"kd_pasien" 	=> $kd_pasien,
					"nama" 			=> $resultKunjungan->NAMA,
					"kd_unit" 		=> $kd_unit,
					"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
					"kd_user_del" 	=> $kd_user,
					"kd_user" 		=> $resultKunjungan->KD_USER,
					"user_name" 	=> $user->User_names,
					"jumlah" 		=> $jumlah,
					"tgl_batal" 	=> date('Y-m-d'),
					"jam_batal" 	=> gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7),
					"ket" 			=> $keterangan
				);

				$result_PG = $this->db->insert('history_trans', $paramsInsert);
				if ($resultKunjungan->ISPAY === true) {
					$paramsInsert['ispay'] = 1;
				} else {
					$paramsInsert['ispay'] = 0;
				}
				$paramsInsert['jumlah'] = $jumlah;
				//$result_SQL = $this->dbSQL->insert('history_trans',$paramsInsert);
			} else {
				$response['tahap'] = 'History Transaksi';
				$result_PG 	= true;
				//$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}


		/* 	========================================================= HISTORY NOTA BILL ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			if ($resultNotaBill->num_rows() > 0) {
				if ($resultKunjungan->TAG == NULL || $resultKunjungan->TAG == '') {
					$no_nota = NULL;
				} else {
					$no_nota = $resultKunjungan->TAG;
				}

				$no_nota = $resultNotaBill->row()->NO_NOTA;
				if ($resultTransaksi->num_rows() > 0) {
					$response['tahap'] = 'History Nota Bill';
					unset($paramsInsert);
					// foreach ($resultTransaksi->result() as $data) {
					// 	if ($data->tag == 't' || $data->tag === true) {
					// 		$paramsInsert = array(
					// 			"kd_kasir" 		=> $resultKunjungan->kd_kasir,
					// 			"no_transaksi" 	=> $resultKunjungan->no_transaksi,
					// 			"urut" 			=> $resultKunjungan->urut_masuk,
					// 			"no_nota" 		=> $no_nota,
					// 			"kd_user" 		=> $kd_user,
					// 			"ket" 			=> $keterangan
					// 		);
					// 		$result_PG	= $this->db->insert('history_nota_bill', $paramsInsert);
					// 		//$result_SQL	= $this->dbSQL->insert('history_nota_bill',$paramsInsert);
					// 	}
					// }
				} else {
					$result_PG 	= true;
					//$result_SQL = true;
				}
			} else {
				$response['tahap'] = 'History Nota Bill';
				$result_PG 	= true;
				//$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= HISTORY DETAIL TRANSAKSI ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			if ($resultTransaksi->num_rows() > 0) {
				$response['tahap'] = 'History Detail Transaksi';
				foreach ($resultTransaksi->result() as $data) {
					unset($paramsInsert);
					$paramsInsert = array(
						"kd_kasir" 		=> $resultKunjungan->KD_KASIR,
						"no_transaksi" 	=> $resultKunjungan->NO_TRANSAKSI,
						"tgl_transaksi" => $tgl_kunjungan,
						"kd_pasien" 	=> $kd_pasien,
						"nama" 			=> $resultKunjungan->NAMA,
						"kd_unit" 		=> $kd_unit,
						"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
						"kd_produk" 	=> $data->KD_PRODUK,
						"uraian" 		=> $data->deskripsi,
						"kd_user_del" 	=> $kd_user,
						"kd_user" 		=> $resultKunjungan->KD_USER,
						"shift" 		=> $resultKunjungan->SHIFT,
						"shiftdel" 		=> $shift,
						"user_name" 	=> $user->User_names,
						"jumlah" 		=> $data->QTY * $data->HARGA,
						"tgl_batal" 	=> date('Y-m-d'),
						"ket" 			=> $keterangan
					);
					$result_PG	= $this->db->insert('history_detail_trans', $paramsInsert);
					//$result_SQL = $this->dbSQL->insert('history_detail_trans',$paramsInsert);
				}
			} else {
				$response['tahap'] = 'History Detail Transaksi';
				$result_PG 	= true;
				//$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= HISTORY DETAIL BAYAR ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			if ($resultTransaksi->num_rows() > 0) {
				$response['tahap'] = 'History Detail Bayar';
				foreach ($resultTransaksi->result() as $dataDetailTrans) {
					// foreach ($resultTransaksi->result() as $dataDetailTrans) {
					foreach ($resultDetailBayar->result() as $dataDetailBayar) {
						unset($paramsInsert);
						$paramsInsert = array(
							"kd_kasir" 		=> $resultKunjungan->kd_kasir,
							"no_transaksi" 	=> $resultKunjungan->no_transaksi,
							"tgl_transaksi" => $tgl_kunjungan,
							"kd_pasien" 	=> $kd_pasien,
							"nama" 			=> $resultKunjungan->nama,
							"kd_unit" 		=> $kd_unit,
							"nama_unit" 	=> $resultKunjungan->nama_unit,
							"kd_pay" 		=> $dataDetailBayar->kd_pay,
							"uraian" 		=> $dataDetailBayar->uraian,
							"kd_user_del" 	=> $kd_user,
							"kd_user" 		=> $resultKunjungan->kd_user,
							"shift" 		=> $resultKunjungan->shift,
							"shiftdel" 		=> $shift,
							"user_name" 	=> $user->user_names,
							"jumlah" 		=> $dataDetailTrans->qty * $dataDetailTrans->harga,
							"tgl_batal" 	=> date('Y-m-d'),
							"ket" 			=> $keterangan
						);
						$result_PG 	= $this->db->insert('history_detail_bayar', $paramsInsert);
						//$result_SQL =$this->dbSQL->insert('history_detail_bayar',$paramsInsert);
					}
					// }
				}
			} else {
				$response['tahap'] = 'History Detail Bayar';
				$result_PG 	= true;
				//$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}


		/* 	========================================================= DETAIL BAYAR ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			if ($resultDetailBayar->num_rows() > 0) {
				$criteriaParams = array(
					'no_transaksi' 	=> $resultKunjungan->no_transaksi,
					'kd_kasir' 		=> $resultKunjungan->kd_kasir,
				);

				$result_PG 	= $this->db->delete('detail_bayar', $criteriaParams);
				//$result_SQL = $this->dbSQL->delete('detail_bayar', $criteriaParams);
				if (($result_PG > 0 || $result_PG === true)) {
					$response['bayar'] = true;
				} else {
					$response['bayar'] = false;
				}
			} else {
				$response['tahap'] = 'Delete Detail Bayar';
				$result_PG 	= true;
				//$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= MR PENYAKIT ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			$resultMR = $this->db->query("SELECT * from mr_penyakit where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
			if ($resultMR->num_rows() > 0) {
				unset($criteriaParams);
				$criteriaParams = array(
					'kd_pasien' 	=> $kd_pasien,
					'kd_unit' 		=> $kd_unit,
					'tgl_masuk' 	=> $tgl_kunjungan,
					'urut_masuk' 	=> $urut_masuk,
				);

				$result_PG 	= $this->db->delete('mr_penyakit', $criteriaParams);
				//$result_SQL = $this->dbSQL->delete('mr_penyakit', $criteriaParams);
			} else {
				$response['tahap'] = 'Delete MR Penyakit';
				$result_PG 	= true;
				//$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= DELETE KUNJUNGAN YANG MENNGINA ============================================== */
		if (substr($kd_unit, 0, 1) == '1') {
			$response['tahap'] = 'Delete Kunjungan Rawat Inap';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $resultKunjungan->KD_KASIR,
				'kd_unit' 		=> $kd_unit,
				'no_transaksi' 	=> $resultKunjungan->NO_TRANSAKSI,
			);
			//$result_SQL = $this->dbSQL->delete("pasien_inap", $criteriaParams); 
			$result_PG = $this->db->delete("pasien_inap", $criteriaParams);
		}


		/* 	========================================================= DELETE ANTRIAN POLIKLINIK ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete Antrian Poliklinik';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_transaksi'	=> $tgl_kunjungan,
			);

			//$result_SQL 	= $this->dbSQL->delete("antrian_poliklinik", $criteriaParams);
			$result_PG 		= $this->db->delete("antrian_poliklinik", $criteriaParams);
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= DELETE SJP KUNJUNGAN ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete SJP Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			//$result_SQL 	= $this->dbSQL->delete("sjp_kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("sjp_kunjungan", $criteriaParams);
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= DELETE RUJUKAN KUNJUNGAN ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete SJP Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			//$result_SQL 	= $this->dbSQL->delete("rujukan_kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("rujukan_kunjungan", $criteriaParams);
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}

		/* 	========================================================= DELETE penanggung_jawab ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete penanggung_jawab';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("penanggung_jawab", $criteriaParams);
			$result_PG 		= $this->db->delete("penanggung_jawab", $criteriaParams);
		} else {
			$result_PG 	= false;
			// $result_SQL = false;
		}


		/* 	========================================================= DELETE KUNJUNGAN PASIEN ============================================== */
		if (($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			//$result_SQL 	= $this->dbSQL->delete("kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("kunjungan", $criteriaParams);
		} else {
			$result_PG 	= false;
			//$result_SQL = false;
		}
		// echo "<pre>".var_export($result_PG, true)."</pre>";
		// echo "<pre>".var_export($result_SQL, true)."</pre>";die;
		if (($result_PG > 0 || $result_PG === true)) {
			$this->db->trans_commit();
			//$this->dbSQL->trans_commit();
			$this->db->close();
			//$this->dbSQL->close();
			$response['success'] = true;
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$this->db->close();
			//$this->dbSQL->close();
			$response['success'] = false;
		}
		echo json_encode($response);
	}

	public function deletekunjungan_()
	{
		$this->db->trans_begin();
		$response      = array();
		$kd_unit       = $_POST['kd_unit'];
		$tgl_kunjungan = $_POST['Tglkunjungan'];
		$kd_pasien     = $_POST['Kodepasein'];
		$urut_masuk    = $_POST['urut'];
		$keterangan    = $_POST['alasan'];

		$criteria = array(
			'kd_pasien'     => $kd_pasien,
			'kd_unit'       => $kd_unit,
			'urut_masuk'    => $urut_masuk,
			'tgl_transaksi' => $tgl_kunjungan,
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("transaksi");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			unset($criteria);
			$criteria = array(
				'no_transaksi' 	=> $query->row()->no_transaksi,
				'kd_kasir' 		=> $query->row()->kd_kasir,
			);

			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("detail_transaksi");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$response['status'] 	= false;
				$response['message'] 	= "Data tidak dapat dihapus karena terdapat transaksi";
			} else {
				$response['status'] 	= true;
				$response['message'] 	= "Berhasil dihapus";
			}
		} else {
			$response['status'] 	= false;
			$response['message'] 	= "Data tidak ditemukan";
		}

		// ===================================== DELETE DATA KUNJUNGAN 
		if ($response['status'] === true || $response['status'] > 0) {
			unset($criteria);
			$criteria = array(
				'kd_pasien'     => $kd_pasien,
				'kd_unit'       => $kd_unit,
				'urut_masuk'    => $urut_masuk,
				'tgl_masuk' 	=> $tgl_kunjungan,
			);
			$this->db->where($criteria);
			$this->db->delete('kunjungan');
			$response['status'] = $this->db->affected_rows();
		}

		if ($response['status'] === true || $response['status'] > 0) {
			$response['status'] 	= true;
			$response['message'] 	= "Data berhasil dihapus";
			$this->db->trans_commit();
		} else {
			$response['status'] 	= false;
			$this->db->trans_rollback();
		}
		$this->db->close();
		echo json_encode($response);
	}


	public function deletekunjungan_RWJ()
	{
		$response              = array();
		$result_PG             = false;
		$result_SQL            = false;
		$kd_unit               = $_POST['kd_unit'];
		$tgl_kunjungan         = $_POST['Tglkunjungan'];
		$kd_pasien             = $_POST['Kodepasein'];
		$urut_masuk            = $_POST['urut'];
		$keterangan            = $_POST['alasan'];
		$shift                 = $this->GetShiftBagian();
		$kd_user               = $this->session->userdata['user_id']['id'];
		$response['transaksi'] = false;
		$response['bayar']     = false;
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();

		$resultKunjungan = $this->db->query("SELECT * from kunjungan k
			inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
			inner join pasien p on k.kd_pasien=p.kd_pasien
			inner join unit u on k.kd_unit=u.kd_unit
			where k.kd_pasien='" . $kd_pasien . "' and k.kd_unit='" . $kd_unit . "' and k.tgl_masuk='" . $tgl_kunjungan . "' and k.urut_masuk=" . $urut_masuk . "")->row();

		$resultTransaksi = $this->db->query("SELECT dt.*, produk.deskripsi from detail_transaksi dt 
											inner join produk on produk.kd_produk = dt.kd_produk
											where no_transaksi='" . $resultKunjungan->NO_TRANSAKSI . "' and kd_kasir='" . $resultKunjungan->KD_KASIR . "' and tgl_transaksi='" . $resultKunjungan->TGL_TRANSAKSI . "'");

		$resultDetailBayar = $this->db->query("SELECT detail_bayar.*,payment.uraian from detail_bayar 
												inner join payment on payment.kd_pay=detail_bayar.kd_pay
												where no_transaksi='" . $resultKunjungan->NO_TRANSAKSI . "' and kd_kasir='" . $resultKunjungan->KD_KASIR . "' 
												and tgl_transaksi='" . $resultKunjungan->TGL_TRANSAKSI . "'");
		$jumlah = 0;
		for ($i = 0; $i < $resultTransaksi->num_rows(); $i++) {
			$total = 0;
			$total = $resultTransaksi->row()->QTY * $resultTransaksi->row()->HARGA;
			$jumlah += $total;
		}
		$resultNotaBill = $this->db->query("SELECT * from nota_bill where no_transaksi='" . $resultKunjungan->NO_TRANSAKSI . "' and kd_kasir='" . $resultKunjungan->KD_KASIR . "' ");
		$user = $this->db->query("select * from zusers where kd_user='" . $kd_user . "'")->row();

		if ($resultTransaksi->num_rows() > 0) {
			$response['transaksi'] = true;
		}

		/* 	========================================================= HISTORY KUNJUNGAN ============================================== */
		$response['tahap'] = 'History Kunjungan';
		$paramsInsert = array(
			"tgl_kunjungan" => $tgl_kunjungan,
			"kd_pasien" 	=> $kd_pasien,
			"nama" 			=> $resultKunjungan->NAMA,
			"kd_unit" 		=> $kd_unit,
			"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
			"kd_user_del" 	=> $kd_user,
			"shift" 		=> $resultKunjungan->SHIFT,
			"shiftdel" 		=> $shift,
			"username" 		=> $user->User_names,
			"ket" 			=> $keterangan,
			"tgl_batal" 	=> date('Y-m-d'),
			"jam_batal" 	=> gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7)
		);
		$result_PG = $this->db->insert('history_batal_kunjungan', $paramsInsert);

		// unset($paramsInsert);
		// $paramsInsert = array(
		// 	"kd_pasien" 	=> $kd_pasien,
		// 	"kd_unit" 		=> $kd_unit,
		// 	"tgl_masuk" 	=> $tgl_kunjungan,
		// 	"urut_masuk" 	=> $urut_masuk,
		// 	"urut" 			=> $urut_masuk,
		// 	"kd_user" 		=> $kd_user,
		// 	"tgl_update" 	=> date('Y-m-d'),
		// 	"jam_update" 	=> gmdate("d/M/Y H:i:s", time()+60*61*7),
		// 	"keterangan" 	=> $keterangan);
		// $result_SQL = $this->dbSQL->insert('history_kunjungan',$paramsInsert);
		$result_SQL = true;

		/* 	========================================================= HISTORY TRANSAKSI ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			// if ($resultTransaksi->num_rows()>0) {
			$response['tahap'] = 'History Transaksi';
			unset($paramsInsert);
			$paramsInsert = array(
				"kd_kasir" 		=> $resultKunjungan->KD_KASIR,
				"no_transaksi" 	=> $resultKunjungan->NO_TRANSAKSI,
				"ispay" 		=> $resultKunjungan->ISPAY,
				"tgl_transaksi" => $tgl_kunjungan,
				"kd_pasien" 	=> $kd_pasien,
				"nama" 			=> $resultKunjungan->NAMA,
				"kd_unit" 		=> $kd_unit,
				"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
				"kd_user_del" 	=> $kd_user,
				"kd_user" 		=> $resultKunjungan->KD_USER,
				"user_name" 	=> $user->User_names,
				"jumlah" 		=> $jumlah,
				"tgl_batal" 	=> date('Y-m-d'),
				"jam_batal" 	=> gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7),
				"ket" 			=> $keterangan
			);

			$result_PG = $this->db->insert('history_trans', $paramsInsert);
			if ($resultKunjungan->ISPAY === true) {
				$paramsInsert['ispay'] = 1;
			} else {
				$paramsInsert['ispay'] = 0;
			}
			$paramsInsert['jumlah'] = $jumlah;
			// $result_SQL = $this->dbSQL->insert('history_trans',$paramsInsert);
			$result_SQL = true;
			$response['tahap'] = 'History Transaksi';
			// }else{
			// $result_PG 	= true;
			// $result_SQL = true;
			// }
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}


		/* 	========================================================= HISTORY NOTA BILL ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			if ($resultNotaBill->num_rows() > 0) {
				if ($resultKunjungan->TAG == NULL || $resultKunjungan->TAG == '') {
					$no_nota = NULL;
				} else {
					$no_nota = $resultKunjungan->TAG;
				}

				$no_nota = $resultNotaBill->row()->NO_NOTA;
				if ($resultTransaksi->num_rows() > 0) {
					$response['tahap'] = 'History Nota Bill';
					unset($paramsInsert);
					// foreach ($resultTransaksi->result() as $data) {
					// 	if ($data->TAG == 't' || $data->TAG === true) {
					// 		$paramsInsert = array(
					// 			"kd_kasir" 		=> $resultKunjungan->kd_kasir,
					// 			"no_transaksi" 	=> $resultKunjungan->no_transaksi,
					// 			"urut" 			=> $resultKunjungan->urut_masuk,
					// 			"no_nota" 		=> $no_nota,
					// 			"kd_user" 		=> $kd_user,
					// 			"ket" 			=> $keterangan
					// 		);
					// 		$result_PG	= $this->db->insert('history_nota_bill', $paramsInsert);
					// 		// $result_SQL	= $this->dbSQL->insert('history_nota_bill', $paramsInsert);
					// 	}
					// }
				} else {
					$result_PG 	= true;
					$result_SQL = true;
				}
			} else {
				$response['tahap'] = 'History Nota Bill';
				$result_PG 	= true;
				$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= HISTORY DETAIL TRANSAKSI ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			// if ($resultTransaksi->num_rows()>0) {
			$response['tahap'] = 'History Detail Transaksi';
			foreach ($resultTransaksi->result() as $data) {
				unset($paramsInsert);
				$paramsInsert = array(
					"kd_kasir" 		=> $resultKunjungan->KD_KASIR,
					"no_transaksi" 	=> $resultKunjungan->NO_TRANSAKSI,
					"tgl_transaksi" => $tgl_kunjungan,
					"kd_pasien" 	=> $kd_pasien,
					"nama" 			=> $resultKunjungan->NAMA,
					"kd_unit" 		=> $kd_unit,
					"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
					"kd_produk" 	=> $data->KD_PRODUK,
					"uraian" 		=> $data->deskripsi,
					"kd_user_del" 	=> $kd_user,
					"kd_user" 		=> $resultKunjungan->KD_USER,
					"shift" 		=> $resultKunjungan->SHIFT,
					"shiftdel" 		=> $shift,
					"user_name" 	=> $user->User_names,
					"jumlah" 		=> $data->QTY * $data->HARGA,
					"tgl_batal" 	=> date('Y-m-d'),
					"ket" 			=> $keterangan
				);
				$result_PG	= $this->db->insert('history_detail_trans', $paramsInsert);
				// $result_SQL = $this->dbSQL->insert('history_detail_trans',$paramsInsert);
				$result_SQL = true;
			}
			$response['tahap'] = 'History Detail Transaksi';
			// }else{
			// $result_PG 	= true;
			// $result_SQL = true;
			// }
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= HISTORY DETAIL BAYAR ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			// if ($resultTransaksi->num_rows()>0) {
			$response['tahap'] = 'History Detail Bayar';
			foreach ($resultTransaksi->result() as $dataDetailTrans) {
				// foreach ($resultTransaksi->result() as $dataDetailTrans) {
				foreach ($resultDetailBayar->result() as $dataDetailBayar) {
					unset($paramsInsert);
					$paramsInsert = array(
						"kd_kasir" 		=> $resultKunjungan->KD_KASIR,
						"no_transaksi" 	=> $resultKunjungan->NO_TRANSAKSI,
						"tgl_transaksi" => $tgl_kunjungan,
						"kd_pasien" 	=> $kd_pasien,
						"nama" 			=> $resultKunjungan->NAMA,
						"kd_unit" 		=> $kd_unit,
						"nama_unit" 	=> $resultKunjungan->NAMA_UNIT,
						"kd_pay" 		=> $dataDetailBayar->KD_PAY,
						"uraian" 		=> $dataDetailBayar->uraian,
						"kd_user_del" 	=> $kd_user,
						"kd_user" 		=> $resultKunjungan->KD_USER,
						"shift" 		=> $resultKunjungan->SHIFT,
						"shiftdel" 		=> $shift,
						"user_name" 	=> $user->User_names,
						"jumlah" 		=> $dataDetailTrans->QTY * $dataDetailTrans->HARGA,
						"tgl_batal" 	=> date('Y-m-d'),
						"ket" 			=> $keterangan
					);
					$result_PG 	= $this->db->insert('history_detail_bayar', $paramsInsert);
					// $result_SQL =$this->dbSQL->insert('history_detail_bayar',$paramsInsert);
					$result_SQL = true;
				}
				// }
			}
			$response['tahap'] = 'History Detail Bayar';
			// }else{
			// $result_PG 	= true;
			// $result_SQL = true;
			// }
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}


		/* 	========================================================= DETAIL BAYAR ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			if ($resultDetailBayar->num_rows() > 0) {
				$criteriaParams = array(
					'no_transaksi' 	=> $resultKunjungan->NO_TRANSAKSI,
					'kd_kasir' 		=> $resultKunjungan->KD_KASIR,
				);

				$result_PG 	= $this->db->delete('detail_bayar', $criteriaParams);
				// $result_SQL = $this->dbSQL->delete('detail_bayar', $criteriaParams);
				if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
					$response['bayar'] = true;
				} else {
					$response['bayar'] = false;
				}
			} else {
				$response['tahap'] = 'Delete Detail Bayar';
				$result_PG 	= true;
				$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= MR PENYAKIT ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$resultMR = $this->db->query("SELECT * from mr_penyakit where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
			if ($resultMR->num_rows() > 0) {
				unset($criteriaParams);
				$criteriaParams = array(
					'kd_pasien' 	=> $kd_pasien,
					'kd_unit' 		=> $kd_unit,
					'tgl_masuk' 	=> $tgl_kunjungan,
					'urut_masuk' 	=> $urut_masuk,
				);

				$result_PG 	= $this->db->delete('mr_penyakit', $criteriaParams);
				// $result_SQL = $this->dbSQL->delete('mr_penyakit', $criteriaParams);
				$result_SQL = true;
			} else {
				$response['tahap'] = 'Delete MR Penyakit';
				$result_PG 	= true;
				$result_SQL = true;
			}
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= DELETE KUNJUNGAN YANG MENNGINA ============================================== */
		if (substr($kd_unit, 0, 1) == '1') {
			$response['tahap'] = 'Delete Kunjungan Rawat Inap';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $resultKunjungan->kd_kasir,
				'kd_unit' 		=> $kd_unit,
				'no_transaksi' 	=> $resultKunjungan->no_transaksi,
			);
			// $result_SQL = $this->dbSQL->delete("pasien_inap", $criteriaParams);
			$result_PG = $this->db->delete("pasien_inap", $criteriaParams);
		}


		/* 	========================================================= DELETE ANTRIAN POLIKLINIK ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete Antrian Poliklinik';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_transaksi'	=> $tgl_kunjungan,
			);

			// $result_SQL 	= $this->dbSQL->delete("antrian_poliklinik", $criteriaParams);
			$result_PG 		= $this->db->delete("antrian_poliklinik", $criteriaParams);
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= DELETE SJP KUNJUNGAN ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete SJP Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("sjp_kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("sjp_kunjungan", $criteriaParams);
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= DELETE RUJUKAN KUNJUNGAN ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete SJP Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("rujukan_kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("rujukan_kunjungan", $criteriaParams);
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= DELETE penanggung_jawab ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete penanggung_jawab';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("penanggung_jawab", $criteriaParams);
			$result_PG 		= $this->db->delete("penanggung_jawab", $criteriaParams);
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		/* 	========================================================= DELETE KUNJUNGAN PASIEN ============================================== */
		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$response['tahap'] = 'Delete Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("kunjungan", $criteriaParams);
		} else {
			$result_PG 	= false;
			$result_SQL = false;
		}

		if (($result_SQL > 0 || $result_SQL === true) && ($result_PG > 0 || $result_PG === true)) {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$this->db->close();
			// $this->dbSQL->close();
			$response['success'] = true;
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$this->db->close();
			// $this->dbSQL->close();
			$response['success'] = false;
		}
		echo json_encode($response);
	}

	public function deletekunjungan()
	{
		$strerror = "";
		$kd_unit = $_POST['kd_unit'];
		$tgl_kunjungan = $_POST['Tglkunjungan'];
		$kd_pasien = $_POST['Kodepasein'];
		$urut_masuk = $_POST['urut'];
		//		$keterangan=$_POST['alasan'];
		$shift = $this->GetShiftBagian();
		$kd_user = $this->session->userdata['user_id']['id'];

		$db = $this->load->database('otherdb2', TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		date_default_timezone_set('Asia/Makassar');

		# POSTGREST
		$kunjunganpg = $this->db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='" . $kd_pasien . "' and k.kd_unit='" . $kd_unit . "' and k.tgl_masuk='" . $tgl_kunjungan . "' and k.urut_masuk=" . $urut_masuk . "")->row();
		$detail_transaksipg = $this->db->query("select dt.*,produk.deskripsi from detail_transaksi dt 
												inner join produk on produk.kd_produk = dt.kd_produk
											where no_transaksi='" . $kunjunganpg->no_transaksi . "' and kd_kasir='" . $kunjunganpg->kd_kasir . "' and tgl_transaksi='" . $kunjunganpg->tgl_transaksi . "'")->result();
		# SQLSERVER
		$kunjungansql = $db->query("select * from kunjungan k
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join pasien p on k.kd_pasien=p.kd_pasien
										inner join unit u on k.kd_unit=u.kd_unit
										where k.kd_pasien='" . $kd_pasien . "' and k.kd_unit='" . $kd_unit . "' and k.tgl_masuk='" . $tgl_kunjungan . "' and k.urut_masuk=" . $urut_masuk . "")->row();

		$user = $this->db->query("select * from zusers where kd_user='" . $kd_user . "'")->row();

		# *********************HISTORY_BATAL_KUNJUNGAN**************************
		# POSTGREST
		$datahistorykunjunganpg = array(
			"tgl_kunjungan" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->nama,
			"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->nama_unit, "kd_user_del" => $kd_user,
			"shift" => $kunjunganpg->shift, "shiftdel" => $shift, "username" => $user->user_names,
			"tgl_batal" => date('Y-m-d'), "jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7)
		);
		$inserthistorybatalkunjunganpg = $this->db->insert('history_batal_kunjungan', $datahistorykunjunganpg);

		# SQLSERVER
		$historykunjungan = $db->query("select * from history_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
		if (count($historykunjungan->result()) > 0) {
			$historykunjungan = $db->query("select TOP 1 * from history_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . " order by urut desc")->row();
			$urutmasukhistorykunjungan = $uruthistorykunjungan->urut + 1;
			$uruthistorykunjungan = $urut_masuk;
		} else {
			$urutmasukhistorykunjungan = $urut_masuk;
			$uruthistorykunjungan = 1;
		}
		$datahistorykunjungansql = array(
			"kd_pasien" => $kd_pasien, "kd_unit" => $kd_unit, "tgl_masuk" => $tgl_kunjungan,
			"urut_masuk" => $urutmasukhistorykunjungan, "urut" => $uruthistorykunjungan, "kd_user" => $kd_user,
			"tgl_update" => date('Y-m-d'), "jam_update" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7)
			//"keterangan"=>$keterangan
		);
		$inserthistorybatalkunjungansql = $db->insert('history_kunjungan', $datahistorykunjungansql);
		# ************************** ************************************* *************************
		$lanjut = true;
		// echo count($detail_transaksipg);
		if (count($detail_transaksipg) > 0 && substr($kd_unit, 0, 1) == '1') {
			$this->db->trans_rollback();
			$db->trans_rollback();
			echo '{success: false,type:7}';
		} else {

			if ($inserthistorybatalkunjunganpg && $inserthistorybatalkunjungansql && $lanjut == true) {
				# *************************************HISTORY_TRANS***************************************
				$jumlah = 0;
				for ($i = 0; $i < count($detail_transaksipg); $i++) {
					$total = 0;
					$total = $detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga;
					$jumlah += $total;
				}
				# POSTGREST
				$datahistorytranspg = array(
					"kd_kasir" => $kunjunganpg->kd_kasir, "no_transaksi" => $kunjunganpg->no_transaksi, "ispay" => $kunjunganpg->ispay,
					"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->nama,
					"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->nama_unit, "kd_user_del" => $kd_user,
					"kd_user" => $kunjunganpg->kd_user, "user_name" => $user->user_names, "jumlah" => $jumlah,
					"tgl_batal" => date('Y-m-d'), "jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7), "ket" => ''
				);
				$inserthistorytranspg = $this->db->insert('history_trans', $datahistorytranspg);

				# SQLSERVER
				if ($kunjunganpg->ispay == 't') {
					$ispay = 1;
				} else {
					$ispay = 0;
				}
				$datahistorytranssql = array(
					"kd_kasir" => $kunjunganpg->kd_kasir, "no_transaksi" => $kunjunganpg->no_transaksi, "ispay" => $ispay,
					"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->nama,
					"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->nama_unit, "kd_user_del" => $kd_user,
					"kd_user" => $kunjunganpg->kd_user, "user_name" => $user->user_names, "jumlah" => $jumlah,
					"tgl_batal" => date('Y-m-d'), "jam_batal" => gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7), "ket" => ''
				);
				$inserthistorytranssql = $db->insert('history_trans', $datahistorytranssql);
				# ************************** ************************************* *************************
				if ($inserthistorytranspg && $inserthistorytranssql) {
					if (count($detail_transaksipg) > 0) {

						for ($i = 0; $i < count($detail_transaksipg); $i++) {
							# ****************************************HISTORY_NOTA_BILL*********************************
							$nota_bill = $this->db->query("select * from nota_bill where no_transaksi='" . $kunjunganpg->no_transaksi . "' and kd_kasir='" . $kunjunganpg->kd_kasir . "' ")->result();
							if (count($nota_bill) > 0) {
								if ($kunjunganpg->tag == NULL || $kunjunganpg->tag == '') {
									$no_nota = NULL;
								} else {
									$no_nota = $kunjunganpg->tag;
								}
								$no_nota = $nota_bill[0]->no_nota;
								if ($detail_transaksipg[$i]->tag == 't') {
									# POSTGREST Nambahan Nota Ku asep kamaludin tgl 12 Mei 2017
									$datahistorynotabill = array(
										"kd_kasir" => $kunjunganpg->kd_kasir, "no_transaksi" => $kunjunganpg->no_transaksi,
										"urut" => $kunjunganpg->urut_masuk,
										"no_nota" => $no_nota, "kd_user" => $kd_user, "ket" => $keterangan
									);
									$inserthistorynotabillpg = $this->db->insert('history_nota_bill', $datahistorynotabill);
									# SQLSERVER
									$datahistorynotabillsql = array(
										"kd_kasir" => $kunjunganpg->kd_kasir, "no_transaksi" => $kunjunganpg->no_transaksi,
										"urut" => $kunjunganpg->urut_masuk,
										"no_nota" => $no_nota, "kd_user" => $kd_user, "ket" => $keterangan
									);
									$inserthistorynotabillsql = $db->insert('history_nota_bill', $datahistorynotabillsql);
								}
							}
							# ************************** ************************************* *************************

							# *************************************HISTORY_DETAIL_TRANS*****************************
							# POSTGREST
							$datahistorydetailtrans = array(
								"kd_kasir" => $kunjunganpg->kd_kasir, "no_transaksi" => $kunjunganpg->no_transaksi,
								"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->nama,
								"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->nama_unit,
								"kd_produk" => $detail_transaksipg[$i]->kd_produk, "uraian" => $detail_transaksipg[$i]->deskripsi, "kd_user_del" => $kd_user,
								"kd_user" => $kunjunganpg->kd_user, "shift" => $kunjunganpg->shift, "shiftdel" => $shift,
								"user_name" => $user->user_names, "jumlah" => $detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga,
								"tgl_batal" => date('Y-m-d'), "ket" => ''
							);
							$inserthistorydetailtranspg = $this->db->insert('history_detail_trans', $datahistorydetailtrans);
							# SQLSERVER	
							$inserthistorydetailtranssql = $db->insert('history_detail_trans', $datahistorydetailtrans);
							# ************************** ************************************* *************************
						}

						if ($inserthistorydetailtranspg && $inserthistorydetailtranssql) {
							# *************************************HISTORY_DETAIL_BAYAR*****************************
							$detail_bayarpg = $this->db->query("select detail_bayar.*,payment.uraian from detail_bayar 
																	inner join payment on payment.kd_pay=detail_bayar.kd_pay
																where no_transaksi='" . $kunjunganpg->no_transaksi . "' and kd_kasir='" . $kunjunganpg->kd_kasir . "' 
																	and tgl_transaksi='" . $kunjunganpg->tgl_transaksi . "'")->result();
							for ($i = 0; $i < count($detail_bayarpg); $i++) {
								# POSTGREST
								$datahistorydetailbayar = array(
									"kd_kasir" => $kunjunganpg->kd_kasir, "no_transaksi" => $kunjunganpg->no_transaksi,
									"tgl_transaksi" => $tgl_kunjungan, "kd_pasien" => $kd_pasien, "nama" => $kunjunganpg->nama,
									"kd_unit" => $kd_unit, "nama_unit" => $kunjunganpg->nama_unit,
									"kd_pay" => $detail_bayarpg[$i]->kd_pay, "uraian" => $detail_bayarpg[$i]->uraian, "kd_user_del" => $kd_user,
									"kd_user" => $kunjunganpg->kd_user, "shift" => $kunjunganpg->shift, "shiftdel" => $shift,
									"user_name" => $user->user_names, "jumlah" => $detail_transaksipg[$i]->qty * $detail_transaksipg[$i]->harga,
									"tgl_batal" => date('Y-m-d'), "ket" => $keterangan
								);
								$inserthistorydetailbayarpg = $this->db->insert('history_detail_bayar', $datahistorydetailbayar);
								# SQLSERVER	
								$inserthistorydetailbayarsql = $db->insert('history_detail_bayar', $datahistorydetailbayar);

								if ($inserthistorydetailbayarpg && $inserthistorydetailbayarsql) {
									$strerror = 'OK';
								} else if (count($detail_bayarpg) < 0) {
									$strerror = 'OK';
								} else {
									$strerror = 'Error';
								}
							}
							# ************************** ************************************* *************************
						} else {
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false,type:1}';
						}
					}

					if (($strerror == 'OK' || $strerror == '')) {
						$deletedetailbayarpg = $this->db->query("delete from detail_bayar where no_transaksi='" . $kunjunganpg->no_transaksi . "' and kd_kasir='" . $kunjunganpg->kd_kasir . "' 
															and tgl_transaksi='" . $kunjunganpg->tgl_transaksi . "'");
						$deletedetailbayarsql = $db->query("delete from detail_bayar where no_transaksi='" . $kunjunganpg->no_transaksi . "' and kd_kasir='" . $kunjunganpg->kd_kasir . "' 
															and tgl_transaksi='" . $kunjunganpg->tgl_transaksi . "'");
						if ($deletedetailbayarpg && $deletedetailbayarsql) {
							$deletemrpenyakitpg = $this->db->query("delete from mr_penyakit where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
							$deletemrpenyakitsql = $db->query("delete from mr_penyakit where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
							//$kunjunganpg->no_transaksi."' and kd_kasir='".$kunjunganpg->kd_kasir.
							if (substr($kd_unit, 0, 1) == '1') {
								$this->db->query("delete from pasien_inap where kd_kasir='" . $kunjunganpg->kd_kasir . "' and kd_unit='" . $kd_unit . "' and no_transaksi='" . $kunjunganpg->no_transaksi . "' ");
								$db->query("delete from pasien_inap where kd_kasir='" . $kunjunganpg->kd_kasir . "' and kd_unit='" . $kd_unit . "' and no_transaksi='" . $kunjunganpg->no_transaksi . "' ");
							}

							$deletekunjunganpg = $this->db->query("delete from kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
							$deletekunjungansql = $db->query("delete from kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");

							$date = $tgl_kunjungan;
							$date = str_replace('/', '-', $date);
							$date = date('d-m-Y', strtotime($date));

							$this->db->query("delete from antrian_poliklinik where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_transaksi='" . $tgl_kunjungan . "' ");
							$db->query("delete from antrian_poliklinik where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and convert(varchar(11), tgl_transaksi, 105)='" . $date . "' ");


							if ($deletekunjunganpg && $deletekunjungansql) {
								$deletesjpkunjunganpg = $this->db->query("delete from sjp_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
								$deletesjpkunjungansql = $db->query("delete from sjp_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");

								$deleterujukankunjunganpg = $this->db->query("delete from rujukan_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");
								$deleterujukankunjungansql = $this->db->query("delete from rujukan_kunjungan where kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' and tgl_masuk='" . $tgl_kunjungan . "' and urut_masuk=" . $urut_masuk . "");

								$this->db->trans_commit();
								$db->trans_commit();
								echo '{success: true}';
							} else {
								$this->db->trans_rollback();
								$db->trans_rollback();
								echo '{success: false,type:2,data:"' . $deletekunjunganpg . '_' . $deletekunjungansql . '"}';
							}
						} else {
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false,type:3}';
						}
					} else {
						$this->db->trans_rollback();
						$db->trans_rollback();
						echo '{success: false,type:4}';
					}
				} else {
					$this->db->trans_rollback();
					$db->trans_rollback();
					echo '{success: false,type:5}';
				}
			} else {
				$this->db->trans_rollback();
				$db->trans_rollback();
				echo '{success: false,type:6}';
			}
		}

		/* $shift = $this->dataKunjungan($_POST['Kodepasein'], $_POST['kd_unit'], $_POST['Tglkunjungan'], "SHIFT");
		$query=$this->db->query("select * from transaksi WHERE kd_pasien='".$_POST['Kodepasein']."'
								and kd_unit='".$_POST['kd_unit']."' and tgl_transaksi='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
		if ($query->num_rows==0)
		{
			echo '{success:true, cari_trans:true, cari_bayar:false}';
		}else{
			foreach($query->result() as $det)
			{
				$kd_kasir=	$det->kd_kasir;
				$no_transaksi=$det->no_transaksi;
			}

			$kd_kasir_lab  = $this->db->query("select kd_kasir from transaksi WHERE no_transaksi='".$no_transaksi."'")->row()->kd_kasir;
			$deskripsi_lab = $this->db->query("select deskripsi from kasir WHERE kd_kasir='".$kd_kasir_lab."'")->row()->deskripsi;
			$query         = $this->db->query("select * from detail_bayar WHERE kd_kasir='".$kd_kasir."' and no_transaksi='".$no_transaksi."'");
			if ($query->num_rows==0)
			{
				$query=$this->db->query("delete from mr_penyakit WHERE kd_pasien='".$_POST['Kodepasein']."'
				and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");

				if ($query > 0) {
					_QMS_Query("delete from mr_penyakit WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
				}

				$query_rad = 0;
				$query_lab = 0;
				$query_rad=$this->db->query("select count(kd_pasien) as t_kunjungan from mr_lab WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."")->row()->t_kunjungan;
				$query_lab=$this->db->query("select count(kd_pasien) as t_kunjungan from mr_rad WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."")->row()->t_kunjungan;

				if ($query_rad==0 && $query_lab==0) {
					// $query=$this->db->query("delete from detail_transaksi WHERE kd_kasir='".$kd_kasir."'
						 // and tgl_transaksi='".$_POST['Tglkunjungan']."' ");
					$query=$this->db->query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
					and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
					
					if ($query > 0) {
						_QMS_Query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
						and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
					}


					if($query){
						$query=$this->db->query("delete from transaksi WHERE kd_pasien='".$_POST['Kodepasein']."'
						and kd_unit='".$_POST['kd_unit']."' and tgl_transaksi='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");

						_QMS_Query("delete from kunjungan WHERE kd_pasien='".$_POST['Kodepasein']."'
						and kd_unit='".$_POST['kd_unit']."' and tgl_masuk='".$_POST['Tglkunjungan']."' and urut_masuk=".$_POST['urut']."");
						if($query){
							$this->db->trans_commit();
							$this->insertBatalKunjung($_POST['Kodepasein'], $_POST['Tglkunjungan'], $_POST['kd_unit'], $shift);
							echo '{success:true}';
						}else{
							$this->db->trans_rollback();
							echo '{success:false}';
						}
					}else{
						$this->db->trans_rollback();
						echo '{success:false}';
					}
				}else{
					$this->db->trans_rollback();
					echo '{success:false, kd_kasir:"'.$deskripsi_lab.'"}';
				}
			}else{
				echo '{success:true, cari_trans:true, cari_bayar:true}';
			}
		} */
	}
	private function insertBatalKunjung($kd_pasien, $tgl_kunjungan, $kd_unit, $shift)
	{
		date_default_timezone_set('Asia/Makassar');
		$data = array(
			'tgl_kunjungan' => $tgl_kunjungan,
			'kd_pasien' 	=> $kd_pasien,
			'nama' 			=> $this->dataPasien($kd_pasien, "nama"),
			'kd_unit' 		=> $kd_unit,
			'nama_unit'		=> $this->dataUnit($kd_unit, "nama_unit"),
			'kd_user_del'	=> $this->session->userdata['user_id']['id'],
			'shift'			=> $shift,
			'shiftdel'		=> $shift,
			'username'		=> $this->dataUser($this->session->userdata['user_id']['id'], "full_name"),
			'tgl_batal' 	=> date("Y-m-d"),
			'jam_batal' 	=> gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7),
		);

		$this->db->insert("history_batal_kunjungan", $data);
	}

	private function dataPasien($kd_pasien, $field)
	{
		return $this->db->query("select * from pasien where kd_pasien='" . $kd_pasien . "'")->row()->$field;
	}

	private function dataUnit($kd_unit, $field)
	{
		return $this->db->query("select * from unit where kd_unit='" . $kd_unit . "'")->row()->$field;
	}

	private function dataUser($kd_user, $field)
	{
		return $this->db->query("select * from zusers where kd_user='" . $kd_user . "'")->row()->$field;
	}

	private function dataKunjungan($kd_pasien, $kd_unit, $tgl_kunjungan, $field)
	{
		return _QMS_Query("select * from kunjungan where 
		kd_pasien = '" . $kd_pasien . "' 
		AND kd_unit='" . $kd_unit . "' 
		AND tgl_masuk='" . $tgl_kunjungan . "'
		")->row()->$field;
	}


	private function GetShiftBagian()
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->shift;
		$lastdate = $this->db->query("SELECT CAST(lastdate as DATE) as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->lastdate;
		$datnow = date('Y-m-d');
		if ($lastdate <> $datnow && $sqlbagianshift === '3') {
			$sqlbagianshift2 = '4';
		} else {
			$sqlbagianshift2 = $sqlbagianshift;
		}

		return $sqlbagianshift2;
	}

	public function hapusBarisGridObat()
	{
		if ($_POST['id_mrresep'] == '') {
			echo '{success:true}';
		} else {
			if (isset($_POST['no_racik']) && $_POST['no_racik'] !== 0 && $_POST['no_racik'] !== '0') {
				$delete = $this->db->query("delete from mr_resepdtl where id_mrresep=" . $_POST['id_mrresep'] . " and no_racik='" . $_POST['no_racik'] . "'");
			} else {
				$delete = $this->db->query("delete from mr_resepdtl where id_mrresep=" . $_POST['id_mrresep'] . " and kd_prd='" . $_POST['kd_prd'] . "' and urut=" . $_POST['urut'] . "");
			}

			$query = $this->db->query("SELECT * FROM mr_resepdtl where id_mrresep='" . $_POST['id_mrresep'] . "'");
			if ($query->num_rows() == 0) {
				$q = $this->db->query("DELETE from mr_resep where id_mrresep='" . $_POST['id_mrresep'] . "'");
			}
			if ($delete) {
				echo '{success:true}';
			} else {
				echo '{success:false}';
			}
		}
	}

	public function getTotKunjungan()
	{
		$kd_bagian = '2';
		$tglAwal = $this->input->post('tglAwal');
		$tglAkhir = $this->input->post('tglAkhir');
		// $total = $this->db->query("select 0 as total");

		$total = $this->db->query("select count(*)as total from kunjungan k
									inner join unit u on u.kd_unit=k.kd_unit
									where k.tgl_masuk BETWEEN '" . $tglAwal . "' AND '" . $tglAkhir . "' and u.kd_bagian='" . $kd_bagian . "'");
		if ($total->num_rows() > 0) {
			$totalkunjungan = $total->row()->total;
		} else {
			$totalkunjungan = 0;
		}

		echo "{success:true, totalkunjungan:'" . $totalkunjungan . " " . "'}";
	}

	public function updateStatusPeriksa()
	{
		//(kd_pasien, kd_unit, tgl_masuk, urut_masuk),
		$update = $this->db->query("update kunjungan set status_periksa=1 
									where kd_pasien='" . $_POST['kd_pasien'] . "' and kd_unit='" . $_POST['kd_unit'] . "' 
										and tgl_masuk='" . $_POST['tgl_masuk'] . "' and urut_masuk=" . $_POST['urut_masuk'] . "");
		if ($update) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function getIcd9()
	{
		$result = $this->db->query("select kd_icd9, deskripsi from icd_9 
									where (upper(deskripsi) like upper('%" . $_POST['text'] . "%') or  upper(kd_icd9) like upper('" . $_POST['text'] . "%'))
									order by deskripsi asc")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function saveIcd9()
	{
		$this->db->trans_begin();
		$mrtindakan = array();
		$mrtindakan['kd_pasien'] = $_POST['kd_pasien'];
		$mrtindakan['kd_unit'] = $_POST['kd_unit'];
		$mrtindakan['tgl_masuk'] = $_POST['tgl_masuk'];
		$mrtindakan['urut_masuk'] = $_POST['urut_masuk'];
		$mrtindakan['no_transaksi'] = $_POST['no_transaksi'];
		$mrtindakan['tgl_tindakan'] = date('Y-m-d');
		$mrtindakan['kd_kasir'] = $_POST['kd_kasir'];

		$jmllist = $_POST['jumlah'];
		for ($i = 0; $i < $jmllist; $i++) {

			$cek = $this->db->query("select * from mr_tindakan where kd_pasien='" . $_POST['kd_pasien'] . "' and kd_unit='" . $_POST['kd_unit'] . "' 
								and tgl_masuk='" . $_POST['tgl_masuk'] . "' and urut_masuk=" . $_POST['urut_masuk'] . " 
								and urut=" . $_POST['urut-' . $i] . " and kd_icd9='" . $_POST['kd_icd9-' . $i] . "'")->result();

			if (count($cek) == 0) {
				/* $urut = $this->db->query("select max(urut) as urut from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
								and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']."");
				if(count($urut->result()) > 0){
					$urut = $urut->row()->urut +1;
				} else{
					$urut = 1;
				} */
				$mrtindakan['kd_tindakan'] = $_POST['kd_icd9-' . $i];
				$mrtindakan['kd_icd9'] = $_POST['kd_icd9-' . $i];
				$mrtindakan['urut'] = $_POST['urut-' . $i];
				$save = $this->db->insert('mr_tindakan', $mrtindakan);
			}
		}

		# Update STATUS ANTRIAN PASIEN antrian_poli -> SEDANG DILAYANI
		$dataupdate = array('id_status_antrian' => 3);
		$criteria = array(
			'tgl_transaksi' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'],
			'sts_ditemukan' => 'true', 'kd_unit' => $_POST['kd_unit']
		);
		$this->db->where($criteria);
		$update = $this->db->update('antrian_poliklinik', $dataupdate);

		if ($update) {
			# Update STATUS ANTRIAN PASIEN kunjungan -> SEDANG DILAYANI
			date_default_timezone_set('Asia/Makassar');
			$JamDilayani = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
			$data = array('id_status_antrian' => 3, 'jam_dilayani' => $JamDilayani);
			$criteria = array(
				'tgl_masuk' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'],
				'kd_unit' => $_POST['kd_unit'], 'urut_masuk' => $_POST['urut_masuk']
			);
			$this->db->where($criteria);
			$updatekunjungan = $this->db->update('kunjungan', $data);
			if ($updatekunjungan) {
				$this->db->trans_commit();
				echo "{success:true}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	public function viewgridicd9()
	{
		$result = $this->db->query("select mr.kd_tindakan,mr.kd_icd9,mr.urut,i.deskripsi 
									from mr_tindakan mr
										inner join icd_9 i on i.kd_icd9=mr.kd_icd9
									where kd_pasien='" . $_POST['kd_pasien'] . "' and kd_unit='" . $_POST['kd_unit'] . "' 
									and tgl_masuk='" . $_POST['tgl_masuk'] . "' and urut_masuk=" . $_POST['urut_masuk'] . " order by urut")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function hapusBarisGridIcd()
	{
		$delete = $this->db->query("delete from mr_tindakan where 
			kd_pasien='" . $_POST['kd_pasien'] . "' 
			and kd_unit='" . $_POST['kd_unit'] . "' 
			and tgl_masuk='" . $_POST['tgl_masuk'] . "' 
			and urut_masuk='" . $_POST['urut_masuk'] . "' 
			and urut='" . $_POST['urut'] . "' 
			and kd_icd9='" . $_POST['kd_icd9'] . "'");
		if ($delete) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}

	public function viewgridriwayatkunjungan()
	{
		$result = $this->db->query("select k.kd_pasien,p.nama,k.tgl_masuk,k.kd_unit,u.nama_unit,k.*,t.kd_kasir,t.no_transaksi,k.kd_dokter
									from kunjungan k
										inner join pasien p on p.kd_pasien=k.kd_pasien
										inner join unit u on u.kd_unit=k.kd_unit
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
									where k.kd_pasien='" . $_POST['kd_pasien'] . "' AND LEFT(k.kd_unit,1) in ('1','2','3')
									order by tgl_masuk desc, jam_masuk desc")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewgridriwayatdiagnosa()
	{
		$result = $this->db->query("SELECT mrp.*,p.penyakit,n.morfologi,k.sebab 
									FROM mr_penyakit mrp 
										LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
										LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
										LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
									WHERE mrp.kd_pasien = '" . $_POST['kd_pasien'] . "' 
										and mrp.kd_unit='" . $_POST['kd_unit'] . "' 
										and mrp.tgl_masuk = '" . $_POST['tgl_masuk'] . "'")->result();
		$row = array();
		for ($i = 0; $i < count($result); $i++) {
			$row[$i]['penyakit'] = $result[$i]->penyakit;
			$row[$i]['kd_penyakit'] = $result[$i]->KD_PENYAKIT;

			if ($result[$i]->STAT_DIAG == 0) {
				$row[$i]['stat_diag'] = 'Diagnosa Awal';
			} else if ($result[$i]->STAT_DIAG == 1) {
				$row[$i]['stat_diag'] = 'Diagnosa Utama';
			} else if ($result[$i]->STAT_DIAG == 2) {
				$row[$i]['stat_diag'] = 'Komplikasi';
			} else if ($result[$i]->STAT_DIAG == 3) {
				$row[$i]['stat_diag'] = 'Diagnosa Sekunder';
			}
			if ($result[$i]->KASUS == '1') {
				$row[$i]['kasus'] =  'Lama';
			} else if ($result[$i]->KASUS == '0') {
				$row[$i]['kasus'] =  'Baru';
			}
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($row) . '}';
	}


	public function viewgridriwayattindakan()
	{
		$result = $this->db->query("select mr.* , ic.deskripsi
									from mr_tindakan mr
									inner join icd_9 ic on ic.kd_icd9=mr.kd_icd9
									where mr.kd_pasien='" . $_POST['kd_pasien'] . "' 
										and mr.kd_unit='" . $_POST['kd_unit'] . "' 
										and mr.tgl_masuk='" . $_POST['tgl_masuk'] . "' 
										and mr.urut_masuk='" . $_POST['urut_masuk'] . "'")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewgridriwayatobat()
	{
		$result = $this->db->query("select bod.*, bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='" . $_POST['kd_pasien'] . "' 
										and bo.kd_unit='" . $_POST['kd_unit'] . "' 
										and t.tgl_transaksi='" . $_POST['tgl_masuk'] . "' 
										and t.urut_masuk='" . $_POST['urut_masuk'] . "'
									order by o.nama_obat")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewgridriwayatlab()
	{
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$result = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
										case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
									From LAB_hasil 
										LEFT join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
										LEFT join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
											on LAB_Test.kd_Test = produk.Kd_Produk
										LEFT join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
										LEFT join unit on unit.kd_unit=lab_hasil.kd_unit_asal
										LEFT join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
									where t.posting_transaksi='t' AND LAB_hasil.Kd_Pasien = '" . $_POST['kd_pasien'] . "' 
										And LAB_hasil.Tgl_Masuk = '" . $_POST['tgl_masuk'] . "'  
										--and LAB_hasil.Urut_Masuk = '" . ($_POST['urut_masuk'] + 1) . "'  
										and LAB_hasil.kd_unit= '" . $kd_unit_lab . "' 
										and lab_hasil.kd_unit_asal='" . $_POST['kd_unit'] . "'
									order by lab_test.kd_lab,lab_test.kd_test asc")->result();

		$row = array();
		for ($i = 0; $i < count($result); $i++) {
			$row[$i]['klasifikasi'] = $result[$i]->klasifikasi;
			$row[$i]['deskripsi'] = $result[$i]->deskripsi;
			$row[$i]['kd_lab'] = $result[$i]->kd_lab;
			$row[$i]['kd_test'] = $result[$i]->kd_test;
			$row[$i]['item_test'] = $result[$i]->item_test;
			$row[$i]['satuan'] = $result[$i]->satuan;
			$row[$i]['normal'] = $result[$i]->normal;
			$row[$i]['kd_metode'] = $result[$i]->kd_metode;
			$row[$i]['judul_item'] = $result[$i]->judul_item;
			$row[$i]['kd_unit_asal'] = $result[$i]->kd_unit_asal;
			$row[$i]['nama_unit_asal'] = $result[$i]->nama_unit_asal;
			$row[$i]['ket_hasil'] = $result[$i]->ket_hasil;
			$row[$i]['urut'] = $result[$i]->urut;

			if ($result[$i]->hasil == 'null' || $result[$i]->hasil == null) {
				$row[$i]['hasil'] = '';
			} else {
				$row[$i]['hasil'] = $result[$i]->hasil;
			}

			if ($result[$i]->ket == 'null' || $result[$i]->ket == null || $result[$i]->ket == 'undefined') {
				$row[$i]['ket'] = '';
			} else {
				$row[$i]['ket'] = $result[$i]->ket;
			}

			if ($result[$i]->satuan == 'null' && $result[$i]->normal == 'null') {
				$row[$i]['metode'] = '';
			} else {
				$row[$i]['metode'] = $result[$i]->metode;
			}
		}

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewgridriwayatrad()
	{
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$result = $this->db->query("SELECT dt.*, p.deskripsi,rh.kd_test,rh.hasil
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi  and rh.kd_test=dt.kd_produk and rh.urut=dt.urut
									where k.kd_pasien='" . $_POST['kd_pasien'] . "' 
										and k.kd_unit='" . $kd_unit_rad . "'
										and tr.kd_unit='" . $_POST['kd_unit'] . "'
										and k.tgl_masuk='" . $_POST['tgl_masuk'] . "' 
										--and k.urut_masuk='" . ($_POST['urut_masuk'] + 1) . "'
										AND t.ispay = 't'
										")->result();

		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function viewanamnese()
	{
		$result = $this->db->query("select anamnese,cat_fisik from kunjungan k
									where k.kd_pasien='" . $_POST['kd_pasien'] . "' 
										and k.kd_unit='" . $_POST['kd_unit'] . "' 
										and k.tgl_masuk='" . $_POST['tgl_masuk'] . "' 
										and k.urut_masuk='" . $_POST['urut_masuk'] . "'");

		if (count($result->result()) > 0) {
			$anamnese = $result->row()->anamnese;
			$catatan 	= $result->row()->cat_fisik;
		} else {
			$anamnese = "";
			$catatan 	= "";
		}

		$result = $this->db->query("SELECT mr_fisik.kondisi, mr_fisik.satuan, mr_dtl.hasil as nilai from 
			mr_konpas mr 
			INNER JOIN mr_konpasdtl mr_dtl ON mr.id_konpas = mr_dtl.id_konpas 
			INNER JOIN mr_kondisifisik mr_fisik ON mr_fisik.id_kondisi = mr_dtl.id_kondisi 
		WHERE 
			mr.kd_pasien = '" . $this->input->post('kd_pasien') . "' 
			and mr.tgl_masuk = '" . $this->input->post('tgl_masuk') . "' 
			and mr.kd_unit = '" . $this->input->post('kd_unit') . "'")->result();

		$resultStatus = $this->db->query("SELECT rwi_rujuk.catatan, mr_status.status from 
			mr_rwi_rujukan rwi_rujuk 
		INNER JOIN mr_status_rwirujukan mr_status ON CAST(rwi_rujuk.id_status as integer) = mr_status.id_status 
		WHERE 
			rwi_rujuk.kd_pasien = '" . $this->input->post('kd_pasien') . "' 
			and rwi_rujuk.tgl_masuk = '" . $this->input->post('tgl_masuk') . "' 
			and rwi_rujuk.kd_unit = '" . $this->input->post('kd_unit') . "'");

		if ($resultStatus->num_rows() > 0) {
			// $catatan 	= $resultStatus->row()->catatan;
			$status 	= $resultStatus->row()->status;
		} else {
			// $catatan 	= "";
			$status 	= "";
		}
		echo "{success:true, anamnese:'$anamnese', ListDataObj:" . json_encode($result) . ", catatan:'" . $catatan . "', status:'" . $status . "'}";
	}

	public function updatestatusantrian()
	{
		$q_kd_unit = "";
		$kd_unit = "";
		if (isset($_POST['kd_unit']) || !empty($_POST['kd_unit'])) {
			$q_kd_unit = " AND kd_unit='" . $_POST['kd_unit'] . "'";
			$kd_unit   = $_POST['kd_unit'];
		}
		//$cek = $this->db->query("select * from antrian_poliklinik where kd_pasien='".$_POST['kd_pasien']."' and tgl_transaksi='".date('Y-m-d')."' and sts_ditemukan='true' and id_status_antrian<>1 ".$q_kd_unit)->result();
		//echo "select * from antrian_poliklinik where kd_pasien='".$_POST['kd_pasien']."' and tgl_transaksi='".date('Y-m-d')."' and sts_ditemukan='true' and id_status_antrian<>1 ".$q_kd_unit;
		//if(count($cek) > 0){
		/* $cekdouble = $this->db->query("select * from antrian_poliklinik where kd_pasien='".$_POST['kd_pasien']."' and tgl_transaksi='".date('Y-m-d')."' and sts_ditemukan='false' and id_status_antrian=1".$q_kd_unit)->result();
			if(count($cekdouble) > 0){
				echo "{success:false, status:'double', message:'Terdapat 2 kunjungan untuk pasien ini'}";
			} else{ */
		//echo "{success:false, status:'ada', message:'Terdapat 2 kunjungan untuk pasien ini' }";
		//}

		//} else {
		$this->db->trans_begin();
		$cekdouble = $this->db->query("select * from antrian_poliklinik where kd_pasien='" . $_POST['kd_pasien'] . "' and tgl_transaksi='" . $_POST['tgl_masuk'] . "' and sts_ditemukan='false' and id_status_antrian=1" . $q_kd_unit);
		if (count($cekdouble->result()) > 1) {
			echo "{success:false, status:'double':message:'Tidak ada antrian pasien ini.'}";
		} else {
			# update STATUS ANTRIAN PASIEN antrian_poliklinik -> TUNGGU PANGGILAN
			//echo count($cekdouble->result());
			$cekqq = $this->db->query("select * from antrian_poliklinik where kd_pasien='" . $_POST['kd_pasien'] . "' " . $q_kd_unit);
			if (count($cekqq->result()) == 0) {
				echo "{success:false, status:'gagal', message:'Tidak ada antrian pasien ini.'}";
			} else {

				$no_antrian = $this->getNoantrian($kd_unit);
				//$no_antrian = $this->getNoantrian($Params['Poli']);
				$dataupdate = array(
					'id_status_antrian' => 1,
					'sts_ditemukan' => 'true',
					'no_antrian' => $no_antrian,
					'no_urut' => $no_antrian
				);
				// $criteria = array('tgl_transaksi'=>date('Y-m-d'),'kd_pasien'=>$_POST['kd_pasien'],'sts_ditemukan'=>'false', 'kd_unit' => $kd_unit);
				$criteria = array('tgl_transaksi' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'], 'kd_unit' => $kd_unit);
				$this->db->where($criteria);
				$update = $this->db->update('antrian_poliklinik', $dataupdate);

				$kodepasien = $_POST['kd_pasien'];
				//$query=_QMS_QUERY("Update antrian_poliklinik set id_status_antrian=1, sts_ditemukan=1, no_antrian='" . $no_antrian ."' where tgl_transaksi='" . date('Y-m-d') . "' and kd_pasien='" . $kodepasien ."' and sts_ditemukan=0".$q_kd_unit);

				//echo $query;

				//               $query = _QMS_QUERY("select * from antrian_poliklinik where $criteria ");
				//      if (count($query->result()) > 0) {
				// _QMS_update('antrian_poliklinik', $dataupdate, $criteria);
				//      } 
				if ($update) {
					# update STATUS ANTRIAN PASIEN kunjungan -> TUNGGU PANGGILAN
					date_default_timezone_set('Asia/Makassar');
					$JamBerkasMasuk = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
					$data = array('id_status_antrian' => 1, 'jam_berkas_masuk' => $JamBerkasMasuk);
					$criteria = array(
						'tgl_masuk' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'],
						'kd_unit' => $kd_unit
					);
					$this->db->where($criteria);
					$updatekunjungan = $this->db->update('kunjungan', $data);
					if ($updatekunjungan) {
						$this->db->trans_commit();
						echo "{success:true, message:'Update data berhasil'}";
					} else {
						$this->db->trans_rollback();
						echo "{success:false, message:'Update data gagal.'}";
					}
				} else {
					echo "{success:false, status:'gagal', message:'Update data gagal.'}";
				}
			}
		}
		//}
	}

	/*
		PERBARUAN 
		OLEH 	: HADAD
		TANGGAL : 2017-08-23
		ALASAN 	: CEK VALIDASI STATUS ANTRIAN
	*/
	public function cek_data_status()
	{
		$response = array();
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'antrian' 		=> $this->input->post('id_antrian'),
			'tgl_masuk' 	=> date_format(date_create($this->input->post('tgl_masuk')), 'Y-m-d'),
		);
		if (strtolower($this->input->post('kd_unit')) == "all") {
			$criteria = "where kd_unit in (" . $this->db->query("SELECT * FROM zusers WHERE kd_user = '" . $this->session->userdata['user_id']['id'] . "'")->row()->kd_unit . ")";
		} else {
			$criteria = "where nama_unit = '" . $this->input->post('kd_unit') . "'";
		}

		$query_unit = $this->db->query("SELECT kd_unit FROM unit " . $criteria);
		if ($query_unit->num_rows() > 0) {
			$tmp_kd_unit = $this->db->query("SELECT * FROM zusers WHERE kd_user = '" . $this->session->userdata['user_id']['id'] . "'")->row()->kd_unit;
			/*$tmp_kd_unit = "";
			foreach ($query_unit->result() as $result) {
				$tmp_kd_unit = "'".$result->kd_unit."',";
			}
			$tmp_kd_unit = substr($tmp_kd_unit, 0, strlen($tmp_kd_unit)-1);
			echo $tmp_kd_unit;die();*/
			// $params['kd_unit'] = $query_unit->row()->kd_unit;
		} else {
			// $params['kd_unit'] = $this->input->post('kd_unit');
			$tmp_kd_unit = "'" . $this->input->post('kd_unit') . "'";
		}

		/*$paramsCriteria = array(
			'kd_pasien' 	=> $params['kd_pasien'],
			'kd_unit' 		=> $params['kd_unit'],
			'tgl_transaksi'	=> $params['tgl_masuk'],
		);*/

		$criteriaAntrian = "";
		if (strtolower($params['antrian']) == strtolower("Tunggu Berkas")) {
			$criteriaAntrian = " and sts_ditemukan = 'false'";
		}
		// echo "aaa"; die;
		$paramsCriteria = "kd_pasien = '" . $params['kd_pasien'] . "' and kd_unit in (" . $tmp_kd_unit . ") and left(kd_unit,1) = '2' and tgl_transaksi = '" . $params['tgl_masuk'] . "' $criteriaAntrian";

		$this->db->select("*");
		$this->db->where($paramsCriteria);
		$this->db->from("antrian_poliklinik");
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$response['status'] = true;
			$response['id_status_antrian'] 	= $query->row()->id_status_antrian;
			$response['kd_unit'] 			= $query->row()->kd_unit;
		} else {
			$response['id_status_antrian'] = 0;
			$response['status'] = false;
		}
		echo json_encode($response);
	}
	public function cek_data_status_LAMA()
	{
		$response = array();
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'tgl_masuk' 	=> date_format(date_create($this->input->post('tgl_masuk')), 'Y-m-d'),
		);

		$query_unit = $this->db->query("SELECT kd_unit FROM unit where nama_unit = '" . $this->input->post('kd_unit') . "'");
		if ($query_unit->num_rows() > 0) {
			$params['kd_unit'] = $query_unit->row()->kd_unit;
		} else {
			$params['kd_unit'] = $this->input->post('kd_unit');
		}

		$paramsCriteria = array(
			'kd_pasien' 	=> $params['kd_pasien'],
			'kd_unit' 		=> $params['kd_unit'],
			'tgl_transaksi'	=> $params['tgl_masuk'],
		);

		$this->db->select("*");
		$this->db->where($paramsCriteria);
		$this->db->from("antrian_poliklinik");
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$response['status'] = true;
			$response['id_status_antrian'] 	= $query->row()->id_status_antrian;
			$response['kd_unit'] 			= $params['kd_unit'];
		} else {
			$response['id_status_antrian'] = 0;
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	public function updatestatusantrian_sedangdilayani()
	{
		$dataupdate = array('id_status_antrian' => $_POST['status']);
		$criteria = array(
			'tgl_transaksi' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'],
			'kd_unit' => $_POST['kd_unit']
		); //'sts_ditemukan'=>'true',
		$this->db->where($criteria);
		$update = $this->db->update('antrian_poliklinik', $dataupdate);

		# Update STATUS ANTRIAN PASIEN kunjungan -> SEDANG DILAYANI
		date_default_timezone_set('Asia/Makassar');
		$JamDilayani = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$data = array('id_status_antrian' => 3, 'jam_dilayani' => $JamDilayani);
		$criteria = array(
			'tgl_masuk' => $_POST['tgl_masuk'], 'kd_pasien' => $_POST['kd_pasien'],
			'kd_unit' => $_POST['kd_unit'], 'urut_masuk' => $_POST['urut_masuk']
		);
		$this->db->where($criteria);
		$updatekunjungan = $this->db->update('kunjungan', $data);
		if ($updatekunjungan) {
			$this->db->trans_commit();
			echo "{success:true}";
		} else {
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}

	function getNoantrian($kd_unit)
	{
		$no = $this->db->query("select TOP 1 max(no_antrian) as no_antrian from antrian_poliklinik 
								where tgl_transaksi='" . date('Y-m-d') . "' and kd_unit='" . $kd_unit . "'
								order by no_antrian desc ");
		if (count($no->result()) > 0) {
			$no_antrian = $no->row()->no_antrian + 1;
		} else {
			$no_antrian = 1;
		}
		return $no_antrian;
	}

	public function getunitscanberkas()
	{
		$result = $this->db->query("select ap.kd_unit,u.nama_unit from antrian_poliklinik ap
									inner join unit u on u.kd_unit=ap.kd_unit 
									where kd_pasien='" . $_POST['kd_pasien'] . "' 
										and tgl_transaksi='" . date('Y-m-d') . "' 
										and id_status_antrian=1 
										and sts_ditemukan='f'")->result();

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function updateunitantrian()
	{
		$this->db->trans_begin();
		# update STATUS ANTRIAN PASIEN antrian_poliklinik -> TUNGGU PANGGILAN
		$no_antrian = $this->getNoantrian($_POST['kd_unit']);
		$dataupdate = array('id_status_antrian' => 1, 'sts_ditemukan' => 'true', 'no_antrian' => $no_antrian);
		$criteria = array(
			'tgl_transaksi' => date('Y-m-d'), 'kd_pasien' => $_POST['kd_pasien'],
			'sts_ditemukan' => 'false', 'kd_unit' => $_POST['kd_unit'], 'id_status_antrian' => 1
		);
		$this->db->where($criteria);
		$update = $this->db->update('antrian_poliklinik', $dataupdate);


		if ($update) {
			# update STATUS ANTRIAN PASIEN kunjungan -> TUNGGU PANGGILAN
			date_default_timezone_set('Asia/Makassar');
			$JamBerkasMasuk = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
			$data = array('id_status_antrian' => 2, 'jam_berkas_masuk' => $JamBerkasMasuk);
			$criteria = array(
				'tgl_masuk' => date('Y-m-d'), 'kd_pasien' => $_POST['kd_pasien'],
				'kd_unit' => $_POST['kd_unit']
			);
			$this->db->where($criteria);
			$updatekunjungan = $this->db->update('kunjungan', $data);
			if ($updatekunjungan) {
				$this->db->trans_commit();
				echo "{success:true}";
			} else {
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else {
			echo "{success:false}";
		}
	}

	public function getDokterPoli()
	{
		$kd_unit = $this->db->query("select setting from sys_setting where key_data='rwj_default_kd_unit'")->row()->setting;
		if ($_POST['kd_unit'] != '') {
			$kdunit = " dk.kd_unit='" . $_POST['kd_unit'] . "'";
		} else {
			$kdunit = " left(kd_unit,1) = '" . $kd_unit . "' ";
		}

		$result = $this->db->query("select distinct(d.kd_dokter),d.nama
									from dokter d
									inner join dokter_klinik dk on dk.kd_dokter=d.kd_dokter
									where " . $kdunit . " and d.kd_dokter != 'XXX'
									order by nama")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function updateDokterPenindak()
	{
		# Upadate kd_dokter KUNJUNGAN
		$criteria = array(
			"kd_pasien" => $_POST['kd_pasien'], "kd_unit" => $_POST['kd_unit'],
			"urut_masuk" => $_POST['urut_masuk'], "tgl_masuk" => $_POST['tgl_masuk']
		);
		$data = array("kd_dokter" => $_POST['kd_dokter']);
		$this->db->where($criteria);
		$update = $this->db->update('kunjungan', $data);

		if ($update) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}

	public function viewgridlasthistorydiagnosa()
	{
		$result = $this->db->query("select *,case when mr.stat_diag = 0 then 'Diagnosa Awal' when mr.stat_diag = 1 then 'Diagnosa Utama' when stat_diag = 2 then 'Komplikasi' else 'Diagnosa Sekunder' end as status_diag,
										case when mr.kasus = '1' then 'Lama' else 'Baru' end as kasuss
									from mr_penyakit mr
										inner join penyakit p on p.kd_penyakit=mr.kd_penyakit
									WHERE mr.kd_pasien = '" . $_POST['kd_pasien'] . "'
									ORDER BY p.penyakit")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function cekKomponen()
	{
		/*$result = $this->db->query("select count(pc.kd_component) as komponen
									from tarif_component tc
										inner join produk_component pc on pc.kd_component = tc.kd_component
										inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
									where kd_produk = '".$_POST['kd_produk']."' and kd_tarif = '".$_POST['kd_tarif']."' 
									and kd_unit = '".$_POST['kd_unit']."' and jc.kd_jenis = '2' 
										and tgl_berlaku = (select tgl_berlaku
											from tarif_component tc
												inner join produk_component pc on pc.kd_component = tc.kd_component
												inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
											where kd_produk = '".$_POST['kd_produk']."' and kd_tarif = '".$_POST['kd_tarif']."' 
											and kd_unit = '".$_POST['kd_unit']."' and jc.kd_jenis = '2'
											order by tgl_berlaku desc limit 1)")->row()->komponen;*/
		$queryCekKomponent 			= $this->db->query("SELECT * FROM tarif_component WHERE kd_produk = '" . $this->input->post('kd_produk') . "' and kd_tarif = '" . $this->input->post('kd_tarif') . "' and kd_component in ('20', '21','27','28','29','31')");
		if ($queryCekKomponent->num_rows() > 0) {
			$result 			= true;
		} else {
			$result 			= false;
		}
		echo '{success:true, komponen:' . $result . '}';
	}

	public function viewgridjasadokterpenindak()
	{
		$result = $this->db->query("select TOP 1 pc.kd_component, component, tgl_berlaku
									from tarif_component tc
										inner join produk_component pc on pc.kd_component = tc.kd_component
										inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
									where kd_produk = '" . $_POST['kd_produk'] . "' and kd_tarif = '" . $_POST['kd_tarif'] . "' and kd_unit = '" . $_POST['kd_unit'] . "' and jc.kd_jenis = '20' 
										and tgl_berlaku = (select tgl_berlaku
											from tarif_component tc
												inner join produk_component pc on pc.kd_component = tc.kd_component
												inner join jenis_component jc on pc.kd_jenis = jc.kd_jenis
											where kd_produk = '" . $_POST['kd_produk'] . "' and kd_tarif = '" . $_POST['kd_tarif'] . "' and kd_unit = '" . $_POST['kd_unit'] . "' and jc.kd_jenis = '20'
											 )")->result();
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
	public function refreshdatagridpenatajasaRWJ()
	{
		/*$Status='false';
		$kdbagian=2;
		//$hari=date('d') -1;
		$kd_user = $this->session->userdata['user_id']['id'];
		$unit = $this->db->query("select kd_unit from zusers where kd_user = '".$kd_user."'")->row();
		$unitpoli = $unit->kd_unit;
		
		if($kd_user == '0')
		{
			$criteria = "(kd_unit <> '0') AND";
		}
		else
		{
			$criteria = ""; 
		}
		if (strlen($_POST['KataKunciNya'])!==0)
                        {
							$_POST['KataKunciNya']=str_replace('~',$_POST['KataKunciNya']);
							$krite="where ".$criteria."  co_status = '".$Status."' and kd_bagian ='".$kdbagian."'    ".$_POST['KataKunciNya']. " order by tgl_masuk desc limit 500 ";
							//$this->db->where(str_replace("~", "'",$criteria ." co_status = '".$Status."' and kd_bagian ='".$kdbagian."'    ".$_POST['KataKunci']. " order by tgl_masuk desc limit 500 "  ) ,null, false) ;
							
						}	else{
								$krite="where ".$criteria . " posting_transaksi = 'FALSE' and co_status = '".$Status."' and kd_bagian =  '".$kdbagian."' 
								 and tgl_transaksi in('".date('Y-m-d 00:00:00')."') order by tgl_masuk desc limit 500";
								}
		// echo "select  * from (
											// select customer.customer,unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, 
											// case when dokter.nama = '0' then 'BELUM DIPILIH' else dokter.nama end as nama_dokter, 
											// kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist,transaksi.posting_transaksi, 
											// knt.jenis_cust,case when kunjungan.id_status_antrian=1 then 'Tunggu Berkas' when kunjungan.id_status_antrian=2 then 'Tunggu Panggilan' 
											// when kunjungan.id_status_antrian=3 then 'Sedang Dilayani' else 'Selesai' end as status_antrian , ap.no_antrian
											// from (((((unit 
											// inner join kunjungan on kunjungan.kd_unit=unit.kd_unit)   
											// left join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											// inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
											// inner join customer on customer.kd_customer= kunjungan.kd_customer)
											// left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
											  
											// inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
											// and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk
											// left join antrian_poliklinik ap on kunjungan.kd_pasien=ap.kd_pasien and kunjungan.tgl_masuk=ap.tgl_transaksi and kunjungan.kd_unit=ap.kd_unit
										// )as resdata $krite";
		$result=$this->db->query("select  * from (
											select customer.customer,unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, 
											case when dokter.nama = '0' then 'BELUM DIPILIH' else dokter.nama end as nama_dokter, 
											kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist,transaksi.posting_transaksi, 
											knt.jenis_cust,case when kunjungan.id_status_antrian=1 then 'Tunggu Berkas' when kunjungan.id_status_antrian=2 then 'Tunggu Panggilan' 
											when kunjungan.id_status_antrian=3 then 'Sedang Dilayani' else 'Selesai' end as status_antrian , ap.no_antrian
											from (((((unit 
											inner join kunjungan on kunjungan.kd_unit=unit.kd_unit)   
											left join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
											inner join customer on customer.kd_customer= kunjungan.kd_customer)
											left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
											  
											inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
											and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk
											left join antrian_poliklinik ap on kunjungan.kd_pasien=ap.kd_pasien and kunjungan.tgl_masuk=ap.tgl_transaksi and kunjungan.kd_unit=ap.kd_unit
										)as resdata $krite")->result();
		foreach ($result as $rec)
			{
				$o=array();
				$o['KD_CUSTOMER']=$rec->kd_customer;
				$o['CUSTOMER']=$rec->customer;
				$o['NAMA_UNIT']=$rec->nama_unit;
				$o['NAMA']=$rec->nama;
				  $o['KD_PASIEN']=$rec->kode_pasien;
				  $o['TANGGAL_TRANSAKSI']=$rec->tgl_transaksi;
				  $o['ALAMAT']=$rec->alamat;
				  $o['NAMA_DOKTER']=$rec->nama_dokter;
				  $o['NO_TRANSAKSI']=$rec->no_transaksi;
				  $o['KD_KASIR']=$rec->kd_kasir;
				  $o['CO_STATUS']=$rec->co_status;
				  $o['KD_KELAS']=$rec->kd_kelas;
				  $o['KD_DOKTER']=$rec->kd_dokter;
				  $o['URUT_MASUK']=$rec->urut_masuk;
				  $o['POSTING_TRANSAKSI']=$rec->posting_transaksi;
				  $o['ANAMNESE']=$rec->anamnese;
				  $o['CAT_FISIK']=$rec->cat_fisik;
				  $o['KD_KASIR']=$rec->kd_kasir;
				  $o['ASAL_PASIEN']=$rec->asal_pasien;
				  $o['CARA_PENERIMAAN']=$rec->cara_penerimaan;
				  $o['STATUS_ANTRIAN']=$rec->status_antrian;
				  $o['NO_ANTRIAN']=$rec->no_antrian;
				  $list[]=$o;	
			} 
		if($result){
			echo '{success:true,  ListDataObj:'.json_encode($list).'}';
		} else{
			echo '{success:false}';
		}		*/
	}
	public function savejasadokterpenindak()
	{
		$this->db->query("DELETE from detail_trdokter where kd_kasir='" . $_POST['kd_kasir'] . "'
								and no_transaksi='" . $_POST['no_transaksi'] . "' and urut=" . $_POST['urut'] . " and tgl_transaksi='" . $_POST['tgl_transaksi'] . "'");
		for ($i = 0; $i < $_POST['jumlah']; $i++) {
			$cek = $this->db->query("select * from detail_trdokter where kd_kasir='" . $_POST['kd_kasir'] . "'
								and no_transaksi='" . $_POST['no_transaksi'] . "' and urut=" . $_POST['urut'] . " and tgl_transaksi='" . $_POST['tgl_transaksi'] . "' 
								and kd_component=" . $_POST['kd_component-' . $i] . "");
			if ($_POST['kd_dokter-' . $i] !== '') {
				$kd_component_dokter = $_POST['kd_component-' . $i];
				$params_get_component_dokter = array(
					'kd_kasir' 		=> $_POST['kd_kasir'],
					'no_transaksi' 	=> $_POST['no_transaksi'],
					'tgl_transaksi'	=> $_POST['tgl_transaksi'],
					'urut'			=> $_POST['urut'],
					'kd_component'	=> $kd_component_dokter,
				);
				$result_coponent_dokter = $this->M_produk->getDataComponentDokter($params_get_component_dokter);

				if (count($cek->result()) > 0) {
					$dataubah = array("kd_dokter" => $_POST['kd_dokter-' . $i], "jp" => $result_coponent_dokter->row()->tarif);
					$criteria = array(
						"kd_kasir" => $_POST['kd_kasir'], "no_transaksi" => $_POST['no_transaksi'], "urut" => $_POST['urut'],
						"tgl_transaksi" => $_POST['tgl_transaksi'], "kd_component" => $_POST['kd_component-' . $i]
					);
					$this->db->where($criteria);
					$result = $this->db->update("detail_trdokter", $dataubah);
				} else {

					$data = array(
						"kd_kasir" => $_POST['kd_kasir'], "no_transaksi" => $_POST['no_transaksi'], "urut" => $_POST['urut'],
						"kd_dokter" => $_POST['kd_dokter-' . $i], "tgl_transaksi" => $_POST['tgl_transaksi'],
						"kd_component" => $_POST['kd_component-' . $i], "jp" => $result_coponent_dokter->row()->tarif
					);
					$result = $this->db->insert("detail_trdokter", $data);
				}
			}
		}

		if ($result) {
			echo '{success:true}';
		} else {
			echo '{success:false}';
		}
	}
	/* public function savejasadokterpenindak(){
		for($i=0;$i<$_POST['jumlah'];$i++){
			$cek = $this->db->query("select * from detail_trdokter where kd_kasir='".$_POST['kd_kasir']."'
								and no_transaksi='".$_POST['no_transaksi']."' and urut=".$_POST['urut']." and tgl_transaksi='".$_POST['tgl_transaksi']."' 
								and kd_component=".$_POST['kd_component-'.$i]."");
			if ($_POST['kd_dokter-'.$i]!=='')
			{						
				if(count($cek->result()) > 0){
					$dataubah = array("kd_dokter"=>$_POST['kd_dokter-'.$i],"jp"=>$_POST['harga']);
					$criteria = array("kd_kasir"=>$_POST['kd_kasir'],"no_transaksi"=>$_POST['no_transaksi'],"urut"=>$_POST['urut'],
									"tgl_transaksi"=>$_POST['tgl_transaksi'],"kd_component"=>$_POST['kd_component-'.$i]);
					$this->db->where($criteria);			
					$result = $this->db->update("detail_trdokter",$dataubah);
				} else{
					$data = array("kd_kasir"=>$_POST['kd_kasir'],"no_transaksi"=>$_POST['no_transaksi'],"urut"=>$_POST['urut'],
							"kd_dokter"=>$_POST['kd_dokter-'.$i],"tgl_transaksi"=>$_POST['tgl_transaksi'],
							"kd_component"=>$_POST['kd_component-'.$i],"jp"=>$_POST['harga']);
					$result = $this->db->insert("detail_trdokter",$data);
				}
			}	
		}
		
		if($result){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}		
	} */

	public function getdokterpenindak()
	{
		//echo $_POST['penjas'];
		if ($_POST['penjas'] == 'rwj') {
			$kd_unit = $_POST['kd_unit'];
			$dok = 'dokter_klinik';
			$result = $this->db->query("SELECT distinct(d.kd_dokter),d.nama
									from dokter d
									
									where --dk.kd_unit='" . $kd_unit . "' and 
									d.kd_dokter != 'XXX'
									and upper(d.nama) like upper('" . $_POST['text'] . "%')
									order by nama")->result();
		} else if ($_POST['penjas'] == 'igd') {
			$kd_unit = $_POST['kd_unit'];
			$dok = 'dokter_klinik';
			$result = $this->db->query("SELECT distinct(d.kd_dokter),d.nama
									from dokter d
									inner join $dok dk on dk.kd_dokter=d.kd_dokter
									where --dk.kd_unit='" . $kd_unit . "' and 
									d.kd_dokter != 'XXX'
									and upper(d.nama) like upper('" . $_POST['text'] . "%')
									order by nama")->result();
		} else if ($_POST['penjas'] == 'rwi') {
			$kd_unit = $_POST['kd_unit'];
			$dok = 'dokter_inap';
			$result = $this->db->query("SELECT distinct(d.kd_dokter),d.nama
									from dokter d
									inner join $dok dk on dk.kd_dokter=d.kd_dokter
									where --dk.kd_unit='" . $kd_unit . "' and 
									d.kd_dokter != 'XXX'
									and upper(d.nama) like upper('" . $_POST['text'] . "%')
									order by nama")->result();
		}



		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function viewgrideditjasadokterpenindak()
	{
		$data_dokter 	= array();
		$index 			= 0;
		$params = array(
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'urut' 			=> $this->input->post('urut'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'kd_tarif' 		=> $this->input->post('kd_tarif'),
		);
		$paramsCriteria = array(
			'kd_produk'	=> $params['kd_produk'],
			'kd_tarif' 	=> $params['kd_tarif'],
			'kd_unit' 	=> $params['kd_unit'],
		);
		$queryComponent = $this->db
			->select("*")
			->where($paramsCriteria)
			->get("tarif_component");
		if ($queryComponent->num_rows > 0) {
			$criteriaKdComponent = "";
			foreach ($queryComponent->result() as $result) {
				$criteriaKdComponent .= "'" . $result->kd_component . "',";
			}
			$criteriaKdComponent = substr($criteriaKdComponent, 0, strlen($criteriaKdComponent) - 1);

			$queryProdukComponent = $this->db->query("SELECT * from produk_component WHERE kd_component in(" . $criteriaKdComponent . ")");


			foreach ($queryProdukComponent->result() as $result) {
				unset($paramsCriteria);
				$paramsCriteria = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $params['urut'],
					'tgl_transaksi' => $params['tgl_transaksi'],
					'kd_component' 	=> $result->kd_component,
				);
				$queryDetailTRDokter = $this->db
					->select("*")
					->where($paramsCriteria)
					->get("detail_trdokter");
				// $queryDetailTRDokter = $this->db->select("detail_trdokter", $paramsCriteria);
				if ($queryDetailTRDokter->num_rows() > 0) {
					unset($paramsCriteria);
					$paramsCriteria = array(
						'kd_dokter' 	=> $queryDetailTRDokter->row()->kd_dokter,
					);
					$queryDokter  		= $this->db
						->select("*")
						->where($paramsCriteria)
						->get("dokter");
					// $queryDokter = $this->db->select("dokter", $paramsCriteria);
					$data_dokter[$index]['component'] 		= $result->component;
					$data_dokter[$index]['jp']				= $queryDetailTRDokter->row()->jp;
					$data_dokter[$index]['kd_component']	= $result->kd_component;
					$data_dokter[$index]['kd_dokter']		= $queryDetailTRDokter->row()->kd_dokter;
					$data_dokter[$index]['kd_kasir']		= $queryDetailTRDokter->row()->kd_kasir;
					$data_dokter[$index]['nama']			= $queryDokter->row()->nama;
					$data_dokter[$index]['no_transaksi']	= $queryDetailTRDokter->row()->no_transaksi;
					$data_dokter[$index]['pajak']			= $queryDetailTRDokter->row()->pajak;
					$data_dokter[$index]['pot_ops']			= $queryDetailTRDokter->row()->pot_ops;
					$data_dokter[$index]['tgl_transaksi']	= $queryDetailTRDokter->row()->tgl_transaksi;
					$data_dokter[$index]['tim_persen']		= $queryDetailTRDokter->row()->tim_persen;
					$data_dokter[$index]['urut']			= $queryDetailTRDokter->row()->urut;
				} else {
					$data_dokter[$index]['component'] 		= $result->component;
					$data_dokter[$index]['jp']				= "";
					$data_dokter[$index]['kd_component']	= $result->kd_component;
					$data_dokter[$index]['kd_dokter']		= "";
					$data_dokter[$index]['kd_kasir']		= $params['kd_kasir'];
					$data_dokter[$index]['nama']			= "";
					$data_dokter[$index]['no_transaksi']	= $params['no_transaksi'];
					$data_dokter[$index]['pajak']			= "";
					$data_dokter[$index]['pot_ops']			= "";
					$data_dokter[$index]['tgl_transaksi']	= $params['tgl_transaksi'];
					$data_dokter[$index]['tim_persen']		= "";
					$data_dokter[$index]['urut']			= $params['urut'];
				}
				$index++;
			}
		}
		/*$result = $this->db->query("select tr.*, d.nama,pc.component 
									from detail_trdokter tr
										inner join dokter d on d.kd_dokter=tr.kd_dokter
										inner join produk_component pc on pc.kd_component=tr.kd_component
									where 
									kd_kasir='".$_POST['kd_kasir']."' and 
									no_transaksi='".$_POST['no_transaksi']."' and 
									urut=".$_POST['urut']." and 
									tgl_transaksi='".$_POST['tgl_transaksi']."'
									order by pc.component")->result();*/
		echo '{success:true, totalrecords:' . count($data_dokter) . ', ListDataObj:' . json_encode($data_dokter) . '}';
	}

	/* 
		PERBARUAN GANTI KELOMPOK PASIEN 
		OLEH 	; HADAD
		TANGGAL : 2016 - 12 - 31
	 */
	public function UpdateGantiKelompok()
	{
		// $this->dbSQL->trans_commit();
		$this->db->trans_begin();

		// $resultSQL 	= false;
		$resultPG 	= false;

		$Kdcustomer       = $_POST['KDCustomer'];
		$KdNoSEP          = $_POST['KDNoSJP'];
		$KdNoAskes        = $_POST['KDNoAskes'];
		$KdPasien         = $_POST['KdPasien'];
		$TglMasuk         = $_POST['TglMasuk'];
		$KdUnit           = $_POST['KdUnit'];
		$UrutMasuk        = $_POST['UrutMasuk'];
		$alasan           = $_POST['alasan'];
		$query_customer 	= $this->db->query("SELECT kd_customer from kunjungan WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");
		$query_history 	= $this->db->query("SELECT max(urut) as urut from history_ganti_customer WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");
		if ($query_history->num_rows() > 0) {
			$tmp_urut 	= (int)$query_history->row()->urut + 1;
		} else {
			$tmp_urut 	= 1;
		}
		$resultQuery = 0;

		$paramsInsert = array(
			'kd_pasien'           => $KdPasien,
			'kd_unit'             => $KdUnit,
			'tgl_masuk'           => $TglMasuk,
			'urut_masuk'          => $UrutMasuk,
			'tgl_update'          => date('Y-m-d') . " 00:00:00",
			'jam_update'          => "1900-01-01 " . date('H:i:s'),
			'keterangan'          => $alasan,
			'urut'                => $tmp_urut,
			'kd_user'             => $this->session->userdata['user_id']['id'],
			'kd_customer_sebelum' => $query_customer->row()->kd_customer,
			'kd_customer_sesudah' => $Kdcustomer,
		);
		$result = $this->db->insert("history_ganti_customer", $paramsInsert);

		if ($result > 0) {
			$resultPG 	= $this->db->query("UPDATE kunjungan 
				SET kd_customer = '" . $Kdcustomer . "', 
				no_sjp='" . $KdNoSEP . "' ,status_sinkronisasi=2 
				WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");

			// $resultSQL 	= $this->dbSQL->query("UPDATE kunjungan SET 
			// 	kd_customer = '".$Kdcustomer."', 
			// 	no_sjp='".$KdNoSEP."' 
			// 	WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");

		}

		if (($resultPG > 0 || $resultPG === true) && isset($KdNoAskes)) {
			$paramsUpdate = array(
				'no_asuransi' 	=> $KdNoAskes,
			);

			$paramsCriteria = array(
				'kd_pasien' 	=> $KdPasien,
			);

			$this->db->where($paramsCriteria);
			$resultPG = $this->db->update("pasien", $paramsUpdate);

			// $this->dbSQL->where($paramsCriteria);
			// $resultSQL = $this->dbSQL->update("pasien", $paramsUpdate);
		} else {
			// $resultSQL 	= false;
			$resultPG 	= false;
		}

		if (($resultPG > 0 || $resultPG === true)) {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$response['success'] = true;
			echo '{success:true, totalrecords:' . count($resultPG) . ', ListDataObj:' . json_encode($resultPG) . '}';
		} else {
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$response['success'] = false;
			echo '{success:false, totalrecords:' . count($resultPG) . ', ListDataObj:' . json_encode($resultPG) . '}';
		}
		$this->db->close();
		// $this->dbSQL->close();
	}

	/* 
		PERBARUAN GANTI KELOMPOK PASIEN 
		OLEH 	; HADAD
		TANGGAL : 2017 - 01 - 05
	*/


	public function UpdateGantiDokter()
	{
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$KdDokterAsal	= 	$this->db->query("SELECT 
								kd_dokter 
								FROM kunjungan 
								WHERE 
								kd_pasien='" . $KdPasien . "' 
								AND tgl_masuk='" . $TglMasuk . "' 
								AND kd_unit='" . $KdUnit . "' 
								AND urut_masuk='" . $UrutMasuk . "'")->row()->kd_dokter;
		$resultQuery = 0;
		$result      = $this->db->query("UPDATE kunjungan SET kd_dokter = '" . $KdDokter . "' ,status_sinkronisasi=2  WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");


		if ($result > 0) {
			$resultQuery 		= _QMS_Query("UPDATE kunjungan SET 
								kd_dokter = '" . $KdDokter . "' 
								WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "' AND urut_masuk='" . $UrutMasuk . "'");

			$resultNoTr 		= $this->db->query("
								SELECT no_transaksi FROM transaksi WHERE 
								kd_pasien='" . $KdPasien . "' AND tgl_transaksi='" . $TglMasuk . "' AND kd_unit='" . $KdUnit . "'");

			if ($resultNoTr->num_rows() > 0) {
				$result 		= $this->db->query("
								UPDATE detail_trdokter SET kd_dokter='" . $KdDokter . "' WHERE no_transaksi='" . $resultNoTr->row()->no_transaksi . "' AND kd_dokter='" . $KdDokterAsal . "'
								");

				if ($result > 0) {
					$resultQuery = _QMS_Query("
								UPDATE detail_trdokter SET kd_dokter='" . $KdDokter . "' WHERE no_transaksi='" . $resultNoTr->row()->no_transaksi . "' AND kd_dokter='" . $KdDokterAsal . "'
								");
				}
			}
		}
		echo '{success:true, totalrecords:' . count($resultQuery) . ', ListDataObj:' . json_encode($resultQuery) . '}';
	}
	public function UpdateGantiUnit()
	{

		date_default_timezone_set('Asia/Makassar');
		$TglAntrian = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$conv_tglmasuk = date('Y/m/d');
		$tglmasuk = date('Y-m-d');

		$db = $this->load->database('otherdb2', TRUE);
		$_kduser = $this->session->userdata['user_id']['id'];

		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$NoTransaksi	= $_POST['NoTransaksi'];
		$KdUnitDulu		= $_POST['KdUnitDulu'];
		$resultQuery = 0;
		$result 	= $this->db->query("UPDATE kunjungan SET kd_unit = '" . $KdUnit . "', kd_dokter = '" . $KdDokter . "' WHERE kd_pasien='" . $KdPasien . "' AND kd_unit='" . $KdUnitDulu . "' AND tgl_masuk='" . $TglMasuk . "' AND urut_masuk='" . $UrutMasuk . "'");
		if ($result > 0) {
			/* $resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_unit = '".$KdUnit."',
							kd_dokter = '".$KdDokter."' 
							WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnitDulu."' AND urut_masuk='".$UrutMasuk."'"); */
			/*if($resultQuery>0){
				$result = $this->db->query("UPDATE transaksi SET kd_unit = '".$KdUnit."' WHERE no_transaksi='".$NoTransaksi."'");
				if($result>0){
					$resultQuery = _QMS_Query("UPDATE transaksi SET kd_unit = '".$KdUnit."' WHERE no_transaksi='".$NoTransaksi."'");
				}
			}*/
		}
		# pertama kali daftar no_antrian belum digenerate, tapi dari tracer setelah berkas ditemukan.
		// $no_antrian = $this->getNoantrian($Params['Poli']);
		$no_antrian = 0;

		# *** Status pasien lama/baru untuk identifikasi tracer ***
		# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
		/* $kunj = _QMS_QUERY("select * from kunjungan where kd_pasien='".$KdPasien."' ");
		if ($kunj->num_rows() == 0) {
			$baru = 1;
		} else {
			$baru = 0;
		}
 */
		$kdunitprioritas = $this->db->query("select setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'")->row()->setting;
		$sqlTracer_no_antrian_prioritas = $this->db->query("select count(*) as jum_prioritas 
													from kunjungan 
													where tgl_masuk='" . $tglmasuk . "' and kd_unit in(" . $kdunitprioritas . ")")->row()->jum_prioritas;
		if ($sqlTracer_no_antrian_prioritas == 0) {
			$no_prioritas = 1;
		} else {
			$no_prioritas = $sqlTracer_no_antrian_prioritas + 1;
		}


		/*$q_update_antrian_poliSQL=_QMS_Query("UPDATE antrian_poliklinik set kd_unit='".$KdUnit."' 
												where  
												kd_pasien='".$KdPasien."' AND no_urut='".$UrutMasuk."'"); */

		# No Antrian Cetak
		/*$cek_dulu_atrian_polinya=_QMS_Query("select top 1 no_urut from antrian_poliklinik 
		where --convert(varchar(11),tgl_transaksi,111) ='".$conv_tglmasuk."' and 
		kd_unit='".$KdUnit."' order by no_urut desc")->result();
		if (count($cek_dulu_atrian_polinya)==0)
		{
			$urutCetak=1;
		}
		else
		{
			$sqlTracer_no_antrian_poli = _QMS_Query("select top 1 no_urut from antrian_poliklinik 
			where --convert(varchar(11),tgl_transaksi,111) ='".$conv_tglmasuk."' and 
			kd_unit='".$KdUnit."' order by no_urut desc")->row()->no_urut;
			if($sqlTracer_no_antrian_poli == 0){
				$urutCetak=1;
			} else{
				$urutCetak=$sqlTracer_no_antrian_poli + 1;
				//echo $urutCetak;
			}
		}*/

		$getUrut = $this->db->query("SELECT max(no_urut) as no_urut FROM antrian_poliklinik where tgl_transaksi between '" . $tglmasuk . " 00:00:00.000" . "' and '" . $TglAntrian . "' and kd_unit = '" . $KdUnit . "' ");

		if ($getUrut->row()->no_urut > 0) {
			$getUrut = $getUrut->row()->no_urut + 1;
		} else {
			$getUrut = 1;
		}

		/* _QMS_Query("INSERT INTO antrian_poliklinik (KD_UNIT, TGL_TRANSAKSI, NO_URUT, KD_PASIEN, KD_USER, ID_STATUS_ANTRIAN, STS_DITEMUKAN, NO_ANTRIAN, NO_PRIORITAS) VALUES (
			'".$KdUnit."', '".$TglAntrian."',  '".$getUrut."', '".$KdPasien."', '".$_kduser."', 0, 0, '', '')"); */
		$this->db->query("INSERT INTO antrian_poliklinik (KD_UNIT, TGL_TRANSAKSI, NO_URUT, KD_PASIEN, KD_USER, ID_STATUS_ANTRIAN, STS_DITEMUKAN, NO_ANTRIAN, NO_PRIORITAS) VALUES (
			'" . $KdUnit . "', '" . $TglAntrian . "',  '" . $getUrut . "', '" . $KdPasien . "', '" . $_kduser . "', 0, 'false', 0, 0)");

		/* _QMS_Query("DELETE FROM antrian_poliklinik WHERE kd_pasien='".$KdPasien."' and kd_unit='".$KdUnitDulu."'");	 */

		$this->db->query("DELETE FROM antrian_poliklinik WHERE kd_pasien='" . $KdPasien . "' and kd_unit='" . $KdUnitDulu . "'");
		# Update antrian poliklinik set no_urut dan no_prioritas

		/*$q_update_antrian_poli_ss=$db->query("updates antrian_poliklinik set kd_unit='".$KdUnit."', no_urut=".$urutCetak." 
												where kd_unit='".$KdUnit."' and tgl_transaksi='".$tglmasuk."' 
													and no_urut=".$urutCetak." and kd_pasien='".$KdPasien."' and kd_user=".$_kduser.""); */


		/*$q_update_antrian_poli=$this->db->query("updates antrian_poliklinik set kd_unit='".$KdUnit."', no_urut=".$urutCetak." 
												where kd_unit='".$KdUnit."' and tgl_transaksi='".$tglmasuk."' 
													and no_urut=".$urutCetak." and kd_pasien='".$KdPasien."' and kd_user=".$_kduser."");*/

		/* $q_simpan_antrian_poli_ss=$db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$KdUnit."','".$TglAntrian."',".$urutCetak.",'".$KdPasien."',$_kduser,0,0,".$no_antrian.",".$no_prioritas.")"); 
		$q_simpan_antrian_poli=$this->db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$KdUnit."','".$tglmasuk."',".$urutCetak.",'".$KdPasien."',$_kduser,0,FALSE,".$no_antrian.",".$no_prioritas." )"); */
		# POSTGRES

		$sqlCekReg = "select count(kd_pasien) as jum from reg_unit where kd_unit='" . $KdUnit . "' AND kd_pasien='" . $KdPasien . "'";
		$arrCekReg = $this->db->query($sqlCekReg)->row()->jum;
		if ($arrCekReg == 0) {
			$sqlReg = "select max(no_register) as no from reg_unit where kd_unit='" . $KdUnit . "'";
			$resReg = $this->db->query($sqlReg)->row()->no;
			$resReg++;
			$arrReg = array(
				'kd_pasien' => $KdPasien,
				'kd_unit' => $KdUnit,
				'no_register' => $resReg,
				'urut_masuk' => 0
			);
			$this->db->insert('reg_unit', $arrReg);
		}
		/*
		if (isset($_POST['KdKasir'])) {
			if ($_POST['KdKasir'] == '01') {

			}

		}*/
		echo '{success:true, totalrecords:' . count($resultQuery) . ', ListDataObj:' . json_encode($resultQuery) . '}';
	}
	public function UpdateGantiUnit_lama()
	{

		date_default_timezone_set('Asia/Makassar');
		$TglAntrian = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$conv_tglmasuk = date('Y/m/d');
		$tglmasuk = date('Y-m-d');

		$db = $this->load->database('otherdb2', TRUE);
		$_kduser = $this->session->userdata['user_id']['id'];

		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$NoTransaksi	= $_POST['NoTransaksi'];
		$KdUnitDulu		= $_POST['KdUnitDulu'];
		$resultQuery = 0;
		$result 	= $this->db->query("UPDATE kunjungan SET kd_unit = '" . $KdUnit . "', kd_dokter = '" . $KdDokter . "' WHERE kd_pasien='" . $KdPasien . "' AND kd_unit='" . $KdUnitDulu . "' AND tgl_masuk='" . $TglMasuk . "' AND urut_masuk='" . $UrutMasuk . "'");
		if ($result > 0) {
			$resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_unit = '" . $KdUnit . "',
							kd_dokter = '" . $KdDokter . "' 
							WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnitDulu . "' AND urut_masuk='" . $UrutMasuk . "'");
			/*if($resultQuery>0){
				$result = $this->db->query("UPDATE transaksi SET kd_unit = '".$KdUnit."' WHERE no_transaksi='".$NoTransaksi."'");
				if($result>0){
					$resultQuery = _QMS_Query("UPDATE transaksi SET kd_unit = '".$KdUnit."' WHERE no_transaksi='".$NoTransaksi."'");
				}
			}*/
		}
		# pertama kali daftar no_antrian belum digenerate, tapi dari tracer setelah berkas ditemukan.
		// $no_antrian = $this->getNoantrian($Params['Poli']);
		$no_antrian = 0;

		# *** Status pasien lama/baru untuk identifikasi tracer ***
		# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
		$kunj = _QMS_QUERY("select * from kunjungan where kd_pasien='" . $KdPasien . "' ");
		if ($kunj->num_rows() == 0) {
			$baru = 1;
		} else {
			$baru = 0;
		}

		$kdunitprioritas = $this->db->query("select setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'")->row()->setting;
		$sqlTracer_no_antrian_prioritas = _QMS_Query("select count(*) as jum_prioritas 
													from kunjungan 
													where tgl_masuk='" . $tglmasuk . "' and kd_unit in(" . $kdunitprioritas . ")")->row()->jum_prioritas;
		if ($sqlTracer_no_antrian_prioritas == 0) {
			$no_prioritas = 1;
		} else {
			$no_prioritas = $sqlTracer_no_antrian_prioritas + 1;
		}


		/*$q_update_antrian_poliSQL=_QMS_Query("UPDATE antrian_poliklinik set kd_unit='".$KdUnit."' 
												where  
												kd_pasien='".$KdPasien."' AND no_urut='".$UrutMasuk."'"); */

		# No Antrian Cetak
		/*$cek_dulu_atrian_polinya=_QMS_Query("select top 1 no_urut from antrian_poliklinik 
		where --convert(varchar(11),tgl_transaksi,111) ='".$conv_tglmasuk."' and 
		kd_unit='".$KdUnit."' order by no_urut desc")->result();
		if (count($cek_dulu_atrian_polinya)==0)
		{
			$urutCetak=1;
		}
		else
		{
			$sqlTracer_no_antrian_poli = _QMS_Query("select top 1 no_urut from antrian_poliklinik 
			where --convert(varchar(11),tgl_transaksi,111) ='".$conv_tglmasuk."' and 
			kd_unit='".$KdUnit."' order by no_urut desc")->row()->no_urut;
			if($sqlTracer_no_antrian_poli == 0){
				$urutCetak=1;
			} else{
				$urutCetak=$sqlTracer_no_antrian_poli + 1;
				//echo $urutCetak;
			}
		}*/

		$getUrut = _QMS_Query("SELECT max(no_urut) as no_urut FROM antrian_poliklinik where tgl_transaksi between '" . $tglmasuk . " 00:00:00.000" . "' and '" . $TglAntrian . "' and kd_unit = '" . $KdUnit . "' ");

		if ($getUrut->row()->no_urut > 0) {
			$getUrut = $getUrut->row()->no_urut + 1;
		} else {
			$getUrut = 1;
		}

		_QMS_Query("INSERT INTO antrian_poliklinik (KD_UNIT, TGL_TRANSAKSI, NO_URUT, KD_PASIEN, KD_USER, ID_STATUS_ANTRIAN, STS_DITEMUKAN, NO_ANTRIAN, NO_PRIORITAS) VALUES (
			'" . $KdUnit . "', '" . $TglAntrian . "',  '" . $getUrut . "', '" . $KdPasien . "', '" . $_kduser . "', 0, 0, '', '')");
		$this->db->query("INSERT INTO antrian_poliklinik (KD_UNIT, TGL_TRANSAKSI, NO_URUT, KD_PASIEN, KD_USER, ID_STATUS_ANTRIAN, STS_DITEMUKAN, NO_ANTRIAN, NO_PRIORITAS) VALUES (
			'" . $KdUnit . "', '" . $TglAntrian . "',  '" . $getUrut . "', '" . $KdPasien . "', '" . $_kduser . "', 0, false, 0, 0)");

		_QMS_Query("DELETE FROM antrian_poliklinik WHERE kd_pasien='" . $KdPasien . "' and kd_unit='" . $KdUnitDulu . "'");

		$this->db->query("DELETE FROM antrian_poliklinik WHERE kd_pasien='" . $KdPasien . "' and kd_unit='" . $KdUnitDulu . "'");
		# Update antrian poliklinik set no_urut dan no_prioritas

		/*$q_update_antrian_poli_ss=$db->query("updates antrian_poliklinik set kd_unit='".$KdUnit."', no_urut=".$urutCetak." 
												where kd_unit='".$KdUnit."' and tgl_transaksi='".$tglmasuk."' 
													and no_urut=".$urutCetak." and kd_pasien='".$KdPasien."' and kd_user=".$_kduser.""); */


		/*$q_update_antrian_poli=$this->db->query("updates antrian_poliklinik set kd_unit='".$KdUnit."', no_urut=".$urutCetak." 
												where kd_unit='".$KdUnit."' and tgl_transaksi='".$tglmasuk."' 
													and no_urut=".$urutCetak." and kd_pasien='".$KdPasien."' and kd_user=".$_kduser."");*/

		/* $q_simpan_antrian_poli_ss=$db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$KdUnit."','".$TglAntrian."',".$urutCetak.",'".$KdPasien."',$_kduser,0,0,".$no_antrian.",".$no_prioritas.")"); 
		$q_simpan_antrian_poli=$this->db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$KdUnit."','".$tglmasuk."',".$urutCetak.",'".$KdPasien."',$_kduser,0,FALSE,".$no_antrian.",".$no_prioritas." )"); */
		# POSTGRES

		$sqlCekReg = "select count(kd_pasien) as jum from reg_unit where kd_unit='" . $KdUnit . "' AND kd_pasien='" . $KdPasien . "'";
		$arrCekReg = _QMS_QUERY($sqlCekReg)->row()->jum;
		if ($arrCekReg == 0) {
			$sqlReg = "select max(no_register) as no from reg_unit where kd_unit='" . $KdUnit . "'";
			$resReg = _QMS_QUERY($sqlReg)->row()->no;
			$resReg++;
			$arrReg = array(
				'kd_pasien' => $KdPasien,
				'kd_unit' => $KdUnit,
				'no_register' => $resReg,
				'urut_masuk' => 0
			);
			_QMS_insert('reg_unit', $arrReg);
		}
		/*
		if (isset($_POST['KdKasir'])) {
			if ($_POST['KdKasir'] == '01') {

			}

		}*/
		echo '{success:true, totalrecords:' . count($resultQuery) . ', ListDataObj:' . json_encode($resultQuery) . '}';
	}
	/* 
		PERBARUAN GANTI KELOMPOK PASIEN 
		OLEH 	; HADAD
		TANGGAL : 2017 - 01 - 05
	 */

	public function cekDetailTransaksi()
	{
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$NoTransaksi	= $_POST['NoTransaksi'];
		$KdKasir 		= $_POST['KdKasir'];
		$result 		= $this->db->query("SELECT * FROM detail_transaksi WHERE no_transaksi='" . $NoTransaksi . "' AND kd_kasir='" . $KdKasir . "'");
		echo json_encode($result->num_rows());
	}

	function cek_transfer_tindakan()
	{
		$this->dbSQL->trans_commit();
		$this->db->trans_commit();
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
		);

		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			//'tgl_transaksi1' => $params['tgl_transaksi'],
		);

		$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);
		foreach ($querySQL->result() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
				'urut' 			=> $data->urut,
			);
			//var_dump($querySQL->result());
			$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
			if ($queryPG->num_rows() == 0) {
				$paramsInsert = array(
					'kd_kasir' 		=> $data->KD_KASIR,
					'no_transaksi' 	=> $data->NO_TRANSAKSI,
					'urut' 			=> $data->URUT,
					'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_user' 		=> $data->KD_USER,
					'kd_tarif' 		=> $data->KD_TARIF,
					'kd_produk' 	=> $data->KD_PRODUK,
					'kd_unit' 		=> $data->KD_UNIT,
					'tgl_berlaku' 	=> date_format(date_create($data->TGL_BERLAKU), 'Y-m-d'),
					//'charge' 		=> $data->CHARGE,
					//'adjust' 		=> $data->ADJUST,
					'folio' 		=> $data->FOLIO,
					'qty' 			=> $data->QTY,
					'harga' 		=> $data->HARGA,
					'shift' 		=> $data->SHIFT,
					'kd_dokter' 	=> $data->KD_DOKTER,
					'kd_unit_tr' 	=> $data->KD_UNIT_TR,
					'cito' 			=> $data->CITO,
					'js' 			=> $data->JS,
					'jp' 			=> $data->JP,
					'no_faktur' 	=> $data->NO_FAKTUR,
				);

				if ($data->CHARGE == '0' || $data->CHARGE == 0) {
					$paramsInsert['charge'] = 'false';
				} else {
					$paramsInsert['charge'] = 'true';
				}

				if ($data->ADJUST == '0' || $data->ADJUST == 0) {
					$paramsInsert['adjust'] = 'false';
				} else {
					$paramsInsert['adjust'] = 'true';
				}

				$resultPG = $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
				if ($resultPG !== true || $resultPG == 0) {
					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				} else {
					$resultSQL 	= true;
					//$resultPG 
				}
			} else {
				$resultPG 	= true;
				$resultSQL 	= true;
			}

			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				unset($criteriaParams);
				unset($queryPG);

				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
					'urut' 			=> $data->urut,
				);


				$criteriaTarifComponent = array(
					'kd_tarif' 	=> $data->KD_TARIF,
					'kd_produk' => $data->KD_PRODUK,
					'kd_unit' 	=> $data->KD_UNIT,
				);
				$kd_component 	= $this->Tbl_data_tarif_component->get($criteriaTarifComponent);
				$querySQL 		= $this->Tbl_data_detail_component->getDetailComponent_SQL($criteriaParams);
				$queryPG 		= $this->Tbl_data_detail_component->getDetailComponent($criteriaParams);

				if ($kd_component->num_rows() > 0) {
					unset($paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'tarif' 		=> $data->HARGA,
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $data->urut,
						'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
						'disc' 			=> 0,
						'markup' 		=> 0,
						'kd_component' 	=> $kd_component->row()->kd_component,
					);

					if ($querySQL->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
						$resultSQL = $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
					} else {
						$resultSQL = true;
					}

					if ($queryPG->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
						$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
					} else {
						$resultPG = true;
					}
				}
				unset($criteriaParams);
				unset($queryPG);
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
					'urut' 			=> $data->URUT
				);
				//var_dump($criteriaParams);
				$querySQL 		= $this->Tbl_data_detail_tr_dokter->get_SQL($criteriaParams);
				$queryPG 		= $this->Tbl_data_detail_tr_dokter->get($criteriaParams);
				unset($paramsInsert);
				if ($querySQL->num_rows() > 0) {
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $data->URUT,
						'kd_dokter'		=> $querySQL->row()->KD_DOKTER,
						'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
						'kd_component' 	=> $querySQL->row()->Kd_job,
						'jp' 			=> $querySQL->row()->JP,
						'pajak' 		=> $querySQL->row()->PAJAK,
						'bayar' 		=> $querySQL->row()->BAYAR
					);
					$paramsInsertSQL = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $data->URUT,
						'kd_dokter'		=> $querySQL->row()->KD_DOKTER,
						'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
						'kd_job' 		=> $querySQL->row()->Kd_job,
						'jp' 			=> $querySQL->row()->JP,
						'pajak' 		=> $querySQL->row()->PAJAK,
						'bayar' 		=> $querySQL->row()->BAYAR
					);
					if ($querySQL->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
						$resultSQL = $this->Tbl_data_detail_tr_dokter->insert_SQL($paramsInsertSQL);
					} else {
						$resultSQL = true;
					}

					if ($queryPG->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
						$resultPG = $this->Tbl_data_detail_tr_dokter->insert($paramsInsert);
					} else {
						$resultPG = true;
					}
				}
				//var_dump($querySQL->row());


				if (($resultPG != true || $resultPG == 0) && ($resultSQL != true || $resultSQL == 0)) {

					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				}
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}

		unset($criteriaParams);
		unset($queryPG);
		unset($querySQL);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			//'tgl_transaksi' => $params['tgl_transaksi'],
		);

		$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
		foreach ($queryPG->result() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => $params['tgl_transaksi'],
				'urut' 			=> $data->urut,
			);
			$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);

			if ($querySQL->num_rows() === 0) {
				$resultPG = $this->Tbl_data_detail_transaksi->deleteDetailTransaksi($criteriaParams);

				if ($resultPG === true || $resultPG > 0) {
					$resultSQL = true;
				} else {
					break;
					$resultPG 	= false;
					$resultSQL 	= false;
				}
			}
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->dbSQL->trans_commit();
			$this->db->trans_commit();
			$response['no_transaksi'] 	= $params['no_transaksi'];
			$response['status'] 		= true;
		} else {
			$this->dbSQL->trans_rollback();
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	function cek_transfer_tindakan_dan_pembayaran()
	{
		$this->dbSQL->trans_commit();
		$this->db->trans_commit();
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
		);

		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			//'tgl_transaksi1' => $params['tgl_transaksi'],
		);

		$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);
		foreach ($querySQL->result() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
				'urut' 			=> $data->urut,
			);
			//var_dump($querySQL->result());
			$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
			if ($queryPG->num_rows() == 0) {
				$paramsInsert = array(
					'kd_kasir' 		=> $data->KD_KASIR,
					'no_transaksi' 	=> $data->NO_TRANSAKSI,
					'urut' 			=> $data->URUT,
					'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_user' 		=> $data->KD_USER,
					'kd_tarif' 		=> $data->KD_TARIF,
					'kd_produk' 	=> $data->KD_PRODUK,
					'kd_unit' 		=> $data->KD_UNIT,
					'tgl_berlaku' 	=> date_format(date_create($data->TGL_BERLAKU), 'Y-m-d'),
					//'charge' 		=> $data->CHARGE,
					//'adjust' 		=> $data->ADJUST,
					'folio' 		=> $data->FOLIO,
					'qty' 			=> $data->QTY,
					'harga' 		=> $data->HARGA,
					'shift' 		=> $data->SHIFT,
					'kd_dokter' 	=> $data->KD_DOKTER,
					'kd_unit_tr' 	=> $data->KD_UNIT_TR,
					'cito' 			=> $data->CITO,
					'js' 			=> $data->JS,
					'jp' 			=> $data->JP,
					'no_faktur' 	=> $data->NO_FAKTUR,
				);

				if ($data->CHARGE == '0' || $data->CHARGE == 0) {
					$paramsInsert['charge'] = 'false';
				} else {
					$paramsInsert['charge'] = 'true';
				}

				if ($data->ADJUST == '0' || $data->ADJUST == 0) {
					$paramsInsert['adjust'] = 'false';
				} else {
					$paramsInsert['adjust'] = 'true';
				}
				$response['tahap'] = "Insert Detail transaksi";
				$resultPG = $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
				if ($resultPG !== true || $resultPG == 0) {
					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				} else {
					$resultSQL 	= true;
					//$resultPG 
				}
			} else {
				$resultPG 	= true;
				$resultSQL 	= true;
			}

			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				unset($criteriaParams);
				unset($queryPG);

				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
					'urut' 			=> $data->urut,
				);


				$criteriaTarifComponent = array(
					'kd_tarif' 	=> $data->KD_TARIF,
					'kd_produk' => $data->KD_PRODUK,
					'kd_unit' 	=> $data->KD_UNIT,
				);
				$kd_component 	= $this->Tbl_data_tarif_component->get($criteriaTarifComponent);
				$querySQL 		= $this->Tbl_data_detail_component->getDetailComponent_SQL($criteriaParams);
				$queryPG 		= $this->Tbl_data_detail_component->getDetailComponent($criteriaParams);

				if ($kd_component->num_rows() > 0) {
					unset($paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'tarif' 		=> $data->HARGA,
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $data->urut,
						'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
						'disc' 			=> 0,
						'markup' 		=> 0,
						'kd_component' 	=> $kd_component->row()->kd_component,
					);

					$response['tahap'] = "Insert detail Component";
					if ($querySQL->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
						$resultSQL = $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
					} else {
						$resultSQL = true;
					}

					if ($queryPG->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
						$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
					} else {
						$resultPG = true;
					}
				}/* else{
					$resultPG = $this->db->query("delete from detail_component where no_transaksi='".$params['no_transaksi']."' and kd_kasir='".$params['kd_kasir']."' and tgl_transaksi='".$params['tgl_transaksi']."' and urut=".$data->urut."");
				} */
				//var_dump($querySQL->row());
				unset($criteriaParams);
				unset($querySQL);
				unset($queryPG);
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
					'urut' 			=> $data->URUT
				);
				//var_dump($criteriaParams);
				$querySQL 		= $this->Tbl_data_detail_tr_dokter->get_SQL($criteriaParams);
				$queryPG 		= $this->Tbl_data_detail_tr_dokter->get($criteriaParams);
				unset($paramsInsert);
				if ($querySQL->num_rows() > 0) {
					//var_dump($querySQL->row());
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $data->URUT,
						'kd_dokter'		=> $querySQL->row()->KD_DOKTER,
						'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
						'kd_component' 	=> $querySQL->row()->Kd_job,
						'jp' 			=> $querySQL->row()->JP,
						'pajak' 		=> $querySQL->row()->PAJAK,
						'bayar' 		=> $querySQL->row()->BAYAR
					);
					$response['tahap'] = "Insert Detail TR Dokter";
					if ($querySQL->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
						$resultSQL = $this->Tbl_data_detail_tr_dokter->insert_SQL($paramsInsert);
					} else {
						$resultSQL = true;
					}

					if ($queryPG->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
						$resultPG = $this->Tbl_data_detail_tr_dokter->insert($paramsInsert);
					} else {
						$resultPG = true;
					}
				}




				if (($resultPG != true || $resultPG == 0) && ($resultSQL != true || $resultSQL == 0)) {

					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				}
			} else {
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}
		//var_dump($querySQL->row());
		unset($criteriaParams);
		unset($querySQL);
		unset($queryPG);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'tgl_transaksi' => $params['tgl_transaksi'],
			//'urut' 			=> $data->URUT
		);
		//var_dump($criteriaParams);
		$querySQL 		= $this->Tbl_data_detail_bayar->getDetailBayar_SQL($criteriaParams);
		$queryPG 		= $this->Tbl_data_detail_bayar->getDetailBayar($criteriaParams);
		unset($paramsInsert);
		unset($paramsInsertSQL);
		//var_dump($querySQL->row());
		$response['tahap'] = "Insert Detail Bayar";
		if ($querySQL->num_rows() > 0) {
			//var_dump($querySQL->row());
			//echo count($querySQL->result());
			foreach ($querySQL->result() as $line) {
				$paramsInsertSQL = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $line->URUT,
					'tgl_transaksi' => date_format(date_create($line->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_user'		=> $line->KD_USER,
					'kd_unit' 		=> $line->KD_UNIT,
					'kd_pay' 		=> $line->KD_PAY,
					'jumlah' 		=> $line->JUMLAH,
					'folio' 		=> $line->FOLIO,
					'shift' 		=> $line->SHIFT,
					'status_bayar' 	=> $line->STATUS_BAYAR,
					'deposit' 		=> $line->DEPOSIT,
					'kd_dokter' 	=> $line->KD_DOKTER,
					'tag' 			=> $line->TAG,
					'verified' 		=> $line->VERIFIED,
					'kd_user_ver' 	=> $line->KD_USER_VER,
					'tgl_ver' 		=> $line->TGL_VER,
					'sisa' 			=> $line->SISA,
					'total_dibayar' => $line->TOTAL_DIBAYAR,
					'reff' 			=> $line->REFF,
					'pembayar' 		=> $line->PEMBAYAR,
					'alamat' 		=> $line->ALAMAT,
					'untuk' 		=> $line->UNTUK
				);
				if ($line->STATUS_BAYAR == 0) {
					$status_bayar = 't';
				} else {
					$status_bayar = 'f';
				}

				if ($line->DEPOSIT == 0) {
					$deposit = 't';
				} else {
					$deposit = 'f';
				}

				if ($line->TAG == 0) {
					$tag = 't';
				} else {
					$tag = 'f';
				}
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $line->URUT,
					'tgl_transaksi' => date_format(date_create($line->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_user'		=> $line->KD_USER,
					'kd_unit' 		=> $line->KD_UNIT,
					'kd_pay' 		=> $line->KD_PAY,
					'jumlah' 		=> $line->JUMLAH,
					'folio' 		=> $line->FOLIO,
					'shift' 		=> $line->SHIFT,
					'status_bayar' 	=> $status_bayar,
					'deposit' 		=> $deposit,
					'kd_dokter' 	=> $line->KD_DOKTER,
					'tag' 			=> $tag,
					'verified' 		=> $line->VERIFIED,
					'kd_user_ver' 	=> $line->KD_USER_VER,
					'tgl_ver' 		=> $line->TGL_VER,
					'sisa' 			=> $line->SISA,
					'total_dibayar' => $line->TOTAL_DIBAYAR,
					'reff' 			=> $line->REFF,
					'pembayar' 		=> $line->PEMBAYAR,
					'alamat' 		=> $line->ALAMAT,
					'untuk' 		=> $line->UNTUK
				);
				if ($querySQL->num_rows() === 0) {
					//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
					$resultSQL = $this->Tbl_data_detail_bayar->insertDetailBayar_SQL($paramsInsertSQL);
				} else {
					$resultSQL = true;
				}
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($line->TGL_TRANSAKSI), 'Y-m-d'),
					'urut' 			=> $line->URUT
				);
				$queryPG 		= $this->Tbl_data_detail_bayar->getDetailBayar($criteriaParams);
				if ($queryPG->num_rows() === 0) {
					//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
					$resultPG = $this->Tbl_data_detail_bayar->insertDetailBayar($paramsInsert);
				} else {
					//echo "huhu";
					$resultPG = true;
				}
			}
		} else {
			$resultPG = $this->db->query("delete from detail_bayar where no_transaksi='" . $params['no_transaksi'] . "' and kd_kasir='" . $params['kd_kasir'] . "' and tgl_transaksi='" . $params['tgl_transaksi'] . "'");
		}

		// unset($criteriaParams);
		unset($querySQL);
		unset($queryPG);
		// $criteriaParams = array(
		// 'kd_kasir' 		=> $params['kd_kasir'],	
		// 'no_transaksi' 	=> $params['no_transaksi'],
		// 'tgl_transaksi' => $params['tgl_transaksi'],
		// 'urut' 			=> $data->URUT
		// );
		$criteriaParams['tgl_transaksi'] = $params['tgl_transaksi'];
		//var_dump($criteriaParams);
		$querySQL 		= $this->Tbl_data_detail_tr_bayar->getDetailTRBayar_SQL($criteriaParams);
		$queryPG 		= $this->Tbl_data_detail_tr_bayar->getDetailTRBayar($criteriaParams);
		unset($paramsInsert);
		unset($paramsInsertSQL);
		//var_dump($querySQL->row());
		$response['tahap'] = "Detail TR Bayar";
		if ($querySQL->num_rows() > 0) {
			//var_dump($querySQL->row());
			foreach ($querySQL->result() as $line) {
				$paramsInsertSQL = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $line->URUT,
					'tgl_transaksi' => date_format(date_create($line->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_pay'		=> $line->KD_PAY,
					'urut_bayar' 	=> $line->URUT_BAYAR,
					'tgl_bayar' 	=> date_format(date_create($line->TGL_BAYAR), 'Y-m-d'),
					'jumlah' 		=> $line->JUMLAH,

				);


				$response['tahap'] = "Detail detail tr bayar";
				if ($querySQL->num_rows() === 0) {
					$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
					$resultSQL = $this->Tbl_data_detail_tr_bayar->insertDetailTRBayar_SQL($paramsInsertSQL);
				} else {
					$resultSQL = true;
				}

				if ($queryPG->num_rows() === 0) {
					$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
					$resultPG = $this->Tbl_data_detail_tr_bayar->insertDetailTRBayar($paramsInsertSQL);
				} else {
					$resultPG = true;
				}
			}
		}

		// unset($criteriaParams);
		unset($querySQL);
		unset($queryPG);
		/*$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],	
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => $params['tgl_transaksi'],
				'urut' 			=> $data->URUT
			);*/
		//var_dump($criteriaParams);
		$querySQL 		= $this->Tbl_data_detail_tr_bayar_component->getDetailTRBayarComponent_SQL($criteriaParams);
		$queryPG 		= $this->Tbl_data_detail_tr_bayar_component->getDetailTRBayarComponent($criteriaParams);
		unset($paramsInsert);
		unset($paramsInsertSQL);
		//var_dump($querySQL->row());
		$response['tahap'] = "Detail TR Bayar Component";
		if ($querySQL->num_rows() > 0) {
			//var_dump($querySQL->row());
			foreach ($querySQL->result() as $line) {
				$paramsInsertSQL = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $line->URUT,
					'tgl_transaksi' => date_format(date_create($line->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_pay'		=> $line->KD_PAY,
					'urut_bayar' 	=> $line->URUT_BAYAR,
					'tgl_bayar' 	=> date_format(date_create($line->TGL_BAYAR), 'Y-m-d'),
					'kd_component' 	=> $line->KD_COMPONENT,
					'jumlah' 		=> $line->JUMLAH,

				);


				$response['tahap'] = "Detail detail tr bayar component";
				if ($querySQL->num_rows() === 0) {
					$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
					$resultSQL = $this->Tbl_data_detail_tr_bayar_component->insertDetailTRBayarComponent_SQL($paramsInsertSQL);
				} else {
					$resultSQL = true;
				}

				if ($queryPG->num_rows() === 0) {
					$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
					$resultPG = $this->Tbl_data_detail_tr_bayar_component->insertDetailTRBayarComponent($paramsInsertSQL);
				} else {
					$resultPG = true;
				}
			}
		}
		unset($criteriaParams);
		unset($queryPG);
		unset($querySQL);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			//'tgl_transaksi' => $params['tgl_transaksi'],
		);

		$queryPG 	= $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
		$response['tahap'] = "Delete Detail Transaksi";
		if ($queryPG->num_rows() > 0) {
			foreach ($queryPG->result() as $data) {
				unset($criteriaParams);
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					// 'tgl_transaksi' => $params['tgl_transaksi'],
					'urut' 			=> $data->urut,
				);
				$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);

				if ($querySQL->num_rows() === 0) {
					$resultPG = $this->Tbl_data_detail_transaksi->deleteDetailTransaksi($criteriaParams);

					if ($resultPG === true || $resultPG > 0) {
						$resultSQL = true;
					} else {
						break;
						$resultPG 	= false;
						$resultSQL 	= false;
					}
				}
			}
		} else {
			$resultPG 	= true;
			$resultSQL 	= true;
		}

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->dbSQL->trans_commit();
			$this->db->trans_commit();
			$this->dbSQL->close();
			$this->db->close();
			$response['no_transaksi'] 	= $params['no_transaksi'];
			$response['status'] 		= true;
		} else {
			$this->dbSQL->trans_rollback();
			$this->db->trans_rollback();
			$this->dbSQL->close();
			$this->db->close();
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	public function UpdateGantiRujukan()
	{

		date_default_timezone_set('Asia/Makassar');
		$TglAntrian = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$conv_tglmasuk = date('Y/m/d');
		$tglmasuk = date('Y-m-d');

		$db = $this->load->database('otherdb2', TRUE);
		$_kduser = $this->session->userdata['user_id']['id'];

		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$NoTransaksi	= $_POST['NoTransaksi'];
		$KdUnitDulu		= $_POST['KdUnitDulu'];
		$CaraNerima		= $_POST['CaraNerima'];
		$KdRujukan		= $_POST['KdRujukan'];
		$resultQuery = 0;
		$result 	= $this->db->query("UPDATE kunjungan SET kd_rujukan = " . $KdRujukan . ", cara_penerimaan = " . $CaraNerima . " WHERE kd_pasien='" . $KdPasien . "' AND kd_unit='" . $KdUnitDulu . "' AND tgl_masuk='" . $TglMasuk . "' AND urut_masuk='" . $UrutMasuk . "'");
		if ($result > 0) {
			$resultQuery 	= _QMS_Query("UPDATE kunjungan SET 
							kd_rujukan = " . $KdRujukan . ", cara_penerimaan = " . $CaraNerima . "
							WHERE kd_pasien='" . $KdPasien . "' AND tgl_masuk='" . $TglMasuk . "' AND kd_unit='" . $KdUnitDulu . "' AND urut_masuk='" . $UrutMasuk . "'");
		}

		echo '{success:true, totalrecords:' . count($resultQuery) . ', ListDataObj:' . json_encode($resultQuery) . '}';
	}

	public function buka_transaksi()
	{
		/* 
			Update by	: HADAD AL GOJALI
			Tgl			: 11-07-2017
			Ket			: BElum ada fitur buka transaksi
		*/
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$response 	= array();
		$queryPG 	= false;
		$querySQL 	= false;

		$_kd_user    = $this->session->userdata['user_id']['id'];
		$params = array(
			'kd_pasien'    => $this->input->post('kd_pasien'),
			'no_transaksi' => $this->input->post('no_transaksi'),
			'kd_unit'      => $this->input->post('kd_unit'),
			'kd_kasir'     => $this->input->post('kd_kasir'),
			'keterangan'   => $this->input->post('keterangan'),
		);
		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " transaksi ",
			'criteria' 	=> " kd_pasien = '" . $params['kd_pasien'] . "' and no_transaksi = '" . $params['no_transaksi'] . "' and kd_unit = '" . $params['kd_unit'] . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_transaksi = $this->get_custom($paramsTable);


		$paramsCriteria = array(
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_kasir' 		=> $params['kd_kasir'],
		);


		$paramsUpdate = array(
			'co_status' 	=> 'false',
			'tgl_co' 		=> NULL,
		);

		$queryPG 	= $this->Tbl_data_transaksi->updateTransaksi($paramsCriteria, $paramsUpdate);
		unset($paramsUpdate);
		$paramsUpdate = array(
			'co_status' 	=> 0,
			'tgl_co' 		=> NULL,
		);
		//$querySQL 	= $this->Tbl_data_transaksi->updateTransaksi_SQL($paramsCriteria, $paramsUpdate);

		if (($queryPG > 0 || $queryPG === true)) {
			unset($paramsCriteria);
			unset($paramsUpdate);
			$paramsCriteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_masuk' 	=> $query_transaksi->row()->TGL_TRANSAKSI,
				'kd_unit' 		=> $params['kd_unit'],
				'urut_masuk' 	=> $query_transaksi->row()->URUT_MASUK,
			);

			$paramsUpdate = array(
				'tgl_keluar' 	=> NULL,
				'jam_keluar' 	=> NULL,
			);

			$queryPG 	= $this->Tbl_data_kunjungan->update($paramsCriteria, $paramsUpdate);
			//$querySQL 	= $this->Tbl_data_kunjungan->updateSQL($paramsCriteria, $paramsUpdate);
		} else {
			$queryPG 	= false;
			//$querySQL 	= false;
		}

		if (($queryPG > 0 || $queryPG === true)) {
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_unit' 		=> $params['kd_unit'],
				'tgl_transaksi'	=> $query_transaksi->row()->TGL_TRANSAKSI,
				'kd_pasien'		=> $params['kd_pasien'],
				'kd_user'		=> $_kd_user,
				'shift'			=> $this->GetShiftBagian(),
				'tgl_buka'		=> date_format(date_create($this->date_now), 'Y-m-d') . " 00:00:00",
				'jam_buka'		=> "1900-01-01 " . date_format(date_create($this->date_now), 'H:i:s'),
				'keterangan'	=> $params['keterangan'],
			);

			$queryPG = $this->db->insert("history_buka_transaksi", $paramsInsert);
		} else {
			$queryPG 	= false;
			//$querySQL 	= false;
		}


		if (($queryPG > 0 || $queryPG === true)) {
			$this->db->trans_commit();
			//$this->dbSQL->trans_commit();
			$response['status'] = true;
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$response['status'] = false;
		}
		echo json_encode($response);
	}

	public function batal_transaksi()
	{
		$_kd_user    = $this->session->userdata['user_id']['id'];
		$response 	= array();
		$queryPG 	= false;
		//$querySQL 	= false;

		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();

		$params = array(
			'kd_pasien' 	=> $this->input->post('kdPasien'),
			'no_transaksi' 	=> $this->input->post('noTrans'),
			'kd_unit'	 	=> $this->input->post('kdUnit'),
			'jumlah'	 	=> $this->input->post('Jumlah'),
			'kd_dokter' 	=> $this->input->post('kdDokter'),
			'tgl_transaksi'	=> $this->input->post('tglTrans'),
			'kd_customer' 	=> $this->input->post('kdCustomer'),
			'keterangan' 	=> $this->input->post('Keterangan'),
		);

		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " pasien ",
			'criteria' 	=> " kd_pasien = '" . $params['kd_pasien'] . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_pasien 	= $this->get_custom($paramsTable);

		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " unit ",
			'criteria' 	=> " kd_unit = '" . $params['kd_unit'] . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_unit 	= $this->get_custom($paramsTable);

		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " zusers ",
			'criteria' 	=> " kd_user = '" . $_kd_user . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_user 	= $this->get_custom($paramsTable);

		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " kunjungan ",
			'criteria' 	=> " kd_pasien = '" . $params['kd_pasien'] . "'and tgl_masuk = '" . $params['tgl_transaksi'] . "' and kd_unit = '" . $params['kd_unit'] . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_kunjungan 	= $this->get_custom($paramsTable);

		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " mr_penyakit ",
			'criteria' 	=> " kd_pasien = '" . $params['kd_pasien'] . "'and tgl_masuk = '" . $params['tgl_transaksi'] . "' and kd_unit = '" . $params['kd_unit'] . "' AND urut_masuk = '" . $query_kunjungan->row()->urut_masuk . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_mr_pennyakit 	= $this->get_custom($paramsTable);

		$paramsTable 	= array(
			'select' 	=> "*",
			'table' 	=> " transaksi ",
			'criteria' 	=> " kd_pasien = '" . $params['kd_pasien'] . "'and tgl_transaksi = '" . $params['tgl_transaksi'] . "' and kd_unit = '" . $params['kd_unit'] . "' AND urut_masuk = '" . $query_kunjungan->row()->urut_masuk . "' ",
			'order' 	=> " ",
			'limit' 	=> " ",
		);
		$query_transaksi 	= $this->get_custom($paramsTable);

		$paramsCriteria = array(
			'kd_pasien' 	=> $params['kd_pasien'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_unit' 		=> $params['kd_unit'],
			'tgl_transaksi'	=> $params['tgl_transaksi'],
		);

		$queryPG 	= $this->Tbl_data_transaksi->deleteTransaksi($paramsCriteria);
		//$querySQL 	= $this->Tbl_data_transaksi->deleteTransaksi_SQL($paramsCriteria);
		$response['tahap'] 	= "PROSES DELETE TRANSAKSI";

		if (($queryPG > 0 || $queryPG === true) && $query_mr_pennyakit->num_rows() > 0) {
			unset($paramsCriteria);
			$paramsCriteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'kd_unit' 		=> $params['kd_unit'],
				'tgl_masuk'		=> $params['tgl_transaksi'],
			);
			$queryPG 	= $this->db->delete("mr_penyakit", $paramsCriteria);
			//$querySQL 	= $this->dbSQL->delete("mr_penyakit", $paramsCriteria);
			$response['tahap'] 	= "PROSES DELETE MR PENYAKIT";
		} else {
			$queryPG 	= true;
			//$querySQL 	= true;
		}


		if (($queryPG > 0 || $queryPG === true) && $query_mr_pennyakit->num_rows() > 0) {
			unset($paramsCriteria);
			$paramsCriteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'kd_unit' 		=> $params['kd_unit'],
				'tgl_masuk'		=> $params['tgl_transaksi'],
			);
			$this->db->delete("mr_diagnosa", $paramsCriteria);
			//$this->dbSQL->delete("mr_diagnosa", $paramsCriteria);
			$response['tahap'] 	= "PROSES DELETE MR DIAGNOSA";
		} else {
			$queryPG 	= true;
			$querySQL 	= true;
		}

		if (($queryPG > 0 || $queryPG === true)) {
			unset($paramsCriteria);
			$paramsCriteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				// 'no_transaksi' 	=> $params['no_transaksi'],
				'kd_unit' 		=> $params['kd_unit'],
				'tgl_masuk'		=> $params['tgl_transaksi'],
			);
			$queryPG 	= $this->db->delete("kunjungan", $paramsCriteria);
			//$querySQL 	= $this->dbSQL->delete("kunjungan", $paramsCriteria);
			$response['tahap'] 	= "PROSES DELETE KUNJUNGAN";
		} else {
			$queryPG 	= false;
			$querySQL 	= false;
		}

		if (($queryPG > 0 || $queryPG === true)) {
			$paramsInsert = array(
				'kd_pasien'     => $params['kd_pasien'],
				'tgl_kunjungan' => $params['tgl_transaksi'],
				'nama'          => $query_pasien->row()->nama,
				'kd_unit' 		=> $params['kd_unit'],
				'nama_unit'		=> $query_unit->row()->nama_unit,
				'kd_user_del'	=> $_kd_user,
				'shift'			=> $query_kunjungan->row()->shift,
				'shiftdel'		=> $this->GetShiftBagian(),
				'username'		=> $query_user->row()->user_names,
				'tgl_batal'		=> date_format(date_create($this->date_now), 'Y-m-d') . " 00:00:00",
				'jam_batal'		=> "1900-01-01 " . date_format(date_create($this->date_now), 'H:i:s'),
			);
			$queryPG = $this->db->insert("history_batal_kunjungan", $paramsInsert);
			$response['tahap'] 	= "PROSES INSERT HISTORY BATAL KUNJUNGAN";
		} else {
			$queryPG 	= false;
			//$querySQL 	= false;
		}

		if (($queryPG > 0 || $queryPG === true)) {
			unset($paramsInsert);
			$paramsInsert = array(
				'kd_kasir'     	=> $query_transaksi->row()->kd_kasir,
				'no_transaksi' 	=> $query_transaksi->row()->no_transaksi,
				'tgl_transaksi' => $params['tgl_transaksi'],
				'kd_pasien'     => $params['kd_pasien'],
				'nama'          => $query_pasien->row()->nama,
				'kd_unit' 		=> $params['kd_unit'],
				'nama_unit'		=> $query_unit->row()->nama_unit,
				'kd_user'		=> $query_transaksi->row()->kd_user,
				'kd_user_del'	=> $_kd_user,
				'shift'			=> $query_kunjungan->row()->shift,
				'shiftdel'		=> $this->GetShiftBagian(),
				'user_name'		=> $query_user->row()->user_names,
				'tgl_batal'		=> date_format(date_create($this->date_now), 'Y-m-d') . " 00:00:00",
				'jam_batal'		=> "1900-01-01 " . date_format(date_create($this->date_now), 'H:i:s'),
				'ket'			=> $params['keterangan'],
				'jumlah'		=> $params['jumlah'],
			);
			$queryPG = $this->db->insert("history_batal_trans", $paramsInsert);
			$response['tahap'] 	= "PROSES INSERT HISTORY BATAL TRANSAKSI";
		} else {
			$queryPG 	= false;
			//$querySQL 	= false;
		}

		if (($queryPG > 0 || $queryPG === true)) {
			$this->db->trans_commit();
			//$this->dbSQL->trans_commit();
			$response['status'] = true;
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$response['status'] = false;
		}

		$this->db->close();
		//$this->dbSQL->close();
		echo json_encode($response);
	}

	private function get_custom($criteria)
	{
		if (isset($criteria['criteria'])) {
			$where = "WHERE";
		} else {
			$where = "";
		}
		$query = $this->db->query("SELECT " . $criteria['select'] . " FROM " . $criteria['table'] . " " . $where . " " . $criteria['criteria'] . " " . $criteria['order'] . " " . $criteria['limit'] . "");
		return $query;
	}

	public function getProdukList()
	{
		$kd_kelas = '';
		if (!isset($_POST['mod_id'])) {
			if ($_POST['modul'] == 'LAB') {
				$kd_kelas  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_igd'")->row()->setting;
			} else if ($_POST['modul'] == 'RAD') {
				$kd_kelas  = $this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_klas_produk_ird'")->row()->setting;
			} else if ($_POST['modul'] == 'PENJAS_IGD') {
				$kd_kelas  = $this->db->query("select setting from sys_setting where key_data = 'igd_default_klas_produk'")->row()->setting;
			} else {
				$kd_kelas  = $this->db->query("select setting from sys_setting where key_data = 'lab_kd_produk_lab_igd'")->row()->setting;
			}
		} else {
			$setting = $this->setSetting($_POST['mod_id'], array('KD_KELAS_TINDAKAN', 'KD_KELAS_LAB', 'KD_KELAS_RAD'));
			if ($_POST['modul'] == 'LAB') {
				$kd_kelas  = $this->getSetting('KD_KELAS_LAB');
			} else if ($_POST['modul'] == 'RAD') {
				$kd_kelas  = $this->getSetting('KD_KELAS_RAD');
			} else {
				$kd_kelas  = $this->getSetting('KD_KELAS_TINDAKAN');
			}
		}
		// echo "<pre>".var_export($setting, true)."</pre>"; die;
		// $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		// $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$kd_unit 	= $_POST['kd_unit'];
		$kp_produk 	= $this->input->post('kp_produk');
		$deskripsi 	= $this->input->post('deskripsi');
		$kd_customer = $this->db->query("SELECT kd_customer FROM customer WHERE customer = '" . $_POST['customer'] . "'")->row()->kd_customer;
		$row 		= $this->db->query("SELECT kd_tarif from tarif_cust WHERE kd_customer='" . $kd_customer . "'")->row();
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='" . $kd_customer . "'")->row()->kd_tarif;
		$gettarifmir = $this->db->query("gettarifmir @kd_unit='" . substr($_POST['kd_unit'], 0, 1) . "' , @tag='" . $kd_customer . "', @kd_kelas='" . $kd_kelas . "' ")->row()->gettarifmir;
		$sql = "SELECT 
			row_number() OVER (ORDER BY rn.deskripsi ASC) as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				CASE WHEN LEFT (p.kd_klas, 1) = '6' THEN '1' ELSE '0' END AS grup,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					-- u.kd_unit = '" . $gettarifmir . "'
					u.kd_unit in ('41','5')
						AND t.kd_tarif='" . $kd_tarif . "'
						and p.kd_klas like '" . $kd_kelas . "%' 
					AND t.tgl_berlaku <= '" . date('Y-m-d') . "' 
					and t.tgl_berakhir is null or t.tgl_berakhir >= '" . date('Y-m-d') . "'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.deskripsi ASC";
		$result = $this->db->query($sql);
		$data       = array();
		$jsonResult = array();
		$i          = 0;
		foreach ($result->result_array() as $row) {
			if (stripos(strtolower($row['deskripsi']), strtolower($deskripsi)) !== false) {
				$tmp_deskripsi 					= $row['deskripsi'];
				$text 							= str_replace(strtolower($deskripsi), "<b>" . strtolower($deskripsi) . "</b>", strtolower($tmp_deskripsi));
				$data[$i]['kd_produk'] 			= $row['kd_produk'];
				$data[$i]['deskripsi']  		= strtoupper($text);
				// $data[$i]['deskripsi'] 		= $row['deskripsi'];
				// $data[$i]['kd_produk'] 	 	= $row['kd_produk'];
				$data[$i]['kp_produk'] 	 		= $row['kp_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['group'] 	 			= $row['grup'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = strtolower($data[$i]['deskripsi']);
				if (stripos($text, "konsul") !== false) {
					$data[$i]['status_konsultasi'] 	= true;
				} else {
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			} else if (stripos(strtolower($row['kp_produk']), strtolower($kp_produk)) !== false) {
				$tmp_kd_produk 					= $row['kp_produk'];
				$text 							= str_replace(strtolower($kp_produk), "<b>" . strtolower($kp_produk) . "</b>", strtolower($tmp_kd_produk));
				$data[$i]['kp_produk'] 			= strtoupper($text);
				$data[$i]['deskripsi']  		= $row['deskripsi'];
				// $data[$i]['deskripsi'] 		= $row['deskripsi'];
				// $data[$i]['kd_produk'] 	 	= $row['kd_produk'];
				$data[$i]['kd_produk'] 	 		= $row['kd_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['group'] 	 			= $row['grup'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = strtolower($data[$i]['deskripsi']);
				if (stripos($text, "konsul") !== false) {
					$data[$i]['status_konsultasi'] 	= true;
				} else {
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			} else if ($kp_produk == "" && $deskripsi == "") {
				$data[$i]['deskripsi'] 			= $row['deskripsi'];
				$data[$i]['kd_produk'] 	 		= $row['kd_produk'];
				$data[$i]['kp_produk'] 	 		= $row['kp_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['group'] 	 			= $row['grup'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = strtolower($data[$i]['deskripsi']);
				if (stripos($text, "konsul") !== false) {
					$data[$i]['status_konsultasi'] 	= true;
				} else {
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			}
		}
		$jsonResult['processResult'] 	= 'SUCCESS';
		$jsonResult['data'] 			= $data;
		echo json_encode($jsonResult);
	}

	public function getLookUpProdukList()
	{
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='" . $_POST['kd_customer'] . "'")->row()->kd_tarif;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		//$kd_unit 	= $_POST['kd_unit'];
		$kp_produk 	= '';
		$deskripsi 	= '';
		$kd_klas 	= $this->input->post('kd_klas');
		$tgl_now 	= date("Y-m-d");
		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " left(tarif.kd_unit,1) ='" . substr($_POST['kd_unit'], 0, 1) . "' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='" . $_POST['kd_customer'] . "' ";
		}
		/* $row 		= $this->db->query("SELECT kd_tarif from tarif_cust WHERE kd_customer='".$kd_customer."'")->row();
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$kd_customer."'")->row()->kd_tarif; */
		$sql = "SELECT 
			row_number() OVER (ORDER BY rn.tgl_berlaku DESC) as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				p.kd_klas,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_bagian = '" . substr($_POST['kd_unit'], 0, 1) . "'
					and u.kd_unit='" . $_POST['kd_unit'] . "'
					AND (c.kd_customer = '" . $_POST['kd_customer'] . "' OR t.kd_tarif='" . $kd_tarif . "') 
					--AND	p.kd_klas='" . $kd_klas . "'
					AND t.tgl_berlaku <= '" . $tgl_now . "'
					and t.tgl_berakhir is null or t.tgl_berakhir >= '" . $tgl_now . "'
			) as rn 
		WHERE rn = 1
		 ";

		$result = $this->db->query($sql);
		// echo "<pre>".var_export($result, true)."</pre>"; die;
		$data       = array();
		$jsonResult = array();
		$i          = 0;
		foreach ($result->result_array() as $row) {


			if (stripos(strtolower($row['deskripsi']), strtolower($deskripsi)) !== false) {
				$tmp_deskripsi 					= $row['deskripsi'];
				$text 							= str_replace(strtolower($deskripsi), "<b>" . strtolower($deskripsi) . "</b>", strtolower($tmp_deskripsi));
				$data[$i]['kd_klas'] 			= $row['kd_klas'];
				$data[$i]['kd_produk'] 			= $row['kd_produk'];
				$data[$i]['deskripsi']  		= strtoupper($text);
				// $data[$i]['deskripsi'] 		= $row['deskripsi'];
				// $data[$i]['kd_produk'] 	 	= $row['kd_produk'];
				$data[$i]['kp_produk'] 	 		= $row['kp_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = strtolower($data[$i]['deskripsi']);
				/* if (stripos($text, "konsul") !== false) {
					$data[$i]['status_konsultasi'] 	= true;
				}else{
					$data[$i]['status_konsultasi'] 	= false;
				} */
				$text = substr($data[$i]['kd_klas'], 0, 1);
				if ($text == '3' || $text == '6') {
					$data[$i]['status_konsultasi'] 	= true;
				} else {
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			} else if (stripos(strtolower($row['kp_produk']), strtolower($kp_produk)) !== false) {
				$tmp_kd_produk 					= $row['kp_produk'];
				$text 							= str_replace(strtolower($kp_produk), "<b>" . strtolower($kp_produk) . "</b>", strtolower($tmp_kd_produk));
				$data[$i]['kp_produk'] 			= strtoupper($text);
				$data[$i]['kd_klas'] 			= $row['kd_klas'];
				$data[$i]['deskripsi']  		= $row['deskripsi'];
				// $data[$i]['deskripsi'] 		= $row['deskripsi'];
				// $data[$i]['kd_produk'] 	 	= $row['kd_produk'];
				$data[$i]['kd_produk'] 	 		= $row['kd_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = strtolower($data[$i]['deskripsi']);
				/* if (stripos($text, "konsul") !== false) {
					$data[$i]['status_konsultasi'] 	= true;
				}else{
					$data[$i]['status_konsultasi'] 	= false;
				} */
				$text = substr($data[$i]['kd_klas'], 0, 1);
				if ($text == '3' || $text == '6') {
					$data[$i]['status_konsultasi'] 	= true;
				} else {
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			} else if ($kp_produk == "" && $deskripsi == "") {
				$data[$i]['deskripsi'] 			= $row['deskripsi'];
				$data[$i]['kd_klas'] 			= $row['kd_klas'];
				$data[$i]['kd_produk'] 	 		= $row['kd_produk'];
				$data[$i]['kp_produk'] 	 		= $row['kp_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				/* $text = strtolower($data[$i]['deskripsi']);
				if (stripos($text, "konsul") !== false) {
					$data[$i]['status_konsultasi'] 	= true;
				}else{
					$data[$i]['status_konsultasi'] 	= false;
				} */
				$text = substr($data[$i]['kd_klas'], 0, 1);
				if ($text == '3' || $text == '6') {
					$data[$i]['status_konsultasi'] 	= true;
				} else {
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			}
		}
		$jsonResult['processResult'] 	= 'SUCCESS';
		$jsonResult['data'] 			= $data;
		echo json_encode($jsonResult);
	}
	public function hapusDataProduk()
	{
		/*
			FUNGSI Hapus data produk ini digunakan di RWJ sama IGD 
			jadir hati-hati dalam merubah fungsi ini
		 */
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$result 	= array();

		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);

		$paramsTransaksi = array(
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
		);

		$this->db->select("*");
		$this->db->from("transaksi");
		$this->db->where($paramsTransaksi);
		$query_transaksi = $this->db->get();

		$paramsUnit = array(
			'kd_unit' 	=> $query_transaksi->row()->KD_UNIT,
		);

		$this->db->select("*");
		$this->db->from("unit");
		$this->db->where($paramsUnit);
		$query_unit = $this->db->get();

		$paramsPasien = array(
			'kd_pasien' 	=> $query_transaksi->row()->KD_PASIEN,
		);

		$this->db->select("*");
		$this->db->from("pasien");
		$this->db->where($paramsPasien);
		$query_pasien = $this->db->get();

		$paramsProduk = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
		);

		$this->db->select("*");
		$this->db->from("produk");
		$this->db->where($paramsProduk);
		$query_produk = $this->db->get();

		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		if ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->URUT;
		}

		$res = $this->db->query("SELECT k.shift,t.kd_user,zu.user_names from kunjungan k
									inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
									inner join zusers zu on zu.kd_user=t.kd_user
								where kd_kasir='" . $this->input->post('kd_kasir') . "' 
									and no_transaksi='" . $this->input->post('no_transaksi') . "' 
									--and tgl_transaksi='" . $this->input->post('tgl_transaksi') . "'
								")->row();
		//echo
		if ($res) {
			$paramsHistoryDetailTrans = array(
				'kd_kasir' 		=> $data['kd_kasir'],
				'no_transaksi' 	=> $data['no_transaksi'],
				'tgl_transaksi'	=> $data['tgl_transaksi'],
				'kd_pasien' 	=> $query_transaksi->row()->KD_PASIEN,
				'nama' 			=> $query_pasien->row()->NAMA,
				'kd_unit' 		=> $query_unit->row()->KD_UNIT,
				'nama_unit' 	=> $query_unit->row()->NAMA_UNIT,
				'kd_produk' 	=> $this->input->post('kd_produk'),
				'uraian' 		=> $query_produk->row()->DESKRIPSI,
				'kd_user' 		=> $res->kd_user,
				'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
				'user_name' 	=> $res->user_names,
				'shift' 		=> $res->shift,
				'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
				'jumlah' 		=> (int)$urut->row()->HARGA * $urut->row()->QTY,
				'tgl_batal' 	=> date('Y-m-d'),
				'ket' 			=> $this->input->post('alasan'),
			);

			$insert		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
			//$insertSQL	= $this->M_produk->insertHistoryDetailTransSQL($paramsHistoryDetailTrans);
			if ($insert > 0) {
				$delete 	= $this->M_produk->deleteDetailComponent($data);
				//$deleteSQL 	= $this->M_produk->deleteDetailComponentSQL($data);

				if ($delete > 0) {
					//$delete 	= $this->M_produk->deleteTrBayarCompSQL();
					$delete 	= $this->M_produk->deleteDetailTransaksi($data);
					//$deleteSQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
					if ($delete > 0) {
						$this->db->trans_commit();
						//$this->dbSQL->trans_commit();
						$result['tahap'] 	= "detail transaksi";
						$result['status'] 	= true;
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						$result['tahap'] 	= "detail transaksi";
						$result['status'] 	= false;
					}
				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					$result['tahap'] 	= "detail komponen";
					$result['status'] 	= false;
				}
			} else {

				$result['tahap'] 	= "insert history";
				$result['status'] 	= false;
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$result['tahap'] 	= "kode user";
			$result['status'] 	= false;
		}



		echo json_encode($result);
	}
	public function getDataUser()
	{
		$query = $this->db->query("SELECT * from zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		$response = array(
			'count_data'  => $query->num_rows(),
			'result_data' => $query->result(),
		);
		echo json_encode($response);
	}

	public function getMasterObat()
	{

		$kdMilik		= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		= $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			= $this->session->userdata['user_id']['id'];
		$milik 			= "";
		$kdcustomer		= $_POST['kd_customer'];
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='" . $kdUser . "'")->row()->kd_milik_lookup;

		$result = $this->db->query("SELECT TOP 10 A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik, 
										C.jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis,
									(SELECT Jumlah as markup
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $kdcustomer . "')
											and kd_Jenis = 1
											and kd_unit_tarif= '0'
											and kd_unit_far='" . $kdUnitFar . "'),
									(SELECT Jumlah as tuslah
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $kdcustomer . "')
											and kd_Jenis = 2
											and kd_unit_tarif= '0'
											and kd_unit_far='" . $kdUnitFar . "'),
									(SELECT Jumlah as adm_racik
											FROM apt_tarif_cust 
											WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $kdcustomer . "')
											and kd_Jenis = 3
											and kd_unit_tarif= '0'
											and kd_unit_far='" . $kdUnitFar . "'),
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $kdcustomer . "')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='" . $kdUnitFar . "'
									) * b.harga_beli as harga_jual,
									(
										SELECT Jumlah 
										FROM apt_tarif_cust 
										WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='" . $kdcustomer . "')
										 and kd_Jenis = 1
										 and kd_unit_tarif= '0'
										 and kd_unit_far='" . $kdUnitFar . "'
									) * b.harga_beli as hargaaslicito,
									C.kd_unit_far,
									C.jml_stok_apt as stok_current,
									(select sum(jumlah) as jml_order from mr_resepdtl mrd 
										inner join mr_resep mr on mrd.id_mrresep = mr.id_mrresep
										where kd_unit_far=C.kd_unit_far and kd_milik=B.kd_milik and kd_prd=A.kd_prd and tgl_masuk = now()::date and mr.dilayani=0
									) as jml_order
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('" . $_POST['text'] . "%') 
									and b.kd_milik in (" . $kd_milik_lookup . ")
									and c.kd_unit_far='" . $kdUnitFar . "'
									" . $milik . "
									and A.aktif='t'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
									A.kd_pabrik,markup,tuslah,adm_racik,harga_jual, c.jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis,C.kd_unit_far
								ORDER BY A.nama_obat
									
								
								")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function getMasterObatAll()
	{

		$kdMilik		= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		= $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			= $this->session->userdata['user_id']['id'];
		$milik 			= "";
		$kdcustomer		= $_POST['kd_customer'];
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='" . $kdUser . "'")->row()->kd_milik_lookup;

		$result = $this->db->query("SELECT TOP 10 '" . $kdMilik . "' AS kd_milik,'" . $kdUnitFar . "' AS kd_unit_far,A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,A.kd_pabrik, 
										SUM(C.jml_stok_apt) AS jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis,
										CASE WHEN EXISTS (SELECT Jumlah AS tuslah FROM apt_tarif_cust_obat 	WHERE
												kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "' ) 
												AND kd_Jenis = 2 
												AND kd_unit_tarif = '0' 
												AND kd_unit_far = '" . $kdUnitFar . "'
												AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd  )
												)
										THEN  (SELECT Jumlah AS tuslah FROM apt_tarif_cust_obat 	WHERE
												kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer ='" . $kdcustomer . "' ) 
												AND kd_Jenis = 2 
												AND kd_unit_tarif = '0' 
												AND kd_unit_far = '" . $kdUnitFar . "'
												AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd  )
												)
										ELSE ( SELECT Jumlah AS tuslah FROM apt_tarif_cust WHERE
													kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "' ) 
													AND kd_Jenis = 2 
													AND kd_unit_tarif = '0' 
													AND kd_unit_far = '" . $kdUnitFar . "')
												
										END AS tuslah ,
											
										CASE WHEN EXISTS (SELECT Jumlah AS adm_racik FROM apt_tarif_cust_obat 	WHERE
												kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "') 
												AND kd_Jenis = 2 
												AND kd_unit_tarif = '0' 
												AND kd_unit_far = '" . $kdUnitFar . "'
												AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd  )
												)
										THEN  (SELECT Jumlah AS adm_racik FROM apt_tarif_cust_obat 	WHERE
												kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer ='" . $kdcustomer . "' ) 
												AND kd_Jenis = 3
												AND kd_unit_tarif = '0' 
												AND kd_unit_far = '" . $kdUnitFar . "' 
												AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd )
												)
										ELSE (SELECT Jumlah AS adm_racik FROM apt_tarif_cust WHERE
														kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "' ) 
														AND kd_Jenis = 3 
														AND kd_unit_tarif = '0' 
														AND kd_unit_far = '" . $kdUnitFar . "') 													
										END AS adm_racik
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('%" . $_POST['text'] . "%') 
									and b.kd_milik in (" . $kd_milik_lookup . ")
									
									" . $milik . "
									--and A.aktif='1'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,A.kd_pabrik,B.kd_milik,m.milik,D.satuan,E.sub_jenis
								ORDER BY A.nama_obat
								")->result_array();

		// echo "<pre>".var_export($result->result_array(),true)."</pre>"; die;

		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getMasterObatAll_RWI()
	{

		$kdMilik		= $this->session->userdata['user_id']['aptkdmilik'];
		$kdUnitFar		= $this->session->userdata['user_id']['aptkdunitfar'];
		$kdUser			= $this->session->userdata['user_id']['id'];
		$milik 			= "";
		$kdcustomer		= $_POST['kd_customer'];
		$kd_milik_lookup = $this->db->query("select kd_milik_lookup from zusers where kd_user='" . $kdUser . "'")->row()->kd_milik_lookup;

		$result = $this->db->query("SELECT TOP 10 '" . $kdMilik . "' AS kd_milik,'" . $kdUnitFar . "' AS kd_unit_far,A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,A.kd_pabrik, 
										SUM(C.jml_stok_apt) AS jml_stok_apt,B.kd_milik,m.milik,D.satuan,E.sub_jenis,
										CASE WHEN EXISTS (SELECT Jumlah AS tuslah FROM apt_tarif_cust_obat 	WHERE
										kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "' ) 
										AND kd_Jenis = 2 
										AND kd_unit_tarif = '0' 
										AND kd_unit_far = '" . $kdUnitFar . "'
										AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd  )
										)
								THEN  (SELECT Jumlah AS tuslah FROM apt_tarif_cust_obat 	WHERE
										kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer ='" . $kdcustomer . "' ) 
										AND kd_Jenis = 2 
										AND kd_unit_tarif = '0' 
										AND kd_unit_far = '" . $kdUnitFar . "'
										AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd  )
										)
								ELSE ( SELECT Jumlah AS tuslah FROM apt_tarif_cust WHERE
											kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "' ) 
											AND kd_Jenis = 2 
											AND kd_unit_tarif = '0' 
											AND kd_unit_far = '" . $kdUnitFar . "')
										
								END AS tuslah ,
									
								CASE WHEN EXISTS (SELECT Jumlah AS adm_racik FROM apt_tarif_cust_obat 	WHERE
										kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "') 
										AND kd_Jenis = 2 
										AND kd_unit_tarif = '0' 
										AND kd_unit_far = '" . $kdUnitFar . "'
										AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd  )
										)
								THEN  (SELECT Jumlah AS adm_racik FROM apt_tarif_cust_obat 	WHERE
										kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer ='" . $kdcustomer . "' ) 
										AND kd_Jenis = 3
										AND kd_unit_tarif = '0' 
										AND kd_unit_far = '" . $kdUnitFar . "' 
										AND kd_sub_jns = (SELECT KD_SUB_JNS FROM APT_OBAT WHERE KD_PRD= A.kd_prd )
										)
								ELSE (SELECT Jumlah AS adm_racik FROM apt_tarif_cust WHERE
												kd_gol = ( SELECT kd_gol FROM apt_gol_cust WHERE kd_customer = '" . $kdcustomer . "' ) 
												AND kd_Jenis = 3 
												AND kd_unit_tarif = '0' 
												AND kd_unit_far = '" . $kdUnitFar . "') 													
								END AS adm_racik
								FROM apt_obat A 
									INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit c ON c.kd_prd=B.kd_prd and c.kd_milik=B.kd_milik  
									INNER JOIN apt_milik m ON m.kd_milik=B.kd_milik
									INNER JOIN apt_satuan D ON D.kd_satuan=A.kd_satuan
									INNER JOIN apt_sub_jenis E ON E.kd_sub_jns=A.kd_sub_jns
								WHERE upper(A.nama_obat) like upper('" . $_POST['text'] . "%') 
									and b.kd_milik in (" . $kd_milik_lookup . ")
									
									" . $milik . "
									and A.aktif='1'  
									and c.jml_stok_apt >0 
								GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,A.kd_pabrik,B.kd_milik,m.milik,D.satuan,E.sub_jenis
								ORDER BY A.nama_obat
								")->result_array(); //tuslah,adm_racik,
		// echo "<pre>".var_export($result, true)."</pre>"; die;
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function get_stok_obat()
	{
		$kd_prd 		= $_POST['kd_prd'];
		$kd_milik 		= $_POST['kd_milik'];
		$kdUnitFar		= $_POST['kd_unit_far'];
		$jml_order		= $this->db->query("select get_jml_order_obat('" . $kdUnitFar . "'," . $kd_milik . ",'" . $kd_prd . "') as jml_order")->row()->jml_order;
		$nilai_stok 	= $this->db->query("select jml_stok_apt from apt_stok_unit where kd_unit_far='" . $kdUnitFar . "' and kd_milik='" . $kd_milik . "' and kd_prd='" . $kd_prd . "'")->row()->jml_stok_apt;
		$stok			=  $nilai_stok - $jml_order;
		echo '{success:true, stok:' . $stok . '}';
	}


	public function getAturanRacik()
	{
		$result = $this->db->query("select * from apt_racik_aturan_buat order by kd_racik_atr")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function getAturanPakai()
	{
		$result = $this->db->query("select * from apt_racik_aturan_pakai order by kd_racik_atr_pk")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function getSatuanRacik()
	{
		$result = $this->db->query("select * from apt_racik_satuan order by satuan")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function getTakaran()
	{
		$result = $this->db->query("select * from apt_takaran WHERE aktif='t' order by line")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}
	public function getCaraPakai()
	{
		$result = $this->db->query("select * from apt_aturan_pakai WHERE aktif='t' order by line")->result();
		$jsonResult = array();
		$jsonResult['processResult'] = 'SUCCESS';
		$jsonResult['listData'] = $result;
		echo json_encode($jsonResult);
	}

	public function hapusDataProduk_()
	{
		/*
			FUNGSI Hapus data produk ini digunakan di RWJ sama IGD 
			jadir hati-hati dalam merubah fungsi ini
		 */
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$result 	= array();

		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		if ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->URUT;
		}

		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
									inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
									inner join zusers zu on zu.kd_user=t.kd_user
								where kd_kasir='" . $this->input->post('kd_kasir') . "' 
									and no_transaksi='" . $this->input->post('no_transaksi') . "' 
									--and tgl_transaksi='" . $this->input->post('tgl_transaksi') . "'
								")->row();
		//echo

		if ($res) {
			$paramsHistoryDetailTrans = array(
				'kd_kasir' 		=> $data['kd_kasir'],
				'no_transaksi' 	=> $data['no_transaksi'],
				'tgl_transaksi'	=> $data['tgl_transaksi'],
				'kd_pasien' 	=> $this->input->post('kd_pasien'),
				'nama' 			=> $this->input->post('nama'),
				'kd_unit' 		=> $this->input->post('kd_unit'),
				'nama_unit' 	=> $this->input->post('nama_unit'),
				'kd_produk' 	=> $this->input->post('kd_produk'),
				'uraian' 		=> $this->input->post('deskripsi'),
				'kd_user' 		=> $res->kd_user,
				'kd_user_del' 	=> $this->session->userdata['user_id']['id'],
				'user_name' 	=> $res->user_names,
				'shift' 		=> $res->shift,
				'shiftdel' 		=> $this->db->query("select shift from bagian_shift where kd_bagian='3'")->row()->shift,
				'jumlah' 		=> $this->input->post('total'),
				'tgl_batal' 	=> date('Y-m-d'),
				'ket' 			=> $this->input->post('alasan'),

			);
			// $insert		= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);
			$insertSQL	= $this->M_produk->insertHistoryDetailTrans($paramsHistoryDetailTrans);

			// echo "<pre>".var_export($insertSQL, true)."</pre>"; die;

			if ($insertSQL > 0) {
				$deleteSQL 	= $this->M_produk->deleteDetailComponent($data);
				// $deleteSQL 	= $this->M_produk->deleteDetailComponentSQL($data);

				// if ($delete>0) {
				//$delete 	= $this->M_produk->deleteTrBayarCompSQL();
				$deleteSQL 	= $this->M_produk->deleteDetailTransaksi($data);
				// $deleteSQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
				if ($deleteSQL > 0) {
					$this->db->trans_commit();
					// $this->dbSQL->trans_commit();
					$result['tahap'] 	= "detail transaksi";
					$result['status'] 	= true;
				} else {
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					$result['tahap'] 	= "detail transaksi";
					$result['status'] 	= false;
				}

				// }else{
				// 	$this->db->trans_rollback();
				// 	//$this->dbSQL->trans_rollback();
				// 	$result['tahap'] 	= "detail komponen";
				// 	$result['status'] 	= false;
				// }
			} else {

				$result['tahap'] 	= "insert history";
				$result['status'] 	= false;
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$result['tahap'] 	= "kode user";
			$result['status'] 	= false;
		}


		echo json_encode($result);
	}

	public function getTransaksiTransfer()
	{

		$kd_unit               = $_POST['kd_unit'];
		$tgl_kunjungan         = $_POST['Tglkunjungan'];
		$kd_pasien             = $_POST['Kodepasein'];
		$urut_masuk            = $_POST['urut'];

		$getTransfer = $this->db->query("
			SELECT dt.kd_produk,p.deskripsi, p.kd_klas,kp.klasifikasi from kunjungan k
			inner join transaksi t on k.kd_pasien=t.kd_pasien 
					and k.kd_unit=t.kd_unit 
					and k.tgl_masuk=t.tgl_transaksi 
					and k.urut_masuk=t.urut_masuk
			inner join detail_transaksi dt on dt.no_transaksi = t.no_transaksi and dt.kd_kasir = t.kd_kasir
			inner join produk p on p.kd_produk = dt.kd_produk
			inner join klas_produk kp on kp.kd_klas = p.kd_klas
			where 
				k.kd_pasien		='" . $kd_pasien . "' 
				and k.kd_unit	='" . $kd_unit . "' 
				and k.tgl_masuk	='" . $tgl_kunjungan . "'
				and k.urut_masuk=" . $urut_masuk . "
				and p.kd_klas = '9'
		")->result();

		if (count($getTransfer) > 0) {
			$result['pesan'] 	= "Data kunjungan pasien tidak berhasil dihapus! Pasien telah berkunjung ke unit penunjang.";
			$result['status'] 	= false;
			echo json_encode($result);
		} else {
			$getTransfer = $this->db->query("
				SELECT dt.kd_produk,p.deskripsi, p.kd_klas,kp.klasifikasi from kunjungan k
				inner join transaksi t on k.kd_pasien=t.kd_pasien 
						and k.kd_unit=t.kd_unit 
						and k.tgl_masuk=t.tgl_transaksi 
						and k.urut_masuk=t.urut_masuk
				inner join detail_transaksi dt on dt.no_transaksi = t.no_transaksi and dt.kd_kasir = t.kd_kasir
				inner join produk p on p.kd_produk = dt.kd_produk
				inner join klas_produk kp on kp.kd_klas = p.kd_klas
				where 
					k.kd_pasien		='" . $kd_pasien . "' 
					and k.kd_unit	='" . $kd_unit . "' 
					and k.tgl_masuk	='" . $tgl_kunjungan . "'
					and k.urut_masuk=" . $urut_masuk . "
					AND p.kd_produk not in('2','3')
			")->result();
			if (count($getTransfer) > 0) {
				$result['pesan'] 	= "Data kunjungan pasien tidak berhasil dihapus! Pasien sudah ada tindakan.";
				$result['status'] 	= false;
				echo json_encode($result);
			} else {
				$result['pesan'] 	= "bisa dihapus";
				$result['status'] 	= true;
				echo json_encode($result);
			}
		}
	}

	public function cek_kunjungan()
	{
		$kdPasien = $this->input->post("Kodepasein");
		$query = $this->db->query("SELECT * FROM kunjungan WHERE kd_pasien='" . $kdPasien . "'");
		if ($query->num_rows() > 0) {
			echo "{adakunjungan:true}";
		} else {
			echo "{adakunjungan:false}";
		}
	}

	public function deletepasien()
	{
		$result		= false;
		$kdPasien 	= $this->input->post("Kodepasein");
		$this->db->trans_begin();

		$this->db->where('kd_pasien', $kdPasien);
		$result = $this->db->delete('pasien');
		if (($result > 0 || $result === true)) {
			$this->db->trans_commit();
			$this->db->close();
			$response['success'] = true;
		} else {
			$this->db->trans_rollback();
			$this->db->close();
			$response['success'] = false;
		}
		echo json_encode($response);
	}
}
