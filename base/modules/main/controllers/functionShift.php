<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionShift extends  MX_Controller
{
	public $ErrLoginMsg = '';
	private $dbSQL      = "";

	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->library('common');
		$this->load->library('result');
	}

	public function index()
	{

		$this->load->view('main/index');
	}

	function getshift()
	{
		$unit = substr($_POST['kd_unit'], 0, 4);
		$shift = $this->db->query(" SELECT shift From rwi_shift WHERE kd_unit = '" . $unit . "' ")->row()->shift;
		echo $shift;
	}

	function getshiftIGD()
	{
		if (isset($_POST['command'])) {
			$kdbagianrwj = $this->getkdbagian();
			$strQuery = "SELECT shift From bagian_shift Where kd_bagian=" . $kdbagianrwj . "";
			$query = $this->db->query($strQuery);

			$res = $query->result();
			if ($query->num_rows() > 0) {
				foreach ($res as $data) {
					$curentshift = $data->shift;
				}
			}
			echo $curentshift;
		} else {

			$kdbagianrwj = $this->getkdbagian();
			$strQuery = "SELECT shift From bagian_shift Where kd_bagian=" . $kdbagianrwj . "";
			$query = $this->db->query($strQuery);

			$res = $query->result();
			if ($query->num_rows() > 0) {
				foreach ($res as $data) {
					$curentshift = $data->shift;
				}
			}
			return $curentshift;
		}
	}
	//----------------END------------------------


	function getkdbagian()
	{
		$kdbagianigd = '';
		$query_kdbagian = $this->db->query("
			SELECT bagian_shift.kd_bagian 
			FROM bagian_shift 
				INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian 
			WHERE bagian = 'Rawat Darurat' 
		");


		$result_kdbagian = $query_kdbagian->result();
		foreach ($result_kdbagian as $kdbag) {
			$kdbagianigd = $kdbag->kd_bagian;
		}
		return $kdbagianigd;
	}
}
