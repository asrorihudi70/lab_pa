<?php
/**

 * @author Ali
 * Editing by MSD
 * @copyright NCI 2015	
 */


//class main extends Controller {
class functionlapDataTarif extends  MX_Controller {

	public $ErrLoginMsg='';
	private $dbSQL      = "";
	public function __construct()
	{

			parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			//$this->dbSQL   = $this->load->database('otherdb3',TRUE);
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function cetak(){
		$param=json_decode($_POST['data']);
		
		//QUERY MASTER PRODUK DENGAN KODE TARIF 'AS'
		$result = $this->db->query("
			
			
			select distinct klasifikasi, deskripsi, kd_produk from (
				select a.kd_klas, kp.klasifikasi, b.kd_unit,c.nama_unit,a.kd_produk,a.deskripsi,b.kd_tarif,b.tarif 
				from produk a 
					inner join tarif b on b.kd_produk = a.kd_produk
					inner join unit c on c.kd_unit = b.kd_unit
					inner join klas_produk kp on a.kd_klas = kp.kd_klas
					where b.kd_tarif='AS' 
					and left(b.kd_unit,1) not in ('7','4')  
					and deskripsi not like 'Biaya%'
					group by a.deskripsi,a.kd_produk,c.nama_unit,a.deskripsi,b.kd_tarif,b.tarif ,b.kd_unit, kp.klasifikasi
					order by kp.klasifikasi, a.deskripsi asc
					
			) as a
			
			 
		")->result();
		
			$html= "<br><br><table  style='font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' cellpadding='2.5' border='1' >
   			<thead>
				<tr>
					<th >No.</th>
					<th >Klasifikasi</th>
					<th >Produk</th>
					<th >Kelas I</th>
					<th >Kelas II</th>
					<th >Kelas III</th>
					<th >Andalas VIP</th>
					<th >ICU/PICU</th>
					<th >NICU</th>
				</tr>
			
			</thead><tbody>
			";
			
			$no=1;
			foreach ($result as $line){
				
				//QUERY UNTUK AMBIL TARIF KELAS 1 BERDASARKAN KODE PRODUK DARI QUERY MASTER (PRODUK YANG MASUK KELAS 1)
				 $result_klas_1 = $this->db->query("
					select b.tarif
					from produk a 
					left join tarif b on b.kd_produk = a.kd_produk
					left join unit c on c.kd_unit = b.kd_unit
					where b.kd_tarif='AS' 
						and left(b.kd_unit,1) not in ('7') 
						and a.kd_produk='".$line->kd_produk."' 
						and c.nama_unit like  '%KELAS I'
						and tarif <> 0
					limit 1
				")->row();
				
				//QUERY UNTUK AMBIL TARIF KELAS 2 BERDASARKAN KODE PRODUK DARI QUERY MASTER (PRODUK YANG MASUK KELAS 2)
				 $result_klas_2 = $this->db->query("
					select b.tarif 
					from produk a 
					left join tarif b on b.kd_produk = a.kd_produk
					left join unit c on c.kd_unit = b.kd_unit
					where b.kd_tarif='AS' 
						and left(b.kd_unit,1) not in ('7') 
						and a.kd_produk='".$line->kd_produk."' 
						and c.nama_unit like '%KLS II'
						and tarif <> 0
					limit 1
				")->row();
				
				//QUERY UNTUK AMBIL TARIF KELAS 3 BERDASARKAN KODE PRODUK DARI QUERY MASTER (PRODUK YANG MASUK KELAS 3 DAN PRODUK RAWAT JALAN)
				$result_klas_3 = $this->db->query("
					select b.tarif 
					from produk a 
					left join tarif b on b.kd_produk = a.kd_produk
					left join unit c on c.kd_unit = b.kd_unit
					where b.kd_tarif='AS' 
						and left(b.kd_unit,1) not in ('7') 
						and a.kd_produk='".$line->kd_produk."' 
						and (left(b.kd_unit,1)='2' or c.nama_unit like '%KLS III')
						and tarif <> 0
					limit 1
				")->row();
				
				//QUERY UNTUK AMBIL TARIF ANDALAS VIP BERDASARKAN KODE PRODUK DARI QUERY MASTER (PRODUK YANG MASUK ANDALAS VIP)
				$result_klas_4 = $this->db->query("
					select b.tarif 
					from produk a 
					left join tarif b on b.kd_produk = a.kd_produk
					left join unit c on c.kd_unit = b.kd_unit
					where b.kd_tarif='AS' 
						and left(b.kd_unit,1) not in ('7') 
						and a.kd_produk='".$line->kd_produk."' 
						and c.nama_unit = 'ANDALAS VIP'
						and tarif <> 0
					limit 1
				")->row();
				
				
				//QUERY UNTUK AMBIL TARIF ICU/PICU  BERDASARKAN KODE PRODUK DARI QUERY MASTER (PRODUK YANG MASUK ICU/PICU )
				$result_klas_5 = $this->db->query("
					select b.tarif 
					from produk a 
					left join tarif b on b.kd_produk = a.kd_produk
					left join unit c on c.kd_unit = b.kd_unit
					where b.kd_tarif='AS' 
						and left(b.kd_unit,1) not in ('7') 
						and a.kd_produk='".$line->kd_produk."' 
						and c.nama_unit = 'ICU/PICU'
						and tarif <> 0
					limit 1
				")->row();
				
				//QUERY UNTUK AMBIL TARIF NICU  BERDASARKAN KODE PRODUK DARI QUERY MASTER (PRODUK YANG MASUK NICU)
				$result_klas_6 = $this->db->query("
					select b.tarif 
					from produk a 
					left join tarif b on b.kd_produk = a.kd_produk
					left join unit c on c.kd_unit = b.kd_unit
					where b.kd_tarif='AS' 
						and left(b.kd_unit,1) not in ('7') 
						and a.kd_produk='".$line->kd_produk."' 
						and c.nama_unit = 'NICU'
						and tarif <> 0
					limit 1
				")->row();
				
				$kelas_1 = 0;
				$kelas_2 = 0;
				$kelas_3 = 0;
				$kelas_4 = 0; //ANDALAS VIP
				$kelas_5 = 0; //ICU/PICU
				$kelas_6 = 0; //NICU
				if(count($result_klas_1) > 0){
					$kelas_1 = $result_klas_1->tarif;
				}
				if(count($result_klas_2) > 0){
					$kelas_2 = $result_klas_2->tarif;
				}
				if(count($result_klas_3) > 0){
					$kelas_3 = $result_klas_3->tarif;
				}
				if(count($result_klas_4) > 0){
					$kelas_4 = $result_klas_4->tarif;
				}
				
				if(count($result_klas_5) > 0){
					$kelas_5 = $result_klas_5->tarif;
				}
				
				if(count($result_klas_6) > 0){
					$kelas_6 = $result_klas_6->tarif;
				}
				
				if($kelas_1 == 0 && $kelas_2 == 0 && $kelas_3 == 0 && $kelas_4 == 0 && $kelas_5 == 0 && $kelas_6 == 0){
				}else{
					$html.="<tr>
						<td align='center'>".$no.". &nbsp;</td>
						<td>".$line->klasifikasi."</td>
						<td>".$line->deskripsi."</td>
						<td align='right'>".number_format($kelas_1, 0, '.', ',')."</td>
						<td align='right'>".number_format($kelas_2, 0, '.', ',')."</td>
						<td align='right'>".number_format($kelas_3, 0, '.', ',')."</td>
						<td align='right'>".number_format($kelas_4, 0, '.', ',')."</td>
						<td align='right'>".number_format($kelas_5, 0, '.', ',')."</td>
						<td align='right'>".number_format($kelas_6, 0, '.', ',')."</td>
						
					</tr>";
				}
				
				
				$no++;
			}
		$html.="
			</tbody>
		</table>";
		
		$prop=array('foot'=>true);
		// echo $html;
		// $this->common->setPdf('P','Laporan Data Tarif',$html);
		if($param->type_file == 1){
			$name='Laporan_Data_Tarif.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
		}else{
			$this->common->setPdf('P','Laporan Data Tarif',$html);
		}
		
	}
	public function cetak__(){
		$param=json_decode($_POST['data']);
		
		
		$NamaPasien 		= $param->NamaPasien;
		$TglPeriksa 		= date('d/M/Y',strtotime($param->tgl_masuk));
		$KdPasien 			= $param->kd_pasien;
		//$KdUnit 			= $param->kd_unit;
		$Tgl 				= date('d/M/Y',strtotime($param->tgl_masuk));
		
		// $UrutMasuk 			= $param->urut_masuk;
		// $urut 				= $param->urut;
		$KdTest 			= $param->KdTest;
		$NamaPemeriksaan 	= $param->NamaPemeriksaan;
		$Hasil 				= $param->Hasil;
		$Kesimpulan 		= $param->Kesimpulan;
		$Dokter 			= $param->Dokter;
		
		
		$data_pasien		= $this->db->query("select * from pasien where kd_pasien='".$KdPasien."'")->row();
		
		$Alamat 			= $data_pasien->alamat;
		$TglLahir 			= date('d/M/Y',strtotime($data_pasien->tgl_lahir)); 
		
		$KDDokter_pengirim 	= $param->Dokter_pengirim;
		
		
		$Dokter_pengirim	= $this->db->query("select nama from dokter where kd_dokter='".$KDDokter_pengirim."'")->row()->nama;
		
		
		$this->load->library('m_pdf');
		$this->m_pdf->load();
		$mpdf=new mPDF('utf-8', 'A4');
		$mpdf->AddPage('P', // L - landscape, P - portrait
			'', '', '', '',
			5, // margin_left
			5, // margin right
			10, // margin top
			15, // margin bottom
			0, // margin header
			5); // margin footer
		$this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            $NameRS = $query[0][0]->NAME;
            $Address = $query[0][0]->ADDRESS;
            $TLP = $query[0][0]->PHONE1;
            $Kota = $query[0][0]->CITY;
            $id_rs = $query[0][0]->CODE;
        } else {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$id_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		$html= "<table  style='font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' cellpadding='2.5' border='0' >
   			<thead>
			<tr >
				<td rowspan='5' width='20'>&nbsp;</td>
   				<th width='100' rowspan='5' >
   					<img src='./ui/images/Logo/lambangjatim.jpg' width='90' height='100' />
   				</th>
   				<th style='font-size: 16;' colspan='5' width='100'>PEMERINTAH PROPINSI JAWA TIMUR</th>
				<th width='90' rowspan='5' >
   					<img src='./ui/images/Logo/RSSM.png' width='90' height='100' />
   				</th>
   			</tr>
			<tr>
				<th style='font-size: 16;' colspan='5'>RUMAH SAKIT UMUM Dr. SOEDONO MADIUN</th>
			
			</tr>	
			<tr>
				<th style='font-size: 16;' colspan='5'>INSTALASI RADIOLOGI</th>
			
			</tr>
			<tr >
				<th style='font-size: 12px; font-weight:none; ' colspan='5' >".$rs->address.", ".$rs->city." ".$telp." ".$fax."</th>	
				
			</tr>
			<tr>
				<th style='font-size: 17;' colspan='5'>".strtoupper($rs->city)." ".$rs->zip."</th>
			
			</tr>
			<tr>
				<th colspan='8'><hr></th>
			</tr>
			<tr>
				
				<td width='200' colspan='2'>Nama</td>
				<td width='10'>:</td>
				<td width='220'>".$NamaPasien ."</td>
				
				<td width='10'></td>
				
				<td width='130'>Tanggal</td>
				<td  width='10'>:</td>
				<td width='220'>".$TglPeriksa."</td>
			</tr>
			<tr>
				<td colspan='2'>No.Medrec</td>
				<td >:</td>
				<td >".$KdPasien."</td>
				
				<td ></td>
				
				<td >Alamat</td>
				<td >:</td>
				<td >".$Alamat."</td>
			</tr>
			<tr>
				<td colspan='2'>Tgl. Lahir</td>
				<td >:</td>
				<td >".$TglLahir ."</td>
				
				<td ></td>
				
				<td >Dokter Pengirim</td>
				<td >:</td>
				<td >".$Dokter_pengirim."</td>
			</tr>
			<tr>
				<th colspan='8'><hr>&nbsp;</th>
			</tr>
			";
		$html.="
			</thead>
			<tbody>
				<tr><th colspan='8' align='left'>TS. Yth.</th></tr>
				<tr><th colspan='8' align='left'>Hasil Pemeriksaan ".$NamaPemeriksaan." :</th></tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan='7'>".nl2br($Hasil)."</td>
				</tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><th colspan='8' align='left'>Kesimpulan:</th></tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan='7'>".nl2br($Kesimpulan)."</td>
				</tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><td colspan='5'></td><td align='center' colspan='3'><b>Hormat kami,</b></td></tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><th colspan='8'>&nbsp;</th></tr>
				<tr><td colspan='5'></td><td align='center' colspan='3'><u><b>".$Dokter."</b></u></td></tr>
			</tbody>
		</table>";
		
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta"); 
		$date_cetak = gmdate("d-M-Y / H:i", time()+60*60*7);
		//$date = date("d-M-Y / H:i");
		$arr = array (
				'odd' => array (
						'L' => array (
								'content' => "",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'C' => array (
								'content' => '{PAGENO}{nbpg}',
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'R' => array (
								'content' => "Printed: ".$date_cetak."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'line' => 0,
				),
				'even' => array ()
		);
		$mpdf->SetFooter($arr);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="cetak_hasil_rad.pdf"' );
		readfile ( 'cetak_hasil_rad.pdf' );	 
	}	
	
	
}

?>