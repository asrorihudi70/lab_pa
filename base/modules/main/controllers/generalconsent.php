<?php
class generalconsent extends  MX_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->library('session','url');
    }
    public function cetak(){
			$common=$this->common;
			$title='General Consent';
			$param=json_decode($_POST['data']);
			$var = $param->kd_pasien;
			$unit = $param->kd_unit;
			$tgl = $param->tgl;
			$html='';
			$response = $this->db->query("SELECT TOP 1 P.kd_pasien, P.nama, u.nama_unit, P.nik, P.no_asuransi, P.tempat_lahir, P.tgl_lahir, '0' AS usia,
                                            CASE WHEN P.jenis_kelamin = 1 THEN 'Laki-laki' WHEN P.jenis_kelamin = 0 THEN 'Perempuan' ELSE '-' END jenis_kelamin,
                                            A.agama,
                                            s.suku,
                                            C.customer,
                                            pkj.pekerjaan,
                                            CASE
                                                WHEN P.status_marita = '0' THEN
                                                'Belum Kawin' 
                                                WHEN P.status_marita = '1' THEN
                                                'Kawin' 
                                                WHEN P.status_marita = '2' THEN
                                                'Janda' ELSE'Duda' 
                                            END status_marita,
                                            P.alamat,
                                            kel.kelurahan,
                                            kec.kecamatan,
                                            kab.kabupaten,
                                            prop.propinsi,
                                            pend.pendidikan,
                                            pj.nama_pj,
                                            pj.hubungan,
                                            pj.alamat AS alamat_pj,
                                            P.telepon,
                                            K.tgl_masuk,
                                            K.jam_masuk,
                                            CASE
                                                WHEN pj.hubungan = 1 THEN '' 
                                                WHEN pj.hubungan = 2 THEN 'Cucu' 
                                                WHEN pj.hubungan = 3 THEN 'Ibu' 
                                                WHEN pj.hubungan = 4 THEN 'Ayah' 
                                                WHEN pj.hubungan = 5 THEN 'Istri' 
                                                WHEN pj.hubungan = 6 THEN 'Suami' 
                                                WHEN pj.hubungan = 7 THEN 'Mertua' 
                                                WHEN pj.hubungan = 8 THEN 'Menantu' 
                                                WHEN pj.hubungan = 9 THEN 'Keluarga' 
                                                WHEN pj.hubungan = 10 THEN 'Saudara' 
                                                WHEN pj.hubungan = 11 THEN 'Lain-Lain'
                                                WHEN pj.hubungan = 13 THEN 'Anak' 
                                                    ELSE '' 
                                                    END AS hubungan_pj 
                                        FROM
                                            pasien P 
                                            INNER JOIN kunjungan K ON K.kd_pasien = P.kd_pasien
                                            LEFT JOIN penanggung_jawab pj ON pj.kd_pasien = K.kd_pasien 
                                            AND pj.kd_unit = K.kd_unit 
                                            AND pj.tgl_masuk = K.tgl_masuk 
                                            AND pj.urut_masuk = K.urut_masuk
                                            INNER JOIN unit u ON u.kd_unit = K.kd_unit
                                            INNER JOIN agama A ON A.kd_agama = P.kd_agama
                                            INNER JOIN suku s ON s.kd_suku = P.kd_suku
                                            INNER JOIN customer C ON C.kd_customer = K.kd_customer
                                            INNER JOIN pekerjaan pkj ON pkj.kd_pekerjaan = P.kd_pekerjaan
                                            INNER JOIN kelurahan kel ON kel.kd_kelurahan = P.kd_kelurahan
                                            INNER JOIN kecamatan kec ON kec.kd_kecamatan = kel.kd_kecamatan
                                            INNER JOIN kabupaten kab ON kab.kd_kabupaten = kec.kd_kabupaten
                                            INNER JOIN propinsi prop ON prop.kd_propinsi = kab.kd_propinsi
                                            INNER JOIN pendidikan pend ON pend.kd_pendidikan = P.kd_pendidikan 
                                        WHERE
                                            k.kd_unit = '".$unit."'
                                            AND P.kd_pasien = '".$var."' 
                                        ORDER BY
                                            k.tgl_masuk DESC"); // ->result();

		$timestamp = strtotime($response->row()->tgl_lahir);
		// Define an array of month names in Indonesian
		$monthNames = [
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember',
		];
		// Extract day, month, and year from the timestamp
		$day = date('d', $timestamp);
		$month = date('n', $timestamp);
		$year = date('Y', $timestamp);
		// Create the formatted date string
		$tanggallahir = $day . '-' . $monthNames[$month] . '-' . $year;
		if($response->num_rows() > 0){
			$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:0px;padding-top:0px;font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center" width="100%" height="45px">
								<hr>
								<font style="font-size:14px;"><b>PERSETUJUAN UMUM/ <i>GENERAL CONSENT</i><br><center>PASIEN RAWAT JALAN</center></b></font>
							</td>
						</tr>
						<tr>
							<td colspan="6" align="left" width="100%">
								<font style="font-size:13px;">Yang membuat pernyataan :</font>
							</td>
						</tr>
					</table>
					<table border="0" cellspacing="0" cellpadding="0" >
                        <tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Nama Pasien</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->nama_pj.'</td>
                            <td style="font-family: Arial;font-size:13px;" width="110px">Umur</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="250px">'.$response->row()->usia.'</td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Alamat</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->alamat_pj.'</td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Telepon/HP</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->telepon.'</td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;"><b>Hubungan dengan pasien sebagai : orangtua/wali/suami/istri/keluarga, kepada pasien : </b></font></td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Nama</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->nama.'</td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Tanggal Lahir</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$tanggallahir.'</td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Alamat</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->alamat.'</td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Pekerjaan</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->pekerjaan.'</td>
                        </tr>
						<tr>
                            <td style="padding-left:5px;padding-top:5px;padding-bottom:5px;font-family: Arial;font-size:13px;" width="130px">Agama</td>
                            <td style="font-family: Arial;font-size:13px;" width="5px">:</td>
                            <td style="font-family: Arial;font-size:13px;" width="230px">'.$response->row()->agama.'</td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>I. PERSETUJUAN PELAYANAN KESEHATAN</b></font></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">Saya mengetahui bahwa saya memiliki kondisi yang membutuhkan perawatan medis, saya memberi ijin kepada dokter dan profesi kesehatan lainnya untuk melakukan prosedur diagnostik dan untuk memberikan pengobatan medis seperti yang diperlukan untuk penilaian secara profesional.
							Prosedur diagnostik dan perawatan medis termasuk terapi tidak terbatas pada ECG, X-Ray, USG, tes darah, terapi fisik dan pemberian obat.
							Persetujuan yang saya berikan tidak termasuk persetujuan untuk prosedur/tindakan invasive (misalnya operasi) atau tindakan yang mempunyai risiko tinggi.</font></td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>II. PELEPASAN INFORMASI MEDIS</b></font></td>
                        </tr>';
						$html .= '
                        <tr>
							<td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">
								<ol type="a">
									<li><font style="font-family: Arial; font-size:13px;">Saya memahami informasi kesehatan yang ada dalam diri saya, termasuk diagnosa, hasil laboratorium, dan hasil tes diagnostik lainnya yang akan digunakan untuk perawatan medis saya. Rumah Sakit Suaka Insan akan menjamin kerahasiaannya.</font></li>
									<li><font style="font-family: Arial; font-size:13px;">Saya memberi wewenang kepada rumah sakit untuk memberikan informasi tentang diagnosa, hasil pelayanan kesehatan, dan pengobatan saya bila diperlukan untuk memproses klaim asuransi/BPJS/Perusahaan/perorangan atau lembaga lain yang bertanggung jawab atas biaya pelayanan kesehatan saya dan atau pemerintah yang berwenang.</font></li>
									<li><font style="font-family: Arial; font-size:13px;">Saya memberikan wewenang kepada Rumah Sakit Suaka Insan untuk memberi informasi tentang diagnosa, hasil pelayanan kesehatan, dan pengobatan saya kepada anggota keluarga saya, yaitu kepada :</font></li>
									<ol type="1">
										<li>.....................................................................................................................</li>
										<li>.....................................................................................................................</li>
										<li>.....................................................................................................................</li>
									</ol>
								</ol>
							</font></td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>III. PRIVASI</b></font></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">
							Saya memberi kuasa kepada RS Suaka Insan untuk menjaga privasi dan kerahasiaan penyakit saya.
							</font></td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>IV. HAK DAN TANGGUNG JAWAB PASIEN</b></font></td>
                        </tr>
                        <tr>
							<td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">
								<ol type="a">
									<li><font style="font-family: Arial; font-size:13px;">Saya memiliki hak untuk mengambil bagian dalam keputusan mengenai penyakit saya dan dalam hal perawatan medis dan rencana pengobatan.</font></li>
									<li><font style="font-family: Arial; font-size:13px;">Saya telah mendapat informasi tentang hak dan kewajiban pasien di Rumah Sakit Suaka Insan melalui leaflet dan pigura yang ditempel di rumah sakit.</font></li>
								</ol>
							</font></td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>V. INFORMASI RAWAT JALAN</b></font></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">
							Saya telah menerima informasi tentang peraturan yang diberlakukan oleh instalasi rawat jalan rumah sakit dan saya beserta bersedia untuk mematuhinya.
							</font></td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>VI. INFORMASI BIAYA</b></font></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">
							Saya memahami tentang informasi biaya pengobatan atau biaya tindakan medis yang dijelaskan oleh petugas rumah sakit.
							</font></td>
                        </tr>
						<tr>
                            <td colspan="6" align="left" style="padding-top:5px;padding-bottom:0px;"><font style="font-family: Arial;font-size:13px;"><b>TANDA TANGAN</b></font></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="justify" style="padding-left:13px;padding-top:0px;padding-bottom:5px;"><font style="font-family: Arial;font-size:13px;">
							Dengan tanda tangan saya dibawah ini, saya menyatakan bahwa saya telah membaca dan memahami item pada persetujuan umum/<i>general consent</i>.
							</font></td>
                        </tr>
					</table>';
		}
		$html.='</body></html>';		
		// echo $html; die;		
		$prop=array('foot'=>true);
		$this->common->setPdfConsent('P','General Consent',$html, $param);
	}
}
?>