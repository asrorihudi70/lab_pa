<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class main extends  MX_Controller {		
	
    public function __construct()
    {
		parent::__construct();  
    }
	 
    public function index()
    {
      $this->load->view('main/index',$data=array('controller'=>$this));
    }
    
	    function Login()
    {
           
            $p1  =$this->input->post('UserName');
            if (strlen($p1)!==0) {
                $p2  =$this->input->post('Password');
                $this->load->model("userman/tblzusers");
                $this->db->where("user_names",$p1);
                //$this->db->where("password",md5($p2)); tidak bisa di compare dlm database ueey
               // $this->db->where("password",p2);
                $res = $this->tblzusers->GetRowList();
                if ($res[1]!=='0' )
                {
                    $cpwd=md5($p2);
                    if (trim($res[0][0]->PASSWORD)===$cpwd)
                    {
                        $data = array(
                       'username'  => $p1,
                       'logged_in'  => TRUE,
                        'logtime'  => time()
                         );
                         session_cache_expire(0.5);
                        $this->session->set_userdata( 'user_id',$data);
                        //redirect('main/index');
                        //  $this->load->view('main/index');
                         $this->load->view('main/index',$data=array('controller'=>$this));
                    }
                    else
                    {
                        $this->ErrLoginMsg="Password Tidak Dikenal";
                         $this->load->view('main/index',$data=array('controller'=>$this));
                    }
                }
                else
                {
                      //redirect('main/index');
                     // $this->load->view('main/index');
                    $this->ErrLoginMsg="Nama User Tidak Dikenal";
                     $this->load->view('main/index',$data=array('controller'=>$this));
                }
            }
             else {
                    $this->ErrLoginMsg="Nama User dan password harus diisi";
                    $this->load->view('main/index',$data=array('controller'=>$this));
            }
    }

   function Logoff()
    {
          $logged = $this->session->userdata('user_id');
          if ($logged)
          {
              if ($logged['logtime']===0)
              {
                  $this->ErrLoginMsg="Anda terlalu lama tidak melakukan aktifitas , silakan login ulang untuk melanjutkan.";
              }
          }
         $this->session->unset_userdata('user_id');

         $this->load->view('main/index',$data=array('controller'=>$this));
     
    }
    // checking user logged user by registered session
    function _is_logged_in()
    {
        //return TRUE;
        
        $logged = $this->session->userdata('user_id');
        if ($logged)
        {
            
            $elapsed=time()-$logged['logtime'];
            if ($elapsed<=900)
            {
                $logged['logtime']=time();
                return true;
            }
       
            else
            {

                $logged['logtime']=0;
                return false;
            }
        }
        else
        {
           $logged['logtime']=0;
            return FALSE;
        }
    }

	
    private function CRUDLoadController($target, $Params, $method)
    {
        //format text pengenal;sintak controller(module/controller/method)

		$tambahdelimiter= ArrayUtils::GetStrListFile( base_url()."ui/jslist/DataAccessList.php");	

        foreach ($tambahdelimiter as $rowDA)
        {
            if (trim($rowDA)!='')
            {
                list($pengenal, $sintak)= explode(';',$rowDA);
                if (trim($target)==trim($pengenal))
                {
                    // load controller
                    list($mod, $ctrl)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    $this->$ctrl->$method($Params);
                }
            }
        }
				    	
    }

    public function CreateDataObj()
    {
        //$this->load->library('nci_lib');

        if ($this->IsAjaxRequest())
        {
            $arrPost=array();

            foreach($_POST as $key=>$value)
            {
                $arrPost[$key]= $value;
            }

            if (count($arrPost)>0)
            {
                $target = $arrPost['Table'];

                if ($target!="")
                {
                    $this->CRUDLoadController($target,$arrPost,'save');

                } else $this->load->view('main/index');

            } else $this->load->view('main/index');

        } else $this->load->view('main/index');

    }

    public function DeleteDataObj()
    {

        if ($this->IsAjaxRequest())
        {

            $arrPost=array();

            foreach($_POST as $key=>$value)
            {
                $arrPost[$key]= $value;
            }

            if (count($arrPost)>0)
            {
                $target = $arrPost['Table'];

                if ($target!="")
                {

                    $this->CRUDLoadController($target,$arrPost,'delete');

                } else $this->load->view('main/index');

            } else $this->load->view('main/index');

        } else $this->load->view('main/index');

    }
	    
    public function ReadDataObj()
    {

        if ($this->IsAjaxRequest())
        {
            $Skip = isset($_REQUEST['Skip']) ? $_REQUEST['Skip'] : 0;
            $Take = isset($_REQUEST['Take']) ? $_REQUEST['Take'] : 1000;
            $Sort = isset($_REQUEST['Sort']) ? $_REQUEST['Sort'] : "";
            $Sortdir = isset($_REQUEST['Sortdir']) ? $_REQUEST['Sortdir'] : "ASC";
            $target = isset($_REQUEST['target']) ? $_REQUEST['target'] : "";
            $param = isset($_REQUEST['param']) ? $_REQUEST['param'] : "";

            //$target ='ViewSettingBahasa';
            //$Params = array(0,1000,'LIST_KEY_DESC','ASC','"GROUP_KEY_ID" = ~030~ AND \"LANGUAGE_ID\"=~1~ ");
            //$skip = (int)$Skip;
            //echo '<script type="text/javascript">alert("'.$param.'");</script>';

            $Params = array((int)$Skip,(int)$Take,$Sort,$Sortdir,$param);

            if ($target=="")
            {
                    $this->load->view('main/index');
            } else $this->CRUDLoadController($target,$Params,'read');

        } else $this->load->view('main/index');

    }


    private function IsAjaxRequest()
    {
        /* AJAX check  */
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
    	} else return false;

    }
    
    public function ExecProc()
    {

    	//format txt proses yang akan digunakan
    	// pengenal;sintakController/ 

        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";

		$tambahdelimiter= ArrayUtils::GetStrListFile(base_url()."ui/jslist/ProcessList.php");	

        //echo '<script type="text/javascript">alert("ada");</script>';

        foreach ($tambahdelimiter as $rowProc)
        {

            if (trim($rowProc)!='')
            {

                list($pengenal, $sintak)= explode(';',$rowProc);

                if (trim($ModuleID)==trim($pengenal))
                {
                    //load controller
                    //return echo json lsg dari controller

                    list($mod, $ctrl, $method)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    $this->$ctrl->$method($Params);

                }
            }
        }
    }
    
    public function ExecReport()
    {
    	//format txt proses yang akan digunakan
    	// pengenal;sintakController/nama

        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";

        //$loadFile=file_get_contents( base_url()."ui/jslist/ReportList.php");
        //$tambahdelimiter= explode(chr(13).chr(10),$loadFile);
	 $tambahdelimiter= ArrayUtils::GetStrListFile( base_url()."ui/jslist/ReportList.php");	
        //echo '<script type="text/javascript">alert("ada");</script>';

        foreach ($tambahdelimiter as $rowProc)
        {

            if (trim($rowProc)!='')
            {

                list($pengenal, $sintak)= explode(';',$rowProc);

                if (trim($ModuleID)==trim($pengenal))
                {
                    //load controller
                    //return echo json lsg dari controller

                    list($mod, $ctrl)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    echo $this->$ctrl->CreateReport('', $UserID,$Params,0,0,'','');

                }
            }
        }
    }

    public function getModule() //$UserID,$ModuleID)
    {
		if  ($this->_is_logged_in())
       {
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
		$arrListJS= ArrayUtils::GetStrListFile(base_url()."ui/jslist/".$ModuleID."jslist.php");	
								
    	$this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetModule($UserID, $ModuleID);

        if ($query->num_rows() > 0)
        {
            //$row = $query->row();
            $row=$query->row_array();
            //$id= $row['Mod_ID'];
            $id= $row['mod_id'];
            //$title = $row['Mod_Name'];
            $title = $row['mod_name'];

            $url= ''; 
        }
        //Dim v = New With {.title = vd.title, .id = vd.id, .url = vd.url, .htm = m}

        echo '{success:true, title:"'.$title.'", id:"'.$id.'", url:"'.$url.'", htm:'.json_encode($arrListJS).'}';
       }
       else
       {
            echo '{success : false, expire:true}';
       }

    }
	
    public function getReport()
    {
	   if  ($this->_is_logged_in())
       {
			$UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
			$ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";

        //$loadFile=file_get_contents( base_url()."ui/jslist/".$ModuleID."jslist.php");
        //$tambahdelimiter= explode(chr(13).chr(10),$loadFile);
		$tambahdelimiter= ArrayUtils::GetStrListFile(base_url()."ui/jslist/".$ModuleID."jslist.php");	

        foreach ($tambahdelimiter as $rowJS)
        {
            if (strlen($rowJS)>0)
            {
                $arrListJS[]=$rowJS;
            }
        }

    	/*  sementara langsung tanpa cek trustee 

         *
         */
        $this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetModule($UserID, $ModuleID);

        if ($query->num_rows() > 0)
        {
            //$row = $query->row();
            $row=$query->row_array();
            $id= $row['mod_id'];
            $title = $row['mod_name'];
            $url= ''; //$row['Mod_URL'];
        }


        $haveDialog="false";

        if (count($arrListJS)!==0)
        {$haveDialog="true";}

        echo '{success:true, title:"'.$title.'", id:"'.$id.'", url:"'.$url.'",
            htm:'.json_encode($arrListJS).', havedialog: '.$haveDialog.'}';
       }
       else
       {
            echo '{success : false, expire:true}';
       }
    }

     Public Function Upload()
    {
        //$direktori = isset($_REQUEST['direktori']) ? $_REQUEST['direktori'] : "";
        //$file = isset($_REQUEST['file']) ? $_REQUEST['file'] : "";
        
        $rEFileTypes = "/^\.(png|gif|tiff|bmp|jpg|jpeg|xls|doc|txt|pdf|ppt|txt|pptx|docx|exe){1}$/i";

        $isFile = is_uploaded_file($_FILES['file']['tmp_name']);

        if ($isFile)
        {

            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($_FILES['file']['name']));

            $lokasi_file = $_FILES['file']['tmp_name'];
            //$nama_file   = $_FILES['file']['name'];
            //$ukuran_file = $_FILES['file']['size'];
            //$type_file = $_FILES['file']['type'];

            $uploads_dir =base_url().'Img Asset/'.$safe_filename;

            //($type_file == $type_allow and strpos($nama_file, " ") == false)

            if (preg_match($rEFileTypes, strrchr($safe_filename, '.')))
            {
                $result = move_uploaded_file($lokasi_file, $uploads_dir);

                if ($result==1)
                {

                    echo '{success:true, result:"Upload Sukses", namafile:"'.$safe_filename.'"}';

                } else echo '{success:false, error:"Upload Gagal"}';

            } else echo '{success:false, error:"Type File Salah"}';
            
        } else echo '{success:false, error:"Upload Gagal"}';

    }
	
    Public Function UploadRef()
    {

        if ($isFile)
        {
            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($_FILES['fileAsetRef']['name']));

            $lokasi_file = $_FILES['fileAsetRef']['tmp_name'];
            //$nama_file   = $_FILES['file']['name'];
            //$ukuran_file = $_FILES['file']['size'];
            //$type_file = $_FILES['fileAsetRef']['type'];

            $uploads_dir =base_url().'Doc Asset/'.$safe_filename;

            $extension= end(explode(".", $file['name']));

            $file_type =  strrchr($safe_filename, '.');

            if (preg_match($rEFileTypes, $file_type))
            {
                $result = move_uploaded_file($lokasi_file, $uploads_dir);

                if ($result==1)
                {

                    echo '{success:true, result: "Upload Sukses", namafile: "'.$safe_filename.'", namaext: "'.$file_type.'"}';

                } else echo '{success:false, error:"Upload Gagal"}';

            } else echo '{success:false, error:"Type File Salah"}';
            
        } else echo '{success:false, error:"Upload Gagal"}';
        
    }
	
	
    Public function getLanguage()
    {
		
        $Kd_User=isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";

        //$Kd_User='0';
        $strLanguageID="1";
        $arrLanguage=array();
		
    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();

        if ($query->num_rows() > 0)
        {
            //$strLanguageID=$row->LANGUAGE_ID;
            $strLanguageID=$row->language_id;
        } else $strLanguageID="1";

        if ($strLanguageID==null or $strLanguageID=="" )
                $strLanguageID='1';

        $this->load->model('main/vilanguage');
        $query = $this->vilanguage->read($strLanguageID);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $rows)
            {
                //$arrLanguage[]=$rows;
                $arrLanguage[]=$this->FillRow($rows);
            }

			//Dim result = New With {.success = True, .total = mListLanguange.Count, .ListLanguange = mListLanguange}
            echo '{success:true, total:'.count($arrLanguage).', ListLanguage:'.json_encode($arrLanguage).'}';

        }
        else
        {
            echo '{success:false}';
        };
				
    }

    private function FillRow($rec)
    {
        $row=new Rowgetlanguage;
        $row->LIST_KEY=$rec["list_key"];
        $row->LABEL=$rec["label"];

        return $row;
    }
	    
    public function getTrustee()
    {
   	    	
    	$Kd_User=isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
    	
        //$Kd_User='0';

        $this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetTrustee1($Kd_User);
        //$idx=0;
        $cGroup="";
        $amList = array();
        $agroup=array();
        $strModName="";

        //foreach($query->result() as $rows){

        foreach($query->result_array() as $rows) {

            $strModName=$this->getModName($Kd_User, $rows['mod_id']);
				
            if ($strModName == "")
                $strModName = $rows['mod_id'];//$rows->Mod_ID;
			
                $strGroupName=$this->getModName($Kd_User, $rows['mod_key']);
				
                if ($strGroupName == "")
                    $strGroupName = $rows['mod_group']; //$rows->Mod_Group;
                
                $strTmpGroup = $rows['mod_group']; //$rows->Mod_Group;

                $rows['mod_group']=$strGroupName;
        	$rows['mod_url'] =str_replace("[mod_id]", $rows['mod_id'], $rows['mod_url']);
        	$rows['mod_url'] =str_replace("[image]", "ui/".$rows['mod_imgurl'],$rows['mod_url']);
        	$rows['mod_url'] =str_replace("[Mod_Name]",$strModName ,$rows['mod_url']);
			
                if (strlen(strstr($cGroup,$strTmpGroup))==0)
                {
                    $htm="";
                    $this->load->model('main/vi_gettrustee');
                    $query1 = $this->vi_gettrustee->readforGetTrustee2($Kd_User,$strTmpGroup);

                    foreach($query1->result_array() as $rows1) {
                        $strModName=$this->getModName($Kd_User,$rows1['mod_id']);

                        if ($strModName=="")
                                $strModName=$rows1['mod_name']; //$rows1->Mod_Name;
						
                	$rows1['mod_url'] = str_replace("[mod_id]", $rows1['mod_id'],$rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[image]", "ui/".$rows1['mod_imgurl'],$rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[Mod_Name]", $strModName, $rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[Mod_Desc]", $rows1['mod_desc'],$rows1['mod_url']);
			$htm .= $rows1['mod_url'].chr(13).chr(10);
					
		}
				
					
		//$agroup[]=array("Mod_Group"=>$strGroupName, "Htm"=>$htm);
                $agroup[]=array("mod_group"=>$strGroupName, "htm"=>$htm);
            }
			
            $amList[]=$rows;
	}

		$dd ='{success:true, total:'.count($amList).', ListTrustee:'.json_encode($amList).', ListReport:'.json_encode($agroup).'}';

        //echo '<script type="text/javascript">alert("'.$dd.'");</script>';
        echo $dd;
			 	
    }     
            
    private function getModName($Kd_User, $strModID)
    {
   		
   		// ambil strIDLang
    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();
        //$strIDLang=$row->LANGUAGE_ID;
        $strIDLang=$row->language_id;

        if ($strIDLang==null)
                $strIDLang=1;

        $this->load->model('main/getmodname');
        $query = $this->getmodname->read($strModID,$strIDLang);

        // isi return function

        $returnVal="";

        if ($query->num_rows() > 0)
        {
            $row= $query->row();
            //$returnVal=$row->LABEL;
            $returnVal=$row->label;
        };

        return $returnVal;
																		   	 	
    }
        
}

class Rowgetlanguage
{
    public $LIST_KEY;
    public $LABEL;

}

class ArrayUtils
{
	// buang element aray yang null, untuk aray satu dimensi
	static function NullElement($val)
	{
		return ($val<>null);
	}
	static function GetStrListFile($FileList)
	{	
			$itm=file_get_contents($FileList);
			$tmp=json_encode($itm);
			$tmp2=str_replace(  '\n','*' ,$tmp);
			$tmp=str_replace(  '\/','/' ,$tmp2);
			//Logger::put( $tmp );
			//Logger::put( $tmp2 );	
			$Res = explode('*', $tmp );
			return $Res;
	}
}

?>