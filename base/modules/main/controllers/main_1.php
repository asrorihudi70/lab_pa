<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class main extends  MX_Controller {		
	public $tanggal;
	public $jam;
	public $db_rs;

    public $ErrLoginMsg='';
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->tanggal = date("Y-m-d");
		$this->jam     = date("H:i:s");
		$this->db_rs   = $this->db->query("SELECT * FROM db_rs");
    }
	
    public function index(){
        $this->load->view('main/index',
        	$data=array(
        		'controller' 	=> $this,
        		'data_rs' 		=> $this->db_rs,
        	)
        );
    }
    
	public function getSetting(){
		$this->load->library("result");
		$keys=$_POST['key'];
		$mod_id=$_POST['mod_id'];
		if($mod_id != null && $mod_id !=''){
			$jum=count($keys);
			$subQuery='';
			if($jum>0){
				$q='';
				for($i=0,$iLen=$jum;$i<$iLen;$i++){
					if($q!=''){
						$q.=",";
					}
					$q.="'".$keys[$i]."'";
				}
				$subQuery="AND key in(".$q.")";
			}
			$res=$this->db->query("SELECT key,value FROM zmodule_setting where mod_id='".$mod_id."' ".$subQuery)->result();
			$arr=array();
			for($i=0,$iLen=count($res);$i<$iLen;$i++){
				$arr[$res[$i]->key]=$res[$i]->value;
			}
			$this->result->setData($arr)->end();
		}else{
			$this->result->setMessage('Mod ID Tidak Ada')->error()->end();
		}
	}

	function menu(){
		session_start();
		$query = "";
		if (isset($_SESSION['nci'])) {
			$this->db->select("*");
			$this->db->where(array( 'kd_user' => $_SESSION['nci'], ));
			$this->db->from("zusers");
			$query = $this->db->get();
		}
			$p1 = $this->input->post('UserName');
			if (strlen($p1) !==0) {
                $p2  =$this->input->post('Password');
                $this->load->model("userman/tblzusers");
                $this->db->where("user_names",$p1);
                
                $res = $this->tblzusers->GetRowList();
                if ($res[1]!=='0' ){
                    $cpwd=md5($p2);
                    if (trim($res[0][0]->PASSWORD)===$cpwd){
                    	$get_member=$this->db->query("SELECT b.group_id FROM zusers a inner join zmember b on a.kd_user = b.kd_user WHERE a.user_names='".$p1."' AND a.password= md5('".$p2."')");
                    	if ($get_member->num_rows() > 0) {
                    		$get_member = $get_member->row()->group_id;
                    	}else{
                    		$get_member = "";
                    	}
						$data = array(
						'id'           => $res[0][0]->KD_USER,
						'kdunit'       => $res[0][0]->KD_UNIT,
						'kd_dokter'    => $res[0][0]->KD_DOKTER,
						'username'     => $p1,
						'logged_in'    => TRUE,
						'logtime'      => time(),
						'currentshift' => $this->getcurrentshift(),
						'aptkdmilik'   => $res[0][0]->KD_MILIK,
						'aptkdunitfar' => $res[0][0]->KD_UNIT_FAR,
						'kd_rs'        =>'3577015',
						'member'	   => $get_member
						);
						$_SESSION['nci']=$res[0][0]->KD_USER;
						// session_cache_expire(0.5);
						$this->session->set_userdata( 'user_id',$data);
						setcookie('NCI',json_encode($data), time() + (86400 * 30), "/");

						$this->db->select("*");
						$this->db->where(array( 'kd_user' => $_SESSION['nci'], ));
						$this->db->from("zusers");
						$query = $this->db->get();
						//
						//$this->session->sess_expiration(0);
						//redirect('main/index');
                         //  $this->load->view('main/index');
                         $this->load->view('main/menu',$data=array(
							'controller' => $this,
							'data'       => $query,
							'data_rs'    => $this->db_rs,
                         ));
                    }
                    else
                    {
                        //$this->ErrLoginMsg="Password Tidak Dikenal";
                         //$this->load->view('main/index',$data=array('controller'=>$this));
                        $data['error'] = "Password Anda Tidak Tepat";
                        $this->load->view('main/index',$data=array(
							'controller' =>$this,'error'=>'Maaf, Password Anda tidak benar',
							'data'       => $query,
							'data_rs'    => $this->db_rs,
                         ));
						 
                    }
                }
                else
                {
					$data['error'] = "User Tidak Terdaftar";
					$this->load->view('main/index',$data =array(
							'controller' => $this,
							'error'      =>'Maaf, Username Anda Belum Terdaftar',
							'data'       => $query,
							'data_rs'    => $this->db_rs,
						)
					);
                }
            }
             else {
				// $this->ErrLoginMsg                  ="Nama User dan password harus diisi";
				//$this->load->view('main/index',$data =array('controller'=>$this));
				$data['error'] = "Nama User dan password harus diisi";
				$this->load->view('main/menu',$data = array(
						'controller' =>$this,
						'error'      =>'Nama User dan Password harus diisi',
						'data'       => $query,
						'data_rs'    => $this->db_rs,
					)
				);
            }
    }


    function login($kd_bagian = null){	
            $p1  =$this->input->post('UserName');
            if (strlen($p1)!==0) {
                $p2  =$this->input->post('Password');
                $this->load->model("userman/tblzusers");
                $this->db->where("user_names",$p1);
                //$this->db->where("password",md5($p2)); tidak bisa di compare dlm database ueey
               // $this->db->where("password",p2);
                $res = $this->tblzusers->GetRowList();
                if ($res[1]!=='0' )
                {
                    $cpwd=md5($p2);
                    if (trim($res[0][0]->PASSWORD)===$cpwd)
                    {
                        $data = array(
							'id'           => $res[0][0]->KD_USER,
							'kdunit'       => $res[0][0]->KD_UNIT,
							'kd_dokter'    => $res[0][0]->KD_DOKTER,
							'username'     => $p1,
							'logged_in'    => TRUE,
							'logtime'      => time(),
							'currentshift' => $this->getcurrentshift(),
							'aptkdmilik'   => $res[0][0]->KD_MILIK,
							'aptkdunitfar' => $res[0][0]->KD_UNIT_FAR,
							'kd_rs'        =>'3577015'
                         );
						 session_start();
						 $_SESSION['nci']=$res[0][0]->KD_USER;
                        // session_cache_expire(0.5);
						//
						//$this->session->sess_expiration(0);
						//redirect('main/index');
						//  $this->load->view('main/index');
						$this->session->set_userdata( 'user_id',$data);
						setcookie('NCI',json_encode($data), time() + (86400 * 30), "/");
						$this->load->view('main/index',$data =array('controller'=>$this, 'data_rs' => $this->db_rs,));
                    }
                    else
                    {
                        //$this->ErrLoginMsg="Password Tidak Dikenal";
                         //$this->load->view('main/index',$data=array('controller'=>$this));
                        $data['error'] = "Password Anda Tidak Tepat";
                        $this->load->view('main/index',$data=array('controller'=>$this, 'data_rs' => $this->db_rs,'error'=>'Maaf, Password Anda tidak benar'));
						 
                    }
                }
                else
                {
                      //redirect('main/index');
                     // $this->load->view('main/index');
                   // $this->ErrLoginMsg="Nama User Tidak Dikenal";
                     //$this->load->view('main/index',$data=array('controller'=>$this));
					  $data['error'] = "User Tidak Terdaftar";
                     $this->load->view('main/index',$data=array('controller'=>$this,'data_rs' => $this->db_rs,'error'=>'Maaf, Username Anda Belum Terdaftar'));
                }
            }
             else {
                   // $this->ErrLoginMsg="Nama User dan password harus diisi";
                    //$this->load->view('main/index',$data=array('controller'=>$this));
                    if(isset($this->session->userdata['user_id']['id']) && $kd_bagian != null){
						$criteria = array(
							'kd_user' 		=> $this->session->userdata['user_id']['id'],
							'kd_bagian' 	=> $kd_bagian,
						);
						$this->db->select("*");
						$this->db->where($criteria);
						$this->db->from("setting_apt");
						$query = $this->db->get();
						if ($query->num_rows() > 0) {
							
							unset($criteria);
							$params     = array(
								'kd_unit_far' => $query->row()->kd_unit_far,
							);
							$criteria 	= array(
								'kd_user' 	=> $this->session->userdata['user_id']['id'],
							);
							$this->db->where($criteria);
							$this->db->update("zusers", $params);


	                        $data = array(
								'id'           => $this->session->userdata['user_id']['id'],
								'kdunit'       => $this->session->userdata['user_id']['kdunit'],
								'kd_dokter'    => $this->session->userdata['user_id']['kd_dokter'],
								'username'     => $this->session->userdata['user_id']['username'],
								'logged_in'    => TRUE,
								'logtime'      => time(),
								'currentshift' => $this->session->userdata['user_id']['currentshift'],
								'aptkdmilik'   => $this->session->userdata['user_id']['aptkdmilik'],
								'aptkdunitfar' => $query->row()->kd_unit_far,
								'kd_rs'        =>'3577015'
							);
							$_SESSION['nci'] =$this->session->userdata['user_id']['id'];
							$this->session->unset_userdata('user_id');
							session_start();
	                        // session_cache_expire(0.5);
							//
							//$this->session->sess_expiration(0);
							//redirect('main/index');
							//  $this->load->view('main/index');
							$this->session->set_userdata( 'user_id',$data);
							setcookie('NCI',json_encode($data), time() + (86400 * 30), "/");
	                    	$this->load->view('main/index',$data=array('controller'=>$this, 'data_rs' => $this->db_rs,));
						}else{
	                    	$this->load->view('main/index',$data=array('controller'=>$this, 'data_rs' => $this->db_rs,));
						}
					}else{
	                    $this->load->view('main/index',$data=array('controller'=>$this, 'data_rs' => $this->db_rs,));
					}
					
            }
    }
	
	public function gantiPass()
	{
		$passlama= md5($_POST['passlama']);
		$passbaru= $_POST['passbaru'];
		$konfpass= md5($_POST['konfpass']);
		
		$kduser=$this->session->userdata['user_id']['id'];
		$cekkauser=$this->db->query("select * from zusers where kd_user='$kduser' and password='$passlama' ")->result();
		if (count($cekkauser)==1)
		{
			$update = $this->db->query("update zusers set password='$konfpass' where kd_user='$kduser'");
		}
		else
		{
			$update=false;
		}
        if ($update)
		{
			echo "{success : true}";
		}
		else
		{
			echo "{success : false}";
		}
		
	}
	public function val_currentship()
	{
	$res = $this->session->userdata['user_id'];
	var_dump($res);
	}
	
	public function getNameUser()
	{
		$kdunit = $this->session->userdata['user_id']['kdunit'];
		$ex = explode(',',$kdunit);
		
		$ada = 0;
		$kd_unit = '';
		for($i=0;$i<count($ex);$i++){
			# TAMPILKAN KD_UNIT AKTIF KHUSUS LAB => 4, RAD => 5
			if(substr($ex[$i],1,1) == 4 || substr($ex[$i],1,1) == 5){
				$ada += 1; 
				$kd_unit = $ex[$i];
			} 
		}
		
		if ($kd_unit=='')
		{
			$unit='';
		}
		else
		{
			$unit=$this->db->query("select nama_unit from unit where kd_unit in (".$kd_unit.") ")->row()->nama_unit;
		}
		
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		if ($unitfar=='')
		{
			$nm_unit_far='';
		}
		else
		{
			$nm_unit_far=$this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$unitfar."' ")->row()->nm_unit_far;
		}
			
		$kdmilik = $this->session->userdata['user_id']['aptkdmilik'];
		if ($kdmilik=='')
		{
			$milik='';
		}
		else
		{
			$milik = $this->db->query("select milik from apt_milik where kd_milik='".$kdmilik."'");
		  if ($milik->num_rows() > 0) {
			$milik = $milik->row()->milik;
		  }else{
			$milik = "";
		  }
		}
		
		$res = $this->session->userdata['user_id']['username'];
		echo "{unitfar:'".$nm_unit_far."',user:'".$res."',milik:'".$milik."',unitaktif:'".$unit."'}";
		//echo $res;	
	}

        public function getKdUser()
        {
        $res = $this->session->userdata['user_id']['id'];
        echo $res;  
        }
    
	
	function getkdbagian()
	{
	//$query_kdbagian = $this->db->query("select bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Jalan'");
	$query_kdbagian = $this->db->query("SELECT bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Jalan'");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianrwj = $kdbag->kd_bagian;
			}
			return $kdbagianrwj;
	}
	
	function getkdbagianall($bagian)
	{
	$query_kdbagian = $this->db->query("select bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian.kd_bagian = $bagian");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianrwj = $kdbag->kd_bagian;
			}
			return $kdbagianrwj;
	}
	
	function getMaxkdbagian()
	{
	if(isset($_POST['command']))
	{
	//$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Jalan'");
	$query_maxkdbagian = $this->db->query("SELECT bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Jalan'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag)
			{
			$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
	}
	}
	
	   //function untuk mengambil nilai shift       
        function getcurrentshift()
       {   
       	// var_dump($this->session->userdata());
           /*if(isset($_POST['command']))
           {
             $kdbagianrwj = $this->getkdbagian();
             //$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
             $strQuery = "SELECT shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
                   //echo $kdbagian.":";
			//$query = $this->db->query($strQuery);
			$query = $this->db->query($strQuery);
           $res = $query->result();
            if ($query->num_rows() > 0)
               {
                foreach($res as $data)
                {
                  // $curentshift = $data->getcurrentshift;
				  $curentshift = $data->shift;

                }
               }
            echo $curentshift;
           }
           else
           {
           $kdbagianrwj = $this->getkdbagian();
          // $strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		  $strQuery = "SELECT shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
                   //echo $kdbagian.":";
           //$query = $this->db->query($strQuery);
		   $query = $this->db->query($strQuery);
           $res = $query->result();
            if ($query->num_rows() > 0)
               {
                foreach($res as $data)
                {
                   //$curentshift = $data->getcurrentshift;
				    $curentshift = $data->shift;

                }
               }
            return $curentshift;
           }
           */
          if(isset($_POST['command'])){
          	$kd_bagian = str_replace("'", "", $_POST['command']);
          	$this->db->select("*");
          	$this->db->where(array('kd_unit_far' => $kd_bagian));
          	$this->db->from("apt_unit");
          	$query = $this->db->get();
          	if($query->num_rows() > 0){
				$this->db->select("*");
				$this->db->where( array( 'kd_unit_far' => $kd_bagian ) );
				$this->db->from("apt_shift_rsj");
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
          			echo $query->row()->shift;
          		}else{
          			echo 0;
          		}
          	}else{
				$this->db->select("*");
				$this->db->where( array( 'kd_bagian' => $kd_bagian ) );
				$this->db->from("bagian_shift");
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
          			echo $query->row()->shift;
          		}else{
          			echo 0;
          		}
          	}
          }else{
          	echo "";
          }
       }
	   
	   /* function getcurrentshiftall()
       {   
           if(isset($_POST['command']))
           {
             $kdbagian = $this->db->query("select kd_bagian from zmodule where mod_id='".$_POST['mod_id']."'")->row()->kd_bagian;
			 if ($kdbagian=='')
			 {
				  $strQuery = "select getcurrentshift(0)";
			 }
			 else
			 {
				  $strQuery = "select getcurrentshift(".$kdbagian.")";
			 }
            
                   //echo $kdbagian.":";
           $query = $this->db->query($strQuery);
           $res = $query->result();
            if ($query->num_rows() > 0)
               {
                foreach($res as $data)
                {
                   $curentshift = $data->getcurrentshift;

                }
               }
            echo $curentshift;
           }
           else
           {
           $kdbagian = $this->db->query("select kd_bagian from zmodule where mod_id='".$_POST['mod_id']."'")->row()->kd_bagian;
           if ($kdbagian=='')
			 {
				  $strQuery = "select getcurrentshift(0)";
			 }
			 else
			 {
				  $strQuery = "select getcurrentshift(".$kdbagian.")";
			 }
            
                   //echo $kdbagian.":";
           $query = $this->db->query($strQuery);
           $res = $query->result();
            if ($query->num_rows() > 0)
               {
                foreach($res as $data)
                {
                   $curentshift = $data->getcurrentshift;

                }
               }
            return $curentshift;
           }
       } */
	  
	    function getcurrentshiftall()
		{   
			$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
			
           if(isset($_POST['command']))
           {
				if($kd_unit_far == ''){
					$kdbagian = $this->db->query("select kd_bagian from zmodule where mod_id='".$_POST['mod_id']."'")->row()->kd_bagian;
					if ($kdbagian=='')
					{
					  $strQuery = "select getcurrentshift(0)";
					}
					else
					{
					  $strQuery = "select getcurrentshift(".$kdbagian.")";
					}

					   //echo $kdbagian.":";
					$query = $this->db->query($strQuery);
					$res = $query->result();
					if ($query->num_rows() > 0)
					{
						foreach($res as $data)
						{
						   $curentshift = $data->getcurrentshift;

						}
					}
					
				} else{
					$res = $this->db->query("select * from apt_unit where kd_unit_far='".$kd_unit_far."'");
					if(count($res->result()) > 0){
						$curentshift = $this->db->query("select shift from apt_shift_rsj where kd_unit_far='".$kd_unit_far."'");
						if(count($curentshift->result()) > 0){
							$curentshift = $curentshift->row()->shift;
						} else{
							$curentshift = "null";
						}
					} else{
						$kdbagian = $this->db->query("select kd_bagian from zmodule where mod_id='".$_POST['mod_id']."'")->row()->kd_bagian;
						if ($kdbagian=='')
						{
						  $strQuery = "select getcurrentshift(0)";
						}
						else
						{
						  $strQuery = "select getcurrentshift(".$kdbagian.")";
						}

						   //echo $kdbagian.":";
						$query = $this->db->query($strQuery);
						$res = $query->result();
						if ($query->num_rows() > 0)
						{
							foreach($res as $data)
							{
							   $curentshift = $data->getcurrentshift;

							}
						}
					}
				}
				echo $curentshift;
           }
           else
           {
           $kdbagian = $this->db->query("select kd_bagian from zmodule where mod_id='".$_POST['mod_id']."'")->row()->kd_bagian;
           if ($kdbagian=='')
			 {
				  $strQuery = "select getcurrentshift(0)";
			 }
			 else
			 {
				  $strQuery = "select getcurrentshift(".$kdbagian.")";
			 }
            
                   //echo $kdbagian.":";
           $query = $this->db->query($strQuery);
           $res = $query->result();
            if ($query->num_rows() > 0)
               {
                foreach($res as $data)
                {
                   $curentshift = $data->getcurrentshift;

                }
               }
            return $curentshift;
           }
		}
    //----------------END------------------------
	
	   function defaultset_pendaftaran()
       {   
          $defaultasu=$this->db->query("select setting from sys_setting where key_data= 'sys_defaut_cus_kel_pasien'")->row()->setting;
			echo $defaultasu;

	  }
	  
	  function defaultset_cus_pendaftaran()
       {   
          $defaultcus=$this->db->query("select setting from sys_setting where key_data= 'sys_defaut_cus'")->row()->setting;
			echo $defaultcus;

	  }
	  function defaultset_kd_cus_pendaftaran()
       {   
          $defaultkdcus=$this->db->query("select setting from sys_setting where key_data= 'sys_defaut_kd_cus'")->row()->setting;
			echo $defaultkdcus;

	  }
	
    function Logoff()
     {
           $logged = $this->session->userdata('user_id');
           if ($logged)
           {
               if ($logged['logtime']===0)
               {
                   $this->ErrLoginMsg="Anda terlalu lama tidak melakukan aktifitas , silakan login ulang untuk melanjutkan.";
               }
           }
          $this->session->unset_userdata('user_id');

          $this->load->view('main/index',$data=array('controller'=>$this, 'data_rs'=>$this->db_rs));

     }
    // checking user logged user by registered session
    function _is_logged_in()
    {
        //return TRUE;
        
        $logged = $this->session->userdata('user_id');
        if ($logged)
        {
            /* 
            $elapsed=time()-$logged['logtime'];
            if ($elapsed<=900000000000000)
            {
                $logged['logtime']=time(); */
                return true;
           /*  }
       
            else
            {

                $logged['logtime']=0;
                return false;
            } */
        }
        else
        {
           //$logged['logtime']=0;
            return FALSE;
        }
    }


    private function CRUDLoadController($target, $Params, $method)
    {
        //format text pengenal;sintak controller(module/controller/method)

        $loadFile=file_get_contents( base_url()."ui/jslist/DataAccessList.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);
		
        foreach ($tambahdelimiter as $rowDA)
        {
            if (trim($rowDA)!='')
            {
                list($pengenal, $sintak)= explode(';',$rowDA);
                if (trim($target)==trim($pengenal))
                {
                    // load controller
                    list($mod, $ctrl)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    $this->$ctrl->$method($Params);
                }
            }
        }
				    	
    }

	
   public function KonsultasiPenataJasa()
   {

	   $kdTransaksi = $this->GetIdTransaksi();
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $kdKasir = "01";
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   
	   $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."','".$kdCostumer."','".$kdKasir."','".$kdTransaksi."')");
	   $res = $query->result();
	   if($res)
	   {
		   echo '{success: true}';
	   }
	   else
	   {
		   echo '{success: false}';
	   }
   } 
   
   
    private function GetIdTransaksi()
    {
        $strNomor="0".str_pad("00",2,'0',STR_PAD_LEFT);
        $retVal=$strNomor."0001";

        $this->load->model('general/tb_transaksi');
        $this->tb_transaksi->db->where("substring(no_transaksi,1,3) = '".$strNomor."'", null, false);
        $res = $this->tb_transaksi->GetRowList( 0, 1, "DESC", "no_transaksi",  "");

        if ($res[1]>0)
        {
            $nm = substr($res[0][0]->NO_TRANSAKSI, -4);
            $nomor = (int) $nm +1;
            $retVal=$strNomor.str_pad($nomor,4,"00000",STR_PAD_LEFT);
    }
     return $retVal;
    }
	
	public function insertloop()
	{
		$queryy = $this->db->query("
		insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
		select t.kd_kasir,
		t.no_transaksi,
		dt.urut,
		dt.tgl_transaksi,
		tc.kd_component,
		tc.tarif
		from tarif_component tc 
		inner join detail_transaksi dt on 
		tc.kd_tarif = dt.kd_tarif
		inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
		and t.kd_unit = tc.kd_unit
		where tc.kd_produk= '112' and tc.kd_unit='202' and dt.tgl_transaksi in ('2015-02-09') and tc.kd_tarif='TU' 
		and  tc.tgl_berlaku in ('2014-03-01') 
		and t.kd_kasir = '01' and t.no_transaksi = '0000007'");
		return $queryy;
	}
	
	public function masukcobaan()
	{
		//$this->load->model('test','',TRUE);
			$query = $this->db->query("
		insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
		select t.kd_kasir,
		t.no_transaksi,
		dt.urut,
		dt.tgl_transaksi,
		tc.kd_component,
		tc.tarif
		from tarif_component tc 
		inner join detail_transaksi dt on 
		tc.kd_tarif = dt.kd_tarif
		inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
		and t.kd_unit = tc.kd_unit
		where tc.kd_produk= '112' and tc.kd_unit='202' and dt.tgl_transaksi in ('2015-02-09') and tc.kd_tarif='TU' 
		and  tc.tgl_berlaku in ('2014-03-01') 
		and t.kd_kasir = '01' and t.no_transaksi = '0000007'");
		if($query)
		{
			echo "sukses";
		}
		else
		{
			echo "gagal";
		}
	}
	
    public function CreateDataObj()
    {
            if ($this->IsAjaxRequest())
            {
                $arrPost=array();

                foreach($_POST as $key=>$value)
                {
                    $arrPost[$key]= $value;
                }

                if (count($arrPost)>0)
                {
                    
                    $target = $arrPost['Table'];

                    if ($target!="")
                    {
                        
                        $this->CRUDLoadController($target,$arrPost,'save');

                    }  else  $this->load->view('main/index');

                }   else $this->load->view('main/index');

            } else $this->load->view('main/index');
//       }
//       else
//       {
//           $this->Logoff();
//
//        }
    }

    public function ReadDataUmur()
    {
            if ($this->IsAjaxRequest())
            {
                $arrPost=array();

                foreach($_POST as $key=>$value)
                {
                    $arrPost[$key]= $value;
                }

                if (count($arrPost)>0)
                {

                    $target = $arrPost['Table'];

                    if ($target!="")
                    {

                        $this->CRUDLoadController($target,$arrPost,'umur');

                    }  else  $this->load->view('main/index');

                }   else $this->load->view('main/index');

            } else $this->load->view('main/index');
//       }
//       else
//       {
//           $this->Logoff();
//
//        }
    }

    public function DeleteDataObj()
    {
//       if  ($this->_is_logged_in()===false)
//       {
           
       
        if ($this->IsAjaxRequest())
        {

            $arrPost=array();

            foreach($_POST as $key=>$value)
            {
                $arrPost[$key]= $value;
            }

            if (count($arrPost)>0)
            {
                $target = $arrPost['Table'];

                if ($target!="")
                {

                    $this->CRUDLoadController($target,$arrPost,'delete');

                } else $this->load->view('main/index');

            } else $this->load->view('main/index');

        } else $this->load->view('main/index');
//        }
//       else
//       {$this->Logoff();}
    }
	    
    public function ReadDataObj()
    {
 
        if ($this->IsAjaxRequest())
        {
            $Skip = isset($_REQUEST['Skip']) ? $_REQUEST['Skip'] : 0;
            $Take = isset($_REQUEST['Take']) ? $_REQUEST['Take'] : 1000;
            $Sort = isset($_REQUEST['Sort']) ? $_REQUEST['Sort'] : "";
            $Sortdir = isset($_REQUEST['Sortdir']) ? $_REQUEST['Sortdir'] : "ASC";
            $target = isset($_REQUEST['target']) ? $_REQUEST['target'] : "";
            $param = isset($_REQUEST['param']) ? $_REQUEST['param'] : "";

            $Params = array((int)$Skip,(int)$Take,$Sort,$Sortdir,$param);
			
            if ($target=="")
            {
                    $this->load->view('main/index');
					
            } else $this->CRUDLoadController($target,$Params,'read');

        } else $this->load->view('main/index');

    }

    public function ReadData()
    {
            if ($this->IsAjaxRequest())
            {
                $arrPost=array();

                foreach($_POST as $key=>$value)
                {
                    $arrPost[$key]= $value;
                }

                if (count($arrPost)>0)
                {

                    $target = $arrPost['Table'];

                    if ($target!="")
                    {

                        $this->CRUDLoadController($target,$arrPost,'viewfunction');

                    }  else  $this->load->view('main/index');

                }   else $this->load->view('main/index');

            } else $this->load->view('main/index');
    }

    private function IsAjaxRequest()
    {
        /* AJAX check  */
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
    	} else return false;

    }
    
    public function ExecProc()
    {

            
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/ProcessList.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);

        /* echo '<script type="text/javascript">alert("ada");</script>'; */

         foreach ($tambahdelimiter as $rowProc)
        {

            if (trim($rowProc)!='')
            {

                list($pengenal, $sintak)= explode(';',$rowProc);

                if (trim($ModuleID)==trim($pengenal))
                {
                    //load controller
                    //return echo json lsg dari controller

                    list($mod, $ctrl, $method)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    $this->$ctrl->$method($Params);

                }
            }
        }

    }
    
    public function ExecReport()
    {
    	//format txt proses yang akan digunakan
       
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/ReportList.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);

        /* echo '<script type="text/javascript">alert("ada");</script>'; */

        foreach ($tambahdelimiter as $rowProc)
        {

            if (trim($rowProc)!='')
            {

                list($pengenal, $sintak)= explode(';',$rowProc);

                if (trim($ModuleID)==trim($pengenal))
                {
                    //load controller
                    //return echo json lsg dari controller

                    list($mod, $ctrl)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    echo $this->$ctrl->CreateReport('', $UserID,$Params,0,0,'','');

                }
            }
        }
//        }
//               else
//       {$this->Logoff();}
    }
    public function getModule(){
		if  ($this->_is_logged_in()){
			$UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
			$ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
			$loadFile=file_get_contents( base_url()."ui/jslist/".$ModuleID."jslist.php");
			$tambahdelimiter= explode(chr(13).chr(10),$loadFile);
			$arrListJS=array();
			foreach ($tambahdelimiter as $rowJS){
				$url=str_replace( "./",base_url() , $rowJS);
				if(file_exists($rowJS)){
					$timeValue=date('U', filemtime($rowJS));
					$arrListJS[]= $url.'?v='.$timeValue;
				}
			}
			$this->load->model('main/vi_gettrustee');
			$query = $this->vi_gettrustee->readforGetModule($UserID, $ModuleID);
			if ($query->num_rows() > 0){
				$row=$query->row_array();
				$id= $row['mod_id'];
				$title = $row['mod_name'];
				$url= ''; 
			}
			echo '{success:true, title:"'.trim($title).'", id:"'.$id.'", url:"'.$url.'", htm:'.json_encode($arrListJS).'}';
		}else{
            echo '{success : false, expire:true}';
		}
    }
    public function getReport()
    {
       if  ($this->_is_logged_in())
       {

       
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/".$ModuleID."jslist.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);
        $arrListJS='';
        foreach ($tambahdelimiter as $rowJS)
        {
            if (strlen($rowJS)>0)
            {
                $arrListJS[]=str_replace( "./",base_url() , $rowJS); //$rowJS;
            }
        }

    	/*  sementara langsung tanpa cek trustee 

         *
         */
        $this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetModule($UserID, $ModuleID);

        if ($query->num_rows() > 0)
        {
            //$row = $query->row();
            $row=$query->row_array();
            $id= $row['mod_id'];
            $title = $row['mod_name'];
            $url= ''; //$row['Mod_URL'];
        }


        $haveDialog="false";

        if (is_array($arrListJS)!==false)
        {$haveDialog="true";}

        echo '{success:true, title:"'.$title.'", id:"'.$id.'", url:"'.$url.'",
            htm:'.json_encode($arrListJS).', havedialog: '.$haveDialog.'}';
        }
        else
       {  echo '{success : false, expire:true}';}
    }

    Public Function Upload()
    {
        //$direktori = isset($_REQUEST['direktori']) ? $_REQUEST['direktori'] : "";
        //$file = isset($_REQUEST['file']) ? $_REQUEST['file'] : "";
        $rEFileTypes = "/^\.(png|gif|tiff|bmp|jpg|jpeg|xls|doc|txt|pdf|ppt|txt|pptx|docx|exe){1}$/i";

        $isFile = is_uploaded_file($_FILES['file']['tmp_name']);

        if ($isFile)
        {

            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($_FILES['file']['name']));

            $lokasi_file = $_FILES['file']['tmp_name'];
            //$nama_file   = $_FILES['file']['name'];
            //$ukuran_file = $_FILES['file']['size'];
            //$type_file = $_FILES['file']['type'];

            $uploads_dir =base_url().'Img Asset/'.$safe_filename;

            //($type_file == $type_allow and strpos($nama_file, " ") == false)

            if (preg_match($rEFileTypes, strrchr($safe_filename, '.')))
            {
                $result = move_uploaded_file($lokasi_file, $uploads_dir);

                if ($result==1)
                {

                    echo '{success:true, result:"Upload Sukses", namafile:"'.$safe_filename.'"}';

                } else echo '{success:false, error:"Upload Gagal"}';

            } else echo '{success:false, error:"Type File Salah"}';
            
        } else echo '{success:false, error:"Upload Gagal"}';

    }
	
    Public Function UploadRef()
    {

        if ($isFile)
        {
            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($_FILES['fileAsetRef']['name']));

            $lokasi_file = $_FILES['fileAsetRef']['tmp_name'];
            //$nama_file   = $_FILES['file']['name'];
            //$ukuran_file = $_FILES['file']['size'];
            //$type_file = $_FILES['fileAsetRef']['type'];

            $uploads_dir =base_url().'Doc Asset/'.$safe_filename;

            $extension= end(explode(".", $file['name']));

            $file_type =  strrchr($safe_filename, '.');

            if (preg_match($rEFileTypes, $file_type))
            {
                $result = move_uploaded_file($lokasi_file, $uploads_dir);

                if ($result==1)
                {

                    echo '{success:true, result: "Upload Sukses", namafile: "'.$safe_filename.'", namaext: "'.$file_type.'"}';

                } else echo '{success:false, error:"Upload Gagal"}';

            } else echo '{success:false, error:"Type File Salah"}';
            
        } else echo '{success:false, error:"Upload Gagal"}';
        
    }
	
    Public function getLanguage()
    {
		
        $Kd_User=isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";

        //$Kd_User='0';
        $strLanguageID="1";
        $arrLanguage=array();
		
    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();

        if ($query->num_rows() > 0)
        {
            //$strLanguageID=$row->LANGUAGE_ID;
            $strLanguageID=$row->language_id;
        } else $strLanguageID="1";

        if ($strLanguageID==null or $strLanguageID=="" )
                $strLanguageID='1';

        $this->load->model('main/vilanguage');
        $query = $this->vilanguage->read($strLanguageID);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $rows)
            {
                //$arrLanguage[]=$rows;
                $arrLanguage[]=$this->FillRow($rows);
            }

//Dim result = New With {.success = True, .total = mListLanguange.Count, .ListLanguange = mListLanguange}
            echo '{success:true, total:'.count($arrLanguage).', ListLanguage:'.json_encode($arrLanguage).'}';

        }
        else
        {
            echo '{success:false}';
        };
				
    }

    private function FillRow($rec)
    {
        $row=new Rowgetlanguage;
        $row->LIST_KEY=$rec["list_key"];
        $row->LABEL=$rec["label"];

        return $row;
    }
	    
    public function getTrustee()
    {
       
    	$Kd_User=isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
		//session_start();
    	//$Kd_User=$_SESSION['nci'];
		//echo $Kd_User;
        //$Kd_User='0';
		
        $this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetTrustee1($Kd_User, $_REQUEST['mod_group']);
		
        //$idx=0;
        $cGroup="";
        $amList = array();
        $agroup=array();
        $strModName="";

        //foreach($query->result() as $rows){
		//echo json_encode($query->result_array());
        foreach($query->result_array() as $rows) {
			
// yang Asli dari Pa Ali $strModName=$this->getModName($Kd_User, $rows['mod_id']);          
            $strModName=$this->getModName($Kd_User, $rows['mod_name']); //Editan Hidayat Untuk Merubah Tampilan Menu menjadi nama bukan ID
	
            if ($strModName == "")
// yang Asli dari Pa Ali $strModName = $rows['mod_id'];//$rows->Mod_ID;
                $strModName = $rows['mod_name'];//Editan Hidayat Untuk Merubah Tampilan Menu menjadi nama bukan ID
			
                $strGroupName=$this->getModName($Kd_User, $rows['mod_key']);
				
                if ($strGroupName == "")
                    $strGroupName = $rows['mod_group']; //$rows->Mod_Group;
                
                $strTmpGroup = $rows['mod_group']; //$rows->Mod_Group;

				$rows['mod_group']=$strGroupName;
				$rows['mod_url'] =str_replace("[mod_id]", $rows['mod_id'], $rows['mod_url']);
				$rows['mod_url'] =str_replace("[image]",base_url()."ui/".$rows['mod_imgurl'],$rows['mod_url']);
				$rows['mod_url'] =str_replace("[Mod_Name]",$strModName ,$rows['mod_url']);
			
                if (strlen(strstr($cGroup,$strTmpGroup))==0)
                {
                    $htm="";
                    $this->load->model('main/vi_gettrustee');
                    $query1 = $this->vi_gettrustee->readforGetTrustee2($Kd_User,$strTmpGroup);

                    foreach($query1->result_array() as $rows1) {
                        $strModName=$this->getModName($Kd_User,$rows1['mod_id']);

                        if ($strModName=="")
                                $strModName=$rows1['mod_name']; //$rows1->Mod_Name;
						
                	$rows1['mod_url'] = str_replace("[mod_id]", $rows1['mod_id'],$rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[image]", base_url()."ui/".$rows1['mod_imgurl'],$rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[Mod_Name]", $strModName, $rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[Mod_Desc]", $rows1['mod_desc'],$rows1['mod_url']);
			$htm .= $rows1['mod_url'].chr(13).chr(10);
					
		}
				
                $agroup[]=array("mod_group"=>$strGroupName, "htm"=>$htm);
            }
			
            $amList[]=$rows;
	}


        $dd ='{success:true, total:'.count($amList).', ListTrustee:'.json_encode($amList).', ListReport:'.json_encode($agroup).'}';
        echo $dd;
    }     
            
    private function getModName($Kd_User, $strModID)
    {
   		
   		// ambil strIDLang
    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();
        //$strIDLang=$row->LANGUAGE_ID;
        $strIDLang=$row->language_id;

        if ($strIDLang==null)
                $strIDLang=1;

        $this->load->model('main/getmodname');
        $query = $this->getmodname->read($strModID,$strIDLang);

        // isi return function

        $returnVal="";

        if ($query->num_rows() > 0)
        {
            $row= $query->row();
            //$returnVal=$row->LABEL;
            $returnVal=$row->label;
        };

        return $returnVal;
																		   	 	
    }
	
	public function GetUmur() {
    /*
      UPDATE TANGGAL LAHIR
      OLEH  : HADAD AL GOJALI
    */

		$tmptgllahirCek = new DateTime(str_replace('/','-', $_POST['TanggalLahir']));
		// echo 'awd';
		if (substr($_POST['TanggalLahir'], 0,4) == '1900' || substr($_POST['TanggalLahir'], 6,5) == '1900' || $_POST['TanggalLahir'] == "01/01/1900") {
			$tmptgllahir = '01-01-1900';
			echo '117 year 0 month 0 day';
		}else{
			$tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $_POST['TanggalLahir'])));
			$now=new DateTime();
			$tglLahir=new DateTime($tmptgllahir);
			$umur=$this->getAge($now,$tglLahir);
			echo $umur['YEAR'].' year '.$umur['MONTH'].' month '.$umur['DAY'].' day';
		}
		//echo json_encode($this->getAge($now,$tglLahir));
		//echo json_encode($res );
         // if ($query->num_rows() > 0)
         // {
             // foreach($res as $data)
             // {
                // $umur = $data->age;
             // }
         // echo $umur;
          // }
         // return $umur;
    }
    
	public function GetUmur_LAB() {
		
    

	$tmptgllahirCek = new DateTime(str_replace('/','-', $_POST['TanggalLahir']));
	//echo  $_POST['TanggalLahir'];
    if (substr($_POST['TanggalLahir'], 0,4) == '1900' || $_POST['TanggalLahir'] == "01/01/1900") {
      $tmptgllahir = '01-01-1900';
      echo '117 year 0 month 0 day';
    }else{
      $tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $_POST['TanggalLahir'])));
      //$tmptgllahir = date_format(date_create($_POST['TanggalLahir']), "d-m-Y");
      $now=new DateTime();
      $tglLahir=new DateTime($tmptgllahir);
      $umur=$this->getAge($now,$tglLahir);
      echo $umur['YEAR'].' year '.$umur['MONTH'].' month '.$umur['DAY'].' day';
    }
		//echo json_encode($this->getAge($now,$tglLahir));
		//echo json_encode($res );
         // if ($query->num_rows() > 0)
         // {
             // foreach($res as $data)
             // {
                // $umur = $data->age;
             // }
         // echo $umur;
          // }
         // return $umur;
    }
    
	public function getAge($tgl1,$tgl2){
		$jumHari=(abs(strtotime($tgl1->format('Y-m-d'))-strtotime($tgl2->format('Y-m-d')))/(60*60*24));
		$ret=array();
		$ret['YEAR']=floor($jumHari/365);
		$sisa=floor($jumHari-($ret['YEAR']*365));
		$ret['MONTH']=floor($sisa/30);
		$sisa=floor($sisa-($ret['MONTH']*30));
		$ret['DAY']=$sisa;
		
		if($ret['YEAR']==0  && $ret['MONTH']==0 && $ret['DAY']==0){
			$ret['DAY']=1;
		}
		return $ret;
	}
  
	public function GetTglLahir() {
		if ($_POST['tmptahun']=='')
		{
			$tmptahun=0;
		}
		else
		{
			$tmptahun=$_POST['tmptahun'];
		}
		if ($_POST['tmpbulan']=='')
		{
			$tmpbulan=0;
		}
		else
		{
			$tmpbulan=$_POST['tmpbulan'];
		}
		if ($_POST['tmphari']=='')
		{
			$tmphari=0;
		}
		else
		{
			$tmphari=$_POST['tmphari'];
		}
        $strQuery = "select to_char(to_date(to_char(now(), 'YYYY-MM-DD'), 'YYYY-MM-DD') - interval '".$tmptahun." year' - interval '".$tmpbulan." month' - interval '".$tmphari." day' , 'DD/MM/YYYY') as tgl_lahir";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
             foreach($res as $data)
             {
                $tglahir = $data->tgl_lahir;
             }
         echo $tglahir;
}
         return $tglahir;
    }
	
	public function posting()
{
	$noTrans = $_POST['_notransaksi'];
	$query = $this->db->query("select postingtransaksi('".$noTrans."')");

	 //--- Query SQL Server ----//
	 /*	$DB = $this->load->database('otherdb',true);
		$DB->query("UPDATE transaksi set posting_transaksi = TRUE where no_transaksi = '$noTrans'");
		$DB->close();*/
	  //--- Akhir Query SQL Server---//
	if($query)
	{
	echo "{success:true}";	
	}
	else
	{
	echo "{success:false}";		
	}
	
}

//---get shift untuk IGD----//
function getkdbagianIGD()
	{
		$kdbagianigd='';
	//$query_kdbagian = $this->db->query("select bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat' ");
	$query_kdbagian = $this->db->query("SELECT bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat' ");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianigd = $kdbag->kd_bagian;
			}
			return $kdbagianigd;
	}
	
	function getMaxkdbagianIGD()
	{
	if(isset($_POST['command']))
	{
		$maxkdbagianigd='';
	$query_maxkdbagian = $this->db->query("SELECT bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat'");
	//$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag)
			{
			$maxkdbagianigd = $maxkdbag->numshift;
			}
			echo $maxkdbagianigd;
	}
	}
	
	   //function untuk mengambil nilai shift         
    function getcurrentshiftIGD()
    {   
		if(isset($_POST['command']))
		{
			$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
			if ($kdUnitFar == ''){
				$kdbagianrwj = $this->getkdbagianIGD();
				//$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
				$strQuery = "SELECT shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
				//echo $kdbagian.":";
				//$query = $this->db->query($strQuery);
				$query = $this->db->query($strQuery);
			}else{
				$query = $this->db->query("SELECT shift from apt_shift_rsj where kd_unit_far='".$kdUnitFar."'");
			}
			$res = $query->result();
			if ($query->num_rows() > 0)
			{
			 foreach($res as $data)
			 {
				$curentshift = $data->shift;
				//$curentshift = $data->getcurrentshift;
				
			 }
			}
			 echo $curentshift;
		}
		else
		{
			$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
			if ($kdUnitFar == ''){
				$kdbagianrwj = $this->getkdbagianIGD();
				//$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
				$strQuery = "SELECT shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
				//echo $kdbagian.":";
				//$query = $this->db->query($strQuery);
				$query = $this->db->query($strQuery);
			}else{
				$query = $this->db->query("SELECT shift from apt_shift_rsj where kd_unit_far='".$kdUnitFar."'");
			}
			$res = $query->result();
			 if ($query->num_rows() > 0)
				{
				 foreach($res as $data)
				 {
					//$curentshift = $data->getcurrentshift;
					$curentshift = $data->shift;
					
				 }
				}
			 return $curentshift;
		}
    }
    //----------------END------------------------


//---get shift untuk RWI----//
function getkdbagianRWI()
	{
	$query_kdbagian = $this->db->query("SELECT bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Inap'");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianrwj = $kdbag->kd_bagian;
			}
			return $kdbagianrwj;
	}

	public function GetPasienTranfer() 
	{
		/* 	
			Update	: M
			Tgl		: 07-02-2017
			Ket		: Ganti koneksi SQL 
		*/
		
        $kdtr = $_POST['notr'];
		$tgl_masuk = $this->input->post('tgl_masuk');
		$criteria_tgl_masuk = "";
		if ($tgl_masuk != null) {
			$criteria_tgl_masuk = " AND kunjungan.tgl_masuk = '".$tgl_masuk."'";
		}
        // $strQuery = "SELECT Unit.Nama_Unit as namaunit, Pasien.Nama as namapasien,
					// unit_kamar.Nama_Unit||'-'||kin.nama_kamar as namaunitkamar,
					// nginap.kd_spesial,
					// nginap.kd_unit_kamar as kodeunitkamar,
					// nginap.no_kamar,
					// Pasien.ALamat, kunjungan.Kd_pasien as kdpas,
					// Transaksi.Tgl_Transaksi as tgtr, Dokter.Nama as Nama_Dokter, 
					// Kunjungan.*, Transaksi.NO_TRANSAKSI as notransaksi, TRANSAKSI.Kd_Kasir as kdkas, 
					// TRANSAKSI.co_status, Unit.kd_kelas, Dokter.Kd_Dokter,kunjungan.kd_unit as kdunit, Transaksi.ORDERLIST, knt.Jenis_Cust
					// From ((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit)
				   // INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien) 
				   // INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter) 
				   // Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer) 
				   // INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit and 
				   // transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.Urut_masuk=kunjungan.Urut_masuk
				   // inner join  nginap  On nginap.kd_Pasien = kunjungan.kd_pasien and nginap.kd_unit =kunjungan.Kd_Unit and nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.Urut_masuk=kunjungan.Urut_masuk
				   // and nginap.akhir=true
				   // inner join unit unit_kamar on unit_kamar.kd_unit=nginap.kd_unit_kamar
				   // inner join kamar_induk kin on kin.no_kamar=nginap.no_kamar where Transaksi.Co_status ='false' and Transaksi.tgl_co is null and unit.kd_bagian = 1
									// and kunjungan.kd_pasien='".$kdtr."' order by Transaksi.Tgl_Transaksi desc limit 1 ";
        // $query = $this->db->query($strQuery);
        $strQuery = "SELECT Unit.Nama_Unit as namaunit, Pasien.Nama as namapasien,
					unit_kamar.Nama_Unit||'-'||kin.nama_kamar as namaunitkamar,
					nginap.kd_spesial,
					nginap.kd_unit_kamar as kodeunitkamar,
					nginap.no_kamar,
					Pasien.ALamat, kunjungan.Kd_pasien as kdpas, 
					kunjungan.kd_customer as kd_customer,
					Transaksi.Tgl_Transaksi as tgtr, 
					Dokter.Nama as Nama_Dokter, 
					Kunjungan.*, Transaksi.NO_TRANSAKSI as notransaksi, TRANSAKSI.Kd_Kasir as kdkas, 
					TRANSAKSI.co_status, Unit.kd_kelas, Dokter.Kd_Dokter,kunjungan.kd_unit as kdunit, Transaksi.ORDERLIST, knt.Jenis_Cust 
					From ((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit)
				   INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien) 
				   INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter) 
				   Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer) 
				   INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit and 
				   transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.Urut_masuk=kunjungan.Urut_masuk
				   inner join  nginap  On nginap.kd_Pasien = kunjungan.kd_pasien and nginap.kd_unit =kunjungan.Kd_Unit and nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.Urut_masuk=kunjungan.Urut_masuk
				   and nginap.akhir='true'
				   inner join unit unit_kamar on unit_kamar.kd_unit=nginap.kd_unit_kamar
				   inner join kamar_induk kin on kin.no_kamar=nginap.no_kamar where Transaksi.Co_status = 'false' and Transaksi.tgl_co is null and unit.kd_bagian = 1
									and kunjungan.kd_pasien='".$kdtr."' $criteria_tgl_masuk order by Transaksi.Tgl_Transaksi desc limit 1";
		// $query =_QMS_Query($strQuery);
		$query =$this->db->query($strQuery);
        $res = $query->result();
		$string = "";
        if ($query->num_rows() > 0)
        {
		 
			foreach($res as $data)
			{
                $string .= "<>".$data->namaunit ;
				$string .= "<>".$data->namapasien;
				$string .= "<>".$data->notransaksi;
				// $string .= "<>".$data->tgtr;
				$string .= "<>".date('Y-m-d',strtotime($data->tgtr));
				$string .= "<>".$data->kdpas;
				$string .= "<>".$data->kdunit;
				$string .= "<>".$data->kdkas;
				$string .= "<>".$data->kodeunitkamar;
				$string .= "<>".$data->namaunitkamar;
				$string .= "<>".$data->kd_spesial;
				$string .= "<>".$data->no_kamar;
				$string .= "<>".$data->kd_customer;
				$string .= "<>".$this->db->query("SELECT getkdtarifcus('".$data->kd_customer."') ")->row()->getkdtarifcus;
				
			}
            echo $string;
		}	
        return $string;
    }
	
	
	
	public function getsistemsettingdefault() 
{
        $command2 = $_POST['command'];
        $strQuery = " select setting from sys_setting  where key_data='$command2' ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string = $data->setting;
				
             }
            echo $string;
		 }	
         return $string;
    }

	public function GetPasienTranferLab_rad() 
	{
		$kdtr    = $_POST['notr'];
		$kdkasir = $_POST['kdkasir'];
		$strQuery = "SELECT 
		Unit.Nama_Unit as namaunit, 
		Pasien.Nama as namapasien, 
		Pasien.ALamat, 
		kunjungan.Kd_pasien as kdpas, 
		Transaksi.Tgl_Transaksi as tgtr, 
		Dokter.Nama as Nama_Dokter, 
		Kunjungan.*,
		Transaksi.NO_TRANSAKSI as notransaksi, 
		TRANSAKSI.Kd_Kasir as kdkas, 
		TRANSAKSI.co_status, 
		Unit.kd_kelas, 
		Dokter.Kd_Dokter, 
		kunjungan.kd_unit as kdunit,
		unit_kamar.Nama_Unit as nama_unit_kamar,
		unit_kamar.Nama_Unit||'-'||kin.nama_kamar as namaunitkamar,
		ngin.kd_spesial,
		ngin.kd_unit_kamar as kodeunitkamar,
		ngin.no_kamar,
		Transaksi.ORDERLIST, 
		knt.Jenis_Cust
				From ((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit)
						INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien) 
						INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter)
						Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer)
					INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit 
						and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.Urut_masuk=kunjungan.Urut_masuk 
					left join nginap ngin on  ngin.kd_Pasien = kunjungan.kd_pasien and ngin.kd_unit =kunjungan.Kd_Unit and ngin.tgl_masuk=kunjungan.tgl_masuk and ngin.Urut_masuk=kunjungan.Urut_masuk and akhir=true
					left join unit unit_kamar on unit_kamar.kd_unit=ngin.kd_unit_kamar
					left join kamar_induk kin on kin.no_kamar=ngin.no_kamar
				where Transaksi.Co_status ='false' 
					and Transaksi.kd_kasir = '$kdkasir' 
					and Transaksi.no_transaksi='$kdtr' 
					--and Transaksi.lunas='false'
				order by Transaksi.Tgl_Transaksi desc limit 1 ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
		
		$string = "";
		if ($query->num_rows() > 0)
		{
			//$string = "";
			foreach($res as $data)
			{
				if (strlen($data->nama_unit_kamar) > 0) {
                	$string .= "<>".$data->nama_unit_kamar;
				}else{
                	$string .= "<>".$data->namaunit;
				}
				$string .= "<>".$data->namapasien;
				$string .= "<>".$data->notransaksi;
				$string .= "<>".$data->tgtr;
				$string .= "<>".$data->kdpas;
				$string .= "<>".$data->kdunit;
				$string .= "<>".$data->kdkas;
				$string .= "<>".$data->kodeunitkamar;
				if ($data->namaunitkamar==''){
					$string .= "<>".$data->namaunit;
				}else{
					$string .= "<>".$data->namaunitkamar;
				}
				$string .= "<>".$data->kd_spesial;
				$string .= "<>".$data->no_kamar;
            }
            echo $string;
		}	
		return $string;
    }
	
	
	public function GettotalTranfer() {
        $kdtr = $_POST['notr'];
		$kdks = $_POST['kd_kasir'];
		 if ($_POST['kd_kasir']=='igd')
		{
			$kdks  = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		}else if ($_POST['kd_kasir']=='rwj')
		{
			$kdks  = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		}else if ($_POST['kd_kasir']=='rwi')
		{
			$kdks  = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		} 
		
		
        $strQuery = "SELECT sum(harga*qty) as total from detail_transaksi where no_transaksi='".$kdtr."' and kd_kasir='".$kdks."'";
        $query = $this->db->query($strQuery);
        $res = $query->result();
		//echo json_encode($res);
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string = $data->total ;
				
             }
         echo $string;
		 }	
         return $string;
    }
	
	
	function getMaxkdbagianRWI()
	{
	if(isset($_POST['command']))
	{
	$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Inap'");
	// $query_maxkdbagian = _QMS_Query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Inap'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag)
			{
			$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
	}
	}
	
	   //function untuk mengambil nilai shift   

       	   
     function getcurrentshiftRWI()
    {   
	if(isset($_POST['command']))
	{
	  $kdbagianrwj = $this->getkdbagianRWI();
	  //$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
	  $strQuery = "Select shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
		// $query = _QMS_Query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                //$curentshift = $data->getcurrentshift;
				$curentshift = $data->shift;
				
             }
            }
         echo $curentshift;
	}
	else
	{
	$kdbagianrwj = $this->getkdbagianRWI();
	//$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
	$strQuery = "Select shift From bagian_shift Where kd_bagian=".$kdbagianrwj."";
		//echo $kdbagian.":";
       $query = $this->db->query($strQuery);
	   // $query = _QMS_Query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                //$curentshift = $data->getcurrentshift;
				$curentshift = $data->shift;
				
             }
            }
         return $curentshift;
	}
    }
    //----------------END------------------------
	
	
    public function Getkdtarif() {
        $Customer = $_POST['customer'];
        $strQuery = "select getkdtarifcus('$Customer') ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string = $data->getkdtarifcus ;
				
             }
         echo $string;
		 }	
         return $string;
    }
	
	public function getKdUnitFar(){
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		echo "{success:true, unitfar:'".$kd_unit_far."'}";
	}
	
	public function cekStokFarmasi(){
		$res = $this->db->query("SELECT s.kd_milik,s.kd_prd,p.minstok,st.jml_stok_apt
								FROM apt_stok_unit_gin s
									INNER JOIN (SELECT sum(jml_stok_apt) as jml_stok_apt,kd_prd,kd_milik,kd_unit_far FROM apt_stok_unit_gin 
												WHERE kd_unit_far='".$_POST['kd_unit_far']."' GROUP BY kd_prd,kd_milik,kd_unit_far) st on st.kd_milik=s.kd_milik 
										and st.kd_prd=s.kd_prd and st.kd_unit_far=s.kd_unit_far
									INNER JOIN apt_produk p on p.kd_milik=s.kd_milik and p.kd_prd=s.kd_prd
								WHERE s.kd_unit_far='".$_POST['kd_unit_far']."' and st.jml_stok_apt <= p.minstok
								GROUP BY s.kd_milik,s.kd_prd,p.minstok,st.jml_stok_apt")->result();
								
		echo "{success:true, jumlah:".count($res)."}";
	}
	public function getDataKunjungan(){
		$sql="SELECT distinct pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, 
			pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir,agama.agama, 
			pasien.gol_darah,pasien.no_asuransi, pasien.wni, pasien.status_marita, 
			pasien.alamat, pasien.kd_kelurahan, pendidikan.pendidikan, 
			pekerjaan.pekerjaan,kb.kd_kabupaten, kb.KABUPATEN, 
			kc.kd_kecamatan, kc.kecamatan, pr.kd_propinsi, 
			pr.PROPINSI, pasien.kd_pendidikan, pasien.kd_pekerjaan, 
			pasien.kd_agama, pasien.alamat_ktp,pasien.nama_ayah,
			pasien.kd_pendidikan_ayah, p1.pendidikan as pendidikan_ayah,
			pasien.kd_pekerjaan_ayah, pek1.pekerjaan as pekerjaan_ayah, pasien.nama_ibu,
			pasien.kd_pendidikan_ibu, p2.pendidikan as pendidikan_ibu ,pasien.kd_pekerjaan_ibu, 
			pek2.pekerjaan as pekerjaan_ibu,pasien.suami_istri as nama_suaimiistri,
			pasien.kd_pendidikan_suamiistri, p3.pendidikan as pendidikan_suamiistri,
			pasien.kd_pekerjaan_suamiistri, pek3.pekerjaan as pekerjaan_suamiistri, 
			pasien.kd_kelurahan_ktp,pasien.kd_pos_ktp, pasien.kd_pos, 
			proktp.kd_propinsi as propinsiktp,kabktp.kd_kabupaten as kabupatenktp, kecktp.kd_kecamatan as kecamatanktp, 
			kelktp.kd_kelurahan as kelurahanktp ,kl.kelurahan,kelktp.kelurahan as kel_ktp, 
			kecktp.kecamatan as kec_ktp,kabktp.kabupaten as kab_ktp ,proktp.propinsi as pro_ktp, pasien.email,
			pasien.handphone,pasien.telepon,hck.kd_pasien as cekkartu,pasien.NIK,
			pasien.pemegang_asuransi as nama_peserta, 
			suku.suku as suku,
			suku.kd_suku as kd_suku
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan 
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan 
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan 
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp 
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten 
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
			left join suku ON suku.kd_suku = pasien.kd_suku  
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien 
			where pasien.kd_pasien='".$_POST['command']."'
			order by pasien.kd_pasien desc limit 1";
		$rec=$this->db->query($sql)->row();	
		$recPenyakit=$this->db->query("select p.kd_penyakit,p.penyakit,m.tgl_masuk,
			case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
			from mr_penyakit m
			inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
			where kd_pasien='".$_POST['command']."'  and m.stat_diag in(0,1) order by tgl_masuk ASC")->result();
		$o=array();
		$o['RIWAYAT_PENYAKIT']         = $recPenyakit;
		$o['KD_PASIEN']                = $rec->kd_pasien;
		$o['NAMA']                     = $rec->nama;
		$o['NAMA_KELUARGA']            = $rec->nama_keluarga;
		$o['JENIS_KELAMIN']            = $rec->jenis_kelamin;
		$o['TEMPAT_LAHIR']             = $rec->tempat_lahir;
		$o['TGL_LAHIR']                = date("d/m/Y",strtotime($rec->tgl_lahir));
		$o['AGAMA']                    = $rec->agama;
		$o['GOL_DARAH']                = $rec->gol_darah;
		$o['WNI']                      = $rec->wni;
		$o['STATUS_MARITA']            = $rec->status_marita;
		$o['ALAMAT']                   = $rec->alamat;
		$o['KD_KELURAHAN']             = $rec->kd_kelurahan;
		$o['PENDIDIKAN']               = $rec->pendidikan;
		$o['PENDIDIKAN_AYAH']          = $rec->pendidikan_ayah;
		$o['PENDIDIKAN_IBU']           = $rec->pendidikan_ibu;
		$o['PENDIDIKAN_SUAMIISTRI']    = $rec->pendidikan_suamiistri;
		$o['PEKERJAAN']                = $rec->pekerjaan;
		$o['PEKERJAAN_AYAH']           = $rec->pekerjaan_ayah;
		$o['PEKERJAAN_IBU']            = $rec->pekerjaan_ibu;
		$o['PEKERJAAN_SUAMIISTRI']     = $rec->pekerjaan_suamiistri;
		$o['KABUPATEN']                = $rec->kabupaten;
		$o['KECAMATAN']                = $rec->kecamatan;
		$o['PROPINSI']                 = $rec->propinsi;
		$o['KD_KABUPATEN']             = $rec->kd_kabupaten;
		$o['KD_KECAMATAN']             = $rec->kd_kecamatan;
		$o['KD_PROPINSI']              = $rec->kd_propinsi;
		$o['KD_PENDIDIKAN']            = $rec->kd_pendidikan;
		$o['KD_PENDIDIKAN_AYAH']       = $rec->kd_pendidikan_ayah;
		$o['KD_PENDIDIKAN_IBU']        = $rec->kd_pendidikan_ibu;
		$o['KD_PENDIDIKAN_SUAMIISTRI'] = $rec->kd_pendidikan_suamiistri;
		$o['KD_PEKERJAAN']             = $rec->kd_pekerjaan;
		$o['KD_PEKERJAAN_AYAH']        = $rec->kd_pekerjaan_ayah;
		$o['KD_PEKERJAAN_IBU']         = $rec->kd_pekerjaan_ibu;
		$o['KD_PEKERJAAN_SUAMIISTRI']  = $rec->kd_pekerjaan_suamiistri;
		$o['KD_AGAMA']                 = $rec->kd_agama;
		$o['ALAMAT_KTP']               = $rec->alamat_ktp;
		$o['NAMA_AYAH']                = $rec->nama_ayah;
		$o['NAMA_IBU']                 = $rec->nama_ibu;
		$o['NAMA_SUAMIISTRI']          = $rec->nama_suaimiistri;
		$o['KD_KELURAHAN_KTP']         = $rec->kd_kelurahan_ktp;
		$o['KD_POS_KTP']               = $rec->kd_pos_ktp;
		$o['KD_POS']                   = $rec->kd_pos;
		$o['PROPINSIKTP']              = $rec->propinsiktp;
		$o['KABUPATENKTP']             = $rec->kabupatenktp;
		$o['KECAMATANKTP']             = $rec->kecamatanktp;
		$o['KELURAHANKTP']             = $rec->kelurahanktp;
		$o['KELURAHAN']                = $rec->kelurahan;
		$o['KEL_KTP']                  = $rec->kel_ktp;
		$o['KEC_KTP']                  = $rec->kec_ktp;
		$o['KAB_KTP']                  = $rec->kab_ktp;
		$o['PRO_KTP']                  = $rec->pro_ktp;
		$o['NO_ASURANSI']              = $rec->no_asuransi;
		$o['EMAIL_PASIEN']             = $rec->email;
		$o['HP_PASIEN']                = $rec->handphone;
		$o['NAMA_PESERTA']                = $rec->nama_peserta;
		$o['TLPN_PASIEN']              = $rec->telepon;
		$o['NIK']              			= $rec->nik;
		$o['KD_CUSTOMER']              = '';
		$o['NAMA_CUSTOMER']            = '';
		$o['JENIS_CUST']               = '';
		$o['KD_UNIT']                  = '';
		$o['NAMA_UNIT']                = '';
		$o['KD_PENYAKIT']              = '';
		$o['NAMA_PENYAKIT']            = '';
		$o['KD_SUKU']            		= $rec->kd_suku;
		$o['SUKU']            			= $rec->suku;
		$sqlKun = "SELECT kunjungan.kd_customer AS KD_CUSTOMER,customer.customer AS NAMA_CUSTOMER,kontraktor.jenis_cust AS JENIS_CUST,
			unit.kd_unit AS KD_UNIT,unit.nama_unit AS NAMA_UNIT,m.kd_penyakit,p.penyakit,
			r.kd_rujukan,r.rujukan,ra.cara_penerimaan,penerimaan 			
			FROM kunjungan 
			inner join customer ON kunjungan.kd_customer=customer.kd_customer
			inner join kontraktor ON kontraktor.kd_customer=kunjungan.kd_customer
			inner join unit ON unit.kd_unit=kunjungan.kd_unit
			left join mr_penyakit m ON m.kd_pasien=kunjungan.kd_pasien AND m.kd_unit=kunjungan.kd_unit AND m.tgl_masuk=kunjungan.tgl_masuk AND  m.urut_masuk=kunjungan.urut_masuk AND stat_diag=0
			left join penyakit p ON p.kd_penyakit=m.kd_penyakit
			left join rujukan r ON r.kd_rujukan=kunjungan.kd_rujukan
			left join rujukan_asal ra ON r.cara_penerimaan=ra.cara_penerimaan ::int8
			WHERE kunjungan.kd_pasien='".$_POST['command']."' AND left(kunjungan.kd_unit,1)='2' ORDER BY kunjungan.tgl_masuk DESC,kunjungan.jam_masuk DESC Limit 1";
		$recKun=$this->db->query($sqlKun)->row();
		if($recKun){
			$o['KD_CUSTOMER']     = $recKun->kd_customer;
			$o['NAMA_CUSTOMER']   = $recKun->nama_customer;
			$o['JENIS_CUST']      = $recKun->jenis_cust;
			$o['KD_UNIT']         = $recKun->kd_unit;
			$o['NAMA_UNIT']       = $recKun->nama_unit;
			$o['KD_PENYAKIT']     = $recKun->kd_penyakit;
			$o['NAMA_PENYAKIT']   = $recKun->kd_penyakit.' - '.$recKun->penyakit;
			$o['KD_RUJUKAN']      = $recKun->kd_rujukan;
			$o['NAMA_RUJUAKAN']   = $recKun->rujukan;
			$o['KD_PENERIMAAN']   = $recKun->cara_penerimaan;
			$o['NAMA_PENERIMAAN'] = $recKun->penerimaan;
		}

		echo json_encode($o);
    
	}
	public function getDataKunjungan_lama(){
		$sql="SELECT distinct top 1 KD_PASIEN=pasien.kd_pasien, NAMA=pasien.nama, NAMA_KELUARGA=pasien.nama_keluarga, 
			JENIS_KELAMIN=pasien.jenis_kelamin, TEMPAT_LAHIR=pasien.tempat_lahir, TGL_LAHIR=pasien.tgl_lahir,AGAMA=agama.agama, 
			GOL_DARAH=pasien.gol_darah,NO_ASURANSI=pasien.no_asuransi, WNI=pasien.wni, STATUS_MARITA=pasien.status_marita, 
			ALAMAT=pasien.alamat, KD_KELURAHAN=pasien.kd_kelurahan, PENDIDIKAN=pendidikan.pendidikan, 
			PEKERJAAN=pekerjaan.pekerjaan,KD_KABUPATEN=kb.kd_kabupaten, KABUPATEN=kb.KABUPATEN, 
			KD_KECAMATAN=kc.kd_kecamatan, KECAMATAN=kc.KECAMATAN, KD_PROPINSI=pr.kd_propinsi, 
			PROPINSI=pr.PROPINSI, KD_PENDIDIKAN=pasien.kd_pendidikan, KD_PEKERJAAN=pasien.kd_pekerjaan, 
			KD_AGAMA=pasien.kd_agama, ALAMAT_KTP=pasien.alamat_ktp,NAMA_AYAH=pasien.nama_ayah,
			KD_PENDIDIKAN_AYAH=pasien.kd_pendidikan_ayah, PENDIDIKAN_AYAH=p1.pendidikan,
			KD_PEKERJAAN_AYAH=pasien.kd_pekerjaan_ayah, PEKERJAAN_AYAH=pek1.pekerjaan, NAMA_IBU=pasien.nama_ibu,
			KD_PENDIDIKAN_IBU=pasien.kd_pendidikan_ibu, PENDIDIKAN_IBU=p2.pendidikan ,KD_PEKERJAAN_IBU=pasien.kd_pekerjaan_ibu, 
			PEKERJAAN_IBU=pek2.pekerjaan,NAMA_SUAMIISTRI=pasien.suami_istri,
			KD_PENDIDIKAN_SUAMIISTRI=pasien.kd_pendidikan_suamiistri, PENDIDIKAN_SUAMIISTRI=p3.pendidikan,
			KD_PEKERJAAN_SUAMIISTRI=pasien.kd_pekerjaan_suamiistri, PEKERJAAN_SUAMIISTRI=pek3.pekerjaan, 
			KD_KELURAHAN_KTP=pasien.kd_kelurahan_ktp,KD_POS_KTP=pasien.kd_pos_ktp, KD_POS=pasien.kd_pos, 
			PROPINSIKTP=proktp.kd_propinsi,KABUPATENKTP=kabktp.kd_kabupaten, KECAMATANKTP=kecktp.kd_kecamatan, 
			KELURAHANKTP=kelktp.kd_kelurahan ,KELURAHAN=kl.kelurahan, KEL_KTP=kelktp.kelurahan , 
			KEC_KTP=kecktp.kecamatan,KAB_KTP=kabktp.kabupaten ,PRO_KTP=proktp.propinsi, EMAIL=pasien.email,
			HANDPHONE=pasien.handphone,TELEPON=pasien.telepon,hck.kd_pasien as cekkartu			
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan 
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan 
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan 
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp 
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten 
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien 
			where pasien.kd_pasien='".$_POST['command']."'
			order by pasien.kd_pasien desc ";
		// $rec=_QMS_Query($sql)->row();	
		$rec=$this->db->query($sql)->row();	
		$recPenyakit=$this->db->query("SELECT  p.kd_penyakit,p.penyakit,CONVERT(varchar(12),m.tgl_masuk,106) AS tgl_masuk,
			case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
			from mr_penyakit m
			inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
			where kd_pasien='".$_POST['command']."'  and m.stat_diag in(0,1) order by tgl_masuk ASC ")->result();
		$o=array();
		$o['RIWAYAT_PENYAKIT']=$recPenyakit;
		$o['KD_PASIEN']=$rec->KD_PASIEN;
		$o['NAMA']=$rec->NAMA;
		$o['NAMA_KELUARGA']=$rec->NAMA_KELUARGA;
		$o['JENIS_KELAMIN']=$rec->JENIS_KELAMIN;
		$o['TEMPAT_LAHIR']=$rec->TEMPAT_LAHIR;
		//$o['TGL_LAHIR'] = date("d/m/Y",strtotime($rec->TGL_LAHIR));
		$o['TGL_LAHIR'] = date_format(date_create($rec->TGL_LAHIR),"d/m/Y");
		$o['AGAMA']=$rec->AGAMA;
		$o['GOL_DARAH']=$rec->GOL_DARAH;
		$o['WNI']=$rec->WNI;
		$o['STATUS_MARITA']=$rec->STATUS_MARITA;
		$o['ALAMAT']=$rec->ALAMAT;
		$o['KD_KELURAHAN']=$rec->KD_KELURAHAN;
		$o['PENDIDIKAN']=$rec->PENDIDIKAN;
		$o['PENDIDIKAN_AYAH']=$rec->PENDIDIKAN_AYAH;
		$o['PENDIDIKAN_IBU']=$rec->PENDIDIKAN_IBU;
		$o['PENDIDIKAN_SUAMIISTRI']=$rec->PENDIDIKAN_SUAMIISTRI;
		$o['PEKERJAAN']=$rec->PEKERJAAN;
		$o['PEKERJAAN_AYAH']=$rec->PEKERJAAN_AYAH;
		$o['PEKERJAAN_IBU']=$rec->PEKERJAAN_IBU;
		$o['PEKERJAAN_SUAMIISTRI']=$rec->PEKERJAAN_SUAMIISTRI;
		$o['KABUPATEN']=$rec->KABUPATEN;
		$o['KECAMATAN']=$rec->KECAMATAN;
		$o['PROPINSI']=$rec->PROPINSI;
		$o['KD_KABUPATEN']=$rec->KD_KABUPATEN;
		$o['KD_KECAMATAN']=$rec->KD_KECAMATAN;
		$o['KD_PROPINSI']=$rec->KD_PROPINSI;
		$o['KD_PENDIDIKAN']=$rec->KD_PENDIDIKAN;
		$o['KD_PENDIDIKAN_AYAH']=$rec->KD_PENDIDIKAN_AYAH;
		$o['KD_PENDIDIKAN_IBU']=$rec->KD_PENDIDIKAN_IBU;
		$o['KD_PENDIDIKAN_SUAMIISTRI']=$rec->KD_PENDIDIKAN_SUAMIISTRI;
		$o['KD_PEKERJAAN']=$rec->KD_PEKERJAAN;
		$o['KD_PEKERJAAN_AYAH']=$rec->KD_PEKERJAAN_AYAH;
		$o['KD_PEKERJAAN_IBU']=$rec->KD_PEKERJAAN_IBU;
		$o['KD_PEKERJAAN_SUAMIISTRI']=$rec->KD_PEKERJAAN_SUAMIISTRI;
		$o['KD_AGAMA']=$rec->KD_AGAMA;
		$o['ALAMAT_KTP']=$rec->ALAMAT_KTP;
		$o['NAMA_AYAH']=$rec->NAMA_AYAH;
		$o['NAMA_IBU']=$rec->NAMA_IBU;
		$o['NAMA_SUAMIISTRI']=$rec->NAMA_SUAMIISTRI;
		$o['KD_KELURAHAN_KTP']=$rec->KD_KELURAHAN_KTP;
		$o['KD_POS_KTP']=$rec->KD_POS_KTP;
		$o['KD_POS']=$rec->KD_POS;
		$o['PROPINSIKTP']=$rec->PROPINSIKTP;
		$o['KABUPATENKTP']=$rec->KABUPATENKTP;
		$o['KECAMATANKTP']=$rec->KECAMATANKTP;
		$o['KELURAHANKTP']=$rec->KELURAHANKTP;
		$o['KELURAHAN']=$rec->KELURAHAN;
		$o['KEL_KTP']=$rec->KEL_KTP;
		$o['KEC_KTP']=$rec->KEC_KTP;
		$o['KAB_KTP']=$rec->KAB_KTP;
		$o['PRO_KTP']=$rec->PRO_KTP;
		$o['NO_ASURANSI']=$rec->NO_ASURANSI;
		$o['EMAIL_PASIEN']=$rec->EMAIL;
		$o['HP_PASIEN']=$rec->HANDPHONE;
		$o['TLPN_PASIEN']=$rec->TELEPON;
		$o['KD_CUSTOMER']='';
		$o['NAMA_CUSTOMER']='';
		$o['JENIS_CUST']='';
		$o['KD_UNIT']='';
		$o['NAMA_UNIT']='';
		$o['KD_PENYAKIT']='';
		$o['NAMA_PENYAKIT']='';
		$sqlKun="SELECT kunjungan.kd_customer AS KD_CUSTOMER,customer.customer AS NAMA_CUSTOMER,kontraktor.jenis_cust AS JENIS_CUST,
			unit.kd_unit AS KD_UNIT,unit.nama_unit AS NAMA_UNIT,m.kd_penyakit,p.penyakit,
			r.kd_rujukan,r.rujukan,ra.cara_penerimaan,penerimaan 			
			FROM kunjungan 
			inner join customer ON kunjungan.kd_customer=customer.kd_customer
			inner join kontraktor ON kontraktor.kd_customer=kunjungan.kd_customer
			inner join unit ON unit.kd_unit=kunjungan.kd_unit
			left join mr_penyakit m ON m.kd_pasien=kunjungan.kd_pasien AND m.kd_unit=kunjungan.kd_unit AND m.tgl_masuk=kunjungan.tgl_masuk AND  m.urut_masuk=kunjungan.urut_masuk AND stat_diag=0
			left join penyakit p ON p.kd_penyakit=m.kd_penyakit
			left join rujukan r ON r.kd_rujukan=kunjungan.kd_rujukan
			left join rujukan_asal ra ON r.cara_penerimaan=ra.cara_penerimaan
			WHERE kunjungan.kd_pasien='".$_POST['command']."' AND left(kunjungan.kd_unit,1)='2' ORDER BY kunjungan.tgl_masuk DESC,kunjungan.jam_masuk DESC  limit 1";
		$recKun=$this->db->query($sqlKun)->row();
		if($recKun){
			$o['KD_CUSTOMER']=$recKun->KD_CUSTOMER;
			$o['NAMA_CUSTOMER']=$recKun->NAMA_CUSTOMER;
			$o['JENIS_CUST']=$recKun->JENIS_CUST;
			$o['KD_UNIT']=$recKun->KD_UNIT;
			$o['NAMA_UNIT']=$recKun->NAMA_UNIT;
			$o['KD_PENYAKIT']=$recKun->kd_penyakit;
			$o['NAMA_PENYAKIT']=$recKun->kd_penyakit.' - '.$recKun->penyakit;
			$o['KD_RUJUKAN']=$recKun->kd_rujukan;
			$o['NAMA_RUJUAKAN']=$recKun->rujukan;
			$o['KD_PENERIMAAN']=$recKun->cara_penerimaan;
			$o['NAMA_PENERIMAAN']=$recKun->penerimaan;
		}
		//echo 'a';
		echo json_encode($o);
    
	}
     /* public function getDataKunjunganIgd(){
		$sql="SELECT distinct top 1 KD_PASIEN=pasien.kd_pasien, NAMA=pasien.nama, NAMA_KELUARGA=pasien.nama_keluarga, 
			JENIS_KELAMIN=pasien.jenis_kelamin, TEMPAT_LAHIR=pasien.tempat_lahir, TGL_LAHIR=pasien.tgl_lahir,AGAMA=agama.agama, 
			GOL_DARAH=pasien.gol_darah,NO_ASURANSI=pasien.no_asuransi, WNI=pasien.wni, STATUS_MARITA=pasien.status_marita, 
			ALAMAT=pasien.alamat, KD_KELURAHAN=pasien.kd_kelurahan, PENDIDIKAN=pendidikan.pendidikan, 
			PEKERJAAN=pekerjaan.pekerjaan,KD_KABUPATEN=kb.kd_kabupaten, KABUPATEN=kb.KABUPATEN, 
			KD_KECAMATAN=kc.kd_kecamatan, KECAMATAN=kc.KECAMATAN, KD_PROPINSI=pr.kd_propinsi, 
			PROPINSI=pr.PROPINSI, KD_PENDIDIKAN=pasien.kd_pendidikan, KD_PEKERJAAN=pasien.kd_pekerjaan, 
			KD_AGAMA=pasien.kd_agama, ALAMAT_KTP=pasien.alamat_ktp,NAMA_AYAH=pasien.nama_ayah,
			KD_PENDIDIKAN_AYAH=pasien.kd_pendidikan_ayah, PENDIDIKAN_AYAH=p1.pendidikan,
			KD_PEKERJAAN_AYAH=pasien.kd_pekerjaan_ayah, PEKERJAAN_AYAH=pek1.pekerjaan, NAMA_IBU=pasien.nama_ibu,
			KD_PENDIDIKAN_IBU=pasien.kd_pendidikan_ibu, PENDIDIKAN_IBU=p2.pendidikan ,KD_PEKERJAAN_IBU=pasien.kd_pekerjaan_ibu, 
			PEKERJAAN_IBU=pek2.pekerjaan,NAMA_SUAMIISTRI=pasien.suami_istri,
			KD_PENDIDIKAN_SUAMIISTRI=pasien.kd_pendidikan_suamiistri, PENDIDIKAN_SUAMIISTRI=p3.pendidikan,
			KD_PEKERJAAN_SUAMIISTRI=pasien.kd_pekerjaan_suamiistri, PEKERJAAN_SUAMIISTRI=pek3.pekerjaan, 
			KD_KELURAHAN_KTP=pasien.kd_kelurahan_ktp,KD_POS_KTP=pasien.kd_pos_ktp, KD_POS=pasien.kd_pos, 
			PROPINSIKTP=proktp.kd_propinsi,KABUPATENKTP=kabktp.kd_kabupaten, KECAMATANKTP=kecktp.kd_kecamatan, 
			KELURAHANKTP=kelktp.kd_kelurahan ,KELURAHAN=kl.kelurahan, KEL_KTP=kelktp.kelurahan , 
			KEC_KTP=kecktp.kecamatan,KAB_KTP=kabktp.kabupaten ,PRO_KTP=proktp.propinsi, EMAIL=pasien.email,
			HANDPHONE=pasien.handphone,TELEPON=pasien.telepon,hck.kd_pasien as cekkartu 
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan 
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan 
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan 
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp 
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten 
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien 
			where pasien.kd_pasien='".$_POST['command']."'
			order by pasien.kd_pasien desc ";
		$rec=_QMS_Query($sql)->row();	
		$recPenyakit=_QMS_Query("select p.kd_penyakit,p.penyakit,CONVERT(varchar(12),m.tgl_masuk,106) AS tgl_masuk,
			case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
			from mr_penyakit m
			inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
			where kd_pasien='".$_POST['command']."'  and m.stat_diag in(0,1) order by tgl_masuk ASC")->result();
		$o=array();
    $o['RIWAYAT_PENYAKIT']         =$recPenyakit;
    $o['KD_PASIEN']                =$rec->KD_PASIEN;
    $o['NAMA']                     =$rec->NAMA;
    $o['NAMA_KELUARGA']            =$rec->NAMA_KELUARGA;
    $o['JENIS_KELAMIN']            =$rec->JENIS_KELAMIN;
    $o['TEMPAT_LAHIR']             =$rec->TEMPAT_LAHIR;
	$o['TGL_LAHIR'] 				= date_format(date_create($rec->TGL_LAHIR),"d/m/Y");
    //$o['TGL_LAHIR']                =date("d/m/Y",strtotime($rec->TGL_LAHIR));
    $o['AGAMA']                    =$rec->AGAMA;
    $o['GOL_DARAH']                =$rec->GOL_DARAH;
    $o['WNI']                      =$rec->WNI;
    $o['STATUS_MARITA']            =$rec->STATUS_MARITA;
    $o['ALAMAT']                   =$rec->ALAMAT;
    $o['KD_KELURAHAN']             =$rec->KD_KELURAHAN;
    $o['PENDIDIKAN']               =$rec->PENDIDIKAN;
    $o['PENDIDIKAN_AYAH']          =$rec->PENDIDIKAN_AYAH;
    $o['PENDIDIKAN_IBU']           =$rec->PENDIDIKAN_IBU;
    $o['PENDIDIKAN_SUAMIISTRI']    =$rec->PENDIDIKAN_SUAMIISTRI;
    $o['PEKERJAAN']                =$rec->PEKERJAAN;
    $o['PEKERJAAN_AYAH']           =$rec->PEKERJAAN_AYAH;
    $o['PEKERJAAN_IBU']            =$rec->PEKERJAAN_IBU;
    $o['PEKERJAAN_SUAMIISTRI']     =$rec->PEKERJAAN_SUAMIISTRI;
    $o['KABUPATEN']                =$rec->KABUPATEN;
    $o['KECAMATAN']                =$rec->KECAMATAN;
    $o['PROPINSI']                 =$rec->PROPINSI;
    $o['KD_KABUPATEN']             =$rec->KD_KABUPATEN;
    $o['KD_KECAMATAN']             =$rec->KD_KECAMATAN;
    $o['KD_PROPINSI']              =$rec->KD_PROPINSI;
    $o['KD_PENDIDIKAN']            =$rec->KD_PENDIDIKAN;
    $o['KD_PENDIDIKAN_AYAH']       =$rec->KD_PENDIDIKAN_AYAH;
    $o['KD_PENDIDIKAN_IBU']        =$rec->KD_PENDIDIKAN_IBU;
    $o['KD_PENDIDIKAN_SUAMIISTRI'] =$rec->KD_PENDIDIKAN_SUAMIISTRI;
    $o['KD_PEKERJAAN']             =$rec->KD_PEKERJAAN;
    $o['KD_PEKERJAAN_AYAH']        =$rec->KD_PEKERJAAN_AYAH;
    $o['KD_PEKERJAAN_IBU']         =$rec->KD_PEKERJAAN_IBU;
    $o['KD_PEKERJAAN_SUAMIISTRI']  =$rec->KD_PEKERJAAN_SUAMIISTRI;
    $o['KD_AGAMA']                 =$rec->KD_AGAMA;
    $o['ALAMAT_KTP']               =$rec->ALAMAT_KTP;
    $o['NAMA_AYAH']                =$rec->NAMA_AYAH;
    $o['NAMA_IBU']                 =$rec->NAMA_IBU;
    $o['NAMA_SUAMIISTRI']          =$rec->NAMA_SUAMIISTRI;
    $o['KD_KELURAHAN_KTP']         =$rec->KD_KELURAHAN_KTP;
    $o['KD_POS_KTP']               =$rec->KD_POS_KTP;
    $o['KD_POS']                   =$rec->KD_POS;
    $o['PROPINSIKTP']              =$rec->PROPINSIKTP;
    $o['KABUPATENKTP']             =$rec->KABUPATENKTP;
    $o['KECAMATANKTP']             =$rec->KECAMATANKTP;
    $o['KELURAHANKTP']             =$rec->KELURAHANKTP;
    $o['KELURAHAN']                =$rec->KELURAHAN;
    $o['KEL_KTP']                  =$rec->KEL_KTP;
    $o['KEC_KTP']                  =$rec->KEC_KTP;
    $o['KAB_KTP']                  =$rec->KAB_KTP;
    $o['PRO_KTP']                  =$rec->PRO_KTP;
    $o['NO_ASURANSI']              =$rec->NO_ASURANSI;
    $o['EMAIL_PASIEN']             =$rec->EMAIL;
    $o['HP_PASIEN']                =$rec->HANDPHONE;
    $o['TLPN_PASIEN']              =$rec->TELEPON;
    $o['KD_CUSTOMER']              ='';
    $o['NAMA_CUSTOMER']            ='';
    $o['JENIS_CUST']               ='';
    $o['KD_UNIT']                  ='';
    $o['NAMA_UNIT']                ='';
    $o['KD_PENYAKIT']              ='';
    $o['NAMA_PENYAKIT']            ='';
    $sqlKun="SELECT top 1 kunjungan.kd_customer AS KD_CUSTOMER,customer.customer AS NAMA_CUSTOMER,kontraktor.jenis_cust AS JENIS_CUST,
      unit.kd_unit AS KD_UNIT,unit.nama_unit AS NAMA_UNIT,m.kd_penyakit,p.penyakit,
      r.kd_rujukan,r.rujukan,ra.cara_penerimaan,penerimaan      
      FROM kunjungan 
      inner join customer ON kunjungan.kd_customer=customer.kd_customer
      inner join kontraktor ON kontraktor.kd_customer=kunjungan.kd_customer
      inner join unit ON unit.kd_unit=kunjungan.kd_unit
      left join mr_penyakit m ON m.kd_pasien=kunjungan.kd_pasien AND m.kd_unit=kunjungan.kd_unit AND m.tgl_masuk=kunjungan.tgl_masuk AND  m.urut_masuk=kunjungan.urut_masuk AND stat_diag=0
      left join penyakit p ON p.kd_penyakit=m.kd_penyakit
      left join rujukan r ON r.kd_rujukan=kunjungan.kd_rujukan
      left join rujukan_asal ra ON r.cara_penerimaan=ra.cara_penerimaan
      WHERE kunjungan.kd_pasien='".$_POST['command']."' AND left(kunjungan.kd_unit,1)='3' ORDER BY kunjungan.tgl_masuk DESC,kunjungan.jam_masuk DESC";
    $recKun=_QMS_Query($sqlKun)->row();
    if($recKun){
      $o['KD_CUSTOMER']=$recKun->KD_CUSTOMER;
      $o['NAMA_CUSTOMER']=$recKun->NAMA_CUSTOMER;
      $o['JENIS_CUST']=$recKun->JENIS_CUST;
      $o['KD_UNIT']=$recKun->KD_UNIT;
      $o['NAMA_UNIT']=$recKun->NAMA_UNIT;
      $o['KD_PENYAKIT']=$recKun->kd_penyakit;
      $o['NAMA_PENYAKIT']=$recKun->kd_penyakit.' - '.$recKun->penyakit;
      $o['KD_RUJUKAN']=$recKun->kd_rujukan;
      $o['NAMA_RUJUAKAN']=$recKun->rujukan;
      $o['NAMA_PENERIMAAN']=$recKun->penerimaan;
      $o['KD_PENERIMAAN']=$recKun->cara_penerimaan;
		}

		echo json_encode($o);
	} */   
	public function getDataKunjunganIgd(){
		/* $sql="SELECT distinct top 1 KD_PASIEN=pasien.kd_pasien, NAMA=pasien.nama, NAMA_KELUARGA=pasien.nama_keluarga, 
			JENIS_KELAMIN=pasien.jenis_kelamin, TEMPAT_LAHIR=pasien.tempat_lahir, TGL_LAHIR=pasien.tgl_lahir,AGAMA=agama.agama, 
			GOL_DARAH=pasien.gol_darah,NO_ASURANSI=pasien.no_asuransi, WNI=pasien.wni, STATUS_MARITA=pasien.status_marita, 
			ALAMAT=pasien.alamat, KD_KELURAHAN=pasien.kd_kelurahan, PENDIDIKAN=pendidikan.pendidikan, 
			PEKERJAAN=pekerjaan.pekerjaan,KD_KABUPATEN=kb.kd_kabupaten, KABUPATEN=kb.KABUPATEN, 
			KD_KECAMATAN=kc.kd_kecamatan, KECAMATAN=kc.KECAMATAN, KD_PROPINSI=pr.kd_propinsi, 
			PROPINSI=pr.PROPINSI, KD_PENDIDIKAN=pasien.kd_pendidikan, KD_PEKERJAAN=pasien.kd_pekerjaan, 
			KD_AGAMA=pasien.kd_agama, ALAMAT_KTP=pasien.alamat_ktp,NAMA_AYAH=pasien.nama_ayah,
			KD_PENDIDIKAN_AYAH=pasien.kd_pendidikan_ayah, PENDIDIKAN_AYAH=p1.pendidikan,
			KD_PEKERJAAN_AYAH=pasien.kd_pekerjaan_ayah, PEKERJAAN_AYAH=pek1.pekerjaan, NAMA_IBU=pasien.nama_ibu,
			KD_PENDIDIKAN_IBU=pasien.kd_pendidikan_ibu, PENDIDIKAN_IBU=p2.pendidikan ,KD_PEKERJAAN_IBU=pasien.kd_pekerjaan_ibu, 
			PEKERJAAN_IBU=pek2.pekerjaan,NAMA_SUAMIISTRI=pasien.suami_istri,
			KD_PENDIDIKAN_SUAMIISTRI=pasien.kd_pendidikan_suamiistri, PENDIDIKAN_SUAMIISTRI=p3.pendidikan,
			KD_PEKERJAAN_SUAMIISTRI=pasien.kd_pekerjaan_suamiistri, PEKERJAAN_SUAMIISTRI=pek3.pekerjaan, 
			KD_KELURAHAN_KTP=pasien.kd_kelurahan_ktp,KD_POS_KTP=pasien.kd_pos_ktp, KD_POS=pasien.kd_pos, 
			PROPINSIKTP=proktp.kd_propinsi,KABUPATENKTP=kabktp.kd_kabupaten, KECAMATANKTP=kecktp.kd_kecamatan, 
			KELURAHANKTP=kelktp.kd_kelurahan ,KELURAHAN=kl.kelurahan, KEL_KTP=kelktp.kelurahan , 
			KEC_KTP=kecktp.kecamatan,KAB_KTP=kabktp.kabupaten ,PRO_KTP=proktp.propinsi, EMAIL=pasien.email,
			HANDPHONE=pasien.handphone,TELEPON=pasien.telepon,hck.kd_pasien as cekkartu 
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan 
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan 
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan 
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp 
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten 
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien 
			where pasien.kd_pasien='".$_POST['command']."'
			order by pasien.kd_pasien desc "; */
		$sql="SELECT distinct pasien.kd_pasien as KD_PASIEN, pasien.nama as NAMA, pasien.nama_keluarga as NAMA_KELUARGA, 
			pasien.jenis_kelamin as JENIS_KELAMIN, pasien.tempat_lahir as TEMPAT_LAHIR, pasien.tgl_lahir as TGL_LAHIR,agama.agama as AGAMA, 
			pasien.gol_darah as GOL_DARAH,pasien.no_asuransi as NO_ASURANSI, pasien.wni as WNI, pasien.status_marita as STATUS_MARITA, 
			pasien.alamat as ALAMAT, pasien.kd_kelurahan as KD_KELURAHAN, pendidikan.pendidikan as PENDIDIKAN, 
			pekerjaan.pekerjaan as PEKERJAAN,kb.kd_kabupaten as KD_KABUPATEN, kb.KABUPATEN as KABUPATEN, 
			kc.kd_kecamatan as KD_KECAMATAN, kc.KECAMATAN as KECAMATAN, pr.kd_propinsi as KD_PROPINSI, 
			pr.PROPINSI as PROPINSI, pasien.kd_pendidikan as KD_PENDIDIKAN, pasien.kd_pekerjaan as KD_PEKERJAAN, 
			pasien.kd_agama as KD_AGAMA, pasien.alamat_ktp as ALAMAT_KTP,pasien.nama_ayah as NAMA_AYAH,
			pasien.kd_pendidikan_ayah as KD_PENDIDIKAN_AYAH, p1.pendidikan as PENDIDIKAN_AYAH,
			pasien.kd_pekerjaan_ayah as KD_PEKERJAAN_AYAH, pek1.pekerjaan as PEKERJAAN_AYAH, pasien.nama_ibu as NAMA_IBU,
			pasien.kd_pendidikan_ibu as KD_PENDIDIKAN_IBU, p2.pendidikan as PENDIDIKAN_IBU ,pasien.kd_pekerjaan_ibu as KD_PEKERJAAN_IBU, 
			pek2.pekerjaan as PEKERJAAN_IBU,pasien.suami_istri as NAMA_SUAMIISTRI,
			pasien.kd_pendidikan_suamiistri as KD_PENDIDIKAN_SUAMIISTRI, p3.pendidikan as PENDIDIKAN_SUAMIISTRI,
			pasien.kd_pekerjaan_suamiistri as KD_PEKERJAAN_SUAMIISTRI, pek3.pekerjaan as PEKERJAAN_SUAMIISTRI, 
			pasien.kd_kelurahan_ktp as KD_KELURAHAN_KTP,pasien.kd_pos_ktp as KD_POS_KTP, pasien.kd_pos as KD_POS, 
			proktp.kd_propinsi as PROPINSIKTP,kabktp.kd_kabupaten as KABUPATENKTP, kecktp.kd_kecamatan as KECAMATANKTP, 
			kelktp.kd_kelurahan as KELURAHANKTP,kl.kelurahan as KELURAHAN, kelktp.kelurahan as KEL_KTP, 
			kecktp.kecamatan as KEC_KTP,kabktp.kabupaten as KAB_KTP,proktp.propinsi as PRO_KTP, pasien.email as EMAIL,
			pasien.handphone as HANDPHONE,
			pasien.telepon as TELEPON,
			suku.kd_suku as kd_suku,
			suku.suku as suku,
			hck.kd_pasien as cekkartu 
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan 
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan 
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan 
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp 
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten 
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
			left join suku ON suku.kd_suku = pasien.kd_suku  
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien 
			where pasien.kd_pasien='".$_POST['command']."'
			order by pasien.kd_pasien desc limit 1";
		$rec=$this->db->query($sql)->row();	
		$recPenyakit=$this->db->query("select p.kd_penyakit,p.penyakit,to_char(m.tgl_masuk,'D Mon YYYY') AS tgl_masuk,
			case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
			from mr_penyakit m
			inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
			where kd_pasien='".$_POST['command']."'  and m.stat_diag in(0,1) order by tgl_masuk ASC")->result();
		$o=array();
    	$o['RIWAYAT_PENYAKIT']         =$recPenyakit;
    	$o['KD_PASIEN']                =$rec->kd_pasien;
    	$o['NAMA']                     =$rec->nama;
    	$o['NAMA_KELUARGA']            =$rec->nama_keluarga;
    	$o['JENIS_KELAMIN']            =$rec->jenis_kelamin;
    	$o['TEMPAT_LAHIR']             =$rec->tempat_lahir;
		$o['TGL_LAHIR'] 				= date_format(date_create($rec->tgl_lahir),"d/m/Y");
    	//$o['TGL_LAHIR']                =date("d/m/Y",strtotime($rec->TGL_LAHIR));
    	$o['AGAMA']                    =$rec->agama;
    	$o['GOL_DARAH']                =$rec->gol_darah;
    	$o['WNI']                      =$rec->wni;
    	$o['STATUS_MARITA']            =$rec->status_marita;
    	$o['ALAMAT']                   =$rec->alamat;
    	$o['KD_KELURAHAN']             =$rec->kd_kelurahan;
    	$o['PENDIDIKAN']               =$rec->pendidikan;
    	$o['PENDIDIKAN_AYAH']          =$rec->pendidikan_ayah;
    	$o['PENDIDIKAN_IBU']           =$rec->pendidikan_ibu;
    	$o['PENDIDIKAN_SUAMIISTRI']    =$rec->pendidikan_suamiistri;
    	$o['PEKERJAAN']                =$rec->pekerjaan;
    	$o['PEKERJAAN_AYAH']           =$rec->pekerjaan_ayah;
    	$o['PEKERJAAN_IBU']            =$rec->pekerjaan_ibu;
    	$o['PEKERJAAN_SUAMIISTRI']     =$rec->pekerjaan_suamiistri;
    	$o['KABUPATEN']                =$rec->kabupaten;
    	$o['KECAMATAN']                =$rec->kecamatan;
    	$o['PROPINSI']                 =$rec->propinsi;
    	$o['KD_KABUPATEN']             =$rec->kd_kabupaten;
    	$o['KD_KECAMATAN']             =$rec->kd_kecamatan;
    	$o['KD_PROPINSI']              =$rec->kd_propinsi;
    	$o['KD_PENDIDIKAN']            =$rec->kd_pendidikan;
    	$o['KD_PENDIDIKAN_AYAH']       =$rec->kd_pendidikan_ayah;
    	$o['KD_PENDIDIKAN_IBU']        =$rec->kd_pendidikan_ibu;
    	$o['KD_PENDIDIKAN_SUAMIISTRI'] =$rec->kd_pendidikan_suamiistri;
    	$o['KD_PEKERJAAN']             =$rec->kd_pekerjaan;
    	$o['KD_PEKERJAAN_AYAH']        =$rec->kd_pekerjaan_ayah;
    	$o['KD_PEKERJAAN_IBU']         =$rec->kd_pekerjaan_ibu;
    	$o['KD_PEKERJAAN_SUAMIISTRI']  =$rec->kd_pekerjaan_suamiistri;
    	$o['KD_AGAMA']                 =$rec->kd_agama;
    	$o['ALAMAT_KTP']               =$rec->alamat_ktp;
    	$o['NAMA_AYAH']                =$rec->nama_ayah;
    	$o['NAMA_IBU']                 =$rec->nama_ibu;
    	$o['NAMA_SUAMIISTRI']          =$rec->nama_suamiistri;
    	$o['KD_KELURAHAN_KTP']         =$rec->kd_kelurahan_ktp;
    	$o['KD_POS_KTP']               =$rec->kd_pos_ktp;
    	$o['KD_POS']                   =$rec->kd_pos;
    	$o['PROPINSIKTP']              =$rec->propinsiktp;
    	$o['KABUPATENKTP']             =$rec->kabupatenktp;
    	$o['KECAMATANKTP']             =$rec->kecamatanktp;
    	$o['KELURAHANKTP']             =$rec->kelurahanktp;
    	$o['KELURAHAN']                =$rec->kelurahan;
    	$o['KEL_KTP']                  =$rec->kel_ktp;
    	$o['KEC_KTP']                  =$rec->kec_ktp;
    	$o['KAB_KTP']                  =$rec->kab_ktp;
    	$o['PRO_KTP']                  =$rec->pro_ktp;
    	$o['NO_ASURANSI']              =$rec->no_asuransi;
    	$o['EMAIL_PASIEN']             =$rec->email;
    	$o['HP_PASIEN']                =$rec->handphone;
    	$o['TLPN_PASIEN']              =$rec->telepon;
    	$o['SUKU']              		= $rec->suku;
    	$o['KD_SUKU']              		= $rec->kd_suku;
    	$o['KD_CUSTOMER']              ='';
    	$o['NAMA_CUSTOMER']            ='';
    	$o['JENIS_CUST']               ='';
    	$o['KD_UNIT']                  ='';
    	$o['NAMA_UNIT']                ='';
    	$o['KD_PENYAKIT']              ='';
    	$o['NAMA_PENYAKIT']            ='';
    
    $sqlKun="SELECT kunjungan.kd_customer AS KD_CUSTOMER,customer.customer AS NAMA_CUSTOMER,kontraktor.jenis_cust AS JENIS_CUST,
      unit.kd_unit AS KD_UNIT,unit.nama_unit AS NAMA_UNIT,m.kd_penyakit,p.penyakit,
      r.kd_rujukan,r.rujukan,ra.cara_penerimaan,penerimaan,kunjungan.kd_dokter      
      FROM kunjungan 
      inner join customer ON kunjungan.kd_customer=customer.kd_customer
      inner join kontraktor ON kontraktor.kd_customer=kunjungan.kd_customer
      inner join unit ON unit.kd_unit=kunjungan.kd_unit
      left join mr_penyakit m ON m.kd_pasien=kunjungan.kd_pasien AND m.kd_unit=kunjungan.kd_unit AND m.tgl_masuk=kunjungan.tgl_masuk AND  m.urut_masuk=kunjungan.urut_masuk AND stat_diag=0
      left join penyakit p ON p.kd_penyakit=m.kd_penyakit
      left join rujukan r ON r.kd_rujukan=kunjungan.kd_rujukan
      left join rujukan_asal ra ON r.cara_penerimaan::varchar=ra.cara_penerimaan
      WHERE kunjungan.kd_pasien='".$_POST['command']."' AND left(kunjungan.kd_unit,1)='3' ORDER BY kunjungan.tgl_masuk DESC,kunjungan.jam_masuk DESC limit 1";
    $recKun=$this->db->query($sqlKun)->row();
    if($recKun){
      $o['KD_CUSTOMER']=$recKun->kd_customer;
      $o['NAMA_CUSTOMER']=$recKun->nama_customer;
      $o['KD_DOKTER']=$recKun->kd_dokter;
      $o['JENIS_CUST']=$recKun->jenis_cust;
      $o['KD_UNIT']=$recKun->kd_unit;
      $o['NAMA_UNIT']=$recKun->nama_unit;
      $o['KD_PENYAKIT']=$recKun->kd_penyakit;
      $o['NAMA_PENYAKIT']=$recKun->kd_penyakit.' - '.$recKun->penyakit;
      $o['KD_RUJUKAN']=$recKun->kd_rujukan;
      $o['NAMA_RUJUAKAN']=$recKun->rujukan;
      $o['NAMA_PENERIMAAN']=$recKun->penerimaan;
      $o['KD_PENERIMAAN']=$recKun->cara_penerimaan;
		}

		echo json_encode($o);
    /* $this->db->close();
    _QMS_Closeconn(); */
	}
	public function ComboGroupTrustee(){
		
	}

	public function get_date_time_server(){
		$response = array(
			'tanggal' 	=> $this->tanggal,
			'jam' 		=> $this->jam,
		);
		echo json_encode($response);
	}

	public function get_unit_asal(){
		$response = array();
		$query = $this->db->query("SELECT * FROM unit_asal where no_transaksi = '".$this->input->post('no_transaksi')."' AND kd_kasir = '".$this->input->post('kd_kasir')."'");

		if ($query->num_rows() > 0) {
			$response['status'] = true;
			$response['data'] = $query->result();
		}else{
			$response['status'] = false;
		}
		echo json_encode($response);
	}
	 public function getPrint(){
		$print_id=$_GET['print_id'];
		$res=$this->db->query("
			SELECT L.value,L.description,L.print_code,P.print_type,L.parameter,P.dir_report as url_report ,CASE WHEN P.print_preview=true then 'Y' ELSE 'N' END AS view
				FROM plugin_printer_list L 
				INNER JOIN plugin_printer P ON P.print_code=L.print_code
				WHERE L.printer_list_id='".$print_id."'")->row();
		if($res){
			$parameter=json_decode($res->parameter);
			$parameter->view=$res->view;
			$res->parameter=json_encode($parameter);
			
			echo json_encode(array('r'=>'S','m'=>'','d'=>$res));
		}else{
			echo json_encode(array('r'=>'E','m'=>'Data Tidak Ada.'));
		}
	}
}

    class Rowgetlanguage
    {
        public $LIST_KEY;
        public $LABEL;

    }

?>