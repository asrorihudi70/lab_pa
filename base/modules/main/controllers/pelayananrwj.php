<?php
class pelayananrwj extends  MX_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->library('session','url');
    }
    public function cetak(){
			$common=$this->common;
			$title='Resume Pelayanan Rawat Jalan';
			$param=json_decode($_POST['data']);
			$var = $param->kd_pasien;
			$unit = $param->kd_unit;
			$tgl = $param->tgl;
			$html='';
			$response = $this->db->query("SELECT TOP 1 P.kd_pasien, P.nama, u.nama_unit, P.nik, P.no_asuransi, P.tempat_lahir, P.tgl_lahir, '0' AS usia,
                                            CASE WHEN P.jenis_kelamin = 1 THEN 'Laki-laki' WHEN P.jenis_kelamin = 0 THEN 'Perempuan' ELSE '-' END jenis_kelamin,
                                            A.agama,
                                            s.suku,
                                            C.customer,
                                            pkj.pekerjaan,
                                            CASE
                                                WHEN P.status_marita = '0' THEN
                                                'Belum Kawin' 
                                                WHEN P.status_marita = '1' THEN
                                                'Kawin' 
                                                WHEN P.status_marita = '2' THEN
                                                'Janda' ELSE'Duda' 
                                            END status_marita,
                                            P.alamat,
                                            kel.kelurahan,
                                            kec.kecamatan,
                                            kab.kabupaten,
                                            prop.propinsi,
                                            pend.pendidikan,
                                            pj.nama_pj,
                                            pj.hubungan,
                                            pj.alamat AS alamat_pj,
                                            P.telepon,
                                            K.tgl_masuk,
                                            K.jam_masuk,
                                            CASE
                                                WHEN pj.hubungan = 1 THEN '' 
                                                WHEN pj.hubungan = 2 THEN 'Cucu' 
                                                WHEN pj.hubungan = 3 THEN 'Ibu' 
                                                WHEN pj.hubungan = 4 THEN 'Ayah' 
                                                WHEN pj.hubungan = 5 THEN 'Istri' 
                                                WHEN pj.hubungan = 6 THEN 'Suami' 
                                                WHEN pj.hubungan = 7 THEN 'Mertua' 
                                                WHEN pj.hubungan = 8 THEN 'Menantu' 
                                                WHEN pj.hubungan = 9 THEN 'Keluarga' 
                                                WHEN pj.hubungan = 10 THEN 'Saudara' 
                                                WHEN pj.hubungan = 11 THEN 'Lain-Lain'
                                                WHEN pj.hubungan = 13 THEN 'Anak' 
                                                    ELSE '' 
                                                    END AS hubungan_pj 
                                        FROM
                                            pasien P 
                                            INNER JOIN kunjungan K ON K.kd_pasien = P.kd_pasien
                                            LEFT JOIN penanggung_jawab pj ON pj.kd_pasien = K.kd_pasien 
                                            AND pj.kd_unit = K.kd_unit 
                                            AND pj.tgl_masuk = K.tgl_masuk 
                                            AND pj.urut_masuk = K.urut_masuk
                                            INNER JOIN unit u ON u.kd_unit = K.kd_unit
                                            INNER JOIN agama A ON A.kd_agama = P.kd_agama
                                            INNER JOIN suku s ON s.kd_suku = P.kd_suku
                                            INNER JOIN customer C ON C.kd_customer = K.kd_customer
                                            INNER JOIN pekerjaan pkj ON pkj.kd_pekerjaan = P.kd_pekerjaan
                                            INNER JOIN kelurahan kel ON kel.kd_kelurahan = P.kd_kelurahan
                                            INNER JOIN kecamatan kec ON kec.kd_kecamatan = kel.kd_kecamatan
                                            INNER JOIN kabupaten kab ON kab.kd_kabupaten = kec.kd_kabupaten
                                            INNER JOIN propinsi prop ON prop.kd_propinsi = kab.kd_propinsi
                                            INNER JOIN pendidikan pend ON pend.kd_pendidikan = P.kd_pendidikan 
                                        WHERE
                                            k.kd_unit = '".$unit."'
                                            AND P.kd_pasien = '".$var."' 
                                        ORDER BY
                                            k.tgl_masuk DESC"); // ->result();

		$timestamp = strtotime($response->row()->tgl_lahir);
		// Define an array of month names in Indonesian
		$monthNames = [
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember',
		];
		// Extract day, month, and year from the timestamp
		$day = date('d', $timestamp);
		$month = date('n', $timestamp);
		$year = date('Y', $timestamp);
		// Create the formatted date string
		$tanggallahir = $day . '-' . $monthNames[$month] . '-' . $year;
		if($response->num_rows() > 0){
			$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:0px;padding-top:0px;font-family: Arial;font-size:13px;">
						<tr>
							<td colspan="6" align="center" width="100%" height="45px">
								<font style="font-size:14px; font-family: \'Times New Roman\', Times, serif;"><b>RESUME PELAYANAN RAWAT JALAN</b></font>
                                <hr>
							</td>
						</tr>
						<tr>
							<td colspan="6" align="left" width="100%">
                            <br>
							</td>
						</tr>
					</table>';
                    // echo '<pre>'.var_export($response->row(),true).'</pre>';
                    // die;
            $html .= '<table border="1" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:5px; vertical-align: top;" width="200px" rowspan="3"><center>Nama Lengkap Pasien : </center><div style="vertical-align: bottom;">'.$response->row()->nama.'</div></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:5px; vertical-align: top;" width="150px" rowspan="3"><center>Nama Tambahan</center><br>&nbsp;</td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:5px; vertical-align: top;" width="200px" rowspan="3"><center>Tempat Tanggal Lahir </center><br><div style="vertical-align: bottom;">'.$response->row()->tempat_lahir.', '.$tanggallahir.'</div></td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:5px; vertical-align: top;" width="100px" rowspan="3"><center>NIK</center> <br> '.$response->row()->nik.'</td>
                            </tr>
                    </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:10px; vertical-align: top; padding-left:5px; border-right: none !important;" width="200px" rowspan="3">Alamat Lengkap : '.$response->row()->alamat.' <br> Kab/Kodya : '.$response->row()->kabupaten.'</td>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size: 13px; padding-bottom: 10px; vertical-align: top; padding-left: 5px; border-right: none !important; border-left: none !important;" width="200px" rowspan="3">RT/RW : <br><br> Prop. : '.$response->row()->propinsi.'</td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:10px; vertical-align: top; padding-left:5px; border-left: none !important;" width="200px" rowspan="3">Kelurahan : '.$response->row()->kelurahan.'</td>
                                <td style="font-family: \'Times New Roman\', Times, serif;font-size:13px; padding-bottom:10px; vertical-align: top; padding-left:5px;" width="150px" rowspan="3"><center>No. Telp</center> <br> '.$response->row()->telepon.'</td>
                            </tr>
                    </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" rowspan="3"><center>Agama</center> <br> ' . $response->row()->agama . '</td>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" rowspan="3"><center>Kebangsaan</center></td>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" rowspan="3"><center>Pekerjaan</center> <br> ' . $response->row()->pekerjaan . '</td>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" rowspan="3"><center>No.KTP/SIM/Paspor</center> <br> ' . $response->row()->nik . '</td>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" rowspan="3"><center>Status Perkawinan</center> <br> K / TK / C / J / D </td>
                                <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" colspan="2" rowspan="3"><center>Jenis Pembayaran</center> <br> ' . $response->row()->customer . '</td>
                            </tr>
                    </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="6" style="font-family: \'Times New Roman\', Times, serif; font-size:13px;">Alergi <br> &nbsp;</td>
                            </tr>
                    </table>';
            $html .= '<table border="1" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="30px" colspan="3"><center>NO</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="80px" colspan="3"><center>Tanggal Berkunjung</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="70px" colspan="3"><center>Diagnosis</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="50px" colspan="3"><center>ICD</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="100px" colspan="3"><center>Obat-Obat/Jenis Pemeriksaan</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="130px" colspan="3"><center>Riwayat Rawat Inap Sejak Kunjungan Terakhir</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="100px" colspan="3"><center>Prosedur Bedah /Operasi Sejak Kunjungan Terakhir</center></td>
                            <td style="font-family: \'Times New Roman\', Times, serif; font-size:13px; vertical-align: top;" width="100px" colspan="3"><center>Nama Jenis Dan Tanda Tangan Petugas Kesehatan (Dokter, Dietisen, Trapis)</center></td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        <td style="height: 65px;" colspan="3">&nbsp;<br>&nbsp;</td>
                        </tr>
                    </table>';
		}
		$html.='</body></html>';		
		// echo $html; die;		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Resume Pelayanan RWJ',$html);
	}
}
