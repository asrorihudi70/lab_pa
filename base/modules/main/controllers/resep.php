<?php
class resep extends  MX_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->library('session','url');
    }
    public function cetak(){
			$common=$this->common;
			$title='RESEP';
			$param=json_decode($_POST['data']);
			$var = $param->kd_pasien;
			$unit = $param->kd_unit;
			$tgl = $param->tgl;
		$query = $this->db->query("SELECT B.catatan_racik,B.satuan_racik,U.nama_unit,AB.racik_aturan AS aturan_racik,AP.singkatan AS aturan_pakai,B.signa,B.takaran,B.jumlah_racik,ASU.satuan,A.KD_PASIEN,B.no_racik,A.CAT_RACIKAN,A.TGL_MASUK,D.NAMA_OBAT,B.JUMLAH,B.CARA_PAKAI,C.NAMA,B.racikan,E.NAMA AS DOKTER 
			FROM MR_RESEP A LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
			LEFT JOIN APT_OBAT D ON D.KD_PRD=B.KD_PRD  
			LEFT JOIN PASIEN C ON C.KD_PASIEN=A.KD_PASIEN
			LEFT JOIN APT_SATUAN ASU ON ASU.KD_SATUAN=D.KD_SATUAN
			LEFT JOIN DOKTER E ON A.KD_DOKTER = E.KD_DOKTER
			LEFT JOIN UNIT U ON U.KD_UNIT = A.KD_UNIT
			LEFT JOIN apt_racik_aturan_buat AB ON AB.kd_racik_atr = CASE WHEN B.aturan_racik='' THEN null ELSE  CONVERT(INT,B.aturan_racik) END
			LEFT JOIN apt_racik_aturan_pakai AP ON AP.kd_racik_atr_pk = CASE WHEN B.aturan_pakai='' THEN null ELSE  CONVERT(INT,B.aturan_pakai) END
			where A.KD_PASIEN = '".$var."' AND A.KD_UNIT = '".$unit."' and A.tgl_masuk='".$tgl."' AND A.dilayani=0 ORDER BY B.no_racik")->result();
		$html='';	
		if (count($query)>0){
			$row=$query[0];
			$date = strtotime($row->TGL_MASUK);
			$html.='<table  style="width:800px;border-bottom:1px dotted #000; padding:10px 0;">
				<tr>
					<td colspan="6" align="center"><b>RESEP OBAT</b></td>
				</tr>
				<tr>
					<td width="120">No. Medrec</td>
					<td width="15">:</td>
					<td colspan="4">'.$row->KD_PASIEN.'</td>
				</tr>
				<tr>
					<td>Nama Pasien</td>
					<td>:</td>
					<td colspan="2">'.$row->NAMA.'</td>
					<td>Unit</td>
					<td>: '.$row->nama_unit.'</td>
				</tr>
				<tr>
					<td>Dokter</td>
					<td>:</td>
					<td>'.$row->DOKTER.'</td>
					<td></td>
					<td>Tanggal</td>
					<td>: '.date('d-M-Y',$date).'</td>
				</tr>
			</table>
			';
			$html.='<table style="width:800px;border-bottom:1px dotted #000; padding:10px 0;">
			<tr>
				<th width="400" align="left" colspan="2">Obat</th>
				<th width="86" align="left">Qty</th>
				<th width="86" align="left">Signa</th>
				<th width="86" align="left">Takaran</th>
				<th width="86" align="left">Aturan Pakai</th>
			</tr>
			<tr>
				<td colspan="3"></td>
			</tr>
			';	
			$no_racik='';
			foreach($query as $obt){
				if($obt->racikan==false){
					$html.='<tr>
						<td colspan="2">'.$obt->NAMA_OBAT.'</td><td>'.$obt->JUMLAH.' '.$obt->satuan.' </td><td>'.$obt->signa.'</td><td>'.$obt->takaran.'</td><td>'.$obt->CARA_PAKAI.'</td>
					</tr>';	
				}else{
					if($no_racik!==$obt->no_racik){
						$html.='<tr>
							<td colspan="2">Racikan '.$obt->no_racik.' ( '.$obt->aturan_racik.' - '.$obt->aturan_pakai.')</td><td>'.$obt->jumlah_racik.' '.$obt->satuan_racik.'</td><td>'.$obt->signa.'</td><td>'.$obt->takaran.'</td><td>'.$obt->cara_pakai.'</td>
						</tr>';	
						$no_racik=$obt->no_racik;
					}
					$cat_racik='';
					if($obt->catatan_racik != null && $obt->catatan_racik != ''){
						$cat_racik='('.$obt->catatan_racik.')';
					}
					$html.='<tr>
						<td colspan="2">&nbsp;&nbsp;&nbsp;- '.$obt->nama_obat.' '.$cat_racik.'</td><td>'.$obt->jumlah.' '.$obt->satuan.' </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					</tr>';	
				}
			}
			$html.='<tr>
				<td colspan="3"></td>
			</tr>';
			// $html.='<tr>
				// <td width="86">Catatan Obat</td>
				// <td width="310">:</td>
				// <td>'.$row->cat_racikan.'</td>
			// </tr>';
			$html.='</table><br>';	
			$html.='<table border="0" width="20">
				<tr>
					<td colspan="5">Riwayat Alergi Obat :</td>
				</tr>
				<tr>
					<td style="border: 1px solid black;" width="20">&nbsp;</td>
					<td width="50">&nbsp;Tidak</td>
					<td style="border: 1px solid black;" width="20">&nbsp;</td>
					<td width="50">&nbsp;Ya</td>
					<td>Nama Obat ...............</td>
				</tr>
			</table>';	
			$html.='<br ><br >';	
			// $html.='<br /><br />';	
			$html.='<div style="margin-top: 50px;float:right;position:absolute; right:100px;padding:5px 10px;" ><div>('.$row->DOKTER.')</div>';	
			$html.='</div>';	
		}else{
			$html.='<div align="center"><h2>Data kosong</h2></div>';	
		}
		$html.='</body></html>';		
		// echo $html; die;		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Resep',$html);
	}
}
?>