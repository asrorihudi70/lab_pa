<?php
class ringkasan_02 extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session','url');
			  $this->load->helper('ncil_lib');
    }
	 
    public function cetak()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));

      $common=$this->common;
   		$result=$this->result;
   		$title='RINGKASAN';
		$param=json_decode($_POST['data']);
		$var = $param->kd_pasien;
		$unit = $param->kd_unit;
		$tgl = $param->tgl;
		$notr = $param->notr;
		$html='';
		 		$query = $this->db->query("SELECT PASIEN.NAMA,PASIEN.KD_PASIEN,KUNJUNGAN.TGL_MASUK,UNIT.NAMA_UNIT FROM PASIEN LEFT JOIN KUNJUNGAN ON PASIEN.KD_PASIEN=KUNJUNGAN.KD_PASIEN
				LEFT JOIN UNIT ON UNIT.KD_UNIT = KUNJUNGAN.KD_UNIT
   where PASIEN.KD_PASIEN = '".$var."' AND KUNJUNGAN.KD_UNIT = '".$unit."' AND KUNJUNGAN.TGL_MASUK='".$tgl."' ")->result();
						  
			 
			$html.='<html>
		   					<head></head>
							<body>';
							
			$html.='<div class="head">
			RINGKASAN MEDIS PASIEN
			 </div>';

		foreach($query as $row)
			{
				$date = strtotime($row->tgl_masuk);
				  $html.='<table style="font-size:11px;border-bottom:1px dotted #000; border-top:1px dotted #000; padding:5px 0;" width="800">
					<tr>
					<td width="80">No. Medrec</td>
					<td width="20">:</td>
					<td >'.$row->kd_pasien.'</td>
					<td></td>
					<td width="60">Poli</td>
					<td width="20">:</td>
					<td>'.$row->nama_unit.'</td>
					</tr>
					<tr>
					<td>Nama</td>
					<td>:</td>
					<td width="130">'.$row->nama.'</td>
					<td></td>
					<td>Tanggal</td>
					<td>:</td>
					<td>'.tanggal($row->tgl_masuk).'</td>
					</tr>
					<tr>
					</tr>
					</table>';
					
			}
			

			$html.='<br />';	
			
				$an = $this->db->query("select anamnese,cat_fisik from kunjungan where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK='".$tgl."' ")->result();
			foreach($an as $a)
			{
				$html.='<div class="text"><br/><b> Anamnesa : </b><br />'.$a->anamnese.'</div>';
			}
			$stringkon = '';
			$kon = $this->db->query("SELECT KONDISI,HASIL,SATUAN FROM MR_KONPAS 
LEFT JOIN MR_KONPASDTL ON MR_KONPAS.ID_KONPAS=MR_KONPASDTL.ID_KONPAS 
LEFT JOIN MR_KONDISIFISIK ON MR_KONPASDTL.ID_KONDISI=MR_KONDISIFISIK.ID_KONDISI 
 where KD_PASIEN = '".$var."' AND MR_KONPAS.KD_UNIT = '".$unit."' AND MR_KONPAS.TGL_MASUK = '".$tgl."' ORDER BY ORDERLIST ")->result();
			foreach($kon as $k)
			{
				$stringkon .= '- '.$k->kondisi.' = '.$k->hasil.' '.$k->satuan.'<br/>'; 
			}

			
			$html.="<div class='text'><br/><b> Kondisi Fisik : </b><br />".$stringkon."</div>";
			
			
			$string = '';
			$peny = $this->db->query("SELECT PENYAKIT FROM MR_PENYAKIT LEFT JOIN PENYAKIT ON MR_PENYAKIT.KD_PENYAKIT=PENYAKIT.KD_PENYAKIT
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK='".$tgl."' ")->result();
			foreach($peny as $p)
			{
				$string .= '- '.$p->penyakit.'<br/>'; 
			}
			
			$html.="<div class='text'><br/><b> Diagnosa : </b><br />".$string."</div>";
			
			
			$stringlab = '';
			/* $lab = $this->db->query("SELECT DESKRIPSI FROM MR_LABKONSUL LEFT JOIN  MR_LABKONSULDTL ON MR_LABKONSUL.ID_LABKONSUL =MR_LABKONSULDTL.ID_LABKONSUL
LEFT JOIN PRODUK ON MR_LABKONSULDTL.KD_PRODUK =PRODUK.KD_PRODUK
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK = '".$tgl."' ")->result(); */
 			if ($this->db->query("select * from kasir where deskripsi='Kasir Laboratorium'")->num_rows()>0) {
				$kodekasir = $this->db->query("select * from kasir where deskripsi='Kasir Laboratorium'")->row()->kd_kasir;
				$queryKasirLab = " and kd_kasir = '".$kodekasir."'";
 			}else{
 				$kodekasir = '';
				$queryKasirLab = " ";
 			}

			//$kodekasir=$this->db->query("select * from kasir where deskripsi='Kasir Laboratorium'")->row()->kd_kasir;
			$lab = $this->db->query("SELECT * from unit_asal  where no_transaksi_asal = '".$notr."' and kd_kasir_asal = '01' ".$queryKasirLab."")->result();

			foreach($lab as $l){
				$query2 	= $this->db->query("select getalltestrad('".$l->no_transaksi."', '".$l->kd_kasir."') as deskripsi")->result();
				foreach ($query2 as $result) {
					$stringlab.=' - '.$result->deskripsi.'<br>';
				}
			}
			/*foreach($lab as $l)
			{
				$stringlab .= '- '.$l->deskripsi.'<br/>'; 
			}*/

			$html.="<div class='text'><br/><b> Tindakan Laboratorium : </b><br />".$stringlab."</div>";
			
			
			$stringrad = '';
			/* $rad = $this->db->query("SELECT DESKRIPSI FROM MR_RADKONSUL 
LEFT JOIN MR_RADKONSULDTL ON MR_RADKONSUL.ID_RADKONSUL =MR_RADKONSULDTL.ID_RADKONSUL 
LEFT JOIN PRODUK ON MR_RADKONSULDTL.KD_PRODUK =PRODUK.KD_PRODUK 
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK = '".$tgl."' ")->result(); */
 			if ($this->db->query("select * from kasir where deskripsi='Kasir Radiologi'")->num_rows()>0) {
				$kodekasir = $this->db->query("select * from kasir where deskripsi='Kasir Radiologi'")->row()->kd_kasir;
				$queryKasirRad = " and kd_kasir = '".$kodekasir."'";
 			}else{
 				$kodekasir = '';
				$queryKasirRad = " ";
 			}

			//$kodekasir=$this->db->query("select * from kasir where deskripsi='Kasir Radiologi'")->row()->kd_kasir;
			$rad = $this->db->query("SELECT * from unit_asal where no_transaksi_asal = '".$notr."' and kd_kasir_asal = '01' ".$queryKasirRad."")->result();
			
			foreach($rad as $r){
				$query2 	= $this->db->query("select getalltestrad('".$r->no_transaksi."', '".$r->kd_kasir."') as deskripsi")->result();
				foreach ($query2 as $result) {
					$stringrad.=' - '.$result->deskripsi.'<br>';
				}
			}
			/*foreach($rad as $r)
			{
				$stringrad .= '- '.$r->deskripsi.'<br/>'; 
			}*/
			
			$html.="<div class='text'><br/><b> Tindakan Radiologi : </b><br />".$stringrad."</div>";
			
			
			

			$stringobt = '';
			$obt = $this->db->query("SELECT NAMA_OBAT,JUMLAH,CARA_PAKAI FROM MR_RESEP 
LEFT JOIN MR_RESEPDTL ON MR_RESEP.ID_MRRESEP=MR_RESEPDTL.ID_MRRESEP 
LEFT JOIN APT_OBAT ON MR_RESEPDTL.KD_PRD =APT_OBAT.KD_PRD 
 where KD_PASIEN = '".$var."' AND KD_UNIT = '".$unit."' AND TGL_MASUK = '".$tgl."' ")->result();
			foreach($obt as $o)
			{
				$stringobt .= '- '.$o->nama_obat.'('.$o->jumlah.')'.' '.$o->cara_pakai.'<br/>'; 
			}
			
			$html.="<div class='text'><br/><b> Obat : </b><br />".$stringobt."</div>";
			$html.='</body></html>';	
			/*echo $tahun.' ';
			echo $nama_triwulan.' ';
			echo $nama_bulan.' ';
			echo $html;*/
			$prop=array('foot'=>true);
			$this->common->setPdf('P','Ringkasan',$html);		
}

}

?>