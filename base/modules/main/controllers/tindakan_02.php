<?php
class tindakan_02 extends  MX_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session','url');
		$this->load->helper('ncil_lib');
    }
    public function cetak(){
		$common=$this->common;
			$title ='TINDAKAN';
			$param =json_decode($_POST['data']);
			$var   = $param->kd_pasien;
			$unit  = $param->kd_unit;
			$tgl   = $param->tgl;
		$query = $this->db->query("SELECT F.CUSTOMER,A.QTY,A.HARGA,H.NAMA_UNIT,G.NAMA AS DOKTER,D.TGL_MASUK,E.ALAMAT,E.KD_PASIEN,E.NAMA,A.NO_TRANSAKSI,B.DESKRIPSI  FROM DETAIL_TRANSAKSI A LEFT JOIN PRODUK B ON A.KD_PRODUK = B.KD_PRODUK 
			LEFT JOIN TRANSAKSI C ON A.NO_TRANSAKSI = C.NO_TRANSAKSI  AND A.KD_KASIR=C.KD_KASIR 
			INNER JOIN KUNJUNGAN D ON D.KD_PASIEN = C.KD_PASIEN AND D.KD_UNIT=C.KD_UNIT AND D.URUT_MASUK=C.URUT_MASUK  
			INNER JOIN PASIEN E ON D.KD_PASIEN = E.KD_PASIEN
			LEFT JOIN CUSTOMER F  ON F.KD_CUSTOMER = D.KD_CUSTOMER
			LEFT JOIN DOKTER G ON G.KD_DOKTER=D.KD_DOKTER
			LEFT JOIN UNIT H ON H.KD_UNIT=D.KD_UNIT
			where D.KD_PASIEN = '".$var."' AND D.KD_UNIT = '".$unit."' AND D.TGL_MASUK='".$tgl."' ")->result();
		$html='';
		if($query){
			$row=$query[0];
			$date = strtotime($row->TGL_MASUK);
			$html.='<table style="border-bottom:1px dotted #000; padding:5px 0;" width="800">
				<tr>
				<td width="90"></td>
				<td width="20"></td>
				<td colspan="3" ></td>
				</tr>
				<tr>
				<td>No. Medrec</td>
				<td>:</td>
				<td width="130">'.$row->KD_PASIEN.'</td>
				<td width="100">No. Trans</td>
				<td width="20">:</td>
				<td>'.$row->NO_TRANSAKSI.'</td>
				</tr>
				<tr>
				<td>Status P.</td>
				<td>:</td>
				<td>'.$row->CUSTOMER.'</td>
				<td>Tanggal</td>
				<td>:</td>
				<td>'.$row->TGL_MASUK.'</td>
				</tr>
				<tr>
				<td>Dokter</td>
				<td>:</td>
				<td colspan="3">'.$row->DOKTER.'</td>
				</tr>
				<tr>
				<td>Nama</td>
				<td>:</td>
				<td colspan="3">'.$row->NAMA.'</td>
				</tr>
				<tr>
				<td>Alamat</td>
				<td>:</td>
				<td colspan="3">'.$row->ALAMAT.'</td>
				</tr>
				<tr>
				<td >POLIKLINIK </td>
				<td >:</td>
				<td colspan="3">'.$row->NAMA_UNIT.'</td>
				</tr>
				</table>
				
				';
			$html.='<table style=" font-size:12px; border-bottom:0px dotted #000; padding:10px 0;">
			<tr>
				<th>No.</th>
				<th width="400" align="left" colspan="2">Uraian</th>
				<th width="100" align="left">Qty</th>
				<th width="282" align="left">Sub Total</th>
			</tr>';
			$i=1;
			$amount=0;
			foreach($query as $t)
				{
					$html.='<tr><td>'.$i.'</td><td colspan="2">'.$t->DESKRIPSI.'</td><td>'.$t->QTY.'</td><td>'.$t->HARGA.'</td></tr>';	
					$i++;
					$amount += $t->HARGA;
				}
			$html.='<tr>
				<td colspan="3" style="border-top:1px dotted #000;" ></td>
				<td style="border-top:1px dotted #000;" >Jumlah Total</td>
				<td style="border-top:1px dotted #000;" >'.$amount.'</td>
				</tr>
				';
			$html.='</table>';
				
			$html.='<br /><br />';	
			$html.='<div style="float:right;position:absolute; right:100px;padding:5px 10px;" >
				<div align="center">'.date('Y-m-d').'</div>';
			$html.='<br /><br />';
			$html.='<div>(..........................)</div>';	
			$html.='<div align="center">'.$this->session->userdata['user_id']['username'].'</div></div>';
			date_default_timezone_set("Asia/Jakarta"); 
			$jam = gmdate("H:i:s", time()+60*60*7);
			$html.='<br /><br /><br /><br />';
			$html.='</body></html>';	
			// echo $html;
			$this->common->setPdf('P','Tindakan',$html);	
		}
	}
}
?>