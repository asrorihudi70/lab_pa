<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewcombobahasautility extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }	 


    public function index()
    {
        //if ($this->my_usession->logged_in)
        //{
        	//echo '{success:true}';
            $this->load->view('main/index');            
        //}
        //else
        //{
        //    redirect('user/login');
        //}
		
    }
   
    public function read($Params=null)
    {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);

        $this->load->model('language/am_language');

/**
         * 			    Skip: 0,
         * 			    Take: 1000,
         * 			    Sort: 'LANGUAGE',
         * 			    Sortdir: 'ASC',
         * 			    target: 'ViewComboBahasaUtility',
         * 			    param: ''
         */

        //$Params = array('0','1000','LANGUAGE','ASC');

        //echo $Params[2];

        $query = $this->am_language->readparam($ar_filters=null,$Params[1],$Params[0],$Params[2],$Params[3]);

        $arrResult=array();

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $rows)
            {
                    //$arrResult[] = $rows;
                    $arrResult[] = $this->FillRow($rows);
            }

    //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}

            echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';

        }
        else echo '{success:false}';
   	
   }

    public function save($Params=null)
    {

//        var params =
//	{
//		Table: 'ViewComboBahasaUtility',
//		Id:selectLanguageUtilityAset,
//		KdUser:mVarKdUserUtilityAset
//	};

        $Id = $Params["Id"];
        $KdUser = $Params["KdUser"];

        $this->load->model("userman/zusers");

        $query = $this->zusers->read($KdUser);

        $data = array('Kd_User'=>$KdUser, 'LANGUAGE_ID'=>$Id);

        $result=0;

        if ($query->num_rows()>0)       
            $result = $this->zusers->updateForViewComboBahasaUtility($data);

        if ($result>0)
        {
            echo '{success:true}';
        } else echo '{success:false, pesan: "proses tidak berhasil"}';
        
    }
   
    private function FillRow($rec)
    {
        $row=new Rowviewcobobahasautility;
        $row->LANGUAGE_ID=$rec["language_id"];
        $row->LANGUAGE=$rec["language"];

        return $row;
    }
}

class Rowviewcobobahasautility
{
    //'LANGUAGE_ID','LANGUAGE'
    public $LANGUAGE_ID;
    public $LANGUAGE;

}




?>