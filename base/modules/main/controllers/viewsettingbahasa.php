<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewsettingbahasa extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();        
        
    }	 


    public function index()
    {
        //if ($this->my_usession->logged_in)
        //{
        	//echo '{success:true}';
        
		$this->load->view('main/index');
		            
        //}
        //else
        //{
        //    redirect('user/login');
        //}
		
   }
   	   
    public function save($Params=null)
    {

/**
                     *     var params =
                     * 	{
                     * 		Table: 'ViewSettingBahasa',
                     * 		BhsAsal:selectImportLanguageSettingBahasa,
                     * 		BhsDest:selectLanguageSettingBahasa,
                     * 		List:getArrListSettingBahasa(),
                     * 		JmlField:4,
                     * 		JmlList:dsListKeySettingBahasa.getCount()
                     * 	};
                     */

        $arrList=$Params['List'];
        $JmlField=(int)$Params['JmlField'];
        $JmlList=(int)$Params['JmlList'];

        $result=0;

        $arr[]=$this->GetListDetail($arrList,$JmlField,$JmlList);

        $this->load->model('main/viewsettingbahasamodel');

        foreach ($arr as $dtlRow)
        {

            //$criteria = "\"GROUP_KEY_ID\" = '". $dtlRow['GROUP_KEY_ID']."' AND \"LIST_KEY_ID\" = '".$dtlRow['LIST_KEY_ID']."' AND \"LANGUAGE_ID\" = '".$dtlRow ['LANGUAGE_ID']."'";
            $criteria = "group_key_id = '". $dtlRow['GROUP_KEY_ID']."'
                AND list_key_id = '".$dtlRow['LIST_KEY_ID']."'
                AND language_id = '".$dtlRow ['LANGUAGE_ID']."'";

            $query = $this->viewsettingbahasamodel->readforsure($criteria);

            if ($query->num_rows()==0)
            {
               $result= $this->viewsettingbahasamodel->create($dtlRow);
            }  else $result= $this->viewsettingbahasamodel->update($dtlRow);

        }

        if ($result>0)
        {
            echo '{success:true}';
        } else echo '{success:false, pesan: "proses tidak berhasil"}';
        
    }
   	
    private function GetListDetail($List, $JmlField, $JmlList)
    {

        $arrList = $this->splitListDetail($List,$JmlField,$JmlField);

        $arrListField=array();
        $arrListRow=array();

        if (count($arrList)>0)
        {

            foreach ($arrList as $str)
            {
                for ($i=0;$i<$JmlField;$i+=1)
                {
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }

                if (count($arrListField)>0)
                {
                    $arrListRow['GROUP_KEY_ID']= $arrListField[0];
                    $arrListRow['LIST_KEY_ID']= $arrListField[1];
                    $arrListRow['LANGUAGE_ID']= $arrListField[2];
                    $arrListRow['LABEL']= $arrListField[3];
                }

            }
        }

        return $arrListRow;

    }

    private function splitListDetail($str, $jmlList, $jmlField)
    {
        $splitList = explode("##[[]]##",$str,$jmlList);

        $arrList=array();

        for ($i=0;$i<$jmlList;$i+=1)
        {
                $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
                $arrList=array($splitRecord);
        }

        return $arrList;
    }
	   
   public function read($Params=null)
   {
  		
/**
* 
* 		    params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'STATUS_ID',
            Sortdir: 'ASC',
            target: 'ViewComboStatusVw',
            param: 'where status_id in ' + DefaultValueStatusCMRequestView
        }
*/
   		
   		
        $this->load->model('main/viewsettingbahasamodel');

        $query = $this->viewsettingbahasamodel->readparam($Params[4], $Params[1], $Params[0], $Params[2], $Params[3]);

        $arrResult=array();

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $rows)
            {
                    //$arrResult[] = $rows;
                    $arrResult[] = $this->FillRow($rows);
            }

    //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}

            echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';

        }
        else echo '{success:false}';
   	
   }

    private function FillRow($rec)
    {
        $row=new Rowviewsettingbahasa;
        $row->GROUP_KEY_ID=$rec["group_key_id"];
        $row->LIST_KEY_ID=$rec["list_key_id"];
        $row->LANGUAGE_ID=$rec["language_id"];
        $row->LABEL=$rec["label"];
        $row->LIST_KEY=$rec["list_key"];
        $row->LIST_KEY_DESC=$rec["list_key_desc"];
        $row->LANGUAGE=$rec["language"];

        return $row;
    }
}

class Rowviewsettingbahasa
{
    public $GROUP_KEY_ID;
    public $LIST_KEY_ID;
    public $LANGUAGE_ID;
    public $LABEL;
    public $LIST_KEY;
    public $LIST_KEY_DESC;
    public $LANGUAGE;

}

?>