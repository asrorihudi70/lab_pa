<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewsetupcompany extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        //if ($this->my_usession->logged_in)
        //{
        	//echo '{success:true}';
            $this->load->view('main/index');
        //}
        //else
        //{
        //    redirect('user/login');
        //}

    }

    public function read($Params=null)
    {

        $this->load->model('setup/am_company');

        $query = $this->am_company->readparam($ar_filters=null,$Params[1],$Params[0],$Params[2],$Params[3]);

        $arrResult=array();

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_object() as $rows)
            {
                $arrResult[] = $this->FillRow($rows);
            }

            //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
            echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';

        }
        else echo '{success:false}';

    }

    public function save($Params=null)
    {

//    var params =
//	{
//            Table: 'ViewSetupCompany',
//	    CompId: Ext.get('txtKode_SetCompany').getValue(),
//	    CompName: Ext.get('txtNameSetCompany').getValue(),
//            CompAddress:Ext.get('txtAddressSetCompany').getValue(),
//            CompTelp:Ext.get('txtTlpSetCompany').getValue(),
//            CompFax:Ext.get('txtFaxSetCompany').getValue(),
//            CompCity:Ext.get('txtCitySetCompany').getValue(),
//            CompPos:Ext.get('txtPosSetCompany').getValue(),
//            CompEmail:Ext.get('txtEmailSetCompany').getValue()
//	};

        $CompId = $Params["CompId"];
        $CompName = $Params["CompName"];
        $CompAddress = $Params["CompAddress"];
        $CompTelp = $Params["CompTelp"];
        $CompFax = $Params["CompFax"];
        $CompCity = $Params["CompCity"];
        $CompPos = $Params["CompPos"];
        $CompEmail = $Params["CompEmail"];

        $this->load->model("setup/am_company");
        
        $query = $this->am_company->read($CompId);

        $data = array('COMP_ID'=>$CompId, 'COMP_NAME'=>$CompName,
            'COMP_ADDRESS'=>$CompAddress, 'COMP_TELP'=>$CompTelp,
            'COMP_FAX'=>$CompFax, 'COMP_CITY'=>$CompCity,
            'COMP_POS_CODE'=>$CompPos, 'COMP_EMAIL'=>$CompEmail);

        if ($query->num_rows()==0)
        {
            $result = $this->am_company->create($data);
        } else $result = $this->am_company->update($data);

        if ($result>0)
        {
            echo '{success:true}';
        } else echo '{success:false, pesan: "proses tidak berhasil"}';

    }

    private function FillRow($rec)
    {
        $row=new Rowviewsetupcompany;

        $row->COMP_ID = $rec->comp_id;   
        $row->COMP_NAME = $rec->comp_name;
        $row->COMP_ADDRESS = $rec->comp_address;
        $row->COMP_TELP = $rec->comp_telp;
        $row->COMP_FAX = $rec->comp_fax;
        $row->COMP_CITY = $rec->comp_city;
        $row->COMP_POS_CODE = $rec->comp_pos_code;
        $row->COMP_EMAIL = $rec->comp_email;
        $row->LOGO = $rec->logo;

        return $row;
    }
}

class Rowviewsetupcompany
{
    public $COMP_ID;
    public $COMP_NAME;
    public $COMP_ADDRESS;
    public $COMP_TELP;
    public $COMP_FAX;
    public $COMP_CITY;
    public $COMP_POS_CODE;
    public $COMP_EMAIL;
    public $LOGO;
}


?>