<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class vi_gettrustee extends Model
{

        private $strQuery="SELECT distinct zmodgroup.mod_group, zmodule.Mod_ID,
            zmodule.parent_id as parent_id, zmodule.mod_name, zmodule.mod_desc,
            zmodule.mod_url, zusers.user_names, zmember.kd_user,
            zmodgroup.mod_img, zmodule.app_id, zmodule.mod_key,
            zmodule.mod_imgurl, zmodule.parent_group, zmodule.mod_type,
            zmodgroup.mod_row, zmodule.aktif, zmodule.urut
            from zmodule inner join
            ztrustee on zmodule.Mod_ID = ztrustee.mod_id
            inner join zmember inner join
            zusers on zmember.Kd_User = zusers.Kd_User
            inner join zgroups on zmember.Group_ID = zgroups.Group_ID
            on ztrustee.group_id = zgroups.Group_ID inner join
            zmodgroup on zmodule.mod_group = zmodgroup.mod_group";
        
        //where aktif=1
	//order by  zmodgroup.mod_row,  zmodgroup.mod_group,  zmodule.mod_id ) as trstee";

	function vi_gettrustee()
	{
		parent::Model();
		$this->load->database();
	}


        function readforGetTrustee1($strKd_User, $mod_group)
        {       
                $criteria = " and zmodule.mod_group in ('".$mod_group."') ";
                if ($mod_group === false) {
                        $criteria = "";
                }
                $strWhere= $this->strQuery." where zmember.kd_user = '".$strKd_User."' 
                        and zmodule.mod_type = 1 and zmodule.app_id = 'ASET' ".$criteria."
                        and zmodule.aktif = 1 order by zmodgroup.mod_row, zmodgroup.mod_group, zmodule.urut";

                $query = $this->db->query($strWhere);

                return $query;
        }
    
	function readforGetTrustee2($strKd_User,$strTmpGroup)
	{		
                $strWhere= $this->strQuery." where zmember.kd_user = '".$strKd_User."' 
                    and zmodule.mod_type = 2 and zmodule.app_id = 'ASET'
                    and zmodule.aktif = 1 and zmodule.parent_group = '".$strTmpGroup."'
                    order by zmodgroup.mod_row, zmodgroup.mod_group, zmodule.mod_id, zmodule.urut";

                $query = $this->db->query($strWhere);

                return $query;
	}

	function readforGetModule($strKd_User, $Mod_ID)
	{		
		//$this->db->order_by('Mod_Group', 'Mod_ID');
                //$this->db->order_by('mod_group', 'mod_id');
		//$this->db->where('Kd_User', $strKd_User);
                //$this->db->where('kd_user', $strKd_User);
		//$this->db->where('Mod_ID', $Mod_ID);
                //$this->db->where('mod_id', $Mod_ID);
	
		//$query = $this->db->get('dbo.vi_gettrustee');
                //$query = $this->get($this->strQuery);

                $strWhere= $this->strQuery." where 
                --  zmember.kd_user = '".$strKd_User."' and 
                    zmodule.mod_id = '".$Mod_ID."'
                    order by zmodgroup.mod_group, zmodule.mod_id";

                $query = $this->db->query($strWhere);

		return $query;
	}


}



?>