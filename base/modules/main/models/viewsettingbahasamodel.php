<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class viewsettingbahasamodel extends Model
{

    function viewsettingbahasamodel()
    {
        parent::Model();
        $this->load->database();
    }

    function readforsure($Params)
    {

        $this->db->where($Params, null, false);
        //$query = $this->db->get('dbo.AM_GROUP_KEY_LANGUAGE');
        $query = $this->db->get('am_group_key_language');

        return $query;
    }
		
    function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {
    	$Params="";

        if ($ar_filters != null)
        {
            if (is_array($ar_filters))
            {
                foreach ($ar_filters as $field => $value)
                {
                    //$this->db->where(strtolower($field), $value);
                    $this->db->where($field, $value);
                }
            } else
            {
                If ($ar_filters!="")
                {
                    //list() = Split(param, "%^%", 2)
                    $p = explode("%^%", $ar_filters);

                    If (count($p) == 2)
                    {
                        //$Params = "\"AM_GROUP_KEY_LANGUAGE\".\"GROUP_KEY_ID\" = '".$p[0]."'";
                        $Params = "am_group_key_language.group_key_id = '".$p[0]."'";

                        $this->load->model('main/getidbahasa');
                        $query = $this->getidbahasa->read($p[1]);

                        if ($query->num_rows() > 0)
                        {
                            $row = $query->row();
                            //$Params.=" AND \"AM_GROUP_KEY_LANGUAGE\".\"LANGUAGE_ID\" = '".$row->LANGUAGE_ID."'";
                            $Params.=" and am_group_key_language.language_id = '".$row->language_id."'";

                        };

                    } else
                    {
                        //$Params = str_replace($ar_filters, "'" ,"~" ); //$this->db->where();
                        $Params = str_replace("~" ,"'",$ar_filters ); //$this->db->where();

                    }

                    $this->db->where($Params, null, false);

                }

            }

        }


        $this->db->select("*");

        //$this->db->from("dbo.AM_GROUP_KEY_LANGUAGE");
        $this->db->from("am_group_key_language");
        //$this->db->join("dbo.AM_LIST_KEY",
        //        "AM_GROUP_KEY_LANGUAGE.LIST_KEY_ID = AM_LIST_KEY.LIST_KEY_ID","inner");
        $this->db->join("am_list_key",
                "am_group_key_language.list_key_id = am_list_key.list_key_id","inner");
        //$this->db->join("dbo.AM_LANGUAGE",
        //        "AM_GROUP_KEY_LANGUAGE.LANGUAGE_ID = AM_LANGUAGE.LANGUAGE_ID","inner");
        $this->db->join("am_language",
                "am_group_key_language.language_id = am_language.language_id","inner");

        if ($ar_sort != null)
        {
            if (is_array($ar_sort))
            {
                foreach ($ar_sort as $field) {
                    $this->db->orderby(strtolower($field), $sortdir);
                }
            } else $this->db->orderby(strtolower ($ar_sort), $sortdir);
        }

        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }

        $query = $this->db->get();

        return $query;
    }

        // akan diganti dengan menggunakan table helper
    
	function create($data)
	{
            //$this->db->set('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
            $this->db->set('group_key_id', $data['GROUP_KEY_ID']);
            //$this->db->set('LIST_KEY_ID', $data['LIST_KEY_ID']);
            $this->db->set('list_key_id', $data['LIST_KEY_ID']);
            //$this->db->set('LANGUAGE_ID', $data['LANGUAGE_ID']);
            $this->db->set('language_id', $data['LANGUAGE_ID']);
            //$this->db->set('LABEL', $data['LABEL']);
            $this->db->set('label', $data['LABEL']);
            //$this->db->insert('dbo.AM_GROUP_KEY_LANGUAGE');
            $this->db->insert('am_group_key_language');

            //echo '{success:true,criteria:'.$this->db->affected_rows().'}';
            return $this->db->affected_rows();
		
	}
	
	function update($data)
	{
            //$this->db->where('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
            $this->db->where('group_key_id', $data['GROUP_KEY_ID']);
            //$this->db->where('LIST_KEY_ID', $data['LIST_KEY_ID']);
            $this->db->where('list_key_id', $data['LIST_KEY_ID']);
            //$this->db->where('LANGUAGE_ID', $data['LANGUAGE_ID']);
            $this->db->where('language_id', $data['LANGUAGE_ID']);
            //$this->db->set('LABEL', $data['LABEL']);
            $this->db->set('label', $data['LABEL']);
            //$this->db->update('dbo.AM_GROUP_KEY_LANGUAGE');
            $this->db->update('am_group_key_language');

            return $this->db->affected_rows();
	}
	    
}



?>