<?php
	$this->load->library('minify');
/**
 * @author Ali
 * @copyright NCI 2010
 */

//pasang script load js mainpage disini

/**
 *      <script type="text/javascript">
 *          strUser = '<%=Me.User.Identity.Name%>'
 *          strIDBahasa ='<%=Request.QueryString("Lang")%>'
 *      </script>
 */
?>
<?php if ($this->_ci_cached_vars['controller']->_is_logged_in()) { ?>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>MediSmart</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/aset-def.css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/fileuploadfield.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/RowEditor.css" rel="stylesheet" type="text/css" />
</head>

<body >
	<div id="loading" align="center" style="margin-top:250px; "  > 
	<center>
    <img alt = "" src="<?php echo base_url(); ?>ui/images/default/shared/blue-loading.gif" width="32" height="32"  style="float:center;vertical-align:middle;"/><br />
    <span id="loading-msg"></span>
	</center>
	</div> 
	
    <script type="text/javascript">
        var strUser;
        var baseURL='<?php echo base_url(); ?>';
        var strModul = 'NCI-MediSmart';
        document.getElementById('loading-msg').innerHTML = 'Loading Core Component... <br>' + strModul;
    </script>		
    
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/adapter/ext/ext-base.js"></script>
    
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-base.js"></script> -->
 	
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-all.js"></script> -->
    
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/ext-all.js"></script>
	<?php $this->minify->press(array('ui/Scripts/CommonRenderer.js')); ?>
	  <!--  
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CommonRenderer.js"></script>
	
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/HeaderCompany.js"></script>
    -->
	<?php $this->minify->press(array('ui/Scripts/StatusBar.js')); ?>
	<?php $this->minify->press(array('ui/Scripts/menu.js')); ?>
	<?php $this->minify->press(array('ui/Scripts/Penamaan.js')); ?>
	<?php $this->minify->press(array('ui/Scripts/Language.js')); ?>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/LookupEmployee.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/InputLogMetering.js"></script>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/WarningLogMetering.js"></script> -->
	<?php $this->minify->press(array('ui/Scripts/Trustee.js')); ?>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Trustee.js"></script> -->
	<?php $this->minify->press(array('ui/Scripts/App.js')); ?>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/App.js"></script> -->
	<?php $this->minify->press(array('ui/Scripts/Module.js')); ?>
    <script type="javascript" src="<?php echo base_url(); ?>ui/Scripts/SessionProvider.js"></script>
    
	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = ' Initialization......... <br> ' + strModul;</script>
			
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GroupSummary.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FileUploadField.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/cook.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowExpander.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowEditor.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/swfobject.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/flashplugin.js"></script>
	<?php $this->minify->press(array('ui/Scripts/UtilityAset.js')); ?>
	<!--<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/UtilityAset.js"></script>-->
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GantiPass.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/SettingBahasa.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FilterRow.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CheckColumn.js"></script>
	
	<script type="text/javascript">
	var msgd =document.getElementById('loading').style.visibility='hidden';
	</script>
	<div id="bd"> 
	<div>
    	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPanel.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPage.js"></script>
    </div>   
	</div>    
	 		
</body> 
</html>
<script>

Ext.onReady(function () {
    if(Ext.isChrome===true){       
        var chromeDatePickerCSS = ".x-date-picker {border-color: #1b376c;background-color:#fff;position: relative;width: 185px;}";
        Ext.util.CSS.createStyleSheet(chromeDatePickerCSS,'chromeDatePickerStyle');
    }
});
</script>
<?php } else { ?>

<!-- =================================================================== LOGIN =============================================== --><!DOCTYPE html>
<html lang="en">
<head>
  <title>Login V1</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/animate/animate.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/select2/select2.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/css/util.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/css/main.css">
<!--===============================================================================================-->
</head>
<body>
  
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <!-- <div style="margin-top:-100px;">
            <h2 style="font-color: #2f8b4e;">Rumah Sakit Universitas Andalas Padang Sumatera Barat</h2>
        </div> -->
        <div style="margin-top:-120px;padding-left:50px;">
            <img src="<?php echo base_url(); ?>Img Asset/img login/header_1.png" width="220">
            <img src="<?php echo base_url(); ?>Img Asset/img login/header_2.png" width="220">
            <img src="<?php echo base_url(); ?>Img Asset/img login/header_3.png" width="220">
          <!-- <div style="min-width:186px; min-height: 109; background-image: url('<?php //echo base_url(); ?>Img Asset/img login/LoginForm_Medismart_Slice_01.png'); background-repeat: no-repeat; "></div>
          <div style="min-width:186px; min-height: 109; background-image: url('<?php //echo base_url(); ?>Img Asset/img login/LoginForm_Medismart_Slice_02.png'); background-repeat: no-repeat; "></div> -->
        </div>
        <!-- <div class="login100-pic js-tilt" data-tilt> -->
          <!-- <img src="<?php //echo base_url(); ?>/Img Asset/logo.png" alt="IMG"> -->
        <!-- </div> -->
        <div align="center" style="padding-left: 200px;">
          <form class="login100-form validate-form" id="formID" method="post" action="<?php echo base_url() ?>index.php/main/dashboard">
            <span class="login100-form-title">
              Sistem Informasi Manajemen Rumah Sakit
            </span>

            <div class="wrap-input100 validate-input" data-validate = "Username">
              <input class="input100" type="text" name="UserName" id="UserName" placeholder="Username">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-user" aria-hidden="true"></i>
              </span>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "Password is required">
              <input class="input100" type="password" name="Password" id="Password" placeholder="Password">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-lock" aria-hidden="true"></i>
              </span>
            </div>
            
            <div class="container-login100-form-btn">
              <button class="login100-form-btn">
                Login
              </button>
            </div>

            <div class="text-center p-t-12">
              <span class="txt1">
                <!-- Forgot -->
              </span>
              <!-- <a class="txt2" href="#">
                Username / Password?
              </a> -->
            </div>

            <div class="text-center p-t-136">
              <!-- <a class="txt2" href="#">
                Create your Account
                <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
              </a> -->
            </div>
          </form> 
        </div>
      </div>
    </div>
  </div>
  
  

  
<!--===============================================================================================-->  
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/bootstrap/js/popper.js"></script>
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/tilt/tilt.jquery.min.js"></script>
  <script >
    $('.js-tilt').tilt({
      scale: 1.1
    })
  </script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/js/main.js"></script>

</body>
</html>

<script type="text/javascript">
$(document).ready(function(e) {
	$("#UserName").focus();
	$('#form1').show();
	$('#form3').hide();
	var w = $('#form1').width();
	var h = $('#form1').height();
	$('#form3').css({'width':'100%','height':h});
	$('#link2').click(function(e) {
		$('#form1').stop().animate().hide(500);
		$('#form3').stop().animate().hide(500);
	});
	$('#link1').click(function(e) {
		$('#form1').stop().animate().show(500);
		$('#form3').stop().animate().hide(500);
	});
	$( "#login" ).click(function( e ) {
		var nama = $("#UserName").val();	
		var pass = $("#Password").val();	
		if(nama == ''){
			alert('Username belum diisi');
			e.preventDefault();
		}else if(pass == ''){
			alert('Password belum diisi');
			e.preventDefault();
		}else{
			$('#form1').stop().animate().hide();
			$("#text-user").empty().append('<b>Selamat Datang, '+nama+'</b>');
			$('#form3').stop().animate().show();	
			var timer = setInterval(function(){
				complete();
				clearInterval(timer);
			},2000);
		}
	});
});
$("#Password").keypress(function (e) {
	var key = e.which;
	if(key == 13){
		e.preventDefault();  
		$("#login").click();
	}
});
$("#UserName").keypress(function (e) {
	var key = e.which;
	if(key == 13){
		e.preventDefault(); 
		$("#login").click();
	}
});   
function complete(){
	$('#formID').submit();
}
</script>

<?php } ?>
