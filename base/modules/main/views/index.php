<?php
$this->load->library('minify');
/**
 * @author Ali
 * @copyright NCI 2010
 */

//pasang script load js mainpage disini

/**
 *      <script type="text/javascript">
 *          strUser = '<%=Me.User.Identity.Name%>'
 *          strIDBahasa ='<%=Request.QueryString("Lang")%>'
 *      </script>
 */
?>
<?php if ($this->_ci_cached_vars['controller']->_is_logged_in()) { ?>

  <html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Nci - Medismart</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/jquery-2.1.4.min.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>ui/images/Logo/LOGO.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/aset-def.css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/fileuploadfield.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/RowEditor.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>ui/css/font-awesome/css/font-awesome.min.css">
    <style type="text/css">
      .grid-row-span .x-grid3-row {
        border-bottom: 0;
      }

      .grid-row-span .x-grid3-col {
        border-bottom: 1px solid #ededed;
      }

      .grid-row-span .row-span {
        border-bottom: 1px solid #fff;
      }

      .grid-row-span .row-span-first {
        position: relative;
      }

      .grid-row-span .row-span-first .x-grid3-cell-inner {
        position: absolute;
      }

      .grid-row-span .row-span-last {
        border-bottom: 1px solid #ededed;
      }
    </style>
  </head>

  <body>
    <div id="loading" align="center" style="margin-top:250px; ">
      <center>
        <img alt="" src="<?php echo base_url(); ?>ui/images/default/shared/blue-loading.gif" width="32" height="32" style="float:center;vertical-align:middle;" /><br />
        <span id="loading-msg"></span>
      </center>
    </div>
    <script type="text/javascript">
      var strUser;
      var baseURL = '<?php echo base_url(); ?>';
      var strModul = 'NCI-MediSmart';
      document.getElementById('loading-msg').innerHTML = 'Loading Core Component... <br>' + strModul;
    </script>

    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/adapter/ext/ext-base.js"></script>

    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-base.js"></script> -->

    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-all.js"></script> -->

    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/ext-all.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/component.js"></script>

    <?php $this->minify->press(array('ui/Scripts/CommonRenderer.js')); ?>
    <!--  
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CommonRenderer.js"></script>
	
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/HeaderCompany.js"></script>
    -->
    <?php $this->minify->press(array('ui/Scripts/StatusBar.js')); ?>
    <?php $this->minify->press(array('ui/Scripts/menu.js')); ?>
    <?php $this->minify->press(array('ui/Scripts/Penamaan.js')); ?>
    <?php $this->minify->press(array('ui/Scripts/Language.js')); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/LookupEmployee.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/InputLogMetering.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/WarningLogMetering.js"></script> -->
    <?php //$this->minify->press(array('ui/Scripts/Trustee.js')); 
    ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Trustee.js"></script>
    <?php $this->minify->press(array('ui/Scripts/App.js')); ?>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/App.js"></script> -->
    <?php $this->minify->press(array('ui/Scripts/Module.js')); ?>
    <script type="javascript" src="<?php echo base_url(); ?>ui/Scripts/SessionProvider.js"></script>

    <script type="text/javascript">
      document.getElementById('loading-msg').innerHTML = ' Initialization......... <br> ' + strModul;
    </script>

    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GroupSummary.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FileUploadField.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/cook.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowExpander.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowEditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/src/widgets/grid/GroupingView.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/swfobject.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/flashplugin.js"></script>
    <?php $this->minify->press(array('ui/Scripts/UtilityAset.js')); ?>
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/UtilityAset.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GantiPass.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/SettingBahasa.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FilterRow.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CheckColumn.js"></script>
    <?php $this->minify->press(array('ui/Scripts/Plugin.js')); ?>
    <script type="text/javascript">
      var msgd = document.getElementById('loading').style.visibility = 'hidden';
    </script>
    <div id="bd">
      <div>
        <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPanel.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPage.js"></script>
      </div>
    </div>

  </body>

  </html>
  <script>
    Ext.onReady(function() {
      if (Ext.isChrome === true) {
        var chromeDatePickerCSS = ".x-date-picker {border-color: #1b376c;background-color:#fff;position: relative;width: 185px;}";
        Ext.util.CSS.createStyleSheet(chromeDatePickerCSS, 'chromeDatePickerStyle');
      }
    });
  </script>
<?php } else { ?>

  <!-- =================================================================== LOGIN =============================================== -->
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Nci - Medismart</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>ui/images/Logo/LOGO.png" />
    <style type="text/css">
      a {
        text-decoration: none;
      }

      a:link,
      a:visited {
        color: #587EBF;
      }

      a:hover,
      a:active {
        color: #587EBF;
        font-weight: bold;
      }

      a:hover {
        background-color: transparent;
      }

      .login-footer {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 10px;
        font-style: italic;
        font-weight: normal;
        color: #e2e2e2;
        padding-left: 5px;
        padding-top: 5px;
      }



      body {
        margin-left: 0;
        margin-top: 0;
        margin-bottom: 0;
        margin-right: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
      }

      .footer {
        background: url("<?php echo base_url(); ?>Img Asset/img login/bg-big.png") -15px -20px no-repeat,
          url("<?php echo base_url(); ?>Img Asset/img login/bg-big-bottom.png") bottom right no-repeat,
          linear-gradient(140deg, #1C7C3D, #7AB12B);
        color: #fff;
        padding: 15px 0;
      }

      h2,
      h3,
      h4 {
        font-family: Arial, Helvetica, sans-serif;
      }
    </style>
  </head>

  <body>


    <table border="0" cellspacing="0" cellpadding="0" height=100% width=100%>
      <tr bgcolor="#1c7c3d">
        <td align="left">&nbsp;</td>
      </tr>


      <tr height="500">
        <td align="center">&nbsp;

          <!-- <div>
            <table id="Login1" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;">
              <tr>
                <td>
                  <div style="width:650px;height:400px;background-image:url('../../Img Asset/img main/background-login.jpg');">
                    <div style="width: 650px;height:140px;">
                      <table width="100%" border="0" cellpadding="5">
                        <tr>
                          <td width="30%" align="right"><img src="<?php echo base_url(); ?>ui/images/Logo/LOGO.png" width="70" /></td>
                          <td>
                            <h2><?php echo $data_rs->row()->NAME; ?></h2>
                            <h4><?php echo $data_rs->row()->ADDRESS; ?></h4>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="width: 650px;height:25px;" align="center">
                      <b>Sistem Managemen Rumah Sakit</b>
                      <hr>
                      <?php //echo var_dump($data_rs->result()); 
                      ?>
                    </div>
                    <div style="width: 650px;min-height:180px;max-height:180px;">
                      <table width="100%" border="0" cellpadding="3" cellspacing="0">
                        <tr>
                          <td width="50%">
                            <form id="formID" method="post" action="<?php echo base_url() ?>index.php/main/menu">
                              <table width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr>
                                  <td valign="top"><label for="Login1_UserName" id="Login1_Label1">Username :</label></td>
                                  <td valign="top"><input name="UserName" type="text" id="UserName" style="width:150px;" />
                                    <span id="Login1_RequiredFieldValidator1" title="User Name is required." style="color:Red;display:none;"></span>
                                  </td>
                                </tr>
                                <tr>
                                  <td valign="top"><label for="Login1_Password" id="Login1_Label2">Password :</label></td>
                                  <td valign="top"><input name="Password" type="password" id="Password" style="width:150px;" />
                                    <span id="Login1_RequiredFieldValidator2" title="Password is required." style="color:Red;display:none;"></span>
                                  </td>
                                </tr>
                                <tr>
                                  <td valign="top"><button type="button" id="login" name="btn" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Login&nbsp;&nbsp;</button></td>
                                  <td valign="top"></td>
                                </tr>
                              </table>
                            </form>
                          </td>
                          <td width="50%" align="center" valign="top">
                            <b>Rumah Sakit</b><br>
                            <b><?php echo $data_rs->row()->NAME; ?></b><br>
                            <?php echo $data_rs->row()->ADDRESS; ?><br>
                            Telepon : <?php echo $data_rs->row()->PHONE1; ?><br>
                            Fax : <?php echo $data_rs->row()->FAX; ?><br>
                            Website : <?php  ?><br>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div align="center">
                      Software Developer:<br>
                      PT. Nuansa Cerah Informasi<br>
                      Bandung - Jawa Barat - Indonesia.
                    </div>

                  </div>
                </td>
              </tr>
            </table>
          </div> -->

          
          <script>
            alert('Silahkan Login Terlebih Dahulu..');
          </script>

        </td>
      </tr>



    </table>

    <?php
    if (isset($error)) {
    ?>
      <script type="text/javascript">
        document.getElementById("Login_Validator").style.display = "";
        document.getElementById("Login_Validator").innerHTML = "<?php echo $error; ?>";
        // alert("");
      </script>
    <?php
    }
    ?>

    <div class="footer" align="center" style="position:fixed; bottom:0;width: 100%;">
      Power by :<br /><img src="<?php echo base_url(); ?>Img Asset/img login/MediSmart.png" width="289" height="64" />
    </div>

  </body>
  <!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/vendor/tilt/tilt.jquery.min.js"></script>
  <!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>/Doc Asset/css login/version 1/js/main.js"></script>

  </html>


  <script type="text/javascript">
    localStorage.setItem('mod_group', false);
    $(document).ready(function(e) {
      $("#UserName").focus();
      $('#form1').show();
      $('#form3').hide();
      var w = $('#form1').width();
      var h = $('#form1').height();
      $('#form3').css({
        'width': '100%',
        'height': h
      });
      $('#link2').click(function(e) {
        $('#form1').stop().animate().hide(500);
        $('#form3').stop().animate().hide(500);
      });
      $('#link1').click(function(e) {
        $('#form1').stop().animate().show(500);
        $('#form3').stop().animate().hide(500);
      });
      $("#login").click(function(e) {
        var nama = $("#UserName").val();
        var pass = $("#Password").val();
        if (nama == '') {
          alert('Username belum diisi');
          e.preventDefault();
        } else if (pass == '') {
          alert('Password belum diisi');
          e.preventDefault();
        } else {
          $('#form1').stop().animate().hide();
          $("#text-user").empty().append('<b>Selamat Datang, ' + nama + '</b>');
          $('#form3').stop().animate().show();
          var timer = setInterval(function() {
            complete();
            clearInterval(timer);
          }, 2000);
        }
      });
    });
    $("#Password").keypress(function(e) {
      var key = e.which;
      if (key == 13) {
        e.preventDefault();
        $("#login").click();
      }
    });
    $("#UserName").keypress(function(e) {
      var key = e.which;
      if (key == 13) {
        e.preventDefault();
        $("#login").click();
      }
    });

    function complete() {
      $('#formID').submit();
    }
  </script>

<?php } ?>