<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nci - Medismart</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>ui/images/Logo/LOGO.png" />
    <style type="text/css">
        a { text-decoration:none; font-family:Verdana, Geneva, sans-serif; font-size:14px; }
        a:link, a:visited { color: #000000; }
        a:hover, a:active { color: #587EBF; text-decoration:underline; }
        a:hover { background-color: transparent; }
    td { text-decoration:none; font-family:Verdana, Geneva, sans-serif; font-size:14px; }
    body {background-image:url("<?php echo base_url(); ?>Img Asset/img main/bgMenu.jpg"); background-size:cover; }
    </style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/Doc Asset/css login/version 1/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
</head>
<?php
  $trustee = array(); 
  $query = "SELECT DISTINCT
  zmodgroup.mod_group
FROM
  zmodule
  INNER JOIN ztrustee ON zmodule.mod_id = ztrustee.mod_id
  INNER JOIN zmember
  INNER JOIN zusers ON zmember.kd_user = zusers.kd_user
  INNER JOIN zgroups ON zmember.group_id = zgroups.group_id ON ztrustee.group_id = zgroups.group_id
  INNER JOIN zmodgroup ON zmodule.mod_group = zmodgroup.mod_group
  where zmember.kd_user = '".$data->row()->kd_user."' 
                    and zmodule.mod_type = 1 and zmodule.app_id = 'ASET'
                    and zmodule.aktif = 1 order by zmodgroup.mod_group";
  $query = $this->db->query($query);
  $bagian_apotek = 6;
  if ($query->num_rows() > 0) {
    foreach ($query->result() as $result) {
      array_push($trustee, strtolower($result->mod_group));
    }
    $bagian_apotek = $data->row()->kd_unit_far;
  }

  //echo var_dump($trustee);
?>
<body style="padding: 0px;margin: 0px;">
<center>
<p></p>
<table width="100%" border="0" cellspacing="1" cellpadding="1">
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <tr>
    <td colspan="2" width="45%" align="left">
      <table width="100%" border="0" height="100%">
        <tr>
          <?php 
            $user_icon = "user-dashboard.png";
          ?>
          <td width="25%" align="center" style="padding: 20px 15px 20px 15px ;"><img src="<?php echo base_url(); ?>Img Asset/<?php echo $user_icon; ?>"></td>
          <td valign="top" style="padding: 15px 10px 15px 10px ;">
            Nama : <b><?php echo $data->row()->full_name; ?></b><br><br>
            <font style="font-size: 10px;">(<a href="<?php echo base_url(); ?>index.php/main/Logoff" style="font-size: 10px;">Logout</a>)</font>
           
            
          </td>
        </tr>
      </table>
    </td>
    <td width="10%"></td>
    <td width="15%" align="right"><img src="<?php echo base_url(); ?>Img Asset/rsunandlogo-small.png"  longdesc="http://http://rsp.unand.ac.id/" width="75" /></td>
    <td width="30%" align="left" valign="top" style="padding-left:15px;"><font color="#fff" style="font-family: 'Arial';">
      <h2><?php echo $data_rs->row()->name; ?></h2>
      <h3 style="margin-top: -15px;"><?php echo $data_rs->row()->city; ?>, <?php echo $data_rs->row()->state; ?>, <?php echo $data_rs->row()->country; ?></h3>
      <?php echo $data_rs->row()->address; ?></font></td>
  </tr>
  <?php 
    // $query = $this->db->query("SELECT distinct(grup_menu) FROM zsetting_menu");
    // if ($query->num_rows() > 0) {
    //   foreach ($query->result() as $grup) {
    //     $query_child = $this->db->query("SELECT * FROM zsetting_menu where grup_menu = '".$grup->grup_menu."'");
      ?>
        <!-- <tr>
          <td width="5%">&nbsp;</td>
          <td colspan="3" align="center" bgcolor="#006633"><b><font color="#FFFFFF"><?php // echo strtoupper(substr($grup->grup_menu, 0, 1)).substr($grup->grup_menu, 1); ?> Office</font></b></td>
          <td width="5%">&nbsp;</td>
        </tr> -->
      <?php 
    //   }
    // }
  ?>
  <!--  ======================================================================================== FRONT OFFICE =========================================== -->
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>
  <tr>
    <td colspan="5" align="center" bgcolor="#006633" style="padding: 5px 10px 5px 10px ;"><b><font color="#FFFFFF">Front Office (Pelayanan)</font></b></td>
  </tr>
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <tr>
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Rawat Jalan"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/2" onclick="storage('Rawat Jalan', '2');">Rawat Jalan&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Rawat Jalan"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    <td width="10%">&nbsp;</td>
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Gawat Darurat"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/3" onclick="storage('Gawat Darurat', '3');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Gawat Darurat"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Instalasi Gawat Darurat</a></td>
  </tr>

  <tr>
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Rawat Inap"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/1" onclick="storage('Rawat Inap', '1');">Rawat Inap&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Rawat Inap"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    <td width="10%">&nbsp;</td>
    <!-- <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Kasir"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/3" onclick="storage('Kasir', '3');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Kasir"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Kasir</a></td> -->
  </tr>
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>
  <!--  ======================================================================================== PENUNJANG MEDIS =========================================== -->

  <tr>
    <td colspan="5" align="center" bgcolor="#006633" style="padding: 5px 10px 5px 10px ;"><b><font color="#FFFFFF">Penunjang Medis</font></b></td>
  </tr>
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <tr>
  <!--  
  <td colspan="2" width="45%" align="right">
       <a style="<?php echo in_array(strtolower("Kamar Operasi"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Kamar Operasi', '72');">Bedah dan Anastesi&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Kamar Operasi"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
-->
  <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Radiologi"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Radiologi', '5');">Radiologi&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Radiologi"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    


    <td width="10%">&nbsp;</td>
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Laboratorium Klinik"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Laboratorium Klinik', '4');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Laboratorium Klinik"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Laboratorium Klinik</a></td>
  </tr>

  <tr>
    <!--
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Radiologi"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Radiologi', '5');">Radiologi&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Radiologi"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    -->
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Apotek"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Apotek', '<?php echo $bagian_apotek; ?>');">Apotek&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Apotek"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    
    <td width="10%">&nbsp;</td>
    <!--
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Endoscopy"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Endoscopy', '72');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Endoscopy"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Endoscopy</a></td>
    -->
    <td colspan="2" width="45%" align="left">
    <a style="<?php echo in_array(strtolower("Endoscopy"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>height="15" /></a></td>


  </tr>

  <!-- 
  <tr>
  
  <!--
 
  <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Radiotherapy"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Radiotherapy', '9');">Radiotherapy&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Radiotherapy"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    -->
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Radiotherapy"), $trustee)?"":"pointer-events: none;color: #737373;"; ?> height="15" /></a></td>
  
    <td width="10%">&nbsp;</td>
    
    <!--
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Fisiotherapy"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Fisiotherapy', '74');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Fisiotherapy"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Rehab Medik</a></td>
    -->

      <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Fisiotherapy"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>"height="15" /></a></td>
 
  </tr>
-->

  <tr>
  <!--  
  <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Apotek"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Apotek', '<?php echo $bagian_apotek; ?>');">Apotek&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Apotek"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    -->
      <td width="10%">&nbsp;</td>
  <!--
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Gizi Pasien"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Gizi Pasien','77');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Gizi Pasien"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Gizi</a></td>
    -->
      <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Gizi Pasien"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>height="15" /></a></td>
  
  
    </tr>

  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <!--  ======================================================================================== Back Office =========================================== -->

  <tr>
    <td colspan="5" align="center" bgcolor="#006633" style="padding: 5px 10px 5px 10px ;"><b><font color="#FFFFFF">Back Office</font></b></td>
  </tr>
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <tr>
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Gudang Farmasi"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Gudang Farmasi', '6');">Gudang Medik ( Farmasi )&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Gudang Farmasi"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    <td width="10%">&nbsp;</td>
  <!--
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Inventaris"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Inventaris');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Inventaris"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Gudang non Medik ( Logistik )</a></td>
    -->
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Medical Record"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Medical Record');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Medical Record"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Rekam Medis</a></td>
   
    </tr>

  <tr>
  <!--  
  <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Akuntansi"), $trustee)||in_array(strtolower("Anggaran"), $trustee)||in_array(strtolower("Keuangan"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage_array(['Anggaran','Akuntansi','Keuangan','Bendahara','Master Data']);">Keuangan  &nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Akuntansi"), $trustee)||in_array(strtolower("Anggaran"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    -->
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Tracer"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Tracer');">Tracer  &nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Tracer"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    
    
      <td width="10%">&nbsp;</td>
    <!--
      <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Medical Record"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Medical Record');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Medical Record"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Rekam Medis</a></td>
    -->
  </tr>

  <tr>
  <!--  
  <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Tracer"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Tracer');">Tracer  &nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Tracer"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    -->
      <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Tracer"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>height="15" /></a></td>
    
      <td width="10%">&nbsp;</td>

      <!--
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Persediaan BHP"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Persediaan BHP');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Persediaan BHP"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Persediaan BHP - Non Medik</a></td>
    -->
      <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("Persediaan BHP"), $trustee)?"":"pointer-events: none;color: #737373;"; ?> height="15" /></a></td>
    
  </tr>

  <!--
  <tr>
  <!--  
  <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Siranap"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Siranap');">Siranap  &nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Siranap"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    -->
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Siranap"), $trustee)?"":"pointer-events: none;color: #737373;"; ?> height="15" /></a></td>
    
    
      <td width="10%">&nbsp;</td>
      <!--
    <td colspan="2" width="45%" align="left"><a style="<?php echo in_array(strtolower("Sisrute"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Sisrute');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Sisrute"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;Sisrute</a></td>
    -->
    <td colspan="2" width="45%" align="left"><a style="<?php echo in_array(strtolower("Sisrute"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>height="15" /></a></td>
    
  </tr>
    -->
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <!--  ======================================================================================== Sistem Administrator =========================================== -->

  <tr>
    <td colspan="5" align="center" bgcolor="#006633" style="padding: 5px 10px 5px 10px ;"><b><font color="#FFFFFF">Sistem Administrator</font></b></td>
  </tr>
  <tr><td colspan="5"><img src="spacer.png" width="1" height="5" /></td></tr>

  <tr>
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Setup"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Setup');">Setup&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Setup"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    <td width="10%">&nbsp;</td>
    <td colspan="2" width="45%" align="left">
      <a style="<?php echo in_array(strtolower("User Manager"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('User Manager');"><img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("User Manager"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" />&nbsp;User Manager</a></td>
  </tr>
  <tr>
    <td colspan="2" width="45%" align="right">
      <a style="<?php echo in_array(strtolower("Setup Farmasi"), $trustee)?"":"pointer-events: none;color: #737373;"; ?>" href="<?php echo base_url(); ?>index.php/main/login/" onclick="storage('Setup Farmasi');">Setup Farmasi&nbsp;<img src="<?php echo base_url(); ?>Img Asset/img main/<?php echo in_array(strtolower("Setup Farmasi"), $trustee)?"uc_ready.png":"uc_warn.png"; ?>" height="15" /></a></td>
    <td width="10%">&nbsp;</td>
    <td colspan="2" width="45%" align="left"></td>
  </tr>
<!-- 
  <tr>
    <td colspan="2" align="right"><a href="">Laboratorium</a>&nbsp;<img src="<?php //echo base_url(); ?>Img Asset/img main/uc_warn.png" height="15" /></td>
    <td>&nbsp;</td>
    <td colspan="2" align="left"><img src="<?php //echo base_url(); ?>Img Asset/img main/uc_warn.png" height="15" />&nbsp;<a href="">Radiologi</a></td>
  </tr> -->

 </table>
  <!--  ======================================================================================== FOOTER =========================================== -->
  <div style="position:fixed; bottom:0; width: 100%; background-color: #006633;" align="center">
    <img src="<?php echo base_url(); ?>Img Asset/MediSmart.png" width="289" height="64" longdesc="http://ncimedismart.com" />
  </div>
<script type="text/javascript">
  function storage(mod_group, kd_bagian = null){
    var tmp_mod_group = "'"+mod_group+"'";
    var baseURL = "<?php echo base_url(); ?>";
    localStorage.setItem('mod_group', tmp_mod_group);
    localStorage.setItem('kd_bagian', kd_bagian);
      $.post(baseURL+"index.php/main/main/cek_trustee",
      {
          // name: "Donald Duck",
          mod_group: tmp_mod_group,
      },
      function(data, status){
          // alert("Data: " + data + "\nStatus: " + status);
      });
    // alert(localStorage.getItem('mod_group'));
  }
  function storage_array(mod_group){
    var tmp_mod_group = "";
    var baseURL = "<?php echo base_url(); ?>";
    for (var i = 0; i < mod_group.length ; i++) {
      tmp_mod_group += "'"+mod_group[i]+"',";
      
    }
    tmp_mod_group = tmp_mod_group.substring(0, tmp_mod_group.length-1);
    localStorage.setItem('mod_group', tmp_mod_group);
    $.post(baseURL+"index.php/main/main/cek_trustee",
    {
        // name: "Donald Duck",
        mod_group: tmp_mod_group,
    },
    function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
    });
    // alert(localStorage.getItem('mod_group'));
  }
</script>
</center>
</body> 
</html>
