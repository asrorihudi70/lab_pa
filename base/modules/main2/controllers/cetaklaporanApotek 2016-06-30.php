<?php
class cetaklaporanApotek extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
    public function index()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
    public function cetaklaporan()
    {
        
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
        
    }
    
    public function LapKasirDetail($UserID,$Params){
		$UserID = '0';
		$common=$this->common;
		$Split = explode("##@@##", $Params, 17);
		//print_r ($Split);
		/* //all shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
	/* 	//1 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1 */
		
		/* //2 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2	 */	
		
		/* //3 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
		/* //semua
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => Semua
		[5] => Unit
		[6] => Semua
		[7] => Semua
		[8] => operator
		[9] => Semua
		[10] => Semua
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$asalpasien = $Split[4];
			$unit = $Split[6];
			$user = $Split[9];
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
					
			/* where tgl_out between '' and ''
				and kd_unit=''
				and kd_unit_far=''
				and opr=
			ok	and shiftapt= */
			
                    //-------------------------------- jumlah shift 1 -----------------------------------------------------------------------------------------------
					if (count($Split) === 13) //1 shif yang di pilih
					{
						if ($Split[12] == 3) {
							$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")) 
											   or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
							$shift = $Split[11];
						} else{
							$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")))";//untuk selain shif 4
							$shift = $Split[11];
						}

					//-------------------------------- jumlah shift 2 -----------------------------------------------------------------------------------------------
					} else if(count($Split) === 15){
						if ($Split[12] === 3 or $Split[14] === 3)
						{
							$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")) 
											  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
							$shift = $Split[11].' Dan '.$Split[13];
						}else{
							$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")))";//untuk selain shif 4
							$shift = $Split[11].' Dan '.$Split[13];
						}

					//-------------------------------- jumlah shift 3 -----------------------------------------------------------------------------------------------	
					} else if(count($Split) === 17){ 
							$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].",".$Split[16].")) 
											  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
							$shift = 'Semua Shift';
					}
					
					//----------------- asal pasien --------------------------------
                    if($asalpasien == 'Semua'){
						$kriteriaUnit ="";
                    } else if($asalpasien == 'RWJ/IGD'){
						$kriteriaUnit=" AND left(kd_unit,1) in('2','3') ";
                    } else if($asalpasien == 'RWI'){
						$kriteriaUnit=" AND left(kd_unit,1)='1' ";
                    } else{
                        $kriteriaUnit=" AND kd_unit='' ";
                    }
					
					//------------------- user/operator ------------------------------
					if($user == 'Semua'){
						$kd_user="";
					} else{
						$kd_user=" AND opr=".$Split[10]."";
					}
					
					//------------------ unit far -------------------------------
					if($unit == 'Semua'){
						$kd_unit_far="";
					} else{
						$kd_unit_far=" AND kd_unit_far='".$Split[7]."'";
					}
		}

		$queryHasil = $this->db->query( " SELECT x.Tgl_Out, x.No_Out, No_Resep, No_Bukti, kd_pasienapt, nmpasien, sub_jumlah, Discount, Tuslah, AdmRacik, AdmNCI, Tunai, Transfer, Kredit,Jumlah 
												FROM 
													(SELECT Tgl_Out, No_Out, No_Resep, No_Bukti, kd_pasienapt, nmpasien,
														Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as sub_jumlah, 
														Jasa as Tuslah, AdmRacik,
														admnci+AdmNCI_Racik as admnci, 
														Case When returapt=0 then Discount Else (-1)* Discount End as discount, 
														Case When returapt=0 then Jml_Bayar Else (-1)* Jml_Bayar End as jumlah
													FROM Apt_Barang_Out 
														WHERE tutup=1 
															".$kd_unit_far."
															".$kd_user."
															".$kriteriaUnit."
													) x 
												inner JOIN 
													(SELECT Tgl_Out, No_Out, 
														Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as tunai, 
														sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, 
														sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit 
													 FROM (SELECT Tgl_Out, No_Out, Type_data, db.tgl_bayar,
															Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End as Jumlah
														FROM apt_Detail_Bayar db 
															inner JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE ".$ParamShift.") det 
														GROUP BY Tgl_Out, No_Out
														  ) y ON x.tgl_out=y.Tgl_Out AND x.No_Out=y.No_Out 
												ORDER BY x.tgl_out, x.No_out

										");

		$query = $queryHasil->result();
		if(count($query) == 0)
		{
			$res= '{ success : false, msg : "No Records Found"}';
		}
		else {									
			$queryRS = $this->db->query("select * from db_rs")->result();
			$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			$no = 0;
			$this->load->library('m_pdf');
			$this->m_pdf->load();	
			
			$mpdf=$common->getPDF('P','LAPORAN KASIR DETAIL');

			//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
			$mpdf->WriteHTML("
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN DETAIL KASIR</th>
						</tr>
						<tr>
							<th>".$tglAwal." s/d ".$tglAkhir."</th>
						</tr>
						<tr>
							<th>UNIT RAWAT : ".$asalpasien."</th>
						</tr>
						<tr>
							<th>".$unit."</th>
						</tr>
						<tr>
							<th align='left'>Kasir : ".$user."</th>
						</tr>
						<tr>
							<th align='left'>Shift : ".$shift."</th>
						</tr>
					</tbody>
				</table><br>
			");
			
			//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
			$mpdf->WriteHTML('
									<table class="t1" border = "1" style="overflow: wrap">
									<thead>
									  <tr>
											<th width="">No</td>
											<th width="" align="center">no. Tr</td>
											<th width="" align="center">No Resep</td>
											<th width="" align="center">Nama Pasien</td>
											<th width="" align="center">Transaksi</td>
											<th width="" align="center">Disc</td>
											<th width="" align="center">Tuslah</td>
											<th width="" align="center">Racik</td>
											<th width="" align="center">Tunai</td>
											<th width="" align="center">Pembulatan</td>
											<th width="" align="center">Kredit</td>
									  </tr>
									</thead>

				');
						$no=0;
						$tot_jumlah=0;
						$tot_disc=0;
						$tot_tuslah=0;
						$tot_admracik=0;
						$tot_tunai=0;
						$tot_pembulatan=0;
						$tot_kredit=0;
					foreach ($query as $line) 
					{
						$no++;       
						$no_out=$line->no_out;
						$tgl_out=$line->tgl_out;
						$no_resep=$line->no_resep;
						$nama=$line->nmpasien;
						$sub_jumlah = $line->sub_jumlah;
						$discount = $line->discount;
						$tuslah = $line->tuslah;
						$admracik=$line->admracik;
						$tunai = $line->tunai;
						$pembulatan = $line->jumlah;
						$kredit = $line->kredit;
						
						$mpdf->WriteHTML('

						<tbody>

								<tr class="headerrow"> 
										<td width="">'.$no.'</td>
										<td width="" align="center">'.$no_out.'</td>
										<td width="" align="center">'.$no_resep.'</td>
										<td width="" align="left">'.$nama.'</td>
										<td width="" align="right">'.number_format($sub_jumlah,0,',','.').'</td>
										<td width="" align="right">'.number_format($discount,0,',','.').'</td>
										<td width="" align="right">'.number_format($tuslah,0,',','.').'</td>
										<td width="" align="right">'.number_format($admracik,0,',','.').'</td>
										<td width="" align="right">'.number_format($tunai,0,',','.').'</td>
										<td width="" align="right">'.number_format($pembulatan,0,',','.').'</td>
										<td width="" align="right">'.number_format($kredit,0,',','.').'</td>
								</tr>

						');
						$tot_jumlah +=$sub_jumlah;
						$tot_disc +=$discount;
						$tot_tuslah +=$tuslah;
						$tot_admracik +=$admracik;
						$tot_tunai +=$tunai;
						$tot_pembulatan +=$pembulatan;
						$tot_kredit +=$kredit;
						
						
					}
					$mpdf->WriteHTML('
						<tr class="headerrow"> 
							<th width="" colspan="4" align="right">Grand Total</td>
							<th width="" align="right">'.number_format($tot_jumlah,0,',','.').'</td>
							<th width="" align="right">'.number_format($tot_disc,0,',','.').'</td>
							<th width="" align="right">'.number_format($tot_tuslah,0,',','.').'</td>
							<th width="" align="right">'.number_format($tot_admracik,0,',','.').'</td>
							<th width="" align="right">'.number_format($tot_tunai,0,',','.').'</td>
							<th width="" align="right">'.number_format($tot_pembulatan,0,',','.').'</td>
							<th width="" align="right">'.number_format($tot_kredit,0,',','.').'</td>
						</tr>

					');	
					$mpdf->WriteHTML('</tbody></table>');
									   $tmpbase = 'base/tmp/Apotek/';
									   $tmpname = time().'APOTEK';
									   $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

									   $res= '{ success : true, msg : "", id : "", title : "Laporan Kasir Detail", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
		}			 
		echo $res;

    }
	
	public function LapResepPasienPerFaktur($UserID,$Params){
		$UserID = '0';
		$common=$this->common;
		$Split = explode("##@@##", $Params, 18);
		//print_r ($Split);
		
		/* //3 shift
		[0] => Operator
		[1] => 0
		[2] => unit_rawat
		[3] => 1
		[4] => unit
		[5] => APT
		[6] => jenis_pasien
		[7] => 0000000001
		[8] => start_date
		[9] => 2015-8-7
		[10] => last_date
		[11] => 2015-8-7
		[12] => shift1
		[13] => true
		[14] => shift2
		[15] => true
		[16] => shift3
		[17] => true */
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[9];
			$tglAkhir = $Split[11];
			$asalpasien = $Split[4];
			$kd_unit_far = $Split[5];
			$kd_user = $Split[1];
			$kd_customer = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_customer == ''){
				$jenispasien='SEMUA JENIS PASIEN';
			}else{
				$jenispasien = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit -----------------------------
			if($Split[3] == 1){
				$asalpasien='Rawat Inap';
				$kd_unit="AND left(o.kd_unit,1)='1'";
			} else if($Split[3] == 2){
				$asalpasien='Rawat Jalan';
				$kd_unit="AND left(o.kd_unit,1)='2'";
			} else if($Split[3] == 3){
				$asalpasien='Inst. Gawat Darurat';
				$kd_unit="AND left(o.kd_unit,1)='3'";
			} else {
				$asalpasien='SEMUA UNIT';
				$kd_unit="";
			}
			
			//------------------------ kriteria unit -----------------------------
			if($kd_customer == ''){
				$kd_customer= "";
			} else{
				$kd_customer= "AND o.kd_customer='".$kd_customer."'";
			}
			
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user="AND o.opr=".$kd_user."";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND o.kd_unit_far='".$kd_unit_far."'";
			}
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[13] == 'true' && $Split[15] == 'true' && $Split[17] == 'true'){
				$ParamShift = "(o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(1,2,3) 
								or o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'true' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "(o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(2,3) 
									or o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "(o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(1,3) 
									or o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'true'){
				$ParamShift = "o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[17] == 'false' && $Split[15] == 'false' && $Split[13] == 'true'){
				$ParamShift = "o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'false' && $Split[15] == 'true' && $Split[13] == 'false'){
				$ParamShift = "o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[17] == 'true' && $Split[15] == 'false' && $Split[13] == 'false'){
				$ParamShift = "o.tgl_out BETWEEN '2015-08-07' AND '2015-08-07' and shiftapt in(3)";
				$shift = '2';
			} 
		}

		$queryHasil = $this->db->query( " SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit
											FROM apt_barang_out o
												left join dokter d on o.dokter=d.kd_dokter
												left join unit u on o.kd_unit=u.kd_unit
												inner join apt_barang_out_detail od on o.no_out=od.no_out and o.tgl_out=od.tgl_out
												inner join apt_obat ao on od.kd_prd=ao.kd_prd
												
											WHERE ".$ParamShift."
												".$kd_customer."
												".$kd_user."
												".$kd_unit_far."
												".$kd_unit."

										");
		/* echo " SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit
											FROM apt_barang_out o
												left join dokter d on o.dokter=d.kd_dokter
												left join unit u on o.kd_unit=u.kd_unit
												inner join apt_barang_out_detail od on o.no_out=od.no_out and o.tgl_out=od.tgl_out
												inner join apt_obat ao on od.kd_prd=ao.kd_prd
												
											WHERE ".$ParamShift."
												".$kd_customer."
												".$kd_user."
												".$kd_unit_far."
												".$kd_unit.""; */
		$query = $queryHasil->result();
		if(count($query) == 0)
		{
			$res= '{ success : false, msg : "No Records Found"}';
		}
		else {									
			$queryRS = $this->db->query("select * from db_rs")->result();
			$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			$no = 0;
			$this->load->library('m_pdf');
			$this->m_pdf->load();	
			
			$mpdf=$common->getPDF('P','LAPORAN RESEP PASIEN BERDASARKAN FAKTUR');

			//-------------------------MENGATUR TAMPILAN TABEL ------------------------------------------------
			$mpdf->WriteHTML("
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN RESEP PASIEN BERDASARKAN FAKTUR</th>
						</tr>
						<tr>
							<th>".$tmptglawal." s/d ".$tmptglakhir."</th>
						</tr>
						<tr>
							<th>UNIT RAWAT : ".$asalpasien."</th>
						</tr>
						<tr>
							<th>".$unit."</th>
						</tr>
						<tr>
							<th>".$jenispasien."</th>
						</tr>
						<tr>
							<th align='left'>Kasir : ".$user."</th>
						</tr>
						<tr>
							<th align='left'>Shift : ".$shift."</th>
						</tr>
					</tbody>
				</table><br>
			");
			
			//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
			$mpdf->WriteHTML('
									<table class="t1" border = "1" style="overflow: wrap">
									<thead>
									  <tr>
											<th width="30" align="center">No</td>
											<th width="70" align="center">Tanggal</td>
											<th width="50" align="center">No. Tr</td>
											<th width="80" align="center">No Resep</td>
											<th width="70" align="center">No. Medrec / Kode Obat</td>
											<th width="200" align="center">Nama Pasien, Dokter dan Unit / Nama Obat</td>
											<th width="50" align="center">Sat</td>
											<th width="50" align="center">Qty</td>
											<th width="50" align="center">Harga</td>
											<th width="60" align="center">Jumlah</td>
									  </tr>
									</thead>

				');
					$no=0;
					$grand=0;
					foreach ($query as $line) 
					{
						$no++;       
						$no_out=$line->no_out;
						$tgl_out=substr($line->tgl_out, 0, -8);
						$no_resep=$line->no_resep;
						$nama_pasien=$line->nmpasien;
						$kd_pasien = $line->kd_pasienapt;
						$dokter = $line->nama_dokter;
						$unit = $line->nama_unit;
						$tgl_out=date('d-M-Y',strtotime($tgl_out));
						
						$mpdf->WriteHTML('

						<tbody>

								<tr class="headerrow"> 
										<th width="">'.$no.'</td>
										<th width="" align="center">'.$tgl_out.'</td>
										<th width="" align="center">'.$no_out.'</td>
										<th width="" align="center">'.$no_resep.'</td>
										<th width="" align="left">'.$kd_pasien.'</td>
										<th width="" align="left">'.$nama.' / '.$unit.' / '.$dokter.'</td>
										<th width="" align="right"></td>
										<th width="" align="right"></td>
										<th width="" align="right"></td>
										<th width="" align="right"></td>
								</tr>

						');
						$queryHasil2 = $this->db->query( "SELECT o.no_out, o.tgl_out, o.no_resep, o.kd_pasienapt, o.nmpasien, o.dokter, d.nama as dokter, 
															o.kd_unit, u.nama_unit, od.kd_prd, ao.nama_obat, ao.kd_satuan, od.jml_out, od.harga_jual, od.jml_out * od.harga_jual as jumlah,
															o.discount, o.jasa as tuslah, o.admracik
										FROM apt_barang_out o
											left join dokter d on o.dokter=d.kd_dokter
											left join unit u on o.kd_unit=u.kd_unit
											inner join apt_barang_out_detail od on o.no_out=od.no_out and o.tgl_out=od.tgl_out
											inner join apt_obat ao on od.kd_prd=ao.kd_prd
											
										WHERE ".$ParamShift."
												".$kd_customer."
												".$kd_user."
												".$kd_unit_far."
												".$kd_unit."

									");
						$query2 = $queryHasil2->result();
						$tot_jumlah=0;
						$tot_disc=0;
						$tot_tuslah=0;
						$tot_admracik=0;
						
						foreach ($query2 as $line2) 
						{
							$kd_prd=$line2->kd_prd;
							$nama_obat=$line2->nama_obat;
							$kd_satuan=$line2->kd_satuan;
							$qty=$line2->jml_out;
							$harga = $line2->harga_jual;
							$jumlah = $line2->jumlah;
							$discount = $line2->discount;
							$tuslah = $line2->tuslah;
							$admracik=$line2->admracik;
							
							$mpdf->WriteHTML('
							<tbody>

								<tr class="headerrow"> 
										<td width=""></td>
										<td width="" align="center"></td>
										<td width="" align="center"></td>
										<td width="" align="center"></td>
										<td width="" align="left">'.$kd_prd.'</td>
										<td width="" align="left">'.$nama_obat.'</td>
										<td width="" align="left">'.$kd_satuan.'</td>
										<td width="" align="right">'.$qty.'</td>
										<td width="" align="right">'.number_format($harga,0,',','.').'</td>
										<td width="" align="right">'.number_format($jumlah,0,',','.').'</td>
								</tr>

							');
							
							$tot_jumlah +=$jumlah;
							$tot_disc +=$discount;
							$tot_tuslah +=$tuslah;
							$tot_admracik +=$admracik;
							
						}
						$sub_total = $tot_jumlah - $tot_disc + $tottusadm ;
						$tottusadm=$tot_tuslah+$tot_admracik;
						$mpdf->WriteHTML('

								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Jumlah :</td>
										<td width="" align="right">'.number_format($tot_jumlah,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Discount(-) :</td>
										<td width="" align="right">'.number_format($tot_disc,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Tuslah + Adm.Racik :</td>
										<td width="" align="right">'.number_format($tottusadm,0,',','.').'</td>
								</tr>
								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Sub Total :</td>
										<td width="" align="right">'.number_format($sub_total,0,',','.').'</td>
								</tr>

							');
						$grand += $sub_total;
					}
					$mpdf->WriteHTML('

								<tr class="headerrow"> 
										<td width="" align="right" colspan="9">Grand Total :</td>
										<td width="" align="right">'.number_format($grand,0,',','.').'</td>
								</tr>

							');
					$mpdf->WriteHTML('</tbody></table>');
									   $tmpbase = 'base/tmp/Apotek/';
									   $tmpname = time().'APOTEK';
									   $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

									   $res= '{ success : true, msg : "", id : "", title : "Laporan Resep Pasien Per Faktur", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
		}			 
		echo $res;

    
	}

}



?>