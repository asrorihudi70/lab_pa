<?php
class Formulir extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session','url');
    }
	 
    public function cetak()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));

      $var = $this->uri->segment(4,0);
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
		  
           $mpdf= new mPDF('utf-8', 'P');

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
          // $mpdf->SetFooter($arr);
		  
		  $style = "<style>
		  body
		  {
		  
		  }
		  table
		  {
			  border-collapse:collapse;
			  border:1px solid #000;
			  width:1300px;
			  height:auto;
			  font-size:16px;
			  float:left;
		  }
		  
		  table tr td
		  {
			  padding:15px 10px;
			  border:0px solid #000;
		  }
		  
		  .first
		  {
			  width : 250px;
		  }
		  
		  .middle_long
		  {
			  width:400px;
		  }
		  
		  .long 
		  {
			  width : 400px;
		  }
		  
		  .short
		  {
			  width:122px;
			  padding:0px !important; 
		  }
		  
		  .head
		  {
			  float:left;
			  width:100px;
			  height:10px;
		  }
		  
		  .bordered
		  {
			  float:left;
			  width:100px;
			  height:100px;
			  padding:2px;
			  border:1px solid #000;
		  }
		  
		   .no_bordered
		  {
			  float:left;
			   width:5px;
			  height:5px;
			  padding:2px;
			  border:none;
		  }
		  
		  
		  </style>
		  ";
		 
		   
		   $mpdf->WriteHTML('<html>
		   					<head>'.$style.'</head>
							<body>');
							
			$mpdf->WriteHTML('<p align="center"><b>DATABASE PASIEN</b></p>');	
			 //$mpdf->WriteHTML($var);
			
		
		$query = $this->db->query("SELECT * FROM(SELECT DISTINCT ON (kd_pasien) pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir,
                          agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, pasien.kd_kelurahan,pasien.telepon,kunjungan.kd_dokter,dokter.nama as dokter,
                          pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk, kunjungan.kd_customer,
						  kl.kelurahan,kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI,pasien.kd_kelurahan_ktp,pasien.alamat_ktp,pasien.kd_pos_ktp,pasien.handphone,
						   pasien.kd_pendidikan, pasien.kd_pekerjaan, pasien.kd_agama,pendidikan.pendidikan,pekerjaan.pekerjaan, agama.agama
                          FROM pasien INNER JOIN
                          kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien inner join
                          dokter ON kunjungan.kd_dokter = dokter.kd_dokter inner join
                          unit ON kunjungan.kd_unit = unit.kd_unit LEFT JOIN
                          penanggung_jawab ON penanggung_jawab.kd_pasien = pasien.kd_pasien LEFT JOIN
                          agama ON pasien.kd_agama = agama.kd_agama INNER JOIN
                          pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
                          pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
						  LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
						  LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
						  LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
                          LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
                          inner join transaksi on transaksi.KD_PASIEN = pasien.KD_PASIEN ) AS resdata
                          where kd_pasien = '".$var."'")->result();
						  
foreach($query as $row)
{						  
     if($row->status_marita == 0)
	 {
		 $status = "<strike> Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($row->status_marita == 1)
	 {
		 $status = "<strike>Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($row->status_marita == 2)
	 {
		 $status = "<strike>Menikah / Tidak Menikah </strike> / Janda / <strike>Duda</strike>";
	 }
	  else if($row->status_marita == 3)
	 {
		 $status = "<strike>Menikah / Tidak Menikah / Janda </strike> / Duda";
	 }
	 
	 if($row->kd_customer == '0000000001')
	 {
		  $customer = "Pribadi / <strike>Jaminan (Asuransi / Perusahaan)</strike>";
	 }
	 else
	 {
		 $customer = "<strike>Pribadi</strike> / Jaminan (Asuransi / Perusahaan)"; 
	 }
	 
	 if($row->wni == 0)
	 {
		 $wni = 'WNI';
	 }
	 else
	 {
		 $wni = 'WNA';
	 }
	 
	 if($row->jenis_kelamin == 't')
	 {
		 $jk = 'L/<strike>P</strike>';
	 }
	 else
	 {
		 $jk = '<strike>L</strike>/P';
	 }
	 
	 
	 if($row->kd_kelurahan_ktp != '')
	 {
	 $get = $this->db->query("
	 SELECT kelurahan,kecamatan,kabupaten from kelurahan left join kecamatan on kelurahan.kd_kecamatan = kecamatan.kd_kecamatan left join kabupaten ON kabupaten.kd_kabupaten = kecamatan.kd_kabupaten
	 
	 where kelurahan.kd_kelurahan = ".$row->kd_kelurahan_ktp);

	 foreach($get->result() as $data)
	 {
		 $kelurahan = $data->kelurahan;
		 $kecamatan = $data->kecamatan;
		 $kabupaten = $data->kabupaten;
	 }
	 }
	 else
	 {
		 $kelurahan = '-';
		 $kecamatan = '-';
		 $kabupaten = '-';
	 }
          $mpdf->WriteHTML('<table>
		
			<tr>
			<td width="263" class="first">No. Rekam Medis</td>
			<td width="21">:</td>
			<td width="252" style="float:left;"><b>'.$row->kd_pasien.'</b></td>
			<td width="226">&nbsp;</td>
			<td width="131" ></td>
			<td width="172" ></td>
			<td width="203" ></td>
			</tr>
			
			<tr>
			<td colspan="7"><b>DATA PASIEN</b></td>
			</tr>
			
			<tr>
			<td>Nama Pasien</td>
			<td>:</td> 
			<td colspan="3">'.$row->nama.'</td>
			<td>Jenis Kelamin</td>
			<td>: '.$jk.'</td>
			</tr>
			
			<tr>
			<td>Tempat Tanggal Lahir</td>
			<td>:</td>
			<td colspan="3">'.$row->tempat_lahir.' , '.$row->tgl_lahir.'</td>
			<td>Agama</td>
			<td>: '.$row->agama.'</td>
			</tr>
			
			<tr>
			<td>Status Perkawinan</td>
			<td>:</td>
			<td colspan="3">'.$status.'</td>
			</tr>
			
			<tr>
			<td>Warga Negara</td>
			<td>:</td>
			<td colspan="3">'.$wni.'</td>
			</tr>
			
			<tr>
			<td>Pekerjaan</td>
			<td>:</td>
			<td colspan="2">'.$row->pekerjaan.'</td>
			<td>Pendidikan</td>
			<td>: '.$row->pendidikan.'</td>
			</tr>
			
			<tr>
			<td>Alamat - Tempat Tinggal</td>
			<td>:</td>
			<td colspan="3">'.$row->alamat.'</td>
			<td>Kelurahan</td>
			<td>: '.$row->kelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$row->kecamatan.'</td>
			<td>Kota : '.$row->kabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$row->kd_pos.'</td>
			</tr>
			
			<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Sesuai KTP</td>
			<td>:</td>
			<td colspan="3">'.$row->alamat_ktp.'</td>
			<td>Kelurahan</td>
			<td>: '.$kelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$kecamatan.'</td>
			<td>Kota : '.$kabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$row->kd_pos_ktp.'</td>
			</tr>
			
			
			<tr>
			<td></td>
			<td></td>
			<td colspan="5">No. KTP &nbsp; : &nbsp;  '.$row->pengenal.' </td>
			</tr>
			
			<tr>
			<td>No. Telepon / Email</td>
			<td>:</td>
			<td>Rumah : '.$row->telepon.'</td>
			<td>HP : '.$row->handphone.'</td>
			</tr>
			
			<tr>
			<td>Kode Biaya</td>
			<td>:</td>
			<td colspan="3">'.$customer.'</td>
			</tr>
			
			<tr>
			<td>Dokter yang dituju</td>
			<td>:</td>
			<td colspan="3">'.$row->dokter.'</td>
			</tr>');
}


$pj = $this->db->query("SELECT * FROM(SELECT DISTINCT ON (kd_pasien) penanggung_jawab.kd_pasien, penanggung_jawab.nama_pj,  penanggung_jawab.jenis_kelamin, penanggung_jawab.tempat_lahir, penanggung_jawab.tgl_lahir,
                          penanggung_jawab.wni, penanggung_jawab.status_marital, penanggung_jawab.alamat, penanggung_jawab.kd_kelurahan,penanggung_jawab.telepon,kunjungan.kd_dokter,dokter.nama as dokter,
                          pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk, kunjungan.kd_customer,
						  kl.kelurahan,kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI,penanggung_jawab.kd_kelurahan_ktp,penanggung_jawab.alamat_ktp,penanggung_jawab.kd_pos_ktp,
						  penanggung_jawab.no_hp,
						   penanggung_jawab.kd_pendidikan, penanggung_jawab.kd_pekerjaan, penanggung_jawab.kd_agama,pendidikan.pendidikan,pekerjaan.pekerjaan
                          FROM penanggung_jawab INNER JOIN
                          kunjungan ON penanggung_jawab.kd_pasien = kunjungan.kd_pasien inner join
                          dokter ON kunjungan.kd_dokter = dokter.kd_dokter inner join
                          unit ON kunjungan.kd_unit = unit.kd_unit LEFT JOIN
                          pendidikan ON penanggung_jawab.kd_pendidikan = pendidikan.kd_pendidikan INNER JOIN
                          pekerjaan ON penanggung_jawab.kd_pekerjaan = pekerjaan.kd_pekerjaan
						  LEFT join kelurahan kl on kl.kd_kelurahan = penanggung_jawab.KD_KELURAHAN
						  LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
						  LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
                          LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
                          inner join transaksi on transaksi.kd_pasien = penanggung_jawab.kd_pasien ) AS resdata
                          where kd_pasien = '".$var."'")->result();
						  
foreach($pj as $r)
{						  
     if($r->status_marital == 0)
	 {
		 $pj_status = "<strike> Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($r->status_marital == 1)
	 {
		 $pj_status = "<strike>Menikah </strike> /  Tidak Menikah / <strike> Janda / Duda</strike>";
	 }
	 else if($r->status_marital == 2)
	 {
		 $pj_status = "<strike>Menikah / Tidak Menikah </strike> / Janda / <strike>Duda</strike>";
	 }
	  else if($r->status_marital == 3)
	 {
		 $pj_status = "<strike>Menikah / Tidak Menikah / Janda </strike> / Duda";
	 }

	 
	 if($r->wni == 0)
	 {
		 $pj_wni = 'WNI';
	 }
	 else
	 {
		 $pj_wni = 'WNA';
	 }
	 
	 if($r->jenis_kelamin == 't')
	 {
		 $pjjk = 'L/<strike>P</strike>';
	 }
	 else
	 {
		 $pjjk = '<strike>L</strike>/P';
	 }
	 
	 
	 if($r->kd_kelurahan_ktp != '')
	 {
	 $pjget = $this->db->query("
	 SELECT kelurahan,kecamatan,kabupaten from kelurahan left join kecamatan on kelurahan.kd_kecamatan = kecamatan.kd_kecamatan left join kabupaten ON kabupaten.kd_kabupaten = kecamatan.kd_kabupaten
	 
	 where kelurahan.kd_kelurahan = ".$r->kd_kelurahan_ktp);

	 foreach($pjget->result() as $pjdata)
	 {
		 $pjkelurahan = $pjdata->kelurahan;
		 $pjkecamatan = $pjdata->kecamatan;
		 $pjkabupaten = $pjdata->kabupaten;
	 }
	 }
	 else
	 {
		 $pjkelurahan = '-';
		 $pjkecamatan = '-';
		 $pjkabupaten = '-';
	 }
}
			$mpdf->WriteHTML('<tr>
			<td colspan="7"><b>DATA PENANGGUNG JAWAB</b></td>
			</tr>
			
			<tr>
			<td>Nama Penanggung Jawab</td>
			<td>:</td> 
			<td colspan="3">'.$r->nama_pj.'</td>
			<td>Jenis Kelamin</td>
			<td>: '.$pjjk.'</td>
			</tr>
			
			<tr>
			<td>Tempat Tanggal Lahir</td>
			<td>:</td>
			<td colspan="3">'.$r->tempat_lahir.', '.$r->tgl_lahir.'</td>
			<td>Agama</td>
			<td>: </td>
			</tr>
			
			<tr>
			<td class="first">Status Perkawinan</td>
			<td>:</td>
			<td colspan="3">'.$pj_status.'</td>
			</tr>
			
			<tr>
			<td>Warga Negara</td>
			<td>:</td>
			<td colspan="3">'.$pj_wni.'</td>
			</tr>
			
			<tr>
			<td>Pekerjaan</td>
			<td>:</td>
			<td colspan="2">'.$r->pekerjaan.'</td>
			<td>Pendidikan</td>
			<td>: '.$r->pendidikan.'</td>
			</tr>
			
			<tr>
			<td>Alamat - Tempat Tinggal</td>
			<td>:</td>
			<td colspan="3">'.$r->alamat.'</td>
			<td>Kelurahan</td>
			<td>: '.$r->kelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$r->kecamatan.'</td>
			<td>Kota : '.$r->kabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$r->kd_pos.'</td>
			</tr>
			
			
			<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Sesuai KTP</td>
			<td>:</td>
			<td colspan="3">'.$r->alamat_ktp.'</td>
			<td>Kelurahan</td>
			<td>: '.$pjkelurahan.'</td>
			</tr>
			
			<tr>
			<td></td>
			<td></td>
			<td>Kec. : '.$pjkecamatan.'</td>
			<td>Kota : '.$pjkabupaten.'</td>
			<td>Kode Pos</td>
			<td>: '.$r->kd_pos_ktp.'</td>
			</tr>
			
			
			<tr>
			<td></td>
			<td></td>
			<td colspan="5">No. KTP &nbsp; : &nbsp;  '.$r->no_ktp.' </td>
			</tr>
			
			<tr>
			<td>No. Telepon / Email</td>
			<td>:</td>
			<td>Rumah : '.$r->telepon.'</td>
			<td>HP : '.$r->no_hp.'</td>
			</tr>
			
			<tr>
			<td>Hubungan dengan pasien</td>
			<td>:</td>
			<td colspan="3"><strike>Suami / Istri / Anak</strike> / Orangtua / <strike>Lain-lain </strike> *</td>
			</tr>
			
			<tr>
			<td colspan="4"></td>
			<td colspan="3" align="center">Tanggerang, '.date('d - M -Y').'</td>
			</tr>
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			
			<tr>
			<td colspan="7"></td>
			</tr>
			
			
			<tr>
			<td colspan="4"></td>
			<td colspan="3" align="center">(.............................................................)</td>
			</tr>
			
			<tr>
			<td colspan="4"></td>
			<td colspan="3" align="center">Pendaftaran</td>
			</tr>

			
			</table>');				
					
							
			$mpdf->WriteHTML("</body></html>");				
							

							
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output("formulir.pdf" ,'I');
exit;
	
	
}

}

?>