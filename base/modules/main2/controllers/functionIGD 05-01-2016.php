<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionIGD extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {

 
          $this->load->view('main/index',$data=array('controller'=>$this));

    }
	
	private function GetShiftBagian()
	{
		$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row()->lastdate;
		
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
		
		return $sqlbagianshift2;
	}
	
	public function UpdateKdCustomer()
	 {
		 $KdTransaksi= $_POST['TrKodeTranskasi'];
		 $KdUnit= $_POST['KdUnit'];
		 $KdDokter= $_POST['KdDokter'];
		 $TglTransaksi= $_POST['TglTransaksi'];
		 $KdCustomer= $_POST['KDCustomer'];
		 $NoSjp= $_POST['KDNoSJP'];
		 $NoAskes= $_POST['KDNoAskes'];
		 $KdPasien = $_POST['TrKodePasien'];
		 $this->db->trans_begin();
		 $query = $this->db->query("select updatekdcostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoSjp."','".$NoAskes."','".$KdCustomer."')");
		 $res = $query->result();
		 
		  //--- Query SQL Server ----//
 
		 //--- Akhir Query SQL Server---//
		 
		 if($res)
		 {	 $this->db->trans_commit();
			 echo "{success:true}";
		 }
		 else
		 {	  $this->db->trans_rollback();
			  echo "{success:false}";
		 }
	 }
	 
	 
public function KonsultasiPenataJasa()
   {

	   $kdTransaksi = $this->GetIdTransaksi();
	   
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $kdUnitAsal = $_POST['KdUnitAsal'];
	   $kdDokterAsal = $_POST['KdDokterAsal'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
	   $kdKasir = "06";
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   
	   $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."',
	   '".$kdCostumer."','".$kdKasir."','".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."',".$antrian.")");
	   $res = $query->result();
	   
	 
	   if($res)
	   {
		   echo '{success: true}';
	   }
	   else
	   {
		   echo '{success: false}';
	   }
   } 
   
   
     private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
			$kdKasir = "06";
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' 
			and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
    private function GetIdTransaksi()
        {   $kdKasir= "06";
			$counter = $this->db->query("select counter from kasir where kd_kasir = '$kdKasir'")->row();
			$no = $counter->counter;
			$retVal2 = $no+1;
			$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kdKasir'");
    		return $retVal2;
        }
	
	
	public function saveDiagnosa()
	{
		
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$a = explode("##[[]]##",$list);
		$this->db->trans_begin();
		for($i=0;$i<=count($a)-1;$i++)
		{
			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
							if($b[$k] == 'Diagnosa Awal')
							{
								$diagnosa = 0;
							}
							else if($b[$k] == 'Diagnosa Utama')
							{
								$diagnosa = 1;
							}
				
							else if($b[$k] == 'Komplikasi')
							{
								$diagnosa = 2;
							}
							else if($b[$k] == 'Diagnosa Sekunder')
							{
								$diagnosa = 3;
							}
							else if($b[$k] == 'Baru')
							{
								$kasus = 'TRUE';
							}
							else if($b[$k] == 'Lama')
							{
								$kasus = 'FALSE';
							}
			}
				$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
				$result = $urut->result();
				foreach ($result as $data)
				{
					$Urutan = $data->geturutmrpenyakit;
				}
			
				$query = $this->db->query("select insertdatapenyakit('".$b[1]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",
				".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");	
		
		
		}
			if($query)
				{
					$this->db->trans_commit();
					echo "{success:true}";
				}
				else
				{
					$this->db->trans_rollback(); 
					echo "{success:false}";
				}
	}
	
	
     public function savedetailpenyakit()
	{
/*		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$kdKasir = "06";
		 $kd_user = $this->session->userdata['user_id']['id'];
		//$urut_masuk = $_POST['UrutMasuk'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
			//echo($b[$k]);
			
			}
				$urut = $this->db->query("select geturutdetailtransaksi('$kdKasir','".$TrKodeTranskasi."','".$Tgl."') ");
				$result = $urut->result();
				foreach ($result as $data)
				{ 
				if($b[6]==0 || $b[6]=='' )
				{
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
				//echo($Urutan);
				}
				//echo();
			//(   charge,adjust,folio,qty,harga,shift,tag)
				$query = $this->db->query("select insert_detail_transaksi
				('$kdKasir',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 0,
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				
				) as hasil
				");	
				$cond = $query->result();
				//var_dump($cond);
				
				//--- Query SQL Server ---//
_QMS_query("IF NOT EXISTS  (SELECT kd_kasir FROM detail_transaksi WHERE kd_kasir = '$kdKasir' AND no_transaksi ='$TrKodeTranskasi' AND  urut=$Urutan and tgl_transaksi='$Tgl') 
	BEGIN
	INSERT INTO detail_transaksi(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag)
    VALUES('$kdKasir', '$TrKodeTranskasi', $Urutan,'$Tgl', $kd_user,'".$b[5]."','".$b[1]."','$KdUnit','".$b[3]."','false','true','',".$b[2].",".$b[4].",$Shift,'false')
	END
	ELSE
	BEGIN
	update detail_transaksi set kd_user=$kd_user,kd_tarif='".$b[5]."',tgl_berlaku='".$b[3]."',charge='false',qty=".$b[2].",shift=$Shift  where kd_kasir = '$kdKasir' AND no_transaksi ='$TrKodeTranskasi' AND  urut=$Urutan and tgl_transaksi='$Tgl'
	END");

_QMS_query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
		Select '$kdKasir','$TrKodeTranskasi',$Urutan,'$Tgl',kd_component, tarif as FieldResult,0
		From Tarif_Component 
		Where KD_Unit='$KdUnit' And Tgl_Berlaku='".$b[3]."' And KD_Tarif='".$b[5]."' And Kd_Produk='".$b[1]."'");

	//PERFORM  insert_detail_component(_kdkasir, _notrans ,_urut,_tgltrans , 0, _kdunit, _tglberlaku, _kdtarif , _kdproduk );

	//--- AKhir Queyr SQL Server --//
				
					
			
				
			

		}
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}*/
		$TrKodeTranskasi 	= $_POST['TrKodeTranskasi'];
		$KdUnit 			= $_POST['KdUnit'];
		$Tgl 				= date("Y-m-d");
		$Shift 				= $this->GetShiftBagian();
		$list 				= $_POST['List'];
		$jmlfield 			= $_POST['JmlField']; 
		$jmlList 			= $_POST['JmlList'];
		$kd_dokter			= $_POST['kdDokter'];
		$urut_masuk 		= $_POST['urut_masuk'];
		$listtrdokter		= json_decode($_POST['listTrDokter']);
		
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$this->db->trans_begin();
		$a 	= explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++){
			$b = explode("@@##$$@@",$a[$i]);
			$urut = $this->db->query("select geturutdetailtransaksi('06','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
			foreach ($result as $data){ 
				if($b[6]==0 || $b[6]=='' ){
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
			}
			$query = $this->db->query("select insert_detail_transaksi
				('06',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 '".$this->session->userdata['user_id']['id']."',
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				)
			");	
			
			
		
			//---------tambahan query untuk insert komopnent tarif dokter--------------//
		
			
			$updt = $this->db->query("update detail_transaksi set kd_dokter = '".$kd_dokter."' 
			where kd_kasir='06' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$Urutan."' AND
			no_transaksi='".$TrKodeTranskasi."'");
			
			//---------Akhir tambahan query untuk insert komopnent tarif dokter--------------//
			
			
		}
		

		
			foreach ($listtrdokter as $arr) 
			{
			//var_dump($arr);
			
			if($arr->kd_job == 'Dokter')
			{
				$kd_job = 1;
			}
			else
			{
				$kd_job = 2;
			}

			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
			kd_unit ='".$KdUnit."' AND
			kd_produk='".$arr->kd_produk."' AND
			tgl_berlaku='".$arr->tgl_berlaku."' AND
			kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
			
			foreach($ctarif as $ct)
			{
			if($ct->jumlah != 0)
			{
				$trDokter = $this->db->query("insert into detail_trdokter select '06','".$arr->no_tran."','".$arr->urut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
    NOT EXISTS (
        SELECT * FROM detail_trdokter WHERE   
			kd_kasir= '06' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$arr->urut."' AND
			kd_dokter = '".$arr->kd_dokter."' AND
			no_transaksi='".$arr->no_tran."'
    )");
			}
			}
			}
		
		$urut=$this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1')->row();
		$urut=substr($urut->id_mrresep,8,12);
		$sisa=4-count(((int)$urut+1));
		$real=date('Ymd');
		for($i=0; $i<$sisa ; $i++){
			$real.="0";
		}
		$real.=((int)$urut+1);
		$urut=$real;
		$result=$this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
		if($result->jumlah>0){
			$result=$this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
			$urut=$result->id_mrresep;
		}else{
			$kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
			$kd='0';
			if(isset($kd_dokter->kd_dokter)){
				$kd=$kd_dokter->kd_dokter;
			}
			$mr_resep=array();
			$mr_resep['kd_pasien']=$_POST['kd_pasien'];
			$mr_resep['kd_unit']=$_POST['KdUnit'];
			$mr_resep['tgl_masuk']=$_POST['Tgl'];
			$mr_resep['urut_masuk']=$_POST['urut_masuk'];
			$mr_resep['kd_dokter']=$kd;
			$mr_resep['id_mrresep']=$urut;
			$mr_resep['cat_racikan']='';
			$mr_resep['tgl_order']=$_POST['Tgl'];
			$mr_resep['dilayani']=0;
			$this->db->insert('mr_resep',$mr_resep);
		}
		$result=$this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=".$urut)->result();
		for($i=0; $i<count($result); $i++){
			$ada=false;
			for($j=0; $j<$_POST['jmlObat']; $j++){
				if($result[$i]->urut==($j+1) && $result[$i]->kd_prd==$_POST['kd_prd'.$j]){
					$ada=true;
				}
			}
			if($ada==false){
				$this->db->query("DELETE FROM mr_resepdtl WHERE id_mrresep=".$result[$i]->id_mrresep." AND urut=".$result[$i]->urut." AND kd_prd='".$result[$i]->kd_prd."'");
			}
		}
		for($i=0; $i<$_POST['jmlObat']; $i++){
			$status=0;
			if($_POST['verified'.$i]=='Not Verified'){
				$status=1;
			}
			$result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".($i+1).",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",'".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",".$_POST['racikan'.$i].") ");
		}
		

		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
// 			echo "SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'";
		}else
		{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	} 
	
	
	public function GetRecordPenyakit($kd,$nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%".$kd."%' OR penyakit ilike '%".$nama."%' ");
		$count = $query->num_rows();
		return $count;
	}
//tambahan sama agung

	public function savePembayaran()
	{
		$kdKasir = "06";
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $this->GetShiftBagian();
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		$this->db->trans_begin();
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
                {
                    $urut_detailbayar = 1;
                }  else {
                    foreach($det_query->result() as $det)
                    {
                            $urut_detailbayar = $det->urutan+1;
                    }
                }
                
		
		
		if($jmllist > 1)
		{
		$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++)
							{
								
								$b = explode("@@##$$@@",$a[$i]);
								for($k=0;$k<=count($b)-1;$k++)
								{
									
						
							
							$_kdproduk = $b[1];
							$_qty = $b[2];
							$_harga = $b[3];
							//$_kdpay = $b[4];
							$_urut = $b[5];
							
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							else if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							else if ($_Typedata == 3)
							{
							$harga = $b[7];	

							}
							
						
						
				
							}
			   $urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
                                        $pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",
										'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
                                       
									   $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",
										'".$b[9]."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE',
										'".$tglbayar."',".$urut_detailbayar.")")->result();
		
				}
				}
		
		else
		{
			
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				$_urut = $b[5];
				
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							 if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							 if ($_Typedata == 3)
							{
							$harga = $b[7];	
							}
							
							
			
	
			}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
				
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',
				".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
				
				$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',
				".$_urut.",'$tgltransaksi',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();
				

				
		
				

		}
						
			    if($pembayaran)
				{
				$this->db->trans_commit();
				echo '{success:true}';	
				}
				else
				{
				$this->db->trans_rollback(); 
				echo '{success:false}';	
				}
			
	
		
	}
	
	
	public function deletedetail_bayar()
	{
	$kdKasir = "06";	
	$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
	$Urut =$_POST['Urut'];
        $TmpTglBayar = explode(' ',$_POST['TrTglbayar']);
		$this->db->trans_begin();
        $TglBayar = $TmpTglBayar[0];
		$query=$this->db->query(
						 "  select hapusdetailbayar('$kdKasir',
						 '".$TrKodeTranskasi."',
						 ".$Urut.",'".$TglBayar."')"
						 
						 
		  
						);
				if($query)
				{
				$this->db->trans_commit();
				echo "{success:true}";
				}
				else
				{	$this->db->trans_rollback();
					echo "{success:false}";
				}
	}
        
		
public function batal_transaksi()
{
	$this->db->trans_begin();
	
	try
	{
	
	$kd_kasir = '06';
	$kd_pasien = $this->input->post('kdPasien');
	$kd_unit  = $this->input->post('kdUnit');
	$kd_dokter = $this->input->post('kdDokter');
	$tgl_trans = $this->input->post('tglTrans');
	$no_trans = $this->input->post('noTrans');
	$kd_customer = $this->input->post('kdCustomer');
	$keterangan = $this->input->post('Keterangan');
	$_kduser = $this->session->userdata['user_id']['id'];
	$username = $this->session->userdata['user_id']['username'];
	$notrans = $this->GetIdTransaksi($kd_kasir);
    $Schurut = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
    $nama = $this->db->query("select nama from pasien where kd_pasien='".$kd_pasien."'")->row()->nama;
	$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
	
	$datahistory = array(
	"kd_kasir"=>$kd_kasir,
    "no_transaksi"=>$notrans,
    "kd_pasien"=>$kd_pasien,
    "kd_unit"=>$kd_unit,
    "tgl_transaksi"=>$tgl_trans,
	"nama"=>$nama,
	"kd_unit"=>$kd_unit,
	"nama_unit"=>$unit,
	"kd_user"=>$_kduser,
	"kd_user_del"=>$_kduser,
	"shift"=>$this->GetShiftBagian(),
	"shiftdel"=>$this->GetShiftBagian(),
	"user_name"=>$username,
	"tgl_batal"=>date('Y-m-d'),
	"ket"=>$keterangan,
	);
	
	$inserthistory = $this->db->insert('history_batal_trans',$datahistory);
	
	
	$data = array("kd_kasir"=>$kd_kasir,
    "no_transaksi"=>$notrans,
    "kd_pasien"=>$kd_pasien,
    "kd_unit"=>$kd_unit,
    "tgl_transaksi"=>$tgl_trans,
    "urut_masuk"=>$Schurut,
    "tgl_co"=>NULL,
    "co_status"=>"False", 
    "orderlist"=>NULL,
    "ispay"=>"False",
    "app"=>"False",
    "kd_user"=>$_kduser,
    "tag"=>NULL,
    "lunas"=>"False",
    "tgl_lunas"=>NULL,
	"batal"=>'True',
	"tgl_batal"=>date('Y-m-d'),
	"kd_kasir_asal"=>$kd_kasir,
	"no_transaksi_asal"=>$no_trans,
	"posting_transaksi"=>"True"
	);
						  
	$insert = $this->db->insert('transaksi',$data);		
	
	
	$dataUpdate = array(
	 "batal"=>'True',
	 "tgl_batal"=>date('Y-m-d'),
	);
	
	$update = $this->db->where('no_transaksi',$no_trans);
	$update = $this->db->where('kd_kasir',$kd_kasir);
	$update = $this->db->update('transaksi',$dataUpdate);				  
	// var_dump($data);
						 
						 
	if($insert)
	{
	  $detail_transaksi = $this->db->query("insert into detail_transaksi
	  select 
	  '$kd_kasir',
	  '$notrans',
	  urut,
	  tgl_transaksi,
	  kd_user,
	  kd_tarif,
	  kd_produk,
	  kd_unit,
	  tgl_berlaku,
	  charge,
	  adjust,
	  folio,
	  qty,
	  harga,
	  shift,
	  kd_dokter,
	  kd_unit_tr,
	  cito,
	  js,
	  jp,
	  no_faktur,
	  flag,
	  tag,
	  hrg_asli,
	  kd_customer,
	  kd_loket
	  from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				  $detail_component = $this->db->query("insert into detail_component
				  select 
				  '$kd_kasir',
				  '$notrans',
				  urut,
				  tgl_transaksi,
				  kd_component,
				  tarif,
				  disc,
				  markup
  				  from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				  $dtl_bayar = $this->db->query("insert into detail_bayar 
				select 
				'$kd_kasir',
				'$notrans',
				urut,
				tgl_transaksi,
				$_kduser,
				kd_unit,
				kd_pay,
				-1*jumlah as jumlah,
				folio,
				shift,
				status_bayar,
				deposit,
				kd_dokter,
				tag,
				verified,
				kd_user_ver,
				tgl_ver,
				-1*sisa as sisa,
				-1*total_dibayar as total_dibayar,
				reff,
				pembayar,
				alamat,
				untuk,
				no_kartu
				from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$date = date('Y-m-d h:i:s');
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
				select 
				'$kd_kasir',
				'$notrans',
				urut,
				tgl_transaksi,
				kd_pay,
				urut_bayar,
				'$date',
				-1*jumlah as jumlah
				from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
				select 
				'$kd_kasir',
				'$notrans',
				urut,
				tgl_transaksi,
				kd_pay,
				urut_bayar,
				'$date',
				kd_component
				-1*jumlah as jumlah
				from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				}
				
				
				
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}
				else
				{
					$this->db->trans_commit();
					echo '{success:true}';
				}
	} 
	catch (Exception $e) 
	{
    echo $e->getMessage();
	}
}
        //fungsi untuk tutup transaksi editing by HDHT
	public function ubah_co_status_transksi ()
	{
                
		$kdunit = $_POST['KDUnit'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$kdKasir = GetKodeKasir($kdunit,'');
                
		$query = $this->db->query("update transaksi set ispay='true',co_status='true' ,  tgl_co='".date("Y-m-d")."' 
		where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$kdKasir'"	);	
		
		
	
		
		if($query)
		{
                    echo "{success:true}";
                }
                else
                {
                    echo "{success:false}";
                }
	 
	}
	//-------------------------------------------------
	
	public function saveTransfer()
	{
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$this->GetShiftBagian();
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$_POST['KasirRWI'];
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$Tglasal=$_POST['Tglasal'];
		$KDalasan =$_POST['KDalasan'];
		$tglhariini=date("Y-m-d");
	    $Kdcustomer=$_POST['Kdcustomer'];
		$this->db->trans_begin();
		$det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' and kd_kasir='$KDkasirIGD' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		      if(pg_num_rows($resulthasil) <= 0)
                                   {
                                        $urut_detailbayar=1;
                                   }else
								   {
								   	while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
										{
											$urut_detailbayar = $line['urutan'];
										}
								   }
			$pay_query = $this->db->query(" insert into detail_bayar 
						 (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

						values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
						
							if($pay_query)
							{
							$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
								SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
								WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
								if($detailTrbayar)
								{	
									$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
									if($statuspembayaran)
									{
											$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
										    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
											Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
											'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
											FROM Detail_Component dc
											INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
											INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
											and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
											WHERE dc.Kd_Kasir = '$KDkasirIGD'
											AND dc.No_Transaksi ='$TrKodeTranskasi'
											ORDER BY dc.Kd_Component");	
										if($detailtrcomponet)
										{	
										
												$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir='$KASIRRWI' order by urutan desc limit 1";
												$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
												if(pg_num_rows($resulthasilurut) <= 0)
											   {
													$uruttujuan=1;
											   }else
												   {
													while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
														{
															$uruttujuan = $line['urutan'];
														}
												   }
												$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
												FROM Produk_Charge pc 
												INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
												WHERE left(kd_unit, 2)='3'")->result();
												 $getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
												foreach($getkdtarifcus as $xkdtarifcus)
															{
															$kdtarifcus = $xkdtarifcus->getkdtarifcus;
															}
												foreach($getkdproduk as $det)
												{
												$kdproduktranfer = $det->kdproduk;
												$kdUnittranfer = $det->unitproduk;
												}
													$gettanggalberlaku=$this->db->query("SELECT gettanggalberlaku
													('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer)")->result();
														foreach($gettanggalberlaku as $detx)
															{
															$tanggalberlaku = $detx->gettanggalberlaku;
															
															}
										$detailtransaksitujuan = $this->db->query("
													INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
													VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
													'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
													$total,$Shift1,'false','$TrKodeTranskasi')
													");	
											if($detailtransaksitujuan)	
												{
													$detailcomponentujuan = $this->db->query
													("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
													   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
													   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
													   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	
													   if($detailcomponentujuan)
													   { 
														  	
													$tranferbyr = $this->db->query("INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
													det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
													  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
													  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");																  
														if($tranferbyr){
														 $this->db->trans_commit();
														 echo '{success:true}';
													    }
													   else{ 
													   $this->db->trans_rollback(); 
													   echo '{success:false}';	
													   }
													   }
													   else{ $this->db->trans_rollback(); 
													   echo '{success:false}';	
													   }
												}
												else
												{
												$this->db->trans_rollback(); 
												 echo '{success:false}';	
												}
										}
										else
										{
										$this->db->trans_rollback(); 
										echo '{success:false}';	
										}

									}								
								else
								{
								$this->db->trans_rollback(); 
								echo '{success:false}';	
								}
								}
								else
								{
								$this->db->trans_rollback(); 
								echo '{success:false}';	
								}
							}
							else
							{
							$this->db->trans_rollback(); 
							echo '{success:false}';	
							}
			
	
		
	}
	
}
?>