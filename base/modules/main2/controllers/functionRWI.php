<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionRWI extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {

 
          $this->load->view('main/index',$data=array('controller'=>$this));

    }
	public function cekpassword_rwi(){
    	$q=$this->db->query("select * from sys_setting where key_data='rwi_password_batal_transaksi_kasir' and setting=md5('".$_POST['passDulu']."') ")->result();
		if ($q)
		{
			echo '{success:true}';
		}
		else{
			echo '{success:false}';
		}
    }
	public function UpdateKdCustomer()
	 {
		 
		 $KdTransaksi= $_POST['TrKodeTranskasi'];
		 $KdUnit= $_POST['KdUnit'];
		 $KdDokter= $_POST['KdDokter'];
		 $TglTransaksi= $_POST['TglTransaksi'];
		 $KdCustomer= $_POST['KDCustomer'];
		 $NoSjp= $_POST['KDNoSJP'];
		 $NoAskes= $_POST['KDNoAskes'];
		 $KdPasien = $_POST['TrKodePasien'];
		 
       $query = $this->db->query("select updatekdcaostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoSjp."','".$NoAskes."','".$KdCustomer."')");
		 $res = $query->result();
		 if($res)
		 {
			 echo "{success:true}";
		 }
		 else
		 {
			  echo "{success:false}";
		 }
	 }
	 
	 public function kelaskamar()
	{
    	$result=$this->db->query("select unit.nama_unit||'-'||kamar.nama_kamar as kelas_kamar,kelas.kelas as kelas,kamar.nama_kamar  as kamar from kamar
								inner join unit on unit.kd_unit=kamar.kd_unit
								inner join kelas on kelas.kd_kelas=unit.kd_kelas where lower(kamar.nama_kamar) like lower('".$_POST['kode']."%') or 
								lower(unit.nama_unit) like lower('".$_POST['kode']."%')")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	public function KonsultasiPenataJasa()
        {
					$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
                    $kdTransaksi = $this->GetIdTransaksi($kdKasir);

                    $kdUnit = $_POST['KdUnit'];
                    $kdDokter = $_POST['KdDokter'];
                    $kdUnitAsal = $_POST['KdUnitAsal'];
                    $kdDokterAsal = $_POST['KdDokterAsal'];
                    $tglTransaksi = $_POST['TglTransaksi'];
                    $kdCostumer = $_POST['KDCustomer'];
                    $kdPasien = $_POST['KdPasien'];
                    $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
                   

                    if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                         {
                                 list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                                 $ctgl= strtotime($tgl.$bln.$thn);
                                 $tgl_masuk=date("Y-m-d",$ctgl);
                             }

                    $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."','".$kdCostumer."','".$kdKasir."','".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."',".$antrian.")");
                    $res = $query->result();


                    if($res)
                    {
                            echo '{success: true}';
                    }
                    else
                    {
                            echo '{success: false}';
                    }
            } 
        
        private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
        /* private function GetIdTransaksi()
        {
            $strNomor="0".str_pad("00",2,'0',STR_PAD_LEFT);
            $retVal=$strNomor."0001";

            $this->load->model('general/tb_transaksi');
            $this->tb_transaksi->db->where("substring(no_transaksi,1,3) = '".$strNomor."'", null, false);
            $res = $this->tb_transaksi->GetRowList( 0, 1, "DESC", "no_transaksi",  "");

            if ($res[1]>0)
            {
                $nm = substr($res[0][0]->NO_TRANSAKSI, -4);
                $nomor = (int) $nm +1;
                $retVal=$strNomor.str_pad($nomor,4,"00000",STR_PAD_LEFT);
           }
     return $retVal;
    } */
	
		private function GetIdTransaksi($kd_kasir){
			$kd_kasir_rwi= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
			   $counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir_rwi'")->row();
				$no = $counter->counter;
				$retVal2 = $no+1;
				$strNomor='';
				$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kd_kasir'");
					$retVal=$strNomor.str_pad($retVal2,7,"000000",STR_PAD_LEFT);
			return $retVal;
		}
	
    public function savePembayaran()
	{
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $this->GetShiftBagian();
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
                {
                    $urut_detailbayar = 1;
                }  else {
                    foreach($det_query->result() as $det)
                    {
                            $urut_detailbayar = $det->urutan+1;
                    }
                }
                
		
		
		if($jmllist > 1)
		{
		$a = explode("##[[]]##",$list);
			for($i=0;$i<=count($a)-1;$i++)
				{
					
					$b = explode("@@##$$@@",$a[$i]);
					for($k=0;$k<=count($b)-1;$k++)
					{
						
			
				
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];
				
				if($_Typedata == 0)
				{
				 $harga = $b[6];
				}
				else if($_Typedata == 1)
				{
				$harga = $b[8];	
				}
				else if ($_Typedata == 3)
				{
				$harga = $b[7];	

				}
				
				}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}

				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",
				'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
			$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
				".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
				".$urut_detailbayar.")")->result();

		}
	}
		
		else
		{
			
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];
				
							if($_Typedata == 0)
							{
							 $harga = $b[6];
							}
							 if($_Typedata == 1)
							{
							$harga = $b[8];	
							}
							 if ($_Typedata == 3)
							{
							$harga = $b[7];	
							}
							
							
			
	
			}
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
				$urutanbayar = $r->geturutbayar;
				}
				
				
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."',
				'".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
                $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$b[9]."',
				".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',
				".$urut_detailbayar.")")->result();
				
		}
						
			    if($pembayaran)
				{
				echo '{success:true}';	
				}
				else
				{
				echo '{success:false}';	
				}
			
	
		
	}
	
	public function saveDiagnosa()
	{
		
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		
	
		$a = explode("##[[]]##",$list);

		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
							if($b[$k] == 'Diagnosa Awal')
							{
								$diagnosa = 0;
							}
							else if($b[$k] == 'Diagnosa Utama')
							{
								$diagnosa = 1;
							}
				
							else if($b[$k] == 'Komplikasi')
							{
								$diagnosa = 2;
							}
							else if($b[$k] == 'Diagnosa Sekunder')
							{
								$diagnosa = 3;
							}
							else if($b[$k] == 'Baru')
							{
								$kasus = 'TRUE';
							}
							else if($b[$k] == 'Lama')
							{
								$kasus = 'FALSE';
							}
			}
				$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
				$result = $urut->result();
				foreach ($result as $data)
				{
					$Urutan = $data->geturutmrpenyakit;
				}
			
				$query = $this->db->query("select insertdatapenyakit('".$b[1]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");	
			
		
		}
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}
	}
	
	public function deletedetail_bayar()
	{
	$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
	$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
	$Urut =$_POST['Urut'];
	$query=$this->db->query(
						 "  select hapusdetailbayar('$kdKasir',
						 '".$TrKodeTranskasi."',
						 ".$Urut.")"
						);
						if($query)
						{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}
	}
public function detail_obatrwi()
{
		$Tgl = date("Y-m-d");
		$urut=$this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1')->row();
		$urut=substr($urut->id_mrresep,8,12);
		$sisa=4-count(((int)$urut+1));
		$real=date('Ymd');
		for($i=0; $i<$sisa ; $i++)
		{
		$real.="0";
		}
		$real.=((int)$urut+1);
		$urut=$real;
		$result=$this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
		if($result->jumlah>0)
		{
			$result=$this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
			tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
			$urut=$result->id_mrresep;
			$update=$this->db->query("update mr_resep set order_mng=false WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
			tgl_masuk='".$_POST['Tgl']."' AND urut_masuk='".$_POST['urut_masuk']."'");
		}else
		{
			$kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
			$kd='0';
			if(isset($kd_dokter->kd_dokter))
			{
				$kd=$kd_dokter->kd_dokter;
			}
			$mr_resep=array();
			$mr_resep['kd_pasien']=$_POST['kd_pasien'];
			$mr_resep['kd_unit']=$_POST['KdUnit'];
			$mr_resep['tgl_masuk']=$_POST['Tgl'];
			$mr_resep['urut_masuk']=$_POST['urut_masuk'];
			$mr_resep['kd_dokter']=$kd;
			$mr_resep['id_mrresep']=$urut;
			$mr_resep['cat_racikan']='';
			$mr_resep['tgl_order']=$Tgl;
			$mr_resep['dilayani']=0;
			$this->db->insert('mr_resep',$mr_resep);
		}
		$result=$this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=".$urut)->result();
		for($i=0; $i<count($result); $i++)
		{
			$ada=false;
			for($j=0; $j<$_POST['jmlObat']; $j++)
			{
				if($result[$i]->urut==($j+1) && $result[$i]->kd_prd==$_POST['kd_prd'.$j])
				{
					$ada=true;
				}
			}
		
		}
		for($i=0; $i<$_POST['jmlObat']; $i++)
		{
			$status=0;
			if($_POST['urut'.$i]==0 ||$_POST['urut'.$i]=='' ||$_POST['urut'.$i]=='undefined')
			{
			$urut_order=($i+1);
			}else
			{
			$urut_order=$_POST['urut'.$i];
			}
			if($_POST['verified'.$i]=='Not Verified')
			{
				$status=1;
			}
			$result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".$urut_order.",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",'".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",
			".$_POST['racikan'.$i].") ");
		}
		if($result)
		{
		echo "{success:true}";
		}
		else
		{
		echo "{success:false}";
		}	

}
    public function savedetailpenyakit()
	{	
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$this->db->trans_begin();
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		 date_default_timezone_set('Asia/Jakarta');
		$Tgl =gmdate("Y-m-d", time()+60*60*7);
		$Shift =$this->GetShiftBagian();
		//$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		//urut_masuk
		if ($_POST['Tgl']=='')
		{
			$Tgl=$_POST['Tgl1'];
		}
		else
		{
			$Tgl =$_POST['Tgl'];
		}
		$query_ambilkamar=$this->db->query("select * from nginap  where kd_pasien='".$_POST['kd_pasien']."' and kd_unit ='".$_POST['KdUnit']."' and urut_masuk=".$_POST['urut_masuk']."
		and tgl_masuk='".$Tgl."'
		and akhir=true and tgl_keluar isnull")->result();
		foreach ($query_ambilkamar as $datainap)
				{ 
				$kd_unit_kamar=$datainap->kd_unit_kamar;
				$no_kamar=$datainap->no_kamar;
				$kd_spesial=$datainap->kd_spesial;
				}
			$urut = $this->db->query("select geturutdetailtransaksi('$kdKasir','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
				foreach ($result as $data)
				{ 
					if($_POST['Urut']==0 || $_POST['Urut']=='' )
					{
						$Urutan = $data->geturutdetailtransaksi;
					}
					else
					{
						if ($_POST['Tgl']=='')
						{
							$Tgl=$_POST['Tgl1'];
						}
						else
						{
							$Tgl =$_POST['Tgl'];
						}
						$Urutan =$_POST['Urut'];
					}
				}
				//echo $Tgl;
				//echo "qty nya : ".$Tgl;
				$query = $this->db->query("select insert_detail_transaksi
				('$kdKasir','".$TrKodeTranskasi."',".$Urutan.",
				'".$Tgl."', 0,'".$_POST['KD_TARIF']."',
				".$_POST['KD_PRODUK'].",'".$KdUnit."',
				'".$_POST['TGLBERLAKU']."',
				'false','true','',".$_POST['QTY'].",
				".$_POST['HARGA'].",".$Shift.",'false'
				)");	
			
		$query_detail_trkamar=$this->db->query("select * from detail_tr_kamar where kd_kasir='$kdKasir' and no_transaksi='".$TrKodeTranskasi."' and urut='".$Urutan."'")->result();
		if (count($query_detail_trkamar)==0)
		{
				$query = $this->db->query("insert into detail_tr_kamar values
				('$kdKasir','".$TrKodeTranskasi."',".$Urutan.",
				'".$Tgl."','".$kd_unit_kamar."','".$no_kamar."',
				'".$kd_spesial."')");	
		}
		
		$query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' and kd_dokter='".$_POST['kdDokter']."' ")->result();
		$query_mr_tindakan=$this->db->query("select max(urut) from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
		$c_urutTindakan=count($query_mr_tindakan);
		$urutTindakan=0;
		if ($c_urutTindakan<>0)
		{
			$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->row()->jml;
			$urutTindakan=$max+1;
		}
		$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$_POST['KD_PRODUK']."'")->row()->kd_klas;
			if ($cek_klas<>'9' && $cek_klas<>'1')
			{
				foreach ($query_cek_kunjungan as $datatindakan)
				{
					$q_cek_mrtind=$this->db->query("select * from mr_tindakan where kd_produk='".$_POST['KD_PRODUK']."' and kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
					$jmlProd=count($q_cek_mrtind);
					if ($jmlProd==0)
					{
						$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$_POST['KD_PRODUK']."','".$_POST['kd_pasien']."','$KdUnit',
						'$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TrKodeTranskasi."','$kdKasir')");	
						$urutTindakan+=1;
					}
					
				}
			}	
			if($query)
				{
					$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
					$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
					$getcounkomponent=$this->db->query("Select count(kd_component) as compo, sum(tarif) as total from detail_component where kd_kasir='$kdKasir' and 
					no_transaksi='".$TrKodeTranskasi."' and urut=$Urutan and tgl_transaksi='".$Tgl."' and kd_component in('".$kdjasadok."','".$kdjasaanas."') ")->result();
															foreach($getcounkomponent as $getcounkomponent_1)
															{
															$countcompo = $getcounkomponent_1->compo;
															$sumtarif = $getcounkomponent_1->total;
															}
															if($countcompo>0)
																	{
																	echo "{success:true, compo:true, tarif:".$sumtarif."}";
																	$this->db->trans_commit();
																	}
															else{
																echo"{success:true}";
																$this->db->trans_commit();
																}
															
				}
				else
				{
					echo "{success:false}";
				    $this->db->trans_rollback();
				}
				
			
	} 
	
private function GetShiftBagian()
{
			
		/*	$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$sqlbagianshift = $db->query("SELECT  CONVERT(VARCHAR(3), shift)AS SHIFTBAG FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row();
			$sqlbagianshift2 = $sqlbagianshift->SHIFTBAG;*/
		
			//$sqlbagianshift2=1;
			$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->shift;
			$lastdate = $this->db->query("SELECT   to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->lastdate;
				$datnow= date('Y-m-d');
			if($lastdate<>$datnow && $sqlbagianshift==='3')
			{
			$sqlbagianshift2 = '4';
			}else{
			$sqlbagianshift2 = $sqlbagianshift;
			}
			
        return $sqlbagianshift2;
}	
public function savetrdokter()
{		
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$KdProd = $_POST['KD_PRODUK'];
		if ($_POST['TGLTRANSAKSI']===''||$_POST['TGLTRANSAKSI']==='null')
		{
		$Tgl =date("Y-m-d");
		}
		else
		{
		$Tgl =$_POST['TGLTRANSAKSI'];
		}
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{
			$kdKomp	= $_POST['KD_KOMPONEN-'.$i];
			$prc	= $_POST['PRC-'.$i];
			$b = explode("@@##$$@@",$a[$i]);
			for($k=0;$k<=count($b)-1;$k++)
			{
			}
			$c = explode("-",$b[1]);
			$d = explode("-",$b[4]);
			$result=$this->db->query("select * from visite_dokter WHERE no_transaksi='".$TrKodeTranskasi."' AND urut='".$_POST['Urut']."' 
			AND kd_kasir='$KASIRRWI' AND tgl_transaksi='$Tgl'" )->result();
			$hasil=array();
			if(count($result)>0)
			{
				$jumlah_dokter=count($result)+1;
				$query = $this->db->query("insert into visite_dokter( kd_kasir,no_transaksi , urut, tgl_transaksi , line,kd_dokter , kd_unit,tag_int, tag_char,kd_job , prc, jp )values
						('$KASIRRWI','".$TrKodeTranskasi."',".$_POST['Urut'].",'".$Tgl."','".$jumlah_dokter."','$c[0]','".$KdUnit."','".$KdProd."','".$kdKomp."','$d[0]','".$prc."',$b[3])");
			}
			else
			{
				$query = $this->db->query("insert into visite_dokter( kd_kasir, no_transaksi , urut,tgl_transaksi ,line,kd_dokter , kd_unit,tag_int,tag_char, kd_job , prc, jp )
						  values('$KASIRRWI','".$TrKodeTranskasi."',".$_POST['Urut'].",'".$Tgl."','1','$c[0]','".$KdUnit."','".$KdProd."','".$kdKomp."','$d[0]','".$prc."',$b[3])");
			}
			/* $query = $this->db->query("update detail_component set tarif=$b[3] where kd_kasir='$KASIRRWI' and no_transaksi='".$TrKodeTranskasi."' 
			and urut='".$_POST['Urut']."' and tgl_transaksi='".$Tgl."' and kd_component='".$kdKomp."' ");
		 */
		}if($query)
		{
			echo "{success:true}";
		}
		else
		{
			echo "{success:false}";
		}												
	} 
	
	public function cekKeVisiteDokterDiawal()
	{
		if ($_POST['no_transaksi']=='')
		{
			$jsonResult=array();
			$jsonResult['processResult']='NOT SUCCESS';
			$jsonResult['listData']='Kosong';
			$jsonResult['jumlah']=0;
			echo json_encode($jsonResult);
		}
		else
		{
			$result=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->result();
			$result2=$this->db->query("select * from produk where kd_produk='".$_POST['kd_produk']."'")->row()->kd_klas;
			$kd_produk=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->row()->tag_int;
			$urut=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->row()->urut;
			$jml=count($result);
			if ($result)
			{
				$jsonResult=array();
				$jsonResult['processResult']='SUCCESS';
				$jsonResult['listData']=$result;
				$jsonResult['jumlah']=$jml;
				$jsonResult['kd_produk']=$kd_produk;
				$jsonResult['urut']=$urut;
			}
			else{
				$jsonResult=array();
				$jsonResult['processResult']='NOT SUCCESS';
				$jsonResult['listData']='kosong';
				$jsonResult['jumlah']=$jml;
			}
			
			$jsonResult['kd_klas']=$result2;
			if ($kd_produk)
			{
				$jsonResult['kd_produk']=$kd_produk;
			}
			else{
				$jsonResult['kd_produk']='kosong';
			}
			/* if ($urut)
			{
				$jsonResult['urut']=$urut;
			}
			else{
				$jsonResult['urut']='kosong';
			} */
			echo json_encode($jsonResult);
		}
		
	}
	public function getVisiteDokterDiawal()
	{
		if ($_POST['no_transaksi']=='')
		{
			$jsonResult=array();
			$jsonResult['processResult']='NOT SUCCESS';
			$jsonResult['listData']='Kosong';
			$jsonResult['jumlah']=0;
			echo json_encode($jsonResult);
		}
		else
		{
		$result=$this->db->query("select d.nama as kd_nama, vd.urut, vd. tgl_transaksi, vd.tag_int from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and vd.tag_int='".$_POST['kd_produk']."' 
									and vd.urut='".$_POST['urut']."'")->result();
		/* $kd_klas=substr($_POST['kd_klas'], 0,2);
			if ($kd_klas=='61')
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and vd.tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=1")->result();
			}
			else
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter 
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and vd.tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=0")->result();
			} */
		$jml=count($result);
		$nm_dok='';
		if ($jml==1)
		{
			foreach ($result as $line)
			{
				$nm_dok.=$line->kd_nama;
			}
		}
		else
		{
			foreach ($result as $line)
			{
				$nm_dok.=$line->kd_nama.';';
			}
		}
		
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['jumlah']=$jml;
		$jsonResult['nama_dok']=$nm_dok;
    	echo json_encode($jsonResult);
		}
	}
	public function cekKeVisiteDokter()
	{
		if ($_POST['no_transaksi']=='')
		{
			$jsonResult=array();
			$jsonResult['processResult']='NOT SUCCESS';
			$jsonResult['listData']='Kosong';
			$jsonResult['jumlah']=0;
			echo json_encode($jsonResult);
		}
		else
		{
			$result=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->result();
			$result2=$this->db->query("select * from produk where kd_produk='".$_POST['kd_produk']."'")->row()->kd_klas;
			$jml=count($result);
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			$jsonResult['jumlah']=$jml;
			$jsonResult['kd_klas']=$result2;
			echo json_encode($jsonResult);
		}
		
	}
	public function getVisiteDokter()
	{
		$kd_klas=substr($_POST['kd_klas'], 0,2);
			if ($kd_klas=='61')
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' 
									and tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=1")->result();
			}
			else
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter 
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."'
									and tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=0")->result();
			}
		$iniTarif=0;
		foreach ($result as $a)
		{
			$iniTarif+=$a->jpp;
		}
		$jml=count($result);
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['jumlah']=$jml;
		$jsonResult['iniTarif']=$iniTarif;
    	echo json_encode($jsonResult);
	}
	public function hapustrdokter()
	{	
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$Tgl =date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{
			$b = explode("@@##$$@@",$a[$i]);
			for($k=0;$k<=count($b)-1;$k++)
			{
			}
			$c = explode("-",$b[1]);
			$d = explode("-",$b[4]);
			$query = $this->db->query("delete from detail_trdokter where kd_kasir='$KASIRRWI'and 
					  no_transaksi ='".$TrKodeTranskasi."'  and
					  urut=".$_POST['Urut']."
					  and tgl_transaksi='".$Tgl."'
					  and kd_dokter= '$c[0]' 
					");
	
		}
		if($query)
		{
		echo "{success:true}";
		}
		else
		{
		echo "{success:false}";
		}												
	} 
	public function ubah_co_status_transksi()
	{
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		/* $KDcus = $_POST['KDcus'];
		$TrKDUnit = $_POST['TrKDUnit'];
		$TrURUTMasuk = $_POST['TrURUTMasuk'];
		$TrNOkamar = $_POST['TrNOkamar'];
		$TrKDkelas = $_POST['TrKDkelas'];
		$TrTgl = $_POST['TrTgl'];
		$TrKdPasien = $_POST['TrKdPasien'];
		$TrUnit_kamar = $_POST['TrUnit_kamar'];
		$Trtapungalasan = $_POST['Trtapungalasan'];
		$kdunit = $_POST['KDUnit']; */
		$kdKasir =$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$query = $this->db->query(" update transaksi set  ispay='true', co_status='true' ,  tgl_co='".date("Y-m-d")."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='".$kdKasir."'");	
		if($query)
		{
		echo "{success:true}";
		/* $querykamar=  $this->db->query("Update Nginap Set Tgl_Keluar = '".date("Y-m-d H:m:s")."' , Akhir='true' Where Kd_Pasien = '" .$TrKdPasien."' And 
		Kd_Unit = '".$TrKDUnit."' And Tgl_Masuk = '".$TrTgl."' And Urut_Masuk = " .$TrURUTMasuk. " 
		And Kd_Unit_Kamar = '" .$TrUnit_kamar. "' And No_Kamar = '".$TrNOkamar ."' And Tgl_Keluar IS NULL");
			if($querykamar)
			{
				$querykunjungan= $this->db->query("Update Kunjungan Set Tgl_Keluar = '" .date("Y-m-d"). "', Keadaan_Pasien = " .$Trtapungalasan.", Jam_Keluar = '" .date("Y-m-d H:m:s"). "' 
				Where Kd_Pasien = '".$TrKdPasien. "' And Kd_Unit = '" .$TrKDUnit."' And Urut_Masuk = " .$TrURUTMasuk. " And Tgl_Keluar IS NULL ");
				if($querykunjungan)
				{
					$baru = $this->db->query("update kamar set digunakan = digunakan -1 Where kd_unit='$TrUnit_kamar' AND no_kamar='$TrNOkamar'");
					$baru2 = $this->db->query("update kamar_induk set digunakan = digunakan -1 Where no_kamar='$TrNOkamar'");
					$querypasien=$this->db->query("Delete From Pasien_Inap Where Kd_Kasir = '$KASIRRWI' And No_Transaksi = '".$TrKodeTranskasi."'");
					if($querypasien)
					{
					echo "{success:true}";
					}
					else
					{
					echo "{success:false}";
					}
				}
				else
				{
				echo "{success:false}";
				}
			}
			else 
			{
			echo "{success:false}";
			} */		
		}
		else
		{
		echo "{success:false}";
		}
	 
	}
        
	public function GetRecordPenyakit($kd,$nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%".$kd."%' OR penyakit ilike '%".$nama."%' ");
		$count = $query->num_rows();
		return $count;
	}
        
    
	public function getTindakan(){
		$result=$this->db->query("select id_status,catatan from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' 
		AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		$hasil=array();
		if(count($result)>0){
			$hasil['id_status']=$result[0]->id_status;
			$hasil['catatan']=$result[0]->catatan;
		}else{
			$hasil['id_status']=-1;
			$hasil['catatan']='';
		}
		
		echo "{success:true,echo:".json_encode($hasil)."}";
	}
        
    public function saveTindakan()
	{
		$this->db->trans_begin();
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$urut=$this->db->query('select id_rwirujukan AS code FROM mr_rwi_rujukan order by id_rwirujukan desc limit 1')->row();
		$id='';
		if(isset($urut->code)){
			$urut=substr($urut->code,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$id=$real;
		}else{
			$id=date('Ymd').'0001';
		}
		
		$count=$this->db->query("select * from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND
		tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($count)>0){
			$id=$count[0]->id_rwirujukan;
			$this->db->where('id_rwirujukan',$id);
			$mr_rwi_rujukan=array();
			$mr_rwi_rujukan['id_status']=$_POST['id_status'];
			$mr_rwi_rujukan['catatan']=$_POST['catatan'];
			if(isset($_POST['kd_unit_tujuan'])){
				$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
			}
			$this->db->update('mr_rwi_rujukan',$mr_rwi_rujukan);
		}else
		{
			$mr_rwi_rujukan=array();
			$mr_rwi_rujukan['kd_pasien']=$_POST['kd_pasien'];
			$mr_rwi_rujukan['kd_unit']=$_POST['kd_unit'];
			$mr_rwi_rujukan['tgl_masuk']=$_POST['tgl_masuk'];
			$mr_rwi_rujukan['urut_masuk']=$_POST['urut_masuk'];
			$mr_rwi_rujukan['id_rwirujukan']=$id;
			$mr_rwi_rujukan['id_status']=$_POST['id_status'];
			$mr_rwi_rujukan['catatan']=$_POST['catatan'];
			if(isset($_POST['kd_unit_tujuan'])){
				$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
			}
			$this->db->insert('mr_rwi_rujukan',$mr_rwi_rujukan);
		}

		$data = array();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        $today = date('Y-m-d H:i:s');
        $kd_pasien = $this->input->post('kd_pasien');
        $kd_unit = $this->input->post('kd_unit');
        $tgl_masuk = $this->input->post('tgl_masuk');
        $urut_masuk = $this->input->post('urut_masuk');
        $tglkeluar = $this->input->post('tglkeluar');
        $carakeluar = $this->input->post('carakeluar');
		if($carakeluar==='' )
		{
		$carakeluar=0;
		}
        $keadaanakhir = $this->input->post('keadaanakhir');
        $sebabmati = $this->input->post('sebabmati');
		$query_cari_kunjungan=$this->db->query("
			select * from kunjungan 
			where kd_pasien = '$kd_pasien' and
				  kd_unit = '$kd_unit' and 
                  tgl_masuk = '$tgl_masuk' and
				  urut_masuk = $urut_masuk and
				  tgl_keluar  isnull 
				  ")->result();
		if(count($query_cari_kunjungan)==0)
		{
		echo "{success:false,cari: true}";
		}
		else
		{		
			$datacari = array(
				'kd_pasien' => $kd_pasien,
				'kd_unit' => $kd_unit,
				'tgl_masuk' => $tgl_masuk,
				'urut_masuk' => $urut_masuk,
			);
			$TrNOkamar=$_POST['no_kamar'];
			$TrUnit_kamar=$_POST['kd_unit_kamar'];
			$TrKodeTranskasi=$_POST['no_transaksi'];
			$data = array(
				'cara_keluar' => $carakeluar,
				'keadaan_pasien' => $keadaanakhir,
				'sebabmati' => $sebabmati,
				'tgl_keluar' => $tglkeluar,
				'jam_keluar' => $today
			);
			$this->db->where($datacari);
			$this->db->update('kunjungan', $data);
			$querykamar=  $this->db->query("Update Nginap Set Tgl_Keluar = '".date("Y-m-d H:m:s")."' , Akhir='true' Where Kd_Pasien = '" .$kd_pasien."' And 
			Kd_Unit = '".$kd_unit."' And Tgl_Masuk = '".$tgl_masuk."' And Urut_Masuk = " .$urut_masuk. " 
			And Kd_Unit_Kamar = '" .$TrUnit_kamar. "' And No_Kamar = '".$TrNOkamar ."' And Tgl_Keluar IS NULL");
			if($querykamar)
			{
				$baru = $this->db->query("update kamar set digunakan = digunakan -1 Where kd_unit='$TrUnit_kamar' AND no_kamar='$TrNOkamar'");
				$baru2 = $this->db->query("update kamar_induk set digunakan = digunakan -1 Where no_kamar='$TrNOkamar'");
				$querypasien=$this->db->query("Delete From Pasien_Inap Where Kd_Kasir = '$KASIRRWI' And No_Transaksi = '".$TrKodeTranskasi."'");
				if($querypasien)
				{
					$regpulang=$this->regonline_pasienpulang($_POST['kd_pasien']);
					if($regpulang){
						$this->db->trans_commit();
						echo "{success:true}";
					} else{
						$this->db->trans_rollback();
						echo "{success:false}";
					}
					
				}
				else
				{
					$this->db->trans_rollback();
					echo "{success:false}";
				} 
			}
			else 
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			} 
		}
		//echo "{success:true}";
	}
        
        public function saveDiagnosaPoliklinik(){
		$this->db->trans_begin();
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$inser_batch=array();
		$this->db->query("DELETE FROM mr_penyakit WHERE kd_pasien='".$kdPasien."' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk='".$urut_masuk."'");
		for($i=0;$i<$jmlList;$i++){
			if($_POST['STAT_DIAG'.$i] == 'Diagnosa Awal'){
				$diagnosa = 0;
			}else if($_POST['STAT_DIAG'.$i]  == 'Diagnosa Utama'){
				$diagnosa = 1;
			}else if($_POST['STAT_DIAG'.$i]  == 'Komplikasi'){
				$diagnosa = 2;
			}else if($_POST['STAT_DIAG'.$i] == 'Diagnosa Sekunder'){
				$diagnosa = 3;
			}
			if($_POST['KASUS'.$i] == 'Baru'){
				$kasus = 'TRUE';
				$zkasus = 1;
			}else if($_POST['KASUS'.$i] == 'Lama'){
				$kasus = 'FALSE';
				$zkasus = 0;
			}
			$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
			$result = $urut->result();
			foreach ($result as $data){
				$Urutan = $data->geturutmrpenyakit;
			}
			$query = $this->db->query("select insertdatapenyakit('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
			if($_POST['NOTE'.$i]==1){
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$query = $this->db->query("select insertneoplasma('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.",".$Urutan.",'".$_POST['DETAIL'.$i]."')");
			}else if($_POST['NOTE'.$i]==2){
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$query = $this->db->query("select insertkecelakaan('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.",".$Urutan.",'".$_POST['DETAIL'.$i]."')");
			}else if($_POST['NOTE'.$i]==0){
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
			}
		}
		if($this->db->trans_status()==true){
			$this->db->trans_commit();
			echo "{success:true,echo:".$this->db->trans_status()."}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}	
	}
        
        public function deletelaboratorium(){
		$result=$this->db->query("select * from mr_labkonsul WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($result)>0){
			$id=$result[0]->id_labkonsul;
			$this->db->query("DELETE FROM mr_labkonsuldtl WHERE id_labkonsul='".$id."' AND kd_produk='".$_POST['kd_produk']."'");
		}
		echo "{success:true}";
	}
        
        public function savelaboratorium(){
		//create id
		$urut=$this->db->query('select id_labkonsul AS code FROM mr_labkonsul order by id_labkonsul desc limit 1')->row();
		$id='';
		if(isset($urut->code)){
			$urut=substr($urut->code,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$id=$real;
		}else{
			$id=date('Ymd').'0001';
		}
		$count=$this->db->query("select * from mr_labkonsul WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND tgl_masuk='".$_POST['tgl_masuk']."' 
		AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($count)>0){
			$id=$count[0]->id_labkonsul;
		}else{
			$mr_labkonsul=array();
			$mr_labkonsul['kd_pasien']=$_POST['kd_pasien'];
			$mr_labkonsul['kd_unit']=$_POST['kd_unit'];
			$mr_labkonsul['tgl_masuk']=$_POST['tgl_masuk'];
			$mr_labkonsul['urut_masuk']=$_POST['urut_masuk'];
			$mr_labkonsul['id_labkonsul']=$id;
			$this->db->insert('mr_labkonsul',$mr_labkonsul);
		}
		$count=$this->db->query("delete from mr_labkonsuldtl WHERE id_labkonsul='".$id."' ");
		for($i=0; $i<$_POST['jum'] ; $i++){
				$mr_labkonsuldtl=array();
				$mr_labkonsuldtl['id_labkonsul']=$id;
				$mr_labkonsuldtl['kd_lab']=$_POST['kd_lab'.$i];
				$mr_labkonsuldtl['kd_produk']=$_POST['kd_produk'.$i];
				$r=$this->db->query("select * from dokter where nama='".$this->session->userdata['user_id']['username']."'")->result();
				if(count($r)>0){
					$code=$r[0]->kd_dokter;
				}else{
					$code='0';
				}
				$mr_labkonsuldtl['kd_dokter']=$code;
				$this->db->insert('mr_labkonsuldtl',$mr_labkonsuldtl);
		}
		echo "{success:true}";
	}
        
 public function saveTransfer()
	{
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$_POST['Shift'];
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$det_query = $this->db->query("select urut+1 as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urutan desc limit 1")->result();
		foreach($det_query as $det)
		{
			$urut_detailbayar = $det->urutan;
		}
			if($urut_detailbayar=="")
			{
			$urut_detailbayar=1;
			}
			$pay_query = $this->db->query(" insert into detail_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
			values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
						
			if($pay_query)
			{
				$detailTrbayar = $this->db->query("	insert into detail_tr_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
				if($detailTrbayar)
				{	
					$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
					if($statuspembayaran)
					{
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");	
						if($detailtrcomponet)
						{	
						
							$urutquery = $this->db->query("select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1")->result();
							foreach($urutquery as $det)
							{
							$uruttujuan = $det->urutan;
							}
							if($uruttujuan=="")
							{
							$uruttujuan=1;
							}
							$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
							FROM Produk_Charge pc 
							INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
							WHERE left(kd_unit, 2)='2'")->result();
							foreach($getkdproduk as $det)
							{
							$kdproduktranfer = $det->kdproduk;
							$kdUnittranfer = $det->unitproduk;
							}
							$detailtransaksitujuan = $this->db->query("
							INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							'$_kduser', 'TU','$kdproduktranfer','$kdUnittranfer','2014-03-01','true','true','',1,$total,$Shift1,'false','$TrKodeTranskasi')
							");	
							if($detailtransaksitujuan)	
							{
								$detailcomponentujuan = $this->db->query
								("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,jumlah,0
								from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi' and Kd_Kasir = '$KDkasirIGD'");	
								   if($detailcomponentujuan)
								   { 
									$tranferbyr = $this->db->query("INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
									det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan) values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
									'$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");																  
									if($tranferbyr)
									{
									 echo '{success:true}';
									}
									else
									{ 
									 echo '{success:false}';	
									}
								   }
								   else
								   {
								    echo '{success:false}';	
								   }
							}
							else
							{
							 echo '{success:false}';	
							}
						}
						else
						{
						 echo '{success:false}';	
						}
					}								
					else
					{
					 echo '{success:false}';	
					}
				}
				else
				{
				echo '{success:false}';	
				}
			}
			else
			{
			echo '{success:false}';	
			}
			
	
		
	}
        
        public function getProduk()
        {
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 $row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
    	$result=$this->db->query(
		"select row_number() OVER () as rnum,rn.*, 1 as qty 
         from(
         select produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual,
		 produk.kp_produk, produk.kd_kat, produk.kd_klas, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
		 max (tarif.tgl_berlaku) as tglberlaku,tarif.tarif as tarifx, tarif.tgl_berakhir,tr.jumlah,
         row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
         from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
         inner join tarif on produk.kd_produk = tarif.kd_produk 
         inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
         inner join unit on tarif.kd_unit = unit.kd_unit 
         left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku
         from tarif_component where kd_component = '".$kdjasadok."' or kd_component = '". $kdjasaanas."' group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
         tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND 
         tr.kd_tarif=tarif.kd_tarif 
         Where tarif.kd_unit ='".$_POST['kd_unit']."' and lower(tarif.kd_tarif)=LOWER('".$row->kd_tarif."') 
		 and upper(produk.deskripsi) like upper('%".$_POST['text']."%')
         and tarif.tgl_berlaku <= '".date('Y-m-d')."'
         group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit,
         unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
         klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku   
         order by produk.deskripsi asc ) 
         as rn where rn = 1 order by rn.deskripsi asc limit 10"
		 )->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	
	public function gettrdokter()
	{
    	$result=$this->db->query("select kd_dokter,nama from dokter where LOWER(kd_dokter) like LOWER('%".$_POST['text']."%') or 
		LOWER(nama) like LOWER('%".$_POST['text']."%')limit 20")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	
	public function getdokter_inap_int($a)
    {
		$kd_klas=substr($a, 0,2);
			if ($kd_klas=='61')
			{
				$result=$this->db->query("select * from dokter_inap_int where LOWER(label) like LOWER('%".$_POST['text']."%') and groups=1 limit 20")->result();
			}
			else
			{
				$result=$this->db->query("select * from dokter_inap_int where LOWER(label) like LOWER('%".$_POST['text']."%') and groups=0 limit 20")->result();
			}
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['coba']=substr($a, 0,2);
    	echo json_encode($jsonResult);
    }
	
	public function cari_trdokter ()
	{	
		
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$getcounkomponent=$this->db->query("Select count(kd_component) as compo, sum(tarif) as total from detail_component where kd_kasir='$KASIRRWI' and 
		no_transaksi='".$_POST['no_transaksi']."' and urut=".$_POST['urut']." and tgl_transaksi='".$_POST['tgl_transaksi']."' and kd_component in('".$kdjasadok."','". $kdjasaanas."') ")->result();
		foreach($getcounkomponent as $getcounkomponent_1)
		{
		$countcompo = $getcounkomponent_1->compo;
		$sumtarif = $getcounkomponent_1->total;
		}
		if($countcompo>0)
		{
		 echo "{success:true, compo:true, tarif:".$sumtarif."}";
		}
		else
		{
		 echo"{success:false}";
		}
		
	}
	public function batal_transaksi()
	{
		$this->db->trans_begin();
		try
		{
			$kd_kasir  =$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
			$kd_pasien = $this->input->post('kdPasien');
			$kd_unit  = $this->input->post('kdUnit');
			$kd_dokter = $this->input->post('kdDokter');
			$tgl_trans = $this->input->post('tglTrans');
			$no_trans = $this->input->post('noTrans');
			$kd_customer = $this->input->post('kdCustomer');
			$keterangan = $this->input->post('Keterangan');
	
			$_kduser = $this->session->userdata['user_id']['id'];
			$username = $this->session->userdata['user_id']['username'];
			
			$notrans = $this->GetIdTransaksi($kd_kasir);
			$Schurut = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
    
			$nama = $this->db->query("select nama from pasien where kd_pasien='".$kd_pasien."'")->row()->nama;
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
			
			
			/* //transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			
			echo $notrans;
			echo "<br>";
			echo $no_trans;
			echo "<br>";
			echo $notransbaru;
			 */
			
			$datahistory = array(
				"kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"nama"=>$nama,
				"kd_unit"=>$kd_unit,
				"nama_unit"=>$unit,
				"kd_user"=>$_kduser,
				"kd_user_del"=>$_kduser,
				"shift"=>$this->GetShiftBagian(),
				"shiftdel"=>$this->GetShiftBagian(),
				"user_name"=>$username,
				"tgl_batal"=>date('Y-m-d'),
				"ket"=>$keterangan
			);
	
			$inserthistory = $this->db->insert('history_batal_trans',$datahistory);
	
	
			$data = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurut,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL,
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
				"kd_kasir_asal"=>$kd_kasir,
				"no_transaksi_asal"=>$no_trans,
				"posting_transaksi"=>"True"
			);
						  
			$insert = $this->db->insert('transaksi',$data);		
			
			
			//transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			$Schurutbaru = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
			
			$databaru = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notransbaru,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurutbaru,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL
			);
			
			$insertbaru = $this->db->insert('transaksi',$databaru);	
			//akhir tambah transaksi nomor baru	
			
	
			$dataUpdate = array(
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
			);
	
			$update = $this->db->where('no_transaksi',$no_trans);
			$update = $this->db->where('kd_kasir',$kd_kasir);
			$update = $this->db->update('transaksi',$dataUpdate);				  
			// var_dump($data);
						 
						 
			if($insert)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				$date = date('Y-m-d');  
				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					-1*jumlah as jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					-1*sisa as sisa,
					-1*total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					-1*jumlah as jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component
					-1*jumlah as jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
			}
			
			if($insertbaru)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  $date = date('Y-m-d');
				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					sisa as sisa,
					total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component,
					jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
			} 
				
				
				
			 if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				echo '{success:false}';
			}
			else
			{
				$this->db->trans_commit(); 
				echo '{success:true}';
			} 
		} 
		catch (Exception $e) 
		{
			echo $e->getMessage();
		}
	}
	
	function regonline_pasienpulang($kd_pasien){
		$strError = "";
		$pasien=$this->common->db2->query("SELECT kd_pasien FROM rs_pasien_rwi WHERE kd_pasien='".$kd_pasien."'");
		
		if(count($pasien->result()) != 0){
			$pasienpulang=$this->common->db2->query("Delete from rs_pasien_rwi where kd_pasien='".$kd_pasien."'");
			if($pasienpulang){
				$strError = "sukses";
			} else{
				$strError = "eror";
			}
		} else{
			$strError = "eror";
		}
		
		return $strError;
		
	}
}

?>