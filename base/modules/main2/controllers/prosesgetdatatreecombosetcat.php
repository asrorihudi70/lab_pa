<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class prosesgetdatatreecombosetcat extends MX_Controller {

    public function __construct()
    {
       
        parent::__construct();
        
    }	 

    public function index()
    {
        $this->load->view('main/index');            

    }


       private function getDetailChild($id)
    {

        $arr = array();
        $this->load->model('setup/tblam_category');
        //x.Open("where type ='G' and left(parent," & id.Length & ") ='" & id & "'order by category_id")
        $this->tblam_category->db->where(" substr(parent,1,".strlen($id).") = '".$id."'" ,null,false);
        $query = $this->tblam_category->GetRowList( 0, 1000, 'ASC', 'category_id', '');

        if ($query[1] > 0)
        {
            foreach ($query[0] as $row)
            {
                $arr[] = $row;
            }
        }

        return $arr;
    }

    public function read($Params) //index()// read()
    {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);

        try
        {
            $this->load->model('setup/tblam_category');
            $this->tblam_category->db->where("type = 'G' and parent is null",null,false);
            $query = $this->tblam_category->GetRowList( 0, 1000, 'ASC', 'category_id', '');

            $arrChild = array();
            $arr=array();

            if ($query[1] > 0)
            {
               
                foreach ($query[0] as $row)
                {
                    $Tree = new clsTreeRow;
                    $Tree->id = $row->CATEGORY_ID;
                    $Tree->text = $row->CATEGORY_NAME;
                    $arrChild = $this->getDetailChild($row->CATEGORY_ID);

                    if (count($arrChild)>0)
                    {
                        $Tree->leaf = false;
                        $Tree->parents = $row->PARENT;
                        $Tree->parents_name="";

                        foreach ($arrChild as $a)
                        {
                            if ($a->PARENT == $Tree->id)
                            {
                                $Tree2 = new clsTreeRow();
                                $Tree2->id = $a->CATEGORY_ID;
                                $Tree2->text = $a->CATEGORY_NAME;
                                $Tree2->leaf = false;
                                $Tree2->parents = $a->PARENT;
                                $Tree2->parents_name = $Tree->text;
                                $Tree->children[]=$Tree2;
                            } else {
                                if (count($Tree->children) > 0)
                                {
                                    foreach ($Tree->children as $b )
                                    {
                                        $Tree3 = new clsTreeRow;
                                        if ($a->PARENT== $b->id)
                                        {
                                            $Tree3->id = $a->CATEGORY_ID;
                                            $Tree3->text = $a->CATEGORY_NAME;
                                            $Tree3->leaf = false;
                                            $Tree3->parents = $a->PARENT;
                                            $Tree3->parents_name = $b->text;
                                            $b->leaf = false;
                                            $b->children[]= $Tree3;
                                        } else {
                                            if (count($b->children)>0)
                                            {
                                                Foreach($b->children as $c)
                                                {
                                                    $Tree4 = new clsTreeRow;
                                                    if ($a->PARENT == $c->id)
                                                    {
                                                        $Tree4->id= $a->CATEGORY_ID;
                                                        $Tree4->text = $a->CATEGORY_NAME;
                                                        $Tree4->leaf = false;
                                                        $Tree4->parents = $a->PARENT;
                                                        $Tree4->parents_name = $c->text;
                                                        $c->leaf = false;
                                                        $c->children[] = $Tree4;
                                                    } else {
                                                        if (count($c->children)>0)
                                                        {
                                                            foreach ($c->children as $d)
                                                            {
                                                                $Tree5 = new clsTreeRow;
                                                                if ($a->PARENT == $d->id)
                                                                {
                                                                    $Tree5->id = $a->CATEGORY_ID;
                                                                    $Tree5->text = $a->CATEGORY_NAME;
                                                                    $Tree5->leaf = false;
                                                                    $Tree5->parents = $a->PARENT;
                                                                    $Tree5->parents_name = $d->text;
                                                                    $d->leaf = false;
                                                                    $d->children[] = $Tree5;
                                                                } else {
                                                                    if (count($d->children) > 0)
                                                                    {
                                                                        foreach ($d->children as $e)
                                                                        {
                                                                            $Tree6 = new clsTreeRow();
                                                                            if ($a->PARENT === $e->id)
                                                                            {
                                                                                $Tree6->id = $a->CATEGORY_ID;
                                                                                $Tree6->text = $a->CATEGORY_NAME;
                                                                                $Tree6->leaf = false;
                                                                                $Tree6->parents = $a->PARENT;
                                                                                $Tree6->parents_name = $e->text;
                                                                                $e->leaf = false;
                                                                                $e->children[]=$Tree6;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        $Tree->leaf = false;
                        $Tree->parents = $row->CATEGORY_ID;
                        $Tree->parents_name = "";
                    }

                    //$arr[] =$Tree;
                    
                    //$arr[] =$Tree;
                    $arr[] =$Tree;
                }

                //Dim res = New With {.success = True, .arr = arr}
                //output json nya aneh
                $res = "{success: true, arr: ".json_encode($arr)."}";

            } else {
                $res = '{success: false}';
            }

        }
        catch(Exception $o)
        {
            $res = '{success: false}';
        }

        echo $res;
    }

//    public function read($Params) //index()// read()
//    {
//		try
//		{
//
//                        $this->load->model('setup/tblam_category');
//                        $this->tblam_category->db->where("category_id <> '9999'");
//			$res = $this->tblam_category->GetRowList( 0, 0, '', 'category_id', '');
//			$prn='';
//			$lst='';
//			foreach ($res[0] as $node)
//			{
//				$x=new clsTreeRow;
//				$x->id=$node->CATEGORY_ID;
//				$x->text=$node->CATEGORY_NAME;
//				$x->parents=$node->PARENT;
//				$x->leaf=false;
//				$x->expanded=false;
//				$x->loader=null;
//                               // $x->parents_name = $lst[$x->parents]->text;
//                                //$lst[$x->parents]->children[]=$x;
////				if ( strlen($node->PARENT)===0 )
////				{
////
////        				$prn=$node->CATEGORY_ID;
////					$x->id=$node->CATEGORY_ID;
////					$x->text=$node->CATEGORY_NAME;
////                                        $x->leaf=false;
////                                        $x->expanded=false;
////                                        $x->loader=null;
////					$lst[$x->id] = $x ;
////
////				}
////				else
////				{
////					$x->id=$node->CATEGORY_ID;
////					$x->text=$node->CATEGORY_NAME;
////                                        $x->parents=$node->PARENT;
////                                        $x->leaf=false;
////                                        $x->expanded=false;
////                                        $x->loader=null;
////                                        $x->parents_name = $lst[$x->parents]->text;
////					$lst[$x->parents]->children[]=$x;
////				}
//			}
//
//		}
//		catch(Exception $o)
//		{
//			echo 'Debug  fail ';
//
//		}
//
//	 	echo '{success: true,  ListDataObj:'.json_encode($res[0]).'}';
//
//
//    }
}

//class TreeRow
//{
//	public $id;
//	public $text;
//	public $parents;
//	public $type;
//	public $expanded;
//	public $children;
//}


?>