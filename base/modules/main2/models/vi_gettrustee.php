<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class vi_gettrustee extends Model
{

        private $strQuery="select distinct zmodgroup.mod_group, zmodule.mod_id,
            zmodule.parent_id as parent_id, zmodule.mod_name, zmodule.mod_desc,
            zmodule.mod_url, zusers.user_names, zmember.kd_user,
            zmodgroup.mod_img, zmodule.app_id, zmodule.mod_key,
            zmodule.mod_imgurl, zmodule.parent_group, zmodule.mod_type,
            zmodgroup.mod_row, zmodule.aktif
            from zmodule inner join
            ztrustee on zmodule.mod_id = ztrustee.mod_id
            inner join zmember inner join
            zusers on zmember.kd_user = zusers.kd_user
            inner join zgroups on zmember.group_id = zgroups.group_id
            on ztrustee.group_id = zgroups.group_id inner join
            zmodgroup on zmodule.mod_group = zmodgroup.mod_group";
        
        //where aktif=1
	//order by  zmodgroup.mod_row,  zmodgroup.mod_group,  zmodule.mod_id ) as trstee";

	function vi_gettrustee()
	{
		parent::Model();
		$this->load->database();
	}


	function readforGetTrustee1($strKd_User)
	{		
		//$this->db->order_by('Mod_Row', 'Mod_Group', 'Mod_ID');
                //$this->db->order_by('mod_row', 'mod_group', 'mod_id');
		//$this->db->where('Kd_User', $strKd_User);
                //$this->db->where('kd_user', $strKd_User);
		//$this->db->where('Mod_Type', 1);
                //$this->db->where('mod_type', 1);
		//$this->db->where('APP_ID', 'ASET');
                //$this->db->where('app_id', 'ASET');
		//$this->db->where('Aktif', 1);
                //$this->db->where('aktif', 1);

		//$query = $this->db->get('dbo.vi_gettrustee');
                //$query = $this->get($this->strQuery);

                $strWhere= $this->strQuery." where zmember.kd_user = '".$strKd_User."' 
                    and zmodule.mod_type = 1 and zmodule.app_id = 'ASET'
                    and zmodule.aktif = 1 order by zmodgroup.mod_row, zmodgroup.mod_group, zmodule.mod_id";

                $query = $this->db->query($strWhere);

		return $query;
	}

	function readforGetTrustee2($strKd_User,$strTmpGroup)
	{		
		//$this->db->order_by('Mod_Row', 'Mod_Group', 'Mod_ID');
                //$this->db->order_by('mod_row', 'mod_group', 'mod_id');
		//$this->db->where('Kd_User', $strKd_User);
                //$this->db->where('kd_user', $strKd_User);
		//$this->db->where('Mod_Type', 2);
                //$this->db->where('mod_type', 2);
		//$this->db->where('APP_ID', 'ASET');
                //$this->db->where('app_id', 'ASET');
		//$this->db->where('Aktif', 1);
                //$this->db->where('aktif', 1);
		//$this->db->where('Parent_Group',$strTmpGroup);
                //$this->db->where('parent_group',$strTmpGroup);
 		//$query = $this->db->get('dbo.vi_gettrustee');
                //$query = $this->get($this->strQuery);

                $strWhere= $this->strQuery." where zmember.kd_user = '".$strKd_User."' 
                    and zmodule.mod_type = 2 and zmodule.app_id = 'ASET'
                    and zmodule.aktif = 1 and zmodule.parent_group = '".$strTmpGroup."'
                    order by zmodgroup.mod_row, zmodgroup.mod_group, zmodule.mod_id";

                $query = $this->db->query($strWhere);

                return $query;
	}

	function readforGetModule($strKd_User, $Mod_ID)
	{		
		//$this->db->order_by('Mod_Group', 'Mod_ID');
                //$this->db->order_by('mod_group', 'mod_id');
		//$this->db->where('Kd_User', $strKd_User);
                //$this->db->where('kd_user', $strKd_User);
		//$this->db->where('Mod_ID', $Mod_ID);
                //$this->db->where('mod_id', $Mod_ID);
	
		//$query = $this->db->get('dbo.vi_gettrustee');
                //$query = $this->get($this->strQuery);

                $strWhere= $this->strQuery." where zmember.kd_user = '".$strKd_User."' 
                    and zmodule.mod_id = '".$Mod_ID."'
                    order by zmodgroup.mod_group, zmodule.mod_id";

                $query = $this->db->query($strWhere);

		return $query;
	}


}



?>