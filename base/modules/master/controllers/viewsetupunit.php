<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class viewsetupunit extends MX_Controller
{

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


	public function index()
	{
        $this->load->view('main/index');
        }


	function read($Params=null){
		

		try{
			//$kd_user=$this->session->userdata['user_id']['id'];
			//$sql="SELECT kd_unit FROM zusers WHERE kd_user='".$kd_user."'";
			//$arrSql=$this->db->query($sql)->row();
			
			# Edit	 	: M
			# Tanggal 	: 04-02-2017
			# Ket		: Tambah order by nama_unit
			# ======================================
			# Edit	 	: M
			# Tanggal 	: 21-07-2017
			# Ket		: Tambah seleksi untuk unit farmasi
			#				parent='6' => adalah kd_unit farmasi
			
			$this->load->model('master/tblam_unit');
			if (strlen($Params[4])!==0){
				
			   // $this->db->where(str_replace("~", "'", $Params[4])." order by nama_unit",null, false);
				if($Params[4] == "parent='6' and type_unit=false "){
					
					$default_kd_unit_apotek = $this->db->query("select setting from sys_setting where key_data='apt_default_kd_unit'")->row()->setting;
					$this->db->where(str_replace("~", "'", $Params[4])."
					union 
					SELECT kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type_unit,aktif,spesial FROM UNIT WHERE kd_unit='".$default_kd_unit_apotek."'
					",null, false); //order by nama_unit
				}else{
					
					$this->db->where(str_replace("~", "'", $Params[4])." ",null, false); //order by nama_unit
				}
			} 
			// echo $Params[4];
			$str =  $Params[4];
			$str2 = explode("'",$str);
			// print_r ($str2);

			if($str2[1] == "3"){ 
				// echo "Aaaa";
				$res = array (
					0 => 
					array (
					  0 => 
					  array(
						 'KD_UNIT' => '3',
						 'KD_BAGIAN' => 3,
						 'KD_KELAS' => 0,
						 'NAMA_UNIT' => 'Inst. Rawat Darurat',
						 'PARENT' => '0',
						 'TYPE_UNIT' => 0,
						 'AKTIF' => 0,
						 'SPESIAL' => 0,
					  ),
					),
					1 => 1,
				);
				// echo '{success:true, totalrecords:1, ListDataObj:[{"KD_UNIT":"3","KD_BAGIAN":3,"KD_KELAS":0,"NAMA_UNIT":"Inst. Rawat Darurat","PARENT":"0","TYPE_UNIT":0,"AKTIF":0,"SPESIAL":0}]}';
			}else{
				$res = $this->tblam_unit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
			// die;
			//$this->db->where("kd_unit in(".$arrSql->kd_unit.")");

		}catch(Exception $o){
			echo 'Debug  fail ';
		}
		// echo "<pre>".var_export($res, true)."</pre>"; die;
	 	echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}


	 function save($Params=null)
	 {
		$this->load->model('master/tblam_unit');
		$Arr['kd_unit']=$Params['KdUnit'];
                $Arr['kd_bagian']=$Params['KdBagian'];
                $Arr['kd_kelas']=$Params['KdKelas'];
		$Arr['nama_unit']=$Params['NamaUnit'];
                $Arr['parent']=$Params['Parent'];
                $Arr['type_unit']=$Params['Type'];
                $Arr['aktif']=$Params['Aktif'];
                $Arr['spesial']=$Params['Spesial'];
                if (strlen($Arr['kd_unit'])===0)
                {
                    $Arr['kd_unit']=$this->GetNewID();
                    $res=$this->tblam_unit->Save($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
                    if ($res>0)
                    {
                        echo '{success: true, KdUnit: "'.$Arr['kd_unit'].'"}';
                    } else echo '{success: false}';
                }
                else
                {
                    $this->db->where("kd_unit = '".$Arr['kd_unit']."'", null, false);
		//$Arr=array_filter( $Arr , "ArrayUtils::NullElement"  );
                    $res=$this->tblam_unit->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
                    if ($res>0)
                    {
                        echo '{success: true}';
                    } else echo '{success: false}';

                }
		//echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	 }

	 function GetNewID()
         {
             $res = $this->tblam_unit->GetRowList( 0 , 1, 'DESC', 'kd_unit', '');
             if ($res[1]===0)
             {
                 return 1;
             }
             else
             {
                 return $res[0][0]->KD_UNIT+1;
             }
         }

	 function delete( $param)
	 {
		$this->load->model('master/tblam_unit');
                $this->tblam_unit->db->where("kd_unit = '".$param['KdUnit']."'", null, false);
	 	$res=$this->tblam_unit->Delete(); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
	 	//return $res;
            if ($res>0)
            {
                echo '{success: true}';
            } else echo '{success: false}';
	 }
}
//VIEWSETUPASSET

?>
