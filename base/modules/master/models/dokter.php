<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class dokter extends TblBase
{
	
	function __construct()
	{
		# Edit	 	: M
		# Tanggal 	: 04-02-2017
		# Ket		: Ganti type get query
		
		$this->StrSql=" kd_dokter, nama, jenis_dokter";
		TblBase::TblBase(true);
		$this->SqlQuery="SELECT * FROM dokter";
	}

	
	function FillRow($rec)
	{
		$row=new RowUnit;
		$row->KD_DOKTER    =$rec->KD_DOKTER;
		$row->NAMA         =$rec->NAMA;
		$row->JENIS_DOKTER =$rec->JENIS_DOKTER;

		return $row;
	}
}
class RowUnit
{
    public $KD_DOKTER;
    public $NAMA;
    public $JENIS_DOKTER;
}

?>
