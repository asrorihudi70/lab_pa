<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class am_kelas extends Model
{

	function am_unit()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('KD_KELAS', $data['KD_KELAS']);
		$this->db->set('KELAS', $data['KELAS']);
                $this->db->set('PARENT', $data['PARENT']);
                $this->db->set('ASKES', $data['ASKES']);
		$this->db->insert('kelas');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('KD_KELAS', $id);
		$query = $this->db->get('kelas');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('kelas');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('KD_KELAS', $data['KD_KELAS']);
		$this->db->set('KELAS', $data['KELAS']);
                $this->db->set('PARENT', $data['PARENT']);
                $this->db->set('ASKES', $data['ASKES']);
		$this->db->update('kelas');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('KD_KELAS', $id);
		$this->db->delete('kelas');

		return $this->db->affected_rows();
	}

}


?>
