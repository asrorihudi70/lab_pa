<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tblam_unit extends TblBase
{

	function __construct()
	{
		/* $this->StrSql="kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type_unit,aktif,spesial";
		$this->TblName='unit';
		TblBase::TblBase(); */

		# Edit	 	: M
		# Tanggal 	: 04-02-2017
		# Ket		: Ganti type get query

		$this->StrSql = "kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type_unit,aktif,spesial";
		TblBase::TblBase(true);
		// $this->SqlQuery="SELECT kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type_unit,aktif,spesial FROM UNIT";

		// $this->SqlQuery="SELECT '9999' as kd_unit, '0' as  kd_bagian, '0' as  kd_kelas, 'All' as  nama_unit, '0' as  parent, 
		// '0' as  type, '1' as aktif, '0' as  spesial Union all SELECT kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type,aktif,spesial FROM UNIT";

		$this->SqlQuery = "SELECT kd_unit,kd_bagian,kd_kelas,nama_unit,parent,type,aktif,spesial FROM UNIT";





		//$this->SqlQuery="SELECT unit.kd_unit, kelas.kelas, bagian.bagian, unit.nama_unit, unit.parent, unit.type_unit, unit.aktif, UNIT.SPESIAL
		//                FROM
		//                UNIT INNER JOIN KELAS ON KELAS.KD_KELAS = UNIT.KD_KELAS
		//                CROSS JOIN BAGIAN WHERE BAGIAN.KD_BAGIAN = UNIT.KD_BAGIAN";
	}


	function FillRow($rec)
	{
		$row = new RowUnit;
		$row->KD_UNIT = $rec->kd_unit;
		$row->KD_BAGIAN = $rec->kd_bagian;
		$row->KD_KELAS = $rec->kd_kelas;
		$row->NAMA_UNIT = $rec->nama_unit;
		$row->PARENT = $rec->parent;
		$row->TYPE_UNIT = $rec->type;
		$row->AKTIF = $rec->aktif;
		$row->SPESIAL = $rec->spesial;

		return $row;
	}
}
class RowUnit
{
	public $KD_UNIT;
	public $KD_BAGIAN;
	public $KD_KELAS;
	public $NAMA_UNIT;
	public $PARENT;
	public $TYPE_UNIT;
	public $AKTIF;
	public $SPESIAL;
}
