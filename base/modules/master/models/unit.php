<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class unit extends Model
{

	function unit()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('KD_UNIT', $data['KD_UNIT']);
                $this->db->set('KD_BAGIAN', $data['KD_BAGIAN']);
                $this->db->set('KD_KELAS', $data['KD_KELAS']);
		$this->db->set('NAMA_UNIT', $data['NAMA_UNIT']);
                $this->db->set('PARENT', $data['NAMA_UNIT']);
                $this->db->set('TYPE_UNIT', $data['TYPE_UNIT']);
                $this->db->set('AKTIF', $data['AKTIF']);
                $this->db->set('SPESIAL', $data['SPESIAL']);
		$this->db->insert('unit');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('KD_UNIT', $id);
		$query = $this->db->get('unit');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('unit');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('KD_UNIT', $data['KD_UNIT']);
		$this->db->set('KD_BAGIAN', $data['KD_BAGIAN']);
                $this->db->set('KD_KELAS', $data['KD_KELAS']);
		$this->db->set('NAMA_UNIT', $data['NAMA_UNIT']);
                $this->db->set('PARENT', $data['NAMA_UNIT']);
                $this->db->set('TYPE_UNIT', $data['TYPE_UNIT']);
                $this->db->set('AKTIF', $data['AKTIF']);
                $this->db->set('SPESIAL', $data['SPESIAL']);
		$this->db->update('unit');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('KD_UNIT', $id);
		$this->db->delete('unit');

		return $this->db->affected_rows();
	}

}


?>
