<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class view_pekerjaan extends Model
{

	function view_pekerjaan()
	{
		parent::Model();
		$this->load->database();
	}

	

	function read($id)
	{
		$this->db->where('kd_pekerjaan', $id);
		$query = $this->db->get('pekerjaan');

		return $query;
	}

	function readAll()
	{
	
		$this->db->order_by('pekerjaan', 'asc');
		$query = $this->db->get('pekerjaan');

		return $query;
	}



}


class Rowam_department
{

	public $KD_PEKERJAAN;
	public $PEKERJAAN;
}

?>