<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class viewunitcmb extends Model
{

	function viewunitcmb()
	{
		parent::Model();
		$this->load->database();
	}

	

	function read($id)
	{
		$this->db->where('kd_unit', $id);
		$query = $this->db->get('unit');

		return $query;
	}

	function readAll()
	{
	
		$this->db->where('parent', '2');
		$this->db->order_by('nama_unit', 'asc');
		$query = $this->db->get('unit');

		return $query;
	}



}


class Rowam_department
{

	public $KD_UNIT;
	public $NAMA_UNIT;
}

?>