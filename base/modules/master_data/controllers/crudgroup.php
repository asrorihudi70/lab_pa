<?php

/**
 * @author
 * @copyright
 */
class crudgroup extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = null) {

        $id = $Params["IDGROUP"];
        $idsd = $Params["IDSD"];
        $nama = $Params["NAMA"];


        $data = array(
            "group_id" => $id,
            "dana_id" => $idsd,
            "group_name" => $nama
        );
        $this->load->model('master_data/tblcrudgroup');


        $criteria = "group_id = '" . $id . "'";
        $this->tblcrudgroup->db->where($criteria, null, false);

        $query = $this->tblcrudgroup->GetRowList(0, 1, "", "", "");

        if ($query[1] == 1) {
            $res = $this->tblcrudgroup->db->where($criteria, null, false);
            $res = $this->tblcrudgroup->update($data);
        } else {
            $res = $this->tblcrudgroup->save($data);
        }
        //  $this->tblviewrencanaasuhandetail->db->where($criteria, null, false);
        echo "{success:true}";
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('master_data/tblcrudgroup');

        $this->tblcrudgroup->db->where($criteria, null, false);

        $query = $this->tblcrudgroup->GetRowList();
        if ($query[1] != 0) {
            $this->tblcrudgroup->db->where($criteria, null, false);
            $result = $this->tblcrudgroup->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>