<?php
class customer extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function save(){
		
		$data=array(
		//'kd_customer'=>$_POST['kd_cus'],
		'account'=>$_POST['account'],
		'customer'=>$_POST['customer'],
		'contact'=>$_POST['kontak'],
		'alamat'=>$_POST['alamat'],
		'kota'=>$_POST['kota'],
		'telepon1'=>$_POST['tlp1'],
		'telepon2'=>$_POST['tlp2'],
		'fax'=>$_POST['fax'],
		'kd_pos'=>$_POST['kode_pos'],
		'negara'=>$_POST['negara']
		);
		if ($_POST['kd_cus']===''){
		$data['kd_customer']=$this->getvendor();
		$_POST['kd_cus']=$data['kd_customer'];
		$customer=$this->db->insert('customer',$data);
		}else{
		$this->db->where('kd_customer',$_POST['kd_cus']);
		$customer=$this->db->update('customer',$data);
		}
		if($customer){
		echo "{success:true,kd_cus:'".$_POST['kd_cus']."'}";
		}
		else{
		echo "{success:false}";	
		}		
	}
	public function delete(){
		$this->db->where('kd_customer',$_POST['kd_cus']);
		$customer=$this->db->delete('customer');
		if($customer){
		echo "{success:true}";
		}
		else{
		echo "{success:false}";	
		}
	}
	public function select(){
		if($_POST['criteria']==="")
		{
		$where="";
		}else{
					$where="where ".$_POST['criteria'];
		}
		$customer=$this->db->query("select * from customer $where order by customer asc")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($customer)." }";
	}
	public function getvendor(){
		 $sqlcounter = $this->db->query("select * from customer order by kd_customer desc limit 1")->row();
		 $kd_vendor = $sqlcounter->kd_customer;
		 $kd_vendor=(int)$kd_vendor+1;
		 $kd_vendor=str_pad($kd_vendor,10,"0",STR_PAD_LEFT);
		 return $kd_vendor;
	}
	public function accounts(){
		$select =$this->db->query("select * from accounts  where(  left(account,1) ='1' and right(account,2) ='01' and 
		upper(account) like upper('".$_POST['text']."%') )
        or ( left(account,1) ='1' and upper(name) like upper('".$_POST['text']."%')	)	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
		
		
	}
	
}