<?php
class vendor extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function save(){
		
		$data=array(
		//'kd_vendor'=>$_POST['kd_ven'],
		'account'=>$_POST['account'],
		'vendor'=>$_POST['vendor'],
		'contact'=>$_POST['kontak'],
		'alamat'=>$_POST['alamat'],
		'kota'=>$_POST['kota'],
		'telepon1'=>$_POST['tlp1'],
		'telepon2'=>$_POST['tlp2'],
		'fax'=>$_POST['fax'],
		'kd_pos'=>$_POST['kode_pos'],
		'negara'=>$_POST['negara']
		);
		if ($_POST['kd_ven']===''){
			$data['kd_vendor']=$this->getvendor();	
			$_POST['kd_ven']=$data['kd_vendor'];
			$vendor=$this->db->insert('vendor',$data);
		}else{
			$this->db->where('kd_vendor',$_POST['kd_ven']);
			$vendor=$this->db->update('vendor',$data);
		}
		if($vendor){
		echo "{success:true,kd_ven:'".$_POST['kd_ven']."'}";
		}
		else{
		echo "{success:false}";	
		}		
	}
	public function delete(){
		$this->db->where('kd_vendor',$_POST['kd_ven']);
		$vendor=$this->db->delete('vendor');
		if($vendor){
		echo "{success:true}";
		}
		else{
		echo "{success:false}";	
		}
	}
	public function select(){
		if($_POST['criteria']==="")
		{
		$where="";
		}else{
					$where="where ".$_POST['criteria'];
		}
		$vendor=$this->db->query("select * from vendor $where order by vendor asc")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($vendor)." }";
	}
	public function getvendor(){
		 $sqlcounter = $this->db->query("select * from vendor order by kd_vendor desc limit 1")->row();
		 $kd_vendor = $sqlcounter->kd_vendor;
		 $kd_vendor=(int)$kd_vendor+1;
		 $kd_vendor=str_pad($kd_vendor,10,"0",STR_PAD_LEFT);
		 return $kd_vendor;
	}
	public function accounts(){
		$select =$this->db->query("select * from accounts  where(  left(account,1) ='2' and 
		upper(account) like upper('".$_POST['text']."%') )
        or ( left(account,1) ='2' and upper(name) like upper('".$_POST['text']."%')	)	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
		
		
	}
	
}