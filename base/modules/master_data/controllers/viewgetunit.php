<?php

/**
 * @author
 * @copyright
 */
class viewgetunit extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        // echo 'a';
        $this->load->model('master_data/tblgetunit');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblgetunit->db->where($criteria, null, false);
        }
        $query = $this->tblgetunit->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>