﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblcrudab extends TblBase {

    function __construct() {
        $this->TblName = 'acc_bd_balance';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new Rowab;

        $row->DANAID = $rec->dana_id;
        $row->MAID = $rec->ma_id;
        $row->YEAR = $rec->years;
        $row->PLAN0 = $rec->plan0;
        return $row;
    }

}

class Rowab {

    public $DANAID;
    public $MAID;
    public $PLAN0;
    public $YEAR;

}
