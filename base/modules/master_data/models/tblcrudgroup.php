﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblcrudgroup extends TblBase {

    function __construct() {
        $this->TblName = 'acc_bd_group';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->GROUPID = $rec->group_id;
        $row->DANA_ID = $rec->dana_id;
        $row->GROUP_NAME = $rec->group_name;
        return $row;
    }

}

class Rowncp {

    public $GROUPID;
    public $DANA_ID;
    public $GROUP_NAME;

}
