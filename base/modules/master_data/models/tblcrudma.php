﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblcrudma extends TblBase {

    function __construct() {
        $this->TblName = 'acc_bd_mata';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->DANAID = $rec->dana_id;
        $row->MAID = $rec->ma_id;
        $row->MANAME = $rec->ma_name;
        $row->MATYPE = $rec->ma_type;
        $row->MALEVEL = $rec->ma_levels;
        $row->MADANAPARENT = $rec->ma_dana_parent;
        $row->MAPARENT = $rec->ma_parent;
        $row->GROUPID = $rec->group_id;
        $row->DEBIT = $rec->isdebit;
        $row->YEAR = $rec->years;
        return $row;
    }

}

class Rowncp {

    public $DANAID;
    public $MAID;
    public $MANAME;
    public $MATYPE;
    public $MALEVEL;
    public $MADANAPARENT;
    public $MAPARENT;
    public $GROUPID;
    public $DEBIT;
    public $YEAR;

}
