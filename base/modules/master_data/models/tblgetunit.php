﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblgetunit extends TblBase {

    function __construct() {
        $this->TblName = '';
        TblBase::TblBase(true);

        $this->SqlQuery = "SELECT kd_bagian, kd_unit, nama_unit FROM UNIT";
    }

    function FillRow($rec) {
        $row = new Rowgetunit;

        $row->kd_bagian = $rec->kd_bagian;
        $row->kd_unit = $rec->kd_unit;
        $row->nama_unit = $rec->nama_unit;
        return $row;
    }

}

class Rowgetunit {
    public $kd_bagian;
    public $kd_unit;
    public $nama_unit;

}
