﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaccount extends TblBase {

    function __construct() {
        $this->StrSql = "account,name,type,levels,parent,groups,showfield";
        $this->SqlQuery = "SELECT * FROM ACCOUNTS ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->ACCOUNT = $rec->account;
        $row->NAME = $rec->name;
        $row->TYPE = $rec->type;
        $row->LEVELS = $rec->levels;
        $row->PARENT = $rec->parent;
        $row->GROUPS = $rec->groups;
        $row->SHOWFIELD = $rec->showfield;
        return $row;
    }

}

class Rowncp {

    public $ACCOUNT;
    public $NAME;
    public $TYPE;
    public $LEVELS;
    public $PARENT;
    public $GROUPS;
    public $SHOWFIELD;

}
