﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewcustomer extends TblBase {

    function __construct() {
        $this->StrSql = "cust_code,customer,contact,address,"
                . "city,state,zip,country,phone1,phone2,fax,due_day,account";
        $this->SqlQuery = "SELECT * FROM ACC_CUSTOMERS ORDER BY Cust_Code";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->CUST_CODE = $rec->cust_code;
        $row->CUSTOMER = $rec->customer;
        $row->CONTACT = $rec->contact;
        $row->ADDRESS = $rec->address;
        $row->CITY = $rec->city;
        $row->STATE = $rec->state;
        $row->ZIP = $rec->zip;
        $row->COUNTRY = $rec->country;
        $row->PHONE1 = $rec->phone1;
        $row->PHONE2 = $rec->phone2;
        $row->FAX = $rec->fax;
        $row->DUE_DAY = $rec->due_day;
        $row->ACCOUNT = $rec->account;
        return $row;
    }

}

class Rowncp {

    public $CUST_CODE;
    public $CUSTOMER;
    public $CONTACT;
    public $ADDRESS;
    public $CITY;
    public $STATE;
    public $ZIP;
    public $COUNTRY;
    public $PHONE1;
    public $PHONE2;
    public $FAX;
    public $DUE_DAY;
    public $ACCOUNT;

}
