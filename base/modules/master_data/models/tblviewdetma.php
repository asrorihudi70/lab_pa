﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewdetma extends TblBase {

    function __construct() {
        $this->StrSql = "dana_id,ma_id,ma_name,ma_type,ma_levels,ma_parent,group_id";
        $this->SqlQuery = "SELECT DANA_ID,MA_ID, MA_Name, MA_TYPE, MA_Levels, MA_Parent, Group_ID FROM ACC_BD_MATA";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->DANAID = $rec->dana_id;
        $row->MAID = $rec->ma_id;
        $row->MANAME = $rec->ma_name;
        $row->MALEVEL = $rec->ma_levels;
        $row->MATYPE = $rec->ma_type;
        $row->MAPARENT = $rec->ma_parent;
        $row->GROUPID = $rec->group_id;
        return $row;
    }

}

class Rowncp {

    public $DANAID;
    public $MAID;
    public $MANAME;
    public $MALEVEL;
    public $MAPARENT;
    public $GROUPID;
    public $MATYPE;

}
