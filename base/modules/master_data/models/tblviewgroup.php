﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewgroup extends TblBase {

    function __construct() {
        $this->StrSql = "group_id,dana_id,group_name,sumber_dana";
        $this->SqlQuery = "SELECT ag.*,ad.Sumber_Dana  
                            FROM ACC_BD_GROUP ag  
                            INNER JOIN ACC_BD_DANA ad ON ag.Dana_ID=ad.Dana_ID ORDER BY ag.Group_ID ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->GROUPID = $rec->group_id;
        $row->DANA_ID = $rec->dana_id;
        $row->GROUP_NAME = $rec->group_name;
        $row->SUMBER_DANA = $rec->sumber_dana;
        return $row;
    }

}

class Rowncp {

    public $GROUPID;
    public $DANA_ID;
    public $GROUP_NAME;
    public $SUMBER_DANA;

}
