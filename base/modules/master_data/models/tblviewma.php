﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewma extends TblBase {

    function __construct() {
        $this->StrSql = "dana_id,ma_id,ma_name,ma_type,ma_levels,ma_dana_parent,ma_parent,group_id,isdebit,years";
        $this->SqlQuery = "SELECT am.dana_id, am.ma_id, am.ma_name, am.ma_type, am.ma_levels, am.ma_dana_parent, am.ma_parent, am.group_id, am.isdebit, am.years, ab.plan0
                            FROM ACC_BD_MATA am
                            LEFT JOIN ACC_BD_Balance ab  ON am.MA_ID=ab.MA_ID and am.dana_id = ab.dana_id and am.years::varchar = ab.years::varchar 
                            ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->DANAID = $rec->dana_id;
        $row->MAID = $rec->ma_id;
        $row->MANAME = $rec->ma_name;
        $row->MATYPE = $rec->ma_type;
        $row->MALEVEL = $rec->ma_levels;
        $row->MADANAPARENT = $rec->ma_dana_parent;
        $row->MAPARENT = $rec->ma_parent;
        $row->GROUPID = $rec->group_id;
        $row->DEBIT = $rec->isdebit;
        $row->YEAR = $rec->years;
        $row->PLAN = $rec->plan0;
        return $row;
    }

}

class Rowncp {

    public $DANAID;
    public $MAID;
    public $MANAME;
    public $MATYPE;
    public $MALEVEL;
    public $MADANAPARENT;
    public $MAPARENT;
    public $GROUPID;
    public $DEBIT;
    public $YEAR;
    public $PLAN;
}
