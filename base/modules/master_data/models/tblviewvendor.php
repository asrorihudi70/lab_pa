﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewvendor extends TblBase {

    function __construct() {
        $this->StrSql = "cust_code,vendor,contact,address,"
                . "city,state,zip,country,phone1,phone2,fax,due_day,account";
        $this->SqlQuery = "SELECT * FROM ACC_VENDORS ORDER BY Vend_Code";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->VEND_CODE = $rec->vend_code;
        $row->VENDOR = $rec->vendor;
        $row->CONTACT = $rec->contact;
        $row->ADDRESS = $rec->address;
        $row->CITY = $rec->city;
        $row->STATE = $rec->state;
        $row->ZIP = $rec->zip;
        $row->COUNTRY = $rec->country;
        $row->PHONE1 = $rec->phone1;
        $row->PHONE2 = $rec->phone2;
        $row->FAX = $rec->fax;
        $row->DUE_DAY = $rec->due_day;
        $row->ACCOUNT = $rec->account;
        return $row;
    }

}

class Rowncp {

    public $VEND_CODE;
    public $VENDOR;
    public $CONTACT;
    public $ADDRESS;
    public $CITY;
    public $STATE;
    public $ZIP;
    public $COUNTRY;
    public $PHONE1;
    public $PHONE2;
    public $FAX;
    public $DUE_DAY;
    public $ACCOUNT;

}
