<?php

class viewgridresep_02 extends MX_Controller{

	public function __construct(){
    	parent::__construct();
    }

	public function index()
	{
    	$this->load->view('main/index');
    }

    public function read($Params=null){
		try{
		  $result = $this->db->query("SELECT A.id_mrresep,B.catatan_racik,'' AS result,B.jumlah_racik,B.satuan_racik,B.takaran,'' AS racikan_text,C.kd_satuan,A.kd_pasien,C.kd_prd,C.nama_obat,B.jumlah,D.satuan,B.cara_pakai,E.nama as kd_dokter,  b.urut,A.id_mrresep,
		  case when(B.verified = 0) then 'Disetujui' else 'Tdk Disetujui' end as verified,
		  case when a.dilayani =0 then 'Belum Dilayani' when a.dilayani =1 then 'Dilayani' end as order_mng, 
		  case when B.order_mng = 'f' then 'Belum Dilayani' when B.order_mng = 't' then 'Dilayani' end as order_mng_det,
		  case when B.racikan = 0 then 0 when B.racikan= 1 then 1 end as racikan,
		  B.aturan_racik, B.aturan_pakai, B.kd_unit_far,B.kd_milik,B.no_racik,SUM(f.jml_stok_apt) as stok_current,
				SUM(f.jml_stok_apt)	as jml_stok_apt,B.signa
				
		  FROM MR_RESEP A
				INNER JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
				INNER JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD  
				INNER JOIN apt_stok_unit f ON b.kd_prd=f.kd_prd and b.kd_milik=f.kd_milik 
				INNER JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
				INNER JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan 
				WHERE ".$Params[4]."  
				GROUP BY A.id_mrresep,A.kd_pasien,C.kd_prd,C.nama_obat,C.kd_satuan,B.jumlah,D.satuan,B.cara_pakai,E.nama, b.urut,A.id_mrresep,a.dilayani,b.verified,b.order_mng,b.racikan,b.aturan_racik,b.aturan_pakai,
b.kd_unit_far,b.kd_milik,b.no_racik,B.signa,B.takaran,B.jumlah_racik,B.satuan_racik,B.catatan_racik order by A.id_mrresep desc, b.no_racik,b.urut
			")->result();
		$arr=array();
		$arrRacik=array();
		if(isset($_GET['resep']) && $_GET['resep']=='Y'){
			for($i=0,$iLen=count($result);$i<$iLen; $i++){
				if($result[$i]->racikan=='1' || $result[$i]->racikan==1){
					$result[$i]->racikan=true;
					$result[$i]->racikan_text='Ya';
					if(!isset($arrRacik[$result[$i]->no_racik])){
						$arrRacik[$result[$i]->no_racik]=array();
					}
					$arrRacik[$result[$i]->no_racik][]=$result[$i];
				}else{
					$result[$i]->racikan=false;
					$result[$i]->racikan_text='Tidak';
					$arr[]=$result[$i];
				}
			}	
			foreach($arrRacik as $key=>$obj){
				$dat=$obj[0];
				$dat->result=json_encode($obj);
				$dat->kd_prd=$key;
				$dat->jumlah=$obj[0]->jumlah_racik;
				$dat->satuan=$obj[0]->satuan_racik;
				$dat->jml_stok_apt=0;
				$dat->nama_obat='[RACIKAN]';
				
				$arr[]=$dat;
			}
		}else{
			for($i=0,$iLen=count($result);$i<$iLen; $i++){
				if($result[$i]->racikan=='1' || $result[$i]->racikan==1){
					$result[$i]->racikan=true;
				}else{
					$result[$i]->racikan=false;
				}
			}	
		}
		}catch(Exception $o){
			echo 'Debug  fail ';
		}
		if(isset($_GET['resep']) && $_GET['resep']=='Y'){
			echo '{success:true, totalrecords:'.count($arr).', ListDataObj:'.json_encode($arr).'}';
		}else{
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
		}
	}
	// public function read($Params=null){
		// try{
		  // $result = $this->db->query("SELECT A.kd_pasien,C.kd_prd,C.nama_obat,B.jumlah,D.satuan,B.cara_pakai,E.nama as kd_dokter,  b.urut,A.id_mrresep,
		  // case when(B.verified = 0) then 'Disetujui' else 'Tdk Disetujui' end as verified,
		  // case when a.order_mng = 'f' then 'Belum Dilayani' when a.order_mng = 't' then 'Dilayani' end as order_mng, 
		  // case when B.order_mng = 'f' then 'Belum Dilayani' when B.order_mng = 't' then 'Dilayani' end as order_mng_det,
		  // case when B.racikan = 0 then 'Tidak' when B.racikan= 1 then 'Ya' end as racikan,
		  // B.aturan_racik, B.aturan_pakai, B.kd_unit_far,B.kd_milik,B.no_racik,f.jml_stok_apt as stok_current,
				// (f.jml_stok_apt -
					// (select sum(jumlah) as jml_order from mr_resepdtl mrd 
						// inner join mr_resep mr on mrd.id_mrresep = mr.id_mrresep
						// where kd_unit_far=B.kd_unit_far and kd_milik=B.kd_milik and kd_prd=B.kd_prd and tgl_masuk = now()::date and mr.dilayani=0) 
				// )	as jml_stok_apt
				
		  // FROM MR_RESEP A
				// INNER JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
				// INNER JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD  
				// INNER JOIN apt_stok_unit f ON b.kd_prd=f.kd_prd and b.kd_milik=f.kd_milik and b.kd_unit_far=f.kd_unit_far
				// INNER JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
				// INNER JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan 
				// WHERE ".$Params[4]."  order by b.urut
			// ")->result();
				
		// }catch(Exception $o){
			// echo 'Debug  fail ';
		// }
		// echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	// }
	/*
	public function save($Params=null)
	{
		$kdpasien = $Params['KdPasien'];
		$urutmasuk = $Params['UrutMasuk'];
		$tglmasuk = $Params['Tgl'];
		$kdunit = $Params['KdUnit'];
		$list = $Params['List'];
		$anamnese = $Params['Anamnese'];
		$catatan= $Params['Catatan'];
		$idkonpas = $this->getIdkonpas($kdpasien,$tglmasuk,$urutmasuk,$kdunit);
		
		$data = array(
		"id_konpas" => $idkonpas,
		"kd_pasien" => $kdpasien,
		"tgl_masuk" => $tglmasuk,
		"kd_unit" => $kdunit,
		"urut_masuk" => $urutmasuk
		);
		
		$this->load->model("poliklinik/tb_konpas");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		 $query =  $this->tb_konpas->db->where($criteria, null, false);
		  $query = $this->tb_konpas->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {
        	 $save = $this->tb_konpas->save($data);		
            }else{
		     echo "";
			}
	
		$this->load->model("rawat_jalan/tb_kunjungan_pasien");
		$update = $this->tb_kunjungan_pasien->db->where($criteria, null, false);
		$dataupdate = array("anamnese"=>$anamnese,"cat_fisik"=>$catatan);
		$update = $this->tb_kunjungan_pasien->update($dataupdate);
		
		
		$cut = explode('<>',$list);
		for($i=0;$i<count($cut)-1;$i++)
		{
			if($cut[$i] == '')
			{
				echo "";
			}
			else
			{
			$data = explode('::',$cut[$i]);
			
			$kondisi = array(
			"id_kondisi" => $data[0],
			"id_konpas" => $idkonpas,
			"hasil" => $data[1]
			);
			$this->load->model("poliklinik/tb_konpasdtl");
			$filter = "id_konpas = '$idkonpas' AND id_kondisi = '".$data[0]."'";
		
				 $detaildata = $this->tb_konpasdtl->db->where($filter, null, false);
				 $detaildata = $this->tb_konpasdtl->GetRowList( 0,1, "", "","");
				 if ($detaildata[1]==0)
				 {
				 $save = $this->tb_konpasdtl->save($kondisi);		
				 }else{
				 $hasil = array("hasil"=>$data[1]);	 
				 $updatedetail = $this->tb_konpasdtl->db->where($filter, null, false);
				 $updatedetail = $this->tb_konpasdtl->update($hasil);
				 }
			}
		
		}
	
		
		$success = "sukses";
		
		echo '{success: true}';

		
	}
	
	private function getIdkonpas($kdpasien,$tglmasuk,$urutmasuk,$kdunit)
	{
		$date= date('Ymd');
		$query = $this->db->query("select id_konpas from mr_konpas order by id_konpas desc limit 1");
		if($query->num_rows() == 0)
		{
		$newid=$date."0001";
		}
		
		else
		{
		$getcurrent = $this->db->query("select id_konpas from mr_konpas where kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND urut_masuk = $urutmasuk AND kd_unit = '$kdunit'");
		if($getcurrent->num_rows != 0)
		{
			$res = $getcurrent->row();
			$newid = $res->id_konpas;
		}
		else
		{	
		$result = $query->row();	
		$lastid = $result->id_konpas;
		$cutid = substr($lastid, -4);
		$newno = (int) $cutid +1;
		$newid = $date.str_pad($newno,4,"00000",STR_PAD_LEFT);
		}
		}
		return $newid;
	}

*/
 }

?>