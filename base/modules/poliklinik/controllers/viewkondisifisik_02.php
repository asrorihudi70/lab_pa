<?php

class viewkondisifisik_02 extends MX_Controller
{

	private $CI;
	public function __construct()
	{
		//parent::Controller();
		parent::__construct();
		$this->CI = &get_instance();
		$this->CI->load->library('session');
	}


	public function index()
	{
		$this->load->view('main/index');
	}


	public function read($Params = null)
	{

		try {

			$this->load->model('poliklinik/tb_mrkondisifisik_02');
			if (strlen($Params[4]) !== 0) {
				//$query = $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
				$criteria = (str_replace("~", "'", $Params[4]));
			}

			$result = $this->db->query('SELECT A.id_kondisi AS "ID_KONDISI",A.kondisi as "KONDISI", A.satuan as "SATUAN",A.orderlist as "ORDERLIST", A.kd_unit as "KD_UNIT", B.hasil as "HASIL"  
FROM MR_KONPASDTL B INNER JOIN (SELECT * FROM MR_KONPAS WHERE ' . $criteria . '  ) 
AS C ON B.ID_KONPAS = C .ID_KONPAS
RIGHT JOIN MR_KONDISIFISIK A ON A.ID_KONDISI = B.ID_KONDISI')->result();



			//$res = $this->tb_mrkondisifisik_02->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

		} catch (Exception $o) {
			echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function save($Params = null)
	{

		$kdpasien  = $Params['KdPasien'];
		$urutmasuk = $Params['UrutMasuk'];
		$tglmasuk  = $Params['Tgl'];
		$kdunit    = $Params['KdUnit'];
		$list      = $Params['List'];
		$anamnese  = $Params['Anamnese'];
		$catatan   = $Params['Catatan'];
		$idkonpas = $this->getIdkonpas($kdpasien, $tglmasuk, $urutmasuk, $kdunit);
		// echo $idkonpas; die;
		$data = array(
			"id_konpas" => $idkonpas,
			"kd_pasien" => $kdpasien,
			"tgl_masuk" => $tglmasuk,
			"kd_unit" => $kdunit,
			"urut_masuk" => $urutmasuk
		);

		$this->load->model("poliklinik/tb_konpas_02");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		$query =  $this->tb_konpas_02->db->where($criteria, null, false);
		$query = $this->tb_konpas_02->GetRowList(0, 1, "", "", "");

		if ($query[0] == 0) {
			$save = $this->tb_konpas_02->save($data);
		} else {
			echo "";
		}

		$this->load->model("rawat_jalan/tb_kunjungan_pasien_02");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		$update = $this->tb_kunjungan_pasien_02->db->where($criteria, null, false);

		$dataupdate = array("anamnese" => $anamnese, "cat_fisik" => $catatan);
		$update = $this->db->query("update kunjungan set anamnese='" . $anamnese . "',cat_fisik='" . $catatan . "'  where kd_pasien = '" . $kdpasien . "' AND tgl_masuk = '" . $tglmasuk . "' AND kd_unit = '" . $kdunit . "' AND urut_masuk = " . $urutmasuk . " ");

		// $update = $this->tb_kunjungan_pasien->update($dataupdate);

		// echo $list;die;
		$cut = explode('<>', $list);
		for ($i = 0; $i < count($cut) - 1; $i++) {
			// echo "<pre>".var_export($cut, true)."<pre>";
			if ($cut[$i] == '') {
				echo "";
			} else {
				$data = explode('::', $cut[$i]);

				$kondisi = array(
					"id_kondisi" => $data[0],
					"id_konpas" => $idkonpas,
					"hasil" => $data[1]
				);
				$this->load->model("poliklinik/tb_konpasdtl_02");
				$filter = "id_konpas = '$idkonpas' AND id_kondisi = '" . $data[0] . "'";
				// echo "<pre>".var_export($data, true)."<pre>";

				// $detaildata = $this->tb_konpasdtl_02->db->where($filter, null, false);
				// $detaildata = $this->tb_konpasdtl_02->GetRowList( 0,1, "", "","");

				$detaildata = $this->db->query("SELECT  TOP 1 *	FROM mr_konpasdtl WHERE id_konpas = '$idkonpas' AND id_kondisi = '" . $data[0] . "'");
				// die;
				if ($detaildata->num_rows() == 0) {
					// $save = $this->tb_konpasdtl_02->save($kondisi);	
					$save =  $this->db->insert("mr_konpasdtl", $kondisi);
				} else {
					$hasil = array("hasil" => $data[1]);
					//  $updatedetail = $this->tb_konpasdtl_02->db->where($filter, null, false);
					//  $updatedetail = $this->tb_konpasdtl_02->update($hasil);
					$updatedetail = $this->db->query("update mr_konpasdtl set hasil='" . $data[1] . "'  where id_konpas = '$idkonpas' AND id_kondisi = '" . $data[0] . "' ");
				}
			}
		}

		$saveCppt = $this->saveCPPT($kdpasien, $tglmasuk, $urutmasuk, $kdunit, $anamnese, $catatan);


		$success = "sukses";

		echo '{success: true}';
	}

	private function saveCPPT($kdPasien, $tglMasuk, $urutMasuk, $kdUnit, $anamnese, $catatan)
	{
		$kdUser = $this->CI->session->userdata['user_id']['id'];
		$fullname = $this->db->query("SELECT full_name FROM zusers WHERE kd_user=" . $kdUser . "")->row()->full_name;
		$hasil = '';
		if ($anamnese != '') {
			$hasil .= 'S:' . $anamnese . ' ';
		}
		if ($catatan != '') {
			$hasil .= 'O:' . $catatan . ' ';
		}
		if ($kdUnit == '3') {
			$criteria = array(
				"kd_pasien" => $kdPasien,
				"tgl_masuk" => $tglMasuk,
				"kd_unit" => $kdUnit,
				"urut_masuk" => $urutMasuk,
				"bagian" => 'Perawat'
			);
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("cppt_igd");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$params = array(
					'hasil' 	=> $hasil,
					'verifikasi' => $fullname,
					'waktu' 	=> date('Y-m-d H:i:s'),
					// 'instruksi' => $value->instruksi,
				);
				$this->db->where($criteria);
				$this->db->update("cppt_igd", $params);
			} else {
				$criteriaGet = array(
					"kd_pasien" => $kdPasien,
					"tgl_masuk" => $tglMasuk,
					"kd_unit" => $kdUnit,
					"urut_masuk" => $urutMasuk
				);
				$this->db->select(" max(urut) as urut ");
				$this->db->where($criteriaGet);
				$this->db->from("cppt_igd");
				$getUrut = $this->db->get();
				$urut = (int)$getUrut->row()->urut + 1;
				$params = array(
					'kd_pasien' => $kdPasien,
					'kd_unit' 	=> $kdUnit,
					'tgl_masuk' => $tglMasuk,
					'urut_masuk' => $urutMasuk,
					'bagian' 	=> 'Perawat',
					'hasil' 	=> $hasil,
					'verifikasi' => $fullname,
					'waktu' 	=> date('Y-m-d H:i:s'),
					'instruksi' => '',
					'urut' 		=> $urut,
				);
				$this->db->insert("cppt_igd", $params);
			}
		}
	}

	private function getIdkonpas($kdpasien, $tglmasuk, $urutmasuk, $kdunit)
	{
		$date = date('Ymd');
		$query = $this->db->query("select top 1 id_konpas from mr_konpas order by id_konpas desc ");
		if ($query->num_rows() == 0) {
			$newid = $date . "0001";
		} else {
			$getcurrent = $this->db->query("select id_konpas from mr_konpas where kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND urut_masuk = $urutmasuk AND kd_unit = '$kdunit'");
			if ($getcurrent->num_rows() != 0) {
				$res = $getcurrent->row();
				$newid = $res->id_konpas;
			} else {
				$result = $query->row();
				$lastid = $result->id_konpas;
				$cutid = substr($lastid, -4);
				$newno = (int) $cutid + 1;
				$newid = $date . str_pad($newno, 4, "00000", STR_PAD_LEFT);
			}
		}
		return $newid;
	}
}
