<?php

class viewpemeriksaangigi2_02 extends MX_Controller
{

	private $CI;
	public function __construct()
	{
		//parent::Controller();
		parent::__construct();
		$this->CI = &get_instance();
		$this->CI->load->library('session');
	}


	public function index()
	{
		$this->load->view('main/index');
	}


	public function read($Params = null)
	{

		try {
			if (strlen($Params[4]) !== 0) {
				$criteria = (str_replace("~", "'", $Params[4]));
			}
			$result = $this->db->query('SELECT 
			A.id_parameter AS "ID_PARAMETER",
			A.gigi_atasbawahposisikiri AS "GIGI_ATASBAWAHPOSISIKIRI",
			A.gigi_atasbawahposisikanan AS "GIGI_ATASBAWAHPOSISIKANAN",
			B.hasil_gigi_atasbawahposisikiri AS "HASIL_GIGI_ATASBAWAHPOSISIKIRI",
			B.hasil_gigi_atasbawahposisikanan AS "HASIL_GIGI_ATASBAWAHPOSISIKANAN"
			FROM MR_DENTALDTL B
			INNER JOIN ( SELECT * FROM MR_DENTAL WHERE ' . $criteria . ' ) 
			AS C ON B.id_dentalcare = C.id_dentalcare 
			RIGHT JOIN MR_DENTALCARE A ON A.id_parameter = B.id_parameter
			WHERE CAST(A.id_parameter AS INT) BETWEEN 9 AND 16
			ORDER BY CAST(A.id_parameter AS INT) ASC')->result();

			//$res = $this->tb_mrkondisifisik->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

		} catch (Exception $o) {
			echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}
}