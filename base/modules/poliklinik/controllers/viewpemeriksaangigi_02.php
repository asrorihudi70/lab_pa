<?php

class viewpemeriksaangigi_02 extends MX_Controller
{

	private $CI;
	public function __construct()
	{
		//parent::Controller();
		parent::__construct();
		$this->CI = &get_instance();
		$this->CI->load->library('session');
	}


	public function index()
	{
		$this->load->view('main/index');
	}


	public function read($Params = null)
	{

		try {
			// $this->load->model('poliklinik/tb_mrkondisifisik');
			if (strlen($Params[4]) !== 0) {
				//$query = $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
				$criteria = (str_replace("~", "'", $Params[4]));
			}
			$result = $this->db->query('SELECT 
			A.id_parameter AS "ID_PARAMETER",
			A.gigi_atasbawahposisikiri AS "GIGI_ATASBAWAHPOSISIKIRI",
			A.gigi_atasbawahposisikanan AS "GIGI_ATASBAWAHPOSISIKANAN",
			B.hasil_gigi_atasbawahposisikiri AS "HASIL_GIGI_ATASBAWAHPOSISIKIRI",
			B.hasil_gigi_atasbawahposisikanan AS "HASIL_GIGI_ATASBAWAHPOSISIKANAN"
			FROM MR_DENTALDTL B
			INNER JOIN ( SELECT * FROM MR_DENTAL WHERE ' . $criteria . ' ) 
			AS C ON B.id_dentalcare = C.id_dentalcare 
			RIGHT JOIN MR_DENTALCARE A ON A.id_parameter = B.id_parameter
			WHERE CAST(A.id_parameter AS INT) BETWEEN 1 AND 8
			ORDER BY CAST(A.id_parameter AS INT) ASC')->result();

			//$res = $this->tb_mrkondisifisik->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

		} catch (Exception $o) {
			echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function save($Params = null)
	{
		$kdpasien  = $Params['KdPasien'];
		$urutmasuk = $Params['UrutMasuk'];
		$tglmasuk  = $Params['Tgl'];
		$kdunit    = $Params['KdUnit'];
		$listatas  = $Params['ListAtas'];
		$listbawah  = $Params['ListBawah'];
		$iddentalcare = $this->getIdDentalcare($kdpasien, $tglmasuk, $urutmasuk, $kdunit);

		// echo $iddentalcare; die;
		$data = array(
			"id_dentalcare" => $iddentalcare,
			"kd_pasien" => $kdpasien,
			"tgl_masuk" => $tglmasuk,
			"kd_unit" => $kdunit,
			"urut_masuk" => $urutmasuk
		);

		$this->load->model("poliklinik/tb_dental_02");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		$query =  $this->tb_dental_02->db->where($criteria, null, false);
		$query = $this->tb_dental_02->GetRowList(0, 1, "", "", "");

		if ($query[0] == 0) {
			$save = $this->tb_dental_02->save($data);
		} else {
			echo "";
		}

		$this->load->model("rawat_jalan/tb_kunjungan_pasien");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		$update = $this->tb_kunjungan_pasien->db->where($criteria, null, false);

		// $dataupdate = array("TEST_BUTA_WARNA" => $testbutawarna);
		// $update = $this->db->query("update kunjungan set TEST_BUTA_WARNA='" . $testbutawarna . "' where kd_pasien = '" . $kdpasien . "' AND tgl_masuk = '" . $tglmasuk . "' AND kd_unit = '" . $kdunit . "' AND urut_masuk = " . $urutmasuk . " ");

		// $update = $this->tb_kunjungan_pasien->update($dataupdate);

		// echo $list;die;
		$array1 = explode('<>', $listatas);
		$array2 = explode('<>', $listbawah);
		$cut = array_merge($array1, $array2);
		for ($i = 0; $i < count($cut) - 1; $i++) {
			// echo "<pre>".var_export($cut, true)."<pre>";
			if ($cut[$i] == '') {
				echo "";
			} else {
				$data = explode('::', $cut[$i]);
				$kondisi = array(
					"id_parameter" => $data[0],
					"id_dentalcare" => $iddentalcare,
					"hasil_gigi_atasbawahposisikiri" => $data[1],
					"hasil_gigi_atasbawahposisikanan" => $data[2]
				);
				$this->load->model("poliklinik/tb_dentaldtl_02");
				$filter = "id_dentalcare = '$iddentalcare' AND id_parameter = '" . $data[0] . "'";
				// echo "<pre>".var_export($data, true)."<pre>";

				// $detaildata = $this->tb_dentaldtl_02->db->where($filter, null, false);
				// $detaildata = $this->tb_dentaldtl_02->GetRowList( 0,1, "", "","");

				$detaildata = $this->db->query("SELECT  TOP 1 *	FROM mr_dentaldtl WHERE id_dentalcare = '$iddentalcare' AND id_parameter = '" . $data[0] . "'");
				// die;
				if ($detaildata->num_rows() == 0) {
					// $save = $this->tb_dentaldtl_02->save($kondisi);	
					$save =  $this->db->insert("mr_dentaldtl", $kondisi);
				} else {
					$hasil = array("hasil" => $data[1]);
					//  $updatedetail = $this->tb_dentaldtl_02->db->where($filter, null, false);
					//  $updatedetail = $this->tb_dentaldtl_02->update($hasil);
					$updatedetail = $this->db->query("update mr_dentaldtl set hasil_gigi_atasbawahposisikiri='" . $data[1] . "', hasil_gigi_atasbawahposisikanan='" . $data[2] . "'  where id_dentalcare = '$iddentalcare' AND id_parameter = '" . $data[0] . "' ");
				}
			}
		}

		$success = "sukses";

		echo '{success: true}';
	}

	private function getIdDentalcare($kdpasien, $tglmasuk, $urutmasuk, $kdunit)
	{
		$date = date('Ymd');
		$query = $this->db->query("SELECT top 1 id_dentalcare FROM mr_dental ORDER BY id_dentalcare DESC");
		if ($query->num_rows() == 0) {
			$newid = $date . "0001";
		} else {
			$getcurrent = $this->db->query("SELECT id_dentalcare FROM mr_dental WHERE kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND urut_masuk = $urutmasuk AND kd_unit = '$kdunit'");
			if ($getcurrent->num_rows() != 0) {
				$res = $getcurrent->row();
				$newid = $res->id_dentalcare;
			} else {
				$result = $query->row();
				$lastid = $result->id_dentalcare;
				$cutid = substr($lastid, -4);
				$newno = (int) $cutid + 1;
				$newid = $date . str_pad($newno, 4, "00000", STR_PAD_LEFT);
			}
		}
		return $newid;
	}
}
