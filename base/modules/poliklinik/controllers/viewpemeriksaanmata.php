<?php

class viewpemeriksaanmata extends MX_Controller
{

	private $CI;
	public function __construct()
	{
		//parent::Controller();
		parent::__construct();
		$this->CI = &get_instance();
		$this->CI->load->library('session');
	}


	public function index()
	{
		$this->load->view('main/index');
	}


	public function read($Params = null)
	{

		try {
			// $this->load->model('poliklinik/tb_mrkondisifisik');
			if (strlen($Params[4]) !== 0) {
				//$query = $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
				$criteria = (str_replace("~", "'", $Params[4]));
			}
			$result = $this->db->query('SELECT A.id_parameter AS "ID_PARAMETER",A.parameter AS "PARAMETER",B.hasil_odre AS "HASIL_ODRE",B.hasil_osle AS "HASIL_OSLE" FROM MR_EYEDTL B
			INNER JOIN ( SELECT * FROM MR_EYE WHERE ' . $criteria . ' ) 
			AS C ON B.id_eyecare = C.id_eyecare 
			RIGHT JOIN MR_EYECARE A ON A.id_parameter = B.id_parameter')->result();

			//$res = $this->tb_mrkondisifisik->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);

		} catch (Exception $o) {
			echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($result) . '}';
	}

	public function save($Params = null)
	{
		$kdpasien  = $Params['KdPasien'];
		$urutmasuk = $Params['UrutMasuk'];
		$tglmasuk  = $Params['Tgl'];
		$kdunit    = $Params['KdUnit'];
		$list      = $Params['List'];
		$testbutawarna  = $Params['TestButaWarna'];
		$ideyecare = $this->getIdeyecare($kdpasien, $tglmasuk, $urutmasuk, $kdunit);

		// echo $ideyecare; die;
		$data = array(
			"id_eyecare" => $ideyecare,
			"kd_pasien" => $kdpasien,
			"tgl_masuk" => $tglmasuk,
			"kd_unit" => $kdunit,
			"urut_masuk" => $urutmasuk
		);

		$this->load->model("poliklinik/tb_eye");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		$query =  $this->tb_eye->db->where($criteria, null, false);
		$query = $this->tb_eye->GetRowList(0, 1, "", "", "");

		if ($query[0] == 0) {
			$save = $this->tb_eye->save($data);
		} else {
			echo "";
		}

		$this->load->model("rawat_jalan/tb_kunjungan_pasien");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		$update = $this->tb_kunjungan_pasien->db->where($criteria, null, false);

		$dataupdate = array("TEST_BUTA_WARNA" => $testbutawarna);
		$update = $this->db->query("update kunjungan set TEST_BUTA_WARNA='" . $testbutawarna . "' where kd_pasien = '" . $kdpasien . "' AND tgl_masuk = '" . $tglmasuk . "' AND kd_unit = '" . $kdunit . "' AND urut_masuk = " . $urutmasuk . " ");

		// $update = $this->tb_kunjungan_pasien->update($dataupdate);

		// echo $list;die;
		$cut = explode('<>', $list);
		for ($i = 0; $i < count($cut) - 1; $i++) {
			// echo "<pre>".var_export($cut, true)."<pre>";
			if ($cut[$i] == '') {
				echo "";
			} else {
				$data = explode('::', $cut[$i]);
				$kondisi = array(
					"id_parameter" => $data[0],
					"id_eyecare" => $ideyecare,
					"hasil_odre" => $data[1],
					"hasil_osle" => $data[2]
				);
				$this->load->model("poliklinik/tb_eyedtl");
				$filter = "id_eyecare = '$ideyecare' AND id_parameter = '" . $data[0] . "'";
				// echo "<pre>".var_export($data, true)."<pre>";

				// $detaildata = $this->tb_eyedtl->db->where($filter, null, false);
				// $detaildata = $this->tb_eyedtl->GetRowList( 0,1, "", "","");

				$detaildata = $this->db->query("SELECT  TOP 1 *	FROM mr_eyedtl WHERE id_eyecare = '$ideyecare' AND id_parameter = '" . $data[0] . "'");
				// die;
				if ($detaildata->num_rows() == 0) {
					// $save = $this->tb_eyedtl->save($kondisi);	
					$save =  $this->db->insert("mr_eyedtl", $kondisi);
				} else {
					$hasil = array("hasil" => $data[1]);
					//  $updatedetail = $this->tb_eyedtl->db->where($filter, null, false);
					//  $updatedetail = $this->tb_eyedtl->update($hasil);
					$updatedetail = $this->db->query("update mr_eyedtl set hasil_odre='" . $data[1] . "', hasil_osle='" . $data[2] . "'  where id_eyecare = '$ideyecare' AND id_parameter = '" . $data[0] . "' ");
				}
			}
		}

		$success = "sukses";

		echo '{success: true}';
	}

	private function getIdeyecare($kdpasien, $tglmasuk, $urutmasuk, $kdunit)
	{
		$date = date('Ymd');
		$query = $this->db->query("SELECT top 1 id_eyecare FROM mr_eye ORDER BY id_eyecare DESC");
		if ($query->num_rows() == 0) {
			$newid = $date . "0001";
		} else {
			$getcurrent = $this->db->query("SELECT id_eyecare FROM mr_eye WHERE kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND urut_masuk = $urutmasuk AND kd_unit = '$kdunit'");
			if ($getcurrent->num_rows() != 0) {
				$res = $getcurrent->row();
				$newid = $res->id_eyecare;
			} else {
				$result = $query->row();
				$lastid = $result->id_eyecare;
				$cutid = substr($lastid, -4);
				$newno = (int) $cutid + 1;
				$newid = $date . str_pad($newno, 4, "00000", STR_PAD_LEFT);
			}
		}
		return $newid;
	}
}
