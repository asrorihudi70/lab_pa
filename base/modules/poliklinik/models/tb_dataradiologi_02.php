<?php

class tb_dataradiologi_02 extends TblBase
{
    function __construct()
    {
		
		$this->TblName='mr_radkonsuldtl';
		TblBase::TblBase(true);
		$admin = (string) 'Admin';
		$this->SqlQuery = 'SELECT * FROM(SELECT A.KD_PASIEN as kd_pasien,A.TGL_MASUK as tgl_masuk,A.KD_UNIT as kd_unit,A.URUT_MASUK as urut_masuk,A.ID_RADKONSUL ,B.KD_PRODUK ,
CASE WHEN (SELECT NAMA FROM DOKTER WHERE KD_DOKTER = B.KD_DOKTER) IS NULL THEN
'."'Admin'".'
ELSE
(SELECT NAMA FROM DOKTER WHERE KD_DOKTER = B.KD_DOKTER)
END
as kd_dokter
,
C.KD_KLAS ,
D.klasifikasi,
C.DESKRIPSI
FROM MR_RADKONSUL A LEFT JOIN MR_RADKONSULDTL B ON A.ID_RADKONSUL = B.ID_RADKONSUL
LEFT JOIN PRODUK C ON C.KD_PRODUK=B.KD_PRODUK
LEFT JOIN KLAS_PRODUK D using (kd_klas)
) AS RESDATA';
		//$this->SQL=$this->db;
    }

    function FillRow($rec)
    {
        $row=new Rowviewrad;
        $row->ID_RADKONSUL=$rec->id_radkonsul;
		$row->KD_PRODUK=$rec->kd_produk;
		$row->KD_KLAS=$rec->kd_klas;
		$row->KLASIFIKASI=$rec->klasifikasi;
		$row->DESKRIPSI=$rec->deskripsi;
		$row->KD_DOKTER=$rec->kd_dokter;
        return $row;
    }
}

class Rowviewrad
{
    public $ID_RADKONSUL;
	public $KD_PRODUK;
	public $KD_KLAS;
	public $DESKRIPSI;
	public $KD_DOKTER;
	public $KLASIFIKASI;
}

?>
