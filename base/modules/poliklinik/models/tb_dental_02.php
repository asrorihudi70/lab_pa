<?php

class tb_dental_02 extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_dental';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkondisi;
        $row->ID_DENTALCARE=$rec->id_dentalcare;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->TGL_MASUK=$rec->tgl_masuk;
        $row->URUT_MASUK=$rec->urut_masuk;
        $row->KD_UNIT=$rec->kd_unit;
        return $row;
    }
}

class Rowviewkondisi
{
    public $ID_DENTALCARE;
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
}

?>
