<?php

class tb_dentaldtl_02 extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_dentaldtl';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkondisidtl;
        $row->ID_DENTALCARE=$rec->id_dentalcare;
        $row->ID_PARAMETER=$rec->id_parameter;
        $row->HASIL_GIGI_ATASBAWAHPOSISIKIRI=$rec->hasil_gigi_atasbawahposisikiri;
        $row->HASIL_GIGI_ATASBAWAHPOSISIKANAN=$rec->hasil_gigi_atasbawahposisikanan;
        return $row;
    }
}

class Rowviewkondisidtl
{
    public $ID_DENTALCARE;
    public $ID_PARAMETER;
    public $HASIL_GIGI_ATASBAWAHPOSISIKIRI;
    public $HASIL_GIGI_ATASBAWAHPOSISIKANAN;
}

?>
