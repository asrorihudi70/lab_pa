<?php

class tb_eyedtl extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_eyedtl';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkondisidtl;
        $row->ID_EYECARE=$rec->id_eyecare;
        $row->ID_PARAMETER=$rec->id_parameter;
        $row->HASIL_ODRE=$rec->hasil_odre;
        $row->HASIL_OSLE=$rec->hasil_osle;
        return $row;
    }
}

class Rowviewkondisidtl
{
    public $ID_EYECARE;
    public $ID_PARAMETER;
    public $HASIL_ODRE;
    public $HASIL_OSLE;
}

?>
