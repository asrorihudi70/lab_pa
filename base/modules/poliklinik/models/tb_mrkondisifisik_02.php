<?php

class tb_mrkondisifisik_02 extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_kondisifisik';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT * FROM MR_KONDISIFISIK LEFT JOIN MR_KONPASDTL ON MR_KONDISIFISIK.ID_KONDISI = MR_KONPASDTL.ID_KONDISI
LEFT JOIN MR_KONPAS ON MR_KONPAS.ID_KONPAS=MR_KONPASDTL.ID_KONPAS
";
    }
	
	public function read()
	{
		   $this->SqlQuery="SELECT * FROM MR_KONDISIFISIK LEFT JOIN MR_KONPASDTL ON MR_KONDISIFISIK.ID_KONDISI = MR_KONPASDTL.ID_KONDISI
LEFT JOIN MR_KONPAS ON MR_KONPAS.ID_KONPAS=MR_KONPASDTL.ID_KONPAS
";
	}

    function FillRow($rec)
    {
        $row=new Rowviewkondisi;
        $row->ID_KONDISI=$rec->id_kondisi;
        $row->KONDISI=$rec->kondisi;
        $row->SATUAN=$rec->satuan;
        $row->ORDERLIST=$rec->orderlist;
        $row->KD_UNIT=$rec->kd_unit;
		$row->HASIL=$rec->hasil;
        return $row;
    }
}

class Rowviewkondisi
{
    public $ID_KONDISI;
    public $KONDISI;
    public $SATUAN;
    public $ORDERLIST;
    public $KD_UNIT;
	public $HASIL;
}

?>
