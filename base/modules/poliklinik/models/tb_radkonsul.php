<?php

class tb_radkonsul extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_radkonsul';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewrad;
        $row->ID_RADKONSUL=$rec->id_radkonsul;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->TGL_MASUK=$rec->tgl_masuk;
        $row->URUT_MASUK=$rec->urut_masuk;
        $row->KD_UNIT=$rec->kd_unit;
        return $row;
    }
}

class Rowviewrad
{
    public $ID_RADKONSUL;
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
}

?>
