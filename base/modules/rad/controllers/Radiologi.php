<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Radiologi extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }
    

    public function save_transfer()
    {   
        $KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
        $Tglasal            = $_POST['Tglasal'];
        $KDunittujuan       = $_POST['KDunittujuan'];
        $KDkasirIGD         = $_POST['KDkasirIGD'];
        $Kdcustomer         = $_POST['Kdcustomer'];
        $TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
        $KdUnit             = $_POST['KdUnit'];
        $Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;
        $total              = str_replace('.','',$_POST['Jumlahtotal']); 
        // $Shift1              = $this->get_shift($this->get_unit($_POST['KdUnit'])->row()->kd_bagian); //update maya
        $Shift1             = $this->get_shift($_POST['KdUnit']); //update maya
        $TglTranasksitujuan = $_POST['TglTranasksitujuan'];
        $KASIRRWI           = $_POST['KasirRWI'];
        $TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
        $_kduser            = $this->session->userdata['user_id']['id'];
        $tgltransfer        = $_POST['TglTransfer'];//date("Y-m-d");
        $tglhariini         = date("Y-m-d");
        $KDalasan           = $_POST['KDalasan'];
        $kd_pasien          = $_POST['KdpasienIGDtujuan'];
        $kd_dokter_         = $this->input->post('kd_dokter_penunjang');
        $no_photo_rad       = $this->input->post('no_photo_rad');

        //memperoleh nilai kd_unit_kamar pasien terakhit menginap
        $this->db->trans_begin();
                
        $det_query   = $this->db->query("SELECT TOP 1 COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '".$TrKodeTranskasi."' and kd_kasir= '".$KDkasirIGD."' order by urut desc");
        // $resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
        // echo "<pre>".var_export($det_query, true)."</pre>"; die;
        if($det_query->num_rows() == 0)
        {
            $urut_detailbayar=1;
        }else{
            // while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
            foreach($det_query as $line)
            {
                $urut_detailbayar = $line['urutan'];
            }
        }
        // '$KDkasirIGD','$TrKodeTranskasi'
        $criteria = array(
            'kd_kasir'      => $KDkasirIGD,
            'no_transaksi'  => $TrKodeTranskasi
        );
        $this->db->where($criteria);
        $this->db->update("transaksi", array( 'posting_transaksi' => '1') );
        
        $this->db->where($criteria);
        $this->db->from("transaksi");

        $query_transaksi = $this->db->get();
		
		
        unset($criteria);
        $criteria = array(
            'kd_pasien'     => $query_transaksi->row()->KD_PASIEN,
            'tgl_masuk'     => $query_transaksi->row()->TGL_TRANSAKSI,
            'urut_masuk'    => $query_transaksi->row()->URUT_MASUK,
            'kd_unit'       => $query_transaksi->row()->KD_UNIT,
        );

        $this->db->where($criteria);
        $this->db->update("kunjungan", array( 'no_foto_rad' => $no_photo_rad, ) );

        if ($query_transaksi->num_rows() > 0) {
            unset($criteria);
            $criteria = array(
                'kd_pasien'     => $query_transaksi->row()->KD_PASIEN,
                'tgl_masuk'     => $query_transaksi->row()->TGL_TRANSAKSI,
                'urut_masuk'    => $query_transaksi->row()->URUT_MASUK,
                'kd_unit'       => $query_transaksi->row()->KD_UNIT,
            );

            $this->db->where($criteria);
            $this->db->update("kunjungan", array( 'kd_dokter' => $kd_dokter_) );
        }

        $pay_query = $this->db->query(" INSERT into detail_bayar 
                    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

                    values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");    
        if($pay_query) //&& $pay_query_SQL)
        {
            $detailTrbayar = $this->db->query(" INSERT into detail_tr_bayar 
                (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
                SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
                WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");     
            if($detailTrbayar) //&& $detailTrbayar_SQL)
            {   
                $statuspembayaran = $this->db->query(" updatestatustransaksi @kd_kasir='$KDkasirIGD',@no_transaksi='$TrKodeTranskasi',@urut= $urut_detailbayar, @tgltransaksi='$tgltransfer' ");  
                if($statuspembayaran)
                {
                    $detailtrcomponet = $this->db->query("INSERT into detail_tr_bayar_component
                    (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
                    Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
                    '$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
                    FROM Detail_Component dc
                    INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
                    INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
                    and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
                    WHERE dc.Kd_Kasir = '$KDkasirIGD'
                    AND dc.No_Transaksi ='$TrKodeTranskasi'
                    ORDER BY dc.Kd_Component"); 
                    if($detailtrcomponet) //&& $detailtrcomponet_SQL)
                    {           
                        $urutquery ="SELECT max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
                        $resulthasilurut = $this->db->query($urutquery)->row()->urutan;
                        if($resulthasilurut <= 0)
                        {
                            $uruttujuan=1;
                        }else
                        {                           
                            $uruttujuan = $resulthasilurut + 1;
                        }
                                                
                        $getkdtarifcus=$this->db->query(" getkdtarifcus @KdCus='$Kdcustomer' ")->result();
                        foreach($getkdtarifcus as $xkdtarifcus)
                        {
                            $kdtarifcus = $xkdtarifcus->getkdtarifcus;
                        }
                                                            
                        $getkdproduk = $this->db->query("SELECT TOP 1 pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
                            FROM Produk_Charge pc 
                            INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
                            WHERE  left(kd_unit,len(kd_unit))='$KdUnit'  order by pc.kd_unit asc ")->result();

                        foreach($getkdproduk as $det1)
                        {
                            $kdproduktranfer = $det1->kdproduk;
                            $kdUnittranfer = $det1->unitproduk;
                        }
                                                
                        $gettanggalberlaku=$this->db->query(" gettanggalberlakuunit
                        @kdtarif='$kdtarifcus',@tglberlaku='$tgltransfer',@tglberakhir='$tglhariini',@kdproduk=$kdproduktranfer,@kdunit='$kdUnittranfer'")->result();
                        foreach($gettanggalberlaku as $detx)
                        {
                            $tanggalberlaku = $detx->gettanggalberlakuunit;
                            
                        }

                        if($tanggalberlaku=='') //&& $tanggalberlaku_SQL=='')
                        {
                            echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}"; 
                            exit;
                        }

                        $kd_unit_tr = $this->db->query("SELECT kd_unit_kamar from nginap 
                                                where kd_pasien = '$kd_pasien' 
                                                    and kd_unit='$KDunittujuan' 
                                                    and tgl_masuk='$TglTranasksitujuan' 
                                                    and akhir = '1'")->result();
                            if (count($kd_unit_tr)==0)
                            {
                                $data = array(
                                    'kd_kasir'      => $KASIRRWI,
                                    'no_transaksi'  => $TRKdTransTujuan,
                                    'urut'          => $uruttujuan,
                                    'tgl_transaksi' => $tgltransfer,
                                    'kd_user'       => $_kduser,
                                    'kd_tarif'      => $kdtarifcus,
                                    'kd_unit_tr'    => $KDunittujuan,
                                    'kd_produk'     => $kdproduktranfer,
                                    'kd_unit'       => $KdUnit,
                                    'tgl_berlaku'   => $tanggalberlaku,
                                    'charge'        => '1',
                                    'adjust'        => '1',
                                    'folio'         => 'E',
                                    'qty'           => 1,
                                    'harga'         => $total,
                                    'shift'         => $Shift1,
                                    'tag'           => '0',
                                    'no_faktur'     => $TrKodeTranskasi
                                );
                                $detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
                            }else
                            {
                                $kd_unit_tr = $this->db->query("SELECT kd_unit_kamar from nginap 
                                                where kd_pasien = '$kd_pasien' 
                                                    and kd_unit='$KDunittujuan' 
                                                    and tgl_masuk='$TglTranasksitujuan' 
                                                    and akhir = '1'")->row()->kd_unit_kamar;
                                $data = array(
                                   'kd_kasir' => $KASIRRWI,
                                   'no_transaksi' => $TRKdTransTujuan,
                                   'urut' => $uruttujuan,
                                   'tgl_transaksi' => $tgltransfer,
                                   'kd_user' => $_kduser,
                                   'kd_tarif' => $kdtarifcus,
                                   'kd_produk' => $kdproduktranfer,
                                   'kd_unit' => $KdUnit,
                                   'kd_unit_tr' => $kd_unit_tr,
                                   'tgl_berlaku' => $tanggalberlaku,
                                   'charge' => '1',
                                   'adjust' => '1',
                                   'folio' => 'E',
                                   'qty' => 1,
                                   'harga' => $total,
                                   'shift' => $Shift1,
                                   'tag' => '0',
                                   'no_faktur' => $TrKodeTranskasi
                                );
                                $detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
                            }
                        
                        
                        if($detailtransaksitujuan) //&& $detailtransaksitujuan_SQL) 
                        {
                            $detailcomponentujuan = $this->db->query("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
                               select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
                               from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
                               and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");                        
                            if($detailcomponentujuan) //&& $detailcomponentujuan_SQL)
                            {               
                                $tranferbyr = $this->db->query("INSERT INTO transfer_bayar
                                (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
                                det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
                                  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
                                  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
                                if($tranferbyr){
                                    $query_balance_det_trans=$this->db->query("SELECT sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
                                    $query_balance_det_tr_bayar=$this->db->query("SELECT sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
                                    if ($query_balance_det_trans == $query_balance_det_tr_bayar){
                                        $query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");          
                            
                                    }
                                            IF ($KASIR_SYS_WI==$KASIRRWI){
                                                            $trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
                                                            ('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");    
                                                            if($trkamar) //&& $trkamar_SQL)
                                                            {
                                                                $this->db->trans_commit();
                                                                //$this->dbSQL->trans_commit();
                                                                echo '{success:true, no_transaksi : "'.$TrKodeTranskasi.'", kd_kasir:"'.$KDkasirIGD.'", }';
                                                            }else
                                                             {
                                                              $this->db->trans_rollback();
                                                              //$this->dbSQL->trans_rollback();
                                                              echo '{success:false}';   
                                                             }
                                                    }else{
                                                    $this->db->trans_commit();
                                                    //$this->dbSQL->trans_commit();
                                                    echo '{success:true, no_transaksi : "'.$TrKodeTranskasi.'", kd_kasir:"'.$KDkasirIGD.'", }';
                                                    
                                                    }
                                            }
                                else{ 
                                        $this->db->trans_rollback();
                                        //$this->dbSQL->trans_rollback();
                                        echo '{success:false}'; 
                                    }
                            } else{ 
                                //$this->dbSQL->trans_rollback();
                                $this->db->trans_rollback();
                                echo '{success:false}'; 
                            }
                        } else {
                            $this->db->trans_rollback();
                            //$this->dbSQL->trans_rollback();
                            echo '{success:false}'; 
                        }
                    } else {
                        $this->db->trans_rollback();
                        //$this->dbSQL->trans_rollback();
                        echo '{success:false}'; 
                    }

                } else {
                    $this->db->trans_rollback();
                    //$this->dbSQL->trans_rollback();
                    echo '{success:false}'; 
                }
            } else {
                $this->db->trans_rollback();
                //$this->dbSQL->trans_rollback();
                echo '{success:false}'; 
            }
        } else {
            $this->db->trans_rollback();
            //$this->dbSQL->trans_rollback();
            echo '{success:false}'; 
            
        }       
    }
    
    public function simpan_template_hasil(){
        $response = array();
        $params = array(
            'template'      => $this->input->post('template'),
            'id_template'   => $this->input->post('id_template'),
        );

        $status = $this->input->post('keterangan');
        $criteria = array(
            'id_template'   => $params['id_template'],
        );

        if (strlen($params['id_template']) == 0) {
            $urut = $this->get_max_urut('id_template', 'rad_template_hasil');
            if ($urut->num_rows() > 0) {
                $params['id_template'] = (int)$urut->row()->urut + 1;
            }
            $params['keterangan']   = $params['template'];
            $params['template']     = "";
            $this->db->insert('rad_template_hasil', $params);
            if ($this->db->trans_status() > 0) {
                $response['status'] = true;
            }else{
                $response['status'] = false;
            }
        }else{
            $this->db->select("*");
            $this->db->where($criteria);
            $this->db->from("rad_template_hasil");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $this->db->where($criteria);
                if ($status === true || $status == 'true') {
                    $this->db->update('rad_template_hasil', array( 'keterangan' => $params['template'] ) );
                }else{
                    $this->db->update('rad_template_hasil', array( 'template' => $params['template'] ) );
                }
                if ($this->db->trans_status() > 0) {
                    $response['status'] = true;
                }else{
                    $response['status'] = false;
                }
            }else{
                $this->db->insert('rad_template_hasil', $params);
                if ($this->db->trans_status() > 0) {
                    $response['status'] = true;
                }else{
                    $response['status'] = false;
                }
            }
        }
        echo json_encode($response);
    }

    public function delete_template_hasil(){
        $response = array();
        $this->db->where( array( 'id_template' => $this->input->post('id_template'), ) );
        $this->db->delete('rad_template_hasil');
        if ($this->db->trans_status() > 0) {
            $response['status'] = true;
        }else{
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    private function get_max_urut($max, $table){
        $this->db->select(" MAX(".$max.") as urut");
        $this->db->from($table);
        return $this->db->get();
    }
    
    public function simpan_hasil_2019_02_21(){
        $response = array();
        $params = array(
            'hasil'      => $this->input->post('hasil'),
            'kd_pasien'  => $this->input->post('kd_pasien'),
            'kd_unit'    => $this->input->post('kd_unit'),
            'tgl_masuk'  => $this->input->post('tgl_masuk'),
            'urut_masuk' => $this->input->post('urut_masuk'),
            'urut'       => $this->input->post('urut'),
            'kd_test'    => $this->input->post('kd_test'),
        );

        $criteria = array(
            'kd_pasien'  => $params['kd_pasien'],
            'kd_unit'    => $params['kd_unit'],
            'tgl_masuk'  => $params['tgl_masuk'],
            'urut_masuk' => $params['urut_masuk'],
            'urut'       => $params['urut'],
            'kd_test'    => $params['kd_test'],
        );

        $this->db->select("*");
        $this->db->where($criteria);
        $this->db->from("rad_hasil");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where($criteria);
            $this->db->update('rad_hasil', array( 'hasil' => $params['hasil'] ) );
            if ($this->db->trans_status() > 0) {
                $response['status'] = true;
            }else{
                $response['status'] = false;
            }
        }else{
            $this->db->insert('rad_hasil', $params);
            if ($this->db->trans_status() > 0) {
                $response['status'] = true;
            }else{
                $response['status'] = false;
            }
        }
        echo json_encode($response);
    }

    public function simpan_hasil(){
        $response = array();
        $params = array(
            'hasil'      => $this->input->post('hasil'),
            'kd_pasien'  => $this->input->post('kd_pasien'),
            'kd_unit'    => $this->input->post('kd_unit'),
            'tgl_masuk'  => $this->input->post('tgl_masuk'),
            'urut_masuk' => $this->input->post('urut_masuk'),
            'urut'       => $this->input->post('urut'),
            'kd_test'    => $this->input->post('kd_test'),
            'catatan_dokter'    => $this->input->post('catatan_dokter'),
            'kd_dokter'  => '',
        );

        $dokter     = $this->input->post('dokter');
        $tgl_baca   = $this->input->post('tgl_baca');
        if (strlen($tgl_baca) == 0) {
            $tgl_baca = date("Y-m-d");
        }
        // $criteria = array(
        //     'nama'          => $params['dokter'],
        //     'kd_dokter'     => $params['dokter'],
        // );
        // $this->db->select("kd_dokter");
        // $this->db->or_where($criteria);
        // $this->db->from("dokter");
        // $query = $this->db->get();
        $query = "SELECT kd_dokter FROM dokter WHERE nama = '".$dokter."' OR kd_dokter = '".$dokter."'";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0) {
            $params['kd_dokter'] = $query->row()->kd_dokter;
        }

        // unset($criteria);
        if ($params['urut'] != '' && $params['kd_test'] != '') {
            $criteria = array(
                'kd_pasien'  => $params['kd_pasien'],
                'kd_unit'    => $params['kd_unit'],
                'tgl_masuk'  => $params['tgl_masuk'],
                'urut_masuk' => $params['urut_masuk'],
                'urut'       => $params['urut'],
                'kd_test'    => $params['kd_test'],
            );


            $this->db->select("*");
            $this->db->where($criteria);
            $this->db->from("rad_hasil");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $this->db->where($criteria);
                $this->db->update('rad_hasil', array( 'hasil' => $params['hasil']) );
                if ($this->db->trans_status() > 0) {
                    $response['status'] = true;
                }else{
                    $response['status'] = false;
                }
            }else{
                $this->db->insert('rad_hasil', $params);
                if ($this->db->trans_status() > 0) {
                    $response['status'] = true;
                }else{
                    $response['status'] = false;
                }
            }
        }
        unset($criteria);
        $criteria = array(
            'kd_pasien'  => $params['kd_pasien'],
            'kd_unit'    => $params['kd_unit'],
            'tgl_masuk'  => $params['tgl_masuk'],
            'urut_masuk' => $params['urut_masuk'],
        );
        $this->db->where($criteria);
        // $this->db->update('rad_hasil', array( 'kd_dokter' => $params['kd_dokter'], 'tgl_baca' => $tgl_baca, ) );
        $this->db->update('rad_hasil', array( 'kd_dokter' => $params['kd_dokter'], 'tgl_baca' => $tgl_baca, 'catatan_dokter' => $params['catatan_dokter']) );

        if ($this->db->trans_status() > 0) {
            $response['status'] = true;
        }else{
            $response['status'] = false;
        }
        echo json_encode($response);
    }
	
    private function get_shift($modul){
        $kd_bagian = $this->db->query("SELECT kd_bagian FROM unit where kd_unit = '".$modul."'")->row()->kd_bagian;
        $sqlbagianshift = $this->db->query("SELECT shift FROM bagian_shift  where kd_bagian='".$kd_bagian."'")->row()->shift;
        $lastdate       = $this->db->query("SELECT CONVERT(DATE,lastdate) as last_update FROM bagian_shift   where kd_bagian='".$kd_bagian."'")->row()->last_update;


        // $sqlbagianshift = $this->db->query("SELECT shift FROM rwi_shift  where kd_unit='".$modul."'")->row()->shift;
        // $lastdate = $this->db->query("SELECT to_char(last_update,'YYYY-mm-dd') as last_update FROM rwi_shift   where kd_unit='".$modul."'")->row()->last_update;
        
        $datnow= date('Y-m-d');
        if($lastdate<>$datnow && $sqlbagianshift==='3')
        {
            $sqlbagianshift2 = '4';
        }else{
            $sqlbagianshift2 = $sqlbagianshift;
        }
        return $sqlbagianshift2;
    }  
	
	public function setStatusBaca(){
		$status 		= $_POST['status'];
		$kd_pasien 		= $_POST['kd_pasien'];
		$kd_unit 		= $_POST['kd_unit'];
		$tgl_masuk 		= $_POST['tgl_masuk'];
		$urut_masuk 	= $_POST['urut_masuk'];
		
		$criteria = array(
			'kd_pasien'  	=> $kd_pasien,
			'kd_unit'  		=> $kd_unit,
			'tgl_masuk'  	=> $tgl_masuk,
			'urut_masuk'  	=> $urut_masuk,
		);
		
		$params = array(
			'flag_baca' 	=> $status
		);
		
		$this->db->where($criteria);
		$update = $this->db->update("rad_hasil", $params);
		if($update  > 0){
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		
		echo json_encode($response);
	}

	public function getStatusBaca(){
		$kd_pasien 		= $_POST['kd_pasien'];
		$kd_unit 		= $_POST['kd_unit'];
		$tgl_masuk 		= $_POST['tgl_masuk'];
		$urut_masuk 	= $_POST['urut_masuk'];
		
		$get_status = $this->db->query("
			select distinct(flag_baca)
			from rad_hasil
			where
				kd_pasien	='".$kd_pasien."' and
				kd_unit		='".$kd_unit."' and
				tgl_masuk 	='".$tgl_masuk."' and
				urut_masuk	='".$urut_masuk."'
		")->row();
		
		echo $get_status->flag_baca;
	}
}




