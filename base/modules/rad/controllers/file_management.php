<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class file_management extends MX_Controller {
	protected $path_upload;
	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');

			$link = glob($_SERVER["DOCUMENT_ROOT"]);
			$this->path_upload = $link[0].'/../../../';
	}	 

	public function index(){
		$this->load->view('main/index');            		
	}

	public function gallery($data = null){
		$path = "./Img Asset/RAD";
		$data_pasien = explode("_", $data);
		
	// chdir('/var/www/');
// echo realpath($this->path_upload) . PHP_EOL;

// echo realpath('/tmp/') . PHP_EOL;die;
		// $path_ = $this->db->query("SELECT * FROM sys_setting where key_data = 'rad_path_hasil'");
		// if ($path_->num_rows() > 0) {
			// $config['upload_path'] = $this->path_upload.$path_->row()->setting;
		// }else{
			// $config['upload_path'] = $path;
		// }
		$transaksi = $this->db->query("SELECT t.no_transaksi, t.tgl_transaksi, t.kd_unit, t.kd_pasien FROM 
				transaksi t 
			INNER JOIN
			unit u ON u.kd_unit = t.kd_unit 
		WHERE t.kd_pasien = '".$data_pasien[0]."' and t.no_transaksi = '".$data_pasien[1]."' AND t.no_transaksi is not null");

		if ($transaksi->num_rows() > 0) {
			$files = $this->db->query("SELECT * from rad_document WHERE file_name like '".$transaksi->row()->kd_pasien."%' and file_name like '%_".$transaksi->row()->no_transaksi."_%' ");
			if ($files->num_rows() == 0) {
				$files = false;
			}
		}else{
			$files = false;
		}
		$path_ = $this->db->query("SELECT * FROM sys_setting where key_data = 'rad_path_hasil'");

		if ($path_->num_rows() > 0) {
			$path = realpath($this->path_upload.$path_->row()->setting). PHP_EOL;
		}

		$this->load->view('rad/gallery', 
			array(
				'files' 	=> $files,
				'path'  	=> $path,
				'medrec'  	=> $data_pasien[0],
			)
		);
	}



	public function upload_hasil(){
		$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|bmp|rar';
		$path = "./Img Asset/RAD";
		// $config['max_size']      = '3000';
		// $config['upload_path']   = 'C:\Users\AlGhazali\Downloads';
		// $path = $this->db->query("SELECT file_location FROM zusers WHERE kd_user = '".$this->session->userdata['user_id']['id']."'")->row()->file_location;
		$path_ = $this->db->query("SELECT * FROM sys_setting where key_data = 'rad_path_hasil'");
		if ($path_->num_rows() > 0) {
			$config['upload_path'] = $this->path_upload.$path_->row()->setting;
		}else{
			$config['upload_path'] = $path;
		}
		
		// $link = glob($_SERVER["DOCUMENT_ROOT"]);
		// echo $link[0];
		// echo realpath(dirname(__FILE__)."/../");
		// var_dump($_SERVER);
		// die;
		// var_dump($config);
		$data_pasien = explode("_", $this->input->post('file_name'));

		$transaksi = $this->db->query("SELECT t.no_transaksi, t.tgl_transaksi, t.kd_unit FROM 
				transaksi t 
			INNER JOIN
			unit u ON u.kd_unit = t.kd_unit 
		WHERE t.kd_pasien = '".$data_pasien[0]."' and t.no_transaksi = '".$data_pasien[1]."'  and t.no_transaksi is not null");
		
		$response = array();
		if ($transaksi->num_rows() > 0) {

			$path_parts = pathinfo($_FILES["file"]["name"]);
			$extension = $path_parts['extension'];

			// echo $_FILES["file"]["size"];die();
			$name_file = $data_pasien[0]."_".$transaksi->row()->no_transaksi."_".time();
			$config['file_name'] = $name_file.".".$extension;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
	
			if ( ! $this->upload->do_upload('file')){
				$response['status'] = false;
				$response['error']  = $this->upload->display_errors();
				$response['laman']  = $config['upload_path'];
			}else{
				$paramsInsert = array(
					'file_name' 	=> $name_file,
					'extension' 	=> $extension,
					'path' 			=> $config['upload_path'],
					'size' 	 		=> round(((int)$_FILES["file"]["size"]/1028), 1),
					'date_upload' 	=> date('Y-m-d'),
					'catatan' 		=> $this->input->post('catatan'),
					// 'no_transaksi' 	=> $transaksi->row()->no_transaksi,
				);

				$this->db->insert('rad_document', $paramsInsert);
				$response['status'] = true;
				$response['path'] 	= $config['upload_path'];
			}
		}else{
			$response['status'] = false;
		}
		echo json_encode($response);
	}
	public function delete_image($medrec, $no_transaksi, $file_name){
		$path = "./Img Asset/RAD";
		$this->db->select("*");
		$this->db->where( array( 'file_name' => $file_name, ) );
		$this->db->from("rad_document"); 
		$query = $this->db->get();

		$path_ = $this->db->query("SELECT * FROM sys_setting where key_data = 'rad_path_hasil'");

		if ($path_->num_rows() > 0) {
			$path = $this->path_upload.$path_->row()->setting;
		}

		$file = $path."/".$query->row()->file_name.".".$query->row()->extension;
		if (file_exists($file)) {
			unlink($file);
			$this->db->where( array( 'file_name' => $file_name, ) );
			$this->db->delete('rad_document');
		}
		$files = $this->db->query("SELECT * from rad_document WHERE file_name like '".$medrec."%' and file_name like '%_".$no_transaksi."_%' ");
		if ($files->num_rows() == 0) {
			$files = false;
		}
		$this->load->view('rad/gallery', 
			array(
				'files'     => $files,
				'path'  	=> $path,
				'medrec'  	=> $medrec,
			)
		);
	}

	public function cetak_image($path, $file){
		$html = "";
		$html .= "<br>";
		$html .= "<hr>";
		$c_path = str_replace('~', '/', $path);
		$c_file = str_replace('~', '/', $file);
		if (file_exists($c_path . $c_file))
		{
			$tmp_file = $c_path . $c_file;
			// header("Content-Type:image/jpeg");
			// header("Content-Length: ".filesize($file));
			// echo $file;
			$imageData = base64_encode(file_get_contents($tmp_file));

			// Format the image SRC:  data:{mime};base64,{data};
			$img = 'data: '.mime_content_type($tmp_file).';base64,'.$imageData;
		}
		$html .= '<img src="'.$img.'" height="99%"/>'; 
		// echo  $path." - ".$file;
		$this->common->setPdf('P', 'Document Rad '.date('d-M-Y'), $html);
	}
}
?>