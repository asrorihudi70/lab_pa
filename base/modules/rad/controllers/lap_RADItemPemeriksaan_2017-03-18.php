<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RADItemPemeriksaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN ITEM PEMERIKSAAN RADIOLOGI';
		$param=json_decode($_POST['data']);
		$_kduser = $this->session->userdata['user_id']['id'];
		$cari_kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$kd_unit = stristr($cari_kd_unit,"'5");
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdCust="";
		$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdCust .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdCust = substr($tmpKdCust, 0, -1);
		
		$kriteria_cust = " AND CUS.KD_CUSTOMER IN(".$tmpKdCust.") ";
   		
		//jenis pasien
		
		
		//kd_customer
		
		
		//jenis pasien
		$unit_rad = $param->asal_pasien;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == -1){
			$crtiteriaUnitRad="T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 0){
			$crtiteriaUnitRad="T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 2){
			$crtiteriaUnitRad="T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		/* if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		} */
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT distinct PRO.KD_PRODUK, PRO.DESKRIPSI
				FROM TRANSAKSI T
					INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
					INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
					INNER JOIN (
						SELECT KD_KASIR, NO_TRANSAKSI 
						FROM DETAIL_BAYAR DB 
						WHERE KD_KASIR IN ('04', '09', '10', '14', '15', '16', '19', '20', '33') GROUP BY KD_KASIR, NO_TRANSAKSI
						) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
					INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK
					INNER JOIN PASIEN PAS ON PAS.KD_PASIEN=T.KD_PASIEN
					INNER JOIN UNIT U ON U.KD_UNIT=T.KD_UNIT
					INNER JOIN CUSTOMER CUS ON CUS.KD_CUSTOMER=KUN.KD_CUSTOMER 
				WHERE ".$crtiteriaUnitRad." ".$kriteria_cust."
				and (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."')  
				GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI, PAS.KD_PASIEN, PAS.NAMA, CUS.CUSTOMER, DT.QTY
					 ORDER BY PRO.DESKRIPSI ASC
                "
		);
		
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();				
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$nama_unit_rad.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		
		$all_total_grandtotal = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">No Medrec</th>
					<th width="13%" align="center">Nama Pasien</th>
					<th width="13%" align="center">Kelompok Pasien</th>
					<th width="13%" align="center">Qty</th>
					
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="4" align="left">'.$line->deskripsi.'</th>
				</tr>
				';
				$queryBody = $this->db->query(
					"
					SELECT PRO.KD_PRODUK, PRO.DESKRIPSI, PAS.KD_PASIEN, PAS.NAMA, CUS.CUSTOMER, DT.QTY 
					FROM TRANSAKSI T
						INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
						INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
						INNER JOIN (
							SELECT KD_KASIR, NO_TRANSAKSI 
							FROM DETAIL_BAYAR DB 
							WHERE KD_KASIR IN ('04', '09', '10', '14', '15', '16', '19', '20', '33') GROUP BY KD_KASIR, NO_TRANSAKSI
							) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
						INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK
						INNER JOIN PASIEN PAS ON PAS.KD_PASIEN=T.KD_PASIEN
						INNER JOIN UNIT U ON U.KD_UNIT=T.KD_UNIT
						INNER JOIN CUSTOMER CUS ON CUS.KD_CUSTOMER=KUN.KD_CUSTOMER
						WHERE ".$crtiteriaUnitRad." ".$kriteria_cust." 
							and (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."')  
							and PRO.KD_PRODUK='".$line->kd_produk."'
							GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI, PAS.KD_PASIEN, PAS.NAMA, CUS.CUSTOMER, DT.QTY
							  ORDER BY PRO.DESKRIPSI, NAMA ASC
						"
				);
				$query2=$queryBody->result();
				$no2=0;
				$all_total_subtotal = 0;
				 foreach($query2 as $line2){
					 
						/* $all_total_jml_pasien += $line2->jml_pasien;
						$all_total_jml_produk += $line2->jml_pasien;
						$all_total_jasa_dok += $line2->c20;
						$all_total_jasa_perawat += $line2->c21;
						$all_total_indeks_tdk_langsung += $line2->c22;
						$all_total_ops_instalasi += $line2->c23;
						$all_total_ops_rs += $line2->c24;
						$all_total_ops_direksi += $line2->c25;
						$all_total_jasa_sarana += $line2->c30;*/
						$all_total_subtotal += $line2->qty; 
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line2->kd_pasien.' </td>
							<td width="" align="left">'.$line2->nama.' </td>
							<td width="" align="left">'.$line2->customer.' </td>
							<td width="" align="left">'.$line2->qty.' </td>
							
							</tr>
						';
					
					
				} 
				$html.='
						<tr> 
							
							<td width="" colspan="4"  align="Right"><b>Sub Total :</b></td>
							<td width="" align="left"><b>'.$all_total_subtotal.'</b></td>
							
							</tr>
						';
				$all_total_grandtotal += $all_total_subtotal;
			}
			
			$html.='
						<tr> 
							
							<td width="" colspan="4"  align="center"><b>Grand Total :</b></td>
							<td width="" align="left"><b>'.$all_total_grandtotal.'</b></td>
							
							</tr>
						';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Item_Pemeriksaan_Radiologi.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P','Lap. Item Pemeriksaan Radiologi',$html);	
		}
		echo $html;
   	}
	
}
?>