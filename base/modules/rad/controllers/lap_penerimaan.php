<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
	#LAPORAN PENERIMAAN PER JENIS PENERIMAAN
   	public function cetakLaporan1(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='LAPORAN PER PENERIMAAN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 1){
				$tipe= 'Perseorangan';
			}else if($kel_pas ==2){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'RD%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'RD%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}	
		/* if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19','09','15','20','10','16','33')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('09','15','20')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('10','16','33')";
			$nama_asal_pasien = 'IGD';
		} */
		
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad ==2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		/* $query = $this->db->query("Select py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
									
									
									
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 

										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5
															
								Group By py.kd_pay, py.Uraian")->result(); */
		$query = $this->db->query("Select py.Uraian, 
										case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
										case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
										case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
											From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
																INNER JOIN unit on unit.kd_unit=t.kd_unit  
																INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
										Where 
										$q_shift
										$kriteria_bayar
										".$crtiteriaUnitRad."
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										 
										and unit.kd_bagian=5  
										Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result(); 
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		// echo $html;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('any name you want'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_per_jenis_penerimaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','LAPORAN PER PENERIMAAN',$html);	
		}
		
		//$html.='</table>';
		
   	}
	
	#LAPORAN PENERIMAAN PER PASIEN
	public function cetakLaporan2(){
		//echo "dddd";
		//die;
		
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'RD%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'RD%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}	
		
		/* if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19','09','15','20','10','16','33')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('09','15','20')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('10','16','33')";
			$nama_asal_pasien = 'IGD';
		} */
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad == 2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		
		// echo ("tesss") ;
		//echo $param->shift21 ;
		// echo var_dump($param);
		// die ;
		
		$q_shift='';
   		$q_shift2='';
   		$q_shift3='';
   		$t_shift='';
   		$t_shift2='';
   		$t_shift3='';
		$q_waktu='';
		$dt1 		= date_create( date('Y-m-d', strtotime($param->start_date)));
		$dt2 		= date_create( date('Y-m-d', strtotime($param->last_date)));
		$date_diff 	= date_diff($dt1,$dt2);
		$range 		=  $date_diff->format("%a");
		
		
		// && 	$param->shift20 == '' && $param->shift21=='' && $param->shift22=='' && $param->shift23==''

		if( $param->shift0 == '' && $param->shift1=='' && $param->shift2=='' && $param->shift3=='' 
			 
		)
		{
			$q_waktu=" ( db.tgl_transaksi between '".$param->start_date."' and  '".$param->last_date."' )";
		}else{
			#GRUP COMBO 1
			if($param->shift0=='true'){
				$q_shift=	" 
								(
									(
										db.tgl_transaksi = '".$param->start_date."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										db.tgl_transaksi = '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift='SHIFT (1,2,3)';
			}else{
				if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
					$s_shift='';
					if($param->shift1=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift3=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift.=" db.tgl_transaksi = '".$param->start_date."'   And db.Shift In (".$s_shift.")";
					if($param->shift3=='true'){
						$q_shift="(".$q_shift." Or  (db.tgl_transaksi= '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift ='SHIFT ('.$s_shift.')';
					$q_shift =$q_shift;
				}
			} 
			
			#GRUP COMBO 2
			// if($param->shift20=='true'){
			// 	$q_shift2=	" 
			// 					(
			// 						(
			// 							db.tgl_transaksi = '".$param->last_date."' And db.Shift In (1,2,3)
			// 						)     
			// 						Or  
			// 						(
			// 							db.tgl_transaksi = '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) 
			// 					) 

			// 				";
			// 	$t_shift2='SHIFT (1,2,3)';
			// }else{
			// 	if($param->shift21=='true' || $param->shift22=='true' || $param->shift23=='true'){
			// 			$s_shift='';
			// 		if($param->shift21=='true'){
			// 			if($s_shift!='')$s_shift.=',';
			// 			$s_shift.='1';
			// 		}
			// 		if($param->shift22=='true'){
			// 			if($s_shift!='')$s_shift.=',';
			// 			$s_shift.='2';
			// 		}
			// 		if($param->shift23=='true'){
			// 			if($s_shift!='')$s_shift.=',';
			// 			$s_shift.='3';
			// 		}
			// 		$q_shift2.=" db.tgl_transaksi = '".$param->last_date."'   And db.Shift In (".$s_shift.")";
			// 		if($param->shift23=='true'){
			// 			$q_shift2="(".$q_shift2." Or  (db.tgl_transaksi= '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
			// 		}
			// 		$t_shift2 ='SHIFT ('.$s_shift.')';
			// 		$q_shift2 =$q_shift2;
			// 	}
			// }
			
				# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			if($range > 1){
				$q_shift3=	" OR (
								(
									(
										db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' -1 day'))."'  And db.Shift In (1,2,3)
									)     
									Or  
									(
										db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +2 day'))."'  And  '".$param->last_date."'  And db.Shift=4) 
									) 
								)
							";		
			}
			//$q_waktu=" ((".$q_shift.") ORrrrrrr ((".$q_shift2.")) ".$q_shift3." )  ";
			//$q_waktu=" ((".$q_shift.") OR ((".$q_shift2.")) ".$q_shift3." )  ";
			$q_waktu=" ((".$q_shift.")  ".$q_shift3." )  ";
			
		}
		
		$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
									
									
									
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 

										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_waktu
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5
															
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									
								
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.'  '.$t_shift.' s/d '.$tgl_akhir.' '.$t_shift2.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.="
			<table border='1' cellpadding='3' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
			<thead>
				 <tr>
					<th width='10'  align='center'>No</th>
					<th width='100'  align='center'>No. Trans</th>
					<th width='100'  align='center'>No. Medrec</th>
					<th width='190'  align='center'>Nama Pasien</th>
					<th  width='100' align='center'>Jenis Penerimaan</th>
					<th  width='100' align='center'>Jumlah Total</th>
				  </tr>
			</thead>";
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			$grand_total=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center" width="5"><b>'.$no.'</b></td>
								<td><b>'.$line->no_transaksi.'</b></td>
								<td><b>'.$line->kd_pasien.'</b></td>
								<td><b>'.$line->nama.'</b></td>
								<td><b>'.$line->uraian.'</b></td>
								<td width="" align="right"></td>
							</tr>';
				$queryIsi = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
									,P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
									
									
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer 

										INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
										INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
										
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_waktu
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5
												and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									
								,P.KD_PRODUK, P.DESKRIPSI, DT.QTY
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
					$sub_total=0;
					foreach ($queryIsi as $line2) 
					{
					//number_format($line2->harga,0,'.',',')
						$html.="<tr>
								<td align='center'></td>
								<td colspan='4'>".$line2->deskripsi." (".$line2->qty.")</td>
								<td align='right'>".number_format($line2->harga * $line2->qty,0, "." , ",")."</td>
							</tr>";
						$sub_total += $line2->harga * $line2->qty;
					}
					$html.='<tr>
								<td align="right" colspan="5"><b>Sub Total</b></td>
								<td align="right">'.number_format($sub_total,0, "." , ",").'</td>
							</tr>';
					$grand_total += $sub_total;
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($grand_total,0, "." , ",").'</td>
					</tr>';
			/* $html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_ut,0, "." , ",").'</b></td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_pt,0, "." , ",").'</b></td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right"><b>'.number_format($total_ssd,0, "." , ",").'</b></td>
					</tr>'; */
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienRAD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_radiologi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            
            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		}
		
		 //echo $html;
		
		//$html.='</table>';
   	}

	#LAPORAN PENERIMAAN PER ASAL Pasien
	public function getUnit(){
		$result=null;
		$list = array();
		if(!empty($_POST['unit'])){
			if($_POST['unit'] == 'Semua'){
				$kd_bagian = " WHERE Kd_Bagian in ('1','2','3') ";
			} else if($_POST['unit'] == 'RWJ'){
				$kd_bagian = " WHERE Kd_Bagian in ('2') ";
			}  else if($_POST['unit'] == 'RWI'){
				$kd_bagian = " WHERE Kd_Bagian in ('1') ";
			} else if($_POST['unit'] == 'IGD'){
				$kd_bagian = " WHERE Kd_Bagian in ('3') ";
			} else{				
				$kd_bagian = " WHERE Kd_Bagian in ('0000') ";
			}
			$result=$this->db->query("SELECT kd_unit,nama_unit FROM UNIT ".$kd_bagian." ")->result();
			
		} else{
			$result=$this->db->query("SELECT kd_unit,nama_unit FROM UNIT WHERE Kd_Bagian in ('1','2','3') ")->result();
		}
		for($i=0;$i<count($result);$i++){
			$list[$i]['KD_UNIT'] = $result[$i]->kd_unit;
			$list[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
		}
		
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($list).'}';
	}
	
	public function cetakPenerimaanPerAsalPasien(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Penerimaan Per Asal Pasien Radiologi';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		# Kriteria payment
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		# jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		# kd_customer
		if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d =='' || $kel_pas_d  == 'Semua'){
				$customerx=" ";
				$customer ='Semua';
		
			}else{
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
				
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		# jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		$querynonorlangsung = "LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
								left JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
								left join unit un on un.kd_unit=tr.kd_unit";
		$fieldnonorlangsung = "distinct tr.kd_unit,un.nama_unit as nama_unit_asal_pasien,'non langsung' as ketkunjungan";
		$groupbynonorlangsunghead = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,tr.kd_unit,t.kd_unit,un.nama_unit,unit.nama_unit";
		$groupbynonorlangsungbody = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,P.KD_PRODUK, P.DESKRIPSI, DT.QTY,tr.kd_unit,un.nama_unit";
		
		if($asal_pasien == 0 ){
			$crtiteriaAsalPasien="and  t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."', '07', '03', '08')";
			$nama_asal_pasien = 'Semua Pasien';
			
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')  and t.kd_pasien not like 'RD%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		} else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		} else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwj."'  and t.kd_pasien like 'RD%'";
			$nama_asal_pasien = 'kunjungan Langsung';
			$querynonorlangsung = "";
			$fieldnonorlangsung = "distinct t.kd_unit,unit.nama_unit as nama_unit_asal_pasien,'langsung' as ketkunjungan";
			$groupbynonorlangsunghead = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,t.kd_unit,unit.nama_unit";
			$groupbynonorlangsungbody = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,P.KD_PRODUK, P.DESKRIPSI, DT.QTY,t.kd_unit,unit.nama_unit";
		}
		
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad == 2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		
		# shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		# Kriteria Unit Asal Pasien
		$tmpKdUnit="";
		$kriteria_unit = "";
		$arrayDataUnit = $param->tmp_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		if(!empty($tmpKdUnit)){			
			$kriteria_unit = " And (tr.kd_unit in (".$tmpKdUnit.")) ";
		} 
		
		
		
		$queryhead = $this->db->query("Select $fieldnonorlangsung
										From Transaksi t 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer 
												INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
												INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
												$querynonorlangsung
										Where  
											$q_shift
											$kriteria_bayar
											".$crtiteriaUnitRad."
											$crtiteriaAsalPasien
											$jniscus
											$customerx
											and unit.kd_bagian=5	
											$kriteria_unit	
									order by nama_unit_asal_pasien asc
								")->result();
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.="
			<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 12;'>
			<thead>
				 <tr>
					<th width='50'  align='center'>No</th>
					<th width='100'  align='center'>No. Trans</th>
					<th width='100'  align='center'>Tgl. Trans</th>
					<th width='100'  align='center'>No. Medrec</th>
					<th width='100'  align='center'>Nama Pasien</th>
					<th width='100' align='center'>Jenis Penerimaan</th>
					<th width='100' align='center'>Jumlah Total</th>
				  </tr>
			</thead>";
		if(count($queryhead) > 0) {
			
			foreach ($queryhead as $linehead) 
			{
				$html.='<tr>
							<th align="center"  bgcolor="#81b8c4" colspan="7">'.str_replace("&","dan",$linehead->nama_unit_asal_pasien).'</th>
						</tr>';
				if($linehead->ketkunjungan == 'langsung'){
					$ckd_unit = "and t.kd_unit='".$linehead->kd_unit."' ";
				} else{					
					$ckd_unit = "and tr.kd_unit='".$linehead->kd_unit."'";
				}
				$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
											case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
											case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
											case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
											From Transaksi t 
													INNER JOIN unit on unit.kd_unit=t.kd_unit  
													INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
													INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
													INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
													INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
													INNER JOIN Customer c on c.kd_customer=k.kd_customer 
													INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
													INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
													$querynonorlangsung
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5	
												$ckd_unit
											$groupbynonorlangsunghead
											Order By ps.Nama, db.No_Transaksi, py.Uraian
									")->result();
				$grand_total=0;
				$no=0;
				$total_ut=0;
				$total_pt=0;
				$total_ssd=0;
				$total_jumlah=0;
				foreach ($query as $line) 
				{	
					$jumlah = $line->ut + $line->pt + $line->ssd;
					$no++;
					$html.='<tr>
								<td align="center"><b>'.$no.'</b></td>
								<td><b>'.$line->no_transaksi.'</b></td>
								<td><b>'.tanggalstring($line->tgl_transaksi).'</b></td>
								<td><b>'.$line->kd_pasien.'</b></td>
								<td><b>'.$line->nama.'</b></td>
								<td align="left"><b>'.$line->uraian.'</b></td>
								<td width="" align="left"></td>
							</tr>';
					$queryIsi = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
													case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
													case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
													case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT,
													P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
														From Transaksi t 
															INNER JOIN unit on unit.kd_unit=t.kd_unit  
															INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
															INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
															INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
															INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
															INNER JOIN Customer c on c.kd_customer=k.kd_customer 
															INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
															INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer
															$querynonorlangsung
														Where  
															$q_shift
															$kriteria_bayar
															".$crtiteriaUnitRad."
															$crtiteriaAsalPasien
															$jniscus
															$customerx
															and unit.kd_bagian=5
															$ckd_unit
															and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
													$groupbynonorlangsungbody
													Order By ps.Nama, db.No_Transaksi, py.Uraian
									")->result();
						$sub_total=0;
						foreach ($queryIsi as $line2) 
						{
							$html.="<tr>
									<td align='center'></td>
									<td></td>
									<td></td>
									<td>".$line2->deskripsi."</td>
									<td>".$line2->qty."</td>
									<td></td>
									<td align='right'>".number_format($line2->harga * $line2->qty,0, "." , ",")."</td>
								</tr>";
							$sub_total += $line2->harga * $line2->qty;
						}
						$html.='<tr>
									<th align="left" colspan="6">Sub Total</th>
									<th align="right">'.number_format($sub_total,0, "." , ",").'</th>
								</tr>';
						$grand_total += $sub_total;
					if($line->ut != 0)
					{
						$total_ut = $total_ut + $line->ut;
					}else if($line->pt != 0)
					{
						$total_pt = $total_pt + $line->pt;
					}else if($line->ssd != 0)
					{
						$total_ssd = $total_ssd + $line->ssd;
					}
					
					$total_jumlah = $total_jumlah + $jumlah;
				}
				$html.='<tr>
							<th width="" align="left" colspan="6" bgcolor="#c0c7d6">GRAND TOTAL : &nbsp;</th>
							<th width="" align="right"  bgcolor="#c0c7d6">'.number_format($grand_total,0, "." , ",").'</th>
						</tr>';
				$querytotal = $this->db->query("Select py.Uraian, sum(DT.HARGA*DT.QTY) as harga
													From Transaksi t 
															INNER JOIN unit on unit.kd_unit=t.kd_unit  
															INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
															INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
															INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
															INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
															INNER JOIN Customer c on c.kd_customer=k.kd_customer 
															INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
															INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
															$querynonorlangsung
													Where  
														$q_shift
														$kriteria_bayar
														".$crtiteriaUnitRad."
														$crtiteriaAsalPasien
														$jniscus
														$customerx
														and unit.kd_bagian=5	
														$ckd_unit	
													Group By py.Uraian
													Order by py.Uraian ")->result();
				foreach ($querytotal as $linetotal)
				{
					$html.='<tr>
							<th width="" align="left" colspan="6" bgcolor="#c0c7d6">'.$linetotal->uraian.' &nbsp;</th>
							<th width="" align="right"  bgcolor="#c0c7d6">'.number_format($linetotal->harga,0, "." , ",").'</th>
						</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienRAD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_radiologi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            
            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Laporan Penerimaan Per Asal Pasien Radiologi',$html);	
		}
	}
	
	public function directPenerimaanPerAsalPasien(){
		ini_set('display_errors', '1');
		$common=$this->common;
   		$result=$this->result;
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Penerimaan Per Asal Pasien Radiologi';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		# Kriteria payment
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
		
		# Kriteria Unit Asal Pasien
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$kriteria_unit = " And (tr.kd_unit in (".$tmpKdUnit.")) ";
   		
		# jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		# kd_customer
		if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d =='' || $kel_pas_d  == 'Semua'){
				$customerx=" ";
				$customer ='Semua';
		
			}else{
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
				
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		# jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		$querynonorlangsung = "LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
								left JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
								left join unit un on un.kd_unit=tr.kd_unit";
		$fieldnonorlangsung = "distinct tr.kd_unit,un.nama_unit as nama_unit_asal_pasien,'non langsung' as ketkunjungan";
		$groupbynonorlangsunghead = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,tr.kd_unit,t.kd_unit,un.nama_unit,unit.nama_unit";
		$groupbynonorlangsungbody = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,P.KD_PRODUK, P.DESKRIPSI, DT.QTY,tr.kd_unit,un.nama_unit";
		
		if($asal_pasien == 0 ){
			$crtiteriaAsalPasien="and  t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."', '07', '03', '08')";
			$nama_asal_pasien = 'Semua Pasien';
			
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')  and t.kd_pasien not like 'RD%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		} else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		} else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwj."'  and t.kd_pasien like 'RD%'";
			$nama_asal_pasien = 'kunjungan Langsung';
			$querynonorlangsung = "";
			$fieldnonorlangsung = "distinct t.kd_unit,unit.nama_unit as nama_unit_asal_pasien,'langsung' as ketkunjungan";
			$groupbynonorlangsunghead = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,t.kd_unit,unit.nama_unit";
			$groupbynonorlangsungbody = "Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,P.KD_PRODUK, P.DESKRIPSI, DT.QTY,t.kd_unit,unit.nama_unit";
		}
		
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad == 2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		
		# shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		# Create Data
		$tp = new TableText(145,13,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 4)
			->setColumnLength(1, 10)
			->setColumnLength(2, 18)
			->setColumnLength(3, 23)
			->setColumnLength(4, 28)
			->setColumnLength(5, 17)
			->setColumnLength(6, 16)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 8,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 8,"left")
			->commit("header")
			->addColumn($telp, 8,"left")
			->commit("header")
			->addColumn($fax, 8,"left")
			->commit("header")
			->addColumn("LAPORAN PENERIMAAN PER ASAL PASIEN RADIOLOGI", 8,"center")
			->commit("header")
			->addColumn($tgl_awal." s/d ".$tgl_akhir, 8,"center")
			->commit("header")
			->addColumn($t_shift, 8,"center")
			->commit("header")
			->addColumn("Kelompok ".$tipe." - ".$nama_asal_pasien." (".$customer.")", 8,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Trans", 1,"left")
			->addColumn("Tgl. Trans", 1,"left")
			->addColumn("No. Medrec", 1,"left")
			->addColumn("Nama Pasien", 1,"left")
			->addColumn("Jenis Penerimaan", 1,"left")
			->addColumn("Jumlah Total", 1,"right")
			->commit("header");
		$tp	->addColumn("--------------------------------------------------------------------------------------------------------------------------", 7,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->commit("header");
		
		$queryhead = $this->db->query("Select $fieldnonorlangsung
										From Transaksi t 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer 
												INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
												INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
												$querynonorlangsung
										Where  
											$q_shift
											$kriteria_bayar
											".$crtiteriaUnitRad."
											$crtiteriaAsalPasien
											$jniscus
											$customerx
											and unit.kd_bagian=5	
											$kriteria_unit	
									order by nama_unit_asal_pasien asc")->result();
		if(count($queryhead) > 0) {
			
			foreach ($queryhead as $linehead) 
			{
				$tp	->addColumn($linehead->nama_unit_asal_pasien, 7,"center")
					->commit("header");
				if($linehead->ketkunjungan == 'langsung'){
					$ckd_unit = "and t.kd_unit='".$linehead->kd_unit."' ";
				} else{					
					$ckd_unit = "and tr.kd_unit='".$linehead->kd_unit."'";
				}
				$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
											case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
											case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
											case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
											From Transaksi t 
													INNER JOIN unit on unit.kd_unit=t.kd_unit  
													INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
													INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
													INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
													INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
													INNER JOIN Customer c on c.kd_customer=k.kd_customer 
													INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
													INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
													$querynonorlangsung
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5	
												$ckd_unit
											$groupbynonorlangsunghead
											Order By ps.Nama, db.No_Transaksi, py.Uraian
									")->result();
				$grand_total=0;
				$no=0;
				$total_ut=0;
				$total_pt=0;
				$total_ssd=0;
				$total_jumlah=0;
				foreach ($query as $line) 
				{	
					$jumlah = $line->ut + $line->pt + $line->ssd;
					$no++;
					$tp	->addColumn($no, 1,"center")
						->addColumn($line->no_transaksi, 1,"left")
						->addColumn(tanggalstring($line->tgl_transaksi), 1,"left")
						->addColumn($line->kd_pasien, 1,"left")
						->addColumn($line->nama, 1,"left")
						->addColumn($line->uraian, 1,"left")
						->addColumn('', 1,"left")
						->commit("header");
					$queryIsi = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
													case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
													case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
													case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT,
													P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
														From Transaksi t 
															INNER JOIN unit on unit.kd_unit=t.kd_unit  
															INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
															INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
															INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
															INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
															INNER JOIN Customer c on c.kd_customer=k.kd_customer 
															INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
															INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer
															$querynonorlangsung
														Where  
															$q_shift
															$kriteria_bayar
															".$crtiteriaUnitRad."
															$crtiteriaAsalPasien
															$jniscus
															$customerx
															and unit.kd_bagian=5
															$ckd_unit
															and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
													$groupbynonorlangsungbody
													Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
					$sub_total=0;	
					foreach ($queryIsi as $line2) 
					{
						$tp	->addColumn('', 1,"left")
							->addColumn('', 1,"left")
							->addColumn('', 1,"left")
							->addColumn($line2->deskripsi, 1,"left")
							->addColumn($line2->qty, 1,"left")
							->addColumn('', 1,"left")
							->addColumn(number_format($line2->harga * $line2->qty,0, "." , ","), 1,"right")
							->commit("header");
						$sub_total += $line2->harga * $line2->qty;
					}
					$tp	->addColumn("Sub Total", 6,"left")
							->addColumn(number_format($sub_total,0, "." , ","), 1,"right")
							->commit("header");
					$grand_total += $sub_total;
					if($line->ut != 0)
					{
						$total_ut = $total_ut + $line->ut;
					}else if($line->pt != 0)
					{
						$total_pt = $total_pt + $line->pt;
					}else if($line->ssd != 0)
					{
						$total_ssd = $total_ssd + $line->ssd;
					}
					
					$total_jumlah = $total_jumlah + $jumlah;
				}
				$tp	->addColumn("GRAND TOTAL : ", 6,"left")
					->addColumn(number_format($grand_total,0, "." , ","), 1,"right")
					->commit("header");
				$querytotal = $this->db->query("Select py.Uraian, sum(DT.HARGA*DT.QTY) as harga
													From Transaksi t 
															INNER JOIN unit on unit.kd_unit=t.kd_unit  
															INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
															INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
															INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
															INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
															INNER JOIN Customer c on c.kd_customer=k.kd_customer 
															INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
															INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
															$querynonorlangsung
													Where  
														$q_shift
														$kriteria_bayar
														".$crtiteriaUnitRad."
														$crtiteriaAsalPasien
														$jniscus
														$customerx
														and unit.kd_bagian=5	
														$ckd_unit	
													Group By py.Uraian
													Order by py.Uraian ")->result();
				foreach ($querytotal as $linetotal)
				{
					$tp	->addColumn($linetotal->uraian, 6,"left")
						->addColumn(number_format($linetotal->harga,0, "." , ","), 1,"right")
						->commit("header");
				}
			}
		} else{
			$tp	->addColumn("Data tidak ada", 7,"left")
				->commit("header");
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_penerimaan_perasal_pasien_rad.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
	}
	
	
	
	public function cetakPenerimaanPerAsalPasien_last(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Penerimaan Per Asal Pasien Radiologi';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		# Kriteria payment
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
		
		# Kriteria Unit Asal Pasien
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$kriteria_unit = " And (tr.kd_unit in (".$tmpKdUnit.")) ";
   		
		# jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			
			if($kel_pas == 1){
				$jniscus=" and  Ktr.Jenis_cust=0 ";
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$jniscus=" and  Ktr.Jenis_cust=1 ";
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$jniscus=" and  Ktr.Jenis_cust=2 ";
				$tipe= 'Asuransi';
			}
		}
		
		# kd_customer
		if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d =='' || $kel_pas_d  == 'Semua'){
				$customerx=" ";
				$customer ='Semua';
		
			}else{
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
				
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		# jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 0 ){
			$crtiteriaAsalPasien="and  t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."', '07', '03', '08')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')  and t.kd_pasien not like 'RD%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		} else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		} else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwj."'  and t.kd_pasien like 'RD%'";
			$nama_asal_pasien = 'kunjungan Langsung';
		}
		
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad == 2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		
		# shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$queryhead = $this->db->query("Select distinct tr.kd_unit,un.nama_unit as nama_unit_asal_pasien
									From Transaksi t 
											LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
											INNER JOIN unit on unit.kd_unit=t.kd_unit  
											INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer 
											INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
											INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											INNER JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
											inner join unit un on un.kd_unit=tr.kd_unit
									Where  
										$q_shift
										$kriteria_bayar
										".$crtiteriaUnitRad."
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										and unit.kd_bagian=5	
										$kriteria_unit	
								Order By un.nama_unit asc")->result();
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.="
			<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
			<thead>
				 <tr>
					<th width='50'  align='center'>No</th>
					<th width='100'  align='center'>No. Trans</th>
					<th width='100'  align='center'>Tgl. Trans</th>
					<th width='100'  align='center'>No. Medrec</th>
					<th width='100'  align='center'>Nama Pasien</th>
					<th  width='100' align='center'>Jenis Penerimaan</th>
					<th  width='100' align='center'>Jumlah Total</th>
				  </tr>
			</thead>";
		if(count($queryhead) > 0) {
			
			foreach ($queryhead as $linehead) 
			{
				$html.='<tr>
							<th align="center"  bgcolor="#81b8c4" colspan="7">'.str_replace("&","dan",$linehead->nama_unit_asal_pasien).'</th>
						</tr>';
				$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
									From Transaksi t 
											LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
											INNER JOIN unit on unit.kd_unit=t.kd_unit  
											INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer 
											INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
											INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											INNER JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
									Where  
										$q_shift
										$kriteria_bayar
										".$crtiteriaUnitRad."
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										and unit.kd_bagian=5	
										and tr.kd_unit='".$linehead->kd_unit."'	
									Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
								Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
				$grand_total=0;
				$no=0;
				$total_ut=0;
				$total_pt=0;
				$total_ssd=0;
				$total_jumlah=0;
				foreach ($query as $line) 
				{	
					$jumlah = $line->ut + $line->pt + $line->ssd;
					$no++;
					$html.='<tr>
								<td align="center"><b>'.$no.'</b></td>
								<td><b>'.$line->no_transaksi.'</b></td>
								<td><b>'.tanggalstring($line->tgl_transaksi).'</b></td>
								<td><b>'.$line->kd_pasien.'</b></td>
								<td><b>'.$line->nama.'</b></td>
								<td><b>'.$line->uraian.'</b></td>
								<td width="" align="right"></td>
							</tr>';
					$queryIsi = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
										case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
										case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
										case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT,
										P.KD_PRODUK, P.DESKRIPSI, DT.QTY, sum(DT.HARGA) as harga
											From Transaksi t 
												LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer 
												INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
												INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer
												INNER JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5
												and tr.kd_unit='".$linehead->kd_unit."'
												and t.kd_pasien='".$line->kd_pasien."' and db.No_Transaksi='".$line->no_transaksi."' and t.Tgl_Transaksi='".$line->tgl_transaksi."'
									Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama,P.KD_PRODUK, P.DESKRIPSI, DT.QTY
									Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
						$sub_total=0;
						foreach ($queryIsi as $line2) 
						{
							$html.="<tr>
									<td align='center'></td>
									<td></td>
									<td></td>
									<td>".$line2->deskripsi."</td>
									<td>".$line2->qty."</td>
									<td></td>
									<td align='right'>".number_format($line2->harga * $line2->qty,0, "." , ",")."</td>
								</tr>";
							$sub_total += $line2->harga * $line2->qty;
						}
						$html.='<tr>
									<th align="left" colspan="6">Sub Total</th>
									<th align="right">'.number_format($sub_total,0, "." , ",").'</th>
								</tr>';
						$grand_total += $sub_total;
					if($line->ut != 0)
					{
						$total_ut = $total_ut + $line->ut;
					}else if($line->pt != 0)
					{
						$total_pt = $total_pt + $line->pt;
					}else if($line->ssd != 0)
					{
						$total_ssd = $total_ssd + $line->ssd;
					}
					
					$total_jumlah = $total_jumlah + $jumlah;
				}
				$html.='<tr>
							<th width="" align="left" colspan="6" bgcolor="#c0c7d6">GRAND TOTAL : &nbsp;</th>
							<th width="" align="right"  bgcolor="#c0c7d6">'.number_format($grand_total,0, "." , ",").'</th>
						</tr>';
				$querytotal = $this->db->query("Select py.Uraian, DT.QTY, sum(DT.HARGA) as harga
									From Transaksi t 
											LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
											INNER JOIN unit on unit.kd_unit=t.kd_unit  
											INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer 
											INNER JOIN DETAIL_TRANSAKSI DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
											INNER JOIN PRODUK P ON P.KD_PRODUK=DT.KD_PRODUK
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											INNER JOIN TRANSAKSI tr on tr.kd_kasir=ua.kd_kasir_asal and tr.no_transaksi=ua.no_transaksi_asal
									Where  
										$q_shift
										$kriteria_bayar
										".$crtiteriaUnitRad."
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										and unit.kd_bagian=5	
										and tr.kd_unit='".$linehead->kd_unit."'	
									Group By py.Uraian, DT.QTY
								Order By py.Uraian")->result();
				foreach ($querytotal as $linetotal)
				{
					$html.='<tr>
							<th width="" align="left" colspan="6" bgcolor="#c0c7d6">'.$linetotal->uraian.' &nbsp;</th>
							<th width="" align="right"  bgcolor="#c0c7d6">'.number_format($linetotal->qty*$linetotal->harga,0, "." , ",").'</th>
						</tr>';
				}
			}
			
			// $html.='<tr>
						// <td width="" align="right" colspan="6"><b>Jumlah &nbsp;</b></td>
						// <td width="" align="right">'.number_format($grand_total,0, "." , ",").'</td>
					// </tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienRAD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_radiologi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            
            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Laporan Penerimaan Per Asal Pasien Radiologi',$html);	
		}
	}
	
	
	
	
}
?>