<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class laporan extends MX_Controller
{
    private $response = array();
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('main/index');
    }
    
    public function pemeriksaan_hasil(){
        $title = "Lap. Dokter Baca Hasil Per Pasien";
        $data = json_decode($this->input->post('data'));
        $criteriaDokter = "";
        if(strtolower($data->kd_dokter) == 'semua'){
            $query = "SELECT * FROM dokter_penunjang where kd_job = '".$data->kd_profesi."' and kd_unit = '".str_replace("'", "", $data->kd_unit)."'";
            $query = $this->db->query($query);
            if($query->num_rows() > 0){
                // $criteriaDokter = " AND (rh.kd_dokter )";
                foreach ($query->result() as $result) {
                    $criteriaDokter .= "'".$result->kd_dokter."',";
                }
                $criteriaDokter = substr($criteriaDokter, 0, -1);
                $criteriaDokter = "  AND rh.kd_dokter in (".$criteriaDokter.") ";
            }
        }else{
            $criteriaDokter = "  AND rh.kd_dokter = '".$data->kd_dokter."' ";
        }
        
        $criteriaCustomer = "";
        if(strtolower($data->kelompok) != 'semua'){
            /*
                $data->kelompok
                0 -> Perseorangan
                1 -> Perusaahaan
                2 -> Asuransi
             */
            $criteriaListCustomer = "";

            if ($data->kd_kelompok == '' ||  strtolower($data->kd_kelompok) == 'semua') {
                $query = "SELECT * FROM kontraktor where jenis_cust = '".$data->kelompok."'";
                $query = $this->db->query($query);
                if($query->num_rows() > 0){
                    foreach ($query->result() as $result) {
                        $criteriaListCustomer .= "'".$result->kd_customer."',";
                    }
                    $criteriaListCustomer = substr($criteriaListCustomer, 0, -1);
                }
            }else{
                $criteriaListCustomer = "'".$data->kd_kelompok."'";
            }
            $criteriaCustomer = " AND k.kd_customer in (".$criteriaListCustomer.")";
        }


        $query = "SELECT 
            rh.kd_pasien,
            rh.kd_unit,
            rh.tgl_masuk,
            rh.urut_masuk,
            CASE WHEN rh.kd_dokter is null THEN d_pengirim.nama ELSE d_hasil.nama END as dokter,
            CASE WHEN rh.tgl_baca is null THEN rh.tgl_masuk ELSE rh.tgl_baca END as tgl_baca,
            c.customer,
            k.no_foto_rad,
            p.nama
        FROM
            rad_hasil rh 
            INNER JOIN kunjungan k ON k.kd_pasien = rh.kd_pasien AND k.kd_unit = rh.kd_unit AND k.tgl_masuk = rh.tgl_masuk AND k.urut_masuk = rh.urut_masuk
            INNER JOIN pasien p ON p.kd_pasien = k.kd_pasien
            INNER JOIN customer c ON c.kd_customer = k.kd_customer
            INNER JOIN dokter d_pengirim ON d_pengirim.kd_dokter = k.kd_dokter
            INNER JOIN dokter d_hasil ON d_hasil.kd_dokter = rh.kd_dokter
        WHERE rh.tgl_masuk between '".$data->tglAwal."' AND '".$data->tglAkhir."' 
        $criteriaDokter
        $criteriaCustomer
        GROUP BY 
            rh.kd_pasien,
            rh.kd_unit,
            rh.tgl_masuk,
            rh.urut_masuk,
            dokter,
            tgl_baca,
            c.customer,
            k.no_foto_rad,
            p.nama
        ";
        $this->response['tgl_awal']	= date_format(date_create($data->tglAwal), 'd/M/Y');
        $this->response['tgl_akhir']= date_format(date_create($data->tglAkhir), 'd/M/Y');
        $this->response['title']= $title;
        $this->response['size'] = 210;
        $this->response['data'] = $this->db->query($query);
        $html = $this->load->view(
            'rad/laporan_pemeriksa_hasil',
            $this->response, true
        );

        if ($data->type_file == 0 || $data->type_file == '0') {
            $this->common->setPdf_penunjang('P', $title, $title, $html);    
        }
    }
}




