<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewpacslist_02 extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

   
	public function read($Params=null){
		try{   
			$this->load->model('rad/tbl_pacs_02');
			if (strlen($Params[4])!== 0){
				$this->db->where(str_replace("~", "'", $Params[4]), null, false) ;
				$res = $this->tbl_pacs_02->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$res = $this->tbl_pacs_02->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
		}
		catch(Exception $o){
			echo '{success: false}';
		}
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
    
	public function save($Params=null)
    {
		$tmpkdpasien = $Params["KdPasien"];
		$tmpkdunit = $Params["KdUnit"];
		$tmptgl = $Params["Tgl"];
		$tmpurutmasuk = $Params["UrutMasuk"];
		$tmpurut = $Params["urut"];
		$tmpkdtest = $Params["KdTest"];
		$tmphasil = $Params["Hasil"];
						
		$urut = $this->db->query("SELECT * From Rad_Hasil 
								where Kd_Pasien='".$tmpkdpasien."' and Kd_Unit='".$tmpkdunit."' and Tgl_masuk = ('".$tmptgl."') 
								and Urut_masuk = ".$tmpurutmasuk." and urut = ".$tmpurut."
								and Kd_Test = ".$tmpkdtest."");
		$result = $urut->result();
		foreach ($result as $data)
		{
			if (count($data) != "")
			{
				$query = $this->db->query("UPDATE Rad_Hasil SET HASIL = '".$tmphasil."' WHERE Kd_Pasien='".$tmpkdpasien."' and Kd_Unit='".$tmpkdunit."' and Tgl_masuk = ('".$tmptgl."') 
											and Urut_masuk = ".$tmpurutmasuk." and urut = ".$tmpurut."
											and Kd_Test = ".$tmpkdtest."");
			   
			}else
			{
				 $query = $this->db->query("INSERT INTO rad_hasil (kd_test,kd_pasien,kd_unit,tgl_masuk,urut_masuk,urut,hasil,keluhan) values"
																."(".$tmpkdtest.",'".$tmpkdpasien."','".$tmpkdunit."','".$tmptgl."',".$tmpurutmasuk.",".$tmpurut.",'".$tmphasil."','')");
			}
			if($query)
			   {
				   $tmphasil='';
				  echo '{success: true, KD_PASIEN: "'.$tmpkdpasien.'", TGL_MASUK: "'.$tmptgl.'", URUT_MASUK: "'.$tmpurutmasuk.'", HASIL:"'.$tmphasil.'"}';
			   }else{
				 echo "{success:false}";
			   }
				
		}
    }

    public function saveTemplate($Params=null)
    {
    	$this->load->model('rad/tblam_template_02');
    	$this->load->database();
		$id_template = $_POST['id_template'];
		$template = $_POST['template'];
		$kd_dokter = $_POST['kd_dokter'];
		$kd_produk = $_POST['kd_produk'];
		$keterangan = $_POST['keterangan'];
		
		if ($id_template === '')
		{
			$id_template=$this->GetNewID();
		}else
		{
			$id_template = $id_template;
		}
		$count=$this->db->query("Select * From rad_template_hasil 
									where id_template=".$id_template." and template='".$template."' and kd_dokter = '".$kd_dokter."' 
									and kd_produk = ".$kd_produk." and keterangan = '".$keterangan."'")->result();
		
			if(count($count)>0)
			{
				 $query = $this->db->query("UPDATE rad_template_hasil SET template = '".$template."',kd_dokter = '".$kd_dokter."',kd_produk = ".$kd_produk.", keterangan = '".$keterangan."' WHERE id_template=".$id_template."");
				 if($query)
				   {
					  echo '{success: true}';
				   }else{
					 echo "{success:false}";
				   }
			}else
			{			
				$queryinsert = $this->db->query("INSERT INTO rad_template_hasil (id_template,template,kd_dokter,kd_produk,keterangan) values"
								."(".$id_template.",'".$template."','".$kd_dokter."',".$kd_produk.",'".$keterangan."')");	
				if($queryinsert)
			   {
				   echo '{success: true}';
			   }
			   else
			   {
				   echo '{success: false}';
			   }   
			}
			
				
		
    }

    function GetNewID()
     {
         /* $res = $this->tblam_template_02->GetRowList( 0 , 1, 'DESC', 'id_template', '');
         if ($res[1]===0)
         {
             return 1;
         }
         else
         {
             return $res[0][0]->KDTEMPLATE+1;
         } */
		 
		$res = $this->db->query("select max(id_template) as id_template from rad_template_hasil");
		if (count($res->result()) > 1)
		{
			return $res->row()->id_template + 1;
		}
		else
		{
			return 1;
		}
	}
	 
	function getKdUnitCurrentRad(){
		$kd_unit=$this->db->query("Select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		echo "{success: true, kd_unit:'$kd_unit'}";
	}

}



?>