﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tbl_pacs extends TblBase {

    function __construct() {
        $this->StrSql   = " transaksi.*, pacs.* ";
        $this->SqlQuery = "SELECT transaksi.*, pacs.* FROM 
            pacs 
            INNER JOIN transaksi ON pacs.kd_kasir = transaksi.kd_kasir AND pacs.no_transaksi = transaksi.no_transaksi
            INNER JOIN unit_asal ON unit_asal.kd_kasir = transaksi.kd_kasir AND unit_asal.no_transaksi = transaksi.no_transaksi
            INNER JOIN transaksi t_asal ON t_asal.kd_kasir = unit_asal.kd_kasir_asal AND t_asal.no_transaksi = unit_asal.no_transaksi_asal
            INNER JOIN kunjungan ON kunjungan.kd_pasien = t_asal.kd_pasien AND kunjungan.tgl_masuk = t_asal.tgl_transaksi AND kunjungan.urut_masuk = t_asal.urut_masuk AND kunjungan.kd_unit = t_asal.kd_unit ";
        $this->TblName  = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new RowData;

        $row->NO_TRANSAKSI  = $rec->no_transaksi;
        $row->KD_KASIR      = $rec->kd_kasir;
        $row->TGL_TRANSAKSI = date_format(date_create($rec->tgl_transaksi), 'Y-m-d');
        $row->STUDY_UID     = $rec->study_uid;
        $row->PATIENT_ID    = $rec->patient_id;
        $query = $this->db->query("SELECT * FROM detail_transaksi dt 
            INNER JOIN transaksi t ON dt.kd_kasir = t.kd_kasir AND dt.no_transaksi = t.no_transaksi 
            INNER JOIN produk p ON p.kd_produk = dt.kd_produk 
            INNER JOIN rad_hasil rh ON rh.kd_pasien = t.kd_pasien and rh.kd_unit = t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi and rh.kd_test=dt.kd_produk and rh.urut=dt.urut WHERE dt.kd_kasir = '".$rec->kd_kasir."' AND dt.no_transaksi = '".$rec->no_transaksi."'");
        $tindakan = "";
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $tindakan .= $result->deskripsi.", ";
            }
            $tindakan = substr($tindakan, 0, -2);
        }
        $row->TINDAKAN      = $tindakan;
        return $row;
    }

}

class RowData {

    public $NO_TRANSAKSI;
    public $TGL_TRANSAKSI;
    public $TINDAKAN;
    public $KD_KASIR;
    public $STUDY_UID;
    public $PATIENT_ID;
}
?>