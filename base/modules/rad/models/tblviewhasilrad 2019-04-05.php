﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tblviewhasilrad extends TblBase {

    function __construct() {
        $this->StrSql = "kd_pasien, nama, alamat, tgl_masuk, kd_dokter, nama_unit, kd_kelas, urut_masuk, nama_dokter,
			       tgl_lahir, jenis_kelamin, gol_darah, nama_unit_asal,kd_unit";
        $this->SqlQuery = "
                        SELECT 
                            transaksi.no_transaksi,
                            transaksi.kd_kasir,
                            Pasien.Kd_pasien, 
                            pasien.Nama, 
                            Pasien.Alamat, 
                            Kunjungan.Tgl_Masuk, 
                            Kunjungan.kd_unit, 
                            uNIT.Nama_unit, 
                            Unit.kd_kelas, 
                            Dokter.Kd_Dokter, 
                            Dokter.Nama as Nama_Dokter,
                            CASE WHEN rad_hasil.kd_dokter is not null THEN rad_hasil.kd_dokter ELSE Dokter.Kd_Dokter END as kd_dokter_hasil,
                            CASE WHEN rad_hasil.kd_dokter is not null THEN rad_hasil.nama ELSE Dokter.Nama END as nama_dokter_hasil,
                            CASE WHEN rad_hasil.tgl_baca is null THEN kunjungan.tgl_masuk ELSE rad_hasil.tgl_baca END as tgl_baca, 
                            kunjungan.urut_masuk,  
                            pasien.tgl_lahir, 
                            pasien.jenis_kelamin, 
                            pasien.gol_darah, 
                            uasal.nama_unit as nama_unit_asal, 
                            pa.study_uid, pa.patient_id 
                        From unit 
                        INNER join (((kunjungan  
                        INNER join  Pasien  on Kunjungan.kd_pasien=pasien.kd_pasien) 
                        INNER join dokter on Dokter.kd_Dokter = kunjungan.Kd_Dokter)) on unit.kd_unit=Kunjungan.kd_unit  
                        INNER JOIN Transaksi   On transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk
                        left JOIN pacs pa on transaksi.no_transaksi = pa.no_transaksi and transaksi.kd_kasir = pa.kd_kasir
                        left JOIN unit_asal ua on transaksi.no_transaksi = ua.no_transaksi and transaksi.kd_kasir = ua.kd_kasir
                        INNER JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                        INNER join unit uasal on trasal.kd_unit = uasal.kd_unit 
                        LEFT JOIN (
                            SELECT 
                                rad_hasil.kd_pasien,
                                rad_hasil.kd_unit,
                                rad_hasil.tgl_masuk,
                                rad_hasil.urut_masuk,
                                rad_hasil.kd_dokter,
                                d_hasil.nama,
                                rad_hasil.tgl_baca
                            FROM 
                                rad_hasil 
                            INNER JOIN dokter d_hasil ON d_hasil.kd_dokter = rad_hasil.kd_dokter 
                            WHERE rad_hasil.kd_dokter is not null
                            GROUP BY 
                                rad_hasil.kd_pasien,
                                rad_hasil.kd_unit,
                                rad_hasil.tgl_masuk,
                                rad_hasil.urut_masuk,
                                rad_hasil.kd_dokter,
                                rad_hasil.tgl_baca,
                                d_hasil.nama
                        ) as rad_hasil ON 
                        rad_hasil.kd_pasien = kunjungan.kd_pasien 
                        AND rad_hasil.kd_unit = kunjungan.kd_unit 
                        AND rad_hasil.tgl_masuk = kunjungan.tgl_masuk 
                        AND rad_hasil.urut_masuk = kunjungan.urut_masuk
                        ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowdokter;

        $row->KD_PASIEN = $rec->kd_pasien;
        $row->NAMA      = $rec->nama;
        $row->ALAMAT    = $rec->alamat;
        $row->TGL_MASUK = $rec->tgl_masuk;
        if ($rec->nama_unit_asal == "") {
            $row->NAMA_UNIT = $rec->nama_unit;
        } else {
            $row->NAMA_UNIT = $rec->nama_unit_asal;
        }
        $row->KD_KELAS    = $rec->kd_kelas;
        $row->URUT_MASUK  = $rec->urut_masuk;
        $row->TGL_LAHIR   = $rec->tgl_lahir;
        if ($rec->jenis_kelamin == 't') {
            $row->JENIS_KELAMIN = 'Laki-Laki';
        } else {
            $row->JENIS_KELAMIN = 'Perempuan';
        }
        $row->GOL_DARAH         = $rec->gol_darah;
        $row->NAMA_UNIT_ASAL    = $rec->nama_unit_asal;
        $row->KD_UNIT           = $rec->kd_unit;
        $row->NO_TRANSAKSI      = $rec->no_transaksi;
        $row->PATIENT_ID        = $rec->patient_id;
        $row->STUDY_UID         = $rec->study_uid;
        $row->KD_KASIR          = $rec->kd_kasir;
        $row->KD_DOKTER         = $rec->kd_dokter;
        $row->NAMA_DOKTER       = $rec->nama_dokter;
        $row->KD_DOKTER_HASIL   = $rec->kd_dokter_hasil;
        $row->NAMA_DOKTER_HASIL = $rec->nama_dokter_hasil;
        $row->TGL_BACA          = $rec->tgl_baca;
        return $row;
    }

}

class Rowdokter {

    public $KD_DOKTER;
    public $NAMA_DOKTER;
    public $KD_PASIEN;
    public $NAMA;
    public $ALAMAT;
    public $TGL_MASUK;
    public $NAMA_UNIT;
    public $KD_KELAS;
    public $URUT_MASUK;
    public $TGL_LAHIR;
    public $JENIS_KELAMIN;
    public $GOL_DARAH;
    public $NAMA_UNIT_ASAL;
    public $NO_TRANSAKSI;
    public $PATIENT_ID;
    public $STUDY_UID;
    public $KD_KASIR;
    public $KD_DOKTER_HASIL;
    public $NAMA_DOKTER_HASIL;
    public $TGL_BACA;

}
?>