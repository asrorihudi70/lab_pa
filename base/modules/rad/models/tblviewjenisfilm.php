﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewjenisfilm extends TblBase
{
	
        function __construct()
        {
        	$this->StrSql="kd_jnsfilm,jns_film,jns_bhn";
        	$this->SqlQuery="select * from RAD_JNSFiLM";
        	$this->TblName='';
                TblBase::TblBase(true);
        }


        function FillRow($rec)
        {
        	$row=new Rowjenisfilm;
                $row->KODE=$rec->kd_jnsfilm;
                $row->JENIS_FILM=$rec->jns_film;
                $row->JENIS_BAHAN=$rec->jns_bhn;
        	return $row;
        }
}
        class Rowjenisfilm
        {
                public $KODE;
                public $JENIS_FILM;
                public $JENIS_BAHAN;
        }