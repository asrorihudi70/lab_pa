﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tblviewtemplatehasilrad extends TblBase {

    function __construct() {
        $this->StrSql = "id_template,template,kd_dokter,kd_produk,keterangan";
        $this->SqlQuery = "select * from rad_template_hasil";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowdokter;

        $row->KODE = $rec->id_template;
        $row->NAMA = $rec->template;
        $row->KD_DOKTER = $rec->kd_dokter;
        $row->KD_PRODUK = $rec->kd_produk;
        $row->KET = $rec->keterangan;
      
        return $row;
    }

}

class Rowdokter {

    public $KODE;
    public $NAMA;
    public $KD_DOKTER;
    public $KD_PRODUK;
    public $KET;

}
?>