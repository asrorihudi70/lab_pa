<?php 
	
?>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
		font-family: Calibri;
		font-size: 14px;
	}
	#content{
		/*width:105mm;*/
		width:<?php echo $size."mm"; ?>;
		/*min-height:99mm;*/
		min-height:274mm;
		height:274mm;
		border:0px solid #000;
		padding: 5px;
		border: 0px solid #000;
	}
	table td, th{
		font-family: Calibri;
		/*font-size: <?php //echo $font_size."px"; ?>;*/
	}

	.data_row{
		font-family: Calibri;
		font-size: 11px;
	}

	/*.row{
		page-break-inside:auto;
		border:1px solid lightgray;
		page-break-inside:auto;
	}*/

	 @media print {
	    html, body {
			display: block; 
			font-family: "Calibri";
			margin: 0;
	    }
		@page {
			width:<?php echo $size."mm"; ?>;
			min-height:99mm;
		}
		/*.row{
			page-break-inside:auto;
			border:1px solid lightgray;
			page-break-inside:auto;
		}*/
		
	}
</style>
<?php 
	$att_titik = " .......... ";
?>

<div id="content">
	<table width="100%" border="0" cellpadding="0" border='0'>	
		<tr>
			<th colspan = "2" align="center"><h3><b><?php echo $title; ?></b></h3></th>
		</tr>	
		<tr>
			<th colspan = "2" align="center"><h3><b>Periode tanggal masuk : <?php echo $tgl_awal." s/d ".$tgl_akhir; ?></b></h3></th>
		</tr>
		<tr>
			<th height="35" width="50%" align="left"></th>
			<th align="right" valign="bottom">Tanggal Cetak : <?php echo date("d/M/Y H:i:s"); ?></th>
		</tr>
	</table>
	<?php
		if ($data->num_rows() > 0) {
			?>
			<table width="100%" border="1" cellpadding="2">	
				<tr>
					<th width="25">No</th>
					<th>Tgl Baca</th>
					<th>Medrec</th>
					<th>Nama</th>
					<th>No Foto</th>
					<th>Customer</th>
					<th>Dokter yang membaca</th>
				</tr>
				<?php $no = 1; foreach ($data->result() as $result) { ?>
					<tr>
						<td align="center"><?php echo $no; ?></td>
						<td align="left"><?php echo date_format(date_create($result->tgl_baca), 'd/M/Y'); ?></td>
						<td align="left"><?php echo $result->kd_pasien; ?></td>
						<td align="left"><?php echo $result->nama; ?></td>
						<td align="left"><?php echo $result->no_foto_rad; ?></td>
						<td align="left"><?php echo $result->customer; ?></td>
						<td align="left"><?php echo $result->dokter; ?></td>
					</tr>
				<?php $no++; } ?>
			</table>
			<?php
		}else{
			echo "Tidak ada hasil yang disimpan";
		}
	?>
</div>