<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewtemplatehasilrad extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
    {
        try
        {   
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('rad/tblviewtemplatehasilrad');
             if (strlen($Params[4])!== 0)
            {
                    $this->db->where($Params[4],null, false) ;
                    $res = $this->tblviewtemplatehasilrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
            }else{
                    $res = $this->tblviewtemplatehasilrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                  }
        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }   
    
   public function save($Params=null)
    {
		$tmpkdpasien = $Params["KdPasien"];
		$tmpkdunit = $Params["KdUnit"];
		$tmptgl = $Params["Tgl"];
		$tmpurutmasuk = $Params["UrutMasuk"];
		$tmpurut = $Params["urut"];
		$tmpkdtest = $Params["KdTest"];
		$tmphasil = $Params["Hasil"];
						
		$urut = $this->db->query("Select * From Rad_Hasil 
								where Kd_Pasien='".$tmpkdpasien."' and Kd_Unit='".$tmpkdunit."' and Tgl_masuk = ('".$tmptgl."') 
								and Urut_masuk = ".$tmpurutmasuk." and urut = ".$tmpurut."
								and Kd_Test = ".$tmpkdtest."");
		$result = $urut->result();
		foreach ($result as $data)
		{
			if (count($data) != "")
			{
				$query = $this->db->query("UPDATE Rad_Hasil SET HASIL = '".$tmphasil."' WHERE Kd_Pasien='".$tmpkdpasien."' and Kd_Unit='".$tmpkdunit."' and Tgl_masuk = ('".$tmptgl."') 
											and Urut_masuk = ".$tmpurutmasuk." and urut = ".$tmpurut."
											and Kd_Test = ".$tmpkdtest."");
			   
			}else
			{
				 $query = $this->db->query("INSERT INTO rad_hasil (kd_test,kd_pasien,kd_unit,tgl_masuk,urut_masuk,urut,hasil,keluhan) values"
																."(".$tmpkdtest.",'".$tmpkdpasien."','".$tmpkdunit."','".$tmptgl."',".$tmpurutmasuk.",".$tmpurut.",'".$tmphasil."','')");
			}
			if($query)
			   {
				  echo '{success: true, KD_PASIEN: "'.$tmpkdpasien.'", TGL_MASUK: "'.$tmptgl.'", URUT_MASUK: "'.$tmpurutmasuk.'", HASIL:"'.$tmphasil.'"}';
			   }else{
				 echo "{success:false}";
			   }
				
		}
    }

}

?>