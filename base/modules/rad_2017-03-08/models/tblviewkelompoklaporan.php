﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tblviewkelompoklaporan extends TblBase {

    function __construct() {
        $this->StrSql = "kd_pay, uraian";
        $this->SqlQuery = "Select Kd_Pay, Uraian from Payment Order By Uraian ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowdokter;

        $row->KODE = $rec->kd_pay;
        $row->NAMA = $rec->uraian;

        return $row;
    }

}

class Rowdokter {

    public $KODE;
    public $NAMA;

}
?>