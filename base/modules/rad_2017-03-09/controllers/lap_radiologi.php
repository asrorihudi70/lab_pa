<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_radiologi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	
	public function cetakTRPerkomponenDetail(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Perkomponen Harian';
		$param=json_decode($_POST['data']);
		$html='';
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query("select distinct no_transaksi , namapasien2 from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
												from detail_transaksi DT   
													inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
														and dt.kd_kasir=tc.kd_kasir 
														and dt.tgl_transaksi=tc.tgl_transaksi 
														and dt.urut=tc.urut   
													inner join produk prd on DT.kd_produk=prd.kd_produk   
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
													inner join customer c on k.kd_customer=c.kd_Customer   
													left join kontraktor knt on c.kd_customer=knt.kd_Customer  
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and (u.kd_bagian=5)
													group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
													order by t.No_transaksi,prd.deskripsi)X ");
												
		$query = $queryBody->result();	
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';	
		$query_kolom = $this->db->query("Select Distinct pc.kd_Component, pc.Component 
											from  Produk_Component pc 
												INNER JOIN Detail_component dc On dc.kd_Component=pc.KD_Component  
												INNER JOIN Detail_transaksi dt ON dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi  And dc.Urut=dt.Urut And dc.Tgl_Transaksi=dt.Tgl_Transaksi  
												INNER JOIN Transaksi t ON t.kd_kasir=dt.kd_kasir And t.No_Transaksi=dt.No_Transaksi  
												INNER JOIN kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit  and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
												INNER JOIN Unit u On u.kd_Unit=k.kd_Unit  
												INNER JOIN customer c on k.kd_customer=c.kd_Customer 
												LEFT JOIN kontraktor knt on c.kd_customer=knt.kd_Customer  
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and (u.kd_bagian=5) 
											AND dc.kd_Component <> 36 order by pc.kd_Component ")->result();
	
		//echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';									
			//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="5">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Transaksi</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>';
		$arr_kd_component=array();
		if(count($query_kolom) > 0) {
			$i=0;
			foreach ($query_kolom as $line_kolom) 
			{
				$html.='<th align="center">'.$line_kolom->component.'</th>';
				$arr_kd_component[$i] = $line_kolom->kd_component;
				$i++;
			}
		}	
		$html.='<th align="center">Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$arr_total=array(); //VARIABEL UNTUK MENAMPUNG TOTAL JUMLAH PERPASIEN
			$t_arr_total=array();
			$p=0; //inisialisasi variabel untuk menyimpan sementara arr jumlah percomponent
			$t_total=0;
			$t_jumlah_total = 0;
			#LOOPING PER PASIEN TRANSAKSI
			foreach ($query as $line) 
			{
				$no++;
				$no_transaksi = $line->no_transaksi;
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.$line->no_transaksi.'</td>
							<td>'.$line->namapasien2.'</td>';
				$deskripsi='';
				$arr_jumlah=array(); // VARIABEL MENAMPUNG TOTAL JUMLAH PERCOMPONENT
				#LOOPING PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$query_detail = $this->db->query("select * from (select t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama as namapasien, (p.kd_pasien ||' '|| p.nama ) as namapasien2, tc.Kd_Component,prd.Deskripsi,sum(tc.tarif) as jumlah   
													from detail_transaksi DT   
														inner join detail_component tc on dt.no_transaksi=tc.no_transaksi 
															and dt.kd_kasir=tc.kd_kasir 
															and dt.tgl_transaksi=tc.tgl_transaksi 
															and dt.urut=tc.urut   
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer  
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=5)
														group by t.No_Transaksi,t.kd_kasir,p.kd_pasien,p.nama,tc.Kd_Component,prd.Deskripsi  
														order by t.No_transaksi,prd.deskripsi) X
													where no_transaksi = '$no_transaksi' and kd_component = '$arr_kd_component[$i]' ")->result();
					#JUMLAH ITEM COMPONENT
					$jumlah_detail=0;
					foreach ($query_detail as $line2) 
					{
						$deskripsi.= $line2->deskripsi.",";
						$jumlah_detail = $jumlah_detail + $line2->jumlah;
					}
					
					#TOTAL DARI JUMLAH UNTUK PERPASIEN (AKUMULASI SEMUA COMPONENT)
					$arr_jumlah[$i] = $jumlah_detail;
					if($i != 0){
						$arr_total[$i] =$arr_total[$i-1] + $arr_jumlah[$i];
					}else{
						$arr_total[$i] =$arr_jumlah[$i];
					}
				}
				$jumlah_total = 0;
				$html.='<td width="" >'.substr($deskripsi,0,-1).'</td>';
				#AKUMULASI JUMLAH PERCOMPONENT
				for($i=0 ; $i<count($arr_kd_component); $i++){
					$html.='<td width="" align="right">'.number_format($arr_jumlah[$i],0, "." , ".").'</td>';
					$jumlah_total = $jumlah_total + $arr_jumlah[$i];
					$t_arr_total[$p][$i] = $arr_jumlah[$i];
				}
				#PROSES AKUMULASI TOTAL JUMLAH PERPASIEN
				$html.='<td width="" align="right">'.number_format($jumlah_total,0, "." , ".").'</td></tr>';
				$p++;
				$t_total = $t_total +  $jumlah_total;
				
			}
				
			$j_p = $p -1; // variabel untuk pengecekan jumlah pasien 
			if($j_p != 0){
				#TRANSPOSE ARRAY TAMPUNGAN 
				array_unshift($t_arr_total,null);
				$t_arr_total = call_user_func_array('array_map', $t_arr_total);
				#AKUMULASI TOTAL 
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$temp_jumlah=0;
					for($y = 0 ; $y < $p ; $y++){
						$temp_jumlah = $temp_jumlah + $t_arr_total[$x][$y];
					}
					$arr_total_semua [$x] = $temp_jumlah;
				}	
			}else{
				$arr_total_semua = array();
				for($x = 0 ; $x < count ($arr_kd_component) ;$x++){
					$arr_total_semua[$x] = $t_arr_total[0][$x];
				}	
			}
			
			$html.='<tr>
						<td colspan="4" align="right"> <b>Total &nbsp;</b></td>';
						for($i=0 ; $i<count($arr_kd_component); $i++){
							$html.='<td width="" align="right">'.number_format($arr_total_semua[$i],0, "." , ".").'</td>';
						}
			$html.='<td width="" align="right">'.number_format($t_total,0, "." , ".").'</td></tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="5" align="center">Data tidak ada</td>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L','Lap. Transaksi Perkomponen Detail',$html);	
		//$html.='</table>';
   	}
	
	public function cetakTRPerkomponenSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Transaksi Perkomponen';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		//-----------------------get kode kasir unit-------------------------------------
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		} 
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}

		$queryBody = $this->db->query( "Select y.tgl_transaksi, y.kd_Component,Sum(y.jml) as jml,count(y.jml_pasien ) as jml_pasien  
										from (  Select t.tgl_transaksi, tc.kd_Component,Sum(tc.Tarif) as jml,t.kd_pasien as jml_pasien   
											from pasien p   
												inner join kunjungan k on p.kd_pasien=k.kd_pasien   
												left join Dokter dok on k.kd_dokter=dok.kd_Dokter     
												inner join unit u on u.kd_unit=k.kd_unit    
												inner join customer c on k.kd_customer=c.kd_Customer   
												left join kontraktor knt on c.kd_Customer=knt.kd_Customer     
												inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
												inner join detail_transaksi DT on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir   
												inner join detail_component tc on dt.no_transaksi=tc.no_transaksi and dt.kd_kasir=tc.kd_kasir and dt.tgl_transaksi=tc.tgl_transaksi and dt.urut=tc.urut   
												inner join produk prd on dt.kd_produk=prd.kd_produk  
												inner join klas_produk kprd on prd.kd_klas=kprd.kd_klas   
											".$criteriaShift."
											".$crtiteriaAsalPasien."
											".$criteriaCustomer."
											and u.kd_bagian=5
											group by t.tgl_Transaksi,tc.kd_Component , t.kd_pasien  ) y  
										group by y.Tgl_Transaksi , y.kd_Component 
										order by y.tgl_Transaksi	");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th width="15" align="center">No</th>
					<th align="center">Tanggal</th>
					<th width="100" align="center">Jml Pasien</th>
					<th align="center">Jasa Saran</th>
					<th align="center">Total</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td>'.date('d-M-Y',strtotime($line->tgl_transaksi)).'</td>
								<td align="right">'.$line->jml_pasien.'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								<td width="" align="right">'.number_format($line->jml,0, "." , ".").'</td>
								
							  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Transaksi Perkomponen Summary',$html);	
		//$html.='</table>';
   	}

	
	public function cetakTRDetail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		$queryHead_t = $this->db->query( " select distinct head from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
											db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
												from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay  
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
													inner join pasien p on t.kd_pasien=p.kd_pasien 
													".$criteriaShift_b."
													".$crtiteriaAsalPasien."
													".$criteriaCustomer."
													and u.kd_bagian=5 
													and pyt.Type_Data <=3  
											union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
													(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
													FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
													Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
														from detail_transaksi DT  
														inner join produk prd on DT.kd_produk=prd.kd_produk   
														inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
														inner join customer c on k.kd_customer=c.kd_Customer   
														left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and (u.kd_bagian=5)  
										order by bayar,kd_pay,No_transaksi) Z ")->result();
		
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">QTY</th>
					<th align="center">Jumlah</th>
					<th align="center">User</th>
				  </tr>
			</thead><tbody>';
		// echo count($queryHead);
		if(count($queryHead_t) > 0) {
			$no=0;
			$jumlah_total = 0;
			foreach ($queryHead_t as $lineHead_t) 
			{
				$head = $lineHead_t->head;
				$html.='<tr>
							<td></td>
							<td><b>'.$head.'</b></td>
							<td></td><td></td><td></td><td></td>
						</tr>';
				$queryHead = $this->db->query( " select distinct namapasien from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=5 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=5)  
											order by bayar,kd_pay,No_transaksi) Z where head='$head' ")->result();
				foreach ($queryHead as $lineHead) 
				{
					$no++;
					$namapasien = $lineHead->namapasien;
					$query = $this->db->query( " select * from (Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama ) as namaPasien,  
												db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head   
													from Detail_bayar db   
														inner join payment py on db.kd_pay=py.kd_Pay  
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
														left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
														inner join unit u on u.kd_unit=t.kd_unit    
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
														inner join customer c on k.kd_customer=c.kd_Customer  left join kontraktor knt on c.kd_customer=knt.kd_Customer     
														inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift_b."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
														and u.kd_bagian=5 
														and pyt.Type_Data <=3  
												union select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
														(Dt.QTY* Dt.Harga) + COALESCE ((SELECT SUM(Harga * QTY )  
														FROM Detail_Bahan WHERE kd_kasir=t.kd_kasir and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut),0) as jumlah, 
														Dt.QTY,Dt.Urut, Dt.tgl_transaksi, 'Pemeriksaan' AS Head     
															from detail_transaksi DT  
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien 
															".$criteriaShift."
															".$crtiteriaAsalPasien."
															".$criteriaCustomer."
															and (u.kd_bagian=5)  
												order by bayar,kd_pay,No_transaksi) Z where namapasien='$namapasien' and head='$head'")->result();
					
					$html.='<tr>
										<td>'.$no.'</td>
										<td>'.$lineHead->namapasien.'</td>
										<td></td><td></td><td></td><td></td>
							</tr>';
					$jumlah = 0;
					foreach ($query as $line) 
					{
						
						$html.='<tr>
									<td></td>
									<td></td>
									<td>'.$line->deskripsi.'</td>
									<td align="center">'.$line->qty.'</td>
									<td align="right">'.number_format($line->jumlah,0, "." , ".").' &nbsp; </td>
									<td align="center">'.$line->kd_user.'</td>
								</tr>';
						$jumlah = $jumlah + $line->jumlah;
					}
					$html.='<tr>
								<td></td>
								<td></td>
								<td align="right">Jumlah &nbsp; </td>
								<td></td>
								<td align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
								<td></td>
							</tr>';
					
				}	
				$jumlah_total = $jumlah_total + $jumlah ;
			}
			$html.='<tr>
						<td colspan="3" align="right"> Total Jumlah &nbsp; </td>
						<td></td>
						<td align="right">'.number_format($jumlah_total,0, "." , ".").' &nbsp;</td>
						<td></td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Transaksi  Detail',$html);	
		//$html.='</table>';
		
	}
	
	public function cetakTRSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Transaksi Harian';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and c.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift_b="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift_b="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		$queryBody = $this->db->query( "Select X.BAYAR,X.Kd_CUSTOMER,X.CUSTOMER,
											SUM(P) AS JML_PASIEN,SUM(JML) AS JML_TR  
											from ( Select 1 AS BAYAR,k.Kd_CUSTOMER,c.CUSTOMER,  1 as P , SUM(db.Jumlah) AS JML   
													from Detail_bayar  db     
														inner join payment py on db.kd_pay=py.kd_Pay       
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay        
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir      
														inner join unit u on u.kd_unit=t.kd_unit        
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
														inner join customer c on k.kd_customer=c.kd_Customer       
														left join kontraktor knt on c.kd_customer=knt.kd_Customer             
														inner join pasien p on t.kd_pasien=p.kd_pasien       
												".$criteriaShift_b."
												".$crtiteriaAsalPasien."
												".$criteriaCustomer."
												and u.kd_bagian=5  
												and t.ispay='T' 
												and pyt.Type_Data <=3
											GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR  ) X 
											GROUP BY x.bayar,X.KD_CUSTOMER, X.CUSTOMER 
										union SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML)  
											FROM (select 0 as Tagih,k.KD_CUSTOMER,c.CUSTOMER, 0 as P, SUM(Dt.QTY* Dt.Harga) + COALESCE((SELECT SUM(Harga * QTY ) 
													FROM Detail_Bahan WHERE kd_kasir = t.kd_kasir and no_transaksi = dt.no_transaksi and tgl_transaksi = dt.tgl_transaksi and urut = dt.urut),0) as JML      
													from detail_transaksi DT       
													inner join produk prd on DT.kd_produk=prd.kd_produk      
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir      
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
													inner join customer c on k.kd_customer=c.kd_Customer      
													left join kontraktor knt on c.kd_customer=knt.kd_Customer            
													inner join pasien p on t.kd_pasien=p.kd_pasien 
														".$criteriaShift."
														".$crtiteriaAsalPasien."
														".$criteriaCustomer."
															and (u.kd_bagian=5)  
														GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR , dt.No_Transaksi, dt.Tgl_Transaksi, dt.Urut ) X 
															GROUP BY x.tagih,X.KD_CUSTOMER, X.CUSTOMER ");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.' '.$asal_pasien.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Kelompok Pasien</th>
					<th align="center">JML Pas</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Penerimaan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$no_b=0;
			$html.='<tbody>';
			$jml_pemeriksaan = 0 ;
			$jml_penerimaan = 0 ;
			$jml_pasien = 0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td align="right">'.$line->jml_pasien.' &nbsp; &nbsp;</td>';
				
				if($line->bayar == 0){
					$html.='<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
							<td align="right"> 0 &nbsp;</td>
						</tr>';
					$jml_pemeriksaan = $jml_pemeriksaan + $line->jml_tr;
				}else{
					$html.='<td align="right"> 0 &nbsp;</td>
							<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
						</tr>';
					$jml_penerimaan = $jml_penerimaan + $line->jml_tr;
				}
				
				$jml_pasien = $jml_pasien + $line->jml_pasien;
			}
		$selisih = 	$jml_pemeriksaan - $jml_penerimaan;
		$html.='<tr>
					<td colspan="2" align="right"> Total : &nbsp;</td>
					<td align="right"> '.$jml_pasien.' &nbsp; &nbsp;</td>
					<td align="right"> '.number_format($jml_pemeriksaan,0, "." , ".").' &nbsp;</td>
					<td align="right"> '.number_format($jml_penerimaan,0, "." , ".").' &nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="right"> Selisih : &nbsp;</td>
					<td> </td>
					<td align="right"> '.number_format($selisih,0, "." , ".").' &nbsp; </td>
					<td> </td>
				</tr>';	
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Lap. Transaksi Summary',$html);	
		//$html.='</table>';
		
	}
}
?>