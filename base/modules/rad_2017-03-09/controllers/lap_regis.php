<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_regis extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
		
	public function cetak(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Buku Registrasi Pendaftaran Radiologi ';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		//$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Pasien Rawat Jalan';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'Pasien Rawat Inap';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'Pasien IGD';
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer="and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($shift == 'All'){
			$criteriaShift=" (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (1,2,3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (1, 2, 3)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (K.SHIFT = 4) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (1)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (1)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4))  ";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (1,2)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (1, 2)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4)) ";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift=" (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (1,3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (1,3)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (K.SHIFT = 4) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (2)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (2)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4))";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="  (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (2,3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (2,3)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (K.SHIFT = 4) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift=" (K.TGL_MASUK >= ".$tAwal.")  and (K.TGL_MASUK <= ".$tAkhir.") and K.SHIFT in (3)
									AND (not (K.TGL_MASUK = ".$tAwal.")) AND  (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien  
								or  (K.TGL_MASUK >= ".$tAwal.") AND (K.TGL_MASUK <= ".$tAkhir.") AND (K.SHIFT IN (3)) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) 
									$crtiteriaAsalPasien   AND (NOT (K.SHIFT = 4)) 
								OR (K.TGL_MASUK BETWEEN '".$tomorrow."' AND '".$tomorrow2."') AND (K.SHIFT = 4) AND (ru.TGL_TRANSAKSI = K.TGL_MASUK) $crtiteriaAsalPasien";
				$sh="Shift 3";
			} 
		}
	
		$queryBody = $this->db->query( "select distinct kd_pasien,nama,alamat,umur,nama_unit, tag from(select * from (SELECT DISTINCT 
										  p.KD_PASIEN, p.NAMA, p.ALAMAT, DATE_PART('year', current_date) - DATE_PART('year', p.tgl_lahir)  AS umur, u.NAMA_UNIT, prd.DESKRIPSI, o.NAMA_OBAT AS NamaProduk, rf.QTY, 
										  K.KD_UNIT, K.TGL_MASUK, K.JAM_MASUK, K.URUT_MASUK, K.BARU, t.NO_TRANSAKSI, t.KD_KASIR, rk.KD_RUJUKAN, r.RUJUKAN, rf.Kd_prd, 
										  ru.NO_REGISTER AS TAG, p.JENIS_KELAMIN
											FROM KUNJUNGAN AS K INNER JOIN
												  TRANSAKSI AS t ON K.KD_PASIEN = t.KD_PASIEN AND K.KD_UNIT = t.KD_UNIT AND K.URUT_MASUK = t.URUT_MASUK AND 
												  K.TGL_MASUK = t.TGL_TRANSAKSI LEFT OUTER JOIN
												  RUJUKAN_KUNJUNGAN AS rk ON K.KD_PASIEN = rk.KD_PASIEN AND K.TGL_MASUK = rk.TGL_MASUK AND K.KD_UNIT = rk.KD_UNIT AND 
												  K.URUT_MASUK = rk.URUT_MASUK LEFT OUTER JOIN
												  RUJUKAN AS r ON rk.KD_RUJUKAN = r.KD_RUJUKAN INNER JOIN
												  CUSTOMER AS c ON K.KD_CUSTOMER = c.KD_CUSTOMER LEFT OUTER JOIN
												  KONTRAKTOR AS knt ON c.KD_CUSTOMER = knt.KD_CUSTOMER INNER JOIN
												  PASIEN AS p ON K.KD_PASIEN = p.KD_PASIEN LEFT OUTER JOIN
												  PEKERJAAN AS pkj ON pkj.KD_PEKERJAAN = p.KD_PEKERJAAN LEFT OUTER JOIN
												  PERUSAHAAN AS prs ON p.KD_PERUSAHAAN = prs.KD_PERUSAHAAN INNER JOIN
												  DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = t.NO_TRANSAKSI AND dt.KD_KASIR = t.KD_KASIR INNER JOIN
												  PRODUK AS prd ON dt.KD_PRODUK = prd.KD_PRODUK LEFT OUTER JOIN
												  UNIT_ASAL AS ua ON t.KD_KASIR = ua.KD_KASIR AND t.NO_TRANSAKSI = ua.NO_TRANSAKSI LEFT OUTER JOIN
												  TRANSAKSI AS tr1 ON ua.NO_TRANSAKSI_ASAL = tr1.NO_TRANSAKSI AND ua.KD_KASIR_ASAL = tr1.KD_KASIR LEFT OUTER JOIN
												  UNIT AS u ON u.KD_UNIT = tr1.KD_UNIT LEFT OUTER JOIN
												  REG_UNIT AS ru ON ru.KD_PASIEN = K.KD_PASIEN AND ru.KD_UNIT = K.KD_UNIT LEFT OUTER JOIN
												  DETAIL_RADFO AS rf ON rf.KD_KASIR = dt.KD_KASIR AND rf.NO_TRANSAKSI = dt.NO_TRANSAKSI AND rf.URUT = dt.URUT AND 
												  rf.TGL_TRANSAKSI = dt.TGL_TRANSAKSI AND rf.QTY > 0 LEFT OUTER JOIN
												  RAD_FO AS fo ON fo.Kd_prd = rf.Kd_prd LEFT OUTER JOIN
												  APT_OBAT AS o ON o.KD_PRD = fo.Kd_prd
											where ($criteriaShift)  $criteriaCustomer ) x 
											ORDER BY TAG, NO_TRANSAKSI )y ");
		$query = $queryBody->result();	
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		//-------------JUDUL-----------------------------------------------
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>'.$sh.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="4">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Pasien</th>
					<th align="center">Umur</th>
					<th align="center">Poliklinik</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Pemakaian Film</th>
					<th align="center">Jumlah</th>
					<th align="center">No. Register</th>
				  </tr>
			</thead><tbody>';
		
		if(count($query) > 0) {
			$no=0;
			
			foreach ($query as $line) 
			{
				$kd_pasien = $line->kd_pasien;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->kd_pasien.' '.$line->nama.' '.$line->alamat.'</td>
								<td>'.$line->umur.'th</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>'.$line->tag.'</td>
						 </tr>';
				$queryBody1 = $this->db->query( "select distinct nama_unit from (SELECT DISTINCT 
										  p.KD_PASIEN, p.NAMA, p.ALAMAT,DATE_PART('year', current_date) - DATE_PART('year', p.tgl_lahir)  AS umur, u.NAMA_UNIT, prd.DESKRIPSI, o.NAMA_OBAT AS NamaProduk, rf.QTY, 
										  K.KD_UNIT, K.TGL_MASUK, K.JAM_MASUK, K.URUT_MASUK, K.BARU, t.NO_TRANSAKSI, t.KD_KASIR, rk.KD_RUJUKAN, r.RUJUKAN, rf.Kd_prd, 
										  ru.NO_REGISTER AS TAG, p.JENIS_KELAMIN
											FROM KUNJUNGAN AS K INNER JOIN
												  TRANSAKSI AS t ON K.KD_PASIEN = t.KD_PASIEN AND K.KD_UNIT = t.KD_UNIT AND K.URUT_MASUK = t.URUT_MASUK AND 
												  K.TGL_MASUK = t.TGL_TRANSAKSI LEFT OUTER JOIN
												  RUJUKAN_KUNJUNGAN AS rk ON K.KD_PASIEN = rk.KD_PASIEN AND K.TGL_MASUK = rk.TGL_MASUK AND K.KD_UNIT = rk.KD_UNIT AND 
												  K.URUT_MASUK = rk.URUT_MASUK LEFT OUTER JOIN
												  RUJUKAN AS r ON rk.KD_RUJUKAN = r.KD_RUJUKAN INNER JOIN
												  CUSTOMER AS c ON K.KD_CUSTOMER = c.KD_CUSTOMER LEFT OUTER JOIN
												  KONTRAKTOR AS knt ON c.KD_CUSTOMER = knt.KD_CUSTOMER INNER JOIN
												  PASIEN AS p ON K.KD_PASIEN = p.KD_PASIEN LEFT OUTER JOIN
												  PEKERJAAN AS pkj ON pkj.KD_PEKERJAAN = p.KD_PEKERJAAN LEFT OUTER JOIN
												  PERUSAHAAN AS prs ON p.KD_PERUSAHAAN = prs.KD_PERUSAHAAN INNER JOIN
												  DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = t.NO_TRANSAKSI AND dt.KD_KASIR = t.KD_KASIR INNER JOIN
												  PRODUK AS prd ON dt.KD_PRODUK = prd.KD_PRODUK LEFT OUTER JOIN
												  UNIT_ASAL AS ua ON t.KD_KASIR = ua.KD_KASIR AND t.NO_TRANSAKSI = ua.NO_TRANSAKSI LEFT OUTER JOIN
												  TRANSAKSI AS tr1 ON ua.NO_TRANSAKSI_ASAL = tr1.NO_TRANSAKSI AND ua.KD_KASIR_ASAL = tr1.KD_KASIR LEFT OUTER JOIN
												  UNIT AS u ON u.KD_UNIT = tr1.KD_UNIT LEFT OUTER JOIN
												  REG_UNIT AS ru ON ru.KD_PASIEN = K.KD_PASIEN AND ru.KD_UNIT = K.KD_UNIT LEFT OUTER JOIN
												  DETAIL_RADFO AS rf ON rf.KD_KASIR = dt.KD_KASIR AND rf.NO_TRANSAKSI = dt.NO_TRANSAKSI AND rf.URUT = dt.URUT AND 
												  rf.TGL_TRANSAKSI = dt.TGL_TRANSAKSI AND rf.QTY > 0 LEFT OUTER JOIN
												  RAD_FO AS fo ON fo.Kd_prd = rf.Kd_prd LEFT OUTER JOIN
												  APT_OBAT AS o ON o.KD_PRD = fo.Kd_prd
											where ($criteriaShift)  $criteriaCustomer ) x where kd_pasien='$kd_pasien'
											ORDER BY nama_unit ");
				$query1 = $queryBody1->result();	
				foreach ($query1 as $line1) 
				{
					$nama_unit = $line1->nama_unit;
					$html.='<tr>	
								<td></td>
								<td></td>
								<td></td>
								<td>'.$nama_unit.'</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>';
					$queryBody2 = $this->db->query( "select * from (SELECT DISTINCT 
											  p.KD_PASIEN, p.NAMA, p.ALAMAT,DATE_PART('year', current_date) - DATE_PART('year', p.tgl_lahir)  AS umur, u.NAMA_UNIT, prd.DESKRIPSI, o.NAMA_OBAT AS NamaProduk, rf.QTY, 
											  K.KD_UNIT, K.TGL_MASUK, K.JAM_MASUK, K.URUT_MASUK, K.BARU, t.NO_TRANSAKSI, t.KD_KASIR, rk.KD_RUJUKAN, r.RUJUKAN, rf.Kd_prd, 
											  ru.NO_REGISTER AS TAG, p.JENIS_KELAMIN
												FROM KUNJUNGAN AS K INNER JOIN
													  TRANSAKSI AS t ON K.KD_PASIEN = t.KD_PASIEN AND K.KD_UNIT = t.KD_UNIT AND K.URUT_MASUK = t.URUT_MASUK AND 
													  K.TGL_MASUK = t.TGL_TRANSAKSI LEFT OUTER JOIN
													  RUJUKAN_KUNJUNGAN AS rk ON K.KD_PASIEN = rk.KD_PASIEN AND K.TGL_MASUK = rk.TGL_MASUK AND K.KD_UNIT = rk.KD_UNIT AND 
													  K.URUT_MASUK = rk.URUT_MASUK LEFT OUTER JOIN
													  RUJUKAN AS r ON rk.KD_RUJUKAN = r.KD_RUJUKAN INNER JOIN
													  CUSTOMER AS c ON K.KD_CUSTOMER = c.KD_CUSTOMER LEFT OUTER JOIN
													  KONTRAKTOR AS knt ON c.KD_CUSTOMER = knt.KD_CUSTOMER INNER JOIN
													  PASIEN AS p ON K.KD_PASIEN = p.KD_PASIEN LEFT OUTER JOIN
													  PEKERJAAN AS pkj ON pkj.KD_PEKERJAAN = p.KD_PEKERJAAN LEFT OUTER JOIN
													  PERUSAHAAN AS prs ON p.KD_PERUSAHAAN = prs.KD_PERUSAHAAN INNER JOIN
													  DETAIL_TRANSAKSI AS dt ON dt.NO_TRANSAKSI = t.NO_TRANSAKSI AND dt.KD_KASIR = t.KD_KASIR INNER JOIN
													  PRODUK AS prd ON dt.KD_PRODUK = prd.KD_PRODUK LEFT OUTER JOIN
													  UNIT_ASAL AS ua ON t.KD_KASIR = ua.KD_KASIR AND t.NO_TRANSAKSI = ua.NO_TRANSAKSI LEFT OUTER JOIN
													  TRANSAKSI AS tr1 ON ua.NO_TRANSAKSI_ASAL = tr1.NO_TRANSAKSI AND ua.KD_KASIR_ASAL = tr1.KD_KASIR LEFT OUTER JOIN
													  UNIT AS u ON u.KD_UNIT = tr1.KD_UNIT LEFT OUTER JOIN
													  REG_UNIT AS ru ON ru.KD_PASIEN = K.KD_PASIEN AND ru.KD_UNIT = K.KD_UNIT LEFT OUTER JOIN
													  DETAIL_RADFO AS rf ON rf.KD_KASIR = dt.KD_KASIR AND rf.NO_TRANSAKSI = dt.NO_TRANSAKSI AND rf.URUT = dt.URUT AND 
													  rf.TGL_TRANSAKSI = dt.TGL_TRANSAKSI AND rf.QTY > 0 LEFT OUTER JOIN
													  RAD_FO AS fo ON fo.Kd_prd = rf.Kd_prd LEFT OUTER JOIN
													  APT_OBAT AS o ON o.KD_PRD = fo.Kd_prd
												where ($criteriaShift)  $criteriaCustomer ) x where kd_pasien='$kd_pasien' and nama_unit='$nama_unit'
												ORDER BY TAG, NO_TRANSAKSI ");
					$query2 = $queryBody2->result();	
					
					
					foreach ($query2 as $line2) 
					{
						$html.='<tr>	
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>'.$line2->deskripsi.'</td>
								<td>'.$line2->namaproduk.'</td>
								<td>'.$line2->qty.'</td>
								<td></td>
							</tr>';
					}
				}
							
			}	
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L','Laporan Rekapitulasi Penerimaan Laboratorium',$html);	
		//$html.='</table>';
	}
	
}
?>