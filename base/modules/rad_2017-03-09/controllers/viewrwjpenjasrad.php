<?php

/**
 * @author Hidayat HDHT
 * @copyright NCI 2015
 */


class viewrwjpenjasrad extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
        try
        {   
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('rad/tblviewrwjpenjasrad');
             if (strlen($Params[4])!==0)
            {
                    $this->db->where($Params[4] ,null, false);
                    $res = $this->tblviewrwjpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
            }else{
                    $this->db->where("tr.tgl_transaksi >= '".$tgl_tampil."' and tr.tgl_transaksi <= '".$date."' and left(u.kd_unit,1) IN ('2','3') ORDER BY tr.no_transaksi desc limit 50" ,null, false);
                    $res = $this->tblviewrwjpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                  }

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }   

}

?>