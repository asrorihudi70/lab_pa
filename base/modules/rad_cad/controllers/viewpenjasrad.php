<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewpenjasrad extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
    {
        try
        {   
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('rad/tblviewpenjasrad');
             if (strlen($Params[4])!== 0)
            {
                    $this->db->where($Params[4] ,null , false) ;
                    $res = $this->tblviewpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
            }else{
                    $kriteria = "tr.tgl_transaksi >= '".$date."' and tr.tgl_transaksi <= '".$tgl_tampil."' and left(u.kd_unit,1) IN ('2','3') and tr.co_Status='0' ORDER BY tr.no_transaksi desc limit 50";
                    $this->db->where($kriteria ,null, false) ;
                    $res = $this->tblviewpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                  }
        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }   

}