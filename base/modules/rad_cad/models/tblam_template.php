<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tblam_template extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="id_template,template,kd_dokter,kd_produk,keterangan";
		$this->TblName='rad_template_hasil';
		TblBase::TblBase();
	}

	
	function FillRow($rec)
	{
		$row=new RowUnit;
		$row->KDTEMPLATE=$rec->id_template;
		$row->TEMPLATE=$rec->template;
        $row->KDDOKTER=$rec->kd_dokter;
		$row->KDPRODUK=$rec->kd_produk;
        $row->KET=$rec->keterangan;

		return $row;
	}
}
class RowUnit
{
    public $KDTEMPLATE;
    public $TEMPLATE;
    public $KDDOKTER;
    public $KDPRODUK;
    public $KET;
}

?>
