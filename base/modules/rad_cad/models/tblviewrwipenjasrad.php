﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewrwipenjasrad extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_pasien,no_transaksi, nama, alamat, spesialisasi, kelas, nama_kamar, kamar, masuk, 
						dokter, kd_dokter, kd_unit_kamar, kd_customer, tgl_masuk, urut_masuk, tgl_inap, kd_kasir, 
						tgl_transaksi, kd_unit, co_status, kd_user, tgl_lahir, jenis_kelamin, gol_darah, posting_transaksi, nama_unit, kd_spesial";
		$this->SqlQuery="
                    SELECT distinct pasien.kd_pasien,Transaksi.No_Transaksi,  
                    pasien.NAMA, pasien.alamat, Spesialisasi.spesialisasi, Kelas.KELAS,  
                    kamar.nama_kamar,  nginap.no_kamar AS KAMAR, 
                    transaksi.tgl_transaksi AS MASUK, dokter.nama AS DOKTER, 
                    dokter.kd_dokter, nginap.kd_unit_Kamar, Kunjungan.kd_CUstomer, 
                    Kunjungan.tgl_masuk, Kunjungan.urut_masuk, nginap.tgl_inap, 
                    transaksi.kd_Kasir, transaksi.no_transaksi, transaksi.Tgl_transaksi, 
                    transaksi.kd_unit,   TRANSAKSI.co_status, TRANSAKSI.kd_user, 
                    pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, transaksi.posting_transaksi, unit.nama_unit, nginap.kd_spesial
                    FROM (unit 
                    inner join kelas on unit.kd_kelas=kelas.kd_kelas) 
                    INNER JOIN (Pasien 
                    INNER JOIN (transaksi  
                    INNER JOIN (((( 
                    SELECT * FROM nginap 
                    WHERE nginap.tgl_keluar IsNull and nginap.akhir = '1' )nginap  
                    inner join spesialisasi on nginap.kd_spesial=spesialisasi.kd_Spesial)  
                    INNER JOIN kamar ON (nginap.no_kamar=kamar.no_kamar) 
                    AND (nginap.kd_unit_kamar=kamar.kd_unit)) 
                    INNER JOIN (kunjungan 
                    INNER JOIN dokter 
                    ON dokter.kd_dokter=KUNJUNGAN.kd_dokter)  
                    ON (kunjungan.kd_unit=nginap.kd_unit) AND (kunjungan.kd_pasien=nginap.kd_pasien) AND (kunjungan.tgl_masuk=nginap.tgl_masuk) AND (kunjungan.urut_masuk=nginap.urut_masuk))  
                    ON (transaksi.kd_pasien=nginap.kd_pasien) AND (transaksi.kd_unit=nginap.kd_unit) and (transaksi.tgl_transaksi=nginap.tgl_masuk) and (transaksi.urut_masuk=nginap.urut_masuk))  
                    ON transaksi.kd_pasien=pasien.kd_pasien) ON unit.kd_unit=nginap.kd_unit_kamar
                                ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                $row->KD_PASIEN=$rec->kd_pasien;
                $row->NO_TRANSAKSI=$rec->no_transaksi;
                $row->NAMA=$rec->nama;
                $row->ALAMAT=$rec->alamat;
                $row->SPESIALISASI=$rec-> spesialisasi;
                $row->KELAS=$rec-> kelas;
                $row->NAMA_KAMAR=$rec-> nama_kamar;
                $row->KAMAR=$rec->kamar;
                $row->MASUK=$rec->masuk;
                $row->DOKTER=$rec->dokter;
                $row->KD_DOKTER=$rec->kd_dokter;
                $row->KD_UNIT_KAMAR=$rec->kd_unit_kamar;
                $row->KD_CUSTOMER=$rec->kd_customer;
                $row->TGL_MASUK=$rec->tgl_masuk;
                $row->URUT_MASUK=$rec->urut_masuk;
                $row->TGL_INAP=$rec->tgl_inap;
                $row->KD_KASIR=$rec->kd_kasir;
                $row->TGL_TRANSAKSI=$rec->tgl_transaksi;
                $row->KD_UNIT=$rec->kd_unit;
                $row->CO_STATUS=$rec->co_status;
                $row->KD_USER=$rec->kd_user;
                $row->TGL_LAHIR=$rec->tgl_lahir;
                $row->JENIS_KELAMIN=$rec->jenis_kelamin;
                $row->GOL_DARAH=$rec->gol_darah;
                $row->POSTING_TRANSAKSI=$rec->posting_transaksi;
                $row->NAMA_UNIT=$rec->nama_unit;				
                $row->KD_SPESIAL=$rec->kd_spesial;
		return $row;
	}
}
class Rowdokter
{
        public $KD_PASIEN;
        public $NO_TRANSAKSI;
        public $NAMA;
        public $ALAMAT;
        public $SPESIALISASI;
        public $KELAS;
        public $NAMA_KAMAR;
        public $KAMAR;
        public $MASUK;
        public $DOKTER;
        public $KD_DOKTER;
        public $KD_UNIT_KAMAR;
        public $KD_CUSTOMER;
        public $TGL_MASUK;
        public $URUT_MASUK;
        public $TGL_INAP;
        public $KD_KASIR;
        public $TGL_TRANSAKSI;
        public $KD_UNIT;
        public $CO_STATUS;
        public $KD_USER;
        public $TGL_LAHIR;
        public $JENIS_KELAMIN;
        public $GOL_DARAH;
        public $POSTING_TRANSAKSI;
        public $NAMA_UNIT;
        public $KD_SPESIAL;
}

?>