<?php

class function_rad_sql extends  MX_Controller {
	
	public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = null) {
         $this->db->trans_begin();

         if($_POST['Modul']=='rwj' || $_POST['Modul']=='igd' || $_POST['Modul']=='rwi'){
          $unitasal =  $_POST['KdUnit'];
         }else{
          $unitasal=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
         }

         $KdPasien = $Params["KdPasien"];     
         $KdUnit_t = $Params["KdUnitTujuan"]; 
         $Tgl = $Params["Tgl"];
         $urut = $Params["URUT"];
         $unitasal = $Params['KdUnit'];
         $IdAsal = '';
         $list = json_decode($_POST['List']);
         $listtrdokter = json_decode($_POST['listTrDokter']);
         $KdKasirAsal = $_POST['KdKasirAsal'];
         $Kamar = $_POST['Kamar'];
         $KdSpesial = $_POST['KdSpesial'];
         $TglTransaksiAsal = $_POST['TglTransaksiAsal'];

         if($KdUnit_t=='' || $TglTransaksiAsal==''){
			//$KdUnit='41';
			
			$TglTransaksiAsal=$Tgl;
		} else{
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}

         if(substr($unitasal, 0, 1) == '1'){
             # RWI
             $IdAsal=1;
         } else if(substr($unitasal, 0, 1) == '2'){
             # RWJ
             $IdAsal=0;
         } else if(substr($unitasal, 0, 1) == '3'){
             # UGD
             $IdAsal=0;
         }else
         {
             // $IdAsal = $this->GetIdAsalPasien($unit);
         }
         $pasienBaru = 0;
         $Shift=$Params['Shift'];
         $KdTransaksi = $_POST['KdTransaksi'];
         $kunjungan_rad = $_POST['kunjungan_rad'];

         $datakunjungan = $this->db->query("select * from kunjungan where kd_pasien = '$KdPasien' and kd_unit = '$KdUnit_t' and tgl_masuk = '$Tgl' and urut_masuk = $urut");
         foreach ($datakunjungan->result() as $rec)
         {
           $KdPasien=$rec->kd_pasien;
           $KdUnit=$rec->kd_unit;
           $Tgl=$rec->tgl_masuk;
           $urut=$rec->urut_masuk;
           $KdDokter=$rec->kd_dokter;
           $Shift=$rec->shift;
           $KdCusto=$rec->kd_customer;
           $NoSJP=$rec->no_sjp;
           $JamKunjungan=$rec->jam_masuk;
         }

         $datatransaksi = $this->db->query("select * from transaksi where kd_pasien = '$KdPasien' and kd_unit = '$KdUnit' and tgl_transaksi = '$Tgl' and urut_masuk = $urut");
         foreach ($datatransaksi->result() as $rec)
         {
           $kdkasir=$rec->kd_kasir;
           $notrans=$rec->no_transaksi;
           $kdpasien=$rec->kd_pasien;
           $unit=$rec->kd_unit;
           $Tgl=$rec->tgl_transaksi;
           $urut=$rec->urut_masuk;
         }

         $simpankeunitasal='';
		if ($kunjungan_rad=='Baru')
		{
			 $datakunjungan = $this->db->query("select * from kunjungan where kd_pasien = '$KdPasien' and kd_unit = '$KdUnit_t' and tgl_masuk = '$Tgl' and urut_masuk = $urut");
	         foreach ($datakunjungan->result() as $rec)
	         {
	           $KdPasien=$rec->kd_pasien;
	           $KdUnit=$rec->kd_unit;
	           $Tgl=$rec->tgl_masuk;
	           $urut=$rec->urut_masuk;
	           $KdDokter=$rec->kd_dokter;
	           $Shift=$rec->shift;
	           $KdCusto=$rec->kd_customer;
	           $NoSJP=$rec->no_sjp;
	           $JamKunjungan=$rec->jam_masuk;
	         }
			$simpankunjunganrad = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru,$JamKunjungan);
         	if ($simpankunjunganrad == 'aya'){
            $simpanmrrad= $this->SimpanMrrad($KdPasien,$KdUnit,$Tgl,$urut);
            // echo $simpanmrrad;
            if($simpanmrrad == 'Ok'){
          		 $datatransaksi = $this->db->query("select * from transaksi where kd_pasien = '$KdPasien' and kd_unit = '$KdUnit' and tgl_transaksi = '$Tgl' and urut_masuk = $urut");
		         foreach ($datatransaksi->result() as $rec)
		         {
		           $kdkasir=$rec->kd_kasir;
		           $notrans=$rec->no_transaksi;
		           $kdpasien=$rec->kd_pasien;
		           $unit=$rec->kd_unit;
		           $Tgl=$rec->tgl_transaksi;
		           $urut=$rec->urut_masuk;
		         }
                 $hasil = $this->SimpanTransaksi($kdkasir,$notrans,$kdpasien,$unit,$Tgl,$urut);
                 if($hasil == 'sae'){
                 	if($unitasal != '' && substr($unitasal, 0, 1) =='1'){
							    # jika bersal dari rawat inap
							    $simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasir,$notrans,$unitasal,$Kamar,$KdSpesial);
      						} else{
      							$simpanunitasalinap='Ok';
      						}
                  $detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasir,$unitasal,$Shift,$unit,$kdpasien,$urut,$TglTransaksiAsal,$KdUnit,$urut);

              if($detail){
                if ($simpankeunitasal=='ya')
							   {
								  if($pasienBaru == 0){
								    # jika bukan Pasien baru/kunjungan langsung
								    $simpanunitasall = $this->SimpanUnitAsal($kdkasir,$notrans,$notrans,$KdKasirAsal,$IdAsal);
								  }else{
									 $simpanunitasall = $this->SimpanUnitAsal($kdkasir,$notrans,$notrans,$kdkasirpasien,$IdAsal);
								}								
							 }else
							 {
							     $simpanunitasall = 'Ok';
							 }

							if($simpanunitasalinap == 'Ok' && $simpanunitasall == 'Ok'){
									$str='Ok';
								} else{
									$str='error';
								}
                    }else{
                    	$str='error';
                    }

                 }else{
                    $str='error';
                 }
            } else{
                $str='error';
            }
         } else{
             $str='error';
         }
         if ($detail){
				$this->db->trans_commit();
				echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}else{
			$detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasir,$unitasal,$Shift,$unit,$kdpasien,$urut,$TglTransaksiAsal,$KdUnit,$urut);
			if($detail){
			$this->db->trans_commit();
			echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}

            
    }

    public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru,$JamKunjungan)
    {
        $strError = "";
        $data = array("kd_pasien"=>$kdpasien,
                      "kd_unit"=>$unit,
                      "tgl_masuk"=>$Tgl,
                      "kd_rujukan"=>"0",
                      "urut_masuk"=>$urut,
                      "jam_masuk"=>$JamKunjungan,
                      "kd_dokter"=>$kddokter,
                      "shift"=>$Shift,
                      "kd_customer"=>$KdCusto,
                      "karyawan"=>"0",
                      "no_sjp"=>$NoSJP,
                      "keadaan_masuk"=>0,
                      "keadaan_pasien"=>0,
                      "cara_penerimaan"=>99,
                      "asal_pasien"=>$IdAsal,
                      "cara_keluar"=>0,
                      "baru"=>$pasienBaru,
                      "kontrol"=>"0"
                   );

        $criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

        $query=_QMS_Query("select * from kunjungan where ".$criterianya)->result();

        if (count($query)==0)
        {
             $result= _QMS_insert('KUNJUNGAN', $data,$criterianya);
             if ($result)
             {
                 $strError = "aya";
             }else{
                 $strError = "teu aya";
             }            
        }else{
        
            $result=_QMS_update('KUNJUNGAN', $data,$criterianya);
            $strError = "aya";
        }
        return $strError;
    }

    public function SimpanMrrad($KdPasien,$unit,$Tgl,$urut)
   {
     $strError = "";
     $data = array("kd_pasien"=>$KdPasien,
               "kd_unit"=>$unit,
               "tgl_masuk"=>$Tgl,
               "urut_masuk"=>$urut
             );
     $cek = _QMS_Query("select * from mr_rad where kd_pasien = '".$KdPasien."' and kd_unit = '".$unit."' and tgl_masuk = '".$Tgl."' and urut_masuk = $urut")->result();
     if(count($cek)==0){
       $result = _QMS_insert('mr_rad',$data);

        if($result){
          $strError='Ok';
        } else{
          $strError='error';
        }
     } else{
     	$strError='Ok';
     }

     if($strError = 'Ok'){
       $strError1='Ok';
     } else{
       $strError1='error';
     }

     return $strError1;
   }

   public function SimpanTransaksi($kdkasir,$notrans,$kdpasien,$unit,$Tgl,$urut)
   {
      $strError = "";
      $data = array(
             "kd_kasir"=>$kdkasir,
             "no_transaksi"=>$notrans,
             "kd_pasien"=>$kdpasien,
             "kd_unit"=>$unit,
             "tgl_transaksi"=>$Tgl,
             "urut_masuk"=>$urut,
             "tgl_co"=>NULL,
             "co_status"=>0,
             "orderlist"=>NULL,
             "ispay"=>0,
             "app"=>0,
             "kd_user"=>$this->session->userdata['user_id']['id'],
             "tag"=>NULL,
             "lunas"=>0,
             "tgl_lunas"=>NULL);

       $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasir."'";

       $query=_QMS_Query("SELECT * FROM TRANSAKSI WHERE ".$criteria)->result();
        
       if (count($query)==0)
       {
         $result = _QMS_insert('TRANSAKSI', $data,$criteria);
         if($result){
         	$datanoreg = $this->db->query("select * from reg_unit where kd_pasien = '$kdpasien' and kd_unit = '$unit' and tgl_transaksi = '$Tgl' and urut_masuk = $urut");
		         foreach ($datanoreg->result() as $rec)
		         {
		           $noreg=$rec->no_register;
		         }
		         $query=_QMS_Query("INSERT INTO REG_UNIT VALUES ('$kdpasien','$unit','$noreg','$Tgl',$urut)");
		         if ($query) {
		         	$strError = "sae";
		         }else{
		         	$strError = "eror";
		         }
           		
         } else{
           $strError = "eror";
         }
       }
       else
       {
         $strError = "sae";
       }
       return $strError;

   }

   private function detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasir,$unitasal,$Shift,$unit,$kdpasien,$urut,$Tglasal,$KdUnitkunjungan,$urutkunjungan){
     $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
     $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
     $rad_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito_pk'")->row()->setting;
     $kdUser=$this->session->userdata['user_id']['id'];
     $urutradhasil=1;
     $j=0;
    for($i=0;$i<count($list);$i++){
      $kd_produk=$list[$i]->KD_PRODUK;
      $qty=$list[$i]->QTY;
      $tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
      $tgl_berlaku=$list[$i]->TGL_BERLAKU;
      if (isset($list[$i]->cito))
      {
        $cito=$list[$i]->cito;
        if($cito=='Ya')
        {$cito='1';
        $harga=$list[$i]->HARGA;
        $hargacito = (((int) $harga) * ((int)$rad_cito_pk))/100;
        $harga=((int)$list[$i]->HARGA)+((int)$hargacito);
        
        }
        else if($cito=='Tidak')
        {$cito='0';
        $harga=$list[$i]->HARGA;
        }
      } else
      {
       $cito='0';
       $harga=$list[$i]->HARGA;
      }
      $kd_tarif=$list[$i]->KD_TARIF;

      $criteria = "kd_kasir='$kdkasir' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'";

      $cekDetailTrx= _QMS_Query("SELECT * FROM detail_transaksi WHERE ".$criteria)->result();
      if (count($cekDetailTrx)==0)
      {
        // $urut = $i+1;
        $urutdetailtransaksi =  $this->db->query("select urut as urutdetail from detail_transaksi where kd_kasir = '$kdkasir' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urutdetail;
        $sql = "exec dbo.V5_insert_detail_transaksi '".$kdkasir."', '".$notrans."',".$urutdetailtransaksi.",
          '".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
          '".$tgl_berlaku."',0,0,'',".$qty.",".$harga.",".$Shift.",0,''";
        $query = _QMS_Query($sql);

        if ($query) {
          $cekkdprd = $this->db->query("SELECT * FROM detail_radfo where no_transaksi = '$notrans' and kd_kasir = '$kdkasir' and urut = $urut and tgl_transaksi = '$Tgl'")->result();
          foreach ($cekkdprd as $data) {
            $cek = _QMS_Query("select * from detail_radfo  where kd_kasir = '".$data->kd_kasir."' and no_transaksi = '".$data->no_transaksi."' and urut = ".$data->urut." and tgl_transaksi = '".$data->tgl_transaksi."' and kd_prd = '".$data->kd_prd."'")->result();
            if (count($cek) !== 0) {

            }else{
              $insertdetailfo = _QMS_Query("insert into detail_radfo values('".$data->kd_kasir."','".$data->no_transaksi."',".$data->urut.",'".$data->tgl_transaksi."','".$data->kd_prd."',".$data->qty.",".$data->qty_rsk.",".$data->harga.")");
            }
            
          }
        }
      }else{
      		$urutdetailtransaksi = $this->db->query("select urut from detail_transaksi where kd_kasir = '$kdkasir' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urut;
        $query = _QMS_Query("update detail_transaksi set qty = '$qty' 
          where kd_kasir = '$kdkasir' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk' and urut = $urutdetailtransaksi");			
      }
        if($cito==='1')
        {
         $query = _QMS_Query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasir."' and no_transaksi='".$notrans."'
         and urut=".$urut." and tgl_transaksi='".$Tgl."'");
          $query = _QMS_Query("update transaksi set cito=1 where kd_kasir='".$kdkasir."' and no_transaksi='".$notrans."'");
        } //tutup cito
	        $qsql=$this->db->query(" select * from detail_component 
	                    where kd_kasir='".$kdkasir."' 
	                      and no_transaksi='".$notrans."'
	                      and urut=".$urut."
	                      and tgl_transaksi='".$Tgl."'")->result();
	      foreach($qsql as $line){
	        $qkd_kasir=$line->kd_kasir;
	        $qno_transaksi=$line->no_transaksi;
	        $qurut=$line->urut;
	        $qtgl_transaksi=$line->tgl_transaksi;
	        $qkd_component=$line->kd_component;
	        $qtarif=$line->tarif;
	      
	      }

	      if($query){
	        $ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
	        where 
	        (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
	        kd_unit ='".$unitasal."' AND
	        kd_produk='".$kd_produk."' AND
	        tgl_berlaku='".$tgl_berlaku."' AND
	        kd_tarif='".$kd_tarif."' group by tarif")->result();
	        foreach($ctarif as $ct)
	        {
	          if($ct->jumlah != 0)
	          {
	            $trDokter = _QMS_Query("insert into detail_trdokter select '$kdkasir','".$notrans."'
	            ,'".$urut."','".$_POST['KdDokter']."','".$Tgl."',0,0,".$ct->tarif.",0,0,0 WHERE
	              NOT EXISTS (
	                SELECT * FROM detail_trdokter WHERE   
	                  kd_kasir= '$kdkasir' AND
	                  tgl_transaksi='".$Tgl."' AND
	                  urut='".$urut."' AND
	                  kd_dokter = '".$_POST['KdDokter']."' AND
	                  no_transaksi='".$notrans."'
	              )");
	          }
	        }

	        
	    }else{
				$query=true;
			}
    } //tutup perulangan

    // $cekdata = $this->db->query("select * from rad_hasil where kd_pasien = '$kdpasien' and kd_unit = '$KdUnitkunjungan' and tgl_masuk = '$Tgl' and urut_masuk = $urut")->result();
	   //      if (count($cekdata)>0) {
	   //      	foreach ($cekdata as $data) {
	   //      		$cek = _QMS_Query("select * from rad_hasil where kd_test = ".$data->kd_test." and kd_pasien = '".$data->kd_pasien."' and kd_unit = '".$data->kd_unit."' and tgl_masuk = '".$data->tgl_masuk."' and urut_masuk = ".$data->urut_masuk." and urut = ".$data->urut."")->result();
	   //      		if ($cek) {
	        			
	   //      		}else{
	   //      			$query = _QMS_Query("insert into rad_hasil 
				// 						(kd_test, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
				// 						 urut, kd_unit_asal, tgl_masuk_asal,keluhan)
				// 						 values(
				// 							".$data->kd_test.",'".$data->kd_pasien."','".$data->kd_unit."','".$data->tgl_masuk."',".$data->urut_masuk.",".$data->urut.",".$data->kd_unit_asal.",'".$data->tgl_masuk_asal."','')
				// 						");
	   //      		}
	        		
	   //      	}
	        	
	   //      }else{

	   //      }
   
    
    return $query;
  }

  	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);
		$result= _QMS_insert('UNIT_ASAL', $data);
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}
        return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result= _QMS_insert('unit_asalinap', $data);
		

		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}

	public function delete(){
		$no_transaksi=$_POST['no_tr'];
		$urut=$_POST['urut'];
		$tgl_transaksi=$_POST['tgl_transaksi'];
		$dtransaksi = _QMS_Query("delete from detail_transaksi where no_transaksi = '".$no_transaksi."' and urut=".$urut." and tgl_transaksi = '".$tgl_transaksi."'");
		if($dtransaksi){
      $dtransaksi = _QMS_Query("delete from detail_radfo  where no_transaksi = '".$no_transaksi."' and urut=".$urut." and tgl_transaksi = '".$tgl_transaksi."'");
			echo '{success:true}';
		}else{
			echo '{success:false}';
		}
	}

  public function saveTransfer(){
    $KASIR_SYS_WI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
    $Tglasal=$_POST['Tglasal'];
    $KDunittujuan=$_POST['KDunittujuan'];
    $KDkasirIGD=$_POST['KDkasirIGD'];
    $Kdcustomer=$_POST['Kdcustomer'];
    $TrKodeTranskasi=$_POST['TrKodeTranskasi'];
    $KdUnit=$_POST['KdUnit'];
    $Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
    $total = str_replace('.','',$_POST['Jumlahtotal']); 
    $Shift1=$_POST['Shift'];
    $TglTranasksitujuan=$_POST['TglTranasksitujuan'];
    $KASIRRWI=$_POST['KasirRWI'];
    $TRKdTransTujuan=$_POST['TRKdTransTujuan'];
    $_kduser = $this->session->userdata['user_id']['id'];
    $tgltransfer=date("Y-m-d");
    $tglhariini=date("Y-m-d");
    $KDalasan =$_POST['KDalasan'];
    $this->db->trans_begin();
        
    $det_query = "select * from detail_bayar where no_transaksi = '$TrKodeTranskasi' and kd_kasir = '$KDkasirIGD' and tgl_transaksi = '$tgltransfer'";
    $detailTr = $this->db->query($det_query);
    foreach ($detailTr->result() as $data) {
      $urut_detailbayar = $data->urut;
      $pay_query = _QMS_Query(" insert into detail_bayar 
          (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
          values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'1')");
    }
            
    if($pay_query)
    {
     
      $detailTrbayar = _QMS_Query(" insert into detail_tr_bayar 
        (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
        SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
        WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
                
      if($detailTrbayar)
      { 

        $statuspembayaran = _QMS_Query("
          UPDATE transaksi SET lunas = CASE WHEN (SELECT SUM(a.harga * a.qty) - max(b.bayar) 
              from detail_transaksi a 
              INNER JOIN  TRANSAKSI t ON t.kd_kasir=a.kd_kasir AND a.no_transaksi = t.no_transaksi 
              INNER JOIN (SELECT kd_kasir,no_transaksi, SUM(jumlah) AS bayar 
                  FROM detail_bayar
                  GROUP BY kd_kasir,no_transaksi) b ON t.kd_kasir=b.kd_kasir AND t.no_transaksi = b.no_transaksi 
            WHERE a.kd_kasir='$KDkasirIGD' AND a.no_transaksi = '$TrKodeTranskasi' 
            GROUP BY a.no_transaksi, a.kd_kasir  ) = 0 THEN 1 ELSE 0 END 
            WHERE kd_kasir='$KDkasirIGD' AND no_transaksi = '$TrKodeTranskasi'
        ");
        // $statuspembayaran = _QMS_Query("exec dbo.V5_updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");  
        if($statuspembayaran)
        {

          $detailtrcomponet = _QMS_Query("insert into detail_tr_bayar_component
          (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
          Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
          '$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
          FROM Detail_Component dc
          INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
          INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
          and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
          WHERE dc.Kd_Kasir = '$KDkasirIGD'
          AND dc.No_Transaksi ='$TrKodeTranskasi'
          ORDER BY dc.Kd_Component"); 
                      
          if($detailtrcomponet)
          { 
            // echo "select * from detail_transaksi where kd_kasir = '$KDkasirIGD' and no_transaksi = '$TrKodeTranskasi'";
            $getdettrans = $this->db->query("select * from detail_transaksi where kd_kasir = '$KASIRRWI' and no_transaksi = '$TRKdTransTujuan'")->result();
            foreach ($getdettrans as $data) {
              $uruttujuan = $data->urut;
              $kdtarifcus = $data->kd_tarif;
              $kdUnittranfer = $data->kd_unit;
              $kdproduktranfer = $data->kd_produk;
              $tanggalberlaku = $data->tgl_berlaku;

             

              $cekdatadet = _QMS_Query("select * from detail_transaksi where kd_kasir = '$KASIRRWI' and no_transaksi = '$TRKdTransTujuan' and urut = $uruttujuan and kd_produk = $kdproduktranfer")->result();
              // echo "select * from detail_transaksi where kd_kasir = '$KASIRRWI' and no_transaksi = '$TrKodeTranskasi' and urut = $uruttujuan and kd_produk = $kdproduktranfer";
                if (count($cekdatadet) > 0) {
                 $detailtransaksitujuan = true;
                }else{
                   $urutan = _QMS_Query("select max(urut) as urutan from detail_transaksi where kd_kasir = '$KASIRRWI' and no_transaksi = '$TRKdTransTujuan'")->row()->urutan;
                   // echo "select urutan  = max(urut) from detail_transaksi where kd_kasir = '$KASIRRWI' and no_transaksi = '$TRKdTransTujuan'";
                  if (count($urutan) !== 0) {
                    $uruttujuan = $urutan +1;
                    
                  }else{
                    $uruttujuan = 1;
                  }
                  // echo $uruttujuan;
                  $detailtransaksitujuan = _QMS_Query("
                              INSERT INTO detail_transaksi
                              (kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
                              tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
                              VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
                              '$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','1','1','',1,
                              $total,$Shift1,'0','$TrKodeTranskasi')
                              ");
                }
            }             
            
            if($detailtransaksitujuan)  
            {
              $detailcomponentujuan = _QMS_Query("
                 INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
                 select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
                 from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
                 and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");  
                            
              if($detailcomponentujuan)
              {           
                $tranferbyr = _QMS_Query("INSERT INTO transfer_bayar
                (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
                det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
                  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
                  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
                if($tranferbyr){
                      IF ( $KASIR_SYS_WI==$KASIRRWI){
                              $trkamar = _QMS_Query("INSERT INTO detail_tr_kamar VALUES
                              ('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");  
                              if($trkamar)
                              {
                                $this->db->trans_commit();
                                echo '{success:true}';
                              }else
                               {
                                $this->db->trans_rollback();
                                echo '{success:false}'; 
                               }
                          }ELSE{
                          $this->db->trans_commit();
                            echo '{success:true}';
                          
                          }
                      }
                else{ 
                    $this->db->trans_rollback();
                    echo '{success:false}'; 
                  }
              } else{ $this->db->trans_rollback();
              echo '{success:false}'; 
              }
            } else {
              $this->db->trans_rollback();
              echo '{success:false}'; 
            }
          } else {
            $this->db->trans_rollback();
            echo '{success:false}'; 
          }

        } else {
          $this->db->trans_rollback();
          echo '{success:false}'; 
        }
      } else {
        $this->db->trans_rollback();
        echo '{success:false}'; 
      }
    } else {
      $this->db->trans_rollback();
      echo '{success:false}'; 
      
    }
  }

  public function UpdatePemakaianFotoRad(){
    $notrans=$_POST['notrans'];
    $tgltransaksi = $_POST['tgltrans'];
    $urut = $_POST['uruttrans'];
    $list = json_decode($_POST['List']);
    $kdkasir = $this->db->query("select kd_kasir from detail_transaksi where no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' and urut = $urut")->row()->kd_kasir;
    for($i=0;$i<count($list);$i++){
      $kd_prd=$list[$i]->KD_PRD_RAD_FO;
      $qty =$list[$i]->QTY_RAD_FO;
      $qty_rsk =$list[$i]->QTY_RSK_RAD_FO;
      $update= _QMS_Query("update detail_radfo set qty=$qty,qty_rsk = $qty_rsk where kd_kasir = '$kdkasir' and no_transaksi  = '$notrans' and tgl_transaksi = '$tgltransaksi' and urut = $urut and kd_prd = '$kd_prd'");
    }
    echo "{success:true}";
  }

}