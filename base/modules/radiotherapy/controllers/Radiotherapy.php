<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Radiotherapy extends MX_Controller {
	protected $kd_unit 	= null; 
	protected $kd_kasir = null; 
	protected $kd_klas  = null; 
	protected $kd_user 	= ""; 
	public function __construct(){
		parent::__construct();

		$Q_unit = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_unit_radiotherapy'");
		if ($Q_unit->num_rows() > 0) {
			$this->kd_unit = $Q_unit->row()->setting;
		}
		
		$Q_kasir = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kasir_radiotherapy'");
		if ($Q_kasir->num_rows() > 0) {
			$this->kd_kasir = $Q_kasir->row()->setting;
		}

		$Q_klas = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kd_klas_radiotherapy'");
		if ($Q_klas->num_rows() > 0) {
			$this->kd_klas = $Q_klas->row()->setting;
		}

		$this->kd_user = $this->session->userdata['user_id']['id'];
	}	 

	public function index(){
		$this->load->view('main/index');            		
	}

	/*
	 *
	 *
		GET DEFAULT UNIT UNTUK GET DATA UNIT KERJA USER
		HADAD AL GOJALI
		2018-08-23
	*/
	public function getDefaultUnit(){
		if ($this->kd_unit == null) {
			$kumpulan_kdUnit=$this->db->query("SELECT kd_unit from zusers where kd_user='$kdUser'")->row()->kd_unit;
			$key="'5";
			$kata='';
			if (strpos($kumpulan_kdUnit,","))
			{
				$pisah_kata=explode(",",$kumpulan_kdUnit);
				for($i=0;$i<count($pisah_kata);$i++)
				{
					$cek_kata=stristr($pisah_kata[$i],$key);
					if ($cek_kata != '' || $cek_kata != null)
					{
						$kata=$cek_kata;
					}
				}
				
			}else
			{
				$kata= $kumpulan_kdUnit;
			}
			echo "{kd_unit:".$kata."}";
		}else{
			echo "{kd_unit:".$this->kd_unit."}";
		}
	}
	/*
	 *
	 *
		GET DEFAULT KASIR UNTUK GET DATA IDENTITAS KD KASIR ENDOSCOPY
		HADAD AL GOJALI
		2018-08-23
	*/
	public function getDefaultKasir(){
		echo "{ kd_kasir : ".$this->kd_kasir." }";
	}


	public function getProduk(){
		$kdklasproduk  = $this->kd_klas;

		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		$kdUnit=$_POST['kd_unit'];
		$criteria  = "";
		$deskripsi = $this->input->post('text');
		if ($deskripsi != null || $deskripsi != '' || strlen($deskripsi) > 0) {
			$criteria = " AND lower(produk.deskripsi) like '".strtolower($deskripsi)."%' ";
		}
		$result=$this->db->query("SELECT 'Tidak' as cito, row_number() OVER () as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
		rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
		from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
		produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
		tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
		From tarif inner join produk on produk.kd_produk = tarif.kd_produk
		inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		inner join unit on tarif.kd_unit = unit.kd_unit
		inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
		where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
		tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
		WHERE
		tarif.kd_unit in (".$kdUnit.") 
		and unit.parent = '".substr($this->kd_unit, 0, 1)."' 
		and produk.kd_klas = '".$this->kd_klas."' 
		and tarif.kd_tarif='".$row->kd_tarif."'
		and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
		and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null)
		and produk.kd_klas like '".$kdklasproduk."%'
		".$criteria."
		group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
		klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
		) as rn where rn = 1 order by rn.deskripsi asc")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).', listData:'.json_encode($result).', processResult:"SUCCESS"}';
	}


	public function save(){
		$response = array();
		$this->db->trans_begin();
		$query = false;
		// if($_POST['Modul']=='rwj' || $_POST['Modul']=='igd' || $_POST['Modul']=='rwi'){
		// }else{
		// 	$unitasal=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
		// }
		$unitasal 			 = $this->input->post('KdUnit');
		$KdUnit              = $this->input->post('KdUnitTujuan');
		$KdTransaksi         = $this->input->post('KdTransaksi');
		$KdPasien            = $this->input->post('KdPasien');
		$TglTransaksiAsal    = $this->input->post('TglTransaksiAsal');
		$NmPasien            = $this->input->post('NmPasien');
		$Ttl                 = $this->input->post('Ttl');
		$Alamat              = $this->input->post('Alamat');
		$JK                  = $this->input->post('JK');
		$GolDarah            = $this->input->post('GolDarah');
		$KdDokter            = $this->input->post('KdDokter');
		$pasienBaru          = $this->input->post("pasienBaru");
		$Tgl                 = $this->input->post('Tgl');
		$Shift               = $this->get_shift($this->input->post('KdUnitTujuan'));
		$jmlfield            = $this->input->post('JmlField');
		$jmlList             = $this->input->post('JmlList');
		$unit                = $this->input->post('KdUnit');
		$TmpNotransAsal      = $this->input->post('TmpNotransaksi');//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal         = $this->input->post('KdKasirAsal');//Kode kasir asal
		$KdCusto             = $this->input->post('KdCusto');
		$TmpCustoLama        = $this->input->post('TmpCustoLama');//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $this->input->post('NamaPesertaAsuransi');
		$NoAskes             = $this->input->post('NoAskes');
		$NoSJP               = $this->input->post('NoSJP');
		$KdSpesial           = $this->input->post('KdSpesial');
		$Kamar               = $this->input->post('Kamar');
		$tmpurut             = $this->input->post('URUT');
		$no_reg              = $this->input->post('no_reg');
		$listtrdokter        = json_decode($_POST['listTrDokter']);
		$list                = json_decode($_POST['List']);

		$kdkasirpasien = $this->unit_asal($unit, $KdUnit);
		if($pasienBaru == 0)
		{
			// $a = substr($unitasal, 0, 1);
			$pasienBaru = 'false';
			if(substr($unitasal, 0, 1) == '1'){
				# RWI
				$IdAsal=1;
			} else if(substr($unitasal, 0, 1) == '2'){
				# RWJ
				$IdAsal=0;
			} else if(substr($unitasal, 0, 1) == '3'){
				# UGD
				$IdAsal=0;
			}else{
				$IdAsal = $this->GetIdAsalPasien($unit);
			}
		} else{
			$IdAsal=2;
			$pasienBaru = 'true';
		}

		$urut = $this->get_last_urut_masuk($KdPasien,$KdUnit,$Tgl,$KdDokter);
		if ($KdTransaksi=='') {
			$notrans = $this->GetIdTransaksi($kdkasirpasien);
		}else{
			$notrans = $KdTransaksi;
		}

		$simpankeunitasal='ya';

		/* ================================================= SIMPAN DATA KUNJUNGAN ================================================= */
		if ($KdTransaksi=='') {
			$query = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
		}else{
			$query = true;  
		}
		$response['step'] = "Insert data kunjungan";
		/* ================================================= SIMPAN DATA KUNJUNGAN ================================================= */


		/* ================================================= SIMPAN DATA TRANSAKSI ================================================= */
		if (($query > 0 || $query===true) && $KdTransaksi=='') {
			$query = $this->simpantransaksi($kdkasirpasien,$notrans,$KdPasien,$KdUnit,$Tgl,$urut,$no_reg,$IdAsal);
		}else{
			$query = false;
		}
		$response['step'] = "Insert data transaksi";
		/* ================================================= SIMPAN DATA TRANSAKSI ================================================= */

		/* ================================================= SIMPAN UNIT ASAL RAWAT INAP ================================================= */
		if(substr($unitasal, 0, 1) == '1' && $KdTransaksi==''){
			# jika bersal dari rawat inap
			$query = $this->simpan_unit_asal_inap($kdkasirpasien,$notrans,$unitasal,$Kamar,$KdSpesial);
		} else{
			$query = true;
		}
		$response['step'] = "Simpan unit asal inap";
		/* ================================================= SIMPAN UNIT ASAL RAWAT INAP ================================================= */


		/* ================================================= SIMPAN UNIT ASAL ================================================= */
		if (($query > 0 || $query===true) && $KdTransaksi=='') {
			if( $pasienBaru == 0){//jika bukan Pasien baru/kunjungan langsung
				$query = $this->simpan_unit_asal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
			}else{
				$query = $this->simpan_unit_asal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
			} 
		}else{
			$query = false;
		}
		$response['step'] = "Simpan unit asal";
		/* ================================================= SIMPAN UNIT ASAL ================================================= */

		/* ================================================= SIMPAN SEMUA TRANSAKSI ================================================= */
		// if ($query > 0 || $query===true) {
		$query = $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal);
		// }else{
			// $query = false;
		// }
		$response['step'] = "Insert semua data transaksi";
		/* ================================================= SIMPAN SEMUA TRANSAKSI ================================================= */

		if ($query > 0 || $query===true) {
			$this->db->trans_commit();
			$response['status'] = true;
			$response['success']  = true;
			$response['notrans']  = $notrans;
			$response['kdPasien'] = $KdPasien;
			$response['kdkasir']  = $kdkasirpasien;
			$response['tgl']      = $Tgl;
			$response['urut']     = $urut;
			$response['noreg']    = $no_reg;
		}else{
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		$this->db->close();
		echo json_encode($response);die;

	}

	private function simpan_unit_asal_inap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$result = "";
		$data = array(
			"kd_kasir"     => $kdkasirpasien,
			"no_transaksi" => $notrans,
			"kd_unit"      => $KdUnit,
			"no_kamar"     => $Kamar,
			"kd_spesial"   => $KdSpesial
		);
		$result=$this->db->insert('unit_asalinap',$data);
		if ($result){
			$result = true;
		}else{
			$result = false;
		}  
		return $result;
	}

	private function simpan_unit_asal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$result = "";
		$data   = array(
			"kd_kasir"          => $kdkasirpasien,
			"no_transaksi"      => $notrans,
			"no_transaksi_asal" => $TmpNotransAsal,
			"kd_kasir_asal"     => $KdKasirAsal,
			"id_asal"           => $IdAsal
		);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		if ($result){
			$result = true;
		}else{
			$result = false;
		}
        return $result;
	}
	
    public function simpantransaksi($kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut,$no_reg,$IdAsal)
    {            
        $kdpasien;
		$unit;
		$Tgl;
		$result = "";
		$kdUser   = $this->session->userdata['user_id']['id'];
		$data     = array("kd_kasir" 		=> $kdkasirasalpasien,
						"no_transaksi"      => $notrans,
						"kd_pasien"         => $kdpasien,
						"kd_unit"           => $KdUnit,
						"tgl_transaksi"     => $Tgl,
						"urut_masuk"        => $Schurut,
						"tgl_co"            => NULL,
						"co_status"         => "False",
						"orderlist"         => NULL,
						"ispay"             => "False",
						"app"               => "False",
						"kd_user"           => "0",
						"tag"               => NULL,
						"lunas"             => "False",
						"tgl_lunas"         => NULL,
						"posting_transaksi" => "False");
        $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirasalpasien."'";
        $this->load->model("general/tb_transaksi");
        $this->tb_transaksi->db->where($criteria, null, false);
        $query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
        
        if ($query[1]==0)
        {          
		   $result =$this->db->query("INSERT into transaksi (kd_kasir,no_transaksi,kd_pasien,kd_unit,tgl_transaksi,urut_masuk,tgl_co,co_status,orderlist,ispay,app,kd_user,tag,lunas,tgl_lunas,posting_transaksi)
										values ('$kdkasirasalpasien','$notrans','$kdpasien','$KdUnit','$Tgl',$Schurut,NULL,'False',NULL,'False','False','$kdUser',NULL,'False',NULL,'True')");
            if($result==1){
            	$yearnow = date("Y");
            	if ($no_reg === '') {
            		if ($IdAsal == 0) {
            			$tmpawalreg = 'FRWJ';
            		}elseif ($IdAsal == 1) {
            			$tmpawalreg = 'FRWI';
            		}else{
            			$tmpawalreg = 'FRRD';
            		}
            		$cekno = $this->db->query("SELECT no_register from reg_unit 
										where kd_unit = '$KdUnit' and no_register like '$tmpawalreg%' AND date_part('year',TGL_TRANSAKSI) = '$yearnow'
										order by NO_REGISTER desc limit 1")->result();
            		if (count($cekno) > 0) {
            			foreach ($cekno as $data) {
            				$tmp1 = $data->no_register;
            			}
            			$tmpsplit = explode($KdUnit, $tmp1);
            			$tmpdata = $tmpsplit[1]+1;
            			$retVal = str_pad($tmpdata, 6, "0", STR_PAD_LEFT);
            			$no_reg_real = $tmpawalreg.$KdUnit.$retVal;
            			$this->tmpnoreg = $no_reg_real;
            			$result = $this->db->query("INSERT into reg_unit values ('$kdpasien','$KdUnit','$no_reg_real','$Tgl','',$Schurut)");
            		}else{
            			$no_reg_real = $tmpawalreg.$KdUnit.'000001';
            			$result = $this->db->query("INSERT into reg_unit values ('$kdpasien','$KdUnit','$no_reg_real','$Tgl','',$Schurut)");
            		}

            	}
				$result = true;
			} else{ 
				$result = false;
			}
        }
        return $result;
    }

	private function GetIdTransaksi($kdkasirpasien)
	{
		$no = 1;
		$res = $this->db->query("SELECT counter from kasir where kd_kasir = '$kdkasirpasien'");
		if ($res->num_rows() > 0) {
			$no = $res->row()->counter;
			$retVal = $no+1;
		}

		$update = $this->db->query("UPDATE kasir set counter=$retVal where kd_kasir='$kdkasirpasien'");
		$res    = $this->db->query("SELECT max(no_transaksi) as nomax from transaksi where kd_kasir = '$kdkasirpasien'");
		foreach ($res->result() as $data) {
			$tmpnomax = $data->nomax;
		}

		if (strlen($retVal) == 1) {
			$retValreal = "000000".$retVal;

		}else if (strlen($retVal) == 2){
			$retValreal = "00000".$retVal;
		}else if (strlen($retVal) == 3){
			$retValreal = "0000".$retVal;
		}else if (strlen($retVal) == 4){
			$retValreal = "000".$retVal;
		}else if (strlen($retVal) == 5){
			$retValreal = "00".$retVal;
		}else if (strlen($retVal) == 6){
			$retValreal = "0".$retVal;
		}else{
			$retValreal = $retVal;
		}
		return $retValreal;
    }

	private function unit_asal($kdUnit_asal, $KdUnit_tujuan)
	{	
		$query = "";
		$query = $this->db->query("SELECT * From asal_pasien Where left(kd_unit,1 ) =  '".substr($kdUnit_asal, 0, 1)."'");
		if ($query->num_rows() > 0) {
			return $this->get_kode_kasir($KdUnit_tujuan, $query->row()->kd_asal);
		}else{
			return false;
		}
	}


	private function get_kode_kasir($KdUnit_tujuan,$cKdUnitAsal)
	{
		$query = "";
		$query = $this->db->query("SELECT * From kasir_unit Where kd_unit='".$KdUnit_tujuan."' and kd_asal= '".$cKdUnitAsal."'");
		if ($query->num_rows() > 0) {
			return $query->row()->kd_kasir;
		}else{
			$query = $this->db->query("SELECT setting From sys_setting where key_data = 'default_kasir_endoscopy'");
			if ($query->num_rows() > 0) {
				return $query->row()->setting;
			}else{
				return false;
			}
		}
	}

	private function get_last_urut_masuk($KdPasien,$KdUnit,$Tgl,$Dokter){
		$urut = 0;
		$query = $this->db->query("SELECT max(urut_masuk) as urut_masuk from kunjungan 
							where kd_pasien='".$KdPasien."' 
								and kd_unit='$KdUnit'
								and tgl_masuk='".$Tgl."'");
		if ($query->num_rows() > 0) {
			$urut = $query->row()->urut_masuk + 1;
		}
		return $urut;
	}

	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
	{
		$result = false;
		$strError = "";
		$JamKunjungan = date('h:i:s');
		$jammasuk = '1900-01-01 '.$JamKunjungan;
		$data = array("kd_pasien"=>$kdpasien,	
                       "kd_unit"=>$unit,
                       "tgl_masuk"=>$Tgl,
                       "kd_rujukan"=>"0",
                       "urut_masuk"=>$urut,
                       "jam_masuk"=>$jammasuk,
                       "kd_dokter"=>$kddokter,
                       "shift"=>$Shift,
                       "kd_customer"=>$KdCusto,
                       "karyawan"=>"0",
					  "no_sjp"=>$NoSJP,
					  "keadaan_masuk"=>0,
					  "keadaan_pasien"=>0,
					  "cara_penerimaan"=>99,
					  "asal_pasien"=>$IdAsal,
					  "cara_keluar"=>0,
					  "baru"=>$pasienBaru,
					  "kontrol"=>"0"
					  );

		$criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

		$query = $this->db->query("select * from kunjungan where ".$criterianya);
		if (count($query->result())==0)
		{
			$result=$this->db->query("INSERT into kunjungan (kd_pasien,kd_unit,tgl_masuk,kd_rujukan,urut_masuk,jam_masuk,kd_dokter,shift,kd_customer,karyawan,no_sjp,
														keadaan_masuk, keadaan_pasien, cara_penerimaan, asal_pasien, cara_keluar, baru, kontrol) values
														('$kdpasien','$unit', '$Tgl','0','$urut','$jammasuk','$kddokter','$Shift','$KdCusto','0','$NoSJP',
														0,0,99,'$IdAsal',0,$pasienBaru,'0')");
			//echo $result;
			if ($result==1)
			{
				$result = true;				
			}else{	
				$result = false;		
				 }
		}else{
			$result=$this->db->query("UPDATE kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto' where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut." ");
			$result = true;		
		}
		return $result;
	}

	private function detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal){
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 // $rad_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito_pk'")->row()->setting;
		 
		 $kdUser=$this->session->userdata['user_id']['id'];
		 $urutlabhasil=1;
		 $j=0;
		// echo 'a';die;
		 for($i=0;$i<count($list);$i++){
			// echo $i;
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=$list[$i]->QTY;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			$harga=$list[$i]->HARGA;
			$cito=$list[$i]->cito;
			if($cito=='Ya')
			{
				$cito='1';
			}else{
					$cito='0';
				}
			$kd_tarif=$list[$i]->KD_TARIF;
			$cekDetailTrx=$this->db->query("SELECT * from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'");

			if (count($cekDetailTrx->result())==0)
			{
				$urutdetailtransaksi = $this->db->query("SELECT geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
				//insert detail_transaksi
				$query = $this->db->query("SELECT insert_detail_transaksi
				(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
					'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
					'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false'
				)
				");

				if ($query) {
					$cekkdprd = $this->db->query("SELECT * FROM rad_fitem fi 
														     INNER JOIN (rad_fo fo INNER JOIN Rad_JnsFilm jf ON fo.Kd_JnsFilm = jf.Kd_JnsFilm) ON fi.kd_prd = fo.kd_prd
														     WHERE fi.kd_produk = '$kd_produk' AND jf.Jns_Bhn = 1")->result();
					foreach ($cekkdprd as $data) {
						$kd_prdradfo = $data->kd_prd;
						$hargaradfo = $data->harga;
						$cek = $this->db->query("select * from detail_radfo  where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and urut = $urutdetailtransaksi and tgl_transaksi = '$Tgl' and kd_prd = '$kd_prdradfo'")->result();
						if (count($cek) !== 0) {

						}else{
							$insertdetailfo = $this->db->query("insert into detail_radfo values('$kdkasirpasien','$notrans',$urutdetailtransaksi,'$Tgl','$kd_prdradfo',0,0,$hargaradfo)");
						}						
					}
				}
			}
			else
			{
				$urutdetailtransaksi = $this->db->query("SELECT urut from detail_transaksi where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk'")->row()->urut;
				$query = $this->db->query("update detail_transaksi set qty = '$qty' 
					where kd_kasir = '$kdkasirpasien' and no_transaksi = '$notrans' and tgl_transaksi = '$Tgl' and kd_produk = '$kd_produk' and urut = $urutdetailtransaksi");
				// $query=false;
			}
			if($cito==='1')
			{
			 $query = $this->db->query("update detail_transaksi set cito=1, harga = $harga where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'
			 
			 "); 
			  $query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 
			 "); 
			}else{
				$query = $this->db->query("update detail_transaksi set cito=0, harga = $harga where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'
			 
			 "); 
			  $query = $this->db->query("update transaksi set cito=0 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
			 
			 ");
			}
			
			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir;
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;
				$qtgl_transaksi=$line->tgl_transaksi;
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
			
			}

			if($qsql){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."') AND
				kd_unit ='".$list[$i]->kd_unit."' AND
				kd_produk='".$kd_produk."' AND
				tgl_berlaku='".$tgl_berlaku."' AND
				kd_tarif='".$kd_tarif."' group by tarif")->result();
				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into detail_trdokter select '$kdkasirpasien','".$notrans."'
						,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '$kdkasirpasien' AND
									tgl_transaksi='".$qtgl_transaksi."' AND
									urut='".$qurut."' AND
									kd_dokter = '".$_POST['KdDokter']."' AND
									no_transaksi='".$notrans."'
							)");
					}
				}				
			}else{
				$query=true;
			}
		}
		return $query;
	}


	public function delete()
	{
		$no_transaksi  = $this->input->post('no_tr');
		$urut          = $this->input->post('urut');
		$tgl_transaksi = $this->input->post('tgl_transaksi');

		if ($no_transaksi === '') {
			echo "{success:true,tmp:'kosong'}";
		}else{
			$dtransaksi = $this->db->query("DELETE from detail_transaksi where no_transaksi = '".$no_transaksi."' and urut=".$urut." and tgl_transaksi = '".$tgl_transaksi."'");
			if($dtransaksi){					 
				 echo "{success:true,tmp:'ada'}";					
			}else{
				echo '{success:false}';
			}
		}
		
	}

	public function getProdukList(){
			$kdklasproduk  = $this->kd_klas;
			$kdjasadok  = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 	$kdjasaanas = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
			$row=$this->db->query("SELECT kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
			if(isset($_POST['penjas'])){
				$penjas=$_POST['penjas'];
				if ($penjas=='langsung'){
					$kdUnit=$this->db->query("SELECT setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
				}
				else{
					$kdUnit=$_POST['kd_unit'];
				}
			} else{
				$kdUnit=$this->db->query("SELECT setting from sys_setting where key_data = 'default_unit_endoscopy'")->row()->setting;
			}
			
			$result="SELECT row_number() OVER () as rnum,rn.jumlah, rn.manual,rn.kp_produk, rn.kd_kat, rn.kd_klas, rn.klasifikasi, rn.parent,
			rn.kd_tarif, rn.kd_produk, rn.deskripsi,rn.kd_unit,rn.nama_unit,(rn.tglberlaku) as tgl_berlaku,rn.tarifx as harga,rn.tgl_berakhir, 1 as qty
			from(Select produk.manual,produk.kp_produk,produk.kd_kat, produk.kd_klas,klas_produk.klasifikasi, klas_produk.parent,tarif.kd_tarif,tarif.tgl_berakhir,
			produk.kd_produk,produk.deskripsi,tarif.kd_unit,unit.nama_unit,max (tarif.tgl_berlaku) as tglberlaku,tr.jumlah,
			tarif.tarif as tarifx,row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
			From tarif inner join produk on produk.kd_produk = tarif.kd_produk
			inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
			inner join unit on tarif.kd_unit = unit.kd_unit
			inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
			left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component
			where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON
			tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
			Where tarif.kd_unit = '".$kdUnit."' 
			and tarif.kd_tarif='".$row->kd_tarif."'
			and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
			and (tarif.tgl_berakhir >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null)
			and produk.kd_klas like '".$kdklasproduk."%' and produk.kp_produk like '".$_POST['text']."%'
			group by tarif.tgl_berlaku, produk.kd_produk,produk.manual, produk.kp_produk,tarif.kd_unit, produk.kd_kat, produk.kd_klas,tr.jumlah,
			klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
			) as rn where rn = 1 order by rn.deskripsi asc";
			// echo $result;
			$jsonResult = array();
			$data       = array();		
			$result     = $this->db->query($result)->row();
			if ($result) {
				$data['deskripsi'] 		= $result->deskripsi;
				$data['kd_produk'] 	 	= $result->kd_produk;
				$data['kp_produk'] 	 	= $result->kp_produk;
				$data['kd_tarif'] 	 	= $result->kd_tarif;
				$data['kd_unit'] 	 	= $result->kd_unit;
				$data['nama_unit'] 	 	= $result->nama_unit;
				$data['rn'] 	 		= $result->rnum;
				$data['rnum'] 	 		= $result->rnum;
				$data['harga'] 	 		= $result->harga;
				$data['tgl_berakhir'] 	= $result->tgl_berakhir;
				$data['tgl_berlaku'] 	= $result->tgl_berlaku;
				$text = strtolower($data['deskripsi']);
				if (stripos($text, "konsul") !== false) {
					$data['status_konsultasi'] 	= true;
				}else{
					$data['status_konsultasi'] 	= false;
				}
				$jsonResult['processResult'] =   'SUCCESS';
			}else{
		    	$jsonResult['processResult'] 	= 'FAILED';
		    	$data = null;
			}
	    	$jsonResult['listData'] 		= $data;
	    	echo json_encode($jsonResult);
	}

	public function save_pembayaran(){
		$kdKasir      = $_POST['kdKasir'];
		$notransaksi  = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift        = $this->get_shift($_POST['kdUnit']); //update maya
		// $shift        = $this->get_shift($this->get_unit($_POST['kdUnit'])->row()->kd_bagian); //update maya
		$flag         = $_POST['Flag'];
		$tglbayar     = $_POST['TglBayar'];//date('Y-m-d');
		$list         = $_POST['List'];
		$jmllist      = $_POST['JmlList'];
		$_kdunit      = $_POST['kdUnit'];
		$_Typedata    = $_POST['Typedata'];
		$_kdpay       = $_POST['bayar'];
		$bayar        = $_POST['Totalbayar'];
		$total        = str_replace('.','',$bayar); 
		$_kduser      = $this->session->userdata['user_id']['id'];
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where kd_kasir = '$kdKasir' and  no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0){
			$urut_detailbayar = 1;
		}  else {
			foreach($det_query->result() as $det){
				$urut_detailbayar = $det->urutan+1;
			}
		}

		$a = explode("##[[]]##",$list);
		if(count($a) > 1)
		{
			for($i=0;$i<=count($a)-1;$i++){				
				$b = explode("@@##$$@@",$a[$i]);
				for($k=0;$k<=count($b)-1;$k++)
				{
					$_kdproduk = $b[1];
					$_qty      = $b[2];
					$_harga    = $b[3];
					$_urut     = $b[5];
					
					if($_Typedata == 0){
					 $harga = $b[6];
					}else if($_Typedata == 1){
						$harga = $b[8];	
					}else if ($_Typedata == 3){
						$harga = $b[7];	
					}
				}
			   $urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
					
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");

            //INSERT KE TABEL DETAIL_TR_BAYAR
             $pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$tgltransaksi."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',
			 ".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();
					}
				}
		else
		{
			
			$b = explode("@@##$$@@",$list);
			for($k=0;$k<=count($b)-1;$k++)
			{
				
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];
				
				if($_Typedata == 0){
					$harga = $b[6];
				}
				if($_Typedata == 1){
					$harga = $b[8];	
				}
				if ($_Typedata == 3){
					$harga = $b[7];	
				}
			
				$urut = $this->db->query("SELECT geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
				
			}
			$pay_query = $this->db->query("SELECT inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");
		
			$pembayaran = $this->db->query("SELECT insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();
		}
		$query_balance_det_trans=$this->db->query("SELECT sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'")->row()->jumlah;
		$query_balance_det_tr_bayar=$this->db->query("SELECT sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'")->row()->jumlah;
		if ($query_balance_det_trans == $query_balance_det_tr_bayar){
			$query_ubah_co_status = $this->db->query(" UPDATE transaksi set co_status='true' ,  tgl_co='".$tglbayar."' where no_transaksi='".$notransaksi ."' and kd_kasir='$kdKasir'");					
		}	
		if($pembayaran){
			echo '{success:true, no_transaksi:"'.$notransaksi.'", kd_kasir:"'.$kdKasir.'"}';	
		}
		else{
			echo '{success:false}';	
		}
	}

	public function save_transfer()
	{	
		$KASIR_SYS_WI       = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal            = $_POST['Tglasal'];
		$KDunittujuan       = $_POST['KDunittujuan'];
		$KDkasirIGD         = $_POST['KDkasirIGD'];
		$Kdcustomer         = $_POST['Kdcustomer'];
		$TrKodeTranskasi    = $_POST['TrKodeTranskasi'];
		$KdUnit             = $_POST['KdUnit'];
		$Kdpay              = $this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;
		$total              = str_replace('.','',$_POST['Jumlahtotal']); 
		// $Shift1              = $this->get_shift($this->get_unit($_POST['KdUnit'])->row()->kd_bagian); //update maya
		$Shift1             = $this->get_shift($_POST['KdUnit']); //update maya
		$TglTranasksitujuan = $_POST['TglTranasksitujuan'];
		$KASIRRWI           = $_POST['KasirRWI'];
		$TRKdTransTujuan    = $_POST['TRKdTransTujuan'];
		$_kduser            = $this->session->userdata['user_id']['id'];
		$tgltransfer        = $_POST['TglTransfer'];//date("Y-m-d");
		$tglhariini         = date("Y-m-d");
		$KDalasan           = $_POST['KDalasan'];
		$kd_pasien          = $_POST['KdpasienIGDtujuan'];

		//memperoleh nilai kd_unit_kamar pasien terakhit menginap
		$this->db->trans_begin();
				
		$det_query   = "SELECT COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '".$TrKodeTranskasi."' and kd_kasir= '".$KDkasirIGD."' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		}
	
		$pay_query = $this->db->query(" INSERT into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 

					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
		if($pay_query) //&& $pay_query_SQL)
		{
			$detailTrbayar = $this->db->query("	INSERT into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");		
			if($detailTrbayar) //&& $detailTrbayar_SQL)
			{	
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				if($statuspembayaran)
				{
					$detailtrcomponet = $this->db->query("INSERT into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");	
					if($detailtrcomponet) //&& $detailtrcomponet_SQL)
					{			
						$urutquery ="SELECT max(urut) as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' and kd_kasir = '$KASIRRWI'";
						$resulthasilurut = $this->db->query($urutquery)->row()->urutan;
						if($resulthasilurut <= 0)
						{
							$uruttujuan=1;
						}else
						{							
							$uruttujuan = $resulthasilurut + 1;
						}
												
						$getkdtarifcus=$this->db->query("SELECT getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
															
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();

						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
												
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();
						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlakuunit;
							
						}

						if($tanggalberlaku=='') //&& $tanggalberlaku_SQL=='')
						{
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
							exit;
						}

						$kd_unit_tr = $this->db->query("SELECT kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 't'")->result();
							if (count($kd_unit_tr)==0)
							{
								$data = array(
									'kd_kasir'      => $KASIRRWI,
									'no_transaksi'  => $TRKdTransTujuan,
									'urut'          => $uruttujuan,
									'tgl_transaksi' => $tgltransfer,
									'kd_user'       => $_kduser,
									'kd_tarif'      => $kdtarifcus,
									'kd_unit_tr'    => $KDunittujuan,
									'kd_produk'     => $kdproduktranfer,
									'kd_unit'       => $KdUnit,
									'tgl_berlaku'   => $tanggalberlaku,
									'charge'        => 'true',
									'adjust'        => 'true',
									'folio'         => 'E',
									'qty'           => 1,
									'harga'         => $total,
									'shift'         => $Shift1,
									'tag'           => 'false',
									'no_faktur'     => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							}else
							{
								$kd_unit_tr = $this->db->query("SELECT kd_unit_kamar from nginap 
												where kd_pasien = '$kd_pasien' 
													and kd_unit='$KDunittujuan' 
													and tgl_masuk='$TglTranasksitujuan' 
													and akhir = 't'")->row()->kd_unit_kamar;
								$data = array(
								   'kd_kasir' => $KASIRRWI,
								   'no_transaksi' => $TRKdTransTujuan,
								   'urut' => $uruttujuan,
								   'tgl_transaksi' => $tgltransfer,
								   'kd_user' => $_kduser,
								   'kd_tarif' => $kdtarifcus,
								   'kd_produk' => $kdproduktranfer,
								   'kd_unit' => $KdUnit,
								   'kd_unit_tr' => $kd_unit_tr,
								   'tgl_berlaku' => $tanggalberlaku,
								   'charge' => 'true',
								   'adjust' => 'true',
								   'folio' => 'E',
								   'qty' => 1,
								   'harga' => $total,
								   'shift' => $Shift1,
								   'tag' => 'false',
								   'no_faktur' => $TrKodeTranskasi
								);
								$detailtransaksitujuan = $this->db->insert('detail_transaksi', $data);
							}
						
						
						if($detailtransaksitujuan) //&& $detailtransaksitujuan_SQL)	
						{
							$detailcomponentujuan = $this->db->query
							("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
							   select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
							   from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
							   and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");						  
							if($detailcomponentujuan) //&& $detailcomponentujuan_SQL)
							{ 			  	
								$tranferbyr = $this->db->query("INSERT INTO transfer_bayar
								(kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
								det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
								  values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
								  '$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								if($tranferbyr){
									$query_balance_det_trans=$this->db->query("SELECT sum(harga * qty) as jumlah from detail_transaksi where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									$query_balance_det_tr_bayar=$this->db->query("SELECT sum(jumlah) as jumlah from detail_tr_bayar where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'")->row()->jumlah;
									if ($query_balance_det_trans == $query_balance_det_tr_bayar){
										$query_ubah_co_status = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".$tgltransfer."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasirIGD'");			
							
									}
											IF ($KASIR_SYS_WI==$KASIRRWI){
															$trkamar = $this->db->query("INSERT INTO detail_tr_kamar VALUES
															('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."','".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
															if($trkamar) //&& $trkamar_SQL)
															{
																$this->db->trans_commit();
																//$this->dbSQL->trans_commit();
																echo '{success:true, no_transaksi : "'.$TrKodeTranskasi.'", kd_kasir:"'.$KDkasirIGD.'", }';
															}else
															 {
															  $this->db->trans_rollback();
															  //$this->dbSQL->trans_rollback();
															  echo '{success:false}';	
															 }
													}else{
													$this->db->trans_commit();
													//$this->dbSQL->trans_commit();
												    echo '{success:true, no_transaksi : "'.$TrKodeTranskasi.'", kd_kasir:"'.$KDkasirIGD.'", }';
													
													}
											}
								else{ 
										$this->db->trans_rollback();
										//$this->dbSQL->trans_rollback();
										echo '{success:false}';	
									}
							} else{ 
								//$this->dbSQL->trans_rollback();
								$this->db->trans_rollback();
								echo '{success:false}';	
							}
						} else {
							$this->db->trans_rollback();
							//$this->dbSQL->trans_rollback();
							echo '{success:false}';	
						}
					} else {
						$this->db->trans_rollback();
						//$this->dbSQL->trans_rollback();
						echo '{success:false}';	
					}

				} else {
					$this->db->trans_rollback();
					//$this->dbSQL->trans_rollback();
					echo '{success:false}';	
				}
			} else {
				$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				echo '{success:false}';	
			}
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			echo '{success:false}';	
			
		}		
	}

	/*-----------------------------------Function Save Transfer Di Pindahakan Dari Function Kasir Penunjang ------------------------------------------
	 Oleh HD-HT
	 TGL 24 Februari 2017
	 Di Madiun
	*/
	private function get_shift($modul){
		$kd_bagian = $this->db->query("SELECT kd_bagian FROM unit where kd_unit = '".$modul."'")->row()->kd_bagian;
		$sqlbagianshift = $this->db->query("SELECT shift FROM bagian_shift  where kd_bagian='".$kd_bagian."'")->row()->shift;
		$lastdate       = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as last_update FROM bagian_shift   where kd_bagian='".$kd_bagian."'")->row()->last_update;


		// $sqlbagianshift = $this->db->query("SELECT shift FROM rwi_shift  where kd_unit='".$modul."'")->row()->shift;
		// $lastdate = $this->db->query("SELECT to_char(last_update,'YYYY-mm-dd') as last_update FROM rwi_shift   where kd_unit='".$modul."'")->row()->last_update;
		
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
		return $sqlbagianshift2;
	}

	private function get_unit($kd_unit){
		$query = false;
		$this->db->select("*");
		$this->db->from("unit");
		$this->db->where( array( 'kd_unit' => $kd_unit, ) );
		$query = $this->db->get();
		return $query;
	}


	public function gettotalbayar(){
		$res = $this->db->query("SELECT (select sum(harga*qty)from detail_transaksi where no_transaksi = '".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."') as totaltagihan,
								(select sum(jumlah) from detail_bayar where no_transaksi = '".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."') as jumlah");
		$totaltagihan=0;
		$jumlah=0;
		$sisabayar=0;
		if(count($res->result())>0){
			$totaltagihan=$res->row()->totaltagihan;
			if($res->row()->jumlah == NULL){
				$jumlah=0;
				$sisabayar=$res->row()->totaltagihan;
			} else{
				$jumlah=$res->row()->jumlah;
				$sisabayar=$res->row()->totaltagihan-$res->row()->jumlah;
			}
		}				
		echo "{success:true,totaltagihan:'".$totaltagihan."',jumlah:'".$jumlah."',sisabayar:'".$sisabayar."'}";					
	}

	public function get_setting_protokol_radiasi(){
		 $query=$this->db->query("SELECT * from setting_item_protokol order by kd_test ASC")->result();
		 echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}
	public function get_data_protokol_radioterapi(){
		$no_transaksi 	= $this->input->post('no_transaksi');
		$kd_kasir  		= $this->input->post('kd_kasir');
		$kd_unit  		= "";
		$urut_masuk		= "";
		$tgl_masuk		= "";
		$query = $this->get_data('*', 'transaksi',  array( 'no_transaksi' => $no_transaksi, 'kd_kasir' => $kd_kasir ) );
		if ($query->num_rows() > 0) {
			$kd_unit 	= $query->row()->kd_unit;
			$tgl_masuk 	= $query->row()->tgl_transaksi;
			$urut_masuk	= $query->row()->urut_masuk;
		}

		 $query=$this->db->query("SELECT * from protokol_radiasi_radioterapi a left join dokter b on a.kd_dokter=b.kd_dokter WHERE 
		 	a.kd_pasien='".$this->input->post('kd_pasien')."'
		 	and a.kd_unit='".$kd_unit."'
		 	and a.urut_masuk='".$urut_masuk."'
		 	and a.tgl_masuk='".$tgl_masuk."'
		 	")->result();
		 echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}
	public function get_grid_resume(){
		$kd_pasien 	 	= $this->input->post('kd_pasien');
		$no_transaksi 	= $this->input->post('no_transaksi');
		$kd_kasir  		= $this->input->post('kd_kasir');
		$kd_unit  		= "";
		$urut_masuk		= "";
		$tgl_masuk		= "";
		$query = $this->get_data('*', 'transaksi',  array( 'no_transaksi' => $no_transaksi, 'kd_kasir' => $kd_kasir ) );
		if ($query->num_rows() > 0) {
			$kd_unit 	= $query->row()->kd_unit;
			$tgl_masuk 	= $query->row()->tgl_transaksi;
			$urut_masuk	= $query->row()->urut_masuk;
		}

		$query=$this->db->query("SELECT
			a.item_test,
			jml,
			tgl_rencana AS tgl_rencana_tindakan 
		FROM
			setting_item_protokol a
			LEFT JOIN (SELECT distinct(kd_test), jml, tgl_rencana FROM resume_protokol where kd_pasien = '".$kd_pasien."' AND kd_unit = '".$kd_unit."' AND tgl_masuk = '".$tgl_masuk."' AND urut_masuk = '".$urut_masuk."') as b ON b.kd_test = a.kd_test
		where a.kd_test <> '0'")->result();

		echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}

	private function get_data($select, $table, $criteria){
		$this->db->select($select);
		$this->db->where($criteria);
		$this->db->from($table);
		return $this->db->get();
	}

	public function savedataprotokolradiasi(){
		$now=date("Y-m-d");
		$kd_pasien 		= $this->input->post('kd_pasien');
		$no_transaksi 	= $this->input->post('no_transaksi');
		$kd_kasir  		= $this->input->post('kd_kasir');
		$kd_unit  		= "";
		$urut_masuk		= "";
		$tgl_masuk		= "";
		$query = $this->get_data('*', 'transaksi',  array( 'no_transaksi' => $no_transaksi, 'kd_kasir' => $kd_kasir ) );
		if ($query->num_rows() > 0) {
			$kd_unit 	= $query->row()->kd_unit;
			$tgl_masuk 	= $query->row()->tgl_transaksi;
			$urut_masuk	= $query->row()->urut_masuk;
		}

		$this->db->query("DELETE from protokol_radiasi_radioterapi WHERE kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_masuk."' and urut_masuk='".$urut_masuk."' ");

		$list = json_decode($_POST['List']);
		/*var_dump(json_encode($list));
		die();*/
		$dataInsert = array(
						'kd_pasien'                     => $kd_pasien,
						'kd_unit'                       => $kd_unit,
						'tgl_masuk'                     => $tgl_masuk,
						'urut_masuk'                    => $urut_masuk,
						'kd_diagnosa_lengkap'           => $this->input->post('kd_diagnosa_lengkap'),
						'stadium_lengkap'               => $this->input->post('stadium_lengkap'),
						'perlu_dilakukan_1'             => $this->input->post('perlu_dilakukan_1'),
						'perlu_dilakukan_2'             => $this->input->post('perlu_dilakukan_2'),
						'perlu_dilakukan_3'             => $this->input->post('perlu_dilakukan_3'),
						'perlu_dilakukan_desc'          => $this->input->post('perlu_dilakukan_desc'),
						'kd_diag_dilapor_kembali'       => $this->input->post('kd_diag_dilapor_kembali'),
						'stadium_dilapor_kembali'       => $this->input->post('stadium_dilapor_kembali'),
						'rencana_pengobatan_1'          => $this->input->post('rencana_pengobatan_1'),    
						'rencana_pengobatan_2'          => $this->input->post('rencana_pengobatan_2'),
						'rencana_pengobatan_3'          => $this->input->post('rencana_pengobatan_3'),
						'rencana_pengobatan_4'          => $this->input->post('rencana_pengobatan_4'),
						'rencana_pengobatan_5'          => $this->input->post('rencana_pengobatan_5'),
						'rencana_pengobatan_other'      => $this->input->post('rencana_pengobatan_other'),
						'is_radiasi_eksterna'           => $this->input->post('is_radiasi_eksterna'),
						'teknik_radiasi_eksterna_1'     => $this->input->post('teknik_radiasi_eksterna_1'),
						'teknik_radiasi_eksterna_2'     => $this->input->post('teknik_radiasi_eksterna_2'),
						'teknik_radiasi_eksterna_3'     => $this->input->post('teknik_radiasi_eksterna_3'),
						'teknik_radiasi_eksterna_4'     => $this->input->post('teknik_radiasi_eksterna_4'),
						'total_dosis_eksterna'          => $this->input->post('total_dosis_eksterna'),
						'jml_fraksi_dosis_eksterna'     => $this->input->post('jml_fraksi_dosis_eksterna'),
						'dosis_per_fraksi_eksterna'     => $this->input->post('dosis_per_fraksi_eksterna'),
						'skema_eksterna_1'              => $this->input->post('skema_eksterna_1'),
						'skema_eksterna_2'              => $this->input->post('skema_eksterna_2'),
						'skema_eksterna_3'              => $this->input->post('skema_eksterna_3'),
						'is_brakhiterapi'               => $this->input->post('is_brakhiterapi'),
						'total_dosis_brakhiterapi'      => $this->input->post('total_dosis_brakhiterapi'),
						'jml_fraksi_dosis_brakhiterapi' => $this->input->post('jml_fraksi_dosis_brakhiterapi'),
						'dosis_per_fraksi_brakhiterapi' => $this->input->post('dosis_per_fraksi_brakhiterapi'),
						'teknik_brakhiterapi_1'         => $this->input->post('teknik_brakhiterapi_1'),
						'teknik_brakhiterapi_2'         => $this->input->post('teknik_brakhiterapi_2'),
						'aplikator_brakhiterapi_1'      => $this->input->post('aplikator_brakhiterapi_1'),
						'aplikator_brakhiterapi_2'      => $this->input->post('aplikator_brakhiterapi_2'),
						'aplikator_brakhiterapi_3'      => $this->input->post('aplikator_brakhiterapi_3'),
						'aplikator_brakhiterapi_4'      => $this->input->post('aplikator_brakhiterapi_4'),
						'aplikator_brakhiterapi_5'      => $this->input->post('aplikator_brakhiterapi_5'),
						'aplikator_brakhiterapi_6'      => $this->input->post('aplikator_brakhiterapi_6'),
						'anastesi_brakhiterapi_1'       => $this->input->post('anastesi_brakhiterapi_1'),
						'anastesi_brakhiterapi_2'       => $this->input->post('anastesi_brakhiterapi_2'),
						'anastesi_brakhiterapi_3'       => $this->input->post('anastesi_brakhiterapi_3'),
						'kd_dokter'                     => $this->input->post('kd_dokter'),
						'catatan'                       => $this->input->post('catatan'),
						'nama_diagnosa_lengkap'         => $this->input->post('nama_diagnosa_lengkap'),
						'nama_diagnosa_dilapor'         => $this->input->post('nama_diagnosa_dilapor')		
					);
					$save=$this->db->insert('protokol_radiasi_radioterapi',$dataInsert);
						if($save){
						//	$this->db->trans_commit();
							$detail=$this->save_detail_protokol($list,array( 'kd_pasien' => $kd_pasien, 'kd_unit' => $kd_unit, 'tgl_masuk' => $tgl_masuk, 'urut_masuk' => $urut_masuk, ));
							if($detail==false){
								echo "{success:true}";
							//	$this->db->trans_commit();
							}else{
								echo "{success:false}";
							//	$this->db->trans_rollback();
							}
						}else{
							echo "{success:false}";
							//$this->db->trans_rollback();
						}
					
	}

	public function save_detail_protokol($List,$key_kunjungan){
		$now=date("Y-m-d");
		$tgl='';
		$error='';
			 for($i=0;$i<count($List);$i++){
			 		$item 		=$List[$i]->item;
			 		$jml 		= "";
			 		$tgl_rencana= null;
			 		if (isset($List[$i]->jml) === true) {
			 			$jml = $List[$i]->jml;
			 		}
			 		if (isset($List[$i]->tgl_rencana) === true) {
			 			$tgl_rencana = $List[$i]->tgl_rencana;
			 		}

					$kd_test = '0';
					$query   = $this->db->query("SELECT kd_test FROM setting_item_protokol where item_test = '".$item."'");
					if ($query->num_rows() > 0) {
						$kd_test = $query->row()->kd_test;
					}

					$data_detail  	= array(
							'kd_pasien' 	=> $key_kunjungan['kd_pasien'],
							'kd_unit'      	=> $key_kunjungan['kd_unit'],
							'tgl_masuk'    	=> $key_kunjungan['tgl_masuk'],
							'urut_masuk'  	=> $key_kunjungan['urut_masuk'],
							'kd_test'       => $kd_test,
							'jml'         	=> $jml,
							'tgl_rencana' 	=> $tgl_rencana
					); 
			 		$save_detail = $this->db->insert('resume_protokol',$data_detail);	
			 }
		if($save_detail){
			$error=false;
		}else{
			$error=true;
		}
		return $error;
	}


	public function check_status_bayar_unit_asal(){
		$response = array();
		$parameter = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$query = "SELECT 
			t_asal.* 
		FROM 
			transaksi t
		INNER JOIN unit_asal ua ON t.no_transaksi = ua.no_transaksi AND t.kd_kasir = ua.kd_kasir
		INNER JOIN transaksi t_asal ON t_asal.no_transaksi = ua.no_transaksi_asal AND t_asal.kd_kasir = ua.kd_kasir_asal
		WHERE
			t.no_transaksi = '".$parameter['no_transaksi']."' AND t.kd_kasir = '".$parameter['kd_kasir']."'";

		$query = $this->db->query($query);
		if($query->num_rows() > 0){
			$response['data'] 			= $query->result();
			if ($query->row()->lunas == 't' || $query->row()->lunas === true) {
				$response['status_lunas'] 	= true;
			}else{
				$response['status_lunas'] 	= false;
			}
		}else{
			$response['data'] 			= array();
			$response['status_lunas'] 	= false;
		}
		$response['count'] = $query->num_rows();
		echo json_encode($response);
	}
	
}
?>