<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_ItemPemeriksaanSummary extends MX_Controller {

	protected $kd_unit 	= null; 
	protected $kd_kasir = null; 
	protected $kd_klas  = null; 
	protected $kd_user 	= ""; 
    public function __construct(){
        parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		ini_set('memory_limit', "256M");

		$Q_unit = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_unit_endoscopy'");
		if ($Q_unit->num_rows() > 0) {
			$this->kd_unit = $Q_unit->row()->setting;
		}
		
		$Q_kasir = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kasir_endoscopy'");
		if ($Q_kasir->num_rows() > 0) {
			$this->kd_kasir = $Q_kasir->row()->setting;
		}

		$Q_klas = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kd_klas_endoscopy'");
		if ($Q_klas->num_rows() > 0) {
			$this->kd_klas = $Q_klas->row()->setting;
		}

		$this->kd_user = $this->session->userdata['user_id']['id'];
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN ITEM PEMERIKSAAN RADIOLOGI - SUMMARY';
		$param=json_decode($_POST['data']);
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdCust="";
		//$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdCust .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdCust = substr($tmpKdCust, 0, -1);
		
		$kriteria_cust = " AND KUN.KD_CUSTOMER IN(".$tmpKdCust.") ";
   		
		//jenis pasien
		
		
		//kd_customer
		/* =================================================================== */

		$asal_pasien=$param->asal_pasien;
		$variabel = "";
		$unit_endoscopy = $param->unit_endoscopy;
		if($unit_endoscopy == 'Semua'){
			$query = $this->db->query("SELECT kd_unit FROM unit WHERE parent = '".$this->kd_unit."'");
			$crtiteriaUnitEndoscopy = "";
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $result) {
					$variabel .= "'".$result->kd_unit."',";
				}
				$variabel = substr($variabel, 0, -1);
				$crtiteriaUnitEndoscopy = "and t.kd_unit in (".$variabel.")";
			}
			$nama_unit_endoscopy = 'Semua Unit Endoscopy';
		}else if($unit_endoscopy == 0){
			$query = $this->db->query("SELECT kd_unit FROM unit WHERE parent = '".$this->kd_unit."'");
			$crtiteriaUnitEndoscopy = "";
			if ($query->num_rows() > 0) {
				// $variabel = "";
				foreach ($query->result() as $result) {
					$variabel .= "'".$result->kd_unit."',";
				}
				$variabel = substr($variabel, 0, -1);
				$crtiteriaUnitEndoscopy = "and t.kd_unit in (".$variabel.")";
			}
			$nama_unit_endoscopy = 'Semua Unit Endoscopy';
		}else{
			$variabel = "'".$param->unit_endoscopy."'";
			$crtiteriaUnitEndoscopy="and T.KD_UNIT = '".$param->unit_endoscopy."'";
			$nama_unit_endoscopy = 'Endoscopy IGD';
		}

		$KdKasirRwj = "";
		$KdKasirRwi = "";
		$KdKasirIGD = "";
		$cekKdKasirRwj=$this->db->query("SELECT * From kasir_unit Where Kd_unit in ($variabel) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("SELECT * From kasir_unit Where Kd_unit in ($variabel) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("SELECT * From kasir_unit Where Kd_unit in ($variabel) and kd_asal='3'");
		if ($cekKdKasirRwj->num_rows()==0){
			$KdKasirRwj='';
		}else{
			foreach ($cekKdKasirRwj->result() as $result) {
				$KdKasirRwj .= "'".$result->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
		}
		

		if ($cekKdKasirRwi->num_rows()==0){
			$KdKasirRwi='';
		}else{
			foreach ($cekKdKasirRwi->result() as $result) {
				$KdKasirRwi .= "'".$result->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
		}

		if ($cekKdKasirIGD->num_rows()==0){
			$KdKasirIGD='';
		}else{
			foreach ($cekKdKasirIGD->result() as $result) {
				$KdKasirIGD .= "'".$result->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
		}

		$list_kd_kasir = ""; 
		if($asal_pasien == 0 || strtolower($asal_pasien) == 'semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			$crtiteriakodekasir="dtb.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			$list_kd_kasir =  "(".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.")";
			$crtiteriakodekasir="dtb.kd_kasir in (".$KdKasirRwj.")";
			$list_kd_kasir =  "(".$KdKasirRwj.")";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwi.")";
			$crtiteriakodekasir="dtb.kd_kasir in (".$KdKasirRwi.")";
			$nama_asal_pasien = 'RWI';
			$list_kd_kasir =  "(".$KdKasirRwi.")";
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirIGD.")";
			$crtiteriakodekasir="dtb.kd_kasir in (".$KdKasirIGD.")";
			$nama_asal_pasien = 'IGD';
			$list_kd_kasir =  "(".$KdKasirIGD.")";
		}else{
			$list_kd_kasir =  "(".$KdKasirRwj.")";
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.")";
			$crtiteriakodekasir="dtb.kd_kasir in (".$KdKasirRwj.")";
		}	

		//shift
		$q_shift='';
   		$t_shift='';
   		/* if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		} */
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT distinct PRO.KD_PRODUK, PRO.DESKRIPSI,SUM(DT.QTY) AS JUM_TOTAL 
				FROM TRANSAKSI T
					INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
					INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
					INNER JOIN (
						SELECT KD_KASIR, NO_TRANSAKSI 
						FROM DETAIL_BAYAR DB 
						WHERE KD_KASIR IN ".$list_kd_kasir." GROUP BY KD_KASIR, NO_TRANSAKSI
						) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
					INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK

				WHERE (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."') ".$crtiteriaUnitEndoscopy." ".$crtiteriaAsalPasien." ".$kriteria_cust."
					
				GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI				
				ORDER BY PRO.DESKRIPSI ASC
                "
		);
		
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();				
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="4">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="4"> Periode '.date_format(date_create($tgl_awal), "d-M-Y").' s/d '.date_format(date_create($tgl_akhir), "d-M-Y").'</th>
					</tr>
					<tr>
						<th colspan="4">Kelompok '.$nama_unit_rad.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		
		$all_total_grandtotal = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center"><h3>No</h3></th>
					<th width="10%" align="center"><h3>Kode Produk</h3></th>
					<th width="70%" align="center"><h3>Item Pemeriksaan</h3></th>
					<th width="15%" align="center"><h3>Qty</h3></th>
					
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			$all_total_subtotal = 0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<td align="center">'.$no.'</td>
					<td align="center">'.$line->kd_produk.'</td>
					<td align="left" style="padding-left:5px;">'.$line->deskripsi.'</td>
					<td align="center">'.$line->jum_total.'</td>
				</tr>';
				$all_total_subtotal += $line->jum_total; 
				// $queryBody = $this->db->query(
					// "
					// SELECT PRO.KD_PRODUK, PRO.DESKRIPSI, SUM(DT.QTY) AS JUM_TOTAL 
					// FROM TRANSAKSI T
						// INNER JOIN KUNJUNGAN KUN ON T.KD_PASIEN=KUN.KD_PASIEN AND T.KD_UNIT=KUN.KD_UNIT AND T.TGL_TRANSAKSI=KUN.TGL_MASUK AND T.URUT_MASUK=KUN.URUT_MASUK
						// INNER JOIN DETAIL_TRANSAKSI DT ON T.KD_KASIR=DT.KD_KASIR AND T.NO_TRANSAKSI=DT.NO_TRANSAKSI
						// INNER JOIN (
							// SELECT KD_KASIR, NO_TRANSAKSI 
							// FROM DETAIL_BAYAR DB 
							// WHERE KD_KASIR IN ('04', '09', '10', '14', '15', '16', '19', '20', '33') GROUP BY KD_KASIR, NO_TRANSAKSI
							// ) X ON X.KD_KASIR=T.KD_KASIR AND X.NO_TRANSAKSI=T.NO_TRANSAKSI
						// INNER JOIN PRODUK PRO ON PRO.KD_PRODUK=DT.KD_PRODUK
						// WHERE ".$crtiteriaUnitRad." ".$crtiteriaAsalPasien." ".$kriteria_cust." 
							// and (T.TGL_TRANSAKSI between '".$tgl_awal."' and '".$tgl_akhir."')  
							// and PRO.KD_PRODUK='".$line->kd_produk."'
						// GROUP BY PRO.KD_PRODUK, PRO.DESKRIPSI
						// ORDER BY PRO.DESKRIPSI ASC
					// "
				// );
				// $query2=$queryBody->result();
				// $no2=0;
				// $all_total_subtotal = 0;
				 // foreach($query2 as $line2){
					 
						// /* $all_total_jml_pasien += $line2->jml_pasien;
						// $all_total_jml_produk += $line2->jml_pasien;
						// $all_total_jasa_dok += $line2->c20;
						// $all_total_jasa_perawat += $line2->c21;
						// $all_total_indeks_tdk_langsung += $line2->c22;
						// $all_total_ops_instalasi += $line2->c23;
						// $all_total_ops_rs += $line2->c24;
						// $all_total_ops_direksi += $line2->c25;
						// $all_total_jasa_sarana += $line2->c30;*/
						// $all_total_subtotal += $line2->jum_total; 
						// $html.='
						// <tr> 
							// <td width="">&nbsp;</th>
							// <td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line2->kd_pasien.' </td>
							// <td width="" align="left">'.$line2->nama.' </td>
							// <td width="" align="left">'.$line2->customer.' </td>
							// <td width="" align="left">'.$line2->jum_total.' </td>
							
							// </tr>
						// ';
					
					
				// } 
				// $html.='
						// <tr> 
							
							// <td width="" colspan="4"  align="Right"><b>Sub Total :</b></td>
							// <td width="" align="left"><b>'.$all_total_subtotal.'</b></td>
							
							// </tr>
						// ';
				$all_total_grandtotal += $all_total_subtotal;
			}
			
			$html.='
						<tr> 
							
							<td width="" colspan="3"  align="center"><b>Grand Total :</b></td>
							<td width="" align="center"><b>'.$all_total_subtotal.'</b></td>
							
							</tr>
						';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Item_Pemeriksaan_Radiologi_Summary.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P','Lap. Item Pemeriksaan Radiologi Summary',$html);	
		}
		echo $html;
   	}
	
}
?>