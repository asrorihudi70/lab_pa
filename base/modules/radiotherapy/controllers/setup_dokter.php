<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class setup_dokter extends MX_Controller {
	protected $kd_unit 	= null; 
	protected $kd_kasir = null; 
	protected $kd_klas  = null; 
	protected $kd_user 	= ""; 
	public function __construct(){
		parent::__construct();

		$Q_unit = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_unit_radiotherapy'");
		if ($Q_unit->num_rows() > 0) {
			$this->kd_unit = $Q_unit->row()->setting;
		}
		
		$Q_kasir = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kasir_radiotherapy'");
		if ($Q_kasir->num_rows() > 0) {
			$this->kd_kasir = $Q_kasir->row()->setting;
		}

		$Q_klas = $this->db->query("SELECT * FROM sys_setting WHERE key_data = 'default_kd_klas_radiotherapy'");
		if ($Q_klas->num_rows() > 0) {
			$this->kd_klas = $Q_klas->row()->setting;
		}

		$this->kd_user = $this->session->userdata['user_id']['id'];
	}	 

	public function index(){
		$this->load->view('main/index');            		
	}


	public function getDokter(){
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kata_kd_unit='';
		if (strpos($kd_unit,","))
		{
			$pisah_kata=explode(",",$kd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kata_kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kata_kd_unit= $kd_unit;
		}
		//$kata_kd_unit = stristr($kd_unit,"'4");
		$nama_unit=$this->db->query("SELECT nama_unit from unit where kd_unit=".$kata_kd_unit."")->row()->nama_unit;
		$criteria="where kd_unit in(".$kata_kd_unit.") ";
		
		$result=$this->db->query("SELECT distinct d.kd_dokter,nama,dokter_inap_job.job as jenis_dokter,dij.job,dij.kd_job from dokter d 
			inner join dokter_penunjang dp on dp.kd_dokter=d.kd_dokter 
			inner join dokter_inap_job dij on dij.kd_job=dp.kd_job 
			inner join dokter_inap_job on dp.kd_job = dokter_inap_job.kd_job
			/*inner join unit u on u.kd_unit=dp.kd_unit */
			$criteria ORDER BY d.kd_dokter
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).', kd_unit:'.$kata_kd_unit.', nama_unit:"'.$nama_unit.'"}';
	}


	public function simpan_setup_dokter(){
		$KdSetupDokter = $_POST['KdSetupDokter'];
		$NamaSetupDokter = $_POST['NamaSetupDokter'];
		// $Spesialisasi = $_POST['Spesialisasi'];
		$kd_job = $_POST['kd_job'];
		$kd_unit = $_POST['kd_unit'];
		$cekdulu=$this->db->query("SELECT * from dokter_penunjang where kd_dokter='".$KdSetupDokter."' and kd_unit='".$kd_unit."'")->result();
		if (count($cekdulu)==0){
			$save=$this->db->query("INSERT into dokter_penunjang values('".$KdSetupDokter."','".$kd_unit."',' ','".$kd_job."')");
			//$save_SQL=_QMS_Query("insert into dokter_penunjang values('".$KdSetupDokter."','".$kd_unit."','".$Spesialisasi."')");
		}else{
			$save=$this->db->query("UPDATE dokter_penunjang set kd_job='".$kd_job."' where kd_dokter='".$KdSetupDokter."' and kd_unit='".$kd_unit."'");
		}
		
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}

	public function hapus_setup_dokter(){
		$KdSetupDokter = $_POST['KdSetupDokter'];
		$kd_unit       = $_POST['kd_unit'];
		$query         = $this->db->query("DELETE FROM dokter_penunjang WHERE kd_dokter='$KdSetupDokter' and kd_unit='$kd_unit' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	public function get_default_dokter(){
		$key=$this->input->post('key');
		$query=$this->db->query("select * from sys_setting a INNER JOIN dokter b on b.kd_dokter = a.setting   where a.key_data='".$key."'")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	}
}
?>