<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cetakgelangpasien extends MX_Controller {
	private $setup_db_sql = false;

    public function __construct() {
        parent::__construct();
		/*$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}*/
        $this->load->library('session', 'url');
    }

    public function cetak() {
//  
		$nomor      = $this->uri->segment(4, 0);
		$status     = $this->uri->segment(5, 0);
		$kd_pasien  =explode("-",$nomor);
		$kd_pasien2 =implode("",$kd_pasien);
		
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
	        $nama_pas = _QMS_Query(" SELECT nama,tgl_lahir from pasien WHERE kd_pasien='" . $nomor . "'");
		}else{
        	$nama_pas = $this->db->query(" SELECT nama,tgl_lahir from pasien WHERE kd_pasien='" . $nomor . "'");
		}
        if (count($nama_pas->result()) > 0) {
            $nama_pas_2 = $nama_pas->row();
            $nama = $nama_pas_2->nama;
            $tgllahir = $nama_pas_2->tgl_lahir;        
		}
		$tgllahir2=date_create($tgllahir);
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		if($status === 'false'){
			$mpdf = new mPDF('utf-8', array(115, 27), '', '', 15, 3, 3, 5, 5, 5, 'P');
		}else{
			$mpdf = new mPDF('utf-8', array(285, 50), '', '', 5, 3, 3, 5, 5, 5, 'P');
		}
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle('Kartu Pasien');
        $mpdf->WriteHTML("
        <style type='text/css'>
		p
        {
        font-family:arial;
        font-size:11px !important;
        line-height:1px;
        font-weigth:bolder;
		text-rotate:90;
        }

        .barcode {
        float:left;
        position:relative;
        margin-left:15px;
        margin-top:-5px;
        }
		#rotate {
			-webkit-transform: rotate(90deg);
			-moz-transform: rotate(90deg);
			-o-transform: rotate(90deg);
			-ms-transform: rotate(90deg);
			transform: rotate(90deg);
		}
		body {
			-webkit-transform: rotate(-90deg);
			-moz-transform: rotate(-90deg);
			-ms-transform: rotate(-90deg);
			-o-transform: rotate(-90deg);
			filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		}
        </style>
        ");
		if($status === 'false'){
			$mpdf->WriteHTML('<html>
							<body>
							<table width="100%" border="0" cellpadding="0" id="rotate" style="margin-left:15px;">
							  <tr>
								<td width="100%" colspan="2"><text transform="rotate(90,0,0)"><b><font style="font-family:arial; font-size:18px;">' . $nama . '</font></b></text></td>
							  </tr>
							  <tr>
								<td width="50%"><b><b><font style="font-family:arial; font-size:16px;">' . date_format($tgllahir2,"d-M-Y") . '</font></b></td>
								<td width="50%" rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<barcode code=' . $nomor . ' type="C39E" class="barcode" size="0.912" height="0.56"/></td>
							  </tr>
							  <tr>
								<td width="50%"><b><b><font style="font-family:arial; font-size:16px;">' . $nomor . '</font></b></td>
							  </tr>
							</table>'
                .'</body></html>');  
		}else{ 
			$nama_ibu 	= $nama;
			$nomor_ibu 	= $nomor;
			$tgl_ibu 	= date_format($tgllahir2,"d-M-Y");
			$mpdf->WriteHTML('<html>
							<body>'.
							'<table width="151px" border="0" cellpadding="0" id="rotate" style="margin-left:250px;margin-top:10px;">
							  <tr>
								<td width="100%"><font style="font-family:arial; font-size:14px;">' . $nama . '</font></td>
							  </tr>
							  <tr>
								<td width="100%" style="padding-top:5px;"><barcode code='.$nomor.' type="C39E" class="barcode" style="margin-left:-15px;" size="0.912" height="0.56"/></td>
							  </tr>
							  <tr>
								<td width="100%"><font style="font-family:arial; font-size:12px;">' . $nomor.' / '.date_format($tgllahir2,"d-M-Y") . '</font></td>
							  </tr>
							</table><br>'.
							'<table width="265px" border="0" cellpadding="0" id="rotate" style="margin-left:642px;">
							  <tr>
								<td width="100%"><text transform="rotate(90,0,0)"><font style="font-family:arial; font-size:14px;">' . $nama . '</font></text></td>
							  </tr>
							  <tr>
								<td width="100%"><font style="font-family:arial; font-size:12px;">' . $nomor . '</font></td>
							  </tr>
							  <tr>
								<td width="100%"><font style="font-family:arial; font-size:12px;">' . date_format($tgllahir2,"d-M-Y") . '</font></td>
							  </tr>
							  <tr>
								<td width="100%" style="padding-top:5px;"><barcode code='.$nomor.' type="C39E" class="barcode" style="margin-left:-15px;" size="0.912" height="0.56"/></td>
							  </tr>
							</table>'
                .'</body></html>');  
		}
		/*$mpdf->WriteHTML('<html>
							<body>
								<p align="center"><b>Aditya Iqbal</b>'
                . '</body></html>');
		*/
		$mpdf->WriteHTML(utf8_encode($html));
        $mpdf->Output("cetak.pdf", 'I');
        exit;
        //} 
		
    }

}
