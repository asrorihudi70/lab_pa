<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewdetailselisihrwi extends MX_Controller
{
    public  $gkdbagian;
    public  $Status;
    public  $id_user;
    public function __construct()
    {
        parent::__construct();
        $this->id_user = $this->session->userdata['user_id']['id'];
    }

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params = null)
    {
        date_default_timezone_set("Asia/Makassar");
        try {
            $parent = $this->db->query("SELECT * FROM zusers where kd_user='" . $this->id_user . "'")->row()->kd_unit;
            $this->load->model('rawat_inap/tblviewselisihkasirrwi');
            if (strlen($Params[4]) !== 0) {
                $this->db->where(str_replace("~", "'", " " . $Params[4] . "  ORDER BY d.urut "), null, false);
                $res = $this->tblviewselisihkasirrwi->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
            }
        } catch (Exception $o) {

            echo '{success: false}';
        }


        echo '{success:true, totalrecords:' . $res[1] . ', ListDataObj:' . json_encode($res[0]) . '}';
    }
}
