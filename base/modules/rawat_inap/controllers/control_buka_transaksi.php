<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_buka_transaksi extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	private $kd_kasir      = "";
	private $id_user       = "";
	private $setup_db_sql  = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('rawat_inap/Model_visite_dokter');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_nginap');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_kamar');
		$this->load->model('Tbl_data_pasien_inap');
		$this->jam      = date("H:i:s");
		$this->tanggal 	= date("Y-m-d");

		$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		}
		
		$this->kd_kasir = '02';
		$this->id_user  = $this->session->userdata['user_id']['id'];
	}

	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function buka_transaksi(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$response 	= array();
		$result_SQL = false;
		$result_PG 	= false;

		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'alasan' 		=> $this->input->post('alasan'),
		);
		// ======================================================================================================== TRANSAKSI
		$paramsCriteria = array(
			'field_criteria' 	=> array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_transaksi'	=> $params['tgl_masuk'],
			),
			'value_criteria' 	=> null,
			'table' 			=> 'transaksi',
			'field_return' 		=> null,
		);
		// var_dump($paramsCriteria);die;
		$queryTransaksi = $this->Tbl_data_transaksi->getCustom($paramsCriteria);
		// ======================================================================================================== NGINAP
		unset($paramsCriteria);
		$paramsCriteria = array(
			'field_criteria' 	=> array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_masuk' 	=> $params['tgl_masuk'],
				'akhir' 		=> 'true',
			),
			'value_criteria' 	=> null,
			'table' 			=> 'nginap',
			'field_return' 		=> null,
		);
		$queryKamar = $this->Tbl_data_transaksi->getCustom($paramsCriteria);
		
		// ======================================================================================================== KUNJUNGAN
		unset($paramsCriteria);
		$paramsCriteria = array(
			'field_criteria' 	=> array(
				'kd_unit' 		=> $queryTransaksi->row()->kd_unit,
				'tgl_masuk' 	=> $params['tgl_masuk'],
				'kd_pasien' 	=> $params['kd_pasien'],
			),
			'value_criteria' 	=> null,
			'table' 			=> 'kunjungan',
			'field_return' 		=> null,
		);
		$queryKunjungan = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

		// ======================================================================================================== BAGIAN SHIFT
		unset($paramsCriteria);
		$paramsCriteria = array(
			'field_criteria' 	=> array(
				'kd_bagian'		=> '1',
			),
			'value_criteria' 	=> null,
			'table' 			=> 'bagian_shift',
			'field_return' 		=> null,
		);
		$queryShift = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

		unset($paramsCriteria);
		unset($paramsUpdate);
		$paramsUpdate = array(
			'tgl_keluar' 	=> null,
			'jam_keluar' 	=> null,
		);
		$paramsCriteria = array(
			'kd_unit' 		=> $queryTransaksi->row()->kd_unit,
			'tgl_masuk' 	=> $params['tgl_masuk'],
			'kd_pasien' 	=> $params['kd_pasien'],
		);
		$response['Tahap'] 	= "Update data kunjungan";
		$result_PG 	= $this->Tbl_data_kunjungan->update($paramsCriteria, $paramsUpdate);
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$result_SQL = $this->Tbl_data_kunjungan->updateSQL($paramsCriteria, $paramsUpdate);
		}else{
			$result_SQL = true;
		}

		
		if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			unset($paramsCriteria);
			unset($paramsUpdate);
			$paramsUpdate = array(
				'tgl_co' 		=> null,
				'co_status' 	=> 'false',
			);
			$paramsCriteria = array(
				'tgl_transaksi' => $params['tgl_masuk'],
				'kd_kasir' 		=> $params['kd_kasir'],
				'kd_pasien' 	=> $params['kd_pasien'],
			);
			
			$response['Tahap'] 	= "Update data transaksi";
			$result_PG 	= $this->Tbl_data_transaksi->updateTransaksi($paramsCriteria, $paramsUpdate);
			unset($paramsUpdate);
			$paramsUpdate = array(
				'tgl_co' 		=> null,
				'co_status' 	=> 0,
			);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$result_SQL = $this->Tbl_data_transaksi->updateTransaksi_SQL($paramsCriteria, $paramsUpdate);
			}else{
				$result_SQL = true;
			}
		}else{
			$result_SQL = false;
			$result_PG 	= false;
		}

		
		if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			unset($paramsCriteria);
			unset($paramsUpdate);
			$paramsUpdate = array(
				'tgl_keluar' 	=> null,
				'jam_keluar' 	=> null,
			);
			$paramsCriteria = array(
				'tgl_masuk' 	=> $params['tgl_masuk'],
				'kd_unit' 		=> $queryTransaksi->row()->kd_unit,
				'kd_pasien' 	=> $params['kd_pasien'],
				'akhir' 		=> 'true',
			);
			
			$response['Tahap'] 	= "Update data nginap";
			$result_PG 	= $this->Tbl_data_nginap->updateNginap($paramsCriteria, $paramsUpdate);
			$paramsCriteria['akhir'] = 1;
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$result_SQL = $this->Tbl_data_nginap->updateNginap_SQL($paramsCriteria, $paramsUpdate);
			}else{
				$result_SQL = true;
			}
		}else{
			$result_SQL = false;
			$result_PG 	= false;
		}

		if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			unset($paramsCriteria);
			unset($paramsUpdate);
			$paramsCriteria = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $queryTransaksi->row()->no_transaksi,
			);

			$this->db->select("*");
			$this->db->where($paramsCriteria);
			$this->db->from("pasien_inap");
			$query = $this->db->get();

			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $queryTransaksi->row()->no_transaksi,
				// 'kd_unit' 		=> $queryTransaksi->row()->kd_unit,
				'kd_unit' 		=> $queryKamar->row()->kd_unit_kamar,
				'no_kamar' 		=> $queryKamar->row()->no_kamar,
				'kd_spesial' 	=> $queryKamar->row()->kd_spesial,
			);
			if ($query->num_rows() == 0) {
				$response['Tahap'] 	= "insert data nginap";
				$result_PG 	= $this->Tbl_data_pasien_inap->insertPasienInap($paramsInsert);
			}else{
				$result_PG 	= true;
			}
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$result_SQL = $this->Tbl_data_pasien_inap->insertPasienInap_SQL($paramsInsert);
			}else{
				$result_SQL = true;
			}
		}else{
			$result_SQL = false;
			$result_PG 	= false;
		}

		if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			unset($paramsUpdate);
			unset($paramsCriteria);
			$paramsCriteria = array(
				'no_kamar'     	=> $queryKamar->row()->no_kamar,
				'kd_unit'		=> $queryKamar->row()->kd_unit_kamar,
			);
			$dataKamar  = $this->Tbl_data_kamar->select($paramsCriteria);
			$paramsUpdate = array(
				'digunakan' 	=> ((int)$dataKamar->row()->digunakan + 1),
			);
			$result_PG  = $this->Tbl_data_kamar->update($paramsCriteria, $paramsUpdate);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$result_SQL = $this->Tbl_data_kamar->update_SQL($paramsCriteria, $paramsUpdate);
			}else{
				$result_SQL = true;
			}
			$response['tahap'] = 'Update data kamar';
		}else{
			$result_SQL = false;
			$result_PG 	= false;
		}

		if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			unset($paramsCriteria);
			unset($paramsInsert);

			$paramsCriteria = array(
				'field_criteria' 	=> array(
					'kd_pasien' 	=> $params['kd_pasien'],
				),
				'value_criteria' 	=> null,
				'table' 			=> 'pasien',
				'field_return' 		=> null,
			);
			$queryPasien = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

			$paramsInsert = array(
				'tgl_masuk' 	=> $params['tgl_masuk'],
				'kd_pasien' 	=> $params['kd_pasien'],
				'nama_pasien' 	=> $queryPasien->row()->nama,
				'kd_unit' 		=> $queryTransaksi->row()->kd_unit,
				'kd_unit_kamar'	=> $queryKamar->row()->kd_unit_kamar,
				'kd_user_buka'	=> $this->id_user,
				'tgl_buka_trans'=> date('Y-m-d')." 00:00:00.000",
				'jam_buka_trans'=> "1900-01-01 ".date('H:i:s'),
				'alasan'		=> $params['alasan'],
				'kd_kasir'		=> $params['kd_kasir'],
				'no_transaksi'	=> $queryTransaksi->row()->no_transaksi,
				'tgl_keluar'	=> date_format(date_create($queryKunjungan->row()->tgl_keluar), "Y-m-d")." 00:00:00.000",
				'jam_keluar'	=> "1900-01-01 ".date_format(date_create($queryKunjungan->row()->jam_keluar), "H:i:s"),
				'shift'			=> $queryShift->row()->shift,
				'no_kamar'		=> $queryKamar->row()->no_kamar,
				'kd_spesial'	=> $queryKamar->row()->kd_spesial,
			);

			$response['Tahap'] 	= "Insert history buka transaksi rwi";
			$result_PG = $this->db->insert("history_buka_transaksi_rwi", $paramsInsert);
			$result_SQL= true;
		}else{
			$result_SQL = false;
			$result_PG 	= false;
		}
		

		// $result_SQL= false;
		if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['status'] = false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}

	public function laporan_buka_transaksi(){
		$result_PG 	= false;
		$result_SQL = false;
		$this->db->trans_commit();
		// $this->dbSQL->trans_commit();
		$response = array();
		$param=json_decode($_POST['data']);

		$params = array(
			'tgl_akhir' 	=> $param->tgl_akhir,
			'tgl_awal' 		=> $param->tgl_awal,
			'type_file'		=> $param->type_file,
			'rd_tanggal'	=> $param->rd_tanggal,
			'unit' 			=> substr($param->unit, 0,strlen($param->unit)-1),
		);

		if ($params['rd_tanggal'] == 1 || $params['rd_tanggal'] == '1') {
			$criteriaTanggal 	= " AND tgl_buka_trans between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'";
			$labelTanggal 		= "Tanggal Buka Transaksi";
		}else if ($params['rd_tanggal'] == 2 || $params['rd_tanggal'] == '2') {
			$criteriaTanggal 	= " AND tgl_masuk between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'";
			$labelTanggal 		= "Tanggal Masuk Kunjungan";
		}else if ($params['rd_tanggal'] == 3 || $params['rd_tanggal'] == '3'){
			$criteriaTanggal 	= " AND tgl_keluar between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'";
			$labelTanggal 		= "Tanggal Keluar/Pulang Pasien";
		}else{
			$criteriaTanggal 	= "";
			$labelTanggal 		= "";
		}

		unset($paramsCriteria);
		$paramsCriteria = array(
			'field_criteria' 	=> "kd_unit_kamar in (select kd_unit from unit where parent in (".$params['unit'].")) ".$criteriaTanggal,
			'value_criteria' 	=> null,
			'table' 			=> 'history_buka_transaksi_rwi',
			'field_return' 		=> null,
		);
		$queryHistory = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

		$html = "";
		$html .= "<table border='0' width='100%'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th align='center' colspan='10'>Laporan Buka Transaksi RWI</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th align='center' colspan='10'>Periode ".$labelTanggal." ".date_format(date_create($params['tgl_awal']), 'd/M/Y')." s/d ".date_format(date_create($params['tgl_akhir']), 'd/M/Y')."</th>";
		$html .= "</tr>";
		
		$html .= "</thead>";
		$html .= "</table>";
		// $html .= "<hr>";
		
		$html .= "<table border='1' width='100%'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th>No</th>";
		$html .= "<th>No Transaksi</th>";
		$html .= "<th>No Medrec</th>";
		$html .= "<th>Nama</th>";
		$html .= "<th>Tgl Buka Trans</th>";
		$html .= "<th>Tgl Masuk</th>";
		$html .= "<th>Tgl Keluar</th>";
		$html .= "<th>Unit</th>";
		$html .= "<th>Petugas</th>";
		$html .= "<th>Alasan</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";
		$no = 1;
		if ($queryHistory->num_rows() > 0) {

			foreach ($queryHistory->result() as $data) {

				unset($paramsCriteria);
				$paramsCriteria = array(
					'field_criteria' 	=> array(
						'kd_unit' 		=> $data->kd_unit,
					),
					'value_criteria' 	=> null,
					'table' 			=> 'unit',
					'field_return' 		=> null,
				);
				$queryUnit = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

				unset($paramsCriteria);
				$paramsCriteria = array(
					'field_criteria' 	=> array(
						'kd_user' 		=> $data->kd_user_buka,
					),
					'value_criteria' 	=> null,
					'table' 			=> 'zusers',
					'field_return' 		=> null,
				);
				$queryUsers = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

				unset($paramsCriteria);
				$paramsCriteria = array(
					'field_criteria' 	=> array(
						'kd_spesial' 	=> $data->kd_spesial,
					),
					'value_criteria' 	=> null,
					'table' 			=> 'spesialisasi',
					'field_return' 		=> null,
				);
				$querySpesial = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

				unset($paramsCriteria);
				$paramsCriteria = array(
					'field_criteria' 	=> array(
						'no_kamar' 	=> $data->no_kamar,
					),
					'value_criteria' 	=> null,
					'table' 			=> 'kamar',
					'field_return' 		=> null,
				);
				$queryKamar = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

				unset($paramsCriteria);
				$paramsCriteria = array(
					'field_criteria' 	=> array(
						'kd_kelas' 		=> $queryUnit->row()->kd_kelas,
					),
					'value_criteria' 	=> null,
					'table' 			=> 'kelas',
					'field_return' 		=> null,
				);
				$queryKelas = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

				$html .= "<tr>";
				$html .= "<td>".$no."</td>";
				$html .= "<td>".$data->no_transaksi."</td>";
				$html .= "<td>".$data->kd_pasien."</td>";
				$html .= "<td>".$data->nama_pasien."</td>";
				$html .= "<td>".date_format(date_create($data->tgl_buka_trans), 'd/M/Y')." ".date_format(date_create($data->jam_buka_trans), 'H:i:s')."</td>";
				$html .= "<td>".date_format(date_create($data->tgl_masuk), 'd/M/Y')."</td>";
				$html .= "<td>".date_format(date_create($data->tgl_keluar), 'd/M/Y')." ".date_format(date_create($data->jam_keluar), 'H:i:s')."</td>";
				$html .= "<td>".$querySpesial->row()->spesialisasi."/ ".$queryKelas->row()->kelas."/ ".$queryKamar->row()->no_kamar." [".$queryKamar->row()->nama_kamar."]</td>";
				// $html .= "<td>".$queryUnit->row()->nama_unit."</td>";
				// Penyakit Dalam/ KLS III/ 306 [ WK C Kelas 3 ]
				// $html .= "<td>".$queryKamar->row()->no_kamar." - ".$queryKamar->row()->nama_kamar."</td>";
				// $html .= "<td>".$querySpesial->row()->spesialisasi."</td>";
				$html .= "<td>".$queryUsers->row()->user_names."</td>";
				$html .= "<td>".$data->alasan."</td>";
				$html .= "</tr>";
				$no++;
			}
		}else{
			$html .= "<tr>";
			$html .= "<td colspan='10' align='center'>Tidak ada data</td>";
			$html .= "</tr>";
		}
		$html .= "</tbody>";
		$html .= "</table>";

		// echo $html;
		if ($params['type_file'] == 1) {
			$no 		= $no+3;
			$print_area	= 'A1:J'.$no;
			$area_wrap	= 'A4:J'.$no;
			/*$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	*/	
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A4:K4')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
				/*'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			 	),
				'borders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					// 'top' => array(
			 	// 	)
			 	),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
						'argb' => 'FFA0A0A0'
					),
					'endcolor' => array(
						'argb' => 'FFFFFFFF'
					)
				)*/
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(8);
			$objPHPExcel->getActiveSheet()->getStyle("A1:I2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 

			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:J'.$no)
						->getNumberFormat()
					    ->setFormatCode($textFormat);


			$objPHPExcel->getActiveSheet()
						->getStyle('A1:J4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			if ($queryHistory->num_rows() == 0) {
				$objPHPExcel->getActiveSheet()
							->getStyle('A5')
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
			/*$objPHPExcel->getActiveSheet()
						->getStyle('A4:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_TOP);*/
			$objPHPExcel->getActiveSheet()
						->getStyle('C5:C'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D5:D'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E5:E'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F5:F'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G5:G'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H5:H'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I5:I'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J5:J'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			/*$objPHPExcel->getActiveSheet()
						->getStyle('K5:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);*/
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=laporan_buka_transaksi-'.$params['tgl_awal'].'_'.$params['tgl_akhir'].'.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan history buka transaksi RWI',$html);	
		}
		/*if (($result_PG>0 || $result_PG===true) && ($result_SQL>0 || $result_SQL===true)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$response['status'] = false;
		}
*/
		// $this->db->close();
		// $this->dbSQL->close();
		// echo json_encode($response);
	}

	public function cek_data_laporan_buka_trans(){
		$result_PG 	= false;
		$result_SQL = false;
		$this->db->trans_commit();
		// $this->dbSQL->trans_commit();
		$response = array();
		// $param=json_decode($_POST['data']);
		$unit = $this->input->post('unit');
		$params = array(
			'tgl_akhir' 	=> $this->input->post('tgl_akhir'),
			'tgl_awal' 		=> $this->input->post('tgl_awal'),
			'type_file'		=> $this->input->post('type_file'),
			'rd_tanggal'	=> $this->input->post('rd_tanggal'),
			'unit' 			=> substr($unit, 0,strlen($unit)-1),
		);
		if ($params['rd_tanggal'] == 1 || $params['rd_tanggal'] == '1') {
			$criteriaTanggal 	= " AND tgl_buka_trans between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'";
			$labelTanggal 		= "Tanggal Buka Transaksi";
		}else if ($params['rd_tanggal'] == 2 || $params['rd_tanggal'] == '2') {
			$criteriaTanggal 	= " AND tgl_masuk between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'";
			$labelTanggal 		= "Tanggal Masuk Kunjungan";
		}else if ($params['rd_tanggal'] == 3 || $params['rd_tanggal'] == '3'){
			$criteriaTanggal 	= " AND tgl_keluar between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'";
			$labelTanggal 		= "Tanggal Keluar/Pulang Pasien";
		}else{
			$criteriaTanggal 	= "";
			$labelTanggal 		= "";
		}

		unset($paramsCriteria);
		$paramsCriteria = array(
			'field_criteria' 	=> "kd_unit_kamar in (select kd_unit from unit where parent in (".$params['unit'].")) ".$criteriaTanggal,
			'value_criteria' 	=> null,
			'table' 			=> 'history_buka_transaksi_rwi',
			'field_return' 		=> null,
		);
		$queryHistory = $this->Tbl_data_transaksi->getCustom($paramsCriteria);
		if ($queryHistory->num_rows() > 0) {
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		$this->db->close();
		// $this->dbSQL->close();
		echo json_encode($response);
	}
}
?>
