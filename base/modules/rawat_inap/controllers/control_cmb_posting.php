<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_cmb_posting extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('rawat_inap/Model_posting');
		$this->jam      = date("H:i:s");
		// $this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 
	
	public function cmbProduk(){
		$x = 0;
		$response 	= array();
		$params 	= array(
			'produk_charge.kd_unit'     => '1',
		);
		$resultData=array();
		$result = $this->Model_posting->getDataComboProduk($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_PRODUK'] = $data['KD_PRODUK'];
			$resultData[$x]['DESKRIPSI'] = $data['DESKRIPSI'];
			$resultData[$x]['KD_UNIT']   = $data['KD_UNIT'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}
	
	public function gridComponent(){
		$x = 0;
		$response 	= array();
		$result = $this->Model_posting->getDataProdukComponent();
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_COMPONENT'] = $data['KD_COMPONENT'];
			$resultData[$x]['COMPONENT']    = $data['COMPONENT'];
			$resultData[$x]['KD_JENIS']     = $data['KD_JENIS'];
			$resultData[$x]['TARIF']        = 0;
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}
}


?>
