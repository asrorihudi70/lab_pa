<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class data_kunjungan extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now      = "";
	private $ErrorPasien  = false;
	private $setup_db_sql = false;

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		}
		$this->tgl_now = date("Y-m-d");
		$this->load->model("Tbl_data_pasien");
		$this->load->model("Tbl_data_kunjungan");
		$this->load->model("Tbl_data_transaksi");
		$this->load->model("Tbl_data_customer");
		$this->load->model("Tbl_data_unit");
		$this->load->model("Tbl_data_dokter");
	}

	public function kunjungan_terakhir(){
		$response = array();
		$criteria = array(
			'kd_pasien' 		=> $this->input->post('kd_pasien'),
			//'left(kd_unit,1)'	=> $this->input->post('kd_unit'),
		);
		$criteriaUnit = array(
			'table' 			=> 'unit',
			'field_criteria' 	=> 'kd_unit',
			'field_return' 		=> 'NAMA_UNIT',
		);

		$criteriaDokter = array(
			'table' 			=> 'dokter',
			'field_criteria' 	=> 'kd_dokter',
			'field_return' 		=> 'NAMA',
		);

		$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, 1);
		if ($resultQuery->num_rows()>0) {
			$criteriaUnit['value_criteria'] 	= $resultQuery->row()->KD_UNIT;
			$criteriaDokter['value_criteria'] 	= $resultQuery->row()->KD_DOKTER;

			$response['kd_rujukan'] 		= (int)$resultQuery->row()->KD_RUJUKAN;
			$response['cara_penerimaan'] 	= (int)$resultQuery->row()->CARA_PENERIMAAN;
			$resultPenerimaan = $this->db->query("SELECT * FROM rujukan WHERE kd_rujukan = '".$resultQuery->row()->KD_RUJUKAN."' and cara_penerimaan = '".$resultQuery->row()->CARA_PENERIMAAN."'");
			if ($resultPenerimaan->num_rows() > 0) {
				$response['penerimaan'] 		= $resultPenerimaan->row()->RUJUKAN;
			}else{
				$response['penerimaan'] 		= "";
			}
			$response['poliklinik'] 		= $resultQuery->row()->KD_UNIT;
			$response['nama_unit'] 			= $this->Tbl_data_unit->getCustom($criteriaUnit);
			$response['kd_unit'] 			= $resultQuery->row()->KD_UNIT;
			$response['kd_dokter']			= $resultQuery->row()->KD_DOKTER;
			$response['nama_dokter']		= $this->Tbl_data_dokter->getCustom($criteriaDokter);
			$criteriaCustomer = array(
				'kontraktor.kd_customer' 	=> $resultQuery->row()->KD_CUSTOMER,
			);
			$resultQuery = $this->Tbl_data_customer->selectDataCustomerDanKontraktor("customer.kd_customer, customer.customer, kontraktor.jenis_cust ", $criteriaCustomer);
			foreach ($resultQuery->result() as $result) {
				$response['customer'] 		= $result->customer;
				$response['kd_customer'] 	= $result->kd_customer;
				$response['jenis_cust'] 	= (int)$result->jenis_cust;
			}
		}else{
			$response['poliklinik'] 		= 0;
			$response['kd_rujukan'] 		= 99;
			$response['cara_penerimaan'] 	= 0;
			$response['customer'] 			= "";
			$response['kd_customer'] 		= 0;
			$response['jenis_cust'] 		= 0;
		}
		$response['count'] 				= $resultQuery->num_rows();

		echo json_encode($response);
	}

	public function kunjungan(){
		$response 	= array();
		$result 	= array();
		$result_SQL = false;
		$result_PG 	= false;
		$this->db->trans_begin();
		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL->trans_begin();
		}
		
		$criteria = "kd_pasien = '".$this->input->post('kd_pasien')."' AND kd_unit like '100%' --AND tgl_keluar IS NOT NULL";
		$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, null);
		
		unset($criteria);
		$criteria = array(
			'kd_pasien' => $this->input->post('kd_pasien'),
		); 
		
		$response['NAMA'] = $this->Tbl_data_pasien->getDataSelectedPasien($criteria)->row()->NAMA;
		
		$x = 0;
		foreach ($resultQuery->result() as $data) {
			$result[$x]['TANGGAL_MASUK'] = date_format(date_create($data->TGL_MASUK), 'Y-m-d');
			$result[$x]['TANGGAL_KELUAR'] = $data->TGL_KELUAR;
			$x++;
		}
		$response['DATA'] = $result;
		echo json_encode($response);
	}

	public function data_kunjungan_inap(){
		$response 	= array();
		$result 	= array();
		$result_SQL = false;
		$result_PG 	= false;
		$this->db->trans_begin();

		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL->trans_begin();
		}
		
		$criteria = "kd_pasien = '".$this->input->post('kd_pasien')."' AND kd_unit like '1%' AND tgl_masuk = '".$this->input->post('tgl_masuk')."'";
		$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, null);
		
		$paramsCriteria = array(
			'tgl_transaksi' => $this->input->post('tgl_masuk'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'kd_unit' 		=> $resultQuery->row()->kd_unit,
			'kd_pasien'		=> $this->input->post('kd_pasien'),
			'co_status'		=> 'true',
		);
		
		unset($resultQuery);
		$resultQuery = $this->Tbl_data_transaksi->getTransaksi($paramsCriteria);

		if ($resultQuery->num_rows() > 0){
			$response['status_inap'] 	= true;
		}else{
			$response['status_inap'] 	= false;
		}
		echo json_encode($response);
	}

	public function update_tanggal_masuk(){
		$response 	= array();
		$result 	= array();
		$result_SQL = false;
		$result_PG 	= false;
		$this->db->trans_begin();

		// SETUP SQL KONEKSI
		if ($this->setup_db_sql === true) {
			$this->dbSQL->trans_begin();
		}
		
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'tgl_masuk_lama'=> $this->input->post('tgl_masuk_lama'),
			'jam_masuk' 	=> $this->input->post('jam_masuk'),
			'jam_masuk_lama'=> $this->input->post('jam_masuk_lama'),
		);

		$params_criteria = array(
			'kd_pasien' 	=> $params['kd_pasien'],
			'kd_unit' 		=> $params['kd_unit'],
			'tgl_masuk' 	=> $params['tgl_masuk_lama'],
		);
		$this->db->select("*");
		$this->db->where($params_criteria);
		$this->db->from("kunjungan");
		$query_kunjungan = $this->db->get();

		if ($query_kunjungan->num_rows() > 0) {
			unset($params_criteria);
			$params_criteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_masuk' 	=> $params['tgl_masuk_lama'],
				'urut_masuk' 	=> $query_kunjungan->row()->urut_masuk,
				'kd_unit' 		=> $query_kunjungan->row()->kd_unit,
				'urut_nginap'	=> "0",
			);

			$this->db->select("*");
			$this->db->where($params_criteria);
			$this->db->from("nginap");
			$query_nginap = $this->db->get();
			if ($query_nginap->num_rows() == 1) {
				$params_update = array(
					'tgl_inap' 		=> $params['tgl_masuk'],
					'jam_inap' 		=> "1900-01-01 ".$params['jam_masuk'],
				);
				$this->db->where($params_criteria);
				$result_PG = $this->db->update("nginap", $params_update);
			}else{
				$result_PG = false;
			}
		}else{
			$result_PG = false;
		}

		if ($result_PG > 0 || $result_PG === true){
			unset($params_criteria);
			unset($params_update);
			$params_criteria = array(
				'kd_pasien' 	=> $params['kd_pasien'],
				'tgl_masuk' 	=> $params['tgl_masuk_lama'],
				'urut_masuk' 	=> $query_kunjungan->row()->urut_masuk,
				'kd_unit' 		=> $query_kunjungan->row()->kd_unit,
			);
			$params_update = array(
				'tgl_masuk' 	=> $params['tgl_masuk'],
				'jam_masuk' 		=> "1900-01-01 ".$params['jam_masuk'],
			);

			$this->db->where($params_criteria);
			$result_PG = $this->db->update("kunjungan", $params_update);
		}else{
			$result_PG = false;
		}
		// $result_PG = false;


		if ($result_PG > 0 || $result_PG === true){
			$response['status'] 	= true;
			$this->db->trans_commit();
		}else{
			$response['status'] 	= false;
			$this->db->trans_rollback();
		}

		$this->db->close();
		echo json_encode($response);
	}
}

?>
