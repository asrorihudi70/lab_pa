<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_CPPT_RWI extends  MX_Controller {		
	private $jam;
	private $dbSQL;
	private $kd_kasir;
	private $id_user;

    public $ErrLoginMsg='';

    public function __construct(){

        parent::__construct();
        $this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
		//$this->load->model('rawat_inap/Model_posting');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
		$this->id_user 	= $this->session->userdata['user_id']['id'];
    }
	 
	public function index(){
		$this->load->view('main/index');
    }	

	public function insert(){
		$result = true;
		$message= "";
		$parameter = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'urut_masuk' 	=> $this->input->post('urut_masuk'),
		);
		$data_grid = $this->input->post('data_grid');
		if ($data_grid != "") {
			$data_grid = json_decode($data_grid);
			foreach ($data_grid as $value) {
				$urut = 0;
				if (isset($value->urut) === true) {
					$urut = $value->urut;
				}
				$criteria = array(
					'kd_pasien' 	=> $parameter['kd_pasien'],
					'kd_unit' 		=> $parameter['kd_unit'],
					'tgl_masuk' 	=> $parameter['tgl_masuk'],
					'urut_masuk' 	=> $parameter['urut_masuk'],
					'urut' 			=> $urut,
				);

				$this->db->select("*");
				$this->db->where($criteria);
				$this->db->from("cppt_rwi");
				$query = $this->db->get();

				if ($query->num_rows() == 0) {
					unset($criteria);
					$criteria = array(
						'kd_pasien' 	=> $parameter['kd_pasien'],
						'kd_unit' 		=> $parameter['kd_unit'],
						'tgl_masuk' 	=> $parameter['tgl_masuk'],
						'urut_masuk' 	=> $parameter['urut_masuk'],
					);
					$this->db->select(" max(urut) as urut ");
					$this->db->where($criteria);
					$this->db->from("cppt_rwi");
					$query = $this->db->get();
					$urut = 1;
					if ($query->num_rows() > 0) {
						$urut = (int)$query->row()->urut + 1;
					}
					$params = array(
						'kd_pasien' => $parameter['kd_pasien'],
						'kd_unit' 	=> $parameter['kd_unit'],
						'tgl_masuk' => $parameter['tgl_masuk'],
						'urut_masuk'=> $parameter['urut_masuk'],
						'bagian' 	=> $value->bagian,
						'hasil' 	=> $value->hasil,
						'verifikasi'=> $value->verifikasi,
						'waktu' 	=> $value->waktu,
						'instruksi' => $value->instruksi,
						'urut' 		=> $urut,
					);

					$this->db->insert("cppt_rwi", $params);
					if ($this->db->trans_status() > 0) {
						$result 	= true;
						$message 	= "Data Berhasil disimpan";
					}else{
						$result 	= false;
						$message 	= "Gagal menyimpan data ".$value->bagian." yang di verifikasi oleh ".$value->verifikasi." pada tanggal ".$value->waktu;
						break;
					}
				}else{
					$params = array(
						'bagian' 	=> $value->bagian,
						'hasil' 	=> $value->hasil,
						'verifikasi'=> $value->verifikasi,
						'instruksi' => $value->instruksi,
						'waktu' 	=> $value->waktu,
					);
					$criteria['urut'] = $value->urut;
					$this->db->where($criteria);
					$this->db->update("cppt_rwi", $params);
					if ($this->db->trans_status() > 0) {
						$result 	= true;
						$message 	= "Data Berhasil disimpan";
					}else{
						$result 	= false;
						$message 	= "Gagal menyimpan data ".$value->bagian." yang di verifikasi oleh ".$value->verifikasi." pada tanggal ".$value->waktu;
						break;
					}
				}
			}
		}

		echo json_encode(
			array(
				'status' 	=> $result,
				'message' 	=> $message,
			)
		);
	}

	function save(){
		$params = array(
			'kd_pasien'  => $this->input->post('kd_pasien'),
			'kd_unit'    => $this->input->post('kd_unit'),
			'tgl_masuk'  => $this->input->post('tgl_masuk'),
			'urut_masuk' => $this->input->post('urut_masuk'),
			'list' 		 => json_decode($_POST['List']),
		);
		/*$flag=$this->input->post('flag');
		if($flag=='enter'){
			$delete=$this->db->query("DELETE FROM cppt_rwi WHERE 
				kd_pasien      ='".$params['kd_pasien']."' 
				and kd_unit    ='".$params['kd_unit']."' 
				and tgl_masuk  ='".$params['tgl_masuk']."' 
				and urut_masuk ='".$params['urut_masuk']."' 
				and bagian     ='' 
				and hasil      ='' 
				and verifikasi =''
			");
		}*/

		$criteria = array(
			'kd_pasien'  	=> $params['kd_pasien'],
			'kd_unit'    	=> $params['kd_unit'],
			'tgl_masuk'  	=> $params['tgl_masuk'],
			'urut_masuk' 	=> $params['urut_masuk'],
		);
		$this->db->where($criteria);
		$this->db->delete('cppt_rwi');
		for($i=0;$i<count($params['list']);$i++){
			if (strlen($params['list'][$i]->BAGIAN) > 0 && strlen($params['list'][$i]->HASIL) > 0 && strlen($params['list'][$i]->VERIFIKASI) > 0) {
				$insert = array(
					'urut'  		=> $i,
					'kd_pasien'  	=> $params['kd_pasien'],
					'kd_unit'    	=> $params['kd_unit'],
					'tgl_masuk'  	=> $params['tgl_masuk'],
					'urut_masuk' 	=> $params['urut_masuk'],
					'tgl'        	=> date_format(date_create($params['list'][$i]->TGL), 'Y-m-d'),
					'jam'        	=> $params['list'][$i]->JAM,
					'bagian'     	=> $params['list'][$i]->BAGIAN,
					'hasil'      	=> $params['list'][$i]->HASIL,
					'verifikasi' 	=> $params['list'][$i]->VERIFIKASI,
				);
				$this->db->insert('cppt_rwi', $insert);
			}
		}
	    /*$now=date("Y-m-d");
		$no=1;
		$this->db->where($params);
		$this->db->delete("cppt_rwi");
		$delete = $this->db->affected_rows();
		if($delete>0){
			for($i=0;$i<count($list);$i++){
				$data =array( 
					'tgl'        => $now,
					'jam'        => $list[$i]->JAM,
					'bagian'     => $list[$i]->BAGIAN,
					'hasil'      => $list[$i]->HASIL,
					'verifikasi' => $list[$i]->VERIFIKASI,
					'kd_pasien'  => $params['kd_pasien'],
					'kd_unit'    => $params['kd_unit'],
					'tgl_masuk'  => $params['tgl_masuk'],
					'urut_masuk' => $params['urut_masuk'],
		       ); 
       	$save=$this->db->insert('cppt_rwi',$data);	
		$no++; }
		}*/
		// if($save){
		// 	echo '{success:true}';	
		// }
		echo json_encode( array( 'status' => true, ) );
	}

	public function get_data(){
		$kd_pasien  = $this->input->post('kd_pasien');
		$kd_unit    = $this->input->post('kd_unit');
		$tgl_masuk  = $this->input->post('tgl_masuk');
		$urut_masuk = $this->input->post('urut_masuk');
		$query=$this->db->query("SELECT 
			kd_pasien, 
		    bagian,
		    hasil,
		    instruksi,
		    urut,
		    waktu,
		    verifikasi  FROM cppt_rwi WHERE kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_masuk."' and urut_masuk='".$urut_masuk."'")->result();
		echo '{success:true, totalresultords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}

	public function deletedetail(){
		$criteria = array(
			'kd_pasien'  => $this->input->post("kd_pasien"),
			'urut'       => $this->input->post("urut"),
			'kd_unit'    => $this->input->post('kd_unit'),
			'tgl_masuk'  => $this->input->post('tgl_masuk'),
			'urut_masuk' => $this->input->post('urut_masuk'),
		);
		$this->db->where($criteria);
		$this->db->delete('cppt_rwi');
		$delete = $this->db->trans_status();
		// $delete=$this->db->query("DELETE  FROM cppt_rwi WHERE urut='".$urut."' AND kd_pasien='".$kd_pasien."'");
		if($delete > 0 || $delete === true){
			echo '{success:true}';	
		}
	}

	public function delete(){
		$criteria = array(
			'kd_pasien'  => $this->input->post("kd_pasien"),
			'waktu'      => $this->input->post("waktu"),
			'kd_unit'    => $this->input->post('kd_unit'),
			'tgl_masuk'  => $this->input->post('tgl_masuk'),
			'urut_masuk' => $this->input->post('urut_masuk'),
			'urut' 		 => $this->input->post('urut'),
		);
		$this->db->where($criteria);
		$this->db->delete('cppt_rwi');
		$delete = $this->db->trans_status();
		// $delete=$this->db->query("DELETE  FROM cppt_rwi WHERE urut='".$urut."' AND kd_pasien='".$kd_pasien."'");
		if($delete > 0 || $delete === true){
			echo '{success:true}';	
		}
	}

	public  function getProfesi(){
		$data       	= array();
		$i          	= 0;
		$jenis_dokter 	= $this->input->post("jenis_dokter");
		if ($jenis_dokter != "" || $jenis_dokter != null) {
			$jenis_dokter = " AND jenis_dokter = '".$jenis_dokter."' ";
		}else{
			$jenis_dokter = " ";
		}
		$query=$this->db->query("SELECT * from dokter WHERE lower(nama) like ('%".strtolower($_POST['text'])."%') ".$jenis_dokter." ORDER BY nama asc");

		foreach ($query->result_array() as $row){
			 $data[$i]['kd_dokter'] 		= $row['KD_DOKTER'];
			 $data[$i]['nama'] 	 			= $row['NAMA'];
			 $data[$i]['status'] 	 	 	= $row['STATUS'];
			 $data[$i]['jenis_dokter'] 	 	= $row['JENIS_DOKTER'];
			$i++;
        }
		$jsonResult['processResult'] 	= 'SUCCESS';
    	$jsonResult['listData'] 		= $data;
    	echo json_encode($jsonResult);
	}
 }