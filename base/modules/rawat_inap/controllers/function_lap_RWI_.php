<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_lap_RWI extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

	}

	function cetak_lap_reg_detail(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        $Split = explode("##@@##", $getcriteria,  5);
		$tglsum = $Split[0];
        $tglsummax = $Split[1];
        $isikriteria=$Split[2];
		if($isikriteria==="ruang")
		{
			$feildkriteria="";
			$criteria="";
			$tmpunit = explode(',', $Split[3]);
			for($i=0;$i<count($tmpunit);$i++)
			{
				$criteria .= "'".$tmpunit[$i]."',";
			}
			$feildkriteria="sp_u.kd_unit";
			$criteria = substr($criteria, 0, -1);

		}

		if($isikriteria==="kelas"){
			$feildkriteria="";
			$criteria="";
			$tmpunit = explode(',', $Split[3]);
			for($i=0;$i<count($tmpunit);$i++)
			{
				$criteria .= "'".$tmpunit[$i]."',";
			}
			$feildkriteria="sp_u.kd_kelas";
			$criteria = substr($criteria, 0, -1);
		}
		
		if($isikriteria==="spesialis")
		{
			$feildkriteria="";
			$criteria="";
			$tmpunit = explode(',', $Split[3]);
			for($i=0;$i<count($tmpunit);$i++)
			{
				$criteria .= "'".$tmpunit[$i]."',";
			}
			$feildkriteria="sp_u.kd_spesial";
			$criteria = substr($criteria, 0, -1);
		}
		
		$pasientype=$Split[4];
		$jenis_pasien='';
		if($pasientype=='kabeh')
		{
			$pasientype="";
			$jenis_pasien ='Semua Pasien';
		}else{
			$pasientype=" and k.Baru='".$Split[4]."'";
			if($Split[4]=='true'){
				$jenis_pasien ='Pasien Baru';
			}else if($Split[4]=='false'){
				$jenis_pasien ='Pasien Lama';
			}
		}

		$no = 0;
		if($isikriteria==="ruang")
		{
			$fieldkriteriadet="sp.kd_unit";
			$groupingby ='u.nama_unit';
		}
		
		if($isikriteria==="spesialis")
		{
			$fieldkriteriadet="ng.kd_spesial";
			$groupingby ='s.spesialisasi';
		}
		
		if($isikriteria==="kelas")
		{
			$fieldkriteriadet="sp.kd_kelas";
			$groupingby ='kl.kelas';
		}
		
		$query = $this->db->query("select distinct spesialis2 from (Select  $groupingby as spesialis2, 
										ps.KD_Pasien as kdpasien,
										ps.Nama as namapasien,
										case when ps.Jenis_Kelamin=true then 'L' else 'P' end as jk,
										ps.Tgl_Lahir,
										ps.Alamat as alamatpas,
										pk.Pekerjaan as pekerjaan,
										k.Tgl_Masuk as tglmas,
										k.Jam_Masuk as jammas,
										c.Customer as customer,
										s.spesialisasi as spesialis,
										z.user_names as username
									From (pasien ps
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)
										inner join  Kunjungan k   on k.Kd_Pasien = ps.Kd_Pasien
										INNER JOIN Customer c on c.Kd_Customer=k.Kd_Customer
										inner join transaksi t on t.kd_pasien = k.kd_pasien and  k.tgl_masuk = t.tgl_transaksi AND k.Kd_Unit=t.Kd_Unit AND k.Urut_Masuk=t.Urut_Masuk
										inner  join nginap ng on ng.kd_pasien = k.kd_pasien and ng.tgl_masuk = k.tgl_masuk AND k.Kd_Unit=ng.Kd_Unit_kamar AND k.Urut_masuk=ng.Urut_masuk
										inner join spesialisasi s on s.kd_spesial = ng.kd_spesial
										inner join spc_unit sp ON sp.kd_unit = k.kd_unit 
										inner join unit u on u.kd_unit=sp.kd_unit
										inner join kelas

										kl ON kl.kd_kelas = sp.kd_kelas inner join Zusers z on t.kd_user = z.kd_user
										inner join kontraktor ktr ON c.kd_customer = ktr.kd_customer
										where $fieldkriteriadet in ($criteria) and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $pasientype
										group by  ps.Kd_Pasien,ps.Nama ,ps.Alamat, ps.jenis_kelamin,
										ps.Tgl_Lahir,k.Tgl_Masuk, pk.pekerjaan,
										k.Jam_masuk , k.Tgl_masuk, k.Urut_masuk, k.Kd_Unit, k.KD_Pasien,$groupingby, c.Customer,s.spesialisasi,z.user_names 
										Order By  $groupingby) x order by spesialis2
										")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="13">Laporan Registrasi Detail Rawat Inap</th>
						</tr>
						<tr>
							<th  colspan="13">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
						<tr>
							<th  colspan="13">'.$jenis_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='
				<table class="t1" border = "1">
					<thead>
					  <tr>
							<th align="center" width="100">Unit</th>
							<th align="center" width="24" >No. </th>	 
							<th align="center" width="80" >No. Medrec</th>
							<th align="center" width="210">Nama Pasien</th>
							<th align="center" width="220" >Alamat</th>
							<th align="center" width="26">JK</th>
							<th align="center" width="150" >Umur</th>
							<th align="center" width="82" >Pekerjaan</th>
							<th align="center" width="68" >Tanggal Masuk</th>
							<th align="center" width="30" >Jam Masuk</th>
							<th align="center" width="100">Spesialisasi</th>
							<th align="center" width="63" >Customer</th>
							<th align="center" width="63" >User</th>
					  </tr>
					</thead>';

        $html.='<tbody>';
		
		if(count($query)>0){
			
			foreach($query as $line){
				$spesialis= $line->spesialis2;
				$html.= '<tr><td>'.$spesialis.'</td><td colspan="12"></td></tr>';
				$query_sub = $this->db->query("select * from (Select  $groupingby as spesialis2, 
												ps.KD_Pasien as kdpasien,
												ps.Nama as namapasien,
												case when ps.Jenis_Kelamin=true then 'L' else 'P' end as jk,
												ps.Tgl_Lahir,
												ps.Alamat as alamatpas,
												pk.Pekerjaan as pekerjaan,
												k.Tgl_Masuk as tglmas,
												k.Jam_Masuk as jammas,
												c.Customer as customer,
												s.spesialisasi as spesialis,
												z.user_names as username
											From (pasien ps
												left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)
												inner join  Kunjungan k   on k.Kd_Pasien = ps.Kd_Pasien
												INNER JOIN Customer c on c.Kd_Customer=k.Kd_Customer
												inner join transaksi t on t.kd_pasien = k.kd_pasien and  k.tgl_masuk = t.tgl_transaksi AND k.Kd_Unit=t.Kd_Unit AND k.Urut_Masuk=t.Urut_Masuk
												inner  join nginap ng on ng.kd_pasien = k.kd_pasien and ng.tgl_masuk = k.tgl_masuk AND k.Kd_Unit=ng.Kd_Unit_kamar AND k.Urut_masuk=ng.Urut_masuk
												inner join spesialisasi s on s.kd_spesial = ng.kd_spesial
												inner join spc_unit sp ON sp.kd_unit = k.kd_unit 
												inner join unit u on u.kd_unit=sp.kd_unit
												inner join kelas

												kl ON kl.kd_kelas = sp.kd_kelas inner join Zusers z on t.kd_user = z.kd_user
												inner join kontraktor ktr ON c.kd_customer = ktr.kd_customer
												where $fieldkriteriadet in ($criteria) and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $pasientype
												group by  ps.Kd_Pasien,ps.Nama ,ps.Alamat, ps.jenis_kelamin,
												ps.Tgl_Lahir,k.Tgl_Masuk, pk.pekerjaan,
												k.Jam_masuk , k.Tgl_masuk, k.Urut_masuk, k.Kd_Unit, k.KD_Pasien,$groupingby, c.Customer,s.spesialisasi,z.user_names 
												Order By  $groupingby) x where spesialis2='$spesialis'
												")->result();
				// echo '{success:true, totalrecords:'.count($query_sub).', listData:'.json_encode($query_sub).'}';
				$no=1;
				foreach($query_sub as $line2){
				
					$tgl_lahir = new DateTime ($line2->tgl_lahir);
					$today = new DateTime($line2->tglmas);
					$diff = $today->diff($tgl_lahir);
					$umurAr=$diff->y;
					$html.='<tr>
								<td></td>
								<td align="center">'.$no.'</td>
								<td width="50" align="left">'.$line2->kdpasien.'</td>
								<td width="50" align="left">'.$line2->namapasien.'</td>
								<td width="50" align="left">'.$line2->alamatpas.'</td>
								<td width="50" align="center">'.$line2->jk.'</td>
								<td width="50" align="center">'.$umurAr.' th </td>
								<td width="50" align="left">'.$line2->pekerjaan.'</td>
								<td width="50" align="center">'.  date('d-M-Y', strtotime($line2->tglmas)).'</td>
								<td width="50" align="left">'.date('h:m', strtotime($line2->jammas)).'</td>
								<td width="50" align="left">'.$line2->spesialis.'</td>
								<td width="50" align="left">'.$line2->customer.'</td>
								<td width="50" align="left">'.$line2->username.'</td>
							</tr>';
					$no++;
				}
			}
			
		}else{
			$html.='<tr><td colspan="13" align="center"> Data Tidak Ada</td></tr>';
		}
		
		$html.='</tbody></table>';
		// $prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Registrasi_Detail_Rawat_Inap.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L',' Laporan Registrasi Detail Rawat Inap',$html);
		
		}
		
	}
	
	function cetak_lap_reg_summary(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        //
		$Split = explode("##@@##", $getcriteria,  4);
		$tglsum = $Split[0];
		$tglsummax = $Split[1];
       
		$criteria="";
		$tmpunit = explode(',', $Split[3]);
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "'".$tmpunit[$i]."',";
		}
		
		$feildkriteria="sp_u.kd_unit";
		$criteria = substr($criteria, 0, -1);
		$pasientype = $Split[2];
		$jenis_pasien='';
		if($pasientype=='kabeh')
		{
			$pasientype="";
			$jenis_pasien ='Semua Pasien';
		}
		else{
			$pasientype=" and k.Baru='".$Split[2]."'";
			if($Split[2]=='true'){
				$jenis_pasien ='Pasien Baru';
			}else if($Split[2]=='false'){
				$jenis_pasien ='Pasien Lama';
			}
		}
		$q = $this->db->query(" Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
								Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
								From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
									Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
									Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
									Case When k.Baru=true Then 1 else 0 end as Br,
									Case When k.Baru=false Then 1 else 0 end as Lm,
									Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
									Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
									case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
									,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
									case when ktr.jenis_cust=0 then 1 else 0 end as umum
									From Unit u
									INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
									Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
									LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='1'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in( $criteria) $pasientype
								) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
		
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="10">Laporan Summary Pasien Rawat Inap</th>
						</tr>
						<tr>
							<th  colspan="10">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
						<tr>
							<th  colspan="10">'.$jenis_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" cellpadding="3">
				<thead>
				<tr>
					<th align="center" width="40" rowspan="2">No. </th>
					<th align="center"  width="100" rowspan="2">Nama Unit </th>
					<th align="center"  width="60" rowspan="2">Jumlah Pasien</th>
					<th align="center" colspan="2">Jenis kelamin</th>
					<th align="center" colspan="2">Kunjungan</th>
					<th align="center"  rowspan="2">Perusahaan</th>
					<th align="center"  rowspan="2">Askes</th>
					<th align="center"  rowspan="2">Umum</th>
				  </tr>
				 <tr>    
					<th align="center" >L</th>
					<th align="center" >P</th>
					<th align="center" >Baru</th>
					<th align="center" >Lama</th>
				 </tr>  
				</thead>
				<tfoot>
				</tfoot><tbody>';
        if($q->num_rows == 0)
        {
           $html.='<tr><td colspan="10" align="center"> Data Tidak Ada</td></tr></tbody>';
        } else {
			$query = $q->result();
		    
			$no = 0;
				foreach ($query as $line) 
				{
				   $no++;
				   $html.='<tr class="headerrow"> 
							<td align="right">'.$no.'</td>

							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.number_format($line->jumlahpasien,0,',','.').'</td>
							<td align="right">'.number_format($line->lk,0,',','.').'</td>
							<td align="right">'.number_format($line->pr,0,',','.').'</td>
							<td align="right">'.number_format($line->br,0,',','.').'</td>
							<td align="right">'.number_format($line->lm,0,',','.').'</td>
							<td align="right">'.number_format($line->perusahaan,0,',','.').'</td>
							<td align="right">'.number_format($line->askes,0,',','.').'</td>
							<td align="right">'.number_format($line->umum,0,',','.').'</td>
							</tr>';
				}
				$html.='</tbody>';
				$queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
											Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
											From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
													Case When k.Baru=true Then 1 else 0 end as Br,
													Case When k.Baru=false Then 1 else 0 end as Lm,
													Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
													,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
													case when ktr.jenis_cust=0 then 1 else 0 end as umum
													From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='1' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in($criteria)
											 ) x")->result();
				foreach ($queryjumlah as $line) 
				{
					$html.='
							<tfoot>
								<tr>
										<th colspan="2" align="right"> Jumlah </th>
										<th align="right">'.number_format($line->jumlahpasien,0,',','.').'</th>
										<th align="right">'.number_format($line->lk,0,',','.').'</th>
									   <th align="right">'.number_format($line->pr,0,',','.').'</th>
									   <th align="right">'.number_format($line->br,0,',','.').'</th>
									   <th align="right">'.number_format($line->lm,0,',','.').'</th>
									   <th align="right">'.number_format($line->perusahaan,0,',','.').'</th>
									   <th align="right">'.number_format($line->askes,0,',','.').'</th>
									   <th align="right">'.number_format($line->umum,0,',','.').'</th>
								</tr>
							</tfoot>';
				}   
		}
		$html.='</table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Registrasi_Summary_Rawat_Inap.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('P',' Laporan Registrasi Summary Rawat Inap',$html);
		
		}
	}
	
	function cetak_lap_pasien_rwi(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        $Split = explode("##@@##", $getcriteria,  11);
		//print_r($Split); 
		
		if(count($Split) > 0){
			$orderby= $Split[1];
			$unitruang= $Split[3];
			$kdUnit= $Split[4];
			$kamar= $Split[6];
			$noKamar= $Split[7];
			$kelompokPasien= $Split[8];
			$customer = $Split[9];
			$kdCustomer = $Split[10];
			
			if($customer=='NULL'){
				$customer='Semua';
			} else{
				$customer=$customer;
			}
			
			if($orderby == 'No Medrec'){
				$orderby='no_medrec';
			} else if($orderby == 'Nama'){
				$orderby='nama';
			}else if($orderby == 'Tanggal Masuk'){
				$orderby='tgl_masuk';
			} else {
				$orderby='nama_kamar';
			}
			
			if($unitruang == 'Semua' and $kamar == 'SEMUA' and $kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kamar == 'SEMUA'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and i.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA' and $kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							ORDER BY ".$orderby."";
				
			} else if($unitruang == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and i.no_kamar='".$noKamar."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003')
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and i.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else {
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and i.no_kamar='".$noKamar."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			}
		}
		
		$kd_kasir = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$queryHasil = $this->db->query( " SELECT p.Kd_Pasien as No_medrec, p.Nama, case when p.Alamat='' then '' else p.Alamat end,  
												p.Nama_Keluarga, case when p.Nama_Keluarga='' then '' else p.Nama_Keluarga end,  
												t.Tgl_Transaksi as Tgl_Masuk, i.No_Kamar, kmr.
												Nama_Kamar  
											FROM (((Pasien_Inap i 
												INNER JOIN (Transaksi t 
												INNER JOIN (Kunjungan k 
												INNER JOIN Customer c ON k.kd_customer = c.kd_customer)  ON t.kd_pasien = k.kd_pasien 
													AND t.kd_unit = k.kd_unit AND t.tgl_transaksi = k.tgl_masuk 
													AND t.urut_masuk = k.urut_masuk)  ON i.Kd_Kasir = t.Kd_Kasir
													AND i.No_Transaksi = t.No_Transaksi) 
												INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
												INNER JOIN Kamar kmr ON i.Kd_Unit = kmr.Kd_Unit AND i.No_Kamar = kmr.No_Kamar ) 
												inner join kontraktor ktr on k.kd_customer = ktr.kd_customer 
											WHERE 
												t.Kd_Kasir = '".$kd_kasir."'
												AND 
												i.Kd_Unit IN (SELECT Kd_Unit FROM Unit ".$param."
											");
											
		$query = $queryHasil->result();
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="7">PASIEN RAWAT INAP (RWI)</th>
						</tr>
						<tr>
							<th  colspan="7">'.date("d M Y").' </th>
						</tr>
						<tr>
							<th  colspan="7">Kelompok Pasien : '.$kelompokPasien.' ( '.$customer.' )</th>
						</tr>
					</tbody>
				</table><br>';
			
			//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
			$html.='
					<table class="t1" border = "1" cellpadding="3">
					<thead>
					  <tr>
						<th>No</td>
						<th  align="center">No Medrec</td>
						<th >Nama Pasien</td>
						<th >Alamat</td>
						<th >Nama Keluarga</td>
						<th >Tgl. Masuk</td>
						<th >Kamar</td>
					  </tr>
					</thead><tbody> ';
			
		if(count($query) == 0)
        {
            $html.='<tr><td colspan="7" align="center" >Data Tidak Ada</td></tr>';
        }else {									
			
			$no = 0;
			
			foreach ($query as $line) 
			{
				$noMedrec=$line->no_medrec;
				$nama=$line->nama;
				$alamat=$line->alamat;
				$namaKeluarga=$line->nama_keluarga;
				$namaKamar=$line->nama_kamar;
				
				$tmptglmasuk=substr($line->tgl_masuk, 0, 10);
				$tmptglmasuk=date('d-M-Y',strtotime($tmptglmasuk));
				
				$no++;            
				$html.='<tr > 
							<td align="center">'.$no.'</td>
							<td >'.$noMedrec.'</td>
							<td >'.$nama.'</td>
							<td >'.$alamat.'</td>
							<td >'.$namaKeluarga.'</td>
							<td >'.$tmptglmasuk.'</td>
							<td >'.$namaKamar.'</td>
						</tr>';
			}
			
		}			 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Pasien_Inap.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('P',' Laporan Pasien Rawat Inap',$html);
		
		}
	}
	
	function cetak_lap_pasien_pulang_rwi(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
	
        $Split = explode("##@@##", $getcriteria,  15);
		
		ini_set('memory_limit', '-1');
		if(count($Split) > 0){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$orderby= $Split[4];
			$unitruang= $Split[6];
			$kdUnit= $Split[7];
			$kamar= $Split[9];
			$noKamar= $Split[10];
			$kelompokPasien= $Split[11];
			$customer = $Split[12];
			$kdCustomer = $Split[13];
			
			if($customer=='NULL'){
				$customer='Semua';
			} else{
				$customer=$customer;
			}
			
			if($orderby == 'No Medrec'){
				$orderby='no_medrec';
			} else if($orderby == 'Nama'){
				$orderby='nama';
			}else if($orderby == 'Tanggal Masuk'){
				$orderby='tgl_masuk';
			} else {
				$orderby='nama_kamar';
			}
			
			
			if($unitruang == 'Semua' and $kamar == 'SEMUA' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							ORDER BY ".$orderby."";
				
			} else if($unitruang == 'Semua' and $kamar == 'SEMUA'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)	
							and g.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'	
							and g.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else if($kelompokPasien == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)	
							and g.no_kamar='".$noKamar."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else{
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'	
							and g.no_kamar='".$noKamar."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			}
			
		}
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="10">LAPORAN PASIEN PULANG ('.$unitruang.')</th>
						</tr>
						<tr>
							<th  colspan="10">Tanggal '.$tglAwal.' s/d '.$tglAkhir.'</th>
						</tr>
						<tr>
							<th  colspan="10">Kelompok Pasien '.$kelompokPasien.' ( '.$customer.' )</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table   border = "1" cellpadding="3">
				<thead>
				  <tr>
					<th>No</th>
					<th align="center">No Medrec</th>
					<th>Nama Pasien</th>
					<th>Alamat</th>
					<th>Telepon</th>
					<th>Nama Keluarga</th>
					<th>Tgl. Masuk</th>
					<th>Keluar</th>
					<th>Lama (Hari)</th>
					<th>Kamar</th>
				  </tr>
				</thead><tbody>';
				
		$queryHasil = $this->db->query( " SELECT p.Kd_Pasien as No_Medrec , p.Nama, p.Alamat, p.Telepon, 
											Nama_keluarga, case when p.Nama_Keluarga = '' then '' else p.Nama_Keluarga end, 
											g.TGL_MASUK , g.Tgl_Keluar, g.No_Kamar, k.Nama_Kamar , g.Tgl_Keluar-g.TGL_MASUK as lama, 
											g.kd_unit_kamar, c.customer
										  FROM ((Nginap g
											INNER JOIN Transaksi t ON t.Kd_Pasien = g.Kd_Pasien 
												AND t.Kd_Unit = g.Kd_Unit 
												AND t.Urut_Masuk = g.Urut_Masuk 
												AND t.Tgl_Transaksi = g.Tgl_Masuk 
												AND t.Tgl_CO = g.Tgl_Keluar 
											INNER JOIN KUNJUNGAN kj ON t.kd_pasien = kj.kd_pasien 
												AND t.kd_unit = kj.kd_unit 
												AND t.Tgl_Transaksi = kj.TGL_MASUK 
												And t.URUT_MASUK = kj.URUT_MASUK 
											inner join CUSTOMER c on kj.kd_customer=c.kd_customer
											INNER JOIN Pasien p ON g.Kd_Pasien = p.Kd_Pasien) ) 
											INNER JOIN KAMAR k ON g.Kd_Unit_Kamar = k.Kd_Unit 
												AND g.No_Kamar = k.No_Kamar 
											inner join kontraktor ktr on kj.kd_customer = ktr.kd_customer 
												AND g.Akhir = 't' 
												AND g.Kd_Unit_Kamar IN (SELECT Kd_Unit FROM Unit ".$param."
											");
											
		$query = $queryHasil->result();
		if(count($query) == 0)
        {
           $html.="<tr><td colspan='10' align='center'> Data Tidak Ada</td></tr>";
        }
        else {		
			$no = 0;
			
				foreach ($query as $line) 
				{
					$noMedrec=$line->no_medrec;
					$nama=$line->nama;
					$alamat=$line->alamat;
					$telepon=$line->telepon;
					$namaKeluarga=$line->nama_keluarga;
					$lama=$line->lama;
					$namaKamar=$line->nama_kamar;
					
					//"2015-05-13 00:00:00"
					$tmptglmasuk=substr($line->tgl_masuk, 0, 10);
					$tmptglkeluar=substr($line->tgl_keluar, 0, 10);
					
					 //$tmpumur=substr(, 0, 2);date('d-M-Y',strtotime($date1 . "+1 days"));
					$tmptglmasuk=date('d-M-Y',strtotime($tmptglmasuk));
					$tmptglkeluar=date('d-M-Y',strtotime($tmptglkeluar));
					
					//"6 days"			
					if($tmptglmasuk == $tmptglkeluar){
						$lamanginap= '1';
					} else{
						$pisahhari = explode(" ", $lama,  2);
						$lamanginap=$pisahhari[0];
					}
					
					$no++;            
					$html.='
						<tr class="headerrow"> 
							<td align="center">'.$no.'</td>
							<td>'.$noMedrec.'</td>
							<td >'.$nama.'</td>
							<td >'.$alamat.'</td>
							<td >'.$telepon.'</td>
							<td >'.$namaKeluarga.'</td>
							<td >'.$tmptglmasuk.'</td>
							<td >'.$tmptglkeluar.'</td>
							<td align="center">'.$lamanginap.'</td>
							<td >'.$namaKamar.'</td>
						</tr>
					';
				}	
		}
		$html.="</tbody></table>";		
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Pasien_Pulang.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('P',' Laporan Pasien Pulang',$html);
		
		}
	}
	function cetak_lap_keluar_masuk(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
		$jenis = $param->jenis;
	
	
        $Split = explode("##@@##", $getcriteria,  4);
		if (count($Split) > 0 )
		{
			$tmptmbahParam = $Split[0];
			$tmptgl = $Split[1];
			if($tmptgl === 'Tanggal')
			{
				$tgl1 = $Split[2];
				$tgl2 = $Split[3];
				if ($tgl1 === $tgl2)
				{
					$kriteria = "Periode ".$tgl1."";
				}else{
					$kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
				}
				$tmptmbahParamtanggal = " K.Tgl_Masuk >= ('".$tgl1."') and K.Tgl_Masuk <= ('".$tgl2."') ";
			}else{   
				$tgl1 = $Split[2];
				$tgl2 = $Split[3];   

				$tmptgl1 = explode("-",$tgl1);
				$bln1 = $tmptgl1[0];
				$thn1 = $tmptgl1[1];
				$tmptgl2 = explode("-",$tgl1);
				$bln2 = $tmptgl2[0];
				$thn2 = $tmptgl2[1];

				switch ($bln1) {
					case '01': $textbln = 'Januari';
						break;
					case '02': $textbln = 'Febuari';
						break;
					case '03': $textbln = 'Maret';
						break;
					case '04': $textbln = 'April';
						break;
					case '05': $textbln = 'Mei';
						break;
					case '06': $textbln = 'Juni';
						break;
					case '07': $textbln = 'Juli';
						break;
					case '08': $textbln = 'Agustus';
						break;
					case '09': $textbln = 'September';
						break;
					case '10': $textbln = 'Oktober';
						break;
					case '11': $textbln = 'November';
						break;
					case '12': $textbln = 'Desember';
				}

				switch ($bln2) {
					case '01': $textbln2 = 'Januari';
						break;
					case '02': $textbln2 = 'Febuari';
						break;
					case '03': $textbln2 = 'Maret';
						break;
					case '04': $textbln2 = 'April';
						break;
					case '05': $textbln2 = 'Mei';
						break;
					case '06': $textbln2 = 'Juni';
						break;
					case '07': $textbln2 = 'Juli';
						break;
					case '08': $textbln2 = 'Agustus';
						break;
					case '09': $textbln2 = 'September';
						break;
					case '10': $textbln2 = 'Oktober';
						break;
					case '11': $textbln2 = 'November';
						break;
					case '12': $textbln2 = 'Desember';
				}
				
				if ($tgl1 === $tgl2)
				{
				   $kriteria = "Periode ".$textbln." ".$thn1."";
				}else
				{
					$kriteria = "Periode ".$textbln." ".$thn1." s/d ".$textbln2." ".$thn2."";
				}
					
				$tmptmbahParamtanggal = " date_part('Year',K.Tgl_Masuk) >= ".$thn1."  and date_part('month',K.Tgl_Masuk) >= ".$bln1."  and date_part('Year',K.Tgl_Masuk) <= ".$thn2."   
											  and date_part('month',K.Tgl_Masuk) <= ".$bln2."   ";
			}
										  
			$Param = "Where ".$tmptmbahParamtanggal." and left(K.kd_unit,1)='1' and to_char(nginap.tgl_inap, 'dd-MM-YYYY') = to_char(k.tgl_masuk, 'dd-MM-YYYY')"
					. " ".$tmptmbahParam." ORDER BY p.nama";
        }
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="14">Daftar Pasien Masuk/Keluar Rawat Inap</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="14">'.$kriteria.'</th>
						</tr>
						<tr>
							<th  colspan="14">'.$jenis.'</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="3">
				<thead>
					<tr>
						<th rowspan="2" align="center"><strong>No</strong></th>
						<th rowspan="2" align="center"><strong>No. medrec</strong></th>
						<th rowspan="2" align="center"><strong>Nama Pasien</strong></th>
						<th rowspan="2" align="center"><strong>Alamat</strong></th>
						<th rowspan="2" align="center"><strong>JK</strong></th>
						<th rowspan="2" align="center"><strong>Umur</strong></th>
						<th rowspan="2" align="center"><strong>Kelas</strong></th>
						<th rowspan="2" align="center"><strong>Ruang</strong></th>
						<th rowspan="2" align="center"><strong>Unit</strong></th>
						<th height="23" colspan="2" align="center"><strong>Masuk</strong></th>
						<th colspan="2" align="center"><strong>Keluar</strong></th>
						<th rowspan="2" align="center"><strong>Lama Rawat</strong></th>
					</tr>
					<tr>
						<th height="23" align="center"><strong>Tanggal</strong></th>
						<th align="center"><strong>Jam</strong></th>
						<th align="center"><strong>Tanggal</strong></th>
						<th align="center"><strong>Jam</strong></th>
					</tr>
				</thead>
				<tfoot></tfoot><tbody>';
				
		$tmpquery = "Select P.Kd_Pasien, P.Nama, P.Alamat, P.tgl_lahir,
					CASE WHEN P.Jenis_Kelamin = 't' then 'L' else 'P' End as JK,
					case when age(K.Tgl_Masuk,P.Tgl_Lahir)='00:00:00' then '1' else age(K.Tgl_Masuk,P.Tgl_Lahir) end as JmlUmur, 
					Spesialisasi.spesialisasi, Kelas.Kelas, kamar.nama_kamar, 
					to_char(k.tgl_masuk, 'dd-MM-YYYY') as tgl_masuk, 
					to_char(k.jam_masuk, 'HH:ss') as Jam_Masuk, 
					to_char(k.tgl_keluar, 'dd-MM-YYYY') as tgl_keluar, 
					to_char(k.jam_keluar, 'HH:ss') as Jam_Keluar
						From pasien p inner join ((kunjungan k inner join 
													(unit u inner join kelas on u.kd_kelas=kelas.kd_kelas) on u.kd_unit=k.kd_unit) 
													INNER JOIN (((Select * from NGINAP Where Akhir='t') nginap inner join spesialisasi on nginap.kd_spesial=spesialisasi.kd_spesial)  
													inner join kamar on nginap.kd_unit_kamar=kamar.kd_unit and nginap.no_kamar=kamar.no_kamar) 
												ON k.KD_Pasien = NGINAP.KD_Pasien AND K.KD_UNIT=NGINAP.KD_UNIT AND K.TGL_MASUK=NGINAP.TGL_MASUK AND K.URUT_MASUK=NGINAP.URUT_MASUK) 
													on p.kd_pasien=k.kd_pasien  ".$Param;
		$q = $this->db->query($tmpquery);
		
		if($q->num_rows == 0)
        {
            $html.="<tr><td colspan='14' align='center'> Data Tidak Ada</td></tr>";
        }else {
        
			$query = $q->result();
			$no = 0;

			
				foreach ($query as $line) 
				{
					$no++;
					$tgl_lahir = new DateTime ($line->tgl_lahir);
					$today = new DateTime($line->tgl_masuk);
					$diff = $today->diff($tgl_lahir);
					$umurAr=$diff->y;
					if($umurAr == 0){
						$umurAr=1;
					}
					$start_date =new DateTime($line->tgl_masuk);
					$end_date =new DateTime($line->tgl_keluar);
					$lama = $start_date->diff($end_date);
					$html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="center">'.$line->kd_pasien.'</td>
							<td align="left">'.$line->nama.'</td>
							<td align="left">'.$line->alamat.'</td>
							<td align="center">'.$line->jk.'</td>
							<td align="center">'.$umurAr.' th</td>
							<td align="left">'.$line->kelas.'</td>
							<td align="left">'.$line->nama_kamar.'</td>
							<td align="center">'.$line->spesialisasi.'</td>
							<td align="center">'.$line->tgl_masuk.'</td>
							<td align="center">'.$line->jam_masuk.'</td>
							<td align="center">'.$line->tgl_keluar.'</td>
							<td align="center">'.$line->jam_keluar.'</td>
							<td align="center">'.$lama->days.' hr </td>
						</tr>';
				}
         }
		$html.="</tbody></table>";		
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Pasien_Keluar_Masuk.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L',' Laporan_Keluar_Masuk',$html);
		
		}
	}
	
	function cetak_lap_perkamar(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
	
        $Split = explode("##@@##", $getcriteria,  4);
		if (count($Split) > 0 ){
			$tgl1 = $Split[0];
			$tgl2 = $Split[1];
			$tmpkelas = $Split[2];
			$kelas = $Split[3];
		}
		if ($tgl1 === $tgl2){
			$kriteria = "Periode ".$tgl1." ";
		}else{
			$kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
		}
		if ($kelas != 0){
			$tmpParamkelas = " and i.no_kamar='".$kelas."' ";
		}else{
			$tmpParamkelas = " ";
		}
		
        if ($kelas != 0){
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.no_kamar='".$kelas."' and i.Akhir='t' ";
        }else{
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.Akhir='t' ";
        }
      
        if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="9">Daftar Pasien Masuk Rawat Inap PerKamar</th>
						</tr>
						<tr>
							<th  colspan="9">Tanggal '.$kriteria.'</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="3">
				<thead>
					<tr>
						<td align="center" width="31"><strong>No.</strong></td>
						<td align="center" colspan="2" width="196"><strong>No. Medrec / Nama Pasien</strong></td>
						<td align="center" width="184"><strong>Alamat</strong></td>
						<td align="center" width="26"><strong>JK</strong></td>
						<td align="center" width="79"><strong>Umur</strong></td>
						<td align="center" width="83"><strong>Agama</strong></td>
						<td align="center" width="112"><strong>Nama Dokter</strong></td>
						<td align="center" width="103"><strong>Tgl Masuk</strong></td>
					</tr>
				</thead>
				<tfoot></tfoot><tbody>';
				
		$Params = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
					and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') ".$tmpParamkelas." and i.Akhir='t' ORDER BY kamar ";
        
        $tmpquery = "SELECT  p.kd_pasien,p.Nama, 
						CASE WHEN p.jenis_kelamin = 't' then 'L' else 'P' END AS JK, Agama.Agama, k.Tgl_Masuk, tgl_lahir,
						CASE WHEN age(k.Tgl_Masuk, tgl_lahir) ='00:00:00' then '1' else age(k.Tgl_Masuk, tgl_lahir) end as JmlUmur,
						P.Alamat,d.Nama AS Nama_Dokter,u.nama_unit AS Kelas,kmr.no_kamar || '(' || kmr.nama_kamar || ')' AS KAMAR, to_char(i.Tgl_Inap, 'dd-MM-YYYY') AS Tgl_Masuk
						FROM unit u
						INNER JOIN 
						(Pasien p  
						inner JOIN Agama on agama.kd_agama=p.kd_agama  
						inner Join (transaksi t 
						inner JOIN ((nginap i 
						inner JOIN kamar kmr ON (i.no_kamar=kmr.no_kamar) AND (i.kd_unit_kamar=kmr.kd_unit)) 
						inner Join(kunjungan k 
						inner JOIN dokter d  ON d.kd_dokter=K.kd_dokter) ON (k.kd_unit=i.kd_unit)  
						AND (k.kd_pasien=i.kd_pasien)  
						AND (k.tgl_masuk=i.tgl_masuk) 
						AND (k.urut_masuk=i.urut_masuk)) ON (t.kd_pasien=i.kd_pasien) 
						AND (t.kd_unit=i.kd_unit)  and (t.tgl_transaksi=i.tgl_masuk) 
						and (t.urut_masuk=i.urut_masuk)) ON t.kd_pasien=p.kd_pasien) ON u.kd_unit=i.kd_unit_kamar ";
        $q = $this->db->query($tmpquery.$Params);
		$tmphquery = $this->db->query(" select kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar from kunjungan k 
										inner join transaksi t on k.kd_pasien = t.kd_pasien and  k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join kamar kmr on kmr.kd_unit = k.kd_unit
										inner join nginap i on kmr.no_kamar = i.no_kamar 
											".$tmptambah."
											group by kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar order by kmr.nama_kamar
										")->result();
		if($q->num_rows == 0){
			 $html.="<tr><td colspan='14' align='center'> Data Tidak Ada</td></tr>";

		}else{
			//perulangan kamar yg dipilih untuk ambil no_kamar
			foreach ($tmphquery as $line){
				$tmpparam2 = " Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
								and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.no_kamar= '".$line->no_kamar."' and i.kd_unit_kamar='".$line->kd_unit_kamar."' and i.Akhir='t' order by kmr.nama_kamar     
								";
				$dquery =  $this->db->query($tmpquery.$tmpparam2);
				$no = 0;
				if($dquery->num_rows == 0){
				   $html.='';
				}else{
					$html.='<tr>
							  <td></td><td colspan="2"><b>'.$line->nama_kamar.'</b></td><td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>';
					foreach ($dquery->result() as $d){
						$no++;
						// $umurAr=$this->date->umur1($d->tgl_lahir,$d->tgl_masuk);
										
						$tgl_lahir = new DateTime ($d->tgl_lahir);
						$today = new DateTime($d->tgl_masuk);
						$diff = $today->diff($tgl_lahir);
						$umurAr=$diff->y;
						$html.='<tr > 
									<td align="right">'.$no.'</td>
									<td width="50" style="border-right:none" align="center">'.$d->kd_pasien.'</td>
									<td width="50" style="border-left:none" align="center">'.$d->nama.'</td>
									<td width="50" >'.$d->alamat.'</td>
									<td width="50" align="center">'.$d->jk.'</td>
									<td width="50" align="center">'.$umurAr.' th </td>
									<td width="50" >'.$d->agama.'</td>
									<td width="50" >'.$d->nama_dokter.'</td>
									<td width="50" >'.$d->tgl_masuk.'</td>
							   </tr> ';
					}
				}
			}
		}  
		$html.="</tbody></table>";		
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Daftar_Pasien_Masuk_RWI_PerKamar.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('P','Daftar_Pasien_Masuk_RWI_PerKamar',$html);
		
		}
	}
	
	function cetak_lap_per_perujuk(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        $Split = explode("##@@##", $getcriteria,  11);
		if (count($Split) > 0 )
		{
			$tgl1 = $Split[0];
			$tgl2 = $Split[1];
			$tmprujuk = $Split[2];
			$rujuk = $Split[3];
			$tmpjenis = $Split[4];
			$unit = $Split[5];
			$criteria = "";
			$tmpunit = explode(',', $Split[5]);
			for($i=0;$i<count($tmpunit);$i++)
			{
			$criteria .= "'".$tmpunit[$i]."',";
			}
			$criteria = substr($criteria, 0, -1);
			
			$tmptambahParam = " ";
			$tmprujukan = "";   
			if ($tgl1 === $tgl2)
			{
				$kriteria = "Periode ".$tgl1;
			}else
			{
				$kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
			}
			if ($rujuk != 'Semua')
			{
				$tmprujukan = "and k.kd_Rujukan = ".$rujuk." ";
			}else
			{
				$tmprujukan = "";
			}
			$Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
					. $tmptambahParam
					. $tmprujukan
					. "and k.kd_Unit in ($criteria)  "
					. "Order By r.rujukan,u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
        }
      
       $tmphquery = $this->db->query("select kd_rujukan, rujukan from rujukan");
                    
        
        $tmpquery = "Select r.kd_rujukan,r.rujukan,u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
                        ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
                        k.Tgl_Masuk, ps.Tgl_Lahir, 
                        case when k.baru = 't' then 'X' else '' end as Baru,
                        case when k.Baru = 'f' then 'X' else '' end as Lama,
                        pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
                        c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

                        From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                        left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                        inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
                        LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
   
        $q = $this->db->query($tmpquery.$Param);
		
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="11">Daftar Rawat Inap PerRujukan</th>
						</tr>
						<tr>
							<th  colspan="11">Tanggal '.$kriteria.'</th>
						</tr>
					</tbody>
				</table><br>';
		
		$html.='
           <table class="t1" border = "1">
           <thead>
            <tr>
              <th align="center" width="24" rowspan="2">No. </th>
              <th align="center" width="137" rowspan="2">No. Medrec </th>
              <th align="center" width="137" rowspan="2">Nama Pasien</th>
              <th align="center" width="273" rowspan="2">Alamat</th>
              <th align="center" width="26" rowspan="2">JK</th>
              <th align="center" width="82" rowspan="2">Umur</th>
              <th align="center" colspan="2">kunjungan</th>
              <th align="center" width="82" rowspan="2">Pekerjaan</th>
              <th align="center" width="68" rowspan="2">Customer</th>
              <th align="center" width="63" rowspan="2">Rujukan</th>
            </tr>
            <tr>
              <th align="center" width="37">Baru</th>
              <th align="center" width="39">Lama</th>
            </tr>
          </thead>
           <tfoot> </tfoot><tbody>';
        if($q->num_rows == 0)
        {
			$html.="<tr><td colspan='11' align='center'> Data Tidak Ada</td></tr>";
        }else {
			$query = $q->result();
		   
			$tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
						. $tmptambahParam
						. $tmprujukan
						. "and k.kd_Unit in ($criteria) "
						. "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
			$dquery =  $this->db->query($tmpquery.$tmpparam2);
			$no = 0;
			if($dquery->num_rows == 0)
			{
			   $html.='';
			}else {
				$lastkdrujukan='';
				foreach ($q->result() as $d)
				{
					$no++;
					if($d->rujukan != $lastkdrujukan){
						$html.='<tr class="headerrow">
									   <th colspan="11" align="left">'.$d->rujukan.'</td>
								</tr>';
						$lastkdrujukan=$d->rujukan;
						$no=1;
					}
					$tgl_lahir = new DateTime ($d->tgl_lahir);
					$today = new DateTime($d->tgl_masuk);
					$diff = $today->diff($tgl_lahir);
					$umurAr=$diff->y;
					$html.='
						
					   <tr class="headerrow"> 
						   <td align="right">'.$no.'</td>
						   <td width="50">'.$d->kd_pasien.'</td>
						   <td width="50">'.$d->nama.'</td>
						   <td width="50">'.$d->alamat.'</td>
						   <td width="50" align="center">'.$d->jk.'</td>
						   <td width="50">'.$umurAr.' </td>
						   <td width="50" align="center">'.$d->baru.'</td>
						   <td width="50" align="center">'.$d->lama.'</td>
						   <td width="50">'.$d->pekerjaan.'</td>
						   <td width="50">'.$d->customer.'</td>
						   <td width="50" align="center">'.$d->textrujukan.'</td>
					   </tr>';
				}  
			}  
		}
		$html.="</tbody></table>";		
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Daftar_Rawat_Inap_PerRujukan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Daftar Rawat Inap PerRujukan',$html);
		
		}
	}
	
	function cetak_lap_ResponTime(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		$kel_pas = $param->kel_pas;
		$type_file = $param->type_file;
		$kel_pas2 = $param->kel_pas2;
		
		if($kel_pas == 1 ){
			$kelompokPasien='Semua';
		}else if($kel_pas == 2 ){
			$kelompokPasien='Perseorangan';
		}else if($kel_pas == 3 ){
			$kelompokPasien='Perusahaan';
		}else if($kel_pas == 4 ){
			$kelompokPasien='Asuransi';
		}
		
		if($kel_pas2 == '0'){
			$kriteria_customer="";
			$customer='Semua';
		}else{
			if($kel_pas==2){
				$kel_pas2='0000000001';
				$customer='Umum';
			}else{
				$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
			}
			
			$kriteria_customer ="  and k.kd_customer = '".$kel_pas2."' ";
			
		}
		$query = $this->db->query(" select p.KD_PASIEN, p.NAMA, p.ALAMAT, km.NAMA_KAMAR,
										date_trunc('day',t.tgl_boleh_plg) as TGL_BLHPLG, 
										date_trunc('second',t.jam_boleh_plg) as  DETIK_BLHPLG,
										date_trunc('day',k.TGL_KELUAR) as  TGL_CETAK,
										date_trunc('second',k.JAM_KELUAR) as  DETIK_CETAK,
										
										CASE WHEN k.JAM_KELUAR >= t.jam_boleh_plg 
											THEN date_part('day',k.tgl_keluar -  t.tgl_boleh_plg )
										ELSE 
											date_part ('day',k.tgl_keluar - t.tgl_boleh_plg )-1 
										END as selisih_tgl, 
										
										date_part('hour',k.JAM_KELUAR - t.jam_boleh_plg) as SELISIH_JAM,
										date_part('minute',k.JAM_KELUAR - t.jam_boleh_plg) as SELISIH_MENIT,
										date_part('second',k.JAM_KELUAR - t.jam_boleh_plg) as SELISIH_DETIK
										from kunjungan k
										  inner join PASIEN p on p.KD_PASIEN = k.KD_PASIEN
										  inner join TRANSAKSI t on t.KD_PASIEN = k.KD_PASIEN and t.KD_UNIT = k.KD_UNIT and t.TGL_TRANSAKSI = k.TGL_MASUK and t.URUT_MASUK = k.URUT_MASUK
										  inner join NGINAP ng on ng.KD_PASIEN = k.KD_PASIEN and ng.KD_UNIT = k.KD_UNIT and ng.TGL_MASUK = k.TGL_MASUK and ng.URUT_MASUK = k.URUT_MASUK
										  inner join KAMAR km on km.KD_UNIT = ng.KD_UNIT_KAMAR and km.NO_KAMAR = ng.NO_KAMAR
										where 
										  T.TGL_CO between '".$tgl_awal."' and '".$tgl_akhir."'
										  and 
										  left(k.KD_UNIT,3) = '100'				
										  and k.TGL_KELUAR is not null				
										  and ng.AKHIR = 't'							
										 ".$kriteria_customer."	")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th colspan="10" style='.$font_style.'>LAPORAN RESPONSE TIME PASIEN PULANG RAWAT INAP</th>
						</tr>
						<tr>
							<th colspan="10" style='.$font_style2.'>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="10"  style='.$font_style2.'> Kelompok Pasien '.$kelompokPasien.' ( '.$customer.' )</th>
						</tr>
					</tbody>
				</table><br>';
		
		$html.='
           <table class="t1" border = "1" cellpadding="2">
           <thead>
            <tr>
              <th align="center" >No. </th>
              <th align="center" >No. Medrec </th>
              <th align="center" >Nama Pasien</th>
              <th align="center" >Alamat</th>
              <th align="center" >Ruang Perawatan</th>
              <th align="center"  >Tgl Boleh Pulang Oleh Dokter</th>
              <th align="center"  >Jam Boleh Pulang Oleh Dokter</th>
              <th align="center"  >Tgl Cetak Billing</th>
              <th align="center"  >Jam Cetak Billing</th>
              <th align="center"  >Response Time</th>
            </tr>
          </thead>
        <tfoot> </tfoot><tbody>';
		$arr_time=array();
		if(count($query)>0){
			$no=0;
			$i=0;
			foreach ($query as $line){
				$arr_time[$i]=$line->selisih_jam.':'.$line->selisih_menit.':'.$line->selisih_detik;
				$jam_plg = substr($line->detik_blhplg,-8);//MENGAMBIL JAM
				if($line->selisih_jam < 10){
					$jam='0'.$line->selisih_jam; 
				}else{
					$jam=$line->selisih_jam; 
				}
				
				if($line->selisih_menit < 10){
					$menit='0'.$line->selisih_menit; 
				}else{
					$menit=$line->selisih_menit; 
				}
				
				if($line->selisih_detik < 10){
					$detik='0'.$line->selisih_detik; 
				}else{
					$detik=$line->selisih_detik; 
				}
				
				
				$no++;
				$html.=' <tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->kd_pasien.'</td>
							<td>'.$line->nama.'</td>
							<td>'.$line->alamat.'</td>
							<td>'.$line->nama_kamar.'</td>
							<td align="center">'.date('d-M-Y', strtotime($line->tgl_blhplg)).'</td>
							<td align="center">'.$jam_plg.'</td>
							<td align="center">'.date('d-M-Y', strtotime($line->tgl_cetak)).'</td>
							<td align="center">'.date('H:i:s', strtotime($line->detik_cetak)).'</td>
							<td align="center">'.$jam.':'.$menit.':'.$detik.'</td>
						</tr>';
				$i++;
			}
			 $sum = strtotime('00:00:00');
			 $sum2=0;  
			 foreach ($arr_time as $v){
					$sum1=strtotime($v)-$sum;
					$sum2 = $sum2+$sum1;
			}
			$sum3=$sum+$sum2;
			$html.='<tr><td colspan="9" align="right"> <b>Total Jam</b></td><td align="center">'.date("H:i:s",$sum3).'</td></tr>';
		}else{
			$html.='<tr><td colspan="10" align="center"> Data Tidak Ada</td></tr>';
		}
		// print_r($arr_time);
		$html.='</tbody></table>';		
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='LAPORAN_RESPONSE_TIME_RWI.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','LAPORAN RESPONSE TIME PASIEN PULANG RAWAT INAP',$html);
		
		}
	}
	
	function cetak_lap_batal_transaksi(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
		$params=explode('#aje#',$getcriteria);
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
			
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="11">'.$params[2].'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="11">'.$params[0].' s/d '.$params[1].'</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table border="1" cellpadding="3">
				<thead>
	 				<tr>
	 					<td align= "center"><b>No.</b></td>
	 					<td align= "center"><b>No. Medrec</b></td>
						<td align= "center"><b>Nama Pasien</b></td>
						<td align= "center"><b>No transaksi</b></td>
						<td align= "center"><b>Unit</b></td>
						<td align= "center"><b>Tanggal batal</b></td>
						<td align= "center"><b>Shift batal</b></td>
						<td align= "center"><b>Jam batal</b></td>
						<td align= "center"><b>Alasan Batal</b></td>
						<td align= "right"><b>Jumlah</b></td>
	 					<td align= "center"><b>Petugas </b></td>
	 				</tr>
				</thead><tbody>
			';
		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$query=$this->db->query("select 
						  kd_kasir,no_transaksi ,tgl_transaksi ,kd_pasien ,nama ,
						  kd_unit,nama_unit,
						  kd_user ,kd_user_del ,
						  shift, shiftdel ,jumlah,
						  user_name ,tgl_batal , ket,jam_batal  from history_batal_trans  
						where 
							kd_kasir='".$kd_kasir."' and 
							tgl_batal between '".$params[0]."' and  '".$params[1]."'
						 ")->result();
		$totall=0;
		if(count($query)>0){
			foreach ($query as $line) 
			{
				$tanggal=date_create($line->tgl_batal);
				$jam=date_create($line->jam_batal);
				$totall+=$line->jumlah;
				$no=1;
				$html.='
						<tr>
							<td>'.$no .'</td>
							<td>'.$line->kd_pasien .'</td>
							<td>'.$line->nama .'</td>
							<td>'.$line->no_transaksi .'</td>
							<td>'.$line->nama_unit .'</td>
							<td align="center">'.date_format($tanggal,"Y/m/d") .'</td>
							<td align="center">'.$line->shiftdel .'</td>
							<td  align="center">'.date_format($jam,"H:i") .'</td>
							<td  >'.$line->ket .'</td>
							<td  align= "right">'.number_format($line->jumlah,0,".",",") .'</td>
							<td  >'.$line->user_name .' </td>
						</tr>
					';
				$no++;
			}
			 $html.='
						<tr>
							<td align= "right" colspan="9"><strong>Grand Total</strong></td>
							<td align= "right"><strong>'.number_format($totall,0,".",",").'</strong></td>
							<td >&nbsp;</td>
						</tr>
						
					';
		}else{
			$html.='<tr><td colspan="11" align="center">Data Tidak Ada</td></tr>';
		}
		 $html.="</tbody></table>";
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Batal_Transaksi_Rawat_Inap.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('P','Laporan_Batal_Transaksi_Rawat_Inap',$html);
		
		}
	}
	
	function cetak_lap_pelayanan_dokter(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
		
		$Split = explode("##@@##", $getcriteria, 12);
		if (count($Split) > 0 ){
			$autocas = $Split[1];
			$unit = $Split[2];
			$kdunit = $Split[3];
			$dokter = $Split[4];
			$kddokter = $Split[5];
			$tglAwal = $Split[6];
			$tglAkhir = $Split[7];
			$kelPasien = $Split[9];
			$kdCustomer = $Split[10];
			//echo $kdCustomer;

			
			if($dokter =='Dokter' || $kddokter =='Semua'){
				$paramdokter="";
			} else{
				$paramdokter =" and dtd.kd_Dokter='$kddokter'";
			}
			
			if($kelPasien=='Semua' || $kdCustomer==NULL){
				$paramcustomer="";
			} else{
				$paramcustomer =" and k.Kd_Customer ='$kdCustomer'";
			}
			
			if($unit =='Unit' || $kdunit=='Semua'){
				$paramunit='';
			} else{
				$paramunit=" and t.kd_unit ='$kdunit' ";
			}
			
			if($autocas == 1){
				$params=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
			} else if($autocas == 2){
				$params=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
			} else if($autocas == 3 || $autocas == 4){
				$params="";
			}
			
		}
		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$queryHasil = $this->db->query( "
                                      Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
											Where 	dt.Kd_kasir='".$kd_kasir."'						
											and  t.ispay='t' 	
											And u.kd_bagian='1'						
											And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
											And dt.Qty * dtd.JP >0	
											".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
										Group By d.Nama, dtd.kd_Dokter 
										Order By Dokter, dtd.kd_Dokter 
                                        
		
                                      ");
		$query = $queryHasil->result();
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="5">Laporan Jasa Pelayanan Dokter Per Pasien</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="5">'.$tglAwal.' s/d '.$tglAkhir.'</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap">
					<thead>
					  <tr>
						<th >No</td>
						<th align="center">DOKTER/PASIEN</th>
						<th align="right">JP. DOKTER</th>
						<th align="right">PAJAK</th>
						<th align="right">JUMLAH</th>
					  </tr>
					</thead><tbody>';
		if(count($query) == 0)
		{
			$html.='<tr><td colspan="5" align="center"> Data Tidak Ada</td></tr>';
		} else {
            $no = 0;	
			$jd = 0;
			$pph = 0;
			$grand =0;
			foreach ($query as $line)
			{

				$no++;
				$html.='<tr class="headerrow">
							<th>'.$no.'</th>
							<td width="200" align="left" colspan="4">'.$line->dokter.'</td>
						</tr>';
				$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
				$dok=$qdok->kd_dokter;

				$queryHasil2 = $this->db->query( "Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
													Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
													From Detail_TRDokter dtd
														INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
														INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
														INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
														INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
														INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
														INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
														INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
														INNER JOIN unit u On u.kd_unit=t.kd_unit
													Where dt.Kd_kasir='".$kd_kasir."'
													And t.ispay='t'
													and u.kd_bagian='1'
													And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
													And dt.Qty * dtd.JP >0
													And dtd.kd_Dokter='$dok'
													".$paramcustomer."".$params."".$paramunit."
													Group By d.Nama, p.Kd_pasien
													Order By Dokter, p.Kd_pasien");
				$query2 = $queryHasil2->result();
				$noo=0;
				$sub_jumlah=0;
				$sub_pph=0;
				$sub_jd=0;
				foreach ($query2 as $line2)
				{
					$noo++;
					$sub_jumlah+=$line2->jumlah;
					$sub_pph +=$line2->pph;
					$sub_jd+=$line2->jd;
					$html.='<tr>
								<td > </td>
								<td >'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
								<td align="right" >'.number_format($line2->jd,0,',',',').'</td>
								<td align="right">'.number_format($line2->pph,0,',',',').'</td>
								<td align="right">'.number_format($line2->jumlah,0,',',',').'</td>
							</tr>';
				}
				$html.='<tr class="headerrow">
						<th align="right" colspan="2">Sub Total</th>
						<th align="right">'.number_format($sub_jd,0,',',',').'</th>
						<th align="right">'.number_format($sub_pph,0,',',',').'</th>
						<th align="right">'.number_format($sub_jumlah,0,',',',').'</th>
					</tr>';
				$jd += $sub_jd;
				$pph += $sub_pph;
				$grand += $sub_jumlah;
			}
			$html.='<tr class="headerrow">
						<th align="right" colspan="2">GRAND TOTAL</th>
						<th align="right">'.number_format($jd,0,',',',').'</th>
						<th align="right">'.number_format($pph,0,',',',').'</th>
						<th align="right" >'.number_format($grand,0,',',',').'</th>
					</tr>';
						
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='LAPORAN_JASA_PELAYANAN_DOKTER_PER_PASIEN.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('P','LAPORAN JASA PELAYANAN DOKTER PER PASIEN',$html);
		
		}
	}
	function cetak_lap_transaksi(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		
		$type_file = $param->type_file;
		
		$Split = explode("##@@##", $getcriteria, 10);
		$tglsum = $Split[0];
		$tglsummax = $Split[1];
		$kdtranfer = $this->db->query("select setting from sys_setting where key_data = 'sys_kd_pay_transfer'")->row()->setting;
		$tglsumx=$Split[0]+strtotime("+1 day");
		$tglsum2= date("Y-m-d", $tglsumx);
		$tglsummaxx=$Split[1]+strtotime("+1 day");
		$tglsummax2= date("Y-m-d", $tglsummaxx);
				
		if($Split[2]==="Semua" || $Split[2]==="undefined")
		{
			$kdUNIT2="";
			$kdUNIT="";
		}else
		{
			
			$kdUNIT2="";
			$kdUNIT="";
		}
				
		if($Split[3]==="Semua")
		{
			$jniscus="";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$Split[3]."";
		}
				
				
		if ($Split[4]==="NULL")
		{
			$customerx="";
		}else{
			$customerx=" And Ktr.kd_Customer='".$Split[4]."'";
		}
		
		if (count($Split)===9)
		{
			$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
			if ($Split[7]=== "ya")
			{
				if($Split[8]=== "ya"){
					$c_tindakan=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')
										or x.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1') )";
				
				}else{
					$c_tindakan=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
				}
			}else{
				$c_tindakan="";
			}
	
		}else {
			if ($Split[7]=== "ya")
			{
				if($Split[8]=== "ya"){
					$c_tindakan=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')
										or x.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1') )";
				
				}else{
					$c_tindakan=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
				}
			}else{
				$c_tindakan="";
			}
			
			$Shift4x="";
		}
		$shiftx = substr($Split[5], 0, -1);
			
					
		$criteria="";
		$tmpunit = explode(',', $Split[3]);
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1);
					
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="5">LAPORAN TRANSAKSI</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="5">Per Produk</th>
						</tr>
						<tr>
							<th  colspan="5">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
					</tbody>
				</table><br>';
	
		$html.='<table class="t1" border = "1">
				<thead>
				 <tr >
					<th align="center" >Unit</th>
					<th align="center"  >Tindakan</th> 
					<th align="center"  > Pasien</th>
					<th align="center"  >Produk</th>
					<th align="center" >(Rp.)</th>
					  
				 </tr></thead>';
			
		$fquery =$this->db->query("select kd_unit,nama_unit from unit where kd_bagian ='01' $kdUNIT 
							group by kd_unit,nama_unit  order by nama_unit asc")->result();
		$html.='<tbody>';
		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		
		foreach($fquery as $f)
		{
			$query = "Select U.Nama_Unit as namaunit, p.deskripsi as keterangan, Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan 
						From ((
								( Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk) 
									INNER JOIN 
									(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
										From  detail_transaksi Where kd_kasir ='$kd_kasir' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
										$Shift4x)  
										Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
									) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi) 
								INNER JOIN Unit u On u.kd_unit=t.kd_unit) 
						INNER JOIN Produk p on p.kd_produk=x.kd_produk 
						LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
						INNER JOIN 
							(
								select  kd_kasir,no_transaksi from DETAIL_BAYAR db where db.kd_pay<> '".$kdtranfer."' group by  kd_kasir,no_transaksi 
							) db On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
			Where 	left(k.kd_Unit,1) ='1'	
					--and t.ispay='true'	
					And k.kd_Unit ='".$f->kd_unit."'								
					$jniscus				
					$customerx
					$c_tindakan						
			Group By u.Nama_Unit, p.Deskripsi		
			Order by U.Nama_Unit, p.Deskripsi ";
			$result =$this->db->query($query)->result();
			$i=1;

			if(count($result) <= 0)
			{
				$html.='';
			}else{
				$html.='<tr><td ><b>'.$f->nama_unit.'</b></td><td></td><td></td><td></td><td></td></tr>';
				$pasientotal=0;
				$Jumlahtindakan=0;
				$duittotal=0;
				foreach ($result as $line)
				{
					$pasientotal+=$line->totalpasien;
					$Jumlahtindakan+=$line->jumlahtindakan;
					$duittotal+=$line->duittotal;

				   $html.='<tr > 
								<td></td>
								<td align="left">'.$i.'. '.$line->keterangan.'</td>
								<td align="right">'. $line->totalpasien.'</td>
								<td align="right">'.$line->jumlahtindakan.'</td>
								<td align="right">'.substr(number_format($line->duittotal,2,',',','),0,-3).'</td>
						   </tr> ';
					$i++;
				}

				$i--;
				$html.='<tr>
							<td></td>
							<td ><b>Sub Total '.$f->nama_unit.' </b></td>
							<td colspan="1" align="right">'.$pasientotal.'</td>
							<td colspan="1" align="right">'.$Jumlahtindakan.'</td>
							<td colspan="1" align="right">'.substr(number_format($duittotal,2,',',','),0,-3).'</td>
						</tr>';						  
			}
        }
		$query2 = "Select  Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From 
					(((Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk) 
						INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
								From  detail_transaksi Where kd_kasir ='$kd_kasir' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
								$Shift4x)  
								Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
						) INNER JOIN Unit u On u.kd_unit=t.kd_unit
					) 	INNER JOIN Produk p on p.kd_produk=x.kd_produk 
						LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
						INNER JOIN 
						(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
						) db On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
					Where 	
					left(k.kd_Unit,1) ='1'	
					--and t.ispay='true'	
					$kdUNIT2
					$jniscus				
					$customerx
					$c_tindakan ";
		$result2 = $this->db->query($query2)->result();
		$i=1;

		//pengecekan untuk menjadi parameter data kosong
		$total_pasien=0;
		foreach ($result2 as $line2)
		{
			$total_pasien = $total_pasien + $line2->totalpasien;
		}
		
		if($total_pasien > 0){
			foreach ($result2 as $line2)
			{
				
				 $html.='<tr class="headerrow"> 
							<td ></td>
							<td ><b>Grand Total</b></td>
							<td width="90" align="right">'.$line2->totalpasien.'</td>
							<td width="90" align="right">'.$line2->jumlahtindakan.'</td>
							<td width="90" align="right">'.substr(number_format($line2->duittotal,2,',',','),0,-3).'</td>
						   </tr>';	
			}	
		}else{
			$html.='<tr><td align="center" colspan="5"> Data Tidak Ada</td></tr>';
		}
		
		
		$html.='</tbody></table>';
		// echo $html;
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='LAPORAN_TRANSAKSI.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('P','LAPORAN TRANSAKSI',$html);
		
		}
	}
	
	function get_kelas_produk(){
		$result=$this->db->query("select Kd_klas,Klasifikasi from Klas_produk Where Kd_klas in ('31','32','61','68','69','65','66','62','67')
									UNION Select '9999' as Kd_klas, 'Semua' as Klasifikasi Order By Klasifikasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

	}
	function getUser()
    {
        
		$result=$this->db->query("Select DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
									From zUsers 
										inner join  zMember ON zmember.kd_user=zuserS.kd_user 
										inner join  zGroups on zmember.group_id=zgroups.group_id 
										inner join zTrustee on zgroups.group_id =ztrustee.group_id 
										inner join zModule On ztrustee.mod_id=zmodule.mod_id 
									UNION 
									Select '9999' as kd_user, 'Semua' as user_names 
									Order By user_names")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	function getPayment(){
		$result=$this->db->query("SELECT kd_pay, uraian FROM payment UNION SELECT '999', 'SEMUA ' order by uraian")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getUnit(){
		$result=$this->db->query("SELECT kd_unit,nama_unit, 'false' as check FROM UNIT WHERE Kd_Bagian = '1' AND Parent = '1'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getSpesialisasi(){
		$result=$this->db->query("SELECT kd_spesial, spesialisasi FROM spesialisasi 
				UNION 
				SELECT 999, 'SEMUA SPESIALISASI' ORDER BY spesialisasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getKelas(){
		$kd_spesial = $_POST['kd_spesial'];
		$kd_unit = $_POST['kd_unit'];
		$kd_units = substr($kd_unit, 0, -1);
		if($kd_spesial==''){
			$kd_spesial_s = "";
		}else{
			$kd_spesial_s = "su.kd_spesial='$kd_spesial' and ";
		}
		$result=$this->db->query("select * from (SELECT u.Kd_Unit, u.Nama_unit FROM Unit u INNER JOIN spc_Unit su ON su.kd_unit=u.kd_unit
									WHERE $kd_spesial_s  u.parent in($kd_units) UNION SELECT '999', 'SEMUA KELAS' ) x
									ORDER BY Kd_Unit")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getDokter(){
		$result=$this->db->query("SELECT kd_dokter,nama FROM Dokter 
									WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003'))
									union select '999' as kd_Dokter, 'Semua' as nama
									ORDER BY Nama ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getKamar(){
		$kd_irna = $_POST['kd_irna2'];
		$kd_irnas = substr($kd_irna, 0, -1);
		$kd_spesial = $_POST['kd_spesial'];
		$kd_kelas = $_POST['kd_kelas'];
		$kd_spesial_s='';
		if( $kd_spesial!='' ){
			$kd_spesial_s = " and kd_spesial='$kd_spesial'  ";
		}else{
			$kd_spesial_s='';
		}
		
		$kd_kelas_s='';
		if( $kd_kelas!='' ){
			$kd_kelas_s = " and kmr.kd_Unit in('$kd_kelas' ) ";
		}else{
			$kd_kelas_s='';
		}
		$result=$this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
									FROM Kamar kmr 
										INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit 
									WHERE 
										left (kmr.kd_Unit,4) in ($kd_irnas)
										$kd_spesial_s
										$kd_kelas_s										
									UNION 
									SELECT '999', 'SEMUA KAMAR' ORDER BY Nama_Kamar ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getKamarRekap(){
		$kd_irna = $_POST['kd_irna2'];
		$kd_irnas = substr($kd_irna, 0, -1);
		$result=$this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
									FROM Kamar kmr 
										INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit 
									WHERE 
										left (kmr.kd_Unit,4) in ($kd_irnas)								
									UNION 
									SELECT '999', 'SEMUA KAMAR' ORDER BY Nama_Kamar ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function cetak_lap_PelDokterPerTindakan_query(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit = $param->unit; 
		$kd_unit = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		// $kd_unit_s = str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false
		if($pasien_pulang =='true'){
			$k_tgl_pasien_plg="	AND g.CO_Status = 't' AND g.Tgl_CO BETWEEN '$tgl_awal' AND '$tgl_akhir'";
		}else{
			$k_tgl_pasien_plg="	AND g.CO_Status = 'f' AND d.Tgl_Transaksi  BETWEEN '$tgl_awal' AND '$tgl_akhir'";
		}
		
		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		#KRITERIA DOKTER
		$dokter = $param->dokter; //tanpa kutip
		$tmpKdDokter='';
		if($dokter == 'Semua' || $dokter == '999'){
			$get_kd_dokter= $this->db->query("SELECT kd_dokter,nama FROM Dokter WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003')) ")->result();
			$k_kd_dokter ='';
			$i=1;
			foreach($get_kd_dokter as $line){
				if($i==1){
					$k_kd_dokter="'".$line->kd_dokter."'";
				}else{
					$k_kd_dokter=$k_kd_dokter.","."'".$line->kd_dokter."'";
				}
				$i++;
			}
			$tmpKdDokter=$k_kd_dokter;
		}else{
			$tmpKdDokter = "'$dokter'";
		}
		
		#KRITERIA OPERATOR
		$operator = $param->operator; //tanpa kutip
		$tmpOperator ='';
		if($operator == 'Semua' || $operator == '9999'){
			$get_kd_user= $this->db->query( "Select DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
													From zUsers 
														inner join  zMember ON zmember.kd_user=zuserS.kd_user 
														inner join  zGroups on zmember.group_id=zgroups.group_id 
														inner join zTrustee on zgroups.group_id =ztrustee.group_id 
														inner join zModule On ztrustee.mod_id=zmodule.mod_id 
											")->result();
			$k_kd_user ='';
			$i=1;
			foreach($get_kd_user as $line){
				if($i==1){
					$k_kd_user="'".$line->kd_user."'";
				}else{
					$k_kd_user=$k_kd_user.","."'".$line->kd_user."'";
				}
				$i++;
			}
			$tmpOperator=$k_kd_user;
		}else{
			$tmpOperator="'$operator'";
		}
		#PEMANGGILAN FUNGSI LAPORAN
		$query = $this->db->query("select distinct dokter from(
													SELECT x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,  
														CASE WHEN x.Tag_Char = '20' THEN x.JP  ELSE 0 END as A1, CASE WHEN x.Tag_Char = '21' THEN x.JP  ELSE 0 END as A2, CASE WHEN x.Tag_Char = '27' THEN x.JP  ELSE 0 END as A3 , 
														CASE WHEN x.Tag_Char = '29' THEN x.JP  ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp  ELSE 0 END as A5, CASE WHEN x.Tag_Char = '31' THEN x.JP  ELSE 0 END as A6 ,  
														sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien,	'15'  as Pajak 
													From 
														(SELECT d.Flag, k.NAMA_KAMAR,u.NAMA_UNIT as Kelas,v.Kd_Dokter, o.Nama as Dokter, g.kd_pasien,p.Kd_Klas, p.Deskripsi, p.Kd_Produk, d.Qty, v.JP as JP , 
															CASE WHEN v.Tag_Char = '20' THEN d.Qty*v.JP ELSE 0 END as JP20, CASE WHEN v.Tag_Char = '21' THEN d.Qty*v.JP ELSE 0 END as JP21, CASE WHEN v.Tag_Char = '27' THEN d.Qty*v.JP ELSE 0 END as JP27, 
															CASE WHEN v.Tag_Char = '28' THEN d.Qty*v.JP ELSE 0 END as JP28 ,CASE WHEN v.Tag_Char = '29' THEN d.Qty*v.JP ELSE 0 END as  JP29, CASE WHEN v.Tag_Char = '31' THEN d.Qty*v.JP ELSE 0 END as JP31, v.Tag_Char 
														FROM (((((Transaksi g INNER JOIN (Detail_Transaksi d 
															LEFT JOIN (Detail_Tr_Kamar dk 
																	INNER JOIN Kamar k ON dk.No_Kamar=k.No_Kamar AND dk.kd_Unit=k.kd_Unit) 
																ON d.kd_kasir=dk.kd_kasir AND d.No_Transaksi=dk.No_Transaksi AND d.urut=dk.urut AND d.Tgl_Transaksi=dk.Tgl_Transaksi) ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi) 
															INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
															LEFT JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
															INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
															INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk  
															INNER JOIN Klas_Produk KP On p.kd_klas=kp.kd_klas ) 
															INNER JOIN Unit u ON k.Kd_Unit = u.Kd_Unit 
															INNER JOIN  Kunjungan kj On kj.Kd_Pasien=g.Kd_pasien And kj.Kd_Unit=g.Kd_Unit     And kj.Tgl_Masuk=g.Tgl_Transaksi And kj.Urut_Masuk=g.Urut_Masuk        
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=kj.Kd_Customer
															LEFT JOIN Pasien_inap i On i.kd_kasir =g.Kd_kasir and i.no_transaksi = g.no_transaksi
															LEFT JOIN Detail_Bayar db On g.KD_kasir=db.KD_kasir And g.No_Transaksi=db.No_Transaksi
														WHERE d.Flag < 2 
														And v.Tag_Char in  ('20','21','27','28','29','31')  
															AND u.Parent in ($kd_unit) 
															And dk.no_kamar in ($tmpKdKamar)
															and Ktr.Jenis_cust in($kd_kelpas)  
															And Ktr.kd_Customer in ($k_customer)
															and kp.kd_klas in ($k_kelas_produk)
															AND db.KD_PAY in ($tmpKdPay)
															and o.kd_dokter in($tmpKdDokter)
															and db.kd_user in($tmpOperator)
															$k_tgl_pasien_plg
														) x 
													group by  x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,x.Tag_Char,x.jp
													Union All
													select '' as NAMA_KAMAR,'' as kelas ,x.KD_DOKTER ,x.NAMA as dokter ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK,
															CASE WHEN x.Tag_Char = '20' THEN x.JP ELSE 0 END as A1 , CASE WHEN x.Tag_Char = '21' THEN x.JP    ELSE 0 END as A2 , CASE WHEN x.Tag_Char = '27' THEN x.JP    ELSE 0 END as A3, 
															CASE WHEN x.Tag_Char = '29' THEN x.JP    ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp    ELSE 0 END as A5 , CASE WHEN x.Tag_Char = '31' THEN x.JP    ELSE 0 END as A6, 
															sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien, '15'  as Pajak
													from 
														(
														select   x.Kelas,dok.KD_DOKTER ,dok.NAMA as nama ,pro.KD_KLAS , pro.DESKRIPSI,pro.KD_PRODUK , dt.qty , vd.JP as JP ,
															CASE WHEN vd.Tag_Char = '20' THEN dt.Qty*vd.JP ELSE 0 END as JP20, CASE WHEN vd.Tag_Char = '21' THEN dt.Qty*vd.JP ELSE 0 END as JP21, CASE WHEN vd.Tag_Char = '27' THEN dt.Qty*vd.JP ELSE 0 END as JP27, 
															CASE WHEN vd.Tag_Char = '28' THEN dt.Qty*vd.JP ELSE 0 END as JP28, CASE WHEN vd.Tag_Char = '29' THEN dt.Qty*vd.JP ELSE 0 END as JP29, CASE WHEN vd.Tag_Char = '31' THEN dt.Qty*vd.JP ELSE 0 END as JP31, 
															vd.Tag_Char,t.kd_pasien
														from DETAIL_TRANSAKSI dt
															inner join VISITE_DOKTER vd on vd.KD_KASIR = dt.KD_KASIR and vd.NO_TRANSAKSI = dt.NO_TRANSAKSI and vd.URUT = dt.URUT
															inner join TRANSAKSI t on t.KD_KASIR = dt.KD_KASIR and t.NO_TRANSAKSI = dt.NO_TRANSAKSI
															inner join DOKTER dok on dok.KD_DOKTER = vd.KD_DOKTER
															inner join PRODUK pro on pro.KD_PRODUK = dt.KD_PRODUK
															Inner Join
															(
																select  u.NAMA_UNIT as Kelas,dt.kd_unit_tr,dt.KD_UNIT,dt.NO_FAKTUR  
																from TRANSAKSI  t
																	inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR= t.KD_KASIR and dt.NO_TRANSAKSI = t.NO_TRANSAKSI
																	inner join UNIT u on t.KD_UNIT = u.KD_UNIT
																where t.KD_KASIR = '02' 
																	AND t.Tgl_CO BETWEEN '$tgl_awal' AND '$tgl_akhir'
																	AND u.Parent in ($kd_unit)
																	and dt.KD_UNIT in ($tmpKdKamar) 
															) x on x.KD_UNIT = t.KD_UNIT  and x.NO_FAKTUR= t.NO_TRANSAKSI
														WHERE dt.Flag < 2  
															And vd.Tag_Char in  ('20','21','27','28','29','31') and vd.KD_KASIR = '30'
															and dok.kd_Dokter in($tmpKdDokter)
															and dt.kd_user in($tmpOperator)
														) x
													group by x.KD_DOKTER ,x.NAMA  ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK , x.TAG_CHAR,x.JP
												) z
												ORDER BY Dokter

												")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
	if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="13">Laporan Jasa Pelayanan Dokter Per Tindakan</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="4">
					<thead>
					  <tr>
						<th align="center" width="40" >No</td>
						<th align="center" >Tindakan</th>
						<th align="center" width="70">Jumlah Pasien</th>
						<th align="center" width="70">Jumlah Produk</th>
						<th align="center" width="70">Dokter</th>
						<th align="center" width="70">Perawat</th>
						<th align="center" width="70">Operator</th>
						<th align="center" width="70">Anasthesi</th>
						<th align="center" width="70"> Ass. Operator</th>
						<th align="center" width="70"> Ass. Anasthesi</th>
						<th align="center" width="70">Jumlah</th>
						<th align="center" width="70">Pajak</th>
						<th align="center" width="70"> Diterima</th>
					  </tr>
					</thead><tbody>';
					

		if(count($query)>0){
			$no=1;
			foreach ($query as $line){
				#AGAR NAMA DOKTER TIDAK TAMPIL BERULANG
				// if($nama_dokter!= $line->dokter){
				// $html.='<tr>
							// <td align="center">'.$no.'</td>
							// <td>'.$line->dokter.'</td>
							// <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						// </tr>';
						// $no++;
				// }
				$nama_dokter=$line->dokter;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->dokter.'</td>
							<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						</tr>';
						$no++;
				$query_body = $this->db->query("select * from(
													SELECT x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,  
														CASE WHEN x.Tag_Char = '20' THEN x.JP  ELSE 0 END as A1, CASE WHEN x.Tag_Char = '21' THEN x.JP  ELSE 0 END as A2, CASE WHEN x.Tag_Char = '27' THEN x.JP  ELSE 0 END as A3 , 
														CASE WHEN x.Tag_Char = '29' THEN x.JP  ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp  ELSE 0 END as A5, CASE WHEN x.Tag_Char = '31' THEN x.JP  ELSE 0 END as A6 ,  
														sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien,	'15'  as Pajak 
													From 
														(SELECT d.Flag, k.NAMA_KAMAR,u.NAMA_UNIT as Kelas,v.Kd_Dokter, o.Nama as Dokter, g.kd_pasien,p.Kd_Klas, p.Deskripsi, p.Kd_Produk, d.Qty, v.JP as JP , 
															CASE WHEN v.Tag_Char = '20' THEN d.Qty*v.JP ELSE 0 END as JP20, CASE WHEN v.Tag_Char = '21' THEN d.Qty*v.JP ELSE 0 END as JP21, CASE WHEN v.Tag_Char = '27' THEN d.Qty*v.JP ELSE 0 END as JP27, 
															CASE WHEN v.Tag_Char = '28' THEN d.Qty*v.JP ELSE 0 END as JP28 ,CASE WHEN v.Tag_Char = '29' THEN d.Qty*v.JP ELSE 0 END as  JP29, CASE WHEN v.Tag_Char = '31' THEN d.Qty*v.JP ELSE 0 END as JP31, v.Tag_Char 
														FROM (((((Transaksi g INNER JOIN (Detail_Transaksi d 
															LEFT JOIN (Detail_Tr_Kamar dk 
																	INNER JOIN Kamar k ON dk.No_Kamar=k.No_Kamar AND dk.kd_Unit=k.kd_Unit) 
																ON d.kd_kasir=dk.kd_kasir AND d.No_Transaksi=dk.No_Transaksi AND d.urut=dk.urut AND d.Tgl_Transaksi=dk.Tgl_Transaksi) ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi) 
															INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
															LEFT JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
															INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
															INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk  
															INNER JOIN Klas_Produk KP On p.kd_klas=kp.kd_klas ) 
															INNER JOIN Unit u ON k.Kd_Unit = u.Kd_Unit 
															INNER JOIN  Kunjungan kj On kj.Kd_Pasien=g.Kd_pasien And kj.Kd_Unit=g.Kd_Unit     And kj.Tgl_Masuk=g.Tgl_Transaksi And kj.Urut_Masuk=g.Urut_Masuk        
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=kj.Kd_Customer
															LEFT JOIN Pasien_inap i On i.kd_kasir =g.Kd_kasir and i.no_transaksi = g.no_transaksi
															LEFT JOIN Detail_Bayar db On g.KD_kasir=db.KD_kasir And g.No_Transaksi=db.No_Transaksi
														WHERE d.Flag < 2 
														And v.Tag_Char in  ('20','21','27','28','29','31')  
															AND u.Parent in ($kd_unit) 
															AND d.Tgl_Transaksi BETWEEN '$tgl_awal' AND '$tgl_akhir'
															And dk.no_kamar in ($tmpKdKamar)
															and Ktr.Jenis_cust in($kd_kelpas)  
															And Ktr.kd_Customer in ($k_customer)
															and kp.kd_klas in ($k_kelas_produk)
															AND db.KD_PAY in ($tmpKdPay)
															and o.kd_dokter in($tmpKdDokter)
															and db.kd_user in($tmpOperator)
															$k_tgl_pasien_plg
														) x 
													group by  x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,x.Tag_Char,x.jp
													Union All
													select '' as NAMA_KAMAR,'' as kelas ,x.KD_DOKTER ,x.NAMA as dokter ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK,
															CASE WHEN x.Tag_Char = '20' THEN x.JP ELSE 0 END as A1 , CASE WHEN x.Tag_Char = '21' THEN x.JP    ELSE 0 END as A2 , CASE WHEN x.Tag_Char = '27' THEN x.JP    ELSE 0 END as A3, 
															CASE WHEN x.Tag_Char = '29' THEN x.JP    ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp    ELSE 0 END as A5 , CASE WHEN x.Tag_Char = '31' THEN x.JP    ELSE 0 END as A6, 
															sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien, '15'  as Pajak
													from 
														(
														select   x.Kelas,dok.KD_DOKTER ,dok.NAMA as nama ,pro.KD_KLAS , pro.DESKRIPSI,pro.KD_PRODUK , dt.qty , vd.JP as JP ,
															CASE WHEN vd.Tag_Char = '20' THEN dt.Qty*vd.JP ELSE 0 END as JP20, CASE WHEN vd.Tag_Char = '21' THEN dt.Qty*vd.JP ELSE 0 END as JP21, CASE WHEN vd.Tag_Char = '27' THEN dt.Qty*vd.JP ELSE 0 END as JP27, 
															CASE WHEN vd.Tag_Char = '28' THEN dt.Qty*vd.JP ELSE 0 END as JP28, CASE WHEN vd.Tag_Char = '29' THEN dt.Qty*vd.JP ELSE 0 END as JP29, CASE WHEN vd.Tag_Char = '31' THEN dt.Qty*vd.JP ELSE 0 END as JP31, 
															vd.Tag_Char,t.kd_pasien
														from DETAIL_TRANSAKSI dt
															inner join VISITE_DOKTER vd on vd.KD_KASIR = dt.KD_KASIR and vd.NO_TRANSAKSI = dt.NO_TRANSAKSI and vd.URUT = dt.URUT
															inner join TRANSAKSI t on t.KD_KASIR = dt.KD_KASIR and t.NO_TRANSAKSI = dt.NO_TRANSAKSI
															inner join DOKTER dok on dok.KD_DOKTER = vd.KD_DOKTER
															inner join PRODUK pro on pro.KD_PRODUK = dt.KD_PRODUK
															Inner Join
															(
																select  u.NAMA_UNIT as Kelas,dt.kd_unit_tr,dt.KD_UNIT,dt.NO_FAKTUR  
																from TRANSAKSI  t
																	inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR= t.KD_KASIR and dt.NO_TRANSAKSI = t.NO_TRANSAKSI
																	inner join UNIT u on t.KD_UNIT = u.KD_UNIT
																where t.KD_KASIR = '02' 
																	AND t.Tgl_CO BETWEEN '$tgl_awal' AND '$tgl_akhir'
																	AND u.Parent in ($kd_unit)
																	and dt.KD_UNIT in ($tmpKdKamar) 
																	
															) x on x.KD_UNIT = t.KD_UNIT  and x.NO_FAKTUR= t.NO_TRANSAKSI
														WHERE 	dt.Flag < 2  
																And vd.Tag_Char in  ('20','21','27','28','29','31') and vd.KD_KASIR = '30'
																and dok.kd_Dokter in($tmpKdDokter)
																and dt.kd_user in($tmpOperator)
														) x
													group by x.KD_DOKTER ,x.NAMA  ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK , x.TAG_CHAR,x.JP
												) z where dokter='$nama_dokter'
												ORDER BY Dokter, Kd_Dokter, Kd_Klas, Deskripsi, Kd_Produk 

												")->result();
				$tot_pasien=0;
				$tot_qty=0;
				$tot_a1=0;
				$tot_a2=0;
				$tot_a3=0;
				$tot_a4=0;
				$tot_a5=0;
				$tot_a6=0;
				$tot_jumlah=0;
				$tot_pajak=0;
				$tot_diterima=0;
				foreach($query_body as $line2){
					$jumlah=$line2->a1 + $line2->a2 + $line2->a3 + $line2->a4 +$line2->a5 + $line2->a6;
					$diterima = $jumlah - (($jumlah*$line2->pajak)/100);
					$html.='<tr>
							<td></td>
							<td>'.$line2->deskripsi.'</td>
							<td align="right">'.$line2->kd_pasien.'</td>
							<td align="right">'.$line2->qty.'</td>
							<td align="right">'.number_format($line2->a1,0,',',',').'</td>
							<td align="right">'.number_format($line2->a2,0,',',',').'</td>
							<td align="right">'.number_format($line2->a3,0,',',',').'</td>
							<td align="right">'.number_format($line2->a4,0,',',',').'</td>
							<td align="right">'.number_format($line2->a5,0,',',',').'</td>
							<td align="right">'.number_format($line2->a6,0,',',',').'</td>
							<td align="right">'.number_format($jumlah,0,',',',').'</td>
							<td align="right">'.$line2->pajak.'</td>
							<td align="right">'.number_format($diterima,0,',',',').'</td>
						</tr>';
					$tot_pasien=$tot_pasien + $line2->kd_pasien;
					$tot_qty=$tot_qty + $line2->qty;
					$tot_a1=$tot_a1+$line2->a1;
					$tot_a2=$tot_a2+$line2->a2;
					$tot_a3=$tot_a3+$line2->a3;
					$tot_a4=$tot_a4+$line2->a4;
					$tot_a5=$tot_a5+$line2->a5;
					$tot_a6=$tot_a6+$line2->a6;
					$tot_jumlah=$tot_jumlah + $jumlah;
					$tot_pajak=$tot_pajak + $line2->pajak;
					$tot_diterima=$tot_diterima + $diterima;
				}
				
				
				$html.='<tr>
							<td></td>
							<td ><b>Subtotal</b></td>
							<td align="right"><b>'.number_format($tot_pasien,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_qty,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a1,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a2,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a3,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a4,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a5,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a6,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_jumlah,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_pajak,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_diterima,0,',',',').'</b></td>
				</tr>';
				//$nama_dokter = $line->dokter;
			}
		}else{
			$html.='<tr><td align="center" colspan="13">Data Tidak Ada</td></tr>';
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan',$html);
		
		}
	}
	
	function cetak_lap_PelDokterPerTindakan_fungsi(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit = $param->unit; 
		$kd_unit = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		$kd_unit= str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		$k_customer= str_replace("'","",$k_customer ); //menghilangkan kutip
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		$k_kelas_produk= str_replace("'","",$k_kelas_produk); //menghilangkan kutip
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false
		if ($pasien_pulang=='true'){
			$status_pulang=1;
		}else{
			$status_pulang=0;
		}
		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		$tmpKdPay= str_replace("'","",$tmpKdPay); //menghilangkan kutip
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		
		$tmpKdKamar= str_replace("'","",$tmpKdKamar); //menghilangkan kutip
		
		#KRITERIA DOKTER
		$dokter = $param->dokter; //tanpa kutip
		$tmpKdDokter='';
		if($dokter == 'Semua' || $dokter == '999'){
			$get_kd_dokter= $this->db->query("SELECT kd_dokter,nama FROM Dokter WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003')) ")->result();
			$k_kd_dokter ='';
			$i=1;
			foreach($get_kd_dokter as $line){
				if($i==1){
					$k_kd_dokter="'".$line->kd_dokter."'";
				}else{
					$k_kd_dokter=$k_kd_dokter.","."'".$line->kd_dokter."'";
				}
				$i++;
			}
			$tmpKdDokter=$k_kd_dokter;
		}else{
			$tmpKdDokter = "'$dokter'";
		}
		$tmpKdDokter= str_replace("'","",$tmpKdDokter); //menghilangkan kutip
		
		#KRITERIA OPERATOR
		$operator = $param->operator; //tanpa kutip
		$tmpOperator ='';
		if($operator == 'Semua' || $operator == '9999'){
			$get_kd_user= $this->db->query( "Select DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
													From zUsers 
														inner join  zMember ON zmember.kd_user=zuserS.kd_user 
														inner join  zGroups on zmember.group_id=zgroups.group_id 
														inner join zTrustee on zgroups.group_id =ztrustee.group_id 
														inner join zModule On ztrustee.mod_id=zmodule.mod_id 
											")->result();
			$k_kd_user ='';
			$i=1;
			foreach($get_kd_user as $line){
				if($i==1){
					$k_kd_user="'".$line->kd_user."'";
				}else{
					$k_kd_user=$k_kd_user.","."'".$line->kd_user."'";
				}
				$i++;
			}
			$tmpOperator=$k_kd_user;
		}else{
			$tmpOperator="'$operator'";
		}
		$tmpOperator= str_replace("'","",$tmpOperator); //menghilangkan kutip
		
		
		#PEMANGGILAN FUNGSI LAPORAN
		
		$kriteria="select * from rwi_lap_jasa_pel_dokter_pertindakan 
									(
										'{".$kd_unit."}',
										'{".$tmpKdKamar."}',
										'{".$kd_kelpas."}',
										'{".$k_customer."}',
										'{".$k_kelas_produk."}',
										'{".$tmpKdPay."}',
										'{".$tmpOperator."}',
										'{".$tmpKdDokter."}',
										'{".$tgl_awal."}',
										'{".$tgl_akhir."}',
										{$status_pulang}
									)";
		$query = $this->db->query($kriteria)->result();
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		
		$t_nama_dokter='';
		$arr_nama_dokter=array();
		$i_t=0; //indeks array nama dokter
		foreach ($query as $line3){
			if($t_nama_dokter != $line3->dokter){
				$arr_nama_dokter[$i_t]=$line3->dokter;
				$i_t++;
			}
			$t_nama_dokter=$line3->dokter;
		}
		
		// print_r($arr_nama_dokter);
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="13">Laporan Jasa Pelayanan Dokter Per Tindakan</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="4">
					<thead>
					  <tr>
						<th align="center" width="40" >No</td>
						<th align="center" >Tindakan</th>
						<th align="center" width="70">Jumlah Pasien</th>
						<th align="center" width="70">Jumlah Produk</th>
						<th align="center" width="70">Dokter</th>
						<th align="center" width="70">Perawat</th>
						<th align="center" width="70">Operator</th>
						<th align="center" width="70">Anasthesi</th>
						<th align="center" width="70"> Ass. Operator</th>
						<th align="center" width="70"> Ass. Anasthesi</th>
						<th align="center" width="70">Jumlah</th>
						<th align="center" width="70">Pajak</th>
						<th align="center" width="70"> Diterima</th>
					  </tr>
					</thead><tbody>'; 
					

		if(count($arr_nama_dokter)>0){
			$no=1;
			foreach($arr_nama_dokter as $nama_dokter){
				$html.='<tr>
						<td align="center">'.$no.'</td>
						<td>'.$nama_dokter.'</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>';
				$tot_pasien=0;
				$tot_qty=0;
				$tot_a1=0;
				$tot_a2=0;
				$tot_a3=0;
				$tot_a4=0;
				$tot_a5=0;
				$tot_a6=0;
				$tot_jumlah=0;
				$tot_pajak=0;
				$tot_diterima=0;
				foreach($query as $line2){
					if($nama_dokter == $line2->dokter){
						$jumlah=$line2->a1 + $line2->a2 + $line2->a3 + $line2->a4 +$line2->a5 + $line2->a6;
						$diterima = $jumlah - (($jumlah*$line2->pajak)/100);
					
						$html.='<tr>
							<td></td>
							<td>'.$line2->deskripsi.'</td>
							<td align="right">'.$line2->kd_pasien.'</td>
							<td align="right">'.$line2->qty.'</td>
							<td align="right">'.number_format($line2->a1,0,',',',').'</td>
							<td align="right">'.number_format($line2->a2,0,',',',').'</td>
							<td align="right">'.number_format($line2->a3,0,',',',').'</td>
							<td align="right">'.number_format($line2->a4,0,',',',').'</td>
							<td align="right">'.number_format($line2->a5,0,',',',').'</td>
							<td align="right">'.number_format($line2->a6,0,',',',').'</td>
							<td align="right">'.number_format($jumlah,0,',',',').'</td>
							<td align="right">'.$line2->pajak.'</td>
							<td align="right">'.number_format($diterima,0,',',',').'</td>
						</tr>';
						$tot_pasien=$tot_pasien + $line2->kd_pasien;
						$tot_qty=$tot_qty + $line2->qty;
						$tot_a1=$tot_a1+$line2->a1;
						$tot_a2=$tot_a2+$line2->a2;
						$tot_a3=$tot_a3+$line2->a3;
						$tot_a4=$tot_a4+$line2->a4;
						$tot_a5=$tot_a5+$line2->a5;
						$tot_a6=$tot_a6+$line2->a6;
						$tot_jumlah=$tot_jumlah + $jumlah;
						$tot_pajak=$tot_pajak + $line2->pajak;
						$tot_diterima=$tot_diterima + $diterima;
					}
					
				}
				$html.='<tr>
							<td></td>
							<td ><b>Subtotal</b></td>
							<td align="right"><b>'.number_format($tot_pasien,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_qty,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a1,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a2,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a3,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a4,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a5,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a6,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_jumlah,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_pajak,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_diterima,0,',',',').'</b></td>
				</tr>';
				$no++;
			}
			
		}else{
			$html.='<tr><td align="center" colspan="13">Data Tidak Ada</td></tr>';
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan',$html);
		
		} 
		
	/* 	if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="13">Laporan Jasa Pelayanan Dokter Per Tindakan</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="4">
					<thead>
					  <tr>
						<th align="center" width="40" >No</td>
						<th align="center" >Tindakan</th>
						<th align="center" width="70">Jumlah Pasien</th>
						<th align="center" width="70">Jumlah Produk</th>
						<th align="center" width="70">Dokter</th>
						<th align="center" width="70">Perawat</th>
						<th align="center" width="70">Operator</th>
						<th align="center" width="70">Anasthesi</th>
						<th align="center" width="70"> Ass. Operator</th>
						<th align="center" width="70"> Ass. Anasthesi</th>
						<th align="center" width="70">Jumlah</th>
						<th align="center" width="70">Pajak</th>
						<th align="center" width="70"> Diterima</th>
					  </tr>
					</thead><tbody>'; */
					

		/* if(count($query)>0){
			$no=1;
			$nama_dokter='';
			foreach ($query as $line){
				#AGAR NAMA DOKTER TIDAK TAMPIL BERULANG
				
				if($nama_dokter!= $line->dokter){
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->dokter.'</td>
							<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						</tr>';
						$no++;
				}
				
				$tot_pasien=0;
				$tot_qty=0;
				$tot_a1=0;
				$tot_a2=0;
				$tot_a3=0;
				$tot_a4=0;
				$tot_a5=0;
				$tot_a6=0;
				$tot_jumlah=0;
				$tot_pajak=0;
				$tot_diterima=0;
				foreach ($query as $line2){
					$tmp_dokter=$line->dokter;
					
					if($tmp_dokter == $line2->dokter){
						$jumlah=$line2->a1 + $line2->a2 + $line2->a3 + $line2->a4 +$line2->a5 + $line2->a6;
						$diterima = $jumlah - (($jumlah*$line2->pajak)/100);
					
						$html.='<tr>
							<td></td>
							<td>'.$line2->deskripsi.'</td>
							<td align="right">'.$line2->kd_pasien.'</td>
							<td align="right">'.$line2->qty.'</td>
							<td align="right">'.number_format($line2->a1,0,',',',').'</td>
							<td align="right">'.number_format($line2->a2,0,',',',').'</td>
							<td align="right">'.number_format($line2->a3,0,',',',').'</td>
							<td align="right">'.number_format($line2->a4,0,',',',').'</td>
							<td align="right">'.number_format($line2->a5,0,',',',').'</td>
							<td align="right">'.number_format($line2->a6,0,',',',').'</td>
							<td align="right">'.number_format($jumlah,0,',',',').'</td>
							<td align="right">'.$line2->pajak.'</td>
							<td align="right">'.number_format($diterima,0,',',',').'</td>
						</tr>';
						$tot_pasien=$tot_pasien + $line2->kd_pasien;
						$tot_qty=$tot_qty + $line2->qty;
						$tot_a1=$tot_a1+$line2->a1;
						$tot_a2=$tot_a2+$line2->a2;
						$tot_a3=$tot_a3+$line2->a3;
						$tot_a4=$tot_a4+$line2->a4;
						$tot_a5=$tot_a5+$line2->a5;
						$tot_a6=$tot_a6+$line2->a6;
						$tot_jumlah=$tot_jumlah + $jumlah;
						$tot_pajak=$tot_pajak + $line2->pajak;
						$tot_diterima=$tot_diterima + $diterima;
					}
				}
				
				$html.='<tr>
							<td></td>
							<td ><b>Subtotal</b></td>
							<td align="right"><b>'.number_format($tot_pasien,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_qty,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a1,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a2,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a3,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a4,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a5,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a6,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_jumlah,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_pajak,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_diterima,0,',',',').'</b></td>
				</tr>';
				
				$nama_dokter = $line->dokter;
			} */
			
		/* }else{
			$html.='<tr><td align="center" colspan="13">Data Tidak Ada</td></tr>';
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan',$html);
		
		} */
	
	}
	function cetak_lap_rekap_arus_pend(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit = $param->unit; 
		$kd_unit = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		$kd_unit= str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		$k_customer= str_replace("'","",$k_customer ); //menghilangkan kutip
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		$k_kelas_produk= str_replace("'","",$k_kelas_produk); //menghilangkan kutip
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false
		if ($pasien_pulang=='true'){
			$status_pulang=1;
		}else{
			$status_pulang=0;
		}
		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		$tmpKdPay= str_replace("'","",$tmpKdPay); //menghilangkan kutip
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		
		$tmpKdKamar= str_replace("'","",$tmpKdKamar); //menghilangkan kutip
		
		
		#KRITERIA SPESIALISASI
		$arrayDataSpesialis = $param->kd_spesialis;
		
		$tmpKdSpesialis='';
		for ($i=0; $i < count($arrayDataSpesialis); $i++) { 
			$tmpKdSpesialis .= "'".$arrayDataSpesialis[$i][0]."',";
		}
		$tmpKdSpesialis = substr($tmpKdSpesialis, 0, -1); //arr kd pay
		
		$semua_spesialis="'"."999"."'";
		if($tmpKdSpesialis==$semua_spesialis ){ //semua kd_pay
			$get_kd_spesialis= $this->db->query("SELECT kd_spesial, spesialisasi FROM spesialisasi")->result();
			$k_kd_spesialis ='';
			$i=1;
			foreach($get_kd_spesialis as $line){
				if($i==1){
					$k_kd_spesialis="'".$line->kd_spesial."'";
				}else{
					$k_kd_spesialis=$k_kd_spesialis.","."'".$line->kd_spesial."'";
				}
				$i++;
			}
			$tmpKdSpesialis=$k_kd_spesialis;
		}
		$tmpKdSpesialis= str_replace("'","",$tmpKdSpesialis);
		
		$arr_explode_spesialis=explode(",",$tmpKdSpesialis);
		
		$i=1;
		$nama_spesialis ='';
		foreach ($arr_explode_spesialis as $line){
			$nm_spesialis= $this->db->query("SELECT * FROM spesialisasi where kd_spesial=$line")->row()->spesialisasi;
			if($i==1){
				$nama_spesialis=$nm_spesialis;
			}else{
				$nama_spesialis=$nama_spesialis.", ".$nm_spesialis;
			}
			$i++;
		}
		
		
		$kriteria="select * from rwi_lap_rekap_arus_pend_pasien 
									(
										'{".$tgl_awal."}',
										'{".$tgl_akhir."}',
										'{".$kd_unit."}',
										'{".$k_kelas_produk."}',
										'{".$kd_kelpas."}',
										'{".$k_customer."}',
										'{".$tmpKdSpesialis."}',
										'{".$tmpKdKamar."}',
										'{".$tmpKdPay."}',
										{$status_pulang}
									)";
		$query = $this->db->query($kriteria)->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="13">REKAPITULASI ARUS PENDAPATAN RAWAT INAP PER PASIEN</th>
						</tr>
						<tr>
							<th style='.$font_style.' colspan="13">PEMBANTU KASIR PENERIMA UANG</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Spesialisasi : '.$nama_spesialis.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="4">
					<thead>
					  <tr>
						<th align="center" rowspan="2" width="40" >No</td>
						<th align="center" rowspan="2">No Medrec</th>
						<th align="center" rowspan="2">Nama Pasien</th>
						<th align="center" rowspan="2">Kamar</th>
						<th align="center" rowspan="2">Makan</th>
						<th align="center" rowspan="2">Visite</th>
						<th align="center" rowspan="2">Operatif</th>
						<th align="center" rowspan="2">Non Op</th>
						<th align="center" rowspan="2">Rad</th>
						<th align="center" rowspan="2">Lab PK</th>
						<th align="center" rowspan="2">Lab PA</th>
						<th align="center" rowspan="2">HD</th>
						<th align="center" rowspan="2">IRM</th>
						<th align="center" rowspan="2">IRD</th>
						<th align="center" rowspan="2">RWJ</th>
						<th align="center" rowspan="2">ADM</th>
						<th align="center" rowspan="2">BHP</th>
						<th align="center" rowspan="2">Farmasi</th>
						<th align="center" rowspan="2">O2</th>
						<th align="center" rowspan="2">Gizi</th>
						<th align="center" rowspan="2">ECG</th>
						<th align="center" rowspan="2" >Transfusi</th>
						<th align="center" rowspan="2">EEG</th>
						<th align="center" rowspan="2">Total POP</th>
						<th align="center" colspan="2">Pembayaran</th>
						<th align="center" colspan="2">Piutang</th>
						<th align="center" rowspan="2">Keterangan (+/-)/Penanggung</th>
					  </tr>
					  <tr>
						<th align="center" >Jml Kwitansi</th>
						<th align="center" >Jml Bayar</th>
						<th align="center" >Jml Pasien</th>
						<th align="center" >Tot Tagihan</th>
					  </tr>
					</thead><tbody>';
		$no=1;
		$tot_kamar=0;
		$tot_konsumsi=0;
		$tot_visite_konsul=0;
		$tot_operatif=0;
		$tot_nonoperatif=0;
		$tot_radiologi=0;
		$tot_laboratoriumpk=0;
		$tot_laboratoriumpa=0;
		$tot_hd=0;
		$tot_irm=0;
		$tot_ird=0;
		$tot_rwj=0;
		$tot_adm=0;
		$tot_bhp=0;
		$tot_obat=0;
		$tot_o2=0;
		$tot_gizi=0;
		$tot_ecg=0;
		$tot_transfusi=0;
		$tot_eeg=0;
		$tot_total_pop=0;
		$tot_jml_kw=0;
		$tot_jml_byr=0;
		$tot_jml_pasien=0;
		$tot_jml_tagihan=0;
		
		foreach ($query as $line){
			$jml_kw=1;
			$jml_pasien=1;
			$total_pop = $line->kamar+$line->konsumsi + $line->visite_konsul + $line->operatif + $line->nonoperatif 
						 + $line->radiologi + $line->laboratoriumpk + $line->laboratoriumpa+  $line->hd 
						+ $line->irm + $line->ird + $line->rwj + $line->adm + $line->bhp + $line->obat+ $line->o2 + $line->gizi+ $line->ecg +$line->transfusi +$line->eeg;
			$tagihan = $total_pop - $line->bayar;	
		
			$html.='<tr>
						<td>'.$no.'</td>
						<td>'.$line->kd_pasien.'</td>
						<td>'.$line->nama_pasien.'</td>
						<td align="right">'.number_format($line->kamar,0,',',',').'</td>
						<td align="right">'.number_format($line->konsumsi,0,',',',').'</td>
						<td align="right">'.number_format($line->visite_konsul,0,',',',').'</td>
						<td align="right">'.number_format($line->operatif,0,',',',').'</td>
						<td align="right">'.number_format($line->nonoperatif,0,',',',').'</td>
						<td align="right">'.number_format($line->radiologi,0,',',',').'</td>
						<td align="right">'.number_format($line->laboratoriumpk,0,',',',').'</td>
						<td align="right">'.number_format($line->laboratoriumpa,0,',',',').'</td>
						<td align="right">'.number_format($line->hd,0,',',',').'</td>
						<td align="right">'.number_format($line->irm,0,',',',').'</td>
						<td align="right">'.number_format($line->ird,0,',',',').'</td>
						<td align="right">'.number_format($line->rwj,0,',',',').'</td>
						<td align="right">'.number_format($line->adm,0,',',',').'</td>
						<td align="right">'.number_format($line->bhp,0,',',',').'</td>
						<td align="right">'.number_format($line->obat,0,',',',').'</td>
						<td align="right">'.number_format($line->o2,0,',',',').'</td>
						<td align="right">'.number_format($line->gizi,0,',',',').'</td>
						<td align="right">'.number_format($line->ecg,0,',',',').'</td>
						<td align="right">'.number_format($line->transfusi,0,',',',').'</td>
						<td align="right">'.number_format($line->eeg,0,',',',').'</td>
						<td align="right">'.number_format($total_pop,0,',',',').'</td>
						<td align="right">'.number_format($jml_kw,0,',',',').'</td>
						<td align="right">'.number_format($line->bayar,0,',',',').'</td>
						<td align="right">'.number_format($jml_pasien,0,',',',').'</td>
						<td align="right">'.number_format($tagihan,0,',',',').'</td>
						<td></td>
					</tr>';
			$no++; 
			$tot_kamar=$tot_kamar + $line->kamar;
			$tot_konsumsi=$tot_konsumsi + $line->konsumsi;
			$tot_visite_konsul=$tot_visite_konsul + $line->visite_konsul;
			$tot_operatif=$tot_operatif + $line->operatif;
			$tot_nonoperatif=$tot_nonoperatif + $line->nonoperatif;
			$tot_radiologi=$tot_radiologi + $line->radiologi;
			$tot_laboratoriumpk=$tot_laboratoriumpk + $line->laboratoriumpk;
			$tot_laboratoriumpa=$tot_laboratoriumpa + $line->laboratoriumpa;
			$tot_hd=$tot_hd + $line->hd;
			$tot_irm=$tot_irm + $line->irm;
			$tot_ird=$tot_ird + $line->ird;
			$tot_rwj=$tot_rwj + $line->rwj;
			$tot_adm=$tot_adm + $line->adm;
			$tot_bhp=$tot_bhp + $line->bhp;
			$tot_obat=$tot_obat + $line->obat;
			$tot_o2=$tot_o2 + $line->o2;
			$tot_gizi=$tot_gizi + $line->gizi;
			$tot_ecg=$tot_ecg + $line->ecg;
			$tot_transfusi=$tot_transfusi + $line->transfusi;
			$tot_eeg=$tot_eeg + $line->eeg;
			$tot_total_pop=$tot_total_pop + $total_pop;
			$tot_jml_kw=$tot_jml_kw + $jml_kw;
			$tot_jml_byr=$tot_jml_byr + $line->bayar;
			$tot_jml_pasien=$tot_jml_pasien + $jml_pasien;
			$tot_jml_tagihan=$tot_jml_tagihan + $tagihan;
		} 
		$html.='<tr>
					<td></td>
					<td></td>
					<td></td>
					<td align="right">'.number_format($tot_kamar,0,',',',').'</td>
					<td align="right">'.number_format($tot_konsumsi,0,',',',').'</td>
					<td align="right">'.number_format($tot_visite_konsul,0,',',',').'</td>
					<td align="right">'.number_format($tot_operatif,0,',',',').'</td>
					<td align="right">'.number_format($tot_nonoperatif,0,',',',').'</td>
					<td align="right">'.number_format($tot_radiologi,0,',',',').'</td>
					<td align="right">'.number_format($tot_laboratoriumpk,0,',',',').'</td>
					<td align="right">'.number_format($tot_laboratoriumpa,0,',',',').'</td>
					<td align="right">'.number_format($tot_hd,0,',',',').'</td>
					<td align="right">'.number_format($tot_irm,0,',',',').'</td>
					<td align="right">'.number_format($tot_ird,0,',',',').'</td>
					<td align="right">'.number_format($tot_rwj,0,',',',').'</td>
					<td align="right">'.number_format($tot_adm,0,',',',').'</td>
					<td align="right">'.number_format($tot_bhp,0,',',',').'</td>
					<td align="right">'.number_format($tot_obat,0,',',',').'</td>
					<td align="right">'.number_format($tot_o2,0,',',',').'</td>
					<td align="right">'.number_format($tot_gizi,0,',',',').'</td>
					<td align="right">'.number_format($tot_ecg,0,',',',').'</td>
					<td align="right">'.number_format($tot_transfusi,0,',',',').'</td>
					<td align="right">'.number_format($tot_eeg,0,',',',').'</td>
					<td align="right">'.number_format($tot_total_pop,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_kw,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_byr,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_pasien,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_tagihan,0,',',',').'</td>
					<td></td>
				</tr>';
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Rekapitulasi_Arus_Pendapatan_RWI.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan_Rekapitulasi_Arus_Pendapatan_RWI',$html);
		
		} 
	
	}
}
?>