<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_lap_penerimaan_per_pasien extends  MX_Controller {		
	
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
		
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

	}

	function laporan(){
		$common 	= $this->common;
   		$result 	= $this->result;
		
		$param 		= json_decode($_POST['data']);
		$params 	= array(
			'kd_pay' 		=> $param->kd_pay,
			'pasien_pulang' => $param->pasien_pulang,
			'shift_1' 		=> $param->shift_1,
			'shift_2' 		=> $param->shift_2,
			'shift_3' 		=> $param->shift_3,
			'shift_all' 	=> $param->shift_all,
			
			'shift_1_2' 	=> $param->shift_1_2,
			'shift_2_2' 	=> $param->shift_2_2,
			'shift_3_2' 	=> $param->shift_3_2,
			'shift_all_2' 	=> $param->shift_all_2,
			
			'tgl_akhir' 	=> $param->tgl_akhir,
			'tgl_awal' 		=> $param->tgl_awal,
			'type_file' 	=> $param->type_file,
			'unit' 			=> $param->unit,
			'kel_pas' 		=> $param->kel_pas,
			'kel_pas2' 		=> $param->kel_pas2,
		);

		
		$criteriaShift_4 = "";
		$criteriaShift 	 = "";
		$q_customer		 = "";
		
		$q_waktu='';
		$q_shift='';
   		$q_shift2='';
   		$q_shift3='';
   		$t_shift='';
   		$t_shift2='';
   		$t_shift3='';
		
		$dt1 		= date_create( date('Y-m-d', strtotime($param->tgl_awal)));
		$dt2 		= date_create( date('Y-m-d', strtotime($param->tgl_akhir)));
		$date_diff 	= date_diff($dt1,$dt2);
		$range 		=  $date_diff->format("%a");
		
		if( $param->shift_all == '' && $param->shift_1=='' && $param->shift_2=='' && $param->shift_3=='' &&
			$param->shift_all_2 == '' && $param->shift_1_2=='' && $param->shift_2_2=='' && $param->shift_3_2=='' )
		{
			$q_waktu=" ( dtb.tgl_bayar between '".$param->tgl_awal."' and  '".$param->tgl_akhir."' )";
		}else{
			#GRUP COMBO 1
			if($param->shift_all=='true'){
				$q_shift=	" 
								(
									(
										dtb.tgl_bayar = '".$param->tgl_awal."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar = '".date('Y-m-d', strtotime($param->tgl_awal . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift='SHIFT (1,2,3)';
			}else{
				if($param->shift_1=='true' || $param->shift_2=='true' || $param->shift_3=='true'){
					$s_shift='';
					if($param->shift_1=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift_2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift_3=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift.=" dtb.tgl_bayar = '".$param->tgl_awal."'   And db.Shift In (".$s_shift.")";
					if($param->shift_3=='true'){
						$q_shift="(".$q_shift." Or  (dtb.tgl_bayar= '".date('Y-m-d', strtotime($param->tgl_awal . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift ='SHIFT ('.$s_shift.')';
					$q_shift =$q_shift;
				}
			}
			
			#GRUP COMBO 2
			if($param->shift_all_2=='true'){
				$q_shift2=	" 
								(
									(
										dtb.tgl_bayar = '".$param->tgl_akhir."' And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar = '".date('Y-m-d', strtotime($param->tgl_akhir . ' +1 day'))."'  And db.Shift=4) 
								) 

							";
				$t_shift2='SHIFT (1,2,3)';
			}else{
				if($param->shift_1_2=='true' || $param->shift_2_2=='true' || $param->shift_3_2=='true'){
					$s_shift='';
					if($param->shift_1_2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='1';
					}
					if($param->shift_2_2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='2';
					}
					if($param->shift_3_2=='true'){
						if($s_shift!='')$s_shift.=',';
						$s_shift.='3';
					}
					$q_shift2.=" dtb.tgl_bayar = '".$param->tgl_akhir."'   And db.Shift In (".$s_shift.")";
					if($param->shift_3_2=='true'){
						$q_shift2="(".$q_shift2." Or  (dtb.tgl_bayar= '".date('Y-m-d', strtotime($param->tgl_akhir . ' +1 day'))."'  And db.Shift=4) )	";
					}
					$t_shift2 ='SHIFT ('.$s_shift.')';
					$q_shift2 =$q_shift2;
				}
			}
			
			
			# 3. RANGE PERIODE TGL BERBEDA > 1 HARI
			if($range > 1){
				$q_shift3=	" OR (
								(
									(
										dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->tgl_awal . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->tgl_akhir . ' -1 day'))."'  And db.Shift In (1,2,3)
									)     
									Or  
									(
										dtb.tgl_bayar between '".date('Y-m-d', strtotime($param->tgl_awal . ' +2 day'))."'  And  '".$param->tgl_akhir."'  And db.Shift=4) 
									) 
								)
							";		
			}
			
			$q_waktu=" ((".$q_shift.") OR ((".$q_shift2.")) ".$q_shift3." )  ";
		}
		
		
		if (isset($params['unit']) && $params['unit']!="") {
			$params['unit'] = " u.Parent IN (".substr($params['unit'], 0, strlen($params['unit'])-1).")";
		}else{
			$params['unit'] = "";
		}

		if (isset($params['kd_pay']) && $params['kd_pay']!="") {
			$q_payment = " And (dtb.Kd_Pay in (".$params['kd_pay'].")) ";
		}else{
			unset($params['kd_pay']);
			$params['kd_pay'] = "";
		}
		
		if (isset($params['kel_pas2']) && $params['kel_pas2']!="") {
			if($params['kel_pas2'] == 1 || $params['kel_pas2'] == '1' ){
				$params['kel_pas2'] = '0000000001';
			}
			
			if($params['kel_pas2']!='Semua') {
				$customer=$this->db->query("SELECT customer from customer where kd_customer='".$params['kel_pas2']."'")->row()->customer;
				$params['kel_pas2']=" AND Ktr.kd_customer='".$params['kel_pas2']."' ";
			} else {
				$q_customer="";
				$customer='SEMUA';
			}
		}else{
			unset($params['kel_pas2']);
			$params['kel_pas2'] = "";
			$customer='SEMUA';
		}
		$KdKasir=$this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		
		$result   = $this->db->query("
										SELECT DISTINCT( un.kd_unit ), un.nama_unit 
										From Transaksi t
											INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
											INNER JOIN 
												(
												SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, db.kd_unit
												FROM Detail_Bayar db 
													INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
														and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
												WHERE   
													".$q_waktu."
													".$q_payment."
													GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, db.kd_unit
												) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
											INNER JOIN Unit U ON X.Kd_Unit = U.Kd_Unit
											INNER JOIN unit Un  ON un.kd_unit = U.parent 
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
										WHERE 
											".$params['unit']."
											AND t.IsPay = 't'
											AND t.Kd_Kasir = '".$KdKasir."'
											AND t.CO_Status = '".$params['pasien_pulang']."' 
											".$params['kel_pas2']."
										GROUP BY un.kd_unit, Un.nama_unit 
										ORDER BY un.Nama_Unit
									")->result();
		$html='';
		$html.='
			<table class="t2"  cellspacing="0" border="0" >
				
					<tr>
						<th colspan="8" align="center">LAPORAN PENERIMAAN (Per Pasien)</th>
					</tr>
					<tr>
						<th colspan="8" align="center">'.$param->tgl_awal.' '.$t_shift.' s/d '.$param->tgl_akhir.' '.$t_shift2.'</th>
					</tr>
					<tr>
						<th colspan="8" align="center">KELOMPOK PASIEN ('.$customer.')</th>
					</tr>
			</table> <br>';
		$html.="<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					<tr>
						<th width='40' align='center'>NO.</th>
						<th width='80'>Unit</th>
						<th width='80'>NO. TRANSAKSI</th>
						<th width='80'>NO. MEDREC</th>
						<th width='250'>NAMA PASIEN</th>
						<th width='100'>JENIS PENERIMAAN</th>
						<th width='100'>NO KWITANSI</th>
						<th width='100' align='center'>JUMLAH</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='8'>Tidak Ada Data</td>
					</tr>";
		} else{
			$jumlahtotal=0;
			$no=0;
			foreach($result as $line){
				$noo=0;
				$no++;
				$html.="<tr>
						<td align='center'>".$no."</td>
						<td align='left' colspan='7'>".$line->nama_unit."</td>
					</tr>";
				$resultbody   = $this->db->query("
													SELECT 
													Un.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama)as Nama, py.Uraian, Sum(x.Jumlah) AS JUMLAH,
														getallnokwitansi('".$KdKasir."', x.No_Transaksi) AS no_kwitansi
													From Transaksi t
														INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
														INNER JOIN 
															(
															SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, db.kd_unit 
															FROM Detail_Bayar db 
																INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
																	and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
															WHERE  
																".$q_waktu."
																".$q_payment."
																GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, db.kd_unit
															) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
														INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
														INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
															And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
														INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
														LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
														INNER JOIN Unit U ON X.Kd_Unit = U.Kd_Unit
														 INNER JOIN unit Un  ON un.kd_unit = U.parent 
														INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
													WHERE 
														un.kd_unit='".$line->kd_unit."' 
														AND t.IsPay = 't'
														AND t.Kd_Kasir = '".$KdKasir."'
														".$q_customer."
													GROUP BY --x.Tgl_Transaksi, 
													Un.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY no_kwitansi, Un.Nama_Unit, Ps.Nama, x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)
										")->result();	
				$jumlah=0;
				foreach($resultbody as $linebody){
					$noo++;
					$html.="<tr>
								<td align='center'></td>
								<td align='center'></td>
								<td align='center'>".$noo.".&nbsp;&nbsp;&nbsp;".$linebody->no_transaksi."</td>
								<td align='center'>".$linebody->kd_pasien."</td>
								<td>".$linebody->nama."</td>
								<td align='center'>".$linebody->uraian."</td>
								<td>".$linebody->no_kwitansi."</td>
								<td align='right'>".number_format($linebody->jumlah,0,'.',',')."</td>
							</tr>";
					$jumlah += $linebody->jumlah;
				}
				$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>JUMLAH ".$line->nama_unit."</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
					</tr>";
				$jumlahtotal += $jumlah;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>JUMLAH &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'>TOTAL &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
				";
				
		}
   			
   				
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($params['type_file'] == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			/*$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:H50');*/
            $objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setFitToWidth(1);
            $objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setFitToHeight(0);                        
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.2);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.2);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:H7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:H4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'G'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

			# Fungsi Wrap Text
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('A:H')
						->getAlignment()
						->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			# END Fungsi Wrap Text

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PENERIMAAN PER PASIEN PER JENIS PENERIMAAN',$html);
			echo $html;
		}
		
	}

}
?>