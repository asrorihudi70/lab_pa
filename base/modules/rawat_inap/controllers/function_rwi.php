<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_rwi extends  MX_Controller {		
	private $jam;
	private $dbSQL;
	private $kd_kasir;
	private $id_user;

    public $ErrLoginMsg='';

    public function __construct()
    {

        parent::__construct();
        $this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
		//$this->load->model('rawat_inap/Model_posting');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
		$this->id_user 	= $this->session->userdata['user_id']['id'];
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

    }	

	function getUnit(){
		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$explode_kd_unit = explode(',',$kd_unit);
		$arr_unit= array();

		if ($this->input->post('parent')) {
			$params['parent'] = $this->input->post('parent');
		}else{
			$params['parent'] = null;
		}


			if ($params['parent'] != null) {
				$query = $this->db->query("SELECT kd_unit, nama_unit from unit where parent='".$params['parent']."'")->result();
			}else{
				$query = $this->db->query("SELECT kd_unit, nama_unit from unit")->result();
			}
			$i = 0;
			foreach($query as $line){
				$arr_unit[$i]['KD_UNIT'] 	= $line->kd_unit;
				$arr_unit[$i]['NAMA_UNIT'] 	= $line->nama_unit;
				$i++;
			}
		echo '{success:true, totalrecords:'.count($arr_unit).', listData:'.json_encode($arr_unit).'}';
	}

   function getCustomer()
    {
		$result=$this->db->query("select * from customer")->result();
		// $result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer order by c.customer")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }

   	function getDataKelas()
    {
		$result=$this->db->query("select * from customer")->result();
		// $result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer order by c.customer")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }

	function getDataUnit(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$response 	= array();
		$data_unit 	= array();
		$params 	= array(
			'nama_unit' => $this->input->post('nama_unit'),
		);
		$query = $this->db->query("SELECT * FROM unit where nama_unit in (".$params['nama_unit'].")");
		$x = 0;
		$y = 0;
		foreach ($query->result() as $data_header) {
			unset($query);
			//$query = $this->db->query("SELECT kd_unit, no_kamar, nama_kamar FROM kamar where kd_unit like '".$data_header->kd_unit."%'");
			$query = $this->db->query("SELECT kd_unit, no_kamar, nama_kamar FROM kamar where kd_unit in (SELECT kd_unit FROM unit WHERE parent = '".$data_header->kd_unit."') and jumlah_bed > 0");
			
			foreach ($query->result() as $data_detail) {
				$data_unit[$x] = array(
					'NO_KAMAR' 	=> $data_detail->no_kamar,
					'NAMA_KAMAR'=> $data_detail->nama_kamar,
					'KD_UNIT' 	=> $data_header->kd_unit,
				);
				$x++;
			}
		}

		
		$response['unit'] = $data_unit;
		echo json_encode($response);
	}


	function laporan_pasien_pulang(){
		$common 	= $this->common;
   		$result 	= $this->result;
   		$title 		= 'Laporan pasien pulang';
		$param 		= json_decode($_POST['data']);
		
		 
		
		/*$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		*/

		// echo json_encode($param->start_date);
		$html='';	
   		
		/* PARAMETER SPESIALISASI */
		$criteriaSpesialisasi = "";
		if(is_numeric($param->tmp_kd_spesial)){
			$criteriaSpesialisasi = " AND g.kd_spesial = '".$param->tmp_kd_spesial."'";
		}else{
			$criteriaSpesialisasi = "";
		}
		
		/* PARAMETER UNIT */
		$criteriaUnit = "";
		if(strlen($param->tmp_kd_unit) > 0){
			$criteriaUnit = $param->tmp_kd_unit;
		}
		
		/*Parameter Customer*/
		$criteriaCustomer = "";
		if(strlen($param->tmp_kd_customer) > 0 || $param->tmp_kd_customer != 'undefined'){
			$criteriaCustomer=" And c.kd_customer in (".$param->tmp_kd_customer.") ";
		}

		/*Parameter No Kamar*/
		$criteriaNo_kamar = "";
		if(strlen($param->tmp_no_kamar) > 0){
			$criteriaNo_kamar = " AND nginap.no_kamar in (".$param->tmp_no_kamar.") ";
		}

		/*Parameter No Kamar*/
		$criteriaSorting = "";
		if($param->sorting_by == 1){
			$criteriaSorting = " ORDER BY no_medrec ";
		}else if($param->sorting_by == 2){
			$criteriaSorting = " ORDER BY nama ";
		}else if($param->sorting_by == 3){
			$criteriaSorting = " ORDER BY tgl_masuk ";
		}else if($param->sorting_by == 4){
			$criteriaSorting = " ORDER BY nama_kamar ";
		}else if($param->sorting_by == 5){
			$criteriaSorting = " ORDER BY tgl_keluar ";
		}

		$query = $this->db->query("SELECT
					COALESCE(EXTRACT(DAY FROM (SUM( CASE WHEN k.tgl_keluar is null THEN NOW() ELSE k.tgl_keluar END - k.tgl_masuk ))),0)::int8 as lama,
					P.Kd_Pasien AS no_medrec,
					P.nama,
					P.alamat,
					P.nama_keluarga,
					P.jenis_kelamin,
					nginap.tgl_masuk,
					nginap.tgl_keluar,
					nginap.no_kamar,
					kamar.nama_kamar,
					k.jam_masuk,
					k.jam_keluar,
					C.customer,
					sp.status_pulang AS keadaan_pasien,
					ck.cara_keluar AS cara_keluar
					FROM
						pasien p
						INNER JOIN kunjungan k ON k.kd_pasien = p.kd_pasien
						INNER JOIN customer c ON k.kd_customer= c.kd_customer
						INNER JOIN status_pulang sp ON sp.kd_status_pulang= k.keadaan_pasien
						INNER JOIN cara_keluar ck ON ck.kd_cara_keluar = k.cara_keluar
						INNER JOIN unit u ON u.kd_unit = k.kd_unit 
						INNER JOIN kelas ON u.kd_kelas = kelas.kd_kelas
						INNER JOIN nginap on k.kd_pasien = nginap.kd_pasien and k.kd_unit = nginap.kd_unit and k.tgl_masuk = nginap.tgl_masuk and k.urut_masuk = nginap.urut_masuk AND nginap.akhir = 'true' 
						INNER JOIN spesialisasi ON nginap.kd_spesial = spesialisasi.kd_spesial
						INNER JOIN kamar ON nginap.kd_unit_kamar = kamar.kd_unit AND nginap.no_kamar = kamar.no_kamar 
					WHERE
					k.tgl_keluar BETWEEN '".$param->start_date."' AND '".$param->last_date."'
					".$criteriaCustomer."
					".$criteriaSpesialisasi."
					AND nginap.Kd_Unit_Kamar IN (SELECT Kd_Unit FROM Unit
					WHERE Parent in (".$criteriaUnit."))  ".$criteriaNo_kamar."
					GROUP BY P.Kd_Pasien,
					nginap.tgl_masuk,
					nginap.tgl_keluar,
					
					k.jam_masuk,
					k.jam_keluar,
					C.customer,
					kamar.nama_kamar,
					nginap.no_kamar,
					sp.status_pulang,
					ck.cara_keluar
					".$criteriaSorting);

		if($param->type_file == true){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}

			$html.='
				<table border = "1" cellpadding="2">
				<thead>
				<tr>
					<th colspan="9" style='.$font_style.'>Laporan Pasien Pulang
				</tr>
				<tr>
					<th colspan="9" style='.$font_style.'>'.date_format(date_create($param->start_date), 'd/M/Y').' - '.date_format(date_create($param->last_date), 'd/M/Y').'
				</tr>
				</thead></table>';

			$html.='
				<table border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="15px">No</th>
						<th align="center" width="40px">No Medrec</th>
						<th align="center">Nama</th>
						<th align="center">Kelamin</th>
						<th align="center">Alamat</th>
						<th align="center">Penjamin</th>
						<th align="center">Keadaan pasien</th>
						<th align="center">Cara Keluar</th>
						<th align="center">Masuk</th>
						<th align="center">Keluar</th>
						<th align="center">Kamar</th>
						<th align="center">Lama</th>

					  </tr>
				</thead><tbody>';
				$no = 1;
			if ($query->num_rows() > 0) {
				$jumlah_lama = 0;
				foreach ($query->result() as $data) {
     	 			// $lama 	= $this->db->query("select * from getumur ('".$data->tgl_keluar."', '".$data->tgl_masuk."')")->row()->getumur;
			       //  $lama 	= str_replace("years","Tahun",$lama);
			       //  $lama 	= str_replace("mons","Bulan",$lama);
			       //  $lama 	= str_replace("days","Hari",$lama);
			       //  $lama 	= str_replace("day","Hari",$lama);
     	 			// $lama 	= substr($lama, 0, strlen($lama)-1);
     	 			$lama 	= $data->lama." hari";
					$html .= "<tr valign='top'>";
					$html .= "<td valign='top' align='left'>".$no."</td>";
					$html .= "<td valign='top' align='left'>".$data->no_medrec."</td>";
					$html .= "<td valign='top' align='left'>".$data->nama."</td>";
					if ($data->jenis_kelamin == 't') {
						$html .= "<td valign='top' align='left'>L</td>";
					}else{
						$html .= "<td valign='top' align='left'>P</td>";
					}
					$html .= "<td valign='top' align='left'>".substr($data->alamat, 0 ,20)."... "."</td>";
					$html .= "<td valign='top' align='left'>".$data->customer."</td>";
					$keadaan_pasien = $data->keadaan_pasien;
					$keadaan_pasien = str_replace("<", "Kurang dari", $keadaan_pasien);
					$keadaan_pasien = str_replace(">", "Lebih dari", $keadaan_pasien);
					$html .= "<td valign='top' align='left'>".$keadaan_pasien."</td>";
					$html .= "<td valign='top' align='left'>".$data->cara_keluar."</td>";
					$html .= "<td valign='top' align='left'>".date_format(date_create($data->tgl_masuk), 'd/M/Y')." ".date_format(date_create($data->jam_masuk), 'H:i')."</td>";
					$html .= "<td valign='top' align='left'>".date_format(date_create($data->tgl_keluar), 'd/M/Y')." ".date_format(date_create($data->jam_keluar), 'H:i')."</td>";
			
					$html .= "<td valign='top' align='left'>".$data->nama_kamar."</td>";
							$html .= "<td valign='top' align='left'>".$lama."</td>";
					$html .= "</tr>";
					$jumlah_lama += $lama;
					$no++;
				}
				$html .= "<tr valign='top'>";
				$html .= "<td colspan='11' align='right'>Lamanya menginap</td>";
				$html .= "<td colspan='3' align='right'>".$jumlah_lama." hari</td>";
				$html .= "</tr>";
			}else{
					$html .= "<tr valign='top'>";
					$html .= "<td colspan='9' align='center'>Tidak ada data</td>";
					$html .= "</tr>";
			}
			$html .= "</tbody></table>";

		$prop=array('foot'=>true);
		if($param->type_file == true){
			$no 		= ((int)$no+4);
			$print_area	= 'A1:K'.$no;
			$area_wrap	= 'A4:K'.$no;
			/*$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	*/	
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A4:J4')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
				/*'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			 	),
				'borders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					// 'top' => array(
			 	// 	)
			 	),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
						'argb' => 'FFA0A0A0'
					),
					'endcolor' => array(
						'argb' => 'FFFFFFFF'
					)
				)*/
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(8);
			$objPHPExcel->getActiveSheet()->getStyle("A1:K2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:A'.((int)$no+4))
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('A'.((int)$no+4).':A'.((int)$no+5))
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);  //No
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10); //Medrec
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(21); //Nama
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5); //Nama
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); //Alamat
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14); //
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14); //
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14); //
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14); //
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(7);  //
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13); //
			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/

			$textFormat='@';//'General','0.00','@';

/*
			$objPHPExcel->getActiveSheet()
						->getStyle('H5:H'.((int)$no+4))
						->getNumberFormat()
					    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);*/

			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore
/*
			$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);*/
			// echo $html;
			
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_pulang_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			$this->common->setPdf('L','Laporan pulang pasien',$html);	
		}
	}


	private function getAge($tgl1,$tgl2){
		$jumHari=(abs(strtotime($tgl1->format('Y-m-d'))-strtotime($tgl2->format('Y-m-d')))/(60*60*24));
		$ret=array();
		$ret['year']=floor($jumHari/365);
		$sisa=floor($jumHari-($ret['year']*365));
		$ret['month']=floor($sisa/30);
		$sisa=floor($sisa-($ret['month']*30));
		$ret['day']=$sisa;
		
		if($ret['year']==0  && $ret['month']==0 && $ret['day']==0){
			$ret['day']=1;
		}
		return $ret;
	}

	/*
		UPDATE PENGECEKKAN DATA IMPORT PEMBAYARAN UNTUK FITUR PELUNASAN
		HADAD AL GOJALI
		2018-02-07
	*/

	
	public function cek_data_import_pelunasan(){
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 	=> $this->input->post('kd_kasir'),
			'urut' 	=> $this->input->post('urut'),
			'tgl_bayar' 	=> $this->input->post('tgl_bayar'),
			'kd_pay' 	=> $this->input->post('kd_pay'),
		);

		$query = $this->db->query("
									SELECT * from detail_bayar db
									where db.kd_kasir = '".$params['kd_kasir']."' and db.no_transaksi = '".$params['no_transaksi']."'
										and db.kd_kasir||db.no_transaksi||db.urut||db.tgl_transaksi||db.kd_pay
										in 
											(
												select dbl.kd_kasir||dbl.no_transaksi||dbl.urut||dbl.tgl_transaksi||dbl.kd_pay
												from detail_bayar_lunas dbl 
												where dbl.kd_kasir = '".$params['kd_kasir']."' 
													and dbl.no_transaksi = '".$params['no_transaksi']."'
													and dbl.urut = '".$params['urut']."'
													and dbl.kd_pay = '".$params['kd_pay']."'
											)"
		);

		$response = array();
		if ($query->num_rows() > 0) {
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}

		echo json_encode($response);
	}
	
	/*
		UPDATE SHIFT PER UNIT
		==================================================================================
	 */
	
	public function getCurrentShift(){
		$response 	= array();
		/* $params 	= array(
			'kd_bagian' 	=> $this->input->post('kd_bagian'),
		); */
		
		$params 	= array(
			'kd_unit' 	=> $this->input->post('kd_bagian'),
		);
		
		$query = $this->db->query("SELECT * FROM rwi_shift WHERE kd_unit = '".$params['kd_unit']."'");
		$response['sekarang'] 	= $query->row()->shift;
		if ($query->num_rows() > 0) {
			if ($query->row()->shift == $query->row()->no_shift) {
				$response['tujuan'] 	= 1;
			}else{
				$response['tujuan'] 	= (int)$query->row()->shift + 1;
			}
		}else{
			$response['tujuan'] = null;	
		}
		
		$response['status'] = true;
		echo json_encode($response);
	}
	
	/*
		==================================================================================
	 */
	//bayi rawat gabung 2018-10-12
	public function getKamarIbu(){
		$medrec = $_POST['medrec'];
		
		$kamar = $this->db->query("
			SELECT dk.nama as nama_dokter,kj.kd_dokter,ag.agama,pr.kd_propinsi,kb.kd_kabupaten,kc.kd_kecamatan,B.*,km.nama_kamar,pr.propinsi,kb.kabupaten,kc.kecamatan,kl.kelurahan,A.kd_spesial,d.kd_kelas,A.kd_unit,A.no_kamar,
			c.spesialisasi||'/ '||e.kelas||'/ '||A.no_kamar||' [ '||D.nama_unit||' ] ' as nama_kamars,D.nama_unit,c.spesialisasi,e.kelas,A.*
			FROM NGINAP A
				INNER JOIN PASIEN B ON B.KD_PASIEN=A.KD_PASIEN
				INNER JOIN spesialisasi c ON c.kd_spesial=a.kd_spesial
				INNER JOIN unit d ON d.kd_unit=a.kd_unit_kamar
				INNER JOIN Kelas e ON d.Kd_Kelas = e.Kd_Kelas
				inner join kelurahan kl on kl.kd_kelurahan = B.KD_KELURAHAN 
				inner join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
				inner join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
				inner join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
				INNER JOIN KAMAR KM ON KM.NO_KAMAR = A.NO_KAMAR AND A.KD_UNIT=KM.KD_UNIT
				INNER JOIN agama ag ON ag.kd_agama = B.kd_agama
				inner join kunjungan kj on
					kj.kd_pasien =a.kd_pasien and 
					kj.kd_unit =a.kd_unit and 
					kj.tgl_masuk =a.tgl_masuk and 
					kj.urut_masuk =a.urut_masuk
				inner join dokter dk on dk.kd_dokter = kj.kd_dokter
			WHERE A.KD_PASIEN='".$medrec."' 
			AND A.AKHIR='T' AND A.TGL_KELUAR IS NULL
		")->row();
		
		if(count($kamar) > 0){
			echo '{success:true, totalrecords:'.count($kamar).', listData:'.json_encode($kamar).'}';
		}else{
			echo '{success:false}';
		}
	}
	
	public function getTempatLahir(){
		$tempat_lahir = $this->db->query(" select setting from sys_setting where key_data ='rwi_setting_tempat_lahir' ")->row();
		if(count($tempat_lahir) > 0){
			$tempat_lahir=$tempat_lahir->setting;
			echo '{success:true,tempat_lahir:"'.$tempat_lahir.'"}';
		}else{
			$tempat_lahir='';
			echo '{success:false}';
		}
	}
	
	public function getHakKelas(){
		$hak_kelas = $this->db->query(" select setting from sys_setting where key_data ='eklaim_customer_bpjs' ")->row()->setting;
		
		$array_hak_kelas = explode (',',$hak_kelas);
		$kd_customer = $_POST['kd_customer'];
		$ada =0;
		if (in_array($kd_customer, $array_hak_kelas)) {
			$ada = 1;
		}
		if($ada == 1){
			echo '{success:true,hak_kelas:true}';
		}else{
			echo '{success:false}';
		}
	}
	
	public function cek_bayi_rawat_gabung(){
		$get_rawat_gabung = $this->db->query(" 
			select rawat_gabung from nginap 
			where kd_pasien='".$_POST['kd_pasien']."' 
				and akhir='t' and tgl_keluar is null
		")->row();
		if(count($get_rawat_gabung) > 0){
			$rawat_gabung=$get_rawat_gabung->rawat_gabung;
			echo '{success:true,rawat_gabung:"'.$rawat_gabung.'"}';
		}else{
			echo '{success:false}';
		}
	}

	public function get_data_dokter(){
		$query=$this->db->query("SELECT dokter.kd_dokter,dokter.nama FROM dokter INNER JOIN dokter_spesial ON
								 dokter.kd_Dokter = dokter_Spesial.kd_Dokter WHERE dokter_spesial.kd_spesial = 31 
								 GROUP BY dokter.kd_dokter,dokter.nama HAVING COUNT ( dokter_spesial.kd_dokter ) > 1 
								 ORDER BY dokter.nama ASC OFFSET 0  LIMIT 500")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	}
	
}