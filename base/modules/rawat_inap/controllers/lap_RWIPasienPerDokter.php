<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWIPasienPerDokter extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PER DOKTER PER DIAGNOSA';
		$param=json_decode($_POST['data']);
		
		// $kd_poli   = $param->kd_poli;
		$kd_dokter = $param->kd_dokter;
		$tglAwal   = $param->tglAwal;
		$tglAkhir  = $param->tglAkhir;
		$type_file = $param->type_file;
		$order_by  = $param->order_by;
		$criteriaUnit="";
		$arrayDataUnit = $param->poliklinik;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$criteriaUnit 	= substr($criteriaUnit, 0, -1);
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if (strtolower($order_by) == strtolower("Medrec") || $order_by == 0) {
			$criteriaOrder = "ORDER BY p.kd_pasien ASC";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")  || $order_by == 1){
			$criteriaOrder = "ORDER BY nama_pasien ASC";
		}else if(strtolower($order_by) == strtolower("Penjamin")  || $order_by == 3){
			$criteriaOrder = "ORDER BY c.customer ASC";
		}else{
			$criteriaOrder = " ORDER BY k.tgl_masuk ASC ";
		}

		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA DOKTER';
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		
		// if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			// $ckd_unit_far=" AND LEFT (u.kd_unit, 1)='2'";
			// $unitfar='SEMUA POLIKLINIK';
		// } else{
			// $unitfar = "";
			// $ckd_unit_far=" AND u.kd_unit = '".$kd_poli."'";
			// $unitfar.="POLIKLINIK "; 
			// $unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit='".$kd_poli."'")->row()->nama_unit); 
		// }
		
		$ckd_unit_far=" AND u.kd_unit in (".$criteriaUnit.")";
		$unitfarres = $this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit in (".$criteriaUnit.")")->result();
		$unitfar = '';
		for($i=0;$i<count($unitfarres);$i++){
			$unitfar.=$unitfarres[$i]->nama_unit.", ";
		}
		$unitfar = substr($unitfar, 0, -2);
		
		$queryHead = $this->db->query(
			"SELECT 
			DISTINCT(d.kd_dokter),
			d.kd_dokter as kd_dokter,
			d.nama as nama_dokter 
			from kunjungan k 
				inner join transaksi t on t.kd_pasien=k.kd_pasien AND t.kd_unit=k.kd_unit AND t.tgl_transaksi=k.tgl_masuk AND t.urut_masuk=k.urut_masuk 
				inner join dokter d on d.kd_dokter=k.kd_dokter 
				inner join unit u on u.kd_unit=k.kd_unit 
				WHERE t.ispay='true' AND (k.tgl_keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$ckd_dokter_far." ".$ckd_unit_far.""
		);
		if ($queryHead->num_rows() == 0) {
			echo "<h3>Tidak ada data</h3>";
		}
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table border="0" id="queryHead">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="6"> Laporan Pasien dari '.$dokterfar.'</th>
					</tr>
					<tr>
						<th colspan="6"> Poliklinik '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100%" height="20" border="1" cellspacing="0" style="border-collapse: collapse;">
			<thead>
				 <tr>
					<th rowspan=2 width="5%" align="center">No</th>
					<th rowspan=2 width="18%" align="center">Nama Dokter/ Pasien</th>
					<th colspan=2 width="6%" align="center">Masuk</th>
					<th colspan=2 width="6%" align="center">Keluar</th>
					<th rowspan=2 width="4%" align="center">Lama Rawat</th>
					<th rowspan=2 width="12.3%" align="center">Kelompok</th>
					<th rowspan=2 width="12.2%" align="center">Unit</th>
					<th rowspan=2 width="12.3%" align="center">Status Diagnosa</th>
					<th rowspan=2 width="12.2%" align="center">Diagnosa</th>
					
				  </tr>
				  <tr>
					<th width="7%" align="center">Tanggal </th>
					<th width="5%" align="center">Jam</th>
					<th width="7%" align="center">Tanggal </th>
					<th width="5%" align="center">Jam</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$total=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
								<tr>
									<th align="left" colspan="11" style="background-color: #fff700; padding-left:5px;">'.$line->nama_dokter.'</th>
								  </tr>';
				$queryBody = $this->db->query( 
					"SELECT 
						DISTINCT(p.kd_pasien),
						p.nama as nama_pasien, 
						c.customer,
						K.tgl_masuk,
						k.jam_masuk,
						K.tgl_keluar,
						k.jam_keluar 
						from kunjungan k 
							inner join pasien p on p.kd_pasien=k.kd_pasien 
							inner join dokter d on d.kd_dokter=k.kd_dokter 
							inner join customer c on c.kd_customer=k.kd_customer 
							inner join unit u on u.kd_unit=k.kd_unit 
							inner join transaksi t on k.kd_unit=t.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.kd_pasien=k.kd_pasien 
						where (k.tgl_keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."')
						AND t.ispay='true'	
						AND d.kd_dokter='".$line->kd_dokter."' ".$ckd_unit_far." 
						GROUP BY K.tgl_masuk, P.kd_pasien, C.customer,k.jam_masuk,k.tgl_keluar,k.jam_keluar	
						$criteriaOrder"
				);
										
				$query2       = $queryBody->result();
				$query2_count = $queryBody->num_rows();
				
				$noo=0;
				foreach ($query2 as $line2){
					$awal  = date_create($line2->tgl_masuk);
					$akhir = date_create($line2->tgl_keluar);
					$diff  =date_diff( $awal, $akhir );
					$noo++;
					$html.="<tr>";
					$html.="<td valign='top' align='center' width='10'>".$noo."</td>";
					$html.="<td width='' style='padding-left:10px;' valign='top'>(".$line2->kd_pasien.") ".$line2->nama_pasien."</td>";
					$html.="<td valign='top' align='center' width='10'>".date("d-m-Y", strtotime($line2->tgl_masuk))."</td>";
					$html.="<td valign='top' align='center' width='10'>".str_replace("1900-01-01 ", "", $line2->jam_masuk)."</td>";
					$html.="<td valign='top' align='center' width='10'>".date("d-m-Y", strtotime($line2->tgl_keluar))."</td>";
					$html.="<td valign='top' align='center' width='10'>".str_replace("1900-01-01 ", "", $line2->jam_keluar)."</td>";
					$html.="<td valign='top' align='center' width='10'>".$diff->d." Hari</td>";
					$html.="<td colspan='8' style='padding:-1px;'>";	
					$queryBodyDetail = $this->db->query( 
							"SELECT 
								DISTINCT(c.kd_customer),
								k.kd_pasien,
								k.kd_unit,
								k.kd_customer,
								k.tgl_masuk,
								k.urut_masuk,
								k.kd_customer,

								p.nama as nama_pasien,
								p.kd_pasien,

								pn.penyakit, 
								pn.kd_penyakit, 

								c.kd_customer,
								c.customer,

								d.kd_dokter,
								d.nama as nama_dokter,

								u.kd_unit,
								u.nama_unit,

								mrp.kd_pasien,
								mrp.kd_unit,
								mrp.stat_diag,
								mrp.urut_masuk,
								mrp.kd_penyakit 
								from kunjungan k 
									left join mr_penyakit mrp on 
									(mrp.kd_pasien=k.kd_pasien and mrp.kd_unit=k.kd_unit and mrp.tgl_masuk=k.tgl_masuk and mrp.urut_masuk=k.urut_masuk) 
									inner join pasien p on p.kd_pasien=k.kd_pasien 
									left join penyakit pn on pn.kd_penyakit=mrp.kd_penyakit 
									inner join customer c on c.kd_customer=k.kd_customer 
									inner join dokter d on d.kd_dokter=k.kd_dokter
									inner join unit u on u.kd_unit=k.kd_unit
									inner join transaksi t on k.kd_unit=t.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.kd_pasien=k.kd_pasien 
								where (k.tgl_keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								AND p.kd_pasien='".$line2->kd_pasien."' 
								AND t.ispay='t'	
								AND d.kd_dokter='".$line->kd_dokter."'
								".$ckd_unit_far."
								".$criteriaOrder.""
						);			
						$query3 = $queryBodyDetail->result();
						if(count($query3) > 0) {	
							$html.='<table  class="table" border="1">';
							foreach ($query3 as $line3) {
								// if($line3->stat_diag=="0" || $line3->stat_diag==false){
								// 	$sts_diagnosa='Awal';
								// }else{
								// 	$sts_diagnosa='Utama';
								// }

								            if ($line3->stat_diag == 0) {
                $tmpstatdiag = 'Diagnosa Awal';
            } else if ($line3->stat_diag == 1) {
                $tmpstatdiag = 'Diagnosa Utama';
            } else if ($line3->stat_diag == 2) {
                $tmpstatdiag = 'Komplikasi';
            } else if ($line3->stat_diag == 3) {
                $tmpstatdiag = 'Diagnosa Sekunder';
            } else if ($line3->stat_diag == 4) {
                $tmpstatdiag = 'Diagnosa Comorbiditas';
            }
								$html.="<tr>";
								$html.="<tr><td width='100%' style='padding-left:10px;' valign='center'>".$line3->customer."</td>";
								$html.="<td width='100%' style='padding-left:10px;' valign='center'>".$line3->nama_unit."</td>";
								$html.="<td width='100%' style='padding-left:10px;' valign='center'>".$tmpstatdiag."</td>";
								$html.="<td width='100%' style='padding-left:10px;' valign='center'>".$line3->penyakit."</td>";
								$html.="</tr>";
							}
							$html.='</table>';


						}
					$html.="</td>
					 </tr>";
				}
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td colspan='10' style='padding:5px;'><b>Jumlah Kunjungan : ".$query2_count."</b></td>";
				$html.="</tr>";
				$total += $query2_count;
			}
			$html.="<tr>";
			$html.="<td></td>";
			$html.="<td colspan='10' style='padding:5px;'><b>Total Kunjungan : ".$total."</b></td>";
			$html.="</tr>";
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>
			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// $this->common->setPdf('L','Lap. Pasien Per Dokter',$html);	

		if ($type_file === true || $type_file == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Lap_Pasien_Per_Dokter_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{
			// echo $html;
			$this->common->setPdf('L','Lap. Per Dokter Per Diagnosa',$html);	
		}

   	}
	
}
?>