<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rawat_inap extends MX_Controller {
	private $dbSQL = false;
    public function __construct(){
        parent::__construct();
		$this->load->library('session');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_visite_dokter');
		$this->load->model('Tbl_data_pasien');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_pasien_inap');
		$this->load->model('Tbl_data_nginap');
		$this->load->model('Tbl_data_spesialisasi');
    }	 

	public function index(){
        $this->load->view('main/index');       
   	}

   	
	public function lap_RADTindakanDokter(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		/*print_r($param);
		die();*/
		$title = "LAPORAN KINERJA PELAKSANA PER TINDAKAN";
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		if (isset($param->kelompok) === true) {
			$kelompok  			= $param->kelompok;
		}else{
			$kelompok 			= "SEMUA";
		}
		$kd_kelompok  			= $param->kd_kelompok;
		$kd_customer  			= $param->kd_customer;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$criteria = " t.ispay = 'true' and ( k.tgl_masuk BETWEEN '".date_format(date_create($tglAwal), 'Y-m-d')."' AND '".date_format(date_create($tglAkhir), 'Y-m-d')."' ) ";

		// var_dump($param);die;
		if (strtoupper($kelompok) != "SEMUA") {
			if (strtoupper($kd_customer) != "SEMUA") {
				$criteria .= " AND (cus.kd_customer = '".$kd_customer."' OR cus.customer = '".$kd_customer."' ) ";
			}
			if (strtoupper($kelompok) == 'PERSEORANGAN') {
				$criteria .= " AND kon.jenis_cust = '0' ";
			}else if (strtoupper($kelompok) == 'PERUSAHAAN'){
				$criteria .= " AND kon.jenis_cust = '1' ";
			}else if (strtoupper($kelompok) == 'ASURANSI'){
				$criteria .= " AND kon.jenis_cust = '2' ";
			}
		}

		if (strlen($param->kd_unit) > 0 && isset($param->kd_unit) === true) {
			$criteria .= " AND K.kd_unit IN ( ".$param->kd_unit." ) ";
		}
		if($param->profesi != "11" && $param->profesi != "12")
		if (strtoupper($param->profesi) != 'SEMUA' && isset($param->profesi) === true ) {
			$criteria .= " AND vd.kd_job = '".$param->profesi."' ";
		}	
	//	var_dump($param->profesi);
		if( $param->profesi == "11"){
			$criteria .= " AND ( d.nama like '%Sp.%' OR nama like '%SpOG%') AND jenis_dokter='1'";	
		}
		if( $param->profesi == "12"){
			$criteria .= " AND nama not ILIKE '%Sp.%' AND nama not ILIKE '%Sp%' AND nama not ILIKE '%SpOG%' AND jenis_dokter='1'";	
		}
		

		if (strtoupper($kd_dokter) != 'SEMUA') {
			if (isset($kd_dokter) === true) {
				if (strlen($kd_dokter) > 0) {
					$criteria .= " and vd.kd_dokter = '".$kd_dokter."' ";
				}
			}
		}

		$query = "SELECT * FROM customer where kd_customer = '".$kd_customer."' OR customer = '".$kd_customer."'";
		$query = $this->db->query($query);
		$label_kelompok = "SEMUA";
		if ($query->num_rows() > 0) {
			$label_kelompok = $query->row()->customer;
		}else{
			$label_kelompok = strtoupper($kelompok);
		}

		$query = "
			SELECT 
				vd.kd_dokter, 
				d.nama as nama_dokter,
				p.deskripsi as produk,
				sum(dt.qty) AS jml_produk,
				count(DISTINCT(k.kd_pasien)) AS jml_pasien
			FROM 
				visite_dokter vd 
				INNER JOIN 
				transaksi t ON 
					vd.kd_kasir = t.kd_kasir AND 
					vd.no_transaksi = t.no_transaksi 
				INNER JOIN 
				detail_transaksi dt ON 
					dt.kd_kasir = vd.kd_kasir AND
					dt.no_transaksi = vd.no_transaksi AND 
					dt.urut = vd.urut AND 
					dt.tgl_transaksi = vd.tgl_transaksi 
				INNER JOIN 
				kunjungan k ON 
					k.kd_pasien = t.kd_pasien AND
					k.tgl_masuk = t.tgl_transaksi AND
					k.kd_unit = t.kd_unit AND 
					k.urut_masuk = t.urut_masuk 
				INNER JOIN 
				dokter d ON 
					d.kd_dokter = vd.kd_dokter 
				INNER JOIN kontraktor kon ON kon.kd_customer = k.kd_customer
				INNER JOIN customer cus ON cus.kd_customer = k.kd_customer
				INNER JOIN 
				produk p ON 
					p.kd_produk = dt.kd_produk
				WHERE 
		".$criteria."
		GROUP BY vd.kd_dokter, d.nama, p.deskripsi
		ORDER BY d.nama";
		$query = $this->db->query($query);
		$list_dokter = array();
		foreach ($query->result() as $result) {
			$data = array();
			$data['kd_dokter']   =  $result->kd_dokter;
			$data['nama_dokter'] =  $result->nama_dokter;
			array_push($list_dokter, $data);	
		}

		$list_dokter 	= array_unique($list_dokter, SORT_REGULAR);
		$list_data 		= array();
		foreach ($list_dokter as $res) {
			foreach ($query->result() as $result) {
				if ($res['kd_dokter'] == $result->kd_dokter) {
					$data = array();
					$data['kd_dokter'] 	= $result->kd_dokter;
					$data['produk'] 	= $result->produk;
					$data['jml_produk']	= $result->jml_produk;
					$data['jml_pasien']	= $result->jml_pasien;
					array_push($list_data, $data);
				}
			}
		}

		$html = "";
		//-------------JUDUL-----------------------------------------------
		$html .='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="4"> KELOMPOK PASIEN '.$label_kelompok.'</th>
					</tr>
					<tr>
						<th colspan="4"> KELOMPOK '.strtoupper($param->label_jenis_dokter).'</th>
					</tr>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html ."";
		$html .= "<table width='100%' border='1' cellspacing='0'>";
		$nomer_head = 1;
		$html .= "<tr>";
		$html .= "<th style='padding:3px;' width='5%'></th>";
		$html .= "<th style='padding:3px;' align='center' width='75%'>PELAKSANA</th>";
		$html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Tindakan</th>";
		// $html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Pasien</th>";
		$html .= "</tr>";
		$total_produk = 0;
		$total_pasien = 0;
		foreach ($list_dokter as $res) {
			$jml_produk = 0;
			$jml_pasien = 0;
			$html .= "<tr>";
			$html .= "<th style='padding:3px;'>".$nomer_head."</th>";
			$html .= "<th style='padding:3px;' colspan='2' align='left'>".$res['kd_dokter']." - ".$res['nama_dokter']."</th>";
			$html .= "</tr>";
			foreach ($list_data as $row) {
				$nomer = 1;
				if ($row['kd_dokter'] == $res['kd_dokter']) {
					$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td style='padding:3px;'> - ".$row['produk']."</td>";
					$html .= "<td style='padding:3px;' align='center'>".$row['jml_produk']."</td>";
					// $html .= "<td style='padding:3px;' align='center'>".$row['jml_pasien']."</td>";
					$html .= "</tr>";
					$jml_produk += $row['jml_produk'];
					$jml_pasien += $row['jml_pasien'];
					$nomer++;
				}
			}
			$html .= "<tr>";
			$html .= "<th></th>";
			$html .= "<th style='padding:3px;' align='right'>Sub Total</th>";
			$html .= "<th style='padding:3px;' align='center'>".$jml_produk."</th>";
			// $html .= "<th style='padding:3px;' align='center'>".$jml_pasien."</th>";
			$html .= "</tr>";
			$nomer_head++;
			$total_produk += $jml_produk;
			$total_pasien += $jml_pasien;
		}
		$html .= "<tr>";
		$html .= "<th></th>";
		$html .= "<th style='padding:3px;' align='right'>Grand Total</th>";
		$html .= "<th style='padding:3px;' align='center'>".$total_produk."</th>";
		// $html .= "<th style='padding:3px;' align='center'>".$total_pasien."</th>";
		$html .= "</tr>";
		$html .= "</table>";	

		$this->common->setPdf_penunjang('P',$title ,$html);	
   	}	
}
?>