<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vidaftarrwi extends MX_Controller
{
	private $setup_db_sql  = true;
    public function __construct()
    {
		/*$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}*/
        parent::__construct();
    }

	public function index()
	{
        $this->load->view('main/index');
		
	}


	function read($Params=null)
	{
		$limitSQL = "";
		$limitPG  = "";
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$limitSQL = " TOP ".$Params[1];
			$limitPG  = " ";
		}else{
			$limitPG  = " LIMIT ".$Params[1];
			$limitSQL = " ";
		}
		$datesfrom = date('Y-m-d', strtotime("-10 days"));
		$datesto   = date('Y-m-d');
		$list = array();
		try
		{
		//pasien rwi adalah pasien rwj dan igd yang belum  --- lunas permintaan pak toni ---
			$this->load->model('rawat_inap/tb_rwi');
			$sqldatasrv="SELECT $limitSQL pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir,
						 pasien.tgl_lahir, agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, pasien.kd_kelurahan,
						 pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit,transaksi.kd_unit, kunjungan.tgl_masuk,kunjungan.jam_masuk, kunjungan.urut_masuk,
						 kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI, pasien.kd_pendidikan, pasien.kd_pekerjaan,
							pasien.kd_agama ,pasien.alamat_ktp,pasien.nama_ayah,pasien.nama_ibu,
							pasien.kd_kelurahan_ktp,pasien.kd_pos_ktp,pasien.kd_pos,proktp.kd_propinsi as propinsiktp,kabktp.kd_kabupaten as kabupatenktp,
							kecktp.kd_kecamatan as kecamatanktp,
							kelktp.kd_kelurahan as kelurahanktp ,kl.kelurahan as kelurahan_pasien,
							kelktp.kelurahan as kel_ktp ,
							kecktp.kecamatan as kec_ktp,kabktp.kabupaten as kab_ktp ,proktp.propinsi as pro_ktp,
							pasien.email,pasien.handphone,pasien.telepon,pasien.no_asuransi,pasien.nik
								FROM pasien
							INNER JOIN kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
							INNER join agama ON pasien.kd_agama = agama.kd_agama
							INNER JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
							INNER JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
							inner join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
							inner join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
							inner join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
							inner join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
							inner join transaksi on transaksi.KD_PASIEN = kunjungan.KD_PASIEN and kunjungan.urut_masuk= transaksi.urut_masuk and kunjungan.kd_unit=transaksi.kd_unit and kunjungan.tgl_masuk=transaksi.tgl_transaksi
							INNER JOIN unit ON transaksi.kd_unit = unit.kd_unit

						left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp
						left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
						left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten
						left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi where "; 
                        /*    if (strlen($Params[4])!==0)
                        {
                           $this->db->where(str_replace("~", "'","left(kunjungan.kd_unit,1) in ('2','3') and  lunas=false and kunjungan.tgl_masuk
						   between '".$datesfrom."' and '".$datesto."' and ".$Params[4]."  Order By kunjungan.Tgl_masuk desc, 
						   kunjungan.Jam_masuk Desc limit 500  " ) ,null, false);
						   	$res = $this->tb_rwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                        }
						else
						{
							$this->db->where(str_replace("~", "'", "left(kunjungan.kd_unit,1) in ('2','3')  and  lunas=false and
							kunjungan.tgl_masuk between '".$datesfrom."' and '".$datesto."' Order By 
							kunjungan.Tgl_masuk desc, kunjungan.Jam_masuk Desc limit 500  " ),null, false);
						    $res = $this->tb_rwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
						} */

						$criteria_asal_pasien = $this->db->query("select * from sys_setting where key_data='rwi_asal_pasien' ")->row()->SETTING;
						if (strlen($Params[4]) !== 0) {
							$kriteria=str_replace("~", "'","left(kunjungan.kd_unit,1) in (".$criteria_asal_pasien.") 
								--and (kunjungan.tgl_masuk between '".$datesfrom."' and '".$datesto."') 
								".$Params[4]."  Order By kunjungan.Tgl_masuk desc, 
							kunjungan.Jam_masuk Desc ".$limitPG);
						} else {
							$kriteria=str_replace("~", "'","left(kunjungan.kd_unit,1) in (".$criteria_asal_pasien.")  
								--and (kunjungan.tgl_masuk between '".$datesfrom."' and '".$datesto."') 
								".$Params[4]."
								Order By 
								kunjungan.Tgl_masuk desc, kunjungan.Jam_masuk Desc ".$limitPG);
						}
						
						//$res    = $this->tb_rwj->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
						// echo $sqldatasrv.$kriteria;
						// $dbsqlsrv = $this->load->database('otherdb2',TRUE);
						// $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
						$res = $this->db->query($sqldatasrv.$kriteria);
						foreach ($res->result() as $rec)
						{
							//echo"aa";
							$o=array();
							$o['KD_PASIEN']=$rec->kd_pasien;
							
							$recPenyakit=$this->db->query("SELECT p.kd_penyakit,p.penyakit, m.tgl_masuk AS tgl_masuk,
								case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
								from mr_penyakit m
								inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
								where kd_pasien='".$rec->kd_pasien."'  and m.stat_diag in(0,1) order by tgl_masuk ASC")->result();
								$o['NAMA']          =$rec->nama;
								$o['NAMA_KELUARGA'] =$rec->nama_keluarga;
								$o['JENIS_KELAMIN'] =$rec->jenis_kelamin;
								$o['TEMPAT_LAHIR']=$rec->tempat_lahir;
								$o['RIWAYAT_PENYAKIT']=$recPenyakit;
								$o['TGL_LAHIR']= date_format(date_create($rec->tgl_lahir), 'd/m/Y');
								$o['AGAMA']=$rec->agama;
								$o['GOL_DARAH']=$rec->gol_darah;
								$o['WNI']=$rec->wni;
								$o['NIK']=$rec->nik;
								$o['STATUS_MARITA']=$rec->status_marita;
								$o['ALAMAT']=$rec->alamat;
								$o['TGL_MASUK']=date_format(date_create($rec->tgl_masuk), 'Y-m-d');
								$o['TGL_MASUK_FORMAT']=date_format(date_create($rec->tgl_masuk), 'd/m/Y');
								$o['KD_KELURAHAN']=$rec->kd_kelurahan;
								$o['PENDIDIKAN']=$rec->pendidikan;
								$o['PEKERJAAN']=$rec->pekerjaan;
								$o['KABUPATEN']=$rec->KABUPATEN;
								$o['KECAMATAN']=$rec->KECAMATAN;
								$o['PROPINSI']=$rec->PROPINSI;
								$o['KD_KABUPATEN']=$rec->kd_kabupaten;
								$o['KD_KECAMATAN']=$rec->kd_kecamatan;
								$o['KD_PROPINSI']=$rec->kd_propinsi;
								$o['KD_PENDIDIKAN']=$rec->kd_pendidikan;
								$o['KD_PEKERJAAN']=$rec->kd_pekerjaan;
								$o['KD_AGAMA']=$rec->kd_agama;
								
								$o['ALAMAT_KTP']=$rec->alamat_ktp;
								$o['NAMA_AYAH']=$rec->nama_ayah;
								$o['NAMA_IBU']=$rec->nama_ibu;
								$o['KD_KELURAHAN_KTP']=$rec->kd_kelurahan_ktp;
								$o['KD_POS_KTP']=$rec->kd_pos_ktp;
								$o['KD_POS']=$rec->kd_pos;
								//$o['anamnese']=$rec->anamnese;
								
								$o['PROPINSIKTP']   = $rec->propinsiktp;
								$o['KABUPATENKTP']  = $rec->kabupatenktp;
								$o['KECAMATANKTP']  = $rec->kecamatanktp;
								$o['KELURAHANKTP']  = $rec->kelurahanktp;
								$o['KELURAHAN']     = $rec->kelurahan_pasien;
								$o['NAMA_UNIT']     = $rec->nama_unit;
								$o['KEL_KTP']       = $rec->kel_ktp;
								$o['KEC_KTP']       = $rec->kec_ktp;
								$o['KAB_KTP']       = $rec->kab_ktp;
								$o['PRO_KTP']       = $rec->pro_ktp;
								$o['NO_ASURANSI']   = $rec->no_asuransi;
								$o['EMAIL_PASIEN']  = $rec->email;
								$o['HP_PASIEN']     = $rec->handphone;
								$o['TLPN_PASIEN']   = $rec->telepon;
								$o['KD_UNIT']       = $rec->kd_unit;
								$o['URUT_MASUK']    = $rec->urut_masuk;
								$o['ALIAS_KD_UNIT'] = substr($rec->kd_unit,0,1);
							$list[]=$o;	
						} 
						
		
                }
				  catch(Exception $o)
		{
			echo 'Debug  fail ';
		}

	 	echo '{success:true, totalrecords:'.count($list).', ListDataObj:'.json_encode($list).'}';
	}
		private function GetIdRWJ() {

			/* $ci =& get_instance();
			  $db = $ci->load->database('otherdb',TRUE);
			  $sqlcounter = $db->query("select top 1 kd_pasien as kdpas from pasien Where kd_pasien Like '%-%' And isnumeric(replace(kd_pasien,'-',''))=1 Order By kd_pasien desc
			  ")->row();
			  $sqlvalsql = $sqlcounter->kdpas;
			  $sqlvalsql3 = str_replace("-", "",$sqlvalsql);
			  $sqlvalsql4 = $sqlvalsql3+1;
			  if ($sqlvalsql4<999999)
			  {
			  $sqlvalsql2=str_pad($sqlvalsql4,8,"0",STR_PAD_LEFT);
			  }
			  else
			  {
			  $sqlvalsql2=str_pad($sqlvalsql4,7,"0",STR_PAD_LEFT);
			  }
			  $getnewmedrecsql = substr($sqlvalsql2, 0,1).'-'.substr($sqlvalsql2,2,2).'-'.substr($sqlvalsql2,4,2).'-'.substr($sqlvalsql2,-2); */


			$db = $this->load->database('otherdb2',TRUE);
		   // $this->load->model('rawat_jalan/getmedrec');
			//$res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien", "");
			$res = $db->query("SELECT top 1 replace(kd_pasien,'-','') as KD_PASIEN from pasien Where kd_pasien Like '%-%' Order By kd_pasien desc");
			if (count($res->result()) > 0) {
				
				$nm = $res->row()->KD_PASIEN;
				//penambahan 1 digit
				//echo (int) $nm+1; 
				$nomor = (int) $nm + 1;


				//echo("'$nomor'");
				if ($nomor < 999999) {
					$retVal = str_pad($nomor, 8, "0", STR_PAD_LEFT);
				} else {
					$retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
				}
				//memberikan '-' pada setiap baris angka
				/* echo $retVal.'<br/>';
				echo str_pad($nomor, 7, "0", STR_PAD_LEFT).'<br/>'; */
					
				$getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
				
				/* echo substr($retVal, 0, 1).'<br/>';
				echo substr($retVal, 1, 2).'<br/>';
				echo substr($retVal, 3, 2).'<br/>';
				echo substr($retVal, -2).'<br/>';
				echo $getnewmedrec.'<br/>'; */
			} else {
				$strNomor = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
				$getnewmedrec = $strNomor . "01";
			}
			/* if ($nomor<$sqlvalsql4)
			  {
			  $getnewmedrec=$getnewmedrecsql ;
			  } */
			
			return $getnewmedrec;
		}
        public function SimpanPasien($Params, & $AppId = "") {
			$strError = "";
			$suku = 0;
			$GolDar=0;
			if ($Params["GolDarah"] == "") {
				$GolDar = 0;
			}
			else
			{
				$GolDar = $Params["GolDarah"];
			}

			if ($Params["KDKECAMATAN"] === "") {
				// $tmppropinsi = $Params["Kelurahan"];
				$tmpkecamatan = $Params["Kd_Kecamatan"];
				$tmpkabupaten = $Params["AsalPasien"];
				if ($Params["Pendidikan"]=='' || $Params["Pendidikan"]=='undefined')
				{
					$tmppendidikan =0;
				}
				else
				{
					$tmppendidikan = $Params["Pendidikan"];
				}
				
				if ($Params["Pekerjaan"]=='' || $Params["Pekerjaan"]=='undefined')
				{
					$tmppekerjaan =0;
				}
				else
				{
					$tmppekerjaan = $Params["Pekerjaan"];
				}
				$tmpagama = $Params["Agama"];
			} else {
				// $tmppropinsi = $Params["KDPROPINSI"];
				$tmpkecamatan = $Params["KDKECAMATAN"];
				$tmpkabupaten = $Params["KDKABUPATEN"];
				if ($Params["KDPENDIDIKAN"]=='' || $Params["KDPENDIDIKAN"]=='undefined')
				{
					$tmppendidikan =0;
				}
				else
				{
					$tmppendidikan = $Params["KDPENDIDIKAN"];
				}
				
				if ($Params["KDPEKERJAAN"]=='' || $Params["KDPEKERJAAN"]=='undefined')
				{
					$tmppekerjaan =0;
				}
				else
				{
					$tmppekerjaan = $Params["KDPEKERJAAN"];
				}
				$tmpagama = $Params["KDAGAMA"];
			}

			if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan') {
				$kode_asuransi = NULL;
			} else {
				$kode_asuransi = $Params["KD_Asuransi"];
			}
			if ($Params["NoAskes"] == "") {
				$tmpno_asuransi = $Params["NoSjp"];
			} else {
				$tmpno_asuransi = $Params["NoAskes"];
			}

			if ($Params["Kelurahan"] == "" or $Params["Kelurahan"] === NULL or $Params["Kelurahan"] === "Pilih Kelurahan...") {

				$intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
			} else {
				if (is_numeric($Params["Kelurahan"])) {
					$intkd_lurah = $Params["Kelurahan"];
				} else {
					$intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
				}
			};


			
			$nik_pasien=$Params["part_number_nik"];
			 if ($Params["NoMedrec"] == " " || $Params["NoMedrec"] == "Automatic from the system...") {
				$AppId = $this->GetIdRWJ();
			} else {
				$AppId = $Params["NoMedrec"];
			}
			$tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $_POST['TglLahir'])));
			$data = array("kd_pasien" => $AppId, "nama" => $Params["NamaPasien"],
				"nama_keluarga" => $Params["NamaKeluarga"], "jenis_kelamin" => $Params["JenisKelamin"],
				"tempat_lahir" => $Params["Tempatlahir"], "tgl_lahir" => $tmptgllahir,
				"kd_agama" => $tmpagama, "gol_darah" => $GolDar,
				"status_marita" => $Params["StatusMarita"], "wni" => $Params["StatusWarga"],
				"alamat" => $Params["Alamat"],
				"kd_kelurahan" => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
				"kd_pekerjaan" => $tmppekerjaan,
				"no_asuransi" => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
				"kd_suku" => $suku, "jabatan" => $Params["Jabatan"],
				"kd_perusahaan" => 0,  "nama_ayah" => $Params["NamaAyahPasien"],
				"nama_ibu" => $Params["NamaIbuPasien"], "kd_pos" => $Params["KdposPasien"],
			);
			if ($Params["StatusWarga"]=='false')
			{
				$wni=0;
			}
			else
			{
				$wni=1;
			}
			if ($Params["JenisKelamin"]=='false')
			{
				$jk=0;
			}else{
				$jk=1;
			}
			$datasqlsrv = array("kd_pasien" => $Params["NoMedrec"], "nama" => $Params["NamaPasien"],
				"nama_keluarga" => $Params["NamaKeluarga"], "jenis_kelamin" => $jk,
				"tempat_lahir" => $Params["Tempatlahir"], "tgl_lahir" => $Params["TglLahir"],
				"kd_agama" => $tmpagama, "gol_darah" => $GolDar,
				"status_marita" => $Params["StatusMarita"], "wni" => $wni,
				"alamat" => $Params["Alamat"],
				"kd_kelurahan" => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
				"kd_pekerjaan" => $tmppekerjaan, 
				"no_asuransi" => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
				"kd_suku" => $suku, "jabatan" => $Params["Jabatan"],
				"kd_perusahaan" => 1,  "kd_pos" => $Params["KdposPasien"]
				
			);

		
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER		  	

		   

			$criteria = "kd_pasien = '" . $AppId . "'";

			$this->load->model("rawat_jalan/tb_pasien");
			$this->tb_pasien->db->where($criteria, null, false);
			//$query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
			$query =$this->db->query("select * from pasien where $criteria ");
			//var_dump($query);
			//_QMS_insert('pasien', $datasqlsrv,$criteria);
			//echo count($query->result());
			if (count($query->result()) == 0) {

				
				//-----------insert to sq1 server Database---------------//
			   
				//-----------akhir insert ke database sql server----------------//
				
				$data["kd_pasien"] = $AppId;
				$result = $this->db->insert('pasien',$data);
				//_QMS_insert('pasien', $datasqlsrv,$criteria);
				$strError = "ada ".$AppId;
			} else {
				$this->tb_pasien->db->where($criteria, null, false);
				$dataUpdate = array("nama" => $Params["NamaPasien"]);
				$result = $this->tb_pasien->Update($data);

				//_QMS_update('pasien', $datasqlsrv,$criteria);

				$strError = "ada ".$AppId;
			} 
			return $strError;
		}
        public function save($Params=null)
        {
	
            $mError = "";
            $AppId = "";
            $SchId = $Params['NoMedrec'];
            $Schunit = "";
            $Schtgl = date('Y-m-d');
            $Schjam = date('Y-m-d H:i:s');
            //$Schurut = "";
            $notrans = "";
			$kdPasien="";
            $tmpmedrec = $Params['NoMedrec'];
            $tglmasuk = $Params['TanggalMasuk'];
            date_default_timezone_set("Asia/Jakarta");
			$jammasuk = "1900-01-01".gmdate(" H:i:s", time()+60*60*7);
			$Schurut = $this->GetUrutKunjungan($Params["NoMedrec"], $Params["Poli"],$tglmasuk);
            $tmptglsekarang=date('Y-m-d');
			//$this->GetIdTransaksi('05');
            $tmptglkunjung=date('Y-m-d',strtotime('-3 day',strtotime($tmptglsekarang)));
            $this->db->trans_begin();
			if ($Params['BayiBrLahir'] === 'True')
			{
				$mError = explode(" ",$this->SimpanPasien($Params, $SchId));
				
				if ($mError[0] == "ada") {
					$kdPasien=$mError[1];
					$mError = $this->SimpanKunjungan($Params, $kdPasien, $tglmasuk, $jammasuk,$Schurut);
					
					if($mError == 'sukses')
					{						
						$mError = explode(" ",$this->SimpanTransaksi($Params, $kdPasien, $Schurut, $notrans, $tglmasuk));
						$notransnya=$mError[1];
						//$mError = $this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans);
						if($mError[0] == 'sukses')
						{
							$mError = $this->updatekamar($Params);
							
							if($mError == 'sukses')
							{
								// $mError = $this->updatekamarinduk($Params);
								
								if($mError == 'sukses')
								{
									/* *************Simpan db mysql untuk regonline******************** */
									// $mError = $this->regonline_simpanpasien($Params);
									// /* **************************************************************** */
									// if($mError === 'sukses'){
										echo '{success: true, KD_PASIEN: "'.$SchId.'",NoTrans: "'.$notransnya.'"}';
										$this->db->trans_commit();
									// } else{
										// echo '{success: false}';
										// $this->db->trans_rollback();
									// }
								}else
								{
									echo '{success: false}';
									$this->db->trans_rollback();
								}
							}else
							{
							echo '{success: false}';
							$this->db->trans_rollback();
							}
						}else
						{
						echo '{success: false}';
						$this->db->trans_rollback();
						}
					}else if($mError === 'erorcari') {
						echo '{success: false ,cari : true}';
					} else{
						echo '{success: false}';
						$this->db->trans_rollback();
					}
				}else {
					$this->db->trans_rollback();
					echo '{success: false}';
				}
			}
			else
			{
				/* $db = $this->load->database('otherdb2',TRUE);
				$strQuery = "Select * from kunjungan where kd_pasien= "."'".$tmpmedrec."'"." 
				and ((left(kd_unit,1) in ('2','9')
				and kunjungan.tgl_masuk >= "."'".$tmptglkunjung."'".") or (left(kd_unit,1)='3'))
				Order By Tgl_masuk desc, Jam_masuk Desc";
				$query = $db->query($strQuery);
				$Schurut = $this->GetUrutKunjungan($Params["NoMedrec"], $Params["Poli"],date('Y-m-d'));
				$res = $query->result();
				if($query->num_rows() > 0)
				{ 
					foreach($res as $data)
					{
						$tmptglkunjungpasien = $data->TGL_MASUK;
						$tmpjamkunjungpasien = $data->JAM_MASUK;
					}*/
					
					$mError = $this->SimpanKunjungan($Params, $SchId, $tglmasuk, $jammasuk,$Schurut);
					
					if($mError == 'sukses')
					{
						$mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans, $tglmasuk));
						//var_dump($mError); 
						$notransnya=$mError[1];
						//$mError = $this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans);
						if($mError[0] == 'sukses')
						{
							$mError = $this->updatekamar($Params);
							
							if($mError == 'sukses')
							{
								// $mError = $this->updatekamarinduk($Params);
								
								if($mError == 'sukses')
								{
									/* *************Simpan db mysql untuk regonline******************** */
									// $mError = $this->regonline_simpanpasien($Params);
									// /* **************************************************************** */
									// if($mError === 'sukses'){
										echo '{success: true, KD_PASIEN: "'.$SchId.'",NoTrans: "'.$notransnya.'"}';
										$this->db->trans_commit();
									// } else{
										// echo '{success: false}';
										// $this->db->trans_rollback();
									// }
								}else
								{
									echo '{success: false}';
									$this->db->trans_rollback();
								}
							}else
							{
							echo '{success: false}';
							$this->db->trans_rollback();
							}
						}else
						{
						echo '{success: false}';
						$this->db->trans_rollback();
						}
					}else if($mError === 'erorcari') {
						echo '{success: false ,cari : true}';
					} else{
						echo '{success: false}';
						$this->db->trans_rollback();
					}
					
				   
				/* } */
			}
                    

					/* if ($mError === 'sukses')
                            {
                            echo '{success: true, KD_PASIEN: "'.$SchId.'"}';
                            }
                     else echo '{success: false}'; */
					 
			/*  if ($this->db->trans_status() === TRUE)
				{
				 $this->db->trans_commit();
				}
				else
				{
				$this->db->trans_rollback();
				} */
        }
        
        public function updatekamar($Params)
        {
		$tmpdigunakan="";
		$nm="";
	

           /* $criterianya = "kd_unit = "."'".$Params["KDUnitKamar"]."'"." and no_kamar = "."'".$Params['Kamar']."'"."";
           $this->load->model("rawat_inap/tb_kamar");
           $this->tb_kamar->db->where($criterianya, null, false); */
		   $criterianya = "kd_unit = "."'".$Params["KDUnitKamar"]."'"." and no_kamar = "."'".$Params['Kamar']."'"."";
		   
           $query = $this->db->query("select digunakan from kamar where $criterianya");
           // echo "select digunakan from kamar where $criterianya";
           if (count($query->result()) != 0)
            {
            	foreach ($query->result() as $data) {
            		$nm = $data->digunakan;
					$tmpdigunakan = $nm +1;
            	}
            	// echo $tmpdigunakan;
				$result =  $this->db->query("update kamar set digunakan=$tmpdigunakan where kd_unit = "."'".$Params["KDUnitKamar"]."'"." 
				and no_kamar = "."'".$Params['Kamar']."'"); 
				// echo "update kamar set digunakan=$tmpdigunakan where kd_unit = "."'".$Params["KDUnitKamar"]."'"." 
				// and no_kamar = "."'".$Params['Kamar']."'";

                $strError = "sukses";
            }else{
               $strError = "eror";
            }
            return $strError;
        }
        public function updatekamarinduk($Params)
        {	
			$tmpdigunakan="";
			$nm="";
	
           $criteria = "no_kamar = "."'".$Params['Kamar']."'"."";
           //$this->load->model("rawat_inap/tb_kamarinduk");
		   $query = $this->db->query("select * from kamar where $criteria");
           //$this->tb_kamarinduk->db->where($criteria, null, false);
           //$query = $this->tb_kamarinduk->GetRowList( 0,1, "", "","");
           if (count($query->result()) != 0)
            {  
			$nm = $query->row()->digunakan;
			$tmpdigunakan = (int) $nm +1;
           
				//$this->tb_kamar->db->where($criteria, null, false);
				//$dataUpdate = array("digunakan"=>$tmpdigunakan);
				$result =  $this->db->query("update kamar_induk set digunakan=$tmpdigunakan where kd_unit = "."'".$Params["KDUnitKamar"]."'"." 
				and no_kamar = "."'".$Params['Kamar']."'"); 
                //$result = $this->tb_kamarinduk->Update($dataUpdate);
				$strError = "sukses";
            }else{
                $strError = "eror";
            }
            return $strError;
        }
   //      public function SimpanKunjungan($Params, $SchId,  $tmptglkunjungpasien, $tmpjamkunjungpasien,$Schurut)
   //      {
    
			// $Shiftbagian= $this->GetShiftBagian();
			// $Shiftbagian= (int) $Shiftbagian;
   //          if ($Schurut==='') {
			// 	$antrian = 0;
			// } else {
			// 	$antrian = $Schurut;
			// }
			
			// $kode_customer = $Params["KdCustomer"];
   //          if ($Params["KdRujukan"] === '' || $Params["KdRujukan"] === 'Pilih Rujukan...') {
   //              $tmpkd_rujuk = 0;
   //          }  else {
   //               $tmpkd_rujuk = $Params["KdRujukan"];
   //          }
           
			// $strError = "";
			// if ($Params['asal_pasien']==0) {
			// 	$cari_kd_unit=$this->db->query("select setting from sys_setting where key_data='igd_default_kd_unit'")->row()->setting;
			// } else if ($Params['asal_pasien']==1) {
			// 	$cari_kd_unit=$this->db->query("select setting from sys_setting where key_data='rwj_default_kd_unit'")->row()->setting;
			// } else if ($Params['asal_pasien']==2) {
			// 	$cari_kd_unit=$this->db->query("select setting from sys_setting where key_data='rwi_default_kd_unit'")->row()->setting;
			// }
			
			// $kdPasien='';
			// if ($SchId=="") {
			// 	$kdPasien=$Params["NoMedrec"];
			// } else {
			// 	$kdPasien=$SchId;
			// }
			
			// $asal_pasien=$this->db->query("select kd_asal from asal_pasien where kd_unit='$cari_kd_unit'")->row()->kd_asal;
   //          $data = array("kd_pasien"=>$kdPasien,"kd_unit"=>$Params["Poli"],"tgl_masuk"=>$tmptglkunjungpasien,"urut_masuk"=>
			// 			  $antrian,"jam_masuk"=>$tmpjamkunjungpasien,"kd_rujukan"=>$Params["KdRujukan"],
			// 			  "cara_penerimaan"=>$Params["CaraPenerimaan"],"asal_pasien"=>$asal_pasien,"kd_dokter"=>$Params["KdDokter"],
			// 			  "baru"=>$Params["Baru"],"kd_customer"=>$kode_customer,"shift"=>$Shiftbagian,"karyawan"=>$Params["Karyawan"],
			// 			  "kontrol"=>$Params["Kontrol"],"antrian"=>$Params["Antrian"],"no_surat"=>$Params["NoSurat"],"alergi"=>$Params["Alergi"],
			// 			  "anamnese"=>$Params["Anamnese"]);
			// $AppId   = $Params["NoMedrec"];
   //          $Schunit = $Params["Poli"];
   //          $Schtgl  = date('Y-m-d');
   //          $Schurut = $antrian;
			// $this->load->model("rawat_jalan/tb_kunjungan_pasien");
			// /*  $criteria = "kd_pasien = '".$AppId."' AND kd_unit = '".$Schunit."' AND tgl_masuk = '".$Schtgl."'";

   //          $this->load->model("rawat_jalan/tb_kunjungan_pasien");
   //          $this->tb_kunjungan_pasien->db->where($criteria, null, false); */
   //          $query =_QMS_QUERY("select * from nginap where kd_pasien='".$AppId."' and tgl_keluar =''")->result();
   //          if (count($query)==0)
   //          { 
			// 	//$data["kd_pasien"] = $kdPasien;
			// 	$result = $this->tb_kunjungan_pasien->Save($data);
			// 	if($result)
			// 	{
			// 		if ($Params["NAMA_PJ"]==="" && $Params["KTP"]==="") {
			// 		  $strError = $this->SimpanNginap($Params, $tmptglkunjungpasien, $tmptglkunjungpasien,$antrian,$kdPasien);	
			// 		}
			// 		 else {
			// 			if ($Params["kdpekerjaanPj"]== '')
			// 			{
			// 				$kdpekerjaan=0;
			// 			}
			// 			else
			// 			{
			// 				$kdpekerjaan=$Params["kdpekerjaanPj"];
			// 			}
			// 			if ($Params["HubunganPj"]== '')
			// 			{
			// 				$HubunganPj=0;
			// 			}
			// 			else
			// 			{
			// 				$HubunganPj=$Params["HubunganPj"];
			// 			}
			// 			if ($Params["AlamatKtpPJ"]== '')
			// 			{
			// 				$AlamatKtpPJ=0;
			// 			}
			// 			else
			// 			{
			// 				$AlamatKtpPJ=$Params["AlamatKtpPJ"];
			// 			}
			// 			if ($Params["KTP"]== '')
			// 			{
			// 				$KTP=0;
			// 			}
			// 			else
			// 			{
			// 				$KTP=$Params["KTP"];
			// 			}
			// 			if ($Params["KdPendidikanPj"]== '')
			// 			{
			// 				$KdPendidikanPj=0;
			// 			}
			// 			else
			// 			{
			// 				$KdPendidikanPj=$Params["KdPendidikanPj"];
			// 			}
			// 			if ($Params["EmailPenanggungjawab"]== '')
			// 			{
			// 				$EmailPenanggungjawab=0;
			// 			}
			// 			else
			// 			{
			// 				$EmailPenanggungjawab=$Params["EmailPenanggungjawab"];
			// 			}
			// 			if ($Params["KelurahanKtpPJ"]== '')
			// 			{
			// 				$KelurahanKtpPJ=0;
			// 			}
			// 			else
			// 			{
			// 				$KelurahanKtpPJ=$Params["KelurahanKtpPJ"];
			// 			}
			// 			$result=$this->db->query("INSERT INTO penanggung_jawab
			// 										(kd_pasien,kd_unit,tgl_masuk,urut_masuk,kd_kelurahan,kd_pekerjaan,
			// 										nama_pj ,alamat ,telepon ,kd_pos,hubungan,alamat_ktp,no_hp,
			// 										kd_pos_ktp ,no_ktp,kd_pendidikan,email,tempat_lahir,tgl_lahir,status_marital,
			// 										kd_agama,kd_kelurahan_ktp,wni,jenis_kelamin
			// 										) values
			// 										(
			// 										'".$kdPasien."',
			// 										'".$Params["Poli"]."',
			// 										'".$tmptglkunjungpasien."',
			// 										".$antrian.",
			// 										'".$Params["KelurahanPJ"]."',
			// 										".$kdpekerjaan."
			// 										,'".$Params["NAMA_PJ"]."','".$Params["AlamatPJ"]."','".$Params["TeleponPj"]."',
			// 										'".$Params["PosPJ"]."','".$HubunganPj."','".$AlamatKtpPJ."',
			// 										'".$Params["HpPj"]."','".$Params["KdPosKtp"]."','".$KTP."',
			// 										'".$KdPendidikanPj."','".$EmailPenanggungjawab."'
			// 										,'".$Params["tempatlahirPenanggungjawab"]."','".$Params["TgllahirPJ"]."',
			// 										'".$Params["StatusMaritalPenanggungjawab"]."',1,".$KelurahanKtpPJ.",
			// 										'".$Params["WNIPJ"]."','".$Params["JKpenanggungJwab"]."'
			// 									 ) ");	
			// 			if($result) {											
			// 				$strError = $this->SimpanNginap($Params, $tmptglkunjungpasien, $tmptglkunjungpasien,$antrian,$kdPasien);
			// 			}else {
			// 				$strError = "eror";
			// 			}
			// 		}  
			// 	}else{
			// 		$strError = "eror";
			// 	} 
			// } else{
   //              $strError = "erorcari";
			// }
   //          return $strError;
   //      }
        
   //      public function SimpanNginap($Params, $tmptglkunjungpasien, $tmptglinapasien,$Schurut,$kdPasien)
   //      {
   //          $strError = "";
   //          date_default_timezone_set("Asia/Jakarta");
   //          $tmptglhariini =  "1900-01-01".gmdate(" H:i:s", time()+60*60*7);//date('Y-m-d H:i:s');
			// if ($Params["spesialisasi"]=='')
			// {
			// 	$spesialisasi=0;
			// }
			// else
			// {
			// 	$spesialisasi=$Params["spesialisasi"];
			// }
   //          $data = array(
   //                          "kd_unit_kamar"=>$Params["KDUnitKamar"],
   //                          "no_kamar"=>$Params['Kamar'],
   //                          "kd_pasien"=>$kdPasien,
   //                          "kd_unit"=>$Params["Poli"],
   //                          "tgl_masuk"=>$tmptglkunjungpasien,
   //                          "urut_masuk"=>$Schurut,
   //                          "tgl_inap"=>$tmptglinapasien,
   //                          "jam_inap"=>$tmptglhariini,
   //                          "bed"=>"",
   //                          "kd_spesial"=>$spesialisasi,
   //                          "akhir"=>"t",
   //                          "urut_nginap"=>1
   //                      );

   //              $this->load->model("rawat_inap/tb_nginap");
   //              $result = $this->tb_nginap->Save($data);
			// 	if ($result)
			// 	{
			// 		$strError = "sukses";
			// 	}
			// 	else
				
			// 	{
			// 		echo 'hehe';
			// 	}
				
                
   //          return $strError;
            
   //      }
        
      

        public function GetKdLurah($kd_Kec)
        {
            $intKdKec = $kd_Kec;
            $intKdLurah;
            
            $criteria = "where kec.kd_kecamatan=".$intKdKec." And (Kelurahan='DEFAULT' or Kelurahan='.' OR Kelurahan is null)";
            $this->load->model("rawat_jalan/tb_getkelurahan");
            $this->tb_getkelurahan->db->where($criteria, null, false);
            $query = $this->tb_getkelurahan->GetRowList( 0,1, "", "","");
          
            
            if ($query[1]!=0)
            {
                if($query[0][0]->KD_KELURAHAN == '')
                {
                    $tmp_kdlurah = $this->GetLastKd_daerah(1);
                    $this->load->model("general/tb_kelurahan");
                    $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                    $result = $this->tb_kelurahan->Save($data);
                    $strError = $tmp_kdlurah;
                }else
                    {
                        $strError = $query[0][0]->KD_KELURAHAN;
                    }
            }
             else
                {
                if ($intKdKec == "")
                    {
                        $intKdKec = $this->GetLastKd_daerah(2);
                        $this->load->model("general/tbl_kecamatan");
                        $data = array("kd_kabupaten"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kecamatan"=>"DEFAULT");
                        $result = $this->tbl_kecamatan->Save($data);
                        $strError = $intKdKec;
                    }
                    else
                        {
                            $intKdKec = $kd_Kec;
                        }
                     $tmp_kdlurah = $this->GetLastKd_daerah(1);
                     $this->load->model("general/tb_kelurahan");
                     $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                     $result = $this->tb_kelurahan->Save($data);
                     $strError = $tmp_kdlurah;
                }        
            return $strError;
        }

        public function GetLastKd_daerah($LevelLokasi)
        {
            if ($LevelLokasi == "1") {
				$this->load->model('general/tb_case1');
				$res = $this->tb_case1->GetRowList(0,1, "", "","");
				if ($res[1]>0)
				{
					$nm = $res[0][0]->KODE;
					$nomor = (int) $nm +1;
				}
            } else if($LevelLokasi == "2") {
				$this->load->model('general/tb_case2');
				$res = $this->tb_case2->GetRowList(0,1, "", "","");
				if ($res[1]>0)
				{
					$nm = $res[0][0]->KODE;
					$nomor = (int) $nm +1;
				}
			} else if($LevelLokasi == "3") {
				$this->load->model('general/tb_case3');
				$res = $this->tb_case3->GetRowList(0,1, "", "","");
				if ($res[1]>0)
				{
					$nm = $res[0][0]->KODE;
					$nomor = (int) $nm +1;
				}
			} else if($LevelLokasi == "4") {
				$this->load->model('general/tb_case4');
				$res = $this->tb_case4->GetRowList(0,1, "", "","");
				if ($res[1]>0)
				{
					$nm = $res[0][0]->KODE;
					$nomor = (int) $nm +1;
				}
			}
			return $nomor;
			//echo($nm);
        }

        public function SimpanTransaksi($Params, $SchId, $Schurut, $notrans, $tglmasuk)
        {
           
            $strError = "";
            $strcek = "";
            $strcekdata = "";
            $kd_unit = "";
            $appto = "";
			$kdPasien='';
			$urutnya=0;
			/* if ($Schurut=="") {
				$urutnya=0;
			} else {
				$urutnya=$Schurut;
			} */
			
			if ($SchId=="") {
				$kdPasien=$Params["NoMedrec"];
			} else {
				$kdPasien=$SchId;
			}
			$kduser = $this->session->userdata['user_id']['id'];			
            $kd_kasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
            $notrans = $this->GetIdTransaksi($kd_kasir);
            //$Schurut = $this->GetAntrian($Params["NoMedrec"],$Params["Poli"],date('Y-m-d'));
           
		   $data = array("kd_kasir"=>$kd_kasir,
                          "no_transaksi"=>$notrans,
                          "kd_pasien"=>$kdPasien,
                          "kd_unit"=>$Params["Poli"],
                          "tgl_transaksi"=>$tglmasuk,
                          "urut_masuk"=>$Schurut,
                          "tgl_co"=>NULL,
                          "co_status"=>"False",
                          "orderlist"=>NULL,
                          "ispay"=>"False",
                          "app"=>"False",
                          "kd_user"=>$kduser,
                          "tag"=>NULL,
                          "lunas"=>"False",
						  "posting_transaksi"=>"false",
                          "tgl_lunas"=>NULL
						  );
						  
			  
            
			$Schkasir= $this->db->query("select * from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
            $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$Schkasir."'";

            $this->load->model("general/tb_transaksi");
            $this->tb_transaksi->db->where($criteria, null, false);
            //$query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
			$query = _QMS_QUERY('select * from transaksi where '.$criteria)->result();
            if (count($query)==0)
            {          
                $data["no_transaksi"] = $notrans;
                $result = $this->tb_transaksi->Save($data);
				/* if($result)
				{
					$querygetappto=$this->db->query("Select getappto(False,".$Params['Baru'].",".$Params['RadioRujukanPasien'].",False,False,'-1')")->result();
					foreach($querygetappto as $getappto) {
						$hasilgetappto = $getappto->getappto;
					}
					if($hasilgetappto!="") {
						$querygetappto=$this->db->query("Select getappto(False,".$Params['Baru'].",".$Params['RadioRujukanPasien'].",False,False,'-1')")->result();
						foreach($querygetappto as $getappto)
						{
							$hasilgetappto = $getappto->getappto;
						}
						$Shiftbagian= $this->GetShiftBagian();
						$Shiftbagian= (int) $Shiftbagian;	
						$kdtarifcus=$this->db->query("Select getkdtarifcus('".$Params['KdCustomer']."')")->result();
						foreach($kdtarifcus as $getkdtarifcus) {
							$KdTarifpasien = $getkdtarifcus->getkdtarifcus;
						}
						$queryinsertdetailtransaksi=$this->db->query("INSERT INTO detail_transaksi
													(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
													tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
													select  
														'$Schkasir',
														'$notrans',
														row_number() OVER () as rnum,
														'".$tglmasuk."',
														".$kduser.",
														rn.kd_tarif,
														rn.kd_produk,
														rn.kd_unit,
														rn.tglberlaku,
														'true',
														'true',
														''
														,1,
														rn.tarifx,
														". $Shiftbagian .",
														'false',
														''  

														from(
														Select AutoCharge.appto,
														tarif.kd_tarif,
														AutoCharge.kd_produk,
														AutoCharge.kd_unit,
														max (tarif.tgl_berlaku) as tglberlaku,
														tarif.tarif as tarifx,
														row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
														From AutoCharge inner join tarif on 
														tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
														inner join produk on produk.kd_produk = tarif.kd_produk  
														Where 
														left(AutoCharge.kd_unit,3)='".substr($Params["Poli"],0,3)."' 
														and AutoCharge.appto in $hasilgetappto
														and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
														and tgl_berlaku <= gettanggalberlakufromklas('$KdTarifpasien','".date('Y-m-d')."','".date('Y-m-d')."','71')
														group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku order by AutoCharge.kd_produk asc
														) as rn
														where rn = 1 ");
													
						$gettglberlaku=$this->db->query("Select gettanggalberlakufromklas('$KdTarifpasien','".date('Y-m-d')."','".date('Y-m-d')."','71')")->result();
						foreach($gettglberlaku as $gettanggalberlakufromklas) {
							$Tglberlaku_pasien = $gettanggalberlakufromklas->gettanggalberlakufromklas;
						}
							
						$query = $this->db->query("
								Select row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
							
								tarif.kd_tarif,
								AutoCharge.kd_produk,
								AutoCharge.kd_unit,
								max (tarif.tgl_berlaku) as tglberlaku,
								max(tarif.tarif) as tarifx
								From AutoCharge inner join tarif on 
								tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
								inner join produk on produk.kd_produk = tarif.kd_produk  
								Where left(AutoCharge.kd_unit,3)='".$Params["Poli"]."' 
								and AutoCharge.appto in $hasilgetappto
								and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
								and tgl_berlaku <= '$Tglberlaku_pasien'
								group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
								tarif.kd_tarif order by AutoCharge.kd_produk asc
						
						");
						if ($query->num_rows() > 0) {
							foreach ($query->result() as $row) {
								$kdprodukpasien=$row->kd_produk;
								$kdTarifpasien=$row->kd_tarif;
								$tglberlakupasien=$row->tglberlaku;
								$kd_unitpasein=$row->kd_unit;
								$urutpasein=$row->rnum;
								$query = $this->db->query("INSERT INTO Detail_Component 
												(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
												Select '$Schkasir','$notrans',$urutpasein,'".$tglmasuk."',kd_component, 
												tarif as FieldResult,0
												From Tarif_Component 
												Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' 
												And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien");
							}
						}

					}
						
				} */
				
                $strError = $this->SimpanPasieninap($Params, $kd_kasir, $notrans,$Params["KDUnitKamar"],$Params['Kamar'],$Params["spesialisasi"]);
                
            }
            if($Params['BayiBrLahir'] === 'True') {
                $strError = $this->SimpanPasienPerinatal($Params, $Params["Poli"],$tglmasuk,$kdPasien,$Schurut)." ".$notrans;
            }else
            {
                $strError = "sukses ".$notrans;
            }
           

            return $strError;

        }
        
        Private function SimpanPasieninap($Params, $kdkasir, $notrans, $kdunit, $nokamar, $kdspesial)
        {
            
            try
			{
				if ($kdspesial=='') {
					$kdspesialfix=0;
				} else {
					$kdspesialfix=$kdspesial;
				}
				$data = array("kd_kasir"=>$kdkasir,
							  "no_transaksi"=>$notrans,
							  "kd_unit"=>$kdunit,
							  "no_kamar"=>$nokamar,
							  "kd_spesial"=>$kdspesialfix);                        
				$this->load->model("rawat_inap/tb_pasieninap");
				$result = $this->tb_pasieninap->Save($data);
				$nik_pasien=$Params["part_number_nik"];
				$kd_pasien=$Params["NoMedrec"];
				$update_pasien="update pasien set part_number_nik='$nik_pasien' where kd_pasien='$kd_pasien'";
				$error = "sukses";   
			} catch(Exception $o) {
				echo 'Debug  fail ';
			}
            return $error;
        }
        
        private function SimpanPasienPerinatal($Params, $unit, $tgl,$kdPasien,$Schurut)
        {
			$noMedrec='';
			if ($Params["NoMedrec"]=='Automatic from the system...' || $Params["NoMedrec"]=='') {
				$noMedrec=$kdPasien;
			} else {
				$noMedrec=$Params["NoMedrec"];
			}
            // $Schurut = $this->GetAntrian($Params["NoMedrec"],$Params["Poli"],$tgl);
            try {
				$data = array("kd_pasien"=>$noMedrec,
                                      "kd_unit"=>$unit,
                                      "tgl_masuk"=>$tgl,
                                      "urut_masuk"=>$Schurut,
                                     );                        
				$this->load->model("rawat_inap/tb_pasienperinatal");
				$result = $this->tb_pasienperinatal->Save($data);
				
				$error = "sukses";   
			}  catch(Exception $o) {
				echo 'Debug  fail ';
			}
            return $error;
        }

        private function GetAntrian($medrec,$Poli,$Tgl)
        {
            $retVal = 0;
            $Dokter = '';
            /* $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  ""); */
			$res= _QMS_QUERY("select urut_masuk from kunjungan where kd_pasien = '" . $medrec . "' and kd_unit = '" . $Poli . "' and tgl_masuk = '" . $Tgl . "'and kd_dokter = '" . $Dokter . "'", null, false);
            if (count($res->result())>0)
            {
                $nm = $res->row()->URUT_MASUK;
                $retVal=$nm;
            } else{
				$retVal=1;
			}
            return $retVal;
        }

        public function GetIdTransaksi($kd_kasir)
        {
			/*$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$sqlcounterkasir = $db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$sqlsqlcounterkasir2 = $sqlcounterkasir->counter;
			$sqlnotr = $sqlsqlcounterkasir2+1;*/
		
			$counter = _QMS_QUERY("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$no = $counter->counter;
			$retVal2 = $no+1;
			//if($sqlnotr>$retVal2)
//			{
			//$retVal2=$sqlnotr;	
			//}
			$strNomor='';
			$query = "update kasir set counter=$retVal2 where kd_kasir='$kd_kasir'";
			$update = _QMS_QUERY($query);
			
			//$retVal=$retVal.str_pad($retVal,7,"0000000",STR_PAD_LEFT);
			$retVal=$strNomor.str_pad($retVal2,7,"000000",STR_PAD_LEFT);
			return $retVal;
        }

        private function GetUrutKunjungan($medrec, $unit, $tanggal)
        {
            $retVal = 1;
            if($medrec === "Automatic from the system..." || $medrec === " ")
            {
                $tmpmedrec = " ";
            }else {
                $tmpmedrec = $medrec;
            }
			/* $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");  */
			$res =_QMS_QUERY("select top 1 urut_masuk from kunjungan where kd_pasien = '" . $tmpmedrec . "' and kd_unit = '" . $unit . "' and tgl_masuk = '" . $tanggal . "' order by urut_masuk desc", null, false);
            if (count($res->result())>0)
            {
                $nm = $res->row()->urut_masuk;
                $nomor = (int) $nm +1;
                $retVal=$nomor;
            }
            return $retVal;
        }

        public function CekdataAutoCharge($kd_unit, $app, $pasien, $rujukan, $kontrol, $cetak, $jenisrujuk)
        {
            $Params = "";
            $strcek = $this->readcekautocharge($app, $pasien, $rujukan, $kontrol, $cetak, $jenisrujuk);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";
                       
                try
		{
			$this->load->model('general/autocharge');
                        $this->autocharge->db->where('left(kd_unit,3)='.$kd_unit.'and AutoCharge.appto in'.$appto.'');
                        $res = $this->autocharge->GetRowList($app, $pasien);
                        if ($res[1]>0)
                            {
                                $nm = $res;
                            }else
                                {
                                $nm = "";
                            }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }
		
		
		private function GetShiftBagian()
        {
			
		/*	$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$sqlbagianshift = $db->query("SELECT  CONVERT(VARCHAR(3), shift)AS SHIFTBAG FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row();
			$sqlbagianshift2 = $sqlbagianshift->SHIFTBAG;*/
		
			//$sqlbagianshift2=1;
			$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->shift;
			$lastdate = $this->db->query("SELECT   to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->lastdate;
				$datnow= date('Y-m-d');
			if($lastdate<>$datnow && $sqlbagianshift==='3')
			{
			$sqlbagianshift2 = '4';
			}else{
			$sqlbagianshift2 = $sqlbagianshift;
			}
			
        return $sqlbagianshift2;
        }

 function readcekautocharge($app, $pasien, $rujukan, $kontrol, $cetak, $jenisrujuk)
	{
            try
		{
			$strQuery = "Select GetAppTo"."(".$app.",".$pasien.",".$rujukan.",".$kontrol.",".$cetak.","."'".$jenisrujuk."'".")";
                        echo($strQuery);
                        $query = $this->db->query($strQuery);
                        $res = $query->result();
                        if ($query->num_rows() > 0)
                        {
                            foreach($res as $data)
                            {
                            $curentshift = $data->getappto;
                            }
                        }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $curentshift;
	}
	
	function regonline_simpanpasien($Params){
		$strError = "";
		$pasien=$this->db->query("SELECT nama, alamat FROM pasien WHERE kd_pasien='".$Params["NoMedrec"]."'");
		if(count($pasien->result()) != 0){
			$nama=$pasien->row()->nama;
			$alamat=$pasien->row()->alamat;
		}
		
		$ruangan=$this->db->query("select nama_kamar from kamar where no_kamar='".$Params["Kamar"]."' and kd_unit='".$Params["Ruang"]."'")->row()->nama_kamar;
		
		$data = array("kd_pasien"=>$Params["NoMedrec"],
								"nama"=>$nama,
								"alamat"=>$alamat,
								"ruangan"=>$ruangan);
		$save=$this->common->db2()->insert("rs_pasien_rwi",$data);
		
		if($save){
			$strError = "sukses";
		} else{
			$strError = "eror";
		}
		return $strError;
		
	}
	
	function cekdaftar(){
		// echo "sss".$this->setup_db_sql; die;
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// $dbsqlsrv = $this->load->database('otherdb2',TRUE);
			$ceksql= $this->db->query("SELECT top 1 * from kunjungan where left(kd_unit,3)='".substr($_POST['kd_unit'],0,3)."'  and tgl_keluar is null and kd_pasien='".$_POST['kd_pasien']."' order by tgl_masuk desc");
		}else{
			$ceksql= $this->db->query("SELECT * from kunjungan where left(kd_unit,3)='".substr($_POST['kd_unit'],0,3)."'  and tgl_keluar is null and kd_pasien='".$_POST['kd_pasien']."' order by tgl_masuk desc limit 1");
		}
		//echo "select top 1 * from kunjungan where left(kd_unit,3)='".substr($_POST['kd_unit'],0,3)."'  and tgl_keluar is null and kd_pasien='".$_POST['kd_pasien']."' order by tgl_masuk desc";
		if(count($ceksql->result()) > 0){
			echo "{success:false,tgl:'".date('d-m-Y',strtotime($ceksql->row()->TGL_MASUK))."'}";
		} else{
			echo "{success:true}";
		}
		
	}
}

?>




			