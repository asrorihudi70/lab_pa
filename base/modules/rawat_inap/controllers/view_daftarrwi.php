<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class view_daftarrwi extends MX_Controller
{

    
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


	public function index()
	{
        $this->load->view('main/index');
        }


	function read($Params=null)
	{
		try
		{
                    $this->load->model('rawat_inap/tb_viewrwi');
                       if (strlen($Params[4])!==0)
                    {
                       $this->db->where(str_replace("~", "'", $Params[4]) ,null, false);
                    }
                    $res = $this->tb_viewrwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                }
                catch(Exception $o)
		{
			echo 'Debug  fail ';

		}

	 	echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}
   
        public function save($Params=null)
        {
            $mError = "";
            $AppId = "";
            $SchId = "";
            $Schunit = "";
            $Schtgl = "";
            $Schurut = "";
            $notrans = "";
            $mError = $this->SimpanPasien($Params, $SchId);
           //echo '"'.$mError.'"';
           if ($mError == "ada")
               {
                  $mError = $this->SimpanKunjungan($Params, $SchId, $Schunit, $Schtgl, $Schurut);
               }
               if ($mError=="ada")
                    {
                        $mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans));
                     }
                     if ($mError[0]=="sae")
                            {
                            echo '{success: true, KD_PASIEN: "'.$SchId.'",NoTrans: "' . $mError[1] . '"}';
                            }
                     else echo '{success: false}';
        }

        public function SimpanPasien($Params, &$AppId="")
        {
            $strError = "";
            $suku = 0;
            if($Params["KDPROPINSI"] === "")
            {
                $tmppropinsi = $Params["Kelurahan"];
                $tmpkecamatan = $Params["Kd_Kecamatan"];
                $tmpkabupaten = $Params["AsalPasien"];
                $tmppendidikan = $Params["Pendidikan"];
                $tmppekerjaan = $Params["Pekerjaan"];
                $tmpagama = $Params["Agama"];
                
            }else{
                $tmppropinsi = $Params["KDPROPINSI"];
                $tmpkecamatan = $Params["KDKABUPATEN"];
                $tmpkabupaten = $Params["KDKECAMATAN"];
                $tmppendidikan = $Params["KDPENDIDIKAN"];
                $tmppekerjaan = $Params["KDPEKERJAAN"];
                $tmpagama = $Params["KDAGAMA"];
                }
                      
            
            $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
            $perusahaan = "";
            if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan')
                {
                    $kode_asuransi = NULL;
                }
                else
                {
                    $kode_asuransi = $Params["KD_Asuransi"];
                }
                if ($Params["NoAskes"] == "")
                    {
                    $tmpno_asuransi = $Params["NoSjp"];
                    }
                else
                    {
                    $tmpno_asuransi = $Params["NoAskes"];
                    }
                $data = array("kd_pasien"=>$Params["NoMedrec"],"nama"=>$Params["NamaPasien"],
                          "nama_keluarga"=>$Params["NamaKeluarga"],"jenis_kelamin"=>$Params["JenisKelamin"],
                          "tempat_lahir"=>$Params["Tempatlahir"],"tgl_lahir"=>$Params["TglLahir"],
                          "kd_agama"=>$tmpagama,"gol_darah"=>$Params["GolDarah"],
                          "status_marita"=>$Params["StatusMarita"],"wni"=>$Params["StatusWarga"],
                          "alamat"=>$Params["Alamat"],"telepon"=>$Params["No_Tlp"],
                          "kd_kelurahan"=>$intkd_lurah,"kd_pendidikan"=>$tmppendidikan,
                          "kd_pekerjaan"=>$tmppekerjaan,"pemegang_asuransi"=>$Params["NamaPeserta"],
                          "no_asuransi"=>$tmpno_asuransi,"kd_asuransi"=>$kode_asuransi,
                          "kd_suku"=>$suku,"jabatan"=>$Params["Jabatan"],
                          "kd_perusahaan"=>0);
            if ($Params["NoMedrec"] == "" || $Params["NoMedrec"] == "Automatic from the system ...")
                {

                  $AppId = $this->GetIdRWJ();
            } else {$AppId = $Params["NoMedrec"];}

            $criteria = "kd_pasien = '".$AppId."'";

            $this->load->model("rawat_jalan/tb_pasien");
            $this->tb_pasien->db->where($criteria, null, false);
            $query = $this->tb_pasien->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {

            $data["kd_pasien"] = $AppId;
            $result = $this->tb_pasien->Save($data);
            $strError = "ada";
            }else{
            $this->tb_pasien->db->where($criteria, null, false);
            $dataUpdate = array("nama"=>$Params["NamaPasien"]);
            $result = $this->tb_pasien->Update($data);
            $strError = "ada";
            }
            return $strError;
        }

        public function GetKdLurah($kd_Kec)
        {
            //echo($kd_Kec);
            $intKdKec = $kd_Kec;
            $intKdLurah;
            
            $criteria = "where kec.kd_kecamatan=".$intKdKec." And (Kelurahan='DEFAULT' or Kelurahan='.' OR Kelurahan is null)";
            $this->load->model("rawat_jalan/tb_getkelurahan");
            $this->tb_getkelurahan->db->where($criteria, null, false);
            $query = $this->tb_getkelurahan->GetRowList( 0,1, "", "","");
            //print_r($query);
            
            if ($query[1]!=0)
            {
                if($query[0][0]->KD_KELURAHAN == '')
                {
                    $tmp_kdlurah = $this->GetLastKd_daerah(1);
                    $this->load->model("general/tb_kelurahan");
                    $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                    $result = $this->tb_kelurahan->Save($data);
                    $strError = $tmp_kdlurah;
                }else
                    {
                        $strError = $query[0][0]->KD_KELURAHAN;
                    }
            }
             else
                {
                if ($intKdKec == "")
                    {
                        $intKdKec = $this->GetLastKd_daerah(2);
                        $this->load->model("general/tbl_kecamatan");
                        $data = array("kd_kabupaten"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kecamatan"=>"DEFAULT");
                        $result = $this->tbl_kecamatan->Save($data);
                        $strError = $intKdKec;
                    }
                    else
                        {
                            $intKdKec = $kd_Kec;
                        }
                     $tmp_kdlurah = $this->GetLastKd_daerah(1);
                     $this->load->model("general/tb_kelurahan");
                     $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                     $result = $this->tb_kelurahan->Save($data);
                     $strError = $tmp_kdlurah;
                }
            return $strError;
        }

        public function GetLastKd_daerah($LevelLokasi)
        {

            if ($LevelLokasi == "1")
            {
                        $this->load->model('general/tb_case1');
                        $res = $this->tb_case1->GetRowList(0,1, "", "","");
                        if ($res[1]>0)
                            {
                                $nm = $res[0][0]->KODE;
                                $nomor = (int) $nm +1;
                            }
            }
            else if($LevelLokasi == "2")
                {
                        $this->load->model('general/tb_case2');
                        $res = $this->tb_case2->GetRowList(0,1, "", "","");
                        if ($res[1]>0)
                            {
                                $nm = $res[0][0]->KODE;
                                $nomor = (int) $nm +1;
                            }
                }
                 else if($LevelLokasi == "3")
                {
                            $this->load->model('general/tb_case3');
                            $res = $this->tb_case3->GetRowList(0,1, "", "","");
                            if ($res[1]>0)
                                {
                                    $nm = $res[0][0]->KODE;
                                    $nomor = (int) $nm +1;
                                }
                }
                 else if($LevelLokasi == "4")
                {
                                $this->load->model('general/tb_case4');
                                $res = $this->tb_case4->GetRowList(0,1, "", "","");
                                if ($res[1]>0)
                                    {
                                        $nm = $res[0][0]->KODE;
                                        $nomor = (int) $nm +1;
                                    }
                }
                return $nomor;
                //echo($nm);
        }

        public function SimpanKunjungan($Params, $SchId)
        {
            if($Params["KDKECAMATAN"] === "")
            {
                $tmppropinsi = $Params["Kelurahan"];
                $tmpkecamatan = $Params["Kd_Kecamatan"];
                $tmpkabupaten = $Params["AsalPasien"];
                $tmppendidikan = $Params["Pendidikan"];
                $tmppekerjaan = $Params["Pekerjaan"];
                $tmpagama = $Params["Agama"];
                
            }else{
                $tmppropinsi = $Params["KDPROPINSI"];
                $tmpkecamatan = $Params["KDKABUPATEN"];
                $tmpkabupaten = $Params["KDKECAMATAN"];
                $tmppendidikan = $Params["KDPENDIDIKAN"];
                $tmppekerjaan = $Params["KDPEKERJAAN"];
                $tmpagama = $Params["KDAGAMA"];
                }
            $antrian = $this->GetUrutKunjungan($Params["Poli"],$Params["TanggalMasuk"],$Params["KdDokter"]);
//            echo($antrian);
            if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan')
                {
                    $kode_customer = '0000000001';
                }
                else
                {
                    $kode_customer = $Params["KdCustomer"];
                }
            $Params["NoMedrec"] = $SchId;
            $strError = "";
            $data = array("kd_pasien"=>$Params["NoMedrec"],"kd_unit"=>$Params["Poli"],"tgl_masuk"=>$Params["TanggalMasuk"],"urut_masuk"=>$antrian,"jam_masuk"=>$Params["JamKunjungan"],
                          "cara_penerimaan"=>$Params["CaraPenerimaan"],"kd_rujukan"=>$Params["KdRujukan"],"asal_pasien"=>$tmpkabupaten,"kd_dokter"=>$Params["KdDokter"],
                          "baru"=>$Params["Baru"],"kd_customer"=>$kode_customer,"shift"=>$Params["Shift"],"karyawan"=>$Params["Karyawan"],
                          "kontrol"=>$Params["Kontrol"],"antrian"=>$Params["Antrian"],"no_surat"=>$Params["NoSurat"],"alergi"=>$Params["Alergi"],
                          "anamnese"=>$Params["Anamnese"],);

            $AppId = $Params["NoMedrec"];
            $Schunit = $Params["Poli"];
            $Schtgl = $Params["TanggalMasuk"];
            $Schurut= $antrian;
            
            $criteria = "kd_pasien = '".$AppId."' AND kd_unit = '".$Schunit."' AND tgl_masuk = '".$Schtgl."' AND urut_masuk = '".$Schurut."'";

            $this->load->model("rawat_jalan/tb_kunjungan_pasien");
            $this->tb_kunjungan_pasien->db->where($criteria, null, false);
            $query = $this->tb_kunjungan_pasien->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {
            $data["kd_pasien"] = $AppId;
            $result = $this->tb_kunjungan_pasien->Save($data);
            $strError = "ada";
            }else{
//            $result = $this->tb_kunjungan_pasien->Update($data);
            $strError = "eror";
            }
            return $strError;
        }

        public function SimpanTransaksi($Params, $SchId, $Schurut, $notrans)
        {
           
            echo($notrans);

            $strError = "";
            $strcek = "";
            $strcekdata = "";
            $kd_unit = "";
            $appto = "";
            $Params["NoMedrec"] = $SchId;
//            $Params["UrutMasuk"] = $Schurut;
            $notrans = $this->GetIdTransaksi();
            //$notrans = $AppId;
            $Schurut = $this->GetAntrian($Params["NoMedrec"],$Params["Poli"],$Params["TanggalMasuk"],$Params["KdDokter"]);
//            echo($Schurut);
            $data = array("kd_kasir"=>"03",
                          "no_transaksi"=>$notrans,
                          "kd_pasien"=>$Params["NoMedrec"],
                          "kd_unit"=>$Params["Poli"],
                          "tgl_transaksi"=>$Params["TanggalMasuk"],
                          "urut_masuk"=>$Schurut,
                          "tgl_co"=>NULL,
                          "co_status"=>0,
                          "orderlist"=>NULL,
                          "is_pay"=>0,
                          "app"=>0,
                          "kd_user"=>$Params["Karyawan"],
                          "tag"=>NULL,
                          "lunas"=>"False",
                          "tgl_lunas"=>NULL,
                          "acc_dr"=>"False");
            
            $Schkasir= "01";
            $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$Schkasir."'";

            $this->load->model("general/tb_transaksi");
            $this->tb_transaksi->db->where($criteria, null, false);
            $query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {          
                $data["no_transaksi"] = $notrans;
				echo $data;
                $result = $this->tb_transaksi->Save($data);
                $strError = "sae ".$notrans;
//                if ($strError == "sae")
//                    {
//                   $cek = $this->readcekautocharge();
//                   echo $cek;
//                        $strQuery = "select getcurrentshift(1)";
//                        $query = $this->db->query($strQuery);
//                        $res = $query->result();
//                        if ($query->num_rows() > 0)
//                        {
//                            foreach($res as $data)
//                            {
//                            $curentshift = $data->getcurrentshift;
//                            }
//                        }
                    }
//                        $strcek = $this->CekdataAutoCharge($kd_unit, $appto);
//                        if ($strcek !== "")
//                        {
//                          $kd_produk = $strcek[0][0]->KD_PRODUK;
//                          $kd_unit = $strcek[0][0]->KD_UNIT;
//
//                          $data = array(
//                              "kd_kasir"=>$Schkasir,
//                              "no_transaksi"=>$AppId,
//                              "urut"=>$Params["UrutMasuk"],
//                              "tgl_transaksi"=>$Params["JamKunjungan"],
//                              "kd_tarif"=>"TU",
//                              "kd_produk"=>$kd_produk,
//                              "kd_unit"=>$kd_unit,
//                              "tgl_berlaku"=>$Params["JamKunjungan"],
//                              "kd_user"=>$Params["Karyawan"],
//                              "shift"=>"1",
//                              "harga"=>25000,
//                              "qty"=>1,
//                              "folio"=>"A",
//                              "tag"=>"False",
//                              "kd_customer"=>"NULL",
//                              "hrg_asli"=>0,
//                              "kd_loket"=>"NULL");
//                            $Scrurut=$Params["UrutMasuk"];
//                            $Scrtgl = $Params["JamKunjungan"];
//                            $Schkasir= "01";
//                            $criteria = "kd_kasir = '".$Schkasir."' and no_transaksi = '".$AppId."' and urut = '".$Scrurut."' and tgl_transaksi = '".$Scrtgl."'";

//                            $this->load->model("general/tb_detailtransaksi");
//                            $this->tb_detailtransaksi->db->where($criteria, null, false);
//                            $query = $this->tb_detailtransaksi->GetRowList( 0,1, "", "","");
//                            if ($query[1]==0)
//                            {
//
//                                $data["no_transaksi"] = $AppId;
//                                $result = $this->tb_detailtransaksi->Save($data);
//                                $strError = "sae";
//                            }
//                        }
//                    }
           
//            }else{
//            $this->tb_transaksi->db->where($criteria, null, false);
//            //$dataUpdate = array("nama"=>$Params["NamaPasien"]);
//            $result = $this->tb_transaksi->Update($data);
//            $strError = "ga";
//            }
            
            return $strError;

        }

        private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }

        private function GetIdRWJ()
        {
            $this->load->model('rawat_jalan/getmedrec');
            $res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien",  "");
           
            if ($res[1]>0)
            {
                $nm = $res[0][0]->KD_PASIEN;
                //penambahan 1 digit
                $nomor = (int) $nm +1;
                //echo("'$nomor'");
                if ($nomor<999999)
                    {
                        $retVal=str_pad($nomor,8,"0",STR_PAD_LEFT);
                    }
                    else
                    {
                        $retVal=str_pad($nomor,7,"0",STR_PAD_LEFT);
                    }
                //memberikan '-' pada setiap baris angka
                $getnewmedrec = substr($retVal, 0,1).'-'.substr($retVal,2,2).'-'.substr($retVal,4,2).'-'.substr($retVal,-2);
            }else
            {
              $strNomor="0-00-".str_pad("00-",2,'0',STR_PAD_LEFT);
              $getnewmedrec=$strNomor."01";
            }
            return $getnewmedrec;
        }

        private function GetIdTransaksi()
        {
            $strNomor="0".str_pad("00",2,'0',STR_PAD_LEFT);
            $retVal=$strNomor."0001";

            $this->load->model('general/tb_transaksi');
            $this->tb_transaksi->db->where("substring(no_transaksi,1,3) = '".$strNomor."'", null, false);
            $res = $this->tb_transaksi->GetRowList( 0, 1, "DESC", "no_transaksi",  "");

            if ($res[1]>0)
            {
                $nm = substr($res[0][0]->NO_TRANSAKSI, -4);
                $nomor = (int) $nm +1;
                $retVal=$strNomor.str_pad($nomor,4,"00000",STR_PAD_LEFT);
            }
        return $retVal;
        }

        private function GetUrutKunjungan($unit, $tanggal,$kddokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_unit = '".$unit."' and tgl_masuk = '".$tanggal."'and kd_dokter = '".$kddokter."'order by urut_masuk desc Limit 1", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $nomor = (int) $nm +1;
                $retVal=$nomor;
            }
            return $retVal;
        }

        public function CekdataAutoCharge($kd_unit, $appto)
        {
            $Params = "";
            $app = "";
            $pasien = "";
            $strcek = $this->readcekautocharge($Params);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";
                       
                try
		{
			$this->load->model('general/autocharge');
                        $this->autocharge->db->where('left(kd_unit,3)='.$kd_unit.'and AutoCharge.appto in'.$appto.'');
                        $res = $this->autocharge->GetRowList($app, $pasien);
                        if ($res[1]>0)
                            {
                                $nm = $res;
                            }else
                                {
                                $nm = "";
                            }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }

        public function CekdataTarif()
        {
            $Params = "";
            $app = "";
            $pasien = "";
            $strcek = $this->readcekautocharge($Params);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";

                try
		{
			

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }

        function readcekautocharge($Params=null)
	{
            $strError = "";
            $app="false";
            $pasien="false";
            $rujukan="false";
            $kontrol="false";
            $kartu="false";
            $jenisrujukan="1";
		try
		{
			$strQuery = "Select GetAppTo"."(".$app.",".$pasien.",".$rujukan.",".$kontrol.",".$kartu.",".$jenisrujukan."::text".")";
                        echo($strQuery);
                        $query = $this->db->query($strQuery);
                        $res = $query->result();
                        if ($query->num_rows() > 0)
                        {
                            foreach($res as $data)
                            {
                            $curentshift = $data->getappto;
                            }
                        }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $curentshift;
	}
}
?>			