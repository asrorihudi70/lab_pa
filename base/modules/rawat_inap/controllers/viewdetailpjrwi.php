<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewdetailpjrwi extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
	public  $id_user;
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();    
        $this->id_user = $this->session->userdata['user_id']['id'];    
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
		date_default_timezone_set("Asia/Jakarta");
        try
        {
		$Status='false';
		$kdbagian=1;
		$kriteria='';
		$response = array();
		//$hari=date('d') -1;
	
			
			/*
			    UPDATE REFRESH DATA 
			    OLEH    : HADAD AL GOJALI
			    TANGGAL : 2017 - 01 - 07
			    DATA TIDAK MERELOAD PADA SAAT MENGGANTI STATUS LUNAS
			*/
			$parent = $this->db->query("SELECT * FROM zusers where kd_user='".$this->id_user."'")->row()->kd_unit;
			$this->load->model('rawat_inap/tblviewkasirrwi');

			// echo $Params[0].'<br>';
			// echo $Params[1].'<br>';
			// echo $Params[3].'<br>'; 
			// echo strtolower($Params[2]).'<br>';
			// echo $Params[4];
			// die;
			if (strlen($Params[4])!==0)
			{
				
					
				//$this->db->where(str_replace("~", "'"," kd_bagian ='".$kdbagian."'  and posting_transaksi='FALSE' ".$Params[4]. "   limit 50 "  ) ,null, false) ;
				$this->db->where(str_replace("~", "'","  
					ng.akhir='1'
					and ng.tgl_keluar is null
					".$Params[4]."   
					ORDER  BY k.tgl_masuk DESC") ,null, false) ;
				// $res = $this->tblviewkasirrwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
				$kriteria = "where ".str_replace("~", "'","  
							ng.akhir='1'
							and ng.tgl_keluar is null
							".$Params[4]."   
							ORDER  BY k.tgl_masuk DESC")."";
				
			}else{
				$this->db->where(str_replace("~", "'", "  
					tgl_transaksi in('".date('Y-m-d')."') AND k.Tgl_Keluar IS NULL
					ORDER BY k.tgl_masuk DESC") ,null, false) ;
				// $res = $this->tblviewkasirrwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
				$kriteria = "where tgl_transaksi in('".date('Y-m-d')."') AND k.Tgl_Keluar IS NULL
							ORDER BY k.tgl_masuk DESC";
			}
			$res = $this->db->query("SELECT 
										p.kd_pasien, 
										p.kd_pasien as kode_pasien, 
										p.nama, t.no_transaksi, 
										t.tgl_transaksi, 
										k.tgl_masuk, 
										k.jam_masuk, 
										k.keadaan_pasien, 
										k.cara_keluar, 
										k.sebabmati as sebab_mati, 
										t.kd_unit, 
										u.nama_unit, 
										i.kd_unit as inap_unit, 
										l.kd_kelas,  
										i.no_kamar, 
										u.parent as parentnya, 
										q.nama_kamar, 
										k.kd_customer, 
										c.customer, 
										tc.Kd_Tarif, 
										t.kd_kasir, 
										k.cat_fisik, 
										k.no_sjp,
										k.anamnese,
										l.kelas, 
										t.urut_masuk, 
										sp.kd_spesial,
										sp.spesialisasi,
										t.co_status, 
										t.posting_transaksi,
										t.lunas as lunas,
										p.alamat, 
										pekerjaan.pekerjaan, p.jenis_kelamin,
										p.tgl_lahir,
										p.No_Asuransi, p.Pemegang_Asuransi, i.kd_spesial, dr.kd_dokter, dr.nama as nama_dokter,
										paytype.deskripsi as cara_bayar,
										pay.uraian as ket_payment,
										paytype.jenis_pay,
										pay.kd_pay
										FROM ((((((((Pasien_Inap i 
										LEFT JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi) 
										INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien)
										INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk)
										INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer)
										INNER JOIN Dokter dr ON k.kd_dokter=dr.kd_dokter) 
										INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit)
										INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas) 
										INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer) 
										LEFT JOIN NGINAP ng ON ng.Kd_Pasien = k.Kd_Pasien AND ng.tgl_masuk = K.Tgl_Masuk AND ng.Kd_Unit = k.Kd_Unit AND ng.Urut_Masuk = k.Urut_Masuk 				
										INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit AND i.No_Kamar = q.No_Kamar 
										INNER JOIN spesialisasi sp ON sp.kd_spesial = i.kd_spesial
										left join pekerjaan on pekerjaan.kd_pekerjaan= p.kd_pekerjaan
										INNER join payment pay on pay.kd_customer = k.kd_customer
										INNER join payment_type paytype on pay.jenis_pay = paytype.jenis_pay $kriteria")->result();
			$i=0;
			foreach($res as $rec){
				/* $response[$i]['KD_PASIEN'] = $rec->kd_pasien; */

				$response[$i]['NAMA_UNIT']         = $rec->nama_unit;
				$response[$i]['NAMA']              = $rec->nama;
				$response[$i]['ALAMAT']            = $rec->alamat;
				$response[$i]['KD_PASIEN']         = $rec->kode_pasien;
				$response[$i]['TANGGAL_TRANSAKSI'] = $rec->tgl_transaksi;
				$response[$i]['TANGGAL_MASUK']     = date_format(date_create($rec->tgl_masuk), 'Y-m-d');
				$response[$i]['JAM_MASUK']     	= date_format(date_create($rec->jam_masuk), 'H:i:s');
				// $response[$i]['NAMA_DOKTER']       = $rec->nama_dokter;
				$response[$i]['NO_TRANSAKSI']      = $rec->no_transaksi;
				$response[$i]['KD_CUSTOMER']       = $rec->kd_customer;
				$response[$i]['CUSTOMER']          = $rec->customer;
				$response[$i]['KD_UNIT']           = $rec->kd_unit;
				$response[$i]['KD_DOKTER']         = $rec->kd_dokter;
				$response[$i]['URUT_MASUK']        = $rec->urut_masuk;
				$response[$i]['NAMA_KAMAR']        = $rec->nama_kamar;
				// $response[$i]['KELAS_RAWAT']        = $rec->kelas_rawat;
				// $response[$i]['PINDAH_KELAS']        = $rec->pindah_kelas;
				//$response[$i]KET_PAYMENT     = $rec->ket_payment;
				//$response[$i]CARA_BAYAR      = $rec->cara_bayar;
				//$response[$i]KD_PAY          = $rec->kd_pay;
				//$response[$i]JENIS_PAY       = $rec->jenis_pay;
				//$response[$i]TYPE_DATA       = $rec->type_data;
				$response[$i]['POSTING_TRANSAKSI'] = $rec->posting_transaksi;
				$response[$i]['CO_STATUS']         = $rec->co_status;
				$response[$i]['PARENTNYA']         = $rec->parentnya;
				$response[$i]['CAT_FISIK']         = $rec->cat_fisik;
				$response[$i]['ANAMNESE']         = $rec->anamnese;
				$response[$i]['LUNAS']             = $rec->lunas;
				$response[$i]['NO_KAMAR']          = $rec->no_kamar;
				// $response[$i]TGL_NGINAP      	= $rec->tgl_inap;
				// $response[$i]['KELAS']             = $rec->namakelas;
				$response[$i]['KD_KELAS']          = $rec->kd_kelas;
				$response[$i]['KD_UNIT_KAMAR']     = $rec->inap_unit;
				// $response[$i]['HAK_KELAS']          = $rec->hak_kelas;
				$response[$i]['NO_SJP']          = $rec->no_sjp;
				$response[$i]['KD_KASIR']          = $rec->kd_kasir;
				$response[$i]['KD_SPESIAL']        = $rec->kd_spesial;
				$response[$i]['SPESIALISASI']      = $rec->spesialisasi;
				$response[$i]['KETERANGAN']        = $rec->spesialisasi."/ ".$rec->kelas."/ ".$rec->no_kamar." [ ".$rec->nama_kamar." ] ";
				$response[$i]['CARA_BAYAR']        = $rec->cara_bayar;
				$response[$i]['KET_PAYMENT']       = $rec->ket_payment;
				$response[$i]['JENIS_PAY']         = $rec->jenis_pay;
				$response[$i]['KD_PAY']         	= $rec->kd_pay;
				$response[$i]['CARA_KELUAR'] 		= $rec->cara_keluar;
				$response[$i]['SEBAB_MATI'] 		= $rec->sebab_mati;
				$response[$i]['KEADAAN_PASIEN'] 	= $rec->keadaan_pasien;
				$response[$i]['PEKERJAAN']         = $rec->pekerjaan;
				$response[$i]['UMUR']              = $this->getAge(date('Y-m-d'), $rec->tgl_lahir);
				if ($rec->jenis_kelamin === true || $rec->jenis_kelamin == 't') {
					$response[$i]['JENIS_KELAMIN'] = 'Laki-laki';
				}else{
					$response[$i]['JENIS_KELAMIN'] = 'Perempuan';
				}
				if($rec->nama_dokter == '0' || $rec->nama_dokter == ''){
					$response[$i]['NAMA_DOKTER']='BELUM DIPILIH';
				} else{
					$response[$i]['NAMA_DOKTER']=$rec->nama_dokter;
				}
				if($rec->co_status == '0'){
					$response[$i]['data_pulang']='false';
				}else{
					$response[$i]['data_pulang']='true';
				}
				
				$i++;
			}
			//  if (strlen($Params[4])!==0)
			// {
				
					
			// 	//$this->db->where(str_replace("~", "'"," kd_bagian ='".$kdbagian."'  and posting_transaksi='FALSE' ".$Params[4]. "   limit 50 "  ) ,null, false) ;
			// 	$this->db->where(str_replace("~", "'","  
			// 		/* k.Tgl_Keluar IS NULL 
					
			// 		(t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in (".$parent."))
			// 		OR i.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in (".$parent.")))
			// 		q.no_kamar in (".$this->get_list_kamar().")
			// 		and  */ ng.akhir='1'
			// 		and ng.tgl_keluar is null
			// 		".$Params[4]."   
			// 		ORDER  BY k.tgl_masuk DESC /* limit ".$Params[0]." */") ,null, false) ;
			// 	$res = $this->tblviewkasirrwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			// }	else{
			// 		 $this->db->where(str_replace("~", "'", "  
			// 		tgl_transaksi in('".date('Y-m-d')."') AND k.Tgl_Keluar IS NULL 
			// 		/* AND (t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in (".$parent."))
			// 		OR i.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in (".$parent."))) 
			// 		q.no_kamar in (".$this->get_list_kamar().") */
			// 		ORDER BY k.tgl_masuk DESC  /* limit ".$Params[0]." */") ,null, false) ;
			// 		 $res = $this->tblviewkasirrwi->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			// 		}
								//echo date('Y-m-'.$hari.' 00:00:00');


        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        // echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
		echo '{success:true, totalrecords:'.count($res).', ListDataObj:'.json_encode($response).'}';


    }
   
	private function getAge($tgl1,$tgl2){
		$jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d'))-strtotime(date_format(date_create($tgl2), 'Y-m-d')))/(60*60*24));
		$ret          = array();
		$ret['YEAR']  = floor($jumHari/365);
		$sisa         = floor($jumHari-($ret['YEAR']*365));
		$ret['MONTH'] = floor($sisa/30);
		$sisa         = floor($sisa-($ret['MONTH']*30));
		$ret['DAY']   = $sisa;
		
		if($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0){
			$ret['DAY'] = 1;
		}
		return $ret;
	}

    private function get_list_kamar(){
    	$response = array();
    	$criteria = array(
    		'kd_user' => $this->session->userdata['user_id']['id'],
    	);

    	$this->db->select("set_kamar");
    	$this->db->from("zusers");
    	$this->db->where($criteria);
    	$query = $this->db->get();
    	return $query->row()->set_kamar;
    }

    public function save($Params=null)
    {
	
	//$ReqId=0;
	$TrKodeTranskasi=$Params['TrKodeTranskasi'];
            $result=$this->simpandetail($Params,$TrKodeTranskasi);
			
           // {
                
            //}
           // else echo '{success: false, pesan: "gagal simpan"}';
	 	 	   	
   }
   
   private function simpandetail($Params=null,$TrKodeTranskasi="")
   {
   		
        
		$ubah=$Params['Ubah'];
		if($ubah==1)
		{
		$Arr['no_transaksi']=$Params['TrKodeTranskasi'];
		$Arr['urut']=$Params['RowReq'];
		$Arr['qty']=$Params['Qty'];
			$this->load->model('rawat_jalan/tblkasirdetailrrjw');
			$this->db->where("no_transaksi = '".$Arr['no_transaksi']."' and urut = '".$Arr['urut']."'  ", null, false);
						$res=$this->tblkasirdetailrrjw->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
                        if ($res>0)
                       {
                            echo '{success: true}';
                       } else echo '{success: false}';

		
		}
		else{
			$List = $Params['List'];
			$JmlField= $Params['JmlField'];
			$JmlList=$Params['JmlList'];
			$TrKodeTranskasi=$Params['TrKodeTranskasi'];
			$kdunit=$Params['KdUnit']; 
			$Shift=$Params['Shift']; 
					$arr[] = $this->GetListDetail($JmlField,$List,$JmlList,$TrKodeTranskasi);

					$retVal="";

					if (count($arr)>0)
					{
						foreach ($arr as $x)
						{
						
							If ($TrKodeTranskasi == "")
								$TrKodeTranskasi = $x['NO_TRANSAKSI'];

							if($x['URUT']==0)
								$x['URUT']=$this->GetUrutRequestDetail($TrKodeTranskasi);

						
							$criteria = "no_transaksi = '".$x['NO_TRANSAKSI']."' AND urut = ".$x['URUT'];
											
							$this->load->model('rawat_jalan/tblkasirdetailrrjw');
							
							$this->tblkasirdetailrrjw->db->where($criteria, null, false);
						   
							$query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");
							$result=0;

						 
							if ($query[1]==0)
							{

							
			 

								$data = array
								(
								
									'no_transaksi'=>$x['NO_TRANSAKSI'],
									'urut'=>$x['URUT'],
									'kd_produk'=>$x['KD_PRODUK'],
									'qty'=>$x['QTY'],
									'tgl_berlaku'=>$x['TGL_BERLAKU'],
									'harga'=>$x['HARGA'],
									'kd_tarif'=>$x['KD_TARIF'],
									'kd_kasir'=>$x['KD_KASIR'],
									'tgl_transaksi'=>$x['TGL_TRANSAKSI'],
									'kd_user'=>$x['KD_USER'],
									'kd_unit'=>$kdunit,
									'charge'=>$x['CHARGE'],
									'adjust'=>$x['ADJUST'],
									'folio'=>$x['FOLIO'],
									'shift'=>$Shift,
									'kd_dokter'=>$x['KD_DOKTER'],
									'kd_unit_tr'=>$x['KD_UNIT_TR'],
									'cito'=>$x['CITO'],
									'js'=>$x['JS'],
									'jp'=>$x['JP'],
									'no_faktur'=>$x['NO_FAKTUR'],
									'flag'=>$x['FLAG'],
									'tag'=>$x['TAG']
								 
									);
								
								
								
								$result = $this->tblkasirdetailrrjw->Save($data);
								if ($result)
								{
								
												$xyz = $this->db->query("
												insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
												select t.kd_kasir,
												t.no_transaksi,
												dt.urut,
												dt.tgl_transaksi,
												tc.kd_component,
												tc.tarif
												from tarif_component tc 
												inner join detail_transaksi dt on 
												tc.kd_tarif = dt.kd_tarif
												inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
												and t.kd_unit = tc.kd_unit
												where tc.kd_produk= '".$x['KD_PRODUK']."' and tc.kd_unit='".$kdunit."'  and tc.kd_tarif='TU' 
												and  tc.tgl_berlaku in ('2014-03-01') 
												and t.kd_kasir = '".$x['KD_KASIR']."' and dt.urut='".$x['URUT']."' and t.no_transaksi = '".$x['NO_TRANSAKSI']."'");
								
										if($xyz )
										{
										
												echo '{success:true}';
												$retVal="0";
										}else{
										echo '{success:false}';
										//throw new exception;
										}
								}
								


							} else {

							   

								$data = array(
									'kd_produk'=>$x['KD_PRODUK'],
									'qty'=>$x['QTY'],
									'tgl_berlaku'=>$x['TGL_BERLAKU'],
									'harga'=>$x['HARGA'],
									'kd_tarif'=>$x['KD_TARIF']);
								$criteria = "no_transaksi = '".$x['NO_TRANSAKSI']."' and urut = ".$x['URUT'];

								$this->tblkasirdetailrrjw->db->where($criteria, null, false);
								$result = $this->tblkasirdetailrrjw->Update($data);

								if ($result==0)
									$retVal="0";
							}	   					
						}
					}

					$this->CekJumlahDetail((int)$JmlList,$TrKodeTranskasi,$arr);

					return $retVal;
		}
   }

   public function delete($Params=null)
   {

		//$hari=date('d') -1;
        $TrKodeTranskasi=$Params['TrKodeTranskasi'];  
		$TrTglTransaksi=$Params['TrTglTransaksi']; 
		$TrKdPasien = $Params['TrKdPasien']; 
		$TrKdNamaPasien = $Params['TrKdNamaPasien']; 
		$TrKdUnit =		 $Params['TrKdUnit']; 
		$TrNamaUnit =	 $Params['TrNamaUnit']; 
		$Uraian =		 $Params['Uraian']; 
		$TrHarga =		 $Params['TrHarga']; 
		$TrKdProduk =	 $Params['TrKdProduk']; 
		$TrTglBatal = 	 date('Y-m-d'.' h:i:s');
		$TrKdKasir = '01';
		$Alasan =		 $Params['AlasanHapus']; 
		
		
		//$TrShiftDel = 2; 
		$TrUserName = $this->session->userdata['user_id']['username']; 
		$TrShiftDel = $this->session->userdata['user_id']['currentshift'];    
        $Hapus=(int)$Params['Hapus'];
		$query = $this->db->query("SELECT InsertHistoryTransaksiDetail(
		'".$TrKdKasir."',
		'".$TrKodeTranskasi."',
		'".$TrTglTransaksi."',
		'".$TrTglBatal."',
		'".$TrKdNamaPasien."',
		'".$TrKdUnit."',
		'".$TrNamaUnit."',
		'".$Uraian."',
		'".$TrUserName."',
		".$TrHarga.",
		'".$Alasan."',
		'$TrShiftDel',
		'".$TrKdProduk."'
		)");
		
		$res = $query->result();
		//var_dump( $res);

		//var_dump($Params);
       	$result=0;
        $flag=0;

       

                $RowReq=$Params['RowReq'];
                
               $criteria = "no_transaksi = '". $TrKodeTranskasi."' AND urut = ".$RowReq;
                $this->load->model('rawat_jalan/tblkasirdetailrrjw');
                $this->tblkasirdetailrrjw->db->where($criteria, null, false);
                $result = $this->tblkasirdetailrrjw->Delete();
			
                    echo '{success: true}';
        
		   	
   }
   
 
   
    private function CekJumlahDetail($jmlRecord, $TrKodeTranskasi, $arr)
    {
        
        $this->load->model('rawat_jalan/tblkasirdetailrrjw');
        //$query = $this->am_request_rawat_jalan_detail->read($TrKodeTranskasi);
        $criteria = "no_transaksi = '".$TrKodeTranskasi."'";
        $this->tblkasirdetailrrjw->db->where($criteria, null, false);
        $query = $this->tblkasirdetailrrjw->GetRowList(0, 1000, "", "", "");

        $ArrList = array();
        $numrow = $query[1];

        //if ($query->num_rows()>0)
        if ($numrow>0)
        {
            //if ($query->num_rows()!=$jmlRecord)
            if ($numrow!=$jmlRecord)
            {
                //if ($jmlRecord<$query->num_rows())
                if ($jmlRecord<$numrow)
                {
                    if (cont($arr)>0)
                    {
                        //foreach($query->result_array() as $y)
                        foreach($query[0] as $y)
                        {
                            $mBol = false;

                            foreach($arr as $z)
                            {
                               
                                if ($y->URUT==$z['URUT'])
                                {
                                    $mBol = true;
                                    break;
                                }
                            }

                            if ($mBol==false)
                                $ArrList[]=$y;

                        }

                        if (count($ArrList)>0)
                            $this->HapusBarisDetail($ArrList);
                    }
                }
            }
        }
    }
	
    private function HapusBarisDetail($arr)
    {
        //$this->load->model('rawat_jalan/am_request_rawat_jalan_detail');        

        $mError="";

        foreach ($arr as $x)
        {
            $this->load->model('rawat_jalan/tblkasirdetailrrjw');
          
            $criteria = "no_transaksi = '". $x['NO_TRANSAKSI']."' AND urut = ".$x['URUT'];
            //$query = $this->am_request_rawat_jalan_detail->readforsure($criteria);
            $this->tblkasirdetailrrjw->db->where($criteria, null, false);
            $query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");

            //if ($query->num_rows()>0)
            if ($query[1]>0)
            {
                
                $result = $this->tblkasirdetailrrjw->Delete();
                if ($result==0)
                {
                        $mError.="";
                } else $mError="Gagal Delete";
            }

        }

        return $mError;

    }

 
	
    private function GetListDetail($JmlField, $List, $JmlList, $TrKodeTranskasi)
    {
	//$tgl=date('d-m-Y');
	//echo $tgl;

        $arrList = $this->splitListDetail($List,$JmlList,$JmlField);

        $arrListField=array();
        $arrListRow=array();
            // echo();
        if (count($arrList)>0)
        {
            foreach ($arrList as $str)
            {
                for ($i=0;$i<$JmlField;$i+=1)
                {
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }
              

                if (count($arrListField)>0)
                {
                    $arrListRow['NO_TRANSAKSI']= $TrKodeTranskasi;
                    if ($arrListField[0]=="" or $arrListField[0] == null)
                    {
                        $arrListRow['URUT']=0;
                    } else $arrListRow['URUT']=$arrListField[0];
					
                    $arrListRow['KD_PRODUK']= $arrListField[1];
                    
					if ($arrListField[3]!="" and $arrListField[3] !="undefined")
                    {
                        list($tgl,$bln,$thn)= explode('/',$arrListField[3],3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $arrListRow['TGL_BERLAKU']=date("Y-m-d",$ctgl);
                    }
                    $arrListRow['QTY']= $arrListField[2];
                    $arrListRow['HARGA']= str_replace(".", "",$arrListField[4]);
                    $arrListRow['KD_TARIF']= 'TU';
					$arrListRow['KD_KASIR']= '01';
					$arrListRow['TGL_TRANSAKSI']= date("Y-m-d");
					$arrListRow['KD_USER']= 0;
					$arrListRow['KD_UNIT']= '202';
					$arrListRow['CHARGE']= 'true';
					$arrListRow['ADJUST']= 'false';
					$arrListRow['FOLIO']= '';
					$arrListRow['SHIFT']= 1;
					$arrListRow['KD_DOKTER']= '';
					$arrListRow['KD_UNIT_TR']= '';
					$arrListRow['CITO']= 0;
					$arrListRow['JS']= 0;
					$arrListRow['JP']= 0;
					$arrListRow['NO_FAKTUR']= '';
					$arrListRow['FLAG']=0;
					$arrListRow['TAG' ]='false';
			
				

                }
            }
        }

        return $arrListRow;
   	
   }

    private function splitListDetail($str, $jmlList, $jmlField)
    {
        $splitList = explode("##[[]]##",$str,$jmlList);

        $arrList=array();

        for ($i=0;$i<$jmlList;$i+=1)
        {
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }

        return $arrList;
    }


   private function GetUrutRequestDetail($TrKodeTranskasi)   
   {
   
        $this->load->model('rawat_jalan/tblkasirdetailrrjw');
        $criteria = "no_transaksi = '".$TrKodeTranskasi."'";
        $this->tblkasirdetailrrjw->db->where($criteria,  null, false);
        $res = $this->tblkasirdetailrrjw->GetRowList( 0, 1, "DESC", "urut", "");

        $retVal =1;

        if ($res[1]>0)
            $retVal = $res[0][0]->URUT+1;



        
        return $retVal;
   }  
   

}

?>