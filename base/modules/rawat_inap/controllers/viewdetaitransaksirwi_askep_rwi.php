<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewdetaitransaksirwi_askep_rwi extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct()
    {
        parent::__construct();  
		if(!isset($this->session->userdata['user_id'])){
			$this->load->library('session');
			$this->session->set_userdata( 'user_id',json_decode($_COOKIE['NCI']));
		}		
		$this->load->model('M_pembayaran');		
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
		// echo 'awdwd';
		date_default_timezone_set("Asia/Jakarta");
		$Status='false';
		$kdbagian=1;
		
		
		$criteria = "";
			$kd_user=$this->session->userdata['user_id']['id'];
			$sql="SELECT kd_unit FROM zusers WHERE kd_user='".$kd_user."'";
			$arrSql=$this->db->query($sql);
			$query='';
			if ($arrSql->num_rows() > 0) {
				$query = "AND  tmp_kd_unit LIKE'1%' ";//and tgl_transaksi in('".date('Y-m-d')."') ";
			}else{
				//$query = " and tgl_transaksi in('".date('Y-m-d')."') ";
			}

			$this->load->model('rawat_inap/tblviewtrrwi_askep');
            if (strlen($Params[4])!==0)
            {
				$this->db->where(str_replace("~", "'",$criteria ." co_status = '".$Status."' and kd_bagian ='".$kdbagian."' ".$Params[4]. " ".$query." order by tgl_transaksi asc, no_urut asc") ,null, false) ;
				$res = $this->tblviewtrrwi_askep->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}else{
				$this->db->where(str_replace("~", "'",$criteria . " posting_transaksi = 'FALSE' and co_status = '".$Status."' ".$query." and kd_bagian =  '".$kdbagian."' and tgl_masuk ='".date('Y-m-d')."' order by tgl_transaksi asc, no_urut asc") ,null, false) ;
				$res = $this->tblviewtrrwi_askep->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
			
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
        $this->db->close();
    }
   
   

}
