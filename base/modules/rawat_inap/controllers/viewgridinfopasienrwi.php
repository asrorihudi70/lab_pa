<?php

/**
 * @author
 * @copyright
 */


class viewgridinfopasienrwi extends MX_Controller {

	private $setup_db_sql  = false;
    public function __construct(){
		/*$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}*/
        parent::__construct();

    }

    public function index(){
        $this->load->view('main/index');
		$this->load->library('common');
    }
	
	/* 
		Edit by : M
		Tgl		: 16-02-2017
		Ket		: Get list pasien rawat inap, get jumlah dan sisa bed, get unit, get kamar

	*/
    
    public function read($Params=null)
    {
       /*  try
        {
			
		$tgl=date('M-d-Y');
			$this->load->model('rawat_jalan/tblviewkunjunganedit');
                         if (strlen($Params[4])!==0)
                        {
							$this->db->where(str_replace("~", "'",$Params[4]. " limit 50 "  ) ,null, false) ;
							$res = $this->tblviewkunjunganedit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
						}	else{
								 $this->db->where(str_replace("~", "'", "") ,null, false) ;
								 $res = $this->tblviewkunjunganedit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
								}

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
 */
		$traces=array();
    	/* $result= $this->db->query("select  * from (
				select k.kd_pasien,p.nama as nama_pasien,p.alamat,u.nama_unit,k.kd_unit,k.urut_masuk,k.tgl_masuk, d.nama as dokter 
				,sjp.no_sjp,cus.customer,to_char(k.jam_masuk, 'HH24:MI:SS' ) jam,k.tgl_keluar,
				kon.customer
				from kunjungan k
				inner join pasien p on p.kd_pasien=k.kd_pasien 
				left join unit u on k.kd_unit=u.kd_unit 
				left join dokter d on d.kd_dokter=k.kd_dokter
				left join customer cus on cus.kd_customer=k.kd_customer
				left join sjp_kunjungan sjp on k.kd_unit=sjp.kd_unit and sjp.kd_pasien=k.kd_pasien AND sjp.tgl_masuk=k.tgl_masuk 
				and sjp.urut_masuk=k.urut_masuk 
				inner join customer kon on kon.kd_customer=k.kd_customer  order by k.tgl_masuk desc
				
										) as resdata WHERE ".str_replace("~", "'", $Params[4]))->result(); */
		$nama      = "";
		$alamat    = "";
		$tgl_masuk = "";
		$kd_unit   = "";
		$no_kamar  = "";
		$limit     = 100;
		$order_by  = "p.nama, k.nama_kamar";
		$pagging   = $this->input->post('pagging');
		if ($pagging != "") {
			$limit = $pagging;
		}
		$order = $this->input->post('order_by');
		if ($order != "") {
			if (strtolower($order) == "kode pasien") {
				$order_by = " t.kd_pasien";
			}else if(strtolower($order) == "nama pasien") {
				$order_by = " p.nama";
			}else if(strtolower($order) == "jenis kelamin") {
				$order_by = " p.jenis_kelamin";
			}else if(strtolower($order) == "usia") {
				$order_by = " p.tgl_lahir";
			}else if(strtolower($order) == "alamat") {
				$order_by = " p.alamat";
			}else if(strtolower($order) == "tanggal masuk") {
				$order_by = " t.tgl_transaksi";
			}else if(strtolower($order) == "keterangan"){
				$order_by = " keterangan, los";
			}else{
				$order_by = " k.nama_kamar";
			}
		}

		if($_POST['nama'] !=''){
			$nama = " AND upper(p.nama) like upper('%".$_POST['nama']."%')";
		}
		
		if($_POST['alamat'] !=''){
			$alamat = " AND upper(p.alamat) like upper('%".$_POST['alamat']."%')";
		}
		
		if($_POST['tgl_masuk'] !=''){
			$tgl_masuk = " AND t.tgl_transaksi between '".$_POST['tgl_masuk']."' and '".$_POST['tgl_masuk2']."'";
		}
		
		if ($_POST['kd_unit'] != 'SEMUA') {
			if($_POST['kd_unit'] !=''){
				$kd_unit = " AND i.Kd_Unit = '".$_POST['kd_unit']."'";
			}
		}
		
		if ($_POST['no_kamar'] != 'SEMUA') {
			if($_POST['no_kamar'] !=''){
				$no_kamar = " AND i.No_Kamar = '".$_POST['no_kamar']."'";
			}
		}
		
		/*$result = _QMS_query("SELECT TOP $limit i.kd_kasir, i.no_transaksi, t.kd_pasien, p.nama, tgl_masuk=t.tgl_transaksi, i.no_kamar, k.nama_kamar, u.nama_unit,
								(year(getdate())-year(p.tgl_lahir)) AS usia, p.alamat,  p.jenis_kelamin as sex,
								keterangan=CASE WHEN DATEDIFF(day, x.tgl_akhir, GETDATE()) IS NULL THEN 'Belum Ada Detil Transaksi'
											when LTrim(STR(DATEDIFF(day, x.tgl_akhir, GETDATE()))) = 0 then '0hari'
											when LTrim(STR(DATEDIFF(day, x.tgl_akhir, GETDATE()))) = 1 then '1hari'
											when LTrim(STR(DATEDIFF(day, x.tgl_akhir, GETDATE()))) = 2 then '2hari'
											when LTrim(STR(DATEDIFF(day, x.tgl_akhir, GETDATE()))) >= 3 then '>=3hari'
											when GETDATE() < x.tgl_akhir then 'pink' END 
							FROM ((((Pasien_Inap i 
								INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi) 
								INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
								INNER JOIN Kamar k ON i.Kd_unit = k.Kd_unit AND i.No_Kamar = k.No_Kamar) 
								INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit) 
								LEFT JOIN 
									(SELECT d.Kd_Kasir, d.No_Transaksi, Tgl_Akhir=MAX(d.Tgl_Transaksi) 
									FROM Detail_Transaksi d 
									GROUP BY d.Kd_Kasir, d.No_Transaksi) x 
								ON t.Kd_Kasir = x.Kd_kasir AND t.No_Transaksi = x.No_Transaksi 
							WHERE i.Kd_Kasir = '02' 
								".$nama.$alamat.$tgl_masuk.$kd_unit.$no_kamar."
							ORDER BY ".$order_by)->result();*/
								
		$result_postgre = $this->db->query("SELECT  
								i.kd_kasir, 
								i.no_transaksi, 
								t.kd_pasien, 
								p.nama, 
								t.tgl_transaksi as tgl_masuk, 
								i.no_kamar, 
								k.nama_kamar, 
								u.nama_unit,
								((t.tgl_transaksi::date - p.tgl_lahir::date)/365) AS usia, 
								p.alamat, 
								p.jenis_kelamin as sex, 
								case when (now()::date - t.tgl_transaksi::date) is null then 'Belum Ada Detil Transaksi'
									when (now()::date - t.tgl_transaksi::date) = 0 then '0hari'
									when (now()::date - t.tgl_transaksi::date) = 1 then '1hari'
									when (now()::date - t.tgl_transaksi::date) = 2 then '2hari'
									when (now()::date - t.tgl_transaksi::date) >= 3 then '>=3hari'
									/* when (now()::date - x.tgl_akhir::date) < 1 then '1hari' */
								END as keterangan , now()::date - t.tgl_transaksi::date as los
							FROM 
							(
								Pasien_Inap i INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi
									INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien
									INNER JOIN Kamar k ON i.Kd_unit = k.Kd_unit AND i.No_Kamar = k.No_Kamar
									INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit) 
								LEFT JOIN (
									SELECT 
										d.Kd_Kasir, 
										d.No_Transaksi, 
										MAX(d.Tgl_Transaksi) as Tgl_Akhir 
									FROM Detail_Transaksi d 
									where d.kd_kasir = '02'
									GROUP BY d.Kd_Kasir, d.No_Transaksi ORDER BY d.no_transaksi desc limit 2000
								) x ON t.Kd_Kasir = x.Kd_kasir AND t.No_Transaksi = x.No_Transaksi 
							WHERE i.Kd_Kasir = '02' AND t.tgl_co is null
								".$nama.$alamat.$tgl_masuk.$kd_unit.$no_kamar."
							ORDER BY ".$order_by." limit ".$limit)->result();
								
    	/*for($i=0; $i<count($result) ;$i++){
    		$trace=array();
    		$date = new DateTime($result[$i]->tgl_masuk);
    		$trace['TGL_MASUK']=strval($date->format('d/m/Y'));
    		$trace['KD_PASIEN']=$result[$i]->kd_pasien;
    		$trace['NAMA_PASIEN']=$result[$i]->nama;
    		$trace['NAMA_UNIT']=$result[$i]->nama_unit;
    		$trace['NAMA_KAMAR']=$result[$i]->nama_kamar;
    		$trace['ALAMAT']=$result[$i]->alamat;
    		$trace['USIA']=$result[$i]->usia;
    		$trace['KETERANGAN']=$result[$i]->keterangan;
			if($result[$i]->sex == 1){
				$trace['JK']='L';
			} else{
				$trace['JK']='P';
    		}
    		$traces[]=$trace;
    	}	*/	
    	for($i=0; $i<count($result_postgre) ;$i++){
    		$trace=array();
			$date                  = new DateTime($result_postgre[$i]->tgl_masuk);
			$trace['TGL_MASUK']    = strval($date->format('d/m/Y'));
			$trace['KD_PASIEN']    = $result_postgre[$i]->kd_pasien;
			$trace['NO_TRANSAKSI'] = $result_postgre[$i]->no_transaksi;
			$trace['NAMA_PASIEN']  = $result_postgre[$i]->nama;
			$trace['NAMA_UNIT']    = $result_postgre[$i]->nama_unit;
			$trace['NAMA_KAMAR']   = $result_postgre[$i]->nama_kamar;
			$trace['ALAMAT']       = $result_postgre[$i]->alamat;
			$trace['USIA']         = $result_postgre[$i]->usia;
			$trace['KETERANGAN']   = $result_postgre[$i]->keterangan;
			$trace['LOS']		   = $result_postgre[$i]->los;
			$trace['mentah']   = $result_postgre[$i]->sex;
			if($result_postgre[$i]->sex == 't'){
				$trace['JK']='L';
			} else{
				$trace['JK']='P';
    		}
    		$traces[]=$trace;
    	}
    	echo '{success:true, totalrecords:'.count($result_postgre).', ListDataObj:'.json_encode($traces).'}';
    }
	
	public function getbed()
    {
		$kd_unit       = $_POST['kd_unit'];
		$no_kamar      = $_POST['no_kamar'];
		$criteriaUnit  = "";
		$criteriaKamar = "";
		$criteria      = "";
		if (strtolower($kd_unit) == strtolower("semua")  || $kd_unit == "") {
			$criteriaUnit = "";
		}else{
			$criteriaUnit = "kd_unit = '".$kd_unit."'";
		}

		if (strtolower($no_kamar) == strtolower("semua") || $no_kamar == "") {
			$criteriaKamar = "";
		}else{
			if (strlen($criteriaUnit) > 0) {
				$criteriaKamar = " AND no_kamar = '".$no_kamar."'";
			}else{
				$criteriaKamar = " no_kamar = '".$no_kamar."'";
			}
		}

		$criteria = $criteriaUnit."".$criteriaKamar;

		if (strlen($criteria) > 0) {
			$criteria = " WHERE ".$criteria." AND  kd_unit not in('71')";
		}else{
			$criteria = " WHERE kd_unit not in('71') ";
		}
		$result = $this->db->query("SELECT Sum(Jumlah_Bed) As jumlah, Sum(Jumlah_Bed - Digunakan) As sisa FROM Kamar ".$criteria)->row();
								
    	echo '{success:true, jml:'.$result->jumlah.', sisa:'.$result->sisa.'}';
    }
	
	public function getunitkelas()
    {
    	/*
    	$query = "SELECT DISTINCT i.kd_unit, u.nama_unit FROM Pasien_Inap i INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit WHERE u.parent in ('1001','1002','1003') ORDER BY u.Nama_Unit ";
    	*/
    	$query = "SELECT u.kd_unit, u.nama_unit
					FROM unit u
					WHERE u.kd_bagian = '1'
					AND u.kd_unit NOT IN ('0', '1', '1001')
					AND u.aktif = 't'
					ORDER BY u.nama_unit";
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$result = _QMS_query($query)->result();
		}else{
			$result = $this->db->query($query)->result();
		}
		$data                 = array();		 
		$index                = 1;		 
		$data[0]['kd_unit']   = "SEMUA";
		$data[0]['nama_unit'] = "SEMUA";
		foreach ($result as $row) {
			$data[$index]['kd_unit']   = $row->kd_unit;
			$data[$index]['nama_unit'] = $row->nama_unit;
			$index++;
		} 
				 
    	echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
    }
	
	public function getkamar(){
		$params=$_POST['text'];
				$kriteria='';
				if($params=='SEMUA'){
					$kriteria='';
				}
				else{
					$kriteria="AND k.kd_Unit = '".$_POST['text']."'";
				}
		/*
		$result = $this->db-> query ("SELECT DISTINCT i.no_kamar, k.nama_kamar 
			FROM Pasien_Inap i 
			INNER JOIN Kamar k ON i.Kd_Unit = k.Kd_Unit AND i.No_Kamar = k.No_Kamar 
			".$kriteria." ORDER BY k.Nama_Kamar ")->result();
		*/

		$result = $this->db-> query ("SELECT DISTINCT k.no_kamar, k.nama_kamar 
			FROM Kamar k
			WHERE k.kd_unit NOT IN ('71')
			".$kriteria."
			ORDER BY k.Nama_Kamar ")->result();

			$data = array();		 
			$index = 1;		 
		 	$data[0]['no_kamar'] 	= "SEMUA";
		 	$data[0]['nama_kamar'] = "SEMUA";
            foreach ($result as $row) {
				$data[$index]['no_kamar'] = $row->no_kamar;
				$data[$index]['nama_kamar'] = $row->nama_kamar;
				$index++;
			}									
    	echo '{success:true, totalrecords:'.count($data).', listData:'.json_encode($data).'}';
    }
	
	public function cetak(){
		$common     = $this->common;
		$result     = $this->result;
		$title      = 'LAPORAN INFORMASI PASIEN RAWAT INAP';
		$param      = json_decode($_POST['data']);
		 
		$nama       = $param->nama;
		$alamat     = $param->alamat;
		$tgl_masuk  = $param->tgl_masuk;
		$tgl_masuk2 = $param->tgl_masuk2;
		$kd_unit    = $param->kd_unit;
		$no_kamar   = $param->no_kamar;


		$limitSQL = "";
		$limitPG  = "";
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$limitSQL = " TOP 100";
			$limitPG  = " ";
		}else{
			$limitPG  = " LIMIT 100";
			$limitSQL = " ";
		}
		
		
		if($param->kelas == ''){
			$kelas =' - ';
		} else{
			$kelas=$param->kelas;
		}
		
		if($param->nama_kamar == ''){
			$nama_kamar =' - ';
		} else{
			$nama_kamar=$param->nama_kamar;
		}
		
		if($nama !=''){
			$crnama = " AND upper(p.nama) like upper('".$nama."%')";
		}else{
			$crnama = "";
		}
		
		if($alamat !=''){
			$cralamat = " AND upper(p.alamat) like upper('".$alamat."%')";
		}else{
			$cralamat = "";
		}
		
		if($tgl_masuk !=''){
			$crtgl_masuk = " AND t.tgl_transaksi between '".$tgl_masuk."' and '".$tgl_masuk2."'";
		}else{
			$crtgl_masuk = "";
		}
		
		if($kd_unit !=''){
			$crkd_unit = " AND i.Kd_Unit = '".$kd_unit."'";
		}else{
			$crkd_unit = "";
		}
		
		if($no_kamar !=''){
			$crno_kamar = " AND i.No_Kamar = '".$no_kamar."'";
		}else{
			$crno_kamar = "";
		}
		
		$result = $this->db->query("SELECT $limitSQL i.kd_kasir, i.no_transaksi, t.kd_pasien, p.nama, t.tgl_transaksi as tgl_masuk, i.no_kamar, k.nama_kamar, u.nama_unit,
								((t.tgl_transaksi::date - p.tgl_lahir::date)/365) AS usia, p.alamat,  p.jenis_kelamin as sex,
								case when (now()::date - t.tgl_transaksi::date) is null then 'Belum Ada Detil Transaksi'
									when (now()::date - t.tgl_transaksi::date) = 0 then '0hari'
									when (now()::date - t.tgl_transaksi::date) = 1 then '1hari'
									when (now()::date - t.tgl_transaksi::date) = 2 then '2hari'
									when (now()::date - t.tgl_transaksi::date) >= 3 then '>=3hari'
									/*when (x.tgl_akhir::date - now()::date) < 1 then '1hari'*/
								END as ket 
							FROM ((((Pasien_Inap i 
								INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi) 
								INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
								INNER JOIN Kamar k ON i.Kd_unit = k.Kd_unit AND i.No_Kamar = k.No_Kamar) 
								INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit) 
								LEFT JOIN 
									(
										SELECT d.Kd_Kasir, d.No_Transaksi, MAX(d.Tgl_Transaksi) as Tgl_Akhir
										FROM Detail_Transaksi d 
										GROUP BY d.Kd_Kasir, d.No_Transaksi
									) x ON t.Kd_Kasir = x.Kd_kasir AND t.No_Transaksi = x.No_Transaksi 
							WHERE i.Kd_Kasir = '02' 
								".$crnama.$cralamat.$crtgl_masuk.$crkd_unit.$crno_kamar."
							ORDER BY p.nama,k.Nama_Kamar ".$limitPG);
		$query = $result->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>Kamar/Ruang '.$nama_kamar.'<br>
					</tr>
					<tr>
						<th>Kelas '.$kelas.'<br>
					</tr>
					<tr>
						<th>Tanggal '.date('d-M-Y',strtotime($tgl_masuk)).' s/d '.date('d-M-Y',strtotime($tgl_masuk2)).'<br>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="80" align="center">No. Trans.</th>
					<th width="80" align="center">No. Medrec</th>
					<th width="60" align="center">Nama Pasien</th>
					<th width="40" align="center">Tgl. Masuk</th>
					<th width="40" align="center">Kamar/Ruang</th>
					<th width="40" align="center">Kelas</th>
					<th width="40" align="center">Keterangan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
						<tr>
							<th align="left">'.$no.'</th>
							<th align="center">'.$line->no_transaksi.'</th>
							<th align="center">'.$line->kd_pasien.'</th>
							<th align="left">'.$line->nama.'</th>
							<th align="left">'.date('d-M-Y',strtotime($line->tgl_masuk)).'</th>
							<th align="left">'.$line->nama_kamar.'</th>
							<th align="left">'.$line->nama_unit.'</th>
							<th align="center">'.$line->ket.'</th>
						  </tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Pasien Rawat Inap',$html);	
   	}

   	/*public function listKamar(){
		$result = _QMS_query("SELECT nama_kamar, digunakan as keterangan from kamar")->row();					
    	echo '{success:true, nama_kamar:'.$result->nama_kamar.', keterangan:'.$result->keterangan.'}';
    }*/

   	public function listKamar_old(){
   		$query = $this->db->query("SELECT no_kamar, nama_kamar,
			case when (jumlah_bed-digunakan) > 0 then 'Tersedia' else 'Tidak' end as keterangan,
			Sum(jumlah_bed) As jumlah, 
			Sum(jumlah_bed - digunakan) As sisa
			  	from kamar k
			  	where jumlah_bed <> '0' 
			group by no_kamar,nama_kamar, jumlah_bed, digunakan order by nama_kamar asc");
    	echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query->result()).'}';
   	}
	public function listKamar(){
   		$query = $this->db->query("SELECT no_kamar, nama_kamar,
			case when (jumlah_bed-digunakan) > 0 then 'Tersedia' else 'Tidak' end as keterangan,
			Sum(jumlah_bed) As jumlah, 
			Sum(jumlah_bed - digunakan) As sisa
			  	from kamar k
			  	where jumlah_bed <> '0' 
			group by no_kamar,nama_kamar, jumlah_bed, digunakan order by nama_kamar asc")->result();
   		/*UNTUK JUMLAH BED YANG MINUS (-) BY FAI*/
   		 for ($i=0; $i < count($query) ; $i++) { 
   		 	$tmpSisa=$query[$i]->sisa;
   			if($query[$i]->sisa < 0){
   				$tmpSisa=0;
   			}
   			$dat[$i]=array(
				'no_kamar'   => $query[$i]->no_kamar,
				'nama_kamar' => $query[$i]->nama_kamar,
				'keterangan' => $query[$i]->keterangan,
				'jumlah' 	 => $query[$i]->jumlah,
				'sisa'  	 => $tmpSisa
			);
   		 }
   		 /**/
    	echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($dat).'}';
   	}
}

?>