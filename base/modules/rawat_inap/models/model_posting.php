<?php 	
class Model_posting extends Model
{
	private $tbl_produk_charge    = 'produk_charge';
	private $tbl_produk           = 'produk';
	private $tbl_produk_component = 'produk_component';
	private $id_user              = "";
	private $dbSQL                = "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		// $this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		======================================================================================================================
		======================================================== SQL QUERY ===================================================
		======================================================================================================================
	 */
	
	public function getDataComboProduk($criteria){
		$result = false;
		$this->db->select("*, produk.deskripsi as deskripsi, produk.kd_produk as kd_produk, produk_charge.kd_unit as kd_unit ");
		if (isset($criteria)) {
			$this->db->where($criteria);
		}
		$this->db->from($this->tbl_produk);
		$this->db->join($this->tbl_produk_charge, $this->tbl_produk_charge.".kd_produk = ".$this->tbl_produk.".kd_produk", "INNER");
		$this->db->order_by($this->tbl_produk.".deskripsi", "ASC");
		$result = $this->db->get();
		// $this->db->close();
		return $result;
	}
	
	public function getDataProdukComponent(){
		$result = false;
		// $this->db->select("*");
		// $this->db->where(" kd_jenis NOT IN ('3') ");
		// $this->db->from($this->tbl_produk_component);
		// $this->db->order_by("component", "ASC");
		$result = $this->db->query("SELECT * FROM produk_component WHERE kd_jenis not in ('3') order by component ASC");
		// $result = $this->db->get();
		// $this->db->close();
		return $result;
	}

	/*
		======================================================================================================================
		==================================================== END SQL QUERY ===================================================
		======================================================================================================================
	 */

}
?>