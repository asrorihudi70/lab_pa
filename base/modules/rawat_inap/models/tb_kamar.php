<?php

class tb_kamar extends TblBase
{
    function __construct()
    {
        $this->TblName='kamar';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkamar;
        $row->KD_UNIT=$rec->kd_unit;
        $row->NO_KAMAR=$rec->no_kamar;
        $row->NAMA_KAMAR=$rec->nama_kamar;
        $row->JUMLAH_BED=$rec->jumlah_bed;
        $row->DIGUNAKAN=$rec->digunakan;
        
        return $row;
    }
}

class Rowviewkamar
{
    public $KD_UNIT;
    public $NO_KAMAR;
    public $NAMA_KAMAR;
    public $JUMLAH_BED;
    public $DIGUNAKAN;
}

?>
