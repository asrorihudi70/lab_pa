<?php

class tbl_dokterspesial_02 extends TblBase
{
    function __construct()
    {
        $this->TblName='viewDokterSpesial';
        TblBase::TblBase(true);

        $this->SqlQuery= "select dokter.kd_dokter, dokter.nama from dokter inner join dokter_spesial on dokter.kd_Dokter=dokter_Spesial.kd_Dokter ";
        }

    function FillRow($rec)
    {
        $row=new Rowviewdokterspesial;

          $row->KD_DOKTER=$rec->kd_dokter;
          $row->NAMA=$rec->nama;
        return $row;
    }

}

class Rowviewdokterspesial
{
          public $KD_DOKTER;
          public $NAMA;
}

?>
