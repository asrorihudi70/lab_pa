<?php

class tbl_kelasselisih extends TblBase
{
    function __construct()
    {
        $this->TblName = 'viewsetupkelasselisih';
        TblBase::TblBase(true);

        $this->SqlQuery = "select sk.kd_kelas, kelas
        -- ,u.kd_unit 
            from spc_kelas sk
            inner join kelas k ON k.kd_kelas = sk.kd_kelas
        -- 	INNER JOIN unit u ON u.kd_kelas = sk.kd_kelas ";
    }

    function FillRow($rec)
    {
        $row = new Rowviewsetupkelasselisih;

        $row->KD_KELAS = $rec->kd_kelas;
        $row->KELAS = $rec->kelas;
        return $row;
    }
}

class Rowviewsetupkelasselisih
{
    public $KD_KELAS;
    public $KELAS;
}
