﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewkasirrwi extends TblBase
{
	function __construct()
	{
		$this->StrSql="customer,nama_kamar,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,
		kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,lunas,co_status,
		kd_kelas,kd_unit_kamar,no_kamar,tgl_inap,namakelas";
	
$this->SqlQuery = "select * from ( select customer.customer,k.kd_kelas,
k.kelas as namakelas,unit.nama_unit,datainap.kd_unit,datainap.no_kamar, 
kamar.nama_kamar,datainap.kd_unit as unitKamar,unit.kd_bagian,nginap.kd_unit_kamar,
nginap.tgl_inap,pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, 
transaksi.tgl_transaksi, dokter.nama as nama_dokter,transaksi.lunas, kunjungan.*,
transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,dokter.kd_dokter,
transaksi.orderlist,transaksi.posting_transaksi, knt.jenis_cust from 
(( kunjungan inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien)
left join dokter on dokter.kd_dokter=kunjungan.kd_dokter) inner join customer on customer.kd_customer= kunjungan.kd_customer
left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer  
inner join transaksi on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
and transaksi.tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk
inner join pasien_inap as datainap on transaksi.no_transaksi=datainap.no_transaksi AND transaksi.kd_kasir = datainap.kd_kasir 
inner join unit on datainap.kd_unit=unit.kd_unit 
inner join nginap on kunjungan.kd_pasien=nginap.kd_pasien and kunjungan.kd_unit=nginap.kd_unit and kunjungan.tgl_masuk=nginap.tgl_masuk 
and kunjungan.urut_masuk=nginap.urut_masuk and nginap.akhir=true
inner join kamar on kamar.no_kamar=nginap.no_kamar and  kamar.kd_unit=nginap.kd_unit_kamar
inner join kelas k ON k.kd_kelas = unit.kd_kelas   ) as resdata";
		$this->TblName='viewtrrwi';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->KD_PASIEN=$rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI=$rec->tgl_transaksi;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->NAMA_KAMAR=$rec->nama_kamar;
		$row->POSTING_TRANSAKSI=$rec->posting_transaksi;
		$row->CO_STATUS=$rec->co_status;
		$row->LUNAS=$rec->lunas;
		$row->NO_KAMAR=$rec->no_kamar;
		$row->TGL_NGINAP=$rec->tgl_inap;
		$row->KELAS=$rec->namakelas;
		$row->KD_KELAS=$rec->kd_kelas;
		$row->KD_UNIT_KAMAR=$rec->kd_unit_kamar;
		return $row;
	}
}
class Rowdokter
{
	
	public $KD_UNIT;
    public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $NAMA_KAMAR;
	public $POSTING_TRANSAKSI;
	PUBlic $CO_STATUS;
	PUBlic $LUNAS;
	Public $KELAS;
	public $TGL_NGINAP;
	public $NO_KAMAR;
	public $KD_KELAS;
	public $KD_UNIT_KAMAR;
}

?>