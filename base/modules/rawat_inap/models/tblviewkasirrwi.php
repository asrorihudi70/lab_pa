﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewkasirrwi extends TblBase
{
	function __construct()
	{
		$this->StrSql = "customer,nama_kamar,no_transaksi,kd_unit,
		
		nama,alamat,

		kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,lunas,co_status,kd_kasir,
		kd_kelas,no_kamar,kd_unit_kamar,
		ket_payment,bbl,rawat_gabung";

		/*$this->SqlQuery = "	select * from (  select customer.customer,k.kd_kelas,unit.parent as parentnya, k.kelas as namakelas,unit.nama_unit,datainap.kd_unit,datainap.no_kamar, 
												kamar.nama_kamar,datainap.kd_unit as unitKamar,unit.kd_bagian,nginap.kd_unit_kamar, nginap.tgl_inap,pasien.nama, pasien.alamat,
												kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, dokter.nama as nama_dokter,transaksi.lunas, 
												kunjungan.*, transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,dokter.kd_dokter, transaksi.orderlist,
												transaksi.posting_transaksi, knt.jenis_cust ,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,payment_type.jenis_pay,payment.kd_pay,payment_type.type_data
											from (( kunjungan inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											left join dokter on dokter.kd_dokter=kunjungan.kd_dokter)
											inner join customer on customer.kd_customer= kunjungan.kd_customer 
											inner join kontraktor knt on knt.kd_customer=kunjungan.kd_customer
											inner join transaksi on transaksi.kd_pasien = kunjungan.kd_pasien
												and transaksi.kd_unit =kunjungan.kd_unit and transaksi.tgl_transaksi=kunjungan.tgl_masuk 
												and transaksi.urut_masuk=kunjungan.urut_masuk
											left join pasien_inap as datainap on transaksi.no_transaksi=datainap.no_transaksi
												AND transaksi.kd_kasir = datainap.kd_kasir 
											inner join unit on kunjungan.kd_unit=unit.kd_unit 
											inner join payment on payment.kd_customer = kunjungan.kd_customer
											inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
											inner join nginap on kunjungan.kd_pasien=nginap.kd_pasien and kunjungan.kd_unit=nginap.kd_unit
											and kunjungan.tgl_masuk=nginap.tgl_masuk and kunjungan.urut_masuk=nginap.urut_masuk and nginap.akhir=true  
											inner join kamar on kamar.no_kamar=nginap.no_kamar and kamar.kd_unit=nginap.kd_unit_kamar
											inner join kelas k ON k.kd_kelas = unit.kd_kelas
							) as resdata ";*/
		/* $this->SqlQuery = "SELECT 
				p.kd_pasien, 
				p.kd_pasien as kode_pasien, 
				p.nama, t.no_transaksi, 
				t.tgl_transaksi, 
				k.tgl_masuk, 
				k.jam_masuk, 
				k.keadaan_pasien, 
				k.cara_keluar, 
				k.sebabmati as sebab_mati, 
				t.kd_unit, 
				u.nama_unit, 
				i.kd_unit as inap_unit, 
				l.kd_kelas, 
				l.kelas, 
				i.no_kamar, 
				u.parent as parentnya, 
				q.Nama_Kamar, k.Kd_Customer, c.Customer, 
				tc.Kd_Tarif, 
				t.Kd_Kasir, 
				k.cat_fisik, 
				CASE WHEN k.hak_kelas is null THEN '-' ELSE k.hak_kelas END AS hak_kelas,
				k.no_sjp,
				k.anamnese, 
				t.Urut_Masuk, 
				sp.kd_spesial,
				sp.spesialisasi,
				t.co_status, 
				t.posting_transaksi,
				u.kelas AS kelas_rawat,
				CASE WHEN u.kelas<>K.hak_kelas THEN 1 ELSE 0 END AS pindah_kelas,
				t.lunas as lunas,
				l.kelas as namakelas, 
				p.Alamat, 
				 pekerjaan.pekerjaan, p.jenis_kelamin,
				 p.tgl_lahir,
			 p.No_Asuransi, p.Pemegang_Asuransi, i.kd_spesial, dr.kd_dokter, dr.nama as Nama_Dokter,
				paytype.deskripsi as cara_bayar,
				pay.uraian as ket_payment,
				paytype.jenis_pay,
				pay.kd_pay,
				k.bbl,
				ng.rawat_gabung
				FROM ((((((((Pasien_Inap i 
				LEFT JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi) 
				INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien)
				INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk)
				INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer)
				INNER JOIN Dokter dr ON k.kd_dokter=dr.kd_dokter) 
				INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit)
				INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas) 
				INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer) 
				LEFT JOIN NGINAP ng ON ng.Kd_Pasien = k.Kd_Pasien AND ng.tgl_masuk = K.Tgl_Masuk AND ng.Kd_Unit = k.Kd_Unit AND ng.Urut_Masuk = k.Urut_Masuk 				
				INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit AND i.No_Kamar = q.No_Kamar 
				INNER JOIN spesialisasi sp ON sp.kd_spesial = i.kd_spesial
				left join pekerjaan on pekerjaan.kd_pekerjaan= p.kd_pekerjaan
				INNER join payment pay on pay.kd_customer = k.kd_customer
				INNER join payment_type paytype on pay.jenis_pay = paytype.jenis_pay
		"; */
		// $this->SqlQuery = "SELECT TOP ?
		// 					p.kd_pasien, 
		// 					p.kd_pasien as kode_pasien, 
		// 					p.nama, t.no_transaksi, 
		// 					t.tgl_transaksi, 
		// 					k.tgl_masuk, 
		// 					k.jam_masuk, 
		// 					k.keadaan_pasien, 
		// 					k.cara_keluar, 
		// 					k.sebabmati as sebab_mati, 
		// 					t.kd_unit, 
		// 					u.nama_unit, 
		// 					i.kd_unit as inap_unit, 
		// 					l.kd_kelas,
		// 					l.kelas,  
		// 					i.no_kamar, 
		// 					u.parent as parentnya, 
		// 					q.Nama_Kamar, k.Kd_Customer, c.Customer, 
		// 					tc.Kd_Tarif, 
		// 					t.Kd_Kasir, 
		// 					k.cat_fisik, 
		// 					k.no_sjp,
		// 					k.anamnese, 
		// 					t.Urut_Masuk, 
		// 					sp.kd_spesial as sp_kd_spesial,
		// 					sp.spesialisasi,
		// 					t.co_status, 
		// 					t.posting_transaksi,
		// 					t.lunas as lunas,
		// 					p.Alamat, 
		// 					pekerjaan.pekerjaan, p.jenis_kelamin,
		// 					p.tgl_lahir,
		// 					p.No_Asuransi, p.Pemegang_Asuransi, i.kd_spesial as inap_kd_spesial, dr.kd_dokter, dr.nama as Nama_Dokter,
		// 					paytype.deskripsi as cara_bayar,
		// 					pay.uraian as ket_payment,
		// 					paytype.jenis_pay,
		// 					pay.kd_pay,
		// 					k.hak_kelas,
		// 					k.bbl,
		// 					ng.rawat_gabung
		// 					FROM ((((((((Pasien_Inap i 
		// 					LEFT JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi) 
		// 					INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien)
		// 					INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk)
		// 					INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer)
		// 					INNER JOIN Dokter dr ON k.kd_dokter=dr.kd_dokter) 
		// 					INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit)
		// 					INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas) 
		// 					INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer) 
		// 					LEFT JOIN NGINAP ng ON ng.Kd_Pasien = k.Kd_Pasien AND ng.tgl_masuk = K.Tgl_Masuk AND ng.Kd_Unit = k.Kd_Unit AND ng.Urut_Masuk = k.Urut_Masuk 				
		// 					INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit AND i.No_Kamar = q.No_Kamar 
		// 					INNER JOIN spesialisasi sp ON sp.kd_spesial = i.kd_spesial
		// 					left join pekerjaan on pekerjaan.kd_pekerjaan= p.kd_pekerjaan
		// 					INNER join payment pay on pay.kd_customer = k.kd_customer
		// 					INNER join payment_type paytype on pay.jenis_pay = paytype.jenis_pay";
		$this->SqlQuery = "SELECT TOP ?
							p.kd_pasien,
							p.kd_pasien AS kode_pasien,
							p.nama,
							t.no_transaksi,
							t.tgl_transaksi,
							k.tgl_masuk,
							k.jam_masuk,
							k.keadaan_pasien,
							k.cara_keluar,
							k.sebabmati AS sebab_mati,
							t.kd_unit,
							u.nama_unit,
							ng.kd_unit_kamar AS inap_unit,
							l.kd_kelas,
							l.kelas,
							ng.no_kamar,
							u.parent AS parentnya,
							q.Nama_Kamar,
							k.Kd_Customer,
							c.Customer,
							tc.Kd_Tarif,
							t.Kd_Kasir,
							k.cat_fisik,
							k.no_sjp,
							k.anamnese,
							t.Urut_Masuk,
							sp.kd_spesial AS sp_kd_spesial,
							sp.spesialisasi,
							t.co_status,
							t.posting_transaksi,
							t.lunas AS lunas,
							p.Alamat,
							pekerjaan.pekerjaan,
							p.jenis_kelamin,
							p.tgl_lahir,
							p.No_Asuransi,
							p.Pemegang_Asuransi,
							ng.kd_spesial AS inap_kd_spesial,
							dr.kd_dokter,
							dr.nama AS Nama_Dokter,
							paytype.deskripsi AS cara_bayar,
							pay.uraian AS ket_payment,
							paytype.jenis_pay,
							pay.kd_pay,
							k.hak_kelas,
							k.bbl,
							ng.rawat_gabung 
							FROM (((((((Transaksi t 
							INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien)
							INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk)
							INNER JOIN NGINAP ng ON ng.Kd_Pasien = k.Kd_Pasien AND ng.tgl_masuk = K.Tgl_Masuk AND ng.Kd_Unit = k.Kd_Unit AND ng.Urut_Masuk = k.Urut_Masuk
							INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer)
							INNER JOIN Dokter dr ON k.kd_dokter=dr.kd_dokter) 
							INNER JOIN Unit u ON ng.Kd_Unit_kamar = u.Kd_Unit)
							INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas) 
							INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer) 
							INNER JOIN Kamar q ON ng.Kd_Unit_kamar = q.Kd_Unit AND ng.No_Kamar = q.No_Kamar 
							INNER JOIN spesialisasi sp ON sp.kd_spesial = ng.kd_spesial
							left join pekerjaan on pekerjaan.kd_pekerjaan= p.kd_pekerjaan
							INNER join payment pay on pay.kd_customer = k.kd_customer
							INNER join payment_type paytype on pay.jenis_pay = paytype.jenis_pay";
		$this->TblName = 'viewtrrwi';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row = new Rowdokter;
		$row->NAMA_UNIT         = $rec->nama_unit;
		$row->NAMA              = $rec->nama;
		$row->ALAMAT            = $rec->Alamat;
		$row->KD_PASIEN         = $rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI = $rec->tgl_transaksi;
		$row->TANGGAL_MASUK     = date_format(date_create($rec->tgl_masuk), 'Y-m-d');
		$row->JAM_MASUK     	= date_format(date_create($rec->jam_masuk), 'H:i:s');
		$row->NAMA_DOKTER       = $rec->Nama_Dokter;
		$row->NO_TRANSAKSI      = $rec->no_transaksi;
		$row->KD_CUSTOMER       = $rec->Kd_Customer;
		$row->CUSTOMER          = $rec->Customer;
		$row->KD_UNIT           = $rec->kd_unit;
		$row->KD_DOKTER         = $rec->kd_dokter;
		$row->URUT_MASUK        = $rec->Urut_Masuk;
		$row->NAMA_KAMAR        = $rec->Nama_Kamar;
		// $row->KELAS_RAWAT        = $rec->kelas_rawat;
		// $row->PINDAH_KELAS        = $rec->pindah_kelas;
		//$row->KET_PAYMENT     = $rec->ket_payment;
		//$row->CARA_BAYAR      = $rec->cara_bayar;
		//$row->KD_PAY          = $rec->kd_pay;
		//$row->JENIS_PAY       = $rec->jenis_pay;
		//$row->TYPE_DATA       = $rec->type_data;
		$row->POSTING_TRANSAKSI = $rec->posting_transaksi;
		$row->CO_STATUS         = $rec->co_status;
		$row->PARENTNYA         = $rec->parentnya;
		$row->CAT_FISIK         = $rec->cat_fisik;
		$row->ANAMNESE         = $rec->anamnese;
		$row->LUNAS             = $rec->lunas;
		$row->NO_KAMAR          = $rec->no_kamar;
		// $row->TGL_NGINAP      	= $rec->tgl_inap;
		$row->KELAS             = $rec->kelas;
		$row->KD_KELAS          = $rec->kd_kelas;
		$row->KD_UNIT_KAMAR     = $rec->inap_unit;
		$row->HAK_KELAS          = $rec->hak_kelas;
		$row->NO_SJP          = $rec->no_sjp;
		$row->KD_KASIR          = $rec->Kd_Kasir;
		$row->KD_SPESIAL        = $rec->sp_kd_spesial;
		$row->SPESIALISASI      = $rec->spesialisasi;
		// $row->KETERANGAN        = $rec->spesialisasi."/ ".$rec->kelas."/ ".$rec->no_kamar." [ ".$rec->nama_kamar." ] ";
		$row->CARA_BAYAR        = $rec->cara_bayar;
		$row->KET_PAYMENT       = $rec->ket_payment;
		$row->JENIS_PAY         = $rec->jenis_pay;
		$row->KD_PAY         	= $rec->kd_pay;
		$row->CARA_KELUAR 		= $rec->cara_keluar;
		$row->SEBAB_MATI 		= $rec->sebab_mati;
		$row->KEADAAN_PASIEN 	= $rec->keadaan_pasien;
		$row->PEKERJAAN         = $rec->pekerjaan;
		$row->UMUR              = $this->getAge(date('Y-m-d'), $rec->tgl_lahir);
		if ($rec->jenis_kelamin === 1 || $rec->jenis_kelamin == '1') {
			$row->JENIS_KELAMIN = 'Laki-laki';
		} else {
			$row->JENIS_KELAMIN = 'Perempuan';
		}
		if ($rec->Nama_Dokter == '0' || $rec->Nama_Dokter == '') {
			$row->NAMA_DOKTER = 'BELUM DIPILIH';
		} else {
			$row->NAMA_DOKTER = $rec->Nama_Dokter;
		}
		$row->BBL 				= $rec->bbl;
		$row->RAWAT_GABUNG 				= $rec->rawat_gabung;
		return $row;
	}
	private function getAge($tgl1, $tgl2)
	{
		$jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d')) - strtotime(date_format(date_create($tgl2), 'Y-m-d'))) / (60 * 60 * 24));
		$ret          = array();
		$ret['YEAR']  = floor($jumHari / 365);
		$sisa         = floor($jumHari - ($ret['YEAR'] * 365));
		$ret['MONTH'] = floor($sisa / 30);
		$sisa         = floor($sisa - ($ret['MONTH'] * 30));
		$ret['DAY']   = $sisa;

		if ($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0) {
			$ret['DAY'] = 1;
		}
		return $ret;
	}
}
class Rowdokter
{

	public $KD_UNIT;
	//public $NAMA_UNIT;
	public $NAMA;
	public $ALAMAT;
	public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $TANGGAL_MASUK;
	public $JAM_MASUK;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $NAMA_KAMAR;
	//public $KET_PAYMENT;
	//public $CARA_BAYAR;
	//public $KD_PAY;
	//public $JENIS_PAY;
	//public $TYPE_DATA;
	public $POSTING_TRANSAKSI;
	public $CO_STATUS;
	public $LUNAS;
	public $KELAS;
	public $TGL_NGINAP;
	public $NO_KAMAR;
	public $KD_KELAS;
	public $KD_UNIT_KAMAR;
	public $KD_KASIR;
	public $SPESIALISASI;
	public $KD_SPESIAL;
	public $KETERANGAN;
	public $CARA_BAYAR;
	public $KET_PAYMENT;
	public $JENIS_PAY;
	public $KD_PAY;
	public $CARA_KELUAR;
	public $SEBAB_MATI;
	public $KEADAAN_PASIEN;
	public $BBL;
	public $RAWAT_GABUNG;
}
