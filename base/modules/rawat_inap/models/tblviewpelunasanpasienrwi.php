﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewpelunasanpasienrwi extends TblBase
{
	function __construct()
	{
		$this->StrSql="no_medrec,nama,tgl_masuk,tgl_pulang,Kd_kasir,no_transaksi,dokter,lunas,kd_unit,nama_unit, kd_unit_kamar, verified";
		$this->SqlQuery = "SELECT t.Kd_Pasien as no_medrec, p.nama, t.Tgl_Transaksi as tgl_masuk, t.Tgl_CO as tgl_pulang, t.Kd_kasir, t.no_transaksi, d.nama as dokter,  
							t.lunas, NG.KD_UNIT, U.NAMA_UNIT,CO_STATUS,q.Nama_Kamar,ng.No_Kamar, ng.kd_unit_kamar
							FROM Transaksi t 
							INNER JOIN kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk
							INNER JOIN nginap ng on ng.Kd_Pasien = t.Kd_Pasien AND ng.Tgl_Masuk = t.Tgl_Transaksi AND ng.Kd_Unit = t.Kd_Unit AND ng.Urut_Masuk = t.Urut_Masuk
							--LEFT JOIN (select * from detail_bayar_lunas limit 1) dbl ON t.kd_kasir = dbl.kd_kasir AND t.NO_TRANSAKSI = dbl.NO_TRANSAKSI
							INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien 
							INNER JOIN dokter d on d.kd_dokter = k.kd_dokter
							INNER JOIN UNIT U ON U.KD_UNIT = NG.KD_UNIT_KAMAR
							INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas
							INNER JOIN Kamar q ON ng.KD_UNIT_KAMAR = q.Kd_Unit AND ng.No_Kamar = q.No_Kamar ";

/*SELECT (t.Kd_Pasien) as no_medrec, p.nama, t.Tgl_Transaksi as tgl_masuk, t.Tgl_CO as tgl_pulang, t.Kd_kasir, t.no_transaksi, d.nama as dokter, t.lunas, NG.KD_UNIT, U.NAMA_UNIT,CO_STATUS,q.Nama_Kamar,ng.No_Kamar, ng.kd_unit_kamar,ng.tgl_keluar,ng.akhir 
FROM Transaksi t 
	inner JOIN kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND 
		t.Urut_Masuk = k.Urut_Masuk 
	INNER JOIN nginap ng on ng.Kd_Pasien = k.Kd_Pasien AND ng.Tgl_Masuk = k.Tgl_masuk AND ng.Kd_Unit = t.Kd_Unit AND ng.Urut_Masuk = k.Urut_Masuk 
	INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien 
	INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
	INNER JOIN UNIT U ON U.KD_UNIT = NG.KD_UNIT 
	INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas 
	left JOIN Kamar q ON ng.Kd_Unit = q.Kd_Unit AND ng.No_Kamar = q.No_Kamar 
WHERE t.CO_Status = 'T' AND 
	t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in ('1001','1002','1003','1')) AND K.TGL_MASUK >= '14/Mar/2017' and K.TGL_MASUK <= '13/Apr/2017' AND 
t.Tgl_CO >= '14/Mar/2017'  and t.Tgl_CO <= '13/Apr/2017' and p.kd_pasien = '6-69-66-40' 
	and p.kd_pasien = '6-69-66-40'
	and ng.akhir = 't' 
ORDER BY ng.tgl_keluar desc*/
		$this->TblName='tblviewpelunasanpasienrwi';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowtblviewhistorypasienrwi;
		$row->KD_PASIEN         = $rec->no_medrec;
		$row->NAMA              = $rec->nama;
		$row->TGL_MASUK     	= date_format(date_create($rec->tgl_masuk), "Y-m-d");
		$row->TGL_PULANG       	= date_format(date_create($rec->tgl_pulang), "Y-m-d");
		$row->KD_KASIR      	= $rec->kd_kasir;
		$row->NO_TRANSAKSI      = $rec->no_transaksi;
		$row->DOKTER      		= $rec->dokter;
		$row->LUNAS      		= $rec->lunas;
		$row->KD_UNIT      		= $rec->kd_unit;
		$row->KD_UNIT_KAMAR 	= $rec->kd_unit_kamar;
		$row->UNIT      		= $rec->nama_unit.' ('.$rec->nama_kamar.')';
		$row->CO_STATUS    		= $rec->co_status;
		// $row->VERIFIED    		= $rec->verified;
		$query_verified = $this->db->query("SELECT verified from detail_bayar_lunas WHERE no_transaksi = '".$rec->no_transaksi."' AND kd_kasir = '".$rec->kd_kasir."' limit 1");

		if ($query_verified->num_rows() > 0) {
			$row->VERIFIED    		= $query_verified->row()->verified;
		}else{
			$row->VERIFIED    		= '0';

		}

		return $row;
	}
}
class Rowtblviewhistorypasienrwi
{	
	public $KD_PASIEN;
	public $NAMA;
	public $TGL_MASUK;
	public $TGL_PULANG;
	public $KD_KASIR;
	public $NO_TRANSAKSI;
	public $DOKTER;
	public $LUNAS;
	public $KD_UNIT;
	public $KD_UNIT_KAMAR;
	public $UNIT;
	public $CO_STATUS;
	public $VERIFIED;
    
}

?>