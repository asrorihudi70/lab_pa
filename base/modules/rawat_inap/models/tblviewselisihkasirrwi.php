<?php

/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewselisihkasirrwi extends TblBase
{
    function __construct()
    {
        $this->StrSql = "kd_kasi,no_transaksi,kd_klas,kd_produk,kp_produk,deskripsi,kd_unit,kd_kelas,kelas,harga,qty,urut,
        tgl_transaksi,kd_unit_tr,faktur,jatah,selisih,disc";
        $this->TblName = 'tblviewselisihkasirrwi';
        TblBase::TblBase(true);
        $this->SqlQuery = "SELECT d.Kd_Kasir,d.No_Transaksi,p.Kd_Klas,d.Kd_Produk,p.Kp_Produk,p.Deskripsi,d.Kd_Unit,k.Kd_Kelas,
	    Kelas = CASE WHEN k.Kd_Kelas = 0 THEN '' ELSE k.Kelas END,d.Harga,d.Qty,d.Urut,d.Tgl_Transaksi,d.Kd_Unit_Tr,
	    Faktur = IsNull( d.No_Faktur, '' ),a.hak as jatah,a.selisih,a.disc
    FROM
        Detail_Transaksi d INNER JOIN Transaksi g ON d.Kd_Kasir = g.Kd_Kasir AND d.No_Transaksi = g.No_Transaksi
        INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk 
        INNER JOIN Unit u ON d.Kd_Unit = u.Kd_Unit 
        INNER JOIN Kelas k ON u.Kd_Kelas = k.Kd_Kelas 
        INNER JOIN Detail_Prsh a ON d.Kd_Kasir = a.Kd_Kasir 
        AND d.No_Transaksi = a.No_Transaksi AND d.Urut = a.Urut 
        AND d.Tgl_Transaksi = a.Tgl_Transaksi ";
    }


    function FillRow($rec)
    {
        $row = new RowSelisih;
        $row->KD_KASIR         = $rec->Kd_Kasir;
        $row->NO_TRANSAKSI     = $rec->No_Transaksi;
        $row->KD_KLAS          = $rec->Kd_Klas;
        $row->KD_PRODUK        = $rec->Kd_Produk;
        $row->DESKRIPSI        = $rec->Deskripsi;
        $row->KD_UNIT          = $rec->Kd_Unit;
        $row->KD_KELAS         = $rec->Kd_Kelas;
        $row->KELAS            = $rec->Kelas;
        $row->HARGA            = $rec->Harga;
        $row->QTY              = $rec->Qty;
        $row->URUT             = $rec->Urut;
        $row->TGL_TRANSAKSI    = date_format(date_create($rec->Tgl_Transaksi), 'Y-m-d');
        $row->FAKTUR           = $rec->Faktur;
        $row->JATAH            = $rec->jatah;
        $row->SELISIH          = $rec->selisih;
        $row->DISC             = $rec->disc;
        return $row;
    }
}
class RowSelisih
{
    public $KD_KASIR;
    public $NO_TRANSAKSI;
    public $KD_KLAS;
    public $KD_PRODUK;
    public $DESKRIPSI;
    public $KD_UNIT;
    public $KD_KELAS;
    public $KELAS;
    public $HARGA;
    public $QTY;
    public $URUT;
    public $TGL_TRANSAKSI;
    public $FAKTUR;
    public $JATAH;
    public $SELISIH;
    public $DISC;
}
