﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewtrrwi extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="customer,nama_kamar,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,cat_fisik,anamnese,tgl_transaksi,
						kd_dokter,nama_dokter,kd_customer,kelas_kamar,kd_unit_kamar,no_kamar,data_pulang,kd_kasir,cara_penerimaan";
		
		$this->SqlQuery = "select * from ( select case when datainap.tgl_keluar isnull then false else true end as data_pulang,customer.customer,unit.nama_unit,datainap.kd_unit,datainap.kd_unit_kamar,kamar.nama_kamar, 
							unt.nama_unit, unt.nama_unit ||'-'||
							kamar.nama_kamar as kelas_kamar,
							unit.kd_bagian, pasien.nama, pasien.alamat,unit.parent as parentnya, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, 
							dokter.nama as nama_dokter, kunjungan.*, transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,
							 unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist,transaksi.posting_transaksi, knt.jenis_cust, datainap.no_kamar
							 from ((((((( kunjungan 
							 inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
							 left join dokter on dokter.kd_dokter=kunjungan.kd_dokter) 
							 inner join customer on customer.kd_customer= kunjungan.kd_customer) 
							 left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer) 
							 inner join transaksi on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit and 
							 transaksi.tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk ) 
							 inner join nginap as datainap on kunjungan.kd_pasien=datainap.kd_pasien and datainap.kd_unit=kunjungan.kd_unit 
							 and datainap.tgl_masuk=kunjungan.tgl_masuk and kunjungan.urut_masuk=datainap.urut_masuk and datainap.akhir=true  ) 
							 inner join unit on datainap.kd_unit=unit.kd_unit) 
							 inner join kamar on kamar.no_kamar=datainap.no_kamar and datainap.kd_unit_kamar=kamar.kd_unit
							 inner join unit as unt on unt.kd_unit=datainap.kd_unit_kamar
							 ) as resdata";
		$this->TblName='viewtrrwi';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->KD_PASIEN=$rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI=$rec->tgl_transaksi;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->NAMA_KAMAR=$rec->nama_kamar;
		$row->PARENTNYA=$rec->parentnya;
		$row->POSTING_TRANSAKSI=$rec->posting_transaksi;
		$row->ANAMNESE=$rec->anamnese;
		$row->CAT_FISIK=$rec->cat_fisik;
		$row->kelas_kamar=$rec->kelas_kamar;
		$row->no_kamar=$rec->no_kamar;
		$row->kd_unit_kamar=$rec->kd_unit_kamar;
		$row->data_pulang=$rec->data_pulang;
		$row->KD_KASIR=$rec->kd_kasir;
		$row->CARA_PENERIMAAN=$rec->cara_penerimaan;
		
		return $row;
	}
}
class Rowdokter
{
	
	public $KD_UNIT;
    public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $NAMA_KAMAR;
	public $POSTING_TRANSAKSI;
	public $ANAMNESE;
	public $CAT_FISIK;
	public $kelas_kamar;
	public $no_kamar;
	public $kd_unit_kamar;
	public $data_pulang;
	public $KD_KASIR;
	public $CARA_PENERIMAAN;
	
	
}

?>