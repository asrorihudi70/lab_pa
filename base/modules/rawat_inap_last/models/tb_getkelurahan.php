<?php

class tb_getkelurahan extends TblBase
{
    function __construct()
    {

        $this->TblName='viewkunjungan';
        TblBase::TblBase(true);

        $this->SqlQuery= "Select kec.kd_kecamatan, kel.kd_kelurahan From kecamatan kec LEFT JOIN Kelurahan Kel
                          ON Kel.kd_Kecamatan = Kec.KD_Kecamatan";
    
        
    }

    function FillRow($rec)
    {
        $row=new Rowviewtb_getkelurahan;

          $row->KD_KECAMATAN=$rec->kd_kecamatan;
          $row->KD_KELURAHAN=$rec->kd_kelurahan;
          
        return $row;
    }

}

class Rowviewtb_getkelurahan
{
        public $KD_KECAMATAN;
        public $KD_KELURAHAN;
}

?>
