<?php

class tb_kunjungan_pasien extends TblBase
{
    function __construct()
    {
        $this->TblName='kunjungan';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkunjunganpasien;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->KD_UNIT=$rec->kd_unit;
          $row->TGL_MASUK=$rec->tgl_masuk;
          $row->URUT_MASUK=$rec->urut_masuk;
          $row->JAM_MASUK=$rec->jam_masuk;
          $row->CARA_PENERIMAAN=$rec->cara_penerimaan;
          $row->KD_RUJUKAN=$rec->kd_rujukan;
          $row->ASAL_PASIEN=$rec->asal_pasien;
          $row->KD_DOKTER=$rec->kd_dokter;
          $row->BARU=$rec->baru;
          $row->KD_CUSTOMER=$rec->kd_customer;
          $row->SHIFT=$rec->shift;
          $row->KARYAWAN=$rec->karyawan;
          $row->KONTROL=$rec->kontrol;
          $row->ANTRIAN=$rec->antrian;
          $row->NO_SURAT=$rec->no_surat;
          $row->ALERGI=$rec->alergi;
          $row->ANAMNESE=$rec->anamnese;

          return $row;
    }

}

class Rowviewkunjunganpasien
{
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $JAM_MASUK;
    public $CARA_PENERIMAAN;
    public $KD_RUJUKAN;
    public $ASAL_PASIEN;
    public $KD_DOKTER;
    public $BARU;
    public $KD_CUSTOMER;
    public $SHIFT;
    public $KARYAWAN;
    public $KONTROL;
    public $ANTRIAN;
    public $NO_SURAT;
    public $ALERGI;
    public $ANAMNESE;
}

?>
