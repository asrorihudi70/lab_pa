<?php

class tb_nginap extends TblBase
{
    function __construct()
    {
        $this->TblName='nginap';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewnginap;
        $row->KD_UNIT_KAMAR=$rec->kd_unit_kamar;
        $row->NO_KAMAR=$rec->no_kamar;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->KD_UNIT=$rec->kd_unit;
        $row->TGL_MASUK=$rec->tgl_masuk;
        $row->URUT_MASUK=$rec->urut_masuk;
        $row->TGL_INAP=$rec->tgl_inap;
        $row->JAM_INAP=$rec->jam_inap;
        $row->TGL_KELUAR=$rec->tgl_keluar;
        $row->JAM_KELUAR=$rec->jam_keluar;
        $row->BED=$rec->bed;
        $row->KD_SPESIAL=$rec->kd_spesial;
        $row->AKHIR=$rec->akhir;
        $row->URUT_INAP=$rec->urut_inap;
        
        return $row;
    }
}

class Rowviewnginap
{
    public $KD_UNIT_KAMAR;
    public $NO_KAMAR;
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $TGL_INAP;
    public $JAM_INAP;
    public $TGL_KELUAR;
    public $JAM_KELUAR;
    public $BED;
    public $KD_SPESIAL;
    public $AKHIR;
    public $URUT_INAP;
}

?>
