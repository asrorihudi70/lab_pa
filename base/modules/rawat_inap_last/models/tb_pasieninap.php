<?php

class tb_pasieninap extends TblBase
{
    function __construct()
    {
        $this->TblName='pasien_inap';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewpasieninap;
        $row->KD_KASIR=$rec->kd_kasir;
        $row->NO_TRANSAKSI=$rec->no_transaksi;
        $row->KD_UNIT=$rec->kd_unit;
        $row->NO_KAMAR=$rec->no_kamar;
        $row->KD_SPESIAL=$rec->kd_spesial;
              
        return $row;
    }
}

class Rowviewpasieninap
{
    public $KD_KASIR;
    public $NO_TRANSAKSI;
    public $KD_UNIT;
    public $NO_KAMAR;
    public $KD_SPESIAL;
}

?>
