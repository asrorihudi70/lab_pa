<?php

class tb_pasienperinatal extends TblBase
{
    function __construct()
    {
        $this->TblName='pasien_perinatal';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewpasienperinatal;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->KD_UNIT=$rec->kd_unit;
        $row->TGL_MASUK=$rec->tgl_masuk;
        $row->URUT_MASUK=$rec->urut_masuk;
              
        return $row;
    }
}

class Rowviewpasienperinatal
{
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
}

?>
