
<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Controller_kunjungan extends MX_Controller
{
	public $params = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		// echo "<pre>".var_export($this->session->userdata, true)."</pre>";
		$query = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		if ($query->num_rows() > 0) {
		}
	}

	private function set_params()
	{

		//Data Penanggung Jawab
		//============================================================
		$angka 	= array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
		$huruf	= array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		$pos 	= "";
		$pos2	= "";
		$pos3	= "";
		$pos4	= "";
		$pos5	= "";
		$pos6	= "";
		$pos7	= "";
		$kelurahan_PJ 	 = $this->input->post('KelurahanPJ');
		$kelurahanKTP_PJ = $this->input->post('KelurahanKtpPJ');
		$hubungan_PJ 	 = $this->input->post('HubunganPj');
		$pendidikanPJ	 = $this->input->post('KdPendidikanPj');
		$pekerjaanPJ	 = $this->input->post('kdpekerjaanPj');
		$perusahaanPJ		 = $this->input->post('PerusahaanPj');
		$kd_bahasa		= $this->input->post('Bahasa');

		for ($i = 0; $i < count($angka); $i++) {
			$pos 	.= strpos($kelurahan_PJ, $angka[$i]);
			$pos2 	.= strpos($kelurahanKTP_PJ, $angka[$i]);
			$pos3	.= strpos($hubungan_PJ, $angka[$i]);
			$pos5	.= strpos($pekerjaanPJ, $angka[$i]);
			$pos6	.= strpos($perusahaanPJ, $angka[$i]);
			$pos7	.= strpos($kd_bahasa, $angka[$i]);
		}

		for ($i = 0; $i < count($huruf); $i++) {
			$pos4 	.= strpos(strtolower($pendidikanPJ), strtolower($huruf[$i]));
		}

		$kelurahan_PJ = '';
		if ($pos != "" || $pos != null) {
			$getKdKelurahan = $this->db->query("SELECT kd_kelurahan FROM kelurahan WHERE LOWER ( kelurahan ) = LOWER ('" . $kelurahan_PJ . "')")->row()->kd_kelurahan;
			$kelurahan_PJ = $getKdKelurahan;
		}

		$kelurahanKTP_PJ = '';
		if ($pos2 != "" || $pos2 != null) {
			$getKdKelurahanKTP = $this->db->query("SELECT kd_kelurahan FROM kelurahan WHERE LOWER ( kelurahan ) = LOWER ('" . $kelurahanKTP_PJ . "')")->row()->kd_kelurahan;
			$kelurahanKTP_PJ = $getKdKelurahanKTP;
		}

		$JK_Penanggung_Jawab = '';
		if (strtolower($this->input->post('JKpenanggungJwab')) == 'laki-laki') {
			$JK_Penanggung_Jawab = 'true';
		} else if (strtolower($this->input->post('JKpenanggungJwab')) == 'perempuan') {
			$JK_Penanggung_Jawab = 'false';
		} else {
			$JK_Penanggung_Jawab = $this->input->post('JKpenanggungJwab');
		}

		//[0, 'Blm Kawin'], [1, 'Kawin'], [2, 'Janda'], [3, 'Duda']
		$status_marital = '';
		if (strtolower($this->input->post('StatusMaritalPenanggungjawab')) == 'blm kawin') {
			$status_marital = '0';
		} else if (strtolower($this->input->post('StatusMaritalPenanggungjawab')) == 'kawin') {
			$status_marital = '1';
		} else if (strtolower($this->input->post('StatusMaritalPenanggungjawab')) == 'janda') {
			$status_marital = '2';
		} else if (strtolower($this->input->post('StatusMaritalPenanggungjawab')) == 'duda') {
			$status_marital = '3';
		} else {
			$status_marital = $this->input->post('StatusMaritalPenanggungjawab');
		}

		if ($pos3 == '' || $pos3 == "" || $pos3 == null) {
			$getHubunganPJ = $this->db->query("SELECT kd_hubungan FROM hubungan WHERE lower( status_hubungan ) = lower( '" . $hubungan_PJ . "' )")->row()->kd_hubungan;
			$hubungan_PJ = $getHubunganPJ;
		}

		if ($pos4 != '' || $pos4 != "" || $pos4 != null) {
			$getpendidikanPJ = $this->db->query("SELECT kd_pendidikan FROM pendidikan WHERE lower( pendidikan ) = lower( '" . $pendidikanPJ . "' )")->row()->kd_pendidikan;
			$pendidikanPJ	 = $getpendidikanPJ;
		}

		if ($pos5 == '' || $pos5 == "" || $pos5 == null) {
			$getpekerjaanPJ = $this->db->query("SELECT kd_pekerjaan FROM pekerjaan WHERE lower( pekerjaan ) = lower( '" . $pekerjaanPJ . "' )")->row()->kd_pekerjaan;
			$pekerjaanPJ	= $getpekerjaanPJ;
		}

		if ($pos6 == '' || $pos6 == "" || $pos6 == null) {
			$getperusahaanPJ = $this->db->query("select kd_perusahaan from perusahaan WHERE lower( perusahaan ) = lower( '" . $perusahaanPJ . "' )")->row()->kd_perusahaan;
			$perusahaanPJ	= $getperusahaanPJ;
		}

		if ($pos7 == '' || $pos7 == "" || $pos7 == null) {
			$tmp_bahasa = substr($kd_bahasa, 0);
			$getKdBahasa = $this->db->query("SELECT kd_bahasa FROM bahasa WHERE lower(nama_bahasa) like lower('%$tmp_bahasa')")->row()->kd_bahasa;
			$kd_bahasa = $getKdBahasa;
		}
		//======================================================================

		/* echo $perusahaanPJ;
		die; */

		//Untuk zona Banjarmasin
		date_default_timezone_set('Asia/Makassar');
		//date_default_timezone_set('singapore');
		//Untuk zona Jawa Timur
		// date_default_timezone_get('jakarta');

		$jam_masuk_kunjungan = date('d-m-Y H:i:s');
		/* echo date('d-m-Y H:i:s');
		die; */

		$this->params['kd_pasien'] 				= $this->input->post('NoMedrec');
		$this->params['ppk_rujukan'] 			= $this->input->post('ppk_rujukan');
		$this->params['no_rujukan'] 			= $this->input->post('no_rujukan');
		$this->params['kd_dokter_dpjp'] 		= $this->input->post('kd_dokter_dpjp');
		$this->params['nama_pasien'] 			= $this->input->post('NamaPasien');
		$this->params['nama_keluarga'] 			= $this->input->post('NamaKeluarga');
		$this->params['jenis_kelamin'] 			= $this->input->post('JenisKelamin');
		$this->params['tempat_lahir'] 			= $this->input->post('Tempatlahir');
		$this->params['tgl_lahir'] 				= $this->input->post('TglLahir');
		$this->params['agama'] 					= $this->input->post('Agama');
		$this->params['golongan_darah'] 		= $this->input->post('GolDarah');
		$this->params['status_marita'] 			= $this->input->post('StatusMarita');
		$this->params['status_warga'] 			= $this->input->post('StatusWarga');
		$this->params['alamat'] 				= $this->input->post('Alamat');
		$this->params['no_telepon'] 			= $this->input->post('No_Tlp');
		$this->params['pendidikan'] 			= $this->input->post('Pendidikan');
		$this->params['pekerjaan'] 				= $this->input->post('Pekerjaan');
		$this->params['suami_istri']	 		= $this->input->post('suami_istrinya');
		$this->params['pendidikan_ayah'] 		= $this->input->post('Pendidikan_Ayah');
		$this->params['pekerjaan_ayah'] 		= $this->input->post('Pekerjaan_Ayah');
		$this->params['pendidikan_ibu'] 		= $this->input->post('Pendidikan_Ibu');
		$this->params['pekerjaan_ibu']	 		= $this->input->post('Pekerjaan_Ibu');
		$this->params['pendidikan_suamiistri'] 	= $this->input->post('Pendidikan_SuamiIstri');
		$this->params['pekerjaan_suamiistri'] 	= $this->input->post('Pekerjaan_SuamiIstri');
		$this->params['nama_peserta'] 			= $this->input->post('NamaPeserta');
		$this->params['kd_asuransi'] 			= $this->input->post('KD_Asuransi');
		$this->params['no_askes'] 				= $this->input->post('NoAskes');
		$this->params['no_sjp'] 				= $this->input->post('NoSjp');
		$this->params['kd_suku']			 	= $this->input->post('Kd_Suku');
		$this->params['jabatan'] 				= $this->input->post('Jabatan');
		$this->params['perusahaan'] 			= $this->input->post('Perusahaan');
		$this->params['perusahaan1']		 	= $this->input->post('Perusahaan1');
		$this->params['cek'] 					= $this->input->post('Cek');
		$this->params['kd_unit']			 	= $this->input->post('Poli');
		$this->params['tgl_masuk'] 				= $this->input->post('TanggalMasuk');
		$this->params['urut_masuk'] 			= 0;
		//$this->params['jam_masuk'] 				= $this->input->post('JamKunjungan');
		$this->params['jam_masuk']				= $jam_masuk_kunjungan;
		$this->params['cara_penerimaan']	 	= $this->input->post('CaraPenerimaan');
		$this->params['kd_rujukan']			 	= $this->input->post('KdRujukan');
		$this->params['kd_rujukan_clone']	 	= $this->input->post('KdRujukanClone');
		$this->params['kd_customer'] 			= $this->input->post('KdCustomer');
		$this->params['kd_dokter']			 	= $this->input->post('KdDokter');
		$this->params['baru'] 					= $this->input->post('Baru');
		$this->params['shift'] 					= $this->input->post('Shift');
		$this->params['karyawan'] 				= $this->input->post('Karyawan');
		$this->params['kontrol']			 	= $this->input->post('Kontrol');
		$this->params['antrian'] 				= $this->input->post('Antrian');
		$this->params['no_surat'] 				= $this->input->post('NoSurat');
		$this->params['alergi'] 				= $this->input->post('Alergi');
		$this->params['anamnese'] 				= $this->input->post('Anamnese');
		$this->params['tahun_lahir'] 			= $this->input->post('TahunLahir');
		$this->params['bulan_lahir'] 			= $this->input->post('BulanLahir');
		$this->params['hari_lahir'] 			= $this->input->post('HariLahir');
		$this->params['kota'] 					= $this->input->post('Kota');
		$this->params['kd_kecamatan'] 			= $this->input->post('Kd_Kecamatan');
		$this->params['kelurahan'] 				= $this->input->post('Kelurahan');
		$this->params['asal_pasien'] 			= $this->input->post('AsalPasien');
		$this->params['kd_propinsi'] 			= $this->input->post('KDPROPINSI');
		$this->params['kd_kabupaten'] 			= $this->input->post('KDKABUPATEN');
		$this->params['kd_kecamatan'] 			= $this->input->post('KDKECAMATAN');
		$this->params['kd_kecamatan_ktp']	 	= $this->input->post('KDKECAMATANKTP');
		$this->params['kd_pendidikan'] 			= $this->input->post('KDPENDIDIKAN');
		$this->params['kd_pekerjaan'] 			= $this->input->post('KDPEKERJAAN');
		$this->params['kd_agama']			 	= $this->input->post('KDAGAMA');
		//$this->params['kelurahan_ktp_pj'] 		= $this->input->post('Kelurahan');
		$this->params['alamat_ktp_pasien'] 		= $this->input->post('AlamatKtpPasien');
		$this->params['kd_kelurahan_ktp_pasien'] = $this->input->post('KdKelurahanKtpPasien');
		$this->params['kd_pos_ktp_pasien'] 		= $this->input->post('KdPostKtpPasien');
		$this->params['nama_ayah_pasien'] 		= $this->input->post('NamaAyahPasien');
		$this->params['nama_ibu_pasien'] 		= $this->input->post('NamaIbuPasien');
		$this->params['kd_pos_pasien'] 			= $this->input->post('KdposPasien');
		$this->params['telepon_pasien'] 		= $this->input->post('TLPNPasien');
		$this->params['nik'] 					= $this->input->post('nik');
		$this->params['hp_pasien'] 				= $this->input->post('HPPasien');
		$this->params['email_pasien'] 			= $this->input->post('EmailPasien');
		$this->params['radio_rujukan_pasien'] 	= $this->input->post('RadioRujukanPasien');
		$this->params['pasien_baru_rujukan'] 	= $this->input->post('PasienBaruRujukan');
		$this->params['no_nik'] 				= $this->input->post('NoNIK');
		$this->params['non_kunjungan'] 			= $this->input->post('NonKunjungan');
		$this->params['kd_diagnosa'] 			= $this->input->post('KdDiagnosa');
		$this->params['part_number_nik'] 		= $this->input->post('part_number_nik');
		$this->params['unit_mana'] 				= $this->input->post('unitMana');
		$this->params['jenis_rujukan'] 			= $this->input->post('JenisRujukan');
		$this->params['catatan_sep'] 			= $this->input->post('catatanSep');
		$this->params['booking_online'] 		= $this->input->post('booking_online');
		$this->params['sub_unit_rehab'] 		= $this->input->post('sub_unit_rehab');
		$this->params['tgl_masuk'] 				= date('Y-m-d');
		$this->params['jam_masuk'] 				= "1990-01-01 " . date('H:i:s');
		$this->params['bahasa']					= $kd_bahasa;

		//HUDI
		//13-05-2020
		// Data Penanggung jawab
		//===========================================================================================
		//Data Pribadi PJ
		$this->params['nama_pj']			 	= $this->input->post('NAMA_PJ');
		$this->params['tempat_lahir_pj'] 		= $this->input->post('tempatlahirPenanggungjawab');
		$this->params['tgl_lahir_pj'] 			= $this->input->post('TgllahirPJ');
		$this->params['wni_pj'] 				= $this->input->post('WNIPJ');
		//$this->params['status_marita_pj'] 		= $this->input->post('StatusMaritalPenanggungjawab');
		$this->params['status_marita_pj'] 		= $status_marital;
		//$this->params['kelurahan_pj']			= $this->input->post('KelurahanPJ');."<br>".."<br>".."<br>".."<br>".;
		$this->params['kelurahan_pj']			= $kelurahan_PJ;
		$this->params['alamat_pj'] 				= $this->input->post('AlamatPJ');
		$this->params['pos_pj']				 	= $this->input->post('PosPJ');
		$this->params['telepon_pj']			 	= $this->input->post('TeleponPj');
		$this->params['hp_pj'] 					= $this->input->post('HpPj');
		//Data KTP PJ
		$this->params['ktp'] 					= $this->input->post('KTP');
		//$this->params['kelurahan_ktp_pj'] 		= $this->input->post('KelurahanKtpPJ');
		$this->params['kelurahan_ktp_pj'] 		= $kelurahanKTP_PJ;
		$this->params['alamat_ktp_pj'] 			= $this->input->post('AlamatKtpPJ');
		$this->params['kd_pos_ktp'] 			= $this->input->post('KdPosKtp');
		//$this->params['hubungan_pj'] 			= $this->input->post('HubunganPj');
		$this->params['hubungan_pj'] 			= $hubungan_PJ;
		//$this->params['kd_pendidikan_pj'] 		= $this->input->post('KdPendidikanPj');
		$this->params['kd_pendidikan_pj'] 		= $pendidikanPJ;
		//$this->params['kd_pekerjaan_pj'] 		= $this->input->post('kdpekerjaanPj');
		$this->params['kd_pekerjaan_pj'] 		= $pekerjaanPJ;
		//$this->params['perusahaan_pj'] 			= $this->input->post('PerusahaanPj');
		$this->params['perusahaan_pj'] 			= $perusahaanPJ;
		$this->params['email_pj'] 				= $this->input->post('EmailPenanggungjawab');
		$this->params['jk_pj'] 					= $this->input->post('JKpenanggungJwab');
		//==========================================================================================

	}
	public function save()
	{
		$result 	= false;
		$response 	= array();
		$response['kd_pasien'] 	= 'Automatic from the system...';
		$this->db->trans_begin();
		$this->set_params();
		//echo $this->params['bahasa'];
		//	die;

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN DATA PASIEN 
			===========================================================================================================================================
		 */
		// echo "hasil : ".$this->get_kd_pasien();
		// die;
		if ($this->params['kd_pasien'] == "" || $this->params['kd_pasien'] == "Automatic from the system...") {
			$this->params['kd_pasien'] = $this->get_kd_pasien();
			$parameter = $this->parameter_pasien();
			$this->db->insert("pasien", $parameter);
			if ($this->db->trans_status() === TRUE) {
				$result = true;
			} else {
				$result = false;
			}
			$response['message'] 	= "Gagal insert data pasien";
		} else {
			//HUDI
			//28-10-2020
			//Input medrec manual atau auto matic
			//===================================================================================================================
			$medrec = $_POST['NoMedrec'];
			$getMedrec = $this->db->query("SELECT count( kd_pasien ) AS jml FROM pasien WHERE kd_pasien = '$medrec'")->row()->jml;
			if ($getMedrec == 0) {
				// echo var_dump($this->parameter_pasien());
				// die;
				//$this->params['kd_pasien'] = $this->get_kd_pasien();
				$parameter = $this->parameter_pasien();
				$this->db->insert("pasien", $parameter);
				if ($this->db->trans_status() === TRUE) {
					$result = true;
				} else {
					$result = false;
				}
				$response['message'] 	= "Gagal insert data pasien";
			} else {
				$parameter = $this->parameter_pasien();
				$this->db->where(array('kd_pasien' => $this->params['kd_pasien']));
				$this->db->update("pasien", $parameter);
				if ($this->db->trans_status() === TRUE) {
					$result = true;
				} else {
					$result = false;
				}
				$response['message'] 	= "Gagal update data pasien";
			}
			//====================================================================================================================
		}
		// echo "1";
		// die;
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN DATA PASIEN 
			===========================================================================================================================================
		 */


		/*
			===========================================================================================================================================
			============================================================================ SIMPAN KUNJUNGAN 
			===========================================================================================================================================
		 */

		$query = $this->get(
			array(
				'select'   => "kd_unit",
				'criteria' => array(
					'kd_unit'          => $this->params['kd_unit'],
					'lower(nama_unit)' => strtolower($this->params['kd_unit']),
				),
				'table' 	=> "unit"
			)
		);
		if ($query->num_rows()) {
			$this->params['kd_unit'] = $query->row()->kd_unit;
		}

		if ($result > 0 || $result === true) {
			$criteria = array(
				'kd_pasien' 	=> $this->params['kd_pasien'],
				'kd_unit' 		=> $this->params['kd_unit'],
				'tgl_masuk' 	=> $this->params['tgl_masuk'],
			);
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("kunjungan");
			$query = $this->db->get();
			if ($query->num_rows() == 0) {
				$parameter = $this->parameter_kunjungan();
				$this->db->insert("kunjungan", $parameter);
				if ($this->db->trans_status() === TRUE) {
					$result = true;
				} else {
					$result = false;
				}
				$response['message'] 	= "Gagal insert kunjungan";
			} else {
				$result = false;
				$response['kd_pasien'] 	= $this->params['kd_pasien'];
				$response['message'] 	= "Pasien sudah berkunjungan kepoli yang sama pada tanggal " . date_format(date_create($this->params['tgl_masuk']), "d/M/Y");
			}
		} else {
			$result = true;
		}

		// echo "aaa <pre>".var_export($response, true)."</pre>"; 

		// die;
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN KUNJUNGAN 
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN SUB UNIT REHAB
			===========================================================================================================================================
		*/
		if ((isset($this->params["sub_unit_rehab"]) === true && $this->params["sub_unit_rehab"] != "") && ($result > 0 || $result === true)) {
			$query = $this->db->query("SELECT * FROM sys_setting where key_data = 'sub_unit'");
			if ($query->num_rows() > 0) {
				$query = $query->row()->setting;
				$query = explode("#", $query);
				if (in_array($this->params['kd_unit'], $query)) {
					$params_sub = array(
						'sub_unit' 		=> $this->params["sub_unit_rehab"],
						'kd_unit' 		=> $this->params['kd_unit'],
						'kd_pasien' 	=> $this->params['kd_pasien'],
						'tgl_masuk' 	=> $this->params['tgl_masuk'],
						'urut_masuk' 	=> $this->params['urut_masuk'],
					);
					$this->db->insert("kunjungan_sub", $params_sub);
				}
			}
		}
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN SUB UNIT REHAB
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN RUJUAKAN KUNJUNGAN
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {
			$query = $this->db->query("SELECT nilai from seting_bpjs where key_setting='customer'");
			if ($query->num_rows() > 0) {
				$query = $query->row()->nilai;
				$query = explode(",", $query);
				if (in_array($this->params['kd_customer'], $query) && ($this->params['no_rujukan'] != "") && ($this->params['kd_dokter_dpjp'] != "")) {
					$parameter = $this->parameter_rujukan();
					$this->db->insert("rujukan_kunjungan", $parameter);
					if ($this->db->trans_status() === TRUE) {
						$result = true;
					} else {
						$result = false;
					}
					$response['message'] 	= "Gagal insert Rujukan Kunjungan";
					$result = true;
				} else {
					$result = true;
				}
			}
		}
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN RUJUAKAN KUNJUNGAN
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN DIAGNOSA
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {
			if ($this->params['kd_diagnosa'] != "" && strlen($this->params['kd_diagnosa']) > 0) {
				if ($this->params['kd_diagnosa'] !== 'null') {
					$parameter = $this->parameter_diagnosa();
					$this->db->insert("mr_penyakit", $parameter);
					if ($this->db->trans_status() === TRUE) {
						$result = true;
					} else {
						$result = false;
					}
					$response['message'] 	= "Gagal insert MR Penyakit";
				} else {
					$result = true;
				}
			} else {
				$result = true;
			}
		}
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN DIAGNOSA
			===========================================================================================================================================
		*/

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN SJP KUNJUNGAN
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {

			if ($this->params['no_sjp'] != "") {
				$parameter = $this->parameter_sjp_kunjungan();
				$this->db->insert("sjp_kunjungan", $parameter);
				if ($this->db->trans_status() === TRUE) {
					$result = true;
				} else {
					$result = false;
				}
				$response['message'] 	= "Gagal insert SJP Kunjungan";
			} else {
				$result = true;
			}
		}
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN SJP KUNJUNGAN
			===========================================================================================================================================
		*/

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN PENANGGUNG JAWAB
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {
			$parameter = $this->parameter_penanggung_jawab();
			$this->db->insert("penanggung_jawab", $parameter);
			if ($this->db->trans_status() === TRUE) {
				$result = true;
			} else {
				$result = false;
			}
			$response['message'] 	= "Gagal insert SJP Kunjungan";

			if ($this->params['no_sjp'] != "") {
			} else {
				$result = true;
			}
		}

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN PENANGGUNG JAWAB
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN BOOKING ONLINE
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {
			unset($criteria);
			$criteria = array(
				'kd_pasien' 	=> $this->params['kd_pasien'],
				'kd_unit' 		=> $this->params['kd_unit'],
				'tgl_kunjungan'	=> $this->params['tgl_masuk'],
			);
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("booking_online");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$this->params['antrian_online'] = $query->row()->antrian;
				$this->db->where($criteria);
				$this->db->update("booking_online", array('kunjungan' => 'true'));
			}
		}

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN BOOKING ONLINE
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN TRANSAKSI
			===========================================================================================================================================
		*/

		if ($result > 0 || $result === true) {
			$query =  $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'");
			if ($query->num_rows() > 0) {
				$this->params['kd_kasir'] 		= $query->row()->setting;
				$this->params['no_transaksi'] 	= $this->get_no_transaksi($query->row()->setting);
				$this->params['kd_user'] 		= $this->session->userdata['user_id']['id'];
				$parameter = $this->parameter_transaksi();
				$this->db->insert("transaksi", $parameter);
				if ($this->db->trans_status() === TRUE) {
					$result = true;
				} else {
					$result = false;
				}
				$response['message'] 	= "Gagal insert Transaksi";
			} else {
				$result = false;
				$response['message'] 	= "Gagal KD Kasir belum terdaftar di sys_setting dengan key_data default_kd_kasir_rwj";
			}
		}

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN TRANSAKSI
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN DETAIL TRANSAKSI
			===========================================================================================================================================
		*/

		if ($result > 0 || $result === true) {
			$query 			= $this->db->query('getkdtarifcus @KdCus ="' . $this->params['kd_customer'] . '";');

			$tarif 			= "";
			$appto 			= "";
			$tgl_berlaku 	= "";

			if ($query->num_rows() < 0) {
				$tarif = $query->row()->getkdtarifcus;
			}

			if ($this->params['pasien_baru_rujukan'] == 'false') {
				$this->params['pasien_baru_rujukan'] = 0;
			} else {
				$this->params['pasien_baru_rujukan'] = 1;
			}
			if ($this->params['radio_rujukan_pasien'] == 'false') {
				$this->params['radio_rujukan_pasien'] = 0;
			} else {
				$this->params['radio_rujukan_pasien'] = 1;
			}

			$query = $this->db->query("getappto @App=0 ,@PasienBaru=" . $this->params['pasien_baru_rujukan'] . ",@Rujukan =" . $this->params['radio_rujukan_pasien'] . ",@Kontrol =0,@CetakKartu =0,@JenisRujukan ='" . $this->params['jenis_rujukan'] . "'");
			// echo "<pre>".var_export($query, true)."</pre>"; die;

			if (count($query->row()) > 0) {
				$appto = $query->row()->getappto;
			}
			// echo "<pre>".var_export($appto, true)."</pre>"; die;

			$query = $this->db->query("gettanggalberlakufromklas @KdTarif = '" . $tarif . "', @TglBerlaku = '" . date('Y-m-d') . "', @TglBerakhir = '" . date('Y-m-d') . "', @KdKlas='71' ");
			if ($query->num_rows() > 0) {
				$tgl_berlaku = $query->row()->gettanggalberlakufromklas;
			}
			if ($tgl_berlaku == "") {
				$tgl_berlaku = date('Y-m-d');
			}

			$this->db->query("INSERT INTO detail_transaksi
				(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,kd_unit_tr,
				tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
				SELECT  top 1 
					'" . $this->params['kd_kasir'] . "',
					'" . $this->params['no_transaksi'] . "',
					row_number() OVER (ORDER BY rn.tglberlaku DESC) as rnum,
					'" . $this->params['tgl_masuk'] . "',
					" . $this->params['kd_user'] . ",
					rn.kd_tarif,
					rn.kd_produk,
					rn.kd_unit,
					rn.kd_unit,
					rn.tglberlaku,
					'true',
					'true',
					'A'
					,1,
					rn.tarifx,
					" . $this->get_shift() . ",
					'false',
					''  
					from(
					Select AutoCharge.appto,
					tarif.kd_tarif,
					AutoCharge.kd_produk,
					AutoCharge.kd_unit,
					max (tarif.tgl_berlaku) as tglberlaku,
					tarif.tarif as tarifx,
					tarif.tgl_berakhir,
					row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
					From AutoCharge inner join tarif on 
					tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
					inner join produk on produk.kd_produk = tarif.kd_produk  
					Where 
					AutoCharge.kd_unit='" . $this->params["kd_unit"] . "'
					and AutoCharge.appto in " . $appto . "
					and LOWER(kd_tarif)=LOWER('" . $tarif . "')
					and tgl_berlaku <= '" . $tgl_berlaku . "'
					AND TGL_BERAKHIR IS NULL
					group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
					tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku , tarif.tgl_berakhir
					) as rn");

			$query = $this->db->query("
				SELECT * from(
					Select row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
					tarif.kd_tarif,
					AutoCharge.kd_produk,
					AutoCharge.kd_unit,
					max (tarif.tgl_berlaku) as tglberlaku,
					max(tarif.tarif) as tarifx
					FROM AutoCharge 
					INNER JOIN tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
					INNER JOIN produk on produk.kd_produk = tarif.kd_produk  
					WHERE 
					AutoCharge.kd_unit='" . $this->params["kd_unit"] . "' and 
					AutoCharge.appto in " . $appto . " and 
					LOWER(kd_tarif)=LOWER('" . $tarif . "') and 
					tgl_berlaku <= '" . $tgl_berlaku . "'
					group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
					tarif.kd_tarif ) as  rn
			");
			if ($query->num_rows() > 0) {
				$kdjasadok  = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_jasa_dok'");
				$kdjasaanas = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'");
				if ($kdjasadok->num_rows() > 0 && $kdjasaanas->num_rows() > 0) {
					$kdjasadok  = $kdjasadok->row()->setting;
					$kdjasaanas = $kdjasaanas->row()->setting;
					foreach ($query->result() as $row) {
						$kdprodukpasien   = $row->kd_produk;
						$kdTarifpasien    = $row->kd_tarif;
						$tglberlakupasien = $row->tglberlaku;
						$kd_unitpasein    = $row->kd_unit;
						$urutan       = $row->rnum;

						$this->db->query("
							INSERT INTO detail_component 
							(
								Kd_Kasir, 
								No_Transaksi, 
								Urut,
								Tgl_Transaksi,
								Kd_Component,
								Tarif, 
								Disc
							)
							Select 
								'" . $this->params['kd_kasir'] . "',
								'" . $this->params['no_transaksi'] . "',
								'" . $urutan . "',
								'" . $this->params['tgl_masuk'] . "',
								kd_component, 
								tarif as FieldResult,
								0
							From Tarif_Component 
							Where 
								KD_Unit='" . $kd_unitpasein . "' And 
								Tgl_Berlaku='" . $tglberlakupasien . "' And 
								KD_Tarif='" . $kdTarifpasien . "' And 
								Kd_Produk='" . $kdprodukpasien . "'");

						$this->db->query("
							INSERT INTO detail_trdokter 
								Select 
									'" . $this->params['kd_kasir'] . "',
									'" . $this->params['no_transaksi'] . "',
									" . $urutan . ",
									'" . $this->params["kd_dokter"] . "',
									'" . $this->params['tgl_masuk'] . "',
									0,
									" . $kdjasadok . ", 
									tarif as FieldResult,
									0,
									0,
									0,0
								From Tarif_Component
								Where 
									(
										KD_Unit='" . $kd_unitpasein . "' And 
										Tgl_Berlaku='" . $tglberlakupasien . "' And 
										KD_Tarif='" . $kdTarifpasien . "' And 
										Kd_Produk='" . $kdprodukpasien . "' and kd_component = '" . $kdjasadok . "')
									OR   (
										KD_Unit='" . $kd_unitpasein . "' 
										And Tgl_Berlaku='" . $tglberlakupasien . "'
										And KD_Tarif='" . $kdTarifpasien . "'
										And Kd_Produk='" . $kdprodukpasien . "' 
										and kd_component = '" . $kdjasaanas . "'
									)
						");

						$this->db->query("
							INSERT INTO detail_prsh 
							(
								Kd_Kasir, 
								No_Transaksi, 
								Urut,
								Tgl_Transaksi,
								hak,
								selisih,
								disc 
							)
							Select 
								'" . $this->params['kd_kasir'] . "',
								'" . $this->params['no_transaksi'] . "',
								'" . $urutan . "',
								'" . $this->params['tgl_masuk'] . "',
								0, 
								tarif as FieldResult,
								0
							From Tarif_Component 
							Where 
								KD_Unit='" . $kd_unitpasein . "' And 
								Tgl_Berlaku='" . $tglberlakupasien . "' And 
								KD_Tarif='" . $kdTarifpasien . "' And 
								Kd_Produk='" . $kdprodukpasien . "'");
					}
				} else {
					$result = false;
					$response['message'] 	= "Cek kembali sys setting dari key_data 'pel_jasa_dok' dan 'pel_JasaDokterAnestasi'";
				}
			}
		}

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN DETAIL TRANSAKSI
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN AUTO PAID
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {
			$query = $this->db->query("SELECT auto_paid from zusers where kd_user = '" . $this->params['kd_user'] . "'");
			if ($query->num_rows() > 0) {
				if ($query->row()->auto_paid === true || $query->row()->auto_paid > 0) {
					$query = "SELECT * FROM detail_transaksi WHERE kd_kasir='" . $this->params['kd_pasien'] . "' AND no_transaksi='" . $this->params['no_transaksi'] . "'";
					$query = $this->db->query($query)->result();
					$totBayar = 0;
					for ($i = 0, $iLen = count($query); $i < $iLen; $i++) {
						$totBayar += ((int)$query[$i]->qty * (int)$query[$i]->harga);
					}
					if ($totBayar > 0) {
						$sqlPay = "SELECT kd_pay FROM payment WHERE kd_customer='" . $this->params["kd_customer"] . "'";
						$resPay = $this->db->query($sqlPay);
						if ($resPay->num_rows() > 0) {
							$sql_pg = "SELECT inserttrpembayaran(
								'" . $this->params['kd_kasir'] . "',
								'" . $this->params['no_transaksi'] . "',
								1,
								'" . $this->params['tgl_masuk'] . "',
								'" . $this->params['kd_user'] . "',
								'" . $this->params['kd_unit'] . "',
								'" . $resPay->row()->kd_pay . "',
								" . $totBayar . ",
								'A',
								" . $this->get_shift() . ",
								true,
								'" . $this->params['tgl_masuk'] . "',
								1
							)";
							$this->db->query($sql_pg);
							// $inserttrpembayaran = $this->db->affected_rows();
							if ($this->db->trans_status() === TRUE) {
								$inserttrpembayaran = true;
							} else {
								$inserttrpembayaran = false;
							}
							$sql_pg = "SELECT insertpembayaran(
								'" . $this->params['kd_kasir'] . "',
								'" . $this->params['no_transaksi'] . "',
								1,
								'" . $this->params['tgl_masuk'] . "',
								'" . $this->params['kd_user'] . "',
								'" . $this->params['kd_unit'] . "',
								'" . $resPay->row()->kd_pay . "',
								" . $totBayar . ",
								'A',
								" . $this->get_shift() . ",
								true,
								'" . $this->params['tgl_masuk'] . "',
								1
							)";
							$this->db->query($sql_pg);
							if ($this->db->trans_status() === TRUE) {
								$inserttrpembayaran = true;
							} else {
								$inserttrpembayaran = false;
							}

							if (($inserttrpembayaran > 0 && $insertpembayaran > 0) || ($inserttrpembayaran === true && $insertpembayaran === true)) {
								$this->db->query("UPDATE detail_transaksi set tag='t' WHERE kd_kasir='" . $this->params['kd_kasir'] . "' AND no_transaksi='" . $this->params['no_transaksi'] . "'");
								// $updatedetailtransaksipg = $this->db->affected_rows();
								if ($this->db->trans_status() === TRUE) {
									$updatedetailtransaksipg = true;
								} else {
									$updatedetailtransaksipg = false;
								}
								$this->db->query("UPDATE detail_bayar set tag='t' WHERE kd_kasir='" . $this->params['kd_kasir'] . "' AND no_transaksi='" . $this->params['no_transaksi'] . "'");
								// $updatedetailbayarpg = $this->db->affected_rows();
								if ($this->db->trans_status() === TRUE) {
									$updatedetailbayarpg = true;
								} else {
									$updatedetailbayarpg = false;
								}
								if (($updatedetailtransaksipg > 0 && $updatedetailbayarpg > 0) || ($updatedetailtransaksipg === true && $updatedetailbayarpg === true)) {
									$result = true;
								} else {
									$result = false;
									$response['message'] 	= "Proses update detail transaksi dan detail bayar gagal diproses";
								}
							} else {
								$result = false;
								$response['message'] 	= "Auto paid gagal diproses";
							}
						} else {
							$result = false;
							$response['message'] 	= "Cek kembali kd_pay dari payment dengan kriteria customer " . $this->params["kd_customer"];
						}
					}
				}
			}
		}

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN AUTO PAID
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN REG UNI 
			===========================================================================================================================================
		*/
		if ($result > 0 || $result === true) {
			$query = "SELECT count(kd_pasien) as jum from reg_unit where kd_unit='" . $this->params['kd_unit'] . "' AND kd_pasien='" . $this->params['kd_pasien'] . "'";
			$query = $this->db->query($query);
			if ($query->num_rows() > 0) {
				if ($query->row()->jum == 0) {
					$query = "SELECT max(no_register) as no from reg_unit where kd_unit='" . $this->params['kd_unit'] . "'";
					$query = $this->db->query($query);
					if ($query->num_rows() > 0) {
						$query = (int)$query->row()->no + 1;
					} else {
						$query = 1;
					}
					$parameter = array(
						'kd_pasien'   => $this->params['kd_pasien'],
						'kd_unit'     => $this->params['kd_unit'],
						'no_register' => $query,
						'urut_masuk'  => 0
					);
					$this->db->insert('reg_unit', $parameter);
					if ($this->db->trans_status() === TRUE) {
						$result = true;
					} else {
						$result = false;
					}
					$response['message'] 	= "Gagal menyimpan reg unit";
				}
			}
		}
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN REG UNI 
			===========================================================================================================================================
		*/
		/*
			===========================================================================================================================================
			============================================================================ SIMPAN NO ANTRIAN 
			===========================================================================================================================================
		*/
		if (($result > 0 || $result === true) && ($this->params['booking_online'] === true || $this->params['booking_online'] == 'true')) {
			$parameter = $this->parameter_antrian_online();
			$this->db->insert("antrian_poliklinik", $parameter);
			if ($this->db->trans_status() === TRUE) {
				$result = true;
			} else {
				$result = false;
			}
			$response['message'] 	= "Gagal simpan no antrian";
		} else if ($result > 0 || $result === true) {
			$parameter = $this->parameter_antrian();
			$this->db->insert("antrian_poliklinik", $parameter);
			if ($this->db->trans_status() === TRUE) {
				$result = true;
			} else {
				$result = false;
			}
			$response['message'] 	= "Gagal simpan no antrian";
		}

		/*
			===========================================================================================================================================
			============================================================================ SIMPAN NO ANTRIAN 
			===========================================================================================================================================
		*/
		// echo "aaa <pre>".var_export($response, true)."</pre>"; 

		// die;
		if ($result > 0 || $result === true) {
			$this->db->trans_commit();
			$response['status'] 	= true;
			$response['kd_pasien'] 	= $this->params['kd_pasien'];
			$response['kd_unit'] 	= $this->params['kd_unit'];
			$response['tgl_masuk'] 	= $this->params['tgl_masuk'];
			$response['urut_masuk'] = $this->params['urut_masuk'];
			$response['message'] 	= "Data berhasil disimpan";
		} else {
			$response['status'] = false;

			$this->db->trans_rollback();
		}

		$this->db->close();
		echo json_encode($response);
	}

	private function parameter_antrian_online()
	{
		$urut = 1;
		$prioritas = 1;
		$query = $this->db->query("SELECT TOP 1 no_urut from antrian_poliklinik where tgl_transaksi ='" . $this->params['tgl_masuk'] . "' and kd_unit='" . $this->params['kd_unit'] . "' order by no_urut desc ");
		if ($query->num_rows() > 0) {
			$urut = (int)$query->row()->no_urut + 1;
		} else {
			$urut = 1;
		}
		$query = $this->db->query("SELECT setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'");
		if ($query->num_rows() > 0) {
			$query = $this->db->query("SELECT count(*) as jum_prioritas from kunjungan where tgl_masuk='" . $this->params['tgl_masuk'] . "' and kd_unit in(" . $query->row()->setting . ")");
			if ($query->num_rows() > 0) {
				$prioritas = (int)$query->row()->jum_prioritas + 1;
			} else {
				$prioritas = 1;
			}
		} else {
			$prioritas = 1;
		}
		$parameter = array(
			'kd_unit'           => $this->params['kd_unit'],
			'tgl_transaksi'     => $this->params['tgl_masuk'],
			'no_urut'           => $urut,
			'kd_pasien'         => $this->params['kd_pasien'],
			'kd_user'           => $this->params['kd_user'],
			'id_status_antrian' => 0,
			'sts_ditemukan'     => 'false',
			'no_antrian'        => 0,
			'no_prioritas'      => $prioritas,
			'no_antrian_online' => $this->params['antrian_online']
		);
		return $parameter;
	}

	private function parameter_antrian()
	{
		$urut = 1;
		$prioritas = 1;
		$query = $this->db->query("SELECT TOP 1 no_urut from antrian_poliklinik where tgl_transaksi ='" . $this->params['tgl_masuk'] . "' and kd_unit='" . $this->params['kd_unit'] . "' order by no_urut desc");
		if ($query->num_rows() > 0) {
			$urut = (int)$query->row()->no_urut + 1;
		} else {
			$urut = 1;
		}

		$query = $this->db->query("SELECT setting from sys_setting where key_data='rwj_kd_unit_no_antrian_prioritas'");
		if ($query->num_rows() > 0) {
			$query = $this->db->query("SELECT count(*) as jum_prioritas from kunjungan where tgl_masuk='" . $this->params['tgl_masuk'] . "' and kd_unit in(" . $query->row()->setting . ")");
			if ($query->num_rows() > 0) {
				$prioritas = (int)$query->row()->jum_prioritas + 1;
			} else {
				$prioritas = 1;
			}
		} else {
			$prioritas = 1;
		}

		$parameter = array(
			'kd_unit'           => $this->params['kd_unit'],
			'tgl_transaksi'     => $this->params['tgl_masuk'],
			'no_urut'           => $urut,
			'kd_pasien'         => $this->params['kd_pasien'],
			'kd_user'           => $this->params['kd_user'],
			'id_status_antrian' => 0,
			'sts_ditemukan'     => 'false',
			'no_antrian'        => 0,
			'no_prioritas'      => $prioritas
		);
		return $parameter;
	}

	private function parameter_transaksi()
	{
		$parameter = array(
			'kd_kasir'      => $this->params['kd_kasir'],
			'no_transaksi'  => $this->params['no_transaksi'],
			'kd_pasien'     => $this->params['kd_pasien'],
			'kd_unit'       => $this->params['kd_unit'],
			'tgl_transaksi' => $this->params['tgl_masuk'],
			'urut_masuk'    => $this->params['urut_masuk'],
			'tgl_co'        => null,
			'co_status'     => 'false',
			'orderlist'     => null,
			'ispay'         => 'false',
			'app'           => 'false',
			'kd_user'       => $this->params['kd_user'],
			'tag'           => null,
			'lunas'         => 'false',
			'tgl_lunas'     => null
		);
		return $parameter;
	}
	private function parameter_penanggung_jawab()
	{
		$parameter = array(
			'kd_pasien'       => $this->params['kd_pasien'],
			'kd_unit'         => $this->params['kd_unit'],
			'tgl_masuk'       => $this->params['tgl_masuk'],
			'urut_masuk'      => $this->params['urut_masuk'],
			'nama_pj'         => $this->params['nama_pj'],
			'alamat'          => $this->params['alamat_pj'],
			'kd_agama'        => $this->params['kd_agama'],
			'telepon'         => $this->params['telepon_pj'],
			'kd_pos'          => $this->params['pos_pj'],
			'hubungan'        => $this->params['hubungan_pj'],
			'alamat_ktp'      => $this->params['alamat_ktp_pj'],
			'no_hp'           => $this->params['hp_pj'],
			'kd_pos_ktp'      => $this->params['kd_pos_ktp'],
			'no_ktp'          => $this->params['ktp'],
			'email'           => $this->params['email_pj'],
			'tempat_lahir'    => $this->params['tempat_lahir_pj'],
			'tgl_lahir'       => $this->params['tgl_lahir_pj'],
			'status_marital'  => $this->params['status_marita_pj'],
			'jenis_kelamin'   => $this->params['jk_pj'],
			'kd_perusahaan'	  => $this->params['perusahaan_pj'],
			'kd_kelurahan'	  => $this->params['kelurahan_pj'],
			'kd_kelurahan_ktp' => $this->params['kelurahan_ktp_pj'],
		);

		if ($this->params['wni_pj'] === false || $this->params['wni_pj'] == 'false') {
			$parameter['wni'] = 'false';
		} else {
			$parameter['wni'] = 'true';
		}
		$query = $this->get(array('select' => "kd_kelurahan", 'criteria' => array('kd_kelurahan' => $this->params['kelurahan_ktp_pj'], 'lower(kelurahan)' => strtolower($this->params['kelurahan_ktp_pj']),), 'table' => "kelurahan"));
		if ($query->num_rows()) {
			//$parameter['kd_kelurahan_ktp'] = $query->row()->kd_kelurahan;	
		}

		$query = $this->get(array('select' => "kd_kelurahan", 'criteria' => array('kd_kelurahan' => $this->params['kelurahan_ktp_pj'], 'lower(kelurahan)' => strtolower($this->params['kelurahan_ktp_pj']),), 'table' => "kelurahan"));
		if ($query->num_rows()) {
			//$parameter['kd_kelurahan'] = $query->row()->kd_kelurahan;	
		}

		if ($this->params['kd_pekerjaan_pj'] == "") {
			$this->params['kd_pekerjaan_pj'] = 0;
		}
		$query = $this->get(array('select' => "kd_pekerjaan", 'criteria' => array('kd_pekerjaan' => $this->params['kd_pekerjaan_pj'], 'lower(pekerjaan)' => strtolower($this->params['kd_pekerjaan_pj']),), 'table' => "pekerjaan"));
		if ($query->num_rows()) {
			$parameter['kd_pekerjaan'] = $query->row()->kd_pekerjaan;
		}
		if ($this->params['kd_pendidikan_pj'] == "") {
			$this->params['kd_pendidikan_pj'] = 0;
		}

		$numb = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
		$tmp_pendidikan = $this->params['kd_pendidikan_pj'];
		$y = "";
		for ($r = 0; $r < count($numb); $r++) {
			$y .= strpos($tmp_pendidikan, $numb[$r]);
		}
		if ($y == '' || $y == "") {
			$parameter['kd_pendidikan'] = $this->db->query("select kd_pendidikan from pendidikan where lower(pendidikan) = lower('$tmp_pendidikan')")->row()->kd_pendidikan;
		} else {
			$parameter['kd_pendidikan'] = $tmp_pendidikan;
		}
		//die;

		/* $query = $this->get( array( 'select' => "1kd_pendidikan", 'criteria' => array( 'kd_pendidikan' => $this->params['kd_pendidikan_pj'], 'lower(pendidikan)' => strtolower($this->params['kd_pendidikan_pj']), ), 'table' => "pendidikan" ));
		if ($query->num_rows()) {
			$parameter['kd_pendidikan'] = $query->row()->kd_pendidikan;	
		} */
		return $parameter;
	}

	private function parameter_sjp_kunjungan()
	{
		$parameter = array(
			'kd_pasien'  => $this->params['kd_pasien'],
			'kd_unit'    => $this->params['kd_unit'],
			'tgl_masuk'  => $this->params['tgl_masuk'],
			'urut_masuk' => $this->params['urut_masuk'],
			'no_sjp'     => $this->params['no_sjp'],
			'note'       => $this->params['catatan_sep']
		);
		return $parameter;
	}

	private function parameter_diagnosa()
	{
		$kasus = 'false';
		$query = $this->db->query("SELECT COUNT(kd_penyakit)AS jum FROM mr_penyakit WHERE kd_penyakit='" . $this->params['kd_diagnosa'] . "' AND kd_pasien='" . $this->params['kd_pasien'] . "'");
		if ($query->num_rows() > 0) {
			if ($query->row()->jum > 0) {
				$kasus = 'true';
			}
		}
		$urutan = 0;
		$query = $this->db->query(" getUrutMrPenyakit @kdpasien='" . $this->params['kd_pasien'] . "',@kdunit='" . $this->params['kd_unit'] . "',@tglmasuk='" . $this->params['tgl_masuk'] . "',@urutmasuk=" . $this->params['urut_masuk'] . " ");
		if (count($query->result()) > 0) {
			$urutan = $query->row()->getUrutMrPenyakit;
		}
		$parameter = array(
			'kd_penyakit' 	=> $this->params['kd_diagnosa'],
			'kd_pasien' 	=> $this->params['kd_pasien'],
			'kd_unit' 	    => $this->params['kd_unit'],
			'tgl_masuk' 	=> $this->params['tgl_masuk'],
			'urut_masuk' 	=> $this->params['urut_masuk'],
			'urut' 	        => $urutan,
			'stat_diag' 	=> 0,
			'kasus' 	    => $kasus,
			'tindakan' 	    => 99,
			'perawatan' 	=> 99,
		);
		return $parameter;
	}
	private function parameter_rujukan()
	{
		$kode_rujukan = $this->params['kd_rujukan'];
		if ($this->params['no_rujukan'] != '' || $this->params['no_rujukan'] != null) {
			if ($this->params['ppk_rujukan'] != null || $this->params['ppk_rujukan'] != '') {
				$kode_rujukan = $this->params['ppk_rujukan'];
			}
		}

		$parameter = array(
			'kd_rujukan'     => $kode_rujukan,
			'kd_pasien'      => $this->params['kd_pasien'],
			'kd_unit'        => $this->params['kd_unit'],
			'tgl_masuk'      => $this->params['tgl_masuk'],
			'urut_masuk'     => $this->params['urut_masuk'],
			'nomor_rujukan'  => $this->params['no_rujukan'],
			'kd_dokter_dpjp' => $this->params['kd_dokter_dpjp']
		);
		return $parameter;
	}
	private function parameter_pasien()
	{
		$parameter = array(
			'kd_pasien' 					=> $this->params['kd_pasien'],
			'kd_agama' 						=> $this->params['kd_agama'],
			'nama' 							=> $this->params['nama_pasien'],
			'gol_darah' 					=> $this->params['golongan_darah'],
			'jenis_kelamin' 				=> $this->params['jenis_kelamin'],
			'status_hidup' 					=> 'true',
			'status_marita' 				=> $this->params['status_marita'],
			'alamat' 						=> $this->params['alamat'],
			'kota' 							=> $this->params['kota'],
			'telepon' 						=> $this->params['telepon_pasien'],
			'kd_pos' 						=> $this->params['kd_pos_pasien'],
			'kd_asuransi' 					=> $this->params['kd_asuransi'],
			'no_asuransi' 					=> $this->params['no_askes'],
			'jabatan' 						=> $this->params['jabatan'],
			'tanda_pengenal' 				=> 0,
			'no_pengenal' 					=> "",
			'keterangan' 					=> null,
			'kode_lama' 					=> null,
			// 'wni' 							=> $this->params['wni_pj'],
			'nama_keluarga' 				=> $this->params['nama_keluarga'],
			'tempat_lahir' 					=> $this->params['tempat_lahir'],
			'pemegang_asuransi' 			=> null,
			'no_reg_lama' 					=> null,
			'kd_suku' 						=> $this->params['kd_suku'],
			'ket_simpan' 					=> null,
			'handphone' 					=> "",
			'email' 						=> $this->params['email_pasien'],
			'nama_ayah' 					=> $this->params['nama_ayah_pasien'],
			'nama_ibu' 						=> $this->params['nama_ibu_pasien'],
			'suami_istri' 					=> $this->params['suami_istri'],
			'alamat_ktp' 					=> $this->params['alamat_ktp_pasien'],
			'kd_pos_ktp' 					=> $this->params['kd_pos_ktp'],
			'part_number_nik' 				=> $this->params['part_number_nik'],
			'nik' 							=> $this->params['nik'],
			'bahasa'						=> $this->params['bahasa'],
		);

		if ($this->params['wni_pj'] === false || $this->params['wni_pj'] == 'false') {
			$parameter['wni']             = 'true';
		} else {
			$parameter['wni']             = 'false';
		}
		$parameter['kd_kelurahan']             = "";
		$parameter['kd_pendidikan']            = "";
		$parameter['kd_pekerjaan']             = "";
		$parameter['kd_perusahaan']            = 0;
		$parameter['kd_kelurahan_ktp']         = "";
		$parameter['kd_pendidikan_ayah']       = "";
		$parameter['kd_pendidikan_ibu']        = "";
		$parameter['kd_pendidikan_suamiistri'] = "";
		$parameter['kd_pekerjaan_ayah']        = "";
		$parameter['kd_pekerjaan_ibu']         = "";
		$parameter['kd_pekerjaan_suamiistri']  = "";
		$parameter['tgl_lahir']                = "";

		$query = $this->get(array('select' => "kd_kelurahan", 'criteria' => array('kd_kelurahan' => $this->params['kelurahan'], 'lower(kelurahan)' => strtolower($this->params['kelurahan']),), 'table' => "kelurahan"));
		if ($query->num_rows()) {
			$parameter['kd_kelurahan'] = $query->row()->kd_kelurahan;
		}

		if ($this->params['pendidikan'] == "") {
			$this->params['pendidikan'] = 0;
		}
		$query = $this->get(array('select' => "kd_pendidikan", 'criteria' => array('kd_pendidikan' => $this->params['pendidikan'], 'lower(pendidikan)' => strtolower($this->params['pendidikan']),), 'table' => "pendidikan"));
		if ($query->num_rows()) {
			$parameter['kd_pendidikan'] = $query->row()->kd_pendidikan;
		}

		if ($this->params['pekerjaan'] == "") {
			$this->params['pekerjaan'] = 0;
		}
		$query = $this->get(array('select' => "kd_pekerjaan", 'criteria' => array('kd_pekerjaan' => $this->params['pekerjaan'], 'lower(pekerjaan)' => strtolower($this->params['pekerjaan']),), 'table' => "pekerjaan"));
		if ($query->num_rows()) {
			$parameter['kd_pekerjaan'] = $query->row()->kd_pekerjaan;
		}

		/*$query = $this->get( array( 'select' => "kd_kelurahan", 'criteria' => array( 'kd_kelurahan' => $this->params['kd_kelurahan_ktp_pasien'], 'lower(kelurahan)' => strtolower($this->params['kd_kelurahan_ktp_pasien']), ), 'table' => "kelurahan" ));
		if ($query->num_rows()) {
			$parameter['kd_kelurahan_ktp'] = $query->row()->kd_kelurahan;
		}*/
		$query = $this->get(array('select' => "kd_kelurahan", 'criteria' => array('kd_kelurahan' => $this->params['kelurahan'], 'lower(kelurahan)' => strtolower($this->params['kelurahan']),), 'table' => "kelurahan"));
		if ($query->num_rows()) {
			$parameter['kd_kelurahan_ktp'] = $query->row()->kd_kelurahan;
		}
		//echo $this->params['kelurahan'];
		//echo $parameter['kd_kelurahan_ktp'];
		//die;

		if ($this->params['pendidikan_ayah'] == "") {
			$this->params['pendidikan_ayah'] = 0;
		}
		$query = $this->get(array('select' => "kd_pendidikan", 'criteria' => array('kd_pendidikan' => $this->params['pendidikan_ayah'], 'lower(pendidikan)' => strtolower($this->params['pendidikan_ayah']),), 'table' => "pendidikan"));
		if ($query->num_rows()) {
			$parameter['kd_pendidikan_ayah'] = $query->row()->kd_pendidikan;
		}

		if ($this->params['pendidikan_ibu'] == "") {
			$this->params['pendidikan_ibu'] = 0;
		}
		$query = $this->get(array('select' => "kd_pendidikan", 'criteria' => array('kd_pendidikan' => $this->params['pendidikan_ibu'], 'lower(pendidikan)' => strtolower($this->params['pendidikan_ibu']),), 'table' => "pendidikan"));
		if ($query->num_rows()) {
			$parameter['kd_pendidikan_ibu'] = $query->row()->kd_pendidikan;
		}


		if ($this->params['pendidikan_suamiistri'] == "") {
			$this->params['pendidikan_suamiistri'] = 0;
		}
		$query = $this->get(array('select' => "kd_pendidikan", 'criteria' => array('kd_pendidikan' => $this->params['pendidikan_suamiistri'], 'lower(pendidikan)' => strtolower($this->params['pendidikan_suamiistri']),), 'table' => "pendidikan"));
		if ($query->num_rows()) {
			$parameter['kd_pendidikan_suamiistri'] = $query->row()->kd_pendidikan;
		}

		if ($this->params['pekerjaan_ayah'] == "") {
			$this->params['pekerjaan_ayah'] = 0;
		}
		$query = $this->get(array('select' => "kd_pekerjaan", 'criteria' => array('kd_pekerjaan' => $this->params['pekerjaan_ayah'], 'lower(pekerjaan)' => strtolower($this->params['pekerjaan_ayah']),), 'table' => "pekerjaan"));
		if ($query->num_rows()) {
			$parameter['kd_pekerjaan_ayah'] = $query->row()->kd_pekerjaan;
		}

		if ($this->params['pekerjaan_ibu'] == "") {
			$this->params['pekerjaan_ibu'] = 0;
		}
		$query = $this->get(array('select' => "kd_pekerjaan", 'criteria' => array('kd_pekerjaan' => $this->params['pekerjaan_ibu'], 'lower(pekerjaan)' => strtolower($this->params['pekerjaan_ibu']),), 'table' => "pekerjaan"));
		if ($query->num_rows()) {
			$parameter['kd_pekerjaan_ibu'] = $query->row()->kd_pekerjaan;
		}


		if ($this->params['pekerjaan_suamiistri'] == "") {
			$this->params['pekerjaan_suamiistri'] = 0;
		}
		$query = $this->get(array('select' => "kd_pekerjaan", 'criteria' => array('kd_pekerjaan' => $this->params['pekerjaan_suamiistri'], 'lower(pekerjaan)' => strtolower($this->params['pekerjaan_suamiistri']),), 'table' => "pekerjaan"));
		if ($query->num_rows()) {
			$parameter['kd_pekerjaan_suamiistri'] = $query->row()->kd_pekerjaan;
		}

		$tgl_lahir = explode("/", $this->params['tgl_lahir']);
		$parameter['tgl_lahir'] = $tgl_lahir[2] . "-" . $tgl_lahir[1] . "-" . $tgl_lahir[0];
		return $parameter;
	}

	private function parameter_kunjungan()
	{
		if ($this->params['cara_penerimaan'] == "") {
			$this->params['cara_penerimaan'] = 99;
		}

		if ($this->params['kd_rujukan'] == "") {
			$this->params['kd_rujukan'] = 0;
		}
		$query = $this->db->query("SELECT MAX(urut_masuk) as urut_masuk FROM kunjungan WHERE 
			kd_pasien = '" . $this->params['kd_pasien'] . "' AND
			kd_unit = '" . $this->params['kd_unit'] . "' AND
			tgl_masuk = '" . $this->params['tgl_masuk'] . "'");
		if ($query->num_rows() > 0) {
			if ($query->row()->urut_masuk == null || $query->row()->urut_masuk == "") {
				$this->params['urut_masuk'] = 0;
				// $this->params['baru'] 		= 'false';
			} else {
				$this->params['urut_masuk'] = (int)$query->row()->urut_masuk + 1;
				// $this->params['baru'] 		= 'true';
			}
		} else {
			$this->params['urut_masuk'] = 0;
			// $this->params['baru'] 		= 'false';
		}

		$query = $this->db->query("SELECT * FROM kunjungan WHERE 
			kd_pasien = '" . $this->params['kd_pasien'] . "'");
		if ($query->num_rows() > 0) {
			$this->params['baru'] 		= 'false';
		} else {
			$this->params['baru'] 		= 'true';
		}

		$query = $this->get(
			array(
				'select' 	=> " kd_asal ",
				'criteria'	=> array(
					'kd_unit' 		=> substr($this->params['kd_unit'], 0, 1),
				),
				'table' 	=> 'asal_pasien '
			)
		);

		if ($query->num_rows() > 0) {
			$this->params['asal_pasien'] = $query->row()->kd_asal;
		}

		$query = $this->get(array('select' => "kd_unit", 'criteria' => array('kd_unit' => $this->params['kd_unit'], 'lower(nama_unit)' => strtolower($this->params['kd_unit']),), 'table' => "unit"));
		if ($query->num_rows()) {
			$parameter['kd_unit'] = $query->row()->kd_unit;
		}

		$query = $this->get(array('select' => "kd_rujukan", 'criteria' => array('kd_rujukan' => $this->params['kd_rujukan'], 'lower(rujukan)' => strtolower($this->params['kd_rujukan']),), 'table' => "rujukan"));
		if ($query->num_rows()) {
			$parameter['kd_rujukan'] = $query->row()->kd_rujukan;
		}

		$query = $this->get(array('select' => "kd_dokter", 'criteria' => array('kd_dokter' => $this->params['kd_dokter'], 'lower(nama)' => strtolower($this->params['kd_dokter']),), 'table' => "dokter"));
		if ($query->num_rows()) {
			$parameter['kd_dokter'] = $query->row()->kd_dokter;
		}

		$query = $this->get(array('select' => "kd_customer", 'criteria' => array('kd_customer' => $this->params['kd_customer'], 'lower(customer)' => strtolower($this->params['kd_customer']),), 'table' => "customer"));
		if ($query->num_rows()) {
			$parameter['kd_customer'] = $query->row()->kd_customer;
		}

		$parameter = array(
			"kd_pasien"       => $this->params['kd_pasien'],
			"kd_unit"         => $this->params['kd_unit'],
			"tgl_masuk"       => $this->params['tgl_masuk'],
			"urut_masuk"      => $this->params['urut_masuk'],
			"jam_masuk"       => $this->params['jam_masuk'],
			"cara_penerimaan" => $this->params['cara_penerimaan'],
			"asal_pasien"     => $this->params['asal_pasien'],
			"kd_rujukan"      => $this->params['kd_rujukan'],
			"kd_dokter"       => $this->params['kd_dokter'],
			"baru"            => $this->params['baru'],
			"kd_customer"     => $this->params['kd_customer'],
			"shift"           => $this->get_shift(),
			"karyawan"        => $this->params['karyawan'],
			"kontrol"         => $this->params['kontrol'],
			"antrian"         => $this->params['antrian'],
			"no_surat"        => $this->params['no_surat'],
			"alergi"          => $this->params['alergi'],
			"anamnese"        => $this->params['anamnese'],
			"no_sjp"          => $this->params['no_sjp'],
			"sub_unit"        => $this->params['sub_unit_rehab'],
			// "nomor_spri"	  => null
		);
		return $parameter;
	}
	private function get($parameter)
	{
		if (isset($parameter['select'])) {
			$this->db->select($parameter['select']);
		} else {
			$this->db->select("*");
		}

		if (isset($parameter['criteria'])) {
			$this->db->or_where($parameter['criteria']);
		}

		$this->db->from($parameter['table']);
		return $this->db->get();
	}

	private function get_shift()
	{
		$sqlbagianshift = $this->db->query("SELECT shift FROM BAGIAN_SHIFT  where KD_BAGIAN='" . substr($this->params['kd_unit'], 0, 1) . "'")->row()->shift;
		$lastdate = $this->db->query("SELECT   CAST(lastdate as DATE) as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='" . substr($this->params['kd_unit'], 0, 1) . "'")->row()->lastdate;
		$datnow = date('Y-m-d');
		if ($lastdate <> $datnow && $sqlbagianshift === '3') {
			$sqlbagianshift2 = '4';
		} else {
			$sqlbagianshift2 = $sqlbagianshift;
		}
		return $sqlbagianshift2;
	}

	private function get_kd_pasien()
	{
		$res = $this->db->query("SELECT TOP 1 replace(kd_pasien,'-','') as KD_PASIEN from pasien Where kd_pasien Like '%-%' Order By kd_pasien desc");
		if (count($res->result()) > 0) {
			$nm = $res->row()->KD_PASIEN;
			$nomor = (int) $nm + 1;
			//echo $nomor.' ';
			if ($nomor < 999999) {
				$retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
			} else {
				$retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
			}
			//echo $retVal.' ';
			$getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
			//echo $getnewmedrec;
		} else {
			$strNomor = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
			$getnewmedrec = $strNomor . "01";
		}
		return $getnewmedrec;
	}


	private function get_no_transaksi($kd_kasir)
	{
		$counter = $this->db->query("SELECT counter from kasir where kd_kasir = '" . $kd_kasir . "'")->row();
		$no = $counter->counter;
		$retVal = $no + 1;
		$query = "UPDATE kasir set counter=$retVal where kd_kasir='" . $kd_kasir . "'";
		$update = $this->db->query($query);
		//-----------akhir insert ke database sql server----------------//
		if (strlen($retVal) == 1) {
			$retValreal = "000000" . $retVal;
		} else if (strlen($retVal) == 2) {
			$retValreal = "00000" . $retVal;
		} else if (strlen($retVal) == 3) {
			$retValreal = "0000" . $retVal;
		} else if (strlen($retVal) == 4) {
			$retValreal = "000" . $retVal;
		} else if (strlen($retVal) == 5) {
			$retValreal = "00" . $retVal;
		} else if (strlen($retVal) == 6) {
			$retValreal = "0" . $retVal;
		} else {
			$retValreal = $retVal;
		}
		return $retValreal;
	}
}
