<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Rawat_jalan extends MX_Controller
{
    private $id_mrresep;
    private $kd_kasir;
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        $this->kd_kasir = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
    }

    public function index()
    {
        $this->load->view('main/index');
    }

    public function save()
    {
        $this->db->trans_begin();
        $result   = false;
        $response = array();
        $params   = array(
            'no_transaksi' => $this->input->post('TrKodeTranskasi'),
            'kd_kasir'     => $this->kd_kasir,
            'shift'        => $this->GetShiftBagian(),
            'kd_pasien'    => $this->input->post('kd_pasien'),
            'kd_unit'      => $this->input->post('KdUnit'),
            'urut_masuk'   => $this->input->post('urut_masuk'),
            'tgl_masuk'    => $this->input->post('Tgl'),
            'kd_dokter'    => $this->input->post('kdDokter'),
            'list_obat'    => $this->input->post('list_obat'),
            'list'         => $this->input->post('List'),
        );

        $tmplunas = $this->db->query("SELECT lunas FROM transaksi where no_Transaksi = '" . $params['no_transaksi'] . "' and kd_kasir = '" . $params['kd_kasir'] . "'")->row()->lunas;
        if ($tmplunas === '1') {
            $this->db->where(array("no_transaksi" => $params['no_transaksi'], "kd_kasir" => $params['kd_kasir'],));
            $this->db->update("transaksi", array("lunas" => '0'));
            $result = $this->db->trans_status();
        } else {
            $result = true;
        }
        // echo "<pre>".var_export($result, true)."</pre>qq";

        if ($result > 0 || $result === true) {
            $result = $this->save_tindakan($params);
        } else {
            $result = false;
        }
        // echo "<pre>".var_export($result, true)."</pre>aa"; die;

        // echo $result; die;
        if ($result > 0 || $result === true) {
            $result = $this->save_obat($params);
        } else {
            $result = false;
        }

        if ($result > 0 || $result === true) {
            $response['status']  = true;
            $response['success'] = true;
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
            $response['status']  = false;
            $response['success'] = false;
        }
        $this->db->close();
        echo json_encode($response);
    }

    private function save_tindakan($params)
    {
        $result = false;
        $kdjasadok  = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
        $kdjasaanas = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
        $a      = explode("##[[]]##", $params['list']);
        // echo(count($a));die;
        for ($i = 0, $iLen = (count($a) - 1); $i < $iLen; $i++) {
            $b = explode("@@##$$@@", $a[$i]);
            $cek_KdUnit             = $this->db->query("SELECT kd_unit from tarif where kd_produk='" . $b[1] . "' and kd_tarif='" . $b[5] . "' and tgl_berlaku='" . $b[3] . "' and kd_unit='" . $params['kd_unit'] . "'"); // KODE UNIT MENGACU LANGSUNG KE TABEL TARIF
            if (count($cek_KdUnit->result()) == 0) {
                $KdUnit             = $this->db->query("SELECT kd_unit from tarif where kd_produk='" . $b[1] . "' and kd_tarif='" . $b[5] . "' and tgl_berlaku='" . $b[3] . "'")->row()->kd_unit; // KODE UNIT MENGACU LANGSUNG KE TABEL TARIF
            } else {
                $KdUnit = $params['kd_unit'];
            }


            if (isset($b[6])) {
                $urut = $this->db->query(" geturutdetailtransaksi @kdkasir='" . $params['kd_kasir'] . "',@notransaksi='" . $params['no_transaksi'] . "',@tgltransaksi='" . $params['tgl_masuk'] . "' ");
                $result = $urut->result();

                foreach ($result as $data) {
                    if ($b[6] == 'x') {
                        $Urutan = $data->geturutdetailtransaksi;
                    } else {
                        $Urutan = $b[6];
                    }
                }
                $result = $this->db->query("insert_detail_transaksi
                @kdkasir = '" . $params['kd_kasir'] . "',
                @notrans = '" . $params['no_transaksi'] . "',
                @urut =  '" . $Urutan . "',
                @tgltrans =  '" . $params['tgl_masuk'] . "',
                @kduser =  '" . $this->session->userdata['user_id']['id'] . "',
                @kdtarif=  '" . $b[5] . "',
                @kdproduk =   '" . $b[1] . "',
                @kdunit=  '" . $KdUnit . "',
                @tglberlaku =  '" . $b[3] . "',
                @charge = '0',
                @adjust = '1',
                @folio =  '',
                @qty =  '" . $b[2] . "',
                @harga= '" . $b[4] . "',
                @shift= " . $params['shift'] . ",
                @tag=  '0'
                ");

                if ($result->num_rows() > 0) {
                    $result = true;
                } else {
                    $result = false;
                    break;
                }


                #---------Akhir tambahan query untuk insert komopnent tarif dokter--------------#
                #_____________________________________________________________________________________
                
                if ($result > 0 || $result === true) {
                    $this->db->query("UPDATE detail_transaksi set 
                        kd_dokter = '" . $params['kd_dokter'] . "' 
                        where kd_kasir='" . $params['kd_kasir'] . "' AND
                        tgl_transaksi='" . $b[7] . "' AND
                        urut='" . $Urutan . "' AND
                        no_transaksi='" . $params['no_transaksi'] . "'");
                } else {
                    $result = false;
                    break;
                }

                if ($b[8] == 'false' && ($result > 0 || $result === true)) {
                    $cek = $this->db->query(
                        "SELECT * from detail_trdokter where 
                        kd_kasir='" . $params['kd_kasir'] . "'
                        and no_transaksi='" . $params['no_transaksi'] . "' 
                        and urut=" . $Urutan . " 
                        and tgl_transaksi='" . $params['tgl_masuk'] . "' 
                        and kd_component=" . $kdjasadok . ""
                    );

                    $data_tarifpgnya = $this->db->query(
                        "SELECT tarif as harga From Tarif_Component
                        Where 
                        KD_Unit='" . $params['kd_unit'] . "' 
                        And Tgl_Berlaku='" . $b[3] . "'
                        And KD_Tarif='" . $b[5] . "'
                        And Kd_Produk=" . $b[1] . " 
                        and kd_component = '" . $kdjasadok . "'"
                    );

                    if ($data_tarifpgnya->num_rows() > 0) {
                        // echo count($cek->num_rows()).'qqq';
                        // echo "<pre>".var_export($cek, true)."</pre>".'aaa'; die;

                        if ($cek->num_rows() > 0) {
                            $dataubah = array("kd_dokter" => $params['kd_dokter'], "jp" => $data_tarifpgnya->row()->harga);
                            $criteria = array(
                                "kd_kasir"      => $params['kd_kasir'],
                                "no_transaksi"  => $params['no_transaksi'],
                                "urut"          => $Urutan,
                                "tgl_transaksi" => $params['tgl_masuk'],
                                "kd_component"  => $kdjasadok
                            );
                            $this->db->where($criteria);
                            $result = $this->db->update("detail_trdokter", $dataubah);
                        } else {
                            $data = array(
                                "kd_kasir"      => $params['kd_kasir'],
                                "no_transaksi"  => $params['no_transaksi'],
                                "urut"          => $Urutan,
                                "kd_dokter"     => $params['kd_dokter'],
                                "tgl_transaksi" => $params['tgl_masuk'],
                                "kd_component"  => $kdjasadok,
                                "jp"            => $data_tarifpgnya->row()->harga
                            );
                            $result = $this->db->insert("detail_trdokter", $data);
                        }


                        if ($result > 0 || $result === true) {
                            $result = true;
                        } else {
                            $result = false;
                        }
                    }
                    // echo "<pre>".var_export($result, true)."</pre>".'aaa'; 

                } else {
                    $result = false;
                }
            }
        }

        return $result;
    }

    private function GetShiftBagian()
    {
        $sqlbagianshift = $this->db->query("SELECT  shift FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->shift;
        $lastdate       = $this->db->query("SELECT CAST(lastdate as date) as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->lastdate;
        $datnow = date('Y-m-d');
        if ($lastdate <> $datnow && $sqlbagianshift === '3') {
            $sqlbagianshift2 = '4';
        } else {
            $sqlbagianshift2 = $sqlbagianshift;
        }

        return $sqlbagianshift2;
    }

    private function save_obat($params)
    {
        $result = false;
        if ($params['list_obat'] != null) {
            $params['list_obat'] = json_decode($params['list_obat']);
            $index = 0;
            foreach ($params['list_obat'] as $key => $value) {
                if ($value->order_mng == 'Belum Dilayani') {
                    $criteria = array(
                        'kd_pasien'  => $params['kd_pasien'],
                        'kd_unit'    => $params['kd_unit'],
                        'tgl_masuk'  => $params['tgl_masuk'],
                        'urut_masuk' => $params['urut_masuk'],
                        'dilayani'   => '0',
                    );
                    $this->db->select("*");
                    $this->db->where($criteria);
                    $this->db->from("mr_resep");
                    $this->db->order_by("tgl_order", "DESC");
                    $this->db->limit(1);
                    $query = $this->db->get();

                    if ($query->num_rows() == 0) {
                        $id_mrresep = $this->db->query('SELECT TOP 1 id_mrresep from mr_resep order by id_mrresep desc');
                        if (count($id_mrresep->result()) > 0) {
                            $id_mrresep = substr($id_mrresep->row()->id_mrresep, 8, 12);
                            $sisa = 4 - count(((int)$id_mrresep + 1));
                            $real = date('Ymd');
                            for ($i = 0; $i < $sisa; $i++) {
                                $real .= "0";
                            }
                            $real .= ((int)$id_mrresep + 1);
                            $this->id_mrresep = $real;
                        } else {
                            $this->id_mrresep = date('Ymd') . '0001';
                        }
                        $mr_resep                = array();
                        $mr_resep['kd_pasien']   = $params['kd_pasien'];
                        $mr_resep['kd_unit']     = $params['kd_unit'];
                        $mr_resep['tgl_masuk']   = $params['tgl_masuk'];
                        $mr_resep['urut_masuk']  = $params['urut_masuk'];
                        $mr_resep['kd_dokter']   = $params['kd_dokter'];
                        $mr_resep['id_mrresep']  = $this->id_mrresep;
                        $mr_resep['cat_racikan'] = '';
                        $mr_resep['tgl_order']   = $params['tgl_masuk'];
                        $mr_resep['dilayani']    = 0;
                        $this->db->insert('mr_resep', $mr_resep);
                        // echo $this->db->trans_status(); die;
                        $result = $this->db->trans_status();
                    } else {
                        $this->id_mrresep = $query->row()->id_mrresep;
                        $result = true;
                    }

                    if ($result > 0 || $result === true) {
                        $takaran       = "";
                        $kd_unit_far   = "";
                        $kd_milik      = 0;
                        $catatan_racik = "";
                        $status = 0;
                        if (isset($value->urut) == true) {
                            if ($value->urut == 0 || $value->urut == '' || $value->urut == 'undefined') {
                                $urut_order = ($index + 1);
                            } else {
                                $urut_order = $value->urut;
                            }
                        } else {
                            $urut_order = ($index + 1);
                        }
                        if ($value->verified == 'Not Verified' || $value->verified == 'Not Verified') {
                            $status = 1;
                        }

                        if (isset($value->takaran) == true) {
                            $takaran = $value->takaran;
                        }
                        if (isset($value->kd_unit_far) == true) {
                            $kd_unit_far = $value->kd_unit_far;
                        }
                        if (isset($value->kd_milik) == true) {
                            $kd_milik = $value->kd_milik;
                        }
                        if (isset($value->catatan_racik) == true) {
                            $catatan_racik = $value->catatan_racik;
                        }
                        $result = $this->db->query("insertmr_resepdtl
                        @id_mrresep = " . $this->id_mrresep . "
                            ,@urut = " . $urut_order . "
                            ,@kd_prd='" . $value->kd_prd . "'
                            ,@jumlah=" . $value->jumlah . "
                            ,@cara_pakai='" . $value->cara_pakai . "'
                            ,@status=0
                            ,@kd_dokter='" . $value->kd_dokter . "'
                            ,@virified=" . $status . "
                            ,@racikan=" . $value->racikan . "
                            ,@order_mng='f'
                            ,@aturan_pakai='" . $value->aturan_pakai . "'
                            ,@aturan_racik='" . $value->aturan_racik . "'
                            ,@kd_unit_far='" . $kd_unit_far . "'
                            ,@kd_milik=" . $kd_milik . "
                            ,@no_racik=" . $value->no_racik . "
                            ,@signa='" . $value->signa . "'
                            ,@takaran='" . $takaran . "'
                            ,@jumlah_racik=" . $value->jumlah_racik . "
                            ,@satuan_racik='" . $value->satuan_racik . "'
                            ,@catatan_racik='" . $catatan_racik . "'
                        ");

                        if ($result->num_rows() > 0) {
                            $result = true;
                        } else {
                            $result = false;
                        }
                    }
                } else {
                    $result = true;
                }
                $index++;
            }
        } else {
            $result = true;
        }
        // die;
        return $result;
    }

    public function getKontraktor()
    {
        $response = array();
        $params   = array(
            'kd_customer'   => $this->input->post('kd_customer'),
        );
        $this->db->select(" * ");
        $this->db->where($params);
        $this->db->from("kontraktor");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $response['status'] = true;
            $response['data']   = $query->result();
            $response['count']  = $query->num_rows();
        } else {
            $response['status'] = false;
        }
        echo json_encode($response);
    }
}
