<?php
class bookingonline extends MX_Controller {
	private $table = "booking_online";
    public function __construct() {
        parent::__construct();
    }
	/* public function __destruct() {
		$this->db->close();
		_QMS_Closeconn();
	}  */
    public function index() {
        $this->load->view('main/index');
    }
    function read($Params = null) {
		$list=array();
        try {
			$sqldatasrv="SELECT distinct top 500 KD_PASIEN=pasien.kd_pasien, NAMA=pasien.nama, NAMA_KELUARGA=pasien.nama_keluarga,
			JENIS_KELAMIN=pasien.jenis_kelamin, TEMPAT_LAHIR=pasien.tempat_lahir, TGL_LAHIR=pasien.tgl_lahir,AGAMA=agama.agama, GOL_DARAH=pasien.gol_darah,NO_ASURANSI=pasien.no_asuransi, 
			WNI=pasien.wni, STATUS_MARITA=pasien.status_marita, ALAMAT=pasien.alamat, KD_KELURAHAN=pasien.kd_kelurahan,
			PENDIDIKAN=pendidikan.pendidikan, PEKERJAAN=pekerjaan.pekerjaan,KD_KABUPATEN=kb.kd_kabupaten, KABUPATEN=kb.KABUPATEN, KD_KECAMATAN=kc.kd_kecamatan, KECAMATAN=kc.KECAMATAN, KD_PROPINSI=pr.kd_propinsi, PROPINSI=pr.PROPINSI,
			KD_PENDIDIKAN=pasien.kd_pendidikan, KD_PEKERJAAN=pasien.kd_pekerjaan, KD_AGAMA=pasien.kd_agama, ALAMAT_KTP=pasien.alamat_ktp,NAMA_AYAH=pasien.nama_ayah,KD_PENDIDIKAN_AYAH=pasien.kd_pendidikan_ayah, PENDIDIKAN_AYAH=p1.pendidikan,KD_PEKERJAAN_AYAH=pasien.kd_pekerjaan_ayah, PEKERJAAN_AYAH=pek1.pekerjaan,
			NAMA_IBU=pasien.nama_ibu,KD_PENDIDIKAN_IBU=pasien.kd_pendidikan_ibu, PENDIDIKAN_IBU=p2.pendidikan ,KD_PEKERJAAN_IBU=pasien.kd_pekerjaan_ibu, PEKERJAAN_IBU=pek2.pekerjaan,NAMA_SUAMIISTRI=pasien.suami_istri,KD_PENDIDIKAN_SUAMIISTRI=pasien.kd_pendidikan_suamiistri, PENDIDIKAN_SUAMIISTRI=p3.pendidikan,KD_PEKERJAAN_SUAMIISTRI=pasien.kd_pekerjaan_suamiistri, PEKERJAAN_SUAMIISTRI=pek3.pekerjaan,
			KD_KELURAHAN_KTP=pasien.kd_kelurahan_ktp,KD_POS_KTP=pasien.kd_pos_ktp, KD_POS=pasien.kd_pos, PROPINSIKTP=proktp.kd_propinsi,KABUPATENKTP=kabktp.kd_kabupaten,
			KECAMATANKTP=kecktp.kd_kecamatan,
			KELURAHANKTP=kelktp.kd_kelurahan ,KELURAHAN=kl.kelurahan,
			KEL_KTP=kelktp.kelurahan ,
			KEC_KTP=kecktp.kecamatan,KAB_KTP=kabktp.kabupaten ,PRO_KTP=proktp.propinsi,
			EMAIL=pasien.email,HANDPHONE=pasien.handphone,TELEPON=pasien.telepon,hck.kd_pasien as cekkartu,pasien.NIK AS NIK
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien
			where"; 
            if (strlen($Params[4]) !== 0) {
                $kriteria=str_replace("~", "'", $Params[4] . " and pasien.kd_pasien Like '%-%'  order by pasien.kd_pasien desc --offset " . $Params[0] . "  limit 500 ");
            } else {
                $kriteria=str_replace("~", "'", " pasien.kd_pasien Like '%-%' order by pasien.kd_pasien desc   --offset " . $Params[0] . " limit 500 ");
            }
			//echo $sqldatasrv.$kriteria;
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
			foreach ($res->result() as $rec){
				$o=array();
				$selectKartuPasien = ("SELECT count(kd_pasien) as counter FROM history_cetak_kartu WHERE tgl_cetak = '".date('Y-m-d')."' AND kd_pasien='".$rec->KD_PASIEN."'");
				$counter = $dbsqlsrv->query($selectKartuPasien);
				$counter = $counter->row()->counter;
				$o['KD_PASIEN']=$rec->KD_PASIEN;
				$o['NAMA']=$rec->NAMA;
				$o['NAMA_KELUARGA']=$rec->NAMA_KELUARGA;
				$o['JENIS_KELAMIN']=$rec->JENIS_KELAMIN;
				$o['TEMPAT_LAHIR']=$rec->TEMPAT_LAHIR;
				//$o['TGL_LAHIR'] = date("d/m/Y",strtotime($rec->TGL_LAHIR));
				$o['TGL_LAHIR'] = date_format(date_create($rec->TGL_LAHIR),"d/m/Y");
				$o['AGAMA']=$rec->AGAMA;
				$o['GOL_DARAH']=$rec->GOL_DARAH;
				$o['WNI']=$rec->WNI;
				$o['STATUS_MARITA']=$rec->STATUS_MARITA;
				$o['ALAMAT']=$rec->ALAMAT;
				$o['KD_KELURAHAN']=$rec->KD_KELURAHAN;
				$o['PENDIDIKAN']=$rec->PENDIDIKAN;
				$o['PENDIDIKAN_AYAH']=$rec->PENDIDIKAN_AYAH;
				$o['PENDIDIKAN_IBU']=$rec->PENDIDIKAN_IBU;
				$o['PENDIDIKAN_SUAMIISTRI']=$rec->PENDIDIKAN_SUAMIISTRI;
				$o['PEKERJAAN']=$rec->PEKERJAAN;
				$o['PEKERJAAN_AYAH']=$rec->PEKERJAAN_AYAH;
				$o['PEKERJAAN_IBU']=$rec->PEKERJAAN_IBU;
				$o['PEKERJAAN_SUAMIISTRI']=$rec->PEKERJAAN_SUAMIISTRI;
				$o['KABUPATEN']=$rec->KABUPATEN;
				$o['KECAMATAN']=$rec->KECAMATAN;
				$o['PROPINSI']=$rec->PROPINSI;
				$o['KD_KABUPATEN']=$rec->KD_KABUPATEN;
				$o['KD_KECAMATAN']=$rec->KD_KECAMATAN;
				$o['KD_PROPINSI']=$rec->KD_PROPINSI;
				$o['KD_PENDIDIKAN']=$rec->KD_PENDIDIKAN;
				$o['KD_PENDIDIKAN_AYAH']=$rec->KD_PENDIDIKAN_AYAH;
				$o['KD_PENDIDIKAN_IBU']=$rec->KD_PENDIDIKAN_IBU;
				$o['KD_PENDIDIKAN_SUAMIISTRI']=$rec->KD_PENDIDIKAN_SUAMIISTRI;
				$o['KD_PEKERJAAN']=$rec->KD_PEKERJAAN;
				$o['KD_PEKERJAAN_AYAH']=$rec->KD_PEKERJAAN_AYAH;
				$o['KD_PEKERJAAN_IBU']=$rec->KD_PEKERJAAN_IBU;
				$o['KD_PEKERJAAN_SUAMIISTRI']=$rec->KD_PEKERJAAN_SUAMIISTRI;
				$o['KD_AGAMA']=$rec->KD_AGAMA;
				$o['ALAMAT_KTP']=$rec->ALAMAT_KTP;
				$o['NAMA_AYAH']=$rec->NAMA_AYAH;
				$o['NAMA_IBU']=$rec->NAMA_IBU;
				$o['NAMA_SUAMIISTRI']=$rec->NAMA_SUAMIISTRI;
				$o['KD_KELURAHAN_KTP']=$rec->KD_KELURAHAN_KTP;
				$o['KD_POS_KTP']=$rec->KD_POS_KTP;
				$o['KD_POS']=$rec->KD_POS;
				$o['PROPINSIKTP']=$rec->PROPINSIKTP;
				$o['KABUPATENKTP']=$rec->KABUPATENKTP;
				$o['KECAMATANKTP']=$rec->KECAMATANKTP;
				$o['KELURAHANKTP']=$rec->KELURAHANKTP;
				$o['KELURAHAN']=$rec->KELURAHAN;
				$o['KEL_KTP']=$rec->KEL_KTP;
				$o['KEC_KTP']=$rec->KEC_KTP;
				$o['KAB_KTP']=$rec->KAB_KTP;
				$o['PRO_KTP']=$rec->PRO_KTP;
				$o['NO_ASURANSI']=$rec->NO_ASURANSI;
				$o['EMAIL_PASIEN']=$rec->EMAIL;
				$o['HP_PASIEN']=$rec->HANDPHONE;
				$o['TLPN_PASIEN']=$rec->TELEPON;
				$o['NIK']=$rec->NIK;
				if ($counter > 0) {
					$o['KARTU'] = 't';
				}else{
					$o['KARTU'] = 'f';
				}
				// $o['KARTU']=$rec->cekkartu;
				$list[]=$o;	
			} 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }

	public function save($Params = null) {
		$response = array();
		$params   = array(
			'kd_pasien'     => $Params['NoMedrec'],
			'kd_unit'       => $Params['Poli'],
			'tgl_booking'   => date('Y-m-d H:i:s'),
			'tgl_kunjungan' => $Params['TanggalMasuk'],
			'kd_dokter'     => $Params['KdDokter'],
			'kd_customer'   => $Params['KdCustomer'],
			'nomor_skdp'   	=> $Params['nomor_skdp'],
		);
		$nama_unit = "";


		$criteria = array(
			'kd_pasien'   => $params['kd_pasien'],
			'kd_unit'     => $params['kd_unit'],
			'tgl_booking' => $params['tgl_booking'],
		);

		$query = $this->get_data(" * ", "unit", array('kd_unit' => $params['kd_unit']) );
		if ($query->num_rows()>0) {
			$nama_unit = $query->row()->nama_unit;
		}

		$query = $this->get_data(" * ", $this->table, $criteria );
		if ($query->num_rows() > 0) {
			$response['message'] = "Pasien sudah booking pada tanggal ".$params['tgl_kunjungan']." di poli ".$nama_unit;
			$response['status']  = false;
		}else{
			$query = $this->get_data(" max(antrian) as antrian ", $this->table, array( 'kd_unit' => $params['kd_unit'],  'tgl_kunjungan' => $params['tgl_kunjungan'], ) );
			if ($query->num_rows() > 0) {
				$params['antrian']   = (int)$query->row()->antrian + 1;
			}else{
				$params['antrian']   = 1;
			}

			$query = $this->get_data(" max(urut_masuk) as urut_masuk ", $this->table, $criteria);
			

			if ($query->num_rows() > 0) {
				$params['urut_masuk']   = (int)$query->row()->urut_masuk + 1;
				$criteria['urut_masuk'] = (int)$query->row()->urut_masuk + 1;
			}else{
				$params['urut_masuk']   = 1;
				$criteria['urut_masuk'] = 1;
			}

			$query = $this->get_data("*", $this->table, $criteria);
			if ($query->num_rows() > 0) {
				$response['message'] = "Pasien sudah booking pada tanggal ".$params['tgl_kunjungan']." di poli ".$nama_unit;
				$response['status']  = false;
			}else{
				$params['kunjungan'] = 'false';
				$this->db->insert($this->table, $params);
				if ($this->db->affected_rows() > 0) {
					$response['status'] = true;
				}else{
					$response['status'] = false;
				}
	    	}
		}

    	if ($response['status'] === true || $response['status'] > 0) {
			$response['KD_PASIEN'] = $params['kd_pasien'];
			$response['message']   = "Berhasil booking";
			$response['success']   = true;
    	}else{
    		$response['success'] = false;
    	}
    	$this->db->close();
    	echo json_encode($response);
    }

    public function getDataKunjungan(){
    	$params = array(
    		'kd_pasien' 	=> $this->input->post('kd_pasien'),
    		'kd_unit' 		=> $this->input->post('kd_unit'),
    		'tgl_kunjungan'	=> $this->input->post('tgl_kunjungan'),
    		'urut_masuk' 	=> $this->input->post('urut_masuk'),
    	);
		$sql="SELECT distinct pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, 
			pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir,agama.agama, 
			pasien.gol_darah,pasien.no_asuransi, pasien.wni, pasien.status_marita, 
			pasien.alamat, pasien.kd_kelurahan, pendidikan.pendidikan, 
			pekerjaan.pekerjaan,kb.kd_kabupaten, kb.KABUPATEN, 
			kc.kd_kecamatan, kc.kecamatan, pr.kd_propinsi, 
			pr.PROPINSI, pasien.kd_pendidikan, pasien.kd_pekerjaan, 
			pasien.kd_agama, pasien.alamat_ktp,pasien.nama_ayah,
			pasien.kd_pendidikan_ayah, p1.pendidikan as pendidikan_ayah,
			pasien.kd_pekerjaan_ayah, pek1.pekerjaan as pekerjaan_ayah, pasien.nama_ibu,
			pasien.kd_pendidikan_ibu, p2.pendidikan as pendidikan_ibu ,pasien.kd_pekerjaan_ibu, 
			pek2.pekerjaan as pekerjaan_ibu,pasien.suami_istri as nama_suaimiistri,
			pasien.kd_pendidikan_suamiistri, p3.pendidikan as pendidikan_suamiistri,
			pasien.kd_pekerjaan_suamiistri, pek3.pekerjaan as pekerjaan_suamiistri, 
			pasien.kd_kelurahan_ktp,pasien.kd_pos_ktp, pasien.kd_pos, 
			proktp.kd_propinsi as propinsiktp,kabktp.kd_kabupaten as kabupatenktp, kecktp.kd_kecamatan as kecamatanktp, 
			kelktp.kd_kelurahan as kelurahanktp ,kl.kelurahan,kelktp.kelurahan as kel_ktp, 
			kecktp.kecamatan as kec_ktp,kabktp.kabupaten as kab_ktp ,proktp.propinsi as pro_ktp, pasien.email,
			pasien.handphone,pasien.telepon,hck.kd_pasien as cekkartu,pasien.NIK,
			pasien.pemegang_asuransi	as nama_peserta	
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan 
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan 
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan 
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp 
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten 
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien 
			where pasien.kd_pasien='".$_POST['command']."'
			order by pasien.kd_pasien desc limit 1";
		$rec=$this->db->query($sql)->row();	
		$recPenyakit=$this->db->query("select p.kd_penyakit,p.penyakit,m.tgl_masuk,
			case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
			from mr_penyakit m
			inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
			where kd_pasien='".$_POST['command']."'  and m.stat_diag in(0,1) order by tgl_masuk ASC")->result();
		$o=array();
		$o['RIWAYAT_PENYAKIT']         = $recPenyakit;
		$o['KD_PASIEN']                = $rec->kd_pasien;
		$o['NAMA']                     = $rec->nama;
		$o['NAMA_KELUARGA']            = $rec->nama_keluarga;
		$o['JENIS_KELAMIN']            = $rec->jenis_kelamin;
		$o['TEMPAT_LAHIR']             = $rec->tempat_lahir;
		$o['TGL_LAHIR']                = date("d/m/Y",strtotime($rec->tgl_lahir));
		$o['AGAMA']                    = $rec->agama;
		$o['GOL_DARAH']                = $rec->gol_darah;
		$o['WNI']                      = $rec->wni;
		$o['STATUS_MARITA']            = $rec->status_marita;
		$o['ALAMAT']                   = $rec->alamat;
		$o['KD_KELURAHAN']             = $rec->kd_kelurahan;
		$o['PENDIDIKAN']               = $rec->pendidikan;
		$o['PENDIDIKAN_AYAH']          = $rec->pendidikan_ayah;
		$o['PENDIDIKAN_IBU']           = $rec->pendidikan_ibu;
		$o['PENDIDIKAN_SUAMIISTRI']    = $rec->pendidikan_suamiistri;
		$o['PEKERJAAN']                = $rec->pekerjaan;
		$o['PEKERJAAN_AYAH']           = $rec->pekerjaan_ayah;
		$o['PEKERJAAN_IBU']            = $rec->pekerjaan_ibu;
		$o['PEKERJAAN_SUAMIISTRI']     = $rec->pekerjaan_suamiistri;
		$o['KABUPATEN']                = $rec->kabupaten;
		$o['KECAMATAN']                = $rec->kecamatan;
		$o['PROPINSI']                 = $rec->propinsi;
		$o['KD_KABUPATEN']             = $rec->kd_kabupaten;
		$o['KD_KECAMATAN']             = $rec->kd_kecamatan;
		$o['KD_PROPINSI']              = $rec->kd_propinsi;
		$o['KD_PENDIDIKAN']            = $rec->kd_pendidikan;
		$o['KD_PENDIDIKAN_AYAH']       = $rec->kd_pendidikan_ayah;
		$o['KD_PENDIDIKAN_IBU']        = $rec->kd_pendidikan_ibu;
		$o['KD_PENDIDIKAN_SUAMIISTRI'] = $rec->kd_pendidikan_suamiistri;
		$o['KD_PEKERJAAN']             = $rec->kd_pekerjaan;
		$o['KD_PEKERJAAN_AYAH']        = $rec->kd_pekerjaan_ayah;
		$o['KD_PEKERJAAN_IBU']         = $rec->kd_pekerjaan_ibu;
		$o['KD_PEKERJAAN_SUAMIISTRI']  = $rec->kd_pekerjaan_suamiistri;
		$o['KD_AGAMA']                 = $rec->kd_agama;
		$o['ALAMAT_KTP']               = $rec->alamat_ktp;
		$o['NAMA_AYAH']                = $rec->nama_ayah;
		$o['NAMA_IBU']                 = $rec->nama_ibu;
		$o['NAMA_SUAMIISTRI']          = $rec->nama_suaimiistri;
		$o['KD_KELURAHAN_KTP']         = $rec->kd_kelurahan_ktp;
		$o['KD_POS_KTP']               = $rec->kd_pos_ktp;
		$o['KD_POS']                   = $rec->kd_pos;
		$o['PROPINSIKTP']              = $rec->propinsiktp;
		$o['KABUPATENKTP']             = $rec->kabupatenktp;
		$o['KECAMATANKTP']             = $rec->kecamatanktp;
		$o['KELURAHANKTP']             = $rec->kelurahanktp;
		$o['KELURAHAN']                = $rec->kelurahan;
		$o['KEL_KTP']                  = $rec->kel_ktp;
		$o['KEC_KTP']                  = $rec->kec_ktp;
		$o['KAB_KTP']                  = $rec->kab_ktp;
		$o['PRO_KTP']                  = $rec->pro_ktp;
		$o['NO_ASURANSI']              = $rec->no_asuransi;
		$o['EMAIL_PASIEN']             = $rec->email;
		$o['HP_PASIEN']                = $rec->handphone;
		$o['NAMA_PESERTA']                = $rec->nama_peserta;
		$o['TLPN_PASIEN']              = $rec->telepon;
		$o['NIK']              = $rec->nik;
		$o['KD_CUSTOMER']              = '';
		$o['NAMA_CUSTOMER']            = '';
		$o['JENIS_CUST']               = '';
		$o['KD_UNIT']                  = '';
		$o['NAMA_UNIT']                = '';
		$o['KD_PENYAKIT']              = '';
		$o['NAMA_PENYAKIT']            = '';
		$sqlKun = "SELECT kunjungan.kd_customer AS KD_CUSTOMER,customer.customer AS NAMA_CUSTOMER,kontraktor.jenis_cust AS JENIS_CUST,
			unit.kd_unit AS KD_UNIT,unit.nama_unit AS NAMA_UNIT,m.kd_penyakit,p.penyakit,
			r.kd_rujukan,r.rujukan,ra.cara_penerimaan,penerimaan 			
			FROM kunjungan 
			inner join customer ON kunjungan.kd_customer=customer.kd_customer
			inner join kontraktor ON kontraktor.kd_customer=kunjungan.kd_customer
			inner join unit ON unit.kd_unit=kunjungan.kd_unit
			left join mr_penyakit m ON m.kd_pasien=kunjungan.kd_pasien AND m.kd_unit=kunjungan.kd_unit AND m.tgl_masuk=kunjungan.tgl_masuk AND  m.urut_masuk=kunjungan.urut_masuk AND stat_diag=0
			left join penyakit p ON p.kd_penyakit=m.kd_penyakit
			left join rujukan r ON r.kd_rujukan=kunjungan.kd_rujukan
			left join rujukan_asal ra ON r.cara_penerimaan=ra.cara_penerimaan ::int8
			WHERE kunjungan.kd_pasien='".$_POST['command']."' AND left(kunjungan.kd_unit,1)='2' ORDER BY kunjungan.tgl_masuk DESC,kunjungan.jam_masuk DESC Limit 1";
		$recKun=$this->db->query($sqlKun)->row();
		if($recKun){
			$o['KD_CUSTOMER']     = $recKun->kd_customer;
			$o['NAMA_CUSTOMER']   = $recKun->nama_customer;
			$o['JENIS_CUST']      = $recKun->jenis_cust;
			$o['KD_UNIT']         = $recKun->kd_unit;
			$o['NAMA_UNIT']       = $recKun->nama_unit;
			$o['KD_PENYAKIT']     = $recKun->kd_penyakit;
			$o['NAMA_PENYAKIT']   = $recKun->kd_penyakit.' - '.$recKun->penyakit;
			$o['KD_RUJUKAN']      = $recKun->kd_rujukan;
			$o['NAMA_RUJUAKAN']   = $recKun->rujukan;
			$o['KD_PENERIMAAN']   = $recKun->cara_penerimaan;
			$o['NAMA_PENERIMAAN'] = $recKun->penerimaan;
		}

		echo json_encode($o);
    }

    public function delete(){
    	$response = array();
    	$criteria = array(
			'kd_pasien'   			=> $this->input->post('kd_pasien'),
			//'tgl_booking' => $this->input->post('tgl_booking'),
			'DATE(tgl_booking)' 	=> date('Y-m-d', strtotime($this->input->post('tgl_booking'))),
			'urut_masuk'  			=> $this->input->post('urut_masuk'),
			'kd_unit'     			=> $this->input->post('kd_unit'),
    	);

    	$this->db->where($criteria);
    	$this->db->delete($this->table);
    	if ($this->db->affected_rows() > 0) {
    		$response['status'] = true;
    	}else{
    		$response['status'] = false;
    	}
    	echo json_encode($response);
    }

    private function get_data($select, $table, $criteria = null, $limit = null){
    	$this->db->select($select);
    	$this->db->from($table);
    	if ($criteria != null) {
    		$this->db->where($criteria);
    	}
    	if ($limit != null) {
    		$this->db->limit($limit);
    	}
    	return $this->db->get();
    }
}
?>			