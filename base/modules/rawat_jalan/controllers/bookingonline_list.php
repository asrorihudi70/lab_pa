<?php
class bookingonline_list extends MX_Controller {
	private $table = "booking_online";
    public function __construct() {
        parent::__construct();
    }
	/* public function __destruct() {
		$this->db->close();
		_QMS_Closeconn();
	}  */
    public function index() {
        $this->load->view('main/index');
    }
    function read($Params = null) {
        try{
			$tgl=date('M-d-Y');
			$this->load->model('rawat_jalan/tbl_bookingonline');
			if (strlen($Params[4])!==0){
				$this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
			}
			$res = $this->tbl_bookingonline->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
        }catch(Exception $o){ 
			echo '{success: false}';
		}
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }

	public function save($Params = null) {
		$response = array();
		$params   = array(
			'kd_pasien'     => $Params['NoMedrec'],
			'kd_unit'       => $Params['Poli'],
			'tgl_booking'   => date('Y-m-d'),
			'tgl_kunjungan' => $Params['TanggalMasuk'],
			'kd_dokter'     => $Params['KdDokter'],
		);


		$criteria = array(
			'kd_pasien'   => $params['kd_pasien'],
			'kd_unit'     => $params['kd_unit'],
			'tgl_booking' => $params['tgl_booking'],
		);

		$query = $this->get_data(" max(urut_masuk) as urut_masuk ", $this->table, $criteria);
		if ($query->num_rows() > 0) {
			$params['urut_masuk']   = (int)$query->row()->urut_masuk + 1;
			$criteria['urut_masuk'] = (int)$query->row()->urut_masuk + 1;
		}else{
			$params['urut_masuk']   = 1;
			$criteria['urut_masuk'] = 1;
		}

		$query = $this->get_data("*", $this->table, $criteria);
		if ($query->num_rows() > 0) {
			$response['message'] = "Pasien sudah booking";
			$response['status']  = false;
		}else{
			$params['kunjungan'] = 'false';
			$this->db->insert($this->table, $params);
			if ($this->db->affected_rows() > 0) {
				$response['status'] = true;
			}else{
				$response['status'] = false;
			}
    	}

    	if ($response['status'] === true || $response['status'] > 0) {
			$response['message'] = "Berhasil booking";
    		$response['success'] = true;
    	}else{
    		$response['success'] = false;
    	}
    	$this->db->close();
    	echo json_encode($response);
    }

    private function get_data($select, $table, $criteria = null, $limit = null){
    	$this->db->select($select);
    	$this->db->from($table);
    	if ($criteria != null) {
    		$this->db->where($criteria);
    	}
    	if ($limit != null) {
    		$this->db->limit($limit);
    	}
    	return $this->db->get();
    }
}
?>			