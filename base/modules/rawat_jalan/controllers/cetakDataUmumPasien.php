<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cetakDataUmumPasien extends MX_Controller {
    private $setup_db_sql = false;
    public function __construct() {
        parent::__construct();
        $this->load->library('session', 'url');
    }

    public function cetak() { 
        $nomor      = $this->uri->segment(4, 0);
        $kd_pasien  = explode("-",$nomor);
        $kd_pasien2 = implode("",$kd_pasien);

        if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
            $nama_pas = _QMS_Query(" SELECT p.kd_pasien, p.nama, p.TGL_LAHIR,p.JENIS_KELAMIN, p.STATUS_HIDUP, p.STATUS_MARITA,p.ALAMAT,p.kota,p.WNI,p.TEMPAT_LAHIR, p.ALAMAT_KTP, p.BAHASA, p.EMAIL,p.NIK kelurahan.kelurahan FROM pasien p
                    INNER JOIN KELURAHAN ON p.KD_KELURAHAN_KTP = kelurahan.kd_kelurahan
                    INNER JOIN PENDIDIKAN ON p.kd_pendidikan = pendidikan.kd_pendidikan
                    INNER JOIN PEKERJAAN ON p.KD_PEKERJAAN = pekerjaan.KD_PEKERJAAN
                    INNER JOIN AGAMA ON p.kd_agama = agama.kd_agama
                                    WHERE kd_pasien = '" . $nomor . "'");
        }else{
            $nama_pas = $this->db->query("SELECT p.kd_pasien, p.nama, p.TGL_LAHIR,p.JENIS_KELAMIN, p.STATUS_HIDUP, p.STATUS_MARITA,p.ALAMAT,p.kota,p.WNI,p.TEMPAT_LAHIR, 
            p.ALAMAT_KTP, p.BAHASA, p.EMAIL,p.NIK, kelurahan.kelurahan FROM pasien p
                    INNER JOIN KELURAHAN ON p.KD_KELURAHAN_KTP = kelurahan.kd_kelurahan
                    INNER JOIN PENDIDIKAN ON p.kd_pendidikan = pendidikan.kd_pendidikan
                    INNER JOIN PEKERJAAN ON p.KD_PEKERJAAN = pekerjaan.KD_PEKERJAAN
                    INNER JOIN AGAMA ON p.kd_agama = agama.kd_agama WHERE kd_pasien = '" . $nomor . "'");
        }
        
        if (count($nama_pas->result()) > 0) {
            // echo "<pre>".var_export($nama_pas2)
            $nama_pas_2 = $nama_pas->row();
            $nama 		= $nama_pas_2->nama;
            $kd_pasien 	= $nama_pas_2->kd_pasien;
            $tgl_lahir 		= $nama_pas_2->TGL_LAHIR;
            $jenis_kelamin 		= $nama_pas_2->JENIS_KELAMIN;
            $status_hidup 		= $nama_pas_2->STATUS_HIDUP;
            $status_marita 		= $nama_pas_2->STATUS_MARITA;
            $alamat 		= $nama_pas_2->ALAMAT;
            $kota		= $nama_pas_2->kota;
            $wni 		= $nama_pas_2->WNI;
            $tempat_lahir 		= $nama_pas_2->TEMPAT_LAHIR;
            $alamat_ktp 		= $nama_pas_2->ALAMAT_KTP;
            $bahasa 		= $nama_pas_2->BAHASA;
            $email 		= $nama_pas_2->EMAIL;
            $nik 		= $nama_pas_2->NIK;
            $kelurahan 		= $nama_pas_2->kelurahan;
        }
            
        if (strlen($nama) >= 22) {
            $nama = substr($nama, 0, 19)." ...";
        }else{
            $nama;
        }
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $kertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'kertasLabelpasien'")->row()->setting;
        $heightKertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'heightkertasLabelpasien'")->row()->setting;
        $widthKertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'widthkertasLabelpasien'")->row()->setting;

        $mpdf = new mPDF(
            'utf-8',    // mode - default ''
            // '21cm 29.7cm',    // format - A4, for example, default ''
            array(356, 216),    // format - A4, for example, default ''
            // array(200, 140),    // format - A4, for example, default ''
            // array( $widthKertas, $heightKertas),    // format - A4, for example, default ''
            0,     // font size - default 0
            '',    // default font family
            0,    // margin_left
            0,    // margin right
            0,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            $kertas);  // L - landscape, P - portrait
        // $mpdf      = new mPDF('', array(188, 23), '', '', 1, 2, 2, 2, 5, 5, 'L');
        //$mpdf = new mPDF('', array(66, 27), '', '', 3, 3, 3, 3, 5, 5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle('Kartu Kunjungan');
        $mpdf->WriteHTML("
        <style>

         p
        {
        font-family:arial;
        font-size:11px !important;
        line-height:1px;
        font-weigth:bolder;
        }

        .barcode {
        float:left;
        position:relative;
        margin-left:-15px;
        margin-top:-5px;

        }

        </style>
        ");
        $mpdf->WriteHTML('<html>');
        $mpdf->WriteHTML('<body>');
        for ($i=1; $i <= 1; $i++) { 
// style="padding-bottom:5px;padding-top:5px;"
        $logo = base_url().'ui/images/Logo/LOGO_.jpg';
        // echo $logo; die;
        $mpdf->WriteHTML('
        <table cellspacing="0" border="0">
                <colgroup width="176"></colgroup>
                <colgroup width="15"></colgroup>
                <colgroup width="91"></colgroup>
                <colgroup width="18"></colgroup>
                <colgroup width="72"></colgroup>
                <colgroup width="79"></colgroup>
                <colgroup width="68"></colgroup>
                <colgroup width="81"></colgroup>
                <colgroup width="62"></colgroup>
                <colgroup width="72"></colgroup>
                <colgroup width="48"></colgroup>
                <tr>
                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=3 height="60" align="center" valign=middle><b><font color="#000000">LOGO</font></b></td>
                    <td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=5 align="left" valign=middle><b><font color="#000000">RUMAH SAKIT SUAKA INSAN</font></b></td>
                    <td style="border-top: 1px solid #000000; border-left: 1px solid #000000" colspan=4 align="center" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td style="border-top: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=5 align="left" valign=middle><b><font color="#000000">Jl. Zafri Zam-zam No. 60 Banjarmasin</font></b></td>
                    <td style="border-left: 1px solid #000000" align="left" valign=middle><b><font color="#000000">NRM</font></b></td>
                    <td colspan=3 align="left" valign=middle><b><font color="#000000">:  KD_PASIEN</font></b></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=5 align="left" valign=middle><b><font color="#000000">Telp. (0511) 3353335 Fax (0511) 3355121</font></b></td>
                    <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000" colspan=3 align="center" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font color="#000000">RM 1.1</font></b></td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" colspan=10 height="21" align="center" valign=middle><b><font size=3 color="#000000">DATA UMUM PASIEN</font></b></td>
                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Nama</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=8 align="left" valign=middle><font color="#000000">Nama_pasien</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Patient name</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><b><font color="#000000">Lk/Male</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br><img src="result_htm_2be2354789c64a24.png" width=18 height=12 hspace=31 vspace=2>
                    </font></td>
                    <td align="left" valign=middle><b><font color="#000000">Pr/Female</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br><img src="result_htm_d4e927a5789ddd56.png" width=14 height=9 hspace=29 vspace=4>
                    </font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Tempat &amp; Tanggal Lahir</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=8 align="left" valign=middle><font color="#000000">TTL</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Place &amp; Date of birth</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><b><font color="#000000">Umur / Age</font></b></td>
                    <td colspan=4 align="left" valign=middle><font color="#000000">: UMUR</font></td>
                    <td align="left" valign=middle><b><font color="#000000">Th-Bl-Hr/Year-Month-Day</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Agama</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><b><font color="#000000">ISLAM<img src="result_htm_2be2354789c64a24.png" width=18 height=12 hspace=36 vspace=2>
                    </font></b></td>
                    <td colspan=2 align="left" valign=middle><b><font color="#000000">Kristen<img src="result_htm_2be2354789c64a24.png" width=18 height=12 hspace=36 vspace=2>
                    </font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Katholik</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Hindu</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Budha</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Lainnya</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Religion</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Moslem</font></i></b></td>
                    <td colspan=2 align="left" valign=middle><b><i><font color="#000000">Christian</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Chatolik</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Hindu</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Budha</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Other</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Status Perkawinan</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><b><font color="#000000">Kawin box</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Tidak/Belum Kawin</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Janda</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Duda</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Martial Status</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Kewarganegraan </font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=8 align="left" valign=middle><font color="#000000">KEWARGANEGARAAN</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Civil Status</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Pendidikan Terakhir</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><b><font color="#000000">Belum/TIdak Sekolah</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">SD</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">SLTP</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">SLTA</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">D3</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">S1</font></b></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Latest Education</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Not/No School</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Elementary</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Junior</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">High Senior</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Diploma</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Bachelor</font></i></b></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Pekerjaan</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=8 align="left" valign=middle><font color="#000000">PEKERJAAN</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Occupation</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Alamat sesuai KTP</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=8 align="left" valign=middle><font color="#000000">ALAMAT</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Address based on ID</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><b><font color="#000000">RT</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">RW</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">NO</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><font color="#000000">No Handphone</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=6 align="left" valign=middle><font color="#000000">NO_HP</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><i><font color="#000000">Mobile Phone Number</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><font color="#000000">Email</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=6 align="left" valign=middle><font color="#000000">EMAIL</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><font color="#000000">Kelurahan</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=6 align="left" valign=middle><font color="#000000">KELURAHAN</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><i><font color="#000000">Village</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><font color="#000000">Kecamatan</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=6 align="left" valign=middle><font color="#000000">KECAMATAN</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><i><font color="#000000">Subcistric</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><font color="#000000">Kabupaten/Kota</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td colspan=6 align="left" valign=middle><font color="#000000">KABUPATEN</font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=2 align="left" valign=middle><b><i><font color="#000000">Distric/City</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Alamat yang mudah dihubungi</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><font color="#000000">ALAMAT_SDR</font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Address than can early be reached</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">No. Telepon/HP</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><font color="#000000">NO_HP_SDR</font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Phone/Mobile Phone Number</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Email</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><font color="#000000">EMAIL_SDR</font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Nama orangtua/suami/istri</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><font color="#000000">NAMA_ORANGGTUA</font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Parents/Husband/Wife name</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Pekerjaan</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><font color="#000000">PEKERJAAN_SDR</font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Occupation</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Bahasa sehari-hari</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><b><font color="#000000">Indonesia</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Daerah</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Asing</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Coloquially</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Indonesia</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Vernacular</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Foreign</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><font color="#000000">Kekhususan</font></b></td>
                    <td align="left" valign=middle><font color="#000000">:</font></td>
                    <td align="left" valign=middle><b><font color="#000000">Kereta Dorong</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Kursi Dorong</font></b></td>
                    <td align="left" valign=middle><b><font color="#000000"><br></font></b></td>
                    <td align="left" valign=middle><b><font color="#000000">Lainnya</font></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><b><i><font color="#000000">Spesificity</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Puschart</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Puschair</font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000"><br></font></i></b></td>
                    <td align="left" valign=middle><b><i><font color="#000000">Other</font></i></b></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=5 align="left" valign=middle><b><font color="#000000">Banjarmasin, </font></b></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="17" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=5 align="left" valign=middle><b><font color="#000000">(        NAMA_PETUGAS_PENDAFTARAN           )</font></b></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="16" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td colspan=5 align="left" valign=middle><b><i><font color="#000000">Tandatangan dan Nama Petugas Pendaftaran</font></i></b></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid #000000" height="16" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000" height="16" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                    <td style="border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font color="#000000"><br></font></td>
                </tr>
            </table>
        ');

        }
       $mpdf->WriteHTML('</body>');
       $mpdf->WriteHTML('</html>');

        $mpdf->WriteHTML(utf8_encode($html));
        $mpdf->Output("cetak.pdf", 'I');
        exit;
    }
}
