<?php
class cetak_02 extends MX_Controller {
    private $setup_db_sql = false;
    public function __construct() {
        parent::__construct();
        $this->load->library('session', 'url');
    }
	public function kontrol(){
		$common=$this->common;
		$common->foot=false;
		$kd_pasien=$_GET['kd_pasien'];
		$kd_unit=$_GET['kd_unit'];
		$tgl_masuk=$_GET['tgl_masuk'];
		$urut_masuk=$_GET['urut_masuk'];
		$alasan=$_GET['alasan'];
		$selanjutnya=$_GET['selanjutnya'];
		$tgl_kembali=new DateTime($_GET['tgl_kembali']);
		$tgl_rujuk=new DateTime($_GET['tgl_rujuk']);
   		$title='SURAT KETERANGAN KONTROL';
		$row = $this->db->query("SELECT nama FROM pasien WHERE kd_pasien='".$kd_pasien."'")->row();
		$html='';	
		if ($row){
			$diagnosa='';
			$diag=$this->db->query("SELECT TOP 1 penyakit FROM mr_penyakit M 
				INNER JOIN penyakit P ON P.kd_penyakit=M.kd_penyakit
				WHERE M.kd_pasien='".$kd_pasien."' AND M.kd_unit='".$kd_unit."' AND M.tgl_masuk='".$tgl_masuk."' 
					AND M.urut_masuk=".$urut_masuk." AND stat_diag=1")->row();
			if($diag){
				$diagnosa=$diag->penyakit;
			}
			$obat='';
			$obt=$this->db->query("SELECT nama_obat FROM mr_resep M 
				INNER JOIN mr_resepdtl P ON P.id_mrresep=M.id_mrresep
				INNER JOIN apt_obat O ON O.kd_prd=P.kd_prd
				WHERE M.kd_pasien='".$kd_pasien."' AND M.kd_unit='".$kd_unit."' AND M.tgl_masuk='".$tgl_masuk."' 
					AND M.urut_masuk=".$urut_masuk."")->result();
			for($i=0,$iLen=count($obt); $i<$iLen;$i++){
				if($obat !=''){
					$obat.=', ';
				}
				$obat.=$obt[$i]->nama_obat;
			}
			$dokter='';
			$dok=$this->db->query("
				SELECT  P.nama FROM kunjungan M 
				INNER JOIN dokter P ON P.kd_dokter=M.kd_dokter
				WHERE M.kd_pasien='".$kd_pasien."' AND M.kd_unit='".$kd_unit."' AND M.tgl_masuk='".$tgl_masuk."' 
					AND M.urut_masuk=".$urut_masuk."")->row();
			if($dok){
				$dokter=$dok->nama;
			}
			$html.='<table  style="width:800px;padding:10px 0;">
				<tr>
					<td colspan="3" align="center"><b><u>SURAT KETERANGAN KONTROL</u></b></td>
				</tr>
				<tr>
					<td width="120">No. RM</td>
					<td colspan="2">: '.$kd_pasien.'</td>
				</tr>
				<tr>
					<td width="120">Nama</td>
					<td colspan="2">: '.$row->nama.'</td>
				</tr>
				<tr>
					<td width="120">Diagnosa</td>
					<td colspan="2">: '.$diagnosa.'</td>
				</tr>
				<tr>
					<td width="120">Terapi</td>
					<td colspan="2">: '.$obat.'</td>
				</tr>
				<tr>
					<td width="120">Tanggal Surat Rujukan</td>
					<td colspan="2">: '.$tgl_rujuk->format('d M Y').'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">Belum dapat di kembalikan ke Fasilitas Perujuk dengan alasan :</td>
				</tr>
				<tr>
					<td colspan="3">'.$alasan.'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">Rencana tindak lanjut yang akan di lakukan pada kunjungan selanjutnya :</td>
				</tr>
				<tr>
					<td colspan="3">'.$selanjutnya.'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">Surat Keterangan ini digunakan untuk 1 (satu) kali kunjungan dengan diagnose di atas pada Tanggal <b>'.$tgl_kembali->format('d M Y').'</b></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td>Padang, '.$tgl_rujuk->format('d M Y').'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td>('.$dokter.')</td>
				</tr>
			</table>
			';
		}else{
			$html.='<div align="center"><h2>Data kosong</h2></div>';	
		}
		$html.='</body></html>';				
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('L',$title,$html,array('type'=>'A6','margin-top'=>4,'margin-left'=>4,'margin-right'=>4));
	}
    public function cetak() {
        $nomor      = $this->uri->segment(4, 0);
        $kd_pasien  = explode("-",$nomor);
        $kd_pasien2 = implode("",$kd_pasien);

        if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
            $nama_pas = _QMS_Query(" SELECT nama,tgl_lahir from pasien WHERE kd_pasien = '" . $nomor . "'");
        }else{
            $nama_pas = $this->db->query(" SELECT nama,tgl_lahir from pasien WHERE kd_pasien = '" . $nomor . "'");
        }
        
        if (count($nama_pas->result()) > 0) {
            $nama_pas_2 = $nama_pas->row();
            $nama 		= $nama_pas_2->nama;
            $tgllahir 	= $nama_pas_2->tgl_lahir;
        }
        if (strlen($nama) >= 22) {
            $nama = substr($nama, 0, 19)." ...";
        }else{
            $nama;
        }
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tgllahir2 = date_create($tgllahir);
        $mpdf      = new mPDF('', array(125, 23), '', '', 1, 2, 2, 2, 5, 5);
        //$mpdf = new mPDF('', array(66, 27), '', '', 3, 3, 3, 3, 5, 5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle('Kartu Pasien');
        $mpdf->WriteHTML("
        <style>

         p
        {
        font-family:arial;
        font-size:11px !important;
        line-height:1px;
        font-weigth:bolder;
        }

        .barcode {
        float:left;
        position:relative;
        margin-left:-15px;
        margin-top:-5px;

        }

        </style>
        ");
       $mpdf->WriteHTML('<html>
							<body>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center"><font style="font-size:13px;font-family:arial;"><b>' . $nama . '</b></font></td>
                                        <td align="center"><font style="font-size:13px;font-family:arial;"><b>' . $nama . '</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height:25px;"><font style="font-size:12px;font-family:arial;">' . date_format($tgllahir2,"d/m/Y") . '</font></td>
                                        <td align="center" style="height:25px;"><font style="font-size:12px;font-family:arial;">' . date_format($tgllahir2,"d/m/Y") . '</font></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><barcode code='.$kd_pasien2.' type="C128B" class="barcode" size="0.900" height="0.50"/></td>
                                        <td align="center"><barcode code='.$kd_pasien2.' type="C128B" class="barcode" size="0.900" height="0.50"/></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font style="font-size:16px; font-family:arial;"><b>' . $nomor . '</b></font></td>
                                        <td align="center"><font style="font-size:16px; font-family:arial;"><b>' . $nomor . '</b></font></td>
                                    </tr>
                                </table></body></html>'); 
		/* $mpdf->WriteHTML('<html>
							<body>
								<p align="center"><b>Aditya Iqbal</b>'
                . '</body></html>'); */

        $mpdf->WriteHTML(utf8_encode($html));//
        $mpdf->Output("cetak.pdf", 'I');
        exit;
        //}
    }

}
