<?php
class cetak_bill_kwitansi extends MX_Controller {
    private $no_transaksi = "";
    private $kd_kasir     = "";
    private $file         = "";
    private $module       = "";
    private $bold_open    = "";
    private $bold_close   = "";
    private $condensed1   = "";
    private $kd_user      = "";

    public function __construct() {
        parent::__construct();
        $this->load->library('session', 'url');
        $query = $this->db->query("SELECT * FROM sys_setting where key_data = 'default_kd_kasir_rwj'");
        if ($query->num_rows() > 0) {
            $this->kd_kasir = $query->row()->setting;
        }
        $this->kd_user    = $this->session->userdata['user_id']['id'];
        $this->file       = "data_billing_rwj.txt";
        $this->module     = "Rawat Jalan";
        $this->bold_open  = Chr(27).Chr(69);
        $this->bold_close = Chr(27).Chr(70);
        $this->condensed1 = chr(15);
    }
    public function cetak(){
        $response = array();
    	$params = array(
            'kwitansi'    => $this->input->post('kwitansi'),
            'billing'     => $this->input->post('billing'),
            'data_produk' => json_decode($this->input->post('data_produk')),
        );
   		$this->no_transaksi = $this->input->post('no_transaksi');

        if ($params['kwitansi'] == 'true' && $params['billing'] == 'true') {
            $response['billing']  = $this->cetak_billing($params);
            $response['kwitansi'] = $this->cetak_kwitansi($params);
        }else if($params['kwitansi'] == 'false' && $params['billing'] == 'true'){
            $response['billing']  = $this->cetak_billing($params);
            $response['kwitansi'] = false;
        }else if($params['kwitansi'] == 'true' && $params['billing'] == 'false'){
            $response['kwitansi'] = $this->cetak_kwitansi($params);
            $response['billing'] = false;
        }

        if ($response['kwitansi'] === true || $response['billing'] === true) {
            if ($response['kwitansi'] === true) {
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                    copy($file, $printer);  # Lakukan cetak
                    unlink($file);
                    # printer windows -> nama "printer" di komputer yang sharing printer
                } else{
                    shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
                }
            }
            $response['status'] = true;
        }
        echo json_encode($response);
    }

    private function cetak_billing($params){
        $data_rs = array();
        $data_rs = $this->data_rs();
        $tp      = new TableText(132,9,'',0,false);
        $setpage = new Pilihkertas;

        $transaksi = $this->get_transaksi( array( 'kd_kasir' => $this->kd_kasir,  'no_transaksi' => $this->no_transaksi, ) );
        if ($transaksi->num_rows() > 0 && count($params['data_produk']) > 0) {
            $tp ->addColumn(chr(27).chr(33).chr(24).$this->bold_open."NO TRANSAKSI : ".$this->kd_kasir."-".$this->kd_kasir.chr(27).chr(33).chr(8).$this->condensed1, 9,"right")
                ->commit("header")
                ->addSpace("header")
                ->addColumn($data_rs['rs_name'], 9,"left")
                ->commit("header")
                ->addColumn($data_rs['rs_address'].", ".$data_rs['rs_city'], 9,"left")
                ->commit("header")
                ->addColumn($data_rs['telp'], 9,"left")
                ->commit("header")
                ->addColumn($data_rs['fax'], 9,"left")
                ->commit("header")
                ->addColumn(chr(27).chr(33).chr(24)."PERINCIAN BIAYA PELAYANAN KESEHATAN ".strtoupper($this->module).chr(27).chr(33).chr(8).$this->bold_close.$this->condensed1, 8,"center")
                ->commit("body");

            $tp ->setColumnLength(0, 23);   //DESKRIPSI
            $tp ->addColumn("===================================================================================================================================", 9,"left")
                ->commit("body");

            $tp ->setColumnLength(0, 21)    //DESKRIPSI
                ->setColumnLength(1, 50);

            $tp ->addColumn("No. Medrec        : ", 1,"left")
                ->addColumn($transaksi->row()->kd_pasien, 1,"left")
                ->commit("body");

            $tp ->addColumn("Nama pasien       : ", 1,"left")
                ->addColumn($transaksi->row()->nama, 1,"left")
                ->commit("body");

            $tp ->addColumn("Alamat            : ", 1,"left")
                ->addColumn($transaksi->row()->alamat, 1,"left")
                ->commit("body");


            $tp ->addColumn("Tanggal Masuk     : ", 1,"left")
                ->addColumn(date_format(date_create($transaksi->row()->tgl_masuk), 'd/m/Y'), 1,"left")
                ->commit("body");


            $tp ->addColumn("Kelompok Pasien   : ", 1,"left")
                ->addColumn($transaksi->row()->customer, 1,"left")
                ->commit("body");

            $tp ->setColumnLength(0, 23);   //DESKRIPSI
            $tp ->addColumn("===================================================================================================================================", 9,"left")
                ->commit("body");
            $tmp_urut="";
            for ($i=0; $i < count($params['data_produk']); $i++) { 
                $tmp_urut .= "'".$params['data_produk'][$i]->urut."',";
            }
            $tmp_urut = substr($tmp_urut, 0, -1);
            $query = $this->get_detail_transaksi($tmp_urut);
            if ($query->num_rows() > 0) {
                $total = 0;
                $tp ->setColumnLength(0, 75)    //DESKRIPSI
                    ->setColumnLength(1, 5)     //RP
                    ->setColumnLength(2, 20)    //NOMINAL       
                    ->setColumnLength(3, 5)     //RP
                    ->setColumnLength(4, 20);   //NOMINAL
                foreach ($query->result() as $value) {
                    $tp ->addColumn($value->deskripsi." ".$value->qty." X ", 1,"left")
                        ->addColumn("Rp. ", 1,"left")
                        ->addColumn(number_format($value->harga, 0, ',', '.'), 1,"left")
                        ->addColumn("Rp. ", 1,"left")
                        ->addColumn(number_format($value->harga * $value->qty, 0, ',', '.'), 1,"left")
                        ->commit("body");
                    $total += ($value->harga * $value->qty);
                }
                $tp ->setColumnLength(0, 23);   //DESKRIPSI
                $tp ->addColumn("===================================================================================================================================", 9,"left")
                    ->commit("body");

                $tp ->setColumnLength(0, 75)    //DESKRIPSI
                    ->setColumnLength(1, 5)     //RP
                    ->setColumnLength(2, 20)    //NOMINAL       
                    ->setColumnLength(3, 5)     //RP
                    ->setColumnLength(4, 20);   //NOMINAL

                $tp ->addColumn("TOTAL BIAYA", 1,"left")
                    ->addColumn(" ", 1,"left")
                    ->addColumn(" ", 1,"left")
                    ->addColumn("Rp. ", 1,"left")
                    ->addColumn(number_format($total, 0, ',', '.'), 1,"left")
                    ->commit("body");

                $tp ->setColumnLength(0, 32);
                $tp ->addColumn("===================================================================================================================================", 9,"left")
                        ->commit("body");
                        
            }
                $tp ->setColumnLength(0, 75)    //DESKRIPSI
                    ->setColumnLength(1, 5)     //RP
                    ->setColumnLength(2, 20)    //NOMINAL       
                    ->setColumnLength(3, 5)     //RP
                    ->setColumnLength(4, 20);   //NOMINAL

            $query = $this->get_detail_pembayaran($tmp_urut);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $value) {
                    $tp ->addColumn($value->uraian, 1,"left")
                        ->addColumn(" ", 1,"left")
                        ->addColumn(" ", 1,"left")
                        ->addColumn("Rp. ", 1,"left")
                        ->addColumn(number_format($value->jumlah, 0, ',', '.'), 1,"left")
                        ->commit("body");
                }
            }

            $tp ->setColumnLength(0, 75)    //DESKRIPSI
                ->setColumnLength(1, 5)     //RP
                ->setColumnLength(2, 10)    //RP 1      
                ->setColumnLength(3, 5)     //RP
                ->setColumnLength(4, 30);

            $tp ->addSpace("body")
                ->addColumn("Keterangan Rangkap : ", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn($data_rs['rs_city'] . ' , ' . date("d F Y"), 1,"right")
                ->commit("body");

            $tp ->addColumn("- Lembar 1 : Pasien", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("Petugas Rincian Biaya Pasien", 1,"right")
                ->commit("body");

            $tp ->addColumn("- Lembar 2 : Keuangan", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"right")
                ->commit("body");

            $tp ->addColumn("- Lembar 3 : Rekam Medis", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->commit("body");

            $tp ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("", 1,"left")
                ->addColumn("Kasir ".$this->module, 1,"right")
                ->commit("body");
            $data = $tp->getText();
            $fp = fopen($this->file, "wb");
            fwrite($fp,$data);
            fclose($fp); 
            
            $handle      = fopen($this->file, 'w');
            $condensed   = Chr(27) . Chr(33) . Chr(4);
            $feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
            $reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
            $formfeed    = chr(12); # mengeksekusi $feed
            $bold1       = Chr(27) . Chr(69); # Teks bold
            $bold0       = Chr(27) . Chr(70); # mengapus Teks bold
            $initialized = chr(27).chr(64);
            $condensed1  = chr(15);
            $condensed0  = chr(18);
            $margin      = chr(27) . chr(78). chr(90);
            $margin_off  = chr(27) . chr(79);
            
            $Data  = $initialized;
            $Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
            // $Data .= $fast_mode; # fast print / low quality
            // $Data .= $low_quality_off; # low quality
            // $Data .= $fast_print_on; # fast print on / enable
            $Data .= $condensed1;
            $Data .= $margin;
            $Data .= $data;
            //$Data .= $margin_off;
            $Data .= $formfeed;

            fwrite($handle, $Data);
            fclose($handle);

            $printer=$this->db->query("select p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                copy($this->file, $printer);  # Lakukan cetak
                unlink($file);
                # printer windows -> nama "printer" di komputer yang sharing printer
            } else{
                shell_exec("lpr -P ".$printer." ".$this->file); # Lakukan cetak linux
            }
            return true;
        }else{
            $tp ->setColumnLength(0, 23);   //DESKRIPSI
            $tp ->addColumn("Data Transaksi tidak ada", 9,"left")
                ->commit("body");

            $data = $tp->getText();
            $fp = fopen($this->file, "wb");
            fwrite($fp,$data);
            fclose($fp); 
            return false;
        }
    }

    private function cetak_kwitansi($params){
        return true;
    }

    private function get_transaksi($criteria){
        $this->db->select(" pasien.*, transaksi.kd_kasir, transaksi.no_transaksi, kunjungan.tgl_masuk, kunjungan.urut_masuk, kunjungan.kd_unit, customer.customer, unit.nama_unit ");
        $this->db->from("transaksi");
        $this->db->join("kunjungan", " kunjungan.kd_pasien = transaksi.kd_pasien AND kunjungan.tgl_masuk = transaksi.tgl_transaksi AND kunjungan.kd_unit = transaksi.kd_unit AND kunjungan.urut_masuk = transaksi.urut_masuk", "INNER");
        $this->db->join("pasien", " pasien.kd_pasien = transaksi.kd_pasien ", "INNER");
        $this->db->join("unit", " unit.kd_unit = kunjungan.kd_unit ", "INNER");
        $this->db->join("customer", " customer.kd_customer = kunjungan.kd_customer ", "INNER");
        $this->db->where($criteria);
        return $this->db->get();
    }

    private function get_detail_transaksi($criteria){
        $query = "SELECT 
            P.deskripsi,
            dt.harga,
            dt.qty,
            dt.urut,
            dt.kd_produk 
        FROM
            detail_transaksi dt
            INNER JOIN produk P ON dt.kd_produk = P.kd_produk 
        WHERE
            no_transaksi = '".$this->no_transaksi."' 
            and kd_kasir = '".$this->kd_kasir."' 
            AND dt.urut IN ( ".$criteria." ) 
        ORDER BY
            deskripsi";
        return $this->db->query($query);
    }

    private function get_detail_pembayaran($criteria){
        $query = "SELECT 
            uraian,
            sum(jumlah) as jumlah 
        from detail_tr_bayar db 
        inner join payment p on db.kd_pay = p.kd_pay 
        where 
            no_transaksi = '".$this->no_transaksi."'  
            and kd_kasir = '".$this->kd_kasir."' 
            AND urut IN ( ".$criteria." ) 
        group by uraian";
        return $this->db->query($query);
    }

    private function data_rs(){
        $response = array();
        $kd_rs = $this->session->userdata['user_id']['kd_rs'];
        $rs    = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
        $response['telp']   = '';
        $response['fax']    = '';
        $response['telp1']  = '';
        $response['rs_name']= '';
        if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
            $response['telp']   = 'Telp. ';
            $response['telp1']  = false;
            if($rs->phone1 != null && $rs->phone1 != ''){
                $response['telp1']  =   true;
                $response['telp']   .=  $rs->phone1;
            }
            if($rs->phone2 != null && $rs->phone2 != ''){
                if($response['telp1']==true){
                    $response['telp'] .= '/'.$rs->phone2.'.';
                }else{
                    $response['telp'] .= $rs->phone2.'.';
                }
            }else{
                $$response['telp'].='.';
            }
        }
        if($rs->fax != null && $rs->fax != ''){
            $response['fax']='Fax. '.$rs->fax.'.';
        }
        $response['rs_name']    = $rs->name;
        $response['rs_address'] = $rs->address;
        $response['rs_city']    = $rs->city;
        return $response;
    }
}
