<?php 
class cetak_spri extends MX_Controller{
    public function __construct(){
        parent ::__construct();
        $this->load->library('session', 'url');
        $this->load->library('common');
    }

    public function cetak(){
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);
            
            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun
         
            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }

        $tgl_masuk = date("Y-m-d");
        $params = json_decode($_POST['data']);

        $no_spri = 0;
        $bln_romawi = 'as';
        $bln = date_format(date_create($tgl_masuk), "m");
        $thn = date_format(date_create($tgl_masuk), "Y");
        if($bln == 1){
            $bln_romawi = "I";
        }else if($bln == 2){
            $bln_romawi = "II";
        }else if($bln == 3){
            $bln_romawi = "III";
        }else if($bln == 4){
            $bln_romawi = "IV";
        }else if($bln == 5){
            $bln_romawi = "V";
        }else if($bln == 6){
            $bln_romawi = "VI";
        }else if($bln == 7){
            $bln_romawi = "VII";
        }else if($bln == 8){
            $bln_romawi = "VIII";
        }else if($bln == 9){
            $bln_romawi = "IX";
        }else if($bln == 10){
            $bln_romawi = "X";
        }else if($bln == 11){
            $bln_romawi = "XI";
        }else if($bln == 12){
            $bln_romawi = "XII";
        }

        $getData = $this->db->query("SELECT
                                        *,
                                        p.alamat as alamat_pasien,
                                        dok.nama as nama_dokter,
                                        p.nama as nama_pasien 
                                    FROM
                                        kunjungan
                                        K INNER JOIN pasien P ON P.kd_pasien = K.kd_pasien
                                        INNER JOIN unit u ON u.kd_unit = K.kd_unit
                                        INNER JOIN pekerjaan pkj ON pkj.kd_pekerjaan = P.kd_pekerjaan
                                        INNER JOIN customer C ON C.kd_customer = K.kd_customer
                                        INNER JOIN rujukan rjk ON rjk.kd_rujukan = K.kd_rujukan
                                        LEFT JOIN mr_diagnosa md ON md.kd_pasien = K.kd_pasien 
                                        AND md.kd_unit = K.kd_unit 
                                        AND md.tgl_masuk = K.tgl_masuk 
                                        AND md.urut_masuk = K.urut_masuk
                                        LEFT JOIN penyakit pyt ON pyt.kd_penyakit = md.diagnosa_utama 
                                        INNER JOIN dokter dok ON dok.kd_dokter = k.kd_dokter
                                    WHERE
                                        k.kd_pasien = '".$params->KD_PASIEN."' 
                                        AND k.kd_unit = '".$params->KD_UNIT."' 
                                        AND k.tgl_masuk = '".$params->TGL_MASUK."' 
                                        AND k.urut_masuk = '".$params->URUT_MASUK."'")->result();
                                        
            if($getData[0]->nomor_spri == NULL || $getData[0]->nomor_spri == ''){
                $max_no_spri = $this->db->query("SELECT max(nomor_spri) FROM kunjungan")->row()->max;
                if($max_no_spri == NULL || $max_no_spri == 'NULL' ){
                    $no_spri = $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'no_spri'")->row()->setting;
                    $no_spri += 1;
                }else{
                    $no_spri = $max_no_spri+1;
                }
                $this->db->query("UPDATE kunjungan SET nomor_spri = '$no_spri' WHERE kd_pasien = '".$params->KD_PASIEN."' AND kd_unit = '".$params->KD_UNIT."' AND tgl_masuk = '".$params->TGL_MASUK."' AND urut_masuk = '".$params->URUT_MASUK."'");
            }else{
                $no_spri = $getData[0]->nomor_spri;
            }

            $nama_unit      = $getData[0]->nama_unit;
            $rujukan        = $getData[0]->rujukan;
            $nama_pasien    = $getData[0]->nama_pasien;
            $tgl_lahir      = date_format(date_create($getData[0]->tgl_lahir), "d-m-Y");
            $kepesertaan    = $getData[0]->customer;
            $no_asuransi    = $getData[0]->no_asuransi;
            $pekerjaan      = $getData[0]->pekerjaan;
            $alamat         = $getData[0]->alamat_pasien;
            $diagnosa       = $getData[0]->penyakit;
            $nama_dokter    = $getData[0]->nama_dokter;
            $nip_dokter     = $getData[0]->nip;
            if($getData[0]->jenis_kelamin == 't'){
                $jk_l = " <b>L</b>";
                $jk_p = " <del>P</del>";
            }else{
                $jk_l = " <del>L</del>";
                $jk_p = " <b>P</b>";
            }
            /* if($params->ALIAS_KD_UNIT == '3'){
                $module = 'IGD';
            }else{
                $module = 'RWJ';
            } */

            $module = $this->db->query("SELECT alias_sms FROM unit WHERE kd_unit = '".$params->KD_UNIT."'")->row()->alias_sms;
                
            /* $html.="<table>
                        <tr>
                            <td style='font-size: 18px;'>HTML Decimal: &#9745;<br>HTML desimal: &#9744;</td>
                        </tr>
                    </table>";   */ 

            $html="";
            $html.='<hr>';
            $html.='<center>';
            $html.='<table width="100%" border="0" callspacing="0">
                        <tr>
                            <td height="30px" valign="top" align="right" style="font-size:11px;">RM. 7</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:14px;"><u><b>SURAT PERMOHONAN RAWAT INAP</b></u></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:12px;"><b>No. RSUD.445/'.$no_spri.'/SPRI/'.$module.'/'.$bln_romawi.'/'.$thn.'</b></td>
                        </tr>
                    </table>';
            $html.='</center>';
            $html.='<br>';
            $html.='<table border="0" style="font-size:11px;">
                        <tr>
                            <td width="110px">Dari Poliklinik / IGD</td>
                            <td width="5px">:</td>
                            <td colspan="3">'.$nama_unit.'</td>
                            <td width="110px" style="padding-left:5px;">No. CM</td>
                            <td width="5px">:</td>
                            <td width="20px">
                                <table border="1" callspacing="0">
                                    <tr>
                                        <td align="center" height="20px" width="20px">'.substr($params->KD_PASIEN,0,-9).'</td>
                                        <td align="center" width="20px">'.substr($params->KD_PASIEN,2,-7).'</td>
                                        <td align="center" width="20px">'.substr($params->KD_PASIEN,3,-6).'</td>
                                        <td align="center" width="20px">'.substr($params->KD_PASIEN,5,-4).'</td>
                                        <td align="center" width="20px">'.substr($params->KD_PASIEN,6,-3).'</td>
                                        <td align="center" width="20px">'.substr($params->KD_PASIEN,8,-1).'</td>
                                        <td align="center" width="20px">'.substr($params->KD_PASIEN,9).'</td>
                                        <td width="20px" style="border:none;"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="20px">Rujukan Dari</td>
                            <td width="5px">:</td>
                            <td colspan="6">'.ucwords($rujukan).'</td>
                        </tr>
                        <tr>
                            <td height="20px">Nama</td>
                            <td>:</td>
                            <td width="200px">'.ucwords($nama_pasien).'</td>
                            <td width="20px" align="center" style="border:1px solid black;">'.$jk_l.'</td>
                            <td width="20px" align="center" style="border:1px solid black;">'.$jk_p.'</td>
                            <td style="padding-left:5px;">Tanggal Lahir</td>
                            <td width="5px">:</td>
                            <td width="20px">
                                <table border="1" callspacing="0">
                                    <tr>
                                        <td align="center" height="20px" width="20px">'.substr($tgl_lahir,0,-9).'</td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,1,-8).'</td>
                                        <td align="center" width="5px" style="border-top:none;border-bottom:none;"></td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,3,-6).'</td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,4,-5).'</td>
                                        <td align="center" width="5px" style="border-top:none;border-bottom:none;"></td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,6,-3).'</td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,7,-2).'</td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,8,-1).'</td>
                                        <td align="center" width="20px">'.substr($tgl_lahir,9).'</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="20px">Kepesertaan</td>
                            <td width="5px">:</td>
                            <td colspan="3">'.$kepesertaan.'</td>
                            <td style="padding-left:5px;">No. Kartu</td>
                            <td width="5px">:</td>
                            <td>'.$no_asuransi.'</td>
                        </tr>
                        <tr>
                            <td height="20px">Pekerjaan</td>
                            <td width="5px">:</td>
                            <td colspan="3">'.ucwords(strtolower($pekerjaan)).'</td>
                            <td style="padding-left:5px;">Alamat</td>
                            <td width="5px">:</td>
                            <td>'.ucwords(strtolower($alamat)).'</td>
                        </tr>
                        <tr>
                            <td height="20px">Diagnosa</td>
                            <td width="5px">:</td>
                            <td colspan="6">'.$diagnosa.'</td>
                        </tr>
                        <tr>
                            <td height="20px">Terapi</td>
                            <td width="5px">:</td>
                            <td colspan="6"></td>
                        </tr>
                        <tr>
                            <td  height="20px" colspan="8"></td>
                        </tr>
                    </table>';
            $html.='<br><br>';
            $html.='<table border="0" style="font-size:11px;">
                        <tr>
                            <td height="20px" width="500px" colspan="4">Kasus : </td>
                            <td>Larantuka, '.tgl_indo($tgl_masuk).'</td>
                        </tr>
                        <tr>
                            <td width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="50px;">Umum</td>
                            <td  width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="150px;">Obstetri</td>
                            <td rowspan="4" valign="top">Dokter Penanggung Jawab Pelayanan</td>
                        </tr>
                        <tr>
                            <td width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="50px;">Bedah Non Trauma</td>
                            <td  width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="150px;">Ginekologi</td>
                        </tr>
                        <tr>
                            <td width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="50px;">Bedah Trauma</td>
                            <td  width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="150px;">Bayi / Anak</td>
                        </tr>
                        <tr>
                            <td width="10px;" style="font-size:14px;" height="20px" align="center">&#9744;</td>
                            <td width="50px;">Neonatus</td>
                            <td  width="10px;" style="font-size:14px;" height="20px" align="center"></td>
                            <td width="150px;"></td>
                        </tr>
                        <tr>
                            <td width="10px;" style="font-size:14px;" height="20px" align="center" colspan="4"></td>
                            <td width="150px;">( <u>'.$nama_dokter.'</u> )</td>
                        </tr>
                        <tr>
                            <td width="10px;" style="font-size:14px;" height="20px" align="center" colspan="4"></td>
                            <td width="150px;">NIP. '.$nip_dokter.'</td>
                        </tr>
                    </table>';
            $common=$this->common;
            $this->common->setPdf_('P', 'Cetak SPRI' , $html, '');	
            // echo $html;
    }
}
?>