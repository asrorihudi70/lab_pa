<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cetakkartukunjungan extends MX_Controller {
    private $setup_db_sql = false;
    public function __construct() {
        parent::__construct();
        $this->load->library('session', 'url');
    }

    public function cetak() { 
        $nomor      = $this->uri->segment(4, 0);
        $kd_pasien  = explode("-",$nomor);
        $kd_pasien2 = implode("",$kd_pasien);

        if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
            $nama_pas = _QMS_Query(" SELECT nama,kd_pasien, alamat from pasien WHERE kd_pasien = '" . $nomor . "'");
        }else{
            $nama_pas = $this->db->query(" SELECT nama,kd_pasien, alamat from pasien WHERE kd_pasien = '" . $nomor . "'");
        }
        
        if (count($nama_pas->result()) > 0) {
            // echo "<pre>".var_export($nama_pas2)
            $nama_pas_2 = $nama_pas->row();
            $nama 		= $nama_pas_2->nama;
            $kd_pasien 		= $nama_pas_2->kd_pasien;
            $alamat 		= $nama_pas_2->alamat;
        }
            
        if (strlen($nama) >= 22) {
            $nama = substr($nama, 0, 19)." ...";
        }else{
            $nama;
        }
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $kertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'kertasLabelpasien'")->row()->setting;
        $heightKertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'heightkertasLabelpasien'")->row()->setting;
        $widthKertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'widthkertasLabelpasien'")->row()->setting;

        $mpdf = new mPDF(
            'utf-8',    // mode - default ''
            // '21cm 29.7cm',    // format - A4, for example, default ''
            array(50, 80),    // format - A4, for example, default ''
            // array(200, 140),    // format - A4, for example, default ''
            // array( $widthKertas, $heightKertas),    // format - A4, for example, default ''
            0,     // font size - default 0
            '',    // default font family
            0,    // margin_left
            0,    // margin right
            0,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            $kertas);  // L - landscape, P - portrait
        // $mpdf      = new mPDF('', array(188, 23), '', '', 1, 2, 2, 2, 5, 5, 'L');
        //$mpdf = new mPDF('', array(66, 27), '', '', 3, 3, 3, 3, 5, 5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle('Kartu Kunjungan');
        $mpdf->WriteHTML("
        <style>

         p
        {
        font-family:arial;
        font-size:11px !important;
        line-height:1px;
        font-weigth:bolder;
        }

        .barcode {
        float:left;
        position:relative;
        margin-left:-15px;
        margin-top:-5px;

        }

        </style>
        ");
        $mpdf->WriteHTML('<html>');
        $mpdf->WriteHTML('<body>');
        for ($i=1; $i <= 1; $i++) { 
// style="padding-bottom:5px;padding-top:5px;"
        $logo = base_url().'ui/images/Logo/LOGO_.jpg';
        // echo $logo; die;
        $mpdf->WriteHTML('
        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
            <tr>
				<td width="15%" rowspan="4"><img src="'.$logo.'" width="21%"></td>
            </tr>
            <tr>
                <td align="center" colspan="2"><font style="font-size:14px; font-family:arial;"><b>KARTU KUNJUNGAN</b></font></td>
            </tr>
            <tr>
                <td align="center" colspan="2" ><font style="font-size:14px; font-family:arial;"><b>KLINIK SPESIALIS SUAKA INSAN</b></font></td>
            </tr>
            <tr>
                <td align="center" colspan="2"><font style="font-size:11px; font-family:arial;"><b>Jl. Zafri Zamzam No. 60 Banjarmasin</b></font></td>
            </tr>
            <tr>
                <td align="center" colspan="3"><font style="font-size:10px; font-family:arial; margin:100px;"><b>Telp. 0511-3367187, 3367188</b></font></td>
            </tr>

            <hr size="10px">
            <tr>
                <td align="left" width = "20%"><font style="font-size:13px; font-family:arial;" ><b>NO. RM </b></font></td>
                <td align="left"><font style="font-size:14px; font-family:arial;"><b>: ' . $kd_pasien . '</b></font></td>
            </tr>
            <tr>
                <td align="left"><font style="font-size:13px; font-family:arial;"><b>NAMA </b></font></td>
                <td align="left"><font style="font-size:14px; font-family:arial;"><b>: ' . $nama . '</b></font></td>
            </tr>
            <tr>
                <td align="left"><font style="font-size:13px; font-family:arial;"><b>ALAMAT </b></font></td>
                <td align="left"><font style="font-size:14px; font-family:arial;"><b>: ' . $alamat . '</b></font></td>
            </tr>
            
        </table>
    ');

        }
       $mpdf->WriteHTML('</body>');
       $mpdf->WriteHTML('</html>');

        $mpdf->WriteHTML(utf8_encode($html));
        $mpdf->Output("cetak.pdf", 'I');
        exit;
    }
}
