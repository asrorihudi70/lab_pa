<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cetaklabelpasien extends MX_Controller {
    private $setup_db_sql = false;
    public function __construct() {
        /*$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
        if ($this->setup_db_sql->num_rows() > 0) {
            $this->setup_db_sql = $this->setup_db_sql->row()->setting;
        }*/
        parent::__construct();
        $this->load->library('session', 'url');
    }

    public function cetak() {
//  
        $nomor      = $this->uri->segment(4, 0);
        $kd_pasien  = explode("-",$nomor);
        $kd_pasien2 = implode("",$kd_pasien);

        if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
            $nama_pas = _QMS_Query(" SELECT nama,tgl_lahir, kd_pasien, jenis_kelamin from pasien WHERE kd_pasien = '" . $nomor . "'");
        }else{
            $nama_pas = $this->db->query(" SELECT nama,tgl_lahir, kd_pasien, jenis_kelamin,nik, dbo.USIA( tgl_lahir, GETDATE()) as umur from pasien WHERE kd_pasien = '" . $nomor . "'");
        }
        
        if (count($nama_pas->result()) > 0) {
            // echo "<pre>".var_export($nama_pas2)
            $nama_pas_2 = $nama_pas->row();
            $nama 		= $nama_pas_2->nama;
            //$tgllahir 	= $nama_pas_2->tgl_lahir;
            $jenis_kelamin  = $nama_pas_2->jenis_kelamin;
            $now            = new DateTime();
            $tgl_lahir      = new DateTime($nama_pas_2->tgl_lahir);
            $tgllahir       = new DateTime($nama_pas_2->tgl_lahir);
            $today          = new DateTime('today');
            $y              = $today->diff($tgllahir)->y;
            $umur           =$nama_pas_2->umur;
            $nik            = $nama_pas_2->nik;

        }
            if(strlen($nama_pas_2->jenis_kelamin == 0 )){
                $jns_kelamin = 'Perempuan';
            }else{
                $jns_kelamin = 'Laki-laki';
            }
        if (strlen($nama) >= 22) {
            $nama = substr($nama, 0, 19)." ...";
        }else{
            $nama;
        }
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        //$tgllahir2 = date_create($tgllahir);
        // $mpdf = new mPDF('c', 'A4-L'); 
        // $mpdf=new mPDF('','', 0, '', 15, 15, 16, 16, 9, 9, 'L');
        $kertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'kertasLabelpasien'")->row()->setting;
        $heightKertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'heightkertasLabelpasien'")->row()->setting;
        $widthKertas = $this->db->query(" SELECT setting from sys_setting where key_data = 'widthkertasLabelpasien'")->row()->setting;

        $mpdf = new mPDF(
            'utf-8',    // mode - default ''
            // '21cm 29.7cm',    // format - A4, for example, default ''
            // array(80, 50),    // format - A4, for example, default ''
            // array(200, 140),    // format - A4, for example, default ''
            array( $widthKertas, $heightKertas),    // format - A4, for example, default ''
            0,     // font size - default 0
            '',    // default font family
            0,    // margin_left
            0,    // margin right
            0,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            $kertas);  // L - landscape, P - portrait
        // $mpdf      = new mPDF('', array(188, 23), '', '', 1, 2, 2, 2, 5, 5, 'L');
        //$mpdf = new mPDF('', array(66, 27), '', '', 3, 3, 3, 3, 5, 5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle('Label Pasien');
        $mpdf->WriteHTML("
        <style>

         p
        {
        font-family:arial;
        font-size:11px !important;
        line-height:1px;
        font-weigth:bolder;
        }

        .barcode {
        float:left;
        position:relative;
        margin-left:-15px;
        margin-top:-5px;

        }

        </style>
        ");
        $mpdf->WriteHTML('<html>');
        $mpdf->WriteHTML('<body>');
        for ($i=1; $i <= 1; $i++) { 
        //     $mpdf->WriteHTML('
        //     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:5px;padding-top:0px;">
        //         <tr>
        //             <td align="center"><font style="font-size:14px; font-family:arial;"><b>' . $nomor . '</b></font></td>
        //         </tr>
        //         <tr>
        //             <td align="center"><font style="font-size:10px;font-family:arial;"><b>' . $nama . '</b></font></td>
        //         </tr>
        //         <tr>
        //             <td align="center"><font style="font-size:10px; font-family:arial;"><b>NIK : ' . $nik . '</b></font></td>
        //         </tr>                
        //         <hr>
        //         <tr>
        //             <td align="center">
        //             <font style="font-size:10px;font-family:arial;">'. date_format($tgllahir, "d-F-Y") . '</font>
        //             </td>
        //         </tr>
        //         <tr>
        //             <td align="center">
        //             <font style="font-size:10px;font-family:arial;">' . $jns_kelamin .'&nbsp;&nbsp;&nbsp;&nbsp;'. ' (' . $umur . ')</font>
        //             </td>
        //         </tr>
        //         <tr>
        //             <td align="center"><barcode code=' . $kd_pasien2 . ' type="C128B" class="barcode" size="0.700" height="0.50"/></td>
        //         </tr>
                
        //     </table>
        // ');

        $mpdf->WriteHTML('
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:5px;padding-top:5px;">
            <tr>
                <td align="center"><font style="font-size:14px; font-family:arial;"><b>' . $nomor . '</b></font></td>
            </tr>
            <tr>
                <td align="center"><font style="font-size:10px;font-family:arial;"><b>' . $nama . '</b></font></td>
            </tr>
            <tr>
                <td align="center"><font style="font-size:8px; font-family:arial;"><b>NIK: ' . $nik . '</b></font></td>
            </tr>
            <hr>
            <tr>
                <td align="center"><font style="font-size:8px;font-family:arial;">' . date_format($tgllahir, "d-F-Y") . '</font></td>
            </tr>
            <tr>
                <td align="center"  valign="top" style="height:20px;"><font style="font-size:10px;font-family:arial;">' . $jns_kelamin .'&nbsp;&nbsp;&nbsp;&nbsp;'. ' (' . $umur . ') </font></td>
            </tr>
            <tr>
                <td align="center" style="height:8px;"><barcode code=' . $kd_pasien2 . ' type="C128B" class="barcode" size="0.700" height="0.30"/></td>
            </tr>
            
        </table>
    ');

            // $mpdf->WriteHTML('
            //     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:25px;padding-top:25px;">
            //         <tr>
            //             <td align="center"><font style="font-size:13px;font-family:arial;"><b>' . $nama . ' ('. $jns_kelamin .')</b></font></td>
            //             <td align="center"><font style="font-size:13px;font-family:arial;"><b>' . $nama . ' ('. $jns_kelamin .')</b></font></td>
            //             <td align="center"><font style="font-size:13px;font-family:arial;"><b>' . $nama . ' ('. $jns_kelamin .')</b></font></td>
            //         </tr>
            //         <tr>
            //             <td align="center" valign="top" style="height:20px;"><font style="font-size:10px;font-family:arial;">' . date_format($tgllahir,"d/m/Y") . ' ('. $umur .')</font></td>
            //             <td align="center" valign="top" style="height:20px;"><font style="font-size:10px;font-family:arial;">' . date_format($tgllahir,"d/m/Y") . ' ('. $umur .')</font></td>
            //             <td align="center" valign="top" style="height:20px;"><font style="font-size:10px;font-family:arial;">' . date_format($tgllahir,"d/m/Y") . ' ('. $umur .')</font></td>
            //         </tr>
            //         <tr>
            //             <td align="center" style="height:8px;"><barcode code='.$kd_pasien2.' type="C128B" class="barcode" size="0.700" height="0.50"/></td>
            //             <td align="center" style="height:8px;"><barcode code='.$kd_pasien2.' type="C128B" class="barcode" size="0.700" height="0.50"/></td>
            //             <td align="center" style="height:8px;"><barcode code='.$kd_pasien2.' type="C128B" class="barcode" size="0.700" height="0.50"/></td>
            //         </tr>
            //         <tr>
            //             <td align="center"><font style="font-size:16px; font-family:arial;"><b>' . $nomor . '</b></font></td>
            //             <td align="center"><font style="font-size:16px; font-family:arial;"><b>' . $nomor . '</b></font></td>
            //             <td align="center"><font style="font-size:16px; font-family:arial;"><b>' . $nomor . '</b></font></td>
            //         </tr>
            //     </table>
            // '); 
        }
       $mpdf->WriteHTML('</body>');
       $mpdf->WriteHTML('</html>');
		/* $mpdf->WriteHTML('<html>
							<body>
								<p align="center"><b>Aditya Iqbal</b>'
                . '</body></html>'); */

        $mpdf->WriteHTML(utf8_encode($html));
        $mpdf->Output("cetak.pdf", 'I');
        exit;
        //}
    }

    public function cetak_(){
        $nomor      = $this->uri->segment(4, 0);
        $kd_pasien  = explode("-",$nomor);
        $kd_pasien2 = implode("",$kd_pasien);
        if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
            $nama_pas = _QMS_Query(" SELECT nama,tgl_lahir,kd_pasien from pasien WHERE kd_pasien = '" . $nomor . "'");
        }else{
            $nama_pas = $this->db->query(" SELECT nama,tgl_lahir,kd_pasien from pasien WHERE kd_pasien = '" . $nomor . "'");
        }
        
        $this->load->view('rawat_jalan/print_label',
            array(
                'count'    => $nama_pas->num_rows(),
                'data'     => $nama_pas,
                'code'     => '<barcode code='.$kd_pasien2.' type="C128B" class="barcode" size="0.700" height="0.50"/>',
            )
        );
    }
}
