<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_data_tarif_component_02 extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('new/Tbl_data_tarif_component');
		$this->load->model('new/Tbl_data_detail_component');
		$this->load->model('new/Tbl_data_detail_transaksi');
		$this->load->model('new/Tbl_data_visite_dokter');
		$this->jam      = date("H:i:s");
		//$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
	}


	public function index()
	{
		$this->load->view('main/index');
	}

	public function get_data_component()
	{
		$response 	= array();
		$resultData = array();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'kd_tarif' 		=> $this->input->post('kd_tarif'),
			'tgl_berlaku' 	=> $this->input->post('tgl_berlaku'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'urut' 			=> $this->input->post('urut'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);


		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi'	=> $params['no_transaksi'],
			'tgl_transaksi'	=> $params['tgl_transaksi'],
			'urut'			=> $params['urut'],
		);

		$queryTarifComponent = $this->Tbl_data_detail_component->getDetailComponent($criteriaParams);
		$x = 0;
		if ($queryTarifComponent->num_rows() > 0) {
			foreach ($queryTarifComponent->result() as $data) {
				$resultData[$x]['KD_KASIR']  		= (string)$data->KD_KASIR;
				$resultData[$x]['NO_TRANSAKSI']	 	= (string)$data->NO_TRANSAKSI;
				$resultData[$x]['URUT'] 	 		= (string)$data->URUT;
				$resultData[$x]['TGL_TRANSAKSI'] 	= (string)$data->TGL_TRANSAKSI;
				$resultData[$x]['TARIF'] 	 		= (int)$data->TARIF;
				$resultData[$x]['DISC']		 		= (int)$data->DISC;
				$resultData[$x]['MARKUP'] 	 		= (int)$data->MARKUP;
				$resultData[$x]['KD_COMPONENT']  	= $data->KD_COMPONENT;
				$resultData[$x]['CHECK_COMPONENT']  = true;
				$resultData[$x]['TARIF_BARU'] 		= ((int)$data->TARIF + (int)$data->MARKUP) - (int)$data->DISC;

				unset($criteriaParamss);
				$criteriaParams = array(
					'field_criteria' 	=> 'kd_component',
					'value_criteria' 	=> $data->KD_COMPONENT,
					'table' 			=> 'produk_component',
					'field_return' 		=> 'COMPONENT',
				);

				$resultData[$x]['COMPONENT'] = $this->Tbl_data_detail_component->getCustom($criteriaParams);
				$x++;
			}
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function update_tarif()
	{
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		$response 	= array();
		//$resultSQL 	= false;
		$resultPG 	= false;

		$params 	= array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'urut' 			=> $this->input->post('urut'),
			'kd_kasir'		=> $this->input->post('kd_kasir'),
			'total_tarif'	=> $this->input->post('total_tarif'),
			'tarif_sebelumnya'	=> $this->input->post('tarif_sebelumnya'),
		);


		$list = explode("##[[]]##", $this->input->post('list'));
		for ($i = 0; $i <= count($list) - 1; $i++) {
			$data = explode("@@##$$@@", $list[$i]);
			for ($k = 0; $k < 1; $k++) {

				// $response[$i]['kd_component']	= $data[0];
				// $response[$i]['markup']			= $data[1];
				// $response[$i]['disc']			= $data[2];


				$kd_component 	= $data[0];
				$markup 		= $data[1];
				$disc 			= $data[2];
				$tarifLama 		= $data[3];
				$tarifBaru 		= $data[4];
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'urut' 			=> $params['urut'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'kd_component' 	=> $kd_component,
				);

				if ($tarifBaru > $tarifLama) {
					$markup = $tarifBaru - $tarifLama;
				} else if ($tarifBaru < $tarifLama) {
					$disc = $tarifLama - $tarifBaru;
				}

				$paramsUpdate = array(
					'markup' 	=> (int)$markup,
					'disc' 		=> (int)$disc,
				);

				$resultPG 	= $this->Tbl_data_detail_component->update($criteriaParams, $paramsUpdate);
				//$resultSQL	= $this->Tbl_data_detail_component->update_SQL($criteriaParams, $paramsUpdate);

				unset($criteriaParams);
				unset($paramsUpdate);
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'urut' 			=> $params['urut'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tag_char' 		=> $kd_component,
				);
				$queryVisiteDokter = $this->Tbl_data_visite_dokter->get($criteriaParams);
				if ($queryVisiteDokter->num_rows() > 0) {

					if (($resultPG === true || $resultPG > 0) /*&& ($resultSQL === true || $resultSQL > 0)*/) {
						unset($criteriaParams);
						unset($paramsUpdate);
						$criteriaParams = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'urut' 			=> $params['urut'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'tag_char' 		=> $kd_component,
						);

						$paramsUpdate = array(
							'vd_markup' 	=> (int)$markup,
							'vd_disc' 		=> (int)$disc,
						);
						$resultPG 	= $this->Tbl_data_visite_dokter->update($criteriaParams, $paramsUpdate);
						//$resultSQL	= $this->Tbl_data_visite_dokter->update_SQL($criteriaParams, $paramsUpdate);
					} else {
						$resultPG 	= false;
						//$resultSQL 	= false;
						break;
					}
				}

				if (($resultPG === false || $resultPG == 0) /*&& ($resultSQL === false || $resultSQL==0)*/) {
					$resultPG 	= false;
					//$resultSQL 	= false;
					break;
				}
			}
		}

		if (($resultPG === true || $resultPG > 0) /*&& ($resultSQL === true || $resultSQL > 0)*/) {
			unset($criteriaParams);
			unset($paramsUpdate);

			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'urut' 			=> $params['urut'],
				'no_transaksi' 	=> $params['no_transaksi'],
			);

			$paramsUpdate = array(
				'harga' 	=> (int)$params['total_tarif'],
			);
			$resultPG 	= $this->Tbl_data_detail_transaksi->update($criteriaParams, $paramsUpdate);
			//$resultSQL	= $this->Tbl_data_detail_transaksi->update_SQL($criteriaParams, $paramsUpdate);
		} else {
			//$resultSQL 	= false;
			$resultPG 	= false;
		}

		if (($resultPG === true || $resultPG > 0) /*&& ($resultSQL === true || $resultSQL > 0)*/) {
			$this->db->trans_commit();
			//$this->dbSQL->trans_commit();
			$response['total_harga'] 	= $params['total_tarif'];
			$response['status'] 		= true;
		} else {
			$this->db->trans_rollback();
			//$this->dbSQL->trans_rollback();
			$response['status'] = false;
		}
		echo json_encode($response);
	}
}
