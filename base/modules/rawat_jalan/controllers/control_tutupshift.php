<?php
class control_tutupshift extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
	public function __construct(){
		parent::__construct();        
	}	 
	public function index(){
		$this->load->view('main/index');
	}

	public function get_data(){
		$response = array();
		$params = array(
			'select' 	=> $this->input->post('select'),
			'table' 	=> $this->input->post('table'),
			'criteria' 	=> str_replace("~","'",$this->input->post('criteria')),
			'limit' 	=> $this->input->post('limit'),
		);

		$this->db->select($params['select']);
		$this->db->from($params['table']);
		if (isset($params['criteria']) === true && $params['criteria'] != "") {
			$this->db->where($params['criteria'], null, false);
		}

		if (isset($params['limit']) === true && $params['limit'] != "") {
			$this->db->limit($params['limit'], null, false);
		}

		$query = $this->db->get();
		$result_data = array();
		if ($query->num_rows() > 0) {
			$response['status']	= true;
			$structure = $this->get_structure($params['table']);
			if ($structure->num_rows() > 0) {
				foreach ($query->result_array() as $result) {
					$data = array();
					foreach ($structure->result() as $res_structure) {
						$data[strtoupper($res_structure->column_name)] = $result[$res_structure->column_name];
					}
					array_push($result_data , $data);
				}
				$response['data'] = $result_data;
			}
		}else{
			$response['status']	= false;
		}

		$response['count'] 	= $query->num_rows();
		echo json_encode($response);
	}

	private function get_structure($table){
		$query = "SELECT column_name, data_type from information_schema.columns where table_name = '".$table."'";
		return $this->db->query($query);
	}
}
?>