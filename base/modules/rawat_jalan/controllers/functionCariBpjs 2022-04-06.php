<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionCariBpjs extends  MX_Controller {		
	private $jam_request  = "";
	private $menit_request  = "";
	private $detik_request  = "";
	private $waktu_respones = "";
    public 	$ErrLoginMsg='';
    public function __construct()
    {

			parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
			$this->jam_request = date('H');
			$this->menit_request = date('i');
			$this->detik_request = date('s');
    }
	 
	public function index()
	{
		$this->load->view('main/index');
	} 
	
	public function getDataBpjs() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
		if(count($url)<>0){ 
			$klinik=$_POST['klinik'];
			$res=$this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='".$klinik."'");
			$poli=null;
			$no=$_POST['no_kartu'];
			if($res->result()){
				$poli=$res->row()->unit_bpjs;
			} 
		}
		$headers=$this->getSignature();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		//https://dvlp.bpjs-kesehatan.go.id/vclaim-rest/Peserta/nokartu/0000104951406/tglSEP/2018-09-27
		$res     = json_decode(file_get_contents($url.$no."/tglSEP/".date('Y-m-d'),false,$context));
		$dat=array(
			'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);

		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url.$no."/tglSEP/".date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat);
	}
	
	public function cariRujukanFKTP() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoka'")->row()->nilai;
		if(count($url)<>0){ 
			$klinik=$_POST['klinik'];
			$res=$this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='".$klinik."'");
			$poli=null;
			$no=$_POST['no_kartu'];
			if($res->result()){
				$poli=$res->row()->unit_bpjs;
			} 
		}
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$tipenya=$_POST['tipe'];
		$start=$_POST['startnya'];
		$limit=$_POST['limitnya'];
		$tglrujuk=$_POST['tgl_rujuk'];
		$urlnya='';
		//echo $tipenya;
		if ($tipenya=='bpjs'){
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNokaMulti'")->row()->nilai.$no;
		}
		else if ($tipenya=='norujukan'){
			$no_rujuk=
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoRujukan'")->row()->nilai.$no;
		}
		//echo $urlnya;
		$res     = json_decode(file_get_contents($urlnya,false,$context));
		
		$dat=array(
			'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);

		if ($res){
		//	var_dump($res);
		/*	echo "{success:true,data_respon:".json_encode($dat)." }";*/
			echo '{success:true, totalrecords:'.count($res->response->rujukan).', data_respon:'.json_encode($dat).'}';
		}
		else{	
			echo "{success:false}";
		}  
	}
   
	public function cariRujukanFKTLDetail() {
		$no_rujukan=$this->input->post('no_rujuk');	
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNokaDetail'")->row()->nilai;
		if(count($url)<>0){ 
			$poli=null;
			$no=$_POST['no_rujuk'];
			
		}
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$no_rujukan,false,$context));
		if ($res){
			echo "{success:true, data: ".json_encode($res)." ,headers:".json_encode($headers)."}";
		}
		else{
			echo "{success:false}";
		}    
	}
	public function cariRujukanFKTPDetail() {
		$no_rujukan=$this->input->post('no_rujuk');	
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNokaDetail'")->row()->nilai;
		if(count($url)<>0){ 
			$poli=null;
			$no=$_POST['no_rujuk'];
			
		}
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$no_rujukan,false,$context));
		if ($res){
			echo "{success:true, data: ".json_encode($res)." ,headers:".json_encode($headers)."}";
		}
		else{
			echo "{success:false}";
		}    
	}
	
	/*public function cariRujukanFKTLDetail() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai;
		if(count($url)<>0){ 
			$poli=null;
			$no=$_POST['no_rujuk'];
			
		}
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		
		$urlnya='';
		$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoRujukan'")->row()->nilai.$no;
		
		$res = json_decode(file_get_contents($urlnya,false,$context));
		if ($res)
		{
			echo "{success:true, data: ".json_encode($res)." ,headers:".json_encode($headers)."}";
		}
		else
		{
			echo "{success:false}";
		}    
	}*/
	public function cariRujukanFKTL() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai;
		if(count($url)<>0){ 
			$klinik=$_POST['klinik'];
			$res=$this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='".$klinik."'");
			$poli=null;
			$no=$_POST['no_kartu'];
			if($res->result()){
				$poli=$res->row()->unit_bpjs;
			} 
		}
		$headers=$this->getSignature_dvlp();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$tipenya=$_POST['tipe'];
		$start=$_POST['startnya'];
		$limit=$_POST['limitnya'];
		$tglrujuk=$_POST['tgl_rujuk'];
		$urlnya='';
		if ($tipenya=='bpjs')
		{
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai.'/'.$no;
		}
		else if ($tipenya=='norujukan')
		{
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoRujukan'")->row()->nilai.$no;
			
		}else if ($tipenya=='tglrujuk')
		{
			
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByTanggal'")->row()->nilai.$tglrujuk.'/query?start='.$start.'&limit='.$limit.' ';
		}
		
		$res     = json_decode(file_get_contents($urlnya,false,$context));
	//	var_dump($res); die();
		$dat=array(
			'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);

		if ($res)
		{
			echo "{success:true,data_respon:".json_encode($dat)." }";
		}
		else
		{
			echo "{success:false}";
		}  
	}

	private function getSignature_dvlp(){
		$header="";
		$tmp_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret_dvlp'")->row()->nilai;
		$tmp_costumerID= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID_dvlp'")->row()->nilai;
		$data      = $tmp_costumerID;
		$secretKey = $tmp_secretKey;
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		$encodedSignature = base64_encode($signature);	
		//$header 	= "Accept:JSON\n"; 
		//$header 	.= "Content-Type:application/json\n";
		$header 	.= "X-cons-id: " .$data ."\n";
		$header 	.= "X-timestamp:" .$tStamp ."\n";
		$header 	.=  "X-signature: " .$encodedSignature."\n";
		return $header;
	}
   private function insert_response_bpjs($url, $resp, $params = null, $method = null, $data_pasien = null){
		$code 		= 0;
		$message 	= "Connection Error";
		$response 	= "";

		foreach ($resp as $key => $value) {
			if ($key == 'metaData') {
				foreach ($value as $records => $val_records) {
					if ($records == 'code') {
						$code = $val_records;
					}else if($records == 'message'){
						$message = $val_records;
					}
				}
			}
		}

		foreach ($resp as $key => $value) {
			if ($key == 'response') {
				$response = json_encode($value);
			}
		}

		$param = array();
		$param['id'] 		= $this->get_max_id_resp_bpjs();
		$param['url'] 		= $url;
		// $param['message'] 	= "[".$code."] - ".$message;
		$param['message'] 	= "[".$code."] - ".$message;
		$param['params']	= $params;
		$param['response']	= $response;
		$param['method']	= $method;
		if ($data_pasien != null) {
		// var_dump($data_pasien);
			// $param['kd_pasien'] = $data_pasien['kd_pasien'];
			// $param['tgl_masuk'] = $data_pasien['tgl_masuk'];
			// $param['kd_unit']   = $data_pasien['kd_unit'];
		}
		$param['waktu']		= "Time request : ".$this->jam_request.":".$this->menit_request.":".$this->detik_request." / Time response : ".$this->jam_request.":".date('i:s');
		$this->db->insert("response_bpjs", $param);
	}
	private function get_max_id_resp_bpjs(){
		$this->db->select(" MAX(id) as max");
		$this->db->from("response_bpjs");
		return (int)$this->db->get()->row()->max + 1;
	}
	private function getSignature(){
		$tmp_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_costumerID= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		
		$data      = $tmp_costumerID;
		$secretKey = $tmp_secretKey;
		// Computes the timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		// Computes the signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		
		// base64 encode…
		$encodedSignature = base64_encode($signature);
		
		// urlencode…
		// $encodedSignature = urlencode($encodedSignature);
		
		$header 	= "Accept:JSON\n";
		$header 	.= "Content-Type:application/json\n";
		$header 	.= "X-cons-id: " .$data ."\n";
		$header 	.= "X-timestamp:" .$tStamp ."\n";
		$header 	.=  "X-signature: " .$encodedSignature."\n";
		return $header; 
	
	}
	public function cari_data_kunjungan_monitoring(){
		$jenis_pelayanan=$this->input->post('poli_pelayanan');
		$tgl_sep= date('Y-m-d', strtotime($this->input->post('tgl_sep')));
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res_dvlp'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='url_monitoring_kunjungan'")->row()->nilai;
		$headers=$this->getSignature_dvlp();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'Tanggal/'.$tgl_sep.'/JnsPelayanan/'.$jenis_pelayanan,false,$context));
		echo '{success:true, totalrecords:'.count($res->response->sep).', listData:'.json_encode($res->response->sep).'}';
	}
	public function cari_data_peserta_monitoring(){
		$no_kartu	=$this->input->post('no_kartu');
		$tgl_awal	=date('Y-m-d', strtotime($this->input->post('tgl_awal')));
		$tgl_akhir	=date('Y-m-d', strtotime($this->input->post('tgl_akhir')));
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res_dvlp'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='url_monitoring_pelayanan_peserta'")->row()->nilai;
		$headers=$this->getSignature_dvlp();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		//"https://dvlp.bpjs-kesehatan.go.id/vclaim-rest/monitoring/HistoriPelayanan/NoKartu/0001023082986/tglAwal/2018-09-01/tglAkhir/2018-10-11"
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$no_kartu.'/tglAwal/'.$tgl_awal.'/tglAkhir/'.$tgl_akhir,false,$context));
		echo '{success:true, totalrecords:'.count($res->response->histori).', listData:'.json_encode($res->response->histori).'}';
	}

	public function hapus_history_sep(){
		$no_sep=$this->input->post('no_sep');
		$delete=$this->db->query("UPDATE kunjungan set no_sep='' where no_sep='".$no_sep."' ");
		if($delete){
			echo "{success:true}";
		}else{
			echo "{success:false,'exception:$error'}";
		}
	}
	public function simpan_pengajuan(){
		$noKartu=$_POST['noKartu'];
		$tglSep=str_replace("T00:00:00","",$_POST['tglSep']);
		$jnsPelayanan=$_POST['jnsPelayanan'];
		$keterangan=$_POST['keterangan'];
		$json='{
			       "request": {
			          "t_sep": {
			             "noKartu": "'.$noKartu.'",
			             "tglSep": "'.$tglSep.'",
			             "jnsPelayanan": "'.$jnsPelayanan.'",
			             "keterangan": "'.$keterangan.'",
			             "user": "Coba Ws"
			          }
			       }
			    }';
		//echo $json; die();
		$url=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlPengajuanSEP'")->row()->nilai;
		//$get_data_bpjs=$this->db->query("select * from rs_visit a inner join rs_patient b on a.patient_id=b.patient_id where no_pendaftaran='$noRegistrasi'");
		//$getJson=$this->getJsonReq($get_data_bpjs);
		$headers=$this->getSignatureVedika_dvlp();
		//$xml   = simplexml_load_string($data);
		//$json  = json_encode($xml);
		//$array = json_decode($json,TRUE); 
		$ch = curl_init();
		//$data = '{"request":"d"}';
		// echo $json; die();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		//$headers[]='Content-Type: Application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		$response = curl_exec($ch);
		curl_close ($ch);

	//	$this->insert_response_bpjs($url, json_decode($response), $jsonData, "POST");
		echo $response;
	}

	private function getSignatureVedika_dvlp(){
		$tmp_secretKey=$this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret_dvlp'")->row()->nilai;
		$tmp_costumerID=  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID_dvlp'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID."&".$tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: ".$tmp_costumerID,"X-Timestamp: ".$tStamp,"X-Signature: ".$encodedSignature);
	}

	private function getSignature_new(){
		$tmp_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$data      = $tmp_costumerID;
		$secretKey = $tmp_secretKey;
		// Computes the timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		// Computes the signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		
		// base64 encode…
		$encodedSignature = base64_encode($signature);
		
		// urlencode…
		// $encodedSignature = urlencode($encodedSignature);
		
		$header 	= "Accept:JSON\n";
		$header 	.= "Content-Type:application/json\n";
		$header 	.= "X-cons-id: " .$data ."\n";
		$header 	.= "X-timestamp:" .$tStamp ."\n";
		$header 	.=  "X-signature: " .$encodedSignature."\n";
		return $header;
	}	

 public function get_unit_rawat_inap(){
 	$query=$this->db->query("SELECT * from map_spc_bpjs a 	INNER JOIN spesialisasi b ON a.kd_spc::INTEGER = b.kd_spesial  WHERE a.kd_spc_bpjs='".$this->input->post('kd_unit')."' ")->result();
 	echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
 }	
	


}
?>
