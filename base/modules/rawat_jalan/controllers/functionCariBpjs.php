<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionCariBpjs extends  MX_Controller
{
	private $jam_request  = "";
	private $menit_request  = "";
	private $detik_request  = "";
	private $waktu_respones = "";
	public 	$ErrLoginMsg = '';
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		$this->load->library('LZCompressor/lzstring');
		$this->jam_request = date('H');
		$this->menit_request = date('i');
		$this->detik_request = date('s');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function getDataBpjs()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
		if (count($url) <> 0) {
			$klinik = $_POST['klinik'];
			$res = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $klinik . "'");
			$poli = null;
			$no = $_POST['no_kartu'];
			if ($res->result()) {
				$poli = $res->row()->unit_bpjs;
			}
		}
		$headers = $this->getSignature();
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$context = stream_context_create($opts);
		//https://dvlp.bpjs-kesehatan.go.id/vclaim-rest/Peserta/nokartu/0000104951406/tglSEP/2018-09-27
		$res     = json_decode(file_get_contents($url . $no . "/tglSEP/" . date('Y-m-d'), false, $context));
		$dat = array(
			'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);

		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url . $no . "/tglSEP/" . date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat);
	}

	function decompress($string, $timestamp)
	{

		$conspwd = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$consid = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		date_default_timezone_set('UTC');
		// $time = strval(time() - strtotime('1970-01-01 00:00:00'));
		$time = $timestamp;
		$key = $consid . $conspwd . $time;
		$encrypt_method = 'AES-256-CBC';
		$key_hash = hex2bin(hash('sha256', $key));
		$iv = substr(hex2bin(hash('sha256', $key)), 0, 16);
		$string = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
		$string = $this->lzstring->decompressFromEncodedURIComponent($string);
		$string = json_decode($string);
		return $string;
	}

	public function cariRujukanFKTP()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoka'")->row()->nilai;
		if (count($url) <> 0) {
			$klinik = $_POST['klinik'];
			$res = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $klinik . "'");
			$poli = null;
			$no = $_POST['no_kartu'];
			if ($res->result()) {
				$poli = $res->row()->unit_bpjs;
			}
		}
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		$tipenya = $_POST['tipe'];
		$start = $_POST['startnya'];
		$limit = $_POST['limitnya'];
		$tglrujuk = $_POST['tgl_rujuk'];
		$urlnya = '';
		//echo $tipenya;
		if ($tipenya == 'bpjs') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoka'")->row()->nilai . $no;
		} else if ($tipenya == 'norujukan') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoRujukan'")->row()->nilai . $no;
		}
		//echo $urlnya;
		// $res     = json_decode(file_get_contents($urlnya, false, $context));
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);

		$dat = array(
			'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);

		$this->insert_response_bpjs($urlnya, $dat['data'], json_encode($res), "GET");
		if ($res) {
			//	var_dump($res);
			/*	echo "{success:true,data_respon:".json_encode($dat)." }";*/
			$jml = 0;
			if ($res->metaData == '200') {
				$jml = count($res->response->rujukan);
			}
			echo '{success:true, totalrecords:' . $jml . ', data_respon:' . json_encode($dat) . '}';
		} else {
			echo "{success:false}";
		}
	}

	public function cariRujukanFKTLDetail()
	{
		$no_rujukan = $this->input->post('no_rujuk');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNokaDetail'")->row()->nilai;
		if (count($url) <> 0) {
			$poli = null;
			$no = $_POST['no_rujuk'];
		}
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $no_rujukan, false, $context));
		$urlnya = $url . $no_rujukan;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		// $this->insert_response_bpjs($url . $no_rujukan, $res, json_encode($res), "GET");
		if ($res) {
			echo "{success:true, data: " . json_encode($res) . " ,headers:" . json_encode($headers) . "}";
		} else {
			echo "{success:false}";
		}
	}
	// egi 20-12-2021--------------------------------------------------------------------------
	public function getListObat()
	{
		$obat = $this->input->post('nama_obat');
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariObatPRB'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $obat, false, $context));
		$urlnya = $url . $obat;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$this->insert_response_bpjs($url . $obat, $res, json_encode($res), "GET");
		if ($res->metaData->code == 200) {
			echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($res->response->list) . '}';
		} else {
			echo "{success:false}";
		}
	}
	//--------------------------------------------------------------------------- 
	public function cariRujukanFKTPDetail()
	{
		$no_rujukan = $this->input->post('no_rujuk');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNokaDetail'")->row()->nilai;
		if (count($url) <> 0) {
			$poli = null;
			$no = $_POST['no_rujuk'];
		}
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $no_rujukan, false, $context));
		$urlnya = $url . $no_rujukan;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$this->insert_response_bpjs($url . $no_rujukan, $res, json_encode($res), "GET");
		if ($res) {
			echo "{success:true, data: " . json_encode($res) . " ,headers:" . json_encode($headers) . "}";
		} else {
			echo "{success:false}";
		}
	}

	public function cariRujukanFKTL()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai;
		if (count($url) <> 0) {
			$klinik = $_POST['klinik'];
			$res = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $klinik . "'");
			$poli = null;
			$no = $_POST['no_kartu'];
			if ($res->result()) {
				$poli = $res->row()->unit_bpjs;
			}
		}
		$headers = $this->getSignature_dvlp();
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$context = stream_context_create($opts);
		$tipenya = $_POST['tipe'];
		$start = $_POST['startnya'];
		$limit = $_POST['limitnya'];
		$tglrujuk = $_POST['tgl_rujuk'];
		$urlnya = '';
		if ($tipenya == 'bpjs') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai . '/' . $no;
		} else if ($tipenya == 'norujukan') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoRujukan'")->row()->nilai . $no;
		} else if ($tipenya == 'tglrujuk') {

			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByTanggal'")->row()->nilai . $tglrujuk . '/query?start=' . $start . '&limit=' . $limit . ' ';
		}

		$res     = json_decode(file_get_contents($urlnya, false, $context));
		//	var_dump($res); die();
		$dat = array(
			'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);

		if ($res) {
			echo "{success:true,data_respon:" . json_encode($dat) . " }";
		} else {
			echo "{success:false}";
		}
	}

	private function getSignature_dvlp()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}
	private function insert_response_bpjs($url, $resp, $params = null, $method = null, $data_pasien = null)
	{
		$code 		= 0;
		$message 	= "Connection Error";
		$response 	= "";

		foreach ($resp as $key => $value) {
			if ($key == 'metaData') {
				foreach ($value as $records => $val_records) {
					if ($records == 'code') {
						$code = $val_records;
					} else if ($records == 'message') {
						$message = $val_records;
					}
				}
			}
		}

		foreach ($resp as $key => $value) {
			if ($key == 'response') {
				$response = json_encode($value);
			}
		}

		$param = array();
		$param['id'] 		= $this->get_max_id_resp_bpjs();
		$param['url'] 		= $url;
		// $param['message'] 	= "[".$code."] - ".$message;
		$param['message'] 	= "[" . $code . "] - " . $message;
		$param['params']	= $params;
		$param['response']	= $response;
		$param['method']	= $method;
		if ($data_pasien != null) {
			// var_dump($data_pasien);
			// $param['kd_pasien'] = $data_pasien['kd_pasien'];
			// $param['tgl_masuk'] = $data_pasien['tgl_masuk'];
			// $param['kd_unit']   = $data_pasien['kd_unit'];
		}
		$param['waktu']		= "Time request : " . $this->jam_request . ":" . $this->menit_request . ":" . $this->detik_request . " / Time response : " . $this->jam_request . ":" . date('i:s');
		$this->db->insert("response_bpjs", $param);
	}
	private function get_max_id_resp_bpjs()
	{
		$this->db->select(" MAX(id) as max");
		$this->db->from("response_bpjs");
		return (int)$this->db->get()->row()->max + 1;
	}
	private function getSignature()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}
	public function cari_data_kunjungan_monitoring()
	{
		$jenis_pelayanan = $this->input->post('poli_pelayanan');
		$tgl_sep = date('Y-m-d', strtotime($this->input->post('tgl_sep')));
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res_dvlp'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_monitoring_kunjungan'")->row()->nilai;
		$headers = $this->getSignature_dvlp();
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url . 'Tanggal/' . $tgl_sep . '/JnsPelayanan/' . $jenis_pelayanan, false, $context));
		echo '{success:true, totalrecords:' . count($res->response->sep) . ', listData:' . json_encode($res->response->sep) . '}';
	}
	// public function cari_data_peserta_monitoring()
	// {
	// 	$no_kartu	= $this->input->post('no_kartu');
	// 	$tgl_awal	= date('Y-m-d', strtotime($this->input->post('tgl_awal')));
	// 	$tgl_akhir	= date('Y-m-d', strtotime($this->input->post('tgl_akhir')));
	// 	$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res_dvlp'")->row()->setting;
	// 	$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_monitoring_pelayanan_peserta'")->row()->nilai;
	// 	$headers = $this->getSignature_dvlp();
	// 	$opts = array(
	// 		'http' => array(
	// 			'method' => 'GET',
	// 			'header' => $headers
	// 		)
	// 	);
	// 	//"https://dvlp.bpjs-kesehatan.go.id/vclaim-rest/monitoring/HistoriPelayanan/NoKartu/0001023082986/tglAwal/2018-09-01/tglAkhir/2018-10-11"
	// 	$context = stream_context_create($opts);
	// 	$res = json_decode(file_get_contents($url . $no_kartu . '/tglAwal/' . $tgl_awal . '/tglAkhir/' . $tgl_akhir, false, $context));
	// 	echo '{success:true, totalrecords:' . count($res->response->histori) . ', listData:' . json_encode($res->response->histori) . '}';
	// }

	public function cari_data_peserta_monitoring()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlMonitoringPelayanan'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		$no = $_POST['no_kartu'];
		$tgl_awal = $_POST['tgl_awal'];
		$tgl_akhir = $_POST['tgl_akhir'];
		// $res = json_decode(file_get_contents($url . '/' . $no, false, $context));
		$urlnya = $url . '' . $no . '/' . 'tglMulai/' . $tgl_awal . '/tglAkhir/' . $tgl_akhir;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		// echo '<pre>' . var_export($response, true) . '</pre>';
		// 		echo $urlnya;
		// 		die;
		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		$this->insert_response_bpjs($url . '/' . $no, $res, json_encode($res), "GET");
		echo json_encode($dat);
	}

	public function cari_data_rencana_kontrol_noka()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='urlGetNoSuratKontrolByNoka'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		$no = $_POST['no_kartu'];
		$bulan = $_POST['bulan'];
		$tahun = $_POST['tahun'];
		$jenis = $_POST['jenis'];
		// $res = json_decode(file_get_contents($url . '/' . $no, false, $context));
		$urlnya = $url . '' . $bulan . '/Tahun/' . $tahun . '/Nokartu/' . $no . '/filter/' . $jenis;
		// echo $urlnya;
		// die();
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		// $this->insert_response_bpjs($url . '/' . $no, $res, json_encode($res), "GET");
		echo json_encode($dat);
	}

	// -------------------------Data Jadwal Dokter-------------------------
	// Egi 01 Maret 2022
	public function cari_data_jadwal_dokter()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='urlGetJadwalDokter'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		$tgl 	= $_POST['tgl'];
		$jenis	= $_POST['jenis'];
		$poli	= $_POST['poli'];

		$urlnya = $url . $jenis . '/KdPoli/' . $poli . '/TglRencanaKontrol/' . $tgl;
		// echo $urlnya;
		// die();
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		// $this->insert_response_bpjs($urlnya, $res, json_encode($res), "GET");
		echo json_encode($dat);
	}
	// --------------------------------------------------------------------

	private function searchMessage($arr)
	{
		$message = '';
		if (gettype($arr) == 'object') {
			foreach ($arr as $key => $value) {
				if (gettype($value) != 'object' && gettype($value) != 'array') {
					if ($key == 'field') {
						$message .= $value;
					}
					if ($key == 'message') {
						$message .= ' ' . $value . '<br>';
					}
				} else {
					$message .= $this->searchMessage($value);
				}
			}
		} else if (gettype($arr) == 'array') {
			for ($i = 0, $iLen = count($arr); $i < $iLen; $i++) {
				$message .= $this->searchMessage($arr[$i]);
			}
		} else if (gettype($arr) == 'string') {
			$message .= $arr;
		}
		return $message;
	}

	public function hapus_history_sep()
	{
		$no_sep = $this->input->post('no_sep');
		$delete = $this->db->query("UPDATE kunjungan set no_sep='' where no_sep='" . $no_sep . "' ");
		if ($delete) {
			echo "{success:true}";
		} else {
			echo "{success:false,'exception:$error'}";
		}
	}
	public function simpan_pengajuan()
	{
		$noKartu = $_POST['noKartu'];
		$tglSep = str_replace("T00:00:00", "", $_POST['tglSep']);
		$jnsPelayanan = $_POST['jnsPelayanan'];
		$keterangan = $_POST['keterangan'];
		$json = '{
			       "request": {
			          "t_sep": {
			             "noKartu": "' . $noKartu . '",
			             "tglSep": "' . $tglSep . '",
			             "jnsPelayanan": "' . $jnsPelayanan . '",
			             "keterangan": "' . $keterangan . '",
			             "user": "Coba Ws"
			          }
			       }
			    }';
		//echo $json; die();
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlPengajuanSEP'")->row()->nilai;
		//$get_data_bpjs=$this->db->query("select * from rs_visit a inner join rs_patient b on a.patient_id=b.patient_id where no_pendaftaran='$noRegistrasi'");
		//$getJson=$this->getJsonReq($get_data_bpjs);
		$headers = $this->getSignatureVedika_dvlp();
		//$xml   = simplexml_load_string($data);
		//$json  = json_encode($xml);
		//$array = json_decode($json,TRUE); 
		$ch = curl_init();
		//$data = '{"request":"d"}';
		// echo $json; die();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		//$headers[]='Content-Type: Application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);

		//	$this->insert_response_bpjs($url, json_decode($response), $jsonData, "POST");
		echo $response;
	}

	private function getSignatureVedika_dvlp()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}

	private function getSignature_new()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}

	public function get_unit_rawat_inap()
	{
		$query = $this->db->query("SELECT * from map_spc_bpjs a 	INNER JOIN spesialisasi b ON a.kd_spc::INTEGER = b.kd_spesial  WHERE a.kd_spc_bpjs='" . $this->input->post('kd_unit') . "' ")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}
}
