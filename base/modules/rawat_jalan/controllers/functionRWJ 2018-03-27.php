<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionRWJ extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

   } 
   //##tambah validasi kunjungan untuk pasien yang akan generate SEP
   public function cekKunjunganPasien()
   {
	   $kd_pasien=$_POST['NoMedrec'];
	   $tgl_masuk=date('Y-m-d');
	   $kd_unit=$_POST['Poli'];
	   $cekKunjungan=$this->db->query("select * from kunjungan where kd_pasien='$kd_pasien' and tgl_masuk='$tgl_masuk' and kd_unit='$kd_unit' ")->result();
	   if (count($cekKunjungan)==0)
	   {
		   echo "{success:true}";
	   }
	   else
	   {
		   echo "{success:false}";
	   }
   }
   //##tambah validasi kunjungan untuk pasien yang akan generate SEP
   public function getDataRujukan()
   {
	   $kdrujuk=$_POST['koderujuk'];
	   $namanya='';
	   $alamatnya='';
	   $kotanya='';
	   $caridata=$this->db->query("select * from rujukan where kd_rujukan=$kdrujuk ")->result();
	   foreach($caridata as $d)
	   {
		   $nama=$d->rujukan;
		   $alamat=$d->alamat;
		   $kota=$d->kota;
	   }
	   echo "{nama: '".$nama."', alamat: '".$alamat."', kota: '".$kota."'}";
   }
   public function getAntrian()
   {
	    error_reporting(0); 
	   //echo $_POST['grupLoket'];
	   if ($_POST['grupLoket']!='kosong')
	   {
		   $carinomorsemua=$this->db->query("select max(no_antrian) as no_antrian from antrian_list_antrian where tanggal='".date('Y-m-d')."' AND id_group='".$_POST['grupLoket']."' ")->row()->no_antrian;
		   $carinomorterpanggil=$this->db->query("select max(no_antrian) as no_antrian from antrian_list_antrian where tanggal='".date('Y-m-d')."' AND id_group='".$_POST['grupLoket']."' and is_dipanggil='1'")->row()->no_antrian;
		   $carinomorbelumterpanggil=$this->db->query("select no_antrian from antrian_list_antrian where tanggal='".date('Y-m-d')."' AND id_group='".$_POST['grupLoket']."' and is_dipanggil='0' order by no_antrian asc limit 1")->row()->no_antrian;
		   
	   }
	   else
	   {
		   //$idgrup=$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['kodeloket']."' ")->row()->id_group;
		   //$cariantriannosemua=$this->db->query("select * as no_antrian from antrian_list_antrian where kode_loket='".$idgrup."' and tanggal='".date('Y-m-d')."' ")->result();
		   //$carinomorsemua=$this->db->query("select max(no_antrian) as no_antrian from antrian_list_antrian where id_group='".$idgrup."' and tanggal='".date('Y-m-d')."' ")->row()->no_antrian;
		   //$carinomorterpanggil=$this->db->query("select max(no_antrian) as no_antrian from antrian_list_antrian where id_group='".$idgrup."' and tanggal='".date('Y-m-d')."' and is_dipanggil='1'")->row()->no_antrian;
		   //$carinomorbelumterpanggil=$this->db->query("select no_antrian from antrian_list_antrian where id_group='".$idgrup."' and tanggal='".date('Y-m-d')."' and is_dipanggil='0' order by no_antrian asc limit 1")->row()->no_antrian;
		   //$carinomorsemua = 0;
	   }
	   echo "{semuanomor: '".$carinomorsemua."', semuanomorterpanggil: '".$carinomorterpanggil."', semuanomorbelumterpanggil: '".$carinomorbelumterpanggil."'}";
   }
	public function simpanDisplayAntrian()
	{
		//$loketnya                =$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
		$sql                       =$this->db->query("update display_antrian set no_antrian=".$_POST['nomor_antri']." , urut_loket=".$_POST['loket'].", prioritas=".$_POST['prioritas']."  where loket='".$_POST['grup']."' ");
		$sql                       =$this->db->query("update display_antrian set cari_loket='".$_POST['grup']."'");
		$jam                       =$this->db->query("select jam from antrian_list_antrian where is_dipanggil = '0' order by no_antrian asc")->row()->jam;
		if($_POST['txt_prioritas'] == "false" || $_POST['txt_prioritas'] == false){
			$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='1' where tanggal='".date('Y-m-d')."' 
			and no_antrian = '".$_POST['nomor_antri']."' and id_group= '".$_POST['grup']."'  ");
		}//else{
		//	$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='0' where tanggal='".date('Y-m-d')."' 
		//	and no_antrian = '".$_POST['nomor_antri']."' ");
		//}
		if ($sql)
		{
			echo "{success: true}";
		}
		else
		{
			echo "{success: false}";
		}
	}
   public function ulangiPanggilDisplayAntrian()
   {
	   $loketnya=$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
	   $updateantrian=$this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='1' where tanggal='".date('Y-m-d')."' and id_group= '".$loketnya."' and no_antrian='".$_POST['nomor_antri']."' ");
	   $sql=$this->db->query("update display_antrian set no_antrian=".$_POST['nomor_antri']." , urut_loket=".$_POST['loket'].", prioritas=TRUE where loket='".$loketnya."' ");
	   $sql=$this->db->query("update display_antrian set cari_loket='".$loketnya."'");
	   if ($sql)
	   {
		   echo "{success: true}";
	   }
	   else
	   {
		   echo "{success: false}";
	   }
   }
	public function getDataBpjs() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
		if(count($url)<>0){ 
			$klinik=$_POST['klinik'];
			$res=$this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='".$klinik."'");
			$poli=null;
			$no=$_POST['no_kartu'];
			if($res->result()){
				$poli=$res->row()->unit_bpjs;
			} 
		}
		$headers=$this->getSignature();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		// echo $url.$no."/tglSEP/".date("Y-m-d");die();
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$no."/tglSEP/".date("Y-m-d"),false,$context));
		$dat=array(
			'poli'=>$poli,
			'user'=>$this->session->userdata['user_id']['id'],
			'data'=>$res,
			'headers'=>$headers,
			'kd_rs'=>$bpjs_kd_res
		);
		echo json_encode($dat);
	}
	
	public function getDataBpjsDenganNIK() {
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNIK'")->row()->nilai;
		$no=$_POST['no_kartu'];
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$this->getSignature()
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$no,false,$context));
		$dat=array(
			'data'=>null
		);
		if($res->metadata->code=='200'){
			$dat['data']=$res;
		}else{
			$dat['message']=$this->searchMessage($res);
		}
		echo json_encode($dat);
	}
	public function getDataBpjsDenganSEP() {
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai; 
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$this->getSignature()
		  )
		);
		$context = stream_context_create($opts);
		$no=$_POST['no_sep'];
		$res = json_decode(file_get_contents($url.'/'.$no,false,$context));
		$dat=array(
			'data'=>null
		);
		if($res->metadata->code=='200'){
			$dat['data']=$res;
		}else{
			$dat['message']=$this->searchMessage($res);
		}
		echo json_encode($dat);
	}
	public function cariRujukanFKTL() {
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai;
		$no=$_POST['no_kartu'];
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$this->getSignature()
		  )
		);
		/* echo $headers;
		var_dump($opts);*/
		//echo $bpjs_url_briging.$_POST['noKartu']; 
		$context = stream_context_create($opts);
		$urlnya='';
		$tipenya=$_POST['tipe'];
		$start=$_POST['startnya'];
		$limit=$_POST['limitnya'];
		$tglrujuk=$_POST['tgl_rujuk'];
		if ($tipenya=='bpjs')
		{
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai.'nokartu/'.$no;
		}
		else if ($tipenya=='norujukan')
		{
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai.$no;
			
		}else if ($tipenya=='tglrujuk')
		{
			
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByTanggal'")->row()->nilai.$tglrujuk.'/query?start='.$start.'&limit='.$limit.' ';
		}
		$now=new DateTime();
		//echo $tipenya.' aaa';
		//echo $now->format("Y/m/d H:i:s");
		$res = json_decode(file_get_contents($urlnya,false,$context));
		$dat=array(
			'data'=>null
		);
		if($res->metadata->code=='200'){
			$dat['data']=$res;
		}else{
			$dat['message']=$this->searchMessage($res);
		}
		echo json_encode($dat);
		
	}
	
	public function cariRujukanFKTP() {
		$bpjs_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$bpjs_data= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$bpjs_url_briging= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
		$bpjs_url_getSep= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlBuatSEP'")->row()->nilai;
		 if(count($bpjs_url_briging)<>0)
		{ 
			date_default_timezone_set('UTC');
			$tStamp = time();//strval(time()-strtotime('1970-01-01 00:00:00'));
			$data = $bpjs_data;
			$secretKey = $bpjs_secretKey;
			$klinik=$_POST['klinik'];
			$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
			$encodedSignature = base64_encode($signature);
			$res=$this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='".$klinik."'");
			$poli=null;
			$no=$_POST['no_kartu'];
			$headers="X-cons-id: ".$data."\r\n" .
					  "X-timestamp: ".$tStamp."\r\n".
					  "X-signature: ".$encodedSignature."\r\n";
			/* $result=array(
				'headers'=>$headers,
				//'id'=>$data,
				//'timestamp'=>	$tStamp,
				//'signature'=>	$encodedSignature,
				'kd_rs'=>	$bpjs_kd_res,
				//'url_briging'=>$common->getSystemProperty('URL_BRIGING', $defaultTenant)->getPropertyValue(),
				'url_getSep'=>$bpjs_url_getSep,
			); */
				if($res->result()){
					$poli=$res->row()->unit_bpjs;
				} 
				$result=array(
					'id'=>$data,
					'timestamp'=>	$tStamp,
					'signature'=>	$encodedSignature,
					'kd_rs'=>	$bpjs_kd_res,
					'url_briging'=>$bpjs_url_briging,
					'url_getSep'=>$bpjs_url_getSep,
					'poli'=>$poli
				); 
			
		//echo json_encode ($result );
		}
		//$headers.=' Content-type: Application/x-www-form-urlencoded\r\n';
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		/* echo $headers;
		var_dump($opts);*/
		//echo $bpjs_url_briging.$_POST['noKartu']; 
		$context = stream_context_create($opts);
		$urlnya='';
		$tipenya=$_POST['tipe'];
		$start=$_POST['startnya'];
		$limit=$_POST['limitnya'];
		$tglrujuk=$_POST['tgl_rujuk'];
		if ($tipenya=='bpjs')
		{
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoka'")->row()->nilai.$no;
		}
		else if ($tipenya=='norujukan')
		{
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoRujukan'")->row()->nilai.$no;
			
		}else if ($tipenya=='tglrujuk')
		{
			
			$urlnya=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByTanggal'")->row()->nilai.$tglrujuk.'/query?start='.$start.'&limit='.$limit.' ';
		}
		$now=new DateTime();
		//echo $tipenya.' aaa';
		//echo $now->format("Y/m/d H:i:s");
		$res = json_decode(file_get_contents($urlnya,false,$context));
		if ($res)
		{
			echo "{success:true, poli:'".$poli."',urlbridging:'".$urlnya."', data: ".json_encode($res).",headers:".json_encode($headers).",url_getSep:'".$bpjs_url_getSep."' , id:'".$data."', timestamp:'".$tStamp."', signature:'".$encodedSignature."'}";
		}
		else
		{
			echo "{success:false}";
		} 
		
	}
	private function searchMessage($arr){
		$message='';
		if(gettype($arr)=='object'){
			foreach($arr as $key=>$value){
				if(gettype($value)!='object' && gettype($value)!='array'){
					if($key=='field'){
						$message.=$value;
					}
					if($key=='message'){
						$message.=' '.$value.'<br>';
					}
				}else{
					$message.=$this->searchMessage($value);
				}
			}
		}else if(gettype($arr)=='array'){
			for($i=0,$iLen=count($arr);$i<$iLen;$i++){
				$message.=$this->searchMessage($arr[$i]);
			}
		}else if(gettype($arr)=='string'){
			$message.=$arr;
		}
		return $message;
	}
	public function createSEP(){
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlBuatSEP'")->row()->nilai;
		$data=$_POST['data'];
		$headers=$this->getSignature();
		$headers.='Content-type: Application/x-www-form-urlencoded\r\n';
		$opts = array(
		  'http'=>array(
			'method'=>"POST",
			'header'=>$headers,
			'content'=>$data
		  )
		);
		$context = stream_context_create($opts);
		$res=json_decode(file_get_contents($url,false,$context));
		$data=array();
		$data['sep']=null;
		if($res->metadata->code=='200'){
			$data['sep']=$res->response;
		}else{
			$data['message']=$this->searchMessage($res);
		}
		echo json_encode($data);
	}
	public function hapusSEP() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlDeleteSEP'")->row()->nilai;
		$headers=$this->getSignature();
		$headers.='Content-type: Application/x-www-form-urlencoded\r\n';	
		$data='<request>
			<data>
			<t_sep>
			<noSep>' .$_POST['sep'].'</noSep>
			<ppkPelayanan>' .$bpjs_kd_res. '</ppkPelayanan>
			</t_sep>
			</data>
			</request>';
		$opts = array(
		  'http'=>array(
			'method'=>"DELETE",
			'header'=>$headers,
			'content'=>$data
		  )
		);
		$context = stream_context_create($opts);
		$res=json_decode(file_get_contents($url,false,$context));
		$data=array();
		$data['data']=$res->response;
		if($res->metadata->code=='200'){
			$data['result']='SUCCESS';
		}else{
			$data['result']='ERROR';
			$data['message']=$this->searchMessage($res);
		}
		echo json_encode($data);
	}
	
	public function updatePulangSEP() {
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlUpdateTgl'")->row()->nilai;
		$headers=$this->getSignature();
		$headers.='Content-type: Application/x-www-form-urlencoded\r\n';	
		//echo 'awdw';
		$tgl_pulang=$_POST['tgl'];
		
		$data='<request>
			<data>
			<t_sep>
			<noSep>' .$_POST['sep'].'</noSep>
			<tglPlg>' . $tgl_pulang .'</tglPlg>
			<ppkPelayanan>' .$bpjs_kd_res. '</ppkPelayanan>
			</t_sep>
			</data>
			</request>';
		$opts = array(
		  'http'=>array(
			'method'=>"PUT",
			'header'=>$headers,
			'content'=>$data
		  )
		);
		
		$context = stream_context_create($opts);
		$res=json_decode(file_get_contents($url,false,$context));
		$data=array();
		$data['data']=$res->response;
		if($res->metadata->code=='200'){
			$data['result']='SUCCESS';
		}else{
			$data['result']='ERROR';
			$data['message']=$this->searchMessage($res);
		}
		echo json_encode($data);
	}
	
	public function cariHistorySEP(){
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlHistoryPasien'")->row()->nilai;
		$no=$_POST['no_kartu'];
		$opts = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>$this->getSignature()
		  )
		);
		$context = stream_context_create($opts);
		$res=json_decode(file_get_contents($url.$no,false,$context));
		$dat=array(
			'data'=>null
		);
		if($res->metadata->code=='200'){
			$dat['data']=$res;
		}else{
			$dat['message']=$this->searchMessage($res);
		}
		echo json_encode($dat);
	}
	
	
	public function cek_order_mng()
	{	 
		$kd_pasien=$_POST['kd_pasien'];
		$kd_unit_knj=$_POST['kd_unit'];
		$tgl_masuk_knj=$_POST['tgl_masuk_knj'];
		 $cek=$this->db->query("select tgl_op,to_char(jam_op, 'HH24:MI' ) as jam_op,no_kamar,kd_tindakan  from ordr_mng_ok where kd_pasien='$kd_pasien' and kd_unit_knj='$kd_unit_knj' and tgl_masuk_knj='$tgl_masuk_knj'")->result();
		 foreach ($cek as $d)
		 {
			 $tgl_op=$d->tgl_op;
			 $jam_op=$d->jam_op;
			 $no_kamar=$d->no_kamar;
			 $kd_tindakan=$d->kd_tindakan;
		 }
		 if(count($cek)>0){
			echo "{success:true , tgl_op:".json_encode($tgl_op).", jam_op:".json_encode($jam_op).", no_kamar:".json_encode($no_kamar).", kd_tindakan:".json_encode($kd_tindakan)."}";
		}else{
			echo "{success:false, data: null}";
		}
			
		
		 
	}
	   	public function caridata_sepbpjs() {
		$bpjs_counter= $this->db->query("select counter_sep  from counter_sep limit 1")->row()->counter_sep;
		$bpjs_counter2=substr($bpjs_counter,16,19);
		$bpjs_counter2=$bpjs_counter2+1;
		if(	strlen($bpjs_counter2)==1)
			{
			$bpjs_counter="";
			$no="00";
			$bpjs_counter="1311R00112150000".$no.$bpjs_counter2;
			}else if(	strlen($bpjs_counter2)==2)
			{
			$bpjs_counter="";
			$no="0";
			$bpjs_counter="1311R00112150000".$no.$bpjs_counter2;
			}else{
			$bpjs_counter="";
			$no="";
			$bpjs_counter="1311R00112150000".$no.$bpjs_counter2;
			}
			
		$bpjs_counter_update= $this->db->query("update counter_sep  set counter_sep='$bpjs_counter' ");
		
		echo json_encode ($bpjs_counter );
		}
	
	 public function printPernyataan(){
	 	$d_nama='&nbsp;';
	 	$d_tgl_lahir='&nbsp;';
	 	$d_alamat1='&nbsp;';
	 	$d_alamat2='&nbsp;';
	 	$d_telp='&nbsp;';
	 	$d_nama2='&nbsp;';
	 	$d_medrec=$this->uri->segment(4,0);
	 	$d_jaminan='&nbsp;';
	 	$d_peserta='&nbsp;';
	 	$d_hubungan='&nbsp;';
	 	
	 	$q_penanggung_jawab=$this->db->query("SELECT a.nama_pj,a.tgl_lahir,a.alamat,a.kd_pos,a.kota,a.telepon,a.no_hp,a.hubungan,B.kelurahan,c.kecamatan FROM penanggung_jawab A
	 	INNER JOIN kelurahan B ON B.kd_kelurahan=A.kd_kelurahan
	 	INNER JOIN kecamatan C ON C.kd_kecamatan=B.kd_kecamatan WHERE kd_pasien='".$d_medrec."' ORDER BY tgl_masuk desc limit 1");
	 	if(count($q_penanggung_jawab->result())>0){
	 		$o_penanggung_jawab=$q_penanggung_jawab->row();
	 		$d_nama=$o_penanggung_jawab->nama_pj;
	 		$d_tgl_lahir=date('d',strtotime($o_penanggung_jawab->tgl_lahir)).' '.$this->common->getMonthByIndex(date('m',strtotime($o_penanggung_jawab->tgl_lahir))-1).' '.date('Y',strtotime($o_penanggung_jawab->tgl_lahir));
	 		$d_alamat1=$o_penanggung_jawab->alamat;
	 		$d_alamat2='Kel. '.$o_penanggung_jawab->kelurahan.' Kec. '.$o_penanggung_jawab->kecamatan.' Kota '.$o_penanggung_jawab->kota.' '.$o_penanggung_jawab->kd_pos;
	 		if($o_penanggung_jawab->telepon !=''){
	 			$d_telp.='Rumah ('.$o_penanggung_jawab->telepon.')';
	 		}
	 		if($o_penanggung_jawab->telepon !=''){
	 			if($d_telp!=''){
	 				$d_telp.=', ';
	 			}
	 			$d_telp.='HP ('.$o_penanggung_jawab->no_hp.')';
	 		}
	 		if($o_penanggung_jawab->hubungan==1){
	 			$d_hubungan='Kep. Keluarga';
	 		}else if($o_penanggung_jawab->hubungan==2){
	 			$d_hubungan='Suami';
	 		}else if($o_penanggung_jawab->hubungan==3){
	 			$d_hubungan='Istri';
	 		}else if($o_penanggung_jawab->hubungan==4){
	 			$d_hubungan='Anak Kandung';
	 		}else if($o_penanggung_jawab->hubungan==5){
	 			$d_hubungan='Anak Tiri';
	 		}else if($o_penanggung_jawab->hubungan==6){
	 			$d_hubungan='Anak Angkat';
	 		}else if($o_penanggung_jawab->hubungan==7){
	 			$d_hubungan='Ayah';
	 		}else if($o_penanggung_jawab->hubungan==8){
	 			$d_hubungan='Ibu';
	 		}else if($o_penanggung_jawab->hubungan==9){
	 			$d_hubungan='Mertua';
	 		}else if($o_penanggung_jawab->hubungan==10){
	 			$d_hubungan='Sdr Kandung';
	 		}else if($o_penanggung_jawab->hubungan==11){
	 			$d_hubungan='Cucu';
	 		}else if($o_penanggung_jawab->hubungan==12){
	 			$d_hubungan='Famili';
	 		}else if($o_penanggung_jawab->hubungan==13){
	 			$d_hubungan='Pembantu';
	 		}else{
	 			$d_hubungan='Lain-Lain';
	 		}
	 	}
	 	$q_pasien=$this->db->query("SELECT * FROM pasien WHERE kd_pasien='".$d_medrec."'");
	 	if(count($q_pasien->result())>0){
	 		$o_pasien=$q_pasien->row();
	 		$d_nama2=$o_pasien->nama;
	 		$d_peserta=$o_pasien->no_asuransi;
	 	}
	 	
	 	$q_kunjungan=$this->db->query("SELECT customer FROM kunjungan A INNER JOIN customer B ON B.kd_customer=A.kd_customer WHERE A.kd_pasien='".$d_medrec."' ORDER BY tgl_masuk DESC limit 1");
	 	if(count($q_kunjungan->result())>0){
	 		$o_kunjungan=$q_kunjungan->row();
	 		$d_jaminan=$o_kunjungan->customer;
	 	}
	 	$html='';
	 	$html.='<table><tr><th>SURAT PERNYATAAN</center></th></tr></table><br></br><br><div class="normal">';
	 	$html.='Saya yang bertanda-tangan di bawah ini:<br><br>';
	 	$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12;
				}
	 			td, div{
	 				text-align: justify;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: 1px dotted black;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12px;
   				}
           </style>';
	 	$html.='<table>
	 				<tr>
	 					<td width="80">Nama</td>
	 					<td width="10">:</td>
	 					<td width="300" class="bottom">'.$d_nama.'</td>
	 					<td width="80">Tgl. Lahir</td>
	 					<td width="10">:</td>
	 					<td class="bottom">'.$d_tgl_lahir.'</td>
	 				</tr>
	 				<tr>
	 					<td width="80">Alamat</td>
	 					<td width="10">:</td>
	 					<td colspan="4" class="bottom">'.$d_alamat1.'</td>
	 				</tr>
	 				<tr>
	 					<td width="80"></td>
	 					<td width="10"></td>
	 					<td colspan="4" class="bottom">'.$d_alamat2.'</td>
	 				</tr>
	 				<tr>
	 					<td width="80">No. Telp.HP</td>
	 					<td width="10">:</td>
	 					<td colspan="4" class="bottom">'.$d_telp.'</td>
	 				</tr>
	 			</table><br>
	 			<table><tr><td style="width: auto;white-space: nowrap;">Hubungan dengan pasien, adalah</td><td width="10">:</td><td class="bottom">'.$d_hubungan.'</td></tr></table>
	 			<br>';
	 	$html.='Bahwa Saya melakukan pendaftaran untuk, pasien an:<br><br>';
	 	$html.='<table>
	 				<tr>
	 					<td width="80">Nama Pasien</td>
	 					<td width="10">:</td>
	 					<td width="300" class="bottom">'.$d_nama2.'</td>
	 					<td width="80">No Med.Rec.</td>
	 					<td width="10">:</td>
	 					<td class="bottom">'.$d_medrec.'</td>
	 				</tr>
	 				<tr>
	 					<td width="80">Jaminan</td>
	 					<td width="10">:</td>
	 					<td width="300" class="bottom">'.$d_jaminan.'</td>
	 					<td width="80">No Peserta</td>
	 					<td width="10">:</td>
	 					<td class="bottom">'.$d_peserta.'</td>
	 				</tr>
	 			</table><br>';
	 	$html.='Menyatakan dengan sesungguhnya bahwa saya:<br><br>';
	 	$html.='<table>
	 				<tr>
	 					<td width="30" align="center" valign="top">1.</td>
	 					<td>Menyadari dan mengetahui bahwa manfaat asuransi dan penjaminan untuk kesehatan mempunyai batasan yang telah ditentukan</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">2.</td>
	 					<td>Berjanji jika oleh sebab apapun juga, biaya rawat inap tidak sesuai dengan indikasi dan menimbulkan indikasi biaya, maka saya berkewajiban untuk membayar selisih biaya tersebut
	 						kepada Rumah Sakit dengan Uang Muka maupun pembayaran pada saat pulang atau jika selisih biaya tersebut diberitahukan kemudian hari maka saya bersedia menyelesaikan kewajiban tersebut kepada
	 						Rumah Sakit ataupun penjamin</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">3.</td>
	 					<td>Bersedia mematuhi ketentuan penjamin dan jika menurut ketentuan biaya yang terjadi tidak dijamin, maka saya bersedia mendapatkan pelayanan kesehatan dengan status pasien umum
	 						hingga akhir hari rawat dan tidak berubah jaminan di tengah perawatan</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">4.</td>
	 					<td>
	 						<table>
	 							<tr>
	 								<td>Menempati Ruang: </td>
	 								<td width="10">(</td>
	 								<td width="80" class="bottom"></td>
	 								<td width="10">)</td>
	 								<td>Kelas: </td>
	 								<td width="10">(</td>
	 								<td width="80" class="bottom"></td>
	 								<td width="10">)</td>
	 								<td>dg harga saat ini: </td>
	 								<td width="10">(</td>
	 								<td width="10">Rp</td>
	 								<td width="80" class="bottom"></td>
	 								<td width="10">)</td>
	 							</tr>
	 						</table>
	 						Karena :
	 						<table>
	 							<tr>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250">Atas Permintaan Sendiri</td>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250">Kelas sesuai hak pasien, Tidak Tersedia</td>
	 								<td></td>
	 							</tr>
	 							<tr>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250">Kelas sesuai hak pasien, Penuh</td>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250"><table><tr><td width="10">(</td><td class="bottom"></td><td width="10">)</td></tr></table></td>
	 								<td></td>
	 							</tr>
	 						</table>
	 					</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">5.</td>
	 					<td>Bersedia membayar selisih biaya yang terjadi,</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">&nbsp;</td>
	 					<td>
	 						<table>
	 							<tr>
	 								<td width="15">a)</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">&nbsp;</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">b)</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">&nbsp;</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">c)</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">&nbsp;</td>
	 								<td class="bottom"></td>
	 							</tr>
	 						</table>
	 					</td>
	 				</tr>
	 			</table><br>';
	 	$html.='Demikian Surat Pernyataan ini saya buat dalam keadaan sadar, tanpa tekanan dan paksaan dan pihak manapun dan dapat mempertanggungjawabkan dengan sebaik-baiknya
	 			dan selanjutnya dapat dipergunakan sebagaimana mestinya.<br><br>';
	 	$html.='<table>
	 				<tr>
	 					<td width="10"></td>
	 					<td align="center" width="150">Yang Menyatakan,</td>
	 					<td width="10"></td>
	 					<td></td>
	 					<td width="10"></td>
	 					<td align="center" width="150">Ttd, Petugas Rumah Sakit</td>
	 					<td width="10"></td>
	 				</tr>
	 				<tr>
	 					<td>&nbsp;</td>
	 				</tr>
	 				<tr>
	 					<td>&nbsp;</td>
	 				</tr>
	 				<tr>
	 					<td>&nbsp;</td>
	 				</tr>
	 				<tr>
	 					<td width="10">(</td>
	 					<td align="center" width="150" class="bottom"></td>
	 					<td width="10">)</td>
	 					<td></td>
	 					<td width="10">(</td>
	 					<td align="center" width="150" class="bottom"></td>
	 					<td width="10">)</td>
	 				</tr>
	 			</table>';
	 	$html.='</div>';
	 	$prop=array('foot'=>false);
	 	$this->common->setPdf('P','Surat Peryataan',$html,$prop);
	 }
	 public function printlebarkeluarmasuk(){
	 $d_medrec=$this->uri->segment(4,0);
	 	$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', 'A4');
		$mpdf->AddPage($type, // L - landscape, P - portrait
				'', '', '', '',
				$marginLeft, // margin_left
				15, // margin right
				15, // margin top
				15, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i");
		$arr = array (
				'odd' => array (
						'L' => array (
								'content' => 'Operator : '.$name,
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'C' => array (
								'content' => "Tgl/Jam : ".$date."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'R' => array (
								'content' => '{PAGENO}{nbpg}',
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'line' => 0,
				),
				'even' => array ()
		);
		for($x=0; $x<35; $x++)
						 {
						 $SPASI.='&nbsp;';
						 $underline.='_';
						 }
	 	$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:1px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 12px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$jkx="";
		$gdarah="";
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr align=justify>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='70' height='50'/>
   				</th>
   				<th align=justify >
   					<b>".$rs->name."</b><br>
			   		<font style='font-size: 11px;font-family: Arial'><b>".$rs->address.", ".$rs->city."</b></font>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>".$telp."</b></font><br>
   				</th>
   			</tr>
   		</table>";
	 	$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><tr border='0'><th > Lembar Masuk Dan Keluar</center></th></tr></table>";
	 	$getkunjungan=$this->db->query("
					
					select kunj.*,kel.kelas, case when jenis_cust=2 then'asuransi' when jenis_cust=1 then 'Langganan' else 'Perorangan'  end as kelpas, dok.nama as namadok from kunjungan kunj inner 
					join  kontraktor kon on kunj.kd_customer=kon.kd_customer  inner join unit on kunj.kd_unit=unit.kd_unit inner join
					kelas kel on unit.kd_kelas=kel.kd_kelas inner join dokter dok on dok.kd_dokter=kunj.kd_dokter where kd_pasien='".$d_medrec."'  and left(kunj.kd_unit,1)='1' order by tgl_masuk desc limit 1
							
		")->row();
		  $getpenanggungjawab=$this->db->query("select pen.*,pek.pekerjaan,kel.kelurahan,kec.kecamatan,kab.kabupaten from penanggung_jawab pen inner join pekerjaan pek 
		  on pek.kd_pekerjaan=pen.kd_pekerjaan
			inner join kelurahan kel on pen.kd_kelurahan=kel.kd_kelurahan
			inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
			inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
											  where kd_pasien='".$d_medrec."' order by tgl_masuk desc limit 1
		
		")->row();
		    
		$GETPASIEN=$this->db->query("select p.kd_pasien,alamat,nama,tempat_lahir,tgl_lahir,nama_keluarga,( extract ( year from age(tgl_lahir))|| ' tahun ' || 
									extract ( month from age(tgl_lahir))|| ' bulan ' ||extract ( day from age(tgl_lahir))|| ' hari '  ) ::text as umur,agama
									,pek.pekerjaan,telepon,kec.kecamatan,kab.kabupaten,prof.propinsi,jenis_kelamin,pen.pendidikan,kelurahan, gol_darah,status_marita from pasien p inner join agama a on p.kd_agama=a.kd_agama
									inner join pekerjaan pek on pek.kd_pekerjaan=p.kd_pekerjaan inner join Pendidikan pen on pen.kd_pendidikan=p.kd_pendidikan
									inner join kelurahan kel on p.kd_kelurahan=kel.kd_kelurahan
									inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
									inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
									inner join propinsi prof on prof.kd_propinsi=kab.kd_propinsi
									where p.kd_pasien='".$d_medrec."'")->row();
		if($GETPASIEN->jenis_kelamin=='f')
		{
		$jkx='Perempuan';
		}else if($GETPASIEN->jenis_kelamin=='t')
		{
		$jkx='laki-laki';
		}if($GETPASIEN->gol_darah==0)
		{
		$gdarah='-';
		}else if($GETPASIEN->gol_darah==1)
		{
		$gdarah='A+';
		}else if($GETPASIEN->gol_darah==2)
		{
		$gdarah='B+';
		}else if($GETPASIEN->gol_darah==3)
		{
		$gdarah='AB+';
		}else if($GETPASIEN->gol_darah==4)
		{
		$gdarah='O+';
		}else if($GETPASIEN->gol_darah==5)
		{
		$gdarah='A-';
		}else if($GETPASIEN->gol_darah==6)
		{
		$gdarah='B-';
		}else if($GETPASIEN->gol_darah==7)
		{
		$gdarah='AB-';
		}else if($GETPASIEN->gol_darah==8)
		{
		$gdarah='O-';
		}
		$marita="";
		if($GETPASIEN->status_marita==0)
		{
		$marita='Blm Kawin';
		}else if($GETPASIEN->status_marita==1)
		{$marita='Kawin';
		}else if($GETPASIEN->status_marita==2)
		{$marita='Janda';
		}else if($GETPASIEN->status_marita==3)
		{$marita='Duda';
		}
		$kelurahan="";
		if($GETPASIEN->kelurahan=="DEFAULT" ||$GETPASIEN->kelurahan=="")
		{
		$kelurahan='-';
		}else
		{
		$kelurahan=$GETPASIEN->kelurahan;
		}
		$kecamatan="";
		if($GETPASIEN->kecamatan=="DEFAULT" ||$GETPASIEN->kecamatan=="")
		{
		$kecamatan='-';
		}else
		{
		$kecamatan=$GETPASIEN->kecamatan;
		}$kabupaten="";
		if($GETPASIEN->kabupaten=="DEFAULT" ||$GETPASIEN->kabupaten=="")
		{
		$kabupaten='-';
		}else
		{
		$kabupaten=$GETPASIEN->kabupaten;
		}$propinsi="";
		if($GETPASIEN->propinsi=="DEFAULT" ||$GETPASIEN->propinsi=="")
		{
		$propinsi='-';
		}else
		{
		$propinsi=$GETPASIEN->propinsi;
		}
		$d_hubungan="";
	if($getpenanggungjawab->hubungan==1){
	 			$d_hubungan='Kep. Keluarga';
	 		}else if($getpenanggungjawab->hubungan==2){
	 			$d_hubungan='Suami';
	 		}else if($getpenanggungjawab->hubungan==3){
	 			$d_hubungan='Istri';
	 		}else if($getpenanggungjawab->hubungan==4){
	 			$d_hubungan='Anak Kandung';
	 		}else if($getpenanggungjawab->hubungan==5){
	 			$d_hubungan='Anak Tiri';
	 		}else if($getpenanggungjawab->hubungan==6){
	 			$d_hubungan='Anak Angkat';
	 		}else if($getpenanggungjawab->hubungan==7){
	 			$d_hubungan='Ayah';
	 		}else if($getpenanggungjawab->hubungan==8){
	 			$d_hubungan='Ibu';
	 		}else if($getpenanggungjawab->hubungan==9){
	 			$d_hubungan='Mertua';
	 		}else if($getpenanggungjawab->hubungan==10){
	 			$d_hubungan='Sdr Kandung';
	 		}else if($getpenanggungjawab->hubungan==11){
	 			$d_hubungan='Cucu';
	 		}else if($getpenanggungjawab->hubungan==12){
	 			$d_hubungan='Famili';
	 		}else if($getpenanggungjawab->hubungan==13){
	 			$d_hubungan='Pembantu';
			}else if($getpenanggungjawab->hubungan==14){
	 			$d_hubungan='Lain-lain';
	 		}else{
	 			$d_hubungan='';
	 		}
			$pekerjaanpen="";
			if ($getpenanggungjawab->pekerjaan=="")
			{$pekerjaanpen="<p>&nbsp;";}
			else{$pekerjaanpen=$getpenanggungjawab->pekerjaan;}
			$alamatpen="";
			
			if ($getpenanggungjawab->alamat=="")
			{$alamatpen="<p>&nbsp;";}
			else{$alamatpen=$getpenanggungjawab->alamat;}
			$kelpen="";
			if ($getpenanggungjawab->kelurahan=="")
			{$kelpen="<p>&nbsp;";}
			else if ($getpenanggungjawab->kelurahan=="DEFAULT")
			{$kelpen="-";}
			else{$kelpen=$getpenanggungjawab->kelurahan;}
			$kecpen="";
			if ($getpenanggungjawab->kecamatan=="")
			{$kecpen="<p>&nbsp;";
			}
			else if ($getpenanggungjawab->kecamatan=="DEFAULT")
			{
			$kecpen="-";
			}
			else{
			$kecpen=$getpenanggungjawab->kecamatan;
			}
			
			$kabpen="";
			if ($getpenanggungjawab->kabupaten=="")
			{$kabpen="<p>&nbsp;";}
			else if ($getpenanggungjawab->kabupaten=="DEFAULT")
			{$kabpen="-";}
			else{$kabpen=$getpenanggungjawab->kabupaten;}
			
			$jammas="";
			if ($getkunjungan->jam_masuk=="")
			{$jammas="<p>&nbsp;";}
			
			else{$jammas=date('H:i:s', strtotime($getkunjungan->jam_masuk));}
			$tglmas="";
			if ($getkunjungan->tgl_masuk=="")
			{$tglmas="<p>&nbsp;";
			}else{$tglmas=date('Y-F-d', strtotime($getkunjungan->tgl_masuk));}
			
			$tlppen="";
			if ($getpenanggungjawab->telepon=="")
			{$tlppen="<p>&nbsp;";
			}else{$tlppen=$getpenanggungjawab->telepon;}
			$hppen="";
			if ($getpenanggungjawab->no_hp=="")
			{$hppen="<p>&nbsp;";
			}else{$hppen=$getpenanggungjawab->no_hp;}
			$kelpas="";
			if ($getkunjungan->kelpas=="")
			{$kelpas="<p>&nbsp;";
			}else{$kelpas=$getkunjungan->kelpas;}
			
			$asuransipas="";
			if ($GETPASIEN->no_asuransi=="")
			{$asuransipas="<p>&nbsp;";
			}else{$asuransipas=$GETPASIEN->no_asuransi;}
			
			$kelaskamr="";
			if ($getkunjungan->kelas=="")
			{$kelaskamr="<p>&nbsp;";}
			
			else{$kelaskamr=$getkunjungan->kelas;}
			$namadok="";
			if ($getkunjungan->namadok=="")
			{$namadok="<p>&nbsp;";}
			
			else{$namadok=$getkunjungan->namadok;}
			
			
			$html.='<table>
	 				<tr>
					<th border:none; align=right>  RM1</th>
					</tr>
	 				
	 			</table>
				<table>
	 				<tr>
	 					<td width="280" ><b>No RM</b> <p>  '.$GETPASIEN->kd_pasien.'</td>
						<td width="220"><b>Tanggal masuk</b> <p>  '.$tglmas.'</td>
						<td width="120"><b>Jam</b> <p>  '.$jammas.'</td>
	 					
	 				</tr>
	 				
	 			</table>
			
				<table>
					<tr>
	 					<td width="390"> <b>Nama</b> <p>  '.$GETPASIEN->nama.'</td>
						
	 					<td width="230"><b>Nama Keluarga</b> <p> '.$GETPASIEN->nama_keluarga.'</td>
	 				</tr>
				</table>
				<table>
					<tr>
						<td colspan="3"><b>Tempat tanggal lahir</b> <p>  '.$GETPASIEN->tempat_lahir.',&nbsp;'. date('Y-F-d', strtotime($GETPASIEN->tgl_lahir)).' </td>
	 					<td colspan="3"><b>Umur</b> <p>   '.$GETPASIEN->umur.'</td>
						<td colspan="1"><b>Kelamin</b> <p>  '.$jkx.'</td>
						<td colspan="1"><b>Darah</b> <p> '.$gdarah.'</td>
					</tr>
				</table>
				<table>
	 				<tr>
						<td width="96,7"><b>Status</b> <p>  '.$marita.'</td>
						<td width="96,7"><b>Agama</b> <p>   '.$GETPASIEN->agama.'</td>
	 					<td width="88,7"><b>Pendidikan</b> <p> '.$GETPASIEN->pendidikan.'</td>
						<td width="88,7"><b>Pekerjaan</b> <p>'.$GETPASIEN->pekerjaan.'  </td>
					</tr>
	 			</table>
				<table>
	 				
	 				<tr>
						<td colspan="8" align= "left"><b>Alamat</b> :'.$GETPASIEN->alamat.'</td>
						</tr>
	 				<tr>
						<td colspan="4" align= "left"><b> Kel :</b> '.$kelurahan.' </td>
	 					<td colspan="4" align= "left"><b>Kec :</b> '.$kecamatan.' </td>
						
	 				</tr>
					
	 				<tr>
						<td colspan="4" align= "left"><b>Kab/Kota :</b> '.$kabupaten.'  </td>
						<td colspan="2" align= "left"> <b>Telepon : </b>'.$GETPASIEN->telepon.'</td>
	 					<td colspan="2" align= "left"><b>Hp : </b> '.$GETPASIEN->handphone.'</td>
		
					</tr>
						<tr>
						<td colspan="2" align= "center"> <b>Penanggung Jawab </b><p>&nbsp;
						'.$d_hubungan.' </td>
	 					<td colspan="2" align= "center"><b>Pekerjaan </b><p>'.$pekerjaanpen.'</td>
						<td colspan="4" align= "center"><b>Alamat </b> <p>'.$alamatpen.'</td>
						</tr>
	 				<tr>
						<td colspan="4" align= "left"><b> Kel :</b> '.$kelpen.' </td>
	 					<td colspan="4" align= "left"><b>Kec :</b> '.$kecpen.'</td>
						
	 				</tr>
					
	 				<tr>
						<td colspan="4" align= "left"><b>Kab/Kota :</b> '.$kabpen.' </td>
						<td colspan="2" align= "left"> <b>Telepon : </b>'.$tlppen.' </td>
	 					<td colspan="2" align= "left"><b>Hp : </b> '.$hppen.' </td>
					</tr>
				<tr>
						<td colspan="1"><b>Kelompok Pasien</b><p>'.$kelpas.'<p>&nbsp;<br>&nbsp;</td>
						<td colspan="1"><b>Nomor Peserta</b><p>'.$asuransipas.'<p>&nbsp;<br>&nbsp;</td>
						 <td colspan="2" ><b> Surat Jaminan</b> 
						 <div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Sudah Ada<br>
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Belum Ada</div></td><br>&nbsp;
					    <td colspan="3"><b>Rujukan dari</b><div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Dokter&nbsp;&nbsp;&nbsp;
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Puskesmas </div>
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Bidan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Perawat</div>
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> lain-lain&nbsp;
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Kemauan Sendiri</td>
						  <td colspan="1"><b>Ruang rawat</b><p>'.$kelaskamr.'<p>&nbsp;<br>&nbsp;</td>

				</tr>
				<tr>
						<td colspan="8" align= "left"><b> Dokter Yang menerima :</b> '.$namadok.' <p>&nbsp;<p>   </td>
				</tr>
				<tr>
	 					<td colspan="8" align= "left"><b> Dokter Yang merawat :</b><p>&nbsp;<p>    </td>	
	 			</tr>
				<tr>
						<td colspan="8" align= "left"><b> Catatan Keluar :</b><p>&nbsp;<p>  </td>
				</tr>
				<tr>
						<td colspan="7" align= "left"><b> Diagnosa Akhir,Utama :</b> <p>&nbsp;  </td>
						<td colspan="1" align= "left"><b> kode :</b><p>&nbsp; </td>
				</tr>
				<tr>
	 					<td colspan="7" align= "left"><b> diagnosa Tambahan :</b> <p>&nbsp;</td>
						<td colspan="1" align= "left"><b> kode :</b> <p>&nbsp;	</td>	
								
	 			</tr>
				<tr>
						<td colspan="2"><b>Tanggal keluar</b><p>&nbsp;<p>&nbsp;</td>
						 <td colspan="2"><b>Lama Dirawat</b> 
						 <div style="float:left; width:1000px; border:1px">
						 Hari &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <p>
						 Minggu :<p>
						 Bulan &nbsp;&nbsp;&nbsp;:<p></td>
						<td colspan="3"><b>Tindakan Operasi</b><p>&nbsp;<p>&nbsp;<p> Tanggal : '.$SPASI.' WIB</td>
						<td colspan="1" align= "left"><b> kode :</b> <p>&nbsp;<p>&nbsp;<p>&nbsp;	</td>
				</tr>
				<tr>
	 					<td colspan="8" align= "center"  border:0px><b> Imunisasi </b> <p>&nbsp;<p>
						1.BGC &nbsp;&nbsp;&nbsp;&nbsp;
						2.DPT &nbsp;&nbsp;&nbsp;&nbsp;
						3.DT &nbsp;&nbsp;&nbsp;&nbsp;
						4.Campak&nbsp;&nbsp;&nbsp;&nbsp;
						5.Polio&nbsp;&nbsp;&nbsp;&nbsp;	
						6.Tetanus Formal Toxoid&nbsp;&nbsp;&nbsp;&nbsp;		
						</td>
				</tr>
				<tr>
						<td colspan="4" ><b>Cara Keluar</b> 
						 <div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Instruksi  Dokter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Kabur<br>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Permintaan dokter &nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> indah Rs Lain</div></td>
						<td colspan="4"><b>Kontrol Ke Poli</b><p>&nbsp;<p>&nbsp;</td>
				</tr>
				<tr>
						<td colspan="4" ><b>Tranfusi Darah</b> 
						 <div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Ya 
						 &nbsp;&nbsp;&nbsp;&nbsp;<span style="border: 1px solid black; float:left">'.$SPASI.'</span> Cc <br>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Tidak <br></div></td>
						<td colspan="4"><b>Tanggal :</b>'.$SPASI.'<b>Jam :<b><p>&nbsp;
				</tr>
					<tr>
	 					<td colspan="4" align= "center"  border:0px><b> Kedaaan Keluar </b> <p>&nbsp;<p>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Sembuh &nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mulai Sembuh &nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Belum Sembuh &nbsp;&nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Cacat&nbsp;&nbsp;&nbsp;<br><br>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati<4 Jam&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati<24 Jam&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati<48 Jam&nbsp;&nbsp;	
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati>48 Jam &nbsp;&nbsp;	
						<p>&nbsp;
						<p>&nbsp;							
						</td>
						<td colspan="4" align= "center"><b> Dokter Pemeriksa </b>
						<p>&nbsp;
						<p>&nbsp;
						<p>&nbsp;
						<p>&nbsp;
						<p><b> '.$underline.'</b>	
						<p>&nbsp;	
						<p>&nbsp;						
						</td>
				</tr>
				</tr>
	 			</table>
				<p>&nbsp;
				<p>&nbsp;
				<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span><b> *( Beri Tanda x)</b>
			
	 			';


		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="lembar keluar masuk.pdf"' );
		readfile ( 'original.pdf' );
	 }
	 public function save_log_bpjs()
	{
	$bpjs=$this->db->query("insert into log_bpjs(nilai,keterangan) values('".$_POST['respon']."','".$_POST['keterangan']."')");
	if($bpjs)
	{
	echo "{success:true}";
	}else{
	
	echo "{success:false}";
	}
	}
	 public function printlabelinap(){
	 $d_medrec=$this->uri->segment(4,0);
	 	$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', array(210,110));
		$mpdf->AddPage('P', // L - landscape, P - portrait
				'', '', '', '',
				24, // margin_left
				24, // margin right
				3, // margin top
				0, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i");
		$arr = array (
				'odd' => array (
						'L' => array (
								'content' => 'Operator : '.$name,
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'C' => array (
								'content' => "Tgl/Jam : ".$date."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'R' => array (
								'content' => '{PAGENO}{nbpg}',
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'line' => 0,
				),
				'even' => array ()
		);
		for($x=0; $x<35; $x++)
						 {
						 $SPASI.='&nbsp;';
						 $underline.='_';
						 }
	 	$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:0px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			table{
	   				width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 13px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
	
	 //$html.="<table style='font-size: 18;font-family: Times New Roman, Helvetica, sans-serif;' cellspacing='0' border='0'><tr border='0'><th > Lembar Masuk Dan Keluar</center></th></tr></table>";
	 	$getkunjungan=$this->db->query("
					
					select kunj.*,kel.kelas, case when jenis_cust=2 then'asuransi' when jenis_cust=1 then 'Langganan' else 'Perorangan'  end as kelpas, dok.nama as namadok ,cus.customer from kunjungan kunj inner 
					join  kontraktor kon on kunj.kd_customer=kon.kd_customer  inner join unit on kunj.kd_unit=unit.kd_unit inner join
					kelas kel on unit.kd_kelas=kel.kd_kelas inner join dokter dok on dok.kd_dokter=kunj.kd_dokter 
					inner join customer cus on cus.kd_customer=kunj.kd_customer where kd_pasien='".$d_medrec."'  and left(kunj.kd_unit,1)='1' order by tgl_masuk desc limit 1
							
		")->row();
		  $getpenanggungjawab=$this->db->query("select pen.*,pek.pekerjaan,kel.kelurahan,kec.kecamatan,kab.kabupaten from penanggung_jawab pen inner join pekerjaan pek 
		  on pek.kd_pekerjaan=pen.kd_pekerjaan
			inner join kelurahan kel on pen.kd_kelurahan=kel.kd_kelurahan
			inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
			inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
											  where kd_pasien='".$d_medrec."' order by tgl_masuk desc limit 1
		
		")->row();
		    
		$GETPASIEN=$this->db->query("select p.kd_pasien,nama_ayah,nama_ibu,alamat,nama,tempat_lahir,tgl_lahir,nama_keluarga,( extract ( year from age(tgl_lahir))|| ' tahun ' || 
									extract ( month from age(tgl_lahir))|| ' bulan ' ||extract ( day from age(tgl_lahir))|| ' hari '  ) ::text as umur,agama
									,pek.pekerjaan,telepon,kec.kecamatan,kab.kabupaten,prof.propinsi,jenis_kelamin,pen.pendidikan,kelurahan, gol_darah,status_marita from pasien p inner join agama a on p.kd_agama=a.kd_agama
									inner join pekerjaan pek on pek.kd_pekerjaan=p.kd_pekerjaan inner join Pendidikan pen on pen.kd_pendidikan=p.kd_pendidikan
									inner join kelurahan kel on p.kd_kelurahan=kel.kd_kelurahan
									inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
									inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
									inner join propinsi prof on prof.kd_propinsi=kab.kd_propinsi
									where p.kd_pasien='".$d_medrec."'")->row();
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><tr border='0'><th > ".$GETPASIEN->nama."</center></th></tr></table>";
	 	
		$jkx='';
		if($GETPASIEN != null && $GETPASIEN->jenis_kelamin=='f')
		{
		$jkx='Perempuan';
		}else if($GETPASIEN->jenis_kelamin=='t')
		{
		$jkx='Laki-laki';
		}if($GETPASIEN->gol_darah==0)
		{
		$gdarah='-';
		}else if($GETPASIEN->gol_darah==1)
		{
		$gdarah='A+';
		}else if($GETPASIEN->gol_darah==2)
		{
		$gdarah='B+';
		}else if($GETPASIEN->gol_darah==3)
		{
		$gdarah='AB+';
		}else if($GETPASIEN->gol_darah==4)
		{
		$gdarah='O+';
		}else if($GETPASIEN->gol_darah==5)
		{
		$gdarah='A-';
		}else if($GETPASIEN->gol_darah==6)
		{
		$gdarah='B-';
		}else if($GETPASIEN->gol_darah==7)
		{
		$gdarah='AB-';
		}else if($GETPASIEN->gol_darah==8)
		{
		$gdarah='O-';
		}
		$marita="";
		//echo $GETPASIEN->status_marita;
		if($GETPASIEN->status_marita==0)
		{
		$marita='Blm Kawin';
		}else if($GETPASIEN->status_marita==1)
		{$marita='Kawin';
		}else if($GETPASIEN->status_marita==2)
		{$marita='Janda';
		}else if($GETPASIEN->status_marita==3)
		{$marita='Duda';
		}
		$kelurahan="";
		if($GETPASIEN->kelurahan=="DEFAULT" ||$GETPASIEN->kelurahan=="")
		{
		$kelurahan='-';
		}else
		{
		$kelurahan=$GETPASIEN->kelurahan;
		}
		$kecamatan="";
		if($GETPASIEN !== null && ($GETPASIEN->kecamatan=="DEFAULT" ||$GETPASIEN->kecamatan==""))
		{
		$kecamatan='-';
		}else
		{
		$kecamatan=$GETPASIEN->kecamatan;
		}$kabupaten="";
		if($GETPASIEN->kabupaten=="DEFAULT" ||$GETPASIEN->kabupaten=="")
		{
		$kabupaten='-';
		}else
		{
		$kabupaten=$GETPASIEN->kabupaten;
		}$propinsi="";
		if($GETPASIEN->propinsi=="DEFAULT" ||$GETPASIEN->propinsi=="")
		{
		$propinsi='-';
		}else
		{
		$propinsi=$GETPASIEN->propinsi;
		}
		$d_hubungan="";
	
			$jammas="";
			if ($getkunjungan->jam_masuk=="")
			{$jammas="<p>&nbsp;";}
			
			else{$jammas=date('H:i:s', strtotime($getkunjungan->jam_masuk));}
			$tglmas="";
			if ($getkunjungan->tgl_masuk=="")
			{$tglmas="<p>&nbsp;";
			}else{$tglmas=date('Y-F-d', strtotime($getkunjungan->tgl_masuk));}
			
			$kelpas="";
			if ($getkunjungan->kelpas=="")
			{$kelpas="<p>&nbsp;";
			}else{$kelpas=$getkunjungan->kelpas;}
			
			$asuransipas="";
			if ($GETPASIEN->no_asuransi=="")
			{$asuransipas="<p>&nbsp;";
			}else{$asuransipas=$GETPASIEN->no_asuransi;}
			$kelaskamr="";
			if ($getkunjungan->kelas=="")
			{$kelaskamr="<p>&nbsp;";}
			
			else{$kelaskamr=$getkunjungan->kelas;}
			$kelaskamr="";
			if ($getkunjungan->namadok=="")
			{$namadok="<p>&nbsp;";}
			
			else{$namadok=$getkunjungan->namadok;}
			
			
			$html.='<table>
	 				<tr>
					<td border:none; align=center> '.$jkx.'/&nbsp;'.date('d-F-Y', strtotime($GETPASIEN->tgl_lahir)).'</td>
					</tr>
	 				
	 			</table>
				<br>
				
				<table style="font-size: 16;font-family: Times New Roman, Helvetica, sans-serif;">
	 				<tr>
					<th border:none;> <b>IDENTITAS</b><t>
					</tr>
	 				
	 			</table>
				<table >
	 				<tr>
	 					<td width="10"></td>
						<td width="10"></td>
						<td width="10"></td>
	 					<td width="10"><b>No medrec</b></td>
	 					<td width="120"><b>: </b>'.$GETPASIEN->kd_pasien.'</td>
	 					
	 				</tr>
	 				<tr>
						
	 					<td><b>1.Nama Lengkap</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="200"><b>'.$GETPASIEN->nama.'</b></td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>2.Tempat & tanggal lahir</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="200">'.$GETPASIEN->Tempat_lahir.','.date_format(date_create($GETPASIEN->tgl_lahir), 'Y-m-d').'</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>3.Jenis Kelamin</b></td>
	 					<td width="10"><b>:<b></td>
	 					<td width="200">'.$jkx.'</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>4.Agama</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="200">'.$GETPASIEN->agama.'</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>5.Pendidikan</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="100">'.$GETPASIEN->pendidikan.'</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>6.Pekerjaan</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="100">'.$GETPASIEN->pekerjaan.'</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>7.Alamat</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="350">'.$GETPASIEN->alamat.'</td>
	 					
	 				</tr>
							<tr >
	 					<td >&nbsp;&nbsp;&nbsp;<b>Kelurahan</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td>'.$kelurahan.'</td>
	 					<td ><b> Kecamatan</b></td>
	 					<td width="120"><b>:</b>&nbsp;'.$kecamatan.'</td>
	 					
	 				</tr >
						<tr>
	 					<td >&nbsp;&nbsp;&nbsp;<b>Kab/kota</b></td>
	 					<td width="10"><b>:</b></td>
						<td>'.$kabupaten.'</td>
	 					<td ><b> Provinsi</b></td>
	 					<td width="120"><b>:</b>&nbsp;'.$propinsi.'</td>
	 				</tr>
					<tr >
	 					<td width="180"><b>8.Nama Ayah/Nama Ibu</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="90">'.$GETPASIEN->nama_ayah.','.$GETPASIEN->nama_ibu.'</td>
	 					<td width="100"><b></b></td>
	 					<td width="10"><b></b></td>
	 					<td ></td>
	 				</tr>
					<tr>
	 					<td width="120"><b>9.Nama Istri/Nama Suami</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="250"></td>
	 				</tr>
					<tr>
	 					<td width="120"><b>10.Status Pasien</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="250">'.$marita.'</td>
	 				</tr>
					<tr>
	 					<td width="120"><b>11.Jenis pembayaran</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="250">'.$getkunjungan->customer.' ('.$getkunjungan->kelpas.')</td>
	 				</tr>
					<tr>
	 					<td width="180"><b>12.Nama Penanggung Jawab</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="90">-</td>
	 					<td width="100"><b>Tlpn/HP</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td >'.$GETPASIEN->telepon.'&nbsp;&nbsp;'.$GETPASIEN->handphone.'</td>
	 				</tr>
					<tr>
	 					<td width="180">&nbsp;&nbsp;&nbsp;<b>Tanggal Print </b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="90"> '. date('Y-F-d') .'</td>
	 					<td width="10"></td>
	 					<td width="10"></td>
	 					<td ></td>
	 				</tr>
	 			</table>
			<table >
			
			</table>
				<table>
	 				
				</table>
				<br>
	 				
	 			';


		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="lembar keluar masuk.pdf"' );
		readfile ( 'original.pdf' );
	 }
	public function cetakstatuspasienumum()
	{
		
   		$common=$this->common;
   		$result=$this->result;
   		$title=' ';
		$param=json_decode($_POST['data']);
		
		/* $kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		} */
		
		/* $queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result(); */
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$kdpasien=$param->nomedrec;
		$html.='
			<table width="100%" border="0">
				<tr>
				  <td align="right">&nbsp;</td>
				</tr>
				<tr>
				  <td align="center"><h1><strong>IRJ UMUM</strong></h1></td>
				</tr>
			  </table>
			  <p></p>';
			
		
		$queryPasien=$this->db->query("select kd_pasien,telepon, nama, to_char(tgl_lahir, 'DD-MM-YYYY' ) as tgl_lahir , case when date_part('year',age(Tgl_Lahir))<=5 then
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn '||
		 to_char(date_part('month',age(Tgl_Lahir)), '999')||' bln ' ||
		 to_char(date_part('days',age(Tgl_Lahir)), '999')||' hari'
		 else
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn'
		end
		  as umur, kd_pendidikan,case when jenis_kelamin=  true then 'L' else 'P' end as jk , 
		  ag.agama , alamat , nama_ibu, nama_ayah,  pekerjaan 
		  from pasien 
		  inner join agama ag on pasien.kd_agama= ag.kd_agama 
		  inner join pekerjaan pek on pasien.kd_pekerjaan = pek.kd_pekerjaan
		  where kd_pasien='$kdpasien' ");
		$data_pasien=$queryPasien->row();
		$belumsekolah='';
		$sd='';
		$smp='';
		$smk='';
		$d3='';
		$s1='';
		$s3='';
		$prof='';
		$jkl='';
		$jkp='';
		if ($data_pasien->kd_pendidikan == 0)
		{
			$belumsekolah=' checked="checked"';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}
		else if ($data_pasien->kd_pendidikan == 2)
		{
			$belumsekolah='';
			$sd=' checked="checked"';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 3 || $data_pasien->kd_pendidikan == 1)
		{
			$belumsekolah='';
			$sd='';
			$smp=' checked="checked"';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 5)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3=' checked="checked"';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 6)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1=' checked="checked"';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 7 || $data_pasien->kd_pendidikan == 4)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma=' checked="checked"';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 8)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3=' checked="checked"';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 9)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof=' checked="checked"';
		}else if ($data_pasien->kd_pendidikan == 10)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk=' checked="checked"';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}
		
		if ($data_pasien->jk == 'L')
		{
			$jkl=' checked="checked"';
			$jkp='';
		}
		else
		{
			$jkl='';
			$jkp=' checked="checked"';
		}
		if (count($queryPasien->result())==0)
		{
			$html.='<table width="100%" border="0">
					<tr>
					  <td width="54%" align="center"><h1>DATA TIDAK ADA</h1>
					  </td>
					</tr>
					</table>';
		}
		else
		{
			$html.='
				  <table width="100%" border="1">
					<tr>
					  <td width="54%"><center>
						&nbsp; <strong>REKAM MEDIS PASIEN RAWAT JALAN</strong>
					  </center></td>
					  <td width="46%"><center>
						&nbsp; <strong>NO. RM : '.$data_pasien->kd_pasien.'</strong>
					  </center></td>
					</tr>
					<tr>
					  <td height="27" colspan="2"><table width="100%" border="0">
						<tr>
						  <td width="17%">Nama</td>
						  <td width="2%">:</td>
						  <td colspan="2">'.$data_pasien->nama.'</td>
						  <td width="16%"><input type="checkbox" name="chbLaki" id="chbLaki" '.$jkl.'>
							<label for="chbLaki">Lk </label></td>
						  <td width="18%"><input type="checkbox" name="chbPerempuan" id="chbPerempuan" '.$jkp.'>
							<label for="chbPerempuan">Pr</label></td>
						</tr>
						<tr>
						  <td>Tgl. lahir / Umur</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->tgl_lahir.' / '.$data_pasien->umur.'</td>
						  <td>Agama </td>
						  <td>: '.$data_pasien->agama.'</td>
						</tr>
						<tr>
						  <td>Pendidikan</td>
						  <td>:</td>
						  <td colspan="4"><input type="checkbox" name="chbBelumSekolah" id="chbBelumSekolah" '.$belumsekolah.'>
							<label for="chbBelumSekolah">Belum Sekolah
							  <input type="checkbox" name="chbTk" id="chbTk">
							  TK </label>
							<input type="checkbox" name="chbSd" id="chbSd" '.$sd.'>
							SD
							<label for="chbSd"> </label>
							<input type="checkbox" name="chbSltp" id="chbSltp" '.$smp.'>
							SLTP
							<label for="chbSltp"> </label>
							<input type="checkbox" name="chbSlta" id="chbSlta" '.$sma.'  '.$smk.'>
							SLTA
							<label for="chbSlta"> </label>
							<input type="checkbox" checked name="chbAkademi" id="chbAkademi"  '.$s1.'  '.$d3.'  '.$s3.' '.$prof.' >
							<label for="chbAkademi">PT / Akademi </label></td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->alamat.'</td>
						  <td>No. PHB / Asuransi I </td>
						  <td>:</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>Telp</td>
						  <td>: '.$data_pasien->telepon.'</td>
						</tr>
						<tr>
						  <td>Nama Orang Tua</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->nama_ayah.' / '.$data_pasien->nama_ibu.'</td>
						  <td>Pekerjaan</td>
						  <td>: '.$data_pasien->pekerjaan.'</td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->alamat.'</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Cara Pembayaran</td>
						  <td>:</td>
						  <td width="24%"><input type="checkbox" name="chbBayarSendiri" id="chbBayarSendiri">
							<label for="chbBayarSendiri">Bayar Sendiri</label></td>
						  <td width="23%"><input type="checkbox" name="chbAsuransi" id="chbAsuransi">
							<label for="chbAsuransi">Asuransi</label></td>
						  <td><input type="checkbox" name="chbKeringanan" id="chbKeringanan">
							Keringanan
							<label for="chbKeringanan"></label></td>
						  <td><input type="checkbox" name="chbGratis" id="chbGratis">
							<label for="chbGratis">Gratis</label></td>
						</tr>
						<tr>
						  <td>Cara Masuk dikirim oleh</td>
						  <td>:</td>
						  <td><input type="checkbox" name="chbDatangSendiri" id="chbDatangSendiri">
							Datang Sendiri
							<label for="chbDatangSendiri"></label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbRsLain" id="chbRsLain">
							RS. Lain
							<label for="chbRsLain"></label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbDokter" id="chbDokter">
							<label for="chbDokter">Dokter</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbKasusPoli" id="chbKasusPoli">
							<label for="chbKasusPoli">Kasus Poli</label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbPuskesmas" id="chbPuskesmas">
							<label for="chbPuskesmas">Puskesmas</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbLainLain" id="chbLainLain">
							<label for="chbLainLain">Lain-lain</label></td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr>
					  <td colspan="2"><table width="100%" border="1">
						<tr>
						  <td width="17%"  style="border-style:none">Riwayat Penyakit dahulu</td>
						  <td width="2%"  style="border-style:none">:</td>
						  <td width="81%"  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit sekarang</td>
						  <td  style="border-style:none">:</td>
						  <td style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit keluarga</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Alergi terhadap</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
					  </table></td>
					</tr>
				  </table>
				  <table width="100%" border="1">
					<tr>
					  <td width="10%" align="center"><strong>Tgl / Jam</strong></td>
					  <td width="37%" align="center"><strong>Anamnesa &amp; Pemeriksaan</strong></td>
					  <td width="25%" align="center"><strong>Diagnose &amp; Terapi</strong></td>
					  <td width="11%" align="center"><strong>Kode ICD</strong></td>
					  <td width="17%" align="center"><strong>Nama Dokter &amp; Paraf</strong></td>
					</tr>
					<tr>
					  <td height="124">&nbsp;</td>
					  <td><p>Anamnesa :</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>Pemeriksaan :</p>
						<p>&nbsp;</p></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				  </table>';
		}
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','IRJ UMUM',$html);	
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
   	}
	public function cetakstatuspasienanak()
	{
		
   		$common=$this->common;
   		$result=$this->result;
   		$title=' ';
		$param=json_decode($_POST['data']);
		
		/* $kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		} */
		
		/* $queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result(); */
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$kdpasien=$param->nomedrec;
		$html.='
			<table width="100%" border="0">
				<tr>
				  <td align="right">&nbsp;RM. RJ. 01. 1b</td>
				</tr>
				<tr>
				  <td align="center"><h1><strong>IRJ ANAK</strong></h1></td>
				</tr>
			  </table>
			  <p></p>';
			
		
		$queryPasien=$this->db->query("select kd_pasien,telepon, nama, to_char(tgl_lahir, 'DD-MM-YYYY' ) as tgl_lahir , case when date_part('year',age(Tgl_Lahir))<=5 then
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn '||
		 to_char(date_part('month',age(Tgl_Lahir)), '999')||' bln ' ||
		 to_char(date_part('days',age(Tgl_Lahir)), '999')||' hari'
		 else
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn'
		end
		  as umur, kd_pendidikan,case when jenis_kelamin=  true then 'L' else 'P' end as jk , 
		  ag.agama , alamat , nama_ibu, nama_ayah,  pekerjaan 
		  from pasien 
		  inner join agama ag on pasien.kd_agama= ag.kd_agama 
		  inner join pekerjaan pek on pasien.kd_pekerjaan = pek.kd_pekerjaan
		  where kd_pasien='$kdpasien' ");
		$data_pasien=$queryPasien->row();
		$belumsekolah='';
		$sd='';
		$smp='';
		$smk='';
		$d3='';
		$s1='';
		$s3='';
		$prof='';
		$jkl='';
		$jkp='';
		if ($data_pasien->kd_pendidikan == 0)
		{
			$belumsekolah=' checked="checked"';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}
		else if ($data_pasien->kd_pendidikan == 2)
		{
			$belumsekolah='';
			$sd=' checked="checked"';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 3 || $data_pasien->kd_pendidikan == 1)
		{
			$belumsekolah='';
			$sd='';
			$smp=' checked="checked"';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 5)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3=' checked="checked"';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 6)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1=' checked="checked"';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 7 || $data_pasien->kd_pendidikan == 4)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma=' checked="checked"';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 8)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3=' checked="checked"';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 9)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof=' checked="checked"';
		}else if ($data_pasien->kd_pendidikan == 10)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk=' checked="checked"';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}
		
		if ($data_pasien->jk == 'L')
		{
			$jkl=' checked="checked"';
			$jkp='';
		}
		else
		{
			$jkl='';
			$jkp=' checked="checked"';
		}
		if (count($queryPasien->result())==0)
		{
			$html.='<table width="100%" border="0">
					<tr>
					  <td width="54%" align="center"><h1>DATA TIDAK ADA</h1>
					  </td>
					</tr>
					</table>';
		}
		else
		{
				$html.='<table width="100%" border="1">
				<tr>
				  <td width="54%"><center>&nbsp; <strong>REKAM MEDIS PASIEN RAWAT JALAN</strong>
				  </center></td>
				  <td width="46%"><center>&nbsp; <strong>NO. RM : '.$data_pasien->kd_pasien.'</strong> </center></td>
				</tr>
				<tr>
				  <td height="27" colspan="2"><table width="100%" border="1">
					<tr>
					  <td width="17%" style="border-style:none">Nama</td>
					  <td width="2%" style="border-style:none">:</td>
					  <td colspan="2" style="border-style:none">'.$data_pasien->nama.'</td>
					  <td width="16%" style="border-style:none"><input type="checkbox" name="chbLaki" id="chbLaki" '.$jkl.'>
					  <label for="chbLaki">Lk </label></td>
					  <td width="18%" style="border-style:none"><input type="checkbox" name="chbPerempuan" id="chbPerempuan" '.$jkp.'>
					  <label for="chbPerempuan">Pr</label></td>
					</tr>
					<tr>
					  <td style="border-style:none">Tgl. lahir / Umur</td>
					  <td style="border-style:none">:</td>
					  <td colspan="2" style="border-style:none">'.$data_pasien->tgl_lahir.' / '.$data_pasien->umur.'</td>
					  <td style="border-style:none">Agama </td>
					  <td style="border-style:none">: '.$data_pasien->agama.'</td>
					</tr>
					<tr>
					  <td style="border-style:none">Pendidikan</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="4"><input type="checkbox" name="chbBelumSekolah" id="chbBelumSekolah" '.$belumsekolah.'>
					  <label for="chbBelumSekolah">Belum Sekolah 
						<input type="checkbox" name="chbTk" id="chbTk" >
					  TK </label>
					  <input type="checkbox" name="chbSd" id="chbSd"  '.$sd.'>
					  SD
					  <label for="chbSd"> </label>
					  <input type="checkbox" name="chbSltp" id="chbSltp"  '.$smp.'>
					  SLTP
					  <label for="chbSltp"> </label>
					  <input type="checkbox" name="chbSlta" id="chbSlta"  '.$sma.'  '.$smk.'>
					  SLTA
					  <label for="chbSlta"> </label>
					  <input type="checkbox" name="chbAkademi" id="chbAkademi"  '.$s1.'  '.$d3.'  '.$s3.' '.$prof.'>
					  <label for="chbAkademi">PT / Akademi </label></td>
					</tr>
					<tr>
					  <td style="border-style:none">Alamat</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="2" rowspan="2">'.$data_pasien->alamat.'</td>
					  <td style="border-style:none">No. PHB / Asuransi I </td>
					  <td style="border-style:none">:</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">Telp</td>
					  <td style="border-style:none">: '.$data_pasien->telepon.'</td>
					</tr>
					<tr>
					  <td style="border-style:none">Nama Orang Tua</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="2">'.$data_pasien->nama_ayah.' / '.$data_pasien->nama_ibu.'</td>
					  <td style="border-style:none">Pekerjaan</td>
					  <td style="border-style:none">: '.$data_pasien->pekerjaan.'</td>
					</tr>
					<tr>
					  <td style="border-style:none">Alamat</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="2" rowspan="2">'.$data_pasien->alamat.'</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">Cara Pembayaran</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" width="24%"><input type="checkbox" name="chbBayarSendiri" id="chbBayarSendiri">
					  <label for="chbBayarSendiri">Bayar Sendiri</label></td>
					  <td style="border-style:none" width="23%"><input type="checkbox" name="chbAsuransi" id="chbAsuransi">
					  <label for="chbAsuransi">Asuransi</label></td>
					  <td style="border-style:none"><input type="checkbox" name="chbKeringanan" id="chbKeringanan">
					  Keringanan
						<label for="chbKeringanan"></label></td>
					  <td style="border-style:none"><input type="checkbox" name="chbGratis" id="chbGratis">
					  <label for="chbGratis">Gratis</label></td>
					</tr>
					<tr>
					  <td style="border-style:none">Cara Masuk dikirim oleh</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none"><input type="checkbox" name="chbDatangSendiri" id="chbDatangSendiri">
					  Datang Sendiri
						<label for="chbDatangSendiri"></label></td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbRsLain" id="chbRsLain">
					  RS. Lain
						<label for="chbRsLain"></label></td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbDokter" id="chbDokter">
					  <label for="chbDokter">Dokter</label></td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbKasusPoli" id="chbKasusPoli">
					  <label for="chbKasusPoli">Kasus Poli</label></td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbPuskesmas" id="chbPuskesmas">
					  <label for="chbPuskesmas">Puskesmas</label></td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbLainLain" id="chbLainLain">
					  <label for="chbLainLain">Lain-lain</label></td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
				  </table></td>
				</tr>
				<tr>
				  <td colspan="2"><table width="100%" border="1">
					<tr>
					  <td width="17%" style="border-style:none">Riwayat Penyakit dahulu</td>
					  <td width="2%" style="border-style:none">:</td>
					  <td width="81%" style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
					<tr>
					  <td style="border-style:none">Riwayat Penyakit sekarang</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
					<tr>
					  <td style="border-style:none">Riwayat Penyakit keluarga</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
					<tr>
					  <td style="border-style:none">Alergi terhadap</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
				  </table></td>
				</tr>
				<tr>
				  <td colspan="2"><table width="100%" border="1">
					<tr>
					  <td width="54%" style="border-style:none"><strong>STATUS VAKSINASI</strong></td>
					  <td width="46%" style="border-style:none"><strong>STATUS GIZI</strong></td>
					</tr>
					<tr>
					  <td style="border-style:none"><table width="100%" border="1">
						<tr>
						  <td width="19%" rowspan="2" align="center">Jenis Imunisasi</td>
						  <td colspan="3" align="center">Dasar</td>
						  <td colspan="3" align="center">Booster</td>
						</tr>
						<tr>
						  <td width="6%" align="center">I</td>
						  <td width="6%" align="center">II</td>
						  <td width="6%" align="center">III</td>
						  <td width="6%" align="center">1 th</td>
						  <td width="6%" align="center">6 th</td>
						  <td width="6%" align="center">12 th</td>
						</tr>
						<tr>
						  <td>BGC</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>DPT</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Polio</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>DT</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Campak</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Lain : ...............</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ...............</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ...............</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					  <td style="border-style:none"><table width="100%" border="0">
						<tr>
						  <td width="33%">- Lingkaran lengan</td>
						  <td width="3%">:</td>
						  <td width="64%">.........................................</td>
						</tr>
						<tr>
						  <td>- Berat badan</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>- Harvard standar</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td colspan="3">Pembelian makanan / minuman :</td>
						</tr>
						<tr>
						  <td>- ASI diberikan sejak</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td align="right">Sampai &nbsp;&nbsp;</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>- PASI diberikan sejak</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td align="right">Sampai &nbsp;&nbsp;</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>- Lain-lain</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
					  </table></td>
					</tr>
				  </table></td>
				</tr>
			  </table>
			  <table width="100%" border="1">
				<tr>
				  <td width="10%" align="center"><strong>Tgl / Jam</strong></td>
				  <td width="37%" align="center"><strong>Anamnesa &amp; Pemeriksaan</strong></td>
				  <td width="25%" align="center"><strong>Diagnose &amp; Terapi</strong></td>
				  <td width="11%" align="center"><strong>Kode ICD</strong></td>
				  <td width="17%" align="center"><strong>Nama Dokter &amp; Paraf</strong></td>
				</tr>
				<tr>
				  <td height="124">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table>';
		}
		$prop=array('foot'=>true);
		$this->common->setPdf('P','IRJ ANAK',$html);	
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
   	}
	public function cetakstatuspasienmata()
	{
		
   		$common=$this->common;
   		$result=$this->result;
   		$title=' ';
		$param=json_decode($_POST['data']);
		
		/* $kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		} */
		
		/* $queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result(); */
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$kdpasien=$param->nomedrec;
		$html.='
			<table width="100%" border="0">
				<tr>
				  <td align="right">&nbsp;RM. RJ. 01. 1d</td>
				</tr>
				<tr>
				  <td align="center"><h1><strong>IRJ MATA</strong></h1></td>
				</tr>
			  </table>
			  <p></p>';
			
		
		$queryPasien=$this->db->query("select kd_pasien,telepon, nama, to_char(tgl_lahir, 'DD-MM-YYYY' ) as tgl_lahir , case when date_part('year',age(Tgl_Lahir))<=5 then
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn '||
		 to_char(date_part('month',age(Tgl_Lahir)), '999')||' bln ' ||
		 to_char(date_part('days',age(Tgl_Lahir)), '999')||' hari'
		 else
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn'
		end
		  as umur, kd_pendidikan,case when jenis_kelamin=  true then 'L' else 'P' end as jk , 
		  ag.agama , alamat , nama_ibu, nama_ayah,  pekerjaan 
		  from pasien 
		  inner join agama ag on pasien.kd_agama= ag.kd_agama 
		  inner join pekerjaan pek on pasien.kd_pekerjaan = pek.kd_pekerjaan
		  where kd_pasien='$kdpasien' ");
		$data_pasien=$queryPasien->row();
		$belumsekolah='';
		$sd='';
		$smp='';
		$smk='';
		$d3='';
		$s1='';
		$s3='';
		$prof='';
		$jkl='';
		$jkp='';
		if ($data_pasien->kd_pendidikan == 0)
		{
			$belumsekolah=' checked="checked"';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}
		else if ($data_pasien->kd_pendidikan == 2)
		{
			$belumsekolah='';
			$sd=' checked="checked"';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 3 || $data_pasien->kd_pendidikan == 1)
		{
			$belumsekolah='';
			$sd='';
			$smp=' checked="checked"';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 5)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3=' checked="checked"';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 6)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1=' checked="checked"';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 7 || $data_pasien->kd_pendidikan == 4)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma=' checked="checked"';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 8)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3=' checked="checked"';
			$prof='';
		}else if ($data_pasien->kd_pendidikan == 9)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk='';
			$d3='';
			$s1='';
			$s3='';
			$prof=' checked="checked"';
		}else if ($data_pasien->kd_pendidikan == 10)
		{
			$belumsekolah='';
			$sd='';
			$smp='';
			$sma='';
			$smk=' checked="checked"';
			$d3='';
			$s1='';
			$s3='';
			$prof='';
		}
		
		if ($data_pasien->jk == 'L')
		{
			$jkl=' checked="checked"';
			$jkp='';
		}
		else
		{
			$jkl='';
			$jkp=' checked="checked"';
		}
		if (count($queryPasien->result())==0)
		{
			$html.='<table width="100%" border="0">
					<tr>
					  <td width="54%" align="center"><h1>DATA TIDAK ADA</h1>
					  </td>
					</tr>
					</table>';
		}
		else
		{
			$html.=' 
				  <table width="100%" border="1">
					<tr>
					  <td width="54%"><center>
						&nbsp; <strong>REKAM MEDIS PASIEN RAWAT JALAN</strong>
					  </center></td>
					  <td width="46%"><center>
						&nbsp; <strong>NO. RM : '.$data_pasien->kd_pasien.'</strong>
					  </center></td>
					</tr>
					<tr>
					  <td height="27" colspan="2"><table width="100%" border="0">
						<tr>
						  <td width="17%">Nama</td>
						  <td width="2%">:</td>
						  <td colspan="2">'.$data_pasien->nama.'</td>
						  <td width="16%"><input type="checkbox" name="chbLaki" id="chbLaki" '.$jkl.'>
							<label for="chbLaki">Lk </label></td>
						  <td width="18%"><input type="checkbox" name="chbPerempuan" id="chbPerempuan" '.$jkp.'>
							<label for="chbPerempuan">Pr</label></td>
						</tr>
						<tr>
						  <td>Tgl. lahir / Umur</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->tgl_lahir.' / '.$data_pasien->umur.'</td>
						  <td>Agama </td>
						  <td>: '.$data_pasien->agama.'</td>
						</tr>
						<tr>
						  <td>Pendidikan</td>
						  <td>:</td>
						  <td colspan="4"><input type="checkbox" name="chbBelumSekolah" id="chbBelumSekolah" '.$belumsekolah.'>
							<label for="chbBelumSekolah">Belum Sekolah
							  <input type="checkbox" name="chbTk" id="chbTk">
							  TK </label>
							<input type="checkbox" name="chbSd" id="chbSd" '.$sd.'>
							SD
							<label for="chbSd"> </label>
							<input type="checkbox" name="chbSltp" id="chbSltp" '.$smp.'>
							SLTP
							<label for="chbSltp"> </label>
							<input type="checkbox" name="chbSlta" id="chbSlta" '.$sma.'  '.$smk.'>
							SLTA
							<label for="chbSlta"> </label>
							<input type="checkbox" checked name="chbAkademi" id="chbAkademi"  '.$s1.'  '.$d3.'  '.$s3.' '.$prof.' >
							<label for="chbAkademi">PT / Akademi </label></td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->alamat.'</td>
						  <td>No. PHB / Asuransi I </td>
						  <td>:</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>Telp</td>
						  <td>: '.$data_pasien->telepon.'</td>
						</tr>
						<tr>
						  <td>Nama Orang Tua</td>
						  <td>:</td>
						  <td colspan="2">'.$data_pasien->nama_ayah.' / '.$data_pasien->nama_ibu.'</td>
						  <td>Pekerjaan</td>
						  <td>: '.$data_pasien->pekerjaan.'</td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2" rowspan="2"> '.$data_pasien->alamat.'</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Cara Pembayaran</td>
						  <td>:</td>
						  <td width="24%"><input type="checkbox" name="chbBayarSendiri" id="chbBayarSendiri">
							<label for="chbBayarSendiri">Bayar Sendiri</label></td>
						  <td width="23%"><input type="checkbox" name="chbAsuransi" id="chbAsuransi">
							<label for="chbAsuransi">Asuransi</label></td>
						  <td><input type="checkbox" name="chbKeringanan" id="chbKeringanan">
							Keringanan
							<label for="chbKeringanan"></label></td>
						  <td><input type="checkbox" name="chbGratis" id="chbGratis">
							<label for="chbGratis">Gratis</label></td>
						</tr>
						<tr>
						  <td>Cara Masuk dikirim oleh</td>
						  <td>:</td>
						  <td><input type="checkbox" name="chbDatangSendiri" id="chbDatangSendiri">
							Datang Sendiri
							<label for="chbDatangSendiri"></label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbRsLain" id="chbRsLain">
							RS. Lain
							<label for="chbRsLain"></label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbDokter" id="chbDokter">
							<label for="chbDokter">Dokter</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbKasusPoli" id="chbKasusPoli">
							<label for="chbKasusPoli">Kasus Poli</label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbPuskesmas" id="chbPuskesmas">
							<label for="chbPuskesmas">Puskesmas</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbLainLain" id="chbLainLain">
							<label for="chbLainLain">Lain-lain</label></td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr>
					  <td colspan="2"><table width="100%" border="1">
						<tr>
						  <td width="17%"  style="border-style:none">Riwayat Penyakit dahulu</td>
						  <td width="2%"  style="border-style:none">:</td>
						  <td width="81%"  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit sekarang</td>
						  <td  style="border-style:none">:</td>
						  <td style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit keluarga</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Alergi terhadap</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
					  </table></td>
					</tr>
					<tr>
      <td colspan="2"><table width="100%" border="1">
        <tr>
          <td width="44%" height="132"><table width="100%" border="0">
            <tr>
              <td width="6%">O.D.</td>
              <td width="4%">:</td>
              <td width="86%">________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
          </table></td>
          <td width="15%">&nbsp;&nbsp;&nbsp;Visus<br>
            &nbsp;&nbsp;&nbsp;Refacto<br>
            &nbsp;&nbsp;&nbsp;Tensio oculi<br>
            &nbsp;&nbsp;&nbsp;Perseptio Coloris<br>
            &nbsp;&nbsp; Projectio illuminis<br>
            &nbsp;&nbsp;&nbsp;Kacamata lama </td>
          <td width="41%"><table width="100%" border="1">
            <tr>
              <td width="10%"  style="border-style:none">O.S.</td>
              <td width="4%"  style="border-style:none">:</td>
              <td width="86%"  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td height="97" colspan="2"><table width="100%" border="1" height="100%">
        <tr>
          <td width="20%" rowspan="2"  style="border-style:none">O.D.</td>
          <td height="26" align="center"  style="border-style:none">DIAGNOSIS ANATOMICA</td>
          <td width="18%" rowspan="2" align="right"  style="border-style:none">O.S.</td>
        </tr>
        <tr>
          <td width="62%" align="center"  style="border-style:none"><img src="./ui/images/Logo/Mata.png" width="344" height="85"  alt=""/></td>
          </tr>
      </table></td>
    </tr>
  </table>
				  <table width="100%" border="1">
					<tr>
					  <td width="10%" align="center"><strong>Tgl / Jam</strong></td>
					  <td width="37%" align="center"><strong>Anamnesa &amp; Pemeriksaan</strong></td>
					  <td width="25%" align="center"><strong>Diagnose &amp; Terapi</strong></td>
					  <td width="11%" align="center"><strong>Kode ICD</strong></td>
					  <td width="17%" align="center"><strong>Nama Dokter &amp; Paraf</strong></td>
					</tr>
					<tr>
					  <td height="124">&nbsp;</td>
					  <td><p>Anamnesa :</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>Pemeriksaan :</p>
						<p>&nbsp;</p></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				  </table>';
		}
		$prop=array('foot'=>true);
		$this->common->setPdf('P','IRJ MATA',$html);	
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
   	}
	private function getSignature(){
		$secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$data= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();//strval(time()-strtotime('1970-01-01 00:00:00'));
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		$encodedSignature = base64_encode($signature);
		$headers="X-cons-id: ".$data."\r\n" .
				  "Accept: JSON\r\n".
				  "Content-Type: application/json\r\n".
				  "X-timestamp: ".$tStamp."\r\n".
				  "X-signature: ".$encodedSignature."\r\n";
		return $headers;
	}
	public function cetaksep(){
		$this->load->library('m_pdf');
		$uri = $_SERVER["REQUEST_URI"];
		$uriArray = explode('/', $uri);
		$param1 = $uriArray[(count($uriArray)-2)];
		$catatantambahan = str_replace('~~_~~','/',$uriArray[(count($uriArray)-1)]);
		$catatantambahanasli = str_replace('%20',' ',$catatantambahan);
	 	$html='';
		$style='<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
				} 
				
					
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
				@media print{
					.no-print, .no-print *{
						display: none !important;
					}
				}
           </style>';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		//echo $kd_rs;
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		//<script>
			//	window.print();
		//	</script>
		$html.="
		<html>
		<head>
			".$style."
			
		</head>
		<body style='padding-top:100px;'>
		<table style='margin: 50px 50px 0px 50px;' cellspacing='0' border='0'>
   			<tr align=justify>
   				<th border='0';  width='10%' align='center'>
   					<img src='".base_url()."ui/images/Logo/RSSM.png' width='50' height='35'/>
   				</th>
   				<th align='center'  >
					<font style='font-size: 12px;font-family: Calibri;line-height:90%;letter-spacing: 2px;'><b>SURAT ELEGIBILITAS PESERTA</b></font><br>
					<font style='font-size: 8px;font-family: Calibri;line-height:90%;letter-spacing: 2px;'>".strtoupper($rs->name)."</font><br>
   				</th>
				<th border='0' width='30%' align='center'>
   					<img src='".base_url()."ui/images/Logo/bpjs.jpg' width='170' height='20'/>
   				</th>
   			</tr>
   		</table>
		<br/>";
		//<img src='".base_url()."ui/images/Logo/RSSM.png' width='60' height='40'/>
		//<input type='button' id='btnPrint' value='Print' class='no-print' style='width:100px' onclick='window.print()' />
		$kd_user=$this->session->userdata['user_id']['id'];
		$sqlUser="SELECT user_names FROM zusers WHERE kd_user='".$kd_user."'";
		$objUser=$this->db->query($sqlUser)->row();
		$operator='-';
		if($objUser){
			$operator=$objUser->user_names;
		}
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$this->getSignature()
		  )
		);
		$kelasRawat='';
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$param1,false,$context),false);
		if($res->metadata->code=='200'){
			if($res->response->peserta->sex == 'P'){
				$jk='WANITA';
			} else{
				$jk='PRIA';
			} 
			if($res->response->jnsPelayanan=='Inap'){
				$kelasRawat=$res->response->klsRawat->nmKelas;
			}
			$nama=$res->response->peserta->nama;
			$peserta=$res->response->peserta->jenisPeserta->nmJenisPeserta;
			$tglsep=tanggalstring($res->response->tglSep);
			$html.='
				
				<table background="white" cellspacing="0" border="0" style="font-color:#000000;font-size: 9px;font-family:  Calibri; margin: 0px 10px;line-height:90%;letter-spacing: 1px;font-stretch: condensed;">
				<tr style="height: 5px;">
					<td width="100">No. SEP</td>
					<td width="6">:</td>
					<td width="300" style="font-size:12px;"><b>'.$param1.'</b></td>
					<td width="26">&nbsp;</td>
					<td width="100">No. Mr</td>
					<td width="8">:</td>
					<td width="100">'.$res->response->peserta->noMr.'</td>
				</tr>
				<tr>
					<td width="100">Tgl. SEP</td>
					<td width="6">:</td>
					<td>'.$tglsep.'</td>
					<td width="26"></td>
					<td></td>
					<td width="8"></td>
					<td></td>
				</tr>
				<tr>
					<td width="100">No. Kartu</td>
					<td width="6">:</td>
					<td style="font-size:12px;"><b>'.$res->response->peserta->noKartu.'</b></td>
					<td width="26">&nbsp;</td>
					<td>Peserta</td>
					<td width="8">:</td>
					<td rowspan="2" valign="top">'.$peserta.'</td>
				</tr>
				<tr>
					<td width="100">Nama Peserta</td>
					<td width="6">:</td>
					<td>'.$nama.'</td>
				</tr>
				<tr>
					<td width="100">Tgl. Lahir</td>
					<td width="6">:</td>
					<td>'.tanggalstring($res->response->peserta->tglLahir).'</td>
					<td width="26"></td>
					<td>COB</td>
					<td width="8">:</td>
					<td></td>
				</tr>
				<tr>
					<td width="100">Jns Kelamin</td>
					<td width="6">:</td>
					<td>'.$jk.'</td>
					<td width="26">&nbsp;</td>
					<td>Jns. Rawat</td>
					<td width="8">:</td>
					<td>'.$res->response->jnsPelayanan.'</td>
					
				</tr>
				<tr>
					<td width="100">Poli Tujuan</td>
					<td width="6">:</td>
					<td>'.$res->response->poliTujuan->nmPoli.'</td>
					<td width="26">&nbsp;</td>
					<td>Kls. Rawat</td>
					<td width="8">:</td>
					<td>'.$kelasRawat.'</td>
				</tr>
				<tr>
					<td width="100">Asal Faskes Tk. 1</td>
					<td width="6">:</td>
					<td>'.$res->response->peserta->provUmum->nmProvider.'</td>
					<td width="26">&nbsp;</td>
					<td>Operator</td>
					<td width="8">:</td>
					<td>'.$operator.'</td>
					
				</tr>
				<tr>
					<td width="100">Diagnosa Awal</td>
					<td width="6">:</td>
					<td>'.$res->response->diagAwal->nmDiag.'</td>
					<td width="26">&nbsp;</td>
					<td></td>
					<td width="8"></td>
					<td></td>
					
				</tr>
				<tr>
					<td width="100">Catatan</td>
					<td width="6">:</td>
					<td>'.$catatantambahanasli.'</td>
					<td width="26">&nbsp;</td>
					<td align="center">Pasien / Keluarga</td>
					<td width="8"></td>
					<td align="center">Petugas BPJS</td>
					
				</tr>
				<tr>
					<td width="100"></td>
					<td width="6"></td>
					<td></td>
					<td width="26">&nbsp;</td>
					<td align="center">Pasien</td>
					<td width="8"></td>
					<td align="center">Kesehatan</td>
				</tr>
				 <tr>
					<td colspan="3";><font style="font-weight:bold;font-size: 7px;font-family: Arial;"><i>*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan</i></td>
					<td width="26"></td>
					<td>&nbsp;</td>
					<td width="8"></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
				   <td colspan="3";><font style="font-weight:bold;font-size: 7px;font-family:Arial;"><i>*SEP bukan sebagai bukti penjamin peserta</td>
				   <td></td>
				  <td>&nbsp;</td>
				   <td></td>
				   <td>&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3";>&nbsp;</td>
				  <td></td>
				  <td>&nbsp;</td>
				  <td></td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3";>Cetakan Ke 1 : '.gmdate("d-M-Y H:i:s", time()+60*61*7).'</td>
					<td width="26"></td>
					<td  align="left" width="100"><hr width="100"></td>
					<td width="8"></td>
					<td><hr width="100"></td>
				</tr> 
				</table>
			</body>
			</html>
			'; 
			//echo $html;
			$this->load->library('m_pdf');
			$this->m_pdf->load();
			$mpdf=new mPDF('utf-8', array(200,60));
			$mpdf->AddPage('P', // L - landscape, P - portrait
				'', '', '', '',
				0, // margin_left
				0, // margin right
				0, // margin top
				0, // margin bottom
				0, // margin header
				0); // margin footer
			//$mpdf->SetDisplayMode('fullpage');
			//$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			//$mpdf->pagenumPrefix = 'Hal : ';
			//$mpdf->pagenumSuffix = '';
			//$mpdf->nbpgPrefix = ' Dari ';
			//$mpdf->nbpgSuffix = '';
			//date_default_timezone_set("Asia/Jakarta"); 
			//$date = gmdate("d-M-Y / H:i", time()+60*60*7);
			//$date = date("d-M-Y / H:i");
			
			//if($this->foot==true){
			//	$mpdf->SetFooter($arr);
			//}
			//$mpdf->SetTitle($title);
			//echo $html; lalalala
			$mpdf->WriteHTML($html);
			//$mpdf->SetJS('this.print();');
			$mpdf->Output($pdfFilePath, "I");
			//$cmd = 'lpr -P Epson-LX-300_NUANSA /var/www/html/medismart/ui/template/headerSEP.pdf';
			// $cmd .=$file;
			//$response = shell_exec($cmd);
			header ( 'Content-type: application/pdf' );
			header ( 'Content-Disposition: attachment; filename="SEP.pdf"' );
			readfile ( 'SEP.pdf' );	 
			//$mpdf->WriteHTML($html);
			//$mpdf->SetJS('this.print();');
			//header ( 'Content-type: application/pdf' );
			//header ( 'Content-Disposition: attachment; filename="SEP.pdf"' );
			//readfile ( 'SEP.pdf' );	
			$kdUser=$this->session->userdata['user_id']['id'];
			$printer = $this->db->query("select p_sep from zusers where kd_user = '$kdUser'")->row()->p_sep;
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
			//$header =  'http://192.168.1.25/medismart_/ui/images/Logo/header-sep.prn';
			$header =  'http://192.168.1.25/medismart/ui/images/Logo/header-sep.prn';
			$handle = fopen($file, 'w');
			$condensed = Chr(27) . Chr(33) . Chr(4);
			$bold1 = Chr(27) . Chr(69);
			$bold0 = Chr(27) . Chr(70);
			$initialized = chr(27).chr(64);
			$condensed2 = Chr(27).Chr(33).Chr(30);
			$condensed1 = chr(15);
			$condensed0 = chr(18);
			$Data  = $initialized;
			$Data .= $condensed1;
			$Data .= "No. Sep             : ".str_pad($condensed2.$bold1.$param1.$bold0.$condensed,65," ")."\n";
			$Data .= "Tgl. Sep            : ".str_pad($bold1.$tglsep.$bold0,69," ")."No. Mr         : ".$res->response->peserta->noMr."\n";
			$Data .= "No. Kartu           : ".str_pad($condensed2.$bold1.$res->response->peserta->noKartu.$bold0.$condensed,67," ")."Peserta        : ".$peserta."\n";
			$Data .= "Nama Peserta        : ".str_pad($nama,65," ")."\n";
			$Data .= "Tgl. Lahir          : ".str_pad(tanggalstring($res->response->peserta->tglLahir),65," ")."COB            : -\n";
			$Data .= "Jns. Kelamin        : ".str_pad($jk,65," ")."Jns. Rawat     : ".$res->response->jnsPelayanan."\n";
			$Data .= "Poli Tujuan         : ".str_pad($res->response->poliTujuan->nmPoli,65," ")."Kls. Rawat     : ".$kelasRawat."\n";
			$Data .= "Asal Faskes TK 1    : ".str_pad($res->response->peserta->provUmum->nmProvider,65," ")."Operator       : ".$operator."\n";
			$Data .= "Diagnosa Awal       : ".str_pad($res->response->diagAwal->nmDiag,65," ")."\n";
			$Data .= "Catatan             : ".str_pad($catatantambahanasli,65," ")."Pasien / Keluarga      Petugas BPJS \n";
			$Data .= "                                                                                             Pasien            Kesehatan\n";
			$Data .= "*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan\n";
			$Data .= "*SEP bukan sebagai bukti penjamin peserta\n";
			$Data .= "\n";
			$Data .= "Cetakan Ke 1 : ".str_pad(gmdate("d-M-Y H:i:s", time()+60*61*7),71," ")."................     .................\n";
			$Data .= "\n";
			fwrite($handle, $Data);
			fclose($handle);
			//$cmd = 'lpr -P //localhost/epson-lx-310-me '.$file;
			//shell_exec("lpr -P ".$printer." -r ".$header);
			shell_exec("lpr -P ".$printer." /var/www/html/medismart_/ui/images/Logo/header-sep.prn");
			shell_exec("lpr -P ".$printer." -r ".$file);
		}
		
	}
	function cetakRWJBatalTransaksi(){
	$var = $this->input->post('data');
	$param=explode('#aje#',$var);
		$d_medrec=$this->uri->segment(4,0);
	 	$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', 'A4');
		$mpdf->AddPage($type, // L - landscape, P - portrait
				'', '', '', '',
				$marginLeft, // margin_left
				15, // margin right
				15, // margin top
				15, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d/M/Y H:i:s", time()+60*61*7);
		$arr = array (
				'odd' => array (
						'L' => array (
								'content' => 'Operator : '.$name,
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'C' => array (
								'content' => "Tgl/Jam : ".$date."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'R' => array (
								'content' => '{PAGENO}{nbpg}',
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'line' => 0,
				),
				'even' => array ()
		);
		for($x=0; $x<35; $x++)
						 {
						 $SPASI.='&nbsp;';
						 $underline.='_';
						 }
	 	$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:1px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 12px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$jkx="";
		$gdarah="";
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<thead><tr align=justify>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='70' height='50'/>
   				</th>
   				<th align=justify >
   					<b>".$rs->name."</b><br>
			   		<font style='font-size: 11px;font-family: Arial'><b>".$rs->address.", ".$rs->city."</b></font>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>".$telp."</b></font><br>
   				</th>
   			</tr></thead>
   		</table>";
	 	$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><thead><tr border='0'><th > ".$param[2]."</center></th></tr></thead></table>";
$html.='<table>
				<thead>
	 				<tr>
	 					<td width="100" align= "center"><b>Medrec</b></td>
						<td width="250" align= "center"><b>Nama Pasien</b></td>
						<td width="100" align= "center"><b>No transaksi</b></td>
						<td width="100" align= "center"><b>Unit</b></td>
						<td width="60" align= "center"><b>Tanggal batal</b></td>
						<td width="50" align= "center"><b>Shift batal</b></td>
						<td width="50" align= "center"><b>Jam batal</b></td>
						<td width="100" align= "center"><b>Alasan Batal</b></td>
						<td width="100" align= "right"><b>Jumlah</b></td>
	 					<td width="100" align= "center"><b>Petugas </b></td>
	 				</tr>"
				</thead>
					
	 				
';
/*$query=$this->db->query("select 
  kd_kasir,
  no_transaksi ,
  tgl_transaksi ,
  kd_pasien ,
  nama ,
  kd_unit,
  nama_unit,
  kd_user ,
  kd_user_del ,
  shift,
  shiftdel ,
  jumlah,
  user_name ,
  tgl_batal ,
  ket,jam_batal  from history_batal_trans  where kd_kasir='".$param[3]."' and tgl_batal between '".$param[0]."' and  '".$param[1]."'")->result();*/
  $query=$this->db->query("Select 
  Nama as nama, 
  No_Transaksi as no_transaksi,
  Kd_Pasien as kd_pasien, 
  Jumlah as jumlah, 
  Ket as ket, 
  User_name as user_name, 
  Nama_Unit as nama_unit,* FROM History_Trans  
Where kd_kasir='".$param[3]."' And tgl_transaksi BETWEEN '".$param[0]."'  And '".$param[1]."'  
GROUP BY Kd_Kasir, No_Transaksi, Tgl_Transaksi, Kd_Pasien, Nama, Kd_Unit,  Nama_Unit, IsPay, 
Kd_User, Kd_User_Del, User_Name, Jumlah, Tgl_Batal,  Ket, Jam_Batal Order By Nama, No_Transaksi, Tgl_Transaksi, User_name")->result();
  $totall=0;
   foreach ($query as $line) 
		{
		$tanggal=date_create($line->tgl_batal);
		$jam=date_create($line->jam_batal);
		$totall+=$line->jumlah;
          $html.='
	 				<tr>
	 					<td width="100"  align="right">'.$line->kd_pasien .'</td>
						<td width="250" >'.$line->nama .'</td>
						<td width="100" align="right">'.$line->no_transaksi .'</td>
						<td width="150" >'.$line->nama_unit .'</td>
						
						<td width="90" >'.date_format($tanggal,"Y/m/d") .'</td>
						<td width="50" align="right">'.$line->shiftdel .'</td>
						<td width="50" align="right">'.date_format($jam,"H:i") .'</td>
						<td width="180" >'.$line->ket .'</td>
						<td width="180"  align= "right">'.number_format($line->jumlah,0,".",",") .'</td>
	 					<td width="100" >'.$line->user_name .' </td>
	 				</tr>"
	 				
				';
				
				
      }
		 $html.='
	 				<tr>
						<td width="180" align= "right" colspan="8"><strong>Grand Total</strong></td>
						<td width="180"  align= "right"><strong>'.number_format($totall,0,".",",").'</strong></td>
	 					<td width="100" >&nbsp;</td>
	 				</tr>"
	 				
				';
		 $html.="</table>";
	$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="lembar keluar masuk.pdf"' );
		readfile ( 'original.pdf' );
	}
	
	function cetakRWJBatalTransaksi_(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$var = $param->criteria;
		$type_file = $param->type_file;
		$param=explode('#aje#',$var);
		$d_medrec=$this->uri->segment(4,0);
	 	
	 	$html='';
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$jkx="";
		$gdarah="";
		
	 	$html.='<table class="t2">
					<thead>
						<tr border="0">
							<th colspan="10"> '.$param[2].'</center></th>
						</tr>
						<tr border="0">
							<th colspan="10"> Periode '.$param[0].' s/d '.$param[1].'</center></th>
						</tr>
					</thead>
				</table>';
		$html.='<br>';
		$html.='<table class="t1" border="1">
				<thead>
	 				<tr>
	 					<td width="100" align= "center"><b>Medrec</b></td>
						<td width="250" align= "center"><b>Nama Pasien</b></td>
						<td width="100" align= "center"><b>No transaksi</b></td>
						<td width="100" align= "center"><b>Unit</b></td>
						<td width="60" align= "center"><b>Tanggal batal</b></td>
						<td width="50" align= "center"><b>Shift batal</b></td>
						<td width="50" align= "center"><b>Jam batal</b></td>
						<td width="100" align= "center"><b>Alasan Batal</b></td>
						<td width="100" align= "right"><b>Jumlah</b></td>
	 					<td width="100" align= "center"><b>Petugas </b></td>
	 				</tr>
				</thead>';
/*$query=$this->db->query("select 
  kd_kasir,
  no_transaksi ,
  tgl_transaksi ,
  kd_pasien ,
  nama ,
  kd_unit,
  nama_unit,
  kd_user ,
  kd_user_del ,
  shift,
  shiftdel ,
  jumlah,
  user_name ,
  tgl_batal ,
  ket,jam_batal  from history_batal_trans  where kd_kasir='".$param[3]."' and tgl_batal between '".$param[0]."' and  '".$param[1]."'")->result();*/
  $query=$this->db->query("Select 
  Nama as nama, 
  No_Transaksi as no_transaksi,
  Kd_Pasien as kd_pasien, 
  Jumlah as jumlah, 
  Ket as ket, 
  User_name as user_name, 
  Nama_Unit as nama_unit,
  Tgl_Batal as tgl_batal,
  Jam_Batal as jam_batal,* FROM History_Trans  
Where kd_kasir='".$param[3]."' And tgl_transaksi BETWEEN '".$param[0]."'  And '".$param[1]."'  
GROUP BY Kd_Kasir, No_Transaksi, Tgl_Transaksi, Kd_Pasien, Nama, Kd_Unit,  Nama_Unit, IsPay, 
Kd_User, Kd_User_Del, User_Name, Jumlah, Tgl_Batal,  Ket, Jam_Batal Order By Nama, No_Transaksi, Tgl_Transaksi, User_name")->result();
  $totall=0;
   foreach ($query as $line) 
		{
		$tanggal=date_create($line->tgl_batal);
		$jam=date_create($line->jam_batal);
		$totall+=$line->jumlah;
          $html.='
	 				<tr>
	 					<td width="100"  align="right">'.$line->kd_pasien .'</td>
						<td width="250" >'.$line->nama .'</td>
						<td width="100" align="right">'.$line->no_transaksi .'</td>
						<td width="150" >'.$line->nama_unit .'</td>
						
						<td width="90" >'.date_format($tanggal,"Y/m/d") .'</td>
						<td width="50" align="right"></td>
						<td width="50" align="right">'.date_format($jam,"H:i") .'</td>
						<td width="180" >'.$line->ket .'</td>
						<td width="180"  align= "right">'.number_format($line->jumlah,0,".",",") .'</td>
	 					<td width="100" >'.$line->user_name .' </td>
	 				</tr>
	 				
				';
			/* $html.='
	 				<tr>
	 					<td width="100"  align="right">'.$line->kd_pasien .'</td>
						<td width="250" >'.$line->nama .'</td>
						<td width="100" align="right">'.$line->no_transaksi .'</td>
						<td width="150" >'.$line->nama_unit .'</td>
						
						<td width="90" >'.date_format($tanggal,"Y/m/d") .'</td>
						<td width="50" align="right">'.$line->shiftdel .'</td>
						<td width="50" align="right">'.date_format($jam,"H:i") .'</td>
						<td width="180" >'.$line->ket .'</td>
						<td width="180"  align= "right">'.number_format($line->jumlah,0,".",",") .'</td>
	 					<td width="100" >'.$line->user_name .' </td>
	 				</tr>"
	 				
				';	
				*/
      }
		 $html.='
	 				<tr>
						<td width="180" align= "right" colspan="8"><strong>Grand Total</strong></td>
						<td width="180"  align= "right"><strong>'.number_format($totall,0,".",",").'</strong></td>
	 					<td width="100" >&nbsp;</td>
	 				</tr>
	 				
				';
		 $html.="</table>";
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name=' Laporan Batal Transaksi Rawat Jalan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L',' Laporan Batal Transaksi Rawat Jalan',$html);
		}
		echo $html;
	}
	
	public function getPoliklinik(){
		$result=$this->db->query("SELECT 1 as id,kd_unit,nama_unit FROM unit
									where parent ='2'
									UNION
									Select 0 as id,'000'as kd_unit, 'SEMUA' as nama_unit
									order by id,nama_unit")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetakRWJRentanTunggu(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN RENTAN TUNGGU';
		$param=json_decode($_POST['data']);
		$html="";
		$kd_unit=$param->kd_unit;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$ctgl = "where k.tgl_masuk >= '".date('Y-m-d',strtotime($tglAwal))."' and k.tgl_masuk <= '".date('Y-m-d',strtotime($tglAkhir))."'";
		if($kd_unit == 'SEMUA' || $kd_unit == ''){
			$cKdunit='';
			$unit = 'SEMUA POLIKLINIK';
		} else{
			$cKdunit="and k.kd_unit='".$kd_unit."'";
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
		}
		
		$queryHead = $this->db->query( "select k.kd_pasien,p.nama,k.kd_unit,u.nama_unit, k.jam_masuk, k.jam_berkas_masuk, k.jam_dilayani, k.jam_keluar
										from kunjungan k
											inner join pasien p on p.kd_pasien=k.kd_pasien
											inner join unit u on u.kd_unit=k.kd_unit
											".$ctgl.$cKdunit."
											
										order by p.nama");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th> Poli : '.$unit.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="60" align="center">No. Medrec</th>
					<th width="80" align="center">Nama</th>
					<th width="60" align="center">Unit</th>
					<th width="40" align="center">Jam Daftar</th>
					<th width="40" align="center">Jam Berkas Datang(Scan Berkas)</th>
					<th width="40" align="center">Jam Dilayani</th>
					<th width="40" align="center">Jam Pulang</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
							<tr>
								<th align="left">'.$no.'</th>
								<th align="left">'.$line->kd_pasien.'</th>
								<th align="left">'.$line->nama.'</th>
								<th align="left">'.$line->nama_unit.'</th>
								<th align="center">'.substr($line->jam_masuk, -8).'</th>
								<th align="center">'.substr($line->jam_berkas_masuk, -8).'</th>
								<th align="center">'.substr($line->jam_dilayani, -8).'</th>
								<th align="center">'.substr($line->jam_keluar, -8).'</th>
							</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Rentan Tunggu',$html);	
   	}
	
	public function cetakRiwayatKunjungan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='RIWAYAT PASIEN';
		$param=json_decode($_POST['data']);
		
		$kd_unit=$param->kd_unit;
		$kd_pasien=$param->kd_pasien;
		$tgl_masuk=$param->tgl_masuk;
		$kd_kasir=$param->kd_kasir;
		$urut_masuk=$param->urut_masuk;
		$no_transaksi=$param->no_transaksi;
		$kd_dokter=$param->kd_dokter;
		
		$pasien = $this->db->query("select *,age(tgl_lahir) as ages from pasien where kd_pasien='".$param->kd_pasien."'")->row();
		$unit = $this->db->query("select nama_unit from unit where kd_unit='".$param->kd_unit."'")->row()->nama_unit;
		$dokter = $this->db->query("select nama from dokter where kd_dokter='".$param->kd_dokter."'")->row()->nama;
		
		$result = $this->db->query("select * from kunjungan where kd_pasien='".$param->kd_pasien."' and tgl_masuk='".$param->tgl_masuk."'");
		
		
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th> PerTanggal Kunjungan</th>
					</tr>
				</tbody>
			</table><br>';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="40" align="left">Nama</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$pasien->nama.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th align="left">Umur</th>
						<th align="left">:</th>
						<th width="80" align="left">'.getUmur($pasien->ages).'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Alamat</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$pasien->alamat.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Tgl. Kunjungan</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.tanggalstring($tgl_masuk).'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Nama Unit</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$unit.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Dokter</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$dokter.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
				</tbody>
			</table><br>';
		#=============================================================ISI============================================================#
		
		# ----------------------------------------------ANAMNESE---------------------------------------------------------------------#
		
		$queryanamnese = $this->db->query("select anamnese from kunjungan k
									where k.kd_pasien='".$kd_pasien."' 
										and k.kd_unit='".$kd_unit."' 
										and k.tgl_masuk='".$tgl_masuk."' 
										and k.urut_masuk='".$urut_masuk."'")->result();
		$resultStatus = $this->db->query("SELECT rwi_rujuk.catatan, mr_status.status from 
			mr_rwi_rujukan rwi_rujuk 
		INNER JOIN mr_status_rwirujukan mr_status ON rwi_rujuk.id_status::integer = mr_status.id_status 
		WHERE 
			rwi_rujuk.kd_pasien = '".$kd_pasien."' 
			and rwi_rujuk.tgl_masuk = '".$tgl_masuk."' 
			and rwi_rujuk.kd_unit = '".$kd_unit."'");

		if ($resultStatus->num_rows() > 0) {
			$catatan 	= $resultStatus->row()->catatan;
			$status 	= $resultStatus->row()->status;
		}else{
			$catatan 	= "";
			$status 	= "";
		}

		$html.='
			<table width="100%" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">ANAMNESE</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				<tr>
					<th colspan="2" align="center">Anamnese / Keluhan</th>
				</tr>
			</thead>';
		if(count($queryanamnese) > 0){
			foreach($queryanamnese as $lineanamnese){
				$html.='<tbody>
							<tr>
								<td align="left" style="padding-left:5px;" width="20%">Anamnesa</td>
								<td align="left" style="padding-left:5px;">'.$lineanamnese->anamnese.'</td>
							</tr>';
			}

			$html.='<tr>';
			$html.='<td style="padding-left:5px;" width="20%">Catatan : </td>';
			$html.='<td style="padding-left:5px;">'.$catatan.'</td>';
			$html.='</tr>';
			$html.='<tr>';
			$html.='<td style="padding-left:5px;" width="20%">Status  : </td>';
			$html.='<td style="padding-left:5px;">Status  : '.$status.'</td>';
			$html.='</tr>';
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>';
			$html.='<tr>';
			$html.='<td style="padding-left:5px;" width="20%">Catatan : </td>';
			$html.='<td style="padding-left:5px;">'.$catatan.'</td>';
			$html.='</tr>';
			$html.='<tr>';
			$html.='<td style="padding-left:5px;" width="20%">Status  : </td>';
			$html.='<td style="padding-left:5px;">Status  : '.$status.'</td>';
			$html.='</tr>';
			$html.='<tr>
							<td align="left">&nbsp;&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}

		/* ====================================================================== KONDISIK PASIEN ========================================== */
		$query_kondisi = $this->db->query("SELECT 
					A.id_kondisi AS id_kondisi,
					A.kondisi as kondisi, 
					A.satuan as satuan,
					A.orderlist as orderlist, 
					A.kd_unit as kd_unit, 
					B.hasil as hasil 
				FROM MR_KONPASDTL B INNER JOIN (SELECT * FROM MR_KONPAS 
				WHERE kd_pasien = '".$kd_pasien."' and mr_konpas.kd_unit='".$kd_unit."' and tgl_masuk = '".$tgl_masuk."' ) AS C ON B.ID_KONPAS = C .ID_KONPAS 
				RIGHT JOIN MR_KONDISIFISIK A ON A.id_kondisi = B.id_kondisi");
			$html.='
			<table width="100%" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="4" align="left">Kondisi</th>
				</tr>
				<tr>
					<th width="5%" align="center">No.</th>
					<th width="55%" align="center">Kondisi</th>
					<th width="20%" align="center">Hasil</th>
					<th width="20%" align="center">Satuan</th>
				</tr>
			</thead>';
			$html .="<tbody>";
			// if ($query_kondisi->num_rows() > 0) {
				$no = 1;
				foreach ($query_kondisi->result() as $result_kondisi) {
					$html .= "<tr>";
					$html .= "<td align='center'>".$no."</td>";
					$html .= "<td style='padding-left:5px;'>".$result_kondisi->kondisi."</td>";
					$html .= "<td style='padding-left:5px;'>".$result_kondisi->hasil."</td>";
					$html .= "<td style='padding-left:5px;'>".$result_kondisi->satuan."</td>";
					$html .= "</tr>";
					$no++;
				}
			// }
			$html .="</tbody>";
			$html .="</table><br>";

		/* ====================================================================== KONDISIK PASIEN ========================================== */
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------ICD 10 / DIAGNOSA---------------------------------------------------------------------#
		$querydiagnosa = $this->db->query("SELECT mrp.*,p.penyakit,n.morfologi,k.sebab 
											FROM mr_penyakit mrp 
												LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
												LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit 
													AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit 
													AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
												LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit 
													AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit 
													AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
											WHERE mrp.kd_pasien = '".$kd_pasien."' 
												and mrp.kd_unit='".$kd_unit."' 
												and mrp.tgl_masuk = '".$tgl_masuk." '")->result();
		
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="4" align="left">ICD 10 / Diagnosa</th>
				</tr>
				<tr>
					<th colspan="4" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="80" align="center">Deskripsi Penyakit</th>
					<th width="60" align="center">Status diagnosa</th>
					<th width="60" align="center">Kasus</th>
				  </tr>
			</thead>';
		if(count($querydiagnosa) > 0){
			$no=0;
			foreach($querydiagnosa as $linediagnosa){
				$no++;
				$diagnosa='';
				$kasus='';
				
				if ($linediagnosa->stat_diag == 0) {
					$diagnosa ='Diagnosa Awal';
				} else if ($linediagnosa->stat_diag == 1) {
					$diagnosa = 'Diagnosa Utama';
				} else if ($linediagnosa->stat_diag == 2) {
					$diagnosa ='Komplikasi';
				} else if ($linediagnosa->stat_diag == 3) {
					$diagnosa = 'Diagnosa Sekunder';
				}
				//true=kasus lama, false=kasus baru
				if ($linediagnosa->kasus == 't') {
					$kasus =  'Lama';
				} else if ($linediagnosa->kasus == 'f') {
					$kasus =  'Baru';
				}
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$linediagnosa->penyakit.'</td>
								<td align="left">'.$diagnosa.'</td>
								<td align="left">'.$kasus.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# ----------------------------------------------ICD 9 / TINDAKAN---------------------------------------------------------------------#
		
		$querytindakan = $this->db->query("select mr.* , ic.deskripsi
									from mr_tindakan mr
									inner join icd_9 ic on ic.kd_icd9=mr.kd_icd9
									where mr.kd_pasien='".$kd_pasien."' 
										and mr.kd_unit='".$kd_unit."' 
										and mr.tgl_masuk='".$tgl_masuk."' 
										and mr.urut_masuk='".$urut_masuk."'")->result();
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">ICD 9 / TINDAKAN</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Deskripsi</th>
				  </tr>
			</thead>';
		if(count($querytindakan) > 0){
			$no=0;
			foreach($querytindakan as $linetindakan){
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$linetindakan->deskripsi.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------------------------OBAT------------------------------------------------------------------------#
		
		$queryobat = $this->db->query("select bod.*, bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='".$kd_pasien."' 
										and bo.kd_unit='".$kd_unit."' 
										and t.tgl_transaksi='".$tgl_masuk."' 
										and t.urut_masuk='".$urut_masuk."'
									order by o.nama_obat")->result();
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">OBAT</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Nama obat</th>
				  </tr>
			</thead>';
		if(count($queryobat) > 0){
			$no=0;
			foreach($queryobat as $lineobat){
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$lineobat->nama_obat.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------------------------LABORATORIUM----------------------------------------------------------------#
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$querylab = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
											case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
										From LAB_hasil 
											inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
											inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
												on LAB_Test.kd_Test = produk.Kd_Produk
											inner join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
											inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
											inner join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
										where LAB_hasil.Kd_Pasien = '".$kd_pasien."' 
											And LAB_hasil.Tgl_Masuk = '".$tgl_masuk."'  
											and LAB_hasil.Urut_Masuk = '".$urut_masuk."'
											and LAB_hasil.kd_unit= '".$kd_unit_lab."' 
											and lab_hasil.kd_unit_asal='".$kd_unit."'
										order by lab_test.kd_lab,lab_test.kd_test asc")->result();
										
		
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="9" align="left">LABORATORIUM</th>
				</tr>
				<tr>
					<th colspan="9" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="20" align="center">Item Pemeriksaan</th>
					<th width="40" align="center">Pemeriksaan</th>
					<th width="40" align="center">Metode</th>
					<th width="20" align="center">Hasil</th>
					<th width="30" align="center">Normal</th>
					<th width="20" align="center">Ket. hasil</th>
					<th width="40" align="center">Satuan</th>
					<th width="40" align="center">Keterangan</th>
				  </tr>
			</thead>';
			
		if(count($querylab) > 0){
			$no=0;
			foreach($querylab as $linelab){
				$hasil = '';
				$ket = '';
				$metode = '';
				
				if ($linelab->hasil == 'null' || $linelab->hasil == null) {
					$hasil ='';
				} else {
					$hasil = $linelab->hasil;
				}
				
				if ($linelab->ket == 'null' || $linelab->ket == null || $linelab->ket == 'undefined') {
					$ket = '';
				} else {
					$ket = $linelab->ket;
				}
				
				if ($linelab->satuan == 'null' && $linelab->satuan == 'null') {
					$metode = '';
				} else {
					$metode = $linelab->metode;
				}
				if($linelab->judul_item == ''){
					$nomor='';
				} else{
					$no++;
					$nomor=$no;
				}
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$nomor.'</td>
								<td align="left">'.$linelab->judul_item.'</td>
								<td align="left">'.$linelab->item_test.'</td>
								<td align="center">'.$metode.'</td>
								<td align="left">&nbsp;'.$hasil.'</td>
								<td align="center">'.$linelab->normal.'</td>
								<td align="center">'.$linelab->ket_hasil.'</td>
								<td align="left">'.$linelab->satuan.'</td>
								<td align="left">'.$ket.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------------------------RADIOLOGI-------------------------------------------------------------------#
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$queryrad = $this->db->query("select dt.*, p.deskripsi
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi
									where k.kd_pasien='".$kd_pasien."' 
										and k.kd_unit='".$kd_unit_rad."'
										and tr.kd_unit='".$kd_unit."'
										and k.tgl_masuk='".$tgl_masuk."' 
										and k.urut_masuk='".$urut_masuk."'")->result();
		
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">RADIOLOGI</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Deskripsi pemeriksaan radiologi</th>
				  </tr>
			</thead>';
		if(count($queryrad) > 0){
			$no=0;
			foreach($queryrad as $linerad){
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$linerad->deskripsi.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Riwayat Pasien',$html);	
   	}
	
	public function cetakRWJTindakanPerPasien_(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN TINDAKAN PERPASIEN';
		$param=json_decode($_POST['data']);
		$html='';
		$kd_unit=$param->kd_unit;
		$kd_customer=$param->kd_customer;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$type_file =$param->type_file;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$ctgl = "where k.tgl_masuk >= '".date('Y-m-d',strtotime($tglAwal))."' and k.tgl_masuk <= '".date('Y-m-d',strtotime($tglAkhir))."'";
		if($kd_unit == 'Semua' || $kd_unit == ''){
			$cKdunit=" and u.parent not in('100') and t.kd_unit !='3'";
			$unit = 'SEMUA POLIKLINIK';
		} else{
			$cKdunit="and t.kd_unit='".$kd_unit."'";
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
		}
		
		if($kd_customer == 'SEMUA' || $kd_customer==''){
			$cKdcustomer = '';
			$customer='SEMUA';
		} else{
			$cKdcustomer = "and k.kd_customer='".$kd_customer."'";
			$customer = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
		}
		
		$queryHead = $this->db->query( "select distinct(t.kd_pasien),ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer
										from detail_transaksi dt 
											inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
											inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
											inner join produk p on p.kd_produk=dt.kd_produk
											inner join pasien ps on ps.kd_pasien=k.kd_pasien
											inner join dokter d on d.kd_dokter=k.kd_dokter
											inner join unit u on u.kd_unit=t.kd_unit
											inner join customer c on c.kd_customer=k.kd_customer
										WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
										".$cKdunit.$cKdcustomer."
										order by  c.customer ,t.tgl_transaksi,ps.nama");
		$query = $queryHead->result();	
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  class="t2" cellspacing="0" border="0">
				
					<tr>
						<td colspan="7">'.$title.'</td>
					</tr>
					<tr>
						<td colspan="7"> Periode '.$awal.' s/d '.$akhir.'</td>
					</tr>
					<tr>
						<td colspan="7"> Poli : '.$unit.'</td>
					</tr>
					<tr>
						<td colspan="7"> Jenis Pasien : '.$customer.'</td>
					</tr>
				
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			
				 <tr>
					<td width="5" align="center">No</td>
					<td width="60" align="center">No. Medrec</td>
					<td width="80" align="center">Nama</td>
					<td width="40" align="center">Tgl. Transaksi</td>
					<td width="60" align="center">Unit</td>
					<td width="40" align="center">Customer</td>
					<td width="40" align="center">Deskripsi tindakan</td>
				  </tr>
			';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
							<tr>
								<td align="left">'.$no.'</td>
								<td align="left">'.$line->kd_pasien.'</td>
								<td align="left">'.$line->nama.'</td>
								<td align="left">'.tanggalstring($line->tgl_transaksi).'</td>
								<td align="left">'.$line->nama_unit.'</td>
								<td align="left">'.$line->customer.'</td>
								<td></td>
							</tr>';
				$queryBody = $this->db->query( "select dt.*, t.kd_pasien,p.deskripsi as desk,ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer
							from detail_transaksi dt 
								inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
								inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
								inner join produk p on p.kd_produk=dt.kd_produk
								inner join pasien ps on ps.kd_pasien=k.kd_pasien
								inner join dokter d on d.kd_dokter=k.kd_dokter
								inner join unit u on u.kd_unit=t.kd_unit
								inner join customer c on c.kd_customer=k.kd_customer
							WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
							and t.kd_unit='".$line->kd_unit."' and k.kd_customer='".$line->kd_customer."' and t.kd_pasien='".$line->kd_pasien."'
							order by c.customer asc ");
					$query2 = $queryBody->result();	
				foreach($query2 as $line2){
					$html.='<tr>
								<td align="left" colspan="6">&nbsp;</td>
								<td align="left">'.$line2->desk.'</td>
							</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="7" align="center">Data tidak ada</td>
				</tr>

			';		
		} 
		
		$html.='</table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('tindakan per pasien'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_tindakan_perpasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('L','Lap. Tindakan PerPasien',$html);	
		}
		echo $html;
   	}

   	public function cetaksepdirect(){
		$this->load->library('m_pdf');
		$uri = $_SERVER["REQUEST_URI"];
		$uriArray = explode('/', $uri);
		$param1 = $_POST['noSep'];
	 	$html='';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		//echo $kd_rs;
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		
		$kd_user=$this->session->userdata['user_id']['id'];
		$sqlUser="SELECT user_names FROM zusers WHERE kd_user='".$kd_user."'";
		$objUser=$this->db->query($sqlUser)->row();
		$operator='-';
		if($objUser){
			$operator=$objUser->user_names;
		}
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$this->getSignature()
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$param1,false,$context),false);
		if($res->metadata->code=='200'){
			if($res->response->peserta->sex == 'P'){
				$jk='WANITA';
			} else{
				$jk='PRIA';
			} 
			$nama=$res->response->peserta->nama;
			$peserta=$res->response->peserta->jenisPeserta->nmJenisPeserta;
			$tglsep=tanggalstring($res->response->tglSep);
			$printer = 'Epson-LX-300_NUANSA';
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
			$handle = fopen($file, 'w');
			$condensed = Chr(27).Chr(33).Chr(4);
			$condensed2 = Chr(27).Chr(33).Chr(8);
			$bold1 = Chr(27) . Chr(69);
			$bold0 = Chr(27) . Chr(70);
			$initialized = chr(27).chr(64);
			$condensed1 = chr(15);
			$condensed0 = chr(18);
			$size0 = Chr(27).Chr(109);
			$Data  = $initialized;
			$Data .= $condensed1;
			$Data .= $condensed;
			
			// $cmd ="jpegoptim --stdim > http://192.168.1.25/medismart/ui/images/Logo/RSSM.png"; //http://192.168.1.25/medismart/ui/images/Logo/RSSM.png";
			// exec('convert -quality 80 "http://192.168.1.25/medismart/MediSmart.pdf" "http://192.168.1.25/medismart/ui/images/Logo/RSSM.png"',$out,$ret);
			// echo $ret;
			// print_r($out);

			// ob_start();
			// $cmd = im
			// imagejpeg($cmd,"/var/www/html/medismart/ui/images/Logo/RSSM.png");
			// echo $cmd;
			// echo 'a';
			// imagedestroy($cmd);
			// $img = ob_get_clean();
			// $handlea = popen($cmd, 'w');
			
			// $Data .= $img;
			// fwrite($handle, $Data);
			// fclose($handle);
			// $print = shell_exec("lpr -P ".$printer." -r ".$file);
			echo 'a';
			//$objPHPExcel = PHPExcel_IOFactory::load("/var/www/html/medismart/ui/template/sep.xlsx");
			echo 'a';
			// $objPHPExcel->setActiveSheetIndex(0)
                            // ->setCellValue('A2', "No")
                            // ->setCellValue('B2', "Name")
                            // ->setCellValue('C2', "Email")
                            // ->setCellValue('D2', "Phone")
                            // ->setCellValue('E2', "Address");
							echo 'b';
			// $file = '/var/www/html/medismart/MediSmart.pdf';
			//echo file_get_contents('/var/www/html/medismart/ui/template/sep.xlsx');
			//echo preg_match( "/\\?".">\\s\\s+\\Z/m", file_get_contents('/var/www/html/medismart/ui/template/sep.xlsx'));
			//foreach (glob("*.php") as $file){if (preg_match( "/\\?".">\\s\\s+\\Z/m", file_get_contents($file))) echo("$file\n");}
			
			
			$cmd = 'lpr -P Epson-LX-300_NUANSA /var/www/html/medismart/ui/template/headerSEP.pdf';
			// $cmd .=$file;
			$response = shell_exec($cmd);
		}
		
	}
	
	 
}
?>