<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionRWJ extends  MX_Controller
{
	private $jam_request  = "";
	private $menit_request  = "";
	private $detik_request  = "";
	private $waktu_respones = "";
	public 	$ErrLoginMsg = '';
	public 	$user = '';
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		$this->load->library('LZCompressor/lzstring');
		$this->jam_request = date('H');
		$this->menit_request = date('i');
		$this->detik_request = date('s');
		$query = $this->db->query("SELECT * FROM zusers where kd_user='" . $this->session->userdata['user_id']['id'] . "'");
		if ($query->num_rows() > 0) {
			$this->user = $query->row()->Full_Name;
		} else {
			$this->user = "";
		}
	}

	public function index()
	{

		$this->load->view('main/index');
	}

	function decompress($string, $timestamp)
	{
		$conspwd = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$consid  = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		date_default_timezone_set('UTC');
		$time = $timestamp;
		$key = $consid . $conspwd . $time;
		$encrypt_method = 'AES-256-CBC';
		$key_hash = hex2bin(hash('sha256', $key));
		$iv = substr(hex2bin(hash('sha256', $key)), 0, 16);
		$string = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
		$string = $this->lzstring->decompressFromEncodedURIComponent($string);
		$string = json_decode($string);
		return $string;
	}

	public function cekKunjungan()
	{
		if ($_POST['kd_pasien'] == '') {
			echo '{success:true}';
		} else {
			$cekRWI = $this->db->query("select count(*) jmlkunjungan from kunjungan where kd_pasien='" . $_POST['kd_pasien'] . "'
			and LEFT(kd_unit,1)='1' AND tgl_keluar is null ")->row()->jmlkunjungan;
			if ($cekRWI == 0) {
				echo '{success:true}';
			} else {
				echo '{success:false}';
			}
		}
	}
	//##tambah validasi kunjungan untuk pasien yang akan generate SEP
	public function cekKunjunganPasien()
	{
		$kd_pasien = $_POST['NoMedrec'];
		$tgl_masuk = date('Y-m-d');
		$kd_unit = $_POST['Poli'];
		$cekKunjungan = $this->db->query("select * from kunjungan where kd_pasien='$kd_pasien' and tgl_masuk='$tgl_masuk' and kd_unit='$kd_unit' ")->result();
		if (count($cekKunjungan) == 0) {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}
	//##tambah validasi kunjungan untuk pasien yang akan generate SEP
	public function getDataRujukan()
	{
		$kdrujuk = $_POST['koderujuk'];
		$namanya = '';
		$alamatnya = '';
		$kotanya = '';
		$caridata = $this->db->query("select * from rujukan where kd_rujukan=$kdrujuk ")->result();
		foreach ($caridata as $d) {
			$nama = $d->rujukan;
			$alamat = $d->alamat;
			$kota = $d->kota;
		}
		echo "{nama: '" . $nama . "', alamat: '" . $alamat . "', kota: '" . $kota . "'}";
	}

	public function jml_antrian_dokter()
	{
		$kd_dokter = $_POST['kd_dokter'];
		$total = 0;
		$caridata = $this->db->query("SELECT 
									(SELECT count(k.kd_pasien) 
									from kunjungan k 
									INNER JOIN ANTRIAN_POLIKLINIK ap ON ap.kd_pasien = k.KD_PASIEN and ap.kd_unit = k.KD_UNIT and ap.tgl_transaksi = k.TGL_MASUK
									where kd_dokter = '".$kd_dokter."'  and k.tgl_masuk = '".date('Y-m-d')."') as jml_yg_dipanggil
									 ")->result(); //(SELECT count (k.kd_pasien) from kunjungan k INNER JOIN ANTRIAN_POLIKLINIK ap ON ap.kd_pasien = k.KD_PASIEN and ap.kd_unit = k.KD_UNIT and ap.tgl_transaksi = k.TGL_MASUK where kd_dokter = '".$kd_dokter."' and ap.id_status_antrian = 2 and k.tgl_masuk = '".date('Y-m-d')."' ) as jml_yg_dipanggil
		if (count($caridata) > 0) {
			$total = $caridata[0]->jml_yg_dipanggil;
		}
		echo "{jml: '" . $total . "'}";
	}

	//hani 2023-02-17
	public function getAntrianPoli()
   {
	
		$carinomorsemua=$this->db->query("select count(no_urut) as no_urut from antrian_poliklinik where tgl_transaksi='".date('Y-m-d')."' AND kd_unit='".$_POST['grupPoli']."'")->row()->no_urut; //AND id_group='".$_POST['grupLoket']."' and
		$carinomorterpanggil=$this->db->query("select count(no_urut) as no_urut from antrian_poliklinik where tgl_transaksi='".date('Y-m-d')."' and id_status_antrian='2' AND kd_unit='".$_POST['grupPoli']."'" )->row()->no_urut; //AND id_group='".$_POST['grupLoket']."' and
		$carinomorbelumterpanggil=$this->db->query("select TOP 1 no_urut from antrian_poliklinik where tgl_transaksi='".date('Y-m-d')."' and  id_status_antrian='0' AND kd_unit='".$_POST['grupPoli']."' order by no_antrian asc")->row(); //AND id_group='".$_POST['grupLoket']."' and
		$sisa_antrian = $this->db->query("select top 1 count(no_urut) as sisa_antrian from antrian_poliklinik where tgl_transaksi='".date('Y-m-d')."' and  id_status_antrian='0' AND kd_unit='".$_POST['grupPoli']."'  order by sisa_antrian asc ")->row()->sisa_antrian;
		// $kodePoli = $this->db->query("select unit_bpjs from map_unit_bpjs where kd_unit ='".$_POST['grupPoli']."'")->row()->unit_bpjs;
	
	//  echo "<pre>".var_export($carinomorbelumterpanggil->row(),true)."</pre>";
		if(count($carinomorbelumterpanggil) == 0){
		$carinomorbelumterpanggil = 0;
		}else{
		$carinomorbelumterpanggil = $carinomorbelumterpanggil->no_urut;
		}
	echo "{semuanomor: '".$carinomorsemua."', semuanomorterpanggil: '".$carinomorterpanggil."', semuanomorbelumterpanggil: '".$carinomorbelumterpanggil."', sisa_antrian:'".$sisa_antrian."', kd_unit:'".$_POST['grupPoli']."'}";
   }

   public function simpanDisplayAntrianPoli()
   {
		$this->db->trans_begin();
	   //$loketnya                =$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
	   $sql                       = $this->db->query("UPDATE display_antrian_poliklinik set no_antrian=" . $_POST['nomor_antri'] . " , prioritas='" . $_POST['txt_prioritas'] . "'  where kd_unit='" . $_POST['kd_unit'] . "' ");
	//    $sql                       = $this->db->query("UPDATE display_antrian_poliklinik set cari_loket='" . $_POST['grup'] . "'");
	//    $jam                       = $this->db->query("SELECT jam from antrian_list_antrian where is_dipanggil = '0' order by no_antrian asc")->row()->jam;

	   // if($_POST['txt_prioritas'] == "false" || $_POST['txt_prioritas'] == false){
	   $updateantrian = $this->db->query("UPDATE antrian_poliklinik set id_status_antrian='1' where tgl_transaksi='" . date('Y-m-d') . "' 
		   and no_urut = '" . $_POST['nomor_antri'] . "' and kd_unit= '" . $_POST['kd_unit'] . "'  ");
	   // }//else{
	   //	$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='0' where tanggal='".date('Y-m-d')."' 
	   //	and no_antrian = '".$_POST['nomor_antri']."' ");
	   //}
	   if ($this->db->trans_status()) {
			$this->db->trans_commit();
		   echo "{success: true}";
	   } else {
			$this->db->trans_rollback();
			echo "{success: false}";
	   }
   }

   public function ulangiPanggilDisplayAntrianPoli()
	{
		$this->db->trans_begin();

		// $loketnya = $this->db->query("select id_group from antrian_list_loket where kode_loket='" . $_POST['loket'] . "' ")->row()->id_group;
		$updateantrian = $this->db->query("update antrian_poliklinik set id_status_antrian='1' where tgl_transaksi='" . date('Y-m-d') . "' and kd_unit= '" . $_POST['kd_unit'] . "' and no_urut='" . $_POST['nomor_antri'] . "' ");
		$sql = $this->db->query("update display_antrian_poliklinik set no_antrian=" . $_POST['nomor_antri'] . " , prioritas='true' where kd_unit='" . $_POST['kd_unit']  . "' ");
		// $sql = $this->db->query("update display_antrian set cari_loket='" . $loketnya . "'");
		if ($this->db->trans_status()) {
			$this->db->trans_commit();
			echo "{success: true}";
		} else {
			$this->db->trans_rollback();
			echo "{success: false}";
		}
	}





	//======================================
	public function getAntrian()
	{
		error_reporting(0);
		//echo $_POST['grupLoket'];
		if ($_POST['grupLoket'] != 'kosong') {
			$carinomorsemua = $this->db->query("SELECT top 1 max(no_antrian) as no_antrian from antrian_list_antrian where tanggal='" . date('Y-m-d') . "' AND id_group='" . $_POST['grupLoket'] . "' ")->row()->no_antrian;
			$carinomorterpanggil = $this->db->query("SELECT top 1 max(no_antrian) as no_antrian from antrian_list_antrian where tanggal='" . date('Y-m-d') . "' AND id_group='" . $_POST['grupLoket'] . "' and is_dipanggil='1'")->row()->no_antrian;
			$carinomorbelumterpanggil = $this->db->query("SELECT top 1 no_antrian from antrian_list_antrian where tanggal='" . date('Y-m-d') . "' AND id_group='" . $_POST['grupLoket'] . "' and is_dipanggil='0' order by no_antrian asc")->row()->no_antrian;
		} else {
			//$idgrup=$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['kodeloket']."' ")->row()->id_group;
			//$cariantriannosemua=$this->db->query("select * as no_antrian from antrian_list_antrian where kode_loket='".$idgrup."' and tanggal='".date('Y-m-d')."' ")->result();
			$carinomorsemua=$this->db->query("select max(no_antrian) as no_antrian from antrian_list_antrian where id_group='".$idgrup."' and tanggal='".date('Y-m-d')."' ")->row()->no_antrian;
			$carinomorterpanggil=$this->db->query("select max(no_antrian) as no_antrian from antrian_list_antrian where id_group='".$idgrup."' and tanggal='".date('Y-m-d')."' and is_dipanggil='1'")->row()->no_antrian;
			$carinomorbelumterpanggil=$this->db->query("select top 1 no_antrian from antrian_list_antrian where id_group='".$idgrup."' and tanggal='".date('Y-m-d')."' and is_dipanggil='0' order by no_antrian asc")->row()->no_antrian;
			//$carinomorsemua = 0;
		}
		echo "{semuanomor: '" . $carinomorsemua . "', semuanomorterpanggil: '" . $carinomorterpanggil . "', semuanomorbelumterpanggil: '" . $carinomorbelumterpanggil . "'}";
	}
	public function simpanDisplayAntrian()
	{	
		//$loketnya                =$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
		$sql                       = $this->db->query("UPDATE display_antrian set no_antrian=" . $_POST['nomor_antri'] . " , urut_loket='" . $_POST['loket'] . "', prioritas='" . $_POST['prioritas'] . "'  where loket='" . $_POST['grup'] . "' ");
		$sql                       = $this->db->query("UPDATE display_antrian set cari_loket='" . $_POST['grup'] . "'");
		$jam                       = $this->db->query("SELECT jam from antrian_list_antrian where is_dipanggil = '0' order by no_antrian asc")->row()->jam;

		$tz = 'Asia/Makassar';
		$dt = new DateTime("now", new DateTimeZone($tz));
		$timestamp = $dt->format('Y-m-d G:i:s');
		// if($_POST['txt_prioritas'] == "false" || $_POST['txt_prioritas'] == false){
		$updateantrian = $this->db->query("UPDATE antrian_list_antrian set kode_loket='" . $_POST['loket'] . "',is_dipanggil='1', jam_panggilan_terakhir='".$timestamp."', qty_panggilan=1 where tanggal='" . date('Y-m-d') . "' 
			and no_antrian = '" . $_POST['nomor_antri'] . "' and id_group= '" . $_POST['grup'] . "'  ");
		// }//else{
		//	$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='0' where tanggal='".date('Y-m-d')."' 
		//	and no_antrian = '".$_POST['nomor_antri']."' ");
		//}
		if ($updateantrian) {
			echo "{success: true}";
		} else {
			echo "{success: false}";
		}
	}
	public function PanggilUlangAntrian()
	{
		//$loketnya                =$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
		$sql                       = $this->db->query("UPDATE display_antrian set no_antrian=" . $_POST['nomor_antri'] . " , urut_loket='" . $_POST['loket'] . "', prioritas='" . $_POST['prioritas'] . "'  where loket='" . $_POST['grup'] . "' ");
		$sql                       = $this->db->query("UPDATE display_antrian set cari_loket='" . $_POST['grup'] . "'");
		$jam                       = $this->db->query("SELECT jam from antrian_list_antrian where is_dipanggil = '0' order by no_antrian asc")->row()->jam;

		// if($_POST['txt_prioritas'] == "false" || $_POST['txt_prioritas'] == false){
			$tz = 'Asia/Makassar';
		$dt = new DateTime("now", new DateTimeZone($tz));
		$timestamp = $dt->format('Y-m-d G:i:s');
		$updateantrian = $this->db->query("UPDATE antrian_list_antrian set kode_loket='" . $_POST['loket'] . "',is_dipanggil='1',jam_panggilan_terakhir='".$timestamp."', QTY_PANGGILAN = (SELECT MAX(QTY_PANGGILAN) FROM antrian_list_antrian WHERE tanggal='" . date('Y-m-d') . "' and no_antrian = '" . $_POST['nomor_antri'] . "' and id_group= '" . $_POST['grup'] . "' ) + 1 where tanggal='" . date('Y-m-d') . "' 
			and no_antrian = '" . $_POST['nomor_antri'] . "' and id_group= '" . $_POST['grup'] . "'  ");
		// }//else{
		//	$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='0' where tanggal='".date('Y-m-d')."' 
		//	and no_antrian = '".$_POST['nomor_antri']."' ");
		//}
		if ($updateantrian) {
			echo "{success: true}";
		} else {
			echo "{success: false}";
		}
	}

	public function HistoryPanggilan()
	{
		//$loketnya                =$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
		$sql                       = $this->db->query("SELECT id_group, no_antrian, kode_loket, jam_panggilan_terakhir, qty_panggilan from antrian_list_antrian where id_group = '" . $_POST['grup'] . "' and tanggal='".date("Y-m-d")."'  order by no_antrian asc")->result();
		if ($sql) {
			echo "{success: true, listData:".json_encode($sql)."}";
		} else {
			echo "{success: false}";
		}
	}

	public function LewatiAntrian()
	{
		//$loketnya                =$this->db->query("select id_group from antrian_list_loket where kode_loket='".$_POST['loket']."' ")->row()->id_group;
		// $sql                       = $this->db->query("UPDATE display_antrian set no_antrian=" . $_POST['nomor_antri'] . " , urut_loket='" . $_POST['loket'] . "', prioritas='" . $_POST['prioritas'] . "'  where loket='" . $_POST['grup'] . "' ");
		// $sql                       = $this->db->query("UPDATE display_antrian set cari_loket='" . $_POST['grup'] . "'");
		$jam = $this->db->query("SELECT jam from antrian_list_antrian where is_dipanggil = '0' order by no_antrian asc")->row()->jam;

		// if($_POST['txt_prioritas'] == "false" || $_POST['txt_prioritas'] == false){
		$tz = 'Asia/Makassar';
		$dt = new DateTime("now", new DateTimeZone($tz));
		$timestamp = $dt->format('Y-m-d G:i:s');
		$updateantrian = $this->db->query("UPDATE antrian_list_antrian set is_dipanggil='1',jam_panggilan_terakhir='".$timestamp."', QTY_PANGGILAN = 1 where tanggal='" . date('Y-m-d') . "' 
			and no_antrian = '" . $_POST['nomor_antri'] . "' and id_group= '" . $_POST['grup'] . "'  ");
		// }//else{
		//	$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='".$_POST['loket']."',is_dipanggil='0' where tanggal='".date('Y-m-d')."' 
		//	and no_antrian = '".$_POST['nomor_antri']."' ");
		//}
		if ($updateantrian) {
			echo "{success: true}";
		} else {
			echo "{success: false}";
		}
	}
	public function ulangiPanggilDisplayAntrian()
	{
		$loketnya = $this->db->query("select id_group from antrian_list_loket where kode_loket='" . $_POST['loket'] . "' ")->row()->id_group;
		$updateantrian = $this->db->query("update antrian_list_antrian set kode_loket='" . $_POST['loket'] . "',is_dipanggil='1' where tanggal='" . date('Y-m-d') . "' and id_group= '" . $loketnya . "' and no_antrian='" . $_POST['nomor_antri'] . "' ");
		$sql = $this->db->query("update display_antrian set no_antrian=" . $_POST['nomor_antri'] . " , urut_loket=" . $_POST['loket'] . ", prioritas=TRUE where loket='" . $loketnya . "' ");
		$sql = $this->db->query("update display_antrian set cari_loket='" . $loketnya . "'");
		if ($sql) {
			echo "{success: true}";
		} else {
			echo "{success: false}";
		}
	}

	public function getDataBpjs()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
		if (count($url) <> 0) {
			$klinik = $_POST['klinik'];
			$res = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $klinik . "'");
			$poli = null;
			$no = $_POST['no_kartu'];
			if ($res->result()) {
				$poli = $res->row()->unit_bpjs;
			}
		}
		$headers = $this->getSignature();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// echo $url.$no."/tglSEP/".date("Y-m-d");die();
		// $context = stream_context_create($opts);
		// $res     = json_decode(file_get_contents($url.$no."/tglSEP/".date("Y-m-d"),false,$context));

		$urlnya = $url . $no . "/tglSEP/" . date("Y-m-d");
		$method = "GET";
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);

		$response_encript = $res->response;
		$res->response = $this->decompress($res->response, $timestamp);

		$dat = array(
			'poli' => $poli,
			'user' => $this->session->userdata['user_id']['id'],
			'data' => $res,
			'header' => $headers,
			'kd_rs' => $bpjs_kd_res,
			'encript' => $response_encript
		);

		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url . $no . "/tglSEP/" . date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat);
	}

	public function getDataBpjsDenganNIK()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNIK'")->row()->nilai;
		$no = $_POST['no_kartu'];
		$headers = $this->getSignature();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		//  $opts = array(
		//   'http'=>array(
		// 	'method'=>'GET',
		// 	'header'=>$this->getSignature()
		//   )
		// );
		// echo str_replace(" ", "", $url.$no."/tglSEP/".date("Y-m-d"));die();
		// $context = stream_context_create($opts);
		// $res     = json_decode(file_get_contents( str_replace(" ", "", $url.$no."/tglSEP/".date("Y-m-d")),false,$context));

		$urlnya = str_replace(" ", "", $url . $no . "/tglSEP/" . date("Y-m-d"));
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		// $this->insert_response_bpjs(str_replace(" ", "", $url . $no . "/tglSEP/" . date("Y-m-d")), $dat['data'], json_encode($res), "GET", $data_pasien);
		// $this->insert_response_bpjs($url . $no . "/tglSEP/" . date("Y-m-d"), $res, json_encode($res), "GET");
		echo json_encode($dat);
	}

	public function getDataBpjsDenganSEP()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$no = $_POST['no_sep'];

		/*$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url . '/' . $no, false, $context)); */

		$urlnya = $url . '/' . $no;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		// $this->insert_response_bpjs($url . '/' . $no, $res, json_encode($res), "GET");
		echo json_encode($dat);
	}

	public function cariRujukanFKTL()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai;
		$no = $_POST['no_kartu'];
		$header = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $header[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $header
			)
		);
		$urlnya = '';
		$tipenya = $_POST['tipe'];
		$tglrujuk = $_POST['tgl_rujuk'];
		if ($tipenya == 'bpjs') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoka'")->row()->nilai . $no;
		} else if ($tipenya == 'norujukan') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTLByNoRujukan'")->row()->nilai . $no;
		} else if ($tipenya == 'tglrujuk') {
			$urlnya = $this->db->query("SELECT nilai FROM seting_bpjs WHERE key_setting='UrlCariRujukanFKTLByTglRujukan'")->row()->nilai . $tglrujuk;
		}
		$now = new DateTime();
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);

		$dat = array(
			'data' => $res
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
			// $dat['jml_rujukan'] = count($res->response->rujukan);
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		$this->insert_response_bpjs($urlnya, $res, json_encode($res), "GET");
		echo json_encode($dat);
	}

	// Updated 21-01-2022 By DP --> Cari Rujukan FKTP V 2.0
	public function cariRujukanFKTP()
	{
		$bpjs_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$bpjs_data = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$bpjs_url_briging = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
		$bpjs_url_getSep = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCreateSEP'")->row()->nilai;
		if (count($bpjs_url_briging) <> 0) {
			date_default_timezone_set('UTC');
			$tStamp = time(); //strval(time()-strtotime('1970-01-01 00:00:00'));
			$data = $bpjs_data;
			$secretKey = $bpjs_secretKey;
			$klinik = $_POST['klinik'];
			$signature = hash_hmac('sha256', $data . "&" . $tStamp, $secretKey, true);
			$encodedSignature = base64_encode($signature);
			$res = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $klinik . "'");
			$poli = null;
			$no = $_POST['no_kartu'];
			$headers = $this->getSignature_new();
			// $headers = "X-cons-id: " . $data . "\r\n" .
			// 	"X-timestamp: " . $tStamp . "\r\n" .
			// 	"X-signature: " . $encodedSignature . "\r\n";
			/* $result=array(
				'headers'=>$headers,
				//'id'=>$data,
				//'timestamp'=>	$tStamp,
				//'signature'=>	$encodedSignature,
				'kd_rs'=>	$bpjs_kd_res,
				//'url_briging'=>$common->getSystemProperty('URL_BRIGING', $defaultTenant)->getPropertyValue(),
				'url_getSep'=>$bpjs_url_getSep,
			); */
			if ($res->result()) {
				$poli = $res->row()->unit_bpjs;
			}
			$result = array(
				'id' => $data,
				'timestamp' =>	$tStamp,
				'signature' =>	$encodedSignature,
				'kd_rs' =>	$bpjs_kd_res,
				'url_briging' => $bpjs_url_briging,
				'url_getSep' => $bpjs_url_getSep,
				'poli' => $poli
			);
		}
		//$headers.=' Content-type: Application/x-www-form-urlencoded\r\n';
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);

		$urlnya = '';
		$tipenya = $_POST['tipe'];
		$start = $_POST['startnya'];
		$limit = $_POST['limitnya'];
		$tglrujuk = $_POST['tgl_rujuk'];
		if ($tipenya == 'bpjs') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoka'")->row()->nilai . $no;
		} else if ($tipenya == 'norujukan') {
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByRujukan'")->row()->nilai . $no;
		} else if ($tipenya == 'tglrujuk') {
			// $urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByTglRujukan'")->row()->nilai . $tglrujuk . '/query?start=' . $start . '&limit=' . $limit . ' ';
			$urlnya = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByTglRujukan'")->row()->nilai . $tglrujuk;
		}
		$now = new DateTime();
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$this->insert_response_bpjs($urlnya, $res, json_encode($res), "GET");
		if ($res) {
			echo "{success:true, poli:'" . $poli . "',urlbridging:'" . $urlnya . "', data: " . json_encode($res) . ",headers:" . json_encode($headers) . ",url_getSep:'" . $bpjs_url_getSep . "' , id:'" . $data . "', timestamp:'" . $tStamp . "', signature:'" . $encodedSignature . "'}";
		} else {
			echo "{success:false}";
		}
	}

	private function searchMessage($arr)
	{
		$message = '';
		if (gettype($arr) == 'object') {
			foreach ($arr as $key => $value) {
				if (gettype($value) != 'object' && gettype($value) != 'array') {
					if ($key == 'field') {
						$message .= $value;
					}
					if ($key == 'message') {
						$message .= ' ' . $value . '<br>';
					}
				} else {
					$message .= $this->searchMessage($value);
				}
			}
		} else if (gettype($arr) == 'array') {
			for ($i = 0, $iLen = count($arr); $i < $iLen; $i++) {
				$message .= $this->searchMessage($arr[$i]);
			}
		} else if (gettype($arr) == 'string') {
			$message .= $arr;
		}
		return $message;
	}
	/*public function createSEP(){
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlBuatSEP'")->row()->nilai;
		$data=$_POST['data'];
		$headers=$this->getSignature();
		$headers.='Content-type: Application/x-www-form-urlencoded\r\n';
		$opts = array(
		  'http'=>array(
			'method'=>"POST",
			'header'=>$headers,
			'content'=>$data
		  )
		);
		$context = stream_context_create($opts);
		$res=json_decode(file_get_contents($url,false,$context));
		$data=array();
		$data['sep']=null;
		if($res->metadata->code=='200'){
			$data['sep']=$res->response;
		}else{
			$data['message']=$this->searchMessage($res);
		}
		echo json_encode($data);
	}*/

	public function createSEP()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlBuatSEP'")->row()->nilai;
		$data = $_POST['data'];

		$headers = $this->getSignatureVedika();
		// $headers.='Content-type: Application/x-www-form-urlencoded\r\n';
		$opts = array(
			'http' => array(
				'method'  => "POST",
				'header'  => $headers,
				'content' => $data
			)
		);
		$xml   = simplexml_load_string($data); // where $xml_string is the XML data you'd like to use (a well-formatted XML string). If retrieving from an external source, you can use file_get_contents to retrieve the data and populate this variable.
		$json  = json_encode($xml); // convert the XML string to JSON
		//print_r($json);die;
		$array = json_decode($json, TRUE); // convert the JSON-encoded string to a PHP variable

		$ch = curl_init($url);
		//The JSON data.
		$jsonData = $json;
		//echo "string";die();

		// print_r($jsonData);die();

		curl_setopt($ch, CURLOPT_URL, $url);

		//Encode the array into JSON.
		$jsonDataEncoded = json_encode($jsonData);

		//Tell cURL that we want to send a POST request.
		curl_setopt($ch, CURLOPT_POST, 1);

		//Attach our encoded JSON string to the POST fields.
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_decode($jsonDataEncoded));

		//Set the content type to application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// print_r($headers);
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		// $response = $ch;

		curl_close($ch);
		// print_r($response);die();
		$this->insert_response_bpjs($url, json_decode($response), $jsonData, "POST");
		echo $response;
	}
	public function hapusSEP_inasis()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlDeleteSEP'")->row()->nilai;
		$headers = $this->getSignature();
		$headers .= 'Content-type: Application/x-www-form-urlencoded\r\n';
		$data = '<request>
			<data>
			<t_sep>
			<noSep>' . $_POST['sep'] . '</noSep>
			<ppkPelayanan>' . $bpjs_kd_res . '</ppkPelayanan>
			</t_sep>
			</data>
			</request>';
		$opts = array(
			'http' => array(
				'method' => "DELETE",
				'header' => $headers,
				'content' => $data
			)
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url, false, $context));
		$data = array();
		$data['data'] = $res->response;
		if ($res->metadata->code == '200') {
			$data['result'] = 'SUCCESS';
		} else {
			$data['result'] = 'ERROR';
			$data['message'] = $this->searchMessage($res);
		}
		echo json_encode($data);
	}

	public function hapusSEP()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlDeleteSEP'")->row()->nilai;
		$name = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $this->session->userdata['user_id']['id'] . "'")->row()->full_name;
		$sep = $_POST['sep'];
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$jsonData = '
				{
				"request": {
					"t_sep": {
						"noSep": "' . $sep . '",
						"user": "' . $name . '"
					}
				}
				}';
		$jsonDataEncoded = json_encode($jsonData);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_decode($jsonDataEncoded));

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($response);
		$response->response = $this->decompress($response->response, $timestamp);
		if ($response->metaData->code == 200) {
			$update = $this->db->query("UPDATE kunjungan set no_sjp='-' where no_sjp='" . $sep . "' ");
			$sql = $this->db->query("DELETE FROM rujukan_kunjungan WHERE kd_pasien in (SELECT kd_pasien FROM history_sep_bpjs WHERE no_sep='" . $sep . "') 
			AND kd_unit in(SELECT kd_unit FROM history_sep_bpjs WHERE no_sep='" . $sep . "') 
			AND tgl_masuk in(SELECT tgl_masuk FROM history_sep_bpjs WHERE no_sep='" . $sep . "')");
			if ($sql) {
				$this->db->query("DELETE FROM history_sep_bpjs WHERE no_sep='" . $sep . "'");
			}
			// print_r($response);die();
		}
		$this->insert_response_bpjs($url, $response, $jsonData, "DELETE");
		echo json_encode($response);
	}

	// Updated 21-01-2022 By DP for Vclaim 2.0
	public function hapusSEPInternal()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url 		 = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlDeleteSEPInternal'")->row()->nilai;
		$name 		 = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $this->session->userdata['user_id']['id'] . "'")->row()->full_name;
		$sep 		 = $_POST['nosep'];
		$nosurat 	 = $_POST['nosurat'];
		$tglrujuk 	 = $_POST['tgl'];
		$poli 		 = $_POST['poli'];
		$histori_sep = $this->db->query("SELECT * FROM history_sep_bpjs WHERE no_sep='" . $sep . "' AND tgl_masuk='" . $tglrujuk . "' AND poli_tujuan='" . $poli . "'")->result();
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		$jsonData = '{
			"request": {
				"t_sep": {
					"noSep": "' . $sep . '",
					"noSurat": "' . $nosurat . '",
					"tglRujukanInternal": "' . $tglrujuk . '",
					"kdPoliTuj": "' . $poli . '",
					"user": "' . $name . '"
				}
			}
		}';

		$jsonDataEncoded = json_encode($jsonData);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_decode($jsonDataEncoded));

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($response);
		$response->response = $this->decompress($response->response, $timestamp);
		if ($response->metaData->code == 200) {
			$cekQuery = $this->db->query("SELECT * FROM kunjungan WHERE no_sjp='" . $sep . "' AND tgl_masuk='" . $tglrujuk . "' AND kd_unit='" . $histori_sep[0]->kd_unit . "'");
			if ($cekQuery->num_rows() > 0) {
				$update = $this->db->query("UPDATE kunjungan set no_sjp='-' where no_sjp='" . $sep . "' AND tgl_masuk='" . $tglrujuk . "' AND kd_unit='" . $histori_sep[0]->kd_unit . "'");
				$sql = $this->db->query("DELETE FROM rujukan_kunjungan WHERE kd_pasien ='" . $histori_sep[0]->kd_pasien . "' 
				AND kd_unit ='" . $histori_sep[0]->kd_pasien . "' AND tgl_masuk ='" . $tglrujuk . "'");
				if ($sql) {
					$this->db->query("DELETE FROM history_sep_bpjs WHERE no_sep='" . $sep . "' AND tgl_masuk='" . $tglrujuk . "' AND poli_tujuan='" . $poli . "'");
				}
			}
		}
		$this->insert_response_bpjs($url, $response, $jsonData, "DELETE");
		echo json_encode($response);
	}

	public function getDataBpjs_by_norujukan_RS_local()
	{
		$response = array(
			'headers' 	=> "",
			'kd_rs' 	=> "",
			'user' 		=> "",
			'data' 		=> array(),
		);
		// $response['data'] = array( 'metaData' => array( 'code' => '200', 'message' => 'OK', ) );

		$query = $this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'");
		$provider = "";
		if ($query->num_rows() > 0) {
			$provider = $query->row()->nilai;
		} else {
			echo 'success: false, listData: {metaData: {code: "201", message: "Setting PPK tidak ada"}, response: null}}';
			die;
		}

		$query = "SELECT 
			pen.kd_penyakit as kode_diagnosa, 
			pen.penyakit as nama_diagnosa,
			'' as keluhan, 
			hsb.no_sep as no_kunjungan,
			'' as pelayanan_kode,
			'' as pelayanan_nama, 
			null as asuransi_nama,
			null as asuransi_no,
			null as tgl_TAT,
			null as tgl_TM,
			'KELAS '||CASE WHEN hsb.kelas_rawat = '3' THEN 'III' WHEN hsb.kelas_rawat = '2' THEN 'II' ELSE 'I' END as kelas_rawat_keterangan, 
			hsb.kelas_rawat as kelas_rawat_kode,
			null as informasi_dinsos,
			null as informasi_noSKTM,
			null as informasi_prolanisPRB,
			null as jenis_peserta_keterangan,
			null as jenis_peserta_kode,
			hsb.kd_pasien as pasien_kd_pasien,
			hsb.no_tlp as pasien_no_telp,
			p.nama as pasien_nama,
			p.nik as pasien_nik,
			hsb.no_kartu as pasien_no_kartu,
			hsb.poli_tujuan as polirujukan,
			r.rujukan as prov_perujuk_nama,
			hsb.tgl_masuk as tgl_kunjungan
			FROM
				history_sep_bpjs hsb
				INNER JOIN rujukan r ON r.kd_prov_perujuk = '" . $provider . "' 
				INNER JOIN pasien p ON p.no_asuransi = hsb.no_kartu
				INNER JOIN penyakit pen ON pen.kd_penyakit = hsb.diag_awal 
			WHERE
				no_rujukan = '" . $this->input->post('no_rujukan') . "' 
				AND kd_unit LIKE'1%' 
			ORDER BY
				tgl_masuk DESC";
		// echo $query;die;
		$query = $this->db->query($query);
		if ($query->num_rows() > 0) {
			// $query = $query->result();
			$response['data'] = array(
				'metaData' 	=> array(
					'code' 		=> '200',
					'message' 	=> 'OK'
				),
				'response' 	=> array(
					'rujukan' 	=> array(
						'diagnosa' 	=> array(
							'kode' 	=> $query->row()->kode_diagnosa,
							'nama' 	=> $query->row()->nama_diagnosa
						),
						'keluhan' 		=> "",
						'noKunjungan'	=> $query->row()->no_kunjungan,
						'pelayanan' 	=> array(
							'kode' 	=> "",
							'nama' 	=> ""
						),
						'peserta' 	=> array(
							'cob' 	=> array(
								'nmAsuransi' 	=> "",
								'noAsuransi' 	=> "",
								'tglTAT' 		=> "",
								'tglTMT' 		=> ""
							),
							'hakKelas' 	=> array(
								'keterangan' 	=> $query->row()->kelas_rawat_keterangan,
								'kode' 			=> $query->row()->kelas_rawat_kode
							),
							'informasi' 	=> array(
								'dinsos'      	=> "",
								'noSKTM'      	=> "",
								'prolanisPRB' 	=> ""
							),
							'jenisPeserta' 	=> array(
								'keterangan'  	=> "",
								'kode'  		=> ""
							),
							'mr' 	=> array(
								'noMR'  		=> $query->row()->pasien_kd_pasien,
								'noTelepon'  	=> $query->row()->pasien_no_telp
							),
							'nama' 		=> $query->row()->pasien_nama,
							'nik' 		=> $query->row()->pasien_nik,
							'noKartu' 	=> $query->row()->pasien_no_kartu,
							'pisa' 		=>  '',
							'provUmum' 	=> array(
								'kdProvider' 	=> $provider,
								'nmProvider' 	=> $query->row()->prov_perujuk_nama
							),
							'sex' 	=> '',
							'statusPeserta' => array(
								'keterangan' 	=> "",
								'kode' 			=> ""
							)
						),
						'poliRujukan'	=> array(
							'kode' 	=> "",
							'nama' 	=> ""
						),
						'provPerujuk' 	=> array(
							'kode' 	=> $provider,
							'nama' 	=> $query->row()->prov_perujuk_nama
						),
						'tglKunjungan' => date_format(date_create($query->row()->tgl_kunjungan), 'Y-m-d'),
						'tglSep' => date_format(date_create($query->row()->tgl_kunjungan), 'Y-m-d')
					),
				)
			);
		}

		echo json_encode($response);
		/* $response = array(
			'headers' 	=> "",
			'kd_rs' 	=> "",
			'user' 		=> "",
			'data' 		=> array(),
		);
		// $response['data'] = array( 'metaData' => array( 'code' => '200', 'message' => 'OK', ) );

		$query = $this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'");
		$provider = "";
		if ($query->num_rows() > 0) {
			$provider = $query->row()->nilai;
		}else{
			echo 'success: false, listData: {metaData: {code: "201", message: "Setting PPK tidak ada"}, response: null}}';
			die;
		}

		$query = "SELECT 
			pen.kd_penyakit as kode_diagnosa, 
			pen.penyakit as nama_diagnosa,
			'' as keluhan, 
			hsb.no_sep as no_kunjungan,
			'' as pelayanan_kode,
			'' as pelayanan_nama, 
			null as asuransi_nama,
			null as asuransi_no,
			null as tgl_TAT,
			null as tgl_TM,
			'KELAS '||CASE WHEN hsb.kelas_rawat = '3' THEN 'III' WHEN hsb.kelas_rawat = '2' THEN 'II' ELSE 'I' END as kelas_rawat_keterangan, 
			hsb.kelas_rawat as kelas_rawat_kode,
			null as informasi_dinsos,
			null as informasi_noSKTM,
			null as informasi_prolanisPRB,
			null as jenis_peserta_keterangan,
			null as jenis_peserta_kode,
			hsb.kd_pasien as pasien_kd_pasien,
			hsb.no_tlp as pasien_no_telp,
			p.nama as pasien_nama,
			p.nik as pasien_nik,
			hsb.no_kartu as pasien_no_kartu,
			hsb.poli_tujuan as polirujukan,
			r.rujukan as prov_perujuk_nama,
			hsb.tgl_masuk as tgl_kunjungan
			FROM
				history_sep_bpjs hsb
				INNER JOIN rujukan r ON r.kd_prov_perujuk = '".$provider."' 
				INNER JOIN pasien p ON p.no_asuransi = hsb.no_kartu
				INNER JOIN penyakit pen ON pen.kd_penyakit = hsb.diag_awal 
			WHERE
				no_rujukan = '".$this->input->post('no_rujukan')."' 
				AND kd_unit LIKE'1%' 
			ORDER BY
				tgl_masuk DESC";
				// echo $query;die;
		$query = $this->db->query($query);
		if ($query->num_rows() > 0) {
			// $query = $query->result();
			$response['data'] = array(
				'metaData' 	=> array(
					'code' 		=> '200',
					'message' 	=> 'OK'
				),
				'response' 	=> array(
					'rujukan' 	=> array(
						'diagnosa' 	=> array(
							'kode' 	=> $query->row()->kode_diagnosa,
							'nama' 	=> $query->row()->nama_diagnosa
						),
						'keluhan' 		=> "",
						'noKunjungan'	=> $query->row()->no_kunjungan,
						'pelayanan' 	=> array(
							'kode' 	=> "",
							'nama' 	=> ""
						),
						'peserta' 	=> array(
							'cob' 	=> array(
								'nmAsuransi' 	=> "",
								'noAsuransi' 	=> "",
								'tglTAT' 		=> "",
								'tglTMT' 		=> ""
							),
							'hakKelas' 	=> array(
								'keterangan' 	=> $query->row()->kelas_rawat_keterangan,
								'kode' 			=> $query->row()->kelas_rawat_kode
							),
							'informasi' 	=> array(
								'dinsos'      	=> "",
								'noSKTM'      	=> "",
								'prolanisPRB' 	=> ""
							),
							'jenisPeserta' 	=> array(
								'keterangan'  	=> "",
								'kode'  		=> ""
							),
							'mr' 	=> array(
								'noMR'  		=> $query->row()->pasien_kd_pasien,
								'noTelepon'  	=> $query->row()->pasien_no_telp
							),
							'nama' 		=> $query->row()->pasien_nama,
							'nik' 		=> $query->row()->pasien_nik,
							'noKartu' 	=> $query->row()->pasien_no_kartu,
							'pisa' 		=>  '',
							'provUmum' 	=> array(
								'kdProvider' 	=> $provider,
								'nmProvider' 	=> $query->row()->prov_perujuk_nama
							),
							'sex' 	=> '',
							'statusPeserta' => array(
								'keterangan' 	=> "",
								'kode' 			=> ""
							)
						),
						'poliRujukan'	=> array(
							'kode' 	=> "",
							'nama' 	=> ""
						),
						'provPerujuk' 	=> array(
							'kode' 	=> $provider,
							'nama' 	=> $query->row()->prov_perujuk_nama
						),
						'tglKunjungan'=> date_format(date_create($query->row()->tgl_kunjungan),'Y-m-d'),
						'tglSep'=> date_format(date_create($query->row()->tgl_kunjungan),'Y-m-d')
					),
				)
			);
		}

		echo json_encode($response); */
	}
	public function get_rujukan_history()
	{
		$response = array();
		$response['listData'] 					= array();
		$query = $this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'");
		if ($query->num_rows() > 0) {
			$query = $query->row()->nilai;
		} else {
			echo 'success: false, listData: {metaData: {code: "201", message: "Setting PPK tidak ada"}, response: null}}';
			die;
		}

		$query = "
			SELECT hsb.*,
			r.rujukan,
			r.alamat,
			r.kota,
			p.nama
			FROM
				history_sep_bpjs hsb
				INNER JOIN rujukan r ON kd_prov_perujuk = '" . $query . "'
				INNER JOIN pasien p ON p.no_asuransi = hsb.no_kartu AND p.kd_pasien = hsb.kd_pasien
			WHERE 
				no_kartu = '" . $this->input->post('no_kartu') . "' and 
				kd_unit like '1%'
			ORDER BY
				tgl_masuk DESC;;
		";
		$query = $this->db->query($query);
		if ($query->num_rows() > 0) {
			$mapping = $this->db->query("SELECT * from map_spc_bpjs where kd_spc_bpjs='" . $query->row()->poli_tujuan . "' OR kd_spc='" . $query->row()->poli_tujuan . "'");
			if ($mapping->num_rows() > 0) {
				$mapping->row()->kd_spc;
				$mapping = $this->db->query("SELECT * from spesialisasi where kd_spesial='" . $mapping->row()->kd_spc . "' ");
				if ($mapping->num_rows() > 0) {
					$mapping = strtoupper($mapping->row()->spesialisasi);
				} else {
					$mapping = "";
				}
			} else {
				$mapping = "";
			}


			$listData = array(
				'noKunjungan' 	=> $query->row()->no_rujukan,
				'tglKunjungan' 	=> $query->row()->tgl_masuk,
				'peserta' 		=> array(
					'noKartu' => $query->row()->no_kartu,
					'nama'    => $query->row()->nama,
				),
				'provPerujuk' => array(
					'nama'    => $query->row()->rujukan,
				),
				'poliRujukan' => array(
					'nama'    => $mapping,
				),
			);
			array_push($response['listData'], $listData);
			$response['success']      = true;
			$response['totalrecords'] = $query->num_rows();
		} else {
			$response['success']      = false;
			$response['totalrecords'] = 0;
		}
		echo json_encode($response);
	}

	public function updatePulangSEP()
	{
		$bpjs_kd_res  = $this->db->query("SELECT setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url          = $this->db->query("SELECT nilai  from seting_bpjs where key_setting='UrlUpdateTgl'")->row()->nilai;
		$ppkPelayanan = $this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='PpkPelayanan'")->row()->nilai;
		$no_sep = $_POST['sep'];
		$tgl_pulang = $_POST['tgl_pulang'];
		$status_pulang = $_POST['status_pulang'];
		$tgl_meninggal = $_POST['tgl_meninggal'];
		$surat_meninggal = $_POST['surat_meninggal'];
		$noLPManual = $_POST['noLPManual'];

		// if($cara_keluar != '4'){
		// 	$no_surat_meninggal = "";
		// 	$tgl_meninggal = "";
		// 	$no_lpmanual = "";
		// }
		$user = $this->db->query("SELECT * FROM zusers where kd_user = '" . $this->session->userdata['user_id']['id'] . "'");
		if ($user->num_rows() > 0) {
			$user = $user->row()->full_name;
		} else {
			$user = "";
		}
		$json = '{  
			"request": 
			{    
				"t_sep":
				{
					"noSep": "' . $no_sep . '",
					"statusPulang":"' . $status_pulang . '",
					"noSuratMeninggal":"' . $surat_meninggal . '",
					"tglMeninggal":"' . $tgl_meninggal . '",
					"tglPulang":"' . $tgl_pulang . '",
					"noLPManual":"' . $noLPManual . '",
					"user":"' . $user . '"
				}
			}
		}';

		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		echo json_encode($respon_array);
		$this->insert_response_bpjs($url, $respon_array, $json, "PUT");
	}

	public function updatePulangSEP_Inasis()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlUpdateTgl'")->row()->nilai;
		$headers = $this->getSignature();
		$headers .= 'Content-type: Application/x-www-form-urlencoded\r\n';
		//echo 'awdw';
		$tgl_pulang = $_POST['tgl'];

		$data = '<request>
				<data>
					<t_sep>
						<noSep>' . $_POST['sep'] . '</noSep>
						<tglPlg>' . $tgl_pulang . '</tglPlg>
						<ppkPelayanan>' . $bpjs_kd_res . '</ppkPelayanan>
					</t_sep>
				</data>
			</request>';

		$opts = array(
			'http' => array(
				'method' => "PUT",
				'header' => $headers,
				'content' => $data
			)
		);

		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url, false, $context));
		$data = array();
		$data['data'] = $res->response;
		if ($res->metadata->code == '200') {
			$data['result'] = 'SUCCESS';
		} else {
			$data['result'] = 'ERROR';
			$data['message'] = $this->searchMessage($res);
		}
		echo json_encode($data);
	}

	public function cariHistorySEP()
	{
		$response = array();
				$query = $this->db->query("SELECT DISTINCT(no_sep) as no_sjp FROM history_sep_bpjs WHERE no_kartu = '" . $this->input->post('no_kartu') . "' order by no_sjp DESC");
		$data  = array();
		if ($query->num_rows() > 0) {
			$index = 0;
			foreach ($query->result() as $result) {
				$data[$index]['noSEP'] = $result->no_sjp;
				$index++;
			}
		}
		$response['data'] = array(
			'response' 	=> array(
				'list' 	=> $data,
			)
		);
		echo json_encode($response);

		/* $response = array();
		$query = $this->db->query("
			SELECT k.no_sjp FROM 
			pasien p 
			inner join 
			kunjungan k 
			on k.kd_pasien  = p.kd_pasien where p.no_asuransi = '" . $this->input->post('no_kartu') . "' AND ( k.no_sjp not in('','-')) order by k.tgl_masuk desc");
		$data  = array();
		if ($query->num_rows() > 0) {
			$index = 0;
			foreach ($query->result() as $result) {
				$data[$index]['noSEP'] = $result->no_sjp;
				$index++;
			}
		}
		$response['data'] = array(
			'response' 	=> array(
				'list' 	=> $data,
			)
		);
		echo json_encode($response); */

		// $url= $this->db->query("SELECT nilai  from seting_bpjs where key_setting='UrlHistoryPasien'")->row()->nilai;
		// $no=$_POST['no_kartu'];
		// $opts = array(
		//   'http'=>array(
		// 	'method'=>"GET",
		// 	'header'=>$this->getSignature()
		//   )
		// );
		// $context = stream_context_create($opts);
		// $res=json_decode(file_get_contents($url.$no,false,$context));
		// $dat=array(
		// 	'data'=>null
		// );
		// if($res->metaData->code=='200'){
		// 	$dat['data']=$res;
		// }else{
		// 	$dat['message']=$this->searchMessage($res);
		// }
		// echo json_encode($dat);
	}


	public function cek_order_mng()
	{
		$kd_pasien = $_POST['kd_pasien'];
		$kd_unit_knj = $_POST['kd_unit'];
		$tgl_masuk_knj = $_POST['tgl_masuk_knj'];
		$cek = $this->db->query("select tgl_op,CAST(jam_op as time ) as jam_op,no_kamar,kd_tindakan  from ordr_mng_ok where kd_pasien='$kd_pasien' and kd_unit_knj='$kd_unit_knj' and tgl_masuk_knj='$tgl_masuk_knj'")->result();
		foreach ($cek as $d) {
			$tgl_op = $d->tgl_op;
			$jam_op = $d->jam_op;
			$no_kamar = $d->no_kamar;
			$kd_tindakan = $d->kd_tindakan;
		}
		if (count($cek) > 0) {
			echo "{success:true , tgl_op:" . json_encode($tgl_op) . ", jam_op:" . json_encode($jam_op) . ", no_kamar:" . json_encode($no_kamar) . ", kd_tindakan:" . json_encode($kd_tindakan) . "}";
		} else {
			echo "{success:false, data: null}";
		}
	}
	public function caridata_sepbpjs()
	{
		$bpjs_counter = $this->db->query("select counter_sep  from counter_sep limit 1")->row()->counter_sep;
		$bpjs_counter2 = substr($bpjs_counter, 16, 19);
		$bpjs_counter2 = $bpjs_counter2 + 1;
		if (strlen($bpjs_counter2) == 1) {
			$bpjs_counter = "";
			$no = "00";
			$bpjs_counter = "1311R00112150000" . $no . $bpjs_counter2;
		} else if (strlen($bpjs_counter2) == 2) {
			$bpjs_counter = "";
			$no = "0";
			$bpjs_counter = "1311R00112150000" . $no . $bpjs_counter2;
		} else {
			$bpjs_counter = "";
			$no = "";
			$bpjs_counter = "1311R00112150000" . $no . $bpjs_counter2;
		}

		$bpjs_counter_update = $this->db->query("update counter_sep  set counter_sep='$bpjs_counter' ");

		echo json_encode($bpjs_counter);
	}

	public function printPernyataan()
	{
		$d_nama = '&nbsp;';
		$d_tgl_lahir = '&nbsp;';
		$d_alamat1 = '&nbsp;';
		$d_alamat2 = '&nbsp;';
		$d_telp = '&nbsp;';
		$d_nama2 = '&nbsp;';
		$d_medrec = $this->uri->segment(4, 0);
		$d_jaminan = '&nbsp;';
		$d_peserta = '&nbsp;';
		$d_hubungan = '&nbsp;';

		$q_penanggung_jawab = $this->db->query("SELECT a.nama_pj,a.tgl_lahir,a.alamat,a.kd_pos,a.kota,a.telepon,a.no_hp,a.hubungan,B.kelurahan,c.kecamatan FROM penanggung_jawab A
	 	INNER JOIN kelurahan B ON B.kd_kelurahan=A.kd_kelurahan
	 	INNER JOIN kecamatan C ON C.kd_kecamatan=B.kd_kecamatan WHERE kd_pasien='" . $d_medrec . "' ORDER BY tgl_masuk desc limit 1");
		if (count($q_penanggung_jawab->result()) > 0) {
			$o_penanggung_jawab = $q_penanggung_jawab->row();
			$d_nama = $o_penanggung_jawab->nama_pj;
			$d_tgl_lahir = date('d', strtotime($o_penanggung_jawab->tgl_lahir)) . ' ' . $this->common->getMonthByIndex(date('m', strtotime($o_penanggung_jawab->tgl_lahir)) - 1) . ' ' . date('Y', strtotime($o_penanggung_jawab->tgl_lahir));
			$d_alamat1 = $o_penanggung_jawab->alamat;
			$d_alamat2 = 'Kel. ' . $o_penanggung_jawab->kelurahan . ' Kec. ' . $o_penanggung_jawab->kecamatan . ' Kota ' . $o_penanggung_jawab->kota . ' ' . $o_penanggung_jawab->kd_pos;
			if ($o_penanggung_jawab->telepon != '') {
				$d_telp .= 'Rumah (' . $o_penanggung_jawab->telepon . ')';
			}
			if ($o_penanggung_jawab->telepon != '') {
				if ($d_telp != '') {
					$d_telp .= ', ';
				}
				$d_telp .= 'HP (' . $o_penanggung_jawab->no_hp . ')';
			}
			if ($o_penanggung_jawab->hubungan == 1) {
				$d_hubungan = 'Kep. Keluarga';
			} else if ($o_penanggung_jawab->hubungan == 2) {
				$d_hubungan = 'Suami';
			} else if ($o_penanggung_jawab->hubungan == 3) {
				$d_hubungan = 'Istri';
			} else if ($o_penanggung_jawab->hubungan == 4) {
				$d_hubungan = 'Anak Kandung';
			} else if ($o_penanggung_jawab->hubungan == 5) {
				$d_hubungan = 'Anak Tiri';
			} else if ($o_penanggung_jawab->hubungan == 6) {
				$d_hubungan = 'Anak Angkat';
			} else if ($o_penanggung_jawab->hubungan == 7) {
				$d_hubungan = 'Ayah';
			} else if ($o_penanggung_jawab->hubungan == 8) {
				$d_hubungan = 'Ibu';
			} else if ($o_penanggung_jawab->hubungan == 9) {
				$d_hubungan = 'Mertua';
			} else if ($o_penanggung_jawab->hubungan == 10) {
				$d_hubungan = 'Sdr Kandung';
			} else if ($o_penanggung_jawab->hubungan == 11) {
				$d_hubungan = 'Cucu';
			} else if ($o_penanggung_jawab->hubungan == 12) {
				$d_hubungan = 'Famili';
			} else if ($o_penanggung_jawab->hubungan == 13) {
				$d_hubungan = 'Pembantu';
			} else {
				$d_hubungan = 'Lain-Lain';
			}
		}
		$q_pasien = $this->db->query("SELECT * FROM pasien WHERE kd_pasien='" . $d_medrec . "'");
		if (count($q_pasien->result()) > 0) {
			$o_pasien = $q_pasien->row();
			$d_nama2 = $o_pasien->nama;
			$d_peserta = $o_pasien->no_asuransi;
		}

		$q_kunjungan = $this->db->query("SELECT customer FROM kunjungan A INNER JOIN customer B ON B.kd_customer=A.kd_customer WHERE A.kd_pasien='" . $d_medrec . "' ORDER BY tgl_masuk DESC limit 1");
		if (count($q_kunjungan->result()) > 0) {
			$o_kunjungan = $q_kunjungan->row();
			$d_jaminan = $o_kunjungan->customer;
		}
		$html = '';
		$html .= '<table><tr><th>SURAT PERNYATAAN</center></th></tr></table><br></br><br><div class="normal">';
		$html .= 'Saya yang bertanda-tangan di bawah ini:<br><br>';
		$html .= '<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12;
				}
	 			td, div{
	 				text-align: justify;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: 1px dotted black;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12px;
   				}
           </style>';
		$html .= '<table>
	 				<tr>
	 					<td width="80">Nama</td>
	 					<td width="10">:</td>
	 					<td width="300" class="bottom">' . $d_nama . '</td>
	 					<td width="80">Tgl. Lahir</td>
	 					<td width="10">:</td>
	 					<td class="bottom">' . $d_tgl_lahir . '</td>
	 				</tr>
	 				<tr>
	 					<td width="80">Alamat</td>
	 					<td width="10">:</td>
	 					<td colspan="4" class="bottom">' . $d_alamat1 . '</td>
	 				</tr>
	 				<tr>
	 					<td width="80"></td>
	 					<td width="10"></td>
	 					<td colspan="4" class="bottom">' . $d_alamat2 . '</td>
	 				</tr>
	 				<tr>
	 					<td width="80">No. Telp.HP</td>
	 					<td width="10">:</td>
	 					<td colspan="4" class="bottom">' . $d_telp . '</td>
	 				</tr>
	 			</table><br>
	 			<table><tr><td style="width: auto;white-space: nowrap;">Hubungan dengan pasien, adalah</td><td width="10">:</td><td class="bottom">' . $d_hubungan . '</td></tr></table>
	 			<br>';
		$html .= 'Bahwa Saya melakukan pendaftaran untuk, pasien an:<br><br>';
		$html .= '<table>
	 				<tr>
	 					<td width="80">Nama Pasien</td>
	 					<td width="10">:</td>
	 					<td width="300" class="bottom">' . $d_nama2 . '</td>
	 					<td width="80">No Med.Rec.</td>
	 					<td width="10">:</td>
	 					<td class="bottom">' . $d_medrec . '</td>
	 				</tr>
	 				<tr>
	 					<td width="80">Jaminan</td>
	 					<td width="10">:</td>
	 					<td width="300" class="bottom">' . $d_jaminan . '</td>
	 					<td width="80">No Peserta</td>
	 					<td width="10">:</td>
	 					<td class="bottom">' . $d_peserta . '</td>
	 				</tr>
	 			</table><br>';
		$html .= 'Menyatakan dengan sesungguhnya bahwa saya:<br><br>';
		$html .= '<table>
	 				<tr>
	 					<td width="30" align="center" valign="top">1.</td>
	 					<td>Menyadari dan mengetahui bahwa manfaat asuransi dan penjaminan untuk kesehatan mempunyai batasan yang telah ditentukan</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">2.</td>
	 					<td>Berjanji jika oleh sebab apapun juga, biaya rawat inap tidak sesuai dengan indikasi dan menimbulkan indikasi biaya, maka saya berkewajiban untuk membayar selisih biaya tersebut
	 						kepada Rumah Sakit dengan Uang Muka maupun pembayaran pada saat pulang atau jika selisih biaya tersebut diberitahukan kemudian hari maka saya bersedia menyelesaikan kewajiban tersebut kepada
	 						Rumah Sakit ataupun penjamin</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">3.</td>
	 					<td>Bersedia mematuhi ketentuan penjamin dan jika menurut ketentuan biaya yang terjadi tidak dijamin, maka saya bersedia mendapatkan pelayanan kesehatan dengan status pasien umum
	 						hingga akhir hari rawat dan tidak berubah jaminan di tengah perawatan</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">4.</td>
	 					<td>
	 						<table>
	 							<tr>
	 								<td>Menempati Ruang: </td>
	 								<td width="10">(</td>
	 								<td width="80" class="bottom"></td>
	 								<td width="10">)</td>
	 								<td>Kelas: </td>
	 								<td width="10">(</td>
	 								<td width="80" class="bottom"></td>
	 								<td width="10">)</td>
	 								<td>dg harga saat ini: </td>
	 								<td width="10">(</td>
	 								<td width="10">Rp</td>
	 								<td width="80" class="bottom"></td>
	 								<td width="10">)</td>
	 							</tr>
	 						</table>
	 						Karena :
	 						<table>
	 							<tr>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250">Atas Permintaan Sendiri</td>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250">Kelas sesuai hak pasien, Tidak Tersedia</td>
	 								<td></td>
	 							</tr>
	 							<tr>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250">Kelas sesuai hak pasien, Penuh</td>
	 								<td width="10"><div style="border: 1px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
	 								<td width="10">&nbsp;</td>
	 								<td width="250"><table><tr><td width="10">(</td><td class="bottom"></td><td width="10">)</td></tr></table></td>
	 								<td></td>
	 							</tr>
	 						</table>
	 					</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">5.</td>
	 					<td>Bersedia membayar selisih biaya yang terjadi,</td>
	 				</tr>
	 				<tr>
	 					<td width="30" align="center" valign="top">&nbsp;</td>
	 					<td>
	 						<table>
	 							<tr>
	 								<td width="15">a)</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">&nbsp;</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">b)</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">&nbsp;</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">c)</td>
	 								<td class="bottom"></td>
	 							</tr>
	 							<tr>
	 								<td width="15">&nbsp;</td>
	 								<td class="bottom"></td>
	 							</tr>
	 						</table>
	 					</td>
	 				</tr>
	 			</table><br>';
		$html .= 'Demikian Surat Pernyataan ini saya buat dalam keadaan sadar, tanpa tekanan dan paksaan dan pihak manapun dan dapat mempertanggungjawabkan dengan sebaik-baiknya
	 			dan selanjutnya dapat dipergunakan sebagaimana mestinya.<br><br>';
		$html .= '<table>
	 				<tr>
	 					<td width="10"></td>
	 					<td align="center" width="150">Yang Menyatakan,</td>
	 					<td width="10"></td>
	 					<td></td>
	 					<td width="10"></td>
	 					<td align="center" width="150">Ttd, Petugas Rumah Sakit</td>
	 					<td width="10"></td>
	 				</tr>
	 				<tr>
	 					<td>&nbsp;</td>
	 				</tr>
	 				<tr>
	 					<td>&nbsp;</td>
	 				</tr>
	 				<tr>
	 					<td>&nbsp;</td>
	 				</tr>
	 				<tr>
	 					<td width="10">(</td>
	 					<td align="center" width="150" class="bottom"></td>
	 					<td width="10">)</td>
	 					<td></td>
	 					<td width="10">(</td>
	 					<td align="center" width="150" class="bottom"></td>
	 					<td width="10">)</td>
	 				</tr>
	 			</table>';
		$html .= '</div>';
		$prop = array('foot' => false);
		$this->common->setPdf('P', 'Surat Peryataan', $html, $prop);
	}
	public function printlebarkeluarmasuk()
	{
		$d_medrec = $this->uri->segment(4, 0);
		$this->load->library('m_pdf');
		$this->m_pdf->load();
		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			15, // margin right
			15, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		for ($x = 0; $x < 35; $x++) {
			$SPASI .= '&nbsp;';
			$underline .= '_';
		}
		$html = '';
		$html .= '<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:1px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 12px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';

		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = '<br>Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$telp .= ' ,Fax. ' . $rs->fax . '.';
		}

		$jkx = "";
		$gdarah = "";
		$html .= "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr align=justify>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO.png' height='50'/>
   				</th>
   				<th align=justify >
   					<b>" . $rs->name . "</b><br>
			   		<font style='font-size: 11px;font-family: Arial'><b>" . $rs->address . ", " . $rs->city . "</b></font>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>" . $telp . "</b></font><br>
   				</th>
   			</tr>
   		</table>";
		$html .= "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><tr border='0'><th > Lembar Masuk Dan Keluar</center></th></tr></table>";
		$getkunjungan = $this->db->query("
					
					select kunj.*,kel.kelas, case when jenis_cust=2 then'asuransi' when jenis_cust=1 then 'Langganan' else 'Perorangan'  end as kelpas, dok.nama as namadok from kunjungan kunj inner 
					join  kontraktor kon on kunj.kd_customer=kon.kd_customer  inner join unit on kunj.kd_unit=unit.kd_unit inner join
					kelas kel on unit.kd_kelas=kel.kd_kelas inner join dokter dok on dok.kd_dokter=kunj.kd_dokter where kd_pasien='" . $d_medrec . "'  and left(kunj.kd_unit,1)='1' order by tgl_masuk desc limit 1
							
		")->row();
		$getpenanggungjawab = $this->db->query("SELECT pen.*,pek.pekerjaan,kel.kelurahan,kec.kecamatan,kab.kabupaten from penanggung_jawab pen inner join pekerjaan pek 
		  on pek.kd_pekerjaan=pen.kd_pekerjaan
			inner join kelurahan kel on pen.kd_kelurahan=kel.kd_kelurahan
			inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
			inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
											  where kd_pasien='" . $d_medrec . "' order by tgl_masuk desc limit 1
		
		")->row();

		$GETPASIEN = $this->db->query("SELECT p.kd_pasien,alamat,nama,tempat_lahir,tgl_lahir,nama_keluarga,( extract ( year from age(tgl_lahir))|| ' tahun ' || 
									extract ( month from age(tgl_lahir))|| ' bulan ' ||extract ( day from age(tgl_lahir))|| ' hari '  ) ::text as umur,agama
									,pek.pekerjaan,p.telepon as  telepon,kec.kecamatan,kab.kabupaten,prof.propinsi,jenis_kelamin,pen.pendidikan,kelurahan, gol_darah,status_marita from pasien p inner join agama a on p.kd_agama=a.kd_agama
									inner join pekerjaan pek on pek.kd_pekerjaan=p.kd_pekerjaan inner join Pendidikan pen on pen.kd_pendidikan=p.kd_pendidikan
									inner join kelurahan kel on p.kd_kelurahan=kel.kd_kelurahan
									inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
									inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
									inner join propinsi prof on prof.kd_propinsi=kab.kd_propinsi
									where p.kd_pasien='" . $d_medrec . "'")->row();
		if ($GETPASIEN->jenis_kelamin == 'f') {
			$jkx = 'Perempuan';
		} else if ($GETPASIEN->jenis_kelamin == 't') {
			$jkx = 'laki-laki';
		}
		if ($GETPASIEN->gol_darah == 0) {
			$gdarah = '-';
		} else if ($GETPASIEN->gol_darah == 1) {
			$gdarah = 'A+';
		} else if ($GETPASIEN->gol_darah == 2) {
			$gdarah = 'B+';
		} else if ($GETPASIEN->gol_darah == 3) {
			$gdarah = 'AB+';
		} else if ($GETPASIEN->gol_darah == 4) {
			$gdarah = 'O+';
		} else if ($GETPASIEN->gol_darah == 5) {
			$gdarah = 'A-';
		} else if ($GETPASIEN->gol_darah == 6) {
			$gdarah = 'B-';
		} else if ($GETPASIEN->gol_darah == 7) {
			$gdarah = 'AB-';
		} else if ($GETPASIEN->gol_darah == 8) {
			$gdarah = 'O-';
		}
		$marita = "";
		if ($GETPASIEN->status_marita == 0) {
			$marita = 'Blm Kawin';
		} else if ($GETPASIEN->status_marita == 1) {
			$marita = 'Kawin';
		} else if ($GETPASIEN->status_marita == 2) {
			$marita = 'Janda';
		} else if ($GETPASIEN->status_marita == 3) {
			$marita = 'Duda';
		}
		$kelurahan = "";
		if ($GETPASIEN->kelurahan == "DEFAULT" || $GETPASIEN->kelurahan == "") {
			$kelurahan = '-';
		} else {
			$kelurahan = $GETPASIEN->kelurahan;
		}
		$kecamatan = "";
		if ($GETPASIEN->kecamatan == "DEFAULT" || $GETPASIEN->kecamatan == "") {
			$kecamatan = '-';
		} else {
			$kecamatan = $GETPASIEN->kecamatan;
		}
		$kabupaten = "";
		if ($GETPASIEN->kabupaten == "DEFAULT" || $GETPASIEN->kabupaten == "") {
			$kabupaten = '-';
		} else {
			$kabupaten = $GETPASIEN->kabupaten;
		}
		$propinsi = "";
		if ($GETPASIEN->propinsi == "DEFAULT" || $GETPASIEN->propinsi == "") {
			$propinsi = '-';
		} else {
			$propinsi = $GETPASIEN->propinsi;
		}
		$d_hubungan = "";
		// echo $getpenanggungjawab->hubungan;die;
		if ($getpenanggungjawab->hubungan == 1) {
			$d_hubungan = 'Kep. Keluarga';
		} else if ($getpenanggungjawab->hubungan == 2) {
			$d_hubungan = 'Suami';
		} else if ($getpenanggungjawab->hubungan == 3) {
			$d_hubungan = 'Istri';
		} else if ($getpenanggungjawab->hubungan == 4) {
			$d_hubungan = 'Anak Kandung';
		} else if ($getpenanggungjawab->hubungan == 5) {
			$d_hubungan = 'Anak Tiri';
		} else if ($getpenanggungjawab->hubungan == 6) {
			$d_hubungan = 'Anak Angkat';
		} else if ($getpenanggungjawab->hubungan == 7) {
			$d_hubungan = 'Ayah';
		} else if ($getpenanggungjawab->hubungan == 8) {
			$d_hubungan = 'Ibu';
		} else if ($getpenanggungjawab->hubungan == 9) {
			$d_hubungan = 'Mertua';
		} else if ($getpenanggungjawab->hubungan == 10) {
			$d_hubungan = 'Sdr Kandung';
		} else if ($getpenanggungjawab->hubungan == 11) {
			$d_hubungan = 'Cucu';
		} else if ($getpenanggungjawab->hubungan == 12) {
			$d_hubungan = 'Famili';
		} else if ($getpenanggungjawab->hubungan == 13) {
			$d_hubungan = 'Pembantu';
		} else if ($getpenanggungjawab->hubungan == 14) {
			$d_hubungan = 'Lain-lain';
		} else {
			$d_hubungan = '';
		}
		$pekerjaanpen = "";
		if ($getpenanggungjawab->pekerjaan == "") {
			$pekerjaanpen = "<p>&nbsp;";
		} else {
			$pekerjaanpen = $getpenanggungjawab->pekerjaan;
		}
		$alamatpen = "";
		$nama_pj = "";
		if ($getpenanggungjawab->nama_pj != "" || $getpenanggungjawab->nama_pj != null) {
			$nama_pj = $getpenanggungjawab->nama_pj;
		}
		if ($getpenanggungjawab->alamat == "") {
			$alamatpen = "<p>&nbsp;";
		} else {
			$alamatpen = $getpenanggungjawab->alamat;
		}
		$kelpen = "";
		if ($getpenanggungjawab->kelurahan == "") {
			$kelpen = "<p>&nbsp;";
		} else if ($getpenanggungjawab->kelurahan == "DEFAULT") {
			$kelpen = "-";
		} else {
			$kelpen = $getpenanggungjawab->kelurahan;
		}
		$kecpen = "";
		if ($getpenanggungjawab->kecamatan == "") {
			$kecpen = "<p>&nbsp;";
		} else if ($getpenanggungjawab->kecamatan == "DEFAULT") {
			$kecpen = "-";
		} else {
			$kecpen = $getpenanggungjawab->kecamatan;
		}

		$kabpen = "";
		if ($getpenanggungjawab->kabupaten == "") {
			$kabpen = "<p>&nbsp;";
		} else if ($getpenanggungjawab->kabupaten == "DEFAULT") {
			$kabpen = "-";
		} else {
			$kabpen = $getpenanggungjawab->kabupaten;
		}

		$jammas = "";
		if ($getkunjungan->jam_masuk == "") {
			$jammas = "<p>&nbsp;";
		} else {
			$jammas = date('H:i:s', strtotime($getkunjungan->jam_masuk));
		}
		$tglmas = "";
		if ($getkunjungan->tgl_masuk == "") {
			$tglmas = "<p>&nbsp;";
		} else {
			$tglmas = date('Y-F-d', strtotime($getkunjungan->tgl_masuk));
		}

		$tlppen = "";
		if ($getpenanggungjawab->telepon == "") {
			$tlppen = "<p>&nbsp;";
		} else {
			$tlppen = $getpenanggungjawab->telepon;
		}
		$hppen = "";
		if ($getpenanggungjawab->no_hp == "") {
			$hppen = "<p>&nbsp;";
		} else {
			$hppen = $getpenanggungjawab->no_hp;
		}
		$kelpas = "";
		if ($getkunjungan->kelpas == "") {
			$kelpas = "<p>&nbsp;";
		} else {
			$kelpas = $getkunjungan->kelpas;
		}

		$asuransipas = "";
		if ($GETPASIEN->no_asuransi == "") {
			$asuransipas = "<p>&nbsp;";
		} else {
			$asuransipas = $GETPASIEN->no_asuransi;
		}

		$kelaskamr = "";
		if ($getkunjungan->kelas == "") {
			$kelaskamr = "<p>&nbsp;";
		} else {
			$kelaskamr = $getkunjungan->kelas;
		}
		$namadok = "";
		if ($getkunjungan->namadok == "") {
			$namadok = "<p>&nbsp;";
		} else {
			$namadok = $getkunjungan->namadok;
		}


		$html .= '<table>
	 				<tr>
					<th border:none; align=right>  RM1</th>
					</tr>
	 				
	 			</table>
				<table>
	 				<tr>
	 					<td width="280" ><b>No RM</b> <p>  ' . $GETPASIEN->kd_pasien . '</td>
						<td width="220"><b>Tanggal masuk</b> <p>  ' . $tglmas . '</td>
						<td width="120"><b>Jam</b> <p>  ' . $jammas . '</td>
	 					
	 				</tr>
	 				
	 			</table>
			
				<table>
					<tr>
	 					<td width="390"> <b>Nama</b> <p>  ' . $GETPASIEN->nama . '</td>
						
	 					<td width="230"><b>Nama Keluarga</b> <p> ' . $GETPASIEN->nama_keluarga . '</td>
	 				</tr>
				</table>
				<table>
					<tr>
						<td colspan="3"><b>Tempat tanggal lahir</b> <p>  ' . $GETPASIEN->tempat_lahir . ',&nbsp;' . date('Y-F-d', strtotime($GETPASIEN->tgl_lahir)) . ' </td>
	 					<td colspan="3"><b>Umur</b> <p>   ' . $GETPASIEN->umur . '</td>
						<td colspan="1"><b>Kelamin</b> <p>  ' . $jkx . '</td>
						<td colspan="1"><b>Darah</b> <p> ' . $gdarah . '</td>
					</tr>
				</table>
				<table>
	 				<tr>
						<td width="96,7"><b>Status</b> <p>  ' . $marita . '</td>
						<td width="96,7"><b>Agama</b> <p>   ' . $GETPASIEN->agama . '</td>
	 					<td width="88,7"><b>Pendidikan</b> <p> ' . $GETPASIEN->pendidikan . '</td>
						<td width="88,7"><b>Pekerjaan</b> <p>' . $GETPASIEN->pekerjaan . '  </td>
					</tr>
	 			</table>
				<table>
	 				
	 				<tr>
						<td colspan="8" align= "left"><b>Alamat</b> :' . $GETPASIEN->alamat . '</td>
						</tr>
	 				<tr>
						<td colspan="4" align= "left"><b> Kel :</b> ' . $kelurahan . ' </td>
	 					<td colspan="4" align= "left"><b>Kec :</b> ' . $kecamatan . ' </td>
						
	 				</tr>
					
	 				<tr>
						<td colspan="4" align= "left"><b>Kab/Kota :</b> ' . $kabupaten . '  </td>
						<td colspan="2" align= "left"> <b>Telepon : </b>' . $GETPASIEN->telepon . '</td>
	 					<td colspan="2" align= "left"><b>Hp : </b> ' . $GETPASIEN->handphone . '</td>
		
					</tr>
						<tr>
						<td colspan="2" align= "center"> <b>Penanggung Jawab </b><p>&nbsp;
						' . $nama_pj . ' </td>
	 					<td colspan="2" align= "center"><b>Pekerjaan </b><p>' . $pekerjaanpen . '</td>
						<td colspan="4" align= "center"><b>Alamat </b> <p>' . $alamatpen . '</td>
						</tr>
	 				<tr>
						<td colspan="4" align= "left"><b> Kel :</b> ' . $kelpen . ' </td>
	 					<td colspan="4" align= "left"><b>Kec :</b> ' . $kecpen . '</td>
						
	 				</tr>
					
	 				<tr>
						<td colspan="4" align= "left"><b>Kab/Kota :</b> ' . $kabpen . ' </td>
						<td colspan="2" align= "left"> <b>Telepon : </b>' . $tlppen . ' </td>
	 					<td colspan="2" align= "left"><b>Hp : </b> ' . $hppen . ' </td>
					</tr>
				<tr>
						<td colspan="1"><b>Kelompok Pasien</b><p>' . $kelpas . '<p>&nbsp;<br>&nbsp;</td>
						<td colspan="1"><b>Nomor Peserta</b><p>' . $asuransipas . '<p>&nbsp;<br>&nbsp;</td>
						 <td colspan="2" ><b> Surat Jaminan</b> 
						 <div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Sudah Ada<br>
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Belum Ada</div></td><br>&nbsp;
					    <td colspan="3"><b>Rujukan dari</b><div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Dokter&nbsp;&nbsp;&nbsp;
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Puskesmas </div>
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Bidan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Perawat</div>
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> lain-lain&nbsp;
						  <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Kemauan Sendiri</td>
						  <td colspan="1"><b>Ruang rawat</b><p>' . $kelaskamr . '<p>&nbsp;<br>&nbsp;</td>

				</tr>
				<tr>
						<td colspan="8" align= "left"><b> Dokter Yang menerima :</b> ' . $namadok . ' <p>&nbsp;<p>   </td>
				</tr>
				<tr>
	 					<td colspan="8" align= "left"><b> Dokter Yang merawat :</b><p>&nbsp;<p>    </td>	
	 			</tr>
				<tr>
						<td colspan="8" align= "left"><b> Catatan Keluar :</b><p>&nbsp;<p>  </td>
				</tr>
				<tr>
						<td colspan="7" align= "left"><b> Diagnosa Akhir,Utama :</b> <p>&nbsp;  </td>
						<td colspan="1" align= "left"><b> kode :</b><p>&nbsp; </td>
				</tr>
				<tr>
	 					<td colspan="7" align= "left"><b> diagnosa Tambahan :</b> <p>&nbsp;</td>
						<td colspan="1" align= "left"><b> kode :</b> <p>&nbsp;	</td>	
								
	 			</tr>
				<tr>
						<td colspan="2"><b>Tanggal keluar</b><p>&nbsp;<p>&nbsp;</td>
						 <td colspan="2"><b>Lama Dirawat</b> 
						 <div style="float:left; width:1000px; border:1px">
						 Hari &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <p>
						 Minggu :<p>
						 Bulan &nbsp;&nbsp;&nbsp;:<p></td>
						<td colspan="3"><b>Tindakan Operasi</b><p>&nbsp;<p>&nbsp;<p> Tanggal : ' . $SPASI . ' WIB</td>
						<td colspan="1" align= "left"><b> kode :</b> <p>&nbsp;<p>&nbsp;<p>&nbsp;	</td>
				</tr>
				<tr>
	 					<td colspan="8" align= "center"  border:0px><b> Imunisasi </b> <p>&nbsp;<p>
						1.BGC &nbsp;&nbsp;&nbsp;&nbsp;
						2.DPT &nbsp;&nbsp;&nbsp;&nbsp;
						3.DT &nbsp;&nbsp;&nbsp;&nbsp;
						4.Campak&nbsp;&nbsp;&nbsp;&nbsp;
						5.Polio&nbsp;&nbsp;&nbsp;&nbsp;	
						6.Tetanus Formal Toxoid&nbsp;&nbsp;&nbsp;&nbsp;		
						</td>
				</tr>
				<tr>
						<td colspan="4" ><b>Cara Keluar</b> 
						 <div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Instruksi  Dokter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Kabur<br>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Permintaan dokter &nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> indah Rs Lain</div></td>
						<td colspan="4"><b>Kontrol Ke Poli</b><p>&nbsp;<p>&nbsp;</td>
				</tr>
				<tr>
						<td colspan="4" ><b>Tranfusi Darah</b> 
						 <div style="float:left; width:1000px; border:1px">
						 <span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Ya 
						 &nbsp;&nbsp;&nbsp;&nbsp;<span style="border: 1px solid black; float:left">' . $SPASI . '</span> Cc <br>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span> Tidak <br></div></td>
						<td colspan="4"><b>Tanggal :</b>' . $SPASI . '<b>Jam :<b><p>&nbsp;
				</tr>
					<tr>
	 					<td colspan="8" align= "center"  border:0px><b> Kedaaan Keluar </b> <p>&nbsp;<p>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Sembuh &nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mulai Sembuh &nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Belum Sembuh &nbsp;&nbsp;&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Cacat&nbsp;&nbsp;&nbsp;<br><br>
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati<4 Jam&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati<24 Jam&nbsp;&nbsp;
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati<48 Jam&nbsp;&nbsp;	
						<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span>
						Mati>48 Jam &nbsp;&nbsp;	
						<p>&nbsp;
						<p>&nbsp;							
						</td>
				</tr>
	 			</table>

				<table>
			 		<tr>
						<td valign="top" width="96,7"><b>Petugas Pendaftaran</b> <p><br><br><br>Nama Lengkap</p></td>
						<td valign="top" width="96,7"><b>DPJP Tambahan</b> <p>1....................... <br>3....................... <br>3.......................</p></td>
			 			<td valign="top" width="88,7"><b>Tanda Tangan</b> <p>1....................... <br>3....................... <br>3.......................</p></td>
						<td valign="top" width="88,7"><b>Dokter Penanggung<br>Pelayanan Utama</b> <p><br><br>Nama Lengkap</p></td>
					</tr>
			 	</table>
				<span style="border: 1px solid black; float:left">&nbsp;&nbsp;&nbsp;</span><b> *( Beri Tanda x)</b>
			
	 			';


		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="lembar keluar masuk.pdf"');
		readfile('original.pdf');
	}
	public function save_log_bpjs()
	{
		$bpjs = $this->db->query("insert into log_bpjs(nilai,keterangan) values('" . $_POST['respon'] . "','" . $_POST['keterangan'] . "')");
		if ($bpjs) {
			echo "{success:true}";
		} else {

			echo "{success:false}";
		}
	}
	public function printlabelinap()
	{
		$d_medrec = $this->uri->segment(4, 0);
		$this->load->library('m_pdf');
		$this->m_pdf->load();
		$mpdf = new mPDF('utf-8', array(210, 110));
		$mpdf->AddPage(
			'P', // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			24, // margin_left
			24, // margin right
			3, // margin top
			0, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i");
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		for ($x = 0; $x < 35; $x++) {
			$SPASI .= '&nbsp;';
			$underline .= '_';
		}
		$html = '';
		$html .= '<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:0px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			table{
	   				width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 13px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';

		//$html.="<table style='font-size: 18;font-family: Times New Roman, Helvetica, sans-serif;' cellspacing='0' border='0'><tr border='0'><th > Lembar Masuk Dan Keluar</center></th></tr></table>";
		$getkunjungan = $this->db->query("
					
					select kunj.*,kel.kelas, case when jenis_cust=2 then'asuransi' when jenis_cust=1 then 'Langganan' else 'Perorangan'  end as kelpas, dok.nama as namadok ,cus.customer from kunjungan kunj inner 
					join  kontraktor kon on kunj.kd_customer=kon.kd_customer  inner join unit on kunj.kd_unit=unit.kd_unit inner join
					kelas kel on unit.kd_kelas=kel.kd_kelas inner join dokter dok on dok.kd_dokter=kunj.kd_dokter 
					inner join customer cus on cus.kd_customer=kunj.kd_customer where kd_pasien='" . $d_medrec . "'  and left(kunj.kd_unit,1)='1' order by tgl_masuk desc limit 1
							
		")->row();
		$getpenanggungjawab = $this->db->query("select pen.*,pek.pekerjaan,kel.kelurahan,kec.kecamatan,kab.kabupaten from penanggung_jawab pen inner join pekerjaan pek 
		  on pek.kd_pekerjaan=pen.kd_pekerjaan
			inner join kelurahan kel on pen.kd_kelurahan=kel.kd_kelurahan
			inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
			inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
											  where kd_pasien='" . $d_medrec . "' order by tgl_masuk desc limit 1
		
		")->row();

		$GETPASIEN = $this->db->query("select p.kd_pasien,nama_ayah,nama_ibu,alamat,nama,tempat_lahir,tgl_lahir,nama_keluarga,( extract ( year from age(tgl_lahir))|| ' tahun ' || 
									extract ( month from age(tgl_lahir))|| ' bulan ' ||extract ( day from age(tgl_lahir))|| ' hari '  ) ::text as umur,agama
									,pek.pekerjaan,telepon,kec.kecamatan,kab.kabupaten,prof.propinsi,jenis_kelamin,pen.pendidikan,kelurahan, gol_darah,status_marita from pasien p inner join agama a on p.kd_agama=a.kd_agama
									inner join pekerjaan pek on pek.kd_pekerjaan=p.kd_pekerjaan inner join Pendidikan pen on pen.kd_pendidikan=p.kd_pendidikan
									inner join kelurahan kel on p.kd_kelurahan=kel.kd_kelurahan
									inner join kecamatan kec on kec.kd_kecamatan=kel.kd_kecamatan
									inner join kabupaten kab on kec.kd_kabupaten=kab.kd_kabupaten
									inner join propinsi prof on prof.kd_propinsi=kab.kd_propinsi
									where p.kd_pasien='" . $d_medrec . "'")->row();
		$html .= "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><tr border='0'><th > " . $GETPASIEN->nama . "</center></th></tr></table>";

		$jkx = '';
		if ($GETPASIEN != null && $GETPASIEN->jenis_kelamin == 'f') {
			$jkx = 'Perempuan';
		} else if ($GETPASIEN->jenis_kelamin == 't') {
			$jkx = 'Laki-laki';
		}
		if ($GETPASIEN->gol_darah == 0) {
			$gdarah = '-';
		} else if ($GETPASIEN->gol_darah == 1) {
			$gdarah = 'A+';
		} else if ($GETPASIEN->gol_darah == 2) {
			$gdarah = 'B+';
		} else if ($GETPASIEN->gol_darah == 3) {
			$gdarah = 'AB+';
		} else if ($GETPASIEN->gol_darah == 4) {
			$gdarah = 'O+';
		} else if ($GETPASIEN->gol_darah == 5) {
			$gdarah = 'A-';
		} else if ($GETPASIEN->gol_darah == 6) {
			$gdarah = 'B-';
		} else if ($GETPASIEN->gol_darah == 7) {
			$gdarah = 'AB-';
		} else if ($GETPASIEN->gol_darah == 8) {
			$gdarah = 'O-';
		}
		$marita = "";
		//echo $GETPASIEN->status_marita;
		if ($GETPASIEN->status_marita == 0) {
			$marita = 'Blm Kawin';
		} else if ($GETPASIEN->status_marita == 1) {
			$marita = 'Kawin';
		} else if ($GETPASIEN->status_marita == 2) {
			$marita = 'Janda';
		} else if ($GETPASIEN->status_marita == 3) {
			$marita = 'Duda';
		}
		$kelurahan = "";
		if ($GETPASIEN->kelurahan == "DEFAULT" || $GETPASIEN->kelurahan == "") {
			$kelurahan = '-';
		} else {
			$kelurahan = $GETPASIEN->kelurahan;
		}
		$kecamatan = "";
		if ($GETPASIEN !== null && ($GETPASIEN->kecamatan == "DEFAULT" || $GETPASIEN->kecamatan == "")) {
			$kecamatan = '-';
		} else {
			$kecamatan = $GETPASIEN->kecamatan;
		}
		$kabupaten = "";
		if ($GETPASIEN->kabupaten == "DEFAULT" || $GETPASIEN->kabupaten == "") {
			$kabupaten = '-';
		} else {
			$kabupaten = $GETPASIEN->kabupaten;
		}
		$propinsi = "";
		if ($GETPASIEN->propinsi == "DEFAULT" || $GETPASIEN->propinsi == "") {
			$propinsi = '-';
		} else {
			$propinsi = $GETPASIEN->propinsi;
		}
		$d_hubungan = "";

		$jammas = "";
		if ($getkunjungan->jam_masuk == "") {
			$jammas = "<p>&nbsp;";
		} else {
			$jammas = date('H:i:s', strtotime($getkunjungan->jam_masuk));
		}
		$tglmas = "";
		if ($getkunjungan->tgl_masuk == "") {
			$tglmas = "<p>&nbsp;";
		} else {
			$tglmas = date('Y-F-d', strtotime($getkunjungan->tgl_masuk));
		}

		$kelpas = "";
		if ($getkunjungan->kelpas == "") {
			$kelpas = "<p>&nbsp;";
		} else {
			$kelpas = $getkunjungan->kelpas;
		}

		$asuransipas = "";
		if ($GETPASIEN->no_asuransi == "") {
			$asuransipas = "<p>&nbsp;";
		} else {
			$asuransipas = $GETPASIEN->no_asuransi;
		}
		$kelaskamr = "";
		if ($getkunjungan->kelas == "") {
			$kelaskamr = "<p>&nbsp;";
		} else {
			$kelaskamr = $getkunjungan->kelas;
		}
		$kelaskamr = "";
		if ($getkunjungan->namadok == "") {
			$namadok = "<p>&nbsp;";
		} else {
			$namadok = $getkunjungan->namadok;
		}


		$html .= '<table>
	 				<tr>
					<td border:none; align=center> ' . $jkx . '/&nbsp;' . date('d-F-Y', strtotime($GETPASIEN->tgl_lahir)) . '</td>
					</tr>
	 				
	 			</table>
				<br>
				
				<table style="font-size: 16;font-family: Times New Roman, Helvetica, sans-serif;">
	 				<tr>
					<th border:none;> <b>IDENTITAS</b><t>
					</tr>
	 				
	 			</table>
				<table >
	 				<tr>
	 					<td width="10"></td>
						<td width="10"></td>
						<td width="10"></td>
	 					<td width="10"><b>No medrec</b></td>
	 					<td width="120"><b>: </b>' . $GETPASIEN->kd_pasien . '</td>
	 					
	 				</tr>
	 				<tr>
						
	 					<td><b>1.Nama Lengkap</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="200"><b>' . $GETPASIEN->nama . '</b></td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>2.Tempat & tanggal lahir</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="200">' . $GETPASIEN->Tempat_lahir . ',' . date_format(date_create($GETPASIEN->tgl_lahir), 'Y-m-d') . '</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>3.Jenis Kelamin</b></td>
	 					<td width="10"><b>:<b></td>
	 					<td width="200">' . $jkx . '</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>4.Agama</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="200">' . $GETPASIEN->agama . '</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>5.Pendidikan</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="100">' . $GETPASIEN->pendidikan . '</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>6.Pekerjaan</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="100">' . $GETPASIEN->pekerjaan . '</td>
	 					
	 				</tr>
					<tr>
	 					<td ><b>7.Alamat</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="350">' . $GETPASIEN->alamat . '</td>
	 					
	 				</tr>
							<tr >
	 					<td >&nbsp;&nbsp;&nbsp;<b>Kelurahan</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td>' . $kelurahan . '</td>
	 					<td ><b> Kecamatan</b></td>
	 					<td width="120"><b>:</b>&nbsp;' . $kecamatan . '</td>
	 					
	 				</tr >
						<tr>
	 					<td >&nbsp;&nbsp;&nbsp;<b>Kab/kota</b></td>
	 					<td width="10"><b>:</b></td>
						<td>' . $kabupaten . '</td>
	 					<td ><b> Provinsi</b></td>
	 					<td width="120"><b>:</b>&nbsp;' . $propinsi . '</td>
	 				</tr>
					<tr >
	 					<td width="180"><b>8.Nama Ayah/Nama Ibu</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="90">' . $GETPASIEN->nama_ayah . ',' . $GETPASIEN->nama_ibu . '</td>
	 					<td width="100"><b></b></td>
	 					<td width="10"><b></b></td>
	 					<td ></td>
	 				</tr>
					<tr>
	 					<td width="120"><b>9.Nama Istri/Nama Suami</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="250"></td>
	 				</tr>
					<tr>
	 					<td width="120"><b>10.Status Pasien</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="250">' . $marita . '</td>
	 				</tr>
					<tr>
	 					<td width="120"><b>11.Jenis pembayaran</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="250">' . $getkunjungan->customer . ' (' . $getkunjungan->kelpas . ')</td>
	 				</tr>
					<tr>
	 					<td width="180"><b>12.Nama Penanggung Jawab</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="90">-</td>
	 					<td width="100"><b>Tlpn/HP</b></td>
	 					<td width="10"><b>:</b></td>
	 					<td >' . $GETPASIEN->telepon . '&nbsp;&nbsp;' . $GETPASIEN->handphone . '</td>
	 				</tr>
					<tr>
	 					<td width="180">&nbsp;&nbsp;&nbsp;<b>Tanggal Print </b></td>
	 					<td width="10"><b>:</b></td>
	 					<td width="90"> ' . date('Y-F-d') . '</td>
	 					<td width="10"></td>
	 					<td width="10"></td>
	 					<td ></td>
	 				</tr>
	 			</table>
			<table >
			
			</table>
				<table>
	 				
				</table>
				<br>
	 				
	 			';


		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="lembar keluar masuk.pdf"');
		readfile('original.pdf');
	}
	public function cetakstatuspasienumum()
	{

		$common = $this->common;
		$result = $this->result;
		$title = ' ';
		$param = json_decode($_POST['data']);

		/* $kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		} */

		/* $queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result(); */
		$html = '';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$kdpasien = $param->nomedrec;
		$html .= '
			<table width="100%" border="0">
				<tr>
				  <td align="right">&nbsp;</td>
				</tr>
				<tr>
				  <td align="center"><h1><strong>IRJ UMUM</strong></h1></td>
				</tr>
			  </table>
			  <p></p>';


		$queryPasien = $this->db->query("select kd_pasien,telepon, nama, to_char(tgl_lahir, 'DD-MM-YYYY' ) as tgl_lahir , case when date_part('year',age(Tgl_Lahir))<=5 then
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn '||
		 to_char(date_part('month',age(Tgl_Lahir)), '999')||' bln ' ||
		 to_char(date_part('days',age(Tgl_Lahir)), '999')||' hari'
		 else
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn'
		end
		  as umur, kd_pendidikan,case when jenis_kelamin=  true then 'L' else 'P' end as jk , 
		  ag.agama , alamat , nama_ibu, nama_ayah,  pekerjaan 
		  from pasien 
		  inner join agama ag on pasien.kd_agama= ag.kd_agama 
		  inner join pekerjaan pek on pasien.kd_pekerjaan = pek.kd_pekerjaan
		  where kd_pasien='$kdpasien' ");
		$data_pasien = $queryPasien->row();
		$belumsekolah = '';
		$sd = '';
		$smp = '';
		$smk = '';
		$d3 = '';
		$s1 = '';
		$s3 = '';
		$prof = '';
		$jkl = '';
		$jkp = '';
		if ($data_pasien->kd_pendidikan == 0) {
			$belumsekolah = ' checked="checked"';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 2) {
			$belumsekolah = '';
			$sd = ' checked="checked"';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 3 || $data_pasien->kd_pendidikan == 1) {
			$belumsekolah = '';
			$sd = '';
			$smp = ' checked="checked"';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 5) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = ' checked="checked"';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 6) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = ' checked="checked"';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 7 || $data_pasien->kd_pendidikan == 4) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = ' checked="checked"';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 8) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = ' checked="checked"';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 9) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = ' checked="checked"';
		} else if ($data_pasien->kd_pendidikan == 10) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = ' checked="checked"';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		}

		if ($data_pasien->jk == 'L') {
			$jkl = ' checked="checked"';
			$jkp = '';
		} else {
			$jkl = '';
			$jkp = ' checked="checked"';
		}
		if (count($queryPasien->result()) == 0) {
			$html .= '<table width="100%" border="0">
					<tr>
					  <td width="54%" align="center"><h1>DATA TIDAK ADA</h1>
					  </td>
					</tr>
					</table>';
		} else {
			$html .= '
				  <table width="100%" border="1">
					<tr>
					  <td width="54%"><center>
						&nbsp; <strong>REKAM MEDIS PASIEN RAWAT JALAN</strong>
					  </center></td>
					  <td width="46%"><center>
						&nbsp; <strong>NO. RM : ' . $data_pasien->kd_pasien . '</strong>
					  </center></td>
					</tr>
					<tr>
					  <td height="27" colspan="2"><table width="100%" border="0">
						<tr>
						  <td width="17%">Nama</td>
						  <td width="2%">:</td>
						  <td colspan="2">' . $data_pasien->nama . '</td>
						  <td width="16%"><input type="checkbox" name="chbLaki" id="chbLaki" ' . $jkl . '>
							<label for="chbLaki">Lk </label></td>
						  <td width="18%"><input type="checkbox" name="chbPerempuan" id="chbPerempuan" ' . $jkp . '>
							<label for="chbPerempuan">Pr</label></td>
						</tr>
						<tr>
						  <td>Tgl. lahir / Umur</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->tgl_lahir . ' / ' . $data_pasien->umur . '</td>
						  <td>Agama </td>
						  <td>: ' . $data_pasien->agama . '</td>
						</tr>
						<tr>
						  <td>Pendidikan</td>
						  <td>:</td>
						  <td colspan="4"><input type="checkbox" name="chbBelumSekolah" id="chbBelumSekolah" ' . $belumsekolah . '>
							<label for="chbBelumSekolah">Belum Sekolah
							  <input type="checkbox" name="chbTk" id="chbTk">
							  TK </label>
							<input type="checkbox" name="chbSd" id="chbSd" ' . $sd . '>
							SD
							<label for="chbSd"> </label>
							<input type="checkbox" name="chbSltp" id="chbSltp" ' . $smp . '>
							SLTP
							<label for="chbSltp"> </label>
							<input type="checkbox" name="chbSlta" id="chbSlta" ' . $sma . '  ' . $smk . '>
							SLTA
							<label for="chbSlta"> </label>
							<input type="checkbox" checked name="chbAkademi" id="chbAkademi"  ' . $s1 . '  ' . $d3 . '  ' . $s3 . ' ' . $prof . ' >
							<label for="chbAkademi">PT / Akademi </label></td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->alamat . '</td>
						  <td>No. PHB / Asuransi I </td>
						  <td>:</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>Telp</td>
						  <td>: ' . $data_pasien->telepon . '</td>
						</tr>
						<tr>
						  <td>Nama Orang Tua</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->nama_ayah . ' / ' . $data_pasien->nama_ibu . '</td>
						  <td>Pekerjaan</td>
						  <td>: ' . $data_pasien->pekerjaan . '</td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->alamat . '</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Cara Pembayaran</td>
						  <td>:</td>
						  <td width="24%"><input type="checkbox" name="chbBayarSendiri" id="chbBayarSendiri">
							<label for="chbBayarSendiri">Bayar Sendiri</label></td>
						  <td width="23%"><input type="checkbox" name="chbAsuransi" id="chbAsuransi">
							<label for="chbAsuransi">Asuransi</label></td>
						  <td><input type="checkbox" name="chbKeringanan" id="chbKeringanan">
							Keringanan
							<label for="chbKeringanan"></label></td>
						  <td><input type="checkbox" name="chbGratis" id="chbGratis">
							<label for="chbGratis">Gratis</label></td>
						</tr>
						<tr>
						  <td>Cara Masuk dikirim oleh</td>
						  <td>:</td>
						  <td><input type="checkbox" name="chbDatangSendiri" id="chbDatangSendiri">
							Datang Sendiri
							<label for="chbDatangSendiri"></label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbRsLain" id="chbRsLain">
							RS. Lain
							<label for="chbRsLain"></label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbDokter" id="chbDokter">
							<label for="chbDokter">Dokter</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbKasusPoli" id="chbKasusPoli">
							<label for="chbKasusPoli">Kasus Poli</label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbPuskesmas" id="chbPuskesmas">
							<label for="chbPuskesmas">Puskesmas</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbLainLain" id="chbLainLain">
							<label for="chbLainLain">Lain-lain</label></td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr>
					  <td colspan="2"><table width="100%" border="1">
						<tr>
						  <td width="17%"  style="border-style:none">Riwayat Penyakit dahulu</td>
						  <td width="2%"  style="border-style:none">:</td>
						  <td width="81%"  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit sekarang</td>
						  <td  style="border-style:none">:</td>
						  <td style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit keluarga</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Alergi terhadap</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
					  </table></td>
					</tr>
				  </table>
				  <table width="100%" border="1">
					<tr>
					  <td width="10%" align="center"><strong>Tgl / Jam</strong></td>
					  <td width="37%" align="center"><strong>Anamnesa &amp; Pemeriksaan</strong></td>
					  <td width="25%" align="center"><strong>Diagnose &amp; Terapi</strong></td>
					  <td width="11%" align="center"><strong>Kode ICD</strong></td>
					  <td width="17%" align="center"><strong>Nama Dokter &amp; Paraf</strong></td>
					</tr>
					<tr>
					  <td height="124">&nbsp;</td>
					  <td><p>Anamnesa :</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>Pemeriksaan :</p>
						<p>&nbsp;</p></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				  </table>';
		}

		$prop = array('foot' => true);
		$this->common->setPdf('P', 'IRJ UMUM', $html);
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
	}
	public function cetakstatuspasienanak()
	{

		$common = $this->common;
		$result = $this->result;
		$title = ' ';
		$param = json_decode($_POST['data']);

		/* $kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		} */

		/* $queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result(); */
		$html = '';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$kdpasien = $param->nomedrec;
		$html .= '
			<table width="100%" border="0">
				<tr>
				  <td align="right">&nbsp;RM. RJ. 01. 1b</td>
				</tr>
				<tr>
				  <td align="center"><h1><strong>IRJ ANAK</strong></h1></td>
				</tr>
			  </table>
			  <p></p>';


		$queryPasien = $this->db->query("select kd_pasien,telepon, nama, to_char(tgl_lahir, 'DD-MM-YYYY' ) as tgl_lahir , case when date_part('year',age(Tgl_Lahir))<=5 then
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn '||
		 to_char(date_part('month',age(Tgl_Lahir)), '999')||' bln ' ||
		 to_char(date_part('days',age(Tgl_Lahir)), '999')||' hari'
		 else
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn'
		end
		  as umur, kd_pendidikan,case when jenis_kelamin=  true then 'L' else 'P' end as jk , 
		  ag.agama , alamat , nama_ibu, nama_ayah,  pekerjaan 
		  from pasien 
		  inner join agama ag on pasien.kd_agama= ag.kd_agama 
		  inner join pekerjaan pek on pasien.kd_pekerjaan = pek.kd_pekerjaan
		  where kd_pasien='$kdpasien' ");
		$data_pasien = $queryPasien->row();
		$belumsekolah = '';
		$sd = '';
		$smp = '';
		$smk = '';
		$d3 = '';
		$s1 = '';
		$s3 = '';
		$prof = '';
		$jkl = '';
		$jkp = '';
		if ($data_pasien->kd_pendidikan == 0) {
			$belumsekolah = ' checked="checked"';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 2) {
			$belumsekolah = '';
			$sd = ' checked="checked"';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 3 || $data_pasien->kd_pendidikan == 1) {
			$belumsekolah = '';
			$sd = '';
			$smp = ' checked="checked"';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 5) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = ' checked="checked"';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 6) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = ' checked="checked"';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 7 || $data_pasien->kd_pendidikan == 4) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = ' checked="checked"';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 8) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = ' checked="checked"';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 9) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = ' checked="checked"';
		} else if ($data_pasien->kd_pendidikan == 10) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = ' checked="checked"';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		}

		if ($data_pasien->jk == 'L') {
			$jkl = ' checked="checked"';
			$jkp = '';
		} else {
			$jkl = '';
			$jkp = ' checked="checked"';
		}
		if (count($queryPasien->result()) == 0) {
			$html .= '<table width="100%" border="0">
					<tr>
					  <td width="54%" align="center"><h1>DATA TIDAK ADA</h1>
					  </td>
					</tr>
					</table>';
		} else {
			$html .= '<table width="100%" border="1">
				<tr>
				  <td width="54%"><center>&nbsp; <strong>REKAM MEDIS PASIEN RAWAT JALAN</strong>
				  </center></td>
				  <td width="46%"><center>&nbsp; <strong>NO. RM : ' . $data_pasien->kd_pasien . '</strong> </center></td>
				</tr>
				<tr>
				  <td height="27" colspan="2"><table width="100%" border="1">
					<tr>
					  <td width="17%" style="border-style:none">Nama</td>
					  <td width="2%" style="border-style:none">:</td>
					  <td colspan="2" style="border-style:none">' . $data_pasien->nama . '</td>
					  <td width="16%" style="border-style:none"><input type="checkbox" name="chbLaki" id="chbLaki" ' . $jkl . '>
					  <label for="chbLaki">Lk </label></td>
					  <td width="18%" style="border-style:none"><input type="checkbox" name="chbPerempuan" id="chbPerempuan" ' . $jkp . '>
					  <label for="chbPerempuan">Pr</label></td>
					</tr>
					<tr>
					  <td style="border-style:none">Tgl. lahir / Umur</td>
					  <td style="border-style:none">:</td>
					  <td colspan="2" style="border-style:none">' . $data_pasien->tgl_lahir . ' / ' . $data_pasien->umur . '</td>
					  <td style="border-style:none">Agama </td>
					  <td style="border-style:none">: ' . $data_pasien->agama . '</td>
					</tr>
					<tr>
					  <td style="border-style:none">Pendidikan</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="4"><input type="checkbox" name="chbBelumSekolah" id="chbBelumSekolah" ' . $belumsekolah . '>
					  <label for="chbBelumSekolah">Belum Sekolah 
						<input type="checkbox" name="chbTk" id="chbTk" >
					  TK </label>
					  <input type="checkbox" name="chbSd" id="chbSd"  ' . $sd . '>
					  SD
					  <label for="chbSd"> </label>
					  <input type="checkbox" name="chbSltp" id="chbSltp"  ' . $smp . '>
					  SLTP
					  <label for="chbSltp"> </label>
					  <input type="checkbox" name="chbSlta" id="chbSlta"  ' . $sma . '  ' . $smk . '>
					  SLTA
					  <label for="chbSlta"> </label>
					  <input type="checkbox" name="chbAkademi" id="chbAkademi"  ' . $s1 . '  ' . $d3 . '  ' . $s3 . ' ' . $prof . '>
					  <label for="chbAkademi">PT / Akademi </label></td>
					</tr>
					<tr>
					  <td style="border-style:none">Alamat</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="2" rowspan="2">' . $data_pasien->alamat . '</td>
					  <td style="border-style:none">No. PHB / Asuransi I </td>
					  <td style="border-style:none">:</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">Telp</td>
					  <td style="border-style:none">: ' . $data_pasien->telepon . '</td>
					</tr>
					<tr>
					  <td style="border-style:none">Nama Orang Tua</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="2">' . $data_pasien->nama_ayah . ' / ' . $data_pasien->nama_ibu . '</td>
					  <td style="border-style:none">Pekerjaan</td>
					  <td style="border-style:none">: ' . $data_pasien->pekerjaan . '</td>
					</tr>
					<tr>
					  <td style="border-style:none">Alamat</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" colspan="2" rowspan="2">' . $data_pasien->alamat . '</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">Cara Pembayaran</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none" width="24%"><input type="checkbox" name="chbBayarSendiri" id="chbBayarSendiri">
					  <label for="chbBayarSendiri">Bayar Sendiri</label></td>
					  <td style="border-style:none" width="23%"><input type="checkbox" name="chbAsuransi" id="chbAsuransi">
					  <label for="chbAsuransi">Asuransi</label></td>
					  <td style="border-style:none"><input type="checkbox" name="chbKeringanan" id="chbKeringanan">
					  Keringanan
						<label for="chbKeringanan"></label></td>
					  <td style="border-style:none"><input type="checkbox" name="chbGratis" id="chbGratis">
					  <label for="chbGratis">Gratis</label></td>
					</tr>
					<tr>
					  <td style="border-style:none">Cara Masuk dikirim oleh</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none"><input type="checkbox" name="chbDatangSendiri" id="chbDatangSendiri">
					  Datang Sendiri
						<label for="chbDatangSendiri"></label></td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbRsLain" id="chbRsLain">
					  RS. Lain
						<label for="chbRsLain"></label></td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbDokter" id="chbDokter">
					  <label for="chbDokter">Dokter</label></td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbKasusPoli" id="chbKasusPoli">
					  <label for="chbKasusPoli">Kasus Poli</label></td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
					<tr>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbPuskesmas" id="chbPuskesmas">
					  <label for="chbPuskesmas">Puskesmas</label></td>
					  <td style="border-style:none">&nbsp;</td>
					  <td style="border-style:none"><input type="checkbox" name="chbLainLain" id="chbLainLain">
					  <label for="chbLainLain">Lain-lain</label></td>
					  <td style="border-style:none">&nbsp;</td>
					</tr>
				  </table></td>
				</tr>
				<tr>
				  <td colspan="2"><table width="100%" border="1">
					<tr>
					  <td width="17%" style="border-style:none">Riwayat Penyakit dahulu</td>
					  <td width="2%" style="border-style:none">:</td>
					  <td width="81%" style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
					<tr>
					  <td style="border-style:none">Riwayat Penyakit sekarang</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
					<tr>
					  <td style="border-style:none">Riwayat Penyakit keluarga</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
					<tr>
					  <td style="border-style:none">Alergi terhadap</td>
					  <td style="border-style:none">:</td>
					  <td style="border-style:none">..............................................................................................................................................................................................</td>
					</tr>
				  </table></td>
				</tr>
				<tr>
				  <td colspan="2"><table width="100%" border="1">
					<tr>
					  <td width="54%" style="border-style:none"><strong>STATUS VAKSINASI</strong></td>
					  <td width="46%" style="border-style:none"><strong>STATUS GIZI</strong></td>
					</tr>
					<tr>
					  <td style="border-style:none"><table width="100%" border="1">
						<tr>
						  <td width="19%" rowspan="2" align="center">Jenis Imunisasi</td>
						  <td colspan="3" align="center">Dasar</td>
						  <td colspan="3" align="center">Booster</td>
						</tr>
						<tr>
						  <td width="6%" align="center">I</td>
						  <td width="6%" align="center">II</td>
						  <td width="6%" align="center">III</td>
						  <td width="6%" align="center">1 th</td>
						  <td width="6%" align="center">6 th</td>
						  <td width="6%" align="center">12 th</td>
						</tr>
						<tr>
						  <td>BGC</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>DPT</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Polio</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>DT</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Campak</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Lain : ...............</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ...............</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ...............</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					  <td style="border-style:none"><table width="100%" border="0">
						<tr>
						  <td width="33%">- Lingkaran lengan</td>
						  <td width="3%">:</td>
						  <td width="64%">.........................................</td>
						</tr>
						<tr>
						  <td>- Berat badan</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>- Harvard standar</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td colspan="3">Pembelian makanan / minuman :</td>
						</tr>
						<tr>
						  <td>- ASI diberikan sejak</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td align="right">Sampai &nbsp;&nbsp;</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>- PASI diberikan sejak</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td align="right">Sampai &nbsp;&nbsp;</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
						<tr>
						  <td>- Lain-lain</td>
						  <td>:</td>
						  <td>.........................................</td>
						</tr>
					  </table></td>
					</tr>
				  </table></td>
				</tr>
			  </table>
			  <table width="100%" border="1">
				<tr>
				  <td width="10%" align="center"><strong>Tgl / Jam</strong></td>
				  <td width="37%" align="center"><strong>Anamnesa &amp; Pemeriksaan</strong></td>
				  <td width="25%" align="center"><strong>Diagnose &amp; Terapi</strong></td>
				  <td width="11%" align="center"><strong>Kode ICD</strong></td>
				  <td width="17%" align="center"><strong>Nama Dokter &amp; Paraf</strong></td>
				</tr>
				<tr>
				  <td height="124">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
			  </table>';
		}
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'IRJ ANAK', $html);
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
	}
	public function cetakstatuspasienmata()
	{

		$common = $this->common;
		$result = $this->result;
		$title = ' ';
		$param = json_decode($_POST['data']);

		/* $kdgol=$param->gol;
		if($kdgol != '0'){
			if($kdgol == '4' || $kdgol == '5'){
				$criteria="AND LEFT(IK.KD_INV,1)in('4','5')";
			} else{
				$criteria="AND LEFT(IK.KD_INV,1)='".$kdgol."'";
			}
			
		} else{
			$criteria="";
		} */

		/* $queryHead = $this->db->query( "  SELECT distinct( CASE LEFT(ik.kd_inv,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
																		  WHEN '2' THEN 'BARANG BERGERAK' 
																		  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'  END) as gol, LEFT(Ik.kd_inv,1) as kd
											FROM  inv_master_brg imb 
												INNER JOIN inv_satuan isa  ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kode ik  ON imb.kd_inv = ik.kd_inv  
											WHERE LEFT(ik.kd_inv,1)<>'8'  
											".$criteria."
											ORDER BY   gol
										");
		$query = $queryHead->result(); */
		$html = '';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$kdpasien = $param->nomedrec;
		$html .= '
			<table width="100%" border="0">
				<tr>
				  <td align="right">&nbsp;RM. RJ. 01. 1d</td>
				</tr>
				<tr>
				  <td align="center"><h1><strong>IRJ MATA</strong></h1></td>
				</tr>
			  </table>
			  <p></p>';


		$queryPasien = $this->db->query("select kd_pasien,telepon, nama, to_char(tgl_lahir, 'DD-MM-YYYY' ) as tgl_lahir , case when date_part('year',age(Tgl_Lahir))<=5 then
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn '||
		 to_char(date_part('month',age(Tgl_Lahir)), '999')||' bln ' ||
		 to_char(date_part('days',age(Tgl_Lahir)), '999')||' hari'
		 else
		 to_char(date_part('year',age(Tgl_Lahir)), '999')||' thn'
		end
		  as umur, kd_pendidikan,case when jenis_kelamin=  true then 'L' else 'P' end as jk , 
		  ag.agama , alamat , nama_ibu, nama_ayah,  pekerjaan 
		  from pasien 
		  inner join agama ag on pasien.kd_agama= ag.kd_agama 
		  inner join pekerjaan pek on pasien.kd_pekerjaan = pek.kd_pekerjaan
		  where kd_pasien='$kdpasien' ");
		$data_pasien = $queryPasien->row();
		$belumsekolah = '';
		$sd = '';
		$smp = '';
		$smk = '';
		$d3 = '';
		$s1 = '';
		$s3 = '';
		$prof = '';
		$jkl = '';
		$jkp = '';
		if ($data_pasien->kd_pendidikan == 0) {
			$belumsekolah = ' checked="checked"';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 2) {
			$belumsekolah = '';
			$sd = ' checked="checked"';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 3 || $data_pasien->kd_pendidikan == 1) {
			$belumsekolah = '';
			$sd = '';
			$smp = ' checked="checked"';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 5) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = ' checked="checked"';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 6) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = ' checked="checked"';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 7 || $data_pasien->kd_pendidikan == 4) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = ' checked="checked"';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 8) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = ' checked="checked"';
			$prof = '';
		} else if ($data_pasien->kd_pendidikan == 9) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = '';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = ' checked="checked"';
		} else if ($data_pasien->kd_pendidikan == 10) {
			$belumsekolah = '';
			$sd = '';
			$smp = '';
			$sma = '';
			$smk = ' checked="checked"';
			$d3 = '';
			$s1 = '';
			$s3 = '';
			$prof = '';
		}

		if ($data_pasien->jk == 'L') {
			$jkl = ' checked="checked"';
			$jkp = '';
		} else {
			$jkl = '';
			$jkp = ' checked="checked"';
		}
		if (count($queryPasien->result()) == 0) {
			$html .= '<table width="100%" border="0">
					<tr>
					  <td width="54%" align="center"><h1>DATA TIDAK ADA</h1>
					  </td>
					</tr>
					</table>';
		} else {
			$html .= ' 
				  <table width="100%" border="1">
					<tr>
					  <td width="54%"><center>
						&nbsp; <strong>REKAM MEDIS PASIEN RAWAT JALAN</strong>
					  </center></td>
					  <td width="46%"><center>
						&nbsp; <strong>NO. RM : ' . $data_pasien->kd_pasien . '</strong>
					  </center></td>
					</tr>
					<tr>
					  <td height="27" colspan="2"><table width="100%" border="0">
						<tr>
						  <td width="17%">Nama</td>
						  <td width="2%">:</td>
						  <td colspan="2">' . $data_pasien->nama . '</td>
						  <td width="16%"><input type="checkbox" name="chbLaki" id="chbLaki" ' . $jkl . '>
							<label for="chbLaki">Lk </label></td>
						  <td width="18%"><input type="checkbox" name="chbPerempuan" id="chbPerempuan" ' . $jkp . '>
							<label for="chbPerempuan">Pr</label></td>
						</tr>
						<tr>
						  <td>Tgl. lahir / Umur</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->tgl_lahir . ' / ' . $data_pasien->umur . '</td>
						  <td>Agama </td>
						  <td>: ' . $data_pasien->agama . '</td>
						</tr>
						<tr>
						  <td>Pendidikan</td>
						  <td>:</td>
						  <td colspan="4"><input type="checkbox" name="chbBelumSekolah" id="chbBelumSekolah" ' . $belumsekolah . '>
							<label for="chbBelumSekolah">Belum Sekolah
							  <input type="checkbox" name="chbTk" id="chbTk">
							  TK </label>
							<input type="checkbox" name="chbSd" id="chbSd" ' . $sd . '>
							SD
							<label for="chbSd"> </label>
							<input type="checkbox" name="chbSltp" id="chbSltp" ' . $smp . '>
							SLTP
							<label for="chbSltp"> </label>
							<input type="checkbox" name="chbSlta" id="chbSlta" ' . $sma . '  ' . $smk . '>
							SLTA
							<label for="chbSlta"> </label>
							<input type="checkbox" checked name="chbAkademi" id="chbAkademi"  ' . $s1 . '  ' . $d3 . '  ' . $s3 . ' ' . $prof . ' >
							<label for="chbAkademi">PT / Akademi </label></td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->alamat . '</td>
						  <td>No. PHB / Asuransi I </td>
						  <td>:</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>Telp</td>
						  <td>: ' . $data_pasien->telepon . '</td>
						</tr>
						<tr>
						  <td>Nama Orang Tua</td>
						  <td>:</td>
						  <td colspan="2">' . $data_pasien->nama_ayah . ' / ' . $data_pasien->nama_ibu . '</td>
						  <td>Pekerjaan</td>
						  <td>: ' . $data_pasien->pekerjaan . '</td>
						</tr>
						<tr>
						  <td>Alamat</td>
						  <td>:</td>
						  <td colspan="2" rowspan="2"> ' . $data_pasien->alamat . '</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>Cara Pembayaran</td>
						  <td>:</td>
						  <td width="24%"><input type="checkbox" name="chbBayarSendiri" id="chbBayarSendiri">
							<label for="chbBayarSendiri">Bayar Sendiri</label></td>
						  <td width="23%"><input type="checkbox" name="chbAsuransi" id="chbAsuransi">
							<label for="chbAsuransi">Asuransi</label></td>
						  <td><input type="checkbox" name="chbKeringanan" id="chbKeringanan">
							Keringanan
							<label for="chbKeringanan"></label></td>
						  <td><input type="checkbox" name="chbGratis" id="chbGratis">
							<label for="chbGratis">Gratis</label></td>
						</tr>
						<tr>
						  <td>Cara Masuk dikirim oleh</td>
						  <td>:</td>
						  <td><input type="checkbox" name="chbDatangSendiri" id="chbDatangSendiri">
							Datang Sendiri
							<label for="chbDatangSendiri"></label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbRsLain" id="chbRsLain">
							RS. Lain
							<label for="chbRsLain"></label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbDokter" id="chbDokter">
							<label for="chbDokter">Dokter</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbKasusPoli" id="chbKasusPoli">
							<label for="chbKasusPoli">Kasus Poli</label></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbPuskesmas" id="chbPuskesmas">
							<label for="chbPuskesmas">Puskesmas</label></td>
						  <td>&nbsp;</td>
						  <td><input type="checkbox" name="chbLainLain" id="chbLainLain">
							<label for="chbLainLain">Lain-lain</label></td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
					</tr>
					<tr>
					  <td colspan="2"><table width="100%" border="1">
						<tr>
						  <td width="17%"  style="border-style:none">Riwayat Penyakit dahulu</td>
						  <td width="2%"  style="border-style:none">:</td>
						  <td width="81%"  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit sekarang</td>
						  <td  style="border-style:none">:</td>
						  <td style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Riwayat Penyakit keluarga</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
						<tr>
						  <td  style="border-style:none">Alergi terhadap</td>
						  <td  style="border-style:none">:</td>
						  <td  style="border-style:none">..............................................................................................................................................................................................</td>
						</tr>
					  </table></td>
					</tr>
					<tr>
      <td colspan="2"><table width="100%" border="1">
        <tr>
          <td width="44%" height="132"><table width="100%" border="0">
            <tr>
              <td width="6%">O.D.</td>
              <td width="4%">:</td>
              <td width="86%">________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>________________________________________</td>
            </tr>
          </table></td>
          <td width="15%">&nbsp;&nbsp;&nbsp;Visus<br>
            &nbsp;&nbsp;&nbsp;Refacto<br>
            &nbsp;&nbsp;&nbsp;Tensio oculi<br>
            &nbsp;&nbsp;&nbsp;Perseptio Coloris<br>
            &nbsp;&nbsp; Projectio illuminis<br>
            &nbsp;&nbsp;&nbsp;Kacamata lama </td>
          <td width="41%"><table width="100%" border="1">
            <tr>
              <td width="10%"  style="border-style:none">O.S.</td>
              <td width="4%"  style="border-style:none">:</td>
              <td width="86%"  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
            <tr>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">&nbsp;</td>
              <td  style="border-style:none">________________________________________</td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td height="97" colspan="2"><table width="100%" border="1" height="100%">
        <tr>
          <td width="20%" rowspan="2"  style="border-style:none">O.D.</td>
          <td height="26" align="center"  style="border-style:none">DIAGNOSIS ANATOMICA</td>
          <td width="18%" rowspan="2" align="right"  style="border-style:none">O.S.</td>
        </tr>
        <tr>
          <td width="62%" align="center"  style="border-style:none"><img src="./ui/images/Logo/Mata.png" width="344" height="85"  alt=""/></td>
          </tr>
      </table></td>
    </tr>
  </table>
				  <table width="100%" border="1">
					<tr>
					  <td width="10%" align="center"><strong>Tgl / Jam</strong></td>
					  <td width="37%" align="center"><strong>Anamnesa &amp; Pemeriksaan</strong></td>
					  <td width="25%" align="center"><strong>Diagnose &amp; Terapi</strong></td>
					  <td width="11%" align="center"><strong>Kode ICD</strong></td>
					  <td width="17%" align="center"><strong>Nama Dokter &amp; Paraf</strong></td>
					</tr>
					<tr>
					  <td height="124">&nbsp;</td>
					  <td><p>Anamnesa :</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>Pemeriksaan :</p>
						<p>&nbsp;</p></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				  </table>';
		}
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'IRJ MATA', $html);
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
	}

	// private function getSignature(){
	// 	$tmp_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
	// 	$tmp_costumerID= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
	// 	/*date_default_timezone_set('UTC');
	// 	$tStamp = time();//strval(time()-strtotime('1970-01-01 00:00:00'));
	// 	$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
	// 	$encodedSignature = base64_encode($signature);
	// 	$headers="X-cons-id:".$data."\r\n" .
	// 			  "Accept:JSON\r\n".
	// 			  "Content-Type:application/json\r\n".
	// 			  "X-timestamp:".$tStamp."\r\n".
	// 			  "X-signature:".$encodedSignature."\r\n";
	// 	return $headers;*/

	// 	$data      = $tmp_costumerID;
	// 	$secretKey = $tmp_secretKey;
	// 	// Computes the timestamp
	// 	date_default_timezone_set('UTC');
	// 	$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
	// 	// Computes the signature by hashing the salt with the secret key as the key
	// 	$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);

	// 	// base64 encode…
	// 	$encodedSignature = base64_encode($signature);

	// 	// urlencode…
	// 	// $encodedSignature = urlencode($encodedSignature);

	// 	$header 	= "Accept:JSON\n";
	// 	$header 	.= "Content-Type:application/json\n";
	// 	$header 	.= "X-cons-id: " .$data ."\n";
	// 	$header 	.= "X-timestamp:" .$tStamp ."\n";
	// 	$header 	.=  "X-signature: " .$encodedSignature."\n";
	// 	return $header;
	// }

	private function getSignature()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}

	private function getSignatureVedika()
	{
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		/*date_default_timezone_set('UTC');
		$tStamp = time();//strval(time()-strtotime('1970-01-01 00:00:00'));
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		$encodedSignature = base64_encode($signature);
		$headers="X-cons-id:".$data."\r\n" .
				  "Accept:JSON\r\n".
				  "Content-Type:application/json\r\n".
				  "X-timestamp:".$tStamp."\r\n".
				  "X-signature:".$encodedSignature."\r\n";
		return $headers;*/

		$data      = $tmp_costumerID;
		$secretKey = $tmp_secretKey;
		// Computes the timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
		// Computes the signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $data . "&" . $tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		// urlencode…
		// $encodedSignature = urlencode($encodedSignature);

		// $response = array();
		// $response['Accept']       = "JSON";
		// $response['Content-Type'] = "application/json";
		// $response['X-cons-id']    = $data;
		// $response['X-timestamp']  = $tStamp;
		// $response['X-signature']  = $encodedSignature;
		// 
		$response = [
			'X-cons-id:' . $data,
			'X-timestamp:' . $tStamp,
			'X-signature:' . $encodedSignature
			// 'Content-Type: application/json'
		];
		return $response;
	}
	public function cetaksep_inasis()
	{
		$this->load->library('m_pdf');
		$uri = $_SERVER["REQUEST_URI"];
		$uriArray = explode('/', $uri);
		$param1 = $uriArray[(count($uriArray) - 2)];
		$catatantambahan = str_replace('~~_~~', '/', $uriArray[(count($uriArray) - 1)]);
		$catatantambahanasli = str_replace('%20', ' ', $catatantambahan);
		$html = '';
		$style = '<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
				} 
				
					
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
				@media print{
					.no-print, .no-print *{
						display: none !important;
					}
				}
           </style>';
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		//echo $kd_rs;
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		//<script>
		//	window.print();
		//	</script>
		$html .= "
		<html>
		<head>
			" . $style . "
			
		</head>
		<body style='padding-top:100px;'>
		<table style='margin: 50px 50px 0px 50px;' cellspacing='0' border='0'>
   			<tr align=justify>
   				<th border='0';  width='10%' align='center'>
   					<img src='" . base_url() . "ui/images/Logo/RSSM.png' width='50' height='35'/>
   				</th>
   				<th align='center'  >
					<font style='font-size: 12px;font-family: Calibri;line-height:90%;letter-spacing: 2px;'><b>SURAT ELEGIBILITAS PESERTA</b></font><br>
					<font style='font-size: 8px;font-family: Calibri;line-height:90%;letter-spacing: 2px;'>" . strtoupper($rs->name) . "</font><br>
   				</th>
				<th border='0' width='30%' align='center'>
   					<img src='" . base_url() . "ui/images/Logo/bpjs.jpg' width='170' height='20'/>
   				</th>
   			</tr>
   		</table>
		<br/>";
		//<img src='".base_url()."ui/images/Logo/RSSM.png' width='60' height='40'/>
		//<input type='button' id='btnPrint' value='Print' class='no-print' style='width:100px' onclick='window.print()' />
		$kd_user = $this->session->userdata['user_id']['id'];
		$sqlUser = "SELECT user_names FROM zusers WHERE kd_user='" . $kd_user . "'";
		$objUser = $this->db->query($sqlUser)->row();
		$operator = '-';
		if ($objUser) {
			$operator = $objUser->user_names;
		}
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $this->getSignature()
			)
		);
		$kelasRawat = '';
		$context = stream_context_create($opts);
		//print_r(file_get_contents($url.$param1,false,$context));die();
		$res = json_decode(file_get_contents($url . $param1, false, $context), false);
		if ($res->metaData->code == '200') {
			if ($res->response->peserta->sex == 'P') {
				$jk = 'WANITA';
			} else {
				$jk = 'PRIA';
			}
			if ($res->response->jnsPelayanan == 'Inap') {
				$kelasRawat = $res->response->klsRawat->nmKelas;
			}
			$nama = $res->response->peserta->nama;
			$peserta = $res->response->peserta->jenisPeserta->nmJenisPeserta;
			$tglsep = tanggalstring($res->response->tglSep);
			$html .= '
				
				<table background="white" cellspacing="0" border="0" style="font-color:#000000;font-size: 9px;font-family:  Calibri; margin: 0px 10px;line-height:90%;letter-spacing: 1px;font-stretch: condensed;">
				<tr style="height: 5px;">
					<td width="100">No. SEP</td>
					<td width="6">:</td>
					<td width="300" style="font-size:12px;"><b>' . $param1 . '</b></td>
					<td width="26">&nbsp;</td>
					<td width="100">No. Mr</td>
					<td width="8">:</td>
					<td width="100">' . $res->response->peserta->noMr . '</td>
				</tr>
				<tr>
					<td width="100">Tgl. SEP</td>
					<td width="6">:</td>
					<td>' . $tglsep . '</td>
					<td width="26"></td>
					<td></td>
					<td width="8"></td>
					<td></td>
				</tr>
				<tr>
					<td width="100">No. Kartu</td>
					<td width="6">:</td>
					<td style="font-size:12px;"><b>' . $res->response->peserta->noKartu . '</b></td>
					<td width="26">&nbsp;</td>
					<td>Peserta</td>
					<td width="8">:</td>
					<td rowspan="2" valign="top">' . $peserta . '</td>
				</tr>
				<tr>
					<td width="100">Nama Peserta</td>
					<td width="6">:</td>
					<td>' . $nama . '</td>
				</tr>
				<tr>
					<td width="100">Tgl. Lahir</td>
					<td width="6">:</td>
					<td>' . tanggalstring($res->response->peserta->tglLahir) . '</td>
					<td width="26"></td>
					<td>COB</td>
					<td width="8">:</td>
					<td></td>
				</tr>
				<tr>
					<td width="100">Jns Kelamin</td>
					<td width="6">:</td>
					<td>' . $jk . '</td>
					<td width="26">&nbsp;</td>
					<td>Jns. Rawat</td>
					<td width="8">:</td>
					<td>' . $res->response->jnsPelayanan . '</td>
					
				</tr>
				<tr>
					<td width="100">Poli Tujuan</td>
					<td width="6">:</td>
					<td>' . $res->response->poliTujuan->nmPoli . '</td>
					<td width="26">&nbsp;</td>
					<td>Kls. Rawat</td>
					<td width="8">:</td>
					<td>' . $kelasRawat . '</td>
				</tr>
				<tr>
					<td width="100">Asal Faskes Tk. 1</td>
					<td width="6">:</td>
					<td>' . $res->response->peserta->provUmum->nmProvider . '</td>
					<td width="26">&nbsp;</td>
					<td>Operator</td>
					<td width="8">:</td>
					<td>' . $operator . '</td>
					
				</tr>
				<tr>
					<td width="100">Diagnosa Awal</td>
					<td width="6">:</td>
					<td>' . $res->response->diagAwal->nmDiag . '</td>
					<td width="26">&nbsp;</td>
					<td></td>
					<td width="8"></td>
					<td></td>
					
				</tr>
				<tr>
					<td width="100">Catatan</td>
					<td width="6">:</td>
					<td>' . $catatantambahanasli . '</td>
					<td width="26">&nbsp;</td>
					<td align="center">Pasien / Keluarga</td>
					<td width="8"></td>
					<td align="center">Petugas BPJS</td>
					
				</tr>
				<tr>
					<td width="100"></td>
					<td width="6"></td>
					<td></td>
					<td width="26">&nbsp;</td>
					<td align="center">Pasien</td>
					<td width="8"></td>
					<td align="center">Kesehatan</td>
				</tr>
				 <tr>
					<td colspan="3";><font style="font-weight:bold;font-size: 7px;font-family: Arial;"><i>*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan</i></td>
					<td width="26"></td>
					<td>&nbsp;</td>
					<td width="8"></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
				   <td colspan="3";><font style="font-weight:bold;font-size: 7px;font-family:Arial;"><i>*SEP bukan sebagai bukti penjamin peserta</td>
				   <td></td>
				  <td>&nbsp;</td>
				   <td></td>
				   <td>&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3";>&nbsp;</td>
				  <td></td>
				  <td>&nbsp;</td>
				  <td></td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3";>Cetakan Ke 1 : ' . gmdate("d-M-Y H:i:s", time() + 60 * 61 * 7) . '</td>
					<td width="26"></td>
					<td  align="left" width="100"><hr width="100"></td>
					<td width="8"></td>
					<td><hr width="100"></td>
				</tr> 
				</table>
			</body>
			</html>
			';
			//echo $html;
			$this->load->library('m_pdf');
			$this->m_pdf->load();
			$mpdf = new mPDF('utf-8', array(200, 60));
			$mpdf->AddPage(
				'P', // L - landscape, P - portrait
				'',
				'',
				'',
				'',
				0, // margin_left
				0, // margin right
				0, // margin top
				0, // margin bottom
				0, // margin header
				0
			); // margin footer
			//$mpdf->SetDisplayMode('fullpage');
			//$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			//$mpdf->pagenumPrefix = 'Hal : ';
			//$mpdf->pagenumSuffix = '';
			//$mpdf->nbpgPrefix = ' Dari ';
			//$mpdf->nbpgSuffix = '';
			//date_default_timezone_set("Asia/Jakarta"); 
			//$date = gmdate("d-M-Y / H:i", time()+60*60*7);
			//$date = date("d-M-Y / H:i");

			//if($this->foot==true){
			//	$mpdf->SetFooter($arr);
			//}
			//$mpdf->SetTitle($title);
			//echo $html; lalalala
			$mpdf->WriteHTML($html);
			//$mpdf->SetJS('this.print();');
			$mpdf->Output($pdfFilePath, "I");
			//$cmd = 'lpr -P Epson-LX-300_NUANSA /var/www/html/medismart/ui/template/headerSEP.pdf';
			// $cmd .=$file;
			//$response = shell_exec($cmd);
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="SEP.pdf"');
			readfile('SEP.pdf');
			//$mpdf->WriteHTML($html);
			//$mpdf->SetJS('this.print();');
			//header ( 'Content-type: application/pdf' );
			//header ( 'Content-Disposition: attachment; filename="SEP.pdf"' );
			//readfile ( 'SEP.pdf' );	
			$kdUser = $this->session->userdata['user_id']['id'];
			$printer = $this->db->query("select p_sep from zusers where kd_user = '$kdUser'")->row()->p_sep;
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
			//$header =  'http://192.168.1.25/medismart_/ui/images/Logo/header-sep.prn';
			$header =  'http://192.168.1.25/medismart/ui/images/Logo/header-sep.prn';
			$handle = fopen($file, 'w');
			$condensed = Chr(27) . Chr(33) . Chr(4);
			$bold1 = Chr(27) . Chr(69);
			$bold0 = Chr(27) . Chr(70);
			$initialized = chr(27) . chr(64);
			$condensed2 = Chr(27) . Chr(33) . Chr(30);
			$condensed1 = chr(15);
			$condensed0 = chr(18);
			$Data  = $initialized;
			$Data .= $condensed1;
			$Data .= "No. Sep             : " . str_pad($condensed2 . $bold1 . $param1 . $bold0 . $condensed, 65, " ") . "\n";
			$Data .= "Tgl. Sep            : " . str_pad($bold1 . $tglsep . $bold0, 69, " ") . "No. Mr         : " . $res->response->peserta->noMr . "\n";
			$Data .= "No. Kartu           : " . str_pad($condensed2 . $bold1 . $res->response->peserta->noKartu . $bold0 . $condensed, 67, " ") . "Peserta        : " . $peserta . "\n";
			$Data .= "Nama Peserta        : " . str_pad($nama, 65, " ") . "\n";
			$Data .= "Tgl. Lahir          : " . str_pad(tanggalstring($res->response->peserta->tglLahir), 65, " ") . "COB            : -\n";
			$Data .= "Jns. Kelamin        : " . str_pad($jk, 65, " ") . "Jns. Rawat     : " . $res->response->jnsPelayanan . "\n";
			$Data .= "Poli Tujuan         : " . str_pad($res->response->poliTujuan->nmPoli, 65, " ") . "Kls. Rawat     : " . $kelasRawat . "\n";
			$Data .= "Asal Faskes TK 1    : " . str_pad($res->response->peserta->provUmum->nmProvider, 65, " ") . "Operator       : " . $operator . "\n";
			$Data .= "Diagnosa Awal       : " . str_pad($res->response->diagAwal->nmDiag, 65, " ") . "\n";
			$Data .= "Catatan             : " . str_pad($catatantambahanasli, 65, " ") . "Pasien / Keluarga      Petugas BPJS \n";
			$Data .= "                                                                                             Pasien            Kesehatan\n";
			$Data .= "*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan\n";
			$Data .= "*SEP bukan sebagai bukti penjamin peserta\n";
			$Data .= "\n";
			$Data .= "Cetakan Ke 1 : " . str_pad(gmdate("d-M-Y H:i:s", time() + 60 * 61 * 7), 71, " ") . "................     .................\n";
			$Data .= "\n";
			fwrite($handle, $Data);
			fclose($handle);
			//$cmd = 'lpr -P //localhost/epson-lx-310-me '.$file;
			//shell_exec("lpr -P ".$printer." -r ".$header);
			shell_exec("lpr -P " . $printer . " /var/www/html/medismart_/ui/images/Logo/header-sep.prn");
			shell_exec("lpr -P " . $printer . " -r " . $file);
		}
	}
	public function cetaksep()
	{
		$setpage 	= new Pilihkertas;
		/*
			============================ BPJS VEDIKA
		 */
		$this->load->library('m_pdf');
		$uri = $_SERVER["REQUEST_URI"];
		$uriArray = explode('/', $uri);
		$param1 = $uriArray[(count($uriArray) - 2)];
		$catatantambahan = str_replace('~~_~~', '/', $uriArray[(count($uriArray) - 1)]);
		$catatantambahanasli = str_replace('%20', ' ', $catatantambahan);
		$html = '';
		$style = '<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
				} 
				
					
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
				@media print{
					.no-print, .no-print *{
						display: none !important;
					}
				}
           </style>';
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		//echo $kd_rs;
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		//<script>
		//	window.print();
		//	</script>
		$html .= "
		<html>
		<head>
			" . $style . "
			
		</head>
		<body style='padding-top:100px;'>
		<table style='margin: 50px 50px 0px 50px;' cellspacing='0' border='0'>
   			<tr align=justify>
   				<th border='0';  width='10%' align='center'>
   					<img src='" . base_url() . "ui/images/Logo/LOGO.png' width='50' height='35'/>
   				</th>
   				<th align='center'  >
					<font style='font-size: 12px;font-family: Calibri;line-height:90%;letter-spacing: 2px;'><b>SURAT ELEGIBILITAS PESERTA</b></font><br>
					<font style='font-size: 8px;font-family: Calibri;line-height:90%;letter-spacing: 2px;'>" . strtoupper($rs->name) . "</font><br>
   				</th>
				<th border='0' width='30%' align='center'>
   					<img src='" . base_url() . "ui/images/Logo/bpjs.jpg' width='170' height='20'/>
   				</th>
   			</tr>
   		</table>
		<br/>";
		//<img src='".base_url()."ui/images/Logo/RSSM.png' width='60' height='40'/>
		//<input type='button' id='btnPrint' value='Print' class='no-print' style='width:100px' onclick='window.print()' />
		$kd_user = $this->session->userdata['user_id']['id'];
		$sqlUser = "SELECT user_names FROM zusers WHERE kd_user='" . $kd_user . "'";
		$objUser = $this->db->query($sqlUser)->row();
		$operator = '-';
		if ($objUser) {
			$operator = $objUser->user_names;
		}
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $this->getSignature_new()
			)
		);
		$kelasRawat = '';
		$no_telepon = "-";
		$context = stream_context_create($opts);
		// print_r(file_get_contents($url.$param1,false,$context));die();
		$res = json_decode(file_get_contents($url . $param1, false, $context), false);



		$this->insert_response_bpjs($url . $param1, $res, null, "GET");
		if ($res->metaData->code == '200') {
			$query_pasien = $this->db->query("SELECT * FROM pasien where kd_pasien = '" . $res->response->peserta->noMr . "'");
			if ($query_pasien->num_rows() > 0) {
				$no_telepon = $query_pasien->row()->telepon;
			}
			if ($res->response->peserta->kelamin == 'P') {
				$jk = 'WANITA';
			} else {
				$jk = 'PRIA';
			}
			if ($res->response->jnsPelayanan == 'Rawat Inap') {
				$kelasRawat = $res->response->peserta->hakKelas;
			}
			$nama = $res->response->peserta->nama;
			$peserta = $res->response->peserta->jnsPeserta;
			$tglsep = tanggalstring($res->response->tglSep);
			$html .= '
				
				<table background="white" cellspacing="0" border="0" style="font-color:#000000;font-size: 9px;font-family:  Calibri; margin: 0px 10px;line-height:90%;letter-spacing: 1px;font-stretch: condensed;">
				<tr style="height: 5px;">
					<td width="100">No. SEP</td>
					<td width="6">:</td>
					<td width="300" style="font-size:12px;"><b>' . $param1 . '</b></td>
					<td width="26">&nbsp;</td>
					<td width="100">No. Mr</td>
					<td width="8">:</td>
					<td width="100">' . $res->response->peserta->noMr . '</td>
				</tr>
				<tr>
					<td width="100">Tgl. SEP</td>
					<td width="6">:</td>
					<td>' . $tglsep . '</td>
					<td width="26"></td>
					<td></td>
					<td width="8"></td>
					<td></td>
				</tr>
				<tr>
					<td width="100">No. Kartu</td>
					<td width="6">:</td>
					<td style="font-size:12px;"><b>' . $res->response->peserta->noKartu . '</b></td>
					<td width="26">&nbsp;</td>
					<td>Peserta</td>
					<td width="8">:</td>
					<td rowspan="2" valign="top">' . $peserta . '</td>
				</tr>
				<tr>
					<td width="100">Nama Peserta</td>
					<td width="6">:</td>
					<td>' . $nama . '</td>
				</tr>
				<tr>
					<td width="100">Tgl. Lahir</td>
					<td width="6">:</td>
					<td>' . tanggalstring($res->response->peserta->tglLahir) . ' Kelamin : ' . $jk . '</td>
					<td width="26"></td>
					<td>COB</td>
					<td width="8">:</td>
					<td></td>
				</tr>
				<tr>
					<td width="100">No Telepon</td>
					<td width="6">:</td>
					<td>' . $no_telepon . '</td>
					<td width="26">&nbsp;</td>
					<td>Jns. Rawat</td>
					<td width="8">:</td>
					<td>' . $res->response->jnsPelayanan . '</td>
					
				</tr>
				<tr>
					<td width="100">Poli Tujuan</td>
					<td width="6">:</td>
					<td>' . $res->response->poli . '</td>
					<td width="26">&nbsp;</td>
					<td>Kls. Rawat</td>
					<td width="8">:</td>
					<td>' . $kelasRawat . '</td>
				</tr>
				<tr>
					<td width="100">Asal Faskes Tk. 1</td>
					<td width="6">:</td>
					<td> - </td>
					<td width="26">&nbsp;</td>
					<td>Operator</td>
					<td width="8">:</td>
					<td>' . $operator . '</td>
				</tr>
				<tr>
					<td width="100">Diagnosa Awal</td>
					<td width="6">:</td>
					<td>' . substr($res->response->diagnosa, 0, 50) . '...</td>
					<td width="26">&nbsp;</td>
					<td></td>
					<td width="8"></td>
					<td></td>
					
				</tr>
				<tr>
					<td width="100">Catatan</td>
					<td width="6">:</td>
					<td>' . $res->response->catatan . '</td>
					<td width="26">&nbsp;</td>
					<td align="center"></td>
					<td colspan="2"  align="center">Pasien / Keluarga Pasien</td>
					
				</tr>
				<tr>
					<td width="100"></td>
					<td width="6"></td>
					<td></td>
					<td width="26">&nbsp;</td>
					<td align="center"></td>
					<td width="8"></td>
					<td align="center"></td>
				</tr>
				 <tr>
					<td colspan="3";><font style="font-weight:bold;font-size: 7px;font-family: Arial;"><i>*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan</i></td>
					<td width="26"></td>
					<td>&nbsp;</td>
					<td width="8"></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
				   <td colspan="3";><font style="font-weight:bold;font-size: 7px;font-family:Arial;"><i>*SEP bukan sebagai bukti penjamin peserta</td>
				   <td></td>
				  <td>&nbsp;</td>
				   <td></td>
				   <td>&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="3";>&nbsp;</td>
				  <td></td>
				  <td>&nbsp;</td>
				  <td></td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3";>Cetakan Ke 1 : ' . gmdate("d-M-Y H:i:s", time() + 60 * 61 * 7) . '</td>
					<td width="26"></td>
					<td  align="left" width="100"></td>
					<td colspan="2" align="center"><hr width="100"></td>
				</tr> 
				</table>
			</body>
			</html>
			';
			//echo $html;
			$this->load->library('m_pdf');
			$this->m_pdf->load();
			$mpdf = new mPDF('utf-8', array(200, 60));
			$mpdf->AddPage(
				'P', // L - landscape, P - portrait
				'',
				'',
				'',
				'',
				0, // margin_left
				0, // margin right
				0, // margin top
				0, // margin bottom
				0, // margin header
				0
			); // margin footer
			//$mpdf->SetDisplayMode('fullpage');
			//$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
			//$mpdf->pagenumPrefix = 'Hal : ';
			//$mpdf->pagenumSuffix = '';
			//$mpdf->nbpgPrefix = ' Dari ';
			//$mpdf->nbpgSuffix = '';
			//date_default_timezone_set("Asia/Jakarta"); 
			//$date = gmdate("d-M-Y / H:i", time()+60*60*7);
			//$date = date("d-M-Y / H:i");

			//readfile ( 'SEP.pdf' );	
			//if($this->foot==true){
			//	$mpdf->SetFooter($arr);
			//}
			//$mpdf->SetTitle($title);
			//echo $html; lalalala
			//$mpdf->SetJS('this.print();');
			//
			//

			$mpdf->WriteHTML($html);
			$mpdf->Output($pdfFilePath, "I");
			//$cmd = 'lpr -P Epson-LX-300_NUANSA /var/www/html/medismart/ui/template/headerSEP.pdf';
			// $cmd .=$file;
			//$response = shell_exec($cmd);
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="SEP.pdf"');
			readfile('SEP.pdf');


			$kdUser  = $this->session->userdata['user_id']['id'];
			$printer = $this->db->query("select p_sep from zusers where kd_user = '$kdUser'")->row()->p_sep;
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
			// $header =  'http://192.168.1.25/medismart_/ui/images/Logo/header-sep.prn';
			$header =  base_url() . 'ui/images/Logo/header-sep.prn';
			$handle = fopen($file, 'w');
			$condensed = Chr(27) . Chr(33) . Chr(4);
			$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
			$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
			$formfeed    = chr(12); # mengeksekusi $feed
			$bold1 = Chr(27) . Chr(69);
			$bold0 = Chr(27) . Chr(70);
			$initialized = chr(27) . chr(64);
			$condensed2 = Chr(27) . Chr(33) . Chr(30);
			$condensed1 = chr(15);
			$condensed0 = chr(18);
			$margin      = chr(27) . chr(78) . chr(90);
			$margin_off  = chr(27) . chr(79);

			$Data  = $initialized;
			$Data .= $setpage->PageLength('laporan/3'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
			// $Data .= $margin;
			$Data .= $condensed1;
			// $Data .= $formfeed;
			$Data .= "No. Sep             : " . str_pad($condensed2 . $bold1 . $param1 . $bold0 . $condensed, 65, " ") . "\n";
			$Data .= "Tgl. Sep            : " . str_pad($bold1 . $tglsep . $bold0, 69, " ") . "No. Mr         : " . $res->response->peserta->noMr . "\n";
			$Data .= "No. Kartu           : " . str_pad($condensed2 . $bold1 . $res->response->peserta->noKartu . $bold0 . $condensed, 67, " ") . "Peserta        : " . $peserta . "\n";
			$Data .= "Nama Peserta        : " . str_pad($nama, 65, " ") . "\n";
			$Data .= "Tgl. Lahir          : " . str_pad(tanggalstring($res->response->peserta->tglLahir), 65, " ") . "COB            : -\n";
			$Data .= "Jns. Kelamin        : " . str_pad($jk, 65, " ") . "Jns. Rawat     : " . $res->response->jnsPelayanan . "\n";
			// $Data .= "Poli Tujuan         : ".str_pad($res->response->poliTujuan->nmPoli,65," ")."Kls. Rawat     : ".$kelasRawat."\n";
			$Data .= "Poli Tujuan         : " . str_pad($res->response->poli, 65, " ") . "Kls. Rawat     : " . $kelasRawat . "\n";
			// $Data .= "Asal Faskes TK 1    : ".str_pad($res->response->peserta->provUmum->nmProvider,65," ")."Operator       : ".$operator."\n";
			$Data .= "Asal Faskes TK 1    : " . str_pad("", 65, " ") . "Operator       : " . $operator . "\n";
			$Data .= "Diagnosa Awal       : " . str_pad($res->response->diagnosa, 65, " ") . "\n";
			// $Data .= "Catatan             : ".str_pad($res->response->catatan ,65," ")."Pasien / Keluarga      Petugas BPJS \n";
			$Data .= "Catatan             : " . str_pad($res->response->catatan, 65, " ") . "                     Pasien / Keluarga pasien\n";
			// $Data .= "                                                                                             Pasien            Kesehatan\n";
			$Data .= "*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan\n";
			$Data .= "*SEP bukan sebagai bukti penjamin peserta\n";
			// $Data .= "Cetakan Ke 1 : ".str_pad(gmdate("d-M-Y H:i:s", time()+60*61*7),71," ")."................     .................\n";
			$Data .= "Cetakan Ke 1 : " . str_pad(gmdate("d-M-Y H:i:s", time() + 60 * 61 * 7), 71, " ") . "                     .................\n";
			$Data .= " \n";
			$Data .= " \n";
			$Data .= " \n";
			fwrite($handle, $Data);
			fclose($handle);
			//$cmd = 'lpr -P //localhost/epson-lx-310-me '.$file;
			//shell_exec("lpr -P ".$printer." -r ".$header);
			//
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				// echo $file;die();
				copy($file, $printer);  # Lakukan cetak
				// unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else {
				shell_exec("lpr -P " . $printer . " /var/www/html/medismart/ui/images/Logo/header-sep.prn");
				shell_exec("lpr -P " . $printer . " " . $file); # Lakukan cetak linux
			}
			//$mpdf->WriteHTML($html);
			//$mpdf->SetJS('this.print();');
			//header ( 'Content-type: application/pdf' );
			//header ( 'Content-Disposition: attachment; filename="SEP.pdf"' );
			// echo "str";die();
		}
	}
	function cetakRWJBatalTransaksi()
	{
		$var = $this->input->post('data');
		$param = explode('#aje#', $var);
		$d_medrec = $this->uri->segment(4, 0);
		$this->load->library('m_pdf');
		$this->m_pdf->load();
		$mpdf = new mPDF('utf-8', 'A4');
		$mpdf->AddPage(
			$type, // L - landscape, P - portrait
			'',
			'',
			'',
			'',
			$marginLeft, // margin_left
			15, // margin right
			15, // margin top
			15, // margin bottom
			0, // margin header
			12
		); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		date_default_timezone_set("Asia/Jakarta");
		$date = gmdate("d/M/Y H:i:s", time() + 60 * 61 * 7);
		$arr = array(
			'odd' => array(
				'L' => array(
					'content' => 'Operator : ' . $name,
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'C' => array(
					'content' => "Tgl/Jam : " . $date . "",
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'R' => array(
					'content' => '{PAGENO}{nbpg}',
					'font-size' => 8,
					'font-style' => '',
					'font-family' => 'serif',
					'color' => '#000000'
				),
				'line' => 0,
			),
			'even' => array()
		);
		for ($x = 0; $x < 35; $x++) {
			$SPASI .= '&nbsp;';
			$underline .= '_';
		}
		$html = '';
		$html .= '<style>
	 			.normal{
					width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:1px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 10px;
				}
	 			table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 12px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';

		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = '<br>Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$telp .= ' ,Fax. ' . $rs->fax . '.';
		}

		$jkx = "";
		$gdarah = "";
		$html .= "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<thead><tr align=justify>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='70' height='50'/>
   				</th>
   				<th align=justify >
   					<b>" . $rs->name . "</b><br>
			   		<font style='font-size: 11px;font-family: Arial'><b>" . $rs->address . ", " . $rs->city . "</b></font>
			   		<font style='font-size: 11px;font-family: Arial, Helvetica'><b>" . $telp . "</b></font><br>
   				</th>
   			</tr></thead>
   		</table>";
		$html .= "<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'><thead><tr border='0'><th > " . $param[2] . "</center></th></tr></thead></table>";
		$html .= '<table>
				<thead>
	 				<tr>
	 					<td width="100" align= "center"><b>Medrec</b></td>
						<td width="250" align= "center"><b>Nama Pasien</b></td>
						<td width="100" align= "center"><b>No transaksi</b></td>
						<td width="100" align= "center"><b>Unit</b></td>
						<td width="60" align= "center"><b>Tanggal batal</b></td>
						<td width="50" align= "center"><b>Shift batal</b></td>
						<td width="50" align= "center"><b>Jam batal</b></td>
						<td width="100" align= "center"><b>Alasan Batal</b></td>
						<td width="100" align= "right"><b>Jumlah</b></td>
	 					<td width="100" align= "center"><b>Petugas </b></td>
	 				</tr>"
				</thead>
					
	 				
';
		/*$query=$this->db->query("select 
  kd_kasir,
  no_transaksi ,
  tgl_transaksi ,
  kd_pasien ,
  nama ,
  kd_unit,
  nama_unit,
  kd_user ,
  kd_user_del ,
  shift,
  shiftdel ,
  jumlah,
  user_name ,
  tgl_batal ,
  ket,jam_batal  from history_batal_trans  where kd_kasir='".$param[3]."' and tgl_batal between '".$param[0]."' and  '".$param[1]."'")->result();*/
		$query = $this->db->query("Select 
  Nama as nama, 
  No_Transaksi as no_transaksi,
  Kd_Pasien as kd_pasien, 
  Jumlah as jumlah, 
  Ket as ket, 
  User_name as user_name, 
  Nama_Unit as nama_unit,* FROM History_Trans  
Where kd_kasir='" . $param[3] . "' And tgl_transaksi BETWEEN '" . $param[0] . "'  And '" . $param[1] . "'  
GROUP BY Kd_Kasir, No_Transaksi, Tgl_Transaksi, Kd_Pasien, Nama, Kd_Unit,  Nama_Unit, IsPay, 
Kd_User, Kd_User_Del, User_Name, Jumlah, Tgl_Batal,  Ket, Jam_Batal Order By Nama, No_Transaksi, Tgl_Transaksi, User_name")->result();
		$totall = 0;
		foreach ($query as $line) {
			$tanggal = date_create($line->tgl_batal);
			$jam = date_create($line->jam_batal);
			$totall += $line->jumlah;
			$html .= '
	 				<tr>
	 					<td width="100"  align="right">' . $line->kd_pasien . '</td>
						<td width="250" >' . $line->nama . '</td>
						<td width="100" align="right">' . $line->no_transaksi . '</td>
						<td width="150" >' . $line->nama_unit . '</td>
						
						<td width="90" >' . date_format($tanggal, "Y/m/d") . '</td>
						<td width="50" align="right">' . $line->shiftdel . '</td>
						<td width="50" align="right">' . date_format($jam, "H:i") . '</td>
						<td width="180" >' . $line->ket . '</td>
						<td width="180"  align= "right">' . number_format($line->jumlah, 0, ".", ",") . '</td>
	 					<td width="100" >' . $line->user_name . ' </td>
	 				</tr>"
	 				
				';
		}
		$html .= '
	 				<tr>
						<td width="180" align= "right" colspan="8"><strong>Grand Total</strong></td>
						<td width="180"  align= "right"><strong>' . number_format($totall, 0, ".", ",") . '</strong></td>
	 					<td width="100" >&nbsp;</td>
	 				</tr>"
	 				
				';
		$html .= "</table>";
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="lembar keluar masuk.pdf"');
		readfile('original.pdf');
	}

	function cetakRWJBatalTransaksi_()
	{
		$common = $this->common;
		$result = $this->result;
		$param = json_decode($_POST['data']);

		$var = $param->criteria;
		$type_file = $param->type_file;
		$param = explode('#aje#', $var);
		$d_medrec = $this->uri->segment(4, 0);

		$html = '';
		if ($type_file == 1) {
			$html .= "<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		} else {
			$html .= "<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
		}
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';

		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = '<br>Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$telp .= ' ,Fax. ' . $rs->fax . '.';
		}

		$jkx = "";
		$gdarah = "";

		$html .= '<table class="t2">
					<thead>
						<tr border="0">
							<th colspan="10"> ' . $param[2] . '</center></th>
						</tr>
						<tr border="0">
							<th colspan="10"> Periode ' . $param[0] . ' s/d ' . $param[1] . '</center></th>
						</tr>
					</thead>
				</table>';
		$html .= '<br>';
		$html .= '<table class="t1" border="1">
				<thead>
	 				<tr>
	 					<td width="100" align= "center"><b>Medrec</b></td>
						<td width="250" align= "center"><b>Nama Pasien</b></td>
						<td width="100" align= "center"><b>No transaksi</b></td>
						<td width="100" align= "center"><b>Unit</b></td>
						<td width="60" align= "center"><b>Tanggal batal</b></td>
						<td width="50" align= "center"><b>Shift batal</b></td>
						<td width="50" align= "center"><b>Jam batal</b></td>
						<td width="100" align= "center"><b>Alasan Batal</b></td>
						<td width="100" align= "right"><b>Jumlah</b></td>
	 					<td width="100" align= "center"><b>Petugas </b></td>
	 				</tr>
				</thead>';
		/*$query=$this->db->query("select 
  kd_kasir,
  no_transaksi ,
  tgl_transaksi ,
  kd_pasien ,
  nama ,
  kd_unit,
  nama_unit,
  kd_user ,
  kd_user_del ,
  shift,
  shiftdel ,
  jumlah,
  user_name ,
  tgl_batal ,
  ket,jam_batal  from history_batal_trans  where kd_kasir='".$param[3]."' and tgl_batal between '".$param[0]."' and  '".$param[1]."'")->result();*/
		$query = $this->db->query("Select 
  Nama as nama, 
  No_Transaksi as no_transaksi,
  Kd_Pasien as kd_pasien, 
  Jumlah as jumlah, 
  Ket as ket, 
  User_name as user_name, 
  Nama_Unit as nama_unit,
  Tgl_Batal as tgl_batal,
  Jam_Batal as jam_batal,* FROM History_Trans  
Where kd_kasir='" . $param[3] . "' And tgl_transaksi BETWEEN '" . $param[0] . "'  And '" . $param[1] . "'  
GROUP BY Kd_Kasir, No_Transaksi, Tgl_Transaksi, Kd_Pasien, Nama, Kd_Unit,  Nama_Unit, IsPay, 
Kd_User, Kd_User_Del, User_Name, Jumlah, Tgl_Batal,  Ket, Jam_Batal Order By Nama, No_Transaksi, Tgl_Transaksi, User_name")->result();
		$totall = 0;
		foreach ($query as $line) {
			$tanggal = date_create($line->tgl_batal);
			$jam = date_create($line->jam_batal);
			$totall += $line->jumlah;
			$html .= '
	 				<tr>
	 					<td width="100"  align="right">' . $line->kd_pasien . '</td>
						<td width="250" >' . $line->nama . '</td>
						<td width="100" align="right">' . $line->no_transaksi . '</td>
						<td width="150" >' . $line->nama_unit . '</td>
						
						<td width="90" >' . date_format($tanggal, "Y/m/d") . '</td>
						<td width="50" align="right"></td>
						<td width="50" align="right">' . date_format($jam, "H:i") . '</td>
						<td width="180" >' . $line->ket . '</td>
						<td width="180"  align= "right">' . number_format($line->jumlah, 0, ".", ",") . '</td>
	 					<td width="100" >' . $line->user_name . ' </td>
	 				</tr>
	 				
				';
			/* $html.='
	 				<tr>
	 					<td width="100"  align="right">'.$line->kd_pasien .'</td>
						<td width="250" >'.$line->nama .'</td>
						<td width="100" align="right">'.$line->no_transaksi .'</td>
						<td width="150" >'.$line->nama_unit .'</td>
						
						<td width="90" >'.date_format($tanggal,"Y/m/d") .'</td>
						<td width="50" align="right">'.$line->shiftdel .'</td>
						<td width="50" align="right">'.date_format($jam,"H:i") .'</td>
						<td width="180" >'.$line->ket .'</td>
						<td width="180"  align= "right">'.number_format($line->jumlah,0,".",",") .'</td>
	 					<td width="100" >'.$line->user_name .' </td>
	 				</tr>"
	 				
				';	
				*/
		}
		$html .= '
	 				<tr>
						<td width="180" align= "right" colspan="8"><strong>Grand Total</strong></td>
						<td width="180"  align= "right"><strong>' . number_format($totall, 0, ".", ",") . '</strong></td>
	 					<td width="100" >&nbsp;</td>
	 				</tr>
	 				
				';
		$html .= "</table>";
		$prop = array('foot' => true);
		//jika type file 1=excel 
		if ($type_file == 1) {
			$name = ' Laporan Batal Transaksi Rawat Jalan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
		} else {
			$this->common->setPdf('L', ' Laporan Batal Transaksi Rawat Jalan', $html);
		}
		echo $html;
	}

	public function getPoliklinik()
	{
		$result = $this->db->query("SELECT 1 as id,kd_unit,nama_unit FROM unit
									where parent ='2'
									UNION
									Select 0 as id,'000'as kd_unit, 'SEMUA' as nama_unit
									order by id,nama_unit")->result();

		echo '{success:true, totalrecords:' . count($result) . ', listData:' . json_encode($result) . '}';
	}

	public function cetakRWJRentanTunggu()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'LAPORAN RENTAN TUNGGU';
		$param = json_decode($_POST['data']);
		$html = "";
		$kd_unit = $param->kd_unit;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$ctgl = "where k.tgl_masuk >= '" . date('Y-m-d', strtotime($tglAwal)) . "' and k.tgl_masuk <= '" . date('Y-m-d', strtotime($tglAkhir)) . "'";
		if ($kd_unit == 'SEMUA' || $kd_unit == '') {
			$cKdunit = '';
			$unit = 'SEMUA POLIKLINIK';
		} else {
			$cKdunit = "and k.kd_unit='" . $kd_unit . "'";
			$unit = $this->db->query("select nama_unit from unit where kd_unit='" . $kd_unit . "'")->row()->nama_unit;
		}

		$queryHead = $this->db->query("select k.kd_pasien,p.nama,k.kd_unit,u.nama_unit, k.jam_masuk, k.jam_berkas_masuk, k.jam_dilayani, k.jam_keluar, k.waktu_ditindak
										from kunjungan k
											inner join pasien p on p.kd_pasien=k.kd_pasien
											inner join unit u on u.kd_unit=k.kd_unit
											" . $ctgl . $cKdunit . "
											
										order by p.nama");
		$query = $queryHead->result();

		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . '<br>
					</tr>
					<tr>
						<th> Periode ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th> Poli : ' . $unit . '</th>
					</tr>
				</tbody>
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="60" align="center">No. Medrec</th>
					<th width="80" align="center">Nama</th>
					<th width="60" align="center">Unit</th>
					<th width="40" align="center">Jam Daftar</th>
					<th width="40" align="center">Jam Dilayani</th>
					<th width="40" align="center">Rentan Tunggu</th>
				  </tr>
			</thead>';

		// <th width="40" align="center">Jam Berkas Datang(Scan Berkas)</th>
		// <th width="40" align="center">Jam Pulang</th>
		if (count($query) > 0) {
			$no = 0;
			foreach ($query as $line) {
				$no++;

				$jam_daftar 	= date_create($line->jam_masuk);
				$jam_dilayani 	= date_create($line->waktu_ditindak);
				$diff			= date_diff($jam_dilayani, $jam_daftar);
				$date			= date_create($diff->h . ":" . $diff->i . ":" . $diff->s);
				$waktu_tunggu 	= date_format($date, "H:i:s");
				$html .= '<tbody>
							<tr>
								<th align="left">' . $no . '</th>
								<th align="left">' . $line->kd_pasien . '</th>
								<th align="left">' . $line->nama . '</th>
								<th align="left">' . $line->nama_unit . '</th>
								<th align="center">' . substr($line->jam_masuk, -8) . '</th>
								<th align="center">' . substr($line->waktu_ditindak, -8) . '</th>
								<th align="center">' . substr($waktu_tunggu, -8) . '</th>
							</tr>';

				// <th align="center">'.substr($line->jam_berkas_masuk, -8).'</th>
				// <th align="center">'.substr($line->jam_keluar, -8).'</th>
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';
		}

		$html .= '</tbody></table>';
		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Lap. Rentan Tunggu', $html);
	}

	public function cetakRiwayatKunjungan()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'RIWAYAT PASIEN';
		$param = json_decode($_POST['data']);

		$kd_unit = $param->kd_unit;
		$kd_pasien = $param->kd_pasien;
		$tgl_masuk = $param->tgl_masuk;
		$kd_kasir = $param->kd_kasir;
		$urut_masuk = $param->urut_masuk;
		$no_transaksi = $param->no_transaksi;
		$kd_dokter = $param->kd_dokter;

		$pasien = $this->db->query("select *,dbo.GetUmur(tgl_lahir, GETDATE()) as ages from pasien where kd_pasien='" . $param->kd_pasien . "'")->row();
		$unit = $this->db->query("select nama_unit from unit where kd_unit='" . $param->kd_unit . "'")->row()->nama_unit;
		$dokter = $this->db->query("select nama from dokter where kd_dokter='" . $param->kd_dokter . "'")->row()->nama;

		$result = $this->db->query("select * from kunjungan where kd_pasien='" . $param->kd_pasien . "' and tgl_masuk='" . $param->tgl_masuk . "'");



		//-------------JUDUL-----------------------------------------------
		$html = '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>' . $title . '</th>
					</tr>
					<tr>
						<th> PerTanggal Kunjungan</th>
					</tr>
				</tbody>
			</table><br>';
		$html .= '
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="40" align="left">Nama</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">' . $pasien->NAMA . '</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th align="left">Umur</th>
						<th align="left">:</th>
						<th width="80" align="left">' . getUmur($pasien->ages) . '</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Alamat</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">' . $pasien->ALAMAT . '</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Tgl. Kunjungan</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">' . tanggalstring($tgl_masuk) . '</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Nama Unit</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">' . $unit . '</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Dokter</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">' . $dokter . '</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
				</tbody>
			</table><br>';
		#=============================================================ISI============================================================#

		# ----------------------------------------------ANAMNESE---------------------------------------------------------------------#

		$queryanamnese = $this->db->query("select anamnese from kunjungan k
									where k.kd_pasien='" . $kd_pasien . "' 
										and k.kd_unit='" . $kd_unit . "' 
										and k.tgl_masuk='" . $tgl_masuk . "' 
										and k.urut_masuk='" . $urut_masuk . "'")->result();
		$resultStatus = $this->db->query("SELECT rwi_rujuk.catatan, mr_status.status from 
			mr_rwi_rujukan rwi_rujuk 
		INNER JOIN mr_status_rwirujukan mr_status ON CAST(rwi_rujuk.id_status as integer) = mr_status.id_status 
		WHERE 
			rwi_rujuk.kd_pasien = '" . $kd_pasien . "' 
			and rwi_rujuk.tgl_masuk = '" . $tgl_masuk . "' 
			and rwi_rujuk.kd_unit = '" . $kd_unit . "'");

		if ($resultStatus->num_rows() > 0) {
			$catatan 	= $resultStatus->row()->catatan;
			$status 	= $resultStatus->row()->status;
		} else {
			$catatan 	= "";
			$status 	= "";
		}

		$html .= '
			<table width="100%" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">ANAMNESE</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				<tr>
					<th colspan="2" align="center">Anamnese / Keluhan</th>
				</tr>
			</thead>';
		if (count($queryanamnese) > 0) {
			foreach ($queryanamnese as $lineanamnese) {
				$html .= '<tbody>
							<tr>
								<td align="left" style="padding-left:5px;" width="20%">Anamnesa</td>
								<td align="left" style="padding-left:5px;">' . $lineanamnese->anamnese . '</td>
							</tr>';
			}

			$html .= '<tr>';
			$html .= '<td style="padding-left:5px;" width="20%">Catatan : </td>';
			$html .= '<td style="padding-left:5px;">' . $catatan . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td style="padding-left:5px;" width="20%">Status  : </td>';
			$html .= '<td style="padding-left:5px;">Status  : ' . $status . '</td>';
			$html .= '</tr>';
			$html .= '<tbody></table><br>';
		} else {
			$html .= '<tbody>';
			$html .= '<tr>';
			$html .= '<td style="padding-left:5px;" width="20%">Catatan : </td>';
			$html .= '<td style="padding-left:5px;">' . $catatan . '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td style="padding-left:5px;" width="20%">Status  : </td>';
			$html .= '<td style="padding-left:5px;">Status  : ' . $status . '</td>';
			$html .= '</tr>';
			$html .= '<tr>
							<td align="left">&nbsp;&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}

		/* ====================================================================== KONDISIK PASIEN ========================================== */
		$query_kondisi = $this->db->query("SELECT 
					A.id_kondisi AS id_kondisi,
					A.kondisi as kondisi, 
					A.satuan as satuan,
					A.orderlist as orderlist, 
					A.kd_unit as kd_unit, 
					B.hasil as hasil 
				FROM MR_KONPASDTL B INNER JOIN (SELECT * FROM MR_KONPAS 
				WHERE kd_pasien = '" . $kd_pasien . "' and mr_konpas.kd_unit='" . $kd_unit . "' and tgl_masuk = '" . $tgl_masuk . "' ) AS C ON B.ID_KONPAS = C .ID_KONPAS 
				RIGHT JOIN MR_KONDISIFISIK A ON A.id_kondisi = B.id_kondisi");
		$html .= '
			<table width="100%" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="4" align="left">Kondisi</th>
				</tr>
				<tr>
					<th width="5%" align="center">No.</th>
					<th width="55%" align="center">Kondisi</th>
					<th width="20%" align="center">Hasil</th>
					<th width="20%" align="center">Satuan</th>
				</tr>
			</thead>';
		$html .= "<tbody>";
		// if ($query_kondisi->num_rows() > 0) {
		$no = 1;
		foreach ($query_kondisi->result() as $result_kondisi) {
			$html .= "<tr>";
			$html .= "<td align='center'>" . $no . "</td>";
			$html .= "<td style='padding-left:5px;'>" . $result_kondisi->kondisi . "</td>";
			$html .= "<td style='padding-left:5px;'>" . $result_kondisi->hasil . "</td>";
			$html .= "<td style='padding-left:5px;'>" . $result_kondisi->satuan . "</td>";
			$html .= "</tr>";
			$no++;
		}
		// }
		$html .= "</tbody>";
		$html .= "</table><br>";

		/* ====================================================================== KONDISIK PASIEN ========================================== */
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#

		# -------------------------------------ICD 10 / DIAGNOSA---------------------------------------------------------------------#
		$querydiagnosa = $this->db->query("SELECT mrp.*,p.penyakit,n.morfologi,k.sebab 
											FROM mr_penyakit mrp 
												LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
												LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit 
													AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit 
													AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
												LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit 
													AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit 
													AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
											WHERE mrp.kd_pasien = '" . $kd_pasien . "' 
												and mrp.kd_unit='" . $kd_unit . "' 
												and mrp.tgl_masuk = '" . $tgl_masuk . " '")->result();

		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="4" align="left">ICD 10 / Diagnosa</th>
				</tr>
				<tr>
					<th colspan="4" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="80" align="center">Deskripsi Penyakit</th>
					<th width="60" align="center">Status diagnosa</th>
					<th width="60" align="center">Kasus</th>
				  </tr>
			</thead>';
		if (count($querydiagnosa) > 0) {
			$no = 0;
			foreach ($querydiagnosa as $linediagnosa) {
				$no++;
				$diagnosa = '';
				$kasus = '';

				if ($linediagnosa->STAT_DIAG == 0) {
					$diagnosa = 'Diagnosa Awal';
				} else if ($linediagnosa->STAT_DIAG == 1) {
					$diagnosa = 'Diagnosa Utama';
				} else if ($linediagnosa->STAT_DIAG == 2) {
					$diagnosa = 'Komplikasi';
				} else if ($linediagnosa->STAT_DIAG == 3) {
					$diagnosa = 'Diagnosa Sekunder';
				}
				//true=kasus lama, false=kasus baru
				if ($linediagnosa->KASUS == '1') {
					$kasus =  'Lama';
				} else if ($linediagnosa->KASUS == '0') {
					$kasus =  'Baru';
				}

				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td align="left">' . $linediagnosa->penyakit . '</td>
								<td align="left">' . $diagnosa . '</td>
								<td align="left">' . $kasus . '</td>
							</tr>';
			}
			$html .= '<tbody></table><br>';
		} else {
			$html .= '<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#

		# ----------------------------------------------ICD 9 / TINDAKAN---------------------------------------------------------------------#

		$querytindakan = $this->db->query("select mr.* , ic.deskripsi
									from mr_tindakan mr
									inner join icd_9 ic on ic.kd_icd9=mr.kd_icd9
									where mr.kd_pasien='" . $kd_pasien . "' 
										and mr.kd_unit='" . $kd_unit . "' 
										and mr.tgl_masuk='" . $tgl_masuk . "' 
										and mr.urut_masuk='" . $urut_masuk . "'")->result();
		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">ICD 9 / TINDAKAN</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Deskripsi</th>
				  </tr>
			</thead>';
		if (count($querytindakan) > 0) {
			$no = 0;
			foreach ($querytindakan as $linetindakan) {
				$no++;

				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td align="left">' . $linetindakan->deskripsi . '</td>
							</tr>';
			}
			$html .= '<tbody></table><br>';
		} else {
			$html .= '<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#

		# -------------------------------------------------------OBAT------------------------------------------------------------------------#

		$queryobat = $this->db->query("select bod.*, bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='" . $kd_pasien . "' 
										and bo.kd_unit='" . $kd_unit . "' 
										and t.tgl_transaksi='" . $tgl_masuk . "' 
										and t.urut_masuk='" . $urut_masuk . "'
									order by o.nama_obat")->result();
		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">OBAT</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Nama obat</th>
				  </tr>
			</thead>';
		if (count($queryobat) > 0) {
			$no = 0;
			foreach ($queryobat as $lineobat) {
				$no++;

				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td align="left">' . $lineobat->nama_obat . '</td>
							</tr>';
			}
			$html .= '<tbody></table><br>';
		} else {
			$html .= '<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#

		# -------------------------------------------------------LABORATORIUM----------------------------------------------------------------#
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$querylab = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
											case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
										From LAB_hasil 
											inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
											inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
												on LAB_Test.kd_Test = produk.Kd_Produk
											inner join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
											inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
											inner join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
										where LAB_hasil.Kd_Pasien = '" . $kd_pasien . "' 
											And LAB_hasil.Tgl_Masuk = '" . $tgl_masuk . "'  
											and LAB_hasil.Urut_Masuk = '" . $urut_masuk . "'
											and LAB_hasil.kd_unit= '" . $kd_unit_lab . "' 
											and lab_hasil.kd_unit_asal='" . $kd_unit . "'
										order by lab_test.kd_lab,lab_test.kd_test asc")->result();


		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="9" align="left">LABORATORIUM</th>
				</tr>
				<tr>
					<th colspan="9" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="20" align="center">Item Pemeriksaan</th>
					<th width="40" align="center">Pemeriksaan</th>
					<th width="40" align="center">Metode</th>
					<th width="20" align="center">Hasil</th>
					<th width="30" align="center">Normal</th>
					<th width="20" align="center">Ket. hasil</th>
					<th width="40" align="center">Satuan</th>
					<th width="40" align="center">Keterangan</th>
				  </tr>
			</thead>';

		if (count($querylab) > 0) {
			$no = 0;
			foreach ($querylab as $linelab) {
				$hasil = '';
				$ket = '';
				$metode = '';

				if ($linelab->hasil == 'null' || $linelab->hasil == null) {
					$hasil = '';
				} else {
					$hasil = $linelab->hasil;
				}

				if ($linelab->ket == 'null' || $linelab->ket == null || $linelab->ket == 'undefined') {
					$ket = '';
				} else {
					$ket = $linelab->ket;
				}

				if ($linelab->satuan == 'null' && $linelab->satuan == 'null') {
					$metode = '';
				} else {
					$metode = $linelab->metode;
				}
				if ($linelab->judul_item == '') {
					$nomor = '';
				} else {
					$no++;
					$nomor = $no;
				}

				$html .= '<tbody>
							<tr>
								<td align="center">' . $nomor . '</td>
								<td align="left">' . $linelab->judul_item . '</td>
								<td align="left">' . $linelab->item_test . '</td>
								<td align="center">' . $metode . '</td>
								<td align="left">&nbsp;' . $hasil . '</td>
								<td align="center">' . $linelab->normal . '</td>
								<td align="center">' . $linelab->ket_hasil . '</td>
								<td align="left">' . $linelab->satuan . '</td>
								<td align="left">' . $ket . '</td>
							</tr>';
			}
			$html .= '<tbody></table><br>';
		} else {
			$html .= '<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#

		# -------------------------------------------------------RADIOLOGI-------------------------------------------------------------------#
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$queryrad = $this->db->query("select dt.*, p.deskripsi
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi
									where k.kd_pasien='" . $kd_pasien . "' 
										and k.kd_unit='" . $kd_unit_rad . "'
										and tr.kd_unit='" . $kd_unit . "'
										and k.tgl_masuk='" . $tgl_masuk . "' 
										and k.urut_masuk='" . $urut_masuk . "'")->result();

		$html .= '
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">RADIOLOGI</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Deskripsi pemeriksaan radiologi</th>
				  </tr>
			</thead>';
		if (count($queryrad) > 0) {
			$no = 0;
			foreach ($queryrad as $linerad) {
				$no++;

				$html .= '<tbody>
							<tr>
								<td align="center">' . $no . '</td>
								<td align="left">' . $linerad->deskripsi . '</td>
							</tr>';
			}
			$html .= '<tbody></table><br>';
		} else {
			$html .= '<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#

		// echo $html; die;

		$prop = array('foot' => true);
		$this->common->setPdf('P', 'Riwayat Pasien', $html);
	}

	public function cetakRWJTindakanPerPasien_()
	{
		$common = $this->common;
		$result = $this->result;
		$title = 'LAPORAN TINDAKAN PERPASIEN';
		$param = json_decode($_POST['data']);
		$html = '';
		$kd_unit = $param->kd_unit;
		$kd_customer = $param->kd_customer;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$type_file = $param->type_file;

		$awal = tanggalstring(date('Y-m-d', strtotime($tglAwal)));
		$akhir = tanggalstring(date('Y-m-d', strtotime($tglAkhir)));

		$ctgl = "where k.tgl_masuk >= '" . date('Y-m-d', strtotime($tglAwal)) . "' and k.tgl_masuk <= '" . date('Y-m-d', strtotime($tglAkhir)) . "'";
		if ($kd_unit == 'Semua' || $kd_unit == '') {
			$cKdunit = " and u.parent not in('100') and t.kd_unit !='3'";
			$unit = 'SEMUA POLIKLINIK';
		} else {
			$cKdunit = "and t.kd_unit='" . $kd_unit . "'";
			$unit = $this->db->query("select nama_unit from unit where kd_unit='" . $kd_unit . "'")->row()->nama_unit;
		}

		if ($kd_customer == 'SEMUA' || $kd_customer == '') {
			$cKdcustomer = '';
			$customer = 'SEMUA';
		} else {
			$cKdcustomer = "and k.kd_customer='" . $kd_customer . "'";
			$customer = $this->db->query("select customer from customer where kd_customer='" . $kd_customer . "'")->row()->customer;
		}

		$queryHead = $this->db->query("select distinct(t.kd_pasien),ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer
										from detail_transaksi dt 
											inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
											inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
											inner join produk p on p.kd_produk=dt.kd_produk
											inner join pasien ps on ps.kd_pasien=k.kd_pasien
											inner join dokter d on d.kd_dokter=k.kd_dokter
											inner join unit u on u.kd_unit=t.kd_unit
											inner join customer c on c.kd_customer=k.kd_customer
										WHERE t.tgl_transaksi >= '" . $tglAwal . "' and t.tgl_transaksi <= '" . $tglAkhir . "'
										" . $cKdunit . $cKdcustomer . "
										order by  c.customer ,t.tgl_transaksi,ps.nama");
		$query = $queryHead->result();
		//-------------JUDUL-----------------------------------------------
		$html .= '
			<table  class="t2" cellspacing="0" border="0">
				
					<tr>
						<td colspan="7">' . $title . '</td>
					</tr>
					<tr>
						<td colspan="7"> Periode ' . $awal . ' s/d ' . $akhir . '</td>
					</tr>
					<tr>
						<td colspan="7"> Poli : ' . $unit . '</td>
					</tr>
					<tr>
						<td colspan="7"> Jenis Pasien : ' . $customer . '</td>
					</tr>
				
			</table><br>';

		//---------------ISI-----------------------------------------------------------------------------------
		$html .= '
			<table width="100" height="20" border = "1">
			
				 <tr>
					<td width="5" align="center">No</td>
					<td width="60" align="center">No. Medrec</td>
					<td width="80" align="center">Nama</td>
					<td width="40" align="center">Tgl. Transaksi</td>
					<td width="60" align="center">Unit</td>
					<td width="40" align="center">Customer</td>
					<td width="40" align="center">Deskripsi tindakan</td>
				  </tr>
			';
		if (count($query) > 0) {
			$no = 0;
			foreach ($query as $line) {
				$no++;
				$html .= '
							<tr>
								<td align="left">' . $no . '</td>
								<td align="left">' . $line->kd_pasien . '</td>
								<td align="left">' . $line->nama . '</td>
								<td align="left">' . tanggalstring($line->tgl_transaksi) . '</td>
								<td align="left">' . $line->nama_unit . '</td>
								<td align="left">' . $line->customer . '</td>
								<td></td>
							</tr>';
				$queryBody = $this->db->query("select dt.*, t.kd_pasien,p.deskripsi as desk,ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer
							from detail_transaksi dt 
								inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
								inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
								inner join produk p on p.kd_produk=dt.kd_produk
								inner join pasien ps on ps.kd_pasien=k.kd_pasien
								inner join dokter d on d.kd_dokter=k.kd_dokter
								inner join unit u on u.kd_unit=t.kd_unit
								inner join customer c on c.kd_customer=k.kd_customer
							WHERE t.tgl_transaksi >= '" . $tglAwal . "' and t.tgl_transaksi <= '" . $tglAkhir . "'
							and t.kd_unit='" . $line->kd_unit . "' and k.kd_customer='" . $line->kd_customer . "' and t.kd_pasien='" . $line->kd_pasien . "'
							order by c.customer asc ");
				$query2 = $queryBody->result();
				foreach ($query2 as $line2) {
					$html .= '<tr>
								<td align="left" colspan="6">&nbsp;</td>
								<td align="left">' . $line2->desk . '</td>
							</tr>';
				}
			}
		} else {
			$html .= '
				<tr class="headerrow"> 
					<td width="" colspan="7" align="center">Data tidak ada</td>
				</tr>

			';
		}

		$html .= '</table>';
		$prop = array('foot' => true);
		if ($type_file == 1) {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected


			$table    = $html;

			# save $table inside temporary file that will be deleted later
			$tmpfile = tempnam(sys_get_temp_dir(), 'html');
			file_put_contents($tmpfile, $table);

			# Create object phpexcel
			$objPHPExcel     = new PHPExcel();

			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
				->getPageSetup()
				->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			

			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
				->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin

			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				)
			);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold

			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
				->getStyle('A1:G4')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
				->getStyle('G7')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center

			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
				->getStyle('G')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right

			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected

			# Fungsi Autosize
			for ($col = 'A'; $col != 'P'; $col++) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
			# END Fungsi Autosize

			$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
			$objPHPExcel->getActiveSheet()->setTitle('tindakan per pasien'); # Change sheet's title if you want

			unlink($tmpfile); # delete temporary file because it isn't needed anymore

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
			header('Content-Disposition: attachment;filename=laporan_tindakan_perpasien.xls'); # specify the download file name
			header('Cache-Control: max-age=0');

			# Creates a writer to output the $objPHPExcel's content
			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$writer->save('php://output');
			exit;
		} else {
			$this->common->setPdf('L', 'Lap. Tindakan PerPasien', $html);
		}
		echo $html;
	}

	public function cetaksepdirect()
	{
		$this->load->library('m_pdf');
		$uri = $_SERVER["REQUEST_URI"];
		$uriArray = explode('/', $uri);
		$param1 = $_POST['noSep'];
		$html = '';
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		//echo $kd_rs;
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();

		$kd_user = $this->session->userdata['user_id']['id'];
		$sqlUser = "SELECT user_names FROM zusers WHERE kd_user='" . $kd_user . "'";
		$objUser = $this->db->query($sqlUser)->row();
		$operator = '-';
		if ($objUser) {
			$operator = $objUser->user_names;
		}
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $this->getSignature()
			)
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url . $param1, false, $context), false);
		if ($res->metadata->code == '200') {
			if ($res->response->peserta->sex == 'P') {
				$jk = 'WANITA';
			} else {
				$jk = 'PRIA';
			}
			$nama = $res->response->peserta->nama;
			$peserta = $res->response->peserta->jenisPeserta->nmJenisPeserta;
			$tglsep = tanggalstring($res->response->tglSep);
			$printer = 'Epson-LX-300_NUANSA';
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
			$handle = fopen($file, 'w');
			$condensed = Chr(27) . Chr(33) . Chr(4);
			$condensed2 = Chr(27) . Chr(33) . Chr(8);
			$bold1 = Chr(27) . Chr(69);
			$bold0 = Chr(27) . Chr(70);
			$initialized = chr(27) . chr(64);
			$condensed1 = chr(15);
			$condensed0 = chr(18);
			$size0 = Chr(27) . Chr(109);
			$Data  = $initialized;
			$Data .= $condensed1;
			$Data .= $condensed;

			// $cmd ="jpegoptim --stdim > http://192.168.1.25/medismart/ui/images/Logo/RSSM.png"; //http://192.168.1.25/medismart/ui/images/Logo/RSSM.png";
			// exec('convert -quality 80 "http://192.168.1.25/medismart/MediSmart.pdf" "http://192.168.1.25/medismart/ui/images/Logo/RSSM.png"',$out,$ret);
			// echo $ret;
			// print_r($out);

			// ob_start();
			// $cmd = im
			// imagejpeg($cmd,"/var/www/html/medismart/ui/images/Logo/RSSM.png");
			// echo $cmd;
			// echo 'a';
			// imagedestroy($cmd);
			// $img = ob_get_clean();
			// $handlea = popen($cmd, 'w');

			// $Data .= $img;
			// fwrite($handle, $Data);
			// fclose($handle);
			// $print = shell_exec("lpr -P ".$printer." -r ".$file);
			echo 'a';
			//$objPHPExcel = PHPExcel_IOFactory::load("/var/www/html/medismart/ui/template/sep.xlsx");
			echo 'a';
			// $objPHPExcel->setActiveSheetIndex(0)
			// ->setCellValue('A2', "No")
			// ->setCellValue('B2', "Name")
			// ->setCellValue('C2', "Email")
			// ->setCellValue('D2', "Phone")
			// ->setCellValue('E2', "Address");
			echo 'b';
			// $file = '/var/www/html/medismart/MediSmart.pdf';
			//echo file_get_contents('/var/www/html/medismart/ui/template/sep.xlsx');
			//echo preg_match( "/\\?".">\\s\\s+\\Z/m", file_get_contents('/var/www/html/medismart/ui/template/sep.xlsx'));
			//foreach (glob("*.php") as $file){if (preg_match( "/\\?".">\\s\\s+\\Z/m", file_get_contents($file))) echo("$file\n");}


			$cmd = 'lpr -P Epson-LX-300_NUANSA /var/www/html/medismart/ui/template/headerSEP.pdf';
			// $cmd .=$file;
			$response = shell_exec($cmd);
		}
	}

	public function pengajuan_sep()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlPengajuanSEP'")->row()->nilai;
		$data = $_POST['data'];

		$headers = $this->getSignatureVedika();
		// $headers.='Content-type: Application/x-www-form-urlencoded\r\n';
		$opts = array(
			'http' => array(
				'method'  => "POST",
				'header'  => $headers,
				'content' => $data
			)
		);
		$xml   = simplexml_load_string($data); // where $xml_string is the XML data you'd like to use (a well-formatted XML string). If retrieving from an external source, you can use file_get_contents to retrieve the data and populate this variable.
		$json  = json_encode($xml); // convert the XML string to JSON
		$array = json_decode($json, TRUE); // convert the JSON-encoded string to a PHP variable

		$ch = curl_init($url);
		//The JSON data.
		$jsonData = $json;
		//echo "string";die();

		// print_r($jsonData);die();

		curl_setopt($ch, CURLOPT_URL, $url);

		//Encode the array into JSON.
		$jsonDataEncoded = json_encode($jsonData);

		//Tell cURL that we want to send a POST request.
		curl_setopt($ch, CURLOPT_POST, 1);

		//Attach our encoded JSON string to the POST fields.
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_decode($jsonDataEncoded));

		//Set the content type to application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// print_r($headers);
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		// $response = $ch;

		curl_close($ch);
		// print_r($response);die();
		$this->insert_response_bpjs($url, json_decode($response), $jsonData, "POST");
		echo $response;
	}

	private function insert_response_bpjs($url, $resp, $params = null, $method = null, $data_pasien = null)
	{
		$code 		= 0;
		$message 	= "Connection Error";
		$response 	= "";

		foreach ($resp as $key => $value) {
			if ($key == 'metaData') {
				foreach ($value as $records => $val_records) {
					if ($records == 'code') {
						$code = $val_records;
					} else if ($records == 'message') {
						$message = $val_records;
					}
				}
			}
		}

		foreach ($resp as $key => $value) {
			if ($key == 'response') {
				$response = json_encode($value);
			}
		}

		$param = array();
		$param['id'] 		= $this->get_max_id_resp_bpjs();
		$param['url'] 		= $url;
		// $param['message'] 	= "[".$code."] - ".$message;
		$param['message'] 	= "[" . $code . "] - " . $message;
		$param['params']	= $params;
		$param['response']	= $response;
		$param['method']	= $method;
		if ($data_pasien != null) {
			// var_dump($data_pasien);
			// $param['kd_pasien'] = $data_pasien['kd_pasien'];
			// $param['tgl_masuk'] = $data_pasien['tgl_masuk'];
			// $param['kd_unit']   = $data_pasien['kd_unit'];
		}
		$param['waktu']		= "Time request : " . $this->jam_request . ":" . $this->menit_request . ":" . $this->detik_request . " / Time response : " . $this->jam_request . ":" . date('i:s');
		$this->db->insert("response_bpjs", $param);
	}

	private function get_max_id_resp_bpjs()
	{
		$this->db->select(" MAX(id) as max");
		$this->db->from("response_bpjs");
		return (int)$this->db->get()->row()->max + 1;
	}

	public function update_sjp()
	{
		$this->db->trans_begin();
		$response = array();
		$params = array(
			'no_sjp' 	=> $this->input->post('no_sjp'),
		);
		$criteria = array(
			'kd_pasien'  => $this->input->post('kd_pasien'),
			'tgl_masuk'  => $this->input->post('tgl_masuk'),
			'kd_unit'    => $this->input->post('kd_unit'),
			'urut_masuk' => $this->input->post('urut_masuk'),
		);
		/*
			===================================== SJP KUNJUNGAN
			===================================================
		 */
		$this->db->where($criteria);
		$this->db->select("*");
		$this->db->from("sjp_kunjungan");
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$this->db->where($criteria);
			$this->db->update("sjp_kunjungan", $params);
			$response['status'] = true;
		} else {
			unset($paramsInsert);
			$paramsInsert = array(
				'no_sjp'     => $this->input->post('no_sjp'),
				'kd_pasien'  => $this->input->post('kd_pasien'),
				'tgl_masuk'  => $this->input->post('tgl_masuk'),
				'kd_unit'    => $this->input->post('kd_unit'),
				'urut_masuk' => $this->input->post('urut_masuk'),
			);
			$this->db->insert("sjp_kunjungan", $paramsInsert);
			$response['status'] = true;
		}

		/*
			========================================  KUNJUNGAN
			===================================================
		 */
		$this->db->where($criteria);
		$this->db->update("kunjungan", $params);

		/*
			========================================  MR PENYAKIT
			===================================================
		*/
		$criteria['kd_penyakit'] = $this->input->post('kd_penyakit');

		unset($paramsInsert);
		$this->db->where($criteria);
		$this->db->select("*");
		$this->db->from("mr_penyakit");
		$query = $this->db->get();
		$paramsInsert = array(
			'kd_penyakit' => $this->input->post('kd_penyakit'),
			'kd_pasien'   => $this->input->post('kd_pasien'),
			'tgl_masuk'   => $this->input->post('tgl_masuk'),
			'kd_unit'     => $this->input->post('kd_unit'),
			'urut_masuk'  => $this->input->post('urut_masuk'),
			'urut'        => 1,
			'stat_diag'   => 0,
			'tindakan'    => '99',
			'perawatan'   => '99',
		);
		if ($query->num_rows() == 0) {
			$paramsInsert['kasus'] = 'false';
		} else {
			$paramsInsert['kasus'] = 'true';
		}

		$response['status'] = $this->db->insert("mr_penyakit", $paramsInsert);
		if ($response['status'] > 0 || $response['status'] === true) {
			$this->db->trans_commit();
			$response['status'] 	= true;
			$response['message'] 	= "Simpan Berhasil";
		} else {
			$this->db->trans_rollback();
			$response['status'] = false;
			$response['message'] 	= "Simpan gagal";
		}
		echo json_encode($response);
	}
	// UPDATE GET DATA BPJS BY NOKA V 1.1 GAN //
	public function getDataBpjs_by_nokartu()
	{
		$no_kartu = $_POST['no_kartu'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNoka'")->row()->nilai;
		$headers = $this->getSignature_new();
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url . '/' . $no_kartu, false, $context));
		$dat = array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url . $no_kartu . "/tglSEP/" . date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		/*if($res->metaData->code=='200'){
			echo '{success:true, totalrecords:'.count($res->response->rujukan).', listData:'.json_encode($res->response->rujukan).'}';
		}else{
			echo json_encode($dat);
		}*/
		echo json_encode($dat);
	}

	public function getDataBpjs_by_nokartu_multi()
	{
		$no_kartu = $_POST['no_kartu'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNokaMulti'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		// echo $timestamp;die;
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $no_kartu, false, $context));
		$urlnya = $url . $no_kartu;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);

		$dat = array(
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		//$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		//$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		//$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		if ($res->metaData->code != 200) {
			$this->insert_response_bpjs($url . $no_kartu, $dat['data'], json_encode($res), "GET", $data_pasien);
			echo '{success:false, listData:' . json_encode($res) . '}';
		} else {
			echo '{success:true, totalrecords:' . count($res->response->rujukan) . ', listData:' . json_encode($res->response->rujukan) . '}';
		}
		/* $opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		//	var_dump($headers); die();
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$no_kartu,false,$context));
		$dat=array(
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		//$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		//$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		//$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		if($res->metaData->code!=200){
			echo '{success:false, listData:'.json_encode($res).'}';
		}else{
			echo '{success:true, totalrecords:'.count($res->response->rujukan).', listData:'.json_encode($res->response->rujukan).'}';
		}
		//var_dump($res);
		//var_dump($res->response->rujukan);
		//	$this->insert_response_bpjs($url.$no_kartu."/tglSEP/".date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		 */
	}

	private function getSignature_new()
	{
		/* $tmp_secretKey= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID= $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$data      = $tmp_costumerID;
		$secretKey = $tmp_secretKey;
		// Computes the timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		// Computes the signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
		
		// base64 encode…
		$encodedSignature = base64_encode($signature);
		
		// urlencode…
		// $encodedSignature = urlencode($encodedSignature);
		
		$header 	= "Accept:JSON\n";
		$header 	.= "Content-Type:application/json\n";
		$header 	.= "X-cons-id: " .$data ."\n";
		$header 	.= "X-timestamp:" .$tStamp ."\n";
		$header 	.=  "X-signature: " .$encodedSignature."\n";
		return $header; */
		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}

	public function get_provisi_from_bpjs()
	{
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='url_get_propinsi'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		$urlnya = $url;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
	}

	public function get_kabupaten_from_bpjs()
	{
		$kd_prov = $this->input->post('kode_propinsi');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='url_get_kabupaten'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		$urlnya = $url . $kd_prov;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		//var_dump($res) ;
		echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
	}

	public function get_kecamatan_from_bpjs()
	{
		$kd_kab = $this->input->post('kode_kab');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='url_get_kecamatan'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		$urlnya = $url . $kd_kab;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
	}

	public function get_kode_spesialis_bpjs()
	{
		$kd_poli_bpjs = $this->input->post('kd_poli_bpjs');
		$query = $this->db->query("SELECT a.kd_unit,a.nama_unit,b.unit_bpjs from unit a inner join map_unit_bpjs b on a.kd_unit=b.kd_unit where b.unit_bpjs='$kd_poli_bpjs'")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	public function get_kode_prov_perujuk()
	{
		$kode_prov = $this->input->post('kode_prov');
		$query = $this->db->query("SELECT * FROM RUJUKAN WHERE kd_prov_perujuk='$kode_prov'")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	public function get_kode_dokter_bpjs()
	{
		/* $now= date("Y-m-d");
		$kd_spesialis=$this->input->post('spesialis');
		$unit=$this->input->post('unit');
		if($unit=='IGD'){
			$pelayanan=1;
		}else if($unit!='RWI'){
			$pelayanan=2;
		}else{
			$pelayanan=1;
		}
		$tgl_rujukan=  date('Y-m-d', strtotime($this->input->post('tgl_rujukan')));
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url=$this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$pelayanan.'/tglPelayanan/'.$now.'/Spesialis/'.$kd_spesialis,false,$context));
		if (count($res->response)>0) {
		    echo '{success:true, totalrecords:'.count($res->response->list).', listData:'.json_encode($res->response->list).'}';
		}else{
		    echo '{success:true, totalrecords:0, listData:[]}';
		} */

		$now = date("Y-m-d");
		$kd_spesialis = $this->input->post('spesialis');
		$unit = $this->input->post('unit');
		$kd_unit_tujuan = $this->input->post('kd_unit');
		if (is_numeric($unit) == true) {
			$unit_referensi_dpjp = $this->db->query("SELECT unit_referensi_dpjp FROM map_unit_bpjs WHERE kd_unit='" . $unit . "'")->row()->unit_referensi_dpjp;
			$unit = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $unit . "'")->row()->unit_bpjs;
		} else {
			$unit_referensi_dpjp = $this->db->query("SELECT unit_referensi_dpjp FROM map_unit_bpjs WHERE kd_unit='" . $kd_unit_tujuan . "'")->row()->unit_referensi_dpjp;
		}

		// HUDI
		// 10-02-2022
		if ($unit != 'RWJ' && $unit != 'IGD') {
			$pelayanan = 1;
		} else {
			$pelayanan = 2;
		}
		$tgl_rujukan =  date('Y-m-d', strtotime($this->input->post('tgl_rujukan')));
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit, false, $context));

		$urlnya = $url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit_referensi_dpjp;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo json_encode($res->response->list);
		// echo count($res->response->list);
		// echo $url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit;
		// die;
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
		} else {
			echo '{success:true, totalrecords:0, listData:[]}';
		}
	}

	public function get_kode_dokter_bpjs2()
	{
		$now = date("Y-m-d");
		$kd_spesialis = $this->input->post('spesialis');
		$unit = $this->input->post('unit');
		if ($unit != 'RWI') {
			//$kd_spesialis=$this->db->query("SELECT unit_bpjs from map_unit_bpjs where kd_unit='$unit'")->row()->unit_bpjs;
			$pelayanan = 2;
		} else {
			//$kd_spesialis='RWI'	;
			$pelayanan = 1;
		}
		//echo $pelayanan; 
		$tgl_rujukan =  date('Y-m-d', strtotime($this->input->post('tgl_rujukan')));
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		// $url= 'https://dvlp.bpjs-kesehatan.go.id/vclaim-rest/referensi/dokter/pelayanan/'.$unit.'/tglPelayanan/'. date("Y-m-d").'/Spesialis/';
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		//echo $url.'/'.$pelayanan.'/tglPelayanan/'.$now.'/Spesialis/'.$kd_spesialis;
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $kd_spesialis, false, $context));
		$urlnya = $url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $kd_spesialis;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		//var_dump($res);
		echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';

		/* $now= date("Y-m-d");
		$kd_spesialis=$this->input->post('spesialis');
		$unit=$this->input->post('unit');
		if($unit!='RWI'){
			//$kd_spesialis=$this->db->query("SELECT unit_bpjs from map_unit_bpjs where kd_unit='$unit'")->row()->unit_bpjs;
			$pelayanan=2;
		}else{
			//$kd_spesialis='RWI'	;
			$pelayanan=1;
		}
		//echo $pelayanan; 
		$tgl_rujukan=  date('Y-m-d', strtotime($this->input->post('tgl_rujukan')));
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url=$this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		// $url= 'https://dvlp.bpjs-kesehatan.go.id/vclaim-rest/referensi/dokter/pelayanan/'.$unit.'/tglPelayanan/'. date("Y-m-d").'/Spesialis/';
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		//echo $url.'/'.$pelayanan.'/tglPelayanan/'.$now.'/Spesialis/'.$kd_spesialis;
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$pelayanan.'/tglPelayanan/'.$now.'/Spesialis/'.$kd_spesialis,false,$context));
		//var_dump($res);
	    echo '{success:true, totalrecords:'.count($res->response->list).', listData:'.json_encode($res->response->list).'}'; */
	}


	public function getDataBpjs_by_norujukan()
	{
		$no_rujukan = $_POST['no_rujukan'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByRujukan'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $no_rujukan, false, $context));
		$urlnya = $url . $no_rujukan;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);


		$dat = array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url . $no_rujukan, $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat);
		/* $no_rujukan=$_POST['no_rujukan'];
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByRujukan'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$no_rujukan,false,$context));
		$dat=array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url.$no_rujukan."/tglSEP/".date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat); */
	}

	// -------------------------------Proses Cek Kunjungan Rujukan Bpjs------------------------------------------------------------
	// Egi 09 Febuari 2022
	public function cari_kunjungan_rujukan()
	{
		$no_rujukan = $this->input->post('no_rujukan');
		$noka = $this->input->post('noka');
		$kd_pasien = $this->input->post('kd_pasien');
		$tgl_awal = Date('Y-m-d', strtotime('-90 days'));
		$tgl_akhir = date('Y-m-d');

		// $cari_kunjungan_rujukan=$this->db->query("SELECT * FROM rujukan_kunjungan WHERE nomor_rujukan='".$no_rujukan."' and kd_pasien='".$kd_pasien."'ORDER BY tgl_masuk desc ")->result();
		// if(count($cari_kunjungan_rujukan)>=1){
		// 	$kunjungan_pertama='false';
		// }else{
		// 	$kunjungan_pertama='true';
		// }
		// echo '{kunjungan_pertama:'.$kunjungan_pertama.'}';

		// Get History Pelayanan
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlMonitoringPelayanan'")->row()->nilai;
		$ppkRujukan = $this->db->query("select keterangan  from seting_bpjs where key_setting='PpkPelayanan'")->row()->keterangan;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);

		// $res = json_decode(file_get_contents($url . '/' . $no, false, $context));
		$urlnya = $url . '' . $noka . '/' . 'tglMulai/' . $tgl_awal . '/tglAkhir/' . $tgl_akhir;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt(
			$curl,
			CURLOPT_CUSTOMREQUEST,
			$method
		);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt(
			$curl,
			CURLOPT_RETURNTRANSFER,
			true
		);
		curl_setopt(
			$curl,
			CURLOPT_SSL_VERIFYHOST,
			false
		);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt(
			$curl,
			CURLOPT_SSL_VERIFYPEER,
			false
		);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$res = json_decode($response);
		$data = [];
		if (
			$res->metaData->code == 200 || $res->metaData->code == '200'
		) {
			$res->response = $this->decompress($res->response, $timestamp);
			// echo '<pre>' . var_export($res, true) . '</pre>';
			// die;
			$i = 0;
			foreach ($res->response->histori as $value) {
				// echo $value->noRujukan."<br>";
				// echo $value->ppkPelayanan."<br>";
				if ($no_rujukan == $value->noRujukan && str_replace(' ', '', $ppkRujukan) == str_replace(' ', '', $value->ppkPelayanan)) {
					$kunjungan_pertama = "false";
					$data = json_encode($value);
					break;
					// echo '{kunjungan_pertama:false, listData:' .json_encode($value). '}';
				} else {
					$kunjungan_pertama = "true";
					// echo '{kunjungan_pertama:true, listData:""}';
				}
			}
		} else {
			$kunjungan_pertama = "true";
		}
		echo '{kunjungan_pertama:' . $kunjungan_pertama . ',listData:' . $data . '}';
	}
	// public function cari_kunjungan_rujukan()
	// {
	// 	$no_rujukan = $this->input->post('no_rujukan');
	// 	$kd_pasien = $this->input->post('kd_pasien');
	// 	$kd_unit = $this->input->post('kd_unit');
	// 	$sep_rwi = $this->input->post('sep_rwi');
	// 	$kode_poli = '';
	// 	$nama_poli = "";
	// 	$kode_dokter = '';
	// 	$nama_dokter = '';
	// 	$diag_awal = '';
	// 	$kd_prov = '';
	// 	$kd_kab = '';
	// 	$kd_kec = '';
	// 	$no_tlp = '';
	// 	$kd_dpjp = '';

	// 	// echo $sep_rwi; die;
	// 	$cari_kunjungan_rujukan = $this->db->query("SELECT * FROM rujukan_kunjungan WHERE nomor_rujukan='" . $no_rujukan . "' and kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' ORDER BY tgl_masuk desc ")->result();
	// 	if (count($cari_kunjungan_rujukan) >= 1) {
	// 		$kunjungan_pertama = 'false';
	// 	} else {
	// 		$kunjungan_pertama = 'true';
	// 	}

	// 	if ($sep_rwi == '') {
	// 	} else {
	// 		$sql = $this->db->query("SELECT no_surat_kontrol as no_surat,a.kode_poli, a.nama_poli,a.kode_dokter,a.nama_dokter, b.diag_awal, b.kd_prov,b.kd_kec,b.kd_kab,b.no_tlp,b.kd_dpjp  FROM history_surat_kontrol_bpjs a INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  WHERE a.no_sep='" . $sep_rwi . "'")->row();
	// 		$kode_poli = $sql->kode_poli;
	// 		$nama_poli = $sql->nama_poli;
	// 		$kode_dokter = $sql->kode_dokter;
	// 		$nama_dokter = $sql->nama_dokter;
	// 		$diag_awal = $sql->diag_awal;
	// 		$kd_prov = $sql->kd_prov;
	// 		$kd_kab = $sql->kd_kab;
	// 		$kd_kec = $sql->kd_kec;
	// 		$no_tlp = $sql->no_tlp;
	// 		$kd_dpjp = $sql->kd_dpjp;
	// 	}

	// 	echo '{kunjungan_pertama:' . $kunjungan_pertama . ',kode_poli:"' . $kode_poli . '",nama_poli:"' . $nama_poli . '",kode_dokter:"' . $kode_dokter . '",nama_dokter:"' . $nama_dokter . '",diag_awal:"' . $diag_awal . '",kd_prov:"' . $kd_prov . '",kd_kab:"' . $kd_kab . '",kd_kec:"' . $kd_kec . '",no_tlp:"' . $no_tlp . '",kd_dpjp:"' . $kd_dpjp . '"}';
	// }

	public function cek_kunjungan_peserta_bpjs()
	{
		$no_rujukan = $this->input->post('no_rujukan');
		$noka = $this->input->post('no_kartu');
		$kd_unit = $this->input->post('kd_unit');
		$cari_kunjungan_rujukan = $this->db->query("SELECT * FROM history_sep_bpjs WHERE no_rujukan='" . $no_rujukan . "' and no_kartu='" . $noka . "' and kd_unit='" . $kd_unit . "' ORDER BY tgl_masuk desc ")->result();
		if (count($cari_kunjungan_rujukan) >= 1) {
			$kunjungan_pertama = 'false';
		} else {
			$kunjungan_pertama = 'true';
		}
		echo '{kunjungan_pertama:' . $kunjungan_pertama . '}';
	}
	// ------------------------------------------------------------------------------------


	public function getJsonData($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $tgl_sep, $asal_rujukan, $asal_pasien, $tujuankunj, $flagprocedure, $assesmentpel, $kdpenunjang, $naikkelas, $pembiayaan, $penanggungjawab, $no_lp, $kd_dpjp_kontrol)
	{
		$ppkPelayanan = $this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='PpkPelayanan'")->row()->nilai;
		$penjamin = '';
		$DPJPLayan = $kodeDPJP;
		$jnsPelayanan = '';
		if ($penjamin_1 != 0 and $penjamin_1 != '0') {
			$penjamin .= "1,";
		}
		if ($penjamin_2 != 0 and $penjamin_2 != '0') {
			$penjamin .= "2,";
		}
		if ($penjamin_3 != 0 and $penjamin_3 != '0') {
			$penjamin .= "3,";
		}
		if ($penjamin_4 != 0 and $penjamin_4 != '0') {
			$penjamin .= "4,";
		}

		/* if ($tglKejadian != '') {
			$tglKejadian = date('Y-m-d', strtotime($tglKejadian));
		} else {
			$tglKejadian = '';
		} */

		// HUDI
		// 15-02-2022
		// Penyesuaian KLL sesuai dengan katalog v2
		if ($lakaLantas == 0) {
			$tglKejadian = '';
			$no_lp = '';
			$keterangan = '';
			$suplesi = 0;
			$no_suplesi = '';
			$kdPropinsi = '';
			$kdKabupaten = '';
			$kdKecamatan = '';
		}

		if ($flaging == 'RWI' and $asal_pasien == 0) {
			// $tujuan = $this->db->query("SELECT kd_spc_bpjs from map_spc_bpjs WHERE kd_spc='" . $tujuan . "'")->row()->kd_spc_bpjs;
			$jnsPelayanan = "1";
			$tglRujukan = date("Y-m-d");
			$ppkRujukan = $this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'")->row()->nilai;
			$DPJPLayan = '';
			// $asal_rujukan = '2';
		} else if ($flaging == 'RWI' and $asal_pasien == 1) {
			// $asal_rujukan = '2';
			$jnsPelayanan = "1";
			$DPJPLayan = '';
			// $tujuan=$this->db->query("SELECT kd_spc_bpjs from map_spc_bpjs WHERE kd_spc='".$tujuan."'")->row()->kd_spc_bpjs;
			$ppkRujukan = $this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'")->row()->nilai;
			// $tglRujukan=date("Y-m-d");
		} else if ($flaging == 'RWI' and $asal_pasien == 2) {
			$tujuan = $this->db->query("SELECT kd_spc_bpjs from map_spc_bpjs WHERE kd_spc='" . $tujuan . "'")->row()->kd_spc_bpjs;
			$jnsPelayanan = "1";
			$DPJPLayan = '';
		} else {
			$jnsPelayanan = "2";
			// $asal_rujukan='1';
		}
		if ($flaging == 'IGD') {
			// $asal_rujukan="2";
			$tglRujukan = date("Y-m-d");
			$noRujukan = "";
		}

		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$json = '{
           "request": {
              "t_sep": {
                 "noKartu": "' . $noKartu . '",
                 "tglSep": "' . $tgl_sep	. '",
                 "ppkPelayanan": "' . $ppkPelayanan . '",
                 "jnsPelayanan": "' . $jnsPelayanan . '",
                 "klsRawat":{
					"klsRawatHak":"' . $klsRawat . '",
                    "klsRawatNaik":"' . $naikkelas . '",
                    "pembiayaan":"' . $pembiayaan . '",
                    "penanggungJawab":"' . $penanggungjawab . '"
				 },
                 "noMR": "' . $noMR . '",
                 "rujukan": {
                    "asalRujukan": "' . $asal_rujukan . '",
                    "tglRujukan": "' . $tglRujukan . '",
                    "noRujukan": "' . $noRujukan . '",
                    "ppkRujukan": "' . $ppkRujukan . '"
                 },
                 "catatan": "' . $catatan . '",
                 "diagAwal": "' . $diagAwal . '",
                 "poli": {
                    "tujuan": "' . $tujuan . '",
                    "eksekutif": "' . $eksekutif . '"
                 },
                 "cob": {
                    "cob": "' . $cob . '"
                 },
                 "katarak": {
                    "katarak": "' . $katarak . '"
                 },
                 "jaminan": {
                    "lakaLantas": "' . $lakaLantas . '",
					"noLP": "' . $no_lp . '",
                    "penjamin": {
                        "tglKejadian": "' . $tglKejadian . '",
                        "keterangan": "' . $keterangan . '",
                        "suplesi": {
                            "suplesi": "' . $suplesi . '",
                            "noSepSuplesi": "' . $no_suplesi . '",
                            "lokasiLaka": {
                                "kdPropinsi": "' . $kdPropinsi . '",
                                "kdKabupaten": "' . $kdKabupaten . '",
                                "kdKecamatan": "' . $kdKecamatan . '"
                                }
                        }
                    }
                 },
				 "tujuanKunj":"' . $tujuankunj . '",
                 "flagProcedure":"' . $flagprocedure . '",
                 "kdPenunjang":"' . $kdpenunjang . '",
                 "assesmentPel":"' . $assesmentpel . '",
                 "skdp": {
                    "noSurat": "' . $noSurat . '",
                    "kodeDPJP": "' . $kd_dpjp_kontrol . '"
                 },
				 "dpjpLayan":"' . $DPJPLayan . '",
                 "noTelp": "' . $noTelp . '",
                 "user": "' . $cari_user . '"
              }
           }
        }';
		return $json;
	}

	public function GenerateSEP()
	{
		$this->db->trans_begin();
		$result = true;
		// error_reporting(E_ERROR | E_PARSE);
		$tglRujukan      = date('Y-m-d', strtotime($this->input->post('tglRujukan')));
		$flaging         = $this->input->post('flaging');
		$noKartu         = $this->input->post('noKartu');
		$klsRawat        = $this->input->post('klsRawat');
		$noMR            = $this->input->post('noMR');
		$asal_rujukan    = $this->input->post('asal_rujukan');
		$noRujukan       = $this->input->post('noRujukan');
		$ppkRujukan      = $this->input->post('ppkRujukan');
		$catatan         = $this->input->post('catatan');
		$diagAwal        = $this->input->post('diagAwal');
		$tujuan          = $this->input->post('tujuan');
		$eksekutif       = $this->input->post('eksekutif');
		$penjamin_1      = $this->input->post('penjamin_1');
		$penjamin_2      = $this->input->post('penjamin_2');
		$penjamin_3      = $this->input->post('penjamin_3');
		$penjamin_4      = $this->input->post('penjamin_4');
		$tglKejadian     = $this->input->post('tglKejadian');
		$keterangan      = $this->input->post('keterangan');
		$kdPropinsi      = $this->input->post('kdPropinsi');
		$kdKabupaten     = $this->input->post('kdKabupaten');
		$kdKecamatan     = $this->input->post('kdKecamatan');
		$noSurat         = $this->input->post('noSurat');
		$noTelp          = $this->input->post('noTelp');
		$katarak         = $this->input->post('katarak');
		$cob             = $this->input->post('cob');
		$suplesi         = $this->input->post('suplesi');
		$kodeDPJP        = $this->input->post('kodeDPJP');
		$no_suplesi      = $this->input->post('no_suplesi');
		$lakaLantas      = $this->input->post('lakaLantas');
		$no_lp      	 = $this->input->post('no_lp');
		$tgl_sep         = $this->input->post('tgl_sep');
		$asal_pasien     = $this->input->post('asal_pasien');
		$unit_tujuan     = $this->input->post('unit_tujuan');
		$asal_pasien     = $this->input->post('asal_pasien');
		$kd_pasien_asal  = $this->input->post('kd_pasien_asal');
		$kd_unit_asal    = $this->input->post('kd_unit_asal');
		$tgl_masuk_asal  = $this->input->post('tgl_masuk_asal');
		$urut_masuk_asal = $this->input->post('urut_masuk_asal');
		$tujuankunj		 = $this->input->post('tujuankunj');
		$flagprocedure	 = $this->input->post('flagprocedure');
		$assesmentpel	 = $this->input->post('assesmentpel');
		$kdpenunjang	 = $this->input->post('kdpenunjang');
		$naikkelas		 = $this->input->post('naikkelas');
		$pembiayaan		 = $this->input->post('pembiayaan');
		$penanggungjawab = $this->input->post('penanggungjawab');
		$kd_unit_tujuan  = $this->input->post('kd_unit_tujuan');
		$kd_dpjp_kontrol = $this->input->post('kd_dpjp_kontrol');
		$kd_unit 	  = "";
		$urut_masuk	  = "";
		// $tgl_masuk	  = date('Y-m-d');
		$tgl_masuk	  = $this->input->post('tgl_kunjungan');

		// echo 'tgl masuk asal'.$tgl_masuk_asal.'<br>';
		// echo 'tgl kunjungan'.$tgl_kunjungan;
		// die;
		// $asal_rujukan = $asal_pasien;
		if ($flaging == "RWI") {
			// if (isset($asal_pasien) === true || strlen($asal_pasien) > 0) {
			// 	if ($asal_pasien == 0 || $asal_pasien == '0') {
			// 		$asal_rujukan = "1";
			// 	} else if ($asal_pasien == 1 || $asal_pasien == '1') {
			// 		$asal_rujukan = "1";
			// 	} else {
			// 		$asal_rujukan = "1";
			// 	}
			// }
			// // $asal_rujukan = $asal_pasien;
			$asal_rujukan = "2";
			$tujuan = '';
		}
		if ($flaging != 'IGD') {
			if (
				$flaging != 'RWI' && $asal_pasien != 0
			) {
				$ppkRujukan = $this->cari_referensi_faskes($ppkRujukan, $asal_rujukan);
				if ($ppkRujukan->response == null) {
					$ppkRujukan = $this->input->post('ppkRujukan');
				} else {
					$ppkRujukan = $ppkRujukan->response->faskes[0]->kode;
				}
			}
		} else {
			$noRujukan = "";
			$ppkRujukan = "";
		}

		if ($flaging == 'RWJ') {
			$tujuan = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs where kd_unit = '" . $kd_unit_tujuan . "'")->row()->unit_bpjs;
		}

		/* if ($kdPropinsi != 0 && $kdPropinsi != '') {
			$kdPropinsi = $this->db->query("SELECT kd_propinsi_bpjs FROM propinsi WHERE kd_propinsi = " . $kdPropinsi . "")->row()->kd_propinsi_bpjs;
		}
		if ($kdKabupaten != 0 && $kdKabupaten != '') {
			$kdKabupaten = $this->db->query("SELECT kd_kabupaten_bpjs FROM kabupaten WHERE kd_kabupaten = " . $kdKabupaten . "")->row()->kd_kabupaten_bpjs;
		}
		if ($kdKecamatan != 0 && $kdKecamatan != '') {
			$kdKecamatan = $this->db->query("SELECT kd_kecamatan_bpjs FROM kecamatan WHERE kd_kecamatan = " . $kdKecamatan . "")->row()->kd_kecamatan_bpjs;
		} */


		/* echo 'laka lantas:'.$lakaLantas.'<br>no.lp:'.$no_lp.'<br>suplesi:'.$suplesi.'<br>no suplesi:'.$no_suplesi.'<br>tgl kejadian:'.$tglKejadian;
		echo '<br>keterangan kejadian:'.$keterangan.'<br>lokasi kejadian:'.$kdPropinsi.'|'.$kdKabupaten.'|'.$kdKecamatan;
		die; */
		$json = $this->getJsonData($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $tgl_sep, $asal_rujukan, $asal_pasien, $tujuankunj, $flagprocedure, $assesmentpel, $kdpenunjang, $naikkelas, $pembiayaan, $penanggungjawab, $no_lp, $kd_dpjp_kontrol);

		$penjamin = "";

		if (
			$penjamin_1 != 0 and $penjamin_1 != '0'
		) {
			$penjamin .= "1,";
		}
		if (
			$penjamin_2 != 0 and $penjamin_2 != '0'
		) {
			$penjamin .= "2,";
		}
		if (
			$penjamin_3 != 0 and $penjamin_3 != '0'
		) {
			$penjamin .= "3,";
		}
		if (
			$penjamin_4 != 0 and $penjamin_4 != '0'
		) {
			$penjamin .= "4,";
		}
		// $query = $this->db->query("SELECT * FROM map_unit_bpjs where unit_bpjs = '" . $tujuan . "'");
		$query = $this->db->query("SELECT * FROM map_unit_bpjs where kd_unit = '" . $kd_unit_tujuan . "'");
		if ($query->num_rows() > 0) {
			$kd_unit = $query->row()->kd_unit;
			if ($flaging == 'RWJ') {
				// $this->db->query("UPDATE kunjungan SET kd_unit='".$kd_unit."' WHERE kd_pasien='".$noMR."' AND tgl_masuk='".$tgl_masuk."' AND left(kd_unit,1)='2'"); 
			}
		} else {
			$kd_unit = $unit_tujuan;
		}

		$criteria = array(
			'kd_pasien'	=> $noMR,
			'kd_unit'	=> $kd_unit,
			'tgl_masuk'	=> $tgl_masuk,
		);
		$this->db->select("max(urut_masuk) as urut_masuk ");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			if ($query->row()->urut_masuk == null || $query->row()->urut_masuk == "") {
				$urut_masuk = 0;
			} else {
				$urut_masuk = $query->row()->urut_masuk;
			}
		} else {
			$urut_masuk = 0;
		}

		$url = $this->db->query("SELECT nilai  from seting_bpjs where key_setting='UrlCreateSEP'")->row()->nilai;
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();
		// echo '<pre>' . var_export($json, true) . '</pre>';
		// echo $url;
		// echo '<pre>' . var_export($headers, true) . '</pre>';
		// echo preg_replace("/[^0-9]/", "", $headers[1]);
		// die;
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		// $response = array();

		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		// //$headers[]='Content-Type: Application/x-www-form-urlencoded';
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// $response = curl_exec($ch);
		// // $response = array();

		if ($flaging == "IGD") {
			$query = $this->db->query("SELECT * FROM seting_bpjs WHERE key_setting='PpkPelayanan'");
			if ($query->num_rows() > 0 && strlen($ppkRujukan) == 0) {
				$ppkRujukan = $query->row()->nilai;
			}

			$tglRujukan = date('Y-m-d');
		}
		$respon_array = json_decode($response);
		$respon_encript = $respon_array->response;
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);

		// $respon_array_json = json_encode($respon_array);

		// var_dump($respon_array->response->sep->noSep);
		// echo $timestamp;
		// print("<pre>" . print_r($respon_array, true) . "</pre>");
		// echo $respon_encript;
		// die;
		// var_dump($respon_array->response["sep"]["noSep"]);

		//echo ($respon_array_json->response);
		// print("<pre>" . print_r($respon_array, true) . "</pre>");
		// die;

		$parameter = array(
			'nomor_rujukan' 		=> $noRujukan,
			'kd_dokter_dpjp' 		=> $kodeDPJP,
			'kd_rujukan' 			=> $ppkRujukan,
			'no_kartu' 				=> $noKartu,
			'kelas_rawat' 			=> $klsRawat,
			'tgl_rujukan' 			=> $tglRujukan,
			'no_rujukan' 			=> $noRujukan,
			'ppk_rujukan' 			=> $ppkRujukan,
			'catatan' 				=> $catatan,
			'diag_awal' 			=> $diagAwal,
			'poli_tujuan' 			=> $tujuan,
			'eksekutif' 			=> $eksekutif,
			'tgl_kejadian' 			=> $tglKejadian,
			'keterangan_kejadian' 	=> $keterangan,
			'kd_prov' 				=> $kdPropinsi,
			'kd_kab' 				=> $kdKabupaten,
			'kd_kec' 				=> $kdKecamatan,
			'no_surat_dpjp' 		=> $noSurat,
			'no_tlp' 				=> $noTelp,
			'cob' 					=> $cob,
			'katarak' 				=> $katarak,
			'lakalantas' 			=> $lakaLantas,
			'suplesi' 				=> $suplesi,
			'kd_dpjp' 				=> $kodeDPJP,
			'no_suplesi' 			=> $no_suplesi,
			'penjamin_1' 			=> $penjamin_1,
			'penjamin_2' 			=> $penjamin_2,
			'penjamin_3' 			=> $penjamin_3,
			'penjamin_4' 			=> $penjamin_4,
			'kd_pasien' 			=> $noMR,
			'kd_unit' 				=> $kd_unit,
			'tgl_masuk' 			=> $tgl_masuk,
			'urut_masuk' 			=> $urut_masuk,
			'asal_rujukan' 			=> $asal_rujukan,
		);
		if (($respon_array->metaData->code == '200' || $respon_array->metaData->code == 200) && count($respon_array) > 0) {
			$result = true;
			$parameter['no_sep'] 		= $respon_array->response->sep->noSep;
			$parameter['tujuan_kunj'] 	= $tujuankunj;
			$parameter['flagprocedure'] = $flagprocedure;
			$parameter['kd_penunjang'] 	= $assesmentpel;
			$parameter['assementpel'] 	= $kdpenunjang;
			$parameter['no_lp'] 		= $no_lp;
			$parameter['prolanisPRB'] 	= $respon_array->response->sep->informasi->prolanisPRB;
		} else {
			$result = false;
			$parameter['no_sep'] = "";
		}

		// echo var_dump($parameter);
		// die;

		if ($result > 0 || $result === true) {
			unset($criteria);
			unset($params);
			if ($kd_pasien_asal == "" && $kd_unit_asal == "" && $tgl_masuk_asal == "" && $urut_masuk_asal == "") {
				$criteria = array(
					'kd_pasien'  => $parameter['kd_pasien'],
					'kd_unit'    => $parameter['kd_unit'],
					'tgl_masuk'  => $parameter['tgl_masuk'],
					'urut_masuk' => $parameter['urut_masuk'],
				);
			} else {
				$criteria = array(
					'kd_pasien'  => $kd_pasien_asal,
					'kd_unit'    => $kd_unit_asal,
					'tgl_masuk'  => $tgl_masuk_asal,
					'urut_masuk' => $urut_masuk_asal,
				);
			}

			$params = array(
				'no_sjp'  => $respon_array->response->sep->noSep,
			);

			$query = $this->get(
				array(
					'select' 	=> " * ",
					'criteria' 	=> array(
						'kd_dokter_dpjp' 	=> $parameter['kd_dpjp'],
					),
					'table' 	=> " map_dokter_bpjs "
				)
			);
			if ($query->num_rows() > 0) {
				// $params['kd_dokter'] = $query->row()->kd_dokter;
			}

			$query = $this->get(
				array(
					'select' 	=> " * ",
					'criteria' 	=> array(
						'unit_bpjs' 	=> $parameter['poli_tujuan'],
					),
					'table' 	=> " map_unit_bpjs "
				)
			);
			if ($query->num_rows() > 0) {
				// $params['kd_unit'] = $query->row()->kd_unit;
			}
			$query = $this->get(
				array(
					'select' 	=> " * ",
					'criteria' 	=> array(
						'kd_prov_perujuk' 	=> $parameter['ppk_rujukan'],
					),
					'table' 	=> " rujukan "
				)
			);
			if ($query->num_rows() > 0) {
				$params['kd_rujukan'] = $query->row()->kd_rujukan;
			}
			if ($flaging == "RWI") {
				$params['hak_kelas'] = $parameter['kelas_rawat'];
			}
			$this->db->where($criteria);
			$this->db->update("kunjungan", $params);
		} else {
			$result = false;
		}

		if (($result > 0 || $result === true) && $flaging == "IGD") {
			unset($criteria);
			unset($params);
			if ($kd_pasien_asal == "" && $kd_unit_asal == "" && $tgl_masuk_asal == "" && $urut_masuk_asal == "") {
				$criteria = array(
					'kd_pasien'  => $parameter['kd_pasien'],
					'kd_unit'    => $parameter['kd_unit'],
					'tgl_masuk'  => $parameter['tgl_masuk'],
					'urut_masuk' => $parameter['urut_masuk'],
				);
			} else {
				$criteria = array(
					'kd_pasien'  => $kd_pasien_asal,
					'kd_unit'    => $kd_unit_asal,
					'tgl_masuk'  => $tgl_masuk_asal,
					'urut_masuk' => $urut_masuk_asal,
				);
			}
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from("mr_penyakit");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$params = array(
					'kd_penyakit' 	=> $parameter['diag_awal'],
				);
				$this->db->where($criteria);
				$this->db->update("mr_penyakit", $params);
			} else {
				$params = array(
					'kd_penyakit' 	=> $parameter['diag_awal'],
					'kd_pasien'   	=> $criteria['kd_pasien'],
					'kd_unit'     	=> $criteria['kd_unit'],
					'tgl_masuk'   	=> $criteria['tgl_masuk'],
					'urut_masuk'  	=> $criteria['urut_masuk'],
					'urut' 			=> '1',
					'stat_diag' 	=> '0',
					'kasus' 		=> 'true',
					'tindakan' 		=> '99',
					'perawatan' 	=> '99'
				);
				$this->db->insert("mr_penyakit", $params);
			}
			$result = $this->db->affected_rows();
		}

		if ($result > 0 || $result === true) {
			$table = $this->get_structure('rujukan_kunjungan');
			if ($table->num_rows() > 0) {
				$params = array();
				unset($params);
				unset($criteria);
				if (
					$kd_pasien_asal == "" && $kd_unit_asal == "" && $tgl_masuk_asal == "" && $urut_masuk_asal == ""
				) {
					$criteria = array(
						'kd_pasien'  => $parameter['kd_pasien'],
						'kd_unit'    => $parameter['kd_unit'],
						'tgl_masuk'  => $parameter['tgl_masuk'],
						'urut_masuk' => $parameter['urut_masuk'],
					);
				} else {
					$criteria = array(
						'kd_pasien'  => $kd_pasien_asal,
						'kd_unit'    => $kd_unit_asal,
						'tgl_masuk'  => $tgl_masuk_asal,
						'urut_masuk' => $urut_masuk_asal,
					);
				}
				foreach ($parameter as $key => $res_param) {
					foreach ($table->result() as $res_table) {
						if (strtolower($key) ==  strtolower($res_table->column_name)) {
							$params[$key] = $res_param;
						}
					}
				}
				$this->db->select("*");
				$this->db->where($criteria);
				$this->db->from("rujukan_kunjungan");
				$query = $this->db->get();

				if ($query->num_rows() > 0) {
					$this->db->where($criteria);
					$this->db->update("rujukan_kunjungan", $params);
				} else {
					$this->db->insert("rujukan_kunjungan", $params);
				}
				$result = $this->db->affected_rows();
			}
		} else {
			$result = false;
		}

		if ($result > 0 || $result === true) {
			$table = $this->get_structure('history_sep_bpjs');
			if ($table->num_rows() > 0) {
				$params = array();
				unset($params);
				unset($criteria);

				if (
					$kd_pasien_asal == "" && $kd_unit_asal == "" && $tgl_masuk_asal == "" && $urut_masuk_asal == ""
				) {
					$criteria = array(
						'kd_pasien'  => $parameter['kd_pasien'],
						'kd_unit'    => $parameter['kd_unit'],
						'tgl_masuk'  => $parameter['tgl_masuk'],
						'urut_masuk' => $parameter['urut_masuk'],
					);
				} else {
					$criteria = array(
						'kd_pasien'  => $kd_pasien_asal,
						'kd_unit'    => $kd_unit_asal,
						'tgl_masuk'  => $tgl_masuk_asal,
						'urut_masuk' => $urut_masuk_asal,
					);
				}
				$this->db->select("*");
				$this->db->where($criteria);
				$this->db->from("history_sep_bpjs");
				$query = $this->db->get();

				foreach ($parameter as $key => $res_param) {
					foreach ($table->result() as $res_table) {
						if (strtolower($key) ==  strtolower($res_table->column_name)) {
							$params[$key] = $res_param;
						}
					}
				}
				// if ($query->num_rows() > 0) {
				// $this->db->where($criteria);
				// $this->db->update("history_sep_bpjs", $params);
				// } else {
				$this->db->insert("history_sep_bpjs", $params);
				// }
				// -------------------Cek Rencana Kontrol/SPRI//Egi//24-03-2022-------------------------------
				$cek_surat_kontrol = $this->db->query("SELECT * FROM history_surat_kontrol_bpjs WHERE no_surat_kontrol='" . $parameter['no_surat_dpjp'] . "'");
				if ($cek_surat_kontrol->num_rows() > 0) {
					$this->db->query("UPDATE history_surat_kontrol_bpjs SET status=TRUE WHERE no_surat_kontrol='" . $parameter['no_surat_dpjp'] . "'");
					$result = $this->db->affected_rows();
				}
				// -------------------------------------------------------------------------------------------
			}
		} else {
			$result = false;
		}
		if ($result > 0 || $result === true) {
			// $this->insert_response_bpjs($url, $respon_array, $json, "POST");
			$this->db->trans_commit();
		} else {
			$respon_array->response = $respon_encript;
			$this->db->trans_rollback();
		}
		$this->db->close();
		$this->insert_response_bpjs($url, $respon_array, $json, "POST", $timestamp);
		echo json_encode($respon_array);
	}

	// -------------------------------generate no surat kontrol--------------------------------------------------------------------
	// egi 2021-11-01
	public function generate_no_surat_kontrol()
	{
		$noka = $this->input->post('noka');
		$kd_unit = $this->input->post('kd_unit');
		$kunjungan_pertama = $this->input->post('kunjungan_pertama');
		$sep_rwi = $this->input->post('sep_rwi');
		$succes = true;
		$kode_poli = '';
		$nama_poli = "";
		$kode_dokter = '';
		$nama_dokter = '';
		$diag_awal = '';
		$kd_prov = '';
		$kd_kab = '';
		$kd_kec = '';
		$no_tlp = '';
		$kd_dpjp = '';
		$str_tglRujukan = '';
		$surat_kontrol = '';

		if ($kunjungan_pertama == 'true') {
			// $sql = $this->db->query("SELECT count(*) as no_surat FROM history_sep_bpjs")->row();
			// if ($sql->no_surat != '') {
			// 	$format = "000000";
			// 	$surat_kontrol = substr($format, 0, -strlen($sql->no_surat + 1)) . ($sql->no_surat + 1);
			// } else {

			if ($sep_rwi == '' || $sep_rwi == 'null') {
				if (substr($kd_unit, 0, 1) == 1) {
					// $sql = $this->db->query("SELECT no_surat_kontrol as no_surat FROM history_surat_kontrol_bpjs a INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  WHERE a.noka='" . $noka . "'")->row();
					$sql = $this->db->query("SELECT no_surat_kontrol as no_surat,tgl_kontrol AS tgl_rujukan 
					FROM history_surat_kontrol_bpjs a 
					LEFT JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  
					WHERE a.noka='" . $noka . "' AND a.jenis_kontrol='1'
					AND a.tgl_kontrol='" . date('Y-m-d') . "'
					AND (a.status=FALSE OR a.status is null)
					ORDER BY a.tgl_kontrol DESC limit 1");
				} else {
					$surat_kontrol = "";
				}
			} else {
				$sql = $this->db->query("SELECT no_surat_kontrol as no_surat,tgl_kontrol AS tgl_rujukan,
				a.kode_poli, a.nama_poli,a.kode_dokter,a.nama_dokter, b.diag_awal, 
				b.kd_prov,b.kd_kec,b.kd_kab,b.no_tlp,b.kd_dpjp  
				FROM history_surat_kontrol_bpjs a 
				INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  
				WHERE a.no_sep='" . $sep_rwi . "' AND a.jenis_kontrol='2'
				AND a.tgl_kontrol='" . date('Y-m-d') . "'
				AND (a.status=FALSE OR a.status is null)
				ORDER BY a.tgl_kontrol DESC limit 1");
			}

			// if ($sql) {
			// 	if ($sql->no_surat != '') {
			// 		$surat_kontrol = $sql->no_surat;
			// 		$str_tglRujukan = $sql->tgl_rujukan;
			// 	} else {
			// 		$succes = false;
			// 	}
			// } else {
			// 	$succes = false;
			// }
			// }
		} else {
			if ($sep_rwi == '' || $sep_rwi == 'null') {
				if (substr($kd_unit, 0, 1) == 1) {
					// $sql = $this->db->query("SELECT no_surat_kontrol as no_surat FROM history_surat_kontrol_bpjs a INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  WHERE a.noka='" . $noka . "'")->row();
					$sql = $this->db->query("SELECT no_surat_kontrol as no_surat,tgl_kontrol AS tgl_rujukan 
					FROM history_surat_kontrol_bpjs a 
					LEFT JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  
					WHERE a.noka='" . $noka . "' AND a.jenis_kontrol='1' 
					AND a.tgl_kontrol='" . date('Y-m-d') . "'
					AND (a.status=FALSE OR a.status is null)
					ORDER BY a.tgl_kontrol DESC limit 1");
				} else {
					// $sql = $this->db->query("SELECT no_surat_kontrol as no_surat FROM history_surat_kontrol_bpjs a INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep and a.kode_poli = b.poli_tujuan WHERE a.noka='" . $noka . "' AND b.kd_unit='" . $kd_unit . "'")->row();
					$sql = $this->db->query("SELECT no_surat_kontrol as no_surat,tgl_kontrol AS tgl_rujukan,a.kode_poli, 
					a.nama_poli,a.kode_dokter,a.nama_dokter, b.diag_awal, b.kd_prov,b.kd_kec,b.kd_kab,b.no_tlp,b.kd_dpjp 
					FROM history_surat_kontrol_bpjs a 
					LEFT JOIN history_sep_bpjs b ON a.no_sep=b.no_sep and a.kode_poli = b.poli_tujuan 
					WHERE a.noka='" . $noka . "' AND b.kd_unit='" . $kd_unit . "' AND a.jenis_kontrol='2' 
					AND a.tgl_kontrol='" . date('Y-m-d') . "'
					AND (a.status=FALSE OR a.status is null)
					ORDER BY a.tgl_kontrol DESC limit 1");
				}
			} else {
				$sql = $this->db->query("SELECT no_surat_kontrol as no_surat,tgl_kontrol AS tgl_rujukan,a.kode_poli, 
				a.nama_poli,a.kode_dokter,a.nama_dokter, b.diag_awal, b.kd_prov,b.kd_kec,b.kd_kab,b.no_tlp,b.kd_dpjp  
				FROM history_surat_kontrol_bpjs a 
				INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep  
				WHERE a.no_sep='" . $sep_rwi . "' AND a.jenis_kontrol='2'
				AND a.tgl_kontrol='" . date('Y-m-d') . "'
				AND (a.status=FALSE OR a.status is null)
				ORDER BY a.tgl_kontrol DESC limit 1");
			}

			// $sql = $this->db->query("SELECT no_surat_kontrol as no_surat FROM history_surat_kontrol_bpjs a INNER JOIN history_sep_bpjs b ON a.no_sep=b.no_sep and b.poli_tujuan = a.kode_poli WHERE a.noka='" . $noka . "' AND b.kd_unit='" . $kd_unit . "'")->row();
		}
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$sql = $sql->row();
				$surat_kontrol = $sql->no_surat;
				$str_tglRujukan = $sql->tgl_rujukan;
				if (substr($kd_unit, 0, 1) != 1) {
					$kode_poli = $sql->kode_poli;
					$nama_poli = $sql->nama_poli;
					$kode_dokter = $sql->kode_dokter;
					$nama_dokter = $sql->nama_dokter;
					$kd_dpjp = $sql->kd_dpjp;
				}
			} else {
				$url = $this->db->query("select nilai  from seting_bpjs where key_setting='urlGetNoSuratKontrolByNoka'")->row()->nilai;
				$headers = $this->getSignature_new();
				$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
				$urlnya = $url . '' . date('m') . '/Tahun/' . date('Y') . '/Nokartu/' . $noka . '/filter/1';
				$method = "GET"; // POST / PUT / DELETE
				$postdata = "";
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $urlnya);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

				$response = curl_exec($curl);
				curl_close($curl);
				$res = json_decode($response);
				$res->response = $this->decompress($res->response, $timestamp);
				if ($res->metaData->code == '200' || $res->metaData->code == 200) {
					foreach ($res->response->list as $value) {
						// echo $value->terbitSEP;
						if ($sep_rwi == '' || $sep_rwi == 'null') {
							if (substr($kd_unit, 0, 1) == 1) {
								// echo $value->jnsKontrol;
								if ($value->jnsKontrol == 1 && $value->terbitSEP == 'Belum' && $value->tglRencanaKontrol == date('Y-m-d')) {
									$sql = $value;
									$surat_kontrol = $value->noSuratKontrol;
									$str_tglRujukan = $value->tglRencanaKontrol;
									break;
								}
							} else {
								$unit_bpjs = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $kd_unit . "'")->row()->unit_bpjs;
								if ($value->jnsKontrol == 2 && $value->terbitSEP == 'Belum' && $value->tglRencanaKontrol == date('Y-m-d') && $value->poliTujuan == $unit_bpjs && $value->jnsPelayanan == 'Rawat Jalan') {
									$sql = $value;
									$surat_kontrol = $value->noSuratKontrol;
									$str_tglRujukan = $value->tglRencanaKontrol;
									$kode_poli = $value->poliTujuan;
									$nama_poli = $value->namaPoliTujuan;
									$kode_dokter = $value->kodeDokter;
									$nama_dokter = $value->namaDokter;

									$url_sep = $this->db->query("SELECT nilai FROM seting_bpjs WHERE key_setting='UrlCariSEP'")->row()->nilai;
									$headers = $this->getSignature_new();
									$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
									$urlnya_sep = $url_sep . $value->noSepAsalKontrol;
									$method = "GET"; // POST / PUT / DELETE
									$postdata = "";
									$curl2 = curl_init();
									curl_setopt($curl2, CURLOPT_URL, $urlnya_sep);
									curl_setopt($curl2, CURLOPT_HEADER, false);
									curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, $method);
									curl_setopt($curl2, CURLOPT_POSTFIELDS, $postdata);

									curl_setopt($curl2, CURLOPT_FOLLOWLOCATION, true);
									curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
									curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, false);
									curl_setopt($curl2, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
									curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);
									curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);

									$response2 = curl_exec($curl2);
									curl_close($curl2);
									$res_sep = json_decode($response2);
									$res_sep->response = $this->decompress($res_sep->response, $timestamp);
									// echo '<pre>' . var_export($res_sep->response, true) . '</pre>';
									// die;
									if ($res_sep->metaData->code == '200' || $res_sep->metaData->code == 200) {
										$kd_dpjp = $res_sep->response->dpjp->kdDPJP;
										break;
									}
								}
							}
						} else {
							if (
								$value->jnsKontrol == 2 && $value->terbitSEP == 'Belum' && $value->noSepAsalKontrol == $sep_rwi && $value->tglRencanaKontrol == date('Y-m-d')
							) {
								$sql = $value;
								$surat_kontrol = $value->noSuratKontrol;
								$str_tglRujukan = $value->tglRencanaKontrol;
								$kode_poli = $value->poliTujuan;
								$nama_poli = $value->namaPoliTujuan;
								$kode_dokter = $value->kodeDokter;
								$nama_dokter = $value->namaDokter;
								$kd_dpjp = $value->kodeDokter;
								break;
							}
						}
					}
				} else {
					$succes = false;
				}
			}
		} else {
			$succes = false;
		}

		// if ($sep_rwi == '') {
		// } else {
		// 	$kode_poli = $sql->kode_poli;
		// 	$nama_poli = $sql->nama_poli;
		// 	$kode_dokter = $sql->kode_dokter;
		// 	$nama_dokter = $sql->nama_dokter;
		// 	$diag_awal = $sql->diag_awal;
		// 	$kd_prov = $sql->kd_prov;
		// 	$kd_kab = $sql->kd_kab;
		// 	$kd_kec = $sql->kd_kec;
		// 	$no_tlp = $sql->no_tlp;
		// 	$kd_dpjp = $sql->kd_dpjp;
		// }

		echo '{succes:"' . $succes . '",surat_kontrol:"' . $surat_kontrol . '",tgl_rujukan:"' . $str_tglRujukan . '",kode_poli:"' . $kode_poli . '",nama_poli:"' . $nama_poli . '",kode_dokter:"' . $kode_dokter . '",nama_dokter:"' . $nama_dokter . '",diag_awal:"' . $diag_awal . '",kd_prov:"' . $kd_prov . '",kd_kab:"' . $kd_kab . '",kd_kec:"' . $kd_kec . '",no_tlp:"' . $no_tlp . '",kd_dpjp:"' . $kd_dpjp . '" }';
	}
	// ----------------------------------------------------------------------------------------------------------------------------

	// -------------------------Pengecekan Surat Kontrol Bpjs---------------------
	// Egi 22 Febuari 2022
	public function cek_surat_kontrol_bpjs()
	{
		$no_surat = $this->input->post('no_surat');

		$url = $this->db->query("SELECT nilai  FROM seting_bpjs WHERE key_setting='UrlCariRencanaKontrol'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		//---------------------------cari Rencana Kontrol--------------------------------------//
		$urlnya = $url . $no_surat;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		if ($res->metaData->code != 200) {
			$this->db->query("UPDATE history_surat_kontrol_bpjs SET status=TRUE WHERE no_surat_kontrol='" . $no_surat . "'");
			$status = 'true';
		} else {
			$this->db->query("UPDATE history_surat_kontrol_bpjs SET status=FALSE WHERE no_surat_kontrol='" . $no_surat . "'");
			$status = 'false';
		}
		echo '{status:' . $status . ',data:' . json_encode($res) . '}';
	}
	// ---------------------------------------------------------------------------

	public function GenerateSEP_()
	{
		$tglRujukan   = date('Y-m-d', strtotime($this->input->post('tglRujukan')));
		$flaging      = $this->input->post('flaging');
		$noKartu      = $this->input->post('noKartu');
		$klsRawat     = $this->input->post('klsRawat');
		$noMR         = $this->input->post('noMR');
		$asal_rujukan = $this->input->post('asal_rujukan');
		$noRujukan    = $this->input->post('noRujukan');
		$ppkRujukan   = $this->input->post('ppkRujukan');
		$catatan      = $this->input->post('catatan');
		$diagAwal     = $this->input->post('diagAwal');
		$tujuan       = $this->input->post('tujuan');
		$eksekutif    = $this->input->post('eksekutif');
		$penjamin_1   = $this->input->post('penjamin_1');
		$penjamin_2   = $this->input->post('penjamin_2');
		$penjamin_3   = $this->input->post('penjamin_3');
		$penjamin_4   = $this->input->post('penjamin_4');
		$tglKejadian  = $this->input->post('tglKejadian');
		$keterangan   = $this->input->post('keterangan');
		$kdPropinsi   = $this->input->post('kdPropinsi');
		$kdKabupaten  = $this->input->post('kdKabupaten');
		$kdKecamatan  = $this->input->post('kdKecamatan');
		$noSurat      = $this->input->post('noSurat');
		$noTelp       = $this->input->post('noTelp');
		$katarak      = $this->input->post('katarak');
		$cob          = $this->input->post('cob');
		$suplesi      = $this->input->post('suplesi');
		$kodeDPJP     = $this->input->post('kodeDPJP');
		$no_suplesi   = $this->input->post('no_suplesi');
		$lakaLantas   = $this->input->post('lakaLantas');
		$tgl_sep      = $this->input->post('tgl_sep');
		$asal_pasien  = $this->input->post('asal_pasien');
		$unit_tujuan  = $this->input->post('unit_tujuan');
		$asal_pasien  = $this->input->post('asal_pasien');
		$kd_unit 	  = "";
		$urut_masuk	  = "";
		$tgl_masuk	  = date('Y-m-d');

		if ($flaging == "RWI") {
			if (isset($asal_pasien) === true || strlen($asal_pasien) > 0) {
				if ($asal_pasien == 0 || $asal_pasien == '0') {
					$asal_rujukan = "2";
				} else if ($asal_pasien == 1 || $asal_pasien == '1') {
					$asal_rujukan = "1";
				} else {
					$asal_rujukan = "2";
				}
			}
		}
		$json = $this->getJsonData($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $tgl_sep, $asal_rujukan, $asal_pasien);

		$penjamin = "";

		if ($penjamin_1 != 0 and $penjamin_1 != '0') {
			$penjamin .= "1,";
		}
		if ($penjamin_2 != 0 and $penjamin_2 != '0') {
			$penjamin .= "2,";
		}
		if ($penjamin_3 != 0 and $penjamin_3 != '0') {
			$penjamin .= "3,";
		}
		if ($penjamin_4 != 0 and $penjamin_4 != '0') {
			$penjamin .= "4,";
		}
		$query = $this->db->query("SELECT * FROM map_unit_bpjs where unit_bpjs = '" . $tujuan . "'");
		if ($query->num_rows() > 0) {
			$kd_unit = $query->row()->kd_unit;
		} else {
			$kd_unit = $unit_tujuan;
		}

		$criteria = array(
			'kd_pasien'	=> $noMR,
			'kd_unit'	=> $kd_unit,
			'tgl_masuk'	=> $tgl_masuk,
		);
		$this->db->select(" max(urut_masuk) as urut_masuk ");
		$this->db->where($criteria);
		$this->db->from("kunjungan");
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			if ($query->row()->urut_masuk == null || $query->row()->urut_masuk == "") {
				$urut_masuk = 0;
			} else {
				$urut_masuk = $query->row()->urut_masuk;
			}
		} else {
			$urut_masuk = 0;
		}


		$url = $this->db->query("SELECT nilai  from seting_bpjs where key_setting='UrlCreateSEP'")->row()->nilai;
		$headers = $this->getSignatureVedika_new();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);

		if ($flaging == "IGD") {
			$query = $this->db->query("SELECT * FROM seting_bpjs WHERE key_setting='PpkPelayanan'");
			if ($query->num_rows() > 0 && strlen($ppkRujukan) == 0) {
				$ppkRujukan = $query->row()->nilai;
			}

			$tglRujukan = date('Y-m-d');
		}
		$respon_array = json_decode($response);
		if (($respon_array->metaData->code == '200' || $respon_array->metaData->code == 200) && count($respon_array) > 0) {
			$parameter = array(
				'nomor_rujukan' 		=> $noRujukan,
				'kd_dokter_dpjp' 		=> $kodeDPJP,
				'kd_rujukan' 			=> $ppkRujukan,
				'no_kartu' 				=> $noKartu,
				'kelas_rawat' 			=> $klsRawat,
				'tgl_rujukan' 			=> $tglRujukan,
				'no_rujukan' 			=> $noRujukan,
				'ppk_rujukan' 			=> $ppkRujukan,
				'catatan' 				=> $catatan,
				'diag_awal' 			=> $diagAwal,
				'poli_tujuan' 			=> $tujuan,
				'eksekutif' 			=> $eksekutif,
				'tgl_kejadian' 			=> $tglKejadian,
				'keterangan_kejadian' 	=> $keterangan,
				'kd_prov' 				=> $kdPropinsi,
				'kd_kab' 				=> $kdKabupaten,
				'kd_kec' 				=> $kdKecamatan,
				'no_surat_dpjp' 		=> $noSurat,
				'no_tlp' 				=> $noTelp,
				'cob' 					=> $cob,
				'katarak' 				=> $katarak,
				'lakalantas' 			=> $lakaLantas,
				'suplesi' 				=> $suplesi,
				'kd_dpjp' 				=> $kodeDPJP,
				'no_suplesi' 			=> $no_suplesi,
				'penjamin_1' 			=> $penjamin_1,
				'penjamin_2' 			=> $penjamin_2,
				'penjamin_3' 			=> $penjamin_3,
				'penjamin_4' 			=> $penjamin_4,
				'no_sep' 				=> $respon_array->response->sep->noSep,
				'kd_pasien' 			=> $noMR,
				'kd_unit' 				=> $kd_unit,
				'tgl_masuk' 			=> $tgl_masuk,
				'urut_masuk' 			=> $urut_masuk,
				'asal_rujukan' 			=> $asal_rujukan,
			);
			$table = $this->get_structure('history_sep_bpjs');
			if ($table->num_rows() > 0) {
				$params = array();
				unset($params);
				foreach ($parameter as $key => $res_param) {
					foreach ($table->result() as $res_table) {
						if (strtolower($key) ==  strtolower($res_table->column_name)) {
							$params[$key] = $res_param;
						}
					}
				}
				$this->db->insert("history_sep_bpjs", $params);
			}

			$table = $this->get_structure('rujukan_kunjungan');
			if ($table->num_rows() > 0) {
				$params = array();
				unset($params);
				foreach ($parameter as $key => $res_param) {
					foreach ($table->result() as $res_table) {
						if (strtolower($key) ==  strtolower($res_table->column_name)) {
							$params[$key] = $res_param;
							// echo $res_param."<br>";
						}
					}
				}
				$this->db->insert("rujukan_kunjungan", $params);
			}

			unset($criteria);
			unset($params);
			$criteria = array(
				'kd_pasien'  => $parameter['kd_pasien'],
				'kd_unit'    => $parameter['kd_unit'],
				'tgl_masuk'  => $parameter['tgl_masuk'],
				'urut_masuk' => $parameter['urut_masuk'],
			);
			$params = array(
				'no_sjp'  => $respon_array->response->sep->noSep,
			);

			$this->db->where($criteria);
			$this->db->update("kunjungan", $params);
		}

		$this->insert_response_bpjs($url, json_decode($response), $json, "POST");
		echo $response;
	}

	private function get_structure($table)
	{
		$query = "SELECT column_name FROM information_schema.columns WHERE table_name = '" . $table . "'";
		return $this->db->query($query);
	}


	private function get($parameter)
	{
		if (isset($parameter['select'])) {
			$this->db->select($parameter['select']);
		} else {
			$this->db->select("*");
		}

		if (isset($parameter['criteria'])) {
			$this->db->where($parameter['criteria']);
		}

		$this->db->from($parameter['table']);
		return $this->db->get();
	}

	private function getSignatureVedika_new()
	{
		/* $tmp_secretKey=$this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID=  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID."&".$tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: ".$tmp_costumerID,"X-Timestamp: ".$tStamp,"X-Signature: ".$encodedSignature); */

		$tmp_secretKey = $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerSecret'")->row()->nilai;
		$tmp_costumerID =  $this->db->query("select nilai  from seting_bpjs where key_setting='ConsumerID'")->row()->nilai;
		$tmp_userkey =  $this->db->query("select nilai  from seting_bpjs where key_setting='user_key'")->row()->nilai;
		date_default_timezone_set('UTC');
		$tStamp = time();
		$signature = hash_hmac('sha256', $tmp_costumerID . "&" . $tStamp, $tmp_secretKey, true);
		$encodedSignature = base64_encode($signature);
		return array("X-Cons-ID: " . $tmp_costumerID, "X-Timestamp: " . $tStamp, "X-Signature: " . $encodedSignature, "user_key: " . $tmp_userkey);
	}

	public function cari_data_diagnosa()
	{
		/* $nama_diagnosa=$this->input->post('nama_diagnosa');
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_diagnosa'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$nama_diagnosa,false,$context));
		if($res->metaData->code==200){
			echo '{success:true, totalrecords:'.count($res->response->diagnosa).', listData:'.json_encode($res->response->diagnosa).'}';
		}else{
			echo json_encode($res);
		}
		$this->insert_response_bpjs($url.$nama_diagnosa, $res, json_encode($res), "GET"); */

		$nama_diagnosa = $this->input->post('nama_diagnosa');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai from seting_bpjs where key_setting='url_cari_diagnosa'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $nama_diagnosa, false, $context));
		$urlnya = $url . $nama_diagnosa;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		if ($res->metaData->code == 200) {
			echo '{success:true, totalrecords:' . count($res->response->diagnosa) . ', listData:' . json_encode($res->response->diagnosa) . '}';
		} else {
			echo json_encode($res);
		}
		// $this->insert_response_bpjs($url . $nama_diagnosa, $res, json_encode($res), "GET");
	}

	public function get_poli_rujukan()
	{
		$nama_poli = $this->input->post('nama_poli_rujuk');
		$query = $this->db->query("SELECT b.unit_bpjs as KODE, a.nama_unit as NAMA, a.kd_unit as kode_unit_nci from   unit a 
			inner join map_unit_bpjs b on a.kd_unit=b.kd_unit")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	public function get_dokter_pertama()
	{
		$kd_pasien = $this->input->post('kd_pasien');
		$no_rujukan = $this->input->post('no_rujukan');
		$query = $this->db->query("SELECT c.nama, a.kd_dokter_dpjp FROM rujukan_kunjungan a inner join map_dokter_bpjs b on 
								 a.kd_dokter_dpjp = b.kd_dokter_dpjp 
								 inner join dokter c on b.kd_dokter=c.kd_dokter
								 WHERE nomor_rujukan = '" . $no_rujukan . "'  AND kd_pasien = '" . $kd_pasien . "'
								 and urut_masuk='0' ")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	public function get_dokter_map_bpjs()
	{
		$kd_dokter_dpjp = $this->input->post('kd_dokter_dpjp');
		$query = $this->db->query("SELECT * FROM dokter a inner join map_dokter_bpjs b on a.kd_dokter=b.kd_dokter 
			WHERE b.kd_dokter_dpjp='" . $kd_dokter_dpjp . "' ")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	// 22 Feb 2022 By DP --> Revisi Pencarian Rujukan FKTP Berdasarkan No. Kartu
	public function getDataBpjs_by_nokartuPeserta()
	{
		$now = date("Y-m-d");
		$no_kartu = $_POST['no_kartu'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanFKTPByNoka'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);

		// echo $url.$no_kartu;
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url.'/'.$no_kartu,false,$context));

		$urlnya = $url . $no_kartu;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		// $this->insert_response_bpjs($url . $no_kartu . "/tglSEP/" . date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat);
	}

	public function hapus_nomor_sep()
	{
		$no_sep = $this->input->post('no_sep');
		$update = $this->db->query("UPDATE kunjungan set no_sjp='-' where no_sjp='" . $no_sep . "' ");
		echo '{success:true}';
	}

	/*public function get_nomor_rujukan_from_rwj(){
		$kd_pasien=$this->input->post("kd_pasien");
		$query=$this->db->query("SELECT * from rujukan_kunjungan WHERE kd_pasien='".$kd_pasien."' and LENGTH(nomor_rujukan)!=18
								 ORDER BY tgl_masuk  DESC NULLS LAST LIMIT 1")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';

	}*/

	public function get_nomor_sep_from_rwj()
	{
		$kd_pasien	 = $this->input->post("kd_pasien");
		//HUDI
		//20-11-2020
		//Penambahan asal pasien
		$asal_pasien = $this->input->post("asal_pasien");
		$kd_unit	 = '';
		if ($asal_pasien == 0) {
			$kd_unit = 3;
		} else {
			$kd_unit = 2;
		}
		//	$no_asuransi$this->input->post("no_asuransi");
		$query = $this->db->query("SELECT no_sjp from kunjungan where kd_pasien = '" . $kd_pasien . "' and kd_unit like '" . $kd_unit . "%' order by tgl_masuk desc limit 1")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	public function generate_nomor_rujukan()
	{
		$nilai = "000";
		$hari = date("d");
		$bulan = date("m");
		$tahun = date("Y");
		$get_ppk_pelayanan = $this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'")->row()->nilai;
		$count 			   = $this->db->query("SELECT count(*) as count from rujukan_kunjungan where tgl_masuk = '" . date("Y-m-d") . "'");
		if ($count->num_rows() > 0) {
			$count = (int)$count->row()->count + 1;
		} else {
			$count = 1;
		}

		$nilai = substr($nilai, strlen($count), strlen($nilai)) . (string)$count;
		$noRujukan = $get_ppk_pelayanan . $hari . $bulan . $tahun . $nilai;
		$no_rujukan = str_replace("R", "r", $noRujukan);
		echo '{success:true, totalrecords:' . count($get_ppk_pelayanan) . ', listData:' . json_encode($noRujukan) . '}';
	}

	public function get_nomor_rujukan()
	{
		$response = array();
		$hari = date("d");
		$bulan = date("m");
		$tahun = date("Y");
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'urut_masuk' 	=> $this->input->post('urut_masuk'),
		);
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from("history_sep_bpjs");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			if ($query->row()->kd_unit == '31') {
				// $noRujukan = $query->row()->no_sep;
				$response['no_rujukan'] 			= $query->row()->no_sep;
				$response['lakalantas'] 			= $query->row()->lakalantas;
				$response['no_lp'] 					= $query->row()->no_lp;
				$response['suplesi'] 				= $query->row()->suplesi;
				$response['no_suplesi'] 			= $query->row()->no_suplesi;
				$response['tgl_kejadian'] 			= $query->row()->tgl_kejadian;
				$response['keterangan_kejadian'] 	= $query->row()->keterangan_kejadian;
				$response['kd_prov_kejadian'] 		= $query->row()->kd_prov;
				$response['kd_kab_kejadian'] 		= $query->row()->kd_kab;
				$response['kd_kec_kejadian'] 		= $query->row()->kd_kec;
			} else {
				// $noRujukan = $query->row()->no_rujukan;
				$response['no_rujukan'] 			= $query->row()->no_rujukan;
				$response['lakalantas'] 			= $query->row()->lakalantas;
				$response['no_lp'] 					= $query->row()->no_lp;
				$response['suplesi'] 				= $query->row()->suplesi;
				$response['no_suplesi'] 			= $query->row()->no_suplesi;
				$response['tgl_kejadian'] 			= $query->row()->tgl_kejadian;
				$response['keterangan_kejadian'] 	= $query->row()->keterangan_kejadian;
				$response['kd_prov_kejadian'] 		= $query->row()->kd_prov;
				$response['kd_kab_kejadian'] 		= $query->row()->kd_kab;
				$response['kd_kec_kejadian'] 		= $query->row()->kd_kec;
			}
			// $noRujukan = $query->row()->no_rujukan;
			$count = $query->num_rows();
		} else {
			$noRujukan = "";
			$count = 0;
		}
		// {success: true, totalrecords: 1, listData: "0050R05821122018"}
		// $get_ppk_pelayanan=$this->db->query("SELECT nilai from seting_bpjs where key_setting='PpkPelayanan'")->row()->nilai;
		// $noRujukan=$get_ppk_pelayanan.$hari.$bulan.$tahun;
		// $no_rujukan=str_replace("R", "r", $noRujukan);
		echo '{success:true, totalrecords:' . $count . ', listData:' . json_encode($response) . '}';
	}

	public function UpdateSEP()
	{
		$flaging     = $_POST['flaging'];
		$noKartu     = $_POST['noKartu'];
		$klsRawat    = $_POST['klsRawat'];
		$noMR        = $_POST['noMR'];
		$tglRujukan  = $_POST['tglRujukan'];
		//echo $tglRujukan; die();
		$noRujukan   = $_POST['noRujukan'];
		$ppkRujukan  = $_POST['ppkRujukan'];
		$catatan     = $_POST['catatan'];
		$diagAwal    = $_POST['diagAwal'];
		$tujuan      = $_POST['tujuan'];
		$eksekutif   = $_POST['eksekutif'];

		$penjamin_1  = $_POST['penjamin_1'];
		$penjamin_2  = $_POST['penjamin_2'];
		$penjamin_3  = $_POST['penjamin_3'];
		$penjamin_4  = $_POST['penjamin_4'];

		$tglKejadian = $_POST['tglKejadian'];
		$keterangan  = $_POST['keterangan'];
		$kdPropinsi  = $_POST['kdPropinsi'];
		$kdKabupaten = $_POST['kdKabupaten'];
		$kdKecamatan = $_POST['kdKecamatan'];
		$noSurat     = $_POST['noSurat'];
		$noTelp      = $_POST['noTelp'];
		$katarak     = $_POST['katarak'];
		$cob         = $_POST['cob'];
		$suplesi     = $_POST['suplesi'];
		$kodeDPJP    = $_POST['kodeDPJP'];
		$no_suplesi  = $_POST['no_suplesi'];
		$lakaLantas  = $_POST['lakaLantas'];
		$no_sep      = $_POST['no_sep'];
		$jsonData = $this->getJsonDataUpdate($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $no_sep);

		// $url=$this->db->query("select nilai  from seting_bpjs where key_setting='UrlUpdateSEP'")->row()->nilai;
		// $headers=$this->getSignatureVedika_new();
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $url);
		// $jsonDataEncoded = json_encode($jsonData);
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		// curl_setopt($ch, CURLOPT_POSTFIELDS, json_decode($jsonDataEncoded));
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// $response = curl_exec($ch);
		// curl_close ($ch);

		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlUpdateSEP'")->row()->nilai;
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		// $jsonDataEncoded = json_encode($parameter_);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		$ch = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($ch);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		$response = json_encode($respon_array);
		$this->insert_response_bpjs($url, $respon_array, $jsonData, "PUT");
		// echo $url;
		// echo '<pre>' . var_export($respon_array, true) . '</pre>';
		// echo '<pre>' . var_export($parameter, true) . '</pre>';
		// echo $parameter->tgl_kejadian;
		// die;
		// print_r($response);die();
		$this->save_history_sep_bpjs_update($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $no_sep);

		// $this->insert_response_bpjs($url, json_decode($response), $jsonData, "POST");
		echo $response;
	}
	//vclaim 2.0 //hani 24-02-2022 // UPDATE SEP IGD
	public function getJsonDataUpdate($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $no_sep)
	{
		$ppkPelayanan = $this->db->query("SELECT nilai from seting_bpjs WHERE key_setting='PpkPelayanan'")->row()->nilai;
		$penjamin = '';
		$jnsPelayanan = '';
		// if($penjamin_1!=0 and $penjamin_1!='0' ){
		// 	$penjamin.="1,";
		// }
		// if($penjamin_2!=0 and $penjamin_2!='0' ){
		// 	$penjamin.="2,";
		// }
		// if($penjamin_3!=0 and $penjamin_3!='0'){
		// 	$penjamin.="3,";
		// }
		// if($penjamin_4!=0 and $penjamin_4!='0'){
		// 	$penjamin.="4,";
		// }
		if ($suplesi == 0) {
			$no_suplesi = '';
		}
		if ($lakaLantas == 0) {
			$tglKejadian = '';
			$keterangan = '';
			$suplesi = 0;
			$no_suplesi = '';
			$kdPropinsi = '';
			$kdKabupaten = '';
			$kdKecamatan = '';
		}
		if ($flaging == 'RWI') {
			$jnsPelayanan = "1";
		} else {
			$jnsPelayanan = "2";
		}
		if ($flaging == 'IGD') {
			$asal_rujukan = "2";
			$tglRujukan = date("Y-m-d");
		} else {
			$asal_rujukan = "1";
		}
		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$json = '
				 {
					"request": {
						"t_sep": {
						"noSep": "' . $no_sep . '",
						"klsRawat": {
							"klsRawatHak": "' . $klsRawat . '",
							"klsRawatNaik": "",
							"pembiayaan": "",
							"penanggungJawab": ""
						},
						"noMR": "' . $noMR . '",
						"catatan": "' . $catatan . '",
						"diagAwal": "' . $diagAwal . '",
						"poli": {
							"tujuan": "' . $tujuan . '",
							"eksekutif": "' . $eksekutif . '"
						},
						"cob": {
							"cob": "' . $cob . '"
						},
						"katarak": {
							"katarak": "' . $katarak . '"
						},
						"jaminan": {
							"lakaLantas": "' . $lakaLantas . '",
							"penjamin": {
							"tglKejadian": "' . $tglKejadian . '",
							"keterangan": "' . $keterangan . '",
							"suplesi": {
								"suplesi": "' . $suplesi . '",
								"noSepSuplesi": "' . $no_suplesi . '",
								"lokasiLaka": {
								"kdPropinsi": "' . $kdPropinsi . '",
								"kdKabupaten": "' . $kdKabupaten . '",
								"kdKecamatan": "' . $kdKecamatan . '"
								}
							}
							}
						},
						"dpjpLayan": "' . $kodeDPJP . '",
						"noTelp": "' . $noTelp . '",
						"user": "Cobaws"
						}
					}
					}               
		';
		return $json;
	}

	private function prameter_update_bpjs($parameter)
	{
		if ($parameter['lakalantas'] == 0) {
			$parameter['tgl_kejadian'] = '';
			$parameter['keterangan_kejadian'] = '';
			$parameter['suplesi'] = '';
			$parameter['no_suplesi'] = '';
			$parameter['kd_prov'] = '';
			$parameter['kd_kab'] = '';
			$parameter['kd_kec'] = '';
		}
		if ($parameter['suplesi'] == 0) {
			$parameter['no_suplesi'] = '';
		}
		$string = '{
			"request": {
				"t_sep": {
					"noSep": "' . $parameter['no_sep'] . '",
					"klsRawat":{
                                "klsRawatHak":"' . $parameter['kelas_rawat'] . '",
                                "klsRawatNaik":"",
                                "pembiayaan":"",
                                "penanggungJawab":""
                              },
					"noMR": "' . $parameter['kd_pasien'] . '",
					"catatan": "' . $parameter['catatan'] . '",
					"diagAwal": "' . $parameter['diag_awal'] . '",
					"poli": {
						"tujuan": "' . $parameter['poli_tujuan'] . '",
						"eksekutif": "' . $parameter['eksekutif'] . '"
					},
					"cob": {
						"cob": "' . $parameter['cob'] . '"
					},
					"katarak":{
						"katarak":"' . $parameter['katarak'] . '"
					},
					"jaminan": {
						"lakaLantas":"' . $parameter['lakalantas'] . '",
						"penjamin":
						{
							"tglKejadian":"' . $parameter['tgl_kejadian'] . '",				
							"keterangan":"' . $parameter['keterangan_kejadian'] . '",
							"suplesi":
							{
								"suplesi":"' . $parameter['suplesi'] . '",
								"noSepSuplesi":"' . $parameter['no_suplesi'] . '",
								"lokasiLaka": 
								{
									"kdPropinsi":"' . $parameter['kd_prov'] . '",
									"kdKabupaten":"' . $parameter['kd_kab'] . '",
									"kdKecamatan":"' . $parameter['kd_kec'] . '"
								}
							}					
						}
					},             
					"dpjpLayan":"' . $parameter['kd_dpjp'] . '",
					"noTelp": "' . $parameter['no_tlp'] . '",
					"user": "' . $parameter['user'] . '"
					}
				}
			}';
		return $string;
	}

	public function updateSep_Rev()
	{
		$parameter = json_decode($this->input->post('parameter'));
		$tmp_penjamin = "";
		if ($parameter->penjamin_1 != 0 || $parameter->penjamin_1 != '0') {
			$tmp_penjamin .= $parameter->penjamin_1 . ",";
		}

		if ($parameter->penjamin_2 != 0 || $parameter->penjamin_2 != '0') {
			$tmp_penjamin .= $parameter->penjamin_2 . ",";
		}

		if ($parameter->penjamin_3 != 0 || $parameter->penjamin_3 != '0') {
			$tmp_penjamin .= $parameter->penjamin_3 . ",";
		}

		if ($parameter->penjamin_4 != 0 || $parameter->penjamin_4 != '0') {
			$tmp_penjamin .= $parameter->penjamin_4 . ",";
		}

		$tmp_penjamin = substr($tmp_penjamin, 0, -1);
		if (count($parameter) > 0) {
			$parameter_bpjs = array(
				'no_sep' 				=> $parameter->no_sep,
				'kelas_rawat' 			=> $parameter->kelas_rawat,
				'kd_pasien' 			=> $parameter->kd_pasien,
				'asal_rujukan' 			=> $parameter->asal_rujukan,
				'tgl_rujukan' 			=> date_format(date_create($parameter->tgl_rujukan), 'Y-m-d'),
				'no_rujukan' 			=> $parameter->no_rujukan,
				'ppk_rujukan' 			=> $parameter->ppk_rujukan,
				'catatan' 				=> $parameter->catatan,
				'diag_awal' 			=> $parameter->diag_awal,
				'poli_tujuan' 			=> $parameter->poli_tujuan,
				'eksekutif' 			=> $parameter->eksekutif,
				'cob' 					=> (int)$parameter->cob,
				'katarak' 				=> (int)$parameter->katarak,
				'no_surat_dpjp' 		=> $parameter->no_surat_dpjp,
				'kd_dpjp' 				=> $parameter->kd_dpjp,
				'lakalantas' 			=> $parameter->lakalantas,
				'penjamin' 				=> $tmp_penjamin,
				'tgl_kejadian' 			=> date_format(date_create($parameter->tgl_kejadian), 'Y-m-d'),
				'keterangan_kejadian' 	=> $parameter->keterangan_kejadian,
				'suplesi' 				=> (int)$parameter->suplesi,
				'no_suplesi' 			=> $parameter->no_suplesi,
				'kd_prov' 				=> $parameter->kd_prov,
				'kd_kab' 				=> $parameter->kd_kab,
				'kd_kec' 				=> $parameter->kd_kec,
				'no_tlp' 				=> $parameter->no_tlp,
				'user' 					=> $this->user,
			);
			$parameter_ = $this->prameter_update_bpjs($parameter_bpjs);
			$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlUpdateSEP'")->row()->nilai;
			$headers = $this->getSignatureVedika_new();
			$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
			// $ch = curl_init();
			// curl_setopt($ch, CURLOPT_URL, $url);
			$jsonDataEncoded = json_encode($parameter_);
			// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			// curl_setopt($ch, CURLOPT_POSTFIELDS, json_decode($jsonDataEncoded));
			// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = array();
			$curl = curl_init();
			// echo '<pre>' . var_export($parameter_, true) . '</pre>';
			// echo $url;
			// die;
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl, CURLOPT_POSTFIELDS, $parameter_);

			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

			// $response = curl_exec($curl);
			// curl_close($curl);

			$table = $this->get_structure('history_sep_bpjs');
			if ($table->num_rows() > 0) {
				$params = array();
				$criteria = array();
				unset($params);
				unset($criteria);
				foreach ($parameter as $key => $res_param) {
					foreach ($table->result() as $res_table) {
						if ($key == "kd_unit"  || $key == "kd_pasien"  || $key == "tgl_masuk" || $key == "urut_masuk") {
							$criteria[$key]	= $res_param;
						} else if ((strtolower($key) ==  strtolower($res_table->column_name))) {
							$params[$key] 	= $res_param;
						}
					}
				}
				$this->db->where($criteria);
				$this->db->update("history_sep_bpjs", $params);
				$query = $this->db->affected_rows();
				if ($query > 0 || $query === true) {
					$query = true;
				} else {
					$query = false;
					$response = '{metaData: {code: "201", message: "Save History SEP BPJS"}}';
				}
			} else {
				$query = false;
			}

			if ($query > 0 || $query === true) {
				$table = $this->get_structure('rujukan_kunjungan');
				if ($table->num_rows() > 0) {
					$params = array();
					$criteria = array();
					unset($params);
					unset($criteria);
					foreach ($parameter as $key => $res_param) {
						foreach ($table->result() as $res_table) {
							if ($key == "kd_unit"  || $key == "kd_pasien"  || $key == "tgl_masuk" || $key == "urut_masuk") {
								$criteria[$key]	= $res_param;
							} else if ((strtolower($key) ==  strtolower($res_table->column_name))) {
								$params[$key] 	= $res_param;
							}
						}
					}
					$this->db->where($criteria);
					$this->db->update("rujukan_kunjungan", $params);
					$query = $this->db->affected_rows();
					if ($query > 0 || $query === true) {
						$query = true;
					} else {
						$query = false;
						$response = '{metaData: {code: "201", message: "Save Rujukan Kunjungan"}}';
					}
				}
			} else {
				$query = false;
			}

			if ($query > 0 || $query === true) {
				$ch = curl_exec($curl);
				curl_close($curl);
				$respon_array = json_decode($ch);
				$respon_array->response = $this->decompress($respon_array->response, $timestamp);
				$response = json_encode($respon_array);
				$this->insert_response_bpjs($url, $respon_array, $parameter_, "PUT");
			}
		} else {
			$response = '{metaData: {code: "201", message: "Error"}}';
		}
		echo $response;
	}

	public function save_history_sep_bpjs($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $no_sep, $unit_tujuan)
	{
		$now = date('Y-m-d');
		$get_last_urut = $this->db->query("SELECT MAX(urut_masuk) as urut from history_sep_bpjs")->row()->urut;
		$urut = $get_last_urut + 1;
		if ($flaging == 'RWI') {
			//	$unit=$unit_tujuan;
			$tglKejadian = date("Y-m-d");
			$unit = $this->db->query("SELECT kd_spc_bpjs from map_spc_bpjs WHERE kd_spc='" . $tujuan . "'")->row()->kd_spc_bpjs;
		} else {
			$unit = $tujuan;
		}
		if ($flaging == 'RWJ') {
			$tglKejadian = date("Y-m-d");
		}
		$paramsInsert = array(
			'no_kartu'              => $noKartu,
			'kelas_rawat'  			=> $klsRawat,
			'kd_pasien'  			=> $noMR,
			'tgl_rujukan' 			=> $tglRujukan,
			'no_rujukan' 			=> $noRujukan,
			'ppk_rujukan' 			=> $ppkRujukan,
			'catatan' 				=> $catatan,
			'diag_awal' 			=> $diagAwal,
			'poli_tujuan' 			=> $unit,
			'eksekutif' 			=> $eksekutif,
			'tgl_kejadian'	 		=> $tglKejadian,
			'keterangan_kejadian' 	=> $keterangan,
			'kd_prov' 				=> $kdPropinsi,
			'kd_kab' 				=> $kdKabupaten,
			'kd_kec' 				=> $kdKecamatan,
			'no_surat_dpjp' 		=> $noSurat,
			'no_tlp'			 	=> $noTelp,
			'cob' 					=> $cob,
			'katarak' 				=> $katarak,
			'lakalantas' 			=> $lakaLantas,
			'suplesi' 				=> $no_suplesi,
			'kd_dpjp'				=> $kodeDPJP,
			'no_suplesi' 			=> $no_suplesi,
			'penjamin_1' 			=> $penjamin_1,
			'penjamin_2'			=> $penjamin_2,
			'penjamin_3' 			=> $penjamin_3,
			'penjamin_4'			=> $penjamin_4,
			'no_sep'  				=> $no_sep
		);
		$this->db->insert("history_sep_bpjs", $paramsInsert);
	}

	public function cari_history_sep()
	{
		$query = $this->db->query("SELECT a.*, b.*, c.*, d.penyakit, rk.kd_dokter_dpjp from 
				history_sep_bpjs a 
				INNER JOIN rujukan_kunjungan rk ON rk.kd_pasien = a.kd_pasien AND rk.tgl_masuk = a.tgl_masuk AND rk.urut_masuk = a.urut_masuk AND rk.kd_unit = a.kd_unit 
				LEFT join map_dokter_bpjs b on a.kd_dpjp=b.kd_dokter_dpjp
				LEFT join dokter c on b.kd_dokter=c.kd_dokter 
				LEFT JOIN penyakit d ON a.diag_awal = d.kd_penyakit 
				WHERE no_kartu='" . $this->input->post('no_kartu') . "' AND no_sep='" . $this->input->post('no_sep') . "'  ");
		if ($query->num_rows() > 0) {
			$data['noSep'] = $query->row()->no_sep;
			$data['peserta']['noKartu'] = $query->row()->no_kartu;
			$data['klsRawat']['klsRawatHak'] = $query->row()->kelas_rawat;
			$data['tglRujukan'] = $query->row()->tgl_rujukan;
			$data['noRujukan'] = $query->row()->no_rujukan;
			$data['ppkRujukan'] = $query->row()->ppk_rujukan;
			$data['catatan'] = $query->row()->catatan;
			$data['kd_diag'] = $query->row()->diag_awal;
			$data['poliTujuan'] = $query->row()->poli_tujuan;
			$data['poliEksekutif'] = $query->row()->eksekutif;
			$data['lokasiKejadian']['tglKejadian'] = $query->row()->tgl_kejadian;
			$data['lokasiKejadian']['ketKejadian'] = $query->row()->keterangan_kejadian;
			$data['lokasiKejadian']['kdProp'] = $query->row()->kd_prov;
			$data['lokasiKejadian']['kdKab'] = $query->row()->kd_kab;
			$data['lokasiKejadian']['kdKec'] = $query->row()->kd_kec;
			$data['kontrol']['noSurat'] = $query->row()->no_surat_dpjp;
			$data['no_tlp'] = $query->row()->no_tlp;
			$data['cob'] = $query->row()->cob;
			$data['katarak'] = $query->row()->katarak;
			$data['kdStatusKecelakaan'] = $query->row()->lakalantas;
			$data['suplesi'] = $query->row()->suplesi;
			$data['dpjp']['kdDPJP'] = $query->row()->kd_dpjp;
			$data['noSuplesi'] = $query->row()->no_suplesi;
			$data['penjamin1'] = $query->row()->penjamin_1;
			$data['penjamin2'] = $query->row()->penjamin_2;
			$data['penjamin3'] = $query->row()->penjamin_3;
			$data['penjamin4'] = $query->row()->penjamin_4;
			$data['peserta']['noMr'] = $query->row()->kd_pasien;
			$data['kdUnit'] = $query->row()->kd_unit;
			$data['tglSep'] = $query->row()->tgl_masuk;
			$data['urutMasuk'] = $query->row()->urut_masuk;
			$data['asalRujukan'] = $query->row()->asal_rujukan;
			$data['tujuanKunj'] = $query->row()->tujuan_kunj;
			$data['flagProcedure'] = $query->row()->flagprocedure;
			$data['kdPenunjang'] = $query->row()->kd_penunjang;
			$data['assementpel'] = $query->row()->assementpel;
			$data['noLp'] = $query->row()->no_lp;
			$data['diagnosa'] = $query->row()->penyakit;
		} else {
			// Get History Pelayanan //vclaim 2.0 //hani 2-02-2022 
			$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEP'")->row()->nilai;
			$headers = $this->getSignature_new();
			$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
			$tgl_awal = Date('Y-m-d', strtotime('-90 days'));
			$tgl_akhir = date('Y-m-d');
			$opts = array(
				'http' => array(
					'method' => 'GET',
					'header' => $headers
				)
			);
			// $context = stream_context_create($opts);

			// $res = json_decode(file_get_contents($url . '/' . $no, false, $context));
			$urlnya = $url . '/' . $this->input->post('no_sep');
			$method = "GET"; // POST / PUT / DELETE
			$postdata = "";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $urlnya);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

			$response = curl_exec($curl);
			curl_close($curl);
			$res = json_decode($response);

			$res->response = $this->decompress($res->response, $timestamp);
			// echo '<pre>' . var_export($res, true) . '</pre>'; 

			if ($res->metaData->code == 200 || $res->metaData->code == '200') {
				$data = $res->response;

				// cari berdasarkan NOKA
				$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariNoka'")->row()->nilai;
				$headers = $this->getSignature_new();
				$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
				$tgl_awal = Date('Y-m-d', strtotime('-90 days'));
				$tgl_akhir = date('Y-m-d');
				$opts = array(
					'http' => array(
						'method' => 'GET',
						'header' => $headers
					)
				);

				$urlnya = $url . '' . $res->response->peserta->noKartu . '/tglSEP/' . $res->response->tglSep;
				$method = "GET"; // POST / PUT / DELETE
				$postdata = "";
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $urlnya);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

				$response = curl_exec($curl);
				curl_close($curl);
				$res_noka = json_decode($response);

				$res_noka->response = $this->decompress($res_noka->response, $timestamp);
				$data->no_tlp = $res_noka->response->peserta->mr->noTelepon;
				// echo '<pre>' . var_export($res_noka, true) . '</pre>'; 

				if ($res_noka->metaData->code == 200 || $res_noka->metaData->code == '200') {
					//cari kode diagnosa
					$url_diag = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_diagnosa'")->row()->nilai;
					$headers_diag = $this->getSignature_new();
					$timestamp_diag = preg_replace("/[^0-9]/", "", $headers_diag[1]);
					$opts_diag = array(
						'http' => array(
							'method' => 'GET',
							'header' => $headers_diag
						)
					);
					// $diagnosa = explode(' ',  $res->response->diagnosa);
					$diagnosa = str_replace(' ', '%20', $res->response->diagnosa);
					$urlnya_diag = $url_diag . $diagnosa;
					$method_diag = "GET"; // POST / PUT / DELETE
					$postdata_diag = "";
					$curl_diag = curl_init();
					curl_setopt($curl_diag, CURLOPT_URL, $urlnya_diag);
					curl_setopt($curl_diag, CURLOPT_HEADER, false);
					curl_setopt($curl_diag, CURLOPT_CUSTOMREQUEST, $method_diag);
					curl_setopt($curl_diag, CURLOPT_POSTFIELDS, $postdata_diag);

					curl_setopt($curl_diag, CURLOPT_FOLLOWLOCATION, true);
					curl_setopt($curl_diag, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl_diag, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($curl_diag, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
					curl_setopt($curl_diag, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl_diag, CURLOPT_HTTPHEADER, $headers_diag);

					$response_diag = curl_exec($curl_diag);
					curl_close($curl_diag);

					$res_diag = json_decode($response_diag);
					$res_diag->response = $this->decompress($res_diag->response, $timestamp_diag);

					$data->kd_diag = $res_diag->response->diagnosa[0]->kode;
				}
			}
		}
		echo '{success:true, totalrecords:' . count($data) . ', listData:' . json_encode($data) . '}';
	}


	public function get_map_dokter_bpjs()
	{
		$kd_dokter_dpjp = $this->input->post('kd_dokter');
		$query = $this->db->query("SELECT * from  map_dokter_bpjs a inner join dokter b on a.kd_dokter=b.kd_dokter WHERE a.kd_dokter_dpjp='" . $kd_dokter_dpjp . "' ")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	public function getDataBpjs_by_nokartu_multi_rs()
	{
		$no_kartu = $_POST['no_kartu'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNokaMultiRS'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		//	var_dump($headers); die();
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $no_kartu, false, $context));
		$urlnya = $url . $no_kartu;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		// $this->insert_response_bpjs($url . $no_kartu, $dat['data'], json_encode($res), "GET");
		// if ($res->metaData->code != 200) {
		// 	echo '{success:false, listData:' . json_encode($res) . '}';
		// } else {
		$jml = 0;
		if ($res->metaData->code != 200) {
			echo '{success:false, listData:' . json_encode($res) . '}';
		} else {
			echo '{success:true, totalrecords:' . count($res->response->rujukan) . ', listData:' . json_encode($res->response->rujukan) . '}';
		}
		/* $no_kartu=$_POST['no_kartu'];
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNokaMultiRS'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		//	var_dump($headers); die();
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$no_kartu,false,$context));
		$dat=array(
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		if($res->metaData->code!=200){
			echo '{success:false, listData:'.json_encode($res).'}';
		}else{
			echo '{success:true, totalrecords:'.count($res->response->rujukan).', listData:'.json_encode($res->response->rujukan).'}';
		} */
	}

	public function save_history_sep_bpjs_update($noKartu, $klsRawat, $noMR, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $tglKejadian, $keterangan, $kdPropinsi, $kdKabupaten, $kdKecamatan, $noSurat, $noTelp, $cob, $katarak, $lakaLantas, $suplesi, $kodeDPJP, $no_suplesi, $penjamin_1, $penjamin_2, $penjamin_3, $penjamin_4, $flaging, $no_sep)
	{
		if ($tujuan == 'IGD') {
			$kd_unit = '31';
		} else {
			$kd_unit = $tujuan;
			$tglKejadian = date("Y-m-d");
		}
		$dataUbah = array(
			"catatan" => $catatan,
			"diag_awal" => $diagAwal,
			"tgl_kejadian" => $tglKejadian,
			"keterangan_kejadian" => $keterangan,
			"poli_tujuan" => $tujuan,
			"kd_prov" => $kdPropinsi,
			"kd_kab" => $kdKabupaten,
			"kd_kec" => $kdKecamatan,
			"lakalantas" => $lakaLantas,
			"suplesi" => $suplesi,
			"no_suplesi" => $no_suplesi,
			"penjamin_1" => $penjamin_1,
			"penjamin_2" => $penjamin_2,
			"penjamin_3" => $penjamin_3,
			"penjamin_4" => $penjamin_4,
			"kd_dpjp"  => $kodeDPJP,
			"katarak" => $katarak,
			"cob"  => $cob
		);

		$criteria = array(
			"kd_pasien" => $noMR,
			"no_sep" => $no_sep
		);
		$this->db->where($criteria);
		$result = $this->db->update('history_sep_bpjs', $dataUbah);
	}



	/*public function getDataBpjs_by_nokartu_multi_rs() {
		$no_kartu=$_POST['no_kartu'];
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNokaMultiRS'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		//	var_dump($headers); die();
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$no_kartu,false,$context));
		$dat=array(
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		if($res->metaData->code!=200){
			echo '{success:false, listData:'.json_encode($res).'}';
		}else{
			echo '{success:true, totalrecords:'.count($res->response->rujukan).', listData:'.json_encode($res->response->rujukan).'}';
		}		
	}*/

	public function getDataBpjs_by_norujukan_RS()
	{
		$no_rujukan = $_POST['no_rujukan'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByRujukanRS'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $no_rujukan, false, $context));
		$urlnya = $url . '/' . $no_rujukan;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		// $this->insert_response_bpjs($url . $no_rujukan . "/tglSEP/" . date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat);
		/* $no_rujukan=$_POST['no_rujukan'];
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByRujukanRS'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$no_rujukan,false,$context));
		$dat=array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url.$no_rujukan."/tglSEP/".date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		echo json_encode($dat); */
	}

	public function cari_map_rujukan_bpjs()
	{
		$query = $this->db->query("SELECT * from rujukan WHERE kd_prov_perujuk='" . $this->input->post('kd_prov_rujukan') . "'")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}

	// 22 Feb 2022 By DP --> Revisi Function Pengajuan SEP
	public function PengajuanApprovalSEP()
	{
		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlPengajuanSEP'")->row()->nilai;
		$json = '{
			       "request": {
			          "t_sep": {
			             "noKartu": "' . $this->input->post('noKartu') . '",
			             "tglSep": "' . $this->input->post('tglSep') . '",
			             "jnsPelayanan": "' . $this->input->post('jnsPelayanan') . '",
						 "jnsPengajuan": "' . $this->input->post('jenisPengajuan') . '",
			             "keterangan": "' . $this->input->post('keterangan') . '",
			             "user": "' . $cari_user . '"
			          }
			       }
			   }';
		// echo $url."<br>";	   
		// echo $json."<br>";
		// var_dump($headers); die();	   

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		// echo '<pre>' . var_export($response, true) . '</pre>';
		// die;
		$this->insert_response_bpjs($url, $respon_array, $json, "POST");

		echo json_encode($respon_array);
	}

	// 22 Feb 2022 By DP --> Penambahan Function Pengajuan Approval SEP
	public function PengajuanApprovalFinger()
	{
		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlPengajuanApprovalSEP'")->row()->nilai;
		$json = '{
			       "request": {
			          "t_sep": {
			             "noKartu": "' . $this->input->post('noKartu') . '",
			             "tglSep": "' . $this->input->post('tglSep') . '",
			             "jnsPelayanan": "' . $this->input->post('jnsPelayanan') . '",
			             "jnsPengajuan": "' . $this->input->post('jenisPengajuan') . '",
			             "keterangan": "' . $this->input->post('keterangan') . '",
			             "user": "' . $cari_user . '"
			          }
			       }
			   }';
		// echo $url."<br>";	   
		// echo $json."<br>";
		// var_dump($headers); die();	   


		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		// echo '<pre>' . var_export($response, true) . '</pre>';
		// die;
		$this->insert_response_bpjs($url, $respon_array, $json, "POST");

		echo json_encode($respon_array);
	}

	public function getDataBpjs_by_nokartu_non_rujukan()
	{
		$no_kartu = $_POST['no_kartu'];
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariByNokaPeserta'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);

		/* $opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url . $no_kartu . '/tglSEP/' . date("Y-m-d"), false, $context)); */

		$urlnya = $url . $no_kartu . "/tglSEP/" . date("Y-m-d");
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			//'poli'    => $poli,
			'user'    => $this->session->userdata['user_id']['id'],
			'data'    => $res,
			'headers' => $headers,
			'kd_rs'   => $bpjs_kd_res
		);
		$data_pasien = array();
		$data_pasien['kd_pasien'] = $this->input->post('kd_pasien');
		$data_pasien['kd_unit']   = $this->input->post('kd_unit');
		$data_pasien['tgl_masuk'] = $this->input->post('tgl_masuk');
		$this->insert_response_bpjs($url . $no_kartu . "/tglSEP/" . date("Y-m-d"), $dat['data'], json_encode($res), "GET", $data_pasien);
		/*if($res->metaData->code=='200'){
			echo '{success:true, totalrecords:'.count($res->response->rujukan).', listData:'.json_encode($res->response->rujukan).'}';
		}else{
			echo json_encode($dat);
		}*/
		echo json_encode($dat);
	}

	public function get_all_list_unit_rwi_bpjs()
	{
		$query = $this->db->query("SELECT * from map_spc_bpjs a inner join spesialisasi b on a.kd_spc::INTEGER=b.kd_spesial ")->result();
		echo '{success:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
	}
	public function get_daftar_rujukan()
	{
		$tipe_fasker = $this->input->post('tipe_fasker');
		if ($tipe_fasker == "1") {
			$faskes = 1;
		} else {
			$faskes = 2;
		}
		$nama_faskes = $this->input->post('nama_faskes');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_ref_faskes'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . $nama_faskes . '/' . $faskes, false, $context));
		$urlnya = $url . $nama_faskes . '/' . $faskes;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		if ($res->metaData->code == 200) {
			echo '{success:true, totalrecords:' . count($res->response->faskes) . ', listData:' . json_encode($res->response->faskes) . '}';
		} else {
			echo json_encode($res);
		}
		$this->insert_response_bpjs($url . $nama_faskes . '/' . $faskes, $res, json_encode($res), "GET");
		/* $tipe_fasker= $this->input->post('tipe_fasker');
		if($tipe_fasker =="1" ){
			$faskes=1;
		}else{
			$faskes=2;
		}
		$nama_faskes=$this->input->post('nama_faskes');
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='url_ref_faskes'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.$nama_faskes.'/'.$faskes,false,$context));
		if($res->metaData->code==200){
			echo '{success:true, totalrecords:'.count($res->response->faskes).', listData:'.json_encode($res->response->faskes).'}';
		}else{
			echo $res;
		}
		$this->insert_response_bpjs($url.$nama_faskes.'/'.$faskes, $res, json_encode($res), "GET"); */
	}

	public function get_poli_rujukan_insert_rujukan()
	{
		$tipe_faskes = 2;
		$nama_poli_rujuk = $this->input->post('nama_poli_rujuk');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai from seting_bpjs where key_setting='url_ref_poli'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $nama_poli_rujuk, false, $context));
		$urlnya = $url . '/' . $nama_poli_rujuk;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		echo '{success:true, totalrecords:' . count($res->response->poli) . ', listData:' . json_encode($res->response->poli) . '}';
		$this->insert_response_bpjs($url . '/' . $nama_poli_rujuk, $res, json_encode($res), "GET");
		/* $tipe_faskes=2;
		$nama_poli_rujuk=$this->input->post('nama_poli_rujuk');
		$bpjs_kd_res= $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url= $this->db->query("select nilai  from seting_bpjs where key_setting='url_ref_poli'")->row()->nilai;
		$headers=$this->getSignature_new();
		$opts = array(
		  'http'=>array(
			'method'=>'GET',
			'header'=>$headers
		  )
		);
		$context = stream_context_create($opts);
		$res = json_decode(file_get_contents($url.'/'.$nama_poli_rujuk,false,$context));
		echo '{success:true, totalrecords:'.count($res->response->poli).', listData:'.json_encode($res->response->poli).'}';
		$this->insert_response_bpjs($url.'/'.$nama_poli_rujuk, $res, json_encode($res), "GET"); */
	}

	public function insert_rujukan()
	{

		$no_sep            = $this->input->post('no_sep');
		$jns_pelayanan     = $this->input->post('jenis_pelayanan');
		$catatan_pelayanan = $this->input->post('catatan');
		$tipe_rujukan      = $this->input->post('tipe_rujukan');
		$nama_poli_rujuk   = $this->input->post('nama_poli_rujuk');
		$tgl_renKunj	   = $this->input->post('tgl_renKunj');
		$bpjs_kd_res       = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url               = $this->db->query("select nilai  from seting_bpjs where key_setting='url_insert_rujukan'")->row()->nilai;
		$kd_user           = $this->session->userdata['user_id']['id'];
		$cari_user         = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$json = ' {
			       "request": {
			          "t_rujukan": {
			             "noSep": "' . $no_sep . '",
			             "tglRujukan": "' . $this->input->post('tgl_rujukan') . '",
						 "tglRencanaKunjungan":"' . $tgl_renKunj . '",
			             "ppkDirujuk": "' . $this->input->post('pkk_dirujuk') . '",
			             "jnsPelayanan": "' . $jns_pelayanan . '",
			             "catatan": "' . $catatan_pelayanan . '",
			             "diagRujukan": "' . $this->input->post('diag_rujukan') . '",
			             "tipeRujukan": "' . $tipe_rujukan . '",
			             "poliRujukan": "' . $this->input->post('poli_rujukan') . '",
			             "user": "' . $cari_user . '"
			          }
			       }
			    }';
		//echo $json;	 die();    
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();
		// echo '<pre>' . var_export($json, true) . '</pre>';
		// echo $url;
		// die;
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt(
			$curl,
			CURLOPT_POSTFIELDS,
			$json
		);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt(
			$curl,
			CURLOPT_RETURNTRANSFER,
			true
		);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt(
			$curl,
			CURLOPT_SSL_CIPHER_LIST,
			'TLSv1'
		);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt(
			$curl,
			CURLOPT_HTTPHEADER,
			$headers
		);

		$response = curl_exec($curl);
		curl_close($curl);

		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		if ($respon_array->metaData->code == 200 && $respon_array->response != '') {
			$this->save_history_rujukan_bpjs($no_sep, $jns_pelayanan, $catatan_pelayanan, $tipe_rujukan, $respon_array, $nama_poli_rujuk, $tgl_renKunj);
		}
		$this->insert_response_bpjs($url, $respon_array, $json, "POST");
		echo json_encode($respon_array);
	}

	public function save_history_rujukan_bpjs($no_sep, $jns_pelayanan, $catatan_pelayanan, $tipe_rujukan, $data, $nama_poli_rujuk, $tgl_renKunj)
	{
		$ppk_dirujuk = $data->response->rujukan->tujuanRujukan->kode;
		if (stripos($ppk_dirujuk, "R") !== false) {
			$tipe_faskes = 2;
		} else {
			$tipe_faskes = 1;
		}
		$paramsInsert = array(
			'no_sep'                    => $no_sep,
			'tgl_rujukan'  				=> $data->response->rujukan->tglRujukan,
			'tgl_renKunj'  				=> $tgl_renKunj,
			'ppk_tujuan_dirujuk'  		=> $data->response->rujukan->tujuanRujukan->kode,
			'jns_pelayanan_rujukan'  	=> $jns_pelayanan,
			'catatan_rujukan'  			=> $catatan_pelayanan,
			'diag_rujukan'  			=> $data->response->rujukan->diagnosa->kode,
			'tipe_rujukan'  			=> $tipe_rujukan,
			'poli_rujukan'  			=> $data->response->rujukan->poliTujuan->kode,
			'no_rujukan'                => $data->response->rujukan->noRujukan,
			'tipe_faskes' 				=> $tipe_faskes,
			'nama_poli_rujukan' 		=> $nama_poli_rujuk,
			// 'response'					=> json_encode($data)
		);
		$this->db->insert("history_rujukan_bpjs", $paramsInsert);
	}

	// --------> 03 Feb 2022 By DP : PRB -> Insert Pembuatan Rujuk Balik <---------
	public function insert_rujukan_balik()
	{

		$no_sep            = $this->input->post('no_sep');
		$noka    		   = $this->input->post('noka');
		$alamat			   = $this->input->post('alamat');
		$email 		       = $this->input->post('email');
		$programPRB 	   = str_replace(' ', '', $this->input->post('programPRB'));
		$kodeDPJP	       = $this->input->post('kodeDPJP');
		$keterangan	       = $this->input->post('keterangan');
		$saran		       = $this->input->post('saran');
		$jml_obat	       = $this->input->post('jumlah');
		$url               = $this->db->query("select nilai  from seting_bpjs where key_setting='url_insert_rujukan_balik'")->row()->nilai;
		$kd_user           = $this->session->userdata['user_id']['id'];
		$cari_user         = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$obatPRB = '';
		for ($i = 0; $jml_obat > $i; $i++) {
			$obatPRB .= '{ 
							"kdObat":"' . $_POST['kode-' . $i] . '",
							"signa1":"' . $_POST['signa1-' . $i] . '",
							"signa2":"' . $_POST['signa2-' . $i] . '",
							"jmlObat":"' . $_POST['jml-' . $i] . '"
						},';
		}
		$obatPRB = substr($obatPRB, 0, -1);
		$list_obat = '{"obat":[' . $obatPRB . ']}';
		$data_obatPRB = json_decode($list_obat);
		$json = ' {
			       "request": {
			          "t_prb": {
			             	"noSep":"' . $no_sep . '",
							"noKartu":"' . $noka . '",
							"alamat":"' . $alamat . '",
							"email":"' . $email . '",
							"programPRB":"' . $programPRB . '",
							"kodeDPJP":"' . $kodeDPJP . '",
							"keterangan":"' . $keterangan . '",
							"saran":"' . $saran . '",
							"user":"' . $kd_user . '",
							"obat":
								[
									' . $obatPRB . '
								]      
							}
						}
			    }';
		//echo $json;	 die();    
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();
		// echo '<pre>' . var_export($json, true) . '</pre>';
		// echo $url;
		// die;
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		if ($respon_array->metaData->code == 200 && $respon_array->response != '') {
			$this->save_history_rujukan_balik_bpjs($no_sep, $noka, $programPRB, $data_obatPRB, $respon_array, $kd_user);
		}
		$this->insert_response_bpjs($url, $respon_array, $json, "POST");
		echo json_encode($respon_array);
	}

	public function save_history_rujukan_balik_bpjs($no_sep, $noka, $programprb, $dataobat, $data, $kd_user)
	{
		$paramsInsert = array(
			'no_sep'                    => $no_sep,
			'no_kartu'                  => $noka,
			'tgl_srb'  					=> $data->response->tglSRB,
			'kode_program_prb'  		=> $programprb,
			'nama_program_prb'		  	=> $data->response->programPRB->nama,
			'kode_dpjp'  				=> $data->response->DPJP->kode,
			'nama_dpjp'		  			=> $data->response->DPJP->nama,
			'saran'			  			=> $data->response->saran,
			'user'			  			=> $kd_user,
			'no_srb'	                => $data->response->noSRB,
			'keterangan' 				=> $data->response->keterangan,
			'email'             		=> $data->response->peserta->email,
			'alamat'             		=> $data->response->peserta->alamat
		);
		$result = $this->db->insert("history_prb_bpjs", $paramsInsert);
		if ($result) {
			for ($i = 0; count($dataobat->obat) > $i; $i++) {
				$paramsInsertPRB = array(
					'no_sep'                    => $no_sep,
					'no_srb'                    => $data->response->noSRB,
					'tgl_srb'  					=> $data->response->tglSRB,
					'kode_obat'			  		=> $dataobat->obat[$i]->kdObat,
					'nama_obat'				  	=> $data->response->obat->list[$i]->nmObat,
					'jml_obat'  				=> $data->response->obat->list[$i]->jmlObat,
					'signa'			  			=> $data->response->obat->list[$i]->signa,
					'urut'			  			=> $i + 1
				);
				$this->db->insert("history_prb_obat_bpjs", $paramsInsertPRB);
			}
		}
	}

	// --------> 03 Feb 2022 By DP : PRB -> Update Pembuatan Rujuk Balik <---------
	public function update_rujukan_balik()
	{
		$no_srb            = $this->input->post('no_srb');
		$no_sep            = $this->input->post('no_sep_update');
		$noka    		   = $this->input->post('noka_update');
		$alamat			   = $this->input->post('alamat_update');
		$email 		       = $this->input->post('email_update');
		$programPRB 	   = str_replace(' ', '', $this->input->post('programPRB_update'));
		$kodeDPJP	       = $this->input->post('kodeDPJP_update');
		$namaDPJP		   = $this->input->post('namaDPJP_update');
		$keterangan	       = $this->input->post('keterangan_update');
		$saran		       = $this->input->post('saran_update');
		$jml_obat	       = $this->input->post('jumlah_update');
		$tgl_rujukan 	   = $this->input->post('tgl_rujukan_update');
		$url               = $this->db->query("select nilai  from seting_bpjs where key_setting='url_update_rujukan_balik'")->row()->nilai;
		$kd_user           = $this->session->userdata['user_id']['id'];
		$cari_user         = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$obatPRB = '';
		$list_obat = '';
		for ($i = 0; $jml_obat > $i; $i++) {
			$obatPRB .= '{ 
							"kdObat":"' . $_POST['kode_update-' . $i] . '",
							"signa1":"' . $_POST['signa1_update-' . $i] . '",
							"signa2":"' . $_POST['signa2_update-' . $i] . '",
							"jmlObat":"' . $_POST['jml_update-' . $i] . '"
						},';
			$list_obat .= '{ 
								"kdObat":"' . $_POST['kode_update-' . $i] . '",
								"nmObat":"' . $_POST['nama_update-' . $i] . '",
								"signa1":"' . $_POST['signa1_update-' . $i] . '",
								"signa2":"' . $_POST['signa2_update-' . $i] . '",
								"jmlObat":"' . $_POST['jml_update-' . $i] . '"
							},';
		}
		$obatPRB = substr($obatPRB, 0, -1);
		$list_obat = '{"obat":[' . substr($list_obat, 0, -1) . ']}';
		$data_obatPRB = json_decode($list_obat);
		$json_update = ' {
			       "request": {
			          "t_prb": {
						  	"noSrb":"' . $no_srb . '",
			             	"noSep":"' . $no_sep . '",
							"alamat":"' . $alamat . '",
							"email":"' . $email . '",
							"kodeDPJP":"' . $kodeDPJP . '",
							"keterangan":"' . $keterangan . '",
							"saran":"' . $saran . '",
							"user":"' . $kd_user . '",
							"obat":
								[
									' . $obatPRB . '
								]      
							}
						}
			    }';
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_update);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		$this->insert_response_bpjs($url, $respon_array, $json_update, "PUT");
		if ($respon_array->metaData->code == 200) {
			$this->update_history_rujukan_balik_bpjs($no_sep, $no_srb, $alamat, $email, $kodeDPJP, $namaDPJP, $keterangan, $saran, $data_obatPRB, $respon_array, $kd_user, $tgl_rujukan);
		}
		echo json_encode($respon_array);
	}

	public function update_history_rujukan_balik_bpjs($no_sep, $no_srb, $alamat, $email, $kode_dpjp, $nama_dpjp, $keterangan, $saran, $dataobat, $data, $kd_user, $tgl_rujukan)
	{
		$paramsUpdate = array(
			'no_sep'            => $no_sep,
			'alamat'  			=> $alamat,
			'email'  			=> $email,
			'tgl_srb'			=> $tgl_rujukan,
			'kode_dpjp'  		=> $kode_dpjp,
			'nama_dpjp'  		=> $nama_dpjp,
			'keterangan'  		=> $keterangan,
			'saran'  			=> $saran,
			'user'  			=> $kd_user
		);
		$criteria = array(
			'no_sep'  => $no_sep,
			'no_srb'  => $no_srb
		);
		$result = $this->db->where($criteria);
		$result = $this->db->update("history_prb_bpjs", $paramsUpdate);
		if ($result) {
			$no = 0;
			for ($i = 0; count($dataobat->obat) > $i; $i++) {
				$urut = $no + 1;
				$cek_obat = $this->db->query("Select * From history_prb_obat_bpjs where no_sep='" . $no_sep . "' and no_srb='" . $no_srb . "' and kode_obat='" . $dataobat->obat[$i]->kdObat . "' and urut='" . $urut . "'");
				// echo "Select * From history_prb_obat_bpjs where no_sep='".$no_sep."' and no_srb='".$no_srb."' and kode_obat='".$dataobat->obat[$i]->kdObat."' and urut='".$urut."' <br>";
				// echo "num".$cek_obat->num_rows(); 
				if ($cek_obat->num_rows() > 0) { //echo "aa1aa";
					$paramsUpdateObat = array(
						'kode_obat'     => $dataobat->obat[$i]->kdObat,
						'tgl_srb'	    => $tgl_rujukan,
						'nama_obat'  	=> $dataobat->obat[$i]->nmObat,
						'jml_obat'  	=> $dataobat->obat[$i]->jmlObat,
						'signa'  		=> $dataobat->obat[$i]->signa1 . 'x' . $dataobat->obat[$i]->signa2
					);
					$criteriaObat = array(
						'no_sep'  => $no_sep,
						'no_srb'  => $no_srb,
						'urut'    => $urut
					);
					$this->db->where($criteriaObat);
					$this->db->update("history_prb_obat_bpjs", $paramsUpdateObat);
				} else { // echo "2";
					$paramsInsertPRB = array(
						'no_sep'                    => $no_sep,
						'no_srb'                    => $no_srb,
						'tgl_srb'  					=> $tgl_rujukan,
						'kode_obat'			  		=> $dataobat->obat[$i]->kdObat,
						'nama_obat'				  	=> $dataobat->obat[$i]->nmObat,
						'jml_obat'  				=> $dataobat->obat[$i]->jmlObat,
						'signa'			  			=> $dataobat->obat[$i]->signa1 . 'x' . $dataobat->obat[$i]->signa2,
						'urut'			  			=> $urut
					);
					$this->db->insert("history_prb_obat_bpjs", $paramsInsertPRB);
				}
				$no++;
			}
		}
	}

	// --------> 15 Feb 2022 By DP : Fingerprint -> Pencarian Peserta  <---------
	public function getValidasiFinger()
	{

		$noka = $this->input->post('noka');
		$poli = $this->input->post('poli_tujuan');
		$poli_tujuan = $this->input->post('poli_tujuan');
		$tgl_sep =  date('Y-m-d', strtotime($this->input->post('tgl_sep')));
		$sql_poli = $this->db->query("SELECT * FROM map_unit_bpjs WHERE unit_bpjs='" . $poli . "'");
		if ($sql_poli->num_rows() > 0) {
			$poli_tujuan = $poli;
		} else {
			$sql = $this->db->query("SELECT * FROM map_unit_bpjs WHERE kd_unit='" . $poli . "'");
			if ($sql->num_rows() > 0) {
				$poli_tujuan = $sql->row()->unit_referensi_dpjp;
			} else {
				echo "{validasi:false, pesan:'Poli Tujuan Tidak Ada', poli:'', search:'tidak ditemukan'}";
			}
		}

		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlValidasiFingerPrint'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $jenis_kontrol . '/KdPoli/' . $poli . '/TglRencanaKontrol/' . $tgl_kontrol, false, $context));
		$urlnya = $url . '/' . $noka . '/TglPelayanan/' . $tgl_sep;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);

		// echo $urlnya;
		// echo '<pre>' . var_export($response->response, true) . '</pre>';
		// die;
		// echo $poli_tujuan;
		$poli_finger = ['JAN', 'HDL', 'MAT', 'IRM'];

		if ($res->metaData->code == 200) {
			if (in_array($poli_tujuan, $poli_finger)) {
				if ($res->response->kode == "0") {
					echo "{validasi:false,pesan:'" . $res->response->status . "', poli:'" . $poli_tujuan . "', search:'ditemukan', kode:'" . $res->response->kode . "'}";
				} else {
					echo "{validasi:true, pesan:'', poli:'" . $poli_tujuan . "', search:'ditemukan', kode:'" . $res->response->kode . "'}";
				}
			} else {
				echo "{validasi:true, pesan:'', poli:'" . $poli_tujuan . "', search:'tidak ditemukan', kode:'" . $res->response->kode . "'}";
			}
		} else {
			echo "{validasi:false, pesan:'" . $res->metaData->message . "', poli:'" . $poli_tujuan . "', search:'tidak ditemukan'}";
		}
	}

	public function cek_history_rujukan()
	{
		$query = $this->db->query("SELECT * from history_rujukan_bpjs WHERE no_sep='" . $this->input->post('no_sep') . "'");
		if ($query->num_rows() > 0) {
			$data['noRujukan'] = $query->row()->no_rujukan;
			$data['noSep'] = $query->row()->no_sep;
			// $data['noKartu'] = $query->row()->no_rujukan;
			// $data['nama'] = $query->row()->no_rujukan;
			// $data['kelasRawat'] = $query->row()->no_rujukan;
			// $data['kelamin'] = $query->row()->no_rujukan;
			// $data['tglLahir'] = $query->row()->no_rujukan;
			// $data['tglSep'] = $query->row()->no_rujukan;
			$data['tglRujukan'] = $query->row()->tgl_rujukan;
			$data['tglRencanaKunjungan'] = $query->row()->tgl_renKunj;
			$data['ppkDirujuk'] = $query->row()->ppk_tujuan_dirujuk;
			// $data['namaPpkDirujuk'] = $query->row()->no_rujukan;
			$data['jnsPelayanan'] = $query->row()->jns_pelayanan_rujukan;
			$data['catatan'] = $query->row()->catatan_rujukan;
			$data['diagRujukan'] = $query->row()->diag_rujukan;
			// $data['namaDiagRujukan'] = $query->row()->no_rujukan;
			$data['tipeRujukan'] = $query->row()->tipe_rujukan;
			// $data['namaTipeRujukan'] = $query->row()->no_rujukan;
			$data['poliRujukan'] = $query->row()->poli_rujukan;
			$data['namaPoliRujukan'] = $query->row()->nama_poli_rujukan;
			echo '{sudah_ada:true, totalrecords:' . $query->num_rows() . ', listData:' . json_encode($data) . '}';
		} else {
			//vclaim 2.0 //hani 23-02-2022 
			//Cek Rujukan dari BPJS
			$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariRujukanKeluarRS'")->row()->nilai;
			$tgl_awal = Date('Y-m-d', strtotime('-30 days'));
			$tgl_akhir = date('Y-m-d');
			$query = [];

			$headers = $this->getSignature_new();
			$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
			$opts = array(
				'http' => array(
					'method' => 'GET',
					'header' => $headers
				)
			);
			$urlnya = $url . '' . $tgl_awal . '/tglAkhir/' . $tgl_akhir;
			$method = "GET"; // POST / PUT / DELETE
			$postdata = "";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $urlnya);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

			$response = curl_exec($curl);
			curl_close($curl);
			$res_noRujukan = json_decode($response);
			$res_noRujukan->response = $this->decompress($res_noRujukan->response, $timestamp);

			if ($res_noRujukan->metaData->code == 200 || $res_noRujukan->metaData->code == '200') {

				foreach ($res_noRujukan->response->list as $value) {
					// echo $value->noSep;
					if ($this->input->post('no_sep') == $value->noSep) {

						$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlRujukanKeluarRS'")->row()->nilai;
						$headers = $this->getSignature_new();
						$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
						$opts = array(
							'http' => array(
								'method' => 'GET',
								'header' => $headers
							)
						);

						$urlnya = $url . '' . $value->noRujukan;
						$method = "GET"; // POST / PUT / DELETE
						$postdata = "";
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $urlnya);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

						curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

						$response = curl_exec($curl);
						curl_close($curl);
						$res = json_decode($response);

						$res->response = $this->decompress($res->response, $timestamp);

						$query = $res->response->rujukan;
						// echo '<pre>'.var_export($res,true)."</pre>";
						// echo $urlnya;
						// die;
						break;
					}
				}
			}

			if (count($query) >= 1) {
				echo '{sudah_ada:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
			} else {
				echo '{sudah_ada:false}';
			}
		}
	}
	// --------> 04 Feb 2022 By DP : Referensi Cari Faskes <---------
	public function cari_faskes()
	{
		$tipe_fasker = $this->input->post('tipe_faskes');
		if ($tipe_fasker == "1") {
			$faskes = 1;
		} else {
			$faskes = 2;
		}
		$nama_faskes = $this->input->post('kd_faskes');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_ref_faskes'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url.$nama_faskes.'/'.$faskes,false,$context));
		$urlnya = $url . $nama_faskes . '/' . $faskes;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		echo '{success:true, totalrecords:' . count($res->response->faskes) . ', listData:' . json_encode($res->response->faskes) . '}';
	}

	// --------> 04 Feb 2022 By DP : Referensi Cari Poli <---------
	public function cari_poli()
	{
		$nama_poli_rujuk = $this->input->post('kd_poli');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_ref_poli'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url.'/'.$nama_poli_rujuk,false,$context));
		$urlnya = $url . '/' . $nama_poli_rujuk;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		echo '{success:true, totalrecords:' . count($res->response->poli) . ', listData:' . json_encode($res->response->poli) . '}';
	}

	// --------> 04 Feb 2022 By DP : Referensi Cari Diagnosa <---------
	public function cari_diagnosa()
	{
		$nama_diagnosa = $this->input->post('kd_diagnosa');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_diagnosa'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url.$nama_diagnosa,false,$context));
		$urlnya = $url . $nama_diagnosa;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		if ($res->metaData->code == 200) {
			echo '{success:true, totalrecords:' . count($res->response->diagnosa) . ', listData:' . json_encode($res->response->diagnosa) . '}';
		} else {
			echo json_encode($res);
		}
	}

	public function update_rujukan()
	{
		$no_sep = $this->input->post('no_sep');
		$jns_pelayanan = $this->input->post('jenis_pelayanan');
		$catatan_pelayanan = $this->input->post('catatan');
		$tipe_rujukan = $this->input->post('tipe_rujukan');
		$no_rujukan = $this->input->post('no_rujukan');
		$ppk_dirujuk = $this->input->post('pkk_dirujuk');
		$tipe = $this->input->post('tipe_rujukan_last');
		$diag_rujukan = $this->input->post('diag_rujukan');
		$poli_rujukan = $this->input->post('poli_rujukan');
		$tgl_dirujuk = $this->input->post('tgl_rujukan');
		$tgl_renKunj = $this->input->post('tgl_renKunj');
		$nama_poli_rujuk = $this->input->post('nama_poli_rujuk');
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_update_rujukan'")->row()->nilai;
		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$json_update = ' {
					       "request": {
					          "t_rujukan": {
					             "noRujukan": "' . $no_rujukan . '",
								 "tglRujukan": "' . $tgl_dirujuk . '",
                            	 "tglRencanaKunjungan":"' . $tgl_renKunj . '",
					             "ppkDirujuk": "' . $ppk_dirujuk . '",
					             "jnsPelayanan": "' . $jns_pelayanan . '",
					             "catatan": "' . $catatan_pelayanan . '",
					             "diagRujukan": "' . $diag_rujukan . '",
					             "tipeRujukan": "' . $tipe_rujukan . '",
					             "poliRujukan": "' . $poli_rujukan . '",
					             "user": "' . $cari_user . '"
					          }
					       }
					    }    ';
		//echo $json_update; die();	
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_update);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		$this->insert_response_bpjs($url, $respon_array, $json_update, "PUT");
		if ($respon_array->metaData->code == 200) {
			$this->update_history_rujukan_bpjs(
				$no_rujukan,
				$ppk_dirujuk,
				$tipe,
				$jns_pelayanan,
				$catatan_pelayanan,
				$diag_rujukan,
				$tipe_rujukan,
				$poli_rujukan,
				$tgl_dirujuk,
				$no_sep,
				$nama_poli_rujuk
			);
		}
		echo json_encode($respon_array);
	}

	public function update_history_rujukan_bpjs(
		$no_rujukan,
		$ppk_dirujuk,
		$tipe,
		$jns_pelayanan,
		$catatan_pelayanan,
		$diag_rujukan,
		$tipe_rujukan,
		$poli_rujukan,
		$tgl_dirujuk,
		$no_sep,
		$nama_poli_rujuk
	) {
		if (stripos($ppk_dirujuk, "R") !== false) {
			$tipe_faskes = 2;
		} else {
			$tipe_faskes = 1;
		}
		$paramsUpdate = array(
			'no_sep'                    => $no_sep,
			'tgl_rujukan'  				=> $tgl_dirujuk,
			'ppk_tujuan_dirujuk'  		=> $ppk_dirujuk,
			'jns_pelayanan_rujukan'  	=> $jns_pelayanan,
			'catatan_rujukan'  			=> $catatan_pelayanan,
			'diag_rujukan'  			=> $diag_rujukan,
			'tipe_rujukan'  			=> $tipe_rujukan,
			'poli_rujukan'  			=> $poli_rujukan,
			'no_rujukan'                => $no_rujukan,
			'tipe_faskes' 				=> $tipe_faskes,
			'nama_poli_rujukan' 		=> $nama_poli_rujuk
		);
		$criteria = array(
			'no_sep'  => $no_sep,
			'no_rujukan'  => $no_rujukan
		);
		$this->db->where($criteria);
		$this->db->update("history_rujukan_bpjs", $paramsUpdate);
	}

	// --------> 04 Feb 2022 By DP : Hapus Rujukan <---------
	public function hapus_rujukan()
	{
		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_delete_rujukan'")->row()->nilai;
		$json_delete = ' {
					        "request": {
					            "t_rujukan": {
					                "noRujukan": "' . $this->input->post('no_rujukan') . '",
					                "user": "' . $cari_user . '"
					            }
					        }
					    }   ';
		//echo $json_delete; die();			    
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_delete);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		$res = json_decode($response);
		if ($res->metaData->code == 200) {
			$delete = $this->db->query("DELETE FROM history_rujukan_bpjs WHERE no_rujukan='" . $this->input->post('no_rujukan') . "' ");
		}
		$this->insert_response_bpjs($url, $respon_array, $json_delete, "DELETE");
		echo json_encode($respon_array);
	}

	// --------> 04 Feb 2022 By DP : Hapus Rujukan Balik <---------
	public function hapus_rujukan_balik()
	{
		$kd_user = $this->session->userdata['user_id']['id'];
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_delete_prb'")->row()->nilai;
		$json_delete = ' {
					        "request": {
					            "t_prb": {
					                "noSrb": "' . $this->input->post('no_rujukan') . '",
									"noSep": "' . $this->input->post('no_sep') . '",
					                "user": "' . $cari_user . '"
					            }
					        }
					    }   ';
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_delete);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		if ($respon_array->metaData->code == 200) {
			$delete = $this->db->query("DELETE FROM history_prb_bpjs WHERE no_srb='" . $this->input->post('no_rujukan') . "' AND no_sep='" . $this->input->post('no_sep') . "'");
			if ($delete) {
				$this->db->query("DELETE FROM history_prb_obat_bpjs WHERE no_srb='" . $this->input->post('no_rujukan') . "' AND no_sep='" . $this->input->post('no_sep') . "'");
			}
		}
		$this->insert_response_bpjs($url, $respon_array, $json_delete, "DELETE");
		echo json_encode($respon_array);
	}

	// ---------------------------Get SEP RWI-----------------------------------------------
	// Egi 07 Febuari 2022
	public function getSepRWI()
	{
		$kd_pasien = $_POST['kd_pasien'];
		$nokartu = $_POST['noka'];
		$no_rujukan = $_POST['no_rujukan'];
		$sep_rwi = '';
		$noka = '';
		$no_surat_kontrol = '';
		$kode_poli = '';
		$nama_poli = "";
		$kode_dokter = '';
		$nama_dokter = '';
		$diag_awal = '';
		$kd_prov = '';
		$kd_kab = '';
		$kd_kec = '';
		$no_tlp = '';
		$kd_dpjp = '';

		$cek_kunjungan = $this->db->query("SELECT k.KD_RUJUKAN,K.CARA_PENERIMAAN,r.RUJUKAN,K.KD_UNIT,u.NAMA_UNIT,hsb.diag_awal,hsk.no_surat_kontrol,hsk.kode_poli,
		hsk.nama_poli,hsk.kode_dokter,hsk.nama_dokter,C.CUSTOMER,K.KD_CUSTOMER,kon.JENIS_CUST,p.NO_ASURANSI,
		(SELECT	COALESCE((SELECT NO_SJP 
		FROM kunjungan 
		WHERE kd_pasien = '" . $kd_pasien . "' AND LEFT ( kd_unit, 1 ) = '1' AND NO_SJP IS NOT NULL 
		AND NO_SJP <> '' ORDER BY TGL_MASUK DESC LIMIT 1),'' ) ) AS SEP_RWI 
		FROM kunjungan k
		INNER JOIN history_sep_bpjs hsb ON hsb.no_sep=k.no_sjp AND hsb.kd_pasien=k.kd_pasien 
		AND hsb.kd_unit=k.kd_unit AND hsb.tgl_masuk=k.tgl_masuk 
		INNER JOIN history_surat_kontrol_bpjs hsk ON hsk.no_sep=hsb.no_sep AND hsk.jenis_kontrol='2'
		INNER JOIN rujukan r ON k.kd_rujukan = r.kd_rujukan
		INNER JOIN unit u ON k.kd_unit = u.kd_unit
		INNER JOIN dokter d ON k.kd_dokter = d.kd_dokter
		INNER JOIN customer c ON c.kd_customer = k.kd_customer
		INNER JOIN kontraktor kon ON kon.kd_customer = c.kd_customer
		LEFT JOIN PASIEN p ON p.KD_PASIEN = k.KD_PASIEN 
		WHERE k.kd_pasien = '" . $kd_pasien . "' AND hsk.tgl_kontrol = '" . date('Y-m-d') . "'
		ORDER BY k.tgl_masuk DESC,	k.urut_masuk DESC	LIMIT 1");
		// echo "<pre>". $cek_kunjungan->row()->sep_rwi."</pre>"; die;
		if ($cek_kunjungan->num_rows()  > 0) {
			$dt = $cek_kunjungan->row();
			$sep_rwi = $dt->sep_rwi;
			$noka = $dt->no_asuransi;
			$diag_awal = $dt->diag_awal;
			$no_surat_kontrol = $dt->no_surat_kontrol;
			$kode_poli = $dt->kode_poli;
			$nama_poli = $dt->nama_poli;
			$kode_dokter = $dt->kode_dokter;
			$nama_dokter = $dt->nama_dokter;
			echo '{success:true, sep_rwi:"' . $sep_rwi . '", noka:"' . $noka . '", noSuratKontrol:"' . $no_surat_kontrol . '", kode_poli:"' . $kode_poli . '", nama_poli:"' . $nama_poli . '", kode_dokter:"' . $kode_dokter . '", nama_dokter:"' . $nama_dokter . '", diag_awal:"' . $diag_awal . '"}';
		} else {
			// Get History Pelayanan
			$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlMonitoringPelayanan'")->row()->nilai;
			$ppkRujukan = $this->db->query("select keterangan  from seting_bpjs where key_setting='PpkPelayanan'")->row()->keterangan;
			$tgl_awal = Date('Y-m-d', strtotime('-90 days'));
			$tgl_akhir = date('Y-m-d');
			$headers = $this->getSignature_new();
			$timestamp = preg_replace(
				"/[^0-9]/",
				"",
				$headers[1]
			);
			$urlnya = $url . '' . $nokartu . '/' . 'tglMulai/' . $tgl_awal . '/tglAkhir/' . $tgl_akhir;
			$method = "GET"; // POST / PUT / DELETE
			$postdata = "";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $urlnya);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

			$response = curl_exec($curl);
			curl_close($curl);
			$res = json_decode($response);

			$res->response = $this->decompress($res->response, $timestamp);
			// echo $urlnya;

			if (
				$res->metaData->code == 200 || $res->metaData->code == '200'
			) {

				foreach ($res->response->histori as $value) {
					// echo $value->ppkPelayanan;
					// echo $value->jnsPelayanan;
					if ($ppkRujukan == $value->ppkPelayanan && $value->jnsPelayanan == '1') { //echo " aa<br>";
						$sep_rwi = $value->noSep;
						$noka = $value->noKartu;
						$diag_awal = explode("-", $value->diagnosa);
						$diag_awal = $diag_awal[0];

						//GET NO SPRI dari BPJS
						$url = $this->db->query("select nilai  from seting_bpjs where key_setting='urlGetNoSuratKontrolByNoka'")->row()->nilai;
						$headers = $this->getSignature_new();
						$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
						$opts = array(
							'http' => array(
								'method' => 'GET',
								'header' => $headers
							)
						);
						$urlnya = $url . '' . date('m') . '/Tahun/' . date('Y') . '/Nokartu/' . $noka . '/filter/2';
						$method = "GET"; // POST / PUT / DELETE
						$postdata = "";
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $urlnya);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

						curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

						$response = curl_exec($curl);
						curl_close($curl);
						$res = json_decode($response);

						$res->response = $this->decompress($res->response, $timestamp);

						if ($res->metaData->code == 200 || $res->metaData->code == '200') {
							foreach ($res->response->list as $value) {
								// echo $value->noSepAsalKontrol."<br>";
								// echo $value->jnsKontrol."<br>";
								// echo $value->jnsPelayanan."<br>";
								// echo $value->terbitSEP."<br>";
								// echo $sep_rwi."<br>";
								if ($value->jnsKontrol == "2" && $value->jnsPelayanan == 'Rawat Inap' && $value->noSepAsalKontrol == $sep_rwi && $value->terbitSEP == "Belum" && $value->tglRencanaKontrol == date('Y-m-d')) {  // && $value->jnsPelayanan == 'Rawat Inap' && $value->noSepAsalKontrol == $sep_rwi
									// echo "ii";
									$no_surat_kontrol = $value->noSuratKontrol;
									$kode_poli = $value->poliTujuan;
									$nama_poli = $value->namaPoliTujuan;
									$kode_dokter = $value->kodeDokter;
									$nama_dokter = $value->namaDokter;
									break;
								}
							}
						}
						break;
					}
				}
				if ($no_surat_kontrol != '') {
					echo '{success:true, sep_rwi:"' . $sep_rwi . '", noka:"' . $noka . '", noSuratKontrol:"' . $no_surat_kontrol . '", kode_poli:"' . $kode_poli . '", nama_poli:"' . $nama_poli . '", kode_dokter:"' . $kode_dokter . '", nama_dokter:"' . $nama_dokter . '", diag_awal:"' . $diag_awal . '"}';
				} else {
					echo '{success:false}';
				}
			} else {
				echo '{success:false}';
			}
		}
	}
	// -------------------------------------------------------------------------------------

	/*DIGUNAKAN UNTUK MENGHAPUS DATA YAHG SALAH TRANSFER APOTEK*/
	public function cek_apotek_transfer()
	{
		$no_transaksi = $this->input->post('no_transaksi');
		/* $del=$this->db->query("DELETE FROM detail_transaksi WHERE kd_produk IN ('1','7') AND folio='A'  AND (LENGTH (NO_FAKTUR)=0 OR NO_FAKTUR IS NULL)  AND no_transaksi='".$no_transaksi."' AND kd_kasir='06'");
		
		if($del){
			echo '{success:true}';
		}else{
			echo '{success:false}';
		} */
		echo '{success:true}';
	}

	public function cek_history_rencana_kontrol()
	{
		// if ($this->input->post('jenis_kontrol') == 1) {
		// 	$query = $this->db->query("SELECT * from history_surat_kontrol_bpjs 
		// 	WHERE noka='" . $this->input->post('no_kartu') . "' AND jenis_kontrol='1' AND (status=FALSE OR status is null) ORDER BY tgl_kontrol LIMIT 1");
		// } else {
		// 	$query = $this->db->query("SELECT * from history_surat_kontrol_bpjs 
		// 	WHERE no_sep='" . $this->input->post('no_sep') . "' AND jenis_kontrol='2' AND (status=FALSE OR status is null) ORDER BY tgl_kontrol LIMIT 1");
		// }
		// if($query->num_rows()>0){
		// 	$data['noSuratKontrol'] 	= $query->row()->no_surat_kontrol;
		// 	$data['jnsPelayanan'] 		= $query->row()->no_surat_kontrol;
		// 	$data['jnsKontrol'] 		= $query->row()->jenis_kontrol;
		// 	$data['namaJnsKontrol'] 	= $query->row()->no_surat_kontrol;
		// 	$data['tglRencanaKontrol'] 	= $query->row()->tgl_kontrol;
		// 	$data['tglTerbitKontrol'] 	= $query->row()->tgl_surat;
		// 	$data['noSepAsalKontrol'] 	= $query->row()->no_sep;
		// 	$data['poliAsal'] 			= $query->row()->no_surat_kontrol;
		// 	$data['namaPoliAsal'] 		= $query->row()->no_surat_kontrol;
		// 	$data['poliTujuan'] 		= $query->row()->kode_poli;
		// 	$data['namaPoliTujuan'] 	= $query->row()->nama_poli;
		// 	$data['tglSEP'] 			= $query->row()->no_surat_kontrol;
		// 	$data['kodeDokter'] 		= $query->row()->kode_dokter;
		// 	$data['namaDokter'] 		= $query->row()->nama_dokter;
		// 	$data['noKartu'] 			= $query->row()->noka;
		// 	$data['nama'] 				= $query->row()->no_surat_kontrol;
		// 	$data['status'] 			= $query->row()->no_surat_kontrol;
		// }
		//GET NO SPRI dari BPJS
		$query = [];
		$kode_poli = '';
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='urlGetNoSuratKontrolByNoka'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$urlnya = $url . '' . date('m') . '/Tahun/' . date('Y') . '/Nokartu/' . $this->input->post('noka') . '/filter/2';
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$res = json_decode($response);

		$res->response = $this->decompress($res->response, $timestamp);

		// echo $urlnya;
		// echo "<pre>" . var_export($res, true) . "</pre>";
		// die;
		$i = 0;
		if ($res->metaData->code == 200 || $res->metaData->code == '200') {
			foreach ($res->response->list as $value) {
				// echo $value->noSepAsalKontrol."<br>";
				// echo $value->jnsKontrol."<br>";
				// echo $value->jnsPelayanan."<br>";
				// echo $value->terbitSEP."<br>";
				// echo $sep_rwi."<br>";
				if ($this->input->post('jenis_kontrol') ==  $value->jnsKontrol && $value->terbitSEP == "Belum") {  // && $value->jnsPelayanan == 'Rawat Inap' && $value->noSepAsalKontrol == $sep_rwi
					// echo "ii";
					// echo "<pre>".var_export($res,true)."</pre>";

					//cari kodepoli untuk jenis kontrol 1
					if ($this->input->post('jenis_kontrol') == 1) {
						$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_poli_kontrol'")->row()->nilai;
						$headers = $this->getSignature_new();
						$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
						$opts = array(
							'http' => array(
								'method' => 'GET',
								'header' => $headers
							)
						);
						$urlnya = $url . '/1/nomor/' . $this->input->post('noka') . '/TglRencanaKontrol/' .  date("Y-m-d");

						$method = "GET"; // POST / PUT / DELETE
						$postdata = "";
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL, $urlnya);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

						curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

						$response = curl_exec($curl);
						curl_close($curl);
						$res = json_decode($response);

						$res->response = $this->decompress($res->response, $timestamp);
						foreach ($res->response->list as $value_poli) {
							if (trim($value->namaPoliTujuan) == strtoupper(trim($value_poli->namaPoli))) {
								$kode_poli = $value_poli->kodePoli;
								break;
							}
						}
					} else {
						$noSep = $this->input->post('no_sep');
						if (trim($value->noSepAsalKontrol) == trim($noSep)) {
							$kode_poli = $value->poliTujuan;
						}
					}
					if ($kode_poli != "" || $kode_poli != null) {
						$query = $value;
						$query->poliTujuan = $kode_poli;
						break;
					}
				}
				$i++;
			}
		}
		if (count($query) >= 1) {
			echo '{sudah_ada:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
		} else {
			echo '{sudah_ada:false}';
		}
	}

	public function hapus_rencana_kontrol()
	{
		$kd_user = $this->session->userdata['user_id']['id'];
		// $no_sep = $this->input->post('no_sep');
		$no_kontrol = $this->input->post("no_kontrol");
		$cari_user = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai from seting_bpjs where key_setting='url_delete_rencana_kontrol'")->row()->nilai;
		$json_delete = ' {
						"request": {
							"t_suratkontrol":{
							"noSuratKontrol": "' . $no_kontrol . '",
							"user": "' . $cari_user . '"
							}
						}
					} ';

		$cek_surat = $this->db->query("select * from history_sep_bpjs b inner join history_surat_kontrol_bpjs a on b.no_surat_dpjp = a.no_surat_kontrol 
			where a.no_surat_kontrol = '" . $no_kontrol . "'")->result();
		if (count($cek_surat) > 0) {
			$respon_array['metaData']['code'] = '0';
			$respon_array['metaData']['message'] = 'No Surat Kontrol sudah digunakan';
			// echo json_encode($respon_array);
			// die();
		}

		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_delete);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		if ($respon_array->metaData->code == 200) {
			// $this->db->query("DELETE FROM history_surat_kontrol_bpjs WHERE no_surat_kontrol='" . $no_kontrol . "' AND no_sep='" . $no_sep . "'");
			$this->db->query("DELETE FROM history_surat_kontrol_bpjs WHERE no_surat_kontrol='" . $no_kontrol . "'");
		}
		$this->insert_response_bpjs($url, $respon_array, $json_delete, "DELETE");
		echo json_encode($respon_array);
	}

	public function cek_kunjungan_kontrol()
	{
		$unit = $this->input->post('unit');
		$spesialis = $this->input->post('spesialis');
		$kd_pasien = $this->input->post('kd_pasien');
		$no_rujukan = $this->input->post('no_rujukan');
		$date_now = date('Y-m-d');

		$cari_kunjungan_rujukan = $this->db->query("SELECT * FROM rujukan_kunjungan WHERE nomor_rujukan='" . $no_rujukan . "' and kd_pasien='" . $kd_pasien . "' and kd_unit='" . $unit . "' ORDER BY tgl_masuk asc")->result();
		$kunjungan_pertama = count($cari_kunjungan_rujukan) >= 1 ? 'true' : 'false';

		// if ($kunjungan_pertama == 'false') {
		// 	$cek_sep = $this->db->query("SELECT * FROM history_sep_bpjs WHERE no_rujukan='" . $no_rujukan . "' and kd_pasien='" . $kd_pasien . "' and kd_unit='" . $unit . "' and ")->result();
		// } 
		echo '{surat_kontrol:"' . $kunjungan_pertama . '"}';
	}

	// --------------Cek Hiistory Sep Bpjs----------------
	public function cek_sep()
	{
		$no_rujukan = $this->input->post('no_rujukan');
		$kd_pasien = $this->input->post('kd_pasien');
		$kd_unit = $this->input->post('kd_unit');
		$cari_kunjungan_rujukan = $this->db->query("SELECT * FROM history_sep_bpjs WHERE no_rujukan='" . $no_rujukan . "' and kd_pasien='" . $kd_pasien . "' and kd_unit='" . $kd_unit . "' ORDER BY tgl_masuk desc ")->result();
		if (count($cari_kunjungan_rujukan) >= 1) {
			$kunjungan_pertama = 'false';
		} else {
			$kunjungan_pertama = 'true';
		}
		echo '{kunjungan_pertama:' . $kunjungan_pertama . '}';
	}

	public function get_poliKontrol()
	{
		$now = date("Y-m-d");
		$jenis_kontrol = $this->input->post('jenis_kontrol');
		$jenis_pelayanan = $this->input->post('jenis_pelayanan');
		$noka = $this->input->post('noka');
		$no_surat_kontrol = $this->input->post('no_surat_kontrol');
		$sep = $this->input->post('sep');
		if ($jenis_kontrol == 1) {
			$no = $noka;
		} else {
			$no = $sep;
		}
		$tgl_kontrol =  date('Y-m-d', strtotime($this->input->post('tgl_kontrol')));
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_poli_kontrol'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);

		$poli_RWI = '';
		$kd_poli_RWI = '';
		$nama_poli_RWI = '';
		if ($jenis_pelayanan == "Rawat Inap" && $jenis_kontrol == 2) {
			$poli_RWI = $this->db->query("Select kode_poli,nama_poli From history_surat_kontrol_bpjs where no_surat_kontrol='" . $no_surat_kontrol . "'")->row();
			$kd_poli_RWI = $poli_RWI->kode_poli;
			$nama_poli_RWI = $poli_RWI->nama_poli;
		}

		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $jenis_kontrol . '/nomor/' . $no . '/TglRencanaKontrol/' . $tgl_kontrol, false, $context));
		$urlnya = $url . '/' . $jenis_kontrol . '/nomor/' . $no . '/TglRencanaKontrol/' . $tgl_kontrol;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo json_encode($res->response->list);
		// echo count($res->response->list);
		// echo $url . '/' . $jenis_kontrol . '/nomor/' . $no . '/TglRencanaKontrol/' . $tgl_kontrol;
		// die;
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '
				, poli_rwi: "' . $kd_poli_RWI . '", nama_poli:"' . $nama_poli_RWI . '"}';
		} else {
			echo '{success:false, totalrecords:0, listData:[],pesan:"' . $res->metaData->message . '"}';
		}
	}

	// ---------------------------Proses Get Cara Keluar BPJS--------------------------------
	// Egi 09 Febuari 2022
	public function get_carakeluar()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='urlGetCaraKeluar'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$urlnya = $url;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
		} else {
			echo '{success:false, totalrecords:0, listData:[],pesan:"' . $res->metaData->message . '"}';
		}
	}
	// --------------------------------------------------------------------------------------

	public function get_dokterKontrol()
	{
		$now = date("Y-m-d");
		$jenis_kontrol = $this->input->post('jenis_kontrol');
		$poli = $this->input->post('poli');
		$tgl_kontrol =  date('Y-m-d', strtotime($this->input->post('tgl_kontrol')));
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_dokter_kontrol'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $jenis_kontrol . '/KdPoli/' . $poli . '/TglRencanaKontrol/' . $tgl_kontrol, false, $context));
		$urlnya = $url . '/' . $jenis_kontrol . '/KdPoli/' . $poli . '/TglRencanaKontrol/' . $tgl_kontrol;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo json_encode($res->response->list);
		// echo count($res->response->list);
		// echo $url . '/' . $jenis_kontrol . '/KdPoli/' . $poli . '/TglRencanaKontrol/' . $tgl_kontrol;
		// die;
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
		} else {
			echo '{success:true, totalrecords:0, listData:[]}';
		}
	}

	public function insert_rencana_kontrol()
	{

		$no_sep            = $this->input->post('no_sep');
		$noka    		   = $this->input->post('noka');
		$tgl_kontrol	   = $this->input->post('tgl_kontrol');
		$tgl_surat		   = $this->input->post('tgl_surat');
		$jenis_kontrol     = $this->input->post('jenisKontrol');
		$kode_poli 		   = $this->input->post('kodePoli');
		$nama_poli	       = $this->input->post('namaPoli');
		$kode_dokter       = $this->input->post('kodeDokter');
		$nama_dokter       = $this->input->post('namaDokter');
		$diagnosa	       = $this->input->post('diagnosa');
		$kd_user           = $this->session->userdata['user_id']['id'];
		$cari_user         = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$key_setting 	   = '';
		$noKontrol		   = '';
		$tmpNoKontrol		   = '';
		if ($jenis_kontrol == 1) {
			$key_setting   = 'url_insert_SPRI';
			$tmpNoKontrol  = 'noKartu';
			$noKontrol	   = $noka;
		} else {
			$key_setting   = 'url_insert_rencana_kontrol';
			$tmpNoKontrol  = 'noSEP';
			$noKontrol	   = $no_sep;
		}
		$url               = $this->db->query("select nilai from seting_bpjs where key_setting='" . $key_setting . "'")->row()->nilai;

		$json = ' {
					"request": {
						"' . $tmpNoKontrol . '":"' . $noKontrol . '",
						"kodeDokter":"' . $kode_dokter . '",
						"poliKontrol":"' . $kode_poli . '",
						"tglRencanaKontrol":"' . $tgl_kontrol . '",
						"user":"' . $cari_user . '"
					}
				}';
		// echo $json;
		// die();
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		if ($respon_array->metaData->code == 200 && $respon_array->response != '') {
			$this->save_history_rencana_kontrol_bpjs($no_sep, $noka, $jenis_kontrol, $kode_poli, $nama_poli, $kode_dokter, $respon_array, $cari_user, $tgl_surat, $tmpNoKontrol);
		}
		$this->insert_response_bpjs($url, $respon_array, $json, "POST");
		echo json_encode($respon_array);
	}

	public function save_history_rencana_kontrol_bpjs($no_sep, $noka, $jenis_kontrol, $kode_poli, $nama_poli, $kode_dokter, $data, $user, $tgl_surat, $cekKontrol)
	{
		$noKontrol = '';
		if ($cekKontrol == 'noKartu') {
			$noKontrol = $data->response->noSPRI;
		} else {
			$noKontrol = $data->response->noSuratKontrol;
		}
		$paramsInsert = array(
			'no_sep'                    => $no_sep,
			'noka'	                    => $noka,
			'no_surat_kontrol'  		=> $noKontrol,
			'tgl_kontrol' 		 		=> $data->response->tglRencanaKontrol,
			'kode_dokter'			  	=> $kode_dokter,
			'nama_dokter'  				=> $data->response->namaDokter,
			'kode_poli'		  			=> $kode_poli,
			'nama_poli'			  		=> $nama_poli,
			'jenis_kontrol'			  	=> $jenis_kontrol,
			'user'		                => $user,
			'tgl_surat'					=> $tgl_surat,
		);
		$this->db->insert("history_surat_kontrol_bpjs", $paramsInsert);
	}

	public function update_rencana_kontrol()
	{
		$no_sep            = $this->input->post('no_sep_update');
		$no_surat_kontrol  = $this->input->post('no_surat_kontrol');
		$noka    		   = $this->input->post('noka_update');
		$tgl_kontrol	   = $this->input->post('tgl_kontrol_update');
		$tgl_surat		   = $this->input->post('tgl_surat_update');
		$jenis_kontrol     = $this->input->post('jenisKontrol_update');
		$kode_poli 		   = $this->input->post('kodePoli_update');
		$nama_poli	       = $this->input->post('namaPoli_update');
		$kode_dokter       = $this->input->post('kodeDokter_update');
		$nama_dokter       = $this->input->post('namaDokter_update');
		$diagnosa	       = $this->input->post('diagnosa_update');
		$kd_user           = $this->session->userdata['user_id']['id'];
		$cari_user         = $this->db->query("SELECT * FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->user_names;
		$key_setting 	   = '';
		$noKontrol 		   = '';
		if ($jenis_kontrol == 1) {
			$key_setting   = 'url_update_SPRI';
			$noKontrol	   = 'noSPRI';
		} else {
			$key_setting   = 'url_update_rencana_kontrol';
			$noKontrol	   = 'noSuratKontrol';
		}
		$url               = $this->db->query("select nilai from seting_bpjs where key_setting='" . $key_setting . "'")->row()->nilai;

		$json_update = ' {
					"request": {
						"' . $noKontrol . '":"' . $no_surat_kontrol . '",
						"kodeDokter":"' . $kode_dokter . '",
						"poliKontrol":"' . $kode_poli . '",
						"tglRencanaKontrol":"' . $tgl_kontrol . '",
						"user":"' . $cari_user . '"
					}
				}';
		$headers = $this->getSignatureVedika_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$response = array();
		$curl = curl_init();
		// echo '<pre>' . var_export($json_update, true) . '</pre>';
		// echo $url;
		// die;
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_update);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);
		$respon_array = json_decode($response);
		$respon_array->response = $this->decompress($respon_array->response, $timestamp);
		$this->insert_response_bpjs($url, $respon_array, $json_update, "PUT");
		if ($respon_array->metaData->code == 200) {
			$this->update_history_rencana_kontrol_bpjs($no_sep, $no_surat_kontrol, $jenis_kontrol, $kode_poli, $nama_poli, $kode_dokter, $respon_array, $cari_user, $tgl_surat);
		}
		echo json_encode($respon_array);
	}

	public function update_history_rencana_kontrol_bpjs($no_sep, $no_surat_kontrol, $jenis_kontrol, $kode_poli, $nama_poli, $kode_dokter, $data, $user, $tgl_surat)
	{
		$paramsUpdate = array(
			'tgl_kontrol' 		 		=> $data->response->tglRencanaKontrol,
			'kode_dokter'			  	=> $kode_dokter,
			'nama_dokter'  				=> $data->response->namaDokter,
			'kode_poli'		  			=> $kode_poli,
			'nama_poli'			  		=> $nama_poli,
			'jenis_kontrol'			  	=> $jenis_kontrol,
			'user'		                => $user,
			'tgl_surat'					=> $tgl_surat
		);
		$criteria = array(
			'no_sep'                    => $no_sep,
			'no_surat_kontrol'			=> $no_surat_kontrol
		);
		$this->db->where($criteria);
		$this->db->update("history_surat_kontrol_bpjs", $paramsUpdate);
	}

	public function get_programPRB()
	{
		$now = date("Y-m-d");
		$unit = $this->input->post('unit');
		$tgl_rujukan =  date('Y-m-d', strtotime($this->input->post('tgl_rujukan')));
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_program_prb'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url, false, $context));
		$urlnya = $url;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo json_encode($res->response->list);
		// echo count($res->response->list);
		// echo $url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit;
		// die;
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
		} else {
			echo '{success:true, totalrecords:0, listData:[]}';
		}
	}

	public function get_dokterPRB()
	{
		$now = date("Y-m-d");
		$kd_spesialis = $this->input->post('spesialis');
		$unit = $this->input->post('unit');
		if (is_numeric($unit) == true) {
			$unit = $this->db->query("SELECT unit_bpjs FROM map_unit_bpjs WHERE kd_unit='" . $unit . "'")->row()->unit_bpjs;
		}
		if ($unit == 'IGD') {
			$pelayanan = 1;
		} else if ($unit != 'RWI') {
			$pelayanan = 2;
		} else {
			$pelayanan = 1;
		}
		$tgl_rujukan =  date('Y-m-d', strtotime($this->input->post('tgl_rujukan')));
		$bpjs_kd_res = $this->db->query("select setting  from sys_setting where key_data='bpjs_kd_res'")->row()->setting;
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='url_cari_dokter_bpjs'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		// $res = json_decode(file_get_contents($url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit, false, $context));
		$urlnya = $url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);
		$res->response = $this->decompress($res->response, $timestamp);
		// echo '<pre>' . var_export($res, true) . '</pre>';
		// echo json_encode($res->response->list);
		// echo count($res->response->list);
		// echo $url . '/' . $pelayanan . '/tglPelayanan/' . $now . '/Spesialis/' . $unit;
		// die;
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
		} else {
			echo '{success:true, totalrecords:0, listData:[]}';
		}
	}

	public function cek_history_rujukan_balik()
	{
		$query = $this->db->query("SELECT * from history_prb_bpjs WHERE no_sep='" . $this->input->post('no_sep') . "'")->result();
		if (count($query) >= 1) {
			echo '{sudah_ada:true, totalrecords:' . count($query) . ', listData:' . json_encode($query) . '}';
		} else {
			echo '{sudah_ada:false}';
		}
		//echo $sudah_ada;
	}

	public function get_history_rujukan_internal()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEPInternal'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		$no = $_POST['no_sep'];
		$urlnya = $url . '/' . $no;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);

		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		if (count($res->response) > 0) {
			echo '{success:true, totalrecords:' . count($res->response->list) . ', listData:' . json_encode($res->response->list) . '}';
		} else {
			echo '{success:true, totalrecords:0, listData:[]}';
		}
	}

	public function cariHistorySEPInternal()
	{
		$response = array();
		$noka = $this->input->post('no_kartu');
		$query = $this->db->query("SELECT no_sep,count(*) as jml FROM history_sep_bpjs WHERE no_kartu='" . $noka . "' GROUP BY no_sep");
		$data  = array();
		if ($query->num_rows() > 0) {
			$index = 0;
			foreach ($query->result() as $result) {
				if ($result->jml > 1) {
					$data[$index]['noSEP'] = $result->no_sep;
					$index++;
				}
			}
		}
		$response['data'] = array(
			'response' 	=> array(
				'list' 	=> $data,
			)
		);
		echo json_encode($response);
	}

	public function getDataBpjsDenganSEPInternal()
	{
		$url = $this->db->query("select nilai  from seting_bpjs where key_setting='UrlCariSEPInternal'")->row()->nilai;
		$headers = $this->getSignature_new();
		$timestamp = preg_replace("/[^0-9]/", "", $headers[1]);
		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => $headers
			)
		);
		// $context = stream_context_create($opts);
		$no = $_POST['no_sep'];
		// $res = json_decode(file_get_contents($url . '/' . $no, false, $context));
		$urlnya = $url . '/' . $no;
		$method = "GET"; // POST / PUT / DELETE
		$postdata = "";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlnya);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($curl);
		curl_close($curl);

		$res = json_decode($response);

		$res->response = $this->decompress($res->response, $timestamp);
		$dat = array(
			'data' => null
		);
		if ($res->metaData->code == '200') {
			$dat['data'] = $res;
		} else {
			$dat['message'] = $this->searchMessage($res);
		}
		$this->insert_response_bpjs($url . '/' . $no, $res, json_encode($res), "GET");
		echo json_encode($dat);
	}

	public function getNoRujukanPendaftaran()
	{
		$params = array(
			'kd_pasien' 	=> $this->input->post('kd_pasien'),
			'tgl_masuk' 	=> $this->input->post('tgl_masuk'),
			'kd_unit' 		=> $this->input->post('kd_unit'),
			'urut_masuk' 	=> $this->input->post('urut_masuk'),
		);
		$this->db->select("*");
		$this->db->where($params);
		$this->db->from("history_sep_bpjs");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$noRujukan 	= $query->row()->no_rujukan;
			$noKartu 	= $query->row()->no_kartu;
			$noSep 		= $query->row()->no_sep;
			$count = $query->num_rows();
			// get no SPRI
			$params1 = array(
				'noka' => $noKartu,
				'no_sep' => $noSep,
			);
			$this->db->select("*");
			$this->db->where($params1);
			$this->db->from("history_surat_kontrol_bpjs");
			$res = $this->db->get();
			if ($res->num_rows() > 0) {
				$no_spri = $res->row()->no_surat_kontrol;
			} else {
				$no_spri = "";
			}
		} else {
			$noRujukan = "";
			$no_spri = "";
			$count = 0;
		}
		$arr = array(
			'no_rujukan' => $noRujukan,
			'no_spri' => $no_spri
		);
		echo '{success:true, totalrecords:' . $count . ', listData:' . json_encode($arr) . '}';
	}
}
