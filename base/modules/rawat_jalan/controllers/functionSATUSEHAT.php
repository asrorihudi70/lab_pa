<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {

class functionSATUSEHAT extends  MX_Controller
{

  public $ErrLoginMsg = '';
  private $dbSQL      = "";

  public function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    date_default_timezone_set("Asia/Makassar");
    $now = strtotime(date('Y-m-d H:i:s'));
    $this->start_time = date_format(date_timestamp_set(new DateTime(), $now), 'c');

    $this->jam_request = date('H');
    $this->menit_request = date('i');
    $this->detik_request = date('s');
  }

  public function index()
  {
    $this->load->view('main/index');
  }

  private function getToken()
  {
<<<<<<< HEAD
    $tmp_secretKey = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_password'")->row()->nilai;
    $tmp_client_id = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    $url = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlToken'")->row()->nilai;
=======
    $tmp_secretKey = $this->db->query("SELECT nilai from SETTING_SATUSEHAT where key_setting='x_password'")->row()->nilai;
    $tmp_client_id = $this->db->query("SELECT nilai from SETTING_SATUSEHAT where key_setting='x_username'")->row()->nilai;
    $url = $this->db->query("SELECT nilai from SETTING_SATUSEHAT where key_setting='urlGetToken'")->row()->nilai;
>>>>>>> 8ffe52a02ac518e1fdd672b59f2548c563f6cd89
    $headers = array("x-username: " . $tmp_client_id, "x-password: " . $tmp_secretKey);
    // echo "<pre>".var_export($headers,true)."</pre>"; 
    $json = '';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($curl);
    curl_close($curl);
    $respon = json_encode($response);
    $respon_array = json_decode($respon);
    // echo "<pre>".var_export($json,true)."</pre>"; 
    // echo "<pre>".var_export($respon_array,true)."</pre>";
    //      die;

    return $respon_array;
  }

  private function getTokenSATUSEHAT()
  {
    $tmp_secretKey = $this->db->query("SELECT nilai from seting_satusehat where key_setting='client_secret'")->row()->nilai;
    $tmp_client_id = $this->db->query("SELECT nilai from seting_satusehat where key_setting='client_id'")->row()->nilai;
    $url = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlTokenSATUSEHAT'")->row()->nilai;

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,

      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false,

      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'client_id=' . $tmp_client_id . '&client_secret=' . $tmp_secretKey . '',

      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));
    $response = curl_exec($curl);
    // echo "<pre>".var_export($curl,true)."</pre>"; 
    // echo "<pre>".var_export($json,true)."</pre>"; 
    // echo "<pre>".var_export($header,true)."</pre>"; 

    return $response;
  }

  function createOrganization()
  {

    $getToken = json_decode($this->getToken());
    $url = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlCreateOrganization'")->row()->nilai;
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();

    // echo "<pre>".var_export($db_rs,true)."</pre>"; die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '{   
            "active": ' . $_POST['active'] . ',
            "organization_parent" : "' . $_POST['organization_parent'] . '",
            "kd_organization" : "' . $_POST['kd_organization'] . '",
            "code_organization" : "' . $_POST['code_organization'] . '" ,
            "display_organization" : "' . $_POST['display_organization'] . '" ,
            "nama_organization" : "' . $_POST['nama_organization'] . '" ,
            "phone_rs" : "' . $db_rs[0]->phone1 . '" ,
            "email_rs" : "' . $db_rs[0]->email . '" ,
            "website_rs" : "' . $db_rs[0]->website . '" ,
            "address_rs" : "' . $db_rs[0]->address . '" ,
            "city_rs" : "' . $db_rs[0]->city . '" ,
            "postalCode_rs" : "' . $db_rs[0]->code . '" ,
            "kd_propinsi_rs" : "' . $db_rs[0]->kd_propinsi . '" ,
            "kd_kab_rs" : "' . $db_rs[0]->kd_kab . '" ,
            "kd_kec_rs" : "' . $db_rs[0]->kd_kec . '" ,
            "kd_kel_rs" : "' . $db_rs[0]->kd_kel . '" 
        }';

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    $dat = array(
      'organization_id' => $res->id,
      'organization_parent'    => $_POST['organization_parent'],
      'nama_organization'    => $res->name,
      'code_organization'    => $res->type[0]->coding[0]->code,
      'active' => $res->active,
      'parent' => $_POST['parent'],
      'kd_organization'   => $res->identifier[0]->value,
    );
    // $cek = $this->db->query("SELECT * FROM organization_satusehat where kd_orga")
    $query = $this->db->insert('organization_satusehat', $dat);

    if ($this->db->affected_rows() == 1) {
      echo 'save';
    } else {
      echo 'Gagal Disimpan';
    }
    // print_r($res); die;

    // echo $response;
  }

  function createLocation()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlCreateLocation'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $ruang = $this->db->query("SELECT * From ruang_satusehat r INNER JOIN gedung_satusehat g ON g.kd_gedung = r.kd_gedung where r.kd_ruang='" . $_POST['kd_ruang'] . "'")->row();
    $unit = $this->db->query("SELECT * FROM unit u where kd_unit='" . $_POST['kdUnit'] . "'")->row();
    $display_location = $this->db->query("SELECT * FROM locationtype_satusehat where code_location = '" . $_POST['code_location'] . "'")->row();

    // G4-1-R2-10013

    $kode_value = $ruang->kd_gedung . '-' . $ruang->lantai . '-' . $ruang->kd_ruang . '-' . $_POST['kdUnit'];
    $description = $ruang->ruang . ', ' . $unit->nama_unit . ', Lantai ' . $ruang->lantai . ', ' . $ruang->gedung. ', ' .$db_rs->name;

    // echo "<pre>".var_export($_POST,true)."</pre>"; 

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '{   
            "active" : true,
            "organization_id" : "' . $_POST['orgnaization_id'] . '",
            "kode_value" : "' . $kode_value . '",
            "kd_ruang" : "' . $ruang->kd_ruang . '",
            "description" : "' . $description . '",
            "phone_rs" : "' . $db_rs[0]->phone1 . '",
            "fax_rs" : "' . $db_rs[0]->fax . '",
            "email_rs" : "' . $db_rs[0]->email . '",
            "website_rs" : "' . $db_rs[0]->website . '",
            "address_rs" : "' . $db_rs[0]->address . '",
            "city_rs" : "' . $db_rs[0]->city . '",
            "postalCode_rs" : "' . $db_rs[0]->code . '",
            "kd_propinsi_rs" : "' . $db_rs[0]->kd_propinsi . '",
            "kd_kab_rs" : "' . $db_rs[0]->kd_kab . '",
            "kd_kec_rs" : "' . $db_rs[0]->kd_kec . '",
            "kd_kel_rs" : "' . $db_rs[0]->kd_kel . '",
            "rt_rs" : "' . $db_rs[0]->rt . '",
            "rw_rs" : "' . $db_rs[0]->rw . '",
            "code_location" : "' . $_POST['code_location'] . '",
            "display_location" : "' . $display_location->display . '",
            "longitude" : "' . $_POST['longitude'] . '",
            "latitude" : "' . $_POST['latitude'] . '",
            "altitude" : "' . $_POST['altitude'] . '",
            "organization_id_rs" : "' . $db_rs[0]->organization_id . '"
        }';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $dat = array(
        'kd_ruang' => $_POST['kd_ruang'],
        'kd_identifier'    => $res->identifier[0]->value,
        'location_id'    => $res->id,
        'longitude'    => $res->position->longitude,
        'latitude' => $res->position->latitude,
        'altitude' => $res->position->altitude,
        'organization_id'   => $_POST['orgnaization_id'],
        'location_type'   => $res->physicalType->coding[0]->code,
        'kd_unit'   => $_POST['kdUnit'],
        'description' => $description
      );

      $get = $this->db->where("kd_unit", $_POST['kdUnit']);
      $get = $this->db->get("unit_satusehat");

      if ($get->num_rows() == 0) {
        // $cek = $this->db->query("SELECT * FROM organization_satusehat where kd_orga")
        $keterangan = "insert";
        $query = $this->db->insert('unit_satusehat', $dat);
      } else {
        $keterangan = "update";
        $update = $this->db->where("kd_unit", $_POST['kdUnit']);
        $update = $this->db->update("unit_satusehat", $dat);
      }
    }

    // die;


    if ($this->db->affected_rows() == 1) {
      echo $keterangan;
    } else {
      echo 'Data Sudah ada';
    }
    // print_r($res); die;

    // echo $response;
  }

  function GetNIKPractioner()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetNIKPractioner'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $_POST['nik']);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $dat = array(
        'kd_dokter' => $_POST['code'],
        'nik'    => $res->entry[0]->resource->identifier[1]->value,
        'id_practioner'    => $res->entry[0]->resource->id,
      );

      $get = $this->db->where("kd_dokter", $_POST['code']);
      $get = $this->db->get("dokter_satusehat");

      if ($get->num_rows() == 0) {
        // $cek = $this->db->query("SELECT * FROM organization_satusehat where kd_orga")
        $keterangan = "save";
        $query = $this->db->insert('dokter_satusehat', $dat);
      } else {
        $keterangan = "update";
        $update = $this->db->where("kd_dokter", $_POST['code']);
        $update = $this->db->update("dokter_satusehat", $dat);
      }
    }

    if ($this->db->affected_rows() == 1) {
      echo $keterangan;
    } else {
      echo 'Data Sudah ada';
    }
    // print_r($res); die;

    // echo $response;
  }
  public function getOrganizationType()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetOrganizationType'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

  public function getLocationType()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetLocationType'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

  public function getImunisasiKFA()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetImmunizationKFA'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

  public function getImunisasiProviderRole()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetImmunizationProviderRole'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

  public function getImunisasiRoute()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetImmunizationRoute'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

  function createImmunization()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlCreateImmunization'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    $data_vaksin = json_decode($_POST['listdata']);
    // echo '<pre>' . var_export($data_vaksin, true) . '</pre>';
    // die;


    $location = $this->db->query("SELECT location_id,description from unit_satusehat where kd_unit = '" . $_POST['KdUnit'] . "'")->result();
    $id_practioner = $this->db->query("SELECT id_practioner from dokter_satusehat where kd_dokter = '" . $_POST['KdDokter'] . "'")->result();
    $tgl = explode(' ', $_POST['TglTransaksi']);
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '{
      "status" : "completed",
      "code_vaccine" : "' . $data_vaksin->code_vaccine . '",
      "display_vaccine" : "' . $data_vaksin->display_vaccine . '",
      "ihs_number" : "' . $_POST['IHS_NUMBER'] . '",
      "name_patient" : "' . $_POST['nama_pasien'] . '",
      "id_ecounter" : "' . $_POST['ecounter'] . '",
      "occuranceDateTime" : "' . $tgl[0] . '",
      "recorded" : "' . $tgl[0] . '",
      "primarySource" : "true",
      "id_location" : "' . $location[0]->location_id . '",
      "display_location" : "' . $location[0]->description . '",
      "lotNumber" : "202009007",
      "code_atc" : "' . $data_vaksin->code_atc . '",
      "display_atc" : "' . $data_vaksin->display_atc . '",
      "value_doseQuantity" : "' . $data_vaksin->jumlah . '",
      "unit_doseQuantity" : "' . $data_vaksin->takaran . '",
      "code_doseQuantity" : "' . $data_vaksin->takaran . '",
      "code_performer" : "' . $data_vaksin->code_perfomerFunction . '",
      "display_performer" : "' . $data_vaksin->display_perfomerFunction . '",
      "id_practioner" : "' . $id_practioner[0]->id_practioner . '",
      "code_reasonCode" : "429060002",
      "display_reasonCode" : "Procedure to meet occupational requirement",
      "code_programEligibility" : "1",
      "display_programEligibility" : "Diverifikasi"
    }';
    // echo "<pre>".var_export($postdata,true)."</pre>"; 

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($res, true) . "</pre>";
    // die;

    if (isset($res->issue[0]->details)) {
      echo "<pre>" . var_export($res->issue[0]->details, true) . "</pre>";
      die;
    } else {
      if ($res->primarySource == 1 || $res->primarySource == '1') {
        $res->primarySource = true;
      } else {
        $res->primarySource = false;
      }

      $dat = array(
        'code_doseQuantity' => $res->doseQuantity->code,
        'unit_doseQuantity' => $res->doseQuantity->unit,
        'value_doseQuantity' => $res->doseQuantity->value,
        'ecounter' => $res->encounter->reference,
        'id_imunisasi' => $res->id,
        'display_location' => $res->location->display,
        'reference_location' => $res->location->reference,
        'lotNumber' => $res->lotNumber,
        'meta_lastUpdated' => $res->meta->lastUpdated,
        'meta_versionId' => $res->meta->versionId,
        'occurrenceDateTime' => $res->occurrenceDateTime,
        'display_patient' => $res->patient->display,
        'reference_patient' => $res->patient->reference,
        'reference_performer' => $res->performer[0]->actor->reference,
        'code_functionPerfomer' => $res->performer[0]->function->coding[0]->code,
        'display_functionPerfomer' => $res->performer[0]->function->coding[0]->display,
        'primarySource' => $res->primarySource,
        'code_programEligibility' => $res->programEligibility[0]->coding[0]->code,
        'display_programEligibility' => $res->programEligibility[0]->coding[0]->display,
        'code_reasonCode' => $res->reasonCode[0]->coding[0]->code,
        'display_reasonCode' => $res->reasonCode[0]->coding[0]->display,
        'recorded' => $res->recorded,
        'resourceType' => $res->resourceType,
        'code_route' => $res->route->coding[0]->code,
        'display_route' => $res->route->coding[0]->display,
        'status' => $res->status,
        'code_vaccine' => $res->vaccineCode->coding[0]->code,
        'display_vaccine' => $res->vaccineCode->coding[0]->display,
        'kd_pasien' => $_POST['kd_pasien'],
        'kd_unit' => $_POST['KdUnit'],
        'tgl_masuk' => $_POST['TglTransaksi'],
        'urut_masuk' => $_POST['UrutMasuk'],
        'kd_dokter' => $_POST['KdDokter'],

      );

      $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      $get = $this->db->where("kd_unit", $_POST['KdUnit']);
      $get = $this->db->where("tgl_masuk", $_POST['TglTransaksi']);
      $get = $this->db->where("urut_masuk", $_POST['UrutMasuk']);
      $get = $this->db->get("immunization_satusehat");

      if ($get->num_rows() == 0) {
        // $cek = $this->db->query("SELECT * FROM organization_satusehat where kd_orga")
        $keterangan = "insert";
        $query = $this->db->insert('immunization_satusehat', $dat);
      } else {
        $keterangan = "update";
        // $update = $this->db->where("kd_unit", $_POST['kdUnit']);
        $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
        $update = $this->db->where("kd_unit", $_POST['KdUnit']);
        $update = $this->db->where("tgl_masuk", $_POST['TglTransaksi']);
        $update = $this->db->where("urut_masuk", $_POST['UrutMasuk']);
        $update = $this->db->update("immunization_satusehat", $dat);
      }
    }

    // die;


    if ($this->db->affected_rows() == 1) {
      // echo $keterangan;
      echo '{success:true,   '. $keterangan.', id_imunisasi:' . $res->id . '}';
    } else {
      echo 'Data Sudah ada';
    }
    // print_r($res); die;

    // echo $response;
  }

  public function getDataImunisasi()
  {
    // echo "aaaaa<pre>".var_export($_POST,true)."</pre>";die;

    $query = $this->db->query("SELECT * FROM immunization_satusehat where kd_pasien='" . $_POST['KD_PASIEN'] . "' and kd_unit='" . $_POST['KD_UNIT'] . "' and tgl_masuk ='" . $_POST['TANGGAL_TRANSAKSI'] . "' and urut_masuk='" . $_POST['URUT_MASUK'] . "'");
    if ($query->num_rows() > 0) {
      echo "{data:" . json_encode($query->result()) . "}";
    } else {
      echo "data tidak ada";
    }
  }
  public function getDiagnosisRole()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetDiagnosisRole'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

  function cariNIK()
  {
    $getToken = json_decode($this->getToken());
<<<<<<< HEAD
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetNIKPatient'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
=======
    $urlnya = $this->db->query("SELECT nilai from SETTING_SATUSEHAT where key_setting='urlGetNIKPatient'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from SETTING_SATUSEHAT where key_setting='x_username'")->row()->nilai;
>>>>>>> 8ffe52a02ac518e1fdd672b59f2548c563f6cd89

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $_POST['nik']);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
<<<<<<< HEAD
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($response, true) . "</pre>";
    // die;

=======
    
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($response, true) . "</pre>";
    // die;
    
>>>>>>> 8ffe52a02ac518e1fdd672b59f2548c563f6cd89
    if ($res->total <= 0) {
      echo '{success: false,pesan:"IHS_Number tidak dapat ditemukan"}';
      die;
    } else {
      $dat = array(
        'ihs_number' => $res->entry[0]->resource->id,
      );
      $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      $get = $this->db->get("pasien");

      if ($get->num_rows() == 0) {
        echo '{success: false,pesan:"kd_pasien tidak dapat ditemukan"}';
        die;
      } else {
        $keterangan = "update";
        $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
        $update = $this->db->update("pasien", $dat);
        if ($this->db->affected_rows() > 0) {
          $res->success = true;
          echo json_encode($res);
          // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
        } else {
          echo '{success: false,pesan:"update gagal"}';
          die;
        }
      }
    }
  }

  function cekIHSNumber()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetIHSNumber'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $_POST['ihs_number']);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      // $dat = array(
      //   'ihs_number' => $res->entry[0]->resource->id,
      //   );

      // $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      // $get = $this->db->get("pasien");

      // if ($get->num_rows() == 0) {
      echo json_encode($res);
      // }else{
      //       $keterangan = "update";
      //       $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      //       $update = $this->db->update("pasien", $dat);
      //       if($this->db->affected_rows() > 0){
      //           echo json_encode($res);
      //           // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
      //       }
      //   }
    }
  }

  function createEcounterID()
  {

    $getToken = json_decode($this->getToken());
    $url = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlCreateEcounter'")->row()->nilai;
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();

    $data = $this->db->query("SELECT 
      p.nama,
      K.kd_dokter,
      K.kd_unit,
      K.jam_masuk,
      ds.id_practioner,
      d.nama AS nama_dokter,
      l.location_id,
      l.description 
      FROM
          pasien
          P INNER JOIN kunjungan K ON K.kd_pasien = P.kd_pasien
          INNER JOIN dokter d ON d.kd_dokter = K.kd_dokter
          INNER JOIN dokter_satusehat ds ON ds.kd_dokter = K.kd_dokter
          INNER JOIN unit_satusehat l ON l.kd_unit = K.kd_unit 
      WHERE
          P.kd_pasien = '" . $_POST['kd_pasien'] . "' AND k.tgl_masuk ='" . $_POST['tgl_masuk'] . "' and k.kd_unit='" . $_POST['kd_unit'] . "'
      ")->result();

    // echo "<pre>".var_export($data,true)."</pre>"; die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '';
    if (count($data) > 0) {
      // $start_period = date_format(date_timestamp_set(new DateTime(), $now), 'c');
      $postdata = '{
              "status" : "arrived",
              "code_atc" : "IMP",
              "display_atc" : "inpatient encounter",
              "ihs_number" : "' . $_POST['ihs_number'] . '",
              "name_patient" : "' . $data[0]->nama . '",
              "code_participant" : "ATND",
              "display_participant" : "attender",
              "id_practitioner" : "' . $data[0]->id_practioner . '",
              "name_practitioner" : "' . $data[0]->nama_dokter . '",
              "start_period" : "' . $this->start_time . '",
              "id_location" : "' . $data[0]->location_id . '",
              "description_location" : "' . $data[0]->description . '",
              "kd_pasien" : "' . $_POST['kd_pasien'] . '",
              "organization_id_rs" : "' . $db_rs[0]->organization_id . '"
          }';
    } else {
      echo "{succsess : false}";
      die;
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $dat = array(
        'ecounter_id' => $res->id,
      );

      $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      $get = $this->db->where("tgl_masuk", $_POST['tgl_masuk']);
      $get = $this->db->where("kd_unit", $_POST['kd_unit']);
      $get = $this->db->get("kunjungan");

      if ($get->num_rows() == 0) {
        echo '{success: false}';
      } else {
        $keterangan = "update";
        $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
        $update = $this->db->where("tgl_masuk", $_POST['tgl_masuk']);
        $update = $this->db->where("kd_unit", $_POST['kd_unit']);
        $update = $this->db->update("kunjungan", $dat);
        if ($this->db->affected_rows() > 0) {
          // echo json_encode($res);
          echo '{success:true,  response:' . json_encode($res) . '}';
          // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
        }
      }
    }
  }

  function createConditionDiagnosis()
  {

    $getToken = json_decode($this->getToken());
    $url = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlCreateDiagnosis'")->row()->nilai;
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();

    $data = $this->db->query("SELECT * FROM penyakit where kd_penyakit ='" . $_POST['code_icd10'] . "'")->result();

    // echo "<pre>".var_export($data,true)."</pre>"; die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    if (count($data) > 0) {
      // $start_period = date_format(date_timestamp_set(new DateTime(), $now), 'c');
      $postdata = '{
        "code_clinicalStatus" : "active",
        "display_clinicalStatus" : "Active",
        "code_category" : "encounter-diagnosis ",
        "display_category" : "Encounter Diagnosis",
        "code_icd10" : "' . $_POST['code_icd10'] . '",
        "display_icd10" : "' . $data[0]->penyakit . '",
        "ihs_number" : "' . $_POST['ihsnumber'] . '",
        "name_patient" : "' . $_POST['name_patient'] . '",
        "encounter_id" : "' . $_POST['ecounter'] . '",
        "display_encounter" : "Kunjungan ' . $_POST['name_patient'] . '  tanggal ' . $_POST['tgl_masuk'] . '"
    }';
    } else {
      echo "{succsess : false}";
      die;
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $dat = array(
        'id_condition' => $res->id,
      );

      $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      $get = $this->db->where("tgl_masuk", $_POST['tgl_masuk']);
      $get = $this->db->where("kd_unit", $_POST['kd_unit']);
      $get = $this->db->get("kunjungan");

      if ($get->num_rows() == 0) {
        echo '{success: false}';
      } else {
        $keterangan = "update";
        $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
        $update = $this->db->where("tgl_masuk", $_POST['tgl_masuk']);
        $update = $this->db->where("kd_unit", $_POST['kd_unit']);
        $update = $this->db->update("kunjungan", $dat);
        if ($this->db->affected_rows() > 0) {
          // echo json_encode($res);
          echo '{success:true,  response:' . json_encode($res) . '}';
          // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
        }
      }
    }
  }

  function UpdateEcounterFinish_API()
  {

    $end = strtotime(date("H:i", strtotime('+20 minutes', strtotime($this->start_time))));
    $time_end_arrive = date_format(date_timestamp_set(new DateTime(), $end), 'c');

    $start_proges = strtotime(date("H:i", strtotime('+10 minutes', strtotime($this->start_time))));
    $time_start_proges = date_format(date_timestamp_set(new DateTime(), $end), 'c');

    $end_proges = strtotime(date("H:i", strtotime('+20 minutes', strtotime($this->start_time))));
    $time_end_proges = date_format(date_timestamp_set(new DateTime(), $end_proges), 'c');

    $start_finish = strtotime(date("H:i", strtotime('+10 minutes', strtotime($this->start_time))));
    $time_start_finish = date_format(date_timestamp_set(new DateTime(), $start_finish), 'c');

    $end_finish = strtotime(date("H:i", strtotime('+30 minutes', strtotime($this->start_time))));
    $time_end_finish = date_format(date_timestamp_set(new DateTime(), $end_finish), 'c');

    $getToken = json_decode($this->getToken());
    $url = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlUpdateFInishEcounter'")->row()->nilai;
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();

    $data = $this->db->query("SELECT
      k.kd_pasien, k.tgl_masuk, k.jam_masuk, k.ecounter_id, k.id_condition,
      p.nama as nama_pasien, p.ihs_number,
      k.kd_dokter,d.nama as nama_dokter, ds.id_practioner,
      k.kd_unit, u.nama_unit, us.location_id, us.description
      FROM kunjungan k 
      INNER JOIN pasien p ON p.kd_pasien = k.kd_pasien
      INNER JOIN dokter d ON d.kd_dokter = k.kd_dokter
      INNER JOIN dokter_satusehat ds ON ds.kd_dokter = k.kd_dokter
      INNER JOIN unit u ON u.kd_unit = k.kd_unit
      INNER JOIN unit_satusehat us ON us.kd_unit = k.kd_unit
      WHERE
          P.kd_pasien = '" . $_POST['kd_pasien'] . "' AND k.tgl_masuk ='" . $_POST['tgl_masuk'] . "' and k.kd_unit='" . $_POST['kd_unit'] . "'
      ")->result();
    $data_diagnosa = $this->db->query("SELECT A.*,B.penyakit,C.morfologi,D.sebab FROM mr_penyakit A
      LEFT JOIN penyakit B ON A.kd_penyakit=B.kd_penyakit 
      LEFT JOIN neoplasma C ON 
      C.kd_penyakit=A.kd_penyakit AND C.kd_pasien=A.kd_pasien AND C.kd_unit=A.kd_unit AND C.tgl_masuk=A.tgl_masuk AND C.tgl_masuk=A.tgl_masuk
      LEFT JOIN kecelakaan D ON
      D.kd_penyakit=A.kd_penyakit AND D.kd_pasien=A.kd_pasien AND D.kd_unit=A.kd_unit AND D.tgl_masuk=A.tgl_masuk AND D.tgl_masuk=A.tgl_masuk
      WHERE A.kd_pasien = '" . $_POST['kd_pasien'] . "' and A.kd_unit='" . $_POST['kd_unit'] . "' and A.tgl_masuk in('" . $_POST['tgl_masuk'] . "')")->result();
    // echo "<pre>".var_export($data,true)."</pre>"; die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    if (count($data) > 0) {
      // $start_period = date_format(date_timestamp_set(new DateTime(), $now), 'c');
      $postdata = '{
          "encounter_id" : "' . $data[0]->ecounter_id . '"
          "kd_pasien" : "' . $data[0]->kd_pasien . '",
          "code_class" : "AMB",
          "display_class" : "ambulatory",
          "ihs_number" : "' . $data[0]->ihs_number . '",
          "name_patient" : "' . $data[0]->nama_pasien . '",
          "code_participant" : "ATND",
          "display_participant" : "attender",
          "id_practitioner" : "' . $data[0]->id_practioner . '",
          "name_practitioner" : "' . $data[0]->nama_dokter . '",
          "start_period" : "2022-06-14T07:00:00+07:00",
          "end_period" : "2022-06-14T09:00:00+07:00",
          "id_location" : "' . $data[0]->location_id . '",
          "description_location" : "' . $data[0]->description . '",
          "id_condition" : "31231232",
          "display_condition" : "123213213",
          "code_diagnosis" : "12312312312",
          "display_diagnosis" : "1241412412",
          "time_start_arrive" : "2022-06-14T07:00:00+07:00",
          "time_end_arrive" : "2022-06-14T08:00:00+07:00",
          "time_end_proges" : "2022-06-14T09:00:00+07:00",
          "time_end_finish": "2022-06-14T09:00:00+07:00",
          "organization_id_rs" : "' . $db_rs[0]->organization_id . '"
      }';
    } else {
      echo "{succsess : false}";
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      // $dat = array(
      //     'id_condition' => $res->id,
      //     );

      // $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      // $get = $this->db->where("tgl_masuk", $_POST['tgl_masuk']);
      // $get = $this->db->where("kd_unit", $_POST['kd_unit']);
      // $get = $this->db->get("kunjungan");

      // if ($get->num_rows() == 0) {
      //     echo '{success: false}';
      // }else{
      //         $keterangan = "update";
      //         $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
      //         $update = $this->db->where("tgl_masuk", $_POST['tgl_masuk']);
      //         $update = $this->db->where("kd_unit", $_POST['kd_unit']);
      //         $update = $this->db->update("kunjungan", $dat);
      //         if($this->db->affected_rows() > 0){
      // echo json_encode($res);
      echo '{success:true,  response:' . json_encode($res) . '}';
      // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
      // }
      // }
    }
  }

  function UpdateEcounterFinish()
  {

    $getToken = json_decode($this->getTokenSATUSEHAT());
    // echo "<pre>".var_export($getToken->access_token,true)."</pre>"; die;

    $end = strtotime(date("H:i", strtotime('+20 minutes', strtotime($this->start_time))));
    $time_end_arrive = date_format(date_timestamp_set(new DateTime(), $end), 'c');

    $start_proges = strtotime(date("H:i", strtotime('+10 minutes', strtotime($this->start_time))));
    $time_start_proges = date_format(date_timestamp_set(new DateTime(), $end), 'c');

    $end_proges = strtotime(date("H:i", strtotime('+20 minutes', strtotime($this->start_time))));
    $time_end_proges = date_format(date_timestamp_set(new DateTime(), $end_proges), 'c');

    $start_finish = strtotime(date("H:i", strtotime('+10 minutes', strtotime($this->start_time))));
    $time_start_finish = date_format(date_timestamp_set(new DateTime(), $start_finish), 'c');

    $end_finish = strtotime(date("H:i", strtotime('+30 minutes', strtotime($this->start_time))));
    $time_end_finish = date_format(date_timestamp_set(new DateTime(), $end_finish), 'c');



    $curl = curl_init();
    $urlnya = 'https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1/Encounter/' . $this->input->post('ecounter') . '';
    $header = array(
      'Content-Type: application/json',
      'Authorization: Bearer ' . $getToken->access_token . ''
    );
    $method = "PUT"; // POST / PUT / DELETE
    $postdata = '{
          "resourceType": "Encounter",
          "id": "' . $this->input->post('ecounter') . '",
          "identifier": [
            {
              "system": "http://sys-ids.kemkes.go.id/encounter/' . $this->input->post('ecounter') . '",
              "value": "0-29-94-16"
            }
          ],
          "status": "finished",
          "class": {
            "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
            "code": "AMB",
            "display": "ambulatory"
          },
          "subject": {
            "reference": "Patient/' . $this->input->post('ihsnumber') . '",
            "display": "PATIENT 6"
          },
          "participant": [
            {
              "type": [
                {
                  "coding": [
                    {
                      "system": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                      "code": "ATND",
                      "display": "attender"
                    }
                  ]
                }
              ],
              "individual": {
                "reference": "Practitioner/10009880728",
                "display": "Practitioner 1"
              }
            }
          ],
          "period": {
            "start": "' . $this->start_time . '",
            "end": "' . $time_end_finish . '"
          },
          "location": [
            {
              "location": {
                "reference": "Location/d45d3d75-307b-4867-8276-9737632f927c",
                "display": "Ruang Poli Mata, Poliklinik Mata, Lantai 1, Gedung Pelayanan"
              }
            }
          ],
          "diagnosis": [
            {
              "condition": {
                "reference": "Condition/' . $this->input->post('condition') . '",
                "display": "Acute appendicitis, other and unspecified"
              },
              "use": {
                "coding": [
                  {
                    "system": "http://terminology.hl7.org/CodeSystem/diagnosis-role",
                    "code": "DD",
                    "display": "Discharge diagnosis"
                  }
                ]
              },
              "rank": 1
            },
            {
              "condition": {
                "reference": "Condition/' . $this->input->post('condition') . '",
                "display": "Non-insulin-dependent diabetes mellitus without complications"
              },
              "use": {
                "coding": [
                  {
                    "system": "http://terminology.hl7.org/CodeSystem/diagnosis-role",
                    "code": "DD",
                    "display": "Discharge diagnosis"
                  }
                ]
              },
              "rank": 2
            }
          ],
          "statusHistory": [
            {
              "status": "arrived",
              "period": {
                "start": "' . $this->start_time . '",
                "end": "' . $time_end_arrive . '"
              }
            },
            {
              "status": "in-progress",
              "period": {
                "start": "' . $time_end_arrive . '",
                "end": "' . $time_end_proges . '"
              }
            },
            {
              "status": "finished",
              "period": {
                "start": "' . $time_end_finish . '",
                "end": "' . $time_end_finish . '"
              }
            }
          ],
          "serviceProvider": {
            "reference":"Organization/99f74413-6c80-4121-867e-19a72c59568a"
          }
        }
        ';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'Authorization: Bearer ' . $getToken->access_token . ''
    ));

    $response = curl_exec($curl);
    // print_r('Curl error: ' . curl_error($curl));
    curl_close($curl);
    $res = json_decode($response);
    $dat = array(
      'id' => (int)$this->db->query("Select max(id) as id from response_encounter_satusehat ")->row()->id + 1,
      'url'    => $urlnya,
      'params_header'    => json_encode($header),
      'params_body'    => $postdata,
      'method' => $method,
      'response'   => $response,
    );
    $query = $this->db->insert('response_encounter_satusehat', $dat);
    // echo "<pre>".var_export($query->affected_rows(), true)."</pre>";

    echo '{success:true,  response:' . json_encode($res) . '}';
  }

  function GetEcounterSubject()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetEcounterSubject'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $this->input->post('ihs_number'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($res, true) . "</pre>";
    // die;

    $result = array();

    foreach ($res->entry as $entry) {
      $resource = $entry->resource;
      if (!isset($resource->issue[0]->diagnostics)) {
        $result[] = array(
          'class_code' => $resource->class->code,
          'class_display' => $resource->class->display,
          'id' => $resource->id,
          'identifier_value' => $resource->identifier[0]->value,
          'location_display' => $resource->location[0]->location->display,
          'location_reference' => $resource->location[0]->location->reference,
          'last_updated' => $resource->meta->lastUpdated,
          'version_id' => $resource->meta->versionId,
          'participant_individual_display' => $resource->participant[0]->individual->display,
          'participant_individual_reference' => $resource->participant[0]->individual->reference,
          'participant_type_coding_code' => $resource->participant[0]->type[0]->coding[0]->code,
          'participant_type_coding_display' => $resource->participant[0]->type[0]->coding[0]->display,
          'period_start' => $resource->period->start,
          'resource_type' => $resource->resourceType,
          'service_provider_reference' => $resource->serviceProvider->reference,
          'status' => $resource->status,
          'status_history_period_start' => $resource->statusHistory[0]->period->start,
          'status_history_status' => $resource->statusHistory[0]->status,
          'subject_display' => $resource->subject->display,
          'subject_reference' => $resource->subject->reference
        );
      }
    }
    echo json_encode($result);

    // if (isset($res->entry[0]->resource->issue[0]->diagnostics)) {
    //   echo $res->entry[0]->resource->issue[0]->diagnostics;
    //   die;
    // } else {
    //   echo json_encode($res);
    //   // $dat = array(
    //   //   'ihs_number' => $res->entry[0]->resource->id,
    //   //   );

    //   // $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
    //   // $get = $this->db->get("pasien");

    //   // if ($get->num_rows() == 0) {
    //   //   echo '{success: false}';
    //   // }else{
    //   //       $keterangan = "update";
    //   //       $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
    //   //       $update = $this->db->update("pasien", $dat);
    //   //       if($this->db->affected_rows() > 0){
    //   //           // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
    //   //       }
    //   //   }
    // }
  }

  function GetConditionSubject()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetConditionEcounter'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $this->input->post('ecounter'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res,true)."</pre>";die;

    $result = array();

    foreach ($res->entry as $entry) {
      $resource = $entry->resource;
    // echo "aaaaa<pre>".var_export($resource,true)."</pre>";die;
      if (!isset($resource->issue[0]->diagnostics)) {
        $result[] = array(
          'category_code' => $resource->category[0]->coding[0]->code,
          'category_display' => $resource->category[0]->coding[0]->display,
          'category_system' => $resource->category[0]->coding[0]->system,
          'clinicalStatus_code' => $resource->clinicalStatus->coding[0]->code,
          'clinicalStatus_display' => $resource->clinicalStatus->coding[0]->display,
          'clinicalStatus_system' => $resource->clinicalStatus->coding[0]->system,
          'code_code' => $resource->code->coding[0]->code,
          'code_display' => $resource->code->coding[0]->display,
          'code_system' => $resource->code->coding[0]->system,
          'encounter_display' => $resource->encounter->display,
          'encounter_reference' => $resource->encounter->reference,
          'meta_lastUpdated' => $resource->meta->lastUpdated,
          'meta_versionId' => $resource->meta->versionId,
          'subject_display' => $resource->subject->display,
          'subject_reference' => $resource->subject->reference,
          'fullUrl' => $entry->fullUrl,
          'id' => $resource->id,
          'resourceType' => $resource->resourceType,
          // 'search' => $resource->search->mode
        );
      }
    }
    echo json_encode($result);

    // if (isset($res->issue[0]->diagnostics)) {
    //   echo $res->issue[0]->diagnostics;
    //   die;
    // } else {
    //   echo json_encode($res);
    //   // $dat = array(
    //   //   'ihs_number' => $res->entry[0]->resource->id,
    //   //   );

    //   // $get = $this->db->where("kd_pasien", $_POST['kd_pasien']);
    //   // $get = $this->db->get("pasien");

    //   // if ($get->num_rows() == 0) {
    //   //   echo '{success: false}';
    //   // }else{
    //   //       $keterangan = "update";
    //   //       $update = $this->db->where("kd_pasien", $_POST['kd_pasien']);
    //   //       $update = $this->db->update("pasien", $dat);
    //   //       if($this->db->affected_rows() > 0){
    //   //           // echo '{success:true,  response:'.$res->entry[0]->resource->id.' "}';
    //   //       }
    //   //   }
    // }
  }


  function save_historyCondition($data)
  {
    // $data = $this->input->post('data');
    $cek = $this->db->query("SELECT * FROM history_condition where id='" . $data->entry[0]->resource->id . "' and subject_reference='" . $data->entry[0]->resource->subject->reference . "'")->num_rows();

    if ($cek == 0) {
      $dat = array(
        "fullUrl" => $data->entry[0]->fullUrl,
        "category_code" => $data->entry[0]->resource->category[0]->coding[0]->code,
        "category_display" => $data->entry[0]->resource->category[0]->coding[0]->display,
        "category_system" => $data->entry[0]->resource->category[0]->coding[0]->system,
        "clinicalStatus_code" => $data->entry[0]->resource->clinicalStatus->coding[0]->code,
        "clinicalStatus_display" => $data->entry[0]->resource->clinicalStatus->coding[0]->display,
        "clinicalStatus_system" => $data->entry[0]->resource->clinicalStatus->coding[0]->system,
        "code_code" => $data->entry[0]->resource->code->coding[0]->code,
        "code_display" => $data->entry[0]->resource->code->coding[0]->display,
        "code_system" => $data->entry[0]->resource->code->coding[0]->system,
        "encounter_display" => $data->entry[0]->resource->encounter->display,
        "encounter_reference" => $data->entry[0]->resource->encounter->reference,
        "id" => $data->entry[0]->resource->id,
        "meta_lastUpdated" => $data->entry[0]->resource->meta->lastUpdated,
        "meta_versionId" => $data->entry[0]->resource->meta->versionId,
        "resourceType" => $data->entry[0]->resource->resourceType,
        "subject_display" => $data->entry[0]->resource->subject->display,
        "subject_reference" => $data->entry[0]->resource->subject->reference,
        "search" => $data->entry[0]->search->mode
      );
      $query = $this->db->insert('history_condition', $dat);
    }

    // echo "<pre>".var_export($dat, true)."</pre>"; die; 

  }

  public function getObservationCategory()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetObservationCategory'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

public function getObservationInterpretation()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetObservationInterpretation'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

public function getUcum()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetUcum'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{ data : " . $storePHP . " }";
  }

public function getLoinc()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetLoinc'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya. '' . $this->input->post('loinc'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($res)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{success:true, listData : " . $storePHP . " }";
  }
  
function createObservation()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlcreateObservation'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    
    $code_value = explode(';',$_POST['code_value']);

    // echo '<pre>' . var_export($code_value, true) . '</pre>';
    // die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '
      {
        "code_category" : "vital-signs",
        "display_category" : "Vital Signs",
        "code_loinc" : "' . $_POST['jenis_observation'] . '",
        "display_loinc" : "' . $_POST['displayJenisObservation'] . '",
        "ihs_number" : "' . $_POST['IHS_NUMBER'] . '",
        "encounter_id" : "' . $_POST['ecounter'] . '",
        "display_ecounter" : "' . $_POST['display_ecounter'] . '",
        "effectiveDateTime" : "' .date('Y-m-d'). '",
        "valueQuantity" : "' . $_POST['value_qty'] . '",
        "unit_value" : "' . $_POST['unit_value'] . '",
        "code_value" : "' . $code_value[0] . '"
      }';
    // echo "<pre>".var_export($postdata,true)."</pre>"; 

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($res, true) . "</pre>";
    // die;

    echo $response;
  }
function GetObservationByEcounter()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetObservationByEcounter'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $_POST['ecounter']);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($res, true) . "</pre>";
    // die;

    if ($res->total <= 0) {
      echo '{success: false,pesan:"IHS_Number tidak dapat ditemukan"}';
      die;
    } else {
      for ($i=0; $i < $res->total ; $i++) { 
        $dat[] = array(
          'code_category' => $res->entry[$i]->resource->category[0]->coding[0]->code,
          'display_category' => $res->entry[$i]->resource->category[0]->coding[0]->display,
          'code_loinc' => $res->entry[$i]->resource->code->coding[0]->code,
          'display_loinc' => $res->entry[$i]->resource->code->coding[0]->display,
          'effectiveDateTime' => $res->entry[$i]->resource->effectiveDateTime,
          'display_ecounter' => $res->entry[$i]->resource->encounter->display,
          'id_ecounter' => $res->entry[$i]->resource->encounter->reference,
          'id_observation' => $res->entry[$i]->resource->id,
          'meta_lastUpdated' => $res->entry[$i]->resource->meta->lastUpdated,
          'meta_versionId' => $res->entry[$i]->resource->meta->versionId,
          'resourceType' => $res->entry[$i]->resource->resourceType,
          'status' => $res->entry[$i]->resource->status,
          'subject' => $res->entry[$i]->resource->subject->reference,
          'unit' => $res->entry[$i]->resource->valueQuantity->unit,
          'value' => $res->entry[$i]->resource->valueQuantity->value,
          'code_value' => $res->entry[$i]->resource->valueQuantity->code
        );
      }
          $res->success = true;
          echo json_encode($dat);
      
    }
  }

function getPractionerProcedure()  {
  $data = $this->db->query("SELECT ds.kd_dokter, d.nama, ds.id_practioner FROM dokter_satusehat ds
                            INNER JOIN dokter d ON d.kd_dokter = ds.kd_dokter")->result();

    echo "{ data : " . json_encode($data) . " }";
}

public function getICD9()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetICD9'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.$_POST['icd9'];
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $this->input->post('icd9'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{success: true, listData : " . $storePHP . " }";
  }

public function getICD10()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetICD10'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.$_POST['icd9'];
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $this->input->post('icd10'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{success: true, listData : " . $storePHP . " }";
  }

public function getBodySite()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetSnomed'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.$_POST['icd9'];
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $this->input->post('bodysite'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));


    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>".var_export($getToken->response,true)."</pre>";die;

    if (isset($res->issue[0]->diagnostics)) {
      echo $res->issue[0]->diagnostics;
      die;
    } else {
      $storePHP = json_encode($res);
    }

    // $storePHP=json_encode($data);
    echo "{success: true, listData : " . $storePHP . " }";
  }

function createProcedure()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlcreateProcedure'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    
    // $code_value = explode(';',$_POST['code_value']);

    $end_finish = strtotime(date("H:i", strtotime('+30 minutes', strtotime($this->start_time))));
    $end_performedPeriod = date_format(date_timestamp_set(new DateTime(), $end_finish), 'c');

    // echo '<pre>' . var_export($code_value, true) . '</pre>';
    // die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '
     {
        "code_category" : "103693007",
        "display_category" : "Diagnostic procedure",
        "text_category" : "Diagnostic procedure",
        "code_icd9" : "' . $_POST['code_icd9'] . '",
        "display_icd9"  : "' . $_POST['icd9'] . '",
        "ihs_number" : "' . $_POST['IHS_NUMBER'] . '",
        "name_patient" : "' . $_POST['nama_pasien'] . '",
        "encounter_id" : "' . $_POST['ecounter'] . '",
        "display_ecounter" : "' . $_POST['display_ecounter'] . '",
        "start_performedPeriod" : "' . $this->start_time . '",
        "end_performedPeriod" : "'.$end_performedPeriod.'",
        "id_practioner" : "' . $_POST['practioner'] . '",
        "name_practioner" : "' . $_POST['name_practioner'] . '",
        "code_icd10" : "' . $_POST['code_icd10'] . '",
        "display_icd10" : "' . $_POST['icd10'] . '",
        "code_bodySite" : "' . $_POST['code_bodysite'] . '",
        "display_bodySite" : "' . $_POST['bodysite'] . '",
        "note" : "' . $_POST['note'] . '"
    }';
    // echo "<pre>".var_export($postdata,true)."</pre>"; 

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    
    echo $response;
  }
function GetProcedureByEcounter()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetProcedureByEcounter'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $_POST['ecounter']);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($res, true) . "</pre>";
    // die;

    if ($res->total <= 0) {
      echo '{success: false,pesan:"IHS_Number tidak dapat ditemukan"}';
      die;
    } else {
      for ($i=0; $i < $res->total ; $i++) { 
        $dat[] = array(
          'code_bodySite' => $res->entry[$i]->resource->bodySite[0]->coding[0]->code,
          'display_bodySite' => $res->entry[$i]->resource->bodySite[0]->coding[0]->display,
          'code_category' => $res->entry[$i]->resource->category->coding[0]->code,
          'display_category' => $res->entry[$i]->resource->category->coding[0]->display,
          'text_category' => $res->entry[$i]->resource->category->text,
          'code_icd9' => $res->entry[$i]->resource->code->coding[0]->code,
          'display_icd9' => $res->entry[$i]->resource->code->coding[0]->display,
          'display_ecounter' => $res->entry[$i]->resource->encounter->display,
          'id_ecounter' => $res->entry[$i]->resource->encounter->reference,
          'id_procedure' => $res->entry[$i]->resource->id,
          'note' => $res->entry[$i]->resource->note[0]->text,
          'performedPeriod_start' => $res->entry[$i]->resource->performedPeriod->start,
          'performedPeriod_end' => $res->entry[$i]->resource->performedPeriod->end,
          'practioner' => $res->entry[$i]->resource->performer[0]->actor->reference,
          'name_practioner' => $res->entry[$i]->resource->performer[0]->actor->display,
          'code_icd10' => $res->entry[$i]->resource->reasonCode[0]->coding[0]->code,
          'display_icd10' => $res->entry[$i]->resource->reasonCode[0]->coding[0]->display,
          'ihs_number' => $res->entry[$i]->resource->subject->reference,
          'nama_patient' => $res->entry[$i]->resource->subject->display
        );
      }
     
          $res->success = true;
          echo json_encode($dat);
      
    }
  }

function createComposition()
  {

    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlcreateComposition'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;
    
    // $code_value = explode(';',$_POST['code_value']);

    $end_finish = strtotime(date("H:i", strtotime('+30 minutes', strtotime($this->start_time))));
    $end_performedPeriod = date_format(date_timestamp_set(new DateTime(), $end_finish), 'c');

    // echo '<pre>' . var_export($code_value, true) . '</pre>';
    // die;
    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "POST"; // POST / PUT / DELETE
    $postdata = '
     {
        "organization_db_rs" : "' . $db_rs[0]->organization_id . '",
        "kd_pasien" : "' . $_POST['kd_pasien'] . '",
        "code_type" : "' . $_POST['code_type'] . '",
        "display_type" : "' . $_POST['type'] . 'y",
        "code_category"  : "LP173421-1",
        "display_category"  : "Report",
        "ihs_number" : "' . $_POST['IHS_NUMBER'] . '",
        "name_patient" : "' . $_POST['nama_pasien'] . '",
        "encounter_id" : "' . $_POST['ecounter'] . '",
        "display_ecounter" : "' . $_POST['display_ecounter'] . '",
        "date" : "'.date("Y-m-d").'",
        "id_practioner" : "' . $_POST['practioner'] . '",
        "name_practioner" : "' . $_POST['name_practioner'] . '",
        "title" : "' . $_POST['title'] . '",
        "code_section" : "' . $_POST['code_section'] . '",
        "display_section" : "' . $_POST['section'] . '",
        "div_text" : "' . $_POST['text_section'] . '"
    }';
    // echo "<pre>".var_export($postdata,true)."</pre>"; 

    curl_setopt($curl, CURLOPT_URL, $urlnya);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    
    echo $response;
  }
function GetCompositionByEcounter()
  {
    $getToken = json_decode($this->getToken());
    $urlnya = $this->db->query("SELECT nilai from seting_satusehat where key_setting='urlGetCompositionByEcounter'")->row()->nilai;
    $db_rs = $this->db->query("SELECT * FROM db_rs")->result();
    $username = $this->db->query("SELECT nilai from seting_satusehat where key_setting='x_username'")->row()->nilai;

    $curl = curl_init();
    // $urlnya = $url.'/Organization';
    $method = "GET"; // POST / PUT / DELETE
    $postdata = '';

    curl_setopt($curl, CURLOPT_URL, $urlnya . '' . $_POST['ecounter']);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array(
      'Content-Type: application/json',
      'x-username: ' . $username . '',
      'token: ' . $getToken->response->token . ''
    ));

    $response = curl_exec($curl);
    $res = json_decode($response);
    curl_close($curl);
    // echo "aaaaa<pre>" . var_export($res, true) . "</pre>";
    // die;

    if ($res->total <= 0) {
      echo '{success: false,pesan:"IHS_Number tidak dapat ditemukan"}';
      die;
    } else {
      for ($i=0; $i < $res->total ; $i++) { 
        $dat[] = array(
          'name_practioner' => $res->entry[$i]->resource->author[0]->display,
          'id_practioner' => $res->entry[$i]->resource->author[0]->reference,
          'code_category' => $res->entry[$i]->resource->category[0]->coding[0]->code,
          'display_category' => $res->entry[$i]->resource->category[0]->coding[0]->display,
          'custodian' => $res->entry[$i]->resource->custodian->reference,
          'tanggal' => $res->entry[$i]->resource->date,
          'display_ecounter' => $res->entry[$i]->resource->encounter->display,
          'id_ecounter' => $res->entry[$i]->resource->encounter->reference,
          'id_composition' => $res->entry[$i]->resource->id,
          'section' => $res->entry[$i]->resource->section[0]->code->coding[0]->code,
          'display_section' => $res->entry[$i]->resource->section[0]->code->coding[0]->display,
          'text_section' => $res->entry[$i]->resource->section[0]->text->div,
          'title' => $res->entry[$i]->resource->title,
          'code_type' => $res->entry[$i]->resource->type->coding[0]->code,
          'display_type' => $res->entry[$i]->resource->type->coding[0]->display,
          'ihs_number' => $res->entry[$i]->resource->subject->reference,
          'nama_patient' => $res->entry[$i]->resource->subject->display
        );
      }
          $res->success = true;
          echo json_encode($dat);
    }
  }
}

