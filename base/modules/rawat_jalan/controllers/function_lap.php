<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_lap extends  MX_Controller
{
	private $kd_kasir;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
		$this->kd_kasir = $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function laporan_poli_pasien_baru_lama()
	{
		$params = json_decode($_POST['data']);
		$kd_unit = $params->tmp_kd_unit[0][0];
		$unit 	= array();
		$params = array(
			'type_file' 	=> $params->type_file,
			'start_date' 	=> $params->start_date,
			'last_date' 	=> $params->last_date,
			'tmp_kd_unit' 	=> $params->tmp_kd_unit,
			'status' 		=> $params->status,
			'order_by' 		=> $params->orderBy,
		);

		//var_dump($kd_unit); die();
		$paramsCriteria = "WHERE tgl_masuk between '" . $params['start_date'] . "' AND '" . $params['last_date'] . "' ";
		$criteriaStatus = "";
		$criteriaUnit 	= "";
		$criteriaOrder 	= "";
		$html 			= "";

		if ($params['status'] != 1) {
			if ($params['status'] == 2) {
				$criteriaStatus = " AND LOWER(ket_kunjungan) = 'baru' ";
			} else {
				$criteriaStatus = " AND LOWER(ket_kunjungan) = 'lama' ";
			}
		}

		if ($params['order_by'] == 2) {
			$criteriaOrder  .= " ORDER BY pas.kd_pasien ";
		} else if ($params['order_by'] == 3) {
			$criteriaOrder  .= " ORDER BY pas.nama ";
		} else if ($params['order_by'] == 4) {
			$criteriaOrder  .= " ORDER BY kun.tgl_masuk ";
		} else {
			$criteriaOrder  .= " ORDER BY ket_kunjungan ";
		}

		$paramsCriteria .= $criteriaStatus;

		if ($kd_unit == "000") {
			$get_unit = $this->db->query("SELECT kd_unit from unit WHERE kd_bagian='2'")->result();
			foreach ($get_unit as  $value) {
				$criteriaUnit .= "'" . $value->kd_unit . "'" . ",";
			}
			$criteriaUnit = substr($criteriaUnit, 0, strlen($criteriaUnit) - 1);
		} else {
			for ($i = 0; $i < count($params['tmp_kd_unit']); $i++) {
				$criteriaUnit .= $params['tmp_kd_unit'][$i][0] . ",";
			}
			$criteriaUnit = substr($criteriaUnit, 0, strlen($criteriaUnit) - 1);
		}


		$query_head = $this->db->query("SELECT DISTINCT un.nama_unit
						FROM kunjungan kun INNER JOIN pasien pas ON pas.kd_pasien = kun.kd_pasien
						INNER JOIN unit un ON un.kd_unit = kun.kd_unit
						" . $paramsCriteria . "
						AND kun.kd_unit IN (" . $criteriaUnit . ")
						ORDER BY un.nama_unit ASC");

		$html .= "<table cellspacing='0' width='100%' border='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<td colspan='7' align='center'>Laporan Poli Pasien Baru dan Lama</td>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<td colspan='7' align='center'>Periode " . date_format(date_create($params['start_date']), 'd/M/Y') . " s/d " . date_format(date_create($params['last_date']), 'd/M/Y') . "</td>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "</tbody>";
		$html .= "</table>";
		$html .= "<br>";
		$html .= "<br>";

		$html .= "<table cellspacing='0' width='100%' border='1'>";
		//	var_dump(count($query_head->result()));
		if ($query_head->num_rows() > 0) {
			$html .= "<thead>";
			$html .= "<tr>";
			$html .= "<td align='center' rowspan='2'>No</td>";
			$html .= "<td align='center' rowspan='2'>Medrec</td>";
			$html .= "<td align='center' rowspan='2'>Nama Pasien</td>";
			$html .= "<td align='center' rowspan='2'>Alamat</td>";
			$html .= "<td align='center' rowspan='2'>Tanggal Masuk</td>";
			$html .= "<td align='center' colspan='2'>Kunjungan</td>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<td align='center'>Baru</td>";
			$html .= "<td align='center'>Lama</td>";
			$html .= "</tr>";
			$html .= "</thead>";
			$html .= "<tbody>";
			$index = 0;
			$total_baru = 0;
			$total_lama = 0;
			foreach ($query_head->result() as $result_head) {
				$no = 1;
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td colspan='6' style='padding-left:5px;'>" . $result_head->nama_unit . "</td>";
				$html .= "</tr>";
				// $query = $this->db->query("SELECT * FROM poli_baru_lama('" . $params['start_date'] . "', '" . $params['last_date'] . "', array[" . $criteriaUnit . "]) " . $paramsCriteria . " and nama_unit= '" . $result_head->nama_unit . "' " . $criteriaOrder);
				$query = $this->db->query("SELECT DISTINCT pas.kd_pasien,pas.nama,pas.alamat,kun.tgl_masuk,kun.kd_unit,un.nama_unit,
						CASE WHEN (SELECT COUNT(kun2.kd_pasien)FROM kunjungan kun2 WHERE kun2.kd_pasien IN (kun.kd_pasien) AND kun2.kd_unit IN (kun.kd_unit)
						AND kun2.tgl_masuk < kun.tgl_masuk) > 0 THEN 'Lama' ELSE 'Baru' END AS ket_kunjungan
						FROM kunjungan kun INNER JOIN pasien pas ON pas.kd_pasien = kun.kd_pasien
						INNER JOIN unit un ON un.kd_unit = kun.kd_unit
						" . $paramsCriteria . "
						AND kun.kd_unit IN (" . $criteriaUnit . ")
						and nama_unit= '" . $result_head->nama_unit . "'
						$criteriaOrder");
				$jumlah_baru = 0;
				$jumlah_lama = 0;
				foreach ($query->result() as $result) {

					$html .= "<tr>";
					$html .= "<td style='padding-left:5px;'>" . $no . "</td>";
					$html .= "<td style='padding-left:5px;'>" . $result->kd_pasien . "</td>";
					$html .= "<td style='padding-left:5px;'>" . $result->nama . "</td>";
					$html .= "<td style='padding-left:5px;'>" . $result->alamat . "</td>";
					$html .= "<td style='padding-left:5px;'>" . date_format(date_create($result->tgl_masuk), 'd/M/Y') . "</td>";
					if (strtolower($result->ket_kunjungan) == 'baru') {
						$html .= "<td align='center'>X</td>";
						$html .= "<td align='center'></td>";
						$total_baru++;
						$jumlah_baru++;
					} else {
						$html .= "<td align='center'></td>";
						$html .= "<td align='center'>X</td>";
						$jumlah_lama++;
						$total_lama++;
					}
					$html .= "</tr>";
					$no++;
				}
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td colspan='4' style='padding-left:5px;'><b>Sub Total</b></td>";
				$html .= "<td style='padding-left:5px;'><b>" . $jumlah_baru . "</b></td>";
				$html .= "<td style='padding-left:5px;'><b>" . $jumlah_lama . "</b></td>";
				$html .= "</tr>";
				$index++;
			}
			$html .= "<tr>";
			$html .= "<td></td>";
			$html .= "<td colspan='4' style='padding-left:5px;'><b>Grand Total</b></td>";
			$html .= "<td style='padding-left:5px;'><b>" . $total_baru . "</b></td>";
			$html .= "<td style='padding-left:5px;'><b>" . $total_lama . "</b></td>";
			$html .= "</tr>";
		} else {
			$html .= "<tr>";
			$html .= "<td colspan='7' align='center'>Data tidak ada</td>";
			$html .= "</tr>";
		}
		$html .= "</tbody>";
		$html .= "</table>";
		if ($params['type_file'] == true) {
			$name = 'laporan_poli_pasien_baru_lama.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=" . $name);
			//echo $html;

		} else {
			$prop = array('foot' => true);
			$common = $this->common;
			$this->common->setPdf('P', 'laporan_poli_pasien_baru_lama', $html);
			//echo $html;
		}
	}

	public function laporan_poli_pasien_baru_lama_direct()
	{
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param = json_decode($_POST['data']);
		$kdUnit = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user = $this->session->userdata['user_id']['id'];
		$user = $this->db->query("SELECT full_name FROM zusers WHERE kd_user='" . $kd_user . "'")->row()->full_name;
		# Create Data
		$tp = new TableText(145, 7, '', 0, false);
		# SET HEADER 
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs = $this->db->query("SELECT * FROM db_rs WHERE code='" . $kd_rs . "'")->row();
		$telp = '';
		$fax = '';
		if (($rs->phone1 != null && $rs->phone1 != '') || ($rs->phone2 != null && $rs->phone2 != '')) {
			$telp = 'Telp. ';
			$telp1 = false;
			if ($rs->phone1 != null && $rs->phone1 != '') {
				$telp1 = true;
				$telp .= $rs->phone1;
			}
			if ($rs->phone2 != null && $rs->phone2 != '') {
				if ($telp1 == true) {
					$telp .= '/' . $rs->phone2 . '.';
				} else {
					$telp .= $rs->phone2 . '.';
				}
			} else {
				$telp .= '.';
			}
		}
		if ($rs->fax != null && $rs->fax != '') {
			$fax = 'Fax. ' . $rs->fax . '.';
		}

		$params = json_decode($_POST['data']);
		$unit 	= array();
		$params = array(
			'type_file' 	=> $params->type_file,
			'start_date' 	=> $params->start_date,
			'last_date' 	=> $params->last_date,
			'tmp_kd_unit' 	=> $params->tmp_kd_unit,
			'status' 		=> $params->status,
			'order_by' 		=> $params->orderBy,
		);

		$paramsCriteria = "WHERE tgl_masuk between '" . $params['start_date'] . "' AND '" . $params['last_date'] . "' ";
		$criteriaStatus = "";
		$criteriaUnit 	= "";
		$criteriaOrder 	= "";
		$html 			= "";

		if ($params['status'] != 1) {
			if ($params['status'] == 2) {
				$criteriaStatus = " AND LOWER(ket_kunjungan) = 'baru' ";
			} else {
				$criteriaStatus = " AND LOWER(ket_kunjungan) = 'lama' ";
			}
		}

		if ($params['order_by'] == 2) {
			$criteriaOrder  .= " ORDER BY kd_pasien ";
		} else if ($params['order_by'] == 3) {
			$criteriaOrder  .= " ORDER BY nama ";
		} else if ($params['order_by'] == 4) {
			$criteriaOrder  .= " ORDER BY tgl_masuk ";
		} else {
			$criteriaOrder  .= " ORDER BY ket_kunjungan ";
		}

		$paramsCriteria .= $criteriaStatus;

		// die();

		for ($i = 0; $i < count($params['tmp_kd_unit']); $i++) {
			$criteriaUnit .= $params['tmp_kd_unit'][$i][0] . ",";
		}

		$criteriaUnit = substr($criteriaUnit, 0, strlen($criteriaUnit) - 1);
		// echo substr($criteriaUnit, 0 ,strlen($criteriaUnit)-1);
		$query_head = $this->db->query("SELECT DISTINCT(nama_unit) FROM poli_baru_lama('" . $params['start_date'] . "', '" . $params['last_date'] . "', array[" . $criteriaUnit . "]) " . $paramsCriteria . " ORDER BY nama_unit");

		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 13)
			->setColumnLength(2, 30)
			->setColumnLength(3, 45)
			->setColumnLength(4, 15)
			->setColumnLength(5, 6)
			->setColumnLength(6, 6)
			->setUseBodySpace(true);

		# SET HEADER REPORT
		$tp->addSpace("header")
			->addColumn($rs->name, 7, "left")
			->commit("header")
			->addColumn($rs->address . ", " . $rs->city, 7, "left")
			->commit("header")
			->addColumn($telp, 7, "left")
			->commit("header")
			->addColumn($fax, 7, "left")
			->commit("header")
			->addColumn("LAPORAN POLI KUNJUNGAN PASIEN BARU DAN LAMA", 7, "center")
			->commit("header")
			->addColumn("Periode " . date_format(date_create($params['start_date']), 'd/M/Y') . " s/d " . date_format(date_create($params['last_date']), 'd/M/Y'), 7, "center")
			->commit("header");
		$tp->addColumn("", 7, "center")
			->commit("header");

		if ($query_head->num_rows() > 0) {

			$tp->addColumn("NO.", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
				->addColumn("NO. MEDREC", 1, "left")
				->addColumn("NAMA PASIEN", 1, "left")
				->addColumn("ALAMAT", 1, "left")
				->addColumn("TANGGAL MASUK", 1, "left")
				->addColumn("KUNJUNGAN", 2, "center")
				->commit("header");
			$tp->addColumn("", 1, "left") # (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
				->addColumn("", 1, "left")
				->addColumn("", 1, "left")
				->addColumn("", 1, "left")
				->addColumn("", 1, "left")
				->addColumn("BARU", 1, "center")
				->addColumn("LAMA", 1, "center")
				->commit("header");

			$index = 0;
			$total_baru = 0;
			$total_lama = 0;
			foreach ($query_head->result() as $result_head) {
				$no = 1;
				$tp->addColumn("", 1, "left")
					->addColumn($result_head->nama_unit, 6, "left")
					->commit("header");

				$query = $this->db->query("SELECT * FROM poli_baru_lama('" . $params['start_date'] . "', '" . $params['last_date'] . "', array[" . $params['tmp_kd_unit'][$index][0] . "]) " . $paramsCriteria . " " . $criteriaOrder);
				$jumlah_baru = 0;
				$jumlah_lama = 0;
				foreach ($query->result() as $result) {

					$tp->addColumn($no, 1, "left")
						->addColumn($result->kd_pasien, 1, "left")
						->addColumn($result->nama, 1, "left")
						->addColumn($result->alamat, 1, "left")
						->addColumn(date_format(date_create($result->tgl_masuk), 'd/M/Y'), 1, "left");
					if (strtolower($result->ket_kunjungan) == 'baru') {
						$tp->addColumn("X", 1, "center")
							->addColumn("", 1, "center");
						$total_baru++;
						$jumlah_baru++;
					} else {
						$tp->addColumn("", 1, "center")
							->addColumn("X", 1, "center");
						$jumlah_lama++;
						$total_lama++;
					}
					$tp->commit("header");
					$no++;
				}

				$tp->addColumn("", 1, "left")
					->addColumn("SUB TOTAL POLI " . $result_head->nama_unit, 4, "left")
					->addColumn($jumlah_baru, 1, "center")
					->addColumn($jumlah_lama, 1, "center")
					->commit("header");
				$index++;
				$tp->addColumn("", 7, "center")
					->commit("header");
			}
			$tp->addColumn("", 1, "left")
				->addColumn("GRAND TOTAL", 4, "left")
				->addColumn($total_baru, 1, "center")
				->addColumn($total_lama, 1, "center")
				->commit("header");
		} else {
			$tp->addColumn("Data Tidak Ada", 7, "center")
				->commit("header");
		}

		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");
		$tp->addLine("footer")
			->addColumn("Operator : " . $user, 3, "left")
			->addColumn(tanggalstring(date('Y-m-d')) . " " . gmdate(" H:i:s", time() + 60 * 60 * 7), 3, "center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data

		$file =  '/home/tmp/lap_kunjungan_baru_lama.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27) . chr(115) . chr(1);
		$margin = chr(27) . chr(78) . chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer = $this->db->query("select p_bill from zusers where kd_user='" . $kd_user . "'")->row()->p_bill; //'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}

	/*
		Laporan Status Pulang
		Hadad Al Gojali
		2017-13-14
	 */
	public function laporan_poli_status_posting()
	{
		$judul_laporan = "Laporan Status Posting (RWJ)";
		$param = json_decode($_POST['data']);
		$unit 	= array();
		$params = array(
			'posting_type' 	=> $param->posting_type,
			'type_file' 	=> $param->type_file,
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'tmp_kd_unit' 	=> $param->tmp_kd_unit,
			'kelompok' 		=> $param->kelompok,
			// 'status' 		=> $params->status,
			'order_by' 		=> $param->orderBy,
		);

		$criteria = "";
		$criteria .= " k.tgl_masuk between '" . $params['start_date'] . "' and '" . $params['last_date'] . "' ";
		$criteria .= " and k.kd_unit in (" . $params['tmp_kd_unit'] . ") ";
		$criteriaOrder = "";
		if ($params['order_by'] == 2) {
			$criteriaOrder  .= " ORDER BY k.kd_pasien ";
		} else if ($params['order_by'] == 3) {
			$criteriaOrder  .= " ORDER BY p.nama ";
		} else if ($params['order_by'] == 4) {
			$criteriaOrder  .= " ORDER BY k.tgl_masuk ";
		} else {
			$criteriaOrder  .= " ORDER BY posting_transaksi ";
		}

		$label_kelompok = "SEMUA";
		if (strtolower($params['kelompok']) != "semua") {
			$query_kelompok = $this->db->query("SELECT customer FROM customer WHERE kd_customer = '" . $params['kelompok'] . "'");
			if ($query_kelompok->num_rows() > 0) {
				$label_kelompok = $query_kelompok->row()->customer;
			}
		}

		$label_posting 		= "Semua";
		if ($params['posting_type'] === 2 || $params['posting_type'] == '2') {
			$criteria 	.= " AND t.posting_transaksi = 'true' ";
			$label_posting = "SUDAH POSTING";
		} else if ($params['posting_type'] === 3 || $params['posting_type'] == '3') {
			$criteria 	.= " AND t.posting_transaksi = 'false' ";
			$label_posting = "BELUM POSTING";
		}

		$query_laporan = "SELECT p.kd_pasien,
			p.nama,
			p.jenis_kelamin,
			t.posting_transaksi,
			k.tgl_masuk,
			u.nama_unit,
			c.customer AS penjamin 
			FROM
				transaksi t
				INNER JOIN kunjungan k ON k.tgl_masuk = t.tgl_transaksi 
				AND k.urut_masuk = t.urut_masuk 
				AND k.kd_unit = t.kd_unit
				INNER JOIN customer c ON c.kd_customer = k.kd_customer
				INNER JOIN pasien p ON t.kd_pasien = p.kd_pasien AND k.kd_pasien = p.kd_pasien
				INNER JOIN unit u ON u.kd_unit = k.kd_unit
			WHERE $criteria $criteriaOrder
		";

		$result_query = $this->db->query($query_laporan);

		$header = array();
		foreach ($result_query->result() as $result_header) {
			$header[] = $result_header->nama_unit;
		}
		$tmp_header   = implode('#', array_unique($header));
		$array_header = explode("#", $tmp_header);

		if ($params['type_file'] === true || $params['type_file'] == 'true') {

			$url      = "Doc Asset/format_laporan/Status Posting RWJ.xls";
			$phpExcel = new PHPExcel();
			$phpExcel = PHPExcel_IOFactory::load($url);

			$sharedStyle = new PHPExcel_Style();
			$sharedStyle->applyFromArray(
				array(
					'borders' => array(
						'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					),
				)
			);
			$sheet    = $phpExcel->getActiveSheet();
			$phpExcel->getActiveSheet()->setCellValue("A3", "Kelompok Pasien : " . $label_kelompok);
			$phpExcel->getActiveSheet()->setCellValue("A4", "Periode : " . date_format(date_create($params['start_date']), 'Y-m-d') . " s/d " . date_format(date_create($params['last_date']), 'Y-m-d'));
			$phpExcel->getActiveSheet()->setCellValue("A5", "Status Posting  : " . $label_posting);
			$baris 		= 9;
			for ($i = 0; $i < count($array_header); $i++) {
				$phpExcel->getActiveSheet()->setCellValue("A" . $baris, $array_header[$i]);
				$phpExcel->getActiveSheet()->mergeCells('A' . $baris . ":I" . $baris);
				$phpExcel->getActiveSheet()
					->getStyle('A' . $baris . ":I" . $baris)
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$no_body 	= 1;
				$baris++;
				foreach ($result_query->result() as $result) {
					if ($result->nama_unit == $array_header[$i]) {
						$phpExcel->getActiveSheet()->setCellValue("A" . $baris, $no_body);
						$phpExcel->getActiveSheet()->setCellValue("B" . $baris, $result->kd_pasien);
						$phpExcel->getActiveSheet()->setCellValue("C" . $baris, $result->nama);

						if ($result->jenis_kelamin == 't' || $result->jenis_kelamin === true) {
							$phpExcel->getActiveSheet()->setCellValue("D" . $baris, "X");
						} else {
							$phpExcel->getActiveSheet()->setCellValue("E" . $baris, "X");
						}


						if ($result->posting_transaksi == 't' || $result->posting_transaksi === true) {
							$phpExcel->getActiveSheet()->setCellValue("F" . $baris, "X");
						} else {
							$phpExcel->getActiveSheet()->setCellValue("G" . $baris, "X");
						}
						$phpExcel->getActiveSheet()->setCellValue("H" . $baris, date_format(date_create($result->tgl_masuk), 'Y-m-d'));
						$phpExcel->getActiveSheet()->setCellValue("I" . $baris, $result->penjamin);
						$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A' . $baris . ':I' . $baris);
						$no_body++;
						$baris++;
					}
				}
			}

			header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
			header('Content-Disposition: attachment;filename=' . str_replace(" ", "_", $judul_laporan) . '.xls'); # specify the download file name
			header('Cache-Control: max-age=0');

			# Creates a writer to output the $objPHPExcel's content
			$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
			$writer->save('php://output');
		} else {
			$html = "";
			if ($result_query->num_rows() > 0) {
				$html .= "<table width='100%' border='0' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
				$html .= "<th>LAPORAN STATUS POSTING PASIEN RWJ</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th>Kelompok Pasien : " . $label_kelompok . "</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th>Periode : " . date_format(date_create($params['start_date']), 'Y-m-d') . " s/d " . date_format(date_create($params['last_date']), 'Y-m-d') . "</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th>Status Posting : " . $label_posting . "</th>";
				$html .= "</tr>";
				$html .= "</thead>";
				$html .= "</table>";
				$html .= "</br>";

				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";

				$html .= "<th width='5%' rowspan='2'>No</th>";
				$html .= "<th width='15%' rowspan='2'>No Medrec</th>";
				$html .= "<th width='35%' rowspan='2'>Nama</th>";
				$html .= "<th colspan='2'>Kelamin</th>";
				$html .= "<th colspan='2'>Status Posting</th>";
				$html .= "<th width='10%' rowspan='2'>Tgl Masuk</th>";
				$html .= "<th width='10%' rowspan='2'>Penjamin</th>";

				$html .= "</tr>";
				$html .= "<tr>";

				$html .= "<th width='7%'>Laki-laki</th>";
				$html .= "<th width='7%'>Perempuan</th>";
				$html .= "<th width='7%'>Sudah</th>";
				$html .= "<th width='7%'>Belum</th>";

				$html .= "</tr>";
				$html .= "</thead>";

				$html .= "<tbody>";
				$no_head = 1;
				$no_body = 1;
				for ($i = 0; $i < count($array_header); $i++) {
					$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td colspan='8'>" . $array_header[$i] . "</td>";
					foreach ($result_query->result() as $result) {
						if ($result->nama_unit == $array_header[$i]) {
							$html .= "<tr>";
							$html .= "<td align='center'>" . $no_body . "</td>";
							$html .= "<td style='padding-left:5px;'>" . $result->kd_pasien . "</td>";
							$html .= "<td style='padding-left:5px;'>" . $result->nama . "</td>";
							if ($result->jenis_kelamin == 't' || $result->jenis_kelamin === true) {
								$html .= "<td align='center'>X</td>";
								$html .= "<td align='center'></td>";
							} else {
								$html .= "<td align='center'></td>";
								$html .= "<td align='center'>X</td>";
							}
							if ($result->posting_transaksi == 't' || $result->posting_transaksi === true) {
								$html .= "<td align='center'>X</td>";
								$html .= "<td align='center'></td>";
							} else {
								$html .= "<td align='center'></td>";
								$html .= "<td align='center'>X</td>";
							}
							$html .= "<td style='padding-left:5px;'>" . date_format(date_create($result->tgl_masuk), 'Y-m-d') . "</td>";
							$html .= "<td style='padding-left:5px;'>" . $result->penjamin . "</td>";
							$html .= "</tr>";
							$no_body++;
						}
					}
					$html .= "</tr>";
					$no_head++;
				}
				$html .= "</tbody>";
				$html .= "</table>";
				// echo $html;
				$this->common->setPdf('P', $judul_laporan, $html);
			}
		}
	}


	function laporan_arus_pendapatan_summary()
	{
		$common = $this->common;
		$result = $this->result;
		$title  = 'Rekapitulasi Arus Pendapatan (RWJ)';
		$param  = json_decode($_POST['data']);

		$tgl_awal_i  = str_replace("/", "-", $param->start_date);
		$tgl_akhir_i = str_replace("/", "-", $param->last_date);
		$tgl_awal    = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir   = date('Y-m-d', strtotime($tgl_akhir_i));

		$html = '';

		/*Parameter Unit*/
		$tmpKdUnit = "";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i = 0; $i < count($arrayDataUnit); $i++) {
			$tmpKdUnit .= $arrayDataUnit[$i][0] . ",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);

		if ($tmpKdUnit == '000') {
			$kduser = $this->session->userdata['user_id']['id'];
			$tmpKdUnit = $this->db->query("select kd_unit from zusers where kd_user='" . $kduser . "'")->row()->kd_unit;
			$kriteria_unit = " And (t.kd_unit in (" . $tmpKdUnit . ")) ";
		} else {
			$kriteria_unit = " And (t.kd_unit in (" . $tmpKdUnit . ")) ";
		}

		/* Parameter Pembayaran*/
		$tmpKdPay = "";
		$arrayDataPay = $param->tmp_payment;
		for ($i = 0; $i < count($arrayDataPay); $i++) {
			$tmpKdPay .= "'" . $arrayDataPay[$i][0] . "',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);

		$semua = "'00'"; //jika semua kd pay

		$temp_kd_pay = "";
		if ($tmpKdPay == $semua) {
			$kd_pay = $this->db->query("select kd_pay from payment ")->result();
			foreach ($kd_pay as $pay) {
				$temp_kd_pay .= "'" . $pay->kd_pay . "',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " " . $temp_kd_pay . " ";
			// $kriteria_bayar = " And (dtb.Kd_Pay in (".$temp_kd_pay.")) ";
		} else {
			$kriteria_bayar = "" . $tmpKdPay . " ";
			// $kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
		}

		/*Parameter Customer*/
		$tmpKdCustomer = "";
		$arrayDataCustomer = $param->tmp_kd_customer;
		if ($arrayDataCustomer == '') {
			$kriteria_customer = " ";
		} else {
			$tmpKdCustomer = "";
			for ($i = 0; $i < count($arrayDataCustomer); $i++) {
				$tmpKdCustomer .= "'" . $arrayDataCustomer[$i][0] . "',";
			}
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$kriteria_customer = " And Ktr.kd_Customer in (" . $tmpKdCustomer . ") ";
		}

		$query = "SELECT
			x.CUSTOMER AS penjamin,
			x.KD_PASIEN AS medrec,
			x.Nama_Pasien AS nama,
			x.NAMA_UNIT AS unit_rawat,
			x.tgl_masuk AS tgl_masuk,
			SUM( x.Operatif ) AS operatif,
			SUM( x.NonOperatif ) AS non_operatif,
			SUM( x.Radiologi ) AS RAD,
			SUM( x.LaboratoriumPK ) AS lab_pk,
			SUM( x.LaboratoriumPA ) AS lab_pa,
			SUM( x.PENDAFTARAN ) AS pendaftaran,
			SUM( x.OBAT ) AS obat,
			SUM( x.Operatif ) + SUM( x.NonOperatif ) + SUM( x.Radiologi ) + SUM( x.LaboratoriumPK ) + SUM( x.LaboratoriumPA ) + SUM( x.PENDAFTARAN ) + SUM( x.OBAT ) AS tot_tagihan,
			y.bayar - (
			SUM( x.Operatif ) + SUM( x.NonOperatif ) + SUM( x.Radiologi ) + SUM( x.LaboratoriumPK ) + SUM( x.LaboratoriumPA ) + SUM( x.PENDAFTARAN ) + SUM( x.OBAT ) 
			) AS SELISIH,
			y.bayar AS tot_bayar,
			x.NO_SJP AS no_sep 
		FROM
			(
				SELECT DISTINCT
					c.CUSTOMER,
					p.NAMA AS Nama_Pasien,
					p.KD_PASIEN,
					u.NAMA_UNIT,
					t.TGL_TRANSAKSI AS tgl_masuk,
					SUM( x.Operatif ) AS Operatif,
					SUM( x.NonOperatif ) AS NonOperatif,
					SUM( x.Radiologi ) AS Radiologi,
					SUM( x.LaboratoriumPK ) AS LaboratoriumPK,
					SUM( x.LaboratoriumPA ) AS LaboratoriumPA,
					SUM( x.Obat ) AS OBAT,
					SUM( x.Pendaftaran ) AS PENDAFTARAN,
					t.NO_TRANSAKSI,
					t.KD_KASIR,
					k.NO_SJP 
				FROM
					TRANSAKSI AS t
					INNER JOIN PASIEN AS p ON t.KD_PASIEN = p.KD_PASIEN
					INNER JOIN UNIT AS u ON t.KD_UNIT = u.KD_UNIT
					INNER JOIN KUNJUNGAN AS k ON t.KD_PASIEN = k.KD_PASIEN 
					AND t.TGL_TRANSAKSI = k.TGL_MASUK 
					AND t.KD_UNIT = k.KD_UNIT 
					AND t.URUT_MASUK = k.URUT_MASUK
					INNER JOIN CUSTOMER AS c ON c.KD_CUSTOMER = k.KD_CUSTOMER
					INNER JOIN (
						SELECT
							dt.NO_TRANSAKSI,
							dt.KD_KASIR,
							dt.TGL_TRANSAKSI,
							dt.URUT,
							dt.HARGA,
							dt.QTY,
							SUM( CASE WHEN kp.kd_klas LIKE '61%' AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0 END ) AS Operatif,
							SUM( CASE WHEN LEFT ( kp.kd_klas, 2 ) IN ( '64', '75' ) AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0 END ) AS NonOperatif,
							SUM( CASE WHEN kp.kd_klas IN ( '9' ) AND dt.kd_produk IN ( '1093' )  AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0  END ) AS Radiologi,
							SUM( CASE WHEN kp.kd_klas IN ( '9' ) AND dt.kd_produk IN ( '1092' )  AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0  END ) AS LaboratoriumPK,
							SUM( CASE WHEN kp.kd_klas IN ( '9' ) AND dt.kd_produk IN ( '3848' )  AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0  END ) AS LaboratoriumPA,
							SUM( CASE WHEN dt.kd_produk IN ( '1', '416', '6388', '6487', '6489' ) AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0 END ) AS Pendaftaran,
							SUM( CASE WHEN kp.kd_klas IN ( '9' ) AND dt.kd_produk IN ( '6440' ) AND dt.kd_kasir = '01' THEN dt.harga * dt.qty ELSE 0 END ) AS Obat 
						FROM
							DETAIL_TRANSAKSI AS dt
							INNER JOIN TRANSAKSI AS t ON t.NO_TRANSAKSI = dt.NO_TRANSAKSI 
							AND t.KD_KASIR = dt.KD_KASIR
							INNER JOIN (
								SELECT
									KD_KASIR,
									NO_TRANSAKSI,
									TGL_TRANSAKSI,
									KD_UNIT,
									KD_PAY,
									SUM( JUMLAH ) AS JUMLAH 
								FROM
									DETAIL_BAYAR 
								WHERE
									( KD_KASIR = '01' ) 
									AND ( LEFT ( KD_UNIT, 1 ) = '2' ) 
								GROUP BY KD_KASIR,NO_TRANSAKSI,TGL_TRANSAKSI,KD_UNIT,KD_PAY 
							) AS db ON t.KD_KASIR = db.KD_KASIR 
							AND t.NO_TRANSAKSI = db.NO_TRANSAKSI
							INNER JOIN PRODUK AS p ON p.KD_PRODUK = dt.KD_PRODUK
							INNER JOIN KLAS_PRODUK AS kp ON kp.KD_KLAS = p.KD_KLAS 
						WHERE
							( dt.KD_KASIR = '01' ) 
							AND ( dt.TGL_TRANSAKSI BETWEEN '$tgl_awal' AND '$tgl_akhir' ) 
							AND ( t.ISPAY = '1' ) 
							AND ( db.KD_PAY IN ($kriteria_bayar) ) 
						GROUP BY dt.KD_KASIR,dt.NO_TRANSAKSI,dt.TGL_TRANSAKSI,dt.URUT,dt.HARGA,dt.QTY 
					) AS x ON t.NO_TRANSAKSI = x.NO_TRANSAKSI 
					AND t.KD_KASIR = x.KD_KASIR 
				WHERE
					( t.KD_KASIR = '01' ) 
					AND ( u.KD_UNIT IN ($tmpKdUnit) ) 
				GROUP BY p.NAMA,c.CUSTOMER,p.KD_PASIEN,t.TGL_TRANSAKSI,k.TGL_MASUK,t.NO_TRANSAKSI,t.KD_KASIR,u.NAMA_UNIT,k.NO_SJP 
			) AS x
			INNER JOIN ( SELECT KD_KASIR, NO_TRANSAKSI, SUM( JUMLAH ) AS bayar FROM DETAIL_BAYAR WHERE ( KD_KASIR = '01' ) GROUP BY KD_KASIR, NO_TRANSAKSI ) AS y ON x.NO_TRANSAKSI = y.NO_TRANSAKSI 
			AND x.KD_KASIR = y.KD_KASIR 
		GROUP BY x.CUSTOMER,x.Nama_Pasien,x.KD_PASIEN,x.tgl_masuk,y.bayar,x.NAMA_UNIT,x.NO_SJP 
		ORDER BY x.NAMA_UNIT,x.CUSTOMER";

		$query_result = $this->db->query($query);
		if ($query_result->num_rows() > 0) {
			if ($param->type_file == false) {
				$table = "";
				$table .= "<table border='0' cellspacing='0' width='100%'>";
				$table .= "<thead>";
				$table .= "<tr>";
				$table .= "<th align='center'>" . $title . "</th>";
				$table .= "</tr>";

				$table .= "<tr>";
				$table .= "<th align='center'>Periode : " . $tgl_awal . " s/d " . $tgl_akhir . "</th>";
				$table .= "</tr>";
				$table .= "</thead>";
				$table .= "</table>";
				$table .= "</hr>";

				$table .= "<table border='1' cellspacing='0' width='100%'>";
				$table .= "<thead>";
				$table .= "<tr>";
				$table .= "<th width='5'>No</th>";
				$table .= "<th>Penjamin</th>";
				$table .= "<th>Medrec</th>";
				$table .= "<th>Nama</th>";
				$table .= "<th>Unit</th>";
				$table .= "<th>Tgk Masuk</th>";
				$table .= "<th>Operatif</th>";
				$table .= "<th>Non Operatif</th>";
				$table .= "<th>Radiologi</th>";
				$table .= "<th>Lab PK</th>";
				$table .= "<th>Lab PA</th>";
				$table .= "<th>Pendaftaran</th>";
				$table .= "<th>Obat</th>";
				$table .= "<th>Total Tagihan</th>";
				$table .= "<th>Selisih</th>";
				$table .= "<th>Total Bayar</th>";
				$table .= "<th>No SEP</th>";
				$table .= "</tr>";
				$table .= "</thead>";
				$table .= "<tbody>";
				$no = 1;
				foreach ($query_result->result() as $result) {
					$table .= "<tr>";
					$table .= "<td>" . $no . "</td>";
					$table .= "<td>" . $result->penjamin . "</td>";
					$table .= "<td>" . $result->medrec . "</td>";
					$table .= "<td>" . $result->nama . "</td>";
					$table .= "<td>" . $result->unit_rawat . "</td>";
					$table .= "<td>" . date_format(date_create($result->tgl_masuk), 'Y-m-d') . "</td>";
					$table .= "<td>" . $result->operatif . "</td>";
					$table .= "<td>" . $result->non_operatif . "</td>";
					$table .= "<td>" . $result->rad . "</td>";
					$table .= "<td>" . $result->lab_pk . "</td>";
					$table .= "<td>" . $result->lab_pa . "</td>";
					$table .= "<td>" . $result->pendaftaran . "</td>";
					$table .= "<td>" . $result->obat . "</td>";
					$table .= "<td>" . $result->tot_tagihan . "</td>";
					$table .= "<td>" . $result->selisih . "</td>";
					$table .= "<td>" . $result->tot_bayar . "</td>";
					$table .= "<td>" . $result->no_sep . "</td>";
					$table .= "</tr>";
					$no++;
				}
				$table .= "</tbody>";
				$table .= "</table>";

				// echo $table;
				$this->common->setPdf('L', $title, $table);
			} else {
				$url = "Doc Asset/format_laporan/" . $title . ".xls";
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel->getActiveSheet();

				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : " . $tgl_awal . " s/d " . $tgl_akhir);
				$phpExcel->getActiveSheet()->setCellValue('A3', "Unit : ");
				$phpExcel->getActiveSheet()->setCellValue('A4', "Kelompok Pasien : ");

				$no    = 1;
				$baris = 7;
				foreach ($query_result->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A' . $baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B' . $baris, $result->penjamin);
					$phpExcel->getActiveSheet()->setCellValue('C' . $baris, $result->medrec);
					$phpExcel->getActiveSheet()->setCellValue('D' . $baris, $result->nama);
					$phpExcel->getActiveSheet()->setCellValue('E' . $baris, $result->unit_rawat);
					$phpExcel->getActiveSheet()->setCellValue('F' . $baris, date_format(date_create($result->tgl_masuk), 'Y-m-d'));
					$phpExcel->getActiveSheet()->setCellValue('G' . $baris, $result->operatif);
					$phpExcel->getActiveSheet()->setCellValue('H' . $baris, $result->non_operatif);
					$phpExcel->getActiveSheet()->setCellValue('I' . $baris, $result->rad);
					$phpExcel->getActiveSheet()->setCellValue('J' . $baris, $result->lab_pk);
					$phpExcel->getActiveSheet()->setCellValue('K' . $baris, $result->lab_pa);
					$phpExcel->getActiveSheet()->setCellValue('L' . $baris, $result->pendaftaran);
					$phpExcel->getActiveSheet()->setCellValue('M' . $baris, $result->obat);
					$phpExcel->getActiveSheet()->setCellValue('N' . $baris, $result->tot_tagihan);
					$phpExcel->getActiveSheet()->setCellValue('O' . $baris, $result->selisih);
					$phpExcel->getActiveSheet()->setCellValue('P' . $baris, $result->tot_bayar);
					$phpExcel->getActiveSheet()->setCellValue('Q' . $baris, $result->no_sep);
					$no++;
					$baris++;
				}

				header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
				header('Content-Disposition: attachment;filename=' . $title . '.xls'); # specify the download file name
				header('Cache-Control: max-age=0');

				# Creates a writer to output the $objPHPExcel's content
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		} else {
			echo "<h3>Data tidak ada</h3>";
		}
	}

	public function laporan_detail_transaksi()
	{
		$common = $this->common;
		$result = $this->result;
		$title  = 'Laporan Detail Transaksi(RWJ)';
		$param       = json_decode($_POST['data']);

		$tgl_awal_i  = str_replace("/", "-", $param->start_date);
		$tgl_akhir_i = str_replace("/", "-", $param->last_date);
		$tgl_awal    = date('d-M-Y', strtotime($tgl_awal_i));
		$tgl_akhir   = date('d-M-Y', strtotime($tgl_akhir_i));
		$type_file   = $param->type_file;
		$order       = $param->orderBy;

		$tmpKdUnit 	= "";
		$label_unit = "";
		for ($i = 0; $i < count($param->tmp_kd_unit); $i++) {
			$tmpKdUnit .= "" . $param->tmp_kd_unit[$i][0] . ",";
			$query     = $this->db->query("SELECT nama_unit FROM unit where kd_unit = " . $param->tmp_kd_unit[$i][0] . "");
			if ($query->num_rows() > 0) {
				$label_unit .= $query->row()->nama_unit . ",";
			}
		}
		$label_unit = substr($label_unit, 0, -1);
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);

		$tmpKdPay = "";
		for ($i = 0; $i < count($param->tmp_payment); $i++) {
			$tmpKdPay .= "'" . $param->tmp_payment[$i][0] . "',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);

		$tmpKdCustomer = "";
		$label_customer = "";
		if (strtoupper($param->kd_customer) == "SEMUA") {
			$query = $this->db->query("SELECT kd_customer FROM customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $result) {
					$tmpKdCustomer .= "'" . $result->kd_customer . "',";
				}
				$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			}
		} else {
			$query      	= $this->db->query("SELECT customer FROM customer where kd_customer = '" . $param->kd_customer . "'");
			if ($query->num_rows() > 0) {
				$label_customer .= $query->row()->customer;
			}
			$tmpKdCustomer 	.= "'" . str_replace("'", "", $param->kd_customer) . "'";
		}

		$query = "SELECT * from function_laporan_detail_pasien_per_tindakan_rwj(array[" . $tmpKdUnit . "], array[" . $tmpKdCustomer . "], '" . $tgl_awal . "', '" . $tgl_akhir . "', '" . $this->kd_kasir . "', array[" . $tmpKdPay . "])";
		$query = $this->db->query($query);

		if ($query->num_rows()) {
			if ($param->type_file == false) {
			} else {

				$url = "Doc Asset/format_laporan/" . $title . ".xls";
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel->getActiveSheet();

				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : " . $tgl_awal . " s/d " . $tgl_akhir);
				$phpExcel->getActiveSheet()->setCellValue('A3', "Unit : " . $label_unit);
				$phpExcel->getActiveSheet()->setCellValue('A4', "Kelompok Pasien : " . $label_customer);

				$no    = 1;
				$baris = 7;
				foreach ($query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A' . $baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B' . $baris, $result->kd_pasien);
					$phpExcel->getActiveSheet()->setCellValue('C' . $baris, $result->nama);
					$phpExcel->getActiveSheet()->setCellValue('D' . $baris, $result->no_sjp);
					$phpExcel->getActiveSheet()->setCellValue('E' . $baris, $result->jk);
					$phpExcel->getActiveSheet()->setCellValue('F' . $baris, date_format(date_create($result->tgl_lahir), 'Y-m-d'));
					$phpExcel->getActiveSheet()->setCellValue('G' . $baris, date_format(date_create($result->tanggal_transaksi), 'Y-m-d'));
					$phpExcel->getActiveSheet()->setCellValue('H' . $baris, $result->no_asuransi);
					$phpExcel->getActiveSheet()->setCellValue('I' . $baris, $result->alamat);
					$phpExcel->getActiveSheet()->setCellValue('J' . $baris, $result->nama_unit);
					$phpExcel->getActiveSheet()->setCellValue('K' . $baris, $result->pemeriksaan);
					$phpExcel->getActiveSheet()->setCellValue('L' . $baris, $result->karcis_kunjungan_1);
					$phpExcel->getActiveSheet()->setCellValue('M' . $baris, $result->karcis_kunjungan_berikutnya);
					$phpExcel->getActiveSheet()->setCellValue('N' . $baris, $result->resep);
					$phpExcel->getActiveSheet()->setCellValue('O' . $baris, $result->radiologi);
					$phpExcel->getActiveSheet()->setCellValue('P' . $baris, $result->laboratorium);
					$phpExcel->getActiveSheet()->setCellValue('Q' . $baris, $result->endoscopy);
					$phpExcel->getActiveSheet()->setCellValue('R' . $baris, $result->radiotherapy);
					$phpExcel->getActiveSheet()->setCellValue('S' . $baris, $result->rehabmedik);
					$no++;
					$baris++;
				}

				header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
				header('Content-Disposition: attachment;filename=' . $title . '.xls'); # specify the download file name
				header('Cache-Control: max-age=0');

				# Creates a writer to output the $objPHPExcel's content
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		} else {
			echo "Tidak ada data";
		}
	}

	public function laporan_bukti_sektor()
	{
		$common = $this->common;
		$result = $this->result;
		$title  = 'LAPORAN PENERIMAAN PERPASIEN';
		$param       = json_decode($_POST['data']);
		// echo '<pre>' . var_export($param, true) . '</pre>';
		// die;
		$tgl_awal_i  = str_replace("/", "-", $param->tgl_awal);
		$tgl_akhir_i = str_replace("/", "-", $param->tgl_akhir);
		$tgl_awal    = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir   = date('Y-m-d', strtotime($tgl_akhir_i));
		$kasir 		 = $param->unit;

		$whereShift = '';
		if (isset($param->shift_awal_1)) {
			$whereShift = '1';
		}
		if (isset($param->shift_awal_2)) {
			$whereShift .= ',2';
		}
		if (isset($param->shift_awal_3)) {
			$whereShift .= ',3';
		}

		$whereShift = trim($whereShift, ',');

		$whereUnit1 = '';
		$whereUnit2 = '';
		if ($param->unit == 'Gawat Darurat') {
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 3 )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 3 ) )';
		} else if ($param->unit == 'Rawat Jalan') {
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 2 )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 2 ) )';
		} else if ($param->unit == 'Rawat Inap') {
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 1 )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 1 ) )';
		} else if ($param->unit == 'Laboratorium') {
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 4 )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 4 ) )';
		} else if ($param->unit == 'Radiologi') {
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 5 )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 5 ) )';
		} else if ($param->unit == 'Apotek') {
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 6 )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN = 6 ) )';
		} else if ($param->unit == 'Semua') {
			$kasir = 'Gawat Darurat,Rawat Jalan,Rawat Inap,Laboratorium,Radiologi,Apotek';
			$whereUnit1 = 'AND T.KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN IN (1,2,3,4,5,6) )';
			$whereUnit2 = 'AND T.KD_KASIR IN ( SELECT DISTINCT KD_KASIR FROM KASIR_UNIT WHERE KD_UNIT IN ( SELECT KD_UNIT FROM UNIT WHERE KD_BAGIAN IN (1,2,3,4,5,6) ) )';
		}

		$html = '';

		$query = "SELECT KD_PASIEN,NAMA,NO_TRANSAKSI,NO_NOTA,NAMA_UNIT,TUNAI = SUM ( TUNAI ),SUBSIDI = SUM ( SUBSIDI ),
		PIUTANG = SUM ( PIUTANG ),KET,TGL_MASUK,KD_UNIT,URUT_MASUK,KD_USER,FULL_NAME 
		FROM(SELECT PS.KD_PASIEN,PS.NAMA,T.NO_TRANSAKSI,NB.NO_NOTA,U.NAMA_UNIT,
		TUNAI = CASE WHEN ( MAX ( PY.KD_PAY ) IN ( 'DP', 'IA', 'TU' ) ) THEN SUM ( NB.JUMLAH ) ELSE 0 END,
		SUBSIDI = CASE WHEN ( MAX ( PY.KD_PAY ) IN ( 'KR' ) ) THEN SUM ( NB.JUMLAH ) ELSE 0 END,
		PIUTANG = CASE WHEN (( MAX ( PY.KD_PAY ) NOT IN ( 'DP', 'IA', 'TU' ) ) AND MAX ( PY.KD_PAY ) NOT IN ( 'KR' ) ) THEN SUM ( NB.JUMLAH ) ELSE 0 END,
		KET = CASE WHEN NB.JUMLAH = 0 THEN 'GAGAL CETAK/DI ULANG' ELSE '' END,
		K.TGL_MASUK,K.KD_UNIT,K.URUT_MASUK,ZU.KD_USER,ZU.FULL_NAME 
			FROM TRANSAKSI T INNER JOIN (SELECT DB.KD_KASIR,DB.NO_TRANSAKSI,DB.URUT,DB.TGL_TRANSAKSI,DB.KD_PAY,DB.JUMLAH 
					FROM DETAIL_BAYAR DB 
					WHERE DB.TGL_TRANSAKSI BETWEEN '" . $tgl_awal . "' AND '" . $tgl_akhir . "' AND DB.SHIFT IN ( " . $whereShift . " ) 
					GROUP BY DB.KD_KASIR,DB.NO_TRANSAKSI,DB.URUT,DB.TGL_TRANSAKSI,DB.KD_PAY,DB.JUMLAH ) X ON T.KD_KASIR = X.KD_KASIR 
			AND T.NO_TRANSAKSI = X.NO_TRANSAKSI INNER JOIN PAYMENT PY ON PY.KD_PAY = X.KD_PAY INNER JOIN KUNJUNGAN K ON K.KD_PASIEN= T.KD_PASIEN 
			AND K.KD_UNIT= T.KD_UNIT AND T.URUT_MASUK= K.URUT_MASUK AND K.TGL_MASUK= T.TGL_TRANSAKSI INNER JOIN UNIT U ON K.KD_UNIT = U.KD_UNIT
			INNER JOIN PASIEN PS ON PS.KD_PASIEN = K.KD_PASIEN LEFT OUTER JOIN NOTA_BILL NB ON NB.KD_KASIR = T.KD_KASIR AND NB.NO_TRANSAKSI = T.NO_TRANSAKSI
			LEFT OUTER JOIN ZUSERS ZU ON ZU.KD_USER = NB.KD_USER 
			WHERE T.ISPAY = 1 " . $whereUnit1 . " " . $whereUnit2 . "
			GROUP BY PS.KD_PASIEN,PS.NAMA,T.NO_TRANSAKSI,T.ISPAY,X.KD_PAY,U.NAMA_UNIT,NB.NO_NOTA,K.TGL_MASUK,K.KD_UNIT,K.URUT_MASUK,
			ZU.KD_USER,ZU.FULL_NAME,NB.JUMLAH ) A 
			GROUP BY KD_PASIEN,NAMA,NO_TRANSAKSI,NO_NOTA,NAMA_UNIT,KET,TGL_MASUK,KD_UNIT,URUT_MASUK,KD_USER,FULL_NAME 
			ORDER BY NO_NOTA,NO_TRANSAKSI";

		$query_result = $this->db->query($query);
		if ($query_result->num_rows() > 0) {
			$html = "";
			$html .= "<table border='0' cellspacing='0' width='100%'>";
			$html .= "<thead>";
			$html .= "<tr>";
			$html .= "<th align='center' colspan=8>" . $title . "</th>";
			$html .= "</tr>";

			$html .= "<tr>";
			$html .= "<th align='center' colspan=8>" . $tgl_awal . " s/d " . $tgl_akhir . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='center' colspan=8>KASIR : " . $kasir . "</th>";
			$html .= "</tr>";
			$html .= "<tr>";
			$html .= "<th align='center' colspan=8>SHIFT : " . $whereShift . "</th>";
			$html .= "</tr>";
			$html .= "</thead>";
			$html .= "</table>";

			$html .= "<table border='0' cellspacing='0' width='100%'>";
			$html .= "<thead>";
			$html .= "<tr>";
			$html .= "<th width='5' style='border-top: 1px solid black;'>NO</th>";
			$html .= "<th width='15' style='border-top: 1px solid black;'>NO MEDREC</th>";
			$html .= "<th width='30' style='border-top: 1px solid black;'>NAMA PASIEN</th>";
			$html .= "<th width='5' style='border-top: 1px solid black;'>NO BILL</th>";
			$html .= "<th width='5' style='border-top: 1px solid black;'>TUNAI</th>";
			$html .= "<th width='5' style='border-top: 1px solid black;'>SUBSIDI RS</th>";
			$html .= "<th width='5' style='border-top: 1px solid black;'>PIUTANG</th>";
			$html .= "<th width='30' style='border-top: 1px solid black;'>KETERANGAN</th>";
			$html .= "</tr>";
			$html .= "</thead>";
			$html .= "<tbody>";
			$no = 1;
			$totalTunai = 0;
			$totalSubsidi = 0;
			$totalPiutang = 0;
			foreach ($query_result->result() as $result) {
				$html .= "<tr>";
				$html .= "<td>" . $no . "</td>";
				$html .= "<td>" . $result->KD_PASIEN . "</td>";
				$html .= "<td>" . $result->NAMA . "</td>";
				$html .= "<td>" . $result->NO_NOTA . "</td>";
				$html .= "<td align='right'>" . number_format($result->TUNAI, 0, ",", ",") . "</td>";
				$html .= "<td align='right'>" . number_format($result->SUBSIDI, 0, ",", ",") . "</td>";
				$html .= "<td align='right'>" . number_format($result->PIUTANG, 0, ",", ",") . "</td>";
				$html .= "<td>" . $result->KET . "</td>";
				$html .= "</tr>";
				$no++;
				$totalTunai += $result->TUNAI;
				$totalSubsidi += $result->SUBSIDI;
				$totalPiutang += $result->PIUTANG;
			}
			$html .= "<tr>";
			$html .= "<td colspan=4 align='right' style='border-top: 1px solid black;border-bottom: 1px solid black;'><b>TOTAL</b></td>";
			$html .= "<td align='right' style='border-top: 1px solid black;border-bottom: 1px solid black;'><b>" . number_format($totalTunai, 0, ",", ",") . "</b></td>";
			$html .= "<td align='right' style='border-top: 1px solid black;border-bottom: 1px solid black;'><b>" . number_format($totalSubsidi, 0, ",", ",") . "</b></td>";
			$html .= "<td align='right' style='border-top: 1px solid black;border-bottom: 1px solid black;'><b>" . number_format($totalPiutang, 0, ",", ",") . "</b></td>";
			$html .= "<td style='border-top: 1px solid black;border-bottom: 1px solid black;'>&nbsp;</td>";
			$html .= "</tr>";
			$html .= "</tbody>";
			$html .= "</table>";
			// $html .= '<script type="text/javascript">
			// 				window.print();
			// 			</script>';

			// echo $html;
			if ($param->type_file == false) {
				$this->common->setPdfBuktiSetor('P', $title, $html);
			} else {
				$name = 'Laporan_Bukti_Setor.xls';
				header("Content-Type: application/vnd.ms-excel");
				header("Expires: 0");
				header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
				header("Content-disposition: attschment; filename=" . $name);
				echo $html;
			}
		} else {
			echo "<h3>Data tidak ada</h3>";
		}
	}
}
