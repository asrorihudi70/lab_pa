<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_laporan_PTP extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

    }
	
	function getUnitGridJasaDokter(){
		$kd_bagian = $this->input->post('kd_bagian');
		$kduser    = $this->session->userdata['user_id']['id'];
		$kd_unit   = $this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$explode_kd_unit = explode(',',$kd_unit);
		
		if (isset($kd_bagian)) {
			$criteria_bagian = " AND kd_bagian = '".$kd_bagian."'";
		}else{
			$criteria_bagian = " ";
		}

		$arr_unit= array();
		$arr_unit[0]['KD_UNIT']='000';
		$arr_unit[0]['NAMA_UNIT']='SEMUA';
		$index = 1;
		for($i=1; $i<=count($explode_kd_unit) ;$i++){
			$kd_unit_b = $explode_kd_unit[$i-1];
			$unit= $this->db->query("select nama_unit from unit where kd_unit=$kd_unit_b ".$criteria_bagian);
			if ($unit->num_rows() > 0) {
				$arr_unit[$index]['KD_UNIT'] = $explode_kd_unit[$i-1];
				foreach( $unit->result() as $line){
					$kd_unit_c = $line->nama_unit;
				}
				$arr_unit[$index]['NAMA_UNIT'] = $kd_unit_c;
				$index++;
			}
		}
		
		echo '{success:true, totalrecords:'.count($arr_unit).', listData:'.json_encode($arr_unit).'}';
	}
	function getUnit(){
		$kd_bagian = $this->input->post('kd_bagian');
		$kduser    = $this->session->userdata['user_id']['id'];
		$kd_unit   = $this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$explode_kd_unit = explode(',',$kd_unit);
		
		if (isset($kd_bagian)) {
			$criteria_bagian = " AND kd_bagian = '".$kd_bagian."'";
		}else{
			$criteria_bagian = " ";
		}

		$arr_unit= array();
		$arr_unit[0]['kd_unit']='000';
		$arr_unit[0]['nama_unit']='SEMUA';
		$index = 1;
		for($i=1; $i<=count($explode_kd_unit) ;$i++){
			$kd_unit_b = $explode_kd_unit[$i-1];
			$unit= $this->db->query("select nama_unit from unit where kd_unit=$kd_unit_b ".$criteria_bagian);
			if ($unit->num_rows() > 0) {
				$arr_unit[$index]['kd_unit'] = $explode_kd_unit[$i-1];
				foreach( $unit->result() as $line){
					$kd_unit_c = $line->nama_unit;
				}
				$arr_unit[$index]['nama_unit'] = $kd_unit_c;
				$index++;
			}
		}
		
		echo '{success:true, totalrecords:'.count($arr_unit).', listData:'.json_encode($arr_unit).'}';
	}

	function getUser()
    {
        
		$result=$this->db->query("SELECT kd_user, full_name FROM zusers order by full_name")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
   
   function getCustomer()
    {
        $kd_kelpas = $_POST['kelpas'];
		if ($kd_kelpas==-1){
			$data=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer order by c.customer")->result();
			$result=array();
			$result[0]['kd_customer'] = '00';
			$result[0]['customer'] = 'SEMUA';
			$i=1;
			foreach ($data as $line){
				$result[$i]['kd_customer'] = $line->kd_customer;
				$result[$i]['customer'] = $line->customer;
				$i++;
			}
		}else{
			$result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer where k.jenis_cust= $kd_kelpas order by c.customer")->result();
		}
		
		// $result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer order by c.customer")->result();
		
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	function getCustomerLaporanPenunjang()
    {
        //$kd_kelpas = $_POST['kelpas'];
		//if ($kd_kelpas==-1){
			$data=$this->db->query("select * from customer  order by customer")->result();
			$result=array();
			/* $result[0]['kd_customer'] = '00';
			$result[0]['customer'] = 'SEMUA'; */
			$i=0;
			foreach ($data as $line){
				$result[$i]['kd_customer'] = $line->kd_customer;
				$result[$i]['customer'] = $line->customer;
				$i++;
			}
		/* }else{
			$result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer where k.jenis_cust= $kd_kelpas order by c.customer")->result();
		} */
		
		// $result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer order by c.customer")->result();
		
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	function getPayment(){
		$result=$this->db->query("SELECT kd_pay, uraian FROM payment order by uraian")->result();
		$arr_payment=array();
		$arr_payment[0]['kd_pay'] = '00';
		$arr_payment[0]['uraian'] = 'SEMUA';
		$i=1;
		foreach ($result as $line){
			$arr_payment[$i]['kd_pay'] = $line->kd_pay;
			$arr_payment[$i]['uraian'] = $line->uraian;
			$i++;
		}
		echo '{success:true, totalrecords:'.count($arr_payment).', listData:'.json_encode($arr_payment).'}';
	}
	
	function laporan_detail(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Tunai Perkomponen Detail ';
		$param=json_decode($_POST['data']);
		
		 
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		// $tmpKdUnit="";
		/*$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}

		$tmpKdUnit = substr($tmpKdUnit, 0, -1);*/
		if(strlen($param->tmp_kd_unit) > 0){
			$_tmp_kd_unit 	= substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			if($_tmp_kd_unit == '000'){
				$kduser = $this->session->userdata['user_id']['id'];
				$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
				$kriteria_unit = " And (t.kd_unit in (".$kd_unit.")) ";
				$kriteria_simple_unit= "kd_unit in(".$kd_unit.")";
			}else{
				$kriteria_unit = " And (t.kd_unit in (".$_tmp_kd_unit.")) ";
				$kriteria_simple_unit= "kd_unit in(".$_tmp_kd_unit.")";
			}
			
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (dtc.Kd_Pay in (".$temp_kd_pay.")) ";
			$kriteria_bayar2 = " And (dtbc.Kd_Pay in (".$temp_kd_pay.")) ";
			$kriteria_bayar_new = " And (dtb.Kd_Pay in (".$temp_kd_pay.")) ";
		}else{
			if(strlen($param->tmp_payment) > 0){
				$_tmp_payment 	= substr($param->tmp_payment, 0, strlen($param->tmp_payment)-1);
				if ($_tmp_payment == "'00'"){
					$kd_pay=$this->db->query("select kd_pay from payment ")->result();
					$temp_kd_pay = '';
					foreach($kd_pay as $pay){
						$temp_kd_pay.= "'".$pay->kd_pay."',";
					}
					$temp_kd_pay = substr($temp_kd_pay, 0, -1);
					$kriteria_bayar = " And (dtc.Kd_Pay in (".$temp_kd_pay.")) ";
					$kriteria_bayar2 = " And (dtbc.Kd_Pay in (".$temp_kd_pay.")) ";
					$kriteria_bayar_new = " And (dtb.Kd_Pay in (".$temp_kd_pay.")) ";
				}else{
					
					$kriteria_bayar = " And (dtc.Kd_Pay in (".$_tmp_payment.")) ";
					$kriteria_bayar2 = " And  (dtbc.Kd_Pay in (".$_tmp_payment.")) ";
					$kriteria_bayar_new = " And (dtb.Kd_Pay in (".$_tmp_payment.")) ";
				}
			}
		}
		
		/*Parameter Customer*/
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		
		
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		
		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan';
		}else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran dan Tindakan';
		}
		
		
		/*Parameter Operator*/
		// $kd_user = $param->kd_user;
		$kd_user = '';
		$kriteria_user ='';
		$user='';
		
		
		/*Parameter Transfer*/
		$kriteria_transfer='';
		$transfer_lab =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_lab'")->row()->setting;
		$transfer_rad =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_rad'")->row()->setting;
		$transfer_apt =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_apt'")->row()->setting;
		if($param->transfer0=='true'){ 
			if($param->transfer1=='true'){ 
				if($param->transfer2=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) "; //lab,rad,apt
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";//lab,rad
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab) or p.kd_produk = $transfer_apt ) ";//lab,apt
			}else{
				$kriteria_transfer=" and p.kd_produk in ($transfer_lab)    ";//lab
			}
		}else if($param->transfer1=='true'){  
			if($param->transfer0=='true'){ 
				if($param->transfer1=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk = $transfer_rad' or p.kd_produk = $transfer_apt ) ";
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_rad    ";
			}
		}else if($param->transfer2=='true'){  
			if($param->transfer0=='true'){ 
				if($param->transfer1=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and (  p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_apt    ";
			}
		}
		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
																		INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
																		INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
																		INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
																		INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
											Where dt.kd_Kasir = '01' and dc.kd_component <> '36'   
											$kriteria_unit 
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		$arr_data_pasien = array();
		
		$tampung_data_component = array();
		$arr_tamp_jumlah=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_sub=array(); //menampung seluruh jumlah menjadi matriks
		$arr_tamp_grand=array();
		$arr_tamp_grand_tot=array();
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+4;
		}else{
			$jml_kolom=4;
		}
		//print_r($arr_kd_component);
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$t_tindakan.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>Operator '.$user.' </th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center">Poliklinik</th>
						<th align="center">Nama Pasien</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
		$query_poli=$this->db->query("select kd_unit, nama_unit from unit where ".$kriteria_simple_unit)->result();
		// echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';
		if(count($query_poli) > 0){
			$jmllinearea=count($result)+5;
			$no_unit=1;
			$u=0; //counter unit
			foreach ($query_poli as $line2){
				$kd_unit = $line2->kd_unit;
				$nama_unit= $line2->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.'</td>
							<td >'.$nama_unit.'</td>';
				for($j=0; $j<count($query_kolom)+2;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
					$query_pasien= $this->db->query("SELECT distinct nama,kd_pasien from ( SELECT Ps.Kd_Pasien, Ps.Nama, u.nama_Unit, u.kd_unit
												FROM Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
																INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi
																INNER JOIN detail_bayar db on db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi and db.urut = dt.urut 
																	and db.tgl_transaksi = dt.tgl_transaksi 
																INNER JOIN Detail_TR_Bayar dtb on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi and dtb.urut = db.urut 
																	and dtb.tgl_transaksi = db.tgl_transaksi 
													INNER JOIN Unit u On u.kd_unit=t.kd_unit
													INNER JOIN Produk p on p.kd_produk= dt.kd_produk
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer
													INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												WHERE t.ispay = 't'     
													/* KRITERIA TINDAKAN*/
													$kriteria_tindakan
													/* KRITERIA UNIT*/
													$kriteria_unit
													/* KRITERIA CUSTOMER*/
													$customerx
													/* KRITERIA TRANSFER*/
													$kriteria_transfer
													/* KRITERIA TANGGAL */
													$kriteria_bayar_new
													$q_shiftB
													And t.kd_kasir = '01' 
													and dt.kd_produk <> '417'  
													and t.tgl_transaksi between '$tgl_awal_i' and '$tgl_akhir_i'
												GROUP BY Ps.Kd_Pasien, Ps.Nama, u.nama_unit , u.kd_unit
												ORDER BY Ps.Nama) Y where kd_unit='$kd_unit' order by nama")->result();
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
				$no_pasien=1;
				$p=0; //counter tindakan
				$jum_total = 0;
				foreach($query_pasien as $line3)
				{
					$kd_pasien = $line3->kd_pasien;
					$nama = $line3->nama;
					
					$html.='<tr>
							<td align="center" width="40px"></td>
							<td ></td>
							<td >'.$no_pasien.'. '.$kd_pasien.' '.$nama.'</td>';
					for($j=0; $j<count($query_kolom)+1;$j++){
						$html.='<td></td>';
					}		
					$html.='</tr>';
												$query_pasien_poli= $this->db->query("SELECT distinct deskripsi
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE dtb.kd_kasir='01'  
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													
													$kriteria_tindakan
													
													$kriteria_unit
													
													$customerx
													
													$kriteria_transfer
													
													and dt.kd_produk <> '417'  
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien'  order by deskripsi ")->result(); 
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
					foreach ($query_pasien_poli as $line4){
						$deskripsi = $line4->deskripsi;
						$html.='<tr>
								<td align="center" width="40px"></td>
								<td ></td>
								<td > - '.$deskripsi.'</td>';
												$query_pasien_poli_des= $this->db->query("SELECT *
													from ( SELECT P .Deskripsi,
															u.kd_unit,
															u.nama_Unit,
															Ps.Kd_Pasien,x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH
													From (SELECT dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dtbc.kd_component, sum(dtbc.Jumlah) 
													as jumlah, Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL 
													FROM DETAIL_TR_BAYAR dtb 
													Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR 
													Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT AND dtb.TGL_BAYAR = db.Tgl_Transaksi 
													inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
													INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component 
												WHERE dtb.kd_kasir='01'  
													And (dtb.TGL_BAYAR between '$tgl_awal_i' and '$tgl_akhir_i')
													$kriteria_bayar2
													and dtb.jumlah <> 0
													GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component ) 
												x  
												INNER JOIN Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi 
												INNER JOIN Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI AND dt.Urut = x.Urut And dt.Tgl_Transaksi = x.Tgl_Transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Pasien ps ON ps.kd_pasien = T .Kd_pasien
												WHERE (p.kd_klas is not null)    
													
													$kriteria_tindakan
													
													$kriteria_unit
													
													$customerx
													
													$kriteria_transfer
													
													and dt.kd_produk <> '417'  
												GROUP BY P .Deskripsi,
														u.kd_unit,
														u.nama_Unit,
														Ps.Kd_Pasien,x.kd_component
											ORDER BY U.Nama_Unit )Y where nama_unit='$nama_unit' and kd_pasien='$kd_pasien' and deskripsi='$deskripsi' order by kd_component ")->result(); 
							$arr_data_component = array();		
							$k=0;
							foreach($query_pasien_poli_des as $line5){
								$arr_data_component[$k] ['kd_component']= $line5->kd_component;
								$arr_data_component[$k] ['jumlah']= $line5->jumlah;
								$k++;
							}
							
							//mengisi kolom nilai komponen
							for($l=0; $l<count($arr_kd_component); $l++){
								$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
								if($cari_kd_component != null){									
									$tampung_data_component[$l] = $cari_kd_component;
								}else{
									$tampung_data_component[$l] =0;
								}
							}
							//hitung nilai komponen per tindakan
							$total_component=array_sum($tampung_data_component);
							for($j=0; $j<count($arr_kd_component);$j++){
								$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
								$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
							}
							
						$html.='<td align="right">'.number_format($total_component,0, "." , ",").'</td></tr>';
						$p++;
						$jum_total = $jum_total + $total_component;
					}
					$no_pasien++;
					$jmllinearea = $jmllinearea+1;	
				}
				
				if($p == 1){
					//transpos array
					
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_total;
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				 }else{
					//transpos array
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="3" > Sub Total</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_total;
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				} 
				$jmllinearea = $jmllinearea+1;	
				$no_unit++;
				$u++;
			}
			
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component) ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom);$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component) ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="3" > GRAND TOTAL</td>';
				for($j=0; $j<=count($query_kolom);$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
				$jmllinearea = $jmllinearea+1;	
			}
			
		}else{
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$jml_kolom.' align="center">Data tidak ada</th>
				</tr>';		
				$jmllinearea=count($result)+7;
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($param->type_file == 1)/* {
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:K'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E:F')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('laporan_tunai_perkomponen_d'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_tunai_perkomponen_detail.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		} */
		{
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Lap_Tunai_PerKomponen_Detail_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{
			$this->common->setPdf('L','Laporan Tunai Perkomponen Detail',$html);	
		}
		
	} 
	
	function laporan_summary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Tunai Perkomponen Summary ';
		$param=json_decode($_POST['data']);
		
		 
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		if($tmpKdUnit == '000'){
			$kduser = $this->session->userdata['user_id']['id'];
			$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$kriteria_unit = " And (t.kd_unit in (".$kd_unit.")) ";
		}else{
			$kriteria_unit = " And (t.kd_unit in (".$tmpKdUnit.")) ";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (dtb.Kd_Pay in (".$temp_kd_pay.")) ";
		}else{
			$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
		}
		
		/*Parameter Customer*/
		$arrayDataCustomer = $param->tmp_kd_customer;
		if($arrayDataCustomer == ''){
			$customerx=" ";
			//$customer='Semua ';
		}else{
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}
		
		
		//$kduser = $this->session->userdata['user_id']['id'];
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			 $q_shift=" ((tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (1,2,3))		
			Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}
		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		// if($param->tindakan0=='true' ){ /*Pendaftaran*/
			// if($param->tindakan1=='true'){/*Pendaftaran & tindakan RWJ*/
				 // $kriteria_tindakan =" and (( p.kd_klas = '1' ) or ( p.kd_klas <> '1' and p.kd_klas <> '9')) ";
				 // $t_tindakan='Laporan Penerimaan Pendaftaran dan Tindakan Rawat Jalan';
			// }else{
				 // $kriteria_tindakan =  "and ( p.kd_klas = '1' ) ";
				 // $t_tindakan='Laporan Penerimaan Pendaftaran';
			// }
		// }else if($param->tindakan1=='true'){  /*Tindakan RWJ*/
			 // $kriteria_tindakan =" and ( p.kd_klas <> '1' and p.kd_klas <> '9') ";
			 // $t_tindakan='Laporan Penerimaan Tindakan Rawat Jalan';
		// }else{
			// $kriteria_tindakan='';
			 // $t_tindakan='';
		// }
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan';
		}else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran dan Tindakan';
		}
		/*Parameter Operator*/
		$kd_user = $param->kd_user;
		$kriteria_user ='';
		$user='';
		if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}
		
		/*Parameter Transfer*/
		$kriteria_transfer='';
		$transfer_lab =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_lab'")->row()->setting;
		$transfer_rad =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_rad'")->row()->setting;
		$transfer_apt =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_apt'")->row()->setting;
		if($param->transfer0=='true'){ /*Lab*/
			if($param->transfer1=='true'){ /*Rad*/
				if($param->transfer2=='true'){ /*Apt*/
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) "; //lab,rad,apt
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";//lab,rad
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab) or p.kd_produk = $transfer_apt ) ";//lab,apt
			}else{
				$kriteria_transfer=" and p.kd_produk in ($transfer_lab)    ";//lab
			}
		}else if($param->transfer1=='true'){  /*Rad*/
			if($param->transfer0=='true'){ /*Lab*/
				if($param->transfer1=='true'){ /*Apt*/
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk = $transfer_rad' or p.kd_produk = $transfer_apt ) ";
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_rad    ";
			}
		}else if($param->transfer2=='true'){  /*Apotek*/
			if($param->transfer0=='true'){ /*Lab*/
				if($param->transfer1=='true'){ /*Rad*/
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and (  p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_apt    ";
			}
		}
		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
											Where dt.kd_Kasir = '01' and dc.kd_component <> '36'   
											$kriteria_unit And $q_shiftB
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
		}
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$t_tindakan.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>Operator '.$user.' </th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center">Poliklinik/Tindakan</th>
						<th align="center">Jumlah Pasien</th>
						<th align="center">Jumlah Produk</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Jasa Pelayanan</th><th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
				
		
		$query_poli = $this->db->query(" SELECT distinct nama_unit from
													( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
														 sum(x.jPL)as jPL
													From 
														(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
															dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
															 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
														 FROM DETAIL_TR_BAYAR dtb
															 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
															 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
															 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
														 WHERE    
															dtb.kd_kasir='01'  And										--> kd_kasir RWJ
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
															--kriteria pembayaran
															$kriteria_bayar
															and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
														 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
														 ) x
														 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
														 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
														 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
														 INNER JOIN Unit u On u.kd_unit=t.kd_unit
														 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
														 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
													 where 
														(p.kd_klas is not null)   
														$kriteria_unit 
														$kriteria_tindakan
														$kriteria_transfer
														And t.IsPay = 't'
														and dt.kd_produk <> '417'  
													GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
													ORDER BY U.Nama_Unit
													)Y 
												 ")->result();
		//echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';
		if(count($query_poli)>0){
			$no_unit = 1;
			$u=0;
			$jum_produk=0;
			foreach($query_poli as $line){
				$nama_unit = $line->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.' </td>
							<td  >'.$nama_unit.'</td>';
				for($j=0; $j<=count($query_kolom)+3;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
				$query_tindakan = $this->db->query(" SELECT distinct deskripsi,jml_pasien,qty from
													( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
														 sum(x.jPL)as jPL
													From 
														(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
															dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
															 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
														 FROM DETAIL_TR_BAYAR dtb
															 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
															 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
															 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
														 WHERE    
															dtb.kd_kasir='01'  And										--> kd_kasir RWJ
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
															--kriteria pembayaran
															$kriteria_bayar
															and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
														 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
														 ) x
														 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
														 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
														 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
														 INNER JOIN Unit u On u.kd_unit=t.kd_unit
														 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
														 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
													 where 
														(p.kd_klas is not null)   
														$kriteria_unit 
														$kriteria_tindakan
														$kriteria_transfer
														And t.IsPay = 't'
														and dt.kd_produk <> '417'  
													GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
													ORDER BY U.Nama_Unit
													)Y where nama_unit='$nama_unit' order by Deskripsi
												 ")->result();
				//echo '{success:true, totalrecords:'.count($query_tindakan).', listData:'.json_encode($query_tindakan).'}';
				$p=0; //counter tindakan
				$jumlah_produk=0;
				$jum_total = 0;
				$jum_jpl = 0;
				$da=0;
				foreach($query_tindakan as $line2){
					$deskripsi = $line2->deskripsi;
					$jml_pasien = $line2->jml_pasien;
					$qty = $line2->qty;
					$html.='<tr>
								<td ></td>
								<td  > - '.$deskripsi.'</td>
								<td  align="right">'.$jml_pasien.'</td>
								<td  align="right">'.$qty.'</td>';
					$query_tindakan_poli = $this->db->query(" SELECT * from
																( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
																	 sum(x.jPL)as jPL
																From 
																	(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
																		dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
																		 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
																	 FROM DETAIL_TR_BAYAR dtb
																		 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
																		 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
																		 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
																	 WHERE    
																		dtb.kd_kasir='01'  And										--> kd_kasir RWJ
																		(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
																		--kriteria pembayaran
																		$kriteria_bayar
																		and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
																	 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
																	 ) x
																	 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
																	 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
																	 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
																	 INNER JOIN Unit u On u.kd_unit=t.kd_unit
																	 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
																	 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
																 where 
																	(p.kd_klas is not null)   
																	$kriteria_unit 
																	$kriteria_tindakan
																	$kriteria_transfer
																	And t.IsPay = 't'
																	and dt.kd_produk <> '417'  
																GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
																ORDER BY U.Nama_Unit
																)Y where nama_unit='$nama_unit' and deskripsi='$deskripsi' order by kd_component 
												 ")->result();
					//echo '{success:true, totalrecords:'.count($query_tindakan_poli).', listData:'.json_encode($query_tindakan_poli).'}';	
					$k=0;
					$jumlah_jpl=0;
					$arr_data_component = array(); //arr tampung data komponent
					foreach($query_tindakan_poli as $line3){
						//var_dump( $line3->jumlah);
						//unset($arr_data_component);
						$arr_data_component[$k] ['kd_component']= $line3->kd_component;
						$arr_data_component[$k] ['jumlah']= $line3->jumlah;
						$jumlah_jpl=$jumlah_jpl+$line3->jpl;
						$k++;
					}
					
					//mengisi kolom nilai komponen
					for($l=0; $l<count($arr_kd_component); $l++){
						$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
						//var_dump( $cari_kd_component);
						if($cari_kd_component != null){									
							$tampung_data_component[$l] = $cari_kd_component;
						}else{
							$tampung_data_component[$l] =0;
						}
					}
					//hitung nilai komponen per tindakan
					$total_component=array_sum($tampung_data_component);
					for($j=0; $j<count($arr_kd_component);$j++){
						$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
						$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
					}
					$html.='<td align="right">'.number_format($jumlah_jpl,0, "." , ",").'</td>';
					$total_semua = $total_component;
					$html.='<td align="right">'.number_format($total_semua,0, "." , ",").'</td></tr>';
					$html.='</tr>';
				
					$p++;
					$jumlah_produk = $jumlah_produk + $qty;
					$jum_total = $jum_total + $total_semua;
					$jum_jpl = $jum_jpl +$jumlah_jpl; 
					$da++;
					
				}
				$jum_produk = $jum_produk +$jumlah_produk;
				if($p == 1){
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="2" > Sub Total</td>';
					$html.='<td></td><td align="right">'.$jumlah_produk.'</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				}else{
					//transpos array
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="2" > Sub Total</td>';
					$html.='<td></td><td align="right">'.$jumlah_produk.'</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				}
				$no_unit++;
				$u++;
			}
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="2" > GRAND TOTAL</td>';
				$html.='<td></td><td align="right">'.$jum_produk.'</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="2" > GRAND TOTAL</td>';
				$html.='<td></td><td align="right">'.$jum_produk.'</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
			}
		}else{
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$jml_kolom.' align="center">Data tidak ada</th>
				</tr>';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($param->type_file == 1){
			$name='Laporan_Tunai_Perkomponen_Summary.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
			
		}else{
			$this->common->setPdf('L','Laporan Tunai Perkomponen Summary',$html);	
			//echo $html;
		}
		
	}
	//fungsi mencari elemen di array multidimensi
	function searchForId($id, $array) {
	   foreach ($array as $key => $val) {
		   if ($val['kd_component'] === $id) {
			   return $val['jumlah'];
		   }
	   }
	   return null;
	}
}