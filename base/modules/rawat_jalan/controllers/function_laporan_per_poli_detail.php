<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_laporan_per_poli_detail extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

    }
	

	
	function cetak(){
		$param=json_decode($_POST['data']);
   		$title='Laporan Transaksi Per Poli Detail';
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		if($tmpKdUnit == '000'){
			$kduser = $this->session->userdata['user_id']['id'];
			$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$kriteria_unit = " And (t.kd_unit in (".$kd_unit.")) ";
		}else{
			$kriteria_unit = " And (t.kd_unit in (".$tmpKdUnit.")) ";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (db.Kd_Pay in (".$temp_kd_pay.")) ";
		}else{
			$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
		}
		
		/*Parameter kelompok pasien*/
		
		$kel_pas = $param->pasien;
		if($kel_pas == -1 ){
			$kriteria_kelpas='';
			$customerx='';
			$t_kelpas='Semua';
			$t_customer='Semua';
			// echo "masuks";
		}else{
			// echo "masuk";
			$kriteria_kelpas=" and Ktr.jenis_cust in ('".$kel_pas."') ";
			if($kel_pas == 0){
				$t_kelpas='Perseorangan';
			}else if($kel_pas == 1){
				$t_kelpas='Perusahaan';
			}else{
				$t_kelpas='Asuransi';
			}
			
			/*Parameter Customer*/
			$arrayDataCustomer = $param->tmp_kd_customer;
		
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$semua="'0'";
			if($tmpKdCustomer == $semua){
				$customerx='';
				$t_customer='Semua';
			}else{
				$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
				$customer=$this->db->query("Select * From customer Where Kd_customer in ($tmpKdCustomer)")->result();
				$t_customer='';
				foreach ($customer as $line){
					$t_customer=$line->customer.", ".$t_customer;
				}
				$t_customer = substr($t_customer, 0, -2);
			
			}
		}
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			 $q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}
		
		
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		if($param->tindakan0=='true' ){ /*Pendaftaran*/
			if($param->tindakan1=='true'){/*Pendaftaran & tindakan RWJ*/
				 $kriteria_tindakan =" and (( pr.kd_klas = '1' ) or ( pr.kd_klas <> '1' and pr.kd_klas <> '9')) ";
				 $t_tindakan='Pendaftaran dan Tindakan Rawat Jalan';
			}else{
				 $kriteria_tindakan =  "and ( pr.kd_klas = '1' ) ";
				 $t_tindakan='Pendaftaran';
			}
		}else if($param->tindakan1=='true'){  /*Tindakan RWJ*/
			 $kriteria_tindakan =" and ( pr.kd_klas <> '1' and pr.kd_klas <> '9') ";
			 $t_tindakan='Tindakan Rawat Jalan';
		}else{
			$kriteria_tindakan='';
			 $t_tindakan='';
		}
		//echo $kriteria_tindakan;
		/*Parameter Operator*/
		$kd_user = $param->kd_user;
		$kriteria_user ='';
		$user='';
		if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and dt.kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}
		
		$query = $this->db->query("SELECT U.Nama_Unit, Count(k.Kd_Pasien) as Jml_Pasien,  coalesce(sum(x.Jumlah),null) as Jumlah  ,
										Sum(c20) as c20,Sum(c21) as c21,Sum(c22) as c22,Sum(c23) as c23, Sum(c24) as c24, Sum(c25) as c25,
										Sum(c26) as c26,Sum(c29) as c29,Sum(c30) as c30,Sum(c31) as c31, Sum(c37) as c37,Sum(c41) as c41 
											From Kunjungan k INNER JOIN Transaksi t  On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit  And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk 
												INNER JOIN  (Select dt.kd_kasir, dt.No_transaksi,  Sum(dt.Qty * c20) as c20, Sum(dt.Qty * c21) as c21, Sum(dt.Qty * c22) as c22, 
															Sum(dt.Qty * c23) as c23, Sum(dt.Qty * c24) as c24, Sum(dt.Qty * c25) as c25, Sum(dt.Qty * c26) as c26, Sum(dt.Qty * c29) as c29,
															Sum(dt.Qty * c30) as c30, Sum(dt.Qty * c31) as c31, Sum(dt.Qty * c37) as c37, Sum(dt.Qty * c41) as c41, 
															Sum(dt.qty*dt.Harga) as Jumlah 
														From detail_transaksi dt INNER JOIN  
															(Select kd_kasir, No_Transaksi, Tgl_transaksi,  
																Sum(c20) as c20,Sum(c21) as c21, Sum(c22) as c22, Sum(c23) as c23, Sum(c24) as c24, Sum(c25) as c25, 
																Sum(c26) as c26, Sum(c29) as c29,  Sum(c30) as c30, Sum(c31) as c31,  Sum(c37) as c37,  Sum(c41) as c41, Urut 
																From  (Select kd_kasir, No_Transaksi, Tgl_transaksi,  
																	Case When Kd_Component=20 Then Tarif Else 0 End as c20, Case When Kd_Component=21 Then Tarif Else 0 End as c21, 
																	Case When Kd_Component=22 Then Tarif Else 0 End as c22, Case When Kd_Component=23 Then Tarif Else 0 End as c23, 
																	Case When Kd_Component=24 Then Tarif Else 0 End as c24, Case When Kd_Component=25 Then Tarif Else 0 End as c25, 
																	Case When Kd_Component=26 Then Tarif Else 0 End as c26, Case When Kd_Component=29 Then Tarif Else 0 End as c29, 
																	Case When Kd_Component=30 Then Tarif Else 0 End as c30, Case When Kd_Component=31 Then Tarif Else 0 End as c31, 
																	Case When Kd_Component=37 Then Tarif Else 0 End as c37, Case When Kd_Component=41 Then Tarif Else 0 End as c41,  Urut  
																From Detail_Component    
																	Where kd_kasir ='01'  ) x 
															Group by kd_kasir, No_Transaksi, Tgl_transaksi, Urut) dc  ON dc.kd_Kasir=dt.kd_kasir And dc.No_Transaksi=dt.no_Transaksi   
																and dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.urut=dt.urut  
															inner join produk pr on dt.kd_produk = pr.kd_produk
															left join DETAIL_BAYAR db  on db.KD_KASIR= dt.KD_KASIR  and db.NO_TRANSAKSI = dt.NO_TRANSAKSI 
																Where 	dt.kd_kasir ='01'
																		And  $q_shift
																		$kriteria_tindakan
																		$kriteria_user
																		$kriteria_bayar
																		And dt.Folio in ('A','E') 
														Group by dt.kd_kasir, dt.No_transaksi) x  ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi 
														INNER JOIN Unit u On u.kd_unit=t.kd_unit 
														LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
															Where t.ispay='t'   
																$kriteria_unit
																$kriteria_kelpas
																$customerx
											 Group By u.Nama_unit Order by Nama_Unit")->result();
		
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			 $html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
						<th colspan="16" style='.$font_style.'>'.$title.' </th>
					</tr>';
			if($param->tindakan0 =='true' || $param->tindakan1 == 'true'){
			$html.='
						<tr>
							<th colspan="16" style='.$font_style.'>'.$t_tindakan.'</th>
						</tr>';
			}
			$html.='
						<tr>
							<th colspan="16" >'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="16"> Kelompok Pasien: '.$t_kelpas.' ('.$t_customer.')</th>
						</tr>
						<tr>
							<th colspan="16">Operator '.$user.' </th>
						</tr>
				</table>';
			  
			$baris=1;
			//---------------ISI-----------------------------------------------------------------------------------
			$html.="
				<table border='1' cellpadding='1'>
				<tr>
						<th align='center' width='20'>No</th>
						<th align='center' width='80'>Poliklinik</th>
						<th align='center' width='60'>Jumlah Pasien</th>
						<th align='center' width='60'>Jasa Dokter</th>
						<th align='center' width='60'>Jasa Perawat/Bid</th>
						<th align='center' width='60'>Indeks Tdk Langsung</th>
						<th align='center' width='60'>Ops. Instalasi</th>
						<th align='center' width='60'>Ops. RS</th>
						<th align='center' width='60'>Ops. Direksi</th>
						<th align='center' width='60'>BHP</th>
						<th align='center' width='60'>Jasa Dokter Anesthesi</th>
						<th align='center' width='60'>Jasa Sarana</th>
						<th align='center' width='60'>Jasa Perawat</th>
						<th align='center' width='60'>Medik</th>
						<th align='center' width='60'>Administrasi</th>
						<th align='center' width='60'>Total</th>
					</tr>
				";
			$no=0;
			$tot_c20 = 0;
			$tot_c21 = 0;
			$tot_c22 = 0;
			$tot_c23 = 0;
			$tot_c24 = 0;
			$tot_c25 = 0;
			$tot_c26 = 0;
			$tot_c29 = 0;
			$tot_c30 = 0;
			$tot_c31 = 0;
			$tot_c37 = 0;
			$tot_c41 = 0;
			$tot_total =0;
			$tot_pasien=0;
			foreach ($query as $line)
			{
				$no++;
				$nama_unit = str_replace('&','DAN', $line->nama_unit);
				$total = $line->c20 + $line->c21 + $line->c22 + $line->c23 + $line->c24 + $line->c25 + $line->c26 + $line->c29 + $line->c30 + $line->c31 + $line->c37 + $line->c41;
				$tot_total = $tot_total +$total;
				$tot_pasien  =  $tot_pasien + $line->jml_pasien;
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.$nama_unit.'</td>
							<td align="right">'.$line->jml_pasien.'</td>
							<td  align="right">'.number_format($line->c20,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c21,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c22,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c23,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c24,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c25,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c26,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c29,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c30,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c31,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c37,0, "," , ",").'</td>
							<td  align="right">'.number_format($line->c41,0, "," , ",").'</td>
							<td  align="right">'.number_format($total,0, "," , ",").'</td>
						</tr>';
				$tot_c20 = $tot_c20 + $line->c20;
				$tot_c21 = $tot_c21 + $line->c21;
				$tot_c22 = $tot_c22 + $line->c22;
				$tot_c23 = $tot_c23 + $line->c23;
				$tot_c24 = $tot_c24 + $line->c24;
				$tot_c25 = $tot_c25 + $line->c25;
				$tot_c26 = $tot_c26 + $line->c26;
				$tot_c29 = $tot_c29 + $line->c29;
				$tot_c30 = $tot_c30 + $line->c30;
				$tot_c31 = $tot_c31 + $line->c31;
				$tot_c37 = $tot_c37 + $line->c37;
				$tot_c41 = $tot_c41 + $line->c41;
				$baris++;
			}
			$html.='<tr>
						<td></td>
						<td><b>Grand Total</b></td>
						<td align="right">'.number_format($tot_pasien,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c20,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c21,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c22,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c23,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c24,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c25,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c26,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c29,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c30,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c31,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c37,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_c41,0, "," , ",").'</td>
						<td  align="right">'.number_format($tot_total,0, "," , ",").'</td>
					</tr>'; 
		$baris= $baris+7;
		$print_area='A1:P'.$baris;
		$area_kanan='C7:P'.$baris;
		$area_='A7:P'.$baris;
		$area_wrap = 'A7:P'.$baris;
		$html.='</table>';
		$prop=array('foot'=>true);
		if($param->type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
				
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'size'  => 11,
						//'name'  => 'Courier New'
					) //*/
			 	)
			);

			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Courier New'
				));
			/* $styleArrayBodyHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				)); */
			/* $objPHPExcel->getActiveSheet()->getStyle('A1:P6')->applyFromArray($styleArrayHead);
			$objPHPExcel->getActiveSheet()->getStyle('A7:P7')->applyFromArray($styleArrayBodyHead); */
			/*$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:P5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:P6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			// # Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			// # END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(13);
			#FUNGSI LANDSCAPE
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Transaksi_Per_Poli_Detail'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Transaksi_Per_Poli_Detail.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('L','Laporan Transaksi Per Poli Detail',$html);	
			// echo $html;
		}  
		
	}

	
}