<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_laporan_per_poli_per_produk extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			  $this->load->library('result');
             $this->load->library('common');
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

    }
	
	function getUnit(){
 		$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$explode_kd_unit = explode(',',$kd_unit);
		$arr_unit= array();
		if(count($explode_kd_unit)==1){
			$unit= $this->db->query("select nama_unit from unit where kd_unit=$kd_unit ")->result();
			foreach( $unit as $line){
				$kd_unit_c = $line->nama_unit;
			}
			$arr_unit[0]['kd_unit'] = $kd_unit;
			$arr_unit[0]['nama_unit'] = $kd_unit_c;
		}else{
			$index = 0;
			for($i=1; $i<=count($explode_kd_unit) ;$i++){
				if (substr($explode_kd_unit[$i-1],1,1) == '2') {
					$arr_unit[$index]['kd_unit'] = $explode_kd_unit[$i-1];
					$kd_unit_b = $explode_kd_unit[$i-1];
					$unit= $this->db->query(" SELECT nama_unit, kd_unit from unit where kd_unit=$kd_unit_b")->result();
					foreach( $unit as $line){			
						$kd_unit_c = $line->nama_unit;
					}	
					$arr_unit[$index]['nama_unit'] 	= $kd_unit_c;
					$index++;
				}
			}
		}
	//	var_dump($arr_unit); die();
		/*for($i=1; $i<=count($explode_kd_unit) ;$i++){
			if (substr($tmp_kd_unit[$i], 0, 1) != '2') {
				unset($arr_unit[$i]);
			}
		}*/
		
		echo '{success:true, totalrecords:'.count($arr_unit).', listData:'.json_encode($arr_unit).'}';
	}
	function getUser()
    {
        
		$result=$this->db->query("SELECT kd_user, full_name FROM zusers order by full_name")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
   
   function getCustomer()
    {
        $kd_kelpas = $_POST['kelpas'];
		$result=$this->db->query("SELECT '0' as kd_customer, 'Semua' as customer UNION ALL SELECT c.kd_customer,c.customer from customer c inner join kontraktor k on k.kd_customer=c.kd_customer  
									where k.jenis_cust= $kd_kelpas
									order by kd_customer")->result();
		// $result=$this->db->query("select * from customer c inner join kontraktor k on k.kd_customer=c.kd_customer order by c.customer")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	function getPayment(){
		$result=$this->db->query("SELECT kd_pay, uraian FROM payment order by uraian")->result();
		$arr_payment=array();
		
		$i=1;
		foreach ($result as $line){
			$arr_payment[0]['kd_pay'] = '00';
			$arr_payment[0]['uraian'] = 'SEMUA';	
			$arr_payment[$i]['kd_pay'] = $line->kd_pay;
			$arr_payment[$i]['uraian'] = $line->uraian;
			$i++;
		}
		echo '{success:true, totalrecords:'.count($arr_payment).', listData:'.json_encode($arr_payment).'}';
	}
	
	
	function cetak(){
		$param=json_decode($_POST['data']);
   		$title='Laporan Transaksi Per Poli Per Produk';
		
		$tgl_awal_i      = str_replace("/","-", $param->start_date);
		$tgl_akhir_i     = str_replace("/","-",$param->last_date) ;
		$tindakan_apotek = $param->tindakan2;
		$tgl_awal        = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir       = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
   		$criteriaTindakanApotek = "";
   		if ($tindakan_apotek == false) {
   			$criteriaTindakanApotek = " AND kd_produk <> '6440' ";
   		}
		/*Parameter Unit*/
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit 	= substr($tmpKdUnit, 0, -1);
		$posisi 	= strpos($tmpKdUnit, '000');
		if($tmpKdUnit == '000' || $posisi !== false){
			$kduser = $this->session->userdata['user_id']['id'];
			$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$kriteria_unit 	= " And (k.kd_unit in (".$kd_unit."))  --or (x.kd_unit_tr in (".$kd_unit."))";
			$kriteria_unit 	= "";
		}else{
			$kriteria_unit = " And (k.kd_unit in (".$tmpKdUnit.")) --or (x.kd_unit_tr in (".$tmpKdUnit.")) ";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (dt.Kd_Pay in (".$temp_kd_pay.")) ";
		}else{
			$kriteria_bayar = " And (dt.Kd_Pay in (".$tmpKdPay.")) ";
		}
		
		/*Parameter kelompok pasien*/
		
		$kel_pas = $param->pasien;
		if($kel_pas == -1 ){
			$kriteria_kelpas='';
			$customerx='';
			$t_kelpas='Semua';
			$t_customer='Semua';
			// echo "masuks";
		}else{
			// echo "masuk";
			$kriteria_kelpas=" and Ktr.jenis_cust in ('".$kel_pas."') ";
			if($kel_pas == 0){
				$t_kelpas='Perseorangan';
			}else if($kel_pas == 1){
				$t_kelpas='Perusahaan';
			}else{
				$t_kelpas='Asuransi';
			}
			
			/*Parameter Customer*/
			$arrayDataCustomer = $param->tmp_kd_customer;
		
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$semua="'0'";
			if($tmpKdCustomer == $semua){
				$customerx='';
				$t_customer='Semua';
			}else{
				$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
				$customer=$this->db->query("Select * From customer Where Kd_customer in ($tmpKdCustomer)")->result();
				$t_customer='';
				foreach ($customer as $line){
					$t_customer=$line->customer.", ".$t_customer;
				}
				$t_customer = substr($t_customer, 0, -2);
			
			}
		}
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			 $q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}
		
		
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		// if($param->tindakan0=='true' ){ /*Pendaftaran*/
			// if($param->tindakan1=='true'){/*Pendaftaran & tindakan RWJ*/
				 // $kriteria_tindakan =" and (( p.kd_klas = '1' ) or ( p.kd_klas <> '1' and p.kd_klas <> '9')) ";
				 // $t_tindakan='Pendaftaran dan Tindakan Rawat Jalan';
			// }else{
				 // $kriteria_tindakan =  "and ( p.kd_klas = '1' ) ";
				 // $t_tindakan='Pendaftaran';
			// }
		// }else if($param->tindakan1=='true'){  /*Tindakan RWJ*/
			 // $kriteria_tindakan =" and ( p.kd_klas <> '1' and p.kd_klas <> '9') ";
			 // $t_tindakan='Tindakan Rawat Jalan';
		// }else{
			// $kriteria_tindakan='';
			 // $t_tindakan='';
		// }
		
		$transfer_lab =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_lab'")->row()->setting;
		$transfer_rad =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_rad'")->row()->setting;
		$transfer_apt =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_apt'")->row()->setting;
		
		if($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==false){
			$kriteria_tindakan =" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true  && $param->tindakan2==false){
			$kriteria_tindakan =" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan Rawat Jalan';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==false  && $param->tindakan2==true){
			$kriteria_tindakan="and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
			$t_tindakan='Transfer';
		} else if($param->tindakan0 ==true && $param->tindakan1 ==false  && $param->tindakan2==true){
			$kriteria_tindakan =" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
									INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	
									or ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt )) ";
			$t_tindakan='Pendaftran dan Transfer';
		} else if($param->tindakan0 ==false && $param->tindakan1 ==true  && $param->tindakan2==true){
			$kriteria_tindakan ="and (x.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') 
									or ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt )) ";
			$t_tindakan='Tindakan dan Transfer';
		} else if($param->tindakan0 ==true && $param->tindakan1 ==true  && $param->tindakan2==false){
			$kriteria_tindakan="and ( p.kd_produk not in ($transfer_lab,$transfer_rad,$transfer_apt ) )";
			$t_tindakan='Pendaftran dan Tindakan';
		} else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran, Tindakan Rawat Jalan dan Transfer';
		}
		
		//echo $kriteria_tindakan;
		/*Parameter Operator*/
		$kd_user = $param->kd_user;
		$kriteria_user ='';
		$user='';
		if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and dt.kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}
		
		/*$query_head=$this->db->query("select distinct nama_unit from (
										Select U.Nama_Unit, p.deskripsi, Count(k.Kd_Pasien) AS Jml_Pasien, Sum(x.Qty) as Qty, coalesce(sum(x.Jumlah),0) as Jumlah From 
											(
												(
													(Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk) 
													INNER JOIN (
														Select 
														dt.kd_kasir, 
														dt.No_transaksi, 
														dt.kd_produk, 
														dt.kd_unit_tr, 
														Sum(Qty) as Qty, 
														Sum(qty*Harga) as Jumlah 
														From detail_transaksi dt 
														inner join DETAIL_BAYAR db on db.KD_KASIR= dt.KD_KASIR and db.NO_TRANSAKSI = dt.NO_TRANSAKSI 
														Where dt.kd_kasir ='01' 
															$kriteria_user
															And  $q_shift
															$kriteria_bayar
														Group by dt.kd_kasir, dt.No_transaksi, dt.kd_produk, dt.kd_unit_tr) x 
													ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi) 
													INNER JOIN Unit u On u.kd_unit=t.kd_unit) 
													INNER JOIN Produk p on p.kd_produk=x.kd_produk 
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												Where t.ispay='t'
													 $kriteria_unit 
													 $kriteria_tindakan
													 $kriteria_kelpas
													 $customerx
													Group By u.Nama_Unit, p.Deskripsi 
												Order by U.Nama_Unit, p.Deskripsi ) z	
									")->result(); */
		$query_head=$this->db->query("SELECT DISTINCT
										U.Nama_Unit 
									From 
									(
										(
											( Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk)  
											INNER JOIN (
												Select 
													kd_kasir, 
													No_transaksi, 
													kd_produk, 
													kd_unit_tr, 
													Sum(Qty) as Qty, 
													Sum(qty*Harga) as Jumlah 
												From detail_transaksi  dt
												Where 
												kd_kasir ='01' 
												$kriteria_user
												And  $q_shift
												/*$kriteria_bayar --Tambahan*/
												$criteriaTindakanApotek
												Group by kd_kasir, No_transaksi, kd_produk, kd_unit_tr
											) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
										) INNER JOIN Unit u On u.kd_unit=t.kd_unit
									) 
									INNER JOIN Produk p on p.kd_produk=x.kd_produk 
									LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
									Where 
									t.ispay='t'
									$kriteria_unit 
									$kriteria_tindakan
									$kriteria_kelpas
									$customerx
									Group By u.Nama_Unit /*--, p.Deskripsi Order by U.Nama_Unit, p.Deskripsi*/
									")->result(); 
			// echo '{success:true, totalrecords:'.count($query_head).', listData:'.json_encode($query_head).'}';
	
		
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table class="t2" cellspacing="0" cellpadding="4" border="0">
					<tr>
						<th colspan="4" style='.$font_style.'>'.$title.'<br>
					</tr>';
			if($param->tindakan0 =='true' || $param->tindakan1 == 'true'){
			$html.='
						<tr>
							<th colspan="4" style='.$font_style.'>'.$t_tindakan.'<br>
						</tr>';
			}
			$html.='
						<tr>
							<th colspan="4" >'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="4"> Kelompok Pasien: '.$t_kelpas.' ('.$t_customer.')</th>
						</tr>
						<tr>
							<th colspan="4">Operator '.$user.' </th>
						</tr>
				</table><br>';
			 
			//---------------ISI-----------------------------------------------------------------------------------
			/*$html.="
				<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					 <tr>
						<th align='center' width='40'>No</th>
						<th align='center' width='120' >Poliklinik/Tindakan</th>
						<th align='center' width='80'>Jumlah Pasien</th>
						<th align='center' width='80'>Jumlah Produk</th>
						<th align='center' width='200'>Total</th>
					</tr>
				</thead>";*/
			$html.="
				<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					 <tr>
						<th align='center' width='40'>No</th>
						<th align='center' width='120' >Poliklinik/Tindakan</th>
						<th align='center' width='80'>Jumlah Pasien</th>
						<th align='center' width='80'>Jumlah Produk</th>
					</tr>
				</thead>";
			$no=1;
			$grand_tot_jml_pasien=0;
			$grand_tot_qty=0;
			$grand_tot_jumlah=0;
			if(count ($query_head) ==0){
				$html.=	' 	<tr>
									<td align="center" colspan="4">Data Tidak Ada</td>
								</tr>
							';
			}else{
				$baris=1;
				foreach ($query_head as $line){
					$nama_unit = str_replace('&','DAN',$line->nama_unit);
					$key_nama_unit = $line->nama_unit;
					$html.=	' 	<tr>
									<td align="center">'.$no.'</td>
									<td colspan="3">'.$nama_unit.'</td>
								</tr>
							';
					$baris++;	
						 $query=$this->db->query("SELECT 
													U.Nama_Unit, 
													p.deskripsi, 
													Count(k.Kd_Pasien) as Jml_Pasien,  
													Sum(x.Qty) as Qty, 
													sum(x.Jumlah) as Jumlah 
												From 
												(
													(
														( Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk)  
														INNER JOIN (
															Select 
																kd_kasir, 
																No_transaksi, 
																kd_produk, 
																kd_unit_tr, 
																Sum(Qty) as Qty, 
																Sum(qty*Harga) as Jumlah 
															From detail_transaksi  dt
															Where 
															kd_kasir ='01' 
															$kriteria_user
															And  $q_shift
															--$kriteria_bayar --Tambahan
															$criteriaTindakanApotek 
															Group by kd_kasir, No_transaksi, kd_produk, kd_unit_tr
														) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
													) INNER JOIN Unit u On u.kd_unit=t.kd_unit
												) 
												INNER JOIN Produk p on p.kd_produk=x.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												Where 
												t.ispay='t'
												$kriteria_unit 
												$kriteria_tindakan
												$kriteria_kelpas
												$customerx
												Group By u.Nama_Unit, p.Deskripsi Order by U.Nama_Unit, p.Deskripsi
													")->result(); 
						
						$no_b=1;
						$tot_jml_pasien=0;
						$tot_qty=0;
						$tot_jumlah=0;
						foreach ($query as $line2){
							if($key_nama_unit == $line2->nama_unit){
								$deskripsi = str_replace('&','dan',$line2->deskripsi);
								$deskripsi = str_replace('>','lebih dari',$line2->deskripsi);
								$deskripsi = str_replace('<','kurang dari',$line2->deskripsi);/*
								$html.=	' <tr>
												<td></td>
												<td>'.$no_b.'. '.$deskripsi.'</td>
												<td align="center">'.$line2->jml_pasien.'</td>
												<td align="center">'.$line2->qty.'</td>
												<td align="right">'.number_format($line2->jumlah,0,',',',').'</td>
										  </tr>';*/
								$html.=	' <tr>
												<td></td>
												<td>'.$no_b.'. '.$deskripsi.'</td>
												<td align="center">'.$line2->jml_pasien.'</td>
												<td align="center">'.$line2->qty.'</td>
										  </tr>';
								$no_b++;
								$baris++;	
								$tot_jml_pasien = $tot_jml_pasien + $line2->jml_pasien ;
								$tot_qty = $tot_qty + $line2->qty;
								$tot_jumlah = $tot_jumlah + $line2->jumlah;
							}
						} 
						$grand_tot_jml_pasien=$grand_tot_jml_pasien + $tot_jml_pasien ;
						$grand_tot_qty=$grand_tot_qty + $tot_qty ;
						$grand_tot_jumlah=$grand_tot_jumlah + $tot_jumlah;
						/*$html.=	' 	<tr>
										<td align="center"></td>
										<td align="right"  style="font-weight: bold;" >Subtotal</td>
										<td align="center"  style="font-weight: bold;" >'.number_format($tot_jml_pasien,0,',',',').'</td>
										<td align="center"  style="font-weight: bold;" >'.number_format($tot_qty,0,',',',').'</td>
										<td align="right"  style="font-weight: bold;" >'.number_format($tot_jumlah,0,',',',').'</td>
									</tr>
							';*/
						$html.=	' 	<tr>
										<td align="center"></td>
										<td align="right"  style="font-weight: bold;" >Subtotal</td>
										<td align="center"  style="font-weight: bold;" >'.number_format($tot_jml_pasien,0,',',',').'</td>
										<td align="center"  style="font-weight: bold;" >'.number_format($tot_qty,0,',',',').'</td>
									</tr>
							';
					$baris++;	
					$no++;
				}/*
					$html.=	' 	<tr>
									<td align="center"></td>
									<td align="right"  style="font-weight: bold;" >Grand Total</td>
									<td align="center"  style="font-weight: bold;" >'.number_format($grand_tot_jml_pasien,0,',',',').'</td>
									<td align="center"  style="font-weight: bold;" >'.number_format($grand_tot_qty,0,',',',').'</td>
									<td align="right"  style="font-weight: bold;" >'.number_format($grand_tot_jumlah,0,',',',').'</td>
								</tr>
						';	*/
					$html.=	' 	<tr>
									<td align="center"></td>
									<td align="right"  style="font-weight: bold;" >Grand Total</td>
									<td align="center"  style="font-weight: bold;" >'.number_format($grand_tot_jml_pasien,0,',',',').'</td>
									<td align="center"  style="font-weight: bold;" >'.number_format($grand_tot_qty,0,',',',').'</td>
								</tr>
						';	
					$baris++;	
			}
		$baris= $baris+8;
		$print_area ='A1:D'.$baris;
		$wrap_area 	='A8:D'.$baris;
		$html.='</table>';
		$prop=array('foot'=>true);
		if($param->type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
				
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $wrap_area);
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:D7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:D4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:D7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('C8:D'.$baris)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);/*
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle('C')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			// $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle('B')
						->getAlignment()->setWrapText(true); 
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Transaksi_Per_Poli_Per_Produk'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Transaksi_Per_Poli_Per_Produk.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P','Laporan Transaksi Per Poli Per Produk',$html);	
			echo $html;
		}  
		
	}

	function cetak_lap_per_poli_per_pasien_backup(){
		$param=json_decode($_POST['data']);
   		$title='Laporan Per Poli Per Pasien Per Transaksi';
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit 	= substr($tmpKdUnit, 0, -1);
		$posisi 	= strpos($tmpKdUnit, '000');
		if($tmpKdUnit == '000' || $posisi !== false){
			$kduser = $this->session->userdata['user_id']['id'];
			$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$kriteria_unit = " And (k.kd_unit in (".$kd_unit.")) ";
		}else{
			$kriteria_unit = " And (k.kd_unit in (".$tmpKdUnit.")) ";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (db.Kd_Pay in (".$temp_kd_pay.")) ";
			$kriteria_bayar2 = " And x.Kd_Pay in (".$temp_kd_pay.") ";
		}else{
			$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
			$kriteria_bayar2 = " And x.Kd_Pay in (".$tmpKdPay.") ";
		}
		
		$kel_pas = $param->pasien;
		if($kel_pas == -1 ){
			$kriteria_kelpas='';
			$customerx='';
			$t_kelpas='Semua';
			$t_customer='Semua';
			// echo "masuks";
		}else{
			// echo "masuk";
			$kriteria_kelpas=" and Ktr.jenis_cust in ('".$kel_pas."') ";
			if($kel_pas == 0){
				$t_kelpas='Perseorangan';
			}else if($kel_pas == 1){
				$t_kelpas='Perusahaan';
			}else{
				$t_kelpas='Asuransi';
			}
			
			/*Parameter Customer*/
			$arrayDataCustomer = $param->tmp_kd_customer;
		
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$semua="'0'";
			if($tmpKdCustomer == $semua){
				$customerx='';
				$t_customer='Semua';
			}else{
				$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
				$customer=$this->db->query("Select * From customer Where Kd_customer in ($tmpKdCustomer)")->result();
				$t_customer='';
				foreach ($customer as $line){
					$t_customer=$line->customer.", ".$t_customer;
				}
				$t_customer = substr($t_customer, 0, -2);
			
			}
		}
		
		if (strtolower($param->order_by) ==  strtolower("Medrec") || $param->order_by == '0') {
			$criteriaOrder = " ORDER BY KD_PASIEN ASC ";
		}else{
			$criteriaOrder = " ORDER BY NAMA ASC ";
		}
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			 $q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}
		
		
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		// if($param->tindakan0=='true' ){ /*Pendaftaran*/
			// if($param->tindakan1=='true'){/*Pendaftaran & tindakan RWJ*/
				 // //$kriteria_tindakan =" and (( prd.kd_klas = '1' ) or ( prd.kd_klas <> '1' and prd.kd_klas <> '9')) ";
				 // $kriteria_tindakan =" and (( prd.kd_klas = '1' ) or ( prd.kd_klas <> '1' )) ";
				 // $t_tindakan='Pendaftaran dan Tindakan Rawat Jalan';
			// }else{
				 // $kriteria_tindakan =  "and ( prd.kd_klas = '1' ) ";
				 // $t_tindakan='Pendaftaran';
			// }
		// }else if($param->tindakan1=='true'){  /*Tindakan RWJ*/
			 // //$kriteria_tindakan =" and ( prd.kd_klas <> '1' and prd.kd_klas <> '9') ";
			 // $kriteria_tindakan =" and ( prd.kd_klas <> '1' ) ";
			 // $t_tindakan=' Tindakan Rawat Jalan';
		// }else{
			// $kriteria_tindakan='';
			 // $t_tindakan='';
		// }
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan';
		}else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran dan Tindakan';
		}
		//echo $kriteria_tindakan;
		/*Parameter Operator*/
		$kd_user = $param->kd_user;
		$kriteria_user ='';
		$user='';
		if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and dt.kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}
		
		$query_head= $this->db->query("SELECT DISTINCT NAMA_UNIT FROM(Select U.Nama_Unit, ps.Kd_pasien, Ps.nama as Nama, prd.deskripsi,sum(dt.qty*dt.Harga) as  Jumlah,  1 as Tag,  
											sum(case when db.KD_PAY IN('TU','IA') then dt.qty*dt.Harga else 0 end) as UT,  
											sum(case when db.KD_PAY not IN('TU','IA','DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as PT ,  
											sum(case when db.KD_PAY IN('DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as SSD , max(coalesce(nb.no_nota,null)) as no_nota  , dt.tgl_transaksi  
										From Kunjungan k  
											INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
											inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
											inner join DETAIL_BAYAR db  on db.KD_KASIR= dt.KD_KASIR  and db.NO_TRANSAKSI = dt.NO_TRANSAKSI 
											inner join PAYMENT pay on pay.KD_PAY = db.KD_PAY 
											inner join produk prd on dt.kd_produk=prd.kd_produk 
											INNER JOIN Unit u On u.kd_unit=t.kd_unit  
											INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
											left join nota_bill nb on nb.kd_kasir = t.kd_kasir and nb.no_transaksi = t.no_transaksi 
										Where t.ispay='t'
											 And  $q_shift
											 $kriteria_unit 
											 $kriteria_bayar 
											 $kriteria_tindakan
											 $kriteria_kelpas
											 $customerx
											 $kriteria_user
										group by U.Nama_Unit, ps.Kd_pasien, Ps.nama, prd.deskripsi, dt.tgl_transaksi  
										Order by Nama_Unit, Nama, no_nota, tag ) Z
										")->result();
		// echo '{success:true, totalrecords:'.count($query_head).', listData:'.json_encode($query_head).'}';
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table class="t2" cellspacing="0" cellpadding="4" border="0">
					<tr>
						<th colspan="5" style='.$font_style.'>'.$title.'<br>
					</tr>';
			if($param->tindakan0 =='true' || $param->tindakan1 == 'true'){
			$html.='
						<tr>
							<th colspan="5" style='.$font_style.'>'.$t_tindakan.'<br>
						</tr>';
			}
			$html.='
						<tr>
							<th colspan="5" >'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="5"> Kelompok Pasien: '.$t_kelpas.' ('.$t_customer.')</th>
						</tr>
						<tr>
							<th colspan="5">Operator '.$user.' </th>
						</tr>
				</table><br>';
			 
			//---------------ISI-----------------------------------------------------------------------------------
			$html.="
				<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					 <tr>
						<th align='center' width='40'>No</th>
						<th align='center' width='100' >No. Medrec</th>
						<th align='center' width='150'>Nama Pasien</th>
						<th align='center' width='70'>No. Kwitansi</th>
						<th align='center' width='200'>Jumlah (Rp)</th>
					</tr>
				</thead>";
		$no=1;
		$baris=1;
		$array_baris = array();
		$grand_total = 0;
		$index 		 = 0;
		if(count($query_head)==0){
			$html.=	'<tr><td colspan="5" align="center">Data Tidak Ada</td></tr>';
		}else{
			$baris_unit 	= 1;
			$baris_pasien 	= 1;
			$baris_tindakan	= 1;
			$baris_item		= 1;
			foreach ($query_head as $line){
				$nama_unit_t = str_replace('&','DAN',$line->nama_unit);
				$nama_unit=$line->nama_unit;
				$html.=	' 	<tr>
									<td align="center">'.$no.'. </td>
									<td >'.$nama_unit_t.'</td>
									<td></td> <td></td> <td></td>
								</tr>
							';
				$baris_item++;
				$baris_unit++;
				$baris++;
				$query_pasien= $this->db->query("SELECT DISTINCT KD_PASIEN,NAMA,NO_NOTA FROM(Select U.Nama_Unit, ps.Kd_pasien, Ps.nama as Nama, prd.deskripsi,sum(dt.qty*dt.Harga) as  Jumlah,  1 as Tag,  
												sum(case when db.KD_PAY IN('TU','IA') then dt.qty*dt.Harga else 0 end) as UT,  
												sum(case when db.KD_PAY not IN('TU','IA','DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as PT ,  
												sum(case when db.KD_PAY IN('DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as SSD , max(coalesce(nb.no_nota,null)) as no_nota  , dt.tgl_transaksi  
											From Kunjungan k  
												INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
												inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
												inner join DETAIL_BAYAR db  on db.KD_KASIR= dt.KD_KASIR  and db.NO_TRANSAKSI = dt.NO_TRANSAKSI 
												inner join PAYMENT pay on pay.KD_PAY = db.KD_PAY 
												inner join produk prd on dt.kd_produk=prd.kd_produk 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit  
												INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
												left join nota_bill nb on nb.kd_kasir = t.kd_kasir and nb.no_transaksi = t.no_transaksi 
											Where t.ispay='t'
												 And  $q_shift
												 $kriteria_unit 
												 $kriteria_bayar 
												 $kriteria_tindakan
												 $kriteria_kelpas
												 $customerx
												 $kriteria_user
											group by U.Nama_Unit, ps.Kd_pasien, Ps.nama, prd.deskripsi, dt.tgl_transaksi  
											Order by Nama 
											) Z where nama_unit='$nama_unit' ".$criteriaOrder."
											");
				// echo '{success:true, totalrecords:'.count($query_pasien).', listData:'.json_encode($query_pasien).'}';
				$no_b=1;
				$total_unit=0;
				foreach ($query_pasien->result() as $line2){
					$baris++;
					$kd_pasien=$line2->kd_pasien;
					$html.=	' 	<tr>
									<td align="center"></td>
									<td >'.$no_b.'. '.$line2->kd_pasien.'</td>
									<td >'.$line2->nama.'</td>
									<td >'.$line2->no_nota.'</td>
									<td></td>
								</tr>
							';
					$baris_item++;
					$baris_pasien++;
					/* $query_deskripsi= $this->db->query("SELECT * FROM(Select U.Nama_Unit, ps.Kd_pasien, Ps.nama as Nama, prd.deskripsi,sum(dt.qty*dt.Harga) as  Jumlah,  1 as Tag,  
												sum(case when db.KD_PAY IN('TU','IA') then dt.qty*dt.Harga else 0 end) as UT,  
												sum(case when db.KD_PAY not IN('TU','IA','DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as PT ,  
												sum(case when db.KD_PAY IN('DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as SSD , max(coalesce(nb.no_nota,null)) as no_nota  , dt.tgl_transaksi  
											From Kunjungan k  
												INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
												inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
												inner join DETAIL_BAYAR db  on db.KD_KASIR= dt.KD_KASIR  and db.NO_TRANSAKSI = dt.NO_TRANSAKSI 
												inner join PAYMENT pay on pay.KD_PAY = db.KD_PAY 
												inner join produk prd on dt.kd_produk=prd.kd_produk 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit  
												INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
												left join nota_bill nb on nb.kd_kasir = t.kd_kasir and nb.no_transaksi = t.no_transaksi 
											Where t.ispay='t'
												 And  $q_shift
												 $kriteria_unit 
												 $kriteria_bayar 
												 $kriteria_tindakan
												 $kriteria_kelpas
												 $customerx
												 $kriteria_user
											group by U.Nama_Unit, ps.Kd_pasien, Ps.nama, prd.deskripsi, dt.tgl_transaksi  
											Order by Nama_Unit, Nama, no_nota, tag ) Z where nama_unit='$nama_unit' and kd_pasien='$kd_pasien'
											"); */
					$query_deskripsi=$this->db->query("Select t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
										case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
										case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
										Sum(x.Jumlah) as jumlah,
											max(x.no_nota) as no_nota , pr.deskripsi
											From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
													INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
															dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
															max(coalesce(nb.no_nota,null)) as no_nota
															FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
																dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
																and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
																and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
																left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  	
																	GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																		AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																			INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																			INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																				And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																			INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																			inner join dokter d on k.kd_dokter=d.kd_dokter
																			LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																			INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																			INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																				WHERE 
																				t.IsPay = 't'
																				and $q_shift
																				$kriteria_unit 
																				 $kriteria_tindakan
																				 $kriteria_kelpas
																				 $customerx
																				 $kriteria_user
																				$kriteria_bayar2
																				 AND t.Kd_Kasir = '01' 
																				AND U.nama_unit='$nama_unit' and k.kd_pasien='$kd_pasien'
														GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama, pr.deskripsi
														");
					$tot_pasien=0;
					
					foreach ($query_deskripsi->result() as $line3){
						$baris++;
						$deskripsi = str_replace('&','dan',$line3->deskripsi);
						$deskripsi = str_replace('>','lebih dari',$line3->deskripsi);
						$deskripsi = str_replace('<','kurang dari',$line3->deskripsi);
						$html.=	' 	<tr>
									<td align="center"></td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;- '.$deskripsi.'</td>
									<td></td>
									<td></td>
									<td align="right" >'.number_format($line3->jumlah,0,',',',').'</td>
								</tr>
							';
						$baris_item++;
						$baris_tindakan++;
						$tot_pasien=$tot_pasien+$line3->jumlah;
					}
					$html.=	' 	<tr>
									<td align="center"></td>
									<td></td>
									<td></td>
									<td  align="right" >Subtotal:</td>
									<td align="right" >'.number_format($tot_pasien,0,',',',').'</td>
								</tr>
							';
					$no_b++;
					$baris++;
					$total_unit = $total_unit+$tot_pasien;
					$baris_item++;
					$array_baris[$index] = $baris_item+7;
					//$baris_item = 1;
					// $array_baris[$index] = $baris_pasien+$baris_tindakan+8;
					$index++;
				}
					$grand_total=$grand_total + $total_unit;
					$html.=	' 	<tr>
									<td align="center"></td>
									<td></td>
									<td></td>
									<td align="right" >Total '.$nama_unit_t.':</td>
									<td align="right" >'.number_format($total_unit,0,',',',').'</td>
								</tr>
							'; 
				$no++;
				$baris_item++;
				$baris++;

			}
			
			/* $query_total= $this->db->query("SELECT sum(ut) as UT, sum(ssd) as SSD,sum(pt) as PT FROM(Select U.Nama_Unit, ps.Kd_pasien, Ps.nama as Nama, prd.deskripsi,sum(dt.qty*dt.Harga) as  Jumlah,  1 as Tag,  
												sum(case when db.KD_PAY IN('TU','IA') then dt.qty*dt.Harga else 0 end) as UT,  
												sum(case when db.KD_PAY not IN('TU','IA','DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as PT ,  
												sum(case when db.KD_PAY IN('DC','J1','J3','R1','R2') then dt.qty*dt.Harga else 0 end) as SSD , max(coalesce(nb.no_nota,null)) as no_nota  , dt.tgl_transaksi  
											From Kunjungan k  
												INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
												inner join detail_transaksi dt ON dt.Kd_Kasir=t.Kd_Kasir And dt.No_Transaksi=t.No_Transaksi 
												inner join DETAIL_BAYAR db  on db.KD_KASIR= dt.KD_KASIR  and db.NO_TRANSAKSI = dt.NO_TRANSAKSI 
												inner join PAYMENT pay on pay.KD_PAY = db.KD_PAY 
												inner join produk prd on dt.kd_produk=prd.kd_produk 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit  
												INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
												left join nota_bill nb on nb.kd_kasir = t.kd_kasir and nb.no_transaksi = t.no_transaksi 
											Where t.ispay='t'
												 And  $q_shift
												 $kriteria_unit 
												 $kriteria_bayar 
												 $kriteria_tindakan
												 $kriteria_kelpas
												 $customerx
												 $kriteria_user
											group by U.Nama_Unit, ps.Kd_pasien, Ps.nama, prd.deskripsi, dt.tgl_transaksi  
											Order by Nama_Unit, Nama, no_nota, tag ) Z 
											")->result(); */
					$query_total= $this->db->query("Select 
										case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
										case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT 
										
											From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
													INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
															dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
															max(coalesce(nb.no_nota,null)) as no_nota
															FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
																dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
																and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
																and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
																left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  	
																	GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																		AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																			INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																			INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																				And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																			INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																			inner join dokter d on k.kd_dokter=d.kd_dokter
																			LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																			INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																			INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																				WHERE 
																				t.IsPay = 't'
																				and $q_shift
																				$kriteria_unit 
																				 $kriteria_tindakan
																				 $kriteria_kelpas
																				 $customerx
																				 $kriteria_user
																				$kriteria_bayar2
																				 AND t.Kd_Kasir = '01' 
														
											")->result();
			foreach ($query_total as $line4){
				 $html.=	' 	<tr>
								<td></td>
								<td align="right" colspan="3" >Total Penerimaan Tunai:</td>
								<td align="right" >'.number_format($line4->ut,0,',',',').'</td>
							</tr>
							<tr>
								<td></td>
								<td align="right" colspan="3" >Total Penerimaan Piutang:</td>
								<td align="right" >'.number_format($line4->pt,0,',',',').'</td>
							</tr>
							<tr>
								<td></td>
								<td align="right" colspan="3" >Total Penerimaan Subsidi RS:</td>
								<td align="right" >'.number_format($line4->ssd,0,',',',').'</td>
							</tr>
							<tr>
									<td></td>
									<td align="right" colspan="3" >Grand Total:</td>
									<td align="right" >'.number_format($grand_total,0,',',',').'</td>
								</tr>
							'; 
			}
		}
		
		$baris=$baris+15;
		$print_area='A1:E'.((int)$baris+2);
		$area_wrap='A7:E'.((int)$baris+2);
		$html.='</table>';
		$prop=array('foot'=>true); 
		if($param->type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			$sharedStyle3 = new PHPExcel_Style();
			$sharedStyle4 = new PHPExcel_Style();
			$sharedStyle5 = new PHPExcel_Style();
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),/*
					'font'  => array(
						'size'  => 11,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle3->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle4->applyFromArray(
				 array(
				 	'borders' => array(
						 //'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle5->applyFromArray(
				 array(
				 	'borders' => array(
						 //'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					)
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle4, "E8".":E".(((int)$baris-9)+5));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle5, "A8".":A".(((int)$baris-9)+5));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".((int)$baris-9).":E".(((int)$baris-9)+5));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A8:E8");
			for ($i=0; $i < $index; $i++) { 
				$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$array_baris[$i].":E".$array_baris[$i]);
			}

			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:E7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:E4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:E7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


			for ($i=0; $i < $index; $i++) { 
				$objPHPExcel->getActiveSheet()
							->getStyle("E".$array_baris[$i])
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			}
			$objPHPExcel->getActiveSheet()
						->getStyle("E".((int)$baris-9).":E".(((int)$baris-9)+5))
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapPerPoliPerPasienPerTrans.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			//$common=$this->common;
			$this->common->setPdf('P','Laporan Per Poli Per Pasien Per Transaksi',$html);	
			echo $html;
		}  
		
	}
	
	function cetak_lap_rekap_jasa_pel_dokter(){
		$common    = $this->common;
		$result    = $this->result;
		$param=json_decode($_POST['data']);
   		$title='Laporan Rekap Jasa Pelayanan Dokter';
		$html='';
		
		$type_file=$param->type_file;
		#KRITERIA UNIT
		$tmpKdUnit="";

		$arrayDataUnit = $param->kd_unit;
	//	var_dump($arrayDataUnit); die();
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			if($arrayDataUnit[$i][0]=="000"){
				$kriteria_unit = " And (t.Kd_Unit like '2%') ";
			}else{
				$tmpKdUnit .= "".$arrayDataUnit[$i][0].",";
				$kriteria_unit = " And (t.Kd_Unit in  (".$tmpKdUnit.")) ";	
			}
			
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		
		
		#KRITERIA JENIS DOKTER
		$jenis_profesi = $param->jenis_profesi;
		if($jenis_profesi == 'Dokter'){
			$kriteria_jenis_dokter=" and d.jenis_dokter=1 ";
			$def_kd_component_jasa_dokter = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PELAYANAN'")->row()->setting;
			$def_kd_component_jasa_dokter_anastesi = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PELAYANAN_DOK_ANASTESI'")->row()->setting;
				
		}else if($jenis_profesi == 'Perawat'){
			$kriteria_jenis_dokter=" and d.jenis_dokter=0 ";
			$def_kd_component_jasa_dokter = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PERAWAT'")->row()->setting;
			$def_kd_component_jasa_dokter_anastesi = $this->db->query("select setting from sys_setting where key_data='COMP_JASA_PERAWAT_ANASTESI'")->row()->setting;
		}else{
			$kriteria_jenis_dokter="  and d.jenis_dokter in(1,0)";
			$def_kd_component_jasa_dokter =$this->db->query("select setting from sys_setting where key_data='COMP_JASA_PERAWAT'")->row()->setting;
		}
		
		#KRITERIA TINDAKAN
		$pendaftaran = $param->pendaftaran; //1=true
		$tindakan_rwj = $param->tindakan_rwj;
		
		 if($pendaftaran == 'true' && $tindakan_rwj == ''){
			$kriteria_tindakan =" and (( pr.kd_klas = '1' )) ";
			$t_tindakan='Pendaftaran';
		}
		if($pendaftaran == '' && $tindakan_rwj == 'true'){
			$kriteria_tindakan =" and (( pr.kd_klas <> '1' and pr.kd_klas <> '9')) ";
			$t_tindakan='Tindakan RWJ';
		}
		if($pendaftaran == 'true' && $tindakan_rwj == 'true'){
			$kriteria_tindakan =" and (( pr.kd_klas = '1' ) or ( pr.kd_klas <> '1' and pr.kd_klas <> '9')) ";
			$t_tindakan ='Pendaftaran dan Tindakan RWJ';
		}
		if($pendaftaran == '' && $tindakan_rwj == ''){
			$kriteria_tindakan ='';
		} 
				 
		#KRITERIA DOKTER 
		$kd_dokter = $param->kd_dokter;
		if($kd_dokter!='Semua'){
			$kriteria_nama_dokter = " and d.Kd_Dokter in('$kd_dokter') ";
		}else{
			$kriteria_nama_dokter ='';
		}
		
		#KRITERIA OPERATOR 
		$operator = $param->operator;
		$nama_operator = $param->nama_operator;
		if($operator == 'Semua'){
			$kriteria_operator=" "; 
		}else{
			$kriteria_operator=" and dt.Kd_user ='$operator' "; 
		}
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas;
		$nama_kel_pas = $param->nama_kel_pas;
		if($kel_pas == -1 || $kel_pas == 'Semua'){
			$kriteria_kel_pas='';
		}else{
			$kriteria_kel_pas=" and Knt.jenis_cust='$kel_pas' ";
		}
		
		#KRITERIA JENIS CUSTOMER
		$jenis_cust = $param->jenis_cust;
		$nama_jenis_cust = $param->nama_jenis_cust;
		if($jenis_cust == 0 || $jenis_cust == 'Semua'){
			$kriteria_jenis_cust='';
		}else{
			$kriteria_jenis_cust=" and Knt.kd_customer='$jenis_cust' ";
		}
		
		#KRITERIA TANGGAL
		$tgl_awal_i = str_replace("/","-", $param->tgl_awal);
		$tgl_akhir_i = str_replace("/","-", $param->tgl_akhir);
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		#judul tgl 
		$j_tgl_awal =$tgl_awal;
		$j_tgl_akhir =$tgl_akhir;
		//periode bulan
		if($param->radio_tgl == 2){
			$tgl_awal_b="date_trunc('month', '".date('Y-m-d',strtotime($tgl_awal))."'::date)";
			$tgl_akhir_b="date_trunc('month', '".date('Y-m-d',strtotime($tgl_akhir))."'::date)+'1month'::interval-'1day'::interval";
			$j_tgl_awal=tanggalstring(date('Y-m-d',strtotime($this->db->query(" select date_trunc('month', '".date('Y-m-d',strtotime($tgl_awal))."'::date) as tawal")->row()->tawal)));
			$j_tgl_akhir=tanggalstring(date('Y-m-d',strtotime($this->db->query("select date_trunc('month', '".date('Y-m-d',strtotime($tgl_akhir))."'::date)+'1month'::interval-'1day'::interval as takhir")->row()->takhir)));
			$kriteria_tgl=" and (dt.Tgl_Transaksi>= $tgl_awal_b And dt.Tgl_Transaksi<= $tgl_akhir_b) ";
		}else{
			$kriteria_tgl=" and (dt.Tgl_Transaksi>= '$tgl_awal' And dt.Tgl_Transaksi<= '$tgl_akhir') ";
			$j_tgl_awal = $tgl_awal;
			$j_tgl_akhir = $tgl_akhir;
		}
		
		$query = $this->db->query( "
									Select d.Nama as nama, 
										sum(case when dtbc.KD_COMPONENT in (".$def_kd_component_jasa_dokter.") and pr.kd_Klas in ('32') and dtd.KD_COMPONENT in(".$def_kd_component_jasa_dokter.") then dtbc.jumlah else 0 end) as JDK, 
										sum(case when dtbc.KD_COMPONENT  in (".$def_kd_component_jasa_dokter.") and dtd.KD_COMPONENT  in (".$def_kd_component_jasa_dokter.")  then dtbc.jumlah else 0 end) as JDT, 
										sum(case when dtbc.KD_COMPONENT in (".$def_kd_component_jasa_dokter_anastesi.") and dtd.kd_component in(".$def_kd_component_jasa_dokter_anastesi.") then dtbc.jumlah else 0 end) as JDA, 
										dt.TGL_TRANSAKSI, 
											Sum(dtd.Pajak * dt.qty) as Pajak, 
									 		Sum(dtd.pot_ops * dt.qty) as pot_ops,  
									 		(sum(case when pr.kd_Klas in ('32') then dtd.JP * dt.qty else 0 end) +
									 		(sum(case when pr.kd_Klas not in ('32') then dtd.JP * dt.qty else 0 end) - 
									 		COALESCE(Sum(dtbc.jumlah * dt.qty),0))-Sum(dtd.Pajak * dt.qty)) as total ,
									 		(sum(case when dtbc.KD_COMPONENT in (".$def_kd_component_jasa_dokter.") and pr.kd_Klas not in ('32') then dtbc.jumlah else 0 end)-Sum(dtd.Pajak * dt.qty)) as jumlah
										From Detail_TRDokter dtd 
											INNER JOIN Detail_Transaksi dt On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut 
											INNER JOIN detail_tr_bayar dtb On dt.Kd_Kasir=dtb.Kd_kasir And dt.No_Transaksi=dtb.no_Transaksi and dt.Tgl_Transaksi=dtb.tgl_Transaksi And dt.Urut=dtb.Urut 
											INNER JOIN detail_tr_bayar_component dtbc On dtbc.Kd_Kasir=dtb.Kd_kasir And dtbc.No_Transaksi=dtb.no_Transaksi and dtbc.Tgl_Transaksi=dtb.tgl_Transaksi And dtbc.Urut=dtb.Urut 
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir 
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter 			
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk 
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer 
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien 
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk 
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
									Where 
												t.ispay='t' 
												$kriteria_tgl	
												$kriteria_operator
												$kriteria_unit
												$kriteria_jenis_dokter
												$kriteria_nama_dokter
												$kriteria_kel_pas
												$kriteria_jenis_cust
												$kriteria_tindakan
									Group By d.Nama, dt.TGL_TRANSAKSI
									")->result();
		/* $query=$this->db->query(" select x.nama, 
										 sum(x.JPDOK) as JPDOK, sum(x.JPA) as JPA, sum(x.jumlah) as jumlah,
										 sum(x.Pajak_ops) as Pajak_ops, sum(x.Pajak_pph) as Pajak_pph,
										 sum(x.Total) As Total
										 From
										 (
											 Select d.Nama, 
												sum(CASE WHEN p.kd_Klas not in ('32') THEN (xdt.Tarif * xdt.Qty ) ELSE 0 END) as JPDok, 
												sum(CASE WHEN p.kd_Klas in ('32') THEN (xdt.Tarif * xdt.Qty ) ELSE 0 END) as JPA, 
												SUM(xdt.Qty * xdt.Tarif) as Jumlah ,
												Sum((xdt.Qty) * (xdt.pajak_op)) as Pajak_ops,
												Sum((xdt.Qty) * (xdt.pajak_pph)) as Pajak_pph ,
												Sum(xdt.Qty * xdt.tarif) - (Sum(xdt.pajak_op) + Sum(xdt.pajak_pph)) as  Total 
											From ((((Kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer) 
											INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
											And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk)  
											INNER JOIN 
												(Select dt.kd_Kasir, dt.no_transaksi, dt.kd_Produk, dtd.Kd_Dokter, 
													Sum(Qty) as Qty, 
													max(dtd.JP) as Tarif, 
													max(dtd.POT_OPS) as pajak_op, 
													max(dtd.pajak) as pajak_pph, 
													max(dt.harga) as harga
												From Detail_Transaksi dt INNER JOIN Detail_trDokter dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
																	And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
												 Where dt.Kd_kasir ='01' 
														$kriteria_tgl	
														$kriteria_operator
														--And Folio in ('A','E') 
												Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter
												) xdt 
											 On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
											INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
											INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
											 INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit
											 Where 
												t.ispay='t' 
												$kriteria_unit
												$kriteria_jenis_dokter
												$kriteria_nama_dokter
												$kriteria_kel_pas
												$kriteria_jenis_cust
												$kriteria_tindakan
											Group By d.Nama
											Having Max(xdt.Tarif) > 0 
										 ) x
									 group by x.Nama Order By x.Nama ")->result();  */
		
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table class="t2" cellspacing="0" cellpadding="4" border="0">
					<tr>
						<th colspan="11" style='.$font_style.'>'.$title.'<br>
					</tr>';
			if($pendaftaran == 'true' || $tindakan_rwj == 'true'){
				$html.='
							<tr>
								<th colspan="11" style='.$font_style.'>'.$t_tindakan.'<br>
							</tr>';
			}
			$html.='
						<tr>
							<th colspan="11">Kelompok Pasien: '.$nama_kel_pas.' ('.$nama_jenis_cust.')</th>
						</tr>
						<tr>
							<th colspan="11" >'.$j_tgl_awal.' s/d '.$j_tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="11">Operator '.$nama_operator.' </th>
						</tr>
				</table><br>';
			 
			//---------------ISI-----------------------------------------------------------------------------------
			$html.="
				<table border='1' cellpadding='3'>
				<thead>
					 <tr>
						<th align='center' width='40'>No</th>
						<th align='center' width='100' >Dokter/Pasien</th>
						<th align='center' width='150'>JS. Konsul</th>
						<th align='center' width='100'>PPH 15.0 %</th>
						<th align='center' width='100'>Jml. Konsul Net</th>
						<th align='center' width='100'>JS. Tindakan</th>
						<th align='center' width='100'>OPS 15.0 %</th>
						<th align='center' width='100'>Jml Tindakan</th>
						<th align='center' width='100'>PPH 0.0 %</th>
						<th align='center' width='100'>Jml. Tind. Net</th>
						<th align='center' width='100'>Penerimaan</th>
					</tr>
				</thead>";	
			$baris=0;
			if(count($query)==0){
				$html.=	' <tr><td align="center" colspan="11">Data Tidak Ada</td></tr>';
			}else{
				$no=1;
				$tot_jumlah=0;
				$tot_pajak_ops=0;
				$tot_jml_tindakan=0;
				$tot_pph=0;
				$tot_jml_tindakan_net=0;
				$tot_penerimaan=0;
				
				foreach($query as $line){
					$jasadokter=$line->jdt+$line->jda;
					$html.=	' 	<tr>
									<td align="center">'.$no.'</td>
									<td >'.$line->nama.'</td>
									<td></td><td></td><td></td>
									<td align="right" >'.number_format($jasadokter,0,',',',').'</td>
									<td align="right" >'.number_format($line->pajak,0,',',',').'</td>
									<td align="right" >'.number_format($jasadokter-($jasadokter*$line->pajak),0,',',',').'</td>
									<td align="right" >'.number_format($line->pajak,0,',',',').'</td>
									<td align="right" >'.number_format($jasadokter-($jasadokter*$line->pajak),0,',',',').'</td>
									<td align="right" >'.number_format($jasadokter-($jasadokter*$line->pajak),0,',',',').'</td>
								</tr>
							';
					$no++;
					$tot_jumlah=$tot_jumlah + $jasadokter;
					$tot_pajak_ops=$tot_pajak_ops+ $line->pajak;
					$tot_jml_tindakan=$tot_jml_tindakan+ ($jasadokter-$line->pajak);
					$tot_pph=$tot_pph+$line->pajak;
					$tot_jml_tindakan_net=$tot_jml_tindakan_net+($jasadokter-$line->pajak);
					$tot_penerimaan=$tot_penerimaan+($jasadokter-($jasadokter*$line->pajak));
					$baris++;
				}
				
				$html.=	' 	<tr>
									<td align="center"></td>
									<td align="right"><b>TOTAL:</b></td>
									<td></td><td></td><td></td>
									<td align="right" >'.number_format($tot_jumlah,0,',',',').'</td>
									<td align="right" >'.number_format($tot_pajak_ops,0,',',',').'</td>
									<td align="right" >'.number_format($tot_jml_tindakan,0,',',',').'</td>
									<td align="right" >'.number_format($tot_pph,0,',',',').'</td>
									<td align="right" >'.number_format($tot_jml_tindakan_net,0,',',',').'</td>
									<td align="right" >'.number_format($tot_penerimaan,0,',',',').'</td>
								</tr>
							';
					
			}
			$html.="</table>";
			$prop=array('foot'=>true); 
			$baris=$baris+15;
			$print_area='A1:K'.$baris;
			$areabaris=$baris-6;
			$area_wrap='A8:K'.$areabaris;
			$area_kanan='C8:K'.$baris;
		if($param->type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$sharedStyle1 = new PHPExcel_Style();
			//$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	)/* ,
					'font'  => array(
						'size'  => 12,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 11,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapRekapJasaPelDokter.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('L','Laporan Rekap Jasa Pelayanan Dokter',$html);	
			// echo $html;
		}  
	}
	
	function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$users=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$tgl_awal_i      = str_replace("/","-", $param->start_date);
		$tgl_akhir_i     = str_replace("/","-",$param->last_date) ;
		$tindakan_apotek = $param->tindakan2;
		$tgl_awal        = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir       = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
   		$criteriaTindakanApotek = "";
   		if ($tindakan_apotek == false) {
   			$criteriaTindakanApotek = " AND kd_produk <> '6440' ";
   		}
		/*Parameter Unit*/
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit 	= substr($tmpKdUnit, 0, -1);
		$posisi 	= strpos($tmpKdUnit, '000');
		if($tmpKdUnit == '000' || $posisi !== false){
			$kduser = $this->session->userdata['user_id']['id'];
			$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$kriteria_unit 	= " And (k.kd_unit in (".$kd_unit."))  --or (x.kd_unit_tr in (".$kd_unit."))";
			$kriteria_unit 	= "";
		}else{
			$kriteria_unit = " And (k.kd_unit in (".$tmpKdUnit.")) --or (x.kd_unit_tr in (".$tmpKdUnit.")) ";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (dt.Kd_Pay in (".$temp_kd_pay.")) ";
		}else{
			$kriteria_bayar = " And (dt.Kd_Pay in (".$tmpKdPay.")) ";
		}
		
		/*Parameter kelompok pasien*/
		
		$kel_pas = $param->pasien;
		if($kel_pas == -1 ){
			$kriteria_kelpas='';
			$customerx='';
			$t_kelpas='Semua';
			$t_customer='Semua';
			// echo "masuks";
		}else{
			// echo "masuk";
			$kriteria_kelpas=" and Ktr.jenis_cust in ('".$kel_pas."') ";
			if($kel_pas == 0){
				$t_kelpas='Perseorangan';
			}else if($kel_pas == 1){
				$t_kelpas='Perusahaan';
			}else{
				$t_kelpas='Asuransi';
			}
			
			/*Parameter Customer*/
			$arrayDataCustomer = $param->tmp_kd_customer;
		
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$semua="'0'";
			if($tmpKdCustomer == $semua){
				$customerx='';
				$t_customer='Semua';
			}else{
				$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
				$customer=$this->db->query("Select * From customer Where Kd_customer in ($tmpKdCustomer)")->result();
				$t_customer='';
				foreach ($customer as $line){
					$t_customer=$line->customer.", ".$t_customer;
				}
				$t_customer = substr($t_customer, 0, -2);
			
			}
		}
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			 $q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}
		
		
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		// if($param->tindakan0=='true' ){ /*Pendaftaran*/
			// if($param->tindakan1=='true'){/*Pendaftaran & tindakan RWJ*/
				 // $kriteria_tindakan =" and (( p.kd_klas = '1' ) or ( p.kd_klas <> '1' and p.kd_klas <> '9')) ";
				 // $t_tindakan='Pendaftaran dan Tindakan Rawat Jalan';
			// }else{
				 // $kriteria_tindakan =  "and ( p.kd_klas = '1' ) ";
				 // $t_tindakan='Pendaftaran';
			// }
		// }else if($param->tindakan1=='true'){  /*Tindakan RWJ*/
			 // $kriteria_tindakan =" and ( p.kd_klas <> '1' and p.kd_klas <> '9') ";
			 // $t_tindakan='Tindakan Rawat Jalan';
		// }else{
			// $kriteria_tindakan='';
			 // $t_tindakan='';
		// }
		
		$transfer_lab =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_lab'")->row()->setting;
		$transfer_rad =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_rad'")->row()->setting;
		$transfer_apt =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_apt'")->row()->setting;
		
		if($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==false){
			$kriteria_tindakan =" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true  && $param->tindakan2==false){
			$kriteria_tindakan =" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan Rawat Jalan';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==false  && $param->tindakan2==true){
			$kriteria_tindakan="and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
			$t_tindakan='Transfer';
		} else if($param->tindakan0 ==true && $param->tindakan1 ==false  && $param->tindakan2==true){
			$kriteria_tindakan =" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
									INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	
									or ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt )) ";
			$t_tindakan='Pendaftran dan Transfer';
		} else if($param->tindakan0 ==false && $param->tindakan1 ==true  && $param->tindakan2==true){
			$kriteria_tindakan ="and (x.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') 
									or ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt )) ";
			$t_tindakan='Tindakan dan Transfer';
		} else if($param->tindakan0 ==true && $param->tindakan1 ==true  && $param->tindakan2==false){
			$kriteria_tindakan="and ( p.kd_produk not in ($transfer_lab,$transfer_rad,$transfer_apt ) )";
			$t_tindakan='Pendaftran dan Tindakan';
		} else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran, Tindakan Rawat Jalan dan Transfer';
		}
		
		
		$kd_user = $param->kd_user;
		$kriteria_user ='';
		$user='';
		if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and dt.kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}
		
		
		$query_head=$this->db->query("SELECT DISTINCT
										U.Nama_Unit 
									From 
									(
										(
											( Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk)  
											INNER JOIN (
												Select 
													kd_kasir, 
													No_transaksi, 
													kd_produk, 
													kd_unit_tr, 
													Sum(Qty) as Qty, 
													Sum(qty*Harga) as Jumlah 
												From detail_transaksi  dt
												Where 
												kd_kasir ='01' 
												$kriteria_user
												And  $q_shift
												--$kriteria_bayar --Tambahan
												$criteriaTindakanApotek
												Group by kd_kasir, No_transaksi, kd_produk, kd_unit_tr
											) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
										) INNER JOIN Unit u On u.kd_unit=t.kd_unit
									) 
									INNER JOIN Produk p on p.kd_produk=x.kd_produk 
									LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
									Where 
									t.ispay='t'
									$kriteria_unit 
									$kriteria_tindakan
									$kriteria_kelpas
									$customerx
									Group By u.Nama_Unit--, p.Deskripsi Order by U.Nama_Unit, p.Deskripsi
									")->result(); 
			// echo '{success:true, totalrecords:'.count($query_head).', listData:'.json_encode($query_head).'}';
	
		
	
		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 5)
			->setColumnLength(1, 50)
			->setColumnLength(2, 30)
			->setColumnLength(3, 30)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 4,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 4,"left")
			->commit("header")
			->addColumn($telp, 4,"left")
			->commit("header")
			->addColumn($fax, 4,"left")
			->commit("header")
			->addColumn("LAPORAN TRANSAKSI PER POLI PER PRODUK", 4,"center")
			->commit("header");
		if($param->tindakan0 =='true' || $param->tindakan1 == 'true'){
			$tp	->addColumn($t_tindakan, 4,"center")
				->commit("header");
		}	
		$tp	->addColumn("Periode ".$tgl_awal." s/d ".$tgl_akhir, 4,"center")
			->commit("header")
			->addColumn("Kelompok Pasien: ".$t_kelpas." (".$t_customer.")", 4,"center")
			->commit("header")
			->addColumn("Operator ".$user, 4,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("POLIKLINIK/TINDAKAN", 1,"left")
			->addColumn("JUMLAH PASIEN", 1,"right")
			->addColumn("JUMLAH PRODUK", 1,"right")
			->commit("header");
		$no=1;
			$grand_tot_jml_pasien=0;
			$grand_tot_qty=0;
			$grand_tot_jumlah=0;
		if(count($query_head) > 0) {
			foreach ($query_head as $line){
				$key_nama_unit = $line->nama_unit;
				$tp	->addColumn($no.".", 1,"left")
					->addColumn($line->nama_unit, 3,"left")
					->commit("header");	
				$query=$this->db->query("SELECT 
										U.Nama_Unit, 
										p.deskripsi, 
										Count(k.Kd_Pasien) as Jml_Pasien,  
										Sum(x.Qty) as Qty, 
										sum(x.Jumlah) as Jumlah 
									From 
									(
										(
											( Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk)  
											INNER JOIN (
												Select 
													kd_kasir, 
													No_transaksi, 
													kd_produk, 
													kd_unit_tr, 
													Sum(Qty) as Qty, 
													Sum(qty*Harga) as Jumlah 
												From detail_transaksi  dt
												Where 
												kd_kasir ='01' 
												$kriteria_user
												And  $q_shift
												--$kriteria_bayar --Tambahan
												$criteriaTindakanApotek 
												Group by kd_kasir, No_transaksi, kd_produk, kd_unit_tr
											) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
										) INNER JOIN Unit u On u.kd_unit=t.kd_unit
									) 
									INNER JOIN Produk p on p.kd_produk=x.kd_produk 
									LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
									Where 
									t.ispay='t'
									$kriteria_unit 
									$kriteria_tindakan
									$kriteria_kelpas
									$customerx
									Group By u.Nama_Unit, p.Deskripsi Order by U.Nama_Unit, p.Deskripsi
										")->result(); 
				
				$no_b=1;
				$tot_jml_pasien=0;
				$tot_qty=0;
				$tot_jumlah=0;
				foreach ($query as $line2){
					if($key_nama_unit == $line2->nama_unit){
						$tp	->addColumn("", 1,"left")
							->addColumn($no_b.'. '.$line2->deskripsi, 1,"left")
							->addColumn($line2->jml_pasien, 1,"right")
							->addColumn($line2->qty, 1,"right")
							->commit("header");
						$no_b++;
						$tot_jml_pasien = $tot_jml_pasien + $line2->jml_pasien ;
						$tot_qty = $tot_qty + $line2->qty;
						$tot_jumlah = $tot_jumlah + $line2->jumlah;
					}
				} 
				$grand_tot_jml_pasien=$grand_tot_jml_pasien + $tot_jml_pasien ;
				$grand_tot_qty=$grand_tot_qty + $tot_qty ;
				$grand_tot_jumlah=$grand_tot_jumlah + $tot_jumlah;
				$tp	->addColumn("", 1,"left")
					->addColumn("SUBTOTAL", 1,"left")
					->addColumn(number_format($tot_jml_pasien,0,',',','), 1,"right")
					->addColumn(number_format($tot_qty,0,',',','), 1,"right")
					->commit("header");
				$tp	->addColumn("", 4,"left")
					->commit("header");	
			$no++;
		}
			$tp	->addColumn("", 1,"left")
					->addColumn("GRAND TOTAL", 1,"left")
					->addColumn(number_format($grand_tot_jml_pasien,0,',',','), 1,"right")
					->addColumn(number_format($grand_tot_qty,0,',',','), 1,"right")
					->commit("header");
			
		}else {	
			$tp	->addColumn("Data tidak ada", 4,"left")
				->commit("header");			
		} 
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$users, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_per_poli_per_produk.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
	function cetak_lap_per_poli_per_pasien(){
		$param=json_decode($_POST['data']);
   		$title='Laporan Per Poli Per Pasien Per Transaksi';
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		$tmpKdUnit="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit 	= substr($tmpKdUnit, 0, -1);
		$posisi 	= strpos($tmpKdUnit, '000');
		if($tmpKdUnit == '000' || $posisi !== false){
			$kduser = $this->session->userdata['user_id']['id'];
			$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$kriteria_unit = " And (k.kd_unit in (".$kd_unit.")) ";
		}else{
			$kriteria_unit = " And (k.kd_unit in (".$tmpKdUnit.")) ";
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$temp_kd_pay = '';
			foreach($kd_pay as $pay){
				$temp_kd_pay.= "'".$pay->kd_pay."',";
			}
			$temp_kd_pay = substr($temp_kd_pay, 0, -1);
			$kriteria_bayar = " And (db.Kd_Pay in (".$temp_kd_pay.")) ";
			$kriteria_bayar2 = " And x.Kd_Pay in (".$temp_kd_pay.") ";
		}else{
			$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
			$kriteria_bayar2 = " And x.Kd_Pay in (".$tmpKdPay.") ";
		}
		
		$kel_pas = $param->pasien;
		if($kel_pas == -1 ){
			$kriteria_kelpas='';
			$customerx='';
			$t_kelpas='Semua';
			$t_customer='Semua';
			// echo "masuks";
		}else{
			// echo "masuk";
			$kriteria_kelpas=" and Ktr.jenis_cust in ('".$kel_pas."') ";
			if($kel_pas == 0){
				$t_kelpas='Perseorangan';
			}else if($kel_pas == 1){
				$t_kelpas='Perusahaan';
			}else{
				$t_kelpas='Asuransi';
			}
			
			/*Parameter Customer*/
			$arrayDataCustomer = $param->tmp_kd_customer;
		
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$semua="'0'";
			if($tmpKdCustomer == $semua){
				$customerx='';
				$t_customer='Semua';
			}else{
				$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
				$customer=$this->db->query("Select * From customer Where Kd_customer in ($tmpKdCustomer)")->result();
				$t_customer='';
				foreach ($customer as $line){
					$t_customer=$line->customer.", ".$t_customer;
				}
				$t_customer = substr($t_customer, 0, -2);
			
			}
		}
		
		if (strtolower($param->order_by) ==  strtolower("Medrec") || $param->order_by == '0') {
			$criteriaOrder = " KD_PASIEN  ";
		}else{
			$criteriaOrder = " NAMA  ";
		}
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			 $q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}
		
		
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			$kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan';
		}else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran dan Tindakan';
		}
		//echo $kriteria_tindakan;
		/*Parameter Operator*/
		$kd_user = $param->kd_user;
		$kriteria_user ='';
		$user='';
		if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and dt.kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}
		
	
		$query = $this->db->query("SELECT t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
								case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  
								case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
								case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
								Sum(x.Jumlah) as jumlah, max(x.no_nota) as no_nota , pr.deskripsi
							From Transaksi t 
							INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
							INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, max(coalesce(nb.no_nota,null)) as no_nota
									 FROM Detail_Bayar db  
										INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
											dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
											and dtb.kd_pay = db.kd_pay 
										INNER JOIN detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
											and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
										LEFT JOIN nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi 
									 WHERE 
										 $q_shift
										 $kriteria_bayar
									GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty
									) X ON dt.Kd_Kasir = X.Kd_Kasir 
									AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
							INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
							INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
							INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
							INNER join dokter d on k.kd_dokter=d.kd_dokter
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
							INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
							INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
							WHERE 
								t.IsPay = 't'
								 And  $q_shift
									 $kriteria_unit 
									 $kriteria_bayar2 
									 $kriteria_tindakan
									 $kriteria_kelpas
									 $customerx
									 $kriteria_user
							GROUP BY  U.Nama_Unit,t.tgl_transaksi, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama, pr.deskripsi
							order by U.Nama_Unit,".$criteriaOrder.",t.tgl_transaksi, pr.deskripsi ")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		 if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table class="t2" cellspacing="0" cellpadding="4" border="0">
					<tr>
						<th colspan="5" style='.$font_style.'>'.$title.'<br>
					</tr>';
			if($param->tindakan0 =='true' || $param->tindakan1 == 'true'){
			$html.='
						<tr>
							<th colspan="5" style='.$font_style.'>'.$t_tindakan.'<br>
						</tr>';
			}
			$html.='
						<tr>
							<th colspan="5" >'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="5"> Kelompok Pasien: '.$t_kelpas.' ('.$t_customer.')</th>
						</tr>
						<tr>
							<th colspan="5">Operator '.$user.' </th>
						</tr>
				</table><br>';
			 
			//---------------ISI-----------------------------------------------------------------------------------
			$html.="
				<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					 <tr>
						<th align='center' width='40'>No</th>
						<th align='center' width='100' >No. Medrec</th>
						<th align='center' width='150'>Nama Pasien</th>
						<th align='center' width='70'>No. Kwitansi</th>
						<th align='center' width='200'>Jumlah (Rp)</th>
					</tr>
				</thead>";
		
		$no=1;
		$no_poli=1;
		$tampung_unit='';
		$tampung_pasien='';
		$sub_total_pasien=0;
		$indeks_baris=0;
		$sub_total_unit =0;
		$grand_total=0;
		$sub_total_ut=0;
		$sub_total_ssd=0;
		$sub_total_pt=0;
		foreach ($query as $line){
			
			if($tampung_unit != $line->nama_unit){
				
				if($no_poli!=1){
					$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Sub Total</th>
						<th align='right'>".number_format($sub_total_pasien,0,',',',')."</th>
					</tr>";
					$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Total Poli ".$tampung_unit."</th>
						<th align='right'>".number_format($sub_total_unit,0,',',',')."</th>
					</tr>";
				}
				
				$html.="<tr>
						<td align='center'>".$no_poli."</td>
						<td colspan='4' align='center'><b>".$line->nama_unit."</b></td>
					</tr>";
				$no=1;
				$no_poli=$no_poli+1;
				$sub_total_unit=0;
			}
			

			if($tampung_pasien != $line->kd_pasien){
				
				if($no!=1){
					$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Sub Total</th>
						<th align='right'>".number_format($sub_total_pasien,0,',',',')."</th>
					</tr>";
				}
				
				$html.="<tr>
						<td></td>
						<td>".$no.".".$line->kd_pasien."</td>
						<td>".$line->nama."</td>
						<td>".$line->no_nota."</td>
						<td></td>
					</tr>";
				$no++;
				$sub_total_pasien=0;
			}
			
			$html.="<tr>
						<td></td>
						<td>- ".$line->deskripsi."</td>
						<td></td>
						<td></td>
						<td align='right'>".number_format($line->jumlah,0,',',',')."</td>
					</tr>";
			$tampung_unit = $line->nama_unit;
			$tampung_pasien = $line->kd_pasien;
			$sub_total_pasien = $sub_total_pasien +$line->jumlah;
			$sub_total_unit = $sub_total_unit +$line->jumlah;
			$indeks_baris= $indeks_baris+1;
			$grand_total = $grand_total +$line->jumlah;
			
			$sub_total_ut = $sub_total_ut + $line->ut ;
			$sub_total_ssd = $sub_total_ssd + $line->ssd ;
			$sub_total_pt = $sub_total_pt + $line->pt;
			
			if($indeks_baris == count($query)){
				$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Sub Total</th>
						<th align='right'>".number_format($sub_total_pasien,0,',',',')."</th>
					</tr>";
				$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Total Poli ".$tampung_unit."</th>
						<th align='right'>".number_format($sub_total_unit,0,',',',')."</th>
					</tr>";
				$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Total Penerimaan TUNAI</th>
						<th align='right'>".number_format($sub_total_ut,0,',',',')."</th>
					</tr>";
				$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Total Penerimaan PIUTANG</th>
						<th align='right'>".number_format($sub_total_pt,0,',',',')."</th>
					</tr>";
				$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>Total Penerimaan SUBSIDI RS</th>
						<th align='right'>".number_format($sub_total_ssd,0,',',',')."</th>
					</tr>";
				
				$html.="<tr>
						<td></td>
						<td></td>
						<td></td>
						<th align='right'>GRAND TOTAL </th>
						<th align='right'>".number_format($grand_total,0,',',',')."</th>
					</tr>";
			}
			
			
		}

		$html.="</table>";
		
		$this->common->setPdf('P','Laporan Per Poli Per Pasien Per Transaksi',$html);	
		echo $html;
	}
}