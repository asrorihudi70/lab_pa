<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class kartupasienprinting extends MX_Controller{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
		$this->load->view('main/index');
    }
	public function getAge($tgl1,$tgl2){
		$jumHari=(abs(strtotime($tgl1->format('Y-m-d'))-strtotime($tgl2->format('Y-m-d')))/(60*60*24));
		$ret=array();
		$ret['year']=floor($jumHari/365);
		$sisa=floor($jumHari-($ret['year']*365));
		$ret['month']=floor($sisa/30);
		$sisa=floor($sisa-($ret['month']*30));
		$ret['day']=$sisa;
		
		if($ret['year']==0  && $ret['month']==0 && $ret['day']==0){
			$ret['day']=1;
		}
		return $ret;
	}
	public function save($Params=NULL){
		$kdpasien = $Params['NoMedrec'];
		$namapasien = $Params['NamaPasien'];
		$alamat = $Params['Alamat'];
		$kdunit = $Params['Poli'];
		$tglmasuk = date('Y-m-d');
		$jammasuk = date('Y-m-d h:i:s');
		$kddokter = $Params['KdDokter'];
		//$unit = $this->db->query("SELECT NAMA_UNIT FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
		//$dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;
		/*  $data = array(
		 "kd_pasien"=>$kdpasien,
		 "nama_pasien"=>$namapasien,
		 "nama_unit"=>$unit,
		 "nama_dokter"=>$dokter,
		 "tgl_masuk"=>$tglmasuk,
		 "jam_masuk"=>$jammasuk,
		 "alamat"=>$alamat
		 ); */
		/* $this->load->model('rawat_jalan/tbltracer');	   
		  $save = $this->tbltracer->save($data); */
	 
		//AWAL SCRIPT PRINT
		$sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, kl.kelurahan,kc.kecamatan,kb.kabupaten,
							CASE WHEN k.BARU = 't' THEN '(B)' ELSE '' END as BARU 
							from pasien t 
							INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
							 INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan 
							 INNER JOIN customer c on k.kd_customer = c.kd_customer 
							 INNER JOIN kelurahan kl on kl.kd_kelurahan = t.kd_kelurahan
							 INNER JOIN kecamatan kc on kc.kd_kecamatan = kl.kd_kecamatan
							 INNER JOIN kabupaten kb on kb.kd_kabupaten = kc.kd_kabupaten
							 WHERE t.KD_PASIEN = '".$kdpasien."' 
							 --And k.tgl_masuk = '".$tglmasuk."' 
							 ORDER BY k.tgl_masuk DESC, jam_masuk DESC limit 1");
			
			
  
		/* $sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
			from pasien t
			INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
			INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
			INNER JOIN customer c on k.kd_customer = c.kd_customer 
			WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' "); */
		if($sql->num_rows == 0){
			$nama = '1';
			$kd_pasien = '2';
			$tgl_masuk = '3';
			$kota = '5';
			$nama_ortu = '6';
			$penjamin = '7';
			$jenis_kelamin = '8';
			$status_marita='9';
			$tgl_lahir = '10';
			$dokter = '11';
			$alamat = '12';
			$customer = '13';
			$jam = '14';
			$baru = '15';
			$umur = '16';
			$tlp = '17';
			$pekerjaan='18';
		}else{
			foreach($sql->result() as $row){
				$nama = $row->nama;
				$kd_pasien = $row->kd_pasien;
				$tgl_masuk = $row->tgl_masuk;
				$kota = $row->kabupaten;
				$kecamatan = $row->kecamatan;
				$nama_ortu = $row->nama_keluarga;
				$penjamin = $row->customer;
				$jenis_kelamin = '';
				$status_marita='';
				if ($row->jenis_kelamin=='t'){
					$jenis_kelamin='Laki-laki';
				}else{
					$jenis_kelamin='Perempuan';
				}
				if ($row->status_marita==0){
					$status_marita='Blm Menikah';
				}else if ($row->status_marita==1){
					$status_marita='Menikah';
				}else if ($row->status_marita==2){
					$status_marita='Janda';
				}else if ($row->status_marita==3){
					$status_marita='Duda';
				}
				//$tgl_lahir =date("d-m-Y",strtotime($row->TGL_LAHIR));
				$tgl_lahir =date_format(date_create($row->tgl_lahir),"d/m/Y");
				//$dokter = $row->nama_dokter;
				$alamat = $row->alamat;
				$customer = $row->customer;
				$baru = $row->baru;
				$umur = '';
				
				if (substr($tgl_lahir, 6, 5) == '1900') {
					$umur= '117 TAHUN 0 BULAN 0 HARI';
				}else{
					$now=new DateTime();
					$tglLahir=new DateTime(date_format(date_create($row->tgl_lahir),"Y-m-d"));
					$arrUmur=$this->getAge($now,$tglLahir);
					if($arrUmur['year']>0){
						$umur.=$arrUmur['year'].' TAHUN ';
					}
					if($arrUmur['month']>0){
						$umur.=$arrUmur['month'].' BULAN ';
					}
					if($arrUmur['day']>0){
						$umur.=$arrUmur['day'].' HARI ';
					}
				}
				//$umur 		= $split[0]." TAHUN";
				/* $query = $this->db->query($strQuery);
				$res = $query->result();
				if ($query->num_rows() > 0)
				{
					 foreach($res as $data)
					 {
						$pisah = explode(" ",$data->age);
						$umur = $pisah[0]." TAHUN";
					 }
					// echo $umur;
				} */
				$tlp='--';
				if ($row->telepon==''){
					$tlp='-';
				}else{
					$tlp=$row->telepon;
				}
				$pekerjaan=$row->pekerjaan;
			}
			$kode_unit=$this->db->query("select kd_unit from kunjungan where kd_pasien='".$kd_pasien."' order by tgl_masuk desc, urut_masuk desc limit 1 ")->row()->kd_unit;
			$nama_unit=$this->db->query("select nama_unit from unit where kd_unit='".$kode_unit."'")->row()->nama_unit;
			
			//$printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_kartu_pasien'")->row()->setting;
			$printer = $this->db->query("SELECT p_kartupasien FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_kartupasien;
			$waktu 		= explode(" ",$tgl_masuk);
			$tanggal 	= $waktu[0];
			//$jam = $waktu[1];
			$x = 0;
			$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
			$handle = fopen($file, 'w');
			$condensed = Chr(27).Chr(33).Chr(4);
			$condensed2 = Chr(27).Chr(33).Chr(32);
			//$size1 = Chr(27) . Chr(80);
			$bold1 = Chr(27) . Chr(69);
			$bold0 = Chr(27) . Chr(70);
			$initialized = chr(27).chr(64);
			$condensed1 = chr(15);
			$condensed0 = chr(18);
			$size0 = Chr(27).Chr(109);
			$Data  = $initialized;
			$Data .= $condensed1;
			/* $Data .= chr(27) . chr(67) . chr(70);
			$Data .= chr(27) . chr(87) . chr(49);
			$Data .= chr(27).chr(77); */
			//$Data .= "".$bold1."INSTALASI REKAM MEDIK".$bold0.""."\n"; $x++;
			$Data .= "\n";$x++;
			if ($baru=='(B)'){
				$Data .= "                                                                        ".$condensed2.$bold1.$nama_unit.$bold0.$condensed;$x++;
			}
			//$Data .= "\n";$x++;
			//$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			
			//$Data .= "			              ".$bold1.$kd_pasien.$bold0." \n";$x++;
			//$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			
			$Data .= "\n";$x++;
			$Data .= "\n";$x++;
			$Data .= str_pad("No. Medrec	: ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed,30," ");$x++;
			$Data .= "\n";$x++;
			//$Data .= "No. Medrec	: ".$kd_pasien." \n";$x++;
			$Data .= "Nama Pasien	: ".$nama." \n";$x++;
			$Data .= "Tgl. Lahir	: ".$tgl_lahir." /".$umur." \n";$x++;
			$Data .= "Jenis Kelamin	: ".$jenis_kelamin." \n";$x++;
			$Data .= "Status 		: ".$status_marita." \n";$x++;
			$Data .= "Alamat Lengkap	: ".$alamat." \n";$x++;
			$Data .= "Kota/Kecamatan	: ".$kota." / ".$kecamatan." \n";$x++;
			//$Data .= "Alamat Lengkap  	: ".$alamat." \n";$x++;
			$Data .= "No. Telp	: ".$tlp." \n";$x++;
			$Data .= "Pekerjaan	: ".$pekerjaan." \n";$x++;
			$Data .= "Nama Keluarga	: ".$nama_ortu." \n";$x++;
			$Data .= "Penjamin	: ".$customer." \n";$x++;
			/* $Data .= "\n";$x++;
			$Data .= $unit." / ".$customer."\n";$x++;
			$Data .= $dokter."\n";$x++;
			$Data .= "\n";$x++;
			$Data .= $tanggal." ".$jam."\n";$x++; */
			//--------ini error
			/* $Data .= "\n";$x++;
			$Data .= "\n";$x++;
			$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
			if($counter == '7'){
			   $Data .=''; 
			   $newVal = 1; 
			   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
			}else{
				$Data .= "\n";
				$Data .= "\n";
				$Data .= "\n";
				$newVal = (int)$counter + 1; 
				$update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
			}
			if ($x < 17){
				$y = $x-17;
				for ($z = 1; $z<=$y;$z++){
					$Data .= "\n";
				}
			} */
			
			$Data .= "\n";$x++;
			//$Data .= "\n";$x++; 
			$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
			if($counter == '9'){
			   $Data .=''; 
			   $newVal = 1; 
			   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
			}else{
				$Data .= "\n";
				$Data .= "\n";
				//$Data .= "\n"; 
				$newVal = (int)$counter + 1; 
				$update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
			}
			if ($x < 17){
				$y = $x-17;
				for ($z = 1; $z<=$y;$z++){
					//$Data .= "\n";
				}
			}
			//echo $Data;
			fwrite($handle, $Data);
			fclose($handle);
			$print = shell_exec("lpr -P ".$printer." -r ".$file);  # Lakukan cetak
			//copy($file, $printer);  # Lakukan cetak
			echo "{success:true}";
		}          
	}
}