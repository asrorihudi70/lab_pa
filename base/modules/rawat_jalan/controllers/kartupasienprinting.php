<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class kartupasienprinting extends MX_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->library('fpdf');
    }

    public function index(){
		$this->load->view('main/index');
    }
	public function cetak($Params=NULL){
        // echo APPPATH;die;
        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $this->db->select("*");
        $this->db->where(array( 'KD_PASIEN' => $Params ));
        $this->db->from("pasien");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
			$mpdf = new mPDF('', array(265, 168), '', '', 1, 2, 2, 2, 5, 5);
			$mpdf = new mPDF(
				'utf-8',    // mode - default ''
				// '21cm 29.7cm',    // format - A4, for example, default ''
				// array(80, 50),    // format - A4, for example, default ''
				array(85, 53),    // format - A4, for example, default ''
				0,     // font size - default 0
				'',    // default font family
				1,    // margin_left
				1,    // margin right
				0,     // margin top
				0,    // margin bottom
				0,     // margin header
				0,     // margin footer
			'P');  // L - landscape, P - portrait
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->SetTitle('Kartu Pasien');
			$mpdf->WriteHTML("
			<style>
			p{
				font-family:arial;
				font-size:large;
				line-height:1px;
				font-weigth:bolder;
			}

			.barcode {
				float:left;
				position:relative;
				margin-left:-15px;
				margin-top:-5px;
			}

			.font{
				font-family:arial;
				margin-left:500px;
			}
			table td{
				font-family:arial;
			}
            </style>
            ");
            /*$mpdf->WriteHTML("<html>
                <body>
                <table width='100%' border='0'>
                	<tr>
                		<td width='55%' style='height:350px;' valign='top'>
			                <font class='font' style='font-size:400%;'> <b>".$query->row()->nama."</b></font><br>
			                <font class='font' style='font-size:200%;'> ".$query->row()->kota."<br> ".date_format(date_create($query->row()->tgl_lahir), 'Y-m-d')."</font><br>
                		</td>
                		<td width='45%'></td>
                	</tr>
                	<tr>
                		<td width='55%' style='height:200px;' valign='top'>
			                <font class='font' style='font-size:160%;'><b>ALamat</b></font><br>
			                <font class='font' style='font-size:180%;'>".$query->row()->alamat."</font><br>
                		</td>
                		<td width='45%' valign='top' align='center'>
                			<barcode code=".$query->row()->kd_pasien." type='C128B' class='barcode' size='1.50' height='1.25'/><br>
			                <font class='font' style='font-size:400%;'> <b>".$query->row()->kd_pasien."</b></font><br>
                		</td>
                	</tr>
                </table>
                </body>
            </html>"); */
            $mpdf->WriteHTML("<html>");
            $mpdf->WriteHTML("<body>");
            $mpdf->WriteHTML("<table width='100%' border='0' cellspacing='0'>");

            $mpdf->WriteHTML("<tr>");
            $mpdf->WriteHTML("<td width='50%' height='80'></td>");
            $mpdf->WriteHTML("<td width='50%'></td>");
            $mpdf->WriteHTML("</tr>");

            $mpdf->WriteHTML("<tr>");
            $mpdf->WriteHTML("<td style='padding-left:3px;'><font style='font-size:15px;'><b>".$query->row()->KD_PASIEN."</b></font></td>");
            $mpdf->WriteHTML("<td></td>");
            $mpdf->WriteHTML("</tr>");

            $mpdf->WriteHTML("<tr>");
            $mpdf->WriteHTML("<td style='padding-left:3px;' colspan='2'><b><font style='font-size:18px;'>".$query->row()->NAMA."</font></b></td>");
            $mpdf->WriteHTML("</tr>");

            $mpdf->WriteHTML("<tr>");
            $mpdf->WriteHTML("<td style='padding-left:3px;' colspan='2'><font style='font-size:13px;'>".$query->row()->TEMPAT_LAHIR.", ".date_format(date_create($query->row()->TGL_LAHIR), 'Y-m-d')."</font></td>");
            $mpdf->WriteHTML("</tr>");
            
            $mpdf->WriteHTML("<tr>");
            $mpdf->WriteHTML("<td height='10'></td>");
            $mpdf->WriteHTML("<td></td>");
            $mpdf->WriteHTML("</tr>");
            
            $mpdf->WriteHTML("<tr>");
            $mpdf->WriteHTML("<td style='padding-left:10px;' height='32' colspan='2'><barcode code=".$query->row()->KD_PASIEN." type='C128B' class='barcode' size='0.62' height='1.00' width='0.50'/></td>");
            $mpdf->WriteHTML("</tr>");


            $mpdf->WriteHTML("</table>");
            $mpdf->WriteHTML("</body>");
            $mpdf->WriteHTML("</html>");

            $mpdf->WriteHTML(utf8_encode($html));//
            $mpdf->Output("cetak.pdf", 'I');
            exit;
        }else{
            echo "Data Pasien tidak ditemukan";
        }
	}
}