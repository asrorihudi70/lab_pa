<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWJBatalKunjungan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
		$common=$this->common;
		$result=$this->result;
		$title='LAPORAN BATAL KUNJUNGAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal   =$param->tgl_awal;
		$tglAkhir  =$param->tgl_akhir;
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$params['type_file'] = $param->type_file;
		if (strtolower($param->tmp_kd_unit) != 'semua' && $param->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			$criteriaUnit = "AND (kd_unit in (".$params['tmp_unit'].") OR left(kd_unit,1) = '3')";
		}else{
			$criteriaUnit = "";
		}
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table border="0" id="queryHead">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100%" height="20" border="1" cellspacing="0" style="border-collapse: collapse;">
			<thead>
				 <tr>
					<th width="5%" rowspan="2" align="center">No</th>
					<th align="center" colspan="3">Pasien</th>
					<th align="center" colspan="2">Kunjungan</th>
					<th width="10%" align="center" rowspan="2">Petugas</th>
					<th width="20%" align="center" rowspan="2">Ket</th>
				  </tr>
				 <tr>
					<th width="15%" align="center">No. Medrec</th>
					<th width="15%" align="center">Nama</th>
					<th width="15%" align="center">Poliklinik</th>
					<th width="15%" align="center">Tanggal Kunjungan</th>
					<th width="15%" align="center">Tanggal Batal</th>
				  </tr>
			</thead>';
		$query = $this->db->query("SELECT * FROM history_batal_kunjungan WHERE (tgl_batal BETWEEN '".$tglAwal."' AND '".$tglAkhir."') $criteriaUnit order by tgl_kunjungan ");
		if($query->num_rows() > 0) {
			$no=0;
			$html.='<tbody>';
			foreach($query->result_array() as $line){
				$no++;
				$html.='<tr>';
				$html.="<td style='padding-left:5px;'>".$no."</td>";
				$html.="<td style='padding-left:5px;'>".$line['kd_pasien']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['nama']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['nama_unit']."</td>";
				$html.="<td style='padding-left:5px;'>".tanggalstring(date('Y-m-d',strtotime($line['tgl_kunjungan'])))."</td>";
				$html.="<td style='padding-left:5px;'>".$line['jam_batal']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['username']."</td>";
				$html.="<td style='padding-left:5px;'>".$line['ket']."</td>";
				$html.='</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>
			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);/*
		$this->common->setPdf('L','Lap. Pasien Per Dokter',$html);	*/

		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Laporan_batal_kunjungan_".date('Y-m-d').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
		}else{
			// echo $html;
			$this->common->setPdf('L',"Laporan batal kunjungan",$html);	
		}
   	}
	
	function print_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,8,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$tglAwal   =$param->tgl_awal;
		$tglAkhir  =$param->tgl_akhir;
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$params['type_file'] = $param->type_file;
		if (strtolower($param->tmp_kd_unit) != 'semua' && $param->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			$criteriaUnit = "AND kd_unit in (".$params['tmp_unit'].")";
		}else{
			$criteriaUnit = "";
		}
		
	
			
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 17)
			->setColumnLength(4, 17)
			->setColumnLength(5, 21)
			->setColumnLength(6, 10)
			->setColumnLength(7, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 8,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 8,"left")
			->commit("header")
			->addColumn($telp, 8,"left")
			->commit("header")
			->addColumn($fax, 8,"left")
			->commit("header")
			->addColumn("LAPORAN BATAL KUNJUNGAN", 8,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 8,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("PASIEN", 3,"center")
			->addColumn("KUNJUNGAN", 2,"center")
			->addColumn("PETUGAS", 1,"left")
			->addColumn("KET", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"center")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE", 1,"left")
			->addColumn("NAMA", 1,"left")
			->addColumn("NAMA UNIT", 1,"left")
			->addColumn("TANGGAL KUNJUNGAN", 1,"left")
			->addColumn("TANGGAL BATAL", 1,"CENTER")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");
		$query = $this->db->query("SELECT * FROM history_batal_kunjungan WHERE (tgl_batal BETWEEN '".$tglAwal."' AND '".$tglAkhir."') $criteriaUnit order by tgl_kunjungan");
		if($query->num_rows() > 0) {
			$no=0;
			foreach($query->result_array() as $line){
				$no++;
				$tp	->addColumn($no, 1,"left")
					->addColumn($line['kd_pasien'], 1,"left")
					->addColumn($line['nama'], 1,"left")
					->addColumn($line['nama_unit'], 1,"left")
					->addColumn(tanggalstring(date('Y-m-d',strtotime($line['tgl_kunjungan']))), 1,"left")
					->addColumn($line['jam_batal'], 1,"left")
					->addColumn($line['username'], 1,"left")
					->addColumn($line['ket'], 1,"left")
					->commit("header");
			}
		}else {		
			$tp	->addColumn("Data tidak ada", 8,"left")
				->commit("header");
		} 
		
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_batal_kunjungan_rwj.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>