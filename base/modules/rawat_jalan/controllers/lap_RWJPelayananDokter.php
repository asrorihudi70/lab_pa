<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWJPelayananDokter extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, '.', ',');
	}
	
	function getUser()
    {
        
		$result=$this->db->query("SELECT '111' as id,kd_user, full_name FROM zusers
									UNION
									SELECT '000' as id,'000' as kd_user, 'Semua' as full_name
									ORDER BY id,full_name")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	function readAllDokter()
    {    
        if (strlen($_POST['query'])>0) {
            $query = $this->db->query("
                select DISTINCT(dokter_klinik.kd_dokter), dokter.kd_dokter, dokter.nama, dokter.jenis_dokter--, dokter_klinik.kd_unit   
                from dokter INNER JOIN 
                dokter_klinik on dokter_klinik.kd_dokter = dokter.kd_dokter 
            ".$_POST['query']."")->result();
        }else{
             $query = $this->db->query("
                select *   
                from dokter")->result();
        }
		$arr = array();
		for($i=0;$i<count($query);$i++){
			$arr[$i]['KD_DOKTER'] = $query[$i]->kd_dokter;
			$arr[$i]['NAMA'] = $query[$i]->nama;
		}
        echo '{listData:'.json_encode($arr).'}';
    }
	
	/*public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and t.kd_unit in(".substr($criteria1, 0, -1).")";
		
		if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		}

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='1'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='0'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter="";
			$dokter='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter=" AND d.kd_dokter='".$kd_dokter."'";
			$dokter=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		
		if($kd_user == '000' || $kd_user == 'Semua' || $kd_user == ''){
			$ckd_user="";
		} else{
			$ckd_user=" AND t.kd_user='".$kd_user."'";
		}
		
		if($detail == true  || $detail == 'true'){
			$criteriaHead = " t.ispay='t' And dt.Kd_kasir='".$KdKasir."' ";
		}else{
			$criteriaHead = " dt.Kd_kasir='".$KdKasir."' ";
		}

		$queryHead = $this->db->query(
			"SELECT distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where 	
											".$criteriaHead."					
											And Folio in ('A','E')				
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 				
											".$ckd_dokter." ".$ckd_customer." ".$criteria." ".$criteriaunit." ".$criteria_profesi."	".$ckd_user."				
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        ")->result();		
		//-------------JUDUL-----------------------------------------------
		if($detail == true  || $detail == 'true'){
			$html='
				<table  cellspacing="0" border="0">
						<tr>
							<th colspan="8"> LAPORAN JASA PELAYANAN '.$dokter.'</th>
						</tr>
						<tr>
							<th colspan="8"> PERIODE '.$awal.' s/d '.$akhir.'</th>
						</tr>
						<tr>
							<th colspan="8"> KELOMPOK PASIEN '.$customerfar.'</th>
						</tr>
						<tr>
							<th colspan="8"> OPERATOR '.$full_name.'</th>
						</tr>
				</table><br>';
		}else{
			$html='
				<table  cellspacing="0" border="0">
						<tr>
							<th colspan="4"> LAPORAN JASA TINDAKAN '.$dokter.'</th>
						</tr>
						<tr>
							<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
						</tr>
						<tr>
							<th colspan="4"> KELOMPOK PASIEN '.$customerfar.'</th>
						</tr>
						<tr>
							<th colspan="4"> OPERATOR '.$full_name.'</th>
						</tr>
				</table><br>';
		}
		//---------------ISI-----------------------------------------------------------------------------------
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		if($detail == true  || $detail == 'true'){
			$html.='
				<table border = "1">
				<thead>
					 <tr>
						<th width="5%" align="center">No</th>
						<th width="13%" align="center">Keterangan</th>
						<th width="13%" align="center">Jml Pasien</th>
						<th width="13%" align="center">Jml Produk</th>
						<th width="13%" align="center">Jasa Dokter</th>
						<th width="13%" align="center">Total</th>
						<th width="13%" align="center">Pajak ('.$pajaknya.'%)</th>
						<th width="13%" align="center">Total Setelah Potongan</th>
					  </tr>
				</thead>';
		}else{
			$html.='
				<table border = "1">
				<thead>
					 <tr>
						<th width="5%" align="center">No</th>
						<th width="13%" align="center">Keterangan</th>
						<th width="13%" align="center">Jml Pasien</th>
						<th width="13%" align="center">Jml Produk</th>
					  </tr>
				</thead>';
		}
		$no_set = 1;	
		$baris=0;
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			$jmllinearea=count($queryHead)+6; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($queryHead as $line){
				$no++;
				if($detail == true  || $detail == 'true'){
					$html.='<tr>
								<td style="padding-left:10px;">'.$no.'</td>
								<td align="left" colspan="7" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>
							</tr>';
				}else{
					$html.='<tr>
								<td style="padding-left:10px;">'.$no.'</td>
								<td align="left" colspan="3" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>
							</tr>';
				}
				$jmllinearea += 1;
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component in ('0', '20') THEN max(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component = 1 THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where dt.Kd_kasir='".$KdKasir."' And Folio in ('A','E') 
								AND (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." 
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						Where 
						t.ispay='t' 
						and xdt.kd_dokter='".$line->kd_dokter."' ".$criteria_profesi."
						".$ckd_customer." ".$criteriaunit." ".$ckd_user."						
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);					
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;
				
				foreach ($query2 as $line2) 
				{
					if($detail == true  || $detail == 'true'){
						$html.="<tr>";
						$html.="<td></td>";
						$tmpDesk = str_replace('&','dan',$line2->deskripsi);
						//$tmpDesk = str_replace('>','lebih dari',$line2->deskripsi);
						$tmpDesk = str_replace('<','kurang dari',$line2->deskripsi);
							$noo++;
							$total_jml_pasien += $line2->jml_pasien;
							$total_qty += $line2->qty_as;
							$total_jpdok += $line2->jpdok;
							$total_jumlah += $line2->jumlah_as;
							$total_pajak += $line2->pajak_pph_as;
							$total_all += $line2->total_as-$line2->pajak_pph_as;
							$html.="<td style='padding-left:10px;'>".$noo." - ".$tmpDesk."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok)."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jumlah_as)."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->pajak_pph_as)."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->total_as-$line2->pajak_pph_as)."</td>";
						$html.="</tr>";
						$jmllinearea += 1;
						$no_set++;
						$baris++;
					}else{
						$html.="<tr>";
						$html.="<td></td>";
						$tmpDesk = str_replace('&','dan',$line2->deskripsi);
						$tmpDesk = str_replace('<','kurang dari',$line2->deskripsi);
							$noo++;
							$total_jml_pasien += $line2->jml_pasien;
							$total_qty += $line2->qty_as;
							$total_jpdok += $line2->jpdok;
							$total_jumlah += $line2->jumlah_as;
							$total_pajak += $line2->pajak_pph_as;
							$total_all += $line2->total_as-$line2->pajak_pph_as;
							$html.="<td style='padding-left:10px;'>".$noo." - ".$tmpDesk."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
						$html.="</tr>";
						$jmllinearea += 1;
						$no_set++;
						$baris++;
					}
				}
				
				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pajak += $total_pajak;
				$all_total_all += $total_all;

				if($detail == true  || $detail == 'true'){
					$html.="<tr>";
					$html.="<td></td>";
					$html.="<td align='right' >Sub Total</td>";
					$html.="<td align='right'>".$total_jml_pasien."</td>";
					$html.="<td align='right'>".$total_qty."</td>";
					$html.="<td align='right'>".number_format($total_jpdok,0,',',',')."</td>";
					$html.="<td align='right'>".$this->rupiah($total_jumlah)."</td>";
					$html.="<td align='right'>".$this->rupiah($total_pajak)."</td>";
					$html.="<td align='right'>".$this->rupiah($total_all)."</td>";
					$html.="</tr>";
				
					$html.="<tr>";
					$html.="<td colspan='8'>&nbsp;</td>";
					$html.="</tr>";
				}else{
					$html.="<tr>";
					$html.="<td></td>";
					$html.="<td align='right' >Sub Total</td>";
					$html.="<td align='right'>".$total_jml_pasien."</td>";
					$html.="<td align='right'>".$total_qty."</td>";
					$html.="</tr>";

					$html.="<tr>";
					$html.="<td colspan='4'>&nbsp;</td>";
					$html.="</tr>";
				}
				$jmllinearea += 1;
				$no_set++;
				
			}

			if($detail == true  || $detail == 'true'){
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td align='right'>Grand Total</td>";
				$html.="<td  align='right'>".$all_total_jml_pasien."</td>";
				$html.="<td  align='right'>".$all_total_qty."</td>";
				$html.="<td  align='right'>".number_format($all_total_jpdok,0,',',',')."</td>";
				$html.="<td align='right'>".number_format($all_total_jumlah,0,',',',')."</td>";
				$html.="<td align='right'>".number_format($all_total_pajak,0,',',',')."</td>";
				$html.="<td  align='right'>".number_format($all_total_all,0,',',',')."</td>";
				$html.="</tr>";
			}else{
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td align='right'>Grand Total</td>";
				$html.="<td  align='right'>".$all_total_jml_pasien."</td>";
				$html.="<td  align='right'>".$all_total_qty."</td>";
				$html.="</tr>";
			}
			$jmllinearea = $jmllinearea + 4;
			$no_set++;
		}else {		
			$jmllinearea = 8;

			if($detail == true  || $detail == 'true'){
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="8" align="center">Data tidak ada</th>
					</tr>
				';
			}else{
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="4" align="center">Data tidak ada</th>
					</tr>
				';
			}
			$no_set++;			
		} 
		$html.='</table>';
		$baris=$baris+17;
		$prop=array('foot'=>true);
		$area_kanan='C9:H'.$baris;
		if($type_file == 1){
			$area_wrap	= 'A7:H'.((int)$no_set+12);
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:H'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	)
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						
				 	)
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, 'A1:H7');
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:H7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:H4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
           
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('pelayanan_dokter_rwj'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_pelayanan_dokter_rwj.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Lap. Jasa Pelayanan Dokter',$html);	
			echo $html;		
		}
   	}*/
   	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		// var_dump($param);
		$title = "LAPORAN KINERJA PELAKSANA PER TINDAKAN";
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kelompok  				= $param->kelompok;
		$kd_kelompok  			= $param->kd_kelompok;
		$kd_customer  			= $param->kd_customer;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("SELECT setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$criteria = " ( K.Tgl_Masuk BETWEEN '".date_format(date_create($tglAwal), 'Y-m-d')."' AND '".date_format(date_create($tglAkhir), 'Y-m-d')."' ) ";

		 //var_dump($kelompok);//die;
		if (strtoupper($kelompok) != "SEMUA") {
			if (strtoupper($kd_customer) != "SEMUA") {
				$criteria .= " AND (cus.kd_customer = '".$kd_customer."' OR cus.customer = '".$kd_customer."' ) ";
			}
			if (strtoupper($kelompok) == 'PERSEORANGAN') {
				$criteria .= " AND kon.jenis_cust = '0' ";
			}else if (strtoupper($kelompok) == 'PERUSAHAAN'){
				$criteria .= " AND kon.jenis_cust = '1' ";
			}else if (strtoupper($kelompok) == 'ASURANSI'){
				$criteria .= " AND kon.jenis_cust = '2' ";
			}
		}

		if (strlen($param->kd_unit) > 0 && isset($param->kd_unit) === true) {
			$criteria .= " AND K.kd_unit IN ( ".$param->kd_unit." ) ";
		}
		
		if (strtoupper($param->profesi) == 'DOKTER' && isset($param->profesi) === true) {
			$criteria .= " AND vd.kd_job = '1' ";
		}else{
			$criteria .= " AND vd.kd_job = '5' ";
		}

		if (strtoupper($kd_dokter) != 'SEMUA') {
			$criteria .= " and vd.kd_dokter = '".$kd_dokter."' ";
		}

		$query = "SELECT * FROM customer where kd_customer = '".$kd_customer."' OR customer = '".$kd_customer."'";
		$query = $this->db->query($query);
		$label_kelompok = "SEMUA";
		if ($query->num_rows() > 0) {
			$label_kelompok = $query->row()->customer;
		}else{
			$label_kelompok = strtoupper($kelompok);
		}

		$query = "
			SELECT
				vd.kd_dokter,
				d.nama AS nama_dokter,
				p.deskripsi  AS produk,
				sum( dt.qty ) AS jml_produk,
				count( DISTINCT ( k.kd_pasien ) ) AS jml_pasien 
			FROM 
				visite_dokter vd 
				INNER JOIN 
				transaksi t ON 
					vd.kd_kasir = t.kd_kasir AND 
					vd.no_transaksi = t.no_transaksi 
				INNER JOIN 
				detail_transaksi dt ON 
					dt.kd_kasir = vd.kd_kasir AND
					dt.no_transaksi = vd.no_transaksi AND 
					dt.urut = vd.urut AND 
					dt.tgl_transaksi = vd.tgl_transaksi 
				INNER JOIN 
				kunjungan k ON 
					k.kd_pasien = t.kd_pasien AND
					k.tgl_masuk = t.tgl_transaksi AND
					k.kd_unit = t.kd_unit AND 
					k.urut_masuk = t.urut_masuk 
				INNER JOIN 
				dokter d ON 
					d.kd_dokter = vd.kd_dokter 
				INNER JOIN kontraktor kon ON kon.kd_customer = k.kd_customer
				INNER JOIN customer cus ON cus.kd_customer = k.kd_customer
				INNER JOIN 
				produk p ON 
					p.kd_produk = dt.kd_produk
				INNER JOIN pasien on k.kd_pasien=pasien.kd_pasien
				WHERE 
		".$criteria."
		AND t.ispay='t'
		GROUP BY vd.kd_dokter, d.nama, k.kd_pasien, p.deskripsi,pasien.nama
		ORDER BY d.nama";
		
		$query = $this->db->query($query);
		$list_dokter = array();
		if(count($query->result() > 1)){
			foreach ($query->result() as $result) {
			$data = array();
			$data['kd_dokter']   =  $result->kd_dokter;
			$data['nama_dokter'] =  $result->nama_dokter;
			array_push($list_dokter, $data);	
		}

		$list_dokter 	= array_unique($list_dokter, SORT_REGULAR);
		$list_data 		= array();
		foreach ($list_dokter as $res) {
			foreach ($query->result() as $result) {
				if ($res['kd_dokter'] == $result->kd_dokter) {
					$data = array();
					$data['kd_dokter'] 	= $result->kd_dokter;
					$data['produk'] 	= $result->produk;
					$data['jml_produk']	= $result->jml_produk;
					$data['jml_pasien']	= $result->jml_pasien;
					array_push($list_data, $data);
				}
			}
		}

		$html = "";
		//-------------JUDUL-----------------------------------------------
		$html .='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="4">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="4"> KELOMPOK PASIEN '.$label_kelompok.'</th>
					</tr>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------
		$html ."";
		$html .= "<table width='100%' border='1' cellspacing='0'>";
		$nomer_head = 1;
		$html .= "<tr>";
		$html .= "<th style='padding:3px;' width='5%'></th>";
		$html .= "<th style='padding:3px;' align='center' width='75%'>PELAKSANA</th>";
		$html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Tindakan</th>";
		// $html .= "<th style='padding:3px;' align='center' width='10%'>Jumlah Pasien</th>";
		$html .= "</tr>";
		$total_produk = 0;
		$total_pasien = 0;
		foreach ($list_dokter as $res) {
			$jml_produk = 0;
			$jml_pasien = 0;
			$html .= "<tr>";
			$html .= "<th style='padding:3px;'>".$nomer_head."</th>";
			$html .= "<th style='padding:3px;' colspan='2' align='left'>".$res['kd_dokter']." - ".$res['nama_dokter']."</th>";
			$html .= "</tr>";
			foreach ($list_data as $row) {
				$nomer = 1;
					if ($row['kd_dokter'] == $res['kd_dokter'] && $row['produk'] == $row['produk']) {
						$html .= "<tr>";
						$html .= "<td></td>";
						$html .= "<td style='padding:3px;'> - ".$row['produk']."</td>";
						$html .= "<td style='padding:3px;' align='center'>".$row['jml_produk']."</td>";
						// $html .= "<td style='padding:3px;' align='center'>".$row['jml_pasien']."</td>";
						$html .= "</tr>";
						$jml_produk += $row['jml_produk'];
						$jml_pasien += $row['jml_pasien'];
						$nomer++;
					}
				}
				$html .= "<tr>";
				$html .= "<th></th>";
				$html .= "<th style='padding:3px;' align='right'>Sub Total</th>";
				$html .= "<th style='padding:3px;' align='center'>".$jml_produk."</th>";
				// $html .= "<th style='padding:3px;' align='center'>".$jml_pasien."</th>";
				$html .= "</tr>";
				$nomer_head++;
				$total_produk += $jml_produk;
				$total_pasien += $jml_pasien;
			}
			$html .= "<tr>";
			$html .= "<th></th>";
			$html .= "<th style='padding:3px;' align='right'>Grand Total</th>";
			$html .= "<th style='padding:3px;' align='center'>".$total_produk."</th>";
			// $html .= "<th style='padding:3px;' align='center'>".$total_pasien."</th>";
			$html .= "</tr>";
			$html .= "</table>";	
			//echo $html;
			// echo $html;die;
			$this->common->setPdf_penunjang('P',$title ,$html);
		}else{
			$html .= "<th style='padding:3px;' align='center' width='75%'>Data Tidak Ada</th>";
			// echo $html;die;
			$this->common->setPdf_penunjang('P',$title ,$html);
		}
			
   	}

	
}
?>