<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWJPendapatanKuitansi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetak(){
        $common=$this->common;
        $result=$this->result;
        $title='LAPORAN DAFTAR TAGIHAN PERUSAHAAN';
        $param=json_decode($_POST['data']);
        
        $kd_poli                = $param->kd_poli;
        $tglAwal                = $param->tglAwal;
        $tglAkhir               = $param->tglAkhir;
        $kd_customer            = $param->kd_kelompok;
        $shift                  = $param->shift;
        $order_by               = $param->order_by;

        $criteriaPoli="";
        for ($i=0; $i < count($kd_poli); $i++) { 
            $criteriaPoli .="'".$kd_poli[$i][0]."',";
        }

        $criteriaPoli = substr($criteriaPoli, 0, -1);

        if (strtoupper($shift) != 'SEMUA') {
            $shiftFar = "Shift = ".substr($shift, 0, -1);
            $shift = " And db.Shift In (".substr($shift, 0, -1).")";
        }else{
            $shift = " And db.Shift In ('1','2','3')";
            $shiftFar = "Semua Shift ";            
        }
        
        if(strtolower($order_by) == strtolower("Nama Pasien")  || $order_by == 0){
            $criteriaOrder = "ORDER BY P.Nama ASC";
        }else if(strtolower($order_by) == strtolower("Penjamin")  || $order_by == 2){
            $criteriaOrder = "ORDER BY c.customer ASC";
        }else{
            $criteriaOrder = "ORDER BY k.tgl_masuk ASC";
        }

        $awal   = tanggalstring(date('Y-m-d',strtotime($tglAwal)));
        $akhir  = tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
        
        if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
            $ckd_customer_far="";
            $customerfar='Semua Kelompok Customer';
        } else{
            $ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
            $customerfar=""; 
            $customerfar.="Kelompok Customer " ; 
            $customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
        }

        $queryHead = $this->db->query("SELECT 
                DISTINCT(U.kd_unit),
                U.Nama_Unit 
            FROM Kunjungan K 
            INNER JOIN Pasien P ON K.Kd_Pasien = P.Kd_Pasien  
            INNER JOIN Customer C ON K.Kd_Customer = C.Kd_Customer 
            INNER JOIN Kontraktor Kt ON C.Kd_Customer = Kt.Kd_Customer 
            INNER JOIN Transaksi T ON T.Kd_Pasien = K.Kd_Pasien AND T.Kd_Unit = K.Kd_Unit  AND K.Tgl_Masuk = T.Tgl_Transaksi 
            INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
            INNER JOIN Reg_Unit Ru ON  Ru.Kd_Pasien = T.Kd_Pasien AND Ru.Kd_Unit = T.Kd_Unit  -- No Data
            INNER JOIN  (
                SELECT Kd_Kasir, No_Transaksi, Max(No_Nota) AS No_Nota, Kd_Unit 
                FROM Nota_Bill 
                GROUP BY Kd_Kasir, No_Transaksi, Kd_Unit
                ) X ON x.no_transaksi = t.no_transaksi and x.kd_kasir = t.kd_kasir and x.kd_unit = t.kd_unit  -- No Data
            WHERE (K.Tgl_Masuk Between '".$tglAwal."'  AND '".$tglAkhir."')  
            and T.Kd_Unit in (".$criteriaPoli.") 
            ".$ckd_customer_far." 
        ");
        

        $query = $queryHead->result();          
        //-------------JUDUL-----------------------------------------------
        $html='
            <table  cellspacing="0" border="0">
                <tbody>
                    <tr>
                        <th>'.$title.'<br>
                    </tr>
                    <tr>
                        <th> Periode '.$awal.' s/d '.$akhir.'</th>
                    </tr>
                    <tr>
                        <th> Laporan Pendapatan Kuitansi dari '.$shiftFar.'</th>
                    </tr>
                </tbody>
            </table><br>';
            
        //---------------ISI-----------------------------------------------------------------------------------
        $html.='
            <table width="100%" height="20" border = "1">
            <thead>
                 <tr>
                    <th width="5%" align="center">No</th>
                    <th width="10%" align="center">Nomer Bukti</th>
                    <th width="10%" align="center">Tanggal</th>
                    <th width="10%" align="center">Nomer Registrasi</th>
                    <th width="15%" align="center">Nama Pasien</th>
                    <th width="10%" align="center">Jumlah Biaya</th>
                    <th width="10%" align="center">Potongan Peminjam</th>
                    <th width="10%" align="center">Jumlah Pembayaran</th>
                    <th width="20%" align="center">Keterangan</th>
                  </tr>
            </thead>';
            //echo count($query);
            $html.="<tbody>";
        if(count($query) > 0) {
            
            $no = 0;
            foreach($query as $line){
                $no++;
                $html.='<tr>
                            <td style="padding-left:10px;">'.$no.'</td>
                            <td align="left" colspan="8" style="padding-left:10px;"><b>'.$line->nama_unit.'</b></td>
                        </tr>';

                $queryBody = $this->db->query("SELECT 
                    U.Nama_Unit, 
                    x.No_Nota as no_nota, 
                    y.Tgl_Transaksi as tgl_transaksi,  
                    RU.No_Register as no_registrasi, 
                    P.Nama as nama_pasien, 
                    SUM(BIAYA + POTONGAN) AS jumlah_biaya, 
                    SUM(POTONGAN) AS potongan , 
                    sum(BIAYA) as jumlah_dibayar ,  
                    c.Customer 
                FROM Kunjungan K 
                INNER JOIN Pasien P ON K.Kd_Pasien = P.Kd_Pasien  
                INNER JOIN Customer C ON K.Kd_Customer = C.Kd_Customer 
                INNER JOIN Kontraktor Kt ON C.Kd_Customer = Kt.Kd_Customer 
                INNER JOIN Transaksi T ON T.Kd_Pasien = K.Kd_Pasien AND T.Kd_Unit = K.Kd_Unit  AND K.Tgl_Masuk = T.Tgl_Transaksi 
                INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
                INNER JOIN Reg_Unit Ru ON  Ru.Kd_Pasien = T.Kd_Pasien AND Ru.Kd_Unit = T.Kd_Unit  -- No Data
                INNER JOIN  (
                    SELECT Kd_Kasir, No_Transaksi, Max(No_Nota) AS No_Nota, Kd_Unit 
                    FROM Nota_Bill 
                    GROUP BY Kd_Kasir, No_Transaksi, Kd_Unit
                    ) X ON x.no_transaksi = t.no_transaksi and x.kd_kasir = t.kd_kasir and x.kd_unit = t.kd_unit  -- No Data
                    INNER JOIN (
                        SELECT 
                        t.Kd_Pasien, 
                        t.Kd_Unit, 
                        db.Kd_Pay, 
                        db.Kd_Kasir, 
                        db.No_Transaksi,  
                        db.Tgl_Transaksi, 
                        sum(CASE WHEN Kd_Pay In ('TU') and db.tag='0' THEN db.Jumlah ELSE 0 END) as biaya, 
                        sum(CASE WHEN Kd_Pay Not In ('TU') And db.Tag='0' THEN db.Jumlah ELSE 0 END) as potongan 
                        FROM Detail_Bayar db 
                            INNER JOIN Transaksi t ON db.Kd_Kasir = t.Kd_Kasir AND db.No_Transaksi = t.No_Transaksi 
                            Where t.kd_unit in (".$criteriaPoli.") 
                            And t.ispay='1'  
                            And (
                                (db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."' ".$shift.")  
                                Or  
                                (db.Tgl_Transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift=4) 
                            )  
                            group by t.Kd_Pasien, t.Kd_Unit, db.Kd_Kasir, db.No_Transaksi,  db.Tgl_Transaksi, db.Kd_Pay 
                        ) Y 
                        ON Y.Kd_Pasien = t.Kd_Pasien AND Y.Kd_Unit = t.Kd_Unit   and y.no_transaksi = t.no_transaksi 
                        INNER JOIN Payment Py ON Y.Kd_Pay = Py.Kd_Pay 
                        WHERE K.Tgl_Masuk Between '".$tglAwal."'  And '".$tglAkhir."'  
                        And T.Kd_Unit='".$line->kd_unit."'
                        ".$ckd_customer_far." 
                    GROUP BY U.Nama_Unit, x.No_Nota, y.Tgl_Transaksi, RU.No_Register, P.Nama, c.Customer
                ".$criteriaOrder."
                ");
                                        
                $query2 = $queryBody->result();
                
                //$noo = 0;
                foreach ($query2 as $line2) 
                {
                    $html.="<tr>";
                    $html.="<td></td>";
                    $html.="<td>".$line2->no_nota."</td>";
                    $html.="<td>".$line2->tgl_transaksi."</td>";
                    $html.="<td>".$line2->no_registrasi."</td>";
                    $html.="<td>".$line2->nama_pasien."</td>";
                    $html.="<td>".$line2->jumlah_biaya."</td>";
                    $html.="<td>".$line2->potongan."</td>";
                    $html.="<td>".$line2->jumlah_dibayar."</td>";
                    $html.="<td>".$line2->customer."</td>";
                    //$noo++;
                    $html.="</tr>";
                }

            }
            
        }else {     
            $html.='
                <tr class="headerrow"> 
                    <th width="" colspan="9" align="center">Data tidak ada</th>
                </tr>

            ';      
        } 

        $html.='</tbody></table>';
        $prop=array('foot'=>true);
        $this->common->setPdf('L','Lap. Tagihan Perusahaan',$html);
   	}
	
}
?>