<?php class lap_bukti_setor extends MX_Controller
{
	private $dbSQL 		= false;
	private $kd_user 	= '';
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('file');
		$this->load->model('m_bukti_setor');
		$this->kd_user = $this->session->userdata['user_id']['id'];
		//$this->user    = $this->session->userdata['user_id']['full_name'];
		//echo $this->user;
	}
	public function preview_bukti_setor($tgl_awal, $tgl_akhir, $shift_awal_1, $shift_awal_2, $shift_awal_3, $shift_akhir_1, $shift_akhir_2, $shift_akhir_3, $unit)
	{
		// echo $unit;
		// die;
		$query = '';
		$day_1 = date('d', strtotime($tgl_awal));
		$day_2 = date('d', strtotime($tgl_akhir));
		$month_1 = date('m', strtotime($tgl_awal));
		$month_2 = date('m', strtotime($tgl_akhir));
		$years_1 = date('Y', strtotime($tgl_awal));
		$years_2 = date('Y', strtotime($tgl_akhir));
		$awal = microtime(true);
		if ($unit == 'Semua') {
			$query = $this->m_bukti_setor->get_all_bukti_setor($tgl_awal, $tgl_akhir, $shift_awal_1, $shift_awal_2, $shift_awal_3, $shift_akhir_1, $shift_akhir_2, $shift_akhir_3)->result();
			//var_dump($query);	
			//echo '{success:true, lama:'.$lama;
			//var_dump($query);
		}
		if ($unit == 'Radiologi') {
			$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
			$query .= "select 'RADIOLOGI' AS DESKRIPSI,
						sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,
						sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
						sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total
						from DETAIL_TR_BAYAR_COMPONENT dtbc
						inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
						INNER JOIN jenis_component jc on jc.kd_jenis =pc.kd_jenis
						inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
						and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT where ";


			if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
				$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
			}

			if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
				$query .= ",'$shift_awal_2'";
			}

			if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
				$query .= ",'$shift_awal_3'";
			}

			$query .= ")) or ";

			for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
				$a = $i + 1;
				$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
				//echo $i.'&nbsp';
			}
			$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.kd_pay in (select p.kd_pay from payment p inner join payment_type pt on p.jenis_pay= pt.jenis_pay WHERE pt.deskripsi='TUNAI')";
		}
		if ($unit == 'Laboratorium') {
			$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
			$query .= "select 'LABORATORIUM' AS DESKRIPSI,
						sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,
						sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
						sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total
						from DETAIL_TR_BAYAR_COMPONENT dtbc
						inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
						INNER JOIN jenis_component jc on jc.kd_jenis =pc.kd_jenis
						inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
						and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT where ";


			if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
				$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
			}

			if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
				$query .= ",'$shift_awal_2'";
			}

			if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
				$query .= ",'$shift_awal_3'";
			}

			$query .= ")) or ";

			for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
				$a = $i + 1;
				$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
				//echo $i.'&nbsp';
			}
			$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and dtbc.kd_pay in (select p.kd_pay from payment p inner join payment_type pt on p.jenis_pay= pt.jenis_pay WHERE pt.deskripsi='TUNAI') ";
		}
		if ($unit == 'Rawat Jalan') {
			$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
			$query .= "select U.NAMA_UNIT DESKRIPSI ,sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
			sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total
			from DETAIL_TR_BAYAR_COMPONENT dtbc
			inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
			INNER JOIN jenis_component jc on jc.kd_jenis= pc.kd_jenis
			inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
			and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
			INNER JOIN TRANSAKSI T ON T.KD_KASIR = DT.KD_KASIR AND T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
			INNER JOIN UNIT U ON U.KD_UNIT = T.KD_UNIT 
			where dtbc.KD_KASIR ='01' AND (";
			if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
				$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
			}

			if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
				$query .= ",'$shift_awal_2'";
			}

			if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
				$query .= ",'$shift_awal_3'";
			}

			$query .= ")) or ";

			for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
				$a = $i + 1;
				$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
				//echo $i.'&nbsp';
			}
			$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1')) and dtbc.kd_pay in (select p.kd_pay from payment p inner join payment_type pt on p.jenis_pay= pt.jenis_pay WHERE pt.deskripsi='TUNAI') GROUP BY U.NAMA_UNIT";
		}
		if ($unit == 'Gawat Darurat') {
			$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
			$query .= "select U.NAMA_UNIT DESKRIPSI ,sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
			sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total
			from DETAIL_TR_BAYAR_COMPONENT dtbc
			inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
			INNER JOIN jenis_component jc on jc.kd_jenis= pc.kd_jenis
			inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
			and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
			INNER JOIN TRANSAKSI T ON T.KD_KASIR = DT.KD_KASIR AND T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
			INNER JOIN UNIT U ON U.KD_UNIT = T.KD_UNIT 
			where dtbc.KD_KASIR ='06' AND ";
			if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
				$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
			}

			if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
				$query .= ",'$shift_awal_2'";
			}

			if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
				$query .= ",'$shift_awal_3'";
			}

			$query .= ")) or ";

			for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
				$a = $i + 1;
				$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
				//echo $i.'&nbsp';
			}
			$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and  dtbc.kd_pay in (select p.kd_pay from payment p inner join payment_type pt on p.jenis_pay= pt.jenis_pay WHERE pt.deskripsi='TUNAI') GROUP BY U.NAMA_UNIT";
		}
		if ($unit == 'Rawat Inap') {
			$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
			$query .= "select U.NAMA_UNIT DESKRIPSI ,sum(case when  jc.kd_jenis ='2' then dtbc.jumlah else 0 end ) as JS,sum(case when  jc.kd_jenis ='3' then dtbc.jumlah else 0 end ) as JP,
			sum(case when dt.KD_PRODUK NOT IN (1,2,3,4) then dtbc.JUMLAH else 0 end) as Total
			from DETAIL_TR_BAYAR_COMPONENT dtbc
			inner JOIN produk_component pc on dtbc.kd_component=pc.kd_component
			INNER JOIN jenis_component jc on jc.kd_jenis= pc.kd_jenis
			inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR = dtbc.KD_KASIR  and dt.NO_TRANSAKSI = dtbc.NO_TRANSAKSI 
			and dt.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI  and dt.URUT = dtbc.URUT 
			INNER JOIN TRANSAKSI T ON T.KD_KASIR = DT.KD_KASIR AND T.NO_TRANSAKSI = DT.NO_TRANSAKSI 
			INNER JOIN UNIT U ON U.KD_UNIT = T.KD_UNIT 
			where dtbc.KD_KASIR ='02' and";
			if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
				$query .= "(dtbc.TGL_BAYAR ='$tgl_awal' and dt.shift in('$shift_awal_1'";
			}

			if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
				$query .= ",'$shift_awal_2'";
			}

			if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
				$query .= ",'$shift_awal_3'";
			}

			$query .= ")) or ";

			for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
				$a = $i + 1;
				$query .= "(dtbc.TGL_BAYAR='$years_2-$month_1-$i'::date  and dt.shift in ('1','2','3') and dtbc.TGL_BAYAR='$years_2-$month_1-$a'::date and dt.shift='4') or";
				//echo $i.'&nbsp';
			}
			$query .= "(dtbc.TGL_BAYAR='$tgl_akhir' and dt.shift ='1') and  dtbc.kd_pay in (select p.kd_pay from payment p inner join payment_type pt on p.jenis_pay= pt.jenis_pay WHERE pt.deskripsi='TUNAI') GROUP BY U.NAMA_UNIT";
		}
		if ($unit == 'Apotek') {
			$tgl_awal_tambah1 = date("d-m-Y", strtotime($tgl_awal . '+1 day'));
			$query .= "SELECT'APOTEK' AS DESKRIPSI,sum(case when  pc.kd_jenis ='2' then adb.jumlah * ac.percent_compo else 0 end ) as JP,
											 sum(case when  pc.kd_jenis ='1' then adb.jumlah * ac.percent_compo else 0 end ) as JS, 
											 '0'  as Total FROM apt_detail_bayar adb
			INNER JOIN apt_barang_out abo ON adb.tgl_out = abo.tgl_out  AND abo.no_out::CHARACTER VARYING = adb.no_out
			INNER JOIN zusers z ON abo.opr :: CHARACTER VARYING = z.kd_user
			INNER JOIN apt_component ac ON ac.kd_unit = abo.kd_unit 
			AND ac.kd_milik = z.kd_milik
			inner join produk_component pc on ac.kd_component=pc.kd_component where ";
			if ($shift_awal_1 != '' || $shift_awal_1 == NULL) {
				$query .= "(adb.TGL_BAYAR ='$tgl_awal' and adb.shift in('$shift_awal_1'";
			}

			if ($shift_awal_2 != '' || $shift_awal_2 == NULL) {
				$query .= ",'$shift_awal_2'";
			}

			if ($shift_awal_3 != '' || $shift_awal_3 == NULL) {
				$query .= ",'$shift_awal_3'";
			}

			$query .= ")) or ";

			for ($i = (int)$day_1 + 1; $i < (int)$day_2; $i++) {
				$a = $i + 1;
				$query .= "(adb.TGL_BAYAR='$years_2-$month_1-$i'::date  and adb.shift in ('1','2','3') and adb.TGL_BAYAR='$years_2-$month_1-$a'::date and adb.shift='4') or";
				//echo $i.'&nbsp';
			}
			$query .= "(adb.TGL_BAYAR='$tgl_akhir' and adb.shift ='1') and  adb.kd_pay in (select p.kd_pay from payment p inner join payment_type pt on p.jenis_pay= pt.jenis_pay WHERE pt.deskripsi='TUNAI') ";
		}
		//var_dump($data_konten);
		$data_konten = $this->db->query($query)->result();
		$akhir = microtime(true);
		$lama = $akhir - $awal;
		//	echo $lama;
		$this->cetakBiling($data_konten, $tgl_awal, $tgl_akhir);
		$content = "
			<code>
				<pre>" . htmlspecialchars(file_get_contents(base_url() . "data_bukti_setor.txt")) . "</pre>
			</code>";
		echo $content;
	}


	private function cetakBiling($data_konten, $tgl_awal, $tgl_akhir)
	{
		$biodataRS = $this->db->query("SELECT name,address,phone1,fax,city,state from db_rs")->result();
		foreach ($biodataRS as $value) {
			$name = $value->name;
			$address = $value->address;
			$phone1 = $value->phone1;
			$fax = $value->fax;
			$city = $value->city;
			$state = $value->state;
		}

		//echo count($data_header);		

		$tp         = new TableText(135, 9, '', 0, false);
		$setpage 	= new Pilihkertas;
		$tp->addColumn($name, 9, "left")->commit("body");
		$tp->setColumnLength(0, 35) 	//DESKRIPSI
			->setColumnLength(1, 50);

		$tp->addColumn($address . ' ' . $city . ' ' .	$state, 1, "left")
			//->addColumn("MADIUN", 1,"left")
			->commit("body");

		$tp->addColumn("Telepon:" . $phone1, 1, "left")
			->addColumn("Fax:" . $fax, 1, "left")
			->commit("body");

		$tp->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 50);
		$tp->setColumnLength(0, 23); 	//DESKRIPSI
		$tp->addColumn("LAPORAN BUKTI SETOR KASIR", 6, "center")->commit("body");
		$tp->addColumn("Periode :" . $tgl_awal . ' s/d ' . $tgl_akhir, 6, "left")->commit("body");
		$tp->addColumn("======================================================================================================================================", 9, "left")->commit("body");


		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 50)
			->setColumnLength(2, 20)
			->setColumnLength(3, 20)
			->setColumnLength(4, 25);
		//->setUseBodySpace(true);

		$tp->addColumn("No", 1, "left")
			->addColumn("DESKRIPSI", 1, "center")
			->addColumn("JS", 1, "center")
			->addColumn("JP", 1, "center")
			->addColumn("Total", 1, "center")
			->commit("body");
		$no = 1;
		$total_all = 0;
		$total_js = 0;
		$total_jp = 0;
		foreach ($data_konten as $value) {
			$total_all = $total_js + $total_jp;
			$total_js = $total_js + $value->js;
			$total_jp = $total_jp + $value->jp;

			$tp->addColumn($no, 1, "left")
				->addColumn($value->deskripsi, 1, "left")
				->addColumn(number_format($value->js, 0, ",", "."), 1, "right")
				->addColumn(number_format($value->jp, 0, ",", "."), 1, "right")
				->addColumn(number_format($value->total, 0, ",", "."), 1, "right")
				//number_format("1000000",2,",",".");
				->commit("body");
			$no++;
		}
		$tp->addColumn("======================================================================================================================================", 9, "left")->commit("body");

		$tp->addColumn("Total", 2, "left")
			->addColumn(number_format($total_js, 0, ",", "."), 1, "right")
			->addColumn(number_format($total_jp, 0, ",", "."), 1, "right")
			->addColumn(number_format($total_all, 0, ",", "."), 1, "right")
			->commit("body");

		$data = $tp->getText();
		/*if ( !write_file(APPPATH.'/files/data_billing.txt', $data)){
		     echo 'Unable to write the file';
		} */
		$fp = fopen("data_bukti_setor.txt", "wb");
		fwrite($fp, $data);
		fclose($fp);
		$file =  'data_bukti_setor.txt';

		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27) . chr(106) . chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed    = chr(12); # mengeksekusi $feed
		$bold1       = Chr(27) . Chr(69); # Teks bold
		$bold0       = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27) . chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$margin      = chr(27) . chr(78) . chr(90);
		$margin_off  = chr(27) . chr(79);

		// $fast_mode	 = chr(27) . chr(120) . chr(48); # fast print / low quality
		// $fast_print_on	 = chr(27) . chr(115). chr(1); # fast print on
		// $low_quality_off = chr(120) . chr(1); # low quality

		$Data  = $initialized;
		$Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
		// $Data .= $fast_mode; # fast print / low quality
		// $Data .= $low_quality_off; # low quality
		// $Data .= $fast_print_on; # fast print on / enable
		$Data .= $condensed1;
		$Data .= $margin;
		$Data .= $data;
		//$Data .= $margin_off;
		$Data .= $formfeed;

		fwrite($handle, $Data);
		fclose($handle);
		//echo $print;

		//$printer=$this->db->query("select p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';

	}
}
