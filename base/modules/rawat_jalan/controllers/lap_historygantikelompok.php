<?php

/**
 * Maya Silviana
 * 09-10-2017
 */


class lap_historygantikelompok extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$parameter = json_decode($this->input->post('data'));
		$params = array(
			'tgl_awal' 		=> date_format(date_create($parameter->tglAwal), 'Y-m-d'),
			'tgl_akhir' 	=> date_format(date_create($parameter->tglAkhir), 'Y-m-d'),
			'type_file' 	=> $parameter->type_file,
		);
		if (strtolower($parameter->tmp_kd_unit) != 'semua' && $parameter->tmp_kd_unit != '') {
			$params['tmp_unit'] = substr($parameter->tmp_kd_unit, 0, strlen($parameter->tmp_kd_unit)-1);
			$criteriaUnit = "AND u.kd_unit in (".$params['tmp_unit'].")";
		}else{
			$criteriaUnit = "";
		}
		
		$query = $this->db->query("SELECT hgc.kd_pasien, p.nama, hgc.tgl_masuk, u.nama_unit, zu.user_names, kd_customer_sebelum, kd_customer_sesudah, hgc.keterangan from 
									history_ganti_customer hgc
									INNER JOIN pasien p ON p.kd_pasien = hgc.kd_pasien 
									INNER JOIN unit u ON u.kd_unit = hgc.kd_unit 
									INNER JOIN zusers zu ON zu.kd_user = hgc.kd_user::character varying
								where 
								hgc.tgl_update >= '".$params['tgl_awal']."' 
								and hgc.tgl_update <= '".$params['tgl_akhir']."' 
								$criteriaUnit
								order by urut asc");
		$awal=tanggalstring(date('Y-m-d',strtotime($params['tgl_awal'])));
		$akhir=tanggalstring(date('Y-m-d',strtotime($params['tgl_akhir'])));
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 10)
			->setColumnLength(2, 20)
			->setColumnLength(3, 11)
			->setColumnLength(4, 19)
			->setColumnLength(5, 10)
			->setColumnLength(6, 13)
			->setColumnLength(7, 13)
			->setColumnLength(8, 20)
			->setUseBodySpace(true);
		
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city,9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN HISTORY GANTI KELOMPOK PASIEN", 9,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 9,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NO. MEDREC", 1,"left")
			->addColumn("NAMA PASIEN", 1,"left")
			->addColumn("TGL MASUK", 1,"left")
			->addColumn("UNIT", 1,"left")
			->addColumn("PETUGAS", 1,"left")
			->addColumn("KELOMPOK", 2,"center")
			->addColumn("KETERANGAN", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"center")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("SEBELUM", 1,"center")
			->addColumn("SESUDAH", 1,"center")
			->addColumn("", 1,"left")
			->commit("header");
	
		$no = 1;
		foreach ($query->result() as $result) {
			$tmp_customer_sebelum = "";
			$tmp_customer_sebelum = $this->db->query("SELECT * from  customer WHERE kd_customer = '".$result->kd_customer_sebelum."'")->row()->customer;
			$tmp_customer_sesudah = "";
			$tmp_customer_sesudah = $this->db->query("SELECT * from  customer WHERE kd_customer = '".$result->kd_customer_sesudah."'")->row()->customer;
			
			$tp	->addColumn($no.".", 1,"left")
				->addColumn($result->kd_pasien, 1,"left")
				->addColumn($result->nama, 1,"left")
				->addColumn(date_format(date_create($result->tgl_masuk), 'd/M/Y'), 1,"left")
				->addColumn($result->nama_unit, 1,"left")
				->addColumn($result->user_names, 1,"left")
				->addColumn($tmp_customer_sebelum, 1,"left")
				->addColumn($tmp_customer_sesudah, 1,"left")
				->addColumn($result->keterangan, 1,"left")
				->commit("header");
			$no++;
		}
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 7,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_history_ganti_kelompok_pasien_rwj.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>