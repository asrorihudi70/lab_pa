<?php

/**
 * Maya Silviana
 * 09-10-2017
 */


class lap_pasienperdokter_rwj extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,6,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$kd_poli   = $param->kd_poli;
		$kd_dokter = $param->kd_dokter;
		$tglAwal   = $param->tglAwal;
		$tglAkhir  = $param->tglAkhir;
		$type_file = $param->type_file;
		$order_by  = $param->order_by;
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if (strtolower($order_by) == strtolower("Medrec") || $order_by == 0) {
			$criteriaOrder = "ORDER BY p.kd_pasien ASC";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")  || $order_by == 1){
			$criteriaOrder = "ORDER BY nama_pasien ASC";
		}else if(strtolower($order_by) == strtolower("Penjamin")  || $order_by == 3){
			$criteriaOrder = "ORDER BY c.customer ASC";
		}else{
			$criteriaOrder = "ORDER BY k.tgl_masuk ASC";
		}

		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA DOKTER';
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		
		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)='2'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit = '".$kd_poli."'";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit='".$kd_poli."'")->row()->nama_unit); 
		}
		
		$queryHead = $this->db->query(
			"SELECT 
			DISTINCT(d.kd_dokter),
			d.kd_dokter as kd_dokter,
			d.nama as nama_dokter 
			from kunjungan k 
				inner join dokter d on d.kd_dokter=k.kd_dokter 
				inner join unit u on u.kd_unit=k.kd_unit 
				WHERE (k.tgl_masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$ckd_dokter_far." ".$ckd_unit_far.""
		);
		
		$query = $queryHead->result();	
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 35)
			->setColumnLength(2, 16)
			->setColumnLength(3, 15)
			->setColumnLength(4, 17)
			->setColumnLength(5, 40)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 6,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 6,"left")
			->commit("header")
			->addColumn($telp, 6,"left")
			->commit("header")
			->addColumn($fax, 6,"left")
			->commit("header")
			->addColumn("LAPORAN DAFTAR PASIEN PER DOKTER", 6,"center")
			->commit("header")
			->addColumn("Periode ".$awal." s/d ".$akhir, 6,"center")
			->commit("header")
			->addColumn ("Laporan Pasien dari ".$dokterfar." dan ".$unitfar, 6,"center")
			->commit("header");
		$tp	->addColumn("", 6,"left")
			->commit("header");
		$tp	->addColumn("NO.", 1,"center")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NAMA DOKTER/PASIEN", 1,"left")
			->addColumn("TANGGAL MASUK", 1,"left")
			->addColumn("KELOMPOK", 1,"left")
			->addColumn("UNIT", 1,"left")
			->addColumn("DIAGNOSA", 1,"left")
			->commit("header");
		
		if(count($query) > 0) {
			$no=0;
			$total=0;
			foreach($query as $line){
				$no++;
				$tp	->addColumn($line->nama_dokter, 6,"left")
					->commit("header");
				$queryBody = $this->db->query( 
					"SELECT 
						DISTINCT(p.kd_pasien),
						p.nama as nama_pasien, 
						c.customer 
						from kunjungan k 
							inner join pasien p on p.kd_pasien=k.kd_pasien 
							inner join dokter d on d.kd_dokter=k.kd_dokter 
							inner join customer c on c.kd_customer=k.kd_customer 
							inner join unit u on u.kd_unit=k.kd_unit 
						where (k.tgl_masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."')
						AND d.kd_dokter='".$line->kd_dokter."' ".$ckd_unit_far." 
						$criteriaOrder"
				);
										
				$query2       = $queryBody->result();
				$query2_count = $queryBody->num_rows();
				
				$noo=0;
				$kd_pasien="";
				foreach ($query2 as $line2) 
				{
					$noo++;
					$tp	->addColumn($noo.".", 1,"left")
						->addColumn("(".$line2->kd_pasien.") ".$line2->nama_pasien, 1,"left");
					
					$queryBodyDetail = $this->db->query( 
							"SELECT 
								DISTINCT(c.kd_customer),
								k.kd_pasien,
								k.kd_unit,
								k.kd_customer,
								k.tgl_masuk,
								k.urut_masuk,
								k.kd_customer,

								p.nama as nama_pasien,
								p.kd_pasien,

								pn.penyakit, 
								pn.kd_penyakit, 

								c.kd_customer,
								c.customer,

								d.kd_dokter,
								d.nama as nama_dokter,

								u.kd_unit,
								u.nama_unit,

								mrp.kd_pasien,
								mrp.kd_unit,
								mrp.urut_masuk,
								mrp.kd_penyakit 
								from kunjungan k 
									left join mr_penyakit mrp on 
									(mrp.kd_pasien=k.kd_pasien and mrp.kd_unit=k.kd_unit and mrp.tgl_masuk=k.tgl_masuk and mrp.urut_masuk=k.urut_masuk) 
									inner join pasien p on p.kd_pasien=k.kd_pasien 
									left join penyakit pn on pn.kd_penyakit=mrp.kd_penyakit 
									inner join customer c on c.kd_customer=k.kd_customer 
									inner join dokter d on d.kd_dokter=k.kd_dokter
									inner join unit u on u.kd_unit=k.kd_unit 
								where (k.tgl_masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								AND p.kd_pasien='".$line2->kd_pasien."' 
								AND d.kd_dokter='".$line->kd_dokter."'
								".$ckd_unit_far."
								".$criteriaOrder.""
						);			
						$query3 = $queryBodyDetail->result();
						if(count($query3) > 0) {	
							foreach ($query3 as $line3) {
								if($kd_pasien == $line3->kd_pasien && $kd_pasien != '') {	
									$tp	->addColumn("", 2,"left")
										->addColumn(tanggalstring(date('Y-m-d',strtotime($line3->tgl_masuk))), 1,"left")
										->addColumn($line3->customer, 1,"left")
										->addColumn($line3->nama_unit, 1,"left")
										->addColumn($line3->penyakit, 1,"left")
										->commit("header");
								}else{
									$tp	->addColumn(tanggalstring(date('Y-m-d',strtotime($line3->tgl_masuk))), 1,"left")
										->addColumn($line3->customer, 1,"left")
										->addColumn($line3->nama_unit, 1,"left")
										->addColumn($line3->penyakit, 1,"left")
										->commit("header");
								}
								
								$kd_pasien = $line3->kd_pasien;
							}
						}
				}
				$tp	->addColumn("", 1,"left")
					->addColumn("Jumlah Kunjungan : ".$query2_count, 5,"left")
					->commit("header");
				$total += $query2_count;
			}
			$tp	->addColumn("", 1,"left")
				->addColumn("Total Kunjungan : ".$total, 5,"left")
				->commit("header");
		}else {	
			$tp	->addColumn("Data tidak ada", 6,"left")
				->commit("header");			
		} 
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_pasienperdokter.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>