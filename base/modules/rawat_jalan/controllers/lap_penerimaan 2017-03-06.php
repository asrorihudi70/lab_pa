<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		//$type_file = $param->type_file;
		$html='';	
   		$tmpKdUnit="";
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
        }
	
        $tmpKdUnit = substr($tmpKdUnit, 0, -1); 
		$kriteria_poli = " t.Kd_Unit IN (".$tmpKdUnit.")  ";
		
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		//tindakan dan pendaftaran
		$kriteria_pend_tind="";
		if($param->tindakan0 =='true' && $param->tindakan1=='false')
		{
			$kriteria_pend_tind =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
		}else if($param->tindakan0 =='false' && $param->tindakan1 =='true'){
			$kriteria_pend_tind =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
		}else if($param->tindakan0 =='true' && $param->tindakan1=='true'){
			$kriteria_pend_tind = " and (dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
									INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	
									 or dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')) ";
		
		}else if($param->tindakan0 =='false' && $param->tindakan1 =='false'){
			$kriteria_pend_tind="";
		}
		
		//jenis pasien
		$kel_pas = $param->pasien; 
		if($kel_pas=="Semua" || $kel_pas== -1 )
		{
			$jniscus=" ";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
		}
		
		//kd_customer
	
		if($kel_pas!= -1 ){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
			}else{
				$customerx=" ";
			}
		}else{
			$customerx=" ";
		}
		
		//$type_file = $_POST['type_file'];
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='SEMUA KELOMPOK PASIEN SHIFT 1,2,3';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='SEMUA KELOMPOK PASIEN SHIFT '.$s_shift;
   				// $q_shift="And ".$q_shift;
   			}
   		}
		#ambil tanggal transaksi
		$c_query="Select distinct t.tgl_transaksi From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
					INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
							dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
							max(coalesce(nb.no_nota,null)) as no_nota
							FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
								dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
								and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
								and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
								left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
								WHERE  
								  $q_shift
								  $kriteria_bayar	
									GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
										AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											inner join dokter d on k.kd_dokter=d.kd_dokter
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
											INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
												WHERE 
												$kriteria_poli
												$kriteria_pend_tind
												$jniscus
												$customerx
												AND t.IsPay = 't'
												 AND t.Kd_Kasir = '01' 
						GROUP BY t.tgl_transaksi, U.Nama_Unit, u.kd_unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
						ORDER BY t.tgl_transaksi";
		$query_head=$this->db->query($c_query)->result();
		
		$html.='
			<table class="t2" cellspacing="0" border="0">
				<tr>
					<th colspan="7" style="font-size:16px">LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:14px">Periode '.$tgl_awal_i.' s/d '.$tgl_akhir_i.'</th>
				</tr>
			</table> <br>
			<table class="t1" border="1">
				<tr>
					<th width="30px">No.</th>
					<th width="80px" >No. Transaksi</th>
					<th width="60px" >No. Medrec</th>
					<th width="80px" >Nama Pasien</th>
					<th width="50px" >No. Kwitansi</th>
					<th width="100px" >Jenis Penerimaan</th>
					<th width="80px">Jumlah(Rp)</th>
				</tr>';
		if(count($query_head)>0)
		{
			$no=1;
			$grand_total=0;
			foreach ($query_head as $line) 
			{
				$tgl_transaksi=$line->tgl_transaksi;
				$tgl_transaksi_j=date('d-m-Y',strtotime($line->tgl_transaksi));
				#ambil unit berdasarkan tgl
				$c_query_body = "Select distinct U.Nama_Unit, u.kd_unit
										From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
												INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
														dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
														max(coalesce(nb.no_nota,null)) as no_nota
														FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
															dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
															and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
															and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
															left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
															GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																	AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																		INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																		INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																			And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																		INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																		inner join dokter d on k.kd_dokter=d.kd_dokter
																		LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																		INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																		INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																			WHERE 
																			$kriteria_poli
																			$kriteria_pend_tind
																			$jniscus
																		    $customerx
																			and t.tgl_transaksi='$tgl_transaksi'
																			and  x.Kd_Pay in (".$tmpKdPay.")
																			AND t.IsPay = 't'
																			 AND t.Kd_Kasir = '01' 
													GROUP BY t.tgl_transaksi, U.Nama_Unit, U.kd_unit,x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY U.Nama_Unit";
				$query_body=$this->db->query($c_query_body)->result();								
				$html.='<tr>
							<td align="center">  &nbsp;<b>'.$no.'.</b></td>
							<td colspan="6">  &nbsp;'.$tgl_transaksi_j.'</td>
						</tr>';
				$jumlah_total=0;
				foreach ($query_body as $line2) 
				{
					$kd_unit = $line2->kd_unit;
					$nama_unit = $line2->nama_unit;
					$html.='<tr>
								<td align="center">  &nbsp;</td>
								<td colspan="6">  &nbsp;<b>'.$nama_unit.'</b></td>
							</tr>';
					#ambil data transaksi berdasarkan tgl dan unit
					$c_query_body2="Select t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
										case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
										case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
											max(x.no_nota) as no_nota 
											From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
													INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
															dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
															max(coalesce(nb.no_nota,null)) as no_nota
															FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
																dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
																and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
																and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
																left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  	
																	GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																		AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																			INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																			INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																				And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																			INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																			inner join dokter d on k.kd_dokter=d.kd_dokter
																			LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																			INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																			INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																				WHERE 
																				t.Kd_Unit IN ('".$kd_unit."')
																				$kriteria_pend_tind
																				$jniscus
																				$customerx
																				and  x.Kd_Pay in (".$tmpKdPay.")
																				and t.tgl_transaksi='$tgl_transaksi'
																				AND t.IsPay = 't'
																				 AND t.Kd_Kasir = '01' 
														GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
														ORDER BY x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)";
					// echo $c_query_body2;
					$query_body2=$this->db->query($c_query_body2)->result();
					$no_b=1;
					$jumlah_unit=0;
					foreach ($query_body2 as $line3) 
					{
						$jumlah = $line3->ut + $line3->ssd + $line3->pt;
						$html.='<tr>
								<td align="center">  &nbsp;</td>
								<td > '.$no_b.'. '.$line3->no_transaksi.'</td>
								<td > &nbsp;'.$line3->kd_pasien.'</td>
								<td > &nbsp;'.$line3->nama.'</td>
								<td > &nbsp;'.$line3->no_nota.'</td>
								<td > &nbsp;'.$line3->uraian.'</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
						$no_b++;
						$jumlah_unit = $jumlah_unit + $jumlah;
					}
					$html.='<tr>
								<td align="center">  &nbsp;</td>
								<td colspan="5" align="right"> <b> Jumlah '.$nama_unit.' &nbsp;</b></td>
								<td width="" align="right">'.number_format($jumlah_unit,0, "." , ".").' &nbsp;</td>
							</tr>';
					$jumlah_total = $jumlah_total + $jumlah_unit;
				}
				
				
				$no++;
				$grand_total = $grand_total + $jumlah_total;
			}
			
			$html.='<tr>
						<td align="center">  &nbsp;</td>
						<td colspan="5" align="right"> <b> Jumlah Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($grand_total,0, "." , ".").' &nbsp;</td>
					</tr>';
			
		}else{
			$html.='<tr>
						<td align="center" colspan="7"> Data Tidak Ada</td>
					</tr>';
		}
		
		$html.="</table>";
		// echo $html;
		$prop=array('foot'=>true);
		// if($type_file == 1){
			// $name=' LAPORAN_PENERIMAAN.xls';
			// header("Content-Type: application/vnd.ms-excel");
			// header("Expires: 0");
			// header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			// header("Content-disposition: attschment; filename=".$name);
			// echo $html;
		// }else{
			$this->common->setPdf('P','LAPORAN_PENERIMAAN',$html);
		// }
		
   	}
}
?>