<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData_(){
		$param       = json_decode($_POST['data']);	
		$tgl_awal_i  = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal    = date('d-M-Y', strtotime($tgl_awal_i));
		$tgl_akhir   = date('d-M-Y', strtotime($tgl_akhir_i));
		$type_file   = $param->type_file;
		$order       = $param->orderBy;
		$KdKasir     = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$html        = '';	
		$tmpKdUnit   = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "".$arrayDataUnit[$i][0].",";
        }
	
        $tmpKdUnit = substr($tmpKdUnit, 0, -1); 
		$kriteria_poli = " t.Kd_Unit IN (".$tmpKdUnit.")  ";
		
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		//tindakan dan pendaftaran
		$kriteria_pend_tind="";
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_pend_tind =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_pend_tind =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$tindakan='Tindakan';
		}else{
			$kriteria_pend_tind="";
			$tindakan='Pendaftran dan Tindakan';
		}
		
		//jenis pasien
		$kel_pas = $param->pasien; 
		if($kel_pas=="Semua" || $kel_pas== -1 )
		{
			$jniscus=" ";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
		}
		
		//kd_customer
		$kel_pas_d = $param->kd_customer;
		if($kel_pas_d != 'Semua'){
			$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
			$namacustomer=$this->db->query("select customer from customer where kd_customer='".$kel_pas_d."'")->row()->customer;
		}else{
			$customerx="";
			$namacustomer="SEMUA";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='SHIFT 1,2,3';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='SHIFT '.$s_shift;
   				// $q_shift="And ".$q_shift;
   			}
   		}
		#ambil tanggal transaksi
		$c_query="SELECT distinct t.tgl_transaksi From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
					INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
							dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
							max(coalesce(nb.no_nota,null)) as no_nota
							FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
								dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
								and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
								and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
								left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
								WHERE  
								  $q_shift
								  $kriteria_bayar	
									GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
										AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											inner join dokter d on k.kd_dokter=d.kd_dokter
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
											INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
												WHERE 
												$kriteria_poli
												$kriteria_pend_tind
												$jniscus
												$customerx
												AND t.IsPay = 't'
												 AND t.Kd_Kasir = '".$KdKasir."' 
						GROUP BY t.tgl_transaksi, U.Nama_Unit, u.kd_unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
						ORDER BY t.tgl_transaksi";
						
		$query_head=$this->db->query($c_query)->result();
		
		$html.='
			<table class="t2" cellspacing="0" border="0" width="100%">
				<tr>
					<th colspan="7" style="font-size:12px">LAPORAN PENERIMAAN (Per Pasien Per '.$tindakan.')</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:11px">Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:11px">Kelompok Pasien '.$namacustomer.'</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:11px">'.$t_shift.'</th>
				</tr>
			</table>
			<table class="t1" border="1" width="100%">
				<tr>
					<th width="30px">No.</th>
					<th width="80px" >No. Transaksi</th>
					<th width="60px" >No. Medrec</th>
					<th width="80px" >Nama Pasien</th>
					<th width="50px" >No. Kwitansi</th>
					<th width="100px" >Jenis Penerimaan</th>
					<th width="80px">Jumlah(Rp)</th>
				</tr>';

		$no_set 		= 1;
		$no_head 		= 1;
		$no_body_head 	= 1;
		$baris=1;
		$array_baris = array();
		$no_body_jumlah	= 1;
		$no_body		= 1;
		$index 		 = 0;
		
		if(count($query_head)>0)
		{
			$no=1;
			$grand_total=0;
			$baris_unit 	= 1;
			$baris_pasien 	= 1;
			$baris_tindakan	= 1;
			$baris_item		= 6;
			foreach ($query_head as $line) 
			{
				$baris_item += 1;
				$tgl_transaksi=$line->tgl_transaksi;
				$tgl_transaksi_j=date('d-m-Y',strtotime($line->tgl_transaksi));
				#ambil unit berdasarkan tgl
				$baris_unit++;
				$baris++;
				$c_query_body = "Select distinct u.kd_unit,U.nama_unit,u.kd_unit as a
										From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
												INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
														dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
														max(coalesce(nb.no_nota,null)) as no_nota
														FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
															dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
															and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
															and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
															left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
															GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																	AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																		INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																		INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																			And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																		INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																		inner join dokter d on k.kd_dokter=d.kd_dokter
																		LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																		INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																		INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																			WHERE 
																			$kriteria_poli
																			$kriteria_pend_tind
																			$jniscus
																		    $customerx
																			and t.tgl_transaksi='$tgl_transaksi'
																			and  x.Kd_Pay in (".$tmpKdPay.")
																			AND t.IsPay = 't'
																			 AND t.Kd_Kasir = '".$KdKasir."' 
													GROUP BY t.tgl_transaksi, U.Nama_Unit, U.kd_unit,x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY U.nama_unit";	
				$query_body=$this->db->query($c_query_body)->result();								
				$html.='<tr>
							<th align="center">  &nbsp;<b>'.$no.'.</b></th>
							<td colspan="6">  &nbsp;'.$tgl_transaksi_j.'</td>
						</tr>';
				$jumlah_total=0;

				$no_set++;
				foreach ($query_body as $line2) 
				{
					$baris_item += 2;
					$baris++;
					$kd_unit = $line2->kd_unit;
					$nama_unit = str_replace('&','DAN',$line2->nama_unit);
					
					$html.="<tr> 
								<td>  &nbsp;</td> 
								<th colspan='6'>$nama_unit</th> 
							</tr>";
					$baris_pasien++;		
					#ambil data transaksi berdasarkan tgl dan unit
					$strOrder='U.nama_unit';
					if((int)$order==2){
						$strOrder='Ps.Nama';
					}else if((int)$order==3){
						$strOrder='no_nota';
					}else if((int)$order==4){
						$strOrder='py.Uraian';
					}
					$c_query_body2="Select t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
										case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
										case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
											max(x.no_nota) as no_nota 
											From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
													INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
															dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
															max(coalesce(nb.no_nota,null)) as no_nota
															FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
																dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
																and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
																and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
																left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  	
																	GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																		AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																			INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																			INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																				And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																			INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																			inner join dokter d on k.kd_dokter=d.kd_dokter
																			LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																			INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																			INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																				WHERE 
																				t.Kd_Unit IN ('".$kd_unit."')
																				$kriteria_pend_tind
																				$jniscus
																				$customerx
																				and  x.Kd_Pay in (".$tmpKdPay.")
																				and t.tgl_transaksi='$tgl_transaksi'
																				AND t.IsPay = 't'
																				 AND t.Kd_Kasir = '".$KdKasir."' 
														GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
														ORDER BY ".$strOrder;//x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut);
					// echo $c_query_body2;
					$query_body2=$this->db->query($c_query_body2)->result();
					$no_b=1;
					$jumlah_unit=0;
					$baris_item+=count($query_body2);
					foreach ($query_body2 as $line3) 
					{
						$baris++;
						$jumlah = $line3->ut + $line3->ssd + $line3->pt;
						$html.='<tr>
								<td align="center">  &nbsp;</td>
								<td > '.$no_b.'. '.$line3->no_transaksi.'</td>
								<td > &nbsp;'.$line3->kd_pasien.'</td>
								<td >'. wordwrap($line3->nama,15,"<br>\n").'</td>
								<td > &nbsp;'.$line3->no_nota.'</td>
								<td > &nbsp;'.$line3->uraian.'</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ",").' &nbsp;</td>
							</tr>';
						$no_b++;
						$jumlah_unit = $jumlah_unit + $jumlah;
						$no_set++;
						$no_body++;
						
						$baris_tindakan++;
					}
					$html.='<tr>
								<td align="center">  &nbsp;</td>
								<th colspan="5" align="right">Jumlah '.$nama_unit.' &nbsp;</th>
								<td width="" align="right">'.number_format($jumlah_unit,0, "." , ",").' &nbsp;</td>
							</tr>
							<tr>
								<td >&nbsp;</td>
								<td colspan="6">&nbsp;</td>
							</tr>';
					
					$jumlah_total = $jumlah_total + $jumlah_unit;
					$no_body_jumlah+=3;
					$array_baris[$index]=$baris_item;
					// $baris++;
					$index++;
					$baris_item += 1;
					//$array_baris[$index] = $baris_item;
					// $baris = $baris_item;
				}
				
				//$baris_item++;
				// $array_baris[$index]=$no_set;
				// $baris++;
				$no_set++;
				$no++;
				$grand_total = $grand_total + $jumlah_total;
				$no_body_head++;
			}
			
			$html.='<tr>
						<td align="center">  &nbsp;</td>
						<td colspan="5" align="right"> <b> JUMLAH TOTAL &nbsp;</b></td>
						<td width="" align="right">'.number_format($grand_total,0, "." , ",").' &nbsp;</td>
					</tr>';

			$no_head++;		
			// $array_baris[$index]=(int)$baris_item+1;
			$baris = (int)$baris_item+1;
		}else{
			$html.='<tr>
						<td align="center" colspan="7"> Data Tidak Ada</td>
					</tr>';
		}
		$print_area='A1:G'.$baris;
		$area_wrap='A6:G'.$baris;
		$html.="</table>";
		if($param->type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			$sharedStyle3 = new PHPExcel_Style();
			$sharedStyle4 = new PHPExcel_Style();
			$sharedStyle5 = new PHPExcel_Style();
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),/*
					'font'  => array(
						'size'  => 11,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					/* 'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$sharedStyle3->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 10,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle4->applyFromArray(
				 array(
				 	'borders' => array(
						 //'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_NONE),
						  //'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_NONE),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 10,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle5->applyFromArray(
				 array(
				 	'borders' => array(
						  //'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_NONE),
						   //'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_NONE),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 10,
						'name'  => 'Courier New'
					)
			 	)
			);
			
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle4, "G6".":G".((int)$baris));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle4, "A6".":A".((int)$baris));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".((int)$baris).":G".((int)$baris));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A6:G6");
			for ($i=0; $i < $index; $i++) { 
				$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".$array_baris[$i].":G".$array_baris[$i]);
			}
			/* for ($i=0; $i < $index; $i++) { 
				$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$array_baris[$i].":G".$array_baris[$i]);
			} */  

			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:G6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


			/* for ($i=0; $i < $index; $i++) { 
				$objPHPExcel->getActiveSheet()
							->getStyle("G")
							->getAlignment()
							->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			} */
			/* $objPHPExcel->getActiveSheet()
						->getStyle("G".((int)$baris-9).":G".(((int)$baris-9)+5))
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_rwj.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$prop=array('foot'=>true);
			$common=$this->common;
			// $this->common->setPdf('P','LAPORAN_PENERIMAAN',$html);
			echo $html;
		}
		
   	}
	
	function printData(){
		$param       = json_decode($_POST['data']);	
		$tgl_awal_i  = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal    = date('d-M-Y', strtotime($tgl_awal_i));
		$tgl_akhir   = date('d-M-Y', strtotime($tgl_akhir_i));
		$type_file   = $param->type_file;
		$order       = $param->orderBy;
		$KdKasir     = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$html        = '';	
		$tmpKdUnit   = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "".$arrayDataUnit[$i][0].",";
        }
	
        $tmpKdUnit = substr($tmpKdUnit, 0, -1); 
		$kriteria_poli = " t.Kd_Unit IN (".$tmpKdUnit.")  ";
		
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		//tindakan dan pendaftaran
		$kriteria_pend_tind="";
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_pend_tind =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_pend_tind =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$tindakan='Tindakan';
		}else{
			$kriteria_pend_tind="";
			$tindakan='Pendaftran dan Tindakan';
		}
		
		//jenis pasien
		$kel_pas = $param->pasien; 
		if($kel_pas=="Semua" || $kel_pas== -1 )
		{
			$jniscus=" ";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
		}
		
		//kd_customer
		$kel_pas_d = $param->kd_customer;
		if($kel_pas_d != 'Semua'){
			$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
			$namacustomer=$this->db->query("select customer from customer where kd_customer='".$kel_pas_d."'")->row()->customer;
		}else{
			$customerx="";
			$namacustomer="SEMUA";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='SHIFT 1,2,3';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='SHIFT '.$s_shift;
   				// $q_shift="And ".$q_shift;
   			}
   		}
		
		$strOrder='U.nama_unit';
		if((int)$order==2){
			$strOrder='Ps.Nama';
		}else if((int)$order==3){
			$strOrder='no_nota';
		}else if((int)$order==4){
			$strOrder='py.Uraian';
		}
		#ambil tanggal transaksi
		$c_query="SELECT t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
					case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
					case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
						max(x.no_nota) as no_nota 
						From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
								INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
												dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
												max(coalesce(nb.no_nota,null)) as no_nota
												FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
													dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
													and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
													and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
													left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  	
												WHERE  
												  $q_shift
												  $kriteria_bayar	
												GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty
											) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
							INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
							INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
							INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
							INNER JOIN dokter d on k.kd_dokter=d.kd_dokter
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
							INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
							INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
							WHERE 
							$kriteria_poli
							$kriteria_pend_tind
							$jniscus
							$customerx
							AND t.IsPay = '1'
							AND t.Kd_Kasir = '".$KdKasir."' 
							GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
							ORDER BY t.tgl_transaksi, ".$strOrder;
						
		$query=$this->db->query($c_query)->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$html.='
			<table class="t2" cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<th colspan="7" style="font-size:12px">LAPORAN PENERIMAAN (Per Pasien Per '.$tindakan.')</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:11px">Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:11px">Kelompok Pasien '.$namacustomer.'</th>
				</tr>
				<tr>
					<th colspan="7" style="font-size:11px">'.$t_shift.'</th>
				</tr>
			</table>
			<table class="t1" border="1" width="100%">
				<tr>
					<th width="30px">No.</th>
					<th width="80px" >No. Transaksi</th>
					<th width="60px" >No. Medrec</th>
					<th width="80px" >Nama Pasien</th>
					<th width="50px" >No. Kwitansi</th>
					<th width="100px" >Jenis Penerimaan</th>
					<th width="80px">Jumlah(Rp)</th>
				</tr>';

		$no_a=1;
		$no_b=1;
		$tmp_tgl_transaksi ='';
		$tmp_unit ='';
		$sub_total =0;
		$total =0;
		$i=0;
		$baris=6;
		$arr_border = array();
		if(count($query)>0)
		{
			foreach ($query as $line) 
			{
				$nama_unit = str_replace('&','DAN',$line->Nama_Unit);
				$tmp_unit2 = str_replace('&','DAN',$tmp_unit);
				#JIKA TANGGAL TMP TIDAK SAMA DENGAN TGL CURRENT, MAKA CETAK TGL CURRENT
				if ($tmp_tgl_transaksi != $line->tgl_transaksi ){
					if($i !=0){
							$html.='<tr>
								<td align="center"> </td>
								<th colspan="5" align="right">JUMLAH '.$tmp_unit2.' &nbsp;</th>
								<td width="" align="right">'.number_format($sub_total,0, "." , ",").' &nbsp;</td>
							</tr>
							<tr>
								<td >&nbsp;</td>
								<td colspan="6">&nbsp;</td>
							</tr>';
							$baris++;
							array_push($arr_border,$baris);
							$baris++;
						}
					$html.='<tr>
						<td align="center"> '.$no_a.'.</td>
						<td colspan="6"> '.date('d-m-Y',strtotime($line->tgl_transaksi)).'</td>
					</tr>';
					$no_a++;
					$baris++;
					#JIKA TANGGAL TMP TIDAK SAMA DENGAN TGL CURRENT NAMUN UNIT TMP TIDAK SAMA DENGAN UNIT CURRENT , MAKA CETAK UNIT CURRENT
					if($tmp_unit != $line->Nama_Unit)
					{
						
						$html.='<tr>
							<td align="center"> </td>
							<th colspan="6"  align="left">'.$nama_unit.' </th>
						</tr>';
						$no_b=1;
						$sub_total=0;
						$baris++;
					}
					#JIKA TANGGAL TMP TIDAK SAMA DENGAN TGL CURRENT NAMUN UNIT TMP SAMA DENGAN UNIT CURRENT , MAKA CETAK UNIT CURRENT
					else{
						
						
						$html.='<tr>
							<td align="center"> </td>
							<th colspan="6" align="left">'.$nama_unit.' </th>
						</tr>';
						$baris++;
						$no_b=1;
						$sub_total=0;
					}	
				}
				
				#JIKA TANGGAL TMP  SAMA DENGAN TGL CURRENT 
				else{
					#JIKA TANGGAL TMP  SAMA DENGAN TGL CURRENT NAMUN UNIT TMP TIDAK SAMA DENGAN UNIT CURRENT , MAKA CETAK UNIT CURRENT
					if($tmp_unit != $line->Nama_Unit){
						
						if($i !=0){
							$html.='<tr>
								<td align="center"> </td>
								<th colspan="5" align="right">JUMLAH '.$tmp_unit2.' &nbsp;</th>
								<td width="" align="right">'.number_format($sub_total,0, "." , ",").' &nbsp;</td>
							</tr>
							<tr>
								<td >&nbsp;</td>
								<td colspan="6">&nbsp;</td>
							</tr>';
							$baris++;
							array_push($arr_border,$baris);
							$baris++;
						}
						
						$html.='<tr>
							<td align="center"> </td>
							<th colspan="6" align="left">'.$nama_unit.' </th>
						</tr>';
						$no_b=1;
						$sub_total=0;
						$baris++;
					}
				}
				
				$jumlah = $line->UT + $line->SSD + $line->PT;
				$html.='<tr>
						<td align="center">  &nbsp;  </td>
						<td > '.$no_b.'. '.$line->No_Transaksi.'</td>
						<td > &nbsp;'.$line->kd_Pasien.'</td>
						<td >'. wordwrap($line->Nama,15,"<br>\n").'</td>
						<td > &nbsp;'.$line->no_nota.'</td>
						<td > &nbsp;'.$line->Uraian.'</td>
						<td width="" align="right">'.number_format($jumlah,0, "." , ",").' &nbsp;</td>
					</tr>';
				$sub_total = $sub_total + $jumlah;
				$total = $total + $jumlah;
				$baris++;
				
				$no_b++;
				$tmp_tgl_transaksi = $line->tgl_transaksi;
				$tmp_unit = $line->Nama_Unit;
				$i++;
				if($i == count($query)){
					$html.='<tr>
						<td align="center"> </td>
						<th colspan="5" align="right">JUMLAH '.$Nama_Unit.' &nbsp;</th>
						<td width="" align="right">'.number_format($sub_total,0, "." , ",").' &nbsp;</td>
					</tr><tr>
								<td >&nbsp;</td>
								<td colspan="6">&nbsp;</td>
							</tr>';
					$baris++;
					array_push($arr_border,$baris);
					$baris++;
				}
			}
			
			$html.='<tr>
				<td align="center"> </td>
				<th colspan="5" align="right">JUMLAH TOTAL &nbsp;</th>
				<td width="" align="right">'.number_format($total,0, "." , ",").' &nbsp;</td>
			</tr>
			<tr>
				<td >&nbsp;</td>
				<td colspan="6">&nbsp;</td>
			</tr>';
			$baris++;
		}else{
			$html.='<tr>
						<td align="center" colspan="7"> Data Tidak Ada</td>
					</tr>';
			$baris++;
		}
		
		$print_area='A1:G'.$baris;
		$area_wrap='A6:G'.$baris;
		$html.="</table>";
		if($param->type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			$sharedStyle3 = new PHPExcel_Style();
			$sharedStyle4 = new PHPExcel_Style();
			$sharedStyle5 = new PHPExcel_Style();
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					
			 	)
			);
			$sharedStyle3->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 10,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle4->applyFromArray(
				 array(
				 	'borders' => array(
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 10,
						'name'  => 'Courier New'
					)
			 	)
			);
			$sharedStyle5->applyFromArray(
				 array(
				 	'borders' => array(
						  'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						  'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),
					'font'  => array(
						'bold'  => true,
						'size'  => 10,
						'name'  => 'Courier New'
					)
			 	)
			);
			
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle4, "G6".":G".((int)$baris));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle4, "A6".":A".((int)$baris));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".((int)$baris).":G".((int)$baris));
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A6:G6");
			for ($i=0; $i < count($arr_border); $i++) { 
				$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".$arr_border[$i].":G".$arr_border[$i]);
			}
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:G6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle('E')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_rwj.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$prop=array('foot'=>true);
			$common=$this->common;
			$this->common->setPdf('P','LAPORAN_PENERIMAAN',$html);
			echo $html;
		}
	}
	
	function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$users=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param       = json_decode($_POST['data']);	
		$tgl_awal_i  = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal    = date('d-M-Y', strtotime($tgl_awal_i));
		$tgl_akhir   = date('d-M-Y', strtotime($tgl_akhir_i));
		$type_file   = $param->type_file;
		$order       = $param->orderBy;
		$KdKasir     = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$html        = '';	
		$tmpKdUnit   = "";
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "".$arrayDataUnit[$i][0].",";
        }
	
        $tmpKdUnit = substr($tmpKdUnit, 0, -1); 
		$kriteria_poli = " t.Kd_Unit IN (".$tmpKdUnit.")  ";
		
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		//tindakan dan pendaftaran
		$kriteria_pend_tind="";
		if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_pend_tind =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_pend_tind =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$tindakan='Tindakan';
		}else{
			$kriteria_pend_tind="";
			$tindakan='Pendaftran dan Tindakan';
		}
		
		//jenis pasien
		$kel_pas = $param->pasien; 
		if($kel_pas=="Semua" || $kel_pas== -1 )
		{
			$jniscus=" ";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
		}
		
		//kd_customer
		$kel_pas_d = $param->kd_customer;
		if($kel_pas_d != 'Semua'){
			$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
			$namacustomer=$this->db->query("select customer from customer where kd_customer='".$kel_pas_d."'")->row()->customer;
		}else{
			$customerx="";
			$namacustomer="SEMUA";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='SHIFT 1,2,3';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='SHIFT '.$s_shift;
   				// $q_shift="And ".$q_shift;
   			}
   		}
		
		$strOrder='U.nama_unit';
		if((int)$order==2){
			$strOrder='Ps.Nama';
		}else if((int)$order==3){
			$strOrder='no_nota';
		}else if((int)$order==4){
			$strOrder='py.Uraian';
		}
		#ambil tanggal transaksi
		$c_query="Select t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
					case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
					case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
						max(x.no_nota) as no_nota 
						From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
								INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
												dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
												max(coalesce(nb.no_nota,null)) as no_nota
												FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
													dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
													and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
													and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
													left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  	
												WHERE  
												  $q_shift
												  $kriteria_bayar	
												GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty
											) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
							INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
							INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
							INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
							INNER JOIN dokter d on k.kd_dokter=d.kd_dokter
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
							INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
							INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
							WHERE 
							$kriteria_poli
							$kriteria_pend_tind
							$jniscus
							$customerx
							AND t.IsPay = 't'
							AND t.Kd_Kasir = '".$KdKasir."' 
							GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
							ORDER BY t.tgl_transaksi, ".$strOrder;
						
		$query=$this->db->query($c_query)->result();
		
		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 3)
			->setColumnLength(1, 15)
			->setColumnLength(2, 15)
			->setColumnLength(3, 35)
			->setColumnLength(4, 13)
			->setColumnLength(5, 18)
			->setColumnLength(6, 20)
			->setUseBodySpace(true);
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 7,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 7,"left")
			->commit("header")
			->addColumn($telp, 7,"left")
			->commit("header")
			->addColumn($fax, 7,"left")
			->commit("header")
			->addColumn("LAPORAN PENERIMAAN (Per Pasien Per ".$tindakan.")", 7,"center")
			->commit("header")
			->addColumn("Periode ".$tgl_awal." s/d ".$tgl_akhir, 7,"center")
			->commit("header")
			->addColumn("Kelompok Pasien ".$namacustomer, 7,"center")
			->commit("header")
			->addColumn($t_shift, 7,"center")
			->commit("header");
		$tp	->addColumn("", 7,"left")
			->commit("header");
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NO. TRANSAKSI", 1,"left")
			->addColumn("NO. MEDREC", 1,"left")
			->addColumn("NAMA PASIEN", 1,"center")
			->addColumn("NO. KWITANSI", 1,"center")
			->addColumn("JENIS PENERIMAAN", 1,"left")
			->addColumn("JUMLAH(Rp)", 1,"right")
			->commit("header");
		$no_a=1;
		$no_b=1;
		$tmp_tgl_transaksi ='';
		$tmp_unit ='';
		$sub_total =0;
		$total =0;
		$i=0;
		foreach ($query as $line) 
		{
			
			#JIKA TANGGAL TMP TIDAK SAMA DENGAN TGL CURRENT, MAKA CETAK TGL CURRENT
			if ($tmp_tgl_transaksi != $line->tgl_transaksi ){
				if($i !=0){
						$tp	->addColumn("", 1,"left")
							->addColumn("JUMLAH ".$tmp_unit, 5,"right")
							->addColumn(number_format($sub_total,0, "." , ","), 1,"right")
							->commit("header");
						$tp	->addColumn("", 7,"left")
							->commit("header");
				}
				$tp	->addColumn($no_a.".", 1,"left")
					->addColumn(date('d-m-Y',strtotime($line->tgl_transaksi)), 6,"left")
					->commit("header");
				$no_a++;
				
				#JIKA TANGGAL TMP TIDAK SAMA DENGAN TGL CURRENT NAMUN UNIT TMP TIDAK SAMA DENGAN UNIT CURRENT , MAKA CETAK UNIT CURRENT
				if($tmp_unit != $line->nama_unit)
				{
					$tp	->addColumn("", 1,"left")
						->addColumn($line->nama_unit, 6,"left")
						->commit("header");
					$no_b=1;
					$sub_total=0;
					
				}
				#JIKA TANGGAL TMP TIDAK SAMA DENGAN TGL CURRENT NAMUN UNIT TMP SAMA DENGAN UNIT CURRENT , MAKA CETAK UNIT CURRENT
				else{
					$tp	->addColumn("", 1,"left")
						->addColumn($line->nama_unit, 6,"left")
						->commit("header");
					
					$no_b=1;
					$sub_total=0;
				}	
			}
			
			#JIKA TANGGAL TMP  SAMA DENGAN TGL CURRENT 
			else{
				#JIKA TANGGAL TMP  SAMA DENGAN TGL CURRENT NAMUN UNIT TMP TIDAK SAMA DENGAN UNIT CURRENT , MAKA CETAK UNIT CURRENT
				if($tmp_unit != $line->nama_unit){
					
					if($i !=0){
						$tp	->addColumn("", 1,"left")
							->addColumn("JUMLAH ".$tmp_unit, 5,"right")
							->addColumn(number_format($sub_total,0, "." , ","), 1,"right")
							->commit("header");
						$tp	->addColumn("", 7,"left")
							->commit("header");
					}
					
					$tp	->addColumn("", 1,"left")
						->addColumn($line->nama_unit, 6,"left")
						->commit("header");
					$no_b=1;
					$sub_total=0;
					
				}
			}
			
			$jumlah = $line->ut + $line->ssd + $line->pt;
			$tp	->addColumn("", 1,"left")
				->addColumn($no_b.". ".$line->no_transaksi, 1,"left")
				->addColumn($line->kd_pasien, 1,"left")
				->addColumn($line->nama, 1,"left")
				->addColumn($line->no_nota, 1,"left")
				->addColumn($line->uraian, 1,"left")
				->addColumn(number_format($jumlah,0, "." , ","), 1,"right")
				->commit("header");
			$sub_total = $sub_total + $jumlah;
			$total = $total + $jumlah;
			
			$no_b++;
			$tmp_tgl_transaksi = $line->tgl_transaksi;
			$tmp_unit = $line->nama_unit;
			$i++;
			if($i == count($query)){
				$tp	->addColumn("", 1,"left")
					->addColumn("JUMLAH ".$tmp_unit, 5,"right")
					->addColumn(number_format($sub_total,0, "." , ","), 1,"right")
					->commit("header");
				$tp	->addColumn("", 7,"left")
					->commit("header");
			}
		}
		$tp	->addColumn("", 7,"left")
			->commit("header");
		$tp	->addColumn("", 1,"left")
			->addColumn("JUMLAH TOTAL", 5,"right")
			->addColumn(number_format($total,0, "." , ","), 1,"right")
			->commit("header");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$users, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_penerimaan_rwj.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}

	public function get_unit(){
			$kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$explode_kd_unit = explode(',',$kd_unit);
		$arr_unit= array();
		if(count($explode_kd_unit)==1){
			$unit= $this->db->query("select nama_unit from unit where kd_unit=$kd_unit ")->result();
			foreach( $unit as $line){
				$kd_unit_c = $line->nama_unit;
			}
			$arr_unit[0]['kd_unit'] = $kd_unit;
			$arr_unit[0]['nama_unit'] = $kd_unit_c;
		}else{
			$index = 0;
			for($i=1; $i<=count($explode_kd_unit) ;$i++){
				if (substr($explode_kd_unit[$i-1],1,1) == '2') {
					$arr_unit[$index]['kd_unit'] = $explode_kd_unit[$i-1];
					$kd_unit_b = $explode_kd_unit[$i-1];
					$unit= $this->db->query("SELECT nama_unit, kd_unit from unit where kd_unit=$kd_unit_b")->result();
					foreach( $unit as $line){
						$kd_unit_c = $line->nama_unit;
					}
					$arr_unit[$index]['nama_unit'] 	= $kd_unit_c;
					$index++;
				}
			}
		}
		
		echo '{success:true, totalrecords:'.count($arr_unit).', listData:'.json_encode($arr_unit).'}';
	}

	public function get_unit_lap(){
		$kd_unit = $this->input->post('kd_unit');
		$query=$this->db->query("SELECT * FROM unit WHERE parent='0' and kd_unit in ('".$kd_unit."')")->result();
		echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	}
}
?>