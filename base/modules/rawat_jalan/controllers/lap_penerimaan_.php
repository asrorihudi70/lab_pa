<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		// $jsonResult['processResult']='SUCCESS';
   		// $jsonResult['data']=$result;
   		// echo json_encode($jsonResult);
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
   	}
   
   	public function printData(){
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$td_t_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->setting;
   		$td_t_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->setting;
   		$td_t_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->setting;
   		
   		$td_n_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->setting;
   		$td_n_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->setting;
   		$td_n_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->setting;
   		
   		$td_i_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->setting;
   		$td_i_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->setting;
   		$td_i_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->setting;
   		
   		$this->load->library('m_pdf');
   		$this->m_pdf->load();
   		$mpdf= new mPDF('utf-8', 'A4');
   		$mpdf->SetDisplayMode('fullpage');
   		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
   		$mpdf->pagenumPrefix = 'Hal : ';
   		$mpdf->pagenumSuffix = '';
   		$mpdf->nbpgPrefix = ' Dari ';
   		$mpdf->nbpgSuffix = '';
   		$date = date("d-M-Y / H:i");
   		$arr = array (
   				'odd' => array (
   						'L' => array (
   								'content' => 'Operator : '.$this->db->query("SELECT user_names FROM zusers where kd_user='".$this->session->userdata['user_id']['id']."'")->row()->user_names,
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'C' => array (
   								'content' => "Tgl/Jam : ".$date."",
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'R' => array (
   								'content' => '{PAGENO}{nbpg}',
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'line' => 0,
   						),
   						'even' => array ()
   				);
   		$mpdf->SetFooter($arr);
   		$mpdf->SetTitle('LAP PENERIMAAN (Per Pasien Per Jenis Penerimaan)');
   		$q_dokter='';
   		if(isset($_POST['kd_dokter'])){
   			$q_dokter="AND k.kd_dokter='".$_POST['kd_dokter']."' ";
   		}
   		$q_jenis='';
   		if($_POST['pasien']>=0 && $_POST['pasien']!='Semua'){
   			$q_jenis="AND Ktr.Jenis_cust=".$_POST['pasien']." ";
   		}

   		$q_customer='';
   		if(isset($_POST['kd_customer'])){
   			$q_customer="AND Ktr.kd_customer='".$_POST['kd_customer']."' ";
   		}

         $q_poliklinik='';
         if(isset($_POST['kd_klinik']) &&  $_POST['kd_klinik']!='Semua'){
            $q_poliklinik=" t.kd_unit='".$_POST['kd_klinik']."' ";
         }else{
            $user = $this->session->userdata['user_id']['id'];
            $query= $this->db->query("SELECT kd_unit FROM zusers where kd_user = '".$user."'");
            $q_poliklinik= " t.kd_unit in (".$query->row()->kd_unit.") ";
         }

   		$q_pembayaran='';
         if($_POST['pasien']>=0 && $_POST['pasien']!='Semua'){
   			$q_pembayaran="AND db.kd_pay='".$_POST['kd_pay']."' ";
   		}

         $q_autocas='';
         if (isset($_POST['autocas'])) {
            if($_POST['autocas'] == 1){
               $q_autocas=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
            } else if($_POST['autocas'] == 2){
               $q_autocas=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
            } else if($_POST['autocas'] == 3 || $_POST['autocas'] == 4){
               $q_autocas="";
            }
         }

   		$q_shift='';
   		$t_shift='';
   		if($_POST['shift0']=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$_POST['start_date']."'  And '".$_POST['last_date']."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."'  And '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='SEMUA KELOMPOK PASIEN SHIFT 1,2,3';
   		}else{
   			if($_POST['shift1']=='true' || $_POST['shift2']=='true' || $_POST['shift3']=='true'){
   				$s_shift='';
   				if($_POST['shift1']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($_POST['shift2']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($_POST['shift3']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$_POST['start_date']."'  And '".$_POST['last_date']."'  And db.Shift In (".$s_shift.")";
   				if($_POST['shift3']=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."'  And '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='SEMUA KELOMPOK PASIEN SHIFT '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
         /*$queri="Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama,  
            case when py.jenis_pay =2 then textcat(py.Uraian,COALESCE(db.no_kartu,''))  else py.Uraian END AS uraian,  
            case when max(py.Kd_pay) in ('IA','TU') Then Sum(Jumlah) Else 0 end AS UT,  
            case when max(py.Kd_pay) in ('00','D1') Then Sum(Jumlah) Else 0 end AS SSD,  
            case when max(py.Kd_pay) not in ('IA','TU','00','D1','KR') Then Sum(Jumlah) Else 0 end AS PT,  
            case when max(py.jenis_pay) in ('2') Then Sum(Jumlah) Else 0 end AS KR
         From (Transaksi t 
            INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
            INNER JOIN Payment py On py.Kd_pay=db.Kd_Pay  
            INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
            INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
            LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
               INNER JOIN unit u On u.kd_unit=t.kd_unit  
         Where 
             t.ispay='1' AND u.kd_bagian='2' 
               ".$q_poliklinik." 
            ".$q_shift."         
            ".$q_jenis."   
            ".$q_customer."   
            ".$q_dokter."
            ".$q_pembayaran."
         Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian ,db.no_kartu ,py.jenis_pay  
         Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut) ";*/
         // echo $queri;
   		$queri="
            SELECT t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi as no_transaksi, 
            t.kd_Pasien as kd_pasien, nama=Max(ps.Nama), py.Uraian, 
            ut=case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(x.Jumlah) Else 0 end,  
            ssd=case when max(py.Kd_pay) in ('J1','J2','J3','S1','S2') Then Sum(x.Jumlah) Else 0 end,  
            pt=case when max(py.Kd_pay) not in ('DP','IA','TU')   And max(py.Kd_pay) not in ('J1','J2','J3','S1','S2')  Then Sum(x.Jumlah) Else 0 end  ,
            max(x.no_nota) as no_nota 
            From Transaksi t 
            INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
            INNER JOIN  (
            SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah  , max(isnull(nb.no_nota,'')) as no_nota 
            FROM Detail_Bayar db  
            INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay 
            inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
            left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
            WHERE   
            ".$q_shift."
            ".$q_pembayaran." 
            GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
            INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
            INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
            INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
            LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
            INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
            INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk WHERE 
            ".$q_poliklinik." 
            ".$q_dokter." 
            AND t.IsPay = 1 
            AND t.Kd_Kasir = '01'  and Ktr.Jenis_cust=0  And ktr.kd_Customer='0000000001'  
            GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian  ORDER BY U.Nama_Unit, 
            Ps.Nama, x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)";
   		// echo $queri;
   		
         //$result=$this->db->query($queri)->result();
   		$result= _QMS_Query($queri)->result();
   		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
   		$mpdf->pagenumPrefix = 'Hal : ';
   		$mpdf->pagenumSuffix = '';
   		$mpdf->nbpgPrefix = ' Dari ';
   		$mpdf->nbpgSuffix = '';
   		$mpdf->WriteHTML("
           <style>
   				
           </style>
           ");
   		 $telp='';
   		 $fax='';
   		 if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($rs->phone1 != null && $rs->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$rs->phone1;
   		 	}
   		 	if($rs->phone2 != null && $rs->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$rs->phone2.'.';
   		 		}else{
   		 			$telp.=$rs->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($rs->fax != null && $rs->fax != ''){
   		 	$fax='<br>Fax. '.$rs->fax.'.';
   		 	
   		 }
   		$mpdf->WriteHTML("
   				<table style='width: 1000px;font-size: 15;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
   					<tr>
   						<td align='left' colspan='2'>
   							<table  cellspacing='0' border='0'>
   								<tr>
   									<td>
   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
   									</td>
   									<td>
   										<b>".$rs->name."</b><br>
			   							<font style='font-size: 11px;'>".$rs->address."</font>
			   							<font style='font-size: 11px;'>".$telp."</font>
			   							<font style='font-size: 11px;'>".$fax."</font>
   									</td>
   								</tr>
   							</table>
   						</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'></td>
   					</tr>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;font-size: 12px;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;font-size: 12px;'>".$t_shift."</td>
   					</tr>
   				</tbody>
   			</table><br>
   							<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
   								<thead>
   									<tr>
   										<th rowspan='2' width='40' align='center'>NO.</th>
				   						<th rowspan='2' width='80'>NO. TRANSAKSI</th>
				   						<th rowspan='2' width='80'>NO. MEDREC</th>
				   						<th rowspan='2' width='250'>NAMA PASIEN</th>
				   						<th colspan='4'>JENIS PENERIMAAN</th>
				   						<th rowspan='2' width='100' align='right'>JUMLAH</th>
   									</tr>
									<tr>
   										<th width='100'>TUNAI</th>
				   						<th width='100'>PIUTANG</th>
				   						<th width='100' >SUBSIDI RS</th>
				   						<th width='100'>CREDIT CARD</th>
   									</tr>
   								</thead>
   								
   				");
   			$jum=0;
   			$ut=0;
   			$ssd=0;
   			$pt=0;
   			$kr=0;
   			for($i=0; $i<count($result); $i++){
   				$j=$result[$i]->ut+$result[$i]->ssd+$result[$i]->pt+$result[$i]->kr;
   				$ut+=$result[$i]->ut;
   				$ssd+=$result[$i]->ssd;
   				$pt+=$result[$i]->pt;
   				$kr+=$result[$i]->kr;
   				$jum+=$j;
   				$mpdf->WriteHTML("<tr>
   						<td align='center'>".($i+1)."</td>
   						<td align='center'>".$result[$i]->no_transaksi."</td>
   						<td align='center'>".$result[$i]->kd_pasien."</td>
   						<td>".$result[$i]->nama."</td>
   						<td align='right'>".number_format($result[$i]->ut,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->pt,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->ssd,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->kr,0,',','.')."</td>
   						<td align='right'>".number_format($j,0,',','.')."</td>
   					</tr>");
   			}
			//<td>".$result[$i]->uraian."</td>
   			if(count($result)==0){
   				$mpdf->WriteHTML("<tr>
   						<td align='center' colspan='6'>Tidak Ada Data</td>
   					</tr>");
   			}
   			$mpdf->WriteHTML("
				   					<tr>
				   						<td align='right' colspan='4' style='font-weight: bold;'>JUMLAH &nbsp;</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($ut,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($pt,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($ssd,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($kr,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($jum,0,',','.')."</td>
				   					</tr>
				   					<tbody>
					   			</tbody>
					   		</table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanRWJ';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$jsonResult['processResult']='SUCCESS';
   		//$jsonResult['list']=$result;
   		$jsonResult['data']=base_url().$tmpbase.$datenow.$tmpname.'.pdf';
   		echo json_encode($jsonResult);
   	}
	
	public function cetak(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$criteria = $param->criteria;
		$type_file = $param->type_file;
		$split= explode("##@@##",$criteria,11);
		
		if(count($split) == 11){
			$tgl_awal_i = str_replace("/","-", $split[0]);
			$tgl_akhir_i = str_replace("/","-", $split[1]) ;
			$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
			$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
			
			$kd_poli = $split[2];
			$kel_pas = $split[3];
			$kel_pas_d = $split[4];
			$shift =  $split[5];
			$shiftx = $split[6];
			$pendaftaran = $split[7];
			$tindakan = $split[8];
			$kd_dokter = $split[9];
			$kd_bayar = $split[10];
			
		}else{
			$tgl_awal_i = str_replace("/","-", $split[0]);
			$tgl_akhir_i = str_replace("/","-", $split[1]) ;
			$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
			$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
			
			$kd_poli = $split[2];
			$kel_pas = $split[3];
			$kel_pas_d = $split[4];
			$shift =  $split[5];
			$shiftx = "";
			$pendaftaran = $split[6];
			$tindakan = $split[7];
			$kd_dokter = $split[8];
			$kd_bayar = $split[9];
		}
		//$awal=tanggalstring(date('Y-m-d',strtotime($tgl_awal)));
		$tglsum2= date('Y-m-d', strtotime('+1 days', strtotime($tgl_awal)));
		$tglsummax2=date('Y-m-d', strtotime('+1 days', strtotime($tgl_akhir)));
		
		//echo $tgl_awal." ". $tgl_akhir."<br/>";
		//echo $tglsum2." ". $tglsummax2."<br/>";
		$shiftxx = substr($shift, 0, -1);
		if($shiftx == ''){
			$kriteria_tgl=" ((db.tgl_transaksi between '$tgl_awal'  And '$tgl_akhir'  And db.Shift In ($shiftxx)) ) ";
		}else{
			$kriteria_tgl=" ((db.tgl_transaksi between '$tgl_awal'  And '$tgl_akhir'  And db.Shift In (1,2,3))  
								Or  (db.Tgl_Transaksi between '$tglsum2'  And '$tglsummax2'  And db.Shift=4) ) ";
		}
		
		if($kd_bayar == 'Semua'){
			$kriteria_bayar = " And (db.Kd_Pay in ('AS','AR','AX','BN','BP','BJ','BI','BA','DP','DC','J1','EM','GS','IJ','IP','IS','IM','IT','IU','IA','JD','JP','JT','JL','JK','JS','RP','HT','PM','PR','G1','G2','G3','G4','G5','G6','PT','PU','G7','AN','IN','TS','KA','PL','TL','R1','R2','J2','KR','J3','SD','S1','S2','T1','TR','RS','TP','TU')) ";
		}else{
			$kriteria_bayar =" And (db.Kd_Pay in ('".$kd_bayar."')) ";
		}
		
		if($kd_poli == 'Semua'){
			$kriteria_poli = " t.Kd_Unit IN ('221','205','202','214','212','223','213','224','249','260','269','274','270','275','276','268','265','263','259','262','261','273','267','271','272','266','264','246','218','211','217','251','208','206','201 ','216','254','204','203','219','258','210','253','257','256','215','235','209','231','237','207','220','255') ";
		}else{
			$kriteria_poli = " t.Kd_Unit IN ('".$kd_poli."')  ";
		}
		
		if($pendaftaran == "ya" && $tindakan == "tidak"){
			$kriteria_pend_tind =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
		}else if($pendaftaran == "tidak" && $tindakan == "ya"){
			$kriteria_pend_tind =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
		}else if($pendaftaran == "ya" && $tindakan == "ya"){
			$kriteria_pend_tind = " and (dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
									INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	
									 or dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									  From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')) ";
		}else{
			$kriteria_pend_tind ="";
		}
		
		if($kel_pas==="Semua")
		{
			$jniscus=" ";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
		}
		if ($kel_pas_d==="NULL")
		{
			$customerx=" ";
		}else{
			$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
		}
		
		if($kd_dokter == 'Semua'){
			$kriteria_dokter=" ";
		}else{
			$kriteria_dokter=" and k.kd_dokter='".$kd_dokter."' ";
		}
		$query_head=$this->db->query("Select distinct t.tgl_transaksi From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
												INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
														dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
														max(coalesce(nb.no_nota,null)) as no_nota
														FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
															dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
															and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
															and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
															left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
															WHERE  
															  $kriteria_tgl
															  $kriteria_bayar	
																GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																	AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																		INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																		INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																			And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																		INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																		inner join dokter d on k.kd_dokter=d.kd_dokter
																		LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																		INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																		INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																			WHERE 
																			$kriteria_poli
																			$kriteria_pend_tind
																			$kriteria_dokter
																			$jniscus
																		    $customerx
																			AND t.IsPay = 't'
																			 AND t.Kd_Kasir = '01' 
													GROUP BY t.tgl_transaksi, U.Nama_Unit, u.kd_unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY t.tgl_transaksi")->result();
   		//echo '{success:true, totalrecords:'.count($query_head).', listData:'.json_encode($query_head).'}';
		$html='';
		if($type_file == 1){
			$style_title="font-size:18px;";
			$style_title2="font-size:15px;";
		}else{
			$style_title="font-size:13px;";
			$style_title2="font-size:11px;";
		}
		$html.='
			
			<table class="t2" cellspacing="0" border="0">
				<tr>
					<th colspan="7" style="'.$style_title.'">LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
				</tr>
				<tr>
					<th colspan="7" style="'.$style_title2.'">Periode '.$tgl_awal_i.' s/d '.$tgl_akhir_i.'</th>
				</tr>
			</table> <br>
			<table class="t1" border="1">
				<tr>
					<th width="30px">No.</th>
					<th width="80px" >No. Transaksi</th>
					<th width="60px" >No. Medrec</th>
					<th width="80px" >Nama Pasien</th>
					<th width="50px" >No. Kwitansi</th>
					<th width="100px" >Jenis Penerimaan</th>
					<th width="80px">Jumlah(Rp)</th>
				</tr>';
		if(count($query_head)>0)
		{
			$no=1;
			$grand_total=0;
			foreach ($query_head as $line) 
			{
				$tgl_transaksi=$line->tgl_transaksi;
				$tgl_transaksi_j=date('d-m-Y',strtotime($line->tgl_transaksi));
				$query_body=$this->db->query("Select distinct U.Nama_Unit, u.kd_unit
										From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
												INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
														dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
														max(coalesce(nb.no_nota,null)) as no_nota
														FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
															dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
															and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
															and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
															left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
															WHERE  
															  db.tgl_transaksi='$tgl_transaksi'
															  $kriteria_bayar	
																GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																	AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																		INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																		INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																			And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																		INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																		inner join dokter d on k.kd_dokter=d.kd_dokter
																		LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																		INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																		INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																			WHERE 
																			$kriteria_poli
																			$kriteria_pend_tind
																			$kriteria_dokter
																			$jniscus
																		    $customerx
																			AND t.IsPay = 't'
																			 AND t.Kd_Kasir = '01' 
													GROUP BY t.tgl_transaksi, U.Nama_Unit, U.kd_unit,x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
													ORDER BY U.Nama_Unit")->result();
													
				$html.='<tr>
							<td align="center">  &nbsp;<b>'.$no.'.</b></td>
							<td colspan="6">  &nbsp;'.$tgl_transaksi_j.'</td>
						</tr>';
				$jumlah_total=0;
				foreach ($query_body as $line2) 
				{
					$kd_unit = $line2->kd_unit;
					$nama_unit = $line2->nama_unit;
					$html.='<tr>
							<td align="center">  &nbsp;</td>
							<td colspan="6">  &nbsp;<b>'.$nama_unit.'</b></td>
						</tr>';
					$query_body2=$this->db->query("Select t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian, 
								case when max(py.Kd_pay) in ('IA','TU') Then Sum(x.Jumlah) Else 0 end as UT,  case when max(py.Kd_pay) in ('J2') Then Sum(x.Jumlah) Else 0 end as SSD,  
								case when max(py.Kd_pay) not in ('IA','TU')   And max(py.Kd_pay) not in ('J2')  Then Sum(x.Jumlah) Else 0 end as PT ,
									max(x.no_nota) as no_nota 
									From Transaksi t INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi 
											INNER JOIN  (SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, 
													dtb.tgl_transaksi, dtb.kd_Pay, (dtb.Jumlah) as jumlah, 
													max(coalesce(nb.no_nota,null)) as no_nota
													FROM Detail_Bayar db  INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir  and 
														dtb.no_transaksi = db.no_transaksi and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
														and dtb.kd_pay = db.kd_pay inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  
														and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  
														left join nota_bill nb on nb.kd_kasir = dtb.kd_kasir and  nb.no_transaksi = dtb.no_transaksi  
														WHERE  
														 db.tgl_transaksi='$tgl_transaksi'
														  $kriteria_bayar	
															GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah, dt.qty) X ON dt.Kd_Kasir = X.Kd_Kasir 
																AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut 
																	INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay  
																	INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit 
																		And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi   
																	INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
																	inner join dokter d on k.kd_dokter=d.kd_dokter
																	LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
																	INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit 
																	INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk 
																		WHERE 
																		t.Kd_Unit IN ('".$kd_unit."')
																		$kriteria_pend_tind
																		$kriteria_dokter
																		$jniscus
																		$customerx
																		AND t.IsPay = 't'
																		 AND t.Kd_Kasir = '01' 
												GROUP BY t.tgl_transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
												ORDER BY x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)")->result();
					$no_b=1;
					$jumlah_unit=0;
					foreach ($query_body2 as $line3) 
					{
						$jumlah = $line3->ut + $line3->ssd + $line3->pt;
						$html.='<tr>
								<td align="center">  &nbsp;</td>
								<td > '.$no_b.'. '.$line3->no_transaksi.'</td>
								<td > &nbsp;'.$line3->kd_pasien.'</td>
								<td > &nbsp;'.$line3->nama.'</td>
								<td > &nbsp;'.$line3->no_nota.'</td>
								<td > &nbsp;'.$line3->uraian.'</td>
								<td align="right"> &nbsp;'.$jumlah.'&nbsp;</td>
							</tr>';
						$no_b++;
						$jumlah_unit = $jumlah_unit + $jumlah;
					}
					$html.='<tr>
								<td align="center">  &nbsp;</td>
								<td colspan="5" align="right"> <b> Jumlah '.$nama_unit.' &nbsp;</b></td>
								<td align="right"> &nbsp;'.$jumlah_unit.'&nbsp;</td>
							</tr>';
					$jumlah_total = $jumlah_total + $jumlah_unit;
				}
				
				
				$no++;
				$grand_total = $grand_total + $jumlah_total;
			}
			
			$html.='<tr>
						<td align="center">  &nbsp;</td>
						<td colspan="5" align="right"> <b> Jumlah Total &nbsp;</b></td>
						<td align="right"> &nbsp;'.$grand_total.'&nbsp;</td>
					</tr>';
			
		}
		
		$html.="</table>";
		//echo $html;
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='LAPORAN_PENERIMAAN.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('P','LAPORAN_PENERIMAAN',$html);
		}
		
	}
}
?>