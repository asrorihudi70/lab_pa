<?php

/**
 * Maya Silviana
 * 09-10-2017
 */


class lap_register_summary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak_direct(){
		ini_set('display_errors', '1');
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		$tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1);
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 18)
			->setColumnLength(2, 13)
			->setColumnLength(3, 6)
			->setColumnLength(4, 6)
			->setColumnLength(5, 5)
			->setColumnLength(6, 5)
			->setColumnLength(7, 10)
			->setColumnLength(8, 12)
			->setColumnLength(9, 10)
			->setColumnLength(10, 16)
			->setColumnLength(11, 10)
			->setColumnLength(12, 5)
			->setUseBodySpace(true);
		
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 13,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 13,"left")
			->commit("header")
			->addColumn($telp, 13,"left")
			->commit("header")
			->addColumn($fax, 13,"left")
			->commit("header")
			->addColumn("LAPORAN SUMMARY PASIEN RAWAT JALAN",13,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 13,"center")
			->addSpace("header")
			->commit("header");
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NAMA UNIT", 1,"left")
			->addColumn("JUMLAH PASIEN", 1,"left")
			->addColumn("JENIS KELAMIN", 2,"center")
			->addColumn("KUNJUNGAN", 2,"center")
			->addColumn("PERUSAHAAN", 1,"left")
			->addColumn("ASURANSI", 4,"center")
			->addColumn("UMUM", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("P", 1,"center")
			->addColumn("L", 1,"center")
			->addColumn("BARU", 1,"left")
			->addColumn("LAMA", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("BPJS NON PBI", 1,"left")
			->addColumn("BPJS PBI", 1,"left")
			->addColumn("BTM/ PM JAMKESDA ", 1,"left")
			->addColumn("LAIN-LAIN", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");	
			
		

		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
       
		
		if($q->num_rows == 0)
        {
            $tp	->addColumn("Data tidak ada", 13,"center")
				->commit("header");
        }else {
		
			$query = $q->result();
			
			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			$no=0;
			foreach ($query as $line) 
			{
			   $no++;
			   $tp	->addColumn(($no).".", 1)
					->addColumn($line->namaunit2, 1,"left")
					->addColumn($line->jumlahpasien, 1,"center")
					->addColumn($line->lk, 1,"center")
					->addColumn($line->pr, 1,"center")
					->addColumn($line->br, 1,"center")
					->addColumn($line->lm, 1,"center")
					->addColumn($line->perusahaan, 1,"center")
					->addColumn($line->non_pbi, 1,"center")
					->addColumn($line->pbi, 1,"center")
					->addColumn($line->jamkesda, 1,"center")
					->addColumn($line->lain_lain, 1,"center")
					->addColumn($line->umum, 1,"center")
					->commit("header");
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_br         += $line->br;
				$total_lm         += $line->lm;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			 $tp	->addColumn("Jumlah", 2,"left")
					->addColumn($total_pasien, 1,"center")
					->addColumn($total_lk, 1,"center")
					->addColumn($total_pr, 1,"center")
					->addColumn($total_br, 1,"center")
					->addColumn($total_lm, 1,"center")
					->addColumn($total_perusahaan, 1,"center")
					->addColumn($total_non_pbi, 1,"center")
					->addColumn($total_pbi, 1,"center")
					->addColumn($total_jamkesda, 1,"center")
					->addColumn($total_lain, 1,"center")
					->addColumn($total_umum, 1,"center")
					->commit("header");
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 7,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_register_summary_rwj.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>