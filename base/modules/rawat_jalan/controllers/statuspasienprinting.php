<?php
class statuspasienprinting extends MX_Controller{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
		$this->load->view('main/index');
    }
	public function printBukti($kdpasien,$unit='2'){
		$this->load->library('common');
		$sql = $this->db->query("SELECT TOP 1 t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = '0' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
			,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
			,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan,k.kd_unit,k.sub_unit,
			d.nama AS dokter,(SELECT TOP 1 p.kd_penyakit+' - '+pe.penyakit FROM mr_penyakit p INNER JOIN penyakit pe ON pe.kd_penyakit=p.kd_penyakit WHERE p.kd_unit=k.kd_unit AND p.kd_pasien=k.kd_pasien AND p.tgl_masuk=k.tgl_masuk AND p.urut_masuk=k.urut_masuk ) as enyakit,
			(SELECT TOP 1 no_urut FROM antrian_poliklinik ap WHERE ap.kd_unit=k.kd_unit AND ap.kd_pasien=k.kd_pasien AND ap.tgl_transaksi=k.tgl_masuk ) as no_urut, k.no_sjp
				from pasien t
				INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
				INNER JOIN unit u on u.kd_unit=k.kd_unit
				INNER JOIN agama ag on ag.kd_agama=t.kd_agama
				INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
				INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
				INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
				INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
				INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
				INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
				INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
				INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
				INNER JOIN customer c on k.kd_customer = c.kd_customer 
				INNER JOIN dokter d on d.kd_dokter = k.kd_dokter 
				left JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
			LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk						
			WHERE t.KD_PASIEN = '".$kdpasien."' and left(k.kd_unit,1)='".$unit."' ORDER BY k.tgl_masuk desc,k.jam_masuk desc ")->row();
		if($sql){
			$hubungan='Keluarga';
			$instalasi='Rawat Jalan';
			if($unit=='3'){
				$instalasi='Rawat Darurat';
			}
			$instalas='Rawat Jalan';
			if($sql->hubungan==1){
				$hubungan='Anak Angkat';
			}else if($sql->hubungan==2){
				$hubungan='Anak Kandung';
			}else if($sql->hubungan==3){
				$hubungan='Anak Tiri';
			}else if($sql->hubungan==4){
				$hubungan='Ayah';
			}else if($sql->hubungan==5){
				$hubungan='Cucu';
			}else if($sql->hubungan==6){
				$hubungan='Famili';
			}else if($sql->hubungan==7){
				$hubungan='Ibu';
			}else if($sql->hubungan==8){
				$hubungan='Istri';
			}else if($sql->hubungan==9){
				$hubungan='Kep. Keluarga';
			}else if($sql->hubungan==10){
				$hubungan='Mertua';
			}else if($sql->hubungan==11){
				$hubungan='Pembantu';
			}else if($sql->hubungan==12){
				$hubungan='Suami';
			}else if($sql->hubungan==13){
				$hubungan='Sdr Kandung';
			}else if($sql->hubungan==14){
				$hubungan='Lain-Lain';
			}
			if ($sql->STATUS_MARITA==0){
				$status_marita='Blm Menikah';
			}else if ($sql->STATUS_MARITA==1){
				$status_marita='Menikah';
			}else if ($sql->STATUS_MARITA==2){
				$status_marita='Janda';
			}else if ($sql->STATUS_MARITA==3){
				$status_marita='Duda';
			}
			$now=new DateTime();
			$tgl_masuk=new DateTime($sql->TGL_MASUK);
			$jam_masuk=new DateTime($sql->JAM_MASUK);
			$tgl_lahir=new DateTime($sql->TGL_LAHIR);
			$birthDt = new DateTime($sql->TGL_LAHIR);
			$jenis_kelamin='Pria';
			if($sql->JENIS_KELAMIN=='0'){
				$jenis_kelamin='Wanita';
			}
			$today = new DateTime('today');
			$y = $today->diff($birthDt)->y;
			$m = $today->diff($birthDt)->m;
			$d = $today->diff($birthDt)->d;
			$jam = "";

			if ($tgl_masuk->format('H:i:s') == "00:00:00") {
				$jam = date("H:i:s");
			}else{
				$jam = $tgl_masuk->format('H:i:s');
			}

			$sub_instalasi 	= "";
			$no_urut 		= $sql->no_urut;

			$sub_instalasi 	= $sql->nama_unit;
			$query = $this->db->query("SELECT * FROM sys_setting where key_data = 'sub_unit'");
			if ($query->num_rows() > 0) {
				$query = $query->row()->setting;

				$query = explode("#", $query);
				foreach ($query as $key => $value) {
					if ($sql->kd_unit == $value) {
						$params_sub = array(
							'sub_unit' 		=> $sql->sub_unit,
							'kd_unit' 		=> $value,
							'waktu_masuk' 	=> date("Y-m-d"),
						);

						$this->db->select(" count(*) as count ");
						$this->db->where($params_sub);
						$this->db->from("kunjungan_sub");
						$query = $this->db->get();
						if ($query->num_rows() > 0) {
							$no_urut = $query->row()->count;
						}

					}
				}
			}
			$umur= $y . " th " . $m . " bl " . $d . " hr";
			$html='<html>
				<head>
					<style>
						.b{
							font-weight: bold;
						}
						.f10{
							font-size: 10px;
						}
						.f11{
							font-size: 11px;
						}
						.f13{
							font-size: 13px;
						}
						.f15{
							font-size: 15px;
						}
						.f17{
							font-size: 17px;
						}
						.f20{
							font-size: 20px !important;
						}
						.f25{
							font-size: 25px;
						}
						.u{
							text-decoration: underline;
						}
						.i{
							font-style: italic;
						}
						.center{
							text-align:center;
						}
						.left{
							text-align:left;
						}
						@media print {
							html, body {
								display: block !important; 
								font-family: "Calibri" !important;
								margin: 0 !important;
							}

							@page {
							  size: 21.59cm 13.97cm !important;
							}

							.logo {
							  width: 30% !important;
							}

						}
					</style>
				</head>
				<body>
					<table>
						<tr>
							<td>
								<img src="./ui/images/Logo/LOGO.png" width="100" height="100" />
							</td>
							<th class="left">
								Rumah Sakit Suaka Insan,  <br>
								JL H. ZAFRI ZAM ZAM NO 60, BANJARMASIN<br>
								KALIMANTAN SELATAN
							</th>
						</tr>
					</table>
					<table width="100%" class="f15">
						<tr>
							<th class="left" width="170">No. Registrasi</th>
							<td>: '.$sql->no_transaksi.'</td>
							<th class="left" width="120">No. RM</th>
							<th class="left">: '.$sql->KD_PASIEN.'</th>
						</tr>
						<tr>
							<th class="left">Tgl. Registrasi</th>
							<td>: '.$tgl_masuk->format('d/m/Y')." ".$jam.'</td>
							<th class="left">Nama</th>
							<th class="left">: '.$sql->NAMA.'</th>
						</tr>
						<tr>
							<th class="left">Instalasi</th>
							<td>: '.$instalasi.'</td>
							<th class="left">Umur</th>
							<th class="left">: '.$umur.'</th>
						</tr>
						<tr>
							<th class="left">Sub. Instalasi</th>
							<td>: '.$sql->nama_unit.'</td>
							<th class="left">Jns Kel</th>
							<td>: '.$jenis_kelamin.'</td>
						</tr>
						<tr>
							<th class="left">Detail Sub. Instalasi</th>
							<td>: '.$sql->sub_unit.'</td>
							<th class="left">Alamat</th>
							<td>: '.$sql->ALAMAT.'</td>
						</tr>
						<tr>
							<th class="left">Nama Dokter(DPJP)</th>
							<td>: '.$sql->dokter.'</td>
							<th class="left">Nama Ibu</th>
							<td>: '.$sql->NAMA_IBU.'</td>
						</tr>
						<tr>
							<th class="left">Cara Bayar</th>
							<td>: '.$sql->CUSTOMER.'</td>
							<th class="left">Diagnosa</th>
							<td>: '.$sql->PENYAKIT.'</td>
						</tr>
						<tr>
							<th class="left">No. Kartu / SEP</th>
							<td>: '.$sql->NO_ASURANSI.' / '.$sql->no_sjp.'</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="left" valign="top" height="62"><b>BUKTI PENDAFTARAN PASIEN</b></td>
							<td valign="top"></td>
							<td valign="top">
								<b><u>Kelengkapan :</u></b><br>
								[ ] S E P<br>
								[ ] S P B K<br>
								[ ] K I A<br>
							</td>
							<td valign="top">
								<b><u></u></b><br>
								[ ] Rujukan / Resume<br>
								[ ] Surat Kontrol<br>
								[ ] Asesmen Dokter<br>
							</td>
						</tr>
						<tr>
							<td valign="top">Padang, '.$now->format('d-m-Y').'</td>
							<td valign="top"></td>
							<td valign="top"></td>
							<td valign="top"></td>
						</tr>
						<tr>
							<td colspan="2">'.$this->session->userdata['user_id']['username'].'</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
					';
					if($sql->KD_CUSTOMER!=='0000000001'){
						$html.='<table width="100%" style="margin-top: 100px;" class="f15">
							<tr>
								<th class="left i" colspan="5">Rumah Sakit Suaka Insan, JL H. ZAFRI ZAM ZAM NO 60, BANJARMASIN, KALIMANTAN SELATAN</th>
							</tr>
							<tr>
								<th class="left" width="170">No. Registrasi</th>
								<td>: '.$sql->no_transaksi.'</td>
								<th class="left" width="120">No. RM</th>
								<th class="left">: '.$sql->KD_PASIEN.'</th>
							</tr>
							<tr>
								<th class="left">Tgl. Registrasi</th>
								<td>: '.$tgl_masuk->format('d/m/Y')." ".$jam.'</td>
								<th class="left">Nama</th>
								<th class="left">: '.$sql->NAMA.'</th>
							</tr>
							<tr>
								<th class="left">Instalasi</th>
								<td>: Rawat Jalan</td>
								<th class="left">Umur</th>
								<th class="left">: '.$umur.'</th>
							</tr>
							<tr>
								<th class="left" style="border-top: 1px dashed black;border-bottom: 1px dashed black;padding:4px;"  width="120" colspan="4">Diagnosa [ICD10]</th>
								<th class="left"  style="border-top: 1px dashed black;border-bottom: 1px dashed black;padding:4px;" width="80">Kode</th>
							</tr>
							<tr>
								<td class="left" style="padding:6px;" colspan="5">Diagnosa Utama</td>
							</tr>
							<tr>
								<td class="left" style="padding:6px;" colspan="5">Diagnosa Tambahan</td>
							</tr>
							<tr><th style="padding:6px;" colspan="5">&nbsp;</th></tr>
							<tr>
								<th class="left" style="border-top: 1px dashed black;border-bottom: 1px dashed black;padding:4px;"  width="120" colspan="4">Tindakan / Prosedur [ICD9-CM]</th>
								<th class="left"  style="border-top: 1px dashed black;border-bottom: 1px dashed black;padding:4px;" width="80">Kode</th>
							</tr>
							<tr>
								<td class="left" style="padding:6px;" colspan="5">Tindakan Utama</td>
							</tr>
							<tr>
								<td class="left" style="padding:6px;" colspan="5">Tindakan Tambahan</td>
							</tr>
							<tr><th style="padding:6px;" colspan="5">&nbsp;</th></tr>
						</table>
						<table width="100%" class="f15">
							<tr>
								<th class="left" colspan="2">Kesimpulan Akhir :</th>
								<td width="300">Kalimantan Selatan, '.$now->format('d-m-Y').'</td>
							</tr>
							<tr>
								<td width="300">[ &nbsp;] Rawat Inap</td>
								<td width="100">Coder</td>
								<td>Dokter Penanggung</td>
							</tr>
							<tr>
								<td width="300">[ &nbsp;] Kotrol Ulang Tgl. .../.../......</td>
							</tr>
							<tr>
								<td width="300">[ &nbsp;] Rujuk</td>
								<td width="100"></td>
								<td></td>
							</tr>
							<tr>
								<td width="300">[ &nbsp;] Intern</td>
								<td width="100">-----------------</td>
								<td>-----------------</td>
							</tr>
						</table>';
					}
			$html.='
				</body>
			</html>';
		}else{
			$html="<center>Data Tidak Ada</center>";
		}
		$this->common->setPdf3('P','Kartu Bukti',$html,array('paper'=>'Legal','header'=>false,'foot'=>false,'margin-top'=>3,'margin-left'=>3,'margin-right'=>3,'margin-bottom'=>3));
	}
	public function printPernyataan($kdpasien){
		$this->load->library('common');
		$sql = $this->db->query("SELECT t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = '0' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
			,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
			,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri,tr.no_transaksi,pj.nama_pj,pj.telepon as telepon_pj,pj.alamat as alamat_pj,pj.hubungan
				from pasien t
				INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
				INNER JOIN unit u on u.kd_unit=k.kd_unit
				INNER JOIN agama ag on ag.kd_agama=t.kd_agama
				INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
				INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
				INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
				INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
				INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
				INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
				INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
				INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
				INNER JOIN customer c on k.kd_customer = c.kd_customer 
				left JOIN transaksi tr on tr.kd_pasien = k.kd_pasien AND tr.kd_unit=k.kd_unit AND tr.tgl_transaksi=k.tgl_masuk
				LEFT JOIN penanggung_jawab pj ON pj.kd_pasien=k.kd_pasien AND pj.kd_unit=k.kd_unit AND pj.tgl_masuk=k.tgl_masuk AND pj.urut_masuk=k.urut_masuk						
			WHERE t.KD_PASIEN = '".$kdpasien."' and left(k.kd_unit,1)='1' ORDER BY tgl_masuk desc,jam_masuk desc limit 1")->row();
		if($sql){
			$hubungan='Keluarga';
			if($sql->hubungan==1){
				$hubungan='Anak Angkat';
			}else if($sql->hubungan==2){
				$hubungan='Anak Kandung';
			}else if($sql->hubungan==3){
				$hubungan='Anak Tiri';
			}else if($sql->hubungan==4){
				$hubungan='Ayah';
			}else if($sql->hubungan==5){
				$hubungan='Cucu';
			}else if($sql->hubungan==6){
				$hubungan='Famili';
			}else if($sql->hubungan==7){
				$hubungan='Ibu';
			}else if($sql->hubungan==8){
				$hubungan='Istri';
			}else if($sql->hubungan==9){
				$hubungan='Kep. Keluarga';
			}else if($sql->hubungan==10){
				$hubungan='Mertua';
			}else if($sql->hubungan==11){
				$hubungan='Pembantu';
			}else if($sql->hubungan==12){
				$hubungan='Suami';
			}else if($sql->hubungan==13){
				$hubungan='Sdr Kandung';
			}else if($sql->hubungan==14){
				$hubungan='Lain-Lain';
			}
			if ($sql->status_marita==0){
				$status_marita='Blm Menikah';
			}else if ($sql->status_marita==1){
				$status_marita='Menikah';
			}else if ($sql->status_marita==2){
				$status_marita='Janda';
			}else if ($sql->status_marita==3){
				$status_marita='Duda';
			}
			$now=new DateTime();
			$tgl_masuk=new DateTime($sql->tgl_masuk);
			$jam_masuk=new DateTime($sql->jam_masuk);
			$tgl_lahir=new DateTime($sql->tgl_lahir);
			$birthDt = new DateTime($sql->tgl_lahir);
			$jenis_kelamin='L';
			if($sql->jenis_kelamin=='f'){
				$jenis_kelamin='P';
			}
			$today = new DateTime('today');
			$y = $today->diff($birthDt)->y;
			$m = $today->diff($birthDt)->m;
			$d = $today->diff($birthDt)->d;
			$umur= $y . " th " . $m . " bl " . $d . " hr";
			$html='<html>
				<head>
					<style>
						body {
							font-family: Arial;
						}
						.b{
							font-weight: bold;
						}
						.f10{
							font-size: 10px;
						}
						.f11{
							font-size: 11px;
						}
						.f15{
							font-size: 15px;
						}
						.f20{
							font-size: 20px;
						}
						.f25{
							font-size: 25px;
						}
						.u{
							text-decoration: underline;
						}
						.i{
							font-style: italic;
						}
						.center{
							text-align:center;
						}
						.left{
							text-align:left;
						}
					</style>
				</head>
				<body>
					<img src="./ui/images/Logo/Header.jpeg" height="70" />
					<table width="100%">
						<tr>
							<td colspan="5" class="u center">SURAT PERNYATAAN</td>
						</tr>
						<tr>
							<th colspan="5" class="left">Saya yang bertanda tangan dibawah ini:</th>
						</tr>
						<tr>
							<td width="30">&nbsp;</td>
							<td width="140">Nama PJ</td>
							<td colspan="3">: '.$sql->nama_pj.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Hubungan</td>
							<td colspan="3">: '.$hubungan.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>No. Telp</td>
							<td colspan="3">: '.$sql->telepon_pj.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Alamat PJ</td>
							<td colspan="3">: '.$sql->alamat_pj.'</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<th colspan="5" class="left">'.$hubungan.' dari pasien atas nama:</th>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>No. RM / No. Reg</td>
							<td colspan="3">: '.$sql->kd_pasien.' / '.$sql->no_transaksi.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Nama</td>
							<td colspan="3">: '.$sql->nama.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Tempat/Tgl. Lahir</td>
							<td colspan="3">: '.$sql->tempat_lahir.' / '.$tgl_lahir->format('d-m-Y').'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Dirawat Di</td>
							<td colspan="3">: '.$sql->nama_unit.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>No. Telp</td>
							<td colspan="3">: '.$sql->telepon.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>NIK</td>
							<td colspan="3">: '.$sql->nik.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Alamat</td>
							<td colspan="3">: '.$sql->alamat.' Kota. '.$sql->kota.' kode Pos. '.$sql->kd_pos.'</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<th colspan="5" class="left">Meyatakan Bahwa:</th>
						</tr>
						<tr>
							<td valign="top">1.</td>
							<td colspan="4">Saya bersedia melengkapi berkas - berkas yang diminta oleh RS SUAKA INSAN
								dalam waktu paling lambat dari 3x24 jam, atau pulang sebelum 2 x 24 jam. Harus melengkapi syarat-syarat sebagai berikut :<br>
								&nbsp;&nbsp;&nbsp;a) Foto Copy Kartu BPJS (3 rangkap)<br>
								&nbsp;&nbsp;&nbsp;b) Foto Copy KTP (3 rangkap)<br>
								&nbsp;&nbsp;&nbsp;c) Foto Copy KK jika Kartu Tanda Penduduk tidak ada (3 rangkap)<br>
							</td>
						</tr>
						<tr>
							<td valign="top">2.</td>
							<td colspan="4"><span class="b">Untuk pasien Kecelakaan Lalu Lintas, </span>Saya bersedia memberikan <span class="b">surat jaminan Jasa Raharja</span> untuk
								Kecelakaan ganda atau <span class="b">Laporan Polisi</span> yang menyatakan kecelakaan tunggal dalam waktu
								paling lambat dari 2 x 24 jam, atau pulang sebelum 2 x 24 jam. Dan jika Tagihan RS Melebihi jaminan 
								Jasa Raharja (Rp. 20.000.000-), saya bersedia melengkapi point No. 1. Atau bersedia menanggung
								seluruh tagihan RS tsb.
							</td>
						</tr>
						<tr>
							<td valign="top">3.</td>
							<td colspan="4"><span class="b">Untuk pasien Pengurusan Denda, </span>Saya bersedia melakukan pengurusan pembayaran <span class="b">premi BPJS beserta DENDA</span>
								dalam waktu 2 x 24 jam, atau pulang sebelum 2 x 24 jam.
							</td>
						</tr>
						<tr>
							<td valign="top">4.</td>
							<td colspan="4">Apabila saya tidak mematuhi ayat <span class="b">1, 2 atau 3, </span>saya bersedia menanggung seluruh tagihan RS.</td>
						</tr>
						<tr>
							<td valign="top">5.</td>
							<td colspan="4"><span class="b">Akibat pilihan bebas pasien/keluarga pasien, </span>tanpa paksaan/bujukan/rayuan dariu pihak manapun
								untuk naik kelas lebih tinggi dari hak kelas rawatan menurut BPJS Kesehatan (Hak Kelas BPJS:.......)
								kelas yang ditempati sekarang (Kelas:...............) dan akan melunasi selisih biaya rawatan terserbut pada
								saat pasien diperbolehkan pulang dari RS.
							</td>
						</tr>
						<tr>
							<td valign="top">6.</td>
							<td colspan="4">Untuk Priode Rawatan saat ini, Saya <span class="b">tidak menggunakan BPJS/KIS/AsKes sebagai penjamin pengobatan </span>
								karena alasan tertentu. Dan saya bersedia menanggung seluruh tagihan RS.
							</td>
						</tr>
						<tr>
							<td valign="top">7.</td>
							<td colspan="4">Bersedia memperbaiki <span class="b">DATA YANG BERBEDA</span> dalam waktu 2 x 24 jam, atau pulang sebelum 2 x 24
								jam. Jika tidak, saya bersedia menanggung seluruh tagihan RS.
							</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="5">Demikian surat pernyataan ini saya buat untuk dapat digunakan sebagaimana mestinya.</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td>Padang, '.$now->format('d-m-Y').'</td>
						</tr>
						<tr>
							<td colspan="3">Saksi 1</td>
							<td>Saksi 2</td>
							<td>Yang Membuat Pernyataan</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" class="i">( Nama Lengkap )</td>
							<td class="i">( Nama Lengkap )</td>
							<th class="left">('.$sql->nama_pj.')</th>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="5" class="f11">*) Silahkan dilingkar sesuai kebutuhan</td>
						</tr>
					</table>
					
				</body>
			</html>';
		}else{
			$html='<center>Data Tidak Ada</center>';
		}
		$this->common->setPdf3('P','Surat Pernyataan',$html,array('header'=>false,'foot'=>false,'margin-top'=>10,'margin-left'=>10,'margin-right'=>10,'margin-bottom'=>10));
	}
    public function printTracerBaru($kdpasien){
		$this->load->library('common');
		$sql = $this->db->query("SELECT top 1 t.*, k.KD_RUJUKAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = '0' THEN '(B)' ELSE '' END as BARU,u.nama_unit,ag.agama,pk.pekerjaan,pd.pendidikan,pdA.pendidikan AS pendidikan_ayah
			,pdI.pendidikan AS pendidikan_ibu,pdSI.pendidikan AS pendidikan_suamiistri,pkA.pekerjaan AS pekerjaan_ayah
			,pkI.pekerjaan AS pekerjaan_ibu,pkSI.pekerjaan AS pekerjaan_suamiistri
			from pasien t
			INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
			INNER JOIN unit u on u.kd_unit=k.kd_unit
			INNER JOIN agama ag on ag.kd_agama=t.kd_agama
			INNER JOIN pendidikan pd on pd.kd_pendidikan=t.kd_pendidikan
			INNER JOIN pendidikan pdA on pdA.kd_pendidikan=t.kd_pendidikan_ayah
			INNER JOIN pendidikan pdI on pdI.kd_pendidikan=t.kd_pendidikan_ibu
			INNER JOIN pendidikan pdSI on pdSI.kd_pendidikan=t.kd_pendidikan_suamiistri
			INNER JOIN pekerjaan pk on pk.kd_pekerjaan=t.kd_pekerjaan
			INNER JOIN pekerjaan pkA on pkA.kd_pekerjaan=t.kd_pekerjaan_ayah
			INNER JOIN pekerjaan pkI on pkI.kd_pekerjaan=t.kd_pekerjaan_ibu
			INNER JOIN pekerjaan pkSI on pkSI.kd_pekerjaan=t.kd_pekerjaan_suamiistri
			INNER JOIN customer c on k.kd_customer = c.kd_customer 
			WHERE t.KD_PASIEN = '".$kdpasien."' and left(k.kd_unit,1)='2' ORDER BY tgl_masuk desc,jam_masuk desc ")->row();
		if($sql){		
			if ($sql->STATUS_MARITA==0){
				$status_marita='Blm Menikah';
			}else if ($sql->STATUS_MARITA==1){
				$status_marita='Menikah';
			}else if ($sql->STATUS_MARITA==2){
				$status_marita='Janda';
			}else if ($sql->STATUS_MARITA==3){
				$status_marita='Duda';
			}
			$tgl_masuk=new DateTime($sql->TGL_MASUK);
			$jam_masuk=new DateTime($sql->JAM_MASUK);
			$tgl_lahir=new DateTime($sql->TGL_LAHIR);
			$birthDt = new DateTime($sql->TGL_LAHIR);
			$jenis_kelamin='L';
			if($sql->JENIS_KELAMIN=='0'){
				$jenis_kelamin='P';
			}
			$today = new DateTime('today');
			$y = $today->diff($birthDt)->y;
			$m = $today->diff($birthDt)->m;
			$d = $today->diff($birthDt)->d;
			$umur= $y . " th " . $m . " bl " . $d . " hr";
			$html='<html>
				<head>
					<style>
						body {
							font-family: Arial;
						}
						.b{
							font-weight: bold;
						}
						.f10{
							font-size: 10px;
						}
						.f11{
							font-size: 11px;
						}
						.f15{
							font-size: 15px;
						}
						.f20{
							font-size: 20px;
						}
						.f22{
							font-size: 22px;
						}
						.f25{
							font-size: 25px;
						}
						.tb td{
							padding-top: 5px;
							padding-bottom: 5px;
						}
						.b0{
							border:0px;
						}
					</style>
				</head>
				<body>
					<table width="100%" border="1" cellspacing="0.1">
						<tr>
							<td width="50%" colspan="4" class="b0">
								<img src="./ui/images/Logo/Header.jpeg" height="70" />
							</td>
							<td align="right" colspan="2" class="b0">
								<div class="b f22">RINGKASAN</div>
								<div class="b f22">RIWAYAT KLINIK</div>
								<div>DOKUMEN UTAMA: RM 1. IRJ</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="f11 b">No. Rekam Medis</div>
								<div class="b f20">&nbsp;'.$sql->KD_PASIEN.'</div>
							</td>
							<td colspan="2">&nbsp;</td>
							<td>
								<div class="f11 b">Tanggal</div>
								<div class="f15">&nbsp;'.$tgl_masuk->format('d/m/Y').' '.$jam_masuk->format('H:i:s').'</div>
							</td>
							<td>
								<div class="f11 b">Klinik Kunjungan I</div>
								<div class="f15">&nbsp;'.$sql->nama_unit.'</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="f11 b">Nama Lengkap</div>
								<div class="f15">&nbsp;'.$sql->NAMA.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Nama Keluarga</div>
								<div class="f15">&nbsp;'.$sql->NAMA_KELUARGA.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Tempat & Tanggal Lahir/ Umur</div>
								<div class="f15">&nbsp;'.$sql->TEMPAT_LAHIR.' - '.$tgl_lahir->format('d/m/Y').'/ '.$umur.'</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="f11 b">Sex</div>
								<div class="f15">&nbsp;'.$jenis_kelamin.'</div>
							</td>
							
							<td>
								<div class="f11 b">Gol. Darah</div>
								<div class="f15">&nbsp;'.$sql->GOL_DARAH.'</div>
							</td>
							<td>
								<div class="f11 b">St. Perkawinan</div>
								<div class="f15">&nbsp;'.$status_marita.'</div>
							</td>
							<td>
								<div class="f11 b">Agama</div>
								<div class="f15">&nbsp;'.$sql->agama.'</div>
							</td>
							<td>
								<div class="f11 b">Pekerjaan</div>
								<div class="f15">&nbsp;'.$sql->pekerjaan.'</div>
							</td>
							<td>
								<div class="f11 b">Pendidikan</div>
								<div class="f15">&nbsp;'.$sql->pendidikan.'</div>
							</td>
						</tr>
						<tr>
							<td colspan="6">
								<div class="f11 b">Alamat Lengkap & Nomor Telepon</div>
								<div class="f15">&nbsp;'.$sql->ALAMAT.' Kota. '.$sql->KOTA.' kode Pos. '.$sql->KD_POS.' Telp. '.$sql->TELEPON.'</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="f11 b">Nama Suami/Istri</div>
								<div class="f15">&nbsp;'.$sql->SUAMI_ISTRI.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Nama Ayah</div>
								<div class="f15">&nbsp;'.$sql->NAMA_AYAH.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Nama Ibu</div>
								<div class="f15">&nbsp;'.$sql->NAMA_IBU.'</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="f11 b">Pendidikan Ayah / Suami / Istri</div>
								<div class="f15">&nbsp;A: '.$sql->pendidikan_ayah.', I: '.$sql->pendidikan_ibu.', S/I: '.$sql->pendidikan_suamiistri.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Pekerjaan Ayah / Suami / Istri</div>
								<div class="f15">&nbsp;A: '.$sql->pekerjaan_ayah.', I: '.$sql->pekerjaan_ibu.', S/I: '.$sql->pekerjaan_suamiistri.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Nama Peserta Asuransi</div>
								<div class="f15">&nbsp;'.$sql->PEMEGANG_ASURANSI.'</div>
							</td>
						</tr>
						<tr>
							
							<td colspan="2">
								<div class="f11 b">Cara Pembayaran</div>
								<div class="f15">&nbsp;'.$sql->CUSTOMER.'</div>
							</td>
							<td colspan="2">
								<div class="f11 b">Nama dan Alamat Dokter Pengirim</div>
								<div class="f15">&nbsp;</div>
							</td>
							<td colspan="2">
								<div class="f11 b">No. Asuransi</div>
								<div class="f15">&nbsp;'.$sql->NO_ASURANSI.'</div>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="50" valign="top">
								<div class="f11 b">Riwayat Alergi</div>
								<div class="f15">&nbsp;</div>
							</td>
							<td colspan="4" valign="top">
								<div class="f11 b">Rawat Inap Operasi</div>
								<div class="f15">&nbsp;</div>
							</td>
						</tr>
					</table>
					<table border="1" cellspacing="0.1" width="100%" style="margin-top: 20px;">
						<thead>
							<tr>
								<th class="f11" width="80">TGL</th>
								<th class="f11" width="100">Nama Dokter</th>
								<th class="f11">Masalah/ Diagnosis</th>
								<th class="f11" width="50">ICD 10</th>
								<th class="f11" width="100">Terapi</th>
								<th class="f11">Keterangan</th>
								<th class="f11" width="70">TTD <br>Dokter</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="f11" height="50" valign="top" align="center">'.$tgl_masuk->format('d/m/Y').'</td>
								<td></td><td></td><td></td><td></td><td></td><td></td>
							</tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
							<tr><td height="50"></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
						</tbody>
					</table>
				</body>
			</html>';
		}else{
			$html='<center>Data Tidak Ada</center>';
		}
		$this->common->setPdf3('P','Kartu Status',$html,array('header'=>false,'foot'=>false,'margin-top'=>5,'margin-left'=>5,'margin-right'=>5,'margin-bottom'=>5));
	}
    public function save($Params=NULL){
		$bataskertas= new Pilihkertas;
		$kdpasien = $Params['NoMedrec'];
		$namapasien = $Params['NamaPasien'];
		$alamat = $Params['Alamat'];
        $kdunit = $Params['Poli'];
		$tglmasuk = $Params['TanggalMasuk'];
		$jammasuk = date('Y-m-d h:i:s');
		$kddokter = $Params['KdDokter'];

		//$unit = $this->db->query("SELECT NAMA_UNIT FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
		//$dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;
					
       

	/*  $data = array(
	 "kd_pasien"=>$kdpasien,
	 "nama_pasien"=>$namapasien,
	 "nama_unit"=>$unit,
	 "nama_dokter"=>$dokter,
	 "tgl_masuk"=>$tglmasuk,
	 "jam_masuk"=>$jammasuk,
	 "alamat"=>$alamat
	 ); */
	 
/* $this->load->model('rawat_jalan/tbltracer');	   
  $save = $this->tbltracer->save($data); */
	 
//AWAL SCRIPT PRINT

			 
		$sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = '0' THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' ");
		/* $sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' "); */
		if($sql->num_rows == 0)
		{
			$nama = '1';
			$kd_pasien = '2';
			$tgl_masuk = '3';
			$kota = '5';
			$nama_ortu = '6';
			$penjamin = '7';
			$jenis_kelamin = '8';
			$status_marita='9';
			$tgl_lahir = '10';
			$dokter = '11';
			$alamat = '12';
			$customer = '13';
			$jam = '14';
			$baru = '15';
			$umur = '16';
			$tlp = '17';
			$pekerjaan='18';
		}
		 else {
			

			foreach($sql->result() as $row)
			{
				$nama = $row->NAMA;
				$kd_pasien = $row->KD_PASIEN;
				$tgl_masuk = $row->TGL_MASUK;
				$kota = $row->KOTA;
				$nama_ortu = $row->NAMA_KELUARGA;
				$penjamin = $row->CUSTOMER;
				$jenis_kelamin = '';
				$status_marita='';
				if ($row->JENIS_KELAMIN=='1')
				{
					$jenis_kelamin='Laki-laki';
				}else
				{
					$jenis_kelamin='Perempuan';
				}
				
				
				if ($row->STATUS_MARITA==0)
				{
					$status_marita='Blm Menikah';
				}else if ($row->STATUS_MARITA==1)
				{
					$status_marita='Menikah';
				}else if ($row->STATUS_MARITA==2)
				{
					$status_marita='Janda';
				}else if ($row->STATUS_MARITA==3)
				{
					$status_marita='Duda';
				}
				
				$tgl_lahir =date("d-m-Y",strtotime($row->TGL_LAHIR));
				//$dokter = $row->nama_dokter;
				$alamat = $row->ALAMAT;
				$customer = $row->CUSTOMER;
				$baru = $row->BARU;
				$umur = '0 years';
				
				$strQuery = "select dbo.GetUmur('".date("Y-m-d",strtotime($row->TGL_LAHIR))."','".date("Y-m-d")."' )";
				$query = $this->db->query($strQuery);
				$res = $query->result();
				 if ($query->num_rows() > 0)
				 {
					 foreach($res as $data)
					 {
						$pisah = explode(" ",$data->age);
						$umur = $pisah[0]." TAHUN";
					 }
					// echo $umur;
				 }
				 $tlp='--';
				 if ($row->TELEPON=='')
				 {
					 $tlp='--';
				 }
				 else
				 {
					 $tlp=$row->TELEPON;
				 }
				 $pekerjaan=$row->PEKERJAAN;
			 }
			 
		//$printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_status_pasien'")->row()->setting;
		$printer = $this->db->query("SELECT p_statuspasien FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_statuspasien;
			
		$waktu = explode(" ",$tgl_masuk);
		$tanggal = $waktu[0];
		//$jam = $waktu[1];


		$x = 0;
		$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$feed=$bataskertas->PageLength('laporan');
		//$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
		$condensed = Chr(27).Chr(33).Chr(4);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		//$size1 = Chr(27) . Chr(80);
		$bold1 = Chr(27) . Chr(69);
		$bold0 = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1 = chr(20);
		$condensed0 = chr(15);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		$size0 = Chr(27).Chr(109);
		$Data  = $initialized;
		$Data .= $feed;
		$Data .= $condensed1;
		/* 
		$Data .= chr(27) . chr(67) . chr(70);
		$Data .= chr(27) . chr(87) . chr(49);
		$Data .= chr(27).chr(77); */
		//$Data .= "".$bold1."INSTALASI REKAM MEDIK".$bold0.""."\n"; $x++;

		$Data .= "\n";$x++;
		$Data .= "			        ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		// $Data .= "No. Medrec	: ".$bold1.$kd_pasien.$bold0." \n";$x++;
		$Data .= "No. Medrec	: ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		$Data .= "Nama Pasien	: ".$nama." \n";$x++;
		$Data .= "Tgl. Lahir/Umur	: ".$tgl_lahir."/".$umur." \n";$x++;
		$Data .= "Jenis Kelamin	: ".$jenis_kelamin." Status : ".$status_marita." \n";$x++;
		$tmp = $tmp = str_split($alamat,25);
		$Data .= "Alamat Lengkap	: ";$x++;
		for ($str = 0; $str < count($tmp); $str++)
		{
			if ($str==0)
			{
				$Data .= $tmp[$str]."\n";$x++;
			}
			else
			{
				$Data .= "	    	  ".$tmp[$str]."\n";$x++;
			}
		}
		//$Data .= "Alamat Lengkap  	: ".$alamat." \n";$x++;
		$Data .= "No. Telp	: ".$tlp." \n";$x++;
		$Data .= "Pekerjaan	: ".$pekerjaan." \n";$x++;
		$Data .= "Nama Orang Tua	: ".$nama_ortu." \n";$x++;
		$Data .= "Penjamin	: ".$customer." \n";$x++;
		/* $Data .= "\n";$x++;
		$Data .= $unit." / ".$customer."\n";$x++;
		$Data .= $dokter."\n";$x++;
		$Data .= "\n";$x++;
		$Data .= $tanggal." ".$jam."\n";$x++; */


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;

		$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
		if($counter == '7')
		{
		   $Data .=''; 
		   $newVal = 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}
		else
		{
		$Data .= "\n";
		$Data .= "\n";
		$Data .= "\n";
			$newVal = (int)$counter + 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}

		if ($x < 17)
		{
			$y = $x-17;
			for ($z = 1; $z<=$y;$z++)
			{
				$Data .= "\n";
			}
		}
		$Data .= $formfeed;
		$Data .= $size0;
		//echo $Data;die();
		fwrite($handle, $Data);
		fclose($handle);
		//echo $printer;
		$print = shell_exec("lpr -P ".$printer." -r ".$file);  # Lakukan cetak
//copy($file, $printer);  # Lakukan cetak

//AKHIR	 	 
	   
	 /*  if($save)
	   { */
		  
		   echo "{success:true}";
	   /* }
	   else
	   {
		   echo "{success:false}";
	   } */
	 
	 

    }
    
   
    
   
            
}
     public function save_LAMA($Params=NULL)
    {
		$bataskertas= new Pilihkertas;
		$kdpasien = $Params['NoMedrec'];
		$namapasien = $Params['NamaPasien'];
		$alamat = $Params['Alamat'];
        $kdunit = $Params['Poli'];
		$tglmasuk = $Params['TanggalMasuk'];
		$jammasuk = date('Y-m-d h:i:s');
		$kddokter = $Params['KdDokter'];

		//$unit = $this->db->query("SELECT NAMA_UNIT FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
		//$dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;
					
       

	/*  $data = array(
	 "kd_pasien"=>$kdpasien,
	 "nama_pasien"=>$namapasien,
	 "nama_unit"=>$unit,
	 "nama_dokter"=>$dokter,
	 "tgl_masuk"=>$tglmasuk,
	 "jam_masuk"=>$jammasuk,
	 "alamat"=>$alamat
	 ); */
	 
/* $this->load->model('rawat_jalan/tbltracer');	   
  $save = $this->tbltracer->save($data); */
	 
//AWAL SCRIPT PRINT

			 
		$sql = _QMS_query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' ");
		/* $sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' "); */
		if($sql->num_rows == 0)
		{
			$nama = '1';
			$kd_pasien = '2';
			$tgl_masuk = '3';
			$kota = '5';
			$nama_ortu = '6';
			$penjamin = '7';
			$jenis_kelamin = '8';
			$status_marita='9';
			$tgl_lahir = '10';
			$dokter = '11';
			$alamat = '12';
			$customer = '13';
			$jam = '14';
			$baru = '15';
			$umur = '16';
			$tlp = '17';
			$pekerjaan='18';
		}
		 else {
			

			foreach($sql->result() as $row)
			{
				$nama = $row->NAMA;
				$kd_pasien = $row->KD_PASIEN;
				$tgl_masuk = $row->TGL_MASUK;
				$kota = $row->KOTA;
				$nama_ortu = $row->NAMA_KELUARGA;
				$penjamin = $row->CUSTOMER;
				$jenis_kelamin = '';
				$status_marita='';
				if ($row->JENIS_KELAMIN==1)
				{
					$jenis_kelamin='Laki-laki';
				}else
				{
					$jenis_kelamin='Perempuan';
				}
				
				
				if ($row->STATUS_MARITA==0)
				{
					$status_marita='Blm Menikah';
				}else if ($row->STATUS_MARITA==1)
				{
					$status_marita='Menikah';
				}else if ($row->STATUS_MARITA==2)
				{
					$status_marita='Janda';
				}else if ($row->STATUS_MARITA==3)
				{
					$status_marita='Duda';
				}
				
				$tgl_lahir =date("d-m-Y",strtotime($row->TGL_LAHIR));
				//$dokter = $row->nama_dokter;
				$alamat = $row->ALAMAT;
				$customer = $row->CUSTOMER;
				$baru = $row->BARU;
				$umur = '0 years';
				
				$strQuery = "select age(timestamp '".date("Y-m-d",strtotime($row->TGL_LAHIR))."')";
				$query = $this->db->query($strQuery);
				$res = $query->result();
				 if ($query->num_rows() > 0)
				 {
					 foreach($res as $data)
					 {
						$pisah = explode(" ",$data->age);
						$umur = $pisah[0]." TAHUN";
					 }
					// echo $umur;
				 }
				 $tlp='--';
				 if ($row->TELEPON=='')
				 {
					 $tlp='--';
				 }
				 else
				 {
					 $tlp=$row->TELEPON;
				 }
				 $pekerjaan=$row->PEKERJAAN;
			 }
			 
		//$printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_status_pasien'")->row()->setting;
		$printer = $this->db->query("SELECT p_statuspasien FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_statuspasien;
			
		$waktu = explode(" ",$tgl_masuk);
		$tanggal = $waktu[0];
		//$jam = $waktu[1];


		$x = 0;
		$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$feed=$bataskertas->PageLength('laporan');
		//$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
		$condensed = Chr(27).Chr(33).Chr(4);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		//$size1 = Chr(27) . Chr(80);
		$bold1 = Chr(27) . Chr(69);
		$bold0 = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1 = chr(20);
		$condensed0 = chr(15);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		$size0 = Chr(27).Chr(109);
		$Data  = $initialized;
		$Data .= $feed;
		$Data .= $condensed1;
		/* 
		$Data .= chr(27) . chr(67) . chr(70);
		$Data .= chr(27) . chr(87) . chr(49);
		$Data .= chr(27).chr(77); */
		//$Data .= "".$bold1."INSTALASI REKAM MEDIK".$bold0.""."\n"; $x++;

		$Data .= "\n";$x++;
		$Data .= "			        ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		// $Data .= "No. Medrec	: ".$bold1.$kd_pasien.$bold0." \n";$x++;
		$Data .= "No. Medrec	: ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		$Data .= "Nama Pasien	: ".$nama." \n";$x++;
		$Data .= "Tgl. Lahir/Umur	: ".$tgl_lahir."/".$umur." \n";$x++;
		$Data .= "Jenis Kelamin	: ".$jenis_kelamin." Status : ".$status_marita." \n";$x++;
		$tmp = $tmp = str_split($alamat,25);
		$Data .= "Alamat Lengkap	: ";$x++;
		for ($str = 0; $str < count($tmp); $str++)
		{
			if ($str==0)
			{
				$Data .= $tmp[$str]."\n";$x++;
			}
			else
			{
				$Data .= "	    	  ".$tmp[$str]."\n";$x++;
			}
		}
		//$Data .= "Alamat Lengkap  	: ".$alamat." \n";$x++;
		$Data .= "No. Telp	: ".$tlp." \n";$x++;
		$Data .= "Pekerjaan	: ".$pekerjaan." \n";$x++;
		$Data .= "Nama Orang Tua	: ".$nama_ortu." \n";$x++;
		$Data .= "Penjamin	: ".$customer." \n";$x++;
		/* $Data .= "\n";$x++;
		$Data .= $unit." / ".$customer."\n";$x++;
		$Data .= $dokter."\n";$x++;
		$Data .= "\n";$x++;
		$Data .= $tanggal." ".$jam."\n";$x++; */


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;

		$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
		if($counter == '7')
		{
		   $Data .=''; 
		   $newVal = 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}
		else
		{
		$Data .= "\n";
		$Data .= "\n";
		$Data .= "\n";
			$newVal = (int)$counter + 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}

		if ($x < 17)
		{
			$y = $x-17;
			for ($z = 1; $z<=$y;$z++)
			{
				$Data .= "\n";
			}
		}
		$Data .= $formfeed;
		$Data .= $size0;
		fwrite($handle, $Data);
		fclose($handle);
		//echo $printer;
		$print = shell_exec("lpr -P ".$printer." -r ".$file);  # Lakukan cetak
//copy($file, $printer);  # Lakukan cetak

//AKHIR	 	 
	   
	 /*  if($save)
	   { */
		  
		   echo "{success:true}";
	   /* }
	   else
	   {
		   echo "{success:false}";
	   } */
	 
	 

    }
    
   
    
   
            
}
    
   
    
   
            
}




