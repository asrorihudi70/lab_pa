<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdatapasien extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        try {
            //$this->load->model('rawat_jalan/tbl_datapasien');
			$sqldatasrv="SELECT top 50 pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, 
						 pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir, 
						 agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, 
						 pasien.alamat, pasien.kd_kelurahan, pendidikan.pendidikan, pekerjaan.pekerjaan, 
						 kb.kd_kabupaten,kb.kabupaten, kc.kd_kecamatan,kc.kecamatan, pr.propinsi,
						 pasien.email,pasien.handphone,pasien.nama_ayah,pasien.nama_ibu,
						 kl.kelurahan,pr.kd_propinsi, pasien.kd_pekerjaan, 
						 pasien.kd_pendidikan, pasien.telepon ,pasien.kd_agama
						 FROM pasien left join agama ON pasien.kd_agama = agama.kd_agama 
						 left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
						 left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
						 left join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
						 left join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
						 left join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
						 left join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI where ";
            if (strlen($Params[4]) !== 0) {
                $kriteria=str_replace("~", "'", $Params[4] . "--limit 50");
               //$res = $this->tbl_datapasien->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
            } else {
                $kriteria=str_replace("~", "'", " kd_pasien Like '%-%' 
						   order by pasien.kd_pasien desc --limit 50");
               // $res = $this->tbl_datapasien->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
            }
			
			//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
			foreach ($res->result() as $rec)
			{
				$o=array();
				//var_dump($rec);
				  $o['KD_PASIEN']=$rec->kd_pasien;
				  $o['NAMA']=$rec->nama;
				  $o['NAMA_KELUARGA']=$rec->nama_keluarga;
				  $o['JENIS_KELAMIN']=$rec->jenis_kelamin;
				  $o['TEMPAT_LAHIR']=$rec->tempat_lahir;
				  $o['TGL_LAHIR']=date("M d Y",strtotime($rec->tgl_lahir));
				  $o['TGL_LAHIR_PASIEN']=date("Y-m-d",strtotime($rec->tgl_lahir));
				  $o['AGAMA']=$rec->agama;
				  $o['GOL_DARAH']=$rec->gol_darah;
				  $o['WNI']=$rec->wni;
				  $o['STATUS_MARITA']=$rec->status_marita;
				  $o['ALAMAT']=$rec->alamat;
				  $o['KD_KELURAHAN']=$rec->kd_kelurahan;
				  $o['PENDIDIKAN']=$rec->pendidikan;
				  $o['KABUPATEN']=$rec->kabupaten;
				  $o['KECAMATAN']=$rec->kecamatan;
				  $o['PROPINSI']=$rec->propinsi;
				  $o['KD_KABUPATEN']=$rec->kd_kabupaten;
				  $o['KD_KECAMATAN']=$rec->kd_kecamatan;
				  $o['KD_PROPINSI']=$rec->kd_propinsi;
				  $o['KD_PENDIDIKAN']=$rec->kd_pendidikan;
				  $o['KD_PEKERJAAN']=$rec->kd_pekerjaan;
				  $o['KD_AGAMA']=$rec->kd_agama;
				  $o['PEKERJAAN']=$rec->pekerjaan;
			
				  $o['AYAH']=$rec->nama_ayah;
				  $o['IBU']=$rec->nama_ibu;
				  $o['KELURAHAN']=$rec->kelurahan;
				  
				  $o['EMAIL']=$rec->email;
				  $o['HP']=$rec->handphone;
				  $o['TELEPON']=$rec->telepon;
				$list[]=$o;	
			} 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }

    public function save($Params = null) {
        $mError = "";
        $parametersave = $Params['TMPPARAM'];
        if ($parametersave === '0') {
            $mError = $this->SimpanPasien($Params);

            if ($mError == "update") {
                echo '{success: true}';
            } else {
                echo '{success: false}';
            }
        } else {
            $mError = $this->SimpanHistoriDelDataPasien($Params);

            if ($mError == "sukses") {
                echo '{success: true}';
            } else {
                echo '{success: false}';
            }
        }
    }

    public function SimpanPasien($Params) {
        $strError = "";
        if ($Params["ID_JENIS_KELAMIN"] === '1') {
            $tmpjk 		= 'TRUE';
            $tmpjkSQL 	= 1;
        } else {
            $tmpjk = 'FALSE';
            $tmpjkSQL 	= 0;
        }
		$tglLahir= date('Y-m-d',strtotime(str_replace('/','-', $Params["TGL_LAHIR"])));
        $data = array("nama" => $Params["NAMA"],
            "tempat_lahir" => $Params["TEMPAT_LAHIR"],
            "tgl_lahir" => $tglLahir,
            "jenis_kelamin" => $tmpjk,
            "gol_darah" => $Params["ID_GOL_DARAH"],
            "alamat" => $Params["ALAMAT"],
            "telepon" => $Params["NO_TELP"],
            "kd_agama" => $Params["KD_AGAMA"],
            "status_marita" => $Params["KD_STS_MARITAL"],
            "kd_pendidikan" => $Params["KD_PENDIDIKAN"],
            "kd_pekerjaan" => $Params["KD_PEKERJAAN"],
            "nama_ayah" => $Params["AYAHPASIEN"],
            "nama_ibu" => $Params["IBUPASIEN"],
            "email" => $Params["Emailpasien"],
            "handphone" => $Params["HPpasien"],
        );

        $datasql = array("nama" => $Params["NAMA"],
            "tempat_lahir" => $Params["TEMPAT_LAHIR"],
            "tgl_lahir" => $tglLahir,
            "jenis_kelamin" => $tmpjkSQL,
            "gol_darah" => $Params["ID_GOL_DARAH"],
            "alamat" => $Params["ALAMAT"],
            "telepon" => $Params["NO_TELP"],
            "kd_agama" => $Params["KD_AGAMA"],
            "status_marita" => $Params["KD_STS_MARITAL"],
            "kd_pendidikan" => $Params["KD_PENDIDIKAN"],
            "kd_pekerjaan" => $Params["KD_PEKERJAAN"]
        );


        $criteria = "kd_pasien = '" . $Params["NO_MEDREC"] . "'";

        $this->load->model("rawat_jalan/tb_pasien");
        $this->tb_pasien->db->where($criteria, null, false);
        $query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {
            $strError = "simpan";
        } else {
            $this->tb_pasien->db->where($criteria, null, false);
//            $dataUpdate = array("nama"=>$Params["NamaPasien"]);
            $result = $this->tb_pasien->Update($data);
            _QMS_update('pasien', $datasql, $criteria);



            $strError = "update";
        }
        return $strError;
    }

    public function SimpanHistoriDelDataPasien($Params) {
        $strError = "";

        $waktu = date("m/d/y"); // 10/11/14
        $jam = date("m/d/y H:i:s");
        $Medrec = $Params['NOMEDREC'];
        $TglMasuk = $Params['TGLMASUK'];
        $UNIT = $Params['KDUNIT'];
        $URUT = $Params['URUTMASUK'];
        $tmptglmasuk = substr_replace($TglMasuk, '', 10);
        $criteria = "t.kd_pasien = '" . $Medrec . "' AND t.tgl_transaksi = '" . $tmptglmasuk . "' AND t.kd_unit = '" . $UNIT . "' AND t.urut_masuk = '" . $URUT . "'";
        $this->load->model("rawat_jalan/tblviewhistorypasien");
        $this->tblviewhistorypasien->db->where($criteria, null, false);
        $query = $this->tblviewhistorypasien->GetRowList(0, 1, "", "", "");

        if ($query[1] != 0) {
            $data = array("kd_kasir" => $query[0][0]->KD_KASIR,
                "no_transaksi" => $query[0][0]->NO_TRANSAKSI,
                "tgl_transaksi" => $query[0][0]->TGL_TRANSAKSI,
                "kd_pasien" => $Params['NOMEDREC'],
                "nama" => $query[0][0]->NAMA,
                "kd_unit" => $query[0][0]->KD_UNIT,
                "nama_unit" => $query[0][0]->NAMA_UNIT,
                "ispay" => 'FALSE',
                "kd_user" => 0,
                "kd_user_del" => 0,
                "user_name" => 'Admin',
                "jumlah" => $query[0][0]->JML,
                "tgl_batal" => $waktu,
                "ket" => $Params["KET"],
                "jam_batal" => $jam
            );
            $this->load->model("rawat_jalan/tblviewhistorydatapasien");
            $result = $this->tblviewhistorydatapasien->Save($data);

            if ($result > 0) {
//                                echo('a');
                $criteria = "kd_pasien = '" . $Medrec . "' AND tgl_masuk = '" . $tmptglmasuk . "' AND kd_unit = '" . $UNIT . "' AND urut_masuk = '" . $URUT . "'";
                //$this->load->model('cm/am_request_cm_detail');
                $this->load->model('rawat_jalan/tb_kunjungan');
                $this->tb_kunjungan->db->where($criteria, null, false);
                $result = $this->tb_kunjungan->Delete();




                $strError = "sukses";
            } else {
                echo '{success: false, pesan: 0}';
            }
        } else {
            
        }
        return $strError;
    }

}

?>