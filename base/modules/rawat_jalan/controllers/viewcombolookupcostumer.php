<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewcombolookupcostumer extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }    


    public function index()
    {
        $this->load->view('main/index');                    
    }
   
   public function read($Params=null)
   {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
                        
        $this->load->model('rawat_jalan/tbl_cmb_customer');
         
        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tbl_cmb_customer->setCriteria($criteria);

        }

        $query = $this->tbl_cmb_customer->readAll();
                    
        $arrResult=array();

        $row=new Row_Customer;
        $row->KD_CUSTOMER='Semua';
        $row->CUSTOMER='Semua';
        $arrResult[]=$row;
        foreach ($query->result_object() as $rows)
        {
            $arrResult[] = $this->FillRow($rows);
        }

    //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
    
        echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';
            
                    
   }
    function FillRow($rec)
    {
        $row=new Row_Customer;
        $row->KD_CUSTOMER   = $rec->KD_CUSTOMER;
        $row->CUSTOMER      = $rec->CUSTOMER;

        return $row;
    }
}



?>