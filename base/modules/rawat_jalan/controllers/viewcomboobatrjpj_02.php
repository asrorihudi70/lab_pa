<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Viewcomboobatrjpj_02 extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('main/index');
    }

    function read($Params=null)
    {

            try
            {
				// --upper(A.nama_obat) like upper('".$_POST['text']."%') and
				$this->load->database();
				$kdcustomer=$Params[4];
				$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
				$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
				// echo "<pre>".var_export($this->session, true)."</pre>"; 
				/* $result=$this->db->query("
						SELECT A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik,ast.satuan, c.jml_stok_apt,
							(SELECT Jumlah as markup
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
									and kd_Jenis = 1),
							(SELECT Jumlah as tuslah
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
									and kd_Jenis = 2),
							(SELECT Jumlah as adm_racik
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
									and kd_Jenis = 3),
							(
								SELECT Jumlah 
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
								 and kd_Jenis = 1
							) * b.harga_beli as harga_jual

						FROM apt_obat A 
						inner join apt_satuan ast on ast.kd_satuan=A.kd_satuan
							INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
							LEFT JOIN apt_stok_unit c ON b.kd_prd=c.kd_prd  
							
						WHERE
							 b.kd_milik=".$kdMilik."
							and c.kd_unit_far='".$kdUnitFar."'
							and c.kd_milik=  ".$kdMilik."
							and b.Tag_Berlaku = 1
							and A.aktif='t'  
							and c.jml_stok_apt >0 
							limit 10								
					")->result(); */
					
				//update hani 
				if($kdUnitFar == 'AP2' || $kdUnitFar == 'UGD'){
					$kd_unit_tarif = 0;
				}else if($kdUnitFar == 'AP1'){
					$kd_unit_tarif = 1;
				}else{
					$kd_unit_tarif = 2;
				}

				/* *******************UPDATE Melinda*********************** */
				$result=$this->db->query("
						SELECT TOP 10 A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,A.kd_pabrik,ast.satuan, sum(c.jml_stok_apt) as jml_stok_apt,
							(SELECT Jumlah as markup
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
									and kd_Jenis = 1 and kd_unit_far='".$kdUnitFar."' and kd_unit_tarif = '".$kd_unit_tarif."'),
							(SELECT Jumlah as tuslah
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
									and kd_Jenis = 2 and kd_unit_far='".$kdUnitFar."' and kd_unit_tarif = '".$kd_unit_tarif."'),
							(SELECT Jumlah as adm_racik
									FROM apt_tarif_cust 
									WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
									and kd_Jenis = 3 and kd_unit_far='".$kdUnitFar."' and kd_unit_tarif = '".$kd_unit_tarif."'),
							(
								SELECT Jumlah 
								FROM apt_tarif_cust 
								WHERE kd_gol = (SELECT kd_gol FROM apt_gol_cust WHERE kd_customer='". $kdcustomer ."')
								 and kd_Jenis = 1 and kd_unit_far='".$kdUnitFar."' and kd_unit_tarif = '".$kd_unit_tarif."'
							) * b.harga_beli as harga_jual

						FROM apt_obat A 
							inner join apt_satuan ast on ast.kd_satuan=A.kd_satuan
							INNER JOIN apt_produk B ON B.kd_prd=A.kd_prd 
							LEFT JOIN apt_stok_unit c ON b.kd_prd=c.kd_prd  
							
						WHERE  
							b.kd_milik=".$kdMilik."
							--and c.kd_unit_far='".$kdUnitFar."'
							and c.kd_milik=  ".$kdMilik."
							and b.Tag_Berlaku = 1
							and A.aktif='1'  
							and c.jml_stok_apt >0 
						GROUP BY A.kd_prd,A.fractions,A.kd_satuan,A.nama_obat,A.kd_sat_besar,b.harga_beli,
							A.kd_pabrik,ast.satuan
														
					")->result();
				/* ****************************************************** */
            }
            catch(Exception $o)
            {
                    echo 'Debug  fail ';

            }

            echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';

    }

   
}

?>