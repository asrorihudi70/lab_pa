<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class viewdatakunjunganpasien extends MX_Controller {
    
    public function __construct() {
        //parent::Controller();
        parent::__construct();
		
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
    	// echo 'a';

		$list = array();	
		$sqldatasrv="SELECT TOP 100 P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER
						FROM PASIEN P
						INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
						inner join customer c on c.kd_customer = k.kd_customer
						INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
						INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
						where";
		if (strlen($Params[4]) !== 0) {
			
            $kriteria=str_replace("~", "'", $Params[4] . "  group by  P.KD_PASIEN, k.urut_masuk,p.NAMA,k.urut_masuk,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA, d.KD_DOKTER , K.TGL_KELUAR
				, c.CUSTOMER
						order by k.TGL_MASUK desc ");
        } else {
			$sqldatasrv="SELECT P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER
						FROM PASIEN P
						INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
						inner join customer c on c.kd_customer = k.kd_customer
						INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
						INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
						";
            $kriteria=str_replace("~", "'", " group by  P.KD_PASIEN, k.urut_masuk,p.NAMA,k.urut_masuk,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA, d.KD_DOKTER , K.TGL_KELUAR
				,c.CUSTOMER
						order by k.TGL_MASUK desc  ");
        }
		//echo $sqldatasrv;
	   //$dbsqlsrv = $this->load->database('default',TRUE);
	   //$res = $dbsqlsrv->query($sqldatasrv.$kriteria);
		$res = $this->db->query($sqldatasrv.$kriteria);
		
		foreach ($res->result() as $rec)
		{
		 /*  $o=array();
		  $o['KD_PASIEN']=$rec->KD_PASIEN;
		  $o['KD_UNIT']=$rec->KD_UNIT;
		  $o['POLI']=$rec->NAMA_UNIT;
		  $o['URUT']=$rec->urut_masuk;
		  $o['TGL']=date("d-M-Y", strtotime($rec->TGL_MASUK));
		  $o['DOKTER']=$rec->NAMA_DOKTER;
		  $o['KD_DOKTER']=$rec->KD_DOKTER;
		  $o['CUSTOMER']=$rec->CUSTOMER;
		  if ($rec->TGL_KELUAR==null)
		  {
		  	$o['TGL_KELUAR']='';
		  }else

		  {
		  	$o['TGL_KELUAR']=date("d-M-Y", strtotime($rec->TGL_KELUAR));
		  }
		  
			$list[]         =$o;	 */
			$o              = array();
			$o['KD_PASIEN'] = $rec->KD_PASIEN;
			$o['KD_UNIT']   = $rec->KD_UNIT;
			$o['POLI']      = $rec->NAMA_UNIT;
			$o['URUT']      = $rec->urut_masuk;
			$o['TGL']       = date("d-M-Y", strtotime($rec->TGL_MASUK));
			$o['TGL_MASUK'] = date_format(date_create($rec->TGL_MASUK), 'Y-m-d');
			$o['DOKTER']    = $rec->NAMA_DOKTER;
			$o['KD_DOKTER'] = $rec->KD_DOKTER;
			$o['CUSTOMER']  = $rec->CUSTOMER;
		  if ($rec->TGL_KELUAR==null){
		  	$o['TGL_KELUAR']='';
		  }else{
		  	$o['TGL_KELUAR']=date("d-M-Y", strtotime($rec->TGL_KELUAR));
		  }
          $list[]=$o;	
		}

        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }

	function getHistoryKunjungan(){
		$dbsqlsrv = $this->load->database('default',TRUE);
		if($_POST['kd_pasien']==''){
			$kd_pasien="";
		} else{
			$kd_pasien=$_POST['kd_pasien'];
		}
		# QUERY POSTGRES
		/* $result=$this->db->query("SELECT top 100 P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER
								FROM PASIEN P
										INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
										inner join customer c on c.kd_customer = k.kd_customer
										INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
										INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
								where P.KD_PASIEN = '".$kd_pasien."' ")->result(); */
		
		#QUERY SQLSERVER
		/* $result=$dbsqlsrv->query("SELECT top 100 P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER
								FROM PASIEN P
										INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
										inner join customer c on c.kd_customer = k.kd_customer
										INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
										INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
								where P.KD_PASIEN = '".$kd_pasien."' 
								order by k.TGL_MASUK desc  ")->result();  */
		$result=$this->db->query("SELECT  P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER
			FROM PASIEN P
					INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
					inner join customer c on c.kd_customer = k.kd_customer
					INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
					INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
			where P.KD_PASIEN = '".$kd_pasien."' order by k.TGL_MASUK desc")->result();  
		foreach ($result as $rec)
		{
		  /* $o=array();
		  $o['KD_PASIEN']=$rec->KD_PASIEN;
		  $o['KD_UNIT']=$rec->KD_UNIT;
		  $o['POLI']=$rec->NAMA_UNIT;
		  $o['URUT']=$rec->urut_masuk;
		  $o['TGL']=date("d-M-Y", strtotime($rec->TGL_MASUK));
		  $o['DOKTER']=$rec->NAMA_DOKTER;
		  $o['KD_DOKTER']=$rec->KD_DOKTER;
		  $o['CUSTOMER']=$rec->CUSTOMER;
		  if ($rec->TGL_KELUAR==null)
		  {
		  	$o['TGL_KELUAR']='';
		  }else

		  {
		  	$o['TGL_KELUAR']=date("d-M-Y", strtotime($rec->TGL_KELUAR));
		  }
		  
          $list[]=$o;	 */
		   $o=array();
		  $o['KD_PASIEN']=$rec->KD_PASIEN;
		  $o['KD_UNIT']=$rec->KD_UNIT;
		  $o['POLI']=$rec->NAMA_UNIT;
		  $o['URUT']=$rec->urut_masuk;
		  $o['TGL']=date("d-M-Y", strtotime($rec->TGL_MASUK));
		  $o['DOKTER']=$rec->NAMA_DOKTER;
		  $o['KD_DOKTER']=$rec->KD_DOKTER;
		  $o['CUSTOMER']=$rec->CUSTOMER;
		  if ($rec->TGL_KELUAR==null){
		  	$o['TGL_KELUAR']='';
		  }else{
		  	$o['TGL_KELUAR']=date("d-M-Y", strtotime($rec->TGL_KELUAR));
		  }
          $list[]=$o;	
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($list) . '}';
	}
    
}

?>			