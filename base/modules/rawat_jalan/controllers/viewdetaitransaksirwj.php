<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewdetaitransaksirwj extends MX_Controller
{
	public  $gkdbagian;
	public  $Status;
	public function __construct()
	{
		parent::__construct();
		if (!isset($this->session->userdata['user_id'])) {
			$this->load->library('session');
			$this->session->set_userdata('user_id', json_decode($_COOKIE['NCI']));
		}
		$this->load->model('M_pembayaran');
	}

	public function index()
	{
		$this->load->view('main/index');
	}

	public function read($Params = null)
	{
		// date_default_timezone_set("Asia/Jakarta");
		date_default_timezone_set('Asia/Makassar');
		/*try
        {*/
		$Status = 'false';
		$kdbagian = 2;




		//$hari=date('d') -1;
		//$kd_user = $this->session->userdata['user_id']['id'];
		//$unit = $this->db->query("select kd_unit from zusers where kd_user = '".$kd_user."'")->row();
		/*$unitpoli = $unit->kd_unit;
		
		if($kd_user == '0')
		{
			$criteria = "(unit.kd_unit <> '0') AND";
			$criteria = "";
		}
		else
		{
			$criteria = ""; 
		}*/
		$criteria = "";
		$kd_user = $this->session->userdata['user_id']['id'];
		$sql = "SELECT kd_unit FROM zusers WHERE kd_user='" . $kd_user . "'";
		$arrSql = $this->db->query($sql);
		$query = '';
		if ($arrSql->num_rows() > 0) {
			$query = " AND left(tmp_kd_unit,1)='2'  AND  tmp_kd_unit IN(" . $arrSql->row()->kd_unit . ") "; //and tgl_transaksi in('".date('Y-m-d')."') ";
		} else {
			//$query = " and tgl_transaksi in('".date('Y-m-d')."') ";
		}

		$this->load->model('rawat_jalan/tblviewtrrwj');
		if (strlen($Params[4]) !== 0) {
			$this->db->where(str_replace("~", "'", $criteria . " co_status = '" . $Status . "' and kd_bagian ='" . $kdbagian . "' " . $Params[4] . " " . $query . " order by  nama_dokter asc, tgl_transaksi asc, jam_masuk ASC "), null, false);
			// $this->db->where(str_replace("~", "'", $criteria . " co_status = '" . $Status . "' and kd_bagian ='" . $kdbagian . "' " . $Params[4] . " " . $query . " order by tgl_transaksi asc, no_urut asc "), null, false);
			$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
		} else {
			$this->db->where(str_replace("~", "'", $criteria . " posting_transaksi = '0' and co_status = '" . $Status . "' " . $query . " and kd_bagian =  '" . $kdbagian . "' order by  nama_dokter asc, tgl_transaksi asc, jam_masuk ASC "), null, false);
			// $this->db->where(str_replace("~", "'", $criteria . " posting_transaksi = '0' and co_status = '" . $Status . "' " . $query . " and kd_bagian =  '" . $kdbagian . "' order by tgl_transaksi asc, no_urut asc "), null, false);
			$res = $this->tblviewtrrwj->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
		}
		//echo date('Y-m-'.$hari.' 00:00:00');

		/*}
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }*/

		echo '{success:true, totalrecords:' . $res[1] . ', ListDataObj:' . json_encode($res[0]) . '}';
		$this->db->close();
	}

	public function save($Params = null)
	{

		//$ReqId=0;
		$TrKodeTranskasi = $Params['TrKodeTranskasi'];
		$result = $this->simpandetail($Params, $TrKodeTranskasi);

		// {

		//}
		// else echo '{success: false, pesan: "gagal simpan"}';

	}

	private function simpandetail($Params = null, $TrKodeTranskasi = "")
	{


		$ubah = $Params['Ubah'];
		if ($ubah == 1) {
			$Arr['no_transaksi'] = $Params['TrKodeTranskasi'];
			$Arr['urut'] = $Params['RowReq'];
			$Arr['qty'] = $Params['Qty'];
			$this->load->model('rawat_jalan/tblkasirdetailrrjw');
			$this->db->where("no_transaksi = '" . $Arr['no_transaksi'] . "' and urut = '" . $Arr['urut'] . "'  ", null, false);
			$res = $this->tblkasirdetailrrjw->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
			if ($res > 0) {
				echo '{success: true}';
			} else echo '{success: false}';
		} else {
			$List = $Params['List'];
			$JmlField = $Params['JmlField'];
			$JmlList = $Params['JmlList'];
			$TrKodeTranskasi = $Params['TrKodeTranskasi'];
			$kdunit = $Params['KdUnit'];
			$Shift = $Params['Shift'];
			$arr[] = $this->GetListDetail($JmlField, $List, $JmlList, $TrKodeTranskasi);

			$retVal = "";

			if (count($arr) > 0) {
				foreach ($arr as $x) {

					if ($TrKodeTranskasi == "")
						$TrKodeTranskasi = $x['NO_TRANSAKSI'];

					if ($x['URUT'] == 0)
						$x['URUT'] = $this->GetUrutRequestDetail($TrKodeTranskasi);


					$criteria = "no_transaksi = '" . $x['NO_TRANSAKSI'] . "' AND urut = " . $x['URUT'];

					$this->load->model('rawat_jalan/tblkasirdetailrrjw');

					$this->tblkasirdetailrrjw->db->where($criteria, null, false);

					$query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");
					$result = 0;


					if ($query[1] == 0) {




						$data = array(

							'no_transaksi' => $x['NO_TRANSAKSI'],
							'urut' => $x['URUT'],
							'kd_produk' => $x['KD_PRODUK'],
							'qty' => $x['QTY'],
							'tgl_berlaku' => $x['TGL_BERLAKU'],
							'harga' => $x['HARGA'],
							'kd_tarif' => $x['KD_TARIF'],
							'kd_kasir' => $x['KD_KASIR'],
							'tgl_transaksi' => $x['TGL_TRANSAKSI'],
							'kd_user' => $x['KD_USER'],
							'kd_unit' => $kdunit,
							'charge' => $x['CHARGE'],
							'adjust' => $x['ADJUST'],
							'folio' => $x['FOLIO'],
							'shift' => $Shift,
							'kd_dokter' => $x['KD_DOKTER'],
							'kd_unit_tr' => $x['KD_UNIT_TR'],
							'cito' => $x['CITO'],
							'js' => $x['JS'],
							'jp' => $x['JP'],
							'no_faktur' => $x['NO_FAKTUR'],
							'flag' => $x['FLAG'],
							'tag' => $x['TAG']

						);



						$result = $this->tblkasirdetailrrjw->Save($data);
						if ($result) {

							$xyz = $this->db->query("
												insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
												select t.kd_kasir,
												t.no_transaksi,
												dt.urut,
												dt.tgl_transaksi,
												tc.kd_component,
												tc.tarif
												from tarif_component tc 
												inner join detail_transaksi dt on 
												tc.kd_tarif = dt.kd_tarif
												inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
												and t.kd_unit = tc.kd_unit
												where tc.kd_produk= '" . $x['KD_PRODUK'] . "' and tc.kd_unit='" . $kdunit . "'  and tc.kd_tarif='TU' 
												and  tc.tgl_berlaku in ('2014-03-01') 
												and t.kd_kasir = '" . $x['KD_KASIR'] . "' and dt.urut='" . $x['URUT'] . "' and t.no_transaksi = '" . $x['NO_TRANSAKSI'] . "'");

							if ($xyz) {

								echo '{success:true}';
								$retVal = "0";
							} else {
								echo '{success:false}';
								//throw new exception;
							}
						}
					} else {



						$data = array(
							'kd_produk' => $x['KD_PRODUK'],
							'qty' => $x['QTY'],
							'tgl_berlaku' => $x['TGL_BERLAKU'],
							'harga' => $x['HARGA'],
							'kd_tarif' => $x['KD_TARIF']
						);
						$criteria = "no_transaksi = '" . $x['NO_TRANSAKSI'] . "' and urut = " . $x['URUT'];

						$this->tblkasirdetailrrjw->db->where($criteria, null, false);
						$result = $this->tblkasirdetailrrjw->Update($data);

						if ($result == 0)
							$retVal = "0";
					}
				}
			}

			$this->CekJumlahDetail((int)$JmlList, $TrKodeTranskasi, $arr);

			return $retVal;
		}
	}

	public function delete($Params = null)
	{
		$data = array();
		$data['TrKodeTranskasi']	= $Params['TrKodeTranskasi'];
		$data['TrTglTransaksi']		= $Params['TrTglTransaksi'];
		$data['TrKdPasien'] 		= $Params['TrKdPasien'];
		$data['TrKdNamaPasien'] 	= $Params['TrKdNamaPasien'];
		$data['TrKdUnit'] 			= $Params['TrKdUnit'];
		$data['TrNamaUnit'] 		= $Params['TrNamaUnit'];
		$data['Uraian'] 			= $Params['Uraian'];
		$data['TrHarga'] 			= $Params['TrHarga'];
		$data['TrKdProduk'] 		= $Params['TrKdProduk'];
		$data['TrUrutMasuk'] 		= $Params['UrutMasuk'];
		$data['TrTglBatal'] 		= gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);
		$data['TrKdKasir'] 			= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwj'")->row()->setting; //'01';
		$data['Alasan']				= $Params['AlasanHapus'];
		$data['RowReq']				= $Params['RowReq'];

		$delete = $this->M_pembayaran->hapustindakan($data);
		if ($delete == 'success') {
			echo "{success:true}";
		} else {
			echo "{success:false}";
		}
	}





	private function CekJumlahDetail($jmlRecord, $TrKodeTranskasi, $arr)
	{

		$this->load->model('rawat_jalan/tblkasirdetailrrjw');
		//$query = $this->am_request_rawat_jalan_detail->read($TrKodeTranskasi);
		$criteria = "no_transaksi = '" . $TrKodeTranskasi . "'";
		$this->tblkasirdetailrrjw->db->where($criteria, null, false);
		$query = $this->tblkasirdetailrrjw->GetRowList(0, 1000, "", "", "");

		$ArrList = array();
		$numrow = $query[1];

		//if ($query->num_rows()>0)
		if ($numrow > 0) {
			//if ($query->num_rows()!=$jmlRecord)
			if ($numrow != $jmlRecord) {
				//if ($jmlRecord<$query->num_rows())
				if ($jmlRecord < $numrow) {
					if (cont($arr) > 0) {
						//foreach($query->result_array() as $y)
						foreach ($query[0] as $y) {
							$mBol = false;

							foreach ($arr as $z) {

								if ($y->URUT == $z['URUT']) {
									$mBol = true;
									break;
								}
							}

							if ($mBol == false)
								$ArrList[] = $y;
						}

						if (count($ArrList) > 0)
							$this->HapusBarisDetail($ArrList);
					}
				}
			}
		}
	}

	private function HapusBarisDetail($arr)
	{
		//$this->load->model('rawat_jalan/am_request_rawat_jalan_detail');        

		$mError = "";

		foreach ($arr as $x) {
			$this->load->model('rawat_jalan/tblkasirdetailrrjw');

			$criteria = "no_transaksi = '" . $x['NO_TRANSAKSI'] . "' AND urut = " . $x['URUT'];
			//$query = $this->am_request_rawat_jalan_detail->readforsure($criteria);
			$this->tblkasirdetailrrjw->db->where($criteria, null, false);
			$query = $this->tblkasirdetailrrjw->GetRowList(0, 1, "", "", "");

			//if ($query->num_rows()>0)
			if ($query[1] > 0) {

				$result = $this->tblkasirdetailrrjw->Delete();
				if ($result == 0) {
					$mError .= "";
				} else $mError = "Gagal Delete";
			}
		}

		return $mError;
	}



	private function GetListDetail($JmlField, $List, $JmlList, $TrKodeTranskasi)
	{
		//$tgl=date('d-m-Y');
		//echo $tgl;

		$arrList = $this->splitListDetail($List, $JmlList, $JmlField);

		$arrListField = array();
		$arrListRow = array();
		// echo();
		if (count($arrList) > 0) {
			foreach ($arrList as $str) {
				for ($i = 0; $i < $JmlField; $i += 1) {
					$splitField = explode("=", $str[$i], 2);
					$arrListField[] = $splitField[1];
				}


				if (count($arrListField) > 0) {
					$arrListRow['NO_TRANSAKSI'] = $TrKodeTranskasi;
					if ($arrListField[0] == "" or $arrListField[0] == null) {
						$arrListRow['URUT'] = 0;
					} else $arrListRow['URUT'] = $arrListField[0];

					$arrListRow['KD_PRODUK'] = $arrListField[1];

					if ($arrListField[3] != "" and $arrListField[3] != "undefined") {
						list($tgl, $bln, $thn) = explode('/', $arrListField[3], 3);
						$ctgl = strtotime($tgl . $bln . $thn);
						$arrListRow['TGL_BERLAKU'] = date("Y-m-d", $ctgl);
					}
					$arrListRow['QTY'] = $arrListField[2];
					$arrListRow['HARGA'] = str_replace(".", "", $arrListField[4]);
					$arrListRow['KD_TARIF'] = 'TU';
					$arrListRow['KD_KASIR'] = '01';
					$arrListRow['TGL_TRANSAKSI'] = date("Y-m-d");
					$arrListRow['KD_USER'] = 0;
					$arrListRow['KD_UNIT'] = '202';
					$arrListRow['CHARGE'] = 'true';
					$arrListRow['ADJUST'] = 'false';
					$arrListRow['FOLIO'] = '';
					$arrListRow['SHIFT'] = 1;
					$arrListRow['KD_DOKTER'] = '';
					$arrListRow['KD_UNIT_TR'] = '';
					$arrListRow['CITO'] = 0;
					$arrListRow['JS'] = 0;
					$arrListRow['JP'] = 0;
					$arrListRow['NO_FAKTUR'] = '';
					$arrListRow['FLAG'] = 0;
					$arrListRow['TAG'] = 'false';
				}
			}
		}

		return $arrListRow;
	}

	private function splitListDetail($str, $jmlList, $jmlField)
	{
		$splitList = explode("##[[]]##", $str, $jmlList);

		$arrList = array();

		for ($i = 0; $i < $jmlList; $i += 1) {
			$splitRecord = explode("@@##$$@@", $splitList[$i], $jmlField);
			$arrList = array($splitRecord);
		}

		return $arrList;
	}


	private function GetUrutRequestDetail($TrKodeTranskasi)
	{

		$this->load->model('rawat_jalan/tblkasirdetailrrjw');
		$criteria = "no_transaksi = '" . $TrKodeTranskasi . "'";
		$this->tblkasirdetailrrjw->db->where($criteria,  null, false);
		$res = $this->tblkasirdetailrrjw->GetRowList(0, 1, "DESC", "urut", "");

		$retVal = 1;

		if ($res[1] > 0)
			$retVal = $res[0][0]->URUT + 1;




		return $retVal;
	}
}
