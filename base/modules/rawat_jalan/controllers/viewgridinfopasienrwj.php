<?php

/**
 * @author
 * @copyright
 */


class viewgridinfopasienrwj extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params = null)
    {
        /*  try
        {
			
		$tgl=date('M-d-Y');
			$this->load->model('rawat_jalan/tblviewkunjunganedit');
                         if (strlen($Params[4])!==0)
                        {
							$this->db->where(str_replace("~", "'",$Params[4]. " limit 50 "  ) ,null, false) ;
							$res = $this->tblviewkunjunganedit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
						}	else{
								 $this->db->where(str_replace("~", "'", "") ,null, false) ;
								 $res = $this->tblviewkunjunganedit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
								}

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
 */
        $traces = array();
        $result = $this->db->query("SELECT  * from (
				select k.kd_pasien,p.nama as nama_pasien,p.alamat,u.nama_unit,k.kd_unit,k.urut_masuk,k.tgl_masuk, d.nama as dokter 
				,sjp.no_sjp,CONVERT(varchar(8), k.jam_masuk, 108) AS jam,k.tgl_keluar,p.telepon,
				kon.customer, zu.full_name,
                case when k.baru=1 THEN 'Baru'
                when k.baru=0 THEN 'Lama'
                END AS kunjungan 
				from kunjungan k
				INNER JOIN pasien p on p.kd_pasien=k.kd_pasien 
				LEFT JOIN unit u on k.kd_unit=u.kd_unit 
				LEFT JOIN dokter d on d.kd_dokter=k.kd_dokter
				LEFT JOIN sjp_kunjungan sjp on 
                    k.kd_unit=sjp.kd_unit and 
                    sjp.kd_pasien=k.kd_pasien AND 
                    sjp.tgl_masuk=k.tgl_masuk and 
                    sjp.urut_masuk=k.urut_masuk 
                LEFT JOIN transaksi t ON 
                    t.kd_pasien = k.kd_pasien AND 
                    t.tgl_transaksi = k.tgl_masuk AND 
                    t.urut_masuk = k.urut_masuk AND 
                    t.kd_unit = k.kd_unit
                LEFT JOIN zusers zu ON 
                    zu.kd_user = t.kd_user
				inner join customer kon on kon.kd_customer=k.kd_customer
				
										) as resdata WHERE " . str_replace("~", "'", $Params[4]) . " order by tgl_masuk desc")->result();
        for ($i = 0; $i < count($result); $i++) {
            $trace                = array();
            $date                 = new DateTime($result[$i]->tgl_masuk);
            $trace['TGL_MASUK']   = strval($date->format('d/m/Y'));
            $datejammasuk         = new DateTime($result[$i]->jam);
            $trace['JAM']         = $result[$i]->jam;
            $trace['KD_PASIEN']   = $result[$i]->kd_pasien;
            $trace['NAMA_PASIEN'] = $result[$i]->nama_pasien;
            $trace['NAMA_UNIT']   = $result[$i]->nama_unit;
            $trace['DOKTER']      = $result[$i]->dokter;
            $trace['ALAMAT']      = $result[$i]->alamat;
            $trace['TELEPON']     = $result[$i]->telepon;
            $trace['OPERATOR']     = $result[$i]->full_name;
            $trace['KUNJUNGAN']   = $result[$i]->kunjungan;
            $traces[] = $trace;
        }
        echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($traces) . '}';
    }
}
