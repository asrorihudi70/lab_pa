<?php
class viewgridkasirjdetail extends MX_Controller {
    public function __construct(){
        parent::__construct();
    }	 
    public function index(){
		$this->load->view('main/index');
    }
	public function read($Params=null,$Params2=null){
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
        //$this->load->model('rawat_jalan/viewrequestcmdetailmodel');
        $this->load->model('rawat_jalan/tblviewkasirrwjdetailgrid');
		// $kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
        if (strlen($Params[4])>0){
            $criteria = str_replace("~" ,"'",$Params[4]." ");
            $this->tblviewkasirrwjdetailgrid->db->where($criteria, null, false);
        }
        $query = $this->tblviewkasirrwjdetailgrid->GetRowList( $Params[0], $Params[1], "","", "");
		$res=$query[0];
		if($res != ''){
			/* for($i=0,$iLen=count($res); $i<$iLen;$i++){
				$result = $this->db->query("1.select tr.*, d.nama,pc.component 
					from detail_trdokter tr
						inner join dokter d on d.kd_dokter=tr.kd_dokter
						inner join produk_component pc on pc.kd_component=tr.kd_component
					where ".$criteria."
						and urut=".$res[$i]->URUT." and tgl_transaksi='".$res[$i]->TGL_TRANSAKSI."'
					order by pc.component")->result();
				$dokter='';
				for($j=0,$jLen=count($result);$j<$jLen;$j++){
					if($dokter!=''){
						$dokter.=', ';
					}
					$dokter.=$result[$j]->nama;
				}
				$res[$i]->NAMA_DOKTER=$dokter;
			} */
			for($i=0,$iLen=count($res); $i<$iLen;$i++){
				$nama_dokter 	= '';
				$urut = $res[$i]->URUT;
				$num = 1;
				$spasi = "";
				$get_data = $this->db->query("SELECT * FROM
														visite_dokter A
														INNER JOIN dokter B ON B.kd_dokter = A.kd_dokter 
													WHERE
													$criteria AND urut = '$urut'")->result();
				foreach($get_data as $rec){
					$nama_dokter .= $rec->NAMA;
					if($num < count($get_data)){
						$spasi = " - ";
						$nama_dokter .= $spasi;
					}
					$num++;
				}
				$res[$i]->NAMA_DOKTER_VISITE = $nama_dokter;
			}
		}
		//Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($res).'}';
        //echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';
	}
}
?>