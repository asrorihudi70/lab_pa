<?php

class viewjadwaldokter extends MX_Controller
 {
 	

	public function __construct()
	{
		//parent::Controller();
		parent::__construct();
		$this->id_user = $this->session->userdata['user_id']['id'];    
	}


	public function index()
	{
		$this->load->view('main/index');
	}


	public function read($Params=null)
	{
		
		try
		{
			$this->load->model('rawat_jalan/tblviewjadwaldokter');
			$kd_unit = $this->db->query("SELECT * FROM zusers where kd_user='".$this->id_user."'")->row()->kd_unit;
           if (strlen($Params[4])!==0)
           {
				// $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
				$this->db->where(str_replace("~", "'", " kd_unit in (".$kd_unit.") ".$Params[4]." order by nama_unit") ,null, false) ;
           }else{
				$this->db->where(str_replace("~", "'", " kd_unit in (".$kd_unit.") ".$Params[4]." order by nama_unit") ,null, false) ;
		   }
			// $this->db->group_by(array("nama_unit"));  
			// $this->db->order_by("nama_unit", "asc");
			$res = $this->tblviewjadwaldokter->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			//return $res;
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
		echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
		
	}


	 function save($param)
	 {
		$db=$this->load->database('otherdb3',true);
		$this->db->trans_begin();
		$this->load->model('setup/tblam_jadwaldokter');
		
		$kd_dokter_asal		= $param['kd_dokter_asal'];
		$nama_dokter_asal	= $param['nama_dokter_asal'];
		$kd_unit_asal		= $param['kd_unit_asal'];
		$nama_unit_asal		= $param['nama_unit_asal'];
		$hari_asal			= $param['hari_asal'];
		$hari_asal_day		= 'DAY_'.$param['hari_asal'];
		
		switch($param['Hari'])
		{
			case 'Minggu' 	: 	$Arr['hari'] = 7;
								break;
			case 'Senin' 	: 	$Arr['hari'] = 1;
								break;
			case 'Selasa' 	: 	$Arr['hari'] = 2;
								break;
			case 'Rabu' 	: 	$Arr['hari'] = 3;
								break;
			case 'Kamis' 	: 	$Arr['hari'] = 4;
								break;
			case 'Jumat' 	: 	$Arr['hari'] = 5;
								break;
			case 'Sabtu' 	: 	$Arr['hari'] = 6;
								break;
		}
		$jam=$param['jam'].':'.$param['menit'].':00';
		$Arr['jam']='1900-01-01 '.$jam;
		$Arr['kd_dokter']		= $param['Kd_Dokter'];
		
		if($param['Kd_Unit'] == ''){
			$Arr['kd_unit']			= $kd_unit_asal;
			$param['Kd_Unit']		= $kd_unit_asal;
		}else{
			$Arr['kd_unit']			= $param['Kd_Unit'];
		}
		$Arr['max_pelayanan']	= $param['maxantri'];
		$Arr['durasi_periksa']	= $param['durasi'];
		
		
		
		$qPostgres=count($this->db->query("
				select * from jadwal_poli 
				where 
					kd_dokter	='".$Arr['kd_dokter']."'
				and kd_unit		='".$Arr['kd_unit']."'
				and hari		='".$Arr['hari']."' 
		")->result());
		
		if ($qPostgres<>0)
		{
			$res=$this->db->query("
				update jadwal_poli set 
					jam				= '".$Arr['jam']."' , 
					max_pelayanan	= ".$Arr['max_pelayanan'].", 
					durasi_periksa	= ".$Arr['durasi_periksa']." 
				where 	
						kd_dokter	='".$kd_dokter_asal."' 
					and kd_unit		='".$kd_unit_asal."' 
					and hari		='".$hari_asal."' 
			");
			
		}
		else
		{
			$cek_jadwal_poli_update=count($this->db->query("
					select * from jadwal_poli 
					where 
						kd_dokter	='".$kd_dokter_asal."' 
					and kd_unit		='".$kd_unit_asal."' 
					and hari		='".$hari_asal."' 
			")->result());
			
			if($cek_jadwal_poli_update > 0){
				$res=$this->db->query("
					update jadwal_poli set 
						kd_dokter		= '".$Arr['kd_dokter']."' ,
						kd_unit			= '".$Arr['kd_unit']."',
						hari			= '".$Arr['hari']."',
						jam				= '".$Arr['jam']."' , 
						max_pelayanan	= ".$Arr['max_pelayanan'].", 
						durasi_periksa	= ".$Arr['durasi_periksa']." 
					where 	
							kd_dokter	='".$kd_dokter_asal."' 
						and kd_unit		='".$kd_unit_asal."' 
						and hari		='".$hari_asal."' 
				");
			}else{
				$res=$this->tblam_jadwaldokter->Save($Arr);
			}
		}
		
		
		$ArrMySql['kd_dokter']		= $param['Kd_Dokter'];
		$ArrMySql['nama_dok']		= $param['Dokter'];
		$ArrMySql['kd_unit']		= $param['Kd_Unit'];
		$ArrMySql['nama_unit']		= $param['Unit'];
		$ArrMySql['hari']			= 'DAY_'.$Arr['hari'];
		$ArrMySql['jam']			= $jam;
		$ArrMySql['max_pelayanan']	= $param['maxantri'];
		$ArrMySql['durasi_periksa']	= $param['durasi'];
		
		$get_dokter 		= $db->query("select dokter_id from rs_dokter where kd_dokter = '".$param['Kd_Dokter']."'")->row();
		$dokter_id	= '';
		
		if(count($get_dokter) > 0){
			$dokter_id = $get_dokter->dokter_id;
		}else{
			//insert rs_dokter ke mysql
			$paramsInsert = array(
				'kd_dokter' 		=> $ArrMySql['kd_dokter'],
				'first_name' 		=> $ArrMySql['nama_dok']
			);
			$simpanMySql 	= $db->insert("rs_dokter", $paramsInsert);
			$dokter_id 		= $db->query("select dokter_id from rs_dokter where kd_dokter='".$ArrMySql['kd_dokter']."'")->row()->dokter_id;
		}
		
		$get_dokter_asal 	= $db->query("select dokter_id from rs_dokter where kd_dokter = '".$kd_dokter_asal."'")->row();
		$dokter_asal_id	= '';
		if(count($get_dokter_asal) > 0){
			$dokter_asal_id = $get_dokter_asal->dokter_id;
		}else{
			//insert rs_dokter ke mysql
			$paramsInsert = array(
				'kd_dokter' 		=> $kd_dokter_asal,
				'first_name' 		=> $nama_dokter_asal
			);
			$simpanMySql 		= $db->insert("rs_dokter", $paramsInsert);
			$dokter_asal_id 	= $db->query("select dokter_id from rs_dokter where kd_dokter='".$kd_dokter_asal."'")->row()->dokter_id;
		}
		
		
		$get_unit = $db->query("select unit_id from rs_unit where unit_code = '".$param['Kd_Unit']."'")->row();
		$unit_id	= '';
		if(count($get_unit) > 0){
			$unit_id = $get_unit->unit_id;
		}else{
			//insert rs_unit ke mysql
			
			$get_unit_bpjs = $this->db->query("select unit_bpjs from map_unit_bpjs where kd_unit='".$param['Kd_Unit']."'")->row();
			$unit_bpjs='';
			if(count ($get_unit_bpjs) > 0){
				$unit_bpjs = $get_unit_bpjs->unit_bpjs;
			}
			$paramsInsert = array(
				'unit_type' 		=> 'UNITTYPE_RWJ',
				'unit_code' 		=> $ArrMySql['kd_unit'],
				'unit_name'			=> $ArrMySql['nama_unit'],
				'active_flag'		=> 1,
				'kd_unit_bpjs'		=> $unit_bpjs
			);
			$simpanMySql 	= $db->insert("rs_unit", $paramsInsert);
			$unit_id 		= $db->query("select unit_id from rs_unit where unit_code='".$ArrMySql['kd_unit']."'")->row()->unit_id;
		
		}
		
		$get_unit_asal = $db->query("select unit_id from rs_unit where unit_code = '".$kd_unit_asal."'")->row();
		$unit_asal_id	= '';
		if(count($get_unit) > 0){
			$unit_asal_id = $get_unit_asal->unit_id;
		}else{
			//insert rs_unit ke mysql
			
			$get_unit_asal_bpjs = $this->db->query("select unit_bpjs from map_unit_bpjs where kd_unit='".$kd_unit_asal."'")->row();
			$unit_asal_bpjs='';
			if(count ($get_unit_asal_bpjs) > 0){
				$unit_asal_bpjs = $get_unit_asal_bpjs->unit_bpjs;
			}
			$paramsInsert = array(
				'unit_type' 		=> 'UNITTYPE_RWJ',
				'unit_code' 		=> $kd_unit_asal,
				'unit_name'			=> $nama_unit_asal,
				'active_flag'		=> 1,
				'kd_unit_bpjs'		=> $unit_asal_bpjs
			);
			$simpanMySql 	= $db->insert("rs_unit", $paramsInsert);
			$unit_asal_id 		= $db->query("select unit_id from rs_unit where unit_code='".$kd_unit_asal."'")->row()->unit_id;
		
		}
		
		
		$qMySql=count(
			$db->query("
			SELECT * from rs_jadwal_poli 
			WHERE 
				dokter_id		='".$dokter_id."' 
				and unit_id		='".$unit_id."' 
				and hari		='".$ArrMySql['hari']."' 
			")->result()
		);
		
		if ($qMySql<>0)
		{
			$simpanMySql=$db->query("
				UPDATE rs_jadwal_poli set 
					jam				='".$jam."' , 
					max_pelayanan	=".$ArrMySql['max_pelayanan'].", 
					durasi_periksa	=".$ArrMySql['durasi_periksa']." 
				WHERE 
					dokter_id		='".$dokter_asal_id."' 
				and unit_id			='".$unit_asal_id."' 
				and hari			='".$hari_asal_day."' 
			"); 
			
			
		}
		else
		{
			$cek_jadwal_update = count(
				$db->query("
				SELECT * from rs_jadwal_poli 
				WHERE 
					dokter_id		='".$dokter_asal_id."' 
				and unit_id			='".$unit_asal_id."' 
				and hari			='".$hari_asal_day."' 
				")->result()
			);
			
			if($cek_jadwal_poli_update > 0){
				$simpanMySql=$db->query("
					UPDATE rs_jadwal_poli set 
						dokter_id		='".$dokter_id."' ,
						unit_id			='".$unit_id."' ,
						hari			='".$ArrMySql['hari']."',
						jam				='".$jam."' , 
						max_pelayanan	=".$ArrMySql['max_pelayanan'].", 
						durasi_periksa	=".$ArrMySql['durasi_periksa']." 
					WHERE 
						dokter_id		='".$dokter_asal_id."' 
					and unit_id			='".$unit_asal_id."' 
					and hari			='".$hari_asal_day."' 
				");
			}else{
				$paramsInsert = array(
					'dokter_id' 		=> $dokter_id,
					'unit_id' 			=> $unit_id,
					'hari' 				=> $ArrMySql['hari'],
					'jam' 				=> $ArrMySql['jam'],
					'max_pelayanan' 	=> $ArrMySql['max_pelayanan'],
					'durasi_periksa' 	=> $ArrMySql['durasi_periksa']
				);
				$simpanMySql = $db->insert("rs_jadwal_poli", $paramsInsert);
			}
			
		
			
		}
		
		
		if ($res && $simpanMySql)
		{
			$this->db->trans_commit();
			echo '{success: true, kd_unit: "'.$Arr['kd_unit'].'" }';
		}else
		{
			$this->db->trans_rollback();
			echo '{success: false, kd_unit: "'.$Arr['kd_unit'].'" }';
		}
	}
	
	
	function GetNewID()
	{
		$res = $this->tblam_employees->GetRowList( 0 , 1, 'DESC', 'emp_id', '');
		if ($res[1]===0)
		{
			return 1;
		}
		else
		{
			return $res[0][0]->EMP_ID+1;
		}
	}


	public function getsession()
	{
		$session = $this->session->userdata['user_id'];
		var_dump($session);
	}
	 
	 function delete($param)
	 {
		$db=$this->load->database('otherdb3',true);
		$this->db->trans_begin();
		
		switch($param['Hari'])
		{
			case 'Minggu' 	: 	$hari = 7;
								break;
			case 'Senin' 	: 	$hari = 1;
								break;
			case 'Selasa' 	: 	$hari = 2;
								break;
			case 'Rabu' 	: 	$hari = 3;
								break;
			case 'Kamis' 	: 	$hari = 4;
								break;
			case 'Jumat' 	: 	$hari = 5;
								break;
			case 'Sabtu' 	: 	$hari = 6;
								break;
		}
		$hari_day		= 'DAY_'.$hari;
		$this->load->model('setup/tblam_jadwaldokter');
		$Sql=$this->db;
		$Sql->where("hari = '".$hari."' and kd_dokter = '".$param['Kd_Dokter']."' AND kd_unit = '".$param['Kd_Unit']."'", null, false);
		$res=$this->tblam_jadwaldokter->Delete(); 
		
		
		if ($res>0)
		{
			//CEK MYSQL
			$get_dokter 		= $db->query("select dokter_id from rs_dokter where kd_dokter = '".$param['Kd_Dokter']."'")->row();
			$dokter_id	= '';
			
			if(count($get_dokter) > 0){
				$dokter_id = $get_dokter->dokter_id;
			}else{
				//insert rs_dokter ke mysql
				$get_dokter = $this->db->query("select nama from dokter where kd_dokter='".$param['Kd_Dokter']."'")->row();
				$paramsInsert = array(
					'kd_dokter' 		=> $param['Kd_Dokter'],
					'first_name' 		=> $get_dokter->nama
				);
				$simpanMySql 	= $db->insert("rs_dokter", $paramsInsert);
				$dokter_id 		= $db->query("select dokter_id from rs_dokter where kd_dokter='".$param['Kd_Dokter']."'")->row()->dokter_id;
			}
			
			$get_unit = $db->query("select unit_id from rs_unit where unit_code = '".$param['Kd_Unit']."'")->row();
			$unit_id	= '';
			if(count($get_unit) > 0){
				$unit_id = $get_unit->unit_id;
			}else{
				//insert rs_unit ke mysql
				
				$get_unit_bpjs = $this->db->query("select unit_bpjs from map_unit_bpjs where kd_unit='".$param['Kd_Unit']."'")->row();
				$unit_bpjs='';
				if(count ($get_unit_bpjs) > 0){
					$unit_bpjs = $get_unit_bpjs->unit_bpjs;
				}
				
				$get_unit_pg = $this->db->query("select nama_unit from unit where kd_unit='".$param['Kd_Unit']."'")->row();
				$paramsInsert = array(
					'unit_type' 		=> 'UNITTYPE_RWJ',
					'unit_code' 		=> $param['Kd_Unit'],
					'unit_name'			=> $get_unit_pg->nama_unit,
					'active_flag'		=> 1,
					'kd_unit_bpjs'		=> $unit_bpjs
				);
				$simpanMySql 	= $db->insert("rs_unit", $paramsInsert);
				$unit_id 		= $db->query("select unit_id from rs_unit where unit_code='".$ArrMySql['kd_unit']."'")->row()->unit_id;
			
			}
			
			
			$result_jadwal_mysql = count(
				$db->query("
					select * FROM rs_jadwal_poli 
					WHERE
						dokter_id	='".$dokter_id."' 
					and unit_id		='".$unit_id."' 
					and hari		='".$hari_day."' 
				")->row()
			);
			
			if($result_jadwal_mysql > 0){
				$delete_jadwal_mysql = $db->query("
					DELETE FROM rs_jadwal_poli 
					WHERE
						dokter_id	='".$dokter_id."' 
					and unit_id		='".$unit_id."' 
					and hari		='".$hari_day."' 
				");
			}else{
				$delete_jadwal_mysql = 1;
			}
			
			
			if($delete_jadwal_mysql > 0){
				$this->db->trans_commit();
				echo '{success: true}';
			}else{
				$this->db->trans_rollback();
				echo var_dump($param);
			}
		} else {
			echo var_dump($param);
		}
	 }	

 }
 //VIEWSETUPEMPLOYEE

?>