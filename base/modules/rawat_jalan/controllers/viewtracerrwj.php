<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewtracerrwj extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
        
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        $list = array();    
        $sqldatasrv="SELECT DISTINCT ps.ALAMAT, ps.Kd_Pasien, ps.nama, ps.tgl_lahir, u.NAMA_UNIT, u.KD_UNIT, k.TGL_MASUK, k.JAM_MASUK, c.customer , 0 AS STS_PRINT, AP.id_status_antrian, 
        AP.STS_DITEMUKAN, d.KD_DOKTER, d.nama AS nama_dokter, AP.NO_ANTRIAN, K.baru, 
	ROW_NUMBER() OVER (PARTITION BY d.nama ORDER BY k.JAM_MASUK ASC) AS NOANTRIAN
        FROM
          pasien ps
          INNER JOIN ( Kunjungan k INNER JOIN unit u ON k.kd_unit= u.kd_unit ) ON k.Kd_Pasien = ps.Kd_Pasien
          INNER JOIN DOKTER d ON d.KD_DOKTER = k.KD_DOKTER
          INNER JOIN customer c ON c.kd_customer = k.kd_customer
          INNER JOIN ANTRIAN_POLIKLINIK AP ON AP.kd_unit = k.kd_unit 
          AND ap.tgl_transaksi = k.tgl_masuk 
          AND ap.kd_pasien = k.kd_pasien 
        WHERE ";

        $tambah = " ";
        if (strlen($Params[4]) !== 0) {
            $kriteria= $Params[4];
			    $tambah = " ORDER BY d.nama ASC, k.JAM_MASUK ASC";
			    // $tambah = " Order by k.tgl_masuk,k.jam_masuk desc";
        } else {
			    $tambah = " ORDER BY d.nama ASC, k.JAM_MASUK ASC";
          // $tambah = " Order by k.tgl_masuk,k.jam_masuk desc";
          $kriteria="";
        }
       // $dbsqlsrv = $this->load->database('otherdb2',TRUE);
       //$res = $dbsqlsrv->query($sqldatasrv.$kriteria);
       // $res = $dbsqlsrv->query($sqldatasrv.$kriteria.$tambah);
       $res = $this->db->query($sqldatasrv.$kriteria.$tambah);
        
        $urutan = 1;
        foreach ($res->result() as $rec)
        {
          $o=array();
          $o['MEDREC']=$rec->Kd_Pasien;
          $o['PASIEN']=$rec->nama;
          $o['UNIT']=$rec->NAMA_UNIT;
          $o['KD_UNIT']=$rec->KD_UNIT;
          $o['TANGGAL']=date("d-M-Y", strtotime($rec->TGL_MASUK));
          $o['POLI']=$rec->JAM_MASUK;
          $o['UMUR']=$this->db->query("SELECT dbo.GetUmur('".date("Y-m-d", strtotime($rec->tgl_lahir))."', '".date("Y-m-d", strtotime($rec->TGL_MASUK))."') as getumur")->row()->getumur;
          $o['CUSTOMER']=$rec->customer;
          $o['PRINT']=$rec->STS_PRINT;
          $o['BARU']=$rec->baru;
          if($rec->NOANTRIAN != NULL)
          {
            $o['NO_ANTRI']=$rec->NOANTRIAN;
          }else{
            $o['NO_ANTRI']=0;
          }
          
          if($rec->STS_DITEMUKAN === 'false'){
            $o['DITEMUKAN']= false;
          }else{
            $o['DITEMUKAN']= true;
          }
          $o['DOKTER'] = $rec->KD_DOKTER;
          $o['NAMA_DOKTER'] = $rec->nama_dokter;
          $o['ALAMAT'] = $rec->ALAMAT;
          $o['JAM_MASUK'] = date("H:i:s", strtotime($rec->JAM_MASUK));
          $o['URUTAN'] = str_pad($urutan, 5, "0", STR_PAD_LEFT);
          
          $list[]=$o;
          $urutan ++;
        }

        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }

    public function save($Params = null) {
        $no_antrian = $this->db->query("select no_antrian from antrian_poliklinik where kd_pasien='".$_POST['KDPASIEN']."' and tgl_transaksi='".date('Y-m-d')."' and sts_ditemukan='true' and id_status_antrian=1")->row()->no_antrian;

        //_QMS_Query("UPDATE ANTRIAN_POLIKLINIK  SET id_status_antrian = 2, sts_ditemukan = 1 WHERE kd_pasien='".$Params["KDPASIEN"]."' and tgl_transaksi='".date('Y-m-d')."' and sts_ditemukan= 0 and kd_unit='".$Params["KDUNIT"]."'");

        date_default_timezone_set("Asia/Jakarta");
        $JamBerkasMasuk = gmdate("Y-m-d H:i:s", time()+60*60*7);
        //_QMS_Query("UPDATE KUNJUNGAN SET jam_berkas_masuk = '".$JamBerkasMasuk."' where tgl_masuk ='".date('Y-m-d')."' and kd_pasien='".$_POST['KDPASIEN']."' and kd_unit='".$Params["KDUNIT"]."'");

        echo '{success: true}';
    }

    function getNoantrian($kd_unit){
        $no = $this->db->query("select max(no_antrian) as no_antrian from antrian_poliklinik 
                                where tgl_transaksi='".date('Y-m-d')."' and kd_unit='".$kd_unit."'
                                order by no_antrian desc limit 1");
        if(count($no->result()) > 0){
            $no_antrian = $no->row()->no_antrian + 1;
        } else{
            $no_antrian = 1;
        }
        return $no_antrian;
    }
    
   

}
