<?php 	
class Model_visite_dokter extends Model
{
	private $tbl_dokter_inap_int = "dokter_inap_int";
	private $tbl_visite          = "visite_dokter";
	private $tbl_tarif_component = "tarif_component";
	private $tbl_unit 			 = "unit";
	private $tbl_dokter 		 = "dokter";
	private $id_user         = "";
	private $dbSQL           = "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		======================================================================================================================
		======================================================== SQL QUERY ===================================================
		======================================================================================================================
	 */
	
	public function getDataDokterInapSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->select("*, KD_COMPONENT as kd_component, KD_JOB as kd_job, PRC as prc ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_dokter_inap_int);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataDokterSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->select("*, NAMA as nama ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_dokter);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataUnitSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->select("*, KD_UNIT as kd_unit, PARENT as parent ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_unit);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataVisiteDokterSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->select("*");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_visite);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataMaxVisiteDokterSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->select(" MAX(LINE) as line ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_visite);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataTarifCompSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->select("*, TARIF as tarif");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_tarif_component);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function insertVisiteDokterSQL($params){
		$result = false;
		try {
			$this->dbSQL->insert($this->tbl_visite, $params);
			if ($this->dbSQL->trans_status()>0 || $this->dbSQL->trans_status()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function deleteVisiteDokterSQL($criteria){
		$result = false;
		try {
			$this->dbSQL->where($criteria);
			$this->dbSQL->delete($this->tbl_visite);
			if ($this->dbSQL->trans_status()>0 || $this->dbSQL->trans_status()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	/*
		======================================================================================================================
		==================================================== END SQL QUERY ===================================================
		======================================================================================================================
	 */
	
	public function getDataDokterInap($criteria){
		$result = false;
		try {
			$this->db->select("*, kd_component, kd_job, prc ");
			$this->db->where($criteria);
			$this->db->from($this->tbl_dokter_inap_int);
			$result = $this->db->get();
			//$this->db->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataMaxVisiteDokter($criteria){
		$result = false;
		try {
			$this->db->select(" MAX(LINE) as line ");
			$this->db->where($criteria);
			$this->db->from($this->tbl_visite);
			$result = $this->db->get();
			//$this->dbSQL->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}
	
	public function getDataVisiteDokter($criteria){
		$result = false;
		try {
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from($this->tbl_visite);
			$result = $this->db->get();
			//$this->db->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataTarifComp($criteria){
		$result = false;
		try {
			$this->db->select("*");
			$this->db->where($criteria);
			$this->db->from($this->tbl_tarif_component);
			$result = $this->db->get();
			//$this->db->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function insertVisiteDokter($params){
		$result = false;
		try {
			$this->db->insert($this->tbl_visite, $params);
			if ($this->db->trans_status()>0 || $this->db->trans_status()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}


	public function getDataUnit($criteria){
		$result = false;
		try {
			$this->db->select(" * ");
			$this->db->where($criteria);
			$this->db->from($this->tbl_unit);
			$result = $this->db->get();
			//$this->db->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function getDataDokter($criteria){
		$result = false;
		try {
			$this->db->select("*, nama as nama");
			$this->db->where($criteria);
			$this->db->from($this->tbl_dokter);
			$result = $this->db->get();
			//$this->db->close();
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}

	public function deleteVisiteDokter($criteria){
		$result = false;
		try {
			$this->db->where($criteria);
			$this->db->delete($this->tbl_visite);
			if ($this->db->trans_status()>0 || $this->db->trans_status()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		return $result;
	}
}
?>