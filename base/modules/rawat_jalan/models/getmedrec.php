<?php

class getmedrec extends TblBase
{
    function __construct()
    {
        $this->TblName='pasien';
        TblBase::TblBase(true);
        //menghilangkan tanda "-" pada record yang di load dari database
        //$this->SqlQuery="Select replace(kd_pasien,'-','') as kd_pasien from pasien Where kd_pasien Like '%-%' Order By kd_pasien desc Limit 1";
        $this->SqlQuery="Select top 1 replace(kd_pasien,'-','') as kd_pasien from pasien Where kd_pasien Like '%-%' Order By kd_pasien desc";
    }

    function FillRow($rec)
    {
        $row=new Rowviewgetmedrec;
        $row->KD_PASIEN=$rec->kd_pasien;
        return $row;
    }
}

class Rowviewgetmedrec
{
    public $KD_PASIEN;
}

?>
