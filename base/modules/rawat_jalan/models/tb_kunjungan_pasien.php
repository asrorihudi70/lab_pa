<?php

class tb_kunjungan_pasien extends TblBase
{
    function __construct()
    {
        $this->TblName='kunjungan';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkunjunganpasien;

          $row->KD_PASIEN=$rec->KD_PASIEN;
          $row->KD_UNIT=$rec->KD_UNIT;
          $row->TGL_MASUK=$rec->TGL_MASUK;
          $row->URUT_MASUK=$rec->URUT_MASUK;
          $row->JAM_MASUK=$rec->JAM_MASUK;
          $row->CARA_PENERIMAAN=$rec->CARA_PENERIMAAN;
          $row->KD_RUJUKAN=$rec->KD_RUJUKAN;
          $row->ASAL_PASIEN=$rec->ASAL_PASIEN;
          $row->KD_DOKTER=$rec->KD_DOKTER;
          $row->BARU=$rec->BARU;
          $row->KD_CUSTOMER=$rec->KD_CUSTOMER;
          $row->SHIFT=$rec->SHIFT;
          $row->KARYAWAN=$rec->KARYAWAN;
          $row->KONTROL=$rec->KONTROL;
          $row->ANTRIAN=$rec->ANTRIAN;
          $row->NO_SURAT=$rec->NO_SURAT;
          $row->ALERGI=$rec->ALERGI;
          $row->ANAMNESE=$rec->ANAMNESE;
          $row->CAT_FISIK=$rec->CAT_FISIK;

          return $row;
    }

}

class Rowviewkunjunganpasien
{
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $JAM_MASUK;
    public $CARA_PENERIMAAN;
    public $KD_RUJUKAN;
    public $ASAL_PASIEN;
    public $KD_DOKTER;
    public $BARU;
    public $KD_CUSTOMER;
    public $SHIFT;
    public $KARYAWAN;
    public $KONTROL;
    public $ANTRIAN;
    public $NO_SURAT;
    public $ALERGI;
    public $ANAMNESE;
    public $CAT_FISIK;
}

?>
