<?php

class tbl_bookingonline extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="
            SELECT 
                bo.*,
                p.nama as  nama_pasien,
                d.nama as  nama_dokter,
                u.nama_unit as  nama_unit,
                c.customer as  customer,
                k.jenis_cust as jenis_cust 
            FROM
                booking_online bo  
            INNER JOIN pasien p ON p.kd_pasien = bo.kd_pasien
            INNER JOIN unit u ON u.kd_unit = bo.kd_unit 
            INNER JOIN dokter d ON d.kd_dokter = bo.kd_dokter 
            INNER JOIN customer c ON c.kd_customer = bo.kd_customer 
            INNER JOIN kontraktor k ON k.kd_customer = bo.kd_customer
        ";
    }

    function FillRow($rec){
        $row = new RowData;
        $row->KD_PASIEN     = $rec->kd_pasien;
        $row->NAMA_PASIEN   = $rec->nama_pasien;
        $row->NAMA_DOKTER   = $rec->nama_dokter;
        $row->NAMA_UNIT     = $rec->nama_unit;
        $row->KD_UNIT       = $rec->kd_unit;
        $row->KD_DOKTER     = $rec->kd_dokter;
        $row->TGL_KUNJUNGAN = date_format(date_create($rec->tgl_kunjungan), 'Y-m-d');
        $row->TGL_BOOKING   = date_format(date_create($rec->tgl_booking), 'Y-m-d');
        $row->URUT_MASUK    = $rec->urut_masuk;
        $row->CUSTOMER      = $rec->customer;
        $row->KD_CUSTOMER   = $rec->kd_customer;
        $row->ANTRIAN       = $rec->antrian;
        $row->JENIS_CUST    = $rec->jenis_cust;
        $row->NOMOR_SKDP    = $rec->nomor_skdp;
        return $row;
    }
}

class RowData{
    public $KD_PASIEN;
    public $NAMA_PASIEN;
    public $NAMA_DOKTER;
    public $NAMA_UNIT;
    public $KD_UNIT;
    public $KD_DOKTER;
    public $TGL_KUNJUNGAN;
    public $TGL_BOOKING;
    public $URUT_MASUK;
    public $ANTRIAN;
    public $CUSTOMER;
    public $KD_CUSTOMER;
    public $JENIS_CUST;
	public $NOMOR_SKDP;
}

?>
