<?php

class tbl_datapasien extends TblBase
{
    function __construct()
    {
        $this->TblName='vi_viewdatapasein';
        TblBase::TblBase(true);

        $this->SqlQuery= "
		 SELECT pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, 
		 pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir, 
		 agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, 
		 pasien.alamat, pasien.kd_kelurahan, pendidikan.pendidikan, pekerjaan.pekerjaan, 
		 kb.kd_kabupaten,kb.kabupaten, kc.kd_kecamatan,kc.kecamatan, pr.propinsi,
		 pasien.email,pasien.handphone,pasien.nama_ayah,pasien.nama_ibu,
		 kl.kelurahan,pr.kd_propinsi, pasien.kd_pekerjaan, 
		 pasien.kd_pendidikan, pasien.telepon ,pasien.kd_agama
		 FROM pasien left join agama ON pasien.kd_agama = agama.kd_agama 
		 left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
		 left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
		 left join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
		 left join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
		 left join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
		 left join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI";
        }

    function FillRow($rec)
    {
        $row=new Rowviewrwj;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA=$rec->nama;
          $row->NAMA_KELUARGA=$rec->nama_keluarga;
          $row->JENIS_KELAMIN=$rec->jenis_kelamin;
          $row->TEMPAT_LAHIR=$rec->tempat_lahir;
          $row->TGL_LAHIR=$rec->tgl_lahir;
          $row->AGAMA=$rec->agama;
          $row->GOL_DARAH=$rec->gol_darah;
          $row->WNI=$rec->wni;
          $row->STATUS_MARITA=$rec->status_marita;
          $row->ALAMAT=$rec->alamat;
          $row->PENDIDIKAN=$rec->pendidikan;
          $row->PEKERJAAN=$rec->pekerjaan;
		  $row->KD_KABUPATEN=$rec->kd_kabupaten;
          $row->KD_KECAMATAN=$rec->kd_kecamatan;
          $row->KECAMATAN=$rec->kecamatan;
		  $row->PROPINSI=$rec->propinsi;
          $row->KABUPATEN=$rec->kabupaten;
		  $row->KD_PROPINSI=$rec->kd_propinsi;
          $row->KD_KELURAHAN=$rec->kd_kelurahan;
          $row->KD_AGAMA=$rec->kd_agama;
          $row->KD_PEKERJAAN=$rec->kd_pekerjaan;
          $row->KD_PENDIDIKAN=$rec->kd_pendidikan;
          $row->TELEPON=$rec->telepon;
		  $row->KELURAHAN=$rec->kelurahan;
          $row->EMAIL=$rec->email;
          $row->HP=$rec->handphone;
          $row->AYAH=$rec->nama_ayah;
		  $row->IBU=$rec->nama_ibu;
	  
        return $row;
    }

}

class Rowviewrwj
{ 			
		  public $KD_KABUPATEN;
	      public $KD_PROPINSI;
		  public $KD_KECAMATAN;
          public $KD_KELURAHAN;
		  Public $KELURAHAN;
		  public $KABUPATEN;
          public $KECAMATAN;
          public $PROPINSI;         
		 
		 Public $EMAIL;
		  public $HP;
          public $AYAH;
          public $IBU;         
		 
		  public $KD_PASIEN;
          public $NAMA;
          public $NAMA_KELUARGA;
          public $JENIS_KELAMIN;
          public $TEMPAT_LAHIR;
          public $TGL_LAHIR;
          public $AGAMA;
          public $GOL_DARAH;
          public $WNI;
          public $STATUS_MARITA;
          public $ALAMAT;
          public $PENDIDIKAN;
          public $PEKERJAAN;
          public $KD_UNIT;
          public $TGL_MASUK;
          public $URUT_MASUK;
         
          public $KD_AGAMA;
          public $KD_PEKERJAAN;
          public $KD_PENDIDIKAN;
          public $TELEPON;
          public $NAMA_UNIT;
}

?>
