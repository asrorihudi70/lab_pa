<?php
class tbl_distribusi_tracer extends TblBase
{
    function __construct()
    {
		$this->TblName  ='tblviewkunjunganedit';
		TblBase::TblBase(true);
		$this->StrSql   ="kd_pasien, nama_pasien, alamat, nama_unit, kd_unit, tgl_masuk, dokter, lokasi, tgl_keluar, tgl_kembali, kd_unit_kembali, status";
		
		$this->SqlQuery = "SELECT 
						p.kd_pasien, p.nama as nama_pasien, p.alamat , k.tgl_masuk, u.nama_unit, u.kd_unit, l.lokasi, s.tgl_keluar, s.tgl_kembali, s.kd_unit_kembali, s.status, LEFT(u.kd_unit, 1) as parent_unit
						from kunjungan k 
						inner join pasien p on k.kd_pasien = p.kd_pasien 
						inner join unit u on  u.kd_unit    = k.kd_unit 
						left join lokasi_kartu l on l.kd_pasien = p.kd_pasien
						left join status_kartu s on s.kd_pasien = p.kd_pasien 
						";

    }

	function FillRow($rec)
	{
		$row=new Rowtblviewkasirdetailrwj;

		$row->KD_PASIEN = $rec->kd_pasien;
		$row->NAMA_PASIEN = $rec->nama_pasien;
		$row->ALAMAT = $rec->alamat;
		$row->TGL_MASUK = $rec->tgl_masuk;
		$row->NAMA_UNIT = $rec->nama_unit;
		$row->KD_UNIT = $rec->kd_unit;
		$row->LOKASI = $rec->lokasi;
		$row->TGL_KELUAR = $rec->tgl_keluar;
		$row->TGL_KEMBALI = $rec->tgl_kembali;
		$row->KD_UNIT_KEMBALI = $rec->kd_unit_kembali;
		$row->STATUS = $rec->status;
		$row->PARENT_UNIT = $rec->parent_unit;
		return $row;
    }

}

class Rowtblviewkasirdetailrwj
{
	public $KD_PASIEN;
	public $NAMA_PASIEN;
	public $ALAMAT;
	public $TGL_MASUK;
	public $NAMA_UNIT;
	public $KD_UNIT;
	public $LOKASI;
	public $TGL_KELUAR;
	public $TGL_KEMBALI;
	public $KD_UNIT_KEMBALI;
	public $STATUS;
	public $PARENT_UNIT;
}

?>
