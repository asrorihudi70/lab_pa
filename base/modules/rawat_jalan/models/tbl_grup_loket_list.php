<?php

class tbl_grup_loket_list extends TblBase
{
    function __construct()
    {
        $this->TblName='loket';
        TblBase::TblBase(true);

        $this->SqlQuery='SELECT * from antrian_group '; //order by id_group asc
    }

    function FillRow($rec)
    {
        $row=new RowviewLoket;
        $row->ID_GROUP=$rec->ID_GROUP;
		$row->NAMA_GROUP=$rec->NAMA_GROUP;
		$row->ID_NAMA=$rec->ID_GROUP." - ".$rec->NAMA_GROUP;
        
        return $row;
    }
    
    function readAll()
    {
    
        //$this->db->where('parent', '2');
        //$this->db->order_by('status', 'asc');
        $this->db->select('*');
        $this->db->from('antrian_group');
        $query = $this->db->get();

        return $query;
    }

}

class RowviewLoket
{
   public $ID_GROUP;  
   public $NAMA_GROUP;  
   public $ID_NAMA;

}

?>
