﻿<?php

class tbl_kelompok_pasien extends TblBase
{
    function __construct()
    {
        $this->TblName='customer';
        TblBase::TblBase(true);

        $this->SqlQuery='SELECT * FROM customer ORDER BY customer ASC';
    }

    function FillRow($rec)
    {
        $row=new RowviewCustomer;
        $row->KD_CUSTOMER   = $rec->kd_customer;
        $row->CUSTOMER = $rec->customer;
        $row->KOTA    = $rec->kota;
		$row->KOTAandCUSTOMER    = $rec->customer." - ".$rec->kota;
        
        return $row;
    }

}

class RowviewCustomer
{
   public $KD_CUSTOMER;
   public $CUSTOMER;  
   public $KOTA;  
   public $KOTAandCUSTOMER;  

}

?>
