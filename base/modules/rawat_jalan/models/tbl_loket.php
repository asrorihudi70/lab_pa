<?php

class tbl_loket extends TblBase
{
    function __construct()
    {
        $this->TblName='loket';
        TblBase::TblBase(true);

        $this->SqlQuery='SELECT * from antrian_list_loket '; //order by CAST(kode_loket as integer) asc
    }

    function FillRow($rec)
    {
        $row=new RowviewLoket;
        $row->GROUPPING  = $rec->ID_GROUP." - ".$rec->NAMA_LOKET;
        $row->KODE_LOKET = $rec->KODE_LOKET;
        $row->LOKET      = $rec->NAMA_LOKET;
        $row->NAMA_LOKET = $rec->NAMA_LOKET;
        $row->ID_GROUP   = $rec->ID_GROUP;
        
        return $row;
    }
    
    function readAll()
    {
    
        //$this->db->where('parent', '2');
        //$this->db->order_by('status', 'asc');
        $this->db->select('*');
        $this->db->from('loket');
        $query = $this->db->get();

        return $query;
    }

}

class RowviewLoket
{
   public $GROUPPING;
   public $KODE_LOKET;
   public $LOKET;
   public $NAMA_LOKET;  
   public $ID_GROUP;  

}

?>
