<?php

class tbltracer extends TblBase
{
    function __construct()
    {
        $this->TblName='tracer';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewpasientracer;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->NAMA=$rec->nama;
        $row->NAMA_UNIT=$rec->pemegang_asuransi;
        $row->NAMA_DOKTER=$rec->no_asuransi;
        $row->TGL_MASUK=$rec->kd_asuransi;
		$row->JAM_MASUK=$rec->jam_masuk;
        $row->ALAMAT=$rec->kd_suku;
        return $row;
    }
}

class Rowviewpasientracer
{
    public $KD_PASIEN;
    public $NAMA;
    public $NAMA_UNIT;
    public $NAMA_DOKTER;
    public $TGL_MASUK;
	public $JAM_MASUK;
    public $ALAMAT;
}

?>
