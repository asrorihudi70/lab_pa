<?php

class tblviewhistorydatapasien extends TblBase
{
    function __construct()
    {
        $this->TblName='history_trans';
        TblBase::TblBase(true);

        $this->SqlQuery= "";
        }

    function FillRow($rec)
    {
        $row=new Rowviewtblviewhistorydatapasien;

          $row->KD_KASIR=$rec->kd_kasir;
          $row->NO_TRANSAKSI=$rec->no_transaksi;
          $row->TGL_TRANSAKSI=$rec->tgl_transaksi;
          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA=$rec->nama;
          $row->KD_UNIT=$rec->kd_unit;
          $row->NAMA_UNIT=$rec->nama_unit;
          $row->JML=$rec->jml;

        return $row;
    }

}

class Rowviewtblviewhistorydatapasien
{
          public $JUMLAH;
          public $KD_KASIR;
          public $NO_TRANSAKSI;
          public $TGL_TRANSAKSI;
          public $KD_PASIEN;
          public $NAMA;
          public $KD_UNIT;
          public $NAMA_UNIT;
          public $JML;

}

?>
