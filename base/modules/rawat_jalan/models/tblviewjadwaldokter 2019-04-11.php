<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewjadwaldokter extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kode_dokter,kode_unit,hari,nama_unit,nama,jam";
	$this->SqlQuery="select * from(select a.kd_dokter,b.kd_unit,b.nama_unit,c.nama as nama, to_char(jam,'HH24:MI') as jam,
	 case
	 when a.hari = 0 then 'Minggu'
	 when a.hari = 1 then 'Senin'
	 when a.hari = 2 then 'Selasa'
	 when a.hari = 3 then 'Rabu'
	 when a.hari = 4 then 'Kamis'
	 when a.hari = 5 then 'Jumat'
	 when a.hari = 6 then 'Sabtu'
	 end as hari
	 from jadwal_poli a
	 INNER JOIN unit b on a.kd_unit=b.kd_unit
	 INNER JOIN dokter c on a.kd_dokter = c.kd_dokter order by a.hari asc) as resdata ";

		$this->TblName='viewjadwaldokter';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row = new Rowviewjadwaldokter;
		//$row=new Rowviewemployee;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->KD_UNIT=$rec->kd_unit;
		$row->HARI=$rec->hari;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->JAM=$rec->jam;

		return $row;
	}
}
class Rowviewjadwaldokter
{
	public $KD_DOKTER;
	public $KD_UNIT;
	public $HARI;
	public $NAMA_UNIT;
	public $NAMA;
	public $JAM;
}

?>