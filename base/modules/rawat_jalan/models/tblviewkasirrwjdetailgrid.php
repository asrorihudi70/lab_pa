<?php
class tblviewkasirrwjdetailgrid extends TblBase
{
    function __construct()
    {
        $this->TblName = 'viewgridkasirjdetail';
        TblBase::TblBase(true);
        $this->StrSql = "kp_produk,kd_produk,deskripsi,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,total,tunai,dicount,Piutang, no_faktur, kd_kasir, folio";

        $this->SqlQuery = "SELECT * from (
				select detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
				detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
				detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
				detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
				detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,d.jumlah_dokter,dokter.nama as dokter, unit.kd_unit as kd_unitt,produk.kp_produk,
				CASE WHEN LEFT (produk.kd_klas, 1) = '6' THEN '1' ELSE '0' END AS grup,d2.jumlah_dokter AS jumlah_dokter_visite
				from  detail_transaksi 
				inner join produk on detail_transaksi.kd_produk = produk.kd_produk 
				left join (
					select count(visite_dokter.kd_dokter) as jumlah_dokter,no_transaksi,urut,tgl_transaksi,kd_kasir 
					from visite_dokter group by no_transaksi,urut,tgl_transaksi,kd_kasir 
					) as d2 ON 
						d2.no_transaksi = detail_transaksi.no_transaksi AND 
						d2.kd_kasir = detail_transaksi.kd_kasir AND 
						d2.urut = detail_transaksi.urut AND 
						d2.tgl_transaksi = detail_transaksi.tgl_transaksi
				inner join unit on detail_transaksi.kd_unit = unit.kd_unit 
			 left join (select count(detail_trdokter.kd_dokter) as jumlah_dokter,no_transaksi,urut,tgl_transaksi,kd_kasir from detail_trdokter group by no_transaksi,urut,tgl_transaksi,kd_kasir ) as d ON d.no_transaksi = detail_transaksi.no_transaksi AND d.kd_kasir = detail_transaksi.kd_kasir AND d.urut = detail_transaksi.urut AND d.tgl_transaksi = detail_transaksi.tgl_transaksi
			  left join
			  customer on detail_transaksi.kd_customer = customer.kd_customer 
			  left  join
			  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter 
			  
			  
			  --order by  detail_transaksi.urut asc
			  ) as resdata ";
    }
    function FillRow($rec)
    {
        $row = new Rowtblviewkasirrwjdetailgrid;
        $row->KD_PRODUK     = $rec->kd_produk;
        $row->DESKRIPSI     = $rec->deskripsi;
        $row->KD_TARIF      = $rec->kd_tarif;
        $row->DESKRIPSI2    = $rec->deskripsi;
        $text = strtolower($rec->deskripsi);
        if (stripos($text, "konsul") !== false) {
            $row->STATUS_KONSULTASI    = true;
        } else {
            $row->STATUS_KONSULTASI = false;
        }
        $row->HARGA         = $rec->harga;
        $row->FLAG          = $rec->flag;
        $row->QTY           = $rec->qty;
        $row->TGL_BERLAKU   = $rec->tgl_berlaku;
        $row->NO_TRANSAKSI  = $rec->no_transaksi;
        $row->URUT          = $rec->urut;
        $row->JUMLAH_DOKTER_VISITE = $rec->jumlah_dokter_visite;
        $row->ADJUST        = $rec->adjust;
        $row->NO_FAKTUR     = $rec->no_faktur;
        $row->KD_DOKTER     = $rec->kd_dokter;
        $row->KD_UNIT       = $rec->kd_unit;
        $row->GROUP           = $rec->grup;
        $row->KD_UNITT      = $rec->kd_unitt;
        $row->CITO          = $rec->cito;
        $row->KD_CUSTOMER   = $rec->kd_customer;
        // Create a DateTime object from the given date string
        $date = new DateTime($rec->tgl_transaksi);
        // Format the date as d - F - Y
        $newtgl_transaksi = $date->format('d - F - Y');
        $row->TGL_TRANSAKSI = $newtgl_transaksi;
        $row->JUMLAH_DOKTER = $rec->jumlah_dokter;
        $row->KP_PRODUK     = $rec->kp_produk;
        $row->FOLIO         = $rec->folio;

        if (stripos($text, "dokter") !== false && $rec->jumlah_dokter_visite > 0) {
            $dokterVisite = $this->db->query("SELECT '(' + STUFF( (SELECT ',' + nama FROM (
            SELECT d.nama FROM visite_dokter vd
            INNER JOIN dokter d ON d.kd_dokter = vd.kd_dokter 
            WHERE vd.kd_kasir = '" . $rec->kd_kasir . "' AND vd.no_transaksi = '" . $rec->no_transaksi . "'
            AND vd.tag_int='" . $rec->kd_produk . "'
            GROUP BY d.nama
            ) t ORDER BY nama FOR XML PATH('')), 1, 1, '') + ')' AS dokter_visite")->row()->dokter_visite;
            $row->DESKRIPSI = $rec->deskripsi . $dokterVisite;
        }
        // $row->NAMA_DOKTER        = $rec->harga;
        /*
            SELECT dt.kd_dokter, d.nama, detail.kd_unit from detail_trdokter dt
inner join detail_transaksi detail on 
    detail.no_transaksi = dt.no_transaksi and 
    detail.kd_kasir = dt.kd_kasir and
    detail.urut = dt.urut 
inner join dokter d on dt.kd_dokter = d.kd_dokter where dt.no_transaksi = '1983842' and dt.kd_kasir='01'

         */
        $query_pg = $this->db->query("SELECT DISTINCT(dt.kd_dokter), d.nama, detail.kd_unit, detail.kd_produk from detail_trdokter dt
        inner join detail_transaksi detail on 
            detail.no_transaksi = dt.no_transaksi and 
            detail.kd_kasir = dt.kd_kasir and
            detail.urut = dt.urut 
        inner join dokter d on dt.kd_dokter = d.kd_dokter where dt.no_transaksi = '" . $rec->no_transaksi . "' and dt.kd_kasir='" . $rec->kd_kasir . "' and dt.urut='" . $rec->urut . "'");
        if ($rec->kd_unit != 6) {
            if ($query_pg->num_rows() > 0) {
                foreach ($query_pg->result() as $result_query) {
                    if ($result_query->kd_unit != 6 && $result_query->kd_produk == $rec->kd_produk) {
                        $row->DOKTER .= $result_query->nama . ",";
                    }
                }
                $row->DOKTER = substr($row->DOKTER, 0, strlen($row->DOKTER) - 1);
            } else {
                $row->DOKTER        = $rec->dokter;
            }
        }
        return $row;
    }
}

class Rowtblviewkasirrwjdetailgrid
{
    public $KD_PRODUK;
    public $DESKRIPSI;
    public $KD_TARIF;
    public $DESKRIPSI2;
    public $STATUS_KONSULTASI;
    public $HARGA;
    public $FLAG;
    public $QTY;
    public $TGL_BERLAKU;
    public $NO_TRANSAKSI;
    public $URUT;
    public $ADJUST;
    public $NO_FAKTUR;
    public $KD_DOKTER;
    public $KD_UNIT;
    public $KD_UNITT;
    public $CITO;
    public $KD_CUSTOMER;
    public $TGL_TRANSAKSI;
    public $JUMLAH_DOKTER;
    public $DOKTER;
    public $NAMA_DOKTER;
    public $KP_PRODUK;
    public $FOLIO;
}
