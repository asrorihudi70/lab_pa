<?php
class tblviewkunjunganedit extends TblBase
{
    function __construct()
    {
        $this->TblName='tblviewkunjunganedit';
        TblBase::TblBase(true);
		$this->StrSql="kd_pasien,nama_unit,kd_unit,urut_masuk,tgl_masuk, dokter 
		,no_sjp,customer,jam,tgl_keluar";
		
        $this->SqlQuery= "
				select k.kd_pasien,p.nama as nama_pasien,p.alamat,u.nama_unit,k.kd_unit,k.urut_masuk,k.tgl_masuk, d.nama as dokter 
				,sjp.no_sjp,cus.customer,to_char(k.jam_masuk, 'HH24:MI:SS' ) jam,k.tgl_keluar, t.no_transaksi, t.co_status,
				kon.customer, d.kd_dokter, dt.kd_kasir 
				from kunjungan k
				inner join pasien p on p.kd_pasien=k.kd_pasien 
				inner join transaksi t on t.kd_pasien=k.kd_pasien and t.tgl_transaksi = k.tgl_masuk and t.urut_masuk = k.urut_masuk and t.kd_unit = k.kd_unit 
				left join detail_transaksi dt on dt.no_transaksi=t.no_transaksi 
				left join unit u on k.kd_unit=u.kd_unit 
				left join dokter d on d.kd_dokter=k.kd_dokter
				left join customer cus on cus.kd_customer=k.kd_customer
				left join sjp_kunjungan sjp on k.kd_unit=sjp.kd_unit and sjp.kd_pasien=k.kd_pasien AND sjp.tgl_masuk=k.tgl_masuk 
				and sjp.urut_masuk=k.urut_masuk 
				inner join customer kon on kon.kd_customer=k.kd_customer 
				";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirdetailrwj;

        $row->KD_PASIEN = $rec->kd_pasien;
        $row->NAMA_PASIEN = $rec->nama_pasien;
        $row->ALAMAT = $rec->alamat;
        $row->TGL_MASUK = $rec->tgl_masuk;
        $row->NAMA_UNIT = $rec->nama_unit;
        $row->KD_UNIT = $rec->kd_unit;
        $row->DOKTER = $rec->dokter;
		$row->URUT = $rec->urut_masuk;
		$row->JAM = $rec->jam;
		$row->TGL_KELUAR = $rec->tgl_keluar;
		$row->NO_SEP = $rec->no_sjp;
		$row->CUST = $rec->customer;
		$row->NO_TRANSAKSI = $rec->no_transaksi;
		$row->KD_DOKTER = $rec->kd_dokter;
		$row->KD_KASIR = $rec->kd_kasir;
		if($rec->co_status == 'f' || $rec->co_status == false){
			$row->CO_STATUS = "Belum";
		}else{
			$row->CO_STATUS = "Sudah";			
		}
        return $row;
    }

}

class Rowtblviewkasirdetailrwj
{
	public $JAM;
	public $TGL_KELUAR;
    public $KD_PASIEN;
    public $NAMA_PASIEN;
    public $ALAMAT;
    public $TGL_MASUK;
    public $NAMA_UNIT;
    public $KD_UNIT;
    public $DOKTER;
	public $URUT;
	public $NO_SEP;
	public $CUST;
	public $NO_TRANSAKSI;
	public $CO_STATUS;
	public $KD_DOKTER;
	public $KD_KASIR;
}

?>
