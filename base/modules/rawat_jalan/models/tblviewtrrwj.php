﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewtrrwj extends TblBase
{

	function __construct()
	{
		$this->StrSql = "customer,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,urut_masuk,
					kd_kasir,asal_pasien,cara_penerimaan,no_urut,agama,status_marita,pendidikan,telepon,baru_kunjungan'";
		$this->SqlQuery = "SELECT TOP ? *,ROW_NUMBER() OVER (PARTITION BY nama_dokter ORDER BY tgl_transaksi ASC, jam_masuk ASC) AS norut from (
			select customer.customer, unit.kd_unit as kode_unit, unit.kd_unit as tmp_kd_unit, unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, 
			case when dokter.nama = '0' then 'BELUM DIPILIH' else dokter.nama end as nama_dokter, 
			kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter as kode_dokter, transaksi.orderlist,transaksi.posting_transaksi, 
			knt.jenis_cust,
			case 
				when ap.id_status_antrian=0 then 'Tunggu Berkas' 
				when ap.id_status_antrian=1 then 'Tunggu Panggilan' 
				when ap.id_status_antrian=2 then 'Sedang Dilayani' 
				else 'Selesai' end as status_antrian, 
			ap.no_antrian,ap.no_urut,ap.id_status_antrian as ap_status_antrian,
			pasien.jenis_kelamin, pasien.tgl_lahir , pekerjaan.pekerjaan ,agama.agama, pasien.nik,
			pasien.status_marita,pendidikan.pendidikan,pasien.telepon,pasien.tempat_lahir,pasien.nama_ibu,suku.suku,kunjungan.baru as baru_kunjungan
			from (((((kunjungan  
			inner join unit on kunjungan.kd_unit=unit.kd_unit)   
			left join pasien on pasien.kd_pasien=kunjungan.kd_pasien
			left join pekerjaan on pekerjaan.kd_pekerjaan= pasien.kd_pekerjaan
			left join agama on agama.kd_agama = pasien.kd_agama
			left join pendidikan on pendidikan.kd_pendidikan = pasien.kd_pendidikan
			) 
			inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
			inner join customer on customer.kd_customer= kunjungan.kd_customer
			)
			left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
			  
			inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
			and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk
			left join antrian_poliklinik ap on kunjungan.kd_pasien=ap.kd_pasien and kunjungan.tgl_masuk=ap.tgl_transaksi and kunjungan.kd_unit=ap.kd_unit
			left join suku on pasien.kd_suku=suku.kd_suku
		 )as resdata ";
		$this->TblName = 'viewkasirrwj';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row = new Rowdokter;
		$row->NAMA_UNIT         = $rec->nama_unit;
		$row->NAMA              = $rec->nama;
		$row->ALAMAT            = $rec->alamat;
		$row->KD_PASIEN         = $rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI = $rec->tgl_transaksi;
		$row->NAMA_DOKTER       = $rec->nama_dokter;
		$row->NO_TRANSAKSI      = $rec->no_transaksi;
		$row->KD_CUSTOMER       = $rec->KD_CUSTOMER;
		$row->CUSTOMER          = $rec->customer;
		$row->KD_UNIT           = $rec->KD_UNIT;
		$row->KD_DOKTER         = $rec->KD_DOKTER;
		$row->URUT_MASUK        = $rec->URUT_MASUK;
		$row->POSTING_TRANSAKSI = $rec->posting_transaksi;
		$row->ANAMNESE          = $rec->ANAMNESE;
		$row->CAT_FISIK         = $rec->CAT_FISIK;
		// $row->TEST_BUTA_WARNA   = $rec->TEST_BUTA_WARNA;
		$row->KD_KASIR          = $rec->kd_kasir;
		$row->ASAL_PASIEN       = $rec->ASAL_PASIEN;
		$row->CARA_PENERIMAAN   = $rec->CARA_PENERIMAAN;
		$row->STATUS_ANTRIAN    = $rec->status_antrian;
		$row->NO_ANTRIAN        = $rec->norut;
		$row->PEKERJAAN        	= $rec->pekerjaan;
		$row->AGAMA        		= $rec->agama;
		$row->PENDIDIKAN        = $rec->pendidikan;
		$row->TELEPON        	= $rec->telepon;
		$row->JAM_MASUK        	= $rec->JAM_MASUK;
		$row->TGL_TINDAK        = $rec->TGL_TINDAK;
		$row->JAM_TINDAK        = $rec->JAM_TINDAK;
		$row->SUKU        		= $rec->suku;
		$row->NAMA_IBU        	= $rec->nama_ibu;
		$row->TGL_LAHIR        	= $rec->tgl_lahir;
		// $row->TGL_LAHIR        	= date("d/m/Y", strtotime(str_replace(" 00:00:00", "", $rec->tgl_lahir)));
		$row->WNI        		= 'WNI';
		$row->TEMPAT_LAHIR      = $rec->tempat_lahir;
		$row->NIK        		= $rec->nik;

		if (isset($rec->jadwal_dokter) === true) {
			$row->JAM_JADWAL_DOKTER        	= date_format(date_create($rec->jadwal_dokter), 'H:i:s');
		} else {
			$row->JAM_JADWAL_DOKTER        	= date("H:i:s");
		}

		if (isset($rec->waktu_ditindak) === true) {
			$row->JAM_DITINDAK        	= date_format(date_create($rec->waktu_ditindak), 'H:i:s');
		} else {
			$row->JAM_DITINDAK        	= date("H:i:s");
		}

		if (isset($rec->waktu_datang) === true) {
			$row->JAM_DATANG        	= date_format(date_create($rec->waktu_datang), 'H:i:s');
		} else {
			$row->JAM_DATANG        	= date("H:i:s");
		}

		//$row->BARU_KUNJUNGAN        	= $rec->baru_kunjungan;
		if ($rec->baru_kunjungan === true || $rec->baru_kunjungan == 't') {
			$row->BARU_KUNJUNGAN = 'Baru';
		} else {
			$row->BARU_KUNJUNGAN = 'Lama';
		}

		if ($rec->status_marita == 0) {
			$row->STATUS_MARITA = 'Blm Kawin';
		} else if ($rec->status_marita == 1) {
			$row->STATUS_MARITA = 'Kawin';
		} else if ($rec->status_marita == 2) {
			$row->STATUS_MARITA = 'Janda';
		} else if ($rec->status_marita == 3) {
			$row->STATUS_MARITA = 'Duda';
		}

		$row->UMUR        		= $this->getAge(date('Y-m-d'), $rec->tgl_lahir);
		if ($rec->jenis_kelamin === true || $rec->jenis_kelamin == 't') {
			$row->JENIS_KELAMIN = 'Laki-laki';
		} else {
			$row->JENIS_KELAMIN = 'Perempuan';
		}
		return $row;
	}

	private function getAge($tgl1, $tgl2)
	{
		$jumHari      = (abs(strtotime(date_format(date_create($tgl1), 'Y-m-d')) - strtotime(date_format(date_create($tgl2), 'Y-m-d'))) / (60 * 60 * 24));
		$ret          = array();
		$ret['YEAR']  = floor($jumHari / 365);
		$sisa         = floor($jumHari - ($ret['YEAR'] * 365));
		$ret['MONTH'] = floor($sisa / 30);
		$sisa         = floor($sisa - ($ret['MONTH'] * 30));
		$ret['DAY']   = $sisa;

		if ($ret['YEAR'] == 0  && $ret['MONTH'] == 0 && $ret['DAY'] == 0) {
			$ret['DAY'] = 1;
		}
		return $ret;
	}
}
class Rowdokter
{
	public $PEKERJAAN;
	public $KD_UNIT;
	public $NAMA_UNIT;
	public $NAMA;
	public $ALAMAT;
	public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $UMUR;
	public $URUT_MASUK;
	public $POSTING_TRANSAKSI;
	public $ANAMNESE;
	public $CAT_FISIK;
	// public $TEST_BUTA_WARNA;
	public $KD_KASIR;
	public $ASAL_PASIEN;
	public $CARA_PENERIMAAN;
	public $STATUS_ANTRIAN;
	public $NO_ANTRIAN;
	public $JENIS_KELAMIN;
	public $AGAMA;
	public $PENDIDIKAN;
	public $STATUS_MARITA;
	public $BARU_KUNJUNGAN;
	public $JAM_JADWAL_DOKTER;
	public $JAM_DITINDAK;
	public $JAM_DATANG;
	public $SUKU;
	public $NAMA_IBU;
	public $WNI;
	public $TGL_LAHIR;
	public $TEMPAT_LAHIR;
	public $NIK;
}
