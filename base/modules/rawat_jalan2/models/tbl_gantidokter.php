<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_gantidokter extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_dokter, kd_unit, kd_pasien,tgl_masuk";
		$this->TblName='kunjungan';
		TblBase::TblBase();
	}

	function FillRow($rec)
	{
		$row=new RowGantiDOkter;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_PASIEN=$rec->kd_pasien;
		$row->TGL_MASUK=$rec->tgl_masuk;
		return $row;
	}
}
class RowGantiDOkter
{
    public $KD_UNIT;
    public $KD_DOKTER;
	public $KD_PASIEN;
	public $TGL_MASUK;
	}

?>
