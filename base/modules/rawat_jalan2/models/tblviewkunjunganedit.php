<?php
class tblviewkunjunganedit extends TblBase
{
    function __construct()
    {
        $this->TblName='tblviewkunjunganedit';
        TblBase::TblBase(true);
		$this->StrSql="kd_pasien,nama_unit,kd_unit,urut_masuk,tgl_masuk, dokter 
		,no_sjp,customer,jam,tgl_keluar";
		
        $this->SqlQuery= "select  * from (
				select k.kd_pasien,u.nama_unit,k.kd_unit,k.urut_masuk,k.tgl_masuk, d.nama as dokter 
				,sjp.no_sjp,cus.customer,to_char(k.jam_masuk, 'HH24:MI:SS' ) jam,k.tgl_keluar,
				kon.customer
				from kunjungan k
				left join unit u on k.kd_unit=u.kd_unit 
				left join dokter d on d.kd_dokter=k.kd_dokter
				left join customer cus on cus.kd_customer=k.kd_customer
				left join sjp_kunjungan sjp on k.kd_unit=sjp.kd_unit and sjp.kd_pasien=k.kd_pasien AND sjp.tgl_masuk=k.tgl_masuk 
				and sjp.urut_masuk=k.urut_masuk 
				inner join customer kon on kon.kd_customer=k.kd_customer  order by k.tgl_masuk desc
				
										) as resdata  ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirdetailrwj;

        $row->KD_PASIEN = $rec->kd_pasien;
        $row->TGL_MASUK = $rec-> tgl_masuk;
        $row->NAMA_UNIT = $rec->nama_unit;
        $row->KD_UNIT = $rec->kd_unit;
        $row->DOKTER = $rec->dokter;
		$row->URUT = $rec->urut_masuk;
		$row->JAM = $rec->jam;
		$row->TGL_KELUAR = $rec->tgl_keluar;
		$row->NO_SJP = $rec->no_sjp;
		$row->CUST = $rec->customer;
        return $row;
    }

}

class Rowtblviewkasirdetailrwj
{
	public $JAM;
	public $TGL_KELUAR;
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $NAMA_UNIT;
    public $KD_UNIT;
    public $DOKTER;
	public $URUT;
	public $NO_SJP;
	public $CUST;
}

?>
