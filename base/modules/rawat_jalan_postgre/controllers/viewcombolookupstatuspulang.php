<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewcombolookupstatuspulang extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }    


    public function index()
    {
        $this->load->view('main/index');                    
    }
   
   public function read($Params=null)
   {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
                        
        $this->load->model('rawat_jalan/tbl_status_pulang');
        
        $query = $this->tbl_status_pulang->readAll();
                    
        
        $arrResult=array();
                
        $row=new Rowam_status;
        $row->ID_STATUS='Semua';
        $row->STATUS='Semua';
        $arrResult[]=$row;
        foreach ($query->result_object() as $rows)
        {
            $arrResult[] = $this->FillRow($rows);
        }

    //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
    
        echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';
            
                    
   }
   function FillRow($rec)
    {
        $row=new Rowam_status;
        $row->ID_STATUS=$rec->id_status;
        $row->STATUS=$rec->status;

        return $row;
    }
}



?>