﻿<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class tbl_cmb_customer extends Model
{
    private $criteria = "";

    function viewcustomercmb()
    {
        parent::Model();
        $this->load->database();
    }

    function setCriteria($data){
        $this->criteria = $data;
    }   

    function read($id)
    {
        $this->db->where('kd_customer', $id);
        $query = $this->db->get('customer');

        return $query;
    }

    function readAll()
    {    
        /*$this->db->select('*');
        $this->db->from('kontraktor k');
        $this->db->join('customer c', 'k.kd_customer = c.kd_customer', 'INNER');
        if (!empty($this->criteria)) {
            $this->db->where($this->criteria);
        }
        $query = $this->db->get();*/
        $query = $this->db->query("
            select *  
            from kontraktor inner join 
            customer on kontraktor.kd_customer = customer.kd_customer 
        ".$this->criteria);

        return $query;
    }
}

class Row_Customer
{

    public $KD_CUSTOMER;
    public $CUSTOMER;
}

?>