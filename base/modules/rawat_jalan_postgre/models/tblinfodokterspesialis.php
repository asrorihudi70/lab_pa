<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblinfodokterspesialis extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_spesial, spesialisasi";
		$this->SqlQuery="select  * from (
										
										select kd_spesial, spesialisasi from spesialisasi
										)as resdata ";
		$this->TblName='infodokterspesial';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
	
		$row->KD_SPESIAL=$rec->kd_spesial;
		$row->SPESIALISASI=$rec->spesialisasi;
		
	
		
		return $row;
	}
}
class Rowdokter
{
	public $KD_SPESIAL;
	public $SPESIALISASI;
	

}

?>