<?php
/**

 * @author Ali
 * Editing by AGUNG
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionRADPoliklinik extends  MX_Controller {

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
	}
 
	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}

	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter)
	{
		# QUERY Sql Server
		$result=_QMS_Query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='".$KdUnit."'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=_QMS_Query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='".$KdUnit."'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			# Akhir QUERY Sql Server
			$urut=$urut_masuk+1;
		} else{
			$urut=1;
		}
		
		return $urut;
	}
   
	private function GetIdTransaksi($kdkasirpasien)
	{
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kdkasirpasien'")->row();
		$no = $counter->counter;
		$retVal = $no+1;
	/*	if($sqlnotr>$retVal)
		{
			$retVal=$sqlnotr;	
		}*/
		$query = "update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'";
		$update = $this->db->query($query);
		
		//-----------insert to sq1 server Database---------------//
			_QMS_query($query);
		//-----------akhir insert ke database sql server----------------//
		return $retVal;
    }
		
	
	public function GetKodeKasirPenunjang($kdunit,$cUnit)
	{
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='".$cUnit."' and kd_asal= '".$cUnit."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}
	
	public function GetKodeAsalPasien($cUnit)
	{	$cKdUnitAsal = "";

		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$cKdUnitAsal = $data->kd_asal;
		}
		
		if ($cKdUnitAsal != "")
		{
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

public function savedetailrad()
{	
	$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
	$rad_cito = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito'")->row()->setting;
	
	if($_POST['unitaktif'] == 'rwj'){
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_rad_order'")->row()->setting;
	} else if($_POST['unitaktif'] == 'rwi'){
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'rwi_default_rad_order'")->row()->setting;
	} else{
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'igd_default_rad_order'")->row()->setting;
	}

	$notrans = $_POST['newnotrans'];
	$KdPasien = $_POST['KdPasien'];
	$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
	$unitasal = $_POST['KdUnit'];
	$KdDokter = $_POST['KdDokter'];
	$pasienBaru=$_POST["pasienBaru"]; # variabel untuk kunjungan langsung
	$Tgl = date("Y-m-d");
	$Shift =$_POST['Shift'];
	$list = json_decode($_POST['List']);
	$jmlList = $_POST['JmlList'];
	$unit = $_POST['unit'];
	$NoSJP = $_POST['NoSJP'];
	$TmpNotransAsal = $_POST['TmpNotransaksi']; # no transaksi asal jika bukan kunjungan langsung
	$KdKasirAsal = $_POST['KdKasirAsal']; # Kode kasir asal
	$KdCusto = $_POST['KdCusto'];
	$TmpCustoLama = $_POST['TmpCustoLama']; # kd customer jika jenis transaksi lama
	$KdSpesial = $_POST['KdSpesial'];
	$Kamar = $_POST['Kamar'];
	$kdUser=$this->session->userdata['user_id']['id'] ;
	$listtrdokter		= json_decode($_POST['listTrDokter']);
	
	if($TglTransaksiAsal=='')
	{
		//$KdUnit='5';
		$TglTransaksiAsal=$Tgl;
	} else
	{
		//$KdUnit=$KdUnit;
		$TglTransaksiAsal=$TglTransaksiAsal;
	}
	if($KdCusto=='')
	{
		$KdCusto=$TmpCustoLama;
	}else
	{
		$KdCusto=$KdCusto;
	}
	//$kdkasirpasien = $this->GetKodeAsalPasien($KdUnit);
	$kdkasirpasien = $this->db->query("select getkodekasirpenunjangrwj('".$KdUnit."') as kdkasir")->row()->kdkasir;
	$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
	//$notrans = $this->GetIdTransaksi($kdkasirpasien);
	if($pasienBaru == 0)
	{
		if(substr($unitasal, 0, 1) == '1'){//RWI
			$IdAsal=1;
		} else if(substr($unitasal, 0, 1) == '2'){//RWJ
			$IdAsal=0;
		} else if(substr($unitasal, 0, 1) == '3'){//UGD
			$IdAsal=0;
		}
	} else
	{
		$IdAsal=2;
	}
	/* $querycek_oke=$this->db->query(" select t.kd_kasir,t.no_transaksi from kunjungan k inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and
	t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi where t.order_mng=true and t.kd_kasir='".$kdkasirpasien."' and k.kd_pasien='$KdPasien' and  t.no_transaksi not  in 
	(select t.no_transaksi
	from transaksi t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi
	inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi
	where t.order_mng=true and t.kd_kasir='".$kdkasirpasien."' and k.kd_pasien='$KdPasien' and t.tgl_transaksi>='$TglTransaksiAsal' ) and t.tgl_transaksi>='$TglTransaksiAsal' 
	order by k.tgl_masuk,k.urut_masuk desc limit 1"); */
	$querycek_oke=_QMS_Query("select top 1 t.kd_kasir,t.no_transaksi 
										from kunjungan k 
											inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi
										where t.kd_kasir='".$kdkasirpasien."' and k.kd_pasien='$KdPasien' 
											and  t.no_transaksi not  in (select t.no_transaksi
																			from transaksi t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi
																			inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi 
																			where t.kd_kasir='".$kdkasirpasien."' and k.kd_pasien='$KdPasien'
																				and t.tgl_transaksi>='$TglTransaksiAsal' ) 
											and t.tgl_transaksi>='$TglTransaksiAsal' 
										order by k.tgl_masuk,k.urut_masuk desc");
	# QUERY Sql Server
	//$querycek_oke=_QMS_Query("select * from unit_asal where no_transaksi_asal='$TmpNotransAsal'");
	# AKhir QUERY Sql Server
	//echo count($querycek_oke->result());
	if($querycek_oke->num_rows()===0)
	{
		//echo 'hehe';
		$this->db->trans_begin();
		$simpankunjungan = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
		$simpanmrrad= $this->SimpanMrRad($KdPasien,$KdUnit,$Tgl,$urut);
		$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$KdUnit,$Tgl,$urut);
		if($KdUnit != '' && substr($KdUnit, 0, 1) =='1'){
			# ***jika bersal dari rawat inap***
			$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$unitasal,$KdPasien,$TglTransaksiAsal);
		}
		
		if( $pasienBaru == 0){
			# ***jika bukan Pasien baru/kunjungan langsung***
			$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
		}else{
			$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
		} 
		
		if($hasil == 'sae'){
			for($i=0;$i<count($list);$i++){
				$kd_produk=$list[$i]->KD_PRODUK;
				$qty=$list[$i]->QTY;
				$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
				$tgl_berlaku=$list[$i]->TGL_BERLAKU;
				if (isset($list[$i]->cito))
						{
							$cito=$list[$i]->cito;
							if($cito=='Ya')
							{$cito='1';
							$harga=$list[$i]->HARGA;
							$hargacito = (((int) $harga) * ((int)$rad_cito))/100;
							$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
							
							}
							else if($cito=='Tidak')
							{$cito='0';
							$harga=$list[$i]->HARGA;
							}
							else
							{
								$cito='0';
								$harga=$list[$i]->HARGA;
							}
						} else
						{
						 $cito='0';
						 $harga=$list[$i]->HARGA;
						}
				$kd_tarif=$list[$i]->KD_TARIF;
				$notrwbah=$list[$i]->NO_TRANSAKSI_BAWAH;
				
				/* $query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' and 
				kd_dokter='".$_POST['KdDokter_mr_tindakan']."' ")->result();
				$kdKasirnya=$this->db->query("select setting from sys_setting where key_data='".$_POST['kd_kasir']."'")->row()->setting;
				$query_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->result();
				$c_urutTindakan=count($query_mr_tindakan);
				$urutTindakan=0;
					if ($c_urutTindakan<>0)
					{
						$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->row()->jml;
						$urutTindakan=$max+1;
					}
					foreach ($query_cek_kunjungan as $datatindakan)
					{
						$cek_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_produk='".$kd_produk."' and
						kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->result();
						$jmlMr=count($cek_mr_tindakan);
						if ($jmlMr==0)
						{
						$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$kd_produk."','".$KdPasien."','$KdUnit',
						'$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TmpNotransAsal."','".$kdKasirnya."')");	
						$urutTindakan+=1;
						}
					} */
					
				if($notrwbah==='' || $notrwbah==='undifined')
				{
					#QUERY Sql Server
					$urutdetailtransaksi = _QMS_Query("select max(urut) as urut from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='".$notrans."'");
					# Akhir QUERY Sql Server
					if(count($urutdetailtransaksi->result()) > 0){
						$urutdetailtransaksi = $urutdetailtransaksi->row()->urut + 1;
					} else{
						$urutdetailtransaksi = 1;
					}
					
					$x=$this->db->query("select urut+1 as urutan from rad_hasil  where kd_pasien='$KdPasien' and kd_unit='5' and tgl_masuk='$Tgl'  and urut_masuk=$urut 
					order by urut desc limit 1")->result();
					if(count($x)===0)
					{
						$u=1;
					}
					else
					{
						foreach($x as $line)
						{
							$u=$line->urutan;
						}
					}
					# ***insert detail_transaksi***
					# QUERY Sql erver 
					$param = "exec dbo.V5_insert_detail_transaksi '".$kdkasirpasien."',
							'".$notrans."',".$urutdetailtransaksi.",'".$Tgl."',
							'".$kdUser."',
							".$kd_tarif.",".$kd_produk.",'".$KdUnit."',
							'".$tgl_berlaku."',0,0,'',".$qty.",".$harga.",".$Shift.",0,''";
					$query = _QMS_Query($param);
					# Akhir QUERY Sql erver 
					
					if($cito==='1') {
						# QUERY Sql erver 
						$query = _QMS_Query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
													and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'"); 
						$query = _QMS_Query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'"); 
						# Akhir QUERY Sql erver 
					}
					# QUERY Sql Server 
					$qsql=_QMS_Query(" select * from detail_component 
					where kd_kasir='".$kdkasirpasien."' 
					and no_transaksi='".$notrans."'
					and urut=".$urutdetailtransaksi."
					and tgl_transaksi='".$Tgl."'")->result();
					# Akhir QUERY Sql Server 
					/* foreach($qsql as $line)
					{
						$qkd_kasir=$line->kd_kasir;
						$qno_transaksi=$line->no_transaksi;
						$qurut=$line->urut;
						$qtgl_transaksi=$line->tgl_transaksi;
						$qkd_component=$line->kd_component;
						$qtarif=$line->tarif;
					} */
					
					if($query)
					{
						$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
						where 
						(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
						kd_unit ='".$unit."' AND
						kd_produk='".$kd_produk."' AND
						tgl_berlaku='".$tgl_berlaku."' AND
						kd_tarif='".$kd_tarif."' group by tarif")->result();
						
						foreach($ctarif as $ct)
						{
							if($ct->jumlah != 0)
							{
								# ***insert detail_trdokter***
								# QUERY Sql erver 
								$param = "exec dbo.V5_insert_detail_trdokter '".$kdkasirpasien."',
										'".$notrans."',".$urutdetailtransaksi.",'".$_POST['KdDokter']."','".$Tgl."',
										0,'".$kdjasadok."',".$ct->tarif."";
								$trDokter = _QMS_Query($param);
								# Akhir QUERY Sql Server 
								
							}
						}
						# QUERY Sql erver 
						$query2 = _QMS_Query("Insert into rad_hasil(kd_test,  kd_pasien,kd_unit, tgl_masuk, urut_masuk,  urut,kd_unit_asal,keluhan) 
													values (".$kd_produk.",'".$KdPasien."','".$KdUnit."','".$Tgl."',".$urut.",".$urutdetailtransaksi.",'".$unitasal."','')");
						# Akhir QUERY Sql Server 
						if($query2)
						{
							$hasil="Ok";
						} 
						else
						{
							$hasil="error";
						}
					} else {
						$hasil="error";
					}
				}
			}
		}else {
			$hasil="error";
		}
		
		if($hasil=="Ok") {
			echo '{success: true, KD_PASIEN: "'.$KdPasien.'", NO_TRANS: "'.$notrans.'", KASIR: "'.$kdkasirpasien.'"}';
		}else  {
			echo "{success:false}";
		}
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		} 
	}
	else{
		$this->db->trans_begin();
		$urut=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
		if(count($urut)==0)
		{
			echo "{success:false, cari:false}";
		}else {
			$querycek_oke=$this->db->query("  select t.kd_kasir,t.no_transaksi from kunjungan k 
			inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and 
			t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi
			where t.order_mng=true and t.kd_kasir='".$kdkasirpasien."' and k.kd_pasien='$KdPasien' and  t.no_transaksi not  in (select t.no_transaksi
			from transaksi t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi
			inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and
			t.urut_masuk=k.urut_masuk and k.tgl_masuk=t.tgl_transaksi where t.order_mng=true and t.kd_kasir='".$kdkasirpasien."' and k.kd_pasien='$KdPasien' 
			and t.tgl_transaksi>='$TglTransaksiAsal' ) and t.tgl_transaksi>='$TglTransaksiAsal' 
			order by k.tgl_masuk,k.urut_masuk desc limit 1")->result();
			foreach( $querycek_oke as $okecari)
			{
				$notrans=$okecari->no_transaksi;
				$kdkasirpasien=$okecari->kd_kasir;
			}
			for($i=0;$i<count($list);$i++){
				$kd_produk=$list[$i]->KD_PRODUK;
				$qty=$list[$i]->QTY;
				$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
				$tgl_berlaku=$list[$i]->TGL_BERLAKU;
				if (isset($list[$i]->cito))
				{
					$cito=$list[$i]->cito;
					if($cito=='Ya')
					{
						$cito='1';
						$harga=$list[$i]->HARGA;
						$hargacito = (((int) $harga) * ((int)$rad_cito))/100;
						$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
					} else if($cito=='Tidak') {
						$cito='0';
						$harga=$list[$i]->HARGA;
					}else
					{
						$cito='0';
						$harga=$list[$i]->HARGA;
					}
				} else {
					$cito='0';
					$harga=$list[$i]->HARGA;
				}
				
				$kd_tarif=$list[$i]->KD_TARIF;
				$notrwbah=$list[$i]->NO_TRANSAKSI_BAWAH;
				
				/* $query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."'
				and kd_dokter='".$_POST['KdDokter_mr_tindakan']."' ")->result();
				$kdKasirnya=$this->db->query("select setting from sys_setting where key_data='".$_POST['kd_kasir']."'")->row()->setting;
				$query_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->result();
				$c_urutTindakan=count($query_mr_tindakan);
				$urutTindakan=0;
				
				if ($c_urutTindakan<>0)
				{
					$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' ")->row()->jml;
					$urutTindakan=$max+1;
				}
				
				foreach ($query_cek_kunjungan as $datatindakan)
				{
					$cek_mr_tindakan=$this->db->query("select * from mr_tindakan where kd_produk='".$kd_produk."' and kd_pasien='".$KdPasien."' 
					and kd_unit='".$KdUnit."' ")->result();
					$jmlMr=count($cek_mr_tindakan);
					if ($jmlMr==0)
					{
						$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$kd_produk."','".$KdPasien."','$KdUnit','$datatindakan->tgl_masuk',
						$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TmpNotransAsal."','".$kdKasirnya."')");	
						$urutTindakan+=1;
					}
					
					
				}
				 */
				if($notrwbah==='' || $notrwbah==='undifined')
				{
					#QUERY Sql Server
					$urutdetailtransaksi = _QMS_Query("select max(urut) as urut from detail_transaksi where kd_kasir='$kdkasirpasien' and no_transaksi='".$notrans."'");
					# Akhir QUERY Sql Server
					if(count($urutdetailtransaksi->result()) > 0){
						$urutdetailtransaksi = 1;
					} else{
						$urutdetailtransaksi = $urutdetailtransaksi->row()->urut + 1;
					}
					
					$x=_QMS_QUERY("select top 1 urut+1 as urutan from rad_hasil  where kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl'  
										and urut_masuk=$urut order by urut desc ")->result();
					if(count($x)===0)
					{
						$u=1;
					}else
					{
						foreach($x as $line){
							$u=$line->urutan;
						}
					}
					# ***insert detail_transaksi***
					# QUERY Sql erver 
					$param = "exec dbo.V5_insert_detail_transaksi '".$kdkasirpasien."',
							'".$notrans."',".$urutdetailtransaksi.",'".$Tgl."',
							'".$kdUser."',
							".$kd_tarif.",".$kd_produk.",'".$KdUnit."',
							'".$tgl_berlaku."',0,0,'',".$qty.",".$harga.",".$Shift.",0,''";
					$query = _QMS_Query($param);
					# Akhir QUERY Sql erver 
					
					if($cito==='1')
					{
						# QUERY Sql erver 
						$query = _QMS_Query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
													and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'"); 
						$query = _QMS_Query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'"); 
						# Akhir QUERY Sql erver 
					}
					# QUERY Sql Server 
					/* $qsql=_QMS_Query(" select * from detail_component 
					where kd_kasir='".$kdkasirpasien."' 
					and no_transaksi='".$notrans."'
					and urut=".$urutdetailtransaksi."
					and tgl_transaksi='".$Tgl."'")->result();
					# Akhir QUERY Sql Server 
					foreach($qsql as $line)
					{
						$qkd_kasir=$line->kd_kasir;
						$qno_transaksi=$line->no_transaksi;
						$qurut=$line->urut;
						$qtgl_transaksi=$line->tgl_transaksi;
						$qkd_component=$line->kd_component;
						$qtarif=$line->tarif;
					} */
					if($query)
					{
						$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
								where (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
								kd_unit ='".$KdUnit."' AND
								kd_produk='".$kd_produk."' AND
								tgl_berlaku='".$tgl_berlaku."' AND
								kd_tarif='".$kd_tarif."' group by tarif")->result();
								foreach($ctarif as $ct)
								{
									if($ct->jumlah != 0)
									{
										# ***insert detail_trdokter***
										# QUERY Sql erver 
										$param = "exec dbo.V5_insert_detail_trdokter '".$kdkasirpasien."',
												'".$notrans."',".$urutdetailtransaksi.",'".$_POST['KdDokter']."','".$Tgl."',
												0,'".$kdjasadok."',".$ct->tarif."";
										$trDokter = _QMS_Query($param);
										# Akhir QUERY Sql Server 
									}
								}
						
						# QUERY Sql erver 
						/* echo "select urut+1 as urutan from rad_hasil  where kd_pasien='$KdPasien' and kd_unit='$KdUnit' and tgl_masuk='$Tgl'  
										and urut_masuk=$urut order by urut desc limit 1       ";
						echo $u;
						echo "Insert into rad_hasil(kd_test,  kd_pasien,kd_unit, tgl_masuk, urut_masuk,  urut,kd_unit_asal,keluhan) 
													values (".$kd_produk.",'".$KdPasien."','".$KdUnit."','".$Tgl."',".$urut.",".$u.",'".$unitasal."','')"; */
						$query2 = _QMS_Query("Insert into rad_hasil(kd_test,  kd_pasien,kd_unit, tgl_masuk, urut_masuk,  urut,kd_unit_asal,keluhan) 
													values (".$kd_produk.",'".$KdPasien."','".$KdUnit."','".$Tgl."',".$urut.",".$u.",'".$unitasal."','')");
						# Akhir QUERY Sql Server 
						if($query2)
						{
							$hasil="Ok";
						} else
						{
							$hasil="error";
						}
					} else
					{
						$hasil="error";
					}
				}else
				{
					$hasil="Ok";
				}
				
			}
			if($hasil=="Ok" ) 
			{
			  // $this->db->trans_commit();
			   echo '{success: true, KD_PASIEN: "'.$KdPasien.'", NO_TRANS: "'.$notrans.'", KASIR: "'.$kdkasirpasien.'"}';
			} else 
			{	
			// $this->db->trans_rollback();
			  echo "{success:false}";
			}
		}
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		} 
	}
}
	
	function saveTrDokter($listtrdokter,$kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut)
	{
		//save jasa dokter
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 
			foreach ($listtrdokter as $arr) 
			{
				//var_dump($arr);
				
				if($arr->kd_job == 'Dokter')
				{
					$kd_job = 1;
				}
				else
				{
					$kd_job = 2;
				}

				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
				where 
				(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
				kd_unit ='".$KdUnit."' AND
				kd_produk='".$arr->kd_produk."' AND
				tgl_berlaku='".$arr->tgl_berlaku."' AND
				kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
				
				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						
						$trDokter = $this->db->query("insert into detail_trdokter select '".$kdkasirasalpasien."','".$notrans."','".$Schurut."',
						'".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
							NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '".$kdkasirasalpasien."' AND
									tgl_transaksi='".$Tgl."' AND
									urut='".$Schurut."' AND
									kd_dokter = '".$arr->kd_dokter."' AND
									no_transaksi='".$notrans."'
							)");
					}
				}
			}
				
				//akhir save jasa dokter
				
	}
	
	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		# QUERY Sql Server
		$result = _QMS_insert('unit_asal',$data);
		# Akhir QUERY Sql Server

		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}
	
	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$KdPasien,$TglTransaksiAsal)
	{
		$query=$this->db->query("select * from nginap where kd_pasien='$KdPasien' and kd_unit ='$KdUnit' and tgl_masuk = 
		'$TglTransaksiAsal' and akhir=true order by tgl_masuk,tgl_masuk desc limit 1")->result();
		foreach($query as $ngingp){
			$Kamar=$ngingp->no_kamar;
			$KdSpesial=$ngingp->kd_spesial;
		}
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
						
		# QUERY Sql Server
		_QMS_insert('unit_asalinap',$data);
		# Akhir QUERY Sql Server


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}
	public function SimpanMrRad($KdPasien,$unit,$Tgl,$urut)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$unit,
							"tgl_masuk"=>$Tgl,
							"urut_masuk"=>$urut
						);
						
		# QUERY Sql Server
		_QMS_insert('mr_lab',$data);
		# Akhir QUERY Sql Server
		return $strError;
	}
        
        
    public function SimpanPasien($kdpasien,$namaPasien,$tglLahirPasien,$alamatPasien,$jkPasien,$goldarPasien,$NoAskes,$NamaPesertaAsuransi)
	{
		$strError = "";
		$suku = 0;         
		
			$data = array("kd_pasien"=>$kdpasien,
					"nama"=>$namaPasien,
					"jenis_kelamin"=>$jkPasien,
					"tgl_lahir"=>$tglLahirPasien,
					"gol_darah"=>$goldarPasien,
					"alamat"=>$alamatPasien,
					"no_asuransi"=>$NoAskes,
					"pemegang_asuransi"=>$NamaPesertaAsuransi,
					"kd_kelurahan"=>0,
					"kd_pendidikan"=>0,
					"kd_pekerjaan"=>0,
					"kd_suku"=>$suku,
					"kd_perusahaan"=>0);
			
			 $datasql = array("kd_pasien"=>$kdpasien,"nama"=>$namaPasien,
					"nama_keluarga"=>"","jenis_kelamin"=>$jkPasien,
					"tempat_lahir"=>"","tgl_lahir"=>$tglLahirPasien,
					"gol_darah"=>$goldarPasien,"status_marita"=>0,
					"wni"=>0,"alamat"=>$alamatPasien,
					"telepon"=>"","kd_kelurahan"=>0,
					"kd_pendidikan"=>0,"kd_pekerjaan"=>0,
					"kd_perusahaan"=>0,
					"no_asuransi"=>$NoAskes,
					"pemegang_asuransi"=>$NamaPesertaAsuransi);
			
		

		$criteria = "kd_pasien = '".$kdpasien."'";

		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList( 0,1, "", "","");
		if ($query[1]==0)
		{
			$data["kd_pasien"] = $kdpasien;
			$result = $this->tb_pasien->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('Pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//
			$strError = "ada";
		}
		return $strError;
	}

	private function GetIdRad()
	{
		 $kdpasien = "";
		$res = $this->db->query("select kd_pasien from pasien where left(kd_pasien,2) = 'RD'  Order By kd_pasien desc Limit 1")->result();
		
		foreach ($res as $line) {
			$kdpasien = $line->kd_pasien;
		 }
		if ($kdpasien != "")
		{
			$nm = $kdpasien;                
			
			$retVal= substr($nm, -5);
			$nomor = (int) $retVal +1;
			$getnewmedrec = "RD".$nomor;
		}else
		{
		  $strNomor= "RD000";
		  $getnewmedrec=$strNomor."01";
		}
		return $getnewmedrec;
	}
	
        
		public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
		{
			$strError = "";
			$tmpkdcusto = '0000000001';
			date_default_timezone_set("Asia/Jakarta");
			$JamKunjungan = gmdate("Y-m-d H:i:s", time()+60*60*7);
			//$JamKunjungan = date('Y-m-d h:i:s');
			
			# DATA ARRAY UNTUK SAVE KE SQL SERVER
			$datasql = array("kd_pasien"=>$kdpasien,  "kd_unit"=>$unit, "tgl_masuk"=>$Tgl,
						  "kd_rujukan"=>"0",  "urut_masuk"=>$urut,  "jam_masuk"=>$JamKunjungan,
						  "kd_dokter"=>$kddokter, "shift"=>$Shift,  "kd_customer"=>$KdCusto,
						  "karyawan"=>"0", "keadaan_masuk"=>0, "keadaan_pasien"=>0,
						  "cara_penerimaan"=>99,  "asal_pasien"=>$IdAsal, "cara_keluar"=>0,
						  "baru"=>$pasienBaru, "kontrol"=>0 );
			# AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
			
			# QUERY Sql Server
			$query = _QMS_Query("select * from kunjungan where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."")->result();
			# Akhir QUERY Sql Server
			if (count($query) == 0)
			{
				# QUERY Sql Server
				_QMS_insert('kunjungan',$datasql);
				# Akhir QUERY Sql Server
				$strError = "aya";
			}else{
				 $strError = "eror";
			}
			return $strError;
		}

	public function gettotalpasienorder_mng()
	{
	     $yesterday = date('Y-m-d',strtotime(date('Y-m-d') . "-3 days"));
		$result = $this->db->query("	select count(k.kd_pasien) as totalpasin from transaksi t inner join kunjungan k on k.kd_pasien=t.kd_pasien 
										and k.kd_unit=t.kd_unit
										 and t.tgl_transaksi=k.tgl_masuk and k.urut_masuk=t.urut_masuk where order_mng=true and kd_kasir='02' and t.tgl_transaksi >='".$yesterday."' 
										 and t.tgl_transaksi <='".date('Y-m-d')."' and t.no_transaksi not in (select t.no_transaksi from transaksi t inner join detail_bayar 
										 db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi
										 where t.order_mng=true and t.kd_kasir='02')")->result();

		foreach ($result as $data)
		{
			$IdAsal = $data->totalpasin;
		}

	echo "{success:true, jumlahpasien:'$IdAsal'}";
	}
		
			public function getPasien(){
		//$tanggal=$_POST['tanggal'];
		$tanggal2 = date('Y-m-d');
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		/* if($_POST['a'] == 0 || $_POST['a'] == '0'){
			$no_transaksi = " and tr.no_transaksi like '".$_POST['text']."%'";
			$nama = "";
			$kd_pasien = "";
		} else if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " and pasien.kd_pasien like '".$_POST['text']."%'";
			$nama = "";
			$no_transaksi = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$no_transaksi = "";
		}
		 */
		 
		if($_POST['text']==='0')
		{
		$kritreia="	WHERE (left(kunjungan.kd_unit,1) in ('1','2','3','4','5')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".date('Y-m-d')."' 
								and tr.order_mng=true and tr.kd_kasir='02' and  tr.no_transaksi not in (select t.no_transaksi 
								from transaksi t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where
								t.order_mng=true and t.kd_kasir='02') )  ";
		}else{
			$kritreia="	WHERE (left(kunjungan.kd_unit,1) in ('1','2','3','4','5')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".date('Y-m-d')."' 
								and tr.order_mng=true and tr.kd_kasir='02' and  tr.kd_pasien like'%".$_POST['text']."%' and
								tr.no_transaksi not in (select t.no_transaksi from transaksi 
								t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=true and t.kd_kasir='02')
								) or 
								(left(kunjungan.kd_unit,1) in ('1','2','3','4','5')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".date('Y-m-d')."' 
								and tr.order_mng=true and tr.kd_kasir='02' and pasien.nama like'%".$_POST['text']."%') 
								and tr.no_transaksi not in (select t.no_transaksi from transaksi 
								t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=true and t.kd_kasir='02')
								or (left(kunjungan.kd_unit,1) in ('1','2','3','4','5') 
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".date('Y-m-d')."' 
								and tr.order_mng=true and tr.kd_kasir='02' and tr.no_transaksi like'%".$_POST['text']."%' and 
								tr.no_transaksi not in (select t.no_transaksi  from transaksi 
								t inner join detail_bayar db on db.kd_kasir=t.kd_kasir and db.no_transaksi=t.no_transaksi where t.order_mng=true and t.kd_kasir='02'))";
		}
		$result=$this->db->query("SELECT pasien.kd_pasien,kunjungan.urut_masuk, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
									case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 
									'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien 
									,pasien.kd_pasien|| '  '||'-'|| '  '||tr.no_transaksi || '  '||'-'|| '  '||pasien.nama as display
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
										inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
										inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
										)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								
								$kritreia
								ORDER BY tr.cito desc, kunjungan.jam_masuk,tr.no_transaksi asc limit 10								
							")->result();
							
			
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	
        public function SimpanTransaksi($kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut)
        {            
            $kdpasien;
			$unit;
			$Tgl;
			
            $strError = "";
             
			# DATA ARRAY UNTUK SAVE KE SQL SERVER
			$datasql = array("kd_kasir"=>$kdkasirasalpasien,
						  "no_transaksi"=>$notrans,
						  "kd_pasien"=>$kdpasien,
						  "kd_unit"=>$KdUnit,
						  "tgl_transaksi"=>$Tgl,
						  "urut_masuk"=>$Schurut,
						  "tgl_co"=>NULL,
						  "co_status"=>0,
						  "ispay"=>0,
						  "app"=>0,
						  "lunas"=>0,
						  "kd_user"=>0);
			# AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
            
			# QUERY Sql Server
			$query = _QMS_Query("select * from transaksi where no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirasalpasien."'")->result();
            # Akhir QUERY Sql Server
			if (count($query)==0)
            {          
				# QUERY Sql Server
				$result =_QMS_insert('transaksi',$datasql);
				# Akhir QUERY Sql Server
                if($result){
					$strError = "sae";
				} else{
					$strError = "error";
				}
            }
            return $strError;
        }
        
	public function getDokterPengirim(){
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$nama=$this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."') AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row()->nama;
		if($nama){
			echo "{success:true,nama:'$nama'}";
		} else{
			echo "{success:false}";
		}
	}
		
public function gethasiltest(){
$gethasil_test_rad=$this->db->query("Select Klas_Produk.Klasifikasi,Produk.Deskripsi,Rad_test.*,Rad_hasil.hasil,Rad_hasil.Urut  
                                From 
                                (Rad_Test 
                                left join Rad_hasil on Rad_test.kd_test=Rad_hasil.kd_Test )  
                                inner join (produk 
                                inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)  
                                on Rad_Test.kd_Test = produk.Kd_Produk 
								where Rad_hasil.kd_pasien='".$_POST['kd_pasien']."' 
								and Rad_hasil.kd_unit='".$_POST['kd_unit']."'    
								and Rad_hasil.tgl_masuk= '".$_POST['tgl_masuk']."'
								and Rad_hasil.urut_masuk= ".$_POST['urut_masuk']."
								and Rad_hasil.kd_test= '".$_POST['kd_produk']."'
								and Rad_hasil.urut= ".$_POST['urut']."
								" )->row()->hasil;
  echo "{ hasil:'".json_encode($gethasil_test_rad)."'}";
								

}
	
	public function getItemPemeriksaan(){
		$no_transaksi=$_POST['no_transaksi'];
		if ($_POST['kasirmana']=='igd')
		{
			$kdkasir  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		}else if ($_POST['kasirmana']=='rwj')
		{
			$kdkasir  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		}else if ($_POST['kasirmana']=='rwi')
		{
			$kdkasir  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		}
		
		if($no_transaksi == ""){
			$where="";
		} else{
			$where=" where no_transaksi_asal='".$no_transaksi ."'  And kd_kasir_asal ='$kdkasir' and left(kdunitkun,1)='5' And kd_kasir ='02' and order_mng=true";
		}
		$kd_unit=$_POST['kdunit'];
		if($kd_unit == ""){
			$whereunit="";
		} else{
			$whereunit=" and left(kdunit_asal,1)='".substr($kd_unit, 0, 1)."'";
		}
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
		$result=$this->db->query(" select * from (
									select     trs.kd_pasien,detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer,tr2.no_transaksi as no_transaksi_asal , tr2.kd_kasir as kd_kasir_asal,  tr2.tgl_transaksi as tgl_transaksi_asal, dokter.nama as namadok,tr.jumlah,trs.order_mng,trs.lunas,kun.urut_masuk as urutkun,kun.tgl_masuk as
								  tglkun,kun.kd_unit as kdunitkun,tr2.kd_unit as kdunit_asal 
								    from  detail_transaksi 
									inner join
									produk on detail_transaksi.kd_produk = produk.kd_produk 
									inner join transaksi trs on trs.kd_kasir=detail_transaksi.kd_kasir and trs.no_transaksi=detail_transaksi.no_transaksi
									inner join  kunjungan kun on trs.kd_pasien=kun.kd_pasien and trs.kd_unit=kun.kd_unit and trs.tgl_transaksi=kun.tgl_masuk and trs.urut_masuk=kun.urut_masuk
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on kun.kd_dokter = dokter.kd_dokter
								  
								  left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  
 group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
 AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif
 inner join unit_asal ua on ua.no_transaksi = trs.no_transaksi and ua.kd_kasir = trs.kd_kasir 
inner join transaksi tr2 on tr2.no_transaksi = ua.no_transaksi_asal  and tr2.kd_kasir = ua.kd_kasir_asal
								  ) as resdata 
								  $where
								  $whereunit
								and tgl_transaksi_asal='".$_POST['tgltrx']."' 
								  ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
}

?>