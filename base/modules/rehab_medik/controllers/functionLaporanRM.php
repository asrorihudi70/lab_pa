<?php
/**

 * @author Ali
 * Editing by MSD
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionLaporanRM extends  MX_Controller {

	public $ErrLoginMsg='';
	private $dbSQL         = "";
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		
		//$this->dbSQL    = $this->load->database('otherdb2',TRUE);
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, '.', '.');
	}
	public function LapTransaksiDetail() {
        $common=$this->common;
   		//$result=$this->result;
   		$title='LAPORAN TRANSAKSI REHAB MEDIK';
		$param=json_decode($_POST['data']);
		
		$asalpasien   = $param->asal_pasien;
		$kdUser       = $param->user;
		$periode      = $param->periode;
		$tglAwal      = $param->tglAwal;
		$tglAkhir     = $param->tglAkhir;
		$customer     = $param->customer;
		$kdCustomer   = $param->kdcustomer;
		$tipe         = $param->tipe;
		$type_file   = $param->type_file;
		$tglAwalAsli  = $param->tglAwal;
		$tglAkhirAsli = $param->tglAkhir;
		$periode      = $param->periode;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$ParamShift2 = "";
		$ParamShift3 = "";
		
		if (strtolower($kdUser) == strtolower('Semua')) {
			$username = "Semua";
		}else{
			$username = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
		}
		
		$res = $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Rehab Medik'")->row();
		//$kd_kasir= $this->db->query("select kd_kasir from kasir where deskripsi='Kasir Laboratorium PA'")->row()->kd_kasir;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='3'")->row()->kd_kasir;
		
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		/* $awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir))); */
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
			$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
			$awal=tanggalstring(date('Y-m-d',strtotime($this->db->query(" select date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date) as tawal")->row()->tawal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($this->db->query("select date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval as takhir")->row()->takhir)));
		}

		$pisahtgl = explode("/", $tglAkhir, 3);
		if (count($pisahtgl) == 2) {

			$bulan = $pisahtgl[0];
			$tahun = $pisahtgl[1];
			$tmpbulan = $bulan;
			if ($bulan == 'Jan') {
				$bulan = '1';
			} elseif ($bulan == 'Feb') {
				$bulan = '2';
			} elseif ($bulan == 'Mar') {
				$bulan = '3';
			} elseif ($bulan == 'Apr') {
				$bulan = '4';
			} elseif ($bulan == 'May') {
				$bulan = '5';
			} elseif ($bulan == 'Jun') {
				$bulan = '6';
			} elseif ($bulan == 'Jul') {
				$bulan = '7';
			} elseif ($bulan == 'Aug') {
				$bulan = '8';
			} elseif ($bulan == 'Sep') {
				$bulan = '9';
			} elseif ($bulan == 'Oct') {
				$bulan = '10';
			} elseif ($bulan == 'Nov') {
				$bulan = '11';
			} elseif ($bulan == 'Dec') {
				$bulan = '12';
			}

			$jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$tglAkhir = $tahun . '-' . $bulan . '-' . $jmlhari;
			$tglAwal = '01/' . $tglAwal;
		}

		if ($asalpasien == 'Semua') {
			$tmpasalpasien = 'Semua Pasien';
		} else if ($asalpasien == 'RWJ/IGD') {
			$tmpasalpasien = 'Rawat Jalan / Gawat Darurat';
		} else if ($asalpasien == 'RWI') {
			$tmpasalpasien = 'Rawat Inap';
		} else {
			$tmpasalpasien = $asalpasien;
		}

		if ($tglAkhirAsli == $tglAkhir) {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tglAkhirAsli;
		} else {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tmpbulan . '/' . $tahun;
		}
		
		if($shift == 'All'){
			$ParamShift2 = " WHERE pyt.Type_Data <=3 
								AND (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") 
								AND db.shift in (1,2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
								OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))
														 ";
			$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
								and dt.shift in (1,2,3) AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
								or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$shift = 'Semua Shift';
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =1 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =1 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,2) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))) ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,2) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))  ";
				$shift="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))
														  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))   ";
				$shift="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =2 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =2 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . "') 
									AND db.shift in (2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (2,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$shift="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =3 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =3 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$shift="Shift 3";
			} 
		}
	   
		//---------------------------------jika kelompok pasien = SEMUA-------------------------------------------
		echo $kdCustomer;
		if ($customer == 'Semua') {
			$kriteria_customer1 = "";
			$kriteria_customer2 = "";
		} else {
			$kriteria_customer1 = " AND k.kd_customer = '" . $kdCustomer . "'";
			$kriteria_customer2 = " AND k.kd_customer = '" . $kdCustomer . "'";
		}

		if (strtolower($kdUser) == strtolower('Semua')) {
			$criteriaUser = "";
		}else{
			$criteriaUser = " AND db.kd_user = '".$kdUser."'";
		}

		if ($asalpasien == 'Semua') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."') $criteriaUser";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."') $criteriaUser";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."') $criteriaUser";
		} else if ($asalpasien == 'RWJ/IGD') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."') $criteriaUser
												AND left(tr.kd_unit,1) in ('2','3')";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND left(tr.kd_unit,1) in ('2','3') $criteriaUser ";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."') $criteriaUser";
		} else if ($asalpasien == 'RWI') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwi."') $criteriaUser
												AND left(tr.kd_unit,1) = '1'";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND left(tr.kd_unit,1) = '1' $criteriaUser";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwi."') $criteriaUser";
		} else {
			$kriteria1 = " AND t.kd_kasir in ('".$kd_kasir."') $criteriaUser
												And left (p.kd_pasien,2)='LB'";

			$kriteria2 = " AND t.kd_kasir in ('".$kd_kasir."')
												And left (p.kd_pasien,2)='LB' $criteriaUser";
			$kriteria3 = " AND t.kd_kasir in ('".$kd_kasir."') $criteriaUser";
		}

        $queryHasil = $this->db->query(" 
											Select p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "" . $kriteria_customer1 . "" . $kriteria1 . "

											union 

											Select p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "" . $kriteria_customer2 . "" . $kriteria1 . "


											");
        $query = $queryHasil->result();
		$html="";
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table border="0">
				<tbody>
					<tr>
						<th colspan="7">Laporan Transaksi Harian ' . $tmpasalpasien . '</th>
					</tr>
					<tr>
						<th colspan="7">Periode dari ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th colspan="7">Kelompok ' . $customer . ' ( ' . $shift . ' )</th>
					</tr>
					<tr>
						<th colspan="7">Operator ' . $username . '</th>
					</tr>
				</tbody>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------	
		$html.='
				<table border = "1" >
				<thead>
				  <tr>
						<th width="10">No</td>
						<th width="80" align="center">Pasien</td>
						<th width="100" align="center">Pemeriksaan</td>
						<th width="40" align="center">Qty</td>
						<th width="70" align="center">Jumlah Penerimaan</td>
						<th width="70" align="center">Jumlah Pemeriksaan</td>
						<th width="70" align="center">User</td>
				  </tr>
				</thead>';
        if (count($query) == 0) {
			$html.='
					<tbody>
						<tr>
							<th colspan="6">Data tidak ada</th>
						</tr> 
					</tbody>
				</table>';
        } else {
			$no=0;
			$grandpenerimaan = 0;
			$grandpemeriksaan = 0;
            foreach ($query as $line) {
                $no++;

                $html.='
						<tbody>
							<tr >
								<td align="center">' . $no . '</td>
								<th width="100" align="left" colspan="6">' . $line->namapasien . '</th>
							</tr> ';
                $kdPasien = $line->kd_pasien;
                $queryPenerimaan = $this->db->query(" 
											Selects 1 as Bayar,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
													max(py.Uraian) as deskripsi,max(db.kd_user) as kd_user,sum(db.Jumlah) as jumlah,  1 as QTY, 'Penerimaan' as Head
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "
											" . $kriteria_customer1 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria1 . "
											group by 
											p.kd_pasien,
											t.kd_kasir,
											db.Kd_Pay
											order by bayar, kd_pay
											");
				$queryPemerikasaan = $this->db->query(" 
											select 0 as Tagih,t.kd_kasir,( p.kd_pasien || ' ' || p.nama ) AS namaPasien,
												Dt.Kd_tarif,
												prd.Deskripsi,
												Dt.kd_user,
												sum( Dt.QTY * Dt.Harga ) AS jumlah,
												sum(Dt.QTY) as qty,
												'Pemeriksaan' AS Head,
												k.kd_customer,
												tr.kd_unit 
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "
											" . $kriteria_customer2 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria2 . "
											group by 
											tagih,
											t.kd_kasir,
											namapasien,
											Dt.Kd_tarif,
											prd.Deskripsi,
											Dt.kd_user,
											k.kd_customer,
											tr.kd_unit
											");
                $query3 = $queryPenerimaan->result();
				$query2 = $queryPemerikasaan->result();
				$noo = 0;
				
				/* Query Pemeriksaan*/
				$sub_jumlah_pemeriksaan=0;
				$ket_head='';
				$ket_head_lalu='';
				foreach ($query2 as $line) {
					$noo++;
                    $namapasien = $line->namapasien;
					$ket_head=$line->head;
                    $deskripsi = $line->deskripsi;
                    $qty = $line->qty;
                    $jumlah = $line->jumlah;
                    $kd_user = $line->kd_user;
					$sub_jumlah_pemeriksaan +=$jumlah;
                    $queryuser2 = $this->db->query("select user_names from zusers where kd_user= '$kd_user'")->row();
                    $namauser = $queryuser2->user_names;
					if ($ket_head==$ket_head_lalu){
						$html.='
						<tbody>
							
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="right">' . number_format($jumlah, 0, '.', '.') . '</td>
								<td width="70" align="center">' . $namauser . '</td>
							</tr>';   
					}else{
						$html.='
						<tbody>
							<tr> 
								<td> </td>
								<th colspan="6" align="left">' . $line->head . '</th>
							</tr>
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="right">' . number_format($jumlah, 0, '.', '.') . '</td>
								<td width="70" align="center">' . $namauser . '</td>
							</tr> ';   
					}
						
					$ket_head_lalu=$line->head;
                }
				
				/* Query Penerimaan*/
				$sub_jumlah_penerimaan=0;
                foreach ($query3 as $line) {
					$noo++;
                    $namapasien = $line->namapasien;
                    $deskripsi = $line->deskripsi;
                    $qty = $line->qty;
                    $jumlah = $line->jumlah;
                    $kd_user = $line->kd_user;
					$sub_jumlah_penerimaan +=$jumlah;
                    $queryuser2 = $this->db->query("select user_names from zusers where kd_user= '$kd_user'")->row();
                    $namauser = $queryuser2->user_names;
					
					$html.='
						<tbody>
							<tr> 
								<td> </td>
								<th colspan="6" align="left">' . $line->head . '</th>
							</tr>
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="right">' . number_format($jumlah, 0, '.', '.') . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="center">' . $namauser . '</td>
							</tr>';     
                }
				
                $html.='
					<tr>
						<td width="10"> </td>
						<th align="right" colspan="3">Sub Total</th>
						<th width="70" align="right">' . number_format($sub_jumlah_penerimaan, 0, '.', '.') . '</th>
						<th width="70" align="right">' . number_format($sub_jumlah_pemeriksaan, 0, '.', '.') . '</th>
						<td width="70"> </td>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>';
                $grandpenerimaan += $sub_jumlah_penerimaan;
				$grandpemeriksaan += $sub_jumlah_pemeriksaan;
            }
            $html.='
				<tr>
					<th align="right" colspan="4">GRAND TOTAL</th>
					<th align="right" width="70">' . number_format($grandpenerimaan, 0, '.', '.') . '</th>
					<th align="right" width="70">' . number_format($grandpemeriksaan, 0, '.', '.') . '</th>
					<th align="right" width="70">&nbsp;</th>
				</tr> ';
            $html.='</tbody></table>';
        }
		$prop=array('foot'=>true);
		if ($type_file === true || $type_file == 'true') {
			//$no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Lap_Transaksi_Rehab_Medik_Detail_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{ 
			// echo $html;
			$this->common->setPdf('P','Lap. Transaksi Rehab Medik ',$html);	
		}
		//$this->common->setPdf('P','Laporan Transaksi Laboratorium',$html);	
    }
	
	public function LapTransaksiSummary(){
		$common=$this->common;
   		$title='LAPORAN TRANSAKSI REHAB MEDIK';
		$param=json_decode($_POST['data']);
		
		$asalpasien   = $param->asal_pasien;
		$kdUser       = $param->user;
		$periode      = $param->periode;
		$tglAwal      = $param->tglAwal;
		$tglAkhir     = $param->tglAkhir;
		$customer     = $param->customer;
		$kdCustomer   = $param->kdcustomer;
		$tipe         = $param->tipe;
		$type_file   = $param->type_file;
		$tglAwalAsli  = $param->tglAwal;
		$tglAkhirAsli = $param->tglAkhir;
		$periode      = $param->periode;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$ParamShift2 = "";
		$ParamShift3 = "";
		
		if (strtolower($kdUser) == strtolower('Semua')) {
			$username = "Semua";
		}else{
			$username = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
		}
		
		$res = $this->db->query("select kd_unit,kd_bagian from unit where nama_unit='Rehab Medik'")->row();
		$kd_unit= $res->kd_unit;
		//$kd_kasir= $this->db->query("select kd_kasir from kasir where deskripsi='Kasir Laboratorium PA'")->row()->kd_kasir;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->kd_unit."' and kd_asal='3'")->row()->kd_kasir;
		
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		/* $awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir))); */
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
			$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
			$awal=tanggalstring(date('Y-m-d',strtotime($this->db->query(" select date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date) as tawal")->row()->tawal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($this->db->query("select date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval as takhir")->row()->takhir)));
		}

		$pisahtgl = explode("/", $tglAkhir, 3);
		if (count($pisahtgl) == 2) {

			$bulan = $pisahtgl[0];
			$tahun = $pisahtgl[1];
			$tmpbulan = $bulan;
			if ($bulan == 'Jan') {
				$bulan = '1';
			} elseif ($bulan == 'Feb') {
				$bulan = '2';
			} elseif ($bulan == 'Mar') {
				$bulan = '3';
			} elseif ($bulan == 'Apr') {
				$bulan = '4';
			} elseif ($bulan == 'May') {
				$bulan = '5';
			} elseif ($bulan == 'Jun') {
				$bulan = '6';
			} elseif ($bulan == 'Jul') {
				$bulan = '7';
			} elseif ($bulan == 'Aug') {
				$bulan = '8';
			} elseif ($bulan == 'Sep') {
				$bulan = '9';
			} elseif ($bulan == 'Oct') {
				$bulan = '10';
			} elseif ($bulan == 'Nov') {
				$bulan = '11';
			} elseif ($bulan == 'Dec') {
				$bulan = '12';
			}

			$jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$tglAkhir = $tahun . '-' . $bulan . '-' . $jmlhari;
			$tglAwal = '01/' . $tglAwal;
		}

		if ($asalpasien == 'Semua') {
			$tmpasalpasien = 'Semua Pasien';
		} else if ($asalpasien == 'RWJ/IGD') {
			$tmpasalpasien = 'Rawat Jalan / Gawat Darurat';
		} else if ($asalpasien == 'RWI') {
			$tmpasalpasien = 'Rawat Inap';
		} else {
			$tmpasalpasien = $asalpasien;
		}

		if ($tglAkhirAsli == $tglAkhir) {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tglAkhirAsli;
		} else {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tmpbulan . '/' . $tahun;
		}
		
		if($shift == 'All'){
			$ParamShift2 = " WHERE pyt.Type_Data <=3 
								AND (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") 
								AND db.shift in (1,2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
								OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))
														 ";
			$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
								and dt.shift in (1,2,3) AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
								or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$shift = 'Semua Shift';
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =1 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =1 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,2) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))) ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,2) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))  ";
				$shift="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))
														  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))   ";
				$shift="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =2 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =2 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . "') 
									AND db.shift in (2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (2,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$shift="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =3 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =3 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$shift="Shift 3";
			} 
		}
	   
		//---------------------------------jika kelompok pasien = SEMUA-------------------------------------------
		if ($customer == 'Semua') {
			$kriteria_customer1 = "";
			$kriteria_customer2 = "";
		} else {
			$kriteria_customer1 = " AND k.kd_customer = '" . $kdCustomer . "'";
			$kriteria_customer2 = " AND k.kd_customer = '" . $kdCustomer . "'";
		}

		if (strtolower($kdUser) == strtolower('Semua')) {
			$criteriaUser = "";
		}else{
			$criteriaUser = " AND db.kd_user = '".$kdUser."'";
		}

		if ($asalpasien == 'Semua') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."') $criteriaUser";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."') $criteriaUser";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."') $criteriaUser";
		} else if ($asalpasien == 'RWJ/IGD') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."') $criteriaUser
												AND left(t.kd_unit,1) in ('2','3')";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND left(tr.kd_unit,1) in ('2','3') $criteriaUser ";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."') $criteriaUser";
		} else if ($asalpasien == 'RWI') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwi."') $criteriaUser
												AND left(t.kd_unit,1) = '1'";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND left(tr.kd_unit,1) = '1' $criteriaUser";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwi."') AND left(t.kd_unit,1) = '1' $criteriaUser";
		} else {
			$kriteria1 = " AND t.kd_kasir in ('".$kd_kasir."') $criteriaUser
												And left (p.kd_pasien,2)='LB'";

			$kriteria2 = " AND t.kd_kasir in ('".$kd_kasir."')
												And left (p.kd_pasien,2)='LB' $criteriaUser";
			$kriteria3 = " AND t.kd_kasir in ('".$kd_kasir."') $criteriaUser";
		}
		$queryBody = $this->db->query( "Select X.BAYAR,X.Kd_CUSTOMER,X.CUSTOMER,
											SUM(P) AS JML_PASIEN,SUM(JML) AS JML_TR  
											from ( Select 1 AS BAYAR,k.Kd_CUSTOMER,c.CUSTOMER,
											k.kd_pasien,  1 as P , SUM(db.Jumlah) AS JML   
													from Detail_bayar  db     
														inner join payment py on db.kd_pay=py.kd_Pay       
														inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay        
														inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir      
														inner join unit u on u.kd_unit=t.kd_unit        
														inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
														inner join customer c on k.kd_customer=c.kd_Customer       
														left join kontraktor knt on c.kd_customer=knt.kd_Customer             
														inner join pasien p on t.kd_pasien=p.kd_pasien       
												" . $ParamShift2 . "" . $kriteria_customer1 . "" . $kriteria1 . "
												and u.kd_bagian=74  
												and t.ispay='T' 
												and pyt.Type_Data <=3
											GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.KD_KASIR,
														k.kd_pasien  ) X 
											GROUP BY x.bayar,X.KD_CUSTOMER, X.CUSTOMER 
										union SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML)  
											FROM (select 0 as Tagih,k.KD_CUSTOMER,c.CUSTOMER,
											k.kd_pasien, 0 as P, SUM(Dt.QTY* Dt.Harga) + COALESCE((SELECT SUM(Harga * QTY ) 
													FROM Detail_Bahan WHERE kd_kasir = t.kd_kasir and no_transaksi = dt.no_transaksi and tgl_transaksi = dt.tgl_transaksi and urut = dt.urut),0) as JML      
													from detail_transaksi DT       
													inner join produk prd on DT.kd_produk=prd.kd_produk      
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir      
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk      
													inner join customer c on k.kd_customer=c.kd_Customer      
													left join kontraktor knt on c.kd_customer=knt.kd_Customer            
													inner join pasien p on t.kd_pasien=p.kd_pasien 
														" . $ParamShift3 . "" . $kriteria_customer2 . "" . $kriteria3 . "
															and (u.kd_bagian=74)  
														GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR ,
															k.kd_pasien, dt.No_Transaksi, dt.Tgl_Transaksi, dt.Urut ) X 
															GROUP BY x.tagih,X.KD_CUSTOMER, X.CUSTOMER ");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="5">'.$title.' '.$asalpasien.'<br>
					</tr>
					<tr>
						<th colspan="5">'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="5">'.$shift.'</th>
					</tr>
					<tr>
						<th colspan="5">Kelompok '.$tipe.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Kelompok Pasien</th>
					<th align="center">Jumlah Pasien</th>
					<th align="center">Pemeriksaan</th>
					<th align="center">Penerimaan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			$no_b=0;
			$html.='<tbody>';
			$jml_pemeriksaan = 0 ;
			$jml_penerimaan = 0 ;
			$jml_pasien = 0;
			foreach ($query as $line) 
			{
				
				$no++;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td align="right">'.$line->jml_pasien.' &nbsp; &nbsp;</td>';
				
				if($line->bayar == 0){
					$html.='<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
							<td align="right"> 0 &nbsp;</td>
						</tr>';
					$jml_pemeriksaan = $jml_pemeriksaan + $line->jml_tr;
				}else{
					$html.='<td align="right"> 0 &nbsp;</td>
							<td align="right">'.number_format($line->jml_tr,0, "." , ".").' &nbsp; </td>
						</tr>';
					$jml_penerimaan = $jml_penerimaan + $line->jml_tr;
				}
				
				$jml_pasien = $jml_pasien + $line->jml_pasien;
			}
		$selisih = 	$jml_pemeriksaan - $jml_penerimaan;
		$html.='<tr>
					<td colspan="2" align="right"> Total : &nbsp;</td>
					<td align="right"> '.$jml_pasien.' &nbsp; &nbsp;</td>
					<td align="right"> '.number_format($jml_pemeriksaan,0, "." , ".").' &nbsp;</td>
					<td align="right"> '.number_format($jml_penerimaan,0, "." , ".").' &nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="right"> Selisih : &nbsp;</td>
					<td> </td>
					<td align="right"> '.number_format($selisih,0, "." , ".").' &nbsp; </td>
					<td> </td>
				</tr>';	
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		// $this->common->setPdf('P','Lap. Transaksi Summary',$html);	

		if ($type_file === true || $type_file == 'true') {
			//$no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$name="Lap_Transaksi_Rehab_Medik_Summary_".date('d-M-Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{ 
			// echo $html;
			$this->common->setPdf('L','Lap. Transaksi Summary',$html);	
		}
		//$html.='</table>';
		
	}
	
	public function LapTransaksi_PerKomponenDetail(){
   		$common=$this->common;
   		$title='LAPORAN TRANSAKSI PER KOMPONEN DETAIL';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		//$type_file = 0;
		/* $arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) "; */
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}else{
			$customerx="";
			//$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and left(tr.kd_unit,1) in ('2')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."' and left(tr.kd_unit,1) in ('1')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."' and left(tr.kd_unit,1) in ('3')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											AND t.kd_Unit in (".$kd_unit.") AND $q_shift
											Group by dc.kd_Component  Order by kd_Component ")->result();
		$arr_kd_component = array(); //array menampung kd_component
		$q_select_komponen_dinamis_1 = '';
		$q_select_komponen_dinamis_2 = '';
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$q_select_komponen_dinamis_1 .= 'sum(c'.$line->kd_component.')'.'as c'.$line->kd_component.',';
				$q_select_komponen_dinamis_2 .= 'SUM(CASE WHEN dtc.kd_component ='.$line->kd_component.' THEN dtc.tarif ELSE 0 END) '.'as c'.$line->kd_component.',';
				$i++;
			}
			$q_select_komponen_dinamis_1 = substr($q_select_komponen_dinamis_1,0,-1);
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
			$q_select_komponen_dinamis_1 = "0 as none";
		}
		
		
		$queryHead = $this->db->query(
			"
			 SELECT unit.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  
								COALESCE(SUM(x.tarif),0) as JUMLAH ,
								$q_select_komponen_dinamis_1

				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN 
				(
					SELECT
					dtc.kd_kasir,
					dtc.no_transaksi,
					dtc.urut,
					dtc.tgl_transaksi,
						$q_select_komponen_dinamis_2  sum(dtc.tarif ) as tarif  
					FROM (
						Detail_Component dtc
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						WHERE    
															$crtiteriakodekasir  And						
															(dtc.TGL_transaksi between '$tgl_awal'   and  '$tgl_akhir')	
															and dtc.tarif <> 0	
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi
				) x ON x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit On unit.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien 
				left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
				inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					 ".$crtiteriaAsalPasien." 
					 $customerx
					  and unit.kd_bagian =  '74'  
				GROUP BY unit.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
				order by Ps.kd_pasien
                "
		);
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query_array = $queryHead->result_array();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		/* $html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>'; */
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>';
			foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
		$html.='
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) { 
			$all_total_= array();
			foreach($query_kolom as $line){
				$all_total_[$line->kd_component] = 0;
			}
			# distinct unit
			$arr_unitlab = array();
			for($i=0;$i<count($query);$i++){
				$arr_unitlab ['unit'][$i] = $query[$i]->nama_unit;
			}
			$arr_unitlab = array_unique($arr_unitlab['unit']);
			
			# distinct pasien
			$arr_kdpasien = array();
			$arr_namapasien = array();
			for($i=0;$i<count($query);$i++){
				$arr_kdpasien ['kd_pasien'][$i] = $query[$i]->kd_pasien;
				$arr_namapasien ['nama'][$i] = $query[$i]->nama;
			}
			$arr_kdpasien = array_unique($arr_kdpasien['kd_pasien']);
			$arr_namapasien = array_unique($arr_namapasien['nama']);
			
			$no=0;
			for($i=0;$i<count($arr_unitlab);$i++){
				$no++;
				$colspan=count($query_kolom)+2;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="'.$colspan.'" align="left">'.$arr_unitlab[$i].'</th>
				</tr>
				';
				$nama='';
				$no2=0;
				for($j=0;$j<count($query);$j++){
					if($arr_unitlab[$i] == $query[$j]->nama_unit && $nama != $query[$j]->nama){
						$no2++;
						$nama=$query[$j]->nama;
						$html.='
						<tr class="headerrow"> 
							<th width="">&nbsp;</th>
							<th width="" colspan="'.$colspan.'" align="left">'.$no2.'. '.$query[$j]->kd_pasien.' ' .'-'. ' '.$query[$j]->nama.'</th>
						</tr>
						';
						$tindakan = '';
						for($k=0;$k<count($query);$k++){
							if($query[$j]->nama_unit == $query[$k]->nama_unit && $nama == $query[$k]->nama && $tindakan != $query[$k]->deskripsi){
								
								
								$html.='
									<tr> 
										<td width="">&nbsp;</th>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$k]->deskripsi.' </td>';
								$all_total_jumlah += $query[$k]->jumlah;
								$xx=0;
								foreach($query_kolom as $line){
									$all_total_[$line->kd_component] += $query_array[$k]['c'.$line->kd_component];
									$html.='
										<td width="" align="right">'.$this->rupiah($query_array[$k]['c'.$line->kd_component]).' </td>';
									$xx++;
								}  
								$html.='
										<td width="" align="right">'.$this->rupiah($query[$k]->jumlah).' </td>
										
										</tr>
									'; 	
								$tindakan = $query[$k]->deskripsi;
								
							}
							
						}
					}
					
				} 
			}
			$html.='
				<tr> 
					<th width="" colspan="2">&nbsp;</th>';
			foreach($query_kolom as $line){
				
				$html.='
					<th width="" align="right">'.$this->rupiah($all_total_[$line->kd_component]).' </th>';
				
			}  $html.='
			<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
			</tr>
					'; 
			
				
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="3" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1 || count($query)>700){
			$name=' Lap_Transaksi_PerKomponen_Det_'.date('d-M-Y').'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		 }else{
			$this->common->setPdf('L','Lap. Transaksi Per Komponen Detail',$html);	
		} 
		//echo $html;
   	}
	
	public function LapTransaksi_PerKomponenSummary(){
   		$common=$this->common;
   		$title='LAPORAN TRANSAKSI PER KOMPONEN SUMMARY';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		//$type_file = 0;
		/* $arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) "; */
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}else{
			$customerx="";
			//$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and left(tr.kd_unit,1) in ('2')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."' and left(tr.kd_unit,1) in ('1')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."' and left(tr.kd_unit,1) in ('3')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (1,2,3))		
			Or  (dt.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="dt.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And dt.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dt.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And dt.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi  
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal 
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											AND t.kd_Unit in (".$kd_unit.") AND $q_shift
											Group by dc.kd_Component  Order by kd_Component ")->result();
		$arr_kd_component = array(); //array menampung kd_component
		$q_select_komponen_dinamis_1 = '';
		$q_select_komponen_dinamis_2 = '';
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$q_select_komponen_dinamis_1 .= 'sum(c'.$line->kd_component.')'.'as c'.$line->kd_component.',';
				$q_select_komponen_dinamis_2 .= 'SUM(CASE WHEN dtc.kd_component ='.$line->kd_component.' THEN dtc.tarif ELSE 0 END) '.'as c'.$line->kd_component.',';
				$i++;
			}
			$q_select_komponen_dinamis_1 = substr($q_select_komponen_dinamis_1,0,-1);
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
		}
		
		
		
		$queryHead = $this->db->query(
			"
			 SELECTs unit.Nama_Unit, count(Ps.Kd_Pasien) as jml_pasien,SUM(dt.Qty)AS Qty, p.Deskripsi,  
								COALESCE(SUM(x.tarif),0) as JUMLAH , 
								$q_select_komponen_dinamis_1
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk   
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN 
				(
					SELECT
					dtc.kd_kasir,
					dtc.no_transaksi,
					dtc.urut,
					dtc.tgl_transaksi,
						$q_select_komponen_dinamis_2  sum(dtc.tarif ) as tarif  
					FROM (
						Detail_Component dtc
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						WHERE    
															$crtiteriakodekasir  And						
															(dtc.TGL_transaksi between '$tgl_awal'   and  '$tgl_akhir')	
															and dtc.tarif <> 0	
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi
				) x ON x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit On unit.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
				inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal 
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					 ".$crtiteriaAsalPasien." 
					 $customerx
					  and unit.kd_bagian =  '74'  
				GROUP BY unit.Nama_Unit,p.Deskripsi
				
				
				
				
                "
		);
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query_array = $queryHead->result_array();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		/* $html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>'; */
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="5%" align="center">Jumlah pasien</th>
					<th width="5%" align="center">Jumlah Tindakan</th>';
			foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
		$html.='
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$all_total_= array();
			foreach($query_kolom as $line){
				$all_total_[$line->kd_component] = 0;
			}
			# distinct unit
			$arr_unitlab = array();
			for($i=0;$i<count($query);$i++){
				$arr_unitlab ['unit'][$i] = $query[$i]->nama_unit;
			}
			$arr_unitlab = array_unique($arr_unitlab['unit']);
			
			# distinct pasien
			$arr_kdpasien = array();
			$arr_namapasien = array();
			for($i=0;$i<count($query);$i++){
				//$arr_kdpasien ['kd_pasien'][$i] = $query[$i]->kd_pasien;
				//$arr_namapasien ['nama'][$i] = $query[$i]->nama;
			}
			//$arr_kdpasien = array_unique($arr_kdpasien['kd_pasien']);
			//$arr_namapasien = array_unique($arr_namapasien['nama']);
			$nama_unit = '';
			$no=0;
			for($i=0;$i<count($arr_unitlab);$i++){
				$no++;
				$colspan=count($query_kolom)+2;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="'.$colspan.'" align="left">'.$arr_unitlab[$i].'</th>
				</tr>
				';
				$nama='';
				$no2=0;
				for($j=0;$j<count($query);$j++){
					if($arr_unitlab[$i] == $query[$j]->nama_unit){
						$no2++;
						
						$tindakan = '';
						//for($k=0;$k<count($query);$k++){
							if($query[$j]->nama_unit == $nama_unit || $tindakan != $query[$j]->deskripsi){
								
								$html.='
									<tr> 
										<td width="">&nbsp;</th>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$j]->deskripsi.' </td>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$j]->jml_pasien.' </td>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$j]->qty.' </td>';
								$all_total_jumlah += $query[$j]->jumlah;
								$xx=0;
								foreach($query_kolom as $line){
									$all_total_[$line->kd_component] += $query_array[$j]['c'.$line->kd_component];
									$html.='
										<td width="" align="right">'.$this->rupiah($query_array[$j]['c'.$line->kd_component]).' </td>';
									$xx++;
								}  
								$html.='
										<td width="" align="right">'.$this->rupiah($query[$j]->jumlah).' </td>
										
										</tr>
									'; 	 
								$tindakan = $query[$j]->deskripsi;
								$nama_unit = $query[$j]->nama_unit;
							}
							
						//}
					}
					
				}
			}
			$html.='
				<tr> 
					<th width="" colspan="4">&nbsp;</th>';
			foreach($query_kolom as $line){
				
				$html.='
					<th width="" align="right">'.$this->rupiah($all_total_[$line->kd_component]).' </th>';
				
			} 
			$html.='
			<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
			</tr>
					';
				
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Transaksi_PerKomponen_Sum_'.date('d-M-Y').'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		 }else{
			$this->common->setPdf('L','Lap. Transaksi Per Komponen Summary',$html);	
		} 
		//echo $html;
   	}
	
	public function LaporanRegisterDetail()
	{
		$common       = $this->common;
		//$result       = $this->result;
		//$title      = 'Laporan Pasien Detail';
		$param        = json_decode($_POST['data']);
		
		$JenisPasien  = $param->JenisPasien;
		$TglAwal      = $param->TglAwal;
		$TglAkhir     = $param->TglAkhir;
		$KelPasien    = $param->KelPasien;
		$KelPasien_d  = $param->KelPasien_d;
		$type_file    = $param->Type_File;
		$JmlList      = $param->JmlList;
		$nama_kel_pas = $param->nama_kel_pas;
		$order_by     = $param->order_by;
		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "kdpasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")){
			$criteriaOrder = "namapasien";
		}else{
			$criteriaOrder = "tglmas";
		}
		//ambil list kd_unit
		$u="";
		
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$criteria    = "";
		$tmpunit     = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$asal_pasien = $param->asal_pasien;
		//echo  $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
			$KdKasirIGD_gabung='';
		}else{
			$KdKasirIGD='';
			$KdKasirIGD_gabung='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
				$KdKasirIGD_gabung .= ",'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			$KdKasirIGD_gabung = substr($KdKasirIGD_gabung, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi."".$KdKasirIGD_gabung.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and left(tr.kd_unit,1) = '2'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi." and left(tr.kd_unit,1) = '1'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD." and left(tr.kd_unit,1) = '3'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$totalpasien = 0;
		$UserID      = 0;
		$tglsum      = $TglAwal; //tgl awal
		$tglsummax   = $TglAkhir; //tgl akhir
		$awal        = tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir       = tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		$html="";
			if ($tmpkelpas !== 'Semua')
				{
					if ($tmpkelpas === 'Umum')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : Umum';
						}
					}elseif ($tmpkelpas === 'Perusahaan')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
						}
					}elseif ($tmpkelpas === 'Asuransi')
					{
						$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
						if ($kelpas !== 'NULL')
						{
							$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
							$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
						}
					}
				}else {
					$Param = $Paramplus." ";
					$tmpTambah = $nama_kel_pas;
				}
				
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];
				$queryRS = "select * from db_rs WHERE code='".$kd_rs."'";
				$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
				$no = 0;

		while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
		{							   
			$telp='';
			$fax='';
			if(($line['phone1'] != null && $line['phone1'] != '')|| ($line['phone2'] != null && $line['phone2'] != '')){
				$telp='<br>Telp. ';
				$telp1=false;
				if($line['phone1'] != null && $line['phone1'] != ''){
					$telp1=true;
					$telp.=$line['phone1'];
				}
				if($line['phone2'] != null && $line['phone2'] != ''){
					if($telp1==true){
						$telp.='/'.$line['phone2'].'.';
					}else{
						$telp.=$line['phone2'].'.';
					}
				}else{
					$telp.='.';
				}
			}
			if($line['fax'] != null && $line['fax']  != ''){
				$fax='<br>Fax. '.$line['fax'] .'.';
			}
        }
		if($type_file == 1){
		$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		#HEADER TABEL LAPORAN
		$html.='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15">Laporan Buku Registrasi Detail Rehab Medik</th>
					</tr>
			';
	if($tmpTambah != ''){
		$html.='
					<tr>
						<th colspan="15">'.$tmpTambah.'</th>
					</tr>';
	}
		$html.='
					<tr>
						<th colspan="15">Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table> <br>';
		$html.='
			<table class="t1" border = "1">
			<thead>
			 <tr>
				   <th align="center" width="24" rowspan="2">No. </th>
				   <th align="center" width="80" rowspan="2">No. Medrec</th>
				   <th align="center" width="210" rowspan="2">Nama Pasien</th>
				   <th align="center" width="220" rowspan="2">Alamat</th>
				   <th align="center" width="26" colspan="2">Kelamin</th>
				   <th align="center" width="100" rowspan="2">Umur</th>
				   <th align="center" colspan="2">Kunjungan</th>
				   <th align="center" width="82" rowspan="2">Pekerjaan</th>
				   <th align="center" width="63" rowspan="2">Rujukan</th>
			 </tr>
			 <tr>
				   <td align="center" width="37"><b>L</b></td>
				   <td align="center" width="37"><b>P</b></td>
				   <td align="center" width="37"><b>Baru</b></td>
				   <td align="center" width="37"><b>Lama</b></td>
			 </tr>
			</thead>';

		$fquery = $this->db->query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='74' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
		$html.='<tbody>';
		$totBaru=0;
		$totLama=0;
		$totL=0;
		$totP=0;
		$totRujukan=0;
		
		foreach($fquery->result_array() as $f)
		//while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "
							Select k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							--k.Jam_masuk as jammas,
							--k.Tgl_masuk as tglmas,
							--dr.nama as dokter, 
							c.customer as customer 
							--zu.user_names as username,
							--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
								inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
									$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan 
									--,k.Jam_masuk,k.Tgl_masuk,dr.nama
									,c.customer 
									--,zu.user_names,k.urut_masuk
								Order By $criteriaOrder 
			   ";
			} else {
					$query = "
								Select k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								--k.Jam_masuk as jammas,
								--k.Tgl_masuk as tglmas,
								--dr.nama as dokter, 
								c.customer as customer 
								--zu.user_names as username,
								--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
								inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
									$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan 
									--,k.Jam_masuk,k.Tgl_masuk,dr.nama
									,c.customer
									--,zu.user_names,k.urut_masuk
								Order By $criteriaOrder ";
				};
       			// echo $query;

		// echo $query;
       $result = pg_query($query) or die('Query failed: ' . pg_last_error());
       $i=1;

		   if(pg_num_rows($result) <= 0)
		   {
				$html.='';
		   }else{
				$html.='<tr><td colspan="8">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					if($line['rujukan']=='x'){
						$totRujukan+=1;
					}

					$html.='
							<tr class="headerrow"> 

							<td align="right">'.$i.'</td>

							<td  align="left" style="padding-left:5px;">'.$line['kdpasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['namapasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['alamatpas'].'</td>';
							/*<td align="center">'.$line['jk'].'</td>
							<td align="center"></td>*/
							if (strtolower($line['jk']) == "l") {
								$html .= "<td align='center'>X</td><td align='center'></td>";
								$totL++;
							}else{
								$html .= "<td align='center'></td><td align='center'>X</td>";
								$totP++;
							}
					$html.='<td  align="left" style="padding-left:5px;">'.$line['umur'].'</td>
							<td  align="center">'.strtoupper($line['pasienbar']).'</td>
							<td  align="center">'.strtoupper($line['pasienlama']).'</td>
							<td  align="left" style="padding-left:5px;">'.$line['pekerjaan'].'</td>
							<td  align="center">'.$line['rujukan'].'</td>
							</tr>
							
					';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$html.='<tr>
					<td colspan="3"><b>Total Pasien Daftar di '.$f['nama_unit'].' : </b></td>
					<td><b>'.$i.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totL.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totP.'</b></td>
					<td></td>
					<td align="center" style="padding:5px;"><b>'.$totBaru.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totLama.'</b></td>
					<td ></td>
					<td align="center"><b>'.$totRujukan.'</b></td>
					</tr>';
					$totL 		= 0;
					$totP 		= 0;
					$totBaru 	= 0;
					$totLama 	= 0;
					$totRujukan	= 0;
			}
        }
		$html.="<tr><td colspan='3'><b>Total Keseluruhan Pasien</b></td><td colspan='8'><b>".$totalpasien."</b></td></tr>";
		// $html.='<tr><td colspan="3">&nbsp;</td><td colspan="12"><b>Baru : '.$totBaru.'/Lama :'.$totLama.'</b></td></tr>';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Pasien_Detail_RehabMedik.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Pasien Detail',$html);
		}
		//echo $html;
	}
	
	public function LaporanRegisterSummary()
    {
		$common=$this->common;
   		$title='Laporan Summary Pasien Rehab Medik';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		$html="";
		//ambil list kd_unit
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1);

		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and left(tr.kd_unit,1) = '2' ";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi." and left(tr.kd_unit,1) = '1' ";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD." and left(tr.kd_unit,1) = '3' ";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		
		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
										LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
											and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user
										left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal 
										where u.kd_bagian='74' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
										
										$crtiteriaAsalPasien
										group by U.Nama_Unit,
											u.Kd_Unit,
											ps.kd_Pasien,ps.Jenis_Kelamin,K .Baru,K .kd_Customer,ktr.jenis_cust
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			if ($u== "Semua" || $u== "")
			{
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			}else{
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien_RehabMedik.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
		
    }
	
	public function LaporanBatalTransaksi(){
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("select 
			  kd_kasir,
			  no_transaksi ,
			  tgl_transaksi ,
			  kd_pasien ,
			  nama ,
			  kd_unit,
			  nama_unit,
			  kd_user ,
			  kd_user_del ,
			 
			  jumlah,
			  user_name ,
			  tgl_batal ,
			  ket,cast(jam_batal as time)as jam_batal  from history_trans  
			where kd_kasir in('35') and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
		")->result();
		$html.='
			<table border="0" >
				
					<tr>
						<th colspan="9" align="center">LAPORAN PEMBATALAN TRANSAKSI</th>
					</tr>
					<tr>
						<th colspan="9" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1'>
				<thead>
					<tr>
						<th width='30' align='center'>NO.</th>
						<th width='50'>No. Trans</th>
						<th width='80'>No. medrec</th>
						<th width='200'>Nama Pasien</th>
						<th width='80'>Unit</th>
						<th width='8'>Tgl Jam Batal</th>
						<th width='90'>Operator</th>
						<th width='90'>Jumlah</th>
						<th width='100'>Keterangan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='9'>Tidak Ada Data</td>
					</tr>";
			$jmllinearea=count($result)+7;
		} else{
			$no=0;
			$jumlah=0;
			$jmllinearea=count($result)+5;
			foreach($result as $line){
				$noo=0;
				$no++;
				$nama_unit = str_replace('&','DAN',$line->nama_unit);
				$html.="<tr>
							<td align='center'>".$no."</td>
							<td align='center'>".$line->no_transaksi."</td>
							<td align='center'>".$line->kd_pasien."</td>
							<td>".wordwrap($line->nama,15,"<br>\n")."</td>
							<td>".$nama_unit."</td>
							<td>".date('d-M-Y', strtotime($line->tgl_batal))." ".$line->jam_batal."</td>
							<td>".$line->user_name."</td>
							<td align='right'>".number_format($line->jumlah,0,'.',',')."</td>
							<td>".wordwrap($line->ket,15,"<br>\n")."</td>
						</tr>";
					$jumlah += $line->jumlah;
				$jmllinearea += 1;
			}
			$html.="
					<tr>
						<th align='right' colspan='7' style='font-weight: bold;'></th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
						<th align='right' style='font-weight: bold;'></th>
					</tr>";
			$jmllinearea = $jmllinearea+1;	
		}	
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H6:H100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('history_pembatalan'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_history_pembatalan_transaksi.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN HISTORY PEMBATALAN TRANSAKSI',$html);
			echo $html;
		}
   	}
	
	public function LaporanRegistrasiDetail_Direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(100,15,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$JenisPasien = $param->JenisPasien;
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$KelPasien = $param->KelPasien;
		$KelPasien_d = $param->KelPasien_d;
		$type_file = $param->Type_File;
		$JmlList = $param->JmlList;
		$nama_kel_pas =$param->nama_kel_pas;
		//ambil list kd_unit
		$u="";
		
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$param->$kd_unit."') and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$param->$kd_unit."') and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ('".$param->$kd_unit."') and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien not like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$totalpasien = 0;
        $UserID = 0;
		$tglsum = $TglAwal; //tgl awal
        $tglsummax = $TglAkhir; //tgl akhir
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria="";
		$tmpunit = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		if ($tmpkelpas !== 'Semua')
			{
				if ($tmpkelpas === 'Umum')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : Umum';
					}
				}elseif ($tmpkelpas === 'Perusahaan')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}elseif ($tmpkelpas === 'Asuransi')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}
			}else {
				$Param = $Paramplus." ";
				$tmpTambah = $nama_kel_pas;
			}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 10)
			->setColumnLength(4, 3)
			->setColumnLength(5, 10)
			->setColumnLength(6, 3)
			->setColumnLength(7, 3)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 3)
			->setColumnLength(11, 10)
			->setColumnLength(12, 10)
			->setColumnLength(13, 10)
			->setColumnLength(14, 10)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("Laporan Buku Registrasi Detail Rawat Jalan", 9,"center")
			->commit("header")
			->addColumn($tmpTambah, 9,"center")
			->commit("header")
			->addColumn("Periode ".$awal." s/d ".$akhir, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		// # SET JUMLAH KOLOM HEADER
		// $tp->setColumnLength(0, 15)
			// ->setColumnLength(1, 2)
			// ->setColumnLength(2, 20)
			// ->setColumnLength(3, 1)
			// ->setColumnLength(4, 5)
			// ->setColumnLength(5, 1)
			// ->setColumnLength(6, 15)
			// ->setColumnLength(7, 2)
			// ->setColumnLength(8, 25)
			// ->setUseBodySpace(true);
		// #QUERY HEAD
		
		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='74' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
							
		// $tp->setColumnLength(0, 3)
			// ->setColumnLength(1, 10)
			// ->setColumnLength(2, 13)
			// ->setColumnLength(3, 10)
			// ->setColumnLength(4, 10)
			// ->setColumnLength(5, 15)
			// ->setColumnLength(6, 10)
			// ->setColumnLength(7, 25)
			// ->setColumnLength(8, 10)
			// ->setColumnLength(9, 10)
			// ->setUseBodySpace(true);
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Medrec", 1,"left")
			->addColumn("Nama Pasien", 1,"left")
			->addColumn("Alamat", 1,"left")
			->addColumn("JK", 1,"left")
			->addColumn("Umur", 1,"left")
			->addColumn("Kunjungan", 2,"center")
			->addColumn("Pekerjaan", 1,"left")
			->commit("header");
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Baru", 1,"left")
			->addColumn("Lama", 1,"left")
			->commit("header");
		$totBaru=0;
		$totLama=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "
							Select k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
								to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
								to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari'
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'x'  else ''  end as pasienbar,
							case when k.Baru=false then 'x'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
							--k.Jam_masuk as jammas,
							--k.Tgl_masuk as tglmas,
							--dr.nama as dokter, 
							c.customer as customer 
							--zu.user_names as username,
							--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
									$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,
									--k.Jam_masuk,k.Tgl_masuk,dr.nama,
									c.customer
									--,zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN 
			   ";
			} else {
					$query = "
								Select k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								--k.Jam_masuk as jammas,
								--k.Tgl_masuk as tglmas,
								--dr.nama as dokter, 
								c.customer as customer 
								--zu.user_names as username,
								--getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								--getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
									$crtiteriaAsalPasien
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,
									--k.Jam_masuk,k.Tgl_masuk,dr.nama,
									c.customer
									--, zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN ";
				};
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;

			if(pg_num_rows($result) <= 0)
			{
				$tp	->addColumn("Data tidak ada", 14,"center")
					->commit("header");
			}else{
				$tp	->addColumn($f['nama_unit'], 15,"left")
					->commit("header");
				// $html.='<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					$tp	->addColumn(($i).".", 1)
						->addColumn($line['kdpasien'], 1,"left")
						->addColumn($line['namapasien'], 1,"left")
						->addColumn($line['alamatpas'], 1,"left")
						->addColumn($line['jk'], 1,"left")
						->addColumn($line['umur'], 1,"left")
						->addColumn($line['pasienbar'], 1,"left")
						->addColumn($line['pasienlama'], 1,"left")
						->addColumn($line['pekerjaan'], 1,"left")
						->commit("header");
					// $html.='
							// <tr class="headerrow"> 

							// <td align="right">'.$i.'</td>

							// <td  align="left">'.$line['kdpasien'].'</td>
							// <td  align="left">'.wordwrap($line['namapasien'],15,"<br>\n").'</td>
							// <td  align="left">'.wordwrap($line['alamatpas'],15,"<br>\n").'</td>
							// <td align="center">'.$line['jk'].'</td>
							// <td  align="center">'.wordwrap($line['umur'],15,"<br>\n").'</td>
							// <td  align="center">'.$line['pasienbar'].'</td>
							// <td  align="center">'.$line['pasienlama'].'</td>
							// <td  align="left">'.wordwrap($line['pekerjaan'],7,"<br>\n").'</td>
							// <td  align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							// <td  align="center">'.$line['rujukan'].'</td>
							// <td  align="left">'.date('H:i A', strtotime($line['jammas'])).'</td>
							// <td  align="left">'.$line['kd_diagnosa'].'</td>
							// <td  align="left">'.wordwrap($line['penyakit'],15,"<br>\n").'</td>
							// <td  align="left">'.$line['username'].'</td>
							// </tr>
							
					// ';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$tp	->addColumn("Total Pasien Daftar di ".$f['nama_unit']." : ", 3)
					->addColumn($i, 12,"left")
					->commit("header");
			}
        }
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/tmp/datalabregisterdet.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
	public function cetaklaporanRWJ_Regisum()
    {
		$common=$this->common;
   		$title='Laporan Summary Pasien Rehab Medik';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$JmlList =  $param->JmlList;
		$type_file = $param->Type_File;
		$html="";
		//ambil list kd_unit
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1);

		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($criteria) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj='';
			foreach($cekKdKasirRwj->result() as $data){
				$KdKasirRwj .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwj = substr($KdKasirRwj, 0, -1);
			//$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi='';
			foreach($cekKdKasirRwi->result() as $data){
				$KdKasirRwi .= "'".$data->kd_kasir."',";
			}
			$KdKasirRwi = substr($KdKasirRwi, 0, -1);
			//$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD='';
			foreach($cekKdKasirIGD->result() as $data){
				$KdKasirIGD .= "'".$data->kd_kasir."',";
			}
			$KdKasirIGD = substr($KdKasirIGD, 0, -1);
			//$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.",".$KdKasirRwi.",".$KdKasirIGD.")";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien not like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirRwi."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien="and t.kd_kasir=".$KdKasirIGD."";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in (".$KdKasirRwj.") and k.kd_pasien like 'LB%'";
			//$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'Kunjungan Langsung';
		}
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		
		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
										LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
											and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user
										where u.kd_bagian='74' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
										
										$crtiteriaAsalPasien
										group by U.Nama_Unit,
											u.Kd_Unit,
											ps.kd_Pasien,ps.Jenis_Kelamin,K .Baru,K .kd_Customer,ktr.jenis_cust
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			if ($u== "Semua" || $u== "")
			{
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			}else{
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien_RehabMedik.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }
	
	public function LapJasaPelayananDokterDetail(){
   		$common=$this->common;
   		$title='LAPORAN DAFTAR PELAYANAN DOKTER DETAIL';
		$param=json_decode($_POST['data']);
		
		$type_file				=$param->type_file;
		$kd_poli    			=$param->kd_poli;
		$pelayananPendaftaran   =$param->pelayananPendaftaran;
		$pelayananTindak    	=1;//$param->pelayananTindak;
		$shift         			= $param->shift;
		$shift1         		= $param->shift1;
		$shift2         		= $param->shift2;
		$shift3         		= $param->shift3;
		$kd_dokter    			=$param->kd_dokter;
		$tglAwal   				=$param->tglAwal;
		$tglAkhir  				=$param->tglAkhir;
		$kd_customer  			=$param->kd_kelompok;
		$kd_profesi  			=$param->kd_profesi;
		$html					='';
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$cari_kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$kd_unit = stristr($cari_kd_unit,"'7");
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){ */
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit in (".$kd_unit."))";
		/* } else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='0'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='1'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer_far="";
			$customerfar='SEMUA KELOMPOK CUSTOMER';
		} else{
			$ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
			$customerfar=""; 
			$customerfar.="KELOMPOK CUSTOMER " ; 
			$customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}

		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far="";
			//$ckd_unit_far=" AND LEFT (u.kd_unit, 1)!='5'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit in ('".$kd_poli."')";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit='".$kd_poli."'")->row()->nama_unit); 
		}
		if($shift == 'All'){
			$criteriaShift="and (dt.Shift In ( 1,2,3)) 
							   Or  (dt.Shift= 4)";
			
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1))";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1,2))";
			
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 1,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 2))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 2,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In (3))";
			} 
		}
		$queryHead = $this->db->query(
			"
			 Select distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where 
											t.lunas='t'		
											and u.kd_unit in(".$kd_unit.")  					
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$criteriaShift."	
											".$ckd_dokter_far." ".$ckd_customer_far." ".$criteria." ".$ckd_unit_far." 				
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="8"> Laporan Pelayanan '.$dokterfar.' dari '.$customerfar.' dan '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Keterangan</th>
					<th width="13%" align="center">Jml Pasien</th>
					<th width="13%" align="center">Jml Produk</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Total</th>
					<th width="13%" align="center">Potongan Pajak ('.$pajaknya.'%)</th>
					<th width="13%" align="center">Total Setelah Potongan</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {

			$no=0;

			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td style="padding-left:10px;">'.$no.'</td>
							<td align="left" colspan="7" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>
						</tr>';
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component in ('20') THEN sum(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component in ('38') THEN sum(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where 
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." ".$criteriaShift."
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						where d.kd_dokter='".$line->kd_dokter."' AND t.kd_unit in (".$kd_unit.")
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;

				foreach ($query2 as $line2) 
				{
				$html.="<tr>";
				$html.="<td></td>";
					$noo++;
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jpdok;
					$total_jumlah += $line2->jpdok * $line2->jml_pasien;
					$total_pajak += ((($line2->jpdok * $line2->jml_pasien)/100)*$pajaknya);
					$total_all += $line2->jpdok-((($line2->jpdok * $line2->jml_pasien)/100)*$pajaknya);
					$html.="<td style='padding-left:10px;'>".$noo." - ".$line2->deskripsi."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok * $line2->jml_pasien)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah(((($line2->jpdok * $line2->jml_pasien)/100)*$pajaknya))."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok-((($line2->jpdok * $line2->jml_pasien)/100)*$pajaknya))."</td>";
				$html.="</tr>";
				}

				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pajak += $total_pajak;
				$all_total_all += $total_all;

				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td align='right' style='padding-right:10px;'><b>Sub Total</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$total_jml_pasien."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$total_qty."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jpdok)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jumlah)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pajak)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_all)."</b></td>";
				$html.="</tr>";
			}
			$html.="<tr>";
			$html.="<td></td>";
			$html.="<td align='right' style='padding-right:10px;'><b>Grand Total</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_jml_pasien."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_qty."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jpdok)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jumlah)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pajak)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_all)."</b></td>";
			$html.="</tr>";
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Jasa_Pelayanan_Dokter.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P','Lap. Jasa Pelayanan Dokter Detail',$html);	
		}
		echo $html;
   	}
	
	public function LapJasaPelayananDokterSummary(){
   		$common=$this->common;
   		$title='LAPORAN DAFTAR PELAYANAN DOKTER SUMMARY';
		$param=json_decode($_POST['data']);
		
		$type_file				=$param->type_file;
		$kd_poli    			=$param->kd_poli;
		$pelayananPendaftaran   =$param->pelayananPendaftaran;
		$pelayananTindak    	=1;//$param->pelayananTindak;
		$shift	         		= $param->shift;
		$shift1         		= $param->shift1;
		$shift2         		= $param->shift2;
		$shift3         		= $param->shift3;
		$kd_dokter    			=$param->kd_dokter;
		$tglAwal   				=$param->tglAwal;
		$tglAkhir  				=$param->tglAkhir;
		$kd_customer  			=$param->kd_kelompok;
		$kd_profesi  			=$param->kd_profesi;
		$html					='';
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$cari_kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$kd_unit = stristr($cari_kd_unit,"'7");
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){ */
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		/* } else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='0'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='1'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer_far="";
			$customerfar='SEMUA KELOMPOK CUSTOMER';
		} else{
			$ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
			$customerfar=""; 
			$customerfar.="KELOMPOK CUSTOMER " ; 
			$customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}

		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)!='5'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit = '".$kd_poli."'";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit='".$kd_poli."'")->row()->nama_unit); 
		}
		if($shift == 'All'){
			$criteriaShift="and (dt.Shift In ( 1,2,3)) 
							   Or  (dt.Shift= 4)";
			
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1))";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1,2))";
			
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 1,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 2))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 2,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In (3))";
			} 
		}
		$queryHead = $this->db->query(
			"
			 Select distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where 
											t.lunas='t'		
											and u.kd_unit=".$kd_unit."  					
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$criteriaShift." 	
											".$ckd_dokter_far." ".$ckd_customer_far." ".$criteria." ".$ckd_unit_far." ".$criteria_profesi."					
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="8"> Laporan Pelayanan '.$dokterfar.' dari '.$customerfar.' dan '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		//<th width="13%" align="center">Jml Pasien</th>
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Keterangan</th>
					<th width="13%" align="center">Jml Produk</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Total</th>
					<th width="13%" align="center">Potongan Pajak ('.$pajaknya.'%)</th>
					<th width="13%" align="center">Total Setelah Potongan</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {

			$no=0;

			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component in ('20') THEN sum(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component in ('38') THEN sum(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria."  ".$criteriaShift."
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						where d.kd_dokter='".$line->kd_dokter."' AND t.kd_unit=".$kd_unit." 	
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;

				foreach ($query2 as $line2) 
				{
					$noo++;
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jpdok;
					$total_jumlah += $line2->jpdok* $line2->jml_pasien;
					$total_pajak += ((($line2->jpdok* $line2->jml_pasien)/100)*$pajaknya);
					$total_all += $line2->jpdok-((($line2->jpdok* $line2->jml_pasien)/100)*$pajaknya);
					
				}

				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pajak += $total_pajak;
				$all_total_all += $total_all;
				/* $html.='<tr>
							<td style="padding-left:10px;">'.$no.'</td>
							<td align="left" colspan="7" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>
						</tr>'; */
				$html.="<tr>";
				$html.='<td style="padding-left:10px;">'.$no.'</td>';
				$html.='<td align="left" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>';
				//$html.="<td style='padding-right:10px;' align='right'><b>".$total_jml_pasien."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$total_qty."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jpdok)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jumlah)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pajak)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_all)."</b></td>";
				$html.="</tr>";
			}
			/* $html.="<tr>";
			$html.="<td></td>";
			$html.="<td align='right' style='padding-right:10px;'><b>Grand Total</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_jml_pasien."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_qty."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jpdok)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jumlah)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pajak)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_all)."</b></td>";
			$html.="</tr>"; */
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Jasa_Pelayanan_Dokter.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('P','Lap. Jasa Pelayanan Dokter Summary',$html);	
		}
		echo $html;
   	}
	
	
	public function LaporanRekapitulasiJasaPelayanan(){
   		$common=$this->common;
   		$title='LAPORAN REKAP JASA PELAYANAN DOKTER';
		$param=json_decode($_POST['data']);
		
		$type_file				=$param->type_file;
		$kd_poli    			=$param->kd_poli;
		//$pelayananPendaftaran   =$param->pelayananPendaftaran;
		//$pelayananTindak    	=1;//$param->pelayananTindak;
		$shift       			= $param->shift0;
		$shift1         		= $param->shift1;
		$shift2         		= $param->shift2;
		$shift3         		= $param->shift3;
		$kd_dokter    			=$param->kd_dokter;
		$tglAwal   				=$param->tglAwal;
		$tglAkhir  				=$param->tglAkhir;
		//$kd_customer  			=$param->kd_kelompok;
		$kd_profesi  			=$param->kd_profesi;
		$html					='';
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kata_kd_unit='';
		if (strpos($carikd_unit,","))
		{
			$pisah_kata=explode(",",$carikd_unit);
			$qu='';
			for($i=0;$i<count($pisah_kata);$i++)
			{
				
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$qu.=$cek_kata.',';
				}
			}
			$kd_unit=substr($qu,0,-1);
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){ */
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit in (".$kd_unit."))";
		/* } else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */
		$asal_pasien = $param->pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriaAsalPasien_Detail="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')";
			$crtiteriaAsalPasien_Detail="and t.kd_kasir in ('".$KdKasirRwj."') AND left(tr.kd_unit,1) in ('2')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwi."'";
			$crtiteriaAsalPasien_Detail="and t.kd_kasir in('".$KdKasirRwi."')  AND left(tr.kd_unit,1) in ('1')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirIGD."')";
			$crtiteriaAsalPasien_Detail="and t.kd_kasir in('".$KdKasirIGD."') AND left(tr.kd_unit,1) in ('3')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriaAsalPasien_Detail="and t.kd_kasir in('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$nama_asal_pasien = 'Langsung';
		}
		/* $asal_pasien = $param->pasien;
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('25','26','27')";
			$nama_asal_pasien = 'Asal Pasien (Semua)';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25')";
			$nama_asal_pasien = 'Asal Pasien (RWJ)';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('25')";
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('26')";
			$nama_asal_pasien = 'Asal Pasien (RWI)';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('26')";
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('27')";
			$nama_asal_pasien = 'Asal Pasien (IGD)';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('27')";
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('25')";
			$nama_asal_pasien = 'Langsung';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('25')";
		} */
		if($kd_profesi == 1 ){
			$criteria_profesi="";
			$profesi="Dokter";			
		} else {
			$criteria_profesi="";			
			$profesi="Perawat";			
		}

		/* if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer_far="";
			$customerfar='SEMUA KELOMPOK CUSTOMER';
		} else{
			$ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
			$customerfar=""; 
			$customerfar.="KELOMPOK CUSTOMER " ; 
			$customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$ckd_customer_far=" And k.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$ckd_customer_far="";
		}
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}

		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)!='4'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit in ('".$kd_poli."')";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit in ('".$kd_poli."')")->row()->nama_unit); 
		}
		if($shift=='true'){
			$criteriaShift="and (dt.Shift In ( 1,2,3)) 
							   Or  (dt.Shift= 4)";
			
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1))";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1,2))";
			
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 1,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 2))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 2,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In (3))";
			} 
		}
		$queryHead = $this->db->query(
			"
			 Select distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
										Where 
											t.lunas='t'	".$crtiteriaAsalPasien_Detail." 	
											and u.kd_unit in(".$kd_unit.")
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$criteriaShift."	
											".$ckd_dokter_far." ".$ckd_customer_far." ".$criteria." ".$ckd_unit_far." ".$criteria_profesi."	
																						
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="11">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="11"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					
					
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$opstind=$this->db->query("select setting from sys_setting where key_data='opstindakan'")->row()->setting;
		$pphnya=$this->db->query("select setting from sys_setting where key_data='pphdokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
		
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Dokter</th>
					<th width="13%" align="center">Jasa Konsul</th>
					<th width="13%" align="center">PPH ('.$pphnya.'%)</th>
					<th width="13%" align="center">Jml Konsul Net</th>
					<th width="13%" align="center">Jasa Tindakan</th>
					<th width="13%" align="center">OPS ('.$opstind.'%)</th>
					<th width="13%" align="center">PPH ('.$pphnya.'%)</th>
					<th width="13%" align="center">Jml Tindakan Net</th>
					<th width="13%" align="center">Penerimaan</th>
				  </tr>
			';
			//echo count($query);
			$html.="";
		if(count($queryHead->result()) > 0) {
		$jmllinearea=count($query);
			$no=0;

			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pph_tindakan = 0;
			$all_total_tindakan_net = 0;
			$all_total_tindakan = 0;
			$all_total_pph = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				$queryBody = $this->db->query( 
					"SELECT
					 x.kd_pasien,
					 x.deskripsi, 
					 x.kd_unit_asal,
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						k.kd_pasien,
						d.Nama, 
						p.Deskripsi, 
						tr.kd_unit as kd_unit_asal,
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(case when xdt.KD_COMPONENT in ('20') THEN max(xdt.Tarif ) else 0 end) as JPDok, 
						(CASE WHEN xdt.kd_component in ('38') THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
								left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
								inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									tc.kd_component, 
									Sum(Qty) as Qty, 
									max(tc.tarif) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut 
								And dtd.Tgl_Transaksi=dt.Tgl_Transaksi
								INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi
										AND dt.kd_kasir = tc.kd_kasir
										AND dt.tgl_transaksi = tc.tgl_transaksi
										AND dt.urut = tc.urut
								INNER JOIN detail_tr_bayar dtb ON dt.Kd_Kasir = dtb.Kd_kasir
														AND dt.No_Transaksi = dtb.no_Transaksi
														AND dt.Tgl_Transaksi = dtb.tgl_Transaksi
														AND dt.Urut = dtb.Urut
								Where 
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." ".$criteriaShift." 										
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, tc.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						where d.kd_dokter='".$line->kd_dokter."' ".$crtiteriaAsalPasien_Detail."  AND t.kd_unit in(".$kd_unit.") ".$criteria_profesi."
						".$ckd_customer_far."
						Group By k.kd_pasien,Nama, Deskripsi,
								kd_unit_asal, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama,x.kd_pasien, x.Deskripsi,
						x.kd_unit_asal	
				 Order By x.Nama, x.Deskripsi"
				);
				/* $queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(case when xdt.KD_COMPONENT in ('20') THEN max(xdt.Tarif ) else 0 end) as JPDok, 
						(CASE WHEN xdt.kd_component in ('38') THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut 
								And dtd.Tgl_Transaksi=dt.Tgl_Transaksi
								INNER JOIN detail_tr_bayar dtb ON dt.Kd_Kasir = dtb.Kd_kasir
								AND dt.No_Transaksi = dtb.no_Transaksi
								AND dt.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dt.Urut = dtb.Urut
								INNER JOIN detail_tr_bayar_component dtbc ON dtbc.Kd_Kasir = dtb.Kd_kasir
								AND dtbc.No_Transaksi = dtb.no_Transaksi
								AND dtbc.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dtbc.Urut = dtb.Urut
								Where 
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." ".$criteriaShift." 										
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						where d.kd_dokter='".$line->kd_dokter."' ".$crtiteriaAsalPasien."  AND t.kd_unit in(".$kd_unit.") ".$criteria_profesi."
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				); */
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pph = 0;
				$total_all = 0;
				$total_tindakan = 0;
				$total_pph_tindakan = 0;
				$total_tindakan_net = 0;

				foreach ($query2 as $line2) 
				{
				
					$noo++;
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jumlah_as;
					$total_jumlah += $line2->jumlah_as*$line2->jml_pasien;
					$total_pph += ((($line2->jumlah_as*$line2->jml_pasien)/100)*$opstind);
					$total_tindakan += $line2->total_as-((($line2->jumlah_as*$line2->jml_pasien)/100)*$opstind);
					$total_pph_tindakan += ($line2->jumlah_as*$line2->jml_pasien) * $pphnya;
					$total_tindakan_net += ($line2->jumlah_as*$line2->jml_pasien)-(($line2->jumlah_as*$line2->jml_pasien)*$pphnya);
					/* $total_pph += (($line2->jumlah_as)*$opstind);
					$total_tindakan += $line2->total_as-(($line2->jumlah_as)*$opstind);
					$total_pph_tindakan += (( ($line2->total_as-(($line2->jumlah_as)*$opstind)) )*$pphnya);
					$total_tindakan_net += ( $line2->total_as-(($line2->jumlah_as)*$opstind))-(( ($line2->total_as-(($line2->jumlah_as)*$opstind)) )*$pphnya); */
					/* $html.="<td style='padding-left:10px;'>".$noo." - ".$line2->deskripsi."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jumlah_as)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah((($line2->jumlah_as/100)*$opstind))."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->total_as-(($line2->jumlah_as/100)*$opstind))."</td>"; */
				
				}

				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pph += $total_pph;
				$all_total_pph_tindakan += $total_jumlah*$pphnya;
				$all_total_tindakan_net += $total_tindakan_net;
				$all_total_tindakan += $total_tindakan;

				$html.="<tr>";
				$html.='<td style="padding-left:10px;">'.$no.'</td>';
				$html.='<td align="left" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>';
				//$html.="<td style='padding-right:10px;' align='right'><b>".$total_jml_pasien."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jumlah)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph)."</b></td>";
				//$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="</tr>";
				$jmllinearea += 1;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pph = 0;
				$total_tindakan = 0;
				$total_pph_tindakan = 0;
				$total_tindakan_net =0;
			}
			$html.="<tr>";
			$html.="<td></td>";
			$html.="<td align='right' style='padding-right:10px;'><b>TOTAL</b></td>";
			//$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_jml_pasien."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jumlah)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pph)."</b></td>";
			//$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pph_tindakan)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan_net)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan_net)."</b></td>";
			$html.="</tr>";
			$jmllinearea = $jmllinearea+1;
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';	
			$jmllinearea=count($query)+5;			
		} 
		$jmllinearea = $jmllinearea+1;
		$html.='</table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			
			$name='lap_pasien_detail.xls';
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:K'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('C1:J100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:J2')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C7:K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('C:D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setWrapText(true); */
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'Z'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('rekap_jasa_dokter'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=lap_rekap_jasa_dokter_rm.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		
		}else{
			$this->common->setPdf('L','Lap.Rekap Jasa Pelayanan Dokter',$html);	
		}
		echo $html;
   	}
	
	
	public function laporan_penerimaanperpasien(){
		$common=$this->common;
		$param=json_decode($_POST['data']);	
		
   		$title='LAPORAN PENERIMAAN PER PASIEN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		/* if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{ */
		if($kel_pas_t== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas_t == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas_t == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas_t == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Asuransi';
		}
		//}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		// print_r($kd_unit);
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and left(tr.kd_unit,1) in ('2')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."' and left(tr.kd_unit,1) in ('1')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."' and left(tr.kd_unit,1) in ('3')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
		}
		
		/* if($kd_unit == "'44'"){
			# "Laboratorium PA Pav.";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24','31')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24')  and t.kd_pasien not like 'LB%'";
				$nama_asal_pasien = 'RWJ PAV';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('31')";
				$nama_asal_pasien = 'RWI PAV';
			} else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('24') and t.kd_pasien like 'LB%'";
				$nama_asal_pasien = 'Langsung';
			}
		} else if($kd_unit == "'45'"){
			# "Laboratorium PA";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25')";
				$nama_asal_pasien = 'RWJ';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('26')";
				$nama_asal_pasien = 'RWI';
			}else if($asal_pasien == 3){
				$crtiteriaAsalPasien="and t.kd_kasir in('27')";
				$nama_asal_pasien = 'IGD';
			}else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('25')";
				$nama_asal_pasien = 'Langsung';
			}
		}  */
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer  
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												$crtiteriaAsalPasien
												$customerx
												and unit.kd_bagian=74 
												and t.kd_unit=$kd_unit  				
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Trans</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
			$html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		
		if($type_file == true){
			$name = "Laporan Rekapitulasi Penerimaan Rehab Medik_".date('d/M/Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;
		}else{
			$common = $this->common;
		$this->common->setPdf('P','LAPORAN PENERIMAAN PER PASIEN',$html);	
		}
		//$html.='</table>';
   	}
	
	public function laporan_penerimaan_perjenispenerimaan(){
		$common=$this->common;
		$param=json_decode($_POST['data']);	
		
   		//$title='Laporan Rekapitulasi Penerimaan Laboratorium';
		$title='LAPORAN PENERIMAAN PER JENIS PENERIMAAN';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = -1;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas_t== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else if($kel_pas_t == 0){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perseorangan';
		}else if($kel_pas_t == 1){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Perusahaan';
		}else if($kel_pas_t == 2){
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas_t." ";
			$tipe= 'Asuransi';
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and left(tr.kd_unit,1) in ('2')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."' and left(tr.kd_unit,1) in ('1')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."' and left(tr.kd_unit,1) in ('3')";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtc.kd_kasir in ('".$KdKasirRwj."')";
		}
		/* if($kd_unit == "'44'"){
			# "Laboratorium PA Pav.";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24','31')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24')  and t.kd_pasien not like 'LB%'";
				$nama_asal_pasien = 'RWJ PAV';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('31')";
				$nama_asal_pasien = 'RWI PAV';
			} else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('24') and t.kd_pasien like 'LB%'";
				$nama_asal_pasien = 'Langsung';
			}
		} else if($kd_unit == "'45'"){
			# "Laboratorium PA";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25')  and t.kd_pasien not like 'LB%'";
				$nama_asal_pasien = 'RWJ';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('26')";
				$nama_asal_pasien = 'RWI';
			}else if($asal_pasien == 3){
				$crtiteriaAsalPasien="and t.kd_kasir in('27')";
				$nama_asal_pasien = 'IGD';
			}else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('25') and t.kd_pasien like 'LB%'";
				$nama_asal_pasien = 'Langsung';
			}
		}  */
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select py.Uraian, 
										case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
										case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
										case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
											From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
																INNER JOIN unit on unit.kd_unit=t.kd_unit  
																INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
										Where 
										$q_shift
										$kriteria_bayar
										$crtiteriaAsalPasien
										$customerx
										and t.kd_unit=$kd_unit  
										and unit.kd_bagian=74  
										Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result();
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th>'.$t_shift.'</th>
					</tr>
					<tr>
						<th>Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumah Subsidi</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->ssd,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_ssd = $total_ssd + $line->ssd;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','LAPORAN PENERIMAAN PER JENIS PENERIMAAN',$html);	
		//$html.='</table>';
		
   	}
	
	function laporan_penerimaan_tunai_perkomponen_det(){
   		$common=$this->common;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN DETAIL';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		//$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}else{
			$customerx="";
			//$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and left(tr.kd_unit,1) in ('2')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."' and left(tr.kd_unit,1) in ('1')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."' and left(tr.kd_unit,1) in ('3')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											AND t.kd_Unit in (".$kd_unit.") AND $q_shift
											Group by dc.kd_Component  Order by kd_Component ")->result();
		$arr_kd_component = array(); //array menampung kd_component
		$q_select_komponen_dinamis_1 = '';
		$q_select_komponen_dinamis_2 = '';
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$q_select_komponen_dinamis_1 .= 'sum(c'.$line->kd_component.')'.'as c'.$line->kd_component.',';
				$q_select_komponen_dinamis_2 .= 'SUM(CASE WHEN dtc.kd_component ='.$line->kd_component.' THEN dtc.Jumlah ELSE 0 END) '.'as c'.$line->kd_component.',';
				$i++;
			}
			$q_select_komponen_dinamis_1 = substr($q_select_komponen_dinamis_1,0,-1);
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
			$q_select_komponen_dinamis_1 = "0 as none";
		}
		
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  
								COALESCE(SUM(x.Jumlah),0) as JUMLAH ,
								$q_select_komponen_dinamis_1

				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						$q_select_komponen_dinamis_2  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						WHERE    
															$crtiteriakodekasir  And						
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	
															$kriteria_bayar
															and dtb.jumlah <> 0	
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					 ".$crtiteriaAsalPasien." 
					 $customerx
					  and unit.kd_bagian=74 
				GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
				order by Ps.kd_pasien
                "
		);
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query_array = $queryHead->result_array();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		/* $html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>'; */
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>';
			foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
		$html.='
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$all_total_= array();
			foreach($query_kolom as $line){
				$all_total_[$line->kd_component] = 0;
			}
			# distinct unit
			$arr_unitlab = array();
			for($i=0;$i<count($query);$i++){
				$arr_unitlab ['unit'][$i] = $query[$i]->nama_unit;
			}
			$arr_unitlab = array_unique($arr_unitlab['unit']);
			
			# distinct pasien
			$arr_kdpasien = array();
			$arr_namapasien = array();
			for($i=0;$i<count($query);$i++){
				$arr_kdpasien ['kd_pasien'][$i] = $query[$i]->kd_pasien;
				$arr_namapasien ['nama'][$i] = $query[$i]->nama;
			}
			$arr_kdpasien = array_unique($arr_kdpasien['kd_pasien']);
			$arr_namapasien = array_unique($arr_namapasien['nama']);
			
			$no=0;
			for($i=0;$i<count($arr_unitlab);$i++){
				$no++;
				$colspan=count($query_kolom)+2;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="'.$colspan.'" align="left">'.$arr_unitlab[$i].'</th>
				</tr>
				';
				$nama='';
				$no2=0;
				for($j=0;$j<count($query);$j++){
					if($arr_unitlab[$i] == $query[$j]->nama_unit && $nama != $query[$j]->nama){
						$no2++;
						$nama=$query[$j]->nama;
						$html.='
						<tr class="headerrow"> 
							<th width="">&nbsp;</th>
							<th width="" colspan="'.$colspan.'" align="left">'.$no2.'. '.$query[$j]->kd_pasien.' ' .'-'. ' '.$query[$j]->nama.'</th>
						</tr>
						';
						$tindakan = '';
						for($k=0;$k<count($query);$k++){
							if($query[$j]->nama_unit == $query[$k]->nama_unit && $nama == $query[$k]->nama && $tindakan != $query[$k]->deskripsi){
								
								/* $all_total_jasa_dok += $query[$k]->c20;
								$all_total_jasa_perawat += $query[$k]->c21;
								$all_total_indeks_tdk_langsung += $query[$k]->c22;
								$all_total_ops_instalasi +=$query[$k]->c23;
								$all_total_ops_rs += $query[$k]->c24;
								$all_total_ops_direksi += $query[$k]->c25;
								$all_total_jasa_sarana += $query[$k]->c30;
								
								 */
								 //var_dump($query_array[$k]);
								$html.='
									<tr> 
										<td width="">&nbsp;</th>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$k]->deskripsi.' </td>';
								$all_total_jumlah += $query[$k]->jumlah;
								$xx=0;
								foreach($query_kolom as $line){
									$all_total_[$line->kd_component] += $query_array[$k]['c'.$line->kd_component];
									$html.='
										<td width="" align="right">'.$this->rupiah($query_array[$k]['c'.$line->kd_component]).' </td>';
									$xx++;
								}  
								$html.='
										<td width="" align="right">'.$this->rupiah($query[$k]->jumlah).' </td>
										
										</tr>
									'; 	
								$tindakan = $query[$k]->deskripsi;
								
							}
							
						}
					}
					
				}
			}
			$html.='
				<tr> 
					<th width="" colspan="2">&nbsp;</th>';
			foreach($query_kolom as $line){
				
				$html.='
					<th width="" align="right">'.$this->rupiah($all_total_[$line->kd_component]).' </th>';
				
			} 
			$html.='
			<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
			</tr>
					';
				
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1 || count($query)>700){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Det_'.date('d-M-Y').'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		 }else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Detail',$html);	
		} 
		//echo $html;
   	}
	
	public function laporan_penerimaan_tunai_perkomponen_sum(){
   		$common=$this->common;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN SUMMARY';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		//$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}else{
			$customerx="";
			//$customer='Semua ';
		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'7";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and left(tr.kd_unit,1) in ('2')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."' and left(tr.kd_unit,1) in ('1')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."' and left(tr.kd_unit,1) in ('3')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi
											LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  											
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											AND t.kd_Unit in (".$kd_unit.") AND $q_shift
											Group by dc.kd_Component  Order by kd_Component ")->result();
		$arr_kd_component = array(); //array menampung kd_component
		$q_select_komponen_dinamis_1 = '';
		$q_select_komponen_dinamis_2 = '';
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$q_select_komponen_dinamis_1 .= 'sum(c'.$line->kd_component.')'.'as c'.$line->kd_component.',';
				$q_select_komponen_dinamis_2 .= 'SUM(CASE WHEN dtc.kd_component ='.$line->kd_component.' THEN dtc.Jumlah ELSE 0 END) '.'as c'.$line->kd_component.',';
				$i++;
			}
			$q_select_komponen_dinamis_1 = substr($q_select_komponen_dinamis_1,0,-1);
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
		}
		
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit, count(Ps.Kd_Pasien) as jml_pasien,SUM(dt.Qty)AS Qty, p.Deskripsi,  
								COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
								$q_select_komponen_dinamis_1

				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						$q_select_komponen_dinamis_2  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						WHERE    
															$crtiteriakodekasir  And						
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	
															$kriteria_bayar
															and dtb.jumlah <> 0	
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien 
				inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  	  
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					 ".$crtiteriaAsalPasien." 
					 $customerx
					  and unit.kd_bagian=74 
				GROUP BY U.Nama_Unit,  p.Deskripsi
                "
		);
		
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query_array = $queryHead->result_array();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		/* $html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>'; */
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="5%" align="center">Jumlah pasien</th>
					<th width="5%" align="center">Jumlah Tindakan</th>';
			foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
		$html.='
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$all_total_= array();
			foreach($query_kolom as $line){
				$all_total_[$line->kd_component] = 0;
			}
			# distinct unit
			$arr_unitlab = array();
			for($i=0;$i<count($query);$i++){
				$arr_unitlab ['unit'][$i] = $query[$i]->nama_unit;
			}
			$arr_unitlab = array_unique($arr_unitlab['unit']);
			
			# distinct pasien
			$arr_kdpasien = array();
			$arr_namapasien = array();
			for($i=0;$i<count($query);$i++){
				//$arr_kdpasien ['kd_pasien'][$i] = $query[$i]->kd_pasien;
				//$arr_namapasien ['nama'][$i] = $query[$i]->nama;
			}
			//$arr_kdpasien = array_unique($arr_kdpasien['kd_pasien']);
			//$arr_namapasien = array_unique($arr_namapasien['nama']);
			$nama_unit = '';
			$no=0;
			for($i=0;$i<count($arr_unitlab);$i++){
				$no++;
				$colspan=count($query_kolom)+2;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="'.$colspan.'" align="left">'.$arr_unitlab[$i].'</th>
				</tr>
				';
				$nama='';
				$no2=0;
				for($j=0;$j<count($query);$j++){
					if($arr_unitlab[$i] == $query[$j]->nama_unit){
						$no2++;
						
						$tindakan = '';
						//for($k=0;$k<count($query);$k++){
							if($query[$j]->nama_unit == $nama_unit || $tindakan != $query[$j]->deskripsi){
								
								$html.='
									<tr> 
										<td width="">&nbsp;</th>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$j]->deskripsi.' </td>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$j]->jml_pasien.' </td>
										<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$query[$j]->qty.' </td>';
								$all_total_jumlah += $query[$j]->jumlah;
								$xx=0;
								foreach($query_kolom as $line){
									$all_total_[$line->kd_component] += $query_array[$j]['c'.$line->kd_component];
									$html.='
										<td width="" align="right">'.$this->rupiah($query_array[$j]['c'.$line->kd_component]).' </td>';
									$xx++;
								}  
								$html.='
										<td width="" align="right">'.$this->rupiah($query[$j]->jumlah).' </td>
										
										</tr>
									'; 	 
								$tindakan = $query[$j]->deskripsi;
								$nama_unit = $query[$j]->nama_unit;
							}
							
						//}
					}
					
				}
			}
			$html.='
				<tr> 
					<th width="" colspan="4">&nbsp;</th>';
			foreach($query_kolom as $line){
				
				$html.='
					<th width="" align="right">'.$this->rupiah($all_total_[$line->kd_component]).' </th>';
				
			} 
			$html.='
			<th width="" align="right">'.$this->rupiah($all_total_jumlah).' </th>
			</tr>
					';
				
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Det_'.date('d-M-Y').'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		 }else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Summary',$html);	
		} 
		//echo $html;
   	}
}

?>