<?php
/**

 * @author Ali
 * Editing by MSD
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupRM extends  MX_Controller {

	public $ErrLoginMsg='';
	private $dbSQL         = "";
	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		
		//$this->dbSQL    = $this->load->database('otherdb2',TRUE);
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function simpanSetupDokter(){
		$KdSetupDokter = $_POST['KdSetupDokter'];
		$NamaSetupDokter = $_POST['NamaSetupDokter'];
		$Spesialisasi = $_POST['Spesialisasi'];
		$kd_unit = $_POST['kd_unit'];
		$cekdulu=$this->db->query("select * from dokter_penunjang where kd_dokter='".$KdSetupDokter."' and kd_unit='".$kd_unit."'")->result();
		if (count($cekdulu)==0)
		{
			$save=$this->db->query("insert into dokter_penunjang values('".$KdSetupDokter."','".$kd_unit."','".$Spesialisasi."')");
		}else
		{
			$save=$this->db->query("update dokter_penunjang set spesialisasi='".$Spesialisasi."' where kd_dokter='".$KdSetupDokter."' and kd_unit='".$kd_unit."'");
		}
		
		if($save){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function hapusSetupDokter(){
		$KdSetupDokter =$_POST['KdSetupDokter'];
		$kd_unit = $_POST['kd_unit'];
		$query = $this->db->query("DELETE FROM dokter_penunjang WHERE kd_dokter='$KdSetupDokter' and kd_unit='$kd_unit' ");
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function getDokter(){
		$DokterPenunjang=$_POST['text'];
		if($DokterPenunjang == ''){
			$criteria="";
		} else{
			$criteria=" where nama like upper('".$DokterPenunjang."%')";
		}
		$result=$this->db->query("select * from dokter  $criteria ORDER BY kd_dokter
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getDokterPenunjang(){
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kata_kd_unit='';
		if (strpos($kd_unit,","))
		{
			$pisah_kata=explode(",",$kd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kata_kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kata_kd_unit= $kd_unit;
		}
		//$kata_kd_unit = stristr($kd_unit,"'4");
		$nama_unit=$this->db->query("select nama_unit from unit where kd_unit=".$kata_kd_unit."")->row()->nama_unit;
		$criteria="where kd_unit in(".$kata_kd_unit.") ";
		
		$result=$this->db->query("select distinct d.kd_dokter,nama,jenis_dokter,spesialisasi from dokter d inner join dokter_penunjang dp on dp.kd_dokter=d.kd_dokter  $criteria ORDER BY d.kd_dokter
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).', kd_unit:'.$kata_kd_unit.', nama_unit:"'.$nama_unit.'"}';
	}
}

?>