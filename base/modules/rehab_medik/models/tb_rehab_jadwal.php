<?php

class tb_rehab_jadwal extends TblBase
{
    function __construct()
    {
        $this->TblName='rehab_jadwal';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewrehab_jadwal;

          $row->KD_KASIR=$rec->kd_kasir;
          $row->TGL_JADWAL=$rec->tgl_jadwal;
          $row->NO_TRANSAKSI=$rec->no_transaksi;
          $row->NO_REG=$rec->no_reg;
          $row->KD_PASIEN=$rec->kd_pasien;
          $row->KD_UNIT=$rec->kd_unit;
          $row->TGL_MASUK=$rec->tgl_masuk;
          $row->URUT_MASUK=$rec->urut_masuk;

          return $row;
    }

}

class Rowviewrehab_jadwal
{
    public $KD_KASIR;
    public $TGL_JADWAL;
    public $NO_TRANSAKSI;
    public $NO_REG;
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
}

?>
