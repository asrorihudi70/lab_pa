<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class control_cmb_kelas extends MX_Controller {

	public function __construct(){
		//parent::Controller();
		parent::__construct();
	}

	public function index(){
		$this->load->view('main/index');
	}


	function read($Params=null)
	{
		$query = array();
		try
		{
			$criteria 	= "";

			if (strlen($Params[4]) !== 0) {
				$criteria = " AND ".str_replace("~", "'", $Params[4]);
			}

			$_query 		= "
				SELECT 
					DISTINCT(kls_bpjs),
					CASE WHEN kls_bpjs = 'KL1' THEN 'Kelas 1' ELSE
						CASE WHEN kls_bpjs = 'KL2' THEN 'Kelas 2' ELSE 
							CASE WHEN kls_bpjs = 'KL3' THEN 'Kelas 3' ELSE 
							kls_bpjs 
							END 
						END 
					END as kelas 
				FROM kamar 
				WHERE 
					kls_bpjs is not null 
					AND kls_bpjs <> '' 
					$criteria
				ORDER BY kls_bpjs ASC
			";
			// $this->load->model('Siranap/tb_unit_apotek');
			
			// if (strlen($Params[4]) !== 0) {
			// 	$this->db->where(str_replace("~", "'", $Params[4] . " order by nm_unit_far ASC offset " . $Params[0] . "  limit 500 "), null, false);
			// }
			$query = $this->db->query($_query);
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';
		}

		if ($query->num_rows() > 0) {
			$data = array();
			array_push($data, 
				array(
					'kls_bpjs' 	=> 'Semua',
					'kelas' 	=> 'Semua',
				)
			);
			foreach ($query->result() as $result) {
				array_push($data, array(
						'kls_bpjs' 	=> $result->kls_bpjs,
						'kelas' 	=> $result->kelas,
					)
				);
			}
			echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($data).'}';
		}else{
			echo '{success:true, totalrecords:0, ListDataObj:'.json_encode($query).'}';
		}
	 	//echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->NM_UNIT_FAR=$rec->nm_unit_far;

		return $row;
	}
}
?>