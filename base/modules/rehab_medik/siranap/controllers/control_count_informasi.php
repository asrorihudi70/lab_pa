<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class control_count_informasi extends MX_Controller {

	public function __construct(){
		//parent::Controller();
		parent::__construct();
	}

	public function index(){
		$this->load->view('main/index');
	}


	function read($Params=null)
	{
		$query = array();
		try
		{
			$criteria 	= "";
			$_query 		= "
				SELECT 
					'Tempat tidur tersedia' as informasi,
					SUM(jumlah_bed) as nilai,
					'success' as status 
				FROM kamar
				UNION
				SELECT 
					'Tempat tidur terpakai' as informasi,
					SUM(digunakan) as nilai,
					'danger' as status 
				FROM kamar
				UNION
				SELECT 
					'Total tidur yang tersedia' as informasi,
					SUM(jumlah_bed + digunakan) as nilai,
					'warning' as status 
				FROM kamar
			";
			// $this->load->model('Siranap/tb_unit_apotek');
			
			// if (strlen($Params[4]) !== 0) {
			// 	$this->db->where(str_replace("~", "'", $Params[4] . " order by nm_unit_far ASC offset " . $Params[0] . "  limit 500 "), null, false);
			// }
			$query = $this->db->query($_query);
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';
		}
		if ($query->num_rows() > 0) {
			echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($query->result()).'}';
		}else{
			echo '{success:true, totalrecords:0, ListDataObj:'.json_encode($query).'}';
		}
	 	//echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->NM_UNIT_FAR=$rec->nm_unit_far;

		return $row;
	}
}
?>