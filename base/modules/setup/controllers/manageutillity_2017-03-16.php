<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class manageutillity extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->library('session');
        $this->load->model('setup/tbl_manageutillity');
    }

    public function konfigurasiuser(){
		if(empty($this->input->post("VarIDObat")) == true || ($this->input->post("VarIDObat") == '') ){
			$milik 	= 0;
		}else{
			$milik 	= $this->input->post("VarIDObat");
		}
		$tracer='0';
		if($this->input->post("VarP_Tracer")=='true'){
			$tracer='1';
		}
		$autoPaid='0';
		if($this->input->post("VarAutoPaid")=='true'){
			$autoPaid='1';
		}
		$list=$this->input->post("list");
		$kd_unit='';
		for($i=0,$iLen=count($list); $i<$iLen;$i++){
			if($kd_unit!=''){
				$kd_unit.=',';
			}
			$kd_unit.="'".$list[$i]."'";
		}
		$kd_user        = $this->session->userdata['user_id']['id'];
		$data_unit		= $this->tbl_manageutillity->getData('zusers', 'kd_user', $kd_user);
		
		/*ambil kd_unit sebelumnya */
		
		foreach ($data_unit->result_array() as $line){
			$unit = $line['kd_unit'];
		}
		$kd_unitA=$unit.",".$kd_unit;
		
		/* menghilangkan duplikasi kd_unit */
		$pecah_unit = explode(",", $kd_unitA);
		$kd_unit_uniq = array_unique($pecah_unit);
		$kd_unitB = implode(",", $kd_unit_uniq);
		
    	$data = array(
            "kd_kelurahan" =>   $this->input->post("VarIDKelurahan"),
            "kd_customer"  =>   $this->input->post("VarIDCustomer"),
            "kd_unit_far"  =>   $this->input->post("VarIDApotek"),
            "kd_milik"     =>   $milik,
            //"kd_unit"      =>   $this->input->post("VarKDUnit"),
            "kd_unit"      =>   $kd_unitB,
            "auto_paid"    =>   $autoPaid,
            "kd_klas_lab"  =>   $this->input->post("VarUnitKlasLab"),
            "kd_klas_rad"  =>   $this->input->post("VarUnitKlasRad"),
            "kd_klas_ok"   =>   $this->input->post("VarUnitKlasOK"),
            "p_bill"       =>   $this->input->post("VarP_Bill"),
            "p_kwitansi"   =>   $this->input->post("VarP_Kwitansi"),
            "p_kasir"      =>   $this->input->post("VarP_Kasir"),
            "p_label"      =>   $this->input->post("VarP_Label"),
            "p_gelang"     =>   $this->input->post("VarP_Gelang"),
            "p_statuspasien"     =>   $this->input->post("VarP_StatusPasien"),
            "p_kartupasien"     =>   $this->input->post("VarP_KartuPasien"),
            "p_sep"     =>   $this->input->post("VarP_SEP"),
            "auto_print_tracer"     =>   $tracer,
    	);
		
        $data['result']     = $this->tbl_manageutillity->set_config($data, $kd_user);
        echo json_encode($data);
    }

    public function getDataSetting(){
        $kd_user            = $this->session->userdata['user_id']['id'];
        $data               = array();
        $query              = $this->tbl_manageutillity->getDataSetting($kd_user);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $result) {
                $data['kd_kecamatan']   = $this->getDataParent("kelurahan", "kd_kelurahan", $result['kd_kelurahan'],'kd_kecamatan');
                $data['kd_kabupaten']   = $this->getDataParent("kecamatan", "kd_kecamatan", $data['kd_kecamatan'], 'kd_kabupaten');
                $data['kd_propinsi']    = $this->getDataParent("kabupaten", "kd_kabupaten", $data['kd_kabupaten'], 'kd_propinsi');

                $data['kecamatan']      = $this->getDataParent("kecamatan", "kd_kecamatan", $data['kd_kecamatan'],'kecamatan');
                $data['kabupaten']      = $this->getDataParent("kabupaten", "kd_kabupaten", $data['kd_kabupaten'],'kabupaten');
                $data['kelurahan']      = $this->getDataParent("kelurahan", "kd_kelurahan", $result['kd_kelurahan'],'kelurahan');
                $data['nama_unit']      = $this->getDataParent("unit", "kd_unit", $result['kd_unit'], "nama_unit");
                $data['kd_parent_unit'] = $this->getDataParent("unit", "kd_unit", $result['kd_unit'], "parent");
                $data['kd_kelurahan']   = $result['kd_kelurahan'];


				$data['customer']       = $this->getDataCustomer($result['kd_customer'], 'customer');
                $data['kd_customer']    = $result['kd_customer'];
                $data['jenis_cust']     = $this->getDataParent("kontraktor", "kd_customer", $result['kd_customer'], "jenis_cust");
                $data['kd_unit_far']    = $result['kd_unit_far'];
                $data['kd_child_unit']  = $result['kd_unit'];
                $data['p_tracer']  = $result['auto_print_tracer'];
                $data['kd_bagian']      = substr($result['kd_unit'], 0, 1);
				if($result['kd_milik'] == 0){
					$data['kd_milik']       = "";
				}else{
					$data['kd_milik']       = $result['kd_milik'];
				}
                $data['auto_paid']      = $result['auto_paid'];
                $data['p_bill']         = $result['p_bill'];
                $data['p_kwitansi']     = $result['p_kwitansi'];
                $data['p_kasir']        = $result['p_kasir'];
                $data['p_label']        = $result['p_label'];
                $data['p_gelang']       = $result['p_gelang'];
                $data['p_statuspasien'] = $result['p_statuspasien'];
                $data['p_kartupasien']  = $result['p_kartupasien'];
                $data['p_sep'] 			= $result['p_sep'];

                $data['kd_klas_lab']    = $result['kd_klas_lab'];
                if ($data['kd_klas_lab'] != null || $data['kd_klas_lab'] != "") {
                    $data['klasifikasi_lab']= $this->getDataParent("klas_produk", "kd_klas", $data['kd_klas_lab'], "klasifikasi");
                }
                $data['kd_klas_rad']    = $result['kd_klas_rad'];
                if ($data['kd_klas_rad'] != null || $data['kd_klas_rad'] != "") {
                    $data['klasifikasi_rad']= $this->getDataParent("klas_produk", "kd_klas", $data['kd_klas_rad'], "klasifikasi");
                }
                $data['kd_klas_ok']    = $result['kd_klas_ok'];
                if ($data['kd_klas_ok'] != null || $data['kd_klas_ok'] != "") {
                    $data['klasifikasi_ok']= $this->getDataParent("klas_produk", "kd_klas", $data['kd_klas_ok'], "klasifikasi");
                }
            }
        }
        echo json_encode($data);
    }

    private function getData($table, $field, $kode){
        //Table = Target tabel yang dituju
        //Field = Target field yang akan dibandingkan
        //Kode  = Adalah nilai Value untuk pembanding field yang akan dituju

        $query     = $this->tbl_manageutillity->getData($table, $field, $kode);
        if ($query->num_rows() > 0) {
            return $query->row()->$field;
        }else{
            return "";
        }
    }
	
    private function getDataParent($table, $field, $kode, $parent){
        //Table = Target tabel yang dituju
        //Field = Target field yang akan dibandingkan
        //Kode  = Adalah nilai Value untuk pembanding field yang akan dituju
		$query=null;
		if($kode != ''){
			$query     = $this->tbl_manageutillity->getData($table, $field, $kode)->row();
		}
		
        
        if ($query && $kode != '') {
            return $query->$parent;
        }else{
            return "";
        }
    }

    private function getDataCustomer($kode, $field){
        //Kode  = Adalah nilai Value untuk pembanding field yang akan dituju

        $query     = $this->tbl_manageutillity->getDataCostumer($kode);
        if ($query->num_rows() > 0) {
            return $query->row()->$field;
        }else{
            return "";
        }
    }
}

?>
