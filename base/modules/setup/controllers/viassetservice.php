<?php
class viassetservice extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
       $this->load->view('main/index');
    }

    // read nya di rubah ke direct SQL execute
    public function read($Params=null)
    {

        $sql = $this->GetStrQuery($Params[4]);
        $query = $this->db->query($sql);

        $arrResult=array();

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_object() as $rows)
            {
                $arrResult[] = $this->FillRow($rows);
            }

            echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';

        }
        else echo '{success:false}';

        //echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($query->result()).'}';

    }

    private function FillRow($rec)
    {
        $row=new Rowviassetservice;

        $row->SERVICE_NAME = $rec->service_name;
        $row->CURRENT_METERING = $rec->current_metering;
        $row->TARGET_METERING = $rec->target_metering;
        $row->ASSET_MAINT_ID = $rec->asset_maint_id;
        $row->SERVICE_TYPE = $rec->service_type;
        $row->IS_AKTIF = $rec->is_aktif;
        $row->METERING_ASSUMPTIONS = $rec->metering_assumptions;
        $row->DAYS_ASSUMPTIONS = $rec->days_assumptions;
        $row->FLAG = $rec->flag;
        $row->SERVICE_ID = $rec->service_id;
        $row->CATEGORY_ID = $rec->category_id;
        $row->UNIT_ID = $rec->unit_id;
        $row->LAST_METERING_SERVICE = $rec->last_metering_service;
        $row->TGL_PENDEKATAN = $rec->tgl_pendekatan;
        $row->PATH_LEGEND = $rec->path_legend;
        $row->LAST_METERING_SERVICE = $rec->last_metering_service;
        $row->TYPE_SERVICE_ID = $rec->type_service_id;
    
        return $row;
    }

    private function GetStrQuery($criteria)
    {
        $mSplit = explode("###1###", $criteria, 2);
        $strKdCat = $mSplit[0];
        $strIDAsset = $mSplit[1];

//dateadd('day', round(days_assumptions/metering_assumptions  * interval,0),
        $str = "Select service_name, interval, unit, start_date, current_metering,target_metering, asset_maint_id,
                type_service_id, service_type, is_aktif, metering_assumptions, days_assumptions, flag, 
                service_id, category_id, unit_id, tgl_pendekatan,
                case when type_service_id = '2' then
                case when cast(current_metering as float) > cast(target_metering as float)
                then './Img Asset/merah.png'
                when cast(current_metering as float) = cast(target_metering as float)
                then './Img Asset/kuning.png' else path_legend end else path_legend end as path_legend,
                last_metering_service
            FROM (
            SELECT s.service_name, a.interval, u.unit,  coalesce(ss.start_date, CURRENT_DATE) as start_date,
                case when a.type_service_id = '2' then cast(ss.meter as character varying)
                else cast(datediff('day', cast(coalesce(last_service_date, start_date) as date), CURRENT_DATE)  as character varying) || ' days' end as current_metering,
                case when a.type_service_id = '2' then cast(coalesce(ss.last_metering_service, 0) + a.interval as character varying)
                else cast(days_assumptions/metering_assumptions * interval as character varying) || ' days' end as target_metering,
                ss.asset_maint_id, a.type_service_id, ts.service_type, a.is_aktif, a.metering_assumptions,                 
                a.days_assumptions, a.flag, a.service_id, a.category_id, a.unit_id,
                coalesce(gettglpendekatan(ss.service_id, ss.category_id, '".$strIDAsset."'),
                dateadd('day', cast(round(cast(days_assumptions/metering_assumptions * interval as numeric)) as integer),
                coalesce(last_service_date, start_date))) AS tgl_pendekatan,
                coalesce(getpathlegendsch(ss.service_id, ss.category_id, '".$strIDAsset."',
                coalesce(gettglpendekatan(ss.service_id, ss.category_id, 'D003'),
                dateadd('day', cast(round(cast(days_assumptions/metering_assumptions * interval as numeric)) as integer),
                coalesce(last_service_date, start_date)))),'./Img Asset/hijau.png') AS path_legend,
                case when a.type_service_id = '2' then cast(last_metering_service as character varying) else
                cast(to_char(last_service_date, 'DD/MM/YYYY') as character varying) end as last_metering_service
            FROM  am_service_category a INNER JOIN
                am_service s ON a.service_id = s.service_id INNER JOIN
                am_unit AS u ON u.unit_id = a.unit_id INNER JOIN
                am_type_service ts ON a.type_service_id = ts.type_service_id LEFT JOIN
                ( SELECT a.asset_maint_id ,l.meter, a.category_id, a.service_id, start_date, target_metering,
                last_metering_service ,a.last_service_date FROM am_asset_service a LEFT JOIN
                am_log_metering l ON l.asset_maint_id = a.asset_maint_id
            WHERE a.asset_maint_id='".$strIDAsset."' ) ss
                ON ss.service_id = a.service_id AND a.category_id = ss.category_id
            WHERE a.category_id='".$strKdCat."' ) x ";

//        $str = "Select service_name, internal, unit, start_date, current_metering,target_metering, asset_maint_id,
//                type_service_id, service_type, is_aktif, metering_assumptions, days_assumptions, flag, service_id, category_id,
//                unit_id, tgl_pendekatan,
//                path_legend = case when type_service_id = '2' then
//                case when cast(current_metering as float) > cast(target_metering as float)
//                then './Img Asset/merah.png'
//                when cast(current_metering as float) = cast(target_metering as float)
//                then './Img Asset/kuning.png' else path_legend end else path_legend end, last_metering_service
//            FROM (
//            SELECT s.service_name, a.interval, u.unit,  coalesce(ss.start_date, CURRENT_DATE) as start_date,
//                case when a.type_service_id = '2' then cast(ss.meter as character varying)
//                else cast(datediff('day', coalesce(last_service_date, start_date), CURRENT_DATE) as character varying) || ' days' end as current_metering,
//                target_metering = case when a.type_service_id = '2' then cast(coalesce(ss.last_metering_service, 0) || a.interval as character varying)
//                else cast(days_assumptions/metering_assumptions * interval as character varying) || ' days' end
//                , ss.asset_maint_id, a.type_service_id, ts.service_type, a.is_aktif, a.metering_assumptions, a.days_assumptions, a.flag,
//                a.service_id, a.category_id, a.unit_id,
//                coalesce(gettglpendekatan(ss.service_id, ss.category_id, '".$strIDAsset."'),
//                dateadd('day',round(days_assumptions/metering_assumptions * interval,0),
//                coalesce(last_service_date, start_date))) AS tgl_pendekatan,
//                coalesce(getpathlegendsch(ss.service_id, ss.category_id, '".$strIDAsset."',
//                coalesce(gettglpendekatan(ss.service_id, ss.category_id, 'D003'),
//                dateadd('day',round(days_assumptions/metering_assumptions  * interval,0),
//                coalesce(last_service_date,start_date)))),'./Img Asset/hijau.png') AS path_legend,
//                case when a.type_service_id = '2' then cast(last_metering_service as varying) else
//                cast(to_char(last_service_date, 'DD/MM/YYYY') as character varying) end as last_metering_service
//            FROM  am_service_category a INNER JOIN
//                am_service s ON a.service_id = s.service_id INNER JOIN
//                am_unit AS u ON u.unit_id = a.unit_id INNER JOIN
//                am_type_service ts ON a.type_service_id = ts.type_service_id LEFT JOIN
//                ( SELECT a.asset_maint_id ,l.meter, a.category_id, a.service_id, start_date, target_metering,
//                last_metering_service ,a.last_service_date FROM am_asset_service a LEFT JOIN
//                am_log_metering l ON l.asset_maint_id = a.asset_maint_id
//            WHERE a.asset_maint_id='".$strIDAsset."' ) ss
//                ON ss.service_id = a.service_id AND a.category_id = ss.category_id
//            WHERE a.category_id='".$strKdCat."' ) x ";
        
        return $str;

    }

    public function save($Params=null)
    {
        $mError = $this->SimpanAssetService($Params);

        if ($mError =="")
        {
            echo '{success : true}';
        } else echo '{success : false, pesan : "'.$mError.'"}';
        
    }
    
    private function SimpanAssetService($Params)
    {
        $strError="";
        $arr = $this->GetListDetail($Params);

        if (count($arr)>0)
        {
            foreach ($arr as $dtlRow)
            {
                $this->load->model('cm/tblam_asset_service');
                $criteria ="asset_maint_id = '".$dtlRow["asset_maint_id"]."'
                    and category_id = '".$dtlRow["category_id"]."'
                    and service_id = '".$dtlRow["service_id"]."'";
                
                $this->tblam_asset_service->db->where($criteria, null, false);
                $query = $this->tblam_asset_service->GetRowList( 0, 1, "","", "");
                if ($query[1]==0)
                {
                    $result = $this->tblam_asset_service->Save($dtlRow);
                    if ($result==0)
                        $strError ='error saving';
                } else
                {
                    $result = $this->tblam_asset_service->Update($dtlRow);
                    if ($result==0)
                        $strError ='error saving';
                }

                
            }
        } else $strError = "error saving";

        return $strError;
        
    }

    private function GetListDetail($Params)
    {

        $jml = 8;
        $arrList = $this->splitListDetail($Params["List"], (int)$Params["JmlList"], $jml);

        $arrListField=array();
        $arrListRow=array();

        if (count($arrList)>0)
        {
            foreach ($arrList as $str)
            {
                for ($i=0;$i<$JmlField;$i+=1)
                {
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }

                if (count($arrListField)>0)
                {
                    $arrListRow['asset_maint_id']= $arrListField[0];
                    $arrListRow['category_id']= $arrListField[1];
                    $arrListRow['service_id']= $arrListField[2];
                    $arrListRow['current_metering']= $arrListField[3];
                    $arrListRow['target_metering']= $arrListField[4];
                    $arrListRow['start_date']= $arrListField[5];

                    if ($arrListField[7] == 2)
                    {
                        $arrListRow['last_metering_service']= $arrListField[6];
                    } else
                    {
                        list($tgl,$bln,$thn)= explode('/',$arrListField[6],3);
                        $ctgl= strtotime($tgl.$bln.$thn);

                        $arrListRow['last_service_date']=date("Y-m-d",$ctgl);
                    }                    

                }
            }
        }

        return $arrListRow;

   }

      private function splitListDetail($str, $jmlList, $jmlField)
    {
        $splitList = explode("##[[]]##",$str,$jmlList);

        $arrList=array();

        for ($i=0;$i<$jmlList;$i+=1)
        {
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }

        return $arrList;
    }
}

class Rowviassetservice
{

    public $SERVICE_NAME;
    public $CURRENT_METERING;
    public $TARGET_METERING;
    public $ASSET_MAINT_ID;
    public $SERVICE_TYPE;
    public $IS_AKTIF;
    public $METERING_ASSUMPTIONS;
    public $DAYS_ASSUMPTIONS;
    public $FLAG;
    public $SERVICE_ID;
    public $CATEGORY_ID;
    public $UNIT_ID;
    public $LAST_METERING_SERVICE;
    public $TGL_PENDEKATAN;
    public $PATH_LEGEND;
    public $LAST_METERING_SERVICE;
    public $TYPE_SERVICE_ID;
    
}

?>
