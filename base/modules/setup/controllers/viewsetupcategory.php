<?php


class viewsetupcategory extends MX_Controller    
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('main/index');
    }


    function read($Params=null)
    {
		
        try
        {

            $this->load->model('setup/tblvicategory');
            if (strlen($Params[4])!==0)
            {
               $this->tblvicategory->db->where(str_replace("~", "'", $Params[4] ) ,null, false) ;
            }
            $res = $this->tblvicategory->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);

            echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';

        }
        catch(Exception $o)
        {
                echo 'Debug  fail ';

        }
		
    }
	 
    function save($Params=null)
    {
        $mError = "";
        $IdCat = "";
        $Ref ="";

        $mError = $this->SimpanCategory($Params, $IdCat);

        if ($mError=="")
        {
            $mError = $this->SimpanAddField($Param, $IdCat);

            if ($mError=="")
            {
                echo '{success : true, CatId : "'.$IdCat.'"}';
            } else echo '{success : false, pesan : 0}';

            $mError = $this->SimpanPartCategory($Param, $IdCat);

            if ($mError=="")
            {
                echo '{success : true, CatId : "'.$IdCat.'"}';
            } else echo '{success : false, pesan : 0}';

        } else echo '{success : false}';

    }


    private function SimpanCategory($Params, &$CatID)
    {
        $strError = "";

        $CATEGORY_NAME = $Params["Category"];
        $TYPE = $Params["Type"];

        if ($Params["ParentName"] <> "" and $Params["ParentName"] <> "Choose Parent...")        
            $PARENT = $Params["ParentName"];

        if ($Params["Id"]=="")
        {
            $CATEGORY_ID = $this->GetIdCategorySetup($PARENT);
        } else $CATEGORY_ID = $Params["Id"];

        $CatID = $CATEGORY_ID;

        $data = array("category_id"=>$CATEGORY_ID, "category_name"=>$CATEGORY_NAME,
            "parent"=>$PARENT, "type"=>$TYPE);

        $criteria = " category_id = '".$CATEGORY_ID."'";

        $this->load->model("setup/tblam_category");
        $this->tblam_category->db->where($criteria,null,false);
        $query = $this->tblam_category->GetRowList( 0, 1, "", "",  "");

        if ($query[1]==0)
        {
            $result = $this->tblam_category->Save($data);

            if ($result==0)
                $strError = "ada";

        } else {
            $result = $this->tblam_category->Update($data);
            if ($result==0)
                $strError = "ada";
        }

        return $strError;
    }

    private function SimpanPartCategory($Param, $strIDCat)
    {
        $arr = $this->GetListDetailPart($Param,$strIDCat);
        $strError = "";

        if (count($arr)>0)
        {
            foreach($arr as $dtlRow)
            {
                $xRowCATEGORY_ID = "";
                $xRowROW_ADD = 0;
                $xRow=array();

                if ($xRowCATEGORY_ID=="")
                    $xRowCATEGORY_ID = $dtlRow["category_id"];

                $criteria = "category_id = '".$xRowCATEGORY_ID."' and
                    part_id = '".$dtlRow["part_id"]."'";

                $this->load->model("setup/tblam_part_category");
                $this->tblam_part_category->db->where($criteria, null, false);
                $query = $this->tblam_part_category->GetRowList( 0, 1, "", "",  "");

                if ($query[1]>0)
                {
                    $result = $this->tblam_part_category->Update($dtlRow);
                    if ($result==0)
                        $strError = "ada";
                } else {
                    $result = $this->tblam_part_category->Save($dtlRow);
                    if ($result==0)
                        $strError = "ada";
                }

                $this->CekJumlahPart((int)$Param["JmlListPart"], $xRowCATEGORY_ID, $arr);

            }
        }

        return $strError;

    }

    private function SimpanAddField($Param, $no_kas_keluar)
    {
        $arr = $this->GetListDetailPart($Param, $no_kas_keluar);
        $strError = "";

        if (count($arr)>0)
        {
            foreach($arr as $dtlRow)
            {
                $xRowCATEGORY_ID = "";
                $xRowROW_ADD = 0;
                $xRow=array();

                if ($xRowCATEGORY_ID=="")
                    $xRowCATEGORY_ID = $dtlRow["category_id"];

                if ($xRowROW_ADD==0)
                {
                    $xRowROW_ADD = $this->GetUrutAddField ($dtlRow);
                    $dtlRow["row_add"]=$xRowROW_ADD;
                }

                $criteria = "category_id = '".$xRowCATEGORY_ID."' and
                    row_add = ".$xRowROW_ADD;

                $this->load->model("setup/tblam_add_field");
                $this->tblam_add_field->db->where($criteria, null, false);
                $query = $this->tblam_add_field->GetRowList( 0, 1, "", "",  "");

                if ($query[1]>0)
                {
                    $result = $this->tblam_add_field->Update($dtlRow);
                    if ($result==0)
                        $strError = "ada";
                } else {
                    $result = $this->tblam_add_field->Save($dtlRow);
                    if ($result==0)
                        $strError = "ada";
                }

                $this->CekJumlahDetail((int)$Param["JmlList"], $xRowCATEGORY_ID, $arr);

            }
        }

        return $strError;

    }

    private function CekJumlahPart($jmlRecord, $CatID, $arr)
    {
        $this->load->model('setup/tblam_part_category');
        $this->tblam_part_category->db->where("category_id = '".$CatID."'", null, false);
        $query = $this->tblam_part_category->GetRowList(0, 1000, "", "", "");

        $ArrList = array();
        $numrow = $query[1];

        if ($numrow>0)
        {
            if ($numrow!=$jmlRecord)
            {
                if ($jmlRecord<$numrow)
                {
                    if (cont($arr)>0)
                    {
                        foreach($query[0] as $y)
                        {
                            $mBol = false;

                            foreach($arr as $z)
                            {
                                if ($y->PART_ID==$z['part_id'])
                                {
                                    $mBol = true;
                                    break;
                                }
                            }

                            if ($mBol==false)
                                $ArrList[]=$y;

                        }

                        if (count($ArrList)>0)
                            $this->HapusBarisPartCat($ArrList);
                    }
                }
            }
        }
    }

    private function CekJumlahDetail($jmlRecord, $CatID, $arr)
    {
        $this->load->model('setup/tblam_add_field');
        $this->tblam_add_field->db->where("category_id = '".$CatID."'", null, false);
        $query = $this->tblam_add_field->GetRowList(0, 1000, "", "", "");

        $ArrList = array();
        $numrow = $query[1];

        if ($numrow>0)
        {
            if ($numrow!=$jmlRecord)
            {
                if ($jmlRecord<$numrow)
                {
                    if (cont($arr)>0)
                    {
                        foreach($query[0] as $y)
                        {
                            $mBol = false;

                            foreach($arr as $z)
                            {
                                if ($y->ROW_ADD==$z['row_add'])
                                {
                                    $mBol = true;
                                    break;
                                }
                            }

                            if ($mBol==false)
                                $ArrList[]=$y;

                        }

                        if (count($ArrList)>0)
                            $this->HapusBarisDetail($ArrList);
                    }
                }
            }
        }
    }

    private function HapusBarisDetail($arr)
    {
        $mError = "";

        foreach ($arr as $x)
        {
            $this->load->model("setup/tblam_add_field");
            $criteria = " category_id = '".$x["category_id"]."' and row_add = ".$x["row_add"];
            $this->tblam_add_field->db->where($criteria, null, false);
            $result = $this->tblam_add_field->Delete();
            if ($result==0)
                $mError .="ada";
        }
        return $mError;
    }

    private function HapusBarisPartCat($arr)
    {
        $mError = "";

        foreach ($arr as $x)
        {
            $this->load->model("setup/tblam_part_category");
            $criteria = " category_id = '".$x["category_id"]."' and part_id = '".$x["part_id"]."'";
            $this->tblam_part_category->db->where($criteria, null, false);
            $result = $this->tblam_part_category->Delete();
            if ($result==0)
                $mError .="ada";
        }
        return $mError;
    }

    public function delete($Params=null)
    {
        $Hapus=(int)$Params['Hapus'];
        $CATEGORY_ID = $Params["Id"];

        $result=0;
        $flag=0;
        
        if ($Hapus==1)
        {

            $this->load->model("setup/tblam_add_field");
            $criteria = "category_id = '".$CATEGORY_ID."'";
            $this->tblam_add_field->db->where($criteria,null,false);
            $query = $this->tblam_add_field->GetRowList( 0, 1000, "", "",  "");

            if ($query[1]>0)
            {
                foreach ($query[0] as $q)
                {
                    $this->load->model("setup/tblam_add_field");
                    $criteria2 = $criteria." and row_add = ".$q->ROW_ADD;
                    $this->tblam_add_field->db->where($criteria2, null, false);
                    $result = $this->tblam_result_cm_serv_part->Delete();

                    if ($result>0)
                        $flag+=1;
                }
            }

            if ($query[1]==$flag)
            {
                $this->load->model("setup/tblam_category");
                $this->tblam_category->db->where($criteria, null, false);
                $result = $this->tblam_category->Delete();
                                    
                if ($result>0)
                {
                    echo "{success : true}";
                } else {echo "{success : false}";}

            } 

        } elseif ((int)$Params["Hapus"] == 2) {
            $ROW_ADD = $Params["RowAdd"];

            $this->load->model("setup/tblam_add_field");
            $criteria = "category_id = '".$CATEGORY_ID."' and row_add = ".$ROW_ADD;
            $this->tblam_add_field->db->where($criteria, null, false);
            $result = $this->tblam_add_field->Delete();
            if ($result>0)
            {
                echo "{success : true}";
            } else echo "{success : false}";
            
        } else {
            $PART_ID = $Params["Part_Id"];
            $this->load->model("setup/tblam_part_category");
            $criteria = "category_id = '".$CATEGORY_ID."' and part_id = '".$PART_ID."'";
            $this->tblam_part_category->db->where($criteria, null, false);
            $result = $this->tblam_part_category->Delete();
            if ($result>0)
            {
                echo "{success : true}";
            } else echo "{success : false}";
        }
    }


    private function GetIdCategorySetup($strIDParent)
    {

        if ($strIDParent=="")
        {
            $this->load->model('setup/tblam_category');
            $criteria = "parent is null";
            $this->tblam_category->db->where($criteria,null,false);
            $query = $this->tblam_category->GetRowList(0, 1, "DESC", "category_id", "");
            if ($query[1]==0)
            {
                $result = "01";
            } else {
                $nomor =(int)substr($query[0][0]->CATEGORY_ID, -2) + 1;
                $result = str_pad($nomor,2,"00",STR_PAD_LEFT); ;
            }
            
        } else {

            $this->load->model('setup/tblam_category');
            //$criteria = "parent is null";
            $this->tblam_category->db->where("parent = '".$strIDParent."'", null, false);
            $query = $this->tblam_category->GetRowList(0, 1, "DESC", "category_id", "");
            if ($query[1]==0)
            {
                $result = "01";
            } else {
                $nomor =(int)substr($query[0][0]->CATEGORY_ID, -2) + 1;
                $result = str_pad($nomor,2,"00",STR_PAD_LEFT); ;
            }

        }

    }
    
    private function GetUrutAddField ($x)
    {
        $result=1;
        
        $this->load->model('setup/tblam_add_field');
        $this->tblam_add_field->db->where("category_id = '".$x["category_id"]."'", null, false);
        
        $res = $this->tblam_add_field->GetRowList(0, 1, "DESC", "row_add", "");
        
        if ($res[1]>0)
            $result=$res[0][0]->ROW_ADD+1;

        return $result;
    }

    private function  GetListDetail($Params, $CatId)
    {
        $jml = (int)$Params["JmlField"];
        $arrList = $this->splitListDetail($Params["List"], (int)$Params["JmlList"], $jml);

        $TrDet=array();

        if (count($arrList)>0)
        {
            foreach ($arrList as $str)
            {
                $arrListField=array();
                for ($i=0;$i<$jml;$i++)
                {
                    $splitField = explode("=", $str[$i],2);
                    $arrListField[]=$splitField[1];
                }

                if (count($arrListField)>0)
                {
                    $TrDet["category_id"]= $CatId;
                    if ($arrListField[0]=="" or isset($arrListField[0]))
                    {
                        $TrDet["row_add"]=0;
                    } else $TrDet["row_add"]= $arrListField[0];                    

                    $TrDet["addfield"]= $arrListField[2];
                    $TrDet["type_field"]= $this->GetTypeField($arrListField[4]);
                    $TrDet["lenght"]= $arrListField[5];
                }
            }
        }

        return $TrDet;
    }

    private function GetListPart($Params, $CatId)
    {
        $jml = 2;
        $arrList = $this->splitListDetail($Params["ListPart"], (int)$Params["JmlListPart"], $jml);

        $TrDet=array();

        if (count($arrList)>0)
        {
            foreach ($arrList as $str)
            {
                $arrListField=array();
                for ($i=0;$i<$jml;$i++)
                {
                    $splitField = explode("=", $str[$i],2);
                    $arrListField[]=$splitField[1];                    
                }

                if (count($arrListField)>0)
                {
                    $TrDet["category_id"]= $CatId;
                    $TrDet["part_id"]= $arrListField[0];
                }
            }
        }

        return $TrDet;
    }

    private function splitListDetail($str, $jmlList, $jmlField)
    {
        $splitList = explode("##[[]]##",$str,$jmlList);

        $arrList=array();

        for ($i=0;$i<$jmlList;$i+=1)
        {
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }

        return $arrList;
    }

    private function  GetTypeField($x)
    {

        if ($x == "Text")
        {
            $result=1;
        } elseif ($x=="Numeric") {
            $result =2;
        } else $result =3;

        return $result;
    }

 

}
//VIEWSETUPCATEGORY

?>