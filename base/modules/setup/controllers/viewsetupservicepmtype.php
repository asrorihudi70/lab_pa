<?php

/**
 * @author inull
 * @copyright 2010
 */

class viewsetupservicepmtype extends MX_Controller
{ 
	
        public function __construct()
        {
            //parent::Controller();
            parent::__construct();

        }

	public function index()
	{
            $this->load->view('main/index');
        }

	function read($Params=null)
	{
		
		try
		{
			
			$this->load->model('setup/tblam_type_service');
                           if (strlen($Params[4])!==0)
                        {
                           $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
                        }
			$res = $this->tblam_type_service->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
		
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
		
	 	echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
		
	}

     function save($Params=null)
     {
            $this->load->model('setup/tblam_type_service');
            $Arr['type_service_id']=$Params['Id'];
            $Arr['service_type']=$Params['ServicePMType'];

            if (strlen($Arr['type_service_id'])===0)
            {
                $Arr['type_service_id']=$this->GetNewID();
                $res=$this->tblam_type_service->Save($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);

                if ($res>0)
                {                    
                    echo '{success: true, ServicePMTypeId: "'.$Arr['type_service_id'].'"}';
                } else echo '{success: false}';
            }
            else
            {
                $this->db->where("type_service_id = '".$Arr['type_service_id']."'", null, false);
                $res=$this->tblam_type_service->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);

                if ($res>0)
                {
                    echo '{success: true}';
                } else echo '{success: false}';

            }

//                if ($res !==0)
//                {
//                    echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
//                }
     }


     function GetNewID()
     {
         $res = $this->tblam_type_service->GetRowList( 0 , 1, 'DESC', 'type_service_id', '');
         if ($res[1]===0)
         {
             return 1;
         }
         else
         {
             return $res[0][0]->TYPE_SERVICE_ID+1;
         }
     }

     function delete( $param)
     {

        $this->load->model('setup/tblam_type_service');
        $Sql=$this->db;
        $Sql->where("type_service_id = '".$param['Id']."'", null, false);
        $res=$this->tblam_type_service->Delete(); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
        //return $res;

        if ($res>0)
        {
            echo '{success: true}';
        } else echo '{success: false}';
     }


}
//VIEWSETUPSERVICEPMTYPE

?>