<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_type_service extends Model
{

	function am_type_service()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('TYPE_SERVICE_ID', $data['TYPE_SERVICE_ID']);
		$this->db->set('SERVICE_TYPE', $data['SERVICE_TYPE']);
		$this->db->insert('am_type_service');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('TYPE_SERVICE_ID', $id);
		$query = $this->db->get('am_type_service');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('am_type_service');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('TYPE_SERVICE_ID', $data['TYPE_SERVICE_ID']);
		$this->db->set('SERVICE_TYPE', $data['SERVICE_TYPE']);
		$this->db->update('am_type_service');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('TYPE_SERVICE_ID', $id);
		$this->db->delete('am_type_service');

		return $this->db->affected_rows();
	}

}



?>