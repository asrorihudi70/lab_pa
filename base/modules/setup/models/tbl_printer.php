﻿<?php

class tbl_printer extends TblBase
{
    function __construct()
    {
        $this->TblName='setting_printer';
        TblBase::TblBase(true);

        $this->SqlQuery='SELECT * FROM setting_printer';
    }

    function FillRow($rec)
    {
        $row=new RowviewPrinter;
        $row->NAMA=$rec->nama;
		$row->LOKASI=$rec->lokasi;
        
        return $row;
    }
    
    function readAll()
    {
    
        //$this->db->where('parent', '2');
        //$this->db->order_by('status', 'asc');
        $this->db->select('*');
        $this->db->from('setting_printer');
        $query = $this->db->get();

        return $query;
    }

}

class RowviewPrinter
{
   public $NAMA;
   public $LOKASI;  

}

?>
