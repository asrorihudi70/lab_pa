<?php
class tblam_history_log_metering extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->TblName='am_history_log_metering';
        TblBase::TblBase();
    }


    function FillRow($rec)
    {
        $row=new Rowtblam_history_log_metering;
        $row->HIST_LOG_ID=$rec->hist_log_id;
        $row->ASSET_MAINT_ID=$rec->asset_maint_id;
        $row->EMP_ID=$rec->emp_id;
        $row->INPUT_DATE=$rec->input_date;
        $row->METER=$rec->meter;
        $row->DESC_LOG=$rec->desc_log;

        return $row;
    }
}
class Rowtblam_history_log_metering
{
    public $HIST_LOG_ID;
    public $ASSET_MAINT_ID;
    public $EMP_ID;
    public $INPUT_DATE;
    public $METER;
    public $DESC_LOG;

}
?>
