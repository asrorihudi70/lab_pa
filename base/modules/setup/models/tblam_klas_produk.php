<?php
class tblam_klas_produk extends TblBase
{
    function __construct()
    {
        $this->TblName='klas_produk';
        TblBase::TblBase();
        $this->SqlQuery= "";
    }
 /* character varying(8) NOT NULL,
   character varying(25),
   smallint,
  parent character varying(6),
  kd_kat integer NOT NULL DEFAULT 0,
  klas_produk*/
    function FillRow($rec)
    {
        $row=new Rowam_klas_produk();
        $row->KD_KLAS=$rec->kd_klas;
        $row->KLASIFIKASI=$rec->klasifikasi;
        $row->PARENT=$rec->parent;
        $row->TYPE_DATA=$rec->type_data;

        return $row;
    }

}

class Rowam_klas_produk
{
    public $KD_KLAS;
    public $KLASIFIKASI;
    public $PARENT;
    public $TYPE_DATA;

}

class clsTreeRow
{

    public $id;
    public $text;
    public $leaf;
    public $expanded;
    public $parents;
    public $parents_name;
    public $type;
    public $children = array();

}


?>
