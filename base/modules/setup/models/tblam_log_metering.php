<?php
class tblam_log_metering extends TblBase
{

	function __construct()
	{
		$this->StrSql="";
		$this->TblName='am_log_metering';
		TblBase::TblBase();
	}

	function FillRow($rec)
	{
		$row=new Rowtblam_log_metering;
                $row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->METER=$rec->meter;

		return $row;
	}
}
class Rowtblam_log_metering
{
    public $ASSET_MAINT_ID;
    public $METER;
}
?>
