<?php
class tblam_part_category extends TblBase
{
    function __construct()
    {
        $this->TblName='am_part_category';
        TblBase::TblBase();
        $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowtblam_part_category;
        $row->CATEGORY_ID=$rec->category_id;
        $row->PART_ID=$rec->part_id;

        return $row;
    }

}

class Rowtblam_part_category
{
    public $CATEGORY_ID;
    public $PART_ID;

}

?>
