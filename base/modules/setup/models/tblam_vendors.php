<?php
class tblam_vendors extends TblBase
{
    function __construct()
    {
        $this->TblName='am_vendors';
        TblBase::TblBase();

        $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowtbltblam_vendors;

        $row->VENDOR_ID =$rec->vendor_id;
        $row->VENDOR = $rec->vendor;
        $row->CONTACT1 = $rec->contact1;
        $row->CONTACT2 = $rec->contact2;
        $row->VEND_ADDRESS = $rec->vend_address;
        $row->VEND_CITY = $rec->vend_city;
        $row->VEND_PHONE1 = $rec->vend_phone1;
        $row->VEND_PHONE2 = $rec->vend_phone2;
        $row->VEND_POS_CODE = $rec->vend_pos_code;
        $row->COUNTRY = $rec->country;

        return $row;
    }

}

class Rowtbltblam_vendors
{
    public $VENDOR_ID;
    public $VENDOR;
    public $CONTACT1;
    public $CONTACT2;
    public $VEND_ADDRESS;
    public $VEND_CITY;
    public $VEND_PHONE1;
    public $VEND_PHONE2;
    public $VEND_POS_CODE;
    public $COUNTRY;

}

?>
