﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblam_category extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="category_id,category_name,parent,type"  ;
		$this->TblName='am_category';
				TblBase::TblBase();
		$this->SQL=$this->db;
	}

	
	function FillRow($rec)
	{
		$row=new Rowam_category;
				$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->PARENT=$rec->parent;
		$row->TYPE=$rec->type;

		return $row;
	}
}
class Rowam_category
{
	public $CATEGORY_ID;
public $CATEGORY_NAME;
public $PARENT;
public $TYPE;

}

?>