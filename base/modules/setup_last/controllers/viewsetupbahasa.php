<?php

class viewsetupbahasa extends MX_Controller    
{
	 public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }


    function read($Params=null)
    {

        try
        {

            $this->load->model('setup/tblam_language');
            if (strlen($Params[4])!==0)
            {
               $this->tblam_language->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
            }
            $res = $this->tblam_language->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);

        }
        catch(Exception $o)
        {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';

    }


    function save($Params=null)
    {
        $this->load->model('setup/tblam_language');
        $Arr['language_id']=$Params['LanguageId'];
        $Arr['language']=$Params['Language'];
        if (strlen($Arr['language_id'])===0)
        {
            $Arr['language_id']=$this->GetNewID();
            $res=$this->tblam_language->Save($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
            if ($res>0)
            {
                echo '{success: true, LanguageId: "'.$Arr['language_id'].'"}';
            } else echo '{success: false}';
        }
        else
        {
            $this->tblam_language->db->where("language_id = '".$Arr['language_id']."'", null, false);
            $res=$this->tblam_language->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
            if ($res>0)
            {
                echo '{success: true}';
            } else echo '{success: false}';
        }

            //echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
     }

     function GetNewID()
     {
         $res = $this->tblam_language->GetRowList( 0 , 1, 'DESC', 'language_id', '');
         if ($res[1]===0)
         {
             return 1;
         }
         else
         {
             return $res[0][0]->LANGUAGE+1;
         }
     }

	 
     function delete($param)
     {
        $this->load->model('setup/tblam_language');
        $this->tblam_language->db->where("language_id = '".$param['LanguageId']."'", null, false);
        $res=$this->tblam_language->Delete(); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
        //return $res;
        if ($res>0)
        {
            echo '{success: true}';
        } else echo '{success: false}';

            //$res=$this->tblam_reference->DeleteData($VConn,$param); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
            //return $res;
     }

}
//VIEWSETUPDEPT

?>