<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_department extends Model
{

	function am_department()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('dept_id', $data['dept_id']);
		$this->db->set('dept_name', $data['DEPT_NAME']);
		$this->db->insert('am_department');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('dept_id', $id);
		$query = $this->db->get('am_department');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('am_department');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('DEPT_ID', $data['DEPT_ID']);
		$this->db->set('DEPT_NAME', $data['DEPT_NAME']);
		$this->db->update('am_department');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('dept_id', $id);
		$this->db->delete('am_department');

		return $this->db->affected_rows();
	}

}


class Rowam_department
{
	public $DEPT_ID;
	public $DEPT_NAME;
}

?>