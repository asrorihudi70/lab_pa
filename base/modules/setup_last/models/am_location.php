<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_location extends Model
{

	function am_location()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('LOCATION_ID', $data['LOCATION_ID']);
		$this->db->set('LOCATION', $data['LOCATION']);
		$this->db->insert('am_location');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('LOCATION_ID', $id);
		$query = $this->db->get('am_location');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('am_location');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('LOCATION_ID', $data['LOCATION_ID']);
		$this->db->set('LOCATION', $data['LOCATION']);
		$this->db->update('am_location');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('LOCATION_ID', $id);
		$this->db->delete('am_location');

		return $this->db->affected_rows();
	}

}



?>