﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblam_asset_maint extends TblBase
{
	
	function __construct()
	{
            $this->StrSql="";
            //"asset_maint_id,category_id,comp_id,dept_id,location_id,asset_maint_name,years,description_asset,asset_id,last_maint_date,status_asset_id,path_image";
            $this->TblName='am_asset_maint';
            TblBase::TblBase();
	}


	function FillRow($rec)
	{
            $row=new Rowam_asset_maint;

            $row->ASSET_MAINT_ID=$rec->asset_maint_id;
            $row->CATEGORY_ID=$rec->category_id;
            $row->COMP_ID=$rec->comp_id;
            $row->DEPT_ID=$rec->dept_id;
            $row->LOCATION_ID=$rec->location_id;
            $row->ASSET_MAINT_NAME=$rec->asset_maint_name;
            $row->YEARS=$rec->years;
            $row->DESCRIPTION_ASSET=$rec->description_asset;
            $row->ASSET_ID=$rec->asset_id;
            $row->LAST_MAINT_DATE=$rec->last_maint_date;

//            if ($rec->last_maint_date!==null)
//            {
//            $dt=new DateTime($rec->last_maint_date);
//            $row->LAST_MAINT_DATE=$dt->format('Ymd'); }
//             else
//            { $row->LAST_MAINT_DATE='00010101'; }

            $row->STATUS_ASSET_ID=$rec->status_asset_id;
            $row->PATH_IMAGE=$rec->path_image;

            return $row;
	}
}
class Rowam_asset_maint
{
    public $ASSET_MAINT_ID;
    public $CATEGORY_ID;
    public $COMP_ID;
    public $DEPT_ID;
    public $LOCATION_ID;
    public $ASSET_MAINT_NAME;
    public $YEARS;
    public $DESCRIPTION_ASSET;
    public $ASSET_ID;
    public $LAST_MAINT_DATE;
    public $STATUS_ASSET_ID;
    public $PATH_IMAGE;

}

?>