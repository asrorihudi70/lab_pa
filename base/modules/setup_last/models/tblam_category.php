<?php
class tblam_category extends TblBase
{
    function __construct()
    {
        $this->TblName='am_category';
        TblBase::TblBase();
        $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowam_category();
        $row->CATEGORY_ID=$rec->category_id;
        $row->CATEGORY_NAME=$rec->category_name;
        $row->PARENT=$rec->parent;
        $row->TYPE=$rec->type;

        return $row;
    }

}

class Rowam_category
{
    public $CATEGORY_ID;
    public $CATEGORY_NAME;
    public $PARENT;
    public $TYPE;

}

class clsTreeRow
{

    public $id;
    public $text;
    public $leaf;
    public $expanded;
    public $parents;
    public $parents_name;
    public $type;
    public $children = array();

}


?>
