﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblam_employees extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="emp_id,dept_id,emp_name,emp_address,emp_city,emp_state,emp_pos_code,emp_phone1,emp_phone2,emp_email,is_aktif"  
;
		$this->TblName='am_employees';
		TblBase::TblBase();
	}


	function FillRow($rec)
	{
		$row=new Rowam_employees;
		$row->EMP_ID=$rec->emp_id;
		$row->DEPT_ID=$rec->dept_id;
		$row->EMP_NAME=$rec->emp_name;
		$row->EMP_ADDRESS=$rec->emp_address;
		$row->EMP_CITY=$rec->emp_city;
		$row->EMP_STATE=$rec->emp_state;
		$row->EMP_POS_CODE=$rec->emp_pos_code;
		$row->EMP_PHONE1=$rec->emp_phone1;
		$row->EMP_PHONE2=$rec->emp_phone2;
		$row->EMP_EMAIL=$rec->emp_email;
		$row->IS_AKTIF=$rec->is_aktif;

		return $row;
	}
}
class Rowam_employees
{
	public $EMP_ID;
public $DEPT_ID;
public $EMP_NAME;
public $EMP_ADDRESS;
public $EMP_CITY;
public $EMP_STATE;
public $EMP_POS_CODE;
public $EMP_PHONE1;
public $EMP_PHONE2;
public $EMP_EMAIL;
public $IS_AKTIF;

}

?>