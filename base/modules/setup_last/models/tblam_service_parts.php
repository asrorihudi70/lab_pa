﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblam_service_parts extends TblBase
{
	
    function __construct()
    {
            $this->StrSql="service_id,category_id,part_id,qty,unit_cost,tot_cost,instruction"  ;
            $this->TblName='am_service_parts';
            TblBase::TblBase();
    }


    function FillRow($rec)
    {
        $row=new Rowam_service_parts;
        $row->SERVICE_ID=$rec->service_id;
        $row->CATEGORY_ID=$rec->category_id;
        $row->PART_ID=$rec->part_id;
        $row->QTY=$rec->qty;
        $row->UNIT_COST=$rec->unit_cost;
        $row->TOT_COST=$rec->tot_cost;
        $row->INSTRUCTION=$rec->instruction;

        return $row;
    }
}

class Rowam_service_parts
{
    public $SERVICE_ID;
    public $CATEGORY_ID;
    public $PART_ID;
    public $QTY;
    public $UNIT_COST;
    public $TOT_COST;
    public $INSTRUCTION;

}

?>