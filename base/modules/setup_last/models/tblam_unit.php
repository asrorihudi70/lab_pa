﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblam_unit extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="unit_id,unit";
		$this->TblName='am_unit';
		TblBase::TblBase();
	}

	
	function FillRow($rec)
	{
		$row=new Rowam_unit;
				$row->UNIT_ID=$rec->unit_id;
		$row->UNIT=$rec->unit;

		return $row;
	}
}
class Rowam_unit
{
	public $UNIT_ID;
public $UNIT;

}

?>