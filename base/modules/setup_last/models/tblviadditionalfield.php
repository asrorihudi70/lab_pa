<?php
class tblviadditionalfield extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT a.category_id, a.row_add, a.addfield, a.type_field, coalesce(b.value, ' ') as value,
                b.asset_maint_id, a.lenght,
            CASE WHEN a.type_field = '1' THEN 'Text' WHEN a.type_field = '2' THEN 'Numeric' ELSE 'Date / Time' END AS typefield
            FROM  am_add_field AS a LEFT OUTER JOIN
                am_asset_maint_detail AS b ON a.category_id = b.category_id AND a.row_add = b.row_add
            
            ) as resdata";

        $this->TblName='viadditionalfield';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblviadditionalfield;

        $row->CATEGORY_ID=$rec->category_id;
        $row->ROW_ADD=$rec->row_add;
        $row->ADDFIELD=$rec->addfield;
        $row->TYPE_FIELD=$rec->type_field;
        $row->VALUE=$rec->value;
        $row->ASSET_MAINT_ID=$rec->asset_maint_id;
        $row->LENGHT=$rec->lenght;
        $row->TYPEFIELD=$rec->typefield;

        return $row;
    }
}

class Rowtblviadditionalfield
{
    public $CATEGORY_ID;
    public $ROW_ADD;
    public $ADDFIELD;
    public $TYPE_FIELD;
    public $VALUE;
    public $ASSET_MAINT_ID;
    public $LENGHT;
    public $TYPEFIELD;

}

?>
