<?php
class tblvipartcategory extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT  am_part_category.part_id, am_spareparts.part_name, am_part_category.category_id,
                am_category.category_name
            FROM am_category INNER JOIN
                am_part_category ON am_category.category_id = am_part_category.category_id INNER JOIN
                  am_spareparts ON am_part_category.part_id = am_spareparts.part_id

            ) as resdata";

        $this->TblName='vipartcategory';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblvipartcategory;
        $row->PART_ID=$rec->part_id;
        $row->PART_NAME=$rec->part_name;
        $row->CATEGORY_ID=$rec->category_id;
        $row->CATEGORY_NAME=$rec->category_name;

        return $row;
    }
}
class Rowtblvipartcategory
{
    public $PART_ID;
    public $PART_NAME;
    public $CATEGORY_ID;
    public $CATEGORY_NAME;

}

?>
