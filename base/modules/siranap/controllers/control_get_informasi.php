<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class control_get_informasi extends MX_Controller {

	public function __construct(){
		//parent::Controller();
		parent::__construct();
	}

	public function index(){
		$this->load->view('main/index');
	}


	function read($Params=null)
	{
		$query = array();
		try
		{
			$criteria 	= "";

			if (strlen($Params[4]) !== 0) {
				$criteria = " WHERE ".str_replace("~", "'", $Params[4]);
			}
			$_query 		= "
				SELECT 
					k.no_kamar, 
					k.nama_kamar, 
					spc.spesialisasi,
					SUM(k.jumlah_bed - k.digunakan) as kosong,
					k.digunakan as digunakan,
					k.kls_bpjs,
					CASE WHEN k.kls_bpjs = 'KL1' THEN 'Kelas 1' ELSE
						CASE WHEN k.kls_bpjs = 'KL2' THEN 'Kelas 2' ELSE 
							CASE WHEN k.kls_bpjs = 'KL3' THEN 'Kelas 3' ELSE 
							k.kls_bpjs 
							END 
						END 
					END as kelas
					FROM 
						kamar K
						LEFT JOIN spc_kamar spc_k ON spc_k.no_kamar = K.no_kamar
						LEFT JOIN spesialisasi spc ON spc.kd_spesial = spc_k.kd_spesial 
					$criteria 
				GROUP BY 
					k.no_kamar, 
					k.nama_kamar, 
					spc.spesialisasi,
					k.jumlah_bed,
					k.digunakan,
					k.kls_bpjs
				ORDER BY k.nama_kamar ASC
			";
			// $this->load->model('Siranap/tb_unit_apotek');
			
			// if (strlen($Params[4]) !== 0) {
			// 	$this->db->where(str_replace("~", "'", $Params[4] . " order by nm_unit_far ASC offset " . $Params[0] . "  limit 500 "), null, false);
			// }
			$query = $this->db->query($_query);
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';
		}
		if ($query->num_rows() > 0) {
			echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($query->result()).'}';
		}else{
			echo '{success:true, totalrecords:0, ListDataObj:'.json_encode($query).'}';
		}
	 	//echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->NM_UNIT_FAR=$rec->nm_unit_far;

		return $row;
	}
}
?>