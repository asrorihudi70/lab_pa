<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class control_get_informasi extends MX_Controller {

	public function __construct(){
		//parent::Controller();
		parent::__construct();
	}

	public function index(){
		$this->load->view('main/index');
	}


	function read($Params=null)
	{
		$query = array();
		try
		{
			$criteria 	= "";

			if (strlen($Params[4]) !== 0) {
				$criteria = " WHERE ".str_replace("~", "'", $Params[4]);
			}
			$_query 		= "
				SELECT 
					p.kd_pasien, 
					p.nama, 
					TO_CHAR(p.tgl_lahir, 'YYYY-mm-dd') as tgl_lahir, 
					age(to_date(TO_CHAR(NOW(), 'YYYY-mm-dd'), 'YYYY-mm-dd'), to_date(TO_CHAR(p.tgl_lahir, 'YYYY-mm-dd'), 'YYYY-mm-dd')) as umur, 
					p.telepon, 
					u.nama_unit, 
					r.rujukan, 
					TO_CHAR(rk.tgl_masuk, 'YYYY-mm-dd') as tgl_rujukan, 
					mr.kd_penyakit, 
					pen.penyakit
				FROM 
					rujukan_kunjungan rk 
					INNER JOIN pasien p ON  p.kd_pasien = rk.kd_pasien 
					INNER JOIN unit u ON u.kd_unit = rk.kd_unit 
					INNER JOIN rujukan r ON r.kd_rujukan::character varying = rk.kd_rujukan
					LEFT JOIN mr_penyakit mr ON mr.kd_pasien = rk.kd_pasien AND mr.kd_unit = rk.kd_unit AND mr.tgl_masuk = rk.tgl_masuk AND mr.urut_masuk = rk.urut_masuk 
					LEFT JOIN penyakit pen ON pen.kd_penyakit = mr.kd_penyakit
					$criteria
			";
			// $this->load->model('Siranap/tb_unit_apotek');
			
			// if (strlen($Params[4]) !== 0) {
			// 	$this->db->where(str_replace("~", "'", $Params[4] . " order by nm_unit_far ASC offset " . $Params[0] . "  limit 500 "), null, false);
			// }
			$query = $this->db->query($_query);
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';
		}
		if ($query->num_rows() > 0) {
			echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($query->result()).'}';
		}else{
			echo '{success:true, totalrecords:0, ListDataObj:'.json_encode($query).'}';
		}
	 	//echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->NM_UNIT_FAR=$rec->nm_unit_far;

		return $row;
	}
}
?>