<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class setup_user extends MX_Controller {
    public function __construct() {
        //parent::Controller();
        parent::__construct();  
        $this->load->model('userman/zmodgroup');
        $this->load->model('userman/zmodule');
    }

    public function index() {
        // $this->load->view('main/user_management');
    }

    public function getModGroup(){
        $query = $this->zmodgroup->read_join_zmodule();
        echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query->result()).'}';
    }

    public function getZmodule(){
        $params = array(
            'mod_group'     => $this->input->post('mod_group'),
        );
        $data = array();
        $query = $this->zmodule->read_group($params);
        foreach ($query->result() as $value) {
            array_push($data, 
                array(
                    'MOD_NAME'  => $value->mod_name,
                    'MOD_ID'    => $value->mod_id,
                )
            );
        }
        echo '{success:true, totalrecords:'.count($data).', ListDataObj:'.json_encode($data).'}';
    }

    public function getSetupZmodule(){
        $params = array(
            'mod_group'    => $this->input->post('mod_group'),
            'group_id'     => $this->input->post('group_id'),
        );
        $data = array();
        $query = $this->db->query("SELECT * from ( 
            select 
                g.group_id, 
                m.mod_id, 
                m.mod_name, 
                m.mod_group from 
            zmodule m 
            inner join ztrustee g on m.mod_id=g.mod_id )as resdata WHERE group_id='".$params['group_id']."' AND mod_group = '".$params['mod_group']."'");
        foreach ($query->result() as $value) {
            array_push($data, 
                array(
                    'MOD_NAME'  => $value->mod_name,
                    'MOD_ID'    => $value->mod_id,
                )
            );
        }
        echo '{success:true, totalrecords:'.count($data).', ListDataObj:'.json_encode($data).'}';
    }

    public function getComboSetTrustee(){
        $params = array(
            'mod_group'    => $this->input->post('mod_group'),
            'group_id'     => $this->input->post('group_id'),
        );
        $data = array();
        $query = $this->db->query("SELECT DISTINCT(m.mod_group) from zmodule m inner join ztrustee g on m.mod_id=g.mod_id WHERE group_id='".$params['group_id']."'");
        foreach ($query->result() as $value) {
            array_push($data, 
                array(
                    'MOD_GROUP'  => $value->mod_group,
                    // 'MOD_ID'    => $value->mod_id,
                )
            );
        }
        echo '{success:true, totalrecords:'.count($data).', ListDataObj:'.json_encode($data).'}';
    }

    public function delete_user(){
        $response    = array();
        $resultQuery = false;
        $this->db->trans_begin();

        $params = array(
            'array_data'    => $this->input->post('array_data'),
        );

        $data_pack_1 = explode("##[[]]##",$params['array_data']);
        for($i=0;$i<count($data_pack_1);$i++)
        {
            $data_pack_2 = explode("@@##$$@@",$data_pack_1[$i]);
            unset($paramsCriteria);
            $paramsCriteria = array(
                'group_id'  => $data_pack_2[1],
                'kd_user'   => $data_pack_2[0],
            );
            $this->db->where($paramsCriteria);
            $resultQuery = $this->db->delete("zmember");
            if ($resultQuery == false || $resultQuery == 0) {
                $resultQuery = false;
                break;
            }
        }
        if ($resultQuery>0 || $resultQuery === true) {
            $this->db->trans_commit();
            $response['status'] = true;
        }else{
            $this->db->trans_rollback();
            $response['status'] = false;
        }
        $this->db->close();
        echo json_encode($response);
    } 
}
?>
