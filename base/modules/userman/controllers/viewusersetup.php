<?php

class viewusersetup extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/user_management');
    }

    public function read($Params = null) {

        try {

            $tgl = date('M-d-Y');
            $this->load->model('userman/tbl_infouser');
            if (strlen($Params[4]) !== 0) {
                $criteria = str_replace("~", "'", $Params[4]);
                $this->tbl_infouser->db->where($criteria, null, false);
            }

            $res = $this->tbl_infouser->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:' . $res[1] . ', ListDataObj:' . json_encode($res[0]) . '}';
    }

    function save($param) {
        $this->load->model('userman/tblam_usersetup');

        $last = $this->db->query("SELECT (CONVERT(INT,kd_user)) as kd_user from zusers ")->result();
        //$last = _QMS_Query("SELECT kd_user=MAX(kd_user) from zusers ")->result();
        foreach ($last as $l) {
            $lid = $l->kd_user;
            $lastid = intval($lid) + 1;
        }
        $this->db->where("kd_user='" . $param['kd_user'] . "'", null, false);
        // $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;

        $res = $this->tblam_usersetup->GetRowList();
        // echo "<pre>".var_export($res[0], true)."</pre>"; 
        // if(!isset($res[0][0])){
        //     echo "insert";
        // }else{
        //     echo "update";
        // }
        // // echo $res[0];
        // die;
        if(!isset($res[0][0])){
            $param['password'] = $param['password'];
            $param['kd_user'] = $lastid;
            $res = $this->tblam_usersetup->Save($param);
			$datasqlsrv = array(
                "kd_user"      => $param['kd_user'],
                "user_names"   => $param['user_names'],
                "full_name"    => $param['full_name'],
                "description2" => $param['description2'],
                "password"     => $param['password'],
                "tag1"         => $param['tag1'],
                "tag2"         => $param['tag2'],
                "kd_dokter"    => $param['kd_dokter'],
            );
			_QMS_insert('zusers',$datasqlsrv);		
            if ($res > 0) {
                echo '{success: true, kd_user: "' . $param['kd_user'] . '" }';
            } else
                echo '{success: false}';
        }
        else {
            $this->db->where("kd_user = '" . $param['kd_user'] . "'", null, false);
            $kriteriasql="kd_user = '" . $param['kd_user'] . "'";
            $data = array(
                "user_names"   => $param['user_names'],
                "full_name"    => $param['full_name'],
                "description2" => $param['description2'],
                "tag1"         => $param['tag1'],
                "tag2"         => $param['tag2'],
                "language_id"  => (int)$param['language_id'],
                "kd_unit"      => $param['kd_unit'],
                "kd_milik"     => $param['kd_milik'],
                "kd_unit_far"  => $param['kd_unit_far'],
                "kd_dokter"    => $param['kd_dokter'],
            );
            if (strlen($param['password']) < 32) {
                $data['password'] = $param['password'];
            }
			$datasqlsrv = array(
                "user_names"   => $param['user_names'],
                "full_name"    => $param['full_name'],
                "description2" => $param['description2'],
                "tag1"         => $param['tag1'],
                "tag2"         => $param['tag2'],
                "kd_dokter"    => $param['kd_dokter'],
            );
            $res = $this->tblam_usersetup->Update($data);
			_QMS_update('zusers',$datasqlsrv,$kriteriasql);
            if ($res > 0) {
                echo '{success: true}';
            } else
                echo '{success: false}';
        }
    }

    public function getsession() {
        $session = $this->session->userdata['user_id'];
        var_dump($session);
    }

    function delete($param) {


        $this->load->model('userman/tblam_usersetup');
        $Sql = $this->db;
        $Sql->where("kd_user = '" . $param['kd_user'] . "'", null, false);
        $res = $this->tblam_usersetup->Delete(); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
        //return $res;
        if ($res > 0) {
            echo '{success: true}';
        } else
            echo var_dump($param); //$param['Hari'].":".$kUnit;//echo '{success: false}';
    }

}

//VIEWSETUPEMPLOYEE
?>