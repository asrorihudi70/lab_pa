<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tbl_user_milik extends TblBase {

    function __construct() {
        $this->StrSql = "kd_milik,milik";
        $this->SqlQuery = "select kd_milik,milik from apt_milik order by milik";
        $this->TblName = 'tbl_user_milik';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowuser;

        $row->KODE = $rec->kd_milik;
        $row->NAMA = $rec->milik;

        return $row;
    }

}

class Rowuser {

    public $KODE;
    public $NAMA;

}

?>