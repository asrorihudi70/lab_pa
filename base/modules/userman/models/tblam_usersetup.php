<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblam_usersetup extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_user,user_names,full_name,description2,password,tag1,tag2,language_id"  
;
		$this->TblName='zusers';
		TblBase::TblBase();
	}


	function FillRow($rec)
	{
		$row=new Rowam_User;
		$row->KD_USER=$rec->Kd_User;
		$row->USER_NAMES=$rec->User_names;
		$row->FULL_NAME=$rec->Full_Name;
		$row->DESCRIPTION2=$rec->Description2;
		$row->PASSWORD=$rec->Password;
		$row->TAG1=$rec->Tag1;
		$row->TAG2=$rec->Tag2;
		$row->LANGUAGE_ID=$rec->language_id;
		return $row;
	}
}
class Rowam_User
{
public $KD_USER;
public $USER_NAMES;
public $FULL_NAME;
public $DESCRIPTION2;
public $PASSWORD;
public $TAG1;
public $TAG2;
public $LANGUAGE_ID;
}

?>