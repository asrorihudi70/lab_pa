﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblzusers extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="Kd_User,kd_dokter,User_names,Full_Name,Description2,Password,Tag1,Tag2,Tag3,language_id,kd_unit,User_id";
		$this->TblName='ZUsers';
			TblBase::TblBase();
		$this->SQL=$this->db;
	}

	
	function FillRow($rec)
	{
		$row=new Rowzusers;
		$row->KD_USER=$rec->Kd_User;
		$row->USER_NAMES=$rec->User_names;
		$row->FULL_NAME=$rec->Full_Name;
		$row->DESCRIPTION2=$rec->Description2;
		$row->PASSWORD=$rec->Password;
		$row->TAG1=$rec->Tag1;
		$row->TAG2=$rec->Tag2;
		$row->TAG3=$rec->Tag3;
		$row->USERID=$rec->User_id;
		$row->LANGUAGE_ID=$rec->language_id;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_MILIK=$rec->kd_milik;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->KD_DOKTER=$rec->kd_dokter;
		return $row;
	}
}
class Rowzusers
{
    public $KD_USER;
    public $USER_NAMES;
    public $FULL_NAME;
    public $DESCRIPTION2;
    public $PASSWORD;
    public $TAG1;
    public $TAG2;
    public $USERID;
    public $LANGUAGE_ID;
    public $KD_UNIT;
	public $KD_MILIK;
	public $KD_UNIT_FAR;
	public $KD_DOKTER;
}
