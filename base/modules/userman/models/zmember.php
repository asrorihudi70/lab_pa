<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class zmember extends Model
{

	function zmember()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Kd_User', $data['Kd_User']);
		$this->db->set('Group_ID', $data['Group_ID']);
		$this->db->insert('zmember');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('Kd_User', $id);
		$this->db->where('Group_ID', $id);
		$query = $this->db->get('zmember');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('zmember');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Kd_User', $data['Kd_User']);
		$this->db->where('Group_ID', $data['Group_ID']);
		$this->db->update('zmember');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Kd_User', $id);
		$this->db->where('Group_ID', $id);
		$this->db->delete('zmember');

		return $this->db->affected_rows();
	}

}



?>