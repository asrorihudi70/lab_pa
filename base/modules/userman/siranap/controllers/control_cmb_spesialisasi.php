<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class control_cmb_spesialisasi extends MX_Controller {

	public function __construct(){
		//parent::Controller();
		parent::__construct();
	}

	public function index(){
		$this->load->view('main/index');
	}


	function read($Params=null)
	{
		$query = array();
		try
		{
			$criteria 	= "";

			if (strlen($Params[4]) !== 0) {
				$criteria = " WHERE ".str_replace("~", "'", $Params[4]);
			}
			$_query 		= "SELECT * FROM spesialisasi $criteria ORDER BY spesialisasi";
			// $this->load->model('Siranap/tb_unit_apotek');
			
			// if (strlen($Params[4]) !== 0) {
			// 	$this->db->where(str_replace("~", "'", $Params[4] . " order by nm_unit_far ASC offset " . $Params[0] . "  limit 500 "), null, false);
			// }
			$query = $this->db->query($_query);
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';
		}

		if ($query->num_rows() > 0) {
			$data = array();
			array_push($data, 
				array(
					'kd_spesial' 	=> 'Semua',
					'spesialisasi' 	=> 'Semua',
				)
			);
			foreach ($query->result() as $result) {
				array_push($data, array(
						'kd_spesial' 	=> $result->kd_spesial,
						'spesialisasi' 	=> $result->spesialisasi,
					)
				);
			}
			echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($data).'}';
		}else{
			echo '{success:true, totalrecords:0, ListDataObj:'.json_encode($query).'}';
		}
	 	//echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->NM_UNIT_FAR=$rec->nm_unit_far;

		return $row;
	}
}
?>