<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tbl_infouser extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_user,user_names,full_name,description,password,tag1,tag2";
		$this->SqlQuery="select  * from (
										
										select u.kd_user as kd_user,u.user_names as user_names,u.full_name as full_name,u.description2 as description,u.password,u.tag1,u.tag2 from zusers u
										)as resdata ";
		$this->TblName='tbl_infouser';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowuser;
	
		$row->KD_USER=$rec->kd_user;
		$row->USER_NAMES=$rec->user_names;
		$row->FULL_NAME=$rec->full_name;
		$row->DESCRIPTION=$rec->description;
		$row->PASSWORD=$rec->password;
		$row->TAG1=$rec->tag1;
		$row->TAG2=$rec->tag2;
			
		return $row;
	}
}
class Rowuser
{
public $KD_USER;
public $USER_NAMES;
public $FULL_NAME;
public $DESCRIPTION;
public $PASSWORD;
public $TAG1;
public $TAG2;
}

?>