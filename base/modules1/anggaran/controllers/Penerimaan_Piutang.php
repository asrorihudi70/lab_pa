<?php
class Penerimaan_Piutang extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function post(){
		$this->db->trans_begin();

		$datacariposting =array(
			'csar_number'	=>	$_POST['csar_number']
		);
		$dataupdateacc_csar =array(
			'posted'		=>	'true',
			'date_posted'	=>	date('Y-m-d')
			);
		$this->db->where($datacariposting);
		$posting = $this->db->update('acc_csar',$dataupdateacc_csar);
		if ($posting) {
			for ($i=0; $i < $_POST['jumlah']; $i++){
				$paid = $this->db->query("select sum(jumlah) as total 
											from ACC_ARFAK_INT aai
											inner join acc_csar ac on aai.csar_number = ac.csar_number
										 	where arf_number = '".$_POST['arf_number'.$i]."' and ac.posted = 't'")->row();
				$datacarifaktur =array(
						'arf_number'	=>	$_POST['arf_number'.$i]
						);
				$dataupdatefaktur =array(
					'paid'	=>	$paid->total
					);
				$this->db->where($datacarifaktur);
				$tmp_faktur = $this->db->update('acc_ar_faktur',$dataupdatefaktur);
				if ($tmp_faktur){
					// $getdatadetailfaktur = $this->db->query("select * from ACC_ARFAK_INT where arf_number = '".$_POST['arf_number'.$i]."' and csar_number = '".$_POST['csar_number']."'")->result();
					// foreach ($getdatadetailfaktur as $data) {
						for ($x=0; $x < $_POST['jumlah_detail']; $x++){
							$cek = $this->db->query("select * from acc_ar_fak_trans where arf_number = '".$_POST['arf_number_detail'.$x]."' and kd_kasir = '".$_POST['kd_kasir_detail'.$x]."' and no_transaksi = '".$_POST['no_transaksi_detail'.$x]."'")->row();
							if (count($cek) != 0) {
								$tmpdatasebelumnya = $cek->paid;
							}else{
								$tmpdatasebelumnya = 0;
							}
							$paidaccar = (int)$tmpdatasebelumnya + (int)$_POST['jumlah_detail'.$x];
							$datacari =array(
								'arf_number'	=>	$_POST['arf_number_detail'.$x],
								'no_transaksi'	=>	$_POST['no_transaksi_detail'.$x],
								'kd_kasir'		=>	$_POST['kd_kasir_detail'.$x],
								);
							$dataupdate =array(
								'paid'	=>	$paidaccar
								);
							$this->db->where($datacari);
							$penerimaan=$this->db->update('acc_ar_fak_trans',$dataupdate);
						}
						
					// }

				}
			}
			if ($penerimaan) {
						$this->db->trans_commit();
						echo "{success:true}";
					}else{
						$this->db->trans_rollback();	
						echo "{success:false}";
					}
		}else{
		    echo "{success:false}";
			$this->db->trans_rollback();				
		}		
	}	

	public function save(){
		$this->db->trans_begin();
		if(isset($this->session->userdata['user_id']))
		{
		 	$kd_user=$this->session->userdata['user_id']['id'];
		}
		else{
		 	$kd_user="";	
		}

		$data=array(
			'csar_date'=>$_POST['csar_date'],
			'cust_code'=>$_POST['cust_code'],
			'account'=>$_POST['account'],
			'pay_code'=>$_POST['pay_code'],
			'notes'=>$_POST['notes'],
			'amount'=>$_POST['amount'],
			'kd_user'=>$kd_user,
			'posted'=>'f',
		);

		if($kd_user===""||$kd_user==="null" || $kd_user==="undifined"){
		 echo "{cari:true}";	
		}
		else{

			if($_POST['csar_number']===''){
				$_POST['csar_date']=date('Y-m-d');
				$data['csar_date']=$_POST['csar_date'];
				$data['csar_number']=$this->getno_ro();
				$_POST['csar_number']=$data['csar_number'];
				$penerimaan=$this->db->insert('acc_csar',$data);
			 }else{
				$data['csar_date']=$_POST['csar_date'];
				$this->db->where('csar_number',$_POST['csar_number']);
				$penerimaan=$this->db->update('acc_csar',$data);
			}
			if($penerimaan){
				for ($i=0; $i < $_POST['jumlah']; $i++){
					if ($_POST['tag'.$i] != 'false') {
						$cekdatatotal = $this->db->query("select sum(jumlah) as jumlahtotal from ACC_AR_FAK_TRANS where arf_number = '".$_POST['arf_number'.$i]."'")->row();
						if (count($cekdatatotal) != 0) {
							$totaltagihan = $cekdatatotal->jumlahtotal;
							$getdatadetailfaktur = $this->db->query("select * from ACC_AR_FAK_TRANS where arf_number = '".$_POST['arf_number'.$i]."'")->result();
							if ($totaltagihan === $_POST['bayar'.$i]) {
								foreach ($getdatadetailfaktur as $data) {
									$no_transaksi = $data->no_transaksi;
									$kd_kasir = $data->kd_kasir;
									$bayar = $data->jumlah;

									$cekdatadetail = $this->db->query("select * from acc_arfak_int where csar_number = '".$_POST['csar_number']."' and no_transaksi = '".$no_transaksi ."' and kd_kasir = '".$kd_kasir."'")->row(); 
									if (count($cekdatadetail) != 0) {
										$caridatadetail =array(
											'csar_number'	=>	$_POST['csar_number'],
											'arf_number'	=>	$_POST['arf_number'.$i],
											'no_transaksi'	=>	$no_transaksi,
											'kd_kasir'		=>	$kd_kasir
										);
										$datadetail = array(
											'jumlah'	=>	$bayar
										);
										$this->db->where($caridatadetail);
										$penerimaan=$this->db->update('acc_arfak_int',$datadetail);
									}else{
										$datadetail =array(
										'csar_number'	=>	$_POST['csar_number'],
										'arf_number'	=>	$_POST['arf_number'.$i],
										'no_transaksi'	=>	$no_transaksi,
										'kd_kasir'		=>	$kd_kasir,
										'jumlah'		=>	$bayar
										);
										$penerimaan=$this->db->insert('acc_arfak_int',$datadetail);
									}
								}	
							}else{
								$tmpsisa = 0;
								foreach ($getdatadetailfaktur as $data) {
									$no_transaksi = $data->no_transaksi;
									$kd_kasir = $data->kd_kasir;
									$sisatagihan = $data->jumlah;
									if ($_POST['bayar'.$i] >= $sisatagihan) {
										
										$cekdatadetail = $this->db->query("select * from acc_arfak_int where csar_number = '".$_POST['csar_number']."' and no_transaksi = '".$no_transaksi ."' and kd_kasir = '".$kd_kasir."'")->row(); 
										if (count($cekdatadetail) != 0) {
											$caridatadetail =array(
												'csar_number'	=>	$_POST['csar_number'],
												'arf_number'	=>	$_POST['arf_number'.$i],
												'no_transaksi'	=>	$no_transaksi,
												'kd_kasir'		=>	$kd_kasir
											);
											$datadetail = array(
												'jumlah'	=>	$sisatagihan
											);
											$this->db->where($caridatadetail);
											$penerimaan=$this->db->update('acc_arfak_int',$datadetail);
										}else{
											$datadetail =array(
											'csar_number'	=>	$_POST['csar_number'],
											'arf_number'	=>	$_POST['arf_number'.$i],
											'no_transaksi'	=>	$no_transaksi,
											'kd_kasir'		=>	$kd_kasir,
											'jumlah'		=>	$sisatagihan
											);
											$penerimaan=$this->db->insert('acc_arfak_int',$datadetail);
										}
										$_POST['bayar'.$i] = $_POST['bayar'.$i] - $sisatagihan;
									}else{

										$no_transaksi = $data->no_transaksi;
										$kd_kasir = $data->kd_kasir;
										$sisatagihan = $data->jumlah;
											$cekdatadetail = $this->db->query("select * from acc_arfak_int where csar_number = '".$_POST['csar_number']."' and no_transaksi = '".$no_transaksi ."' and kd_kasir = '".$kd_kasir."'")->row(); 
											if (count($cekdatadetail) != 0) {
												$caridatadetail =array(
													'csar_number'	=>	$_POST['csar_number'],
													'arf_number'	=>	$_POST['arf_number'.$i],
													'no_transaksi'	=>	$no_transaksi,
													'kd_kasir'		=>	$kd_kasir
												);
												$datadetail = array(
													'jumlah'	=>	$sisatagihan
												);
												$this->db->where($caridatadetail);
												$penerimaan=$this->db->update('acc_arfak_int',$datadetail);
											}else{
												$datadetail =array(
												'csar_number'	=>	$_POST['csar_number'],
												'arf_number'	=>	$_POST['arf_number'.$i],
												'no_transaksi'	=>	$no_transaksi,
												'kd_kasir'		=>	$kd_kasir,
												'jumlah'		=>	$_POST['bayar'.$i]
												);
												$penerimaan=$this->db->insert('acc_arfak_int',$datadetail);
											}
											$_POST['bayar'.$i] = 0;
										
									}
								}
							}

						}else{

						}	
					}else{
						// echo 'b';
					}
							
				}

				if ($penerimaan) {
					$this->db->trans_commit();
					echo "{success:true,csar_number:'".$_POST['csar_number']."'}";	
				}else{
					$this->db->trans_rollback();	
					echo "{success:false}";
				}
							
			}else{
				$this->db->trans_rollback();	
				echo "{success:false}";	
			}
			
				 		
		}
	}

	public function savedetail(){
		$this->db->trans_begin();
		if(isset($this->session->userdata['user_id']))
		{
		 	$kd_user=$this->session->userdata['user_id']['id'];
		}
		else{
		 	$kd_user="";	
		}

		$data=array(
			'csar_date'=>$_POST['csar_date'],
			'cust_code'=>$_POST['cust_code'],
			'account'=>$_POST['account'],
			'pay_code'=>$_POST['pay_code'],
			'notes'=>$_POST['notes'],
			'amount'=>$_POST['amount'],
			'kd_user'=>$kd_user,
			'posted'=>'f',
		);

		if($kd_user===""||$kd_user==="null" || $kd_user==="undifined"){
		 echo "{cari:true}";	
		}
		else{

			if($_POST['csar_number']===''){
				$_POST['csar_date']=date('Y-m-d');
				$data['csar_date']=$_POST['csar_date'];
				$data['csar_number']=$this->getno_ro();
				$_POST['csar_number']=$data['csar_number'];
				$penerimaan=$this->db->insert('acc_csar',$data);
			 }else{
				$data['csar_date']=$_POST['csar_date'];
				$this->db->where('csar_number',$_POST['csar_number']);
				$penerimaan=$this->db->update('acc_csar',$data);
			}
			if($penerimaan){
					for ($i=0; $i < $_POST['jumlah']; $i++){

						$cekdatadetail = $this->db->query("select * from acc_arfak_int where csar_number = '".$_POST['csar_number']."' and no_transaksi = '". $_POST['no_transaksi'.$i] ."' and kd_kasir = '".$_POST['kd_kasir'.$i]."'")->row(); 
						if (count($cekdatadetail) != 0) {
							$caridatadetail =array(
								'csar_number'	=>	$_POST['csar_number'],
								'arf_number'	=>	$_POST['arf_number'.$i],
								'no_transaksi'	=>	$_POST['no_transaksi'.$i],
								'kd_kasir'		=>	$_POST['kd_kasir'.$i]
							);
							$datadetail = array(
								'jumlah'	=>	$_POST['bayar'.$i]
							);
							$this->db->where($caridatadetail);
							$penerimaan=$this->db->update('acc_arfak_int',$datadetail);
						}else{
							$datadetail =array(
							'csar_number'	=>	$_POST['csar_number'],
							'arf_number'	=>	$_POST['arf_number'.$i],
							'no_transaksi'	=>	$_POST['no_transaksi'.$i],
							'kd_kasir'		=>	$_POST['kd_kasir'.$i],
							'jumlah'		=>	$_POST['bayar'.$i]
							);
							$penerimaan=$this->db->insert('acc_arfak_int',$datadetail);							
						}
					}
					if ($penerimaan) {
						$this->db->trans_commit();
						echo "{success:true,csar_number:'".$_POST['csar_number']."'}";	
					}else{
						$this->db->trans_rollback();	
						echo "{success:false}";
					}									
				}else{
					$this->db->trans_rollback();	
					echo "{success:false}";	
				}
				
							
		
		}
	}
	public function delete(){
		$whereacc_arfak_int=array(			
			'csar_number'=>$_POST['csar_number']
		);
		$this->db->where($whereacc_arfak_int);
		$penerimaan=$this->db->delete('acc_arfak_int');

		if($penerimaan){
			$whereacc_csar=array(
			'csar_number'=>$_POST['csar_number'],
			'csar_date'=>$_POST['csar_date']
			);
			$this->db->where($whereacc_csar);
			$penerimaan=$this->db->delete('acc_csar');
			if ($penerimaan) {
				echo "{success:true}";	
			}else{
				echo "{success:false}";	
			}			
		}
		else{
			echo "{success:false}";	
		}		
	}
	public function select(){
		$acc = '';
		if ($_POST['posting'] === 'Posting') {
			$acc = "ac.posted='t' and ";
		}else if ($_POST['posting'] === 'Belum Posting') {
			$acc = "ac.posted='f' and ";
		}else{
			$acc = '';
		}
		if($_POST['csar_number']==="" && $_POST['tgl']==="")
		{
			$where="where  ac.csar_date in ('".date('Y-m-d')."')";
		}else{
			if($_POST['csar_number']!=="" || $_POST['tgl']===""){
				$where="where $acc ac.csar_number like '".$_POST['csar_number']."%'";
			}
			else if($_POST['csar_number']==="" || $_POST['tgl']!==""){
				$where="where $acc ac.csar_date between '".$_POST['tgl']."' and '".$_POST['tgl2']."'";
			}else{
				$where="where $acc ac.csar_number like '".$_POST['csar_number']."%' and   ac.csar_date between '".$_POST['tgl']."'  and '".$_POST['tgl2']."'";
			}
			
		}
		$penerimaan=$this->db->query("	select ac.posted,ac.csar_date,ac.csar_number,ac.account, a.name, ac.amount,ac.cust_code, ac.pay_code, ap.payment,ac.notes, c.customer || '(' || c.kd_customer || ')' as customer 
									from acc_csar ac  
									inner join accounts a on a.account = ac.account 
									inner join acc_payment ap on ap.pay_code = ac.pay_code 
									inner join customer c on ac.cust_code = c.kd_customer 
											 $where")->result();
		echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($penerimaan)." }";
		}

	public function accounts(){
		$select =$this->db->query("select * from accounts  where(  
		upper(account) like upper('".$_POST['text']."%') )
        or ( upper(name) like upper('".$_POST['text']."%')	)	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
	}
	
	public function pay_acc(){
		$select =$this->db->query("select * from acc_payment  where(  
		upper(pay_code::varchar(10)) like upper('".$_POST['text']."%'))
        or (upper(payment) like upper('".$_POST['text']."%'))	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
	}

	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("select csar_number from acc_csar
		where substring(csar_number from 6 for 7)='$today'	
		ORDER BY csar_number DESC limit 1")->row();
		if(count($no_sementara)==0)
		{
			$no_sementara='CSAR/'.$today."/000001";
		}else{
			$no_sementara=$no_sementara->csar_number;
			$no_sementara=substr($no_sementara,13,6);
			$no_sementara=(int)$no_sementara+1;
			$no_sementara='CSAR/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}

	public function getUrut_penerimaan_detail($arf_number){
		$no_=$this->db->query("select urut from ACC_ARFAK_INT where arf_number = '$arf_number' order by urut desc")->row();
		if(count($no_)==0)
		{
			$no_= (int)1;
		}else{			
			$no_= (int)$no_->urut+1;
		}
		return $no_;
	}
	
	public function select_detail(){
		$select=$this->db->query("select ac.*,a.name  
								from acc_ar_detail ac
								inner join accounts a on a.account=ac.account  
								where ar_number = '".$_POST['criteria']."'
								order by ac.line asc,ac.account asc")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function getdatadetailfaktur(){
		$select=$this->db->query(" SELECT aaft.arf_number, aaft.no_transaksi, t.kd_pasien, p.nama, u.kd_unit, u.nama_unit, aaft.jumlah - aaft.paid as value,aaft.kd_kasir
									from acc_ar_faktur aaf
									inner join acc_ar_fak_trans aaft on aaf.arf_number = aaft.arf_number
									inner join transaksi t on aaft.no_transaksi = t.no_transaksi and aaft.kd_kasir = t.kd_kasir
									inner join pasien p on t.kd_pasien = p.kd_pasien
									inner join unit u on t.kd_unit = u.kd_unit
									where aaf.arf_number = '".$_POST['criteria']."' and posted = 't' 
																	")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function loaddatageneralfaktur(){
		$cust_code = $_POST["cust_code"];
		$select=$this->db->query("SELECT *, amount - paid as sisa, '' as  tag 
									from ACC_AR_FAKTUR
									where cust_code = '".$cust_code."' and posted = 't' and (amount - paid) > 0 ")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function loaddatadetailfaktur(){
		$arf_number = $_POST["arf_number"];
		$select=$this->db->query("SELECT aaft.no_transaksi,aaft.jumlah, aaft.jumlah - aaft.paid AS sisa, t.kd_pasien,p.nama,k.no_sjp,u.nama_unit,aaft.kd_kasir
									FROM acc_ar_fak_trans aaft
									INNER JOIN transaksi t ON t.no_transaksi = aaft.no_transaksi AND t.kd_kasir = aaft.kd_kasir
									INNER JOIN kunjungan k ON t.kd_pasien = k.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
									INNER JOIN pasien p ON t.kd_pasien = p.kd_pasien
									INNER JOIN unit u ON u.kd_unit = t.kd_unit
									where arf_number = '".$arf_number."'")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function loaddatadetailcsar(){
		$csar_number = $_POST["csar"];
		$select=$this->db->query("SELECT aaft.arf_number, aai.no_transaksi,aaft.jumlah, aaft.jumlah - aai.jumlah AS sisa, aai.jumlah as bayar, t.kd_pasien,p.nama,k.no_sjp,u.nama_unit,aai.kd_kasir
									FROM acc_arfak_int aai
									INNER JOIN ACC_AR_FAK_TRANS aaft ON aaft.arf_number = aai.arf_number and aaft.no_transaksi = aai.no_transaksi AND aaft.kd_kasir = aai.kd_kasir 
									INNER JOIN transaksi t ON t.no_transaksi = aai.no_transaksi AND t.kd_kasir = aai.kd_kasir
									INNER JOIN kunjungan k ON t.kd_pasien = k.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
									INNER JOIN pasien p ON t.kd_pasien = p.kd_pasien
									INNER JOIN unit u ON u.kd_unit = t.kd_unit
									where csar_number = '".$csar_number."'")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function loaddatageneralcsar(){
		$csar_number = $_POST["csar"];
		$select=$this->db->query("select 'true' as tag,aai.arf_number,arf_date,aaf.cust_code,csar_number, amount, amount - sum(jumlah)  as sisa, sum(jumlah) as bayar
									from ACC_AR_FAKTUR aaf
									inner join ACC_ARFAK_INT aai on aai.arf_number = aaf.arf_number									
									where csar_number = '".$csar_number."'
									group by csar_number,amount,aai.arf_number,arf_date,aaf.cust_code")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}

	public function loaddatahistorypembayaran(){
		$arf_number = $_POST["arf_number"];
		$select=$this->db->query("select ac.csar_number,arf_number,TO_CHAR(date_posted, 'dd/MM/YYYY') as date_posted,sum(aai.jumlah) as jumlah
									from acc_csar ac
									inner join acc_arfak_int aai on aai.csar_number = ac.csar_number
									where posted = 't' and arf_number = '".$arf_number."'
									group by ac.csar_number,arf_number,date_posted")->result();
		echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}


	
	
}