<?php
class Pengeluaran extends MX_Controller{
	public function __construct(){
		parent :: __construct();
		
	}
	public function index(){
		$this->load->view('main/index');
	} 
	public function save(){
		$this->db->trans_begin();
		if(isset($this->session->userdata['user_id']))
		{
		$kd_user=$this->session->userdata['user_id']['id'];
		}else{
		$kd_user="";	
		}
		$data=array(
		'personal'=>$_POST['pengirim'],
		'account'=>$_POST['account'],
		'pay_code'=>$_POST['type_bayar'],
		'pay_no'=>$_POST['pay_no'],
		'notes1'=>$_POST['notes'],
		'amount'=>$_POST['amount'],
		'kd_user'=>$kd_user,
		'type'=>1,
		);
		if($kd_user===""||$kd_user==="null" || $kd_user==="undifined"){
		 echo "{cari:true}";	
		}else{
		if($_POST['no_voucher']===''){
			$_POST['tgl_cso']=date('Y-m-d');
			$data['cso_date']=$_POST['tgl_cso'];
			$data['cso_number']=$this->getno_ro();
			$_POST['no_voucher']=$data['cso_number'];
			$Pengeluaran=$this->db->insert('acc_cso',$data);
		 }else{
			$data['cso_date']=$_POST['tgl_cso'];
			$this->db->where('cso_number',$_POST['no_voucher']);
			$Pengeluaran=$this->db->update('acc_cso',$data);
		}
		if($Pengeluaran){
			for ($i=0; $i<$_POST['jumlah']; $i++){
				if($_POST['value'.$i]===""||$_POST['value'.$i]==="null"||$_POST['value'.$i]==="undifined"){
					$_POST['value'.$i]=0;
				}
				if (isset($_POST['line'.$i])){
				 if($_POST['line'.$i]==="" || $_POST['line'.$i]===""){
					$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['no_voucher']."'order by line desc limit 1");
					if (count($line->result())===0 ){
						$line=1;				
					}else{
						$line=(int)$line->row()->line + 1;
					}			
					$data=array(
							'cso_number'=>$_POST['no_voucher'],
							'cso_date'=>$_POST['tgl_cso'],
							'line'=>$line,
							'account'=>$_POST['account_detail'.$i],
							'description'=>$_POST['description_detail'.$i],
							'value'=>$_POST['value'.$i],
							);
					$Pengeluaran=$this->db->insert('acc_cso_detail',$data);	 
				 }else{
					$data=array(
							'cso_date'=>$_POST['tgl_cso'],
							'account'=>$_POST['account_detail'.$i],
							'description'=>$_POST['description_detail'.$i],
							'value'=>$_POST['value'.$i],
							);
							$where =array(
							'cso_number'=>$_POST['no_voucher'],
							'line'=>$_POST['line'.$i],
							);
							
					$this->db->where($where);	
					$Pengeluaran=$this->db->update('acc_cso_detail',$data);	 
				 
				 }	
				}else{
					$line=$this->db->query("select line from acc_cso_detail where cso_number= '".$_POST['no_voucher']."'order by line desc limit 1");
					if (count($line->result())===0 ){
						$line=1;				
					}else{
						$line=(int)$line->row()->line + 1;
					}			
					$data=array(
							'cso_number'=>$_POST['no_voucher'],
							'cso_date'=>$_POST['tgl_cso'],
							'line'=>$line,
							'account'=>$_POST['account_detail'.$i],
							'description'=>$_POST['description_detail'.$i],
							'value'=>$_POST['value'.$i],
							);
					$Pengeluaran=$this->db->insert('acc_cso_detail',$data);	
				}
			}
			if ($Pengeluaran){
				$this->db->trans_commit();
				echo "{success:true,no_voucher:'".$_POST['no_voucher']."'}";
			}else{
				$this->db->trans_rollback();	
				echo "{success:false}";
			}
		}
		else{
		$this->db->trans_rollback();	
		echo "{success:false}";	
		} 		
		}
	}
	public function delete(){
		if ($_POST['line']==="" ||$_POST['line']==="undifined"  ||$_POST['line']==="null"){
		echo "{tidak_ada_line:true}";	
		}else{
		$where=array(
		'cso_number'=>$_POST['no_voucher'],
		'line'=>$_POST['line']
		);
		$this->db->where($where);
		$Pengeluaran=$this->db->delete('acc_cso_detail');
		if($Pengeluaran){
		echo "{success:true}";
		}
		else{
		echo "{success:false}";	
		}
		}
	}
	public function select(){
		if($_POST['cso_number']==="" && $_POST['tgl']==="")
		{
		$where="where ac.type=1 and cso_date in ('".date('Y-m-d')."')";
		}else{
			if($_POST['cso_number']!=="" || $_POST['tgl']===""){
				$where="where ac.type=1 and cso_number like '".$_POST['cso_number']."%'";
			}
			else if($_POST['cso_number']==="" || $_POST['tgl']!==""){
				$where="where ac.type=1 and cso_date between '".$_POST['tgl']."' and '".$_POST['tgl2']."'";
			}else{
				$where="where ac.type=1 and cso_number like '".$_POST['cso_number']."%' and   cso_date between '".$_POST['tgl']."'  and '".$_POST['tgl2']."'";
			}
			
		}
		$Pengeluaran=$this->db->query("	select *,a.name as nama_account,case when no_tag='' or no_tag isnull then false else true end as statusl from acc_cso ac 
										inner join accounts a
										on ac.account=a.account
										inner join acc_payment  ap
										on ap.pay_code=ac.pay_code
										$where order by ac.cso_number asc")->result();
	echo"{success:true ,totalrecords:1, ListDataObj:".json_encode($Pengeluaran)." }";
	}
	
	public function accounts(){
		$select =$this->db->query("select * from accounts  where(  
		upper(account) like upper('".$_POST['text']."%') )
        or ( upper(name) like upper('".$_POST['text']."%')	)	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);
		}
	public function pay_acc(){
		$select =$this->db->query("select * from acc_payment  where(  
		upper(pay_code::varchar(10)) like upper('".$_POST['text']."%'))
        or (upper(payment) like upper('".$_POST['text']."%'))	limit 10")->result();
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$select;
    	echo json_encode($jsonResult);

		
	}
	public function getno_ro(){
		$today=date('Y/m');
		$no_sementara=$this->db->query("select cso_number from acc_cso
		where substring(cso_number from 4 for 7)='$today' and substring(cso_number from 1 for 3)='PO/'	
		ORDER BY cso_number DESC limit 1")->row();
		if(count($no_sementara)==0)
		{
		$no_sementara='PO/'.$today."/000001";
		}else{
			$no_sementara=$no_sementara->cso_number;
		$no_sementara=substr($no_sementara,11,6);
		$no_sementara=(int)$no_sementara+1;
		$no_sementara='PO/'.$today."/".str_pad($no_sementara, 6, "0", STR_PAD_LEFT); 
		}
		return $no_sementara;
	}
	
	public function post(){
		$this->db->trans_begin();
		$tgl=date('Y-m-d');
		$gln=$this->gl_number();
		$no_voucher=substr($_POST['no_voucher'],1,17);
		$data=array(
		'journal_code'=>'AC',
		'gl_number'=>$gln,
		'gl_date'=>$tgl,
		'reference'=>$_POST['no_voucher'],
		'notes'=>$_POST['catatan'],
		);
		$Pengeluaran=$this->db->insert('acc_gl_trans',$data);
		if($Pengeluaran){
			$Pengeluaran=$this->db->query("insert into acc_gl_detail select 'AC','".$gln."',
			'".$tgl."',".$this->get_line('AC',$gln,$tgl).",account,'".$_POST['catatan']."',amount,false,true from  acc_cso 
			where cso_number='".$no_voucher."'");
			if($Pengeluaran){
				$query=$this->db->query("select account,value from  acc_cso_detail 
				where cso_number='".$no_voucher."'")->result();
				if(count($query)!=0){
					for($i=0; $i<count($query); $i++){
					$Pengeluaran=$this->db->query("insert into acc_gl_detail values ('AC','".$gln."',
					'".$tgl."',".$this->get_line('AC',$gln,$tgl).",'".$query[$i]->account."','".$_POST['catatan']."','".$query[$i]->value."',true,true)");	
					}
					if($Pengeluaran){
						$this->db->query("update acc_cso set no_tag='".$_POST['no_voucher']."',
						date_tag='".$tgl."' where cso_number='".$no_voucher."'");
						$this->db->query("update acc_cso_detail set posted=true where cso_number='".$no_voucher."'");
						echo "{success:true}";
						$this->db->trans_commit();
					}else{
						echo "{success:false}";	
						$this->db->trans_rollback();	
					}
				}else{
					echo "{success:false}";	
					$this->db->trans_rollback();	
				}
			}else{
				echo "{success:false}";
				$this->db->trans_rollback();				
			}
		}else{
		    echo "{success:false}";
			$this->db->trans_rollback();				
		}
	}
	
	public function get_line($jc,$gln,$tgl){
		/*
		 character varying(2) NOT NULL,
		 double precision NOT NULL DEFAULT 0,
		 timestamp without time zone NOT NULL
		*/
		$line=$this->db->query("select line from acc_gl_detail where 
		journal_code	='".$jc."' and gl_number	='".$gln."' and	gl_date	='".$tgl."'  order by line desc limit 1 ")->row();
		if(count($line)===0){
		$line=1;
		}else{
		$line=(int)$line->line+1;	
		}
		return $line; 
	}
	
	public function gl_number(){
		$gl_number=$this->db->query("select gl_number from acc_gl_trans order by gl_number desc limit 1 ")->row();
		if(count($gl_number)===0){
		$gl_number=1;
		}else{
		$gl_number=(int)$gl_number->gl_number+1;	
		}
		return $gl_number; 
	}
	
	public function select_detail(){
	$select=$this->db->query("select ac.*,a.name  from acc_cso_detail ac
			inner join accounts a on a.account=ac.account  where cso_number='".$_POST['criteria']."'
			order by ac.line asc,ac.account asc")->result();
			echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
	}
	
	
}