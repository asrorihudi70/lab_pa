<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class cetakfakturar extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }
    
    
    public function Cetak($Params=NULL)
    {
        $mError = "";
       
        if ($_POST['Faktur'] === 'Faktur' && $_POST['surat_tagihan'] === 'Tagihan') {
        	
            $mError = $this->cetakSuratpengantar($Params);
            if ($mError=="sukses") {
                $mError = $this->cetakFaktur($Params);
                if ($mError=="sukses")
                {
                    echo '{success: true}';
                }
                else{
                    echo $mError;
                }
            }else{
                echo $mError;
            }
            
        }else{
            if ($_POST['surat_tagihan'] === 'Tagihan') {
                $mError = $this->cetakSuratpengantar($Params);
                if ($mError=="sukses")
                {
                    echo '{success: true}';
                }
                else{
                    echo $mError;
                }
            }else{
                $mError = $this->cetakFaktur($Params);
                if ($mError=="sukses")
                {
                    echo '{success: true}';
                }
                else{
                    echo $mError;
                }
            }
        }
        
    }
    
    public function cetakSuratpengantar($Params)
    {
        
        $nofaktur = $_POST['no_faktur'];
        $total = $_POST['total'];
        $strError = "";

        $kd_user = $this->session->userdata['user_id']['id'];
        $q = $this->db->query("SELECT a.*, b.customer
                                FROM acc_ar_faktur a
                                INNER JOIN customer b on a.cust_code = b.kd_customer
                                WHERE arf_number = '".$nofaktur."'");
        
        if($q->num_rows == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
        foreach ($q->result() as $data)
        {
            $Total = $data->amount;
            $date = $data->arf_date;
            $customer = $data->customer;
        }

        $getdate = $this->db->query("SELECT min(b.tgl_transaksi),max(b.tgl_transaksi)
									FROM ACC_AR_FAK_TRANS a
									inner join transaksi b on a.kd_kasir = b.kd_kasir and a.no_transaksi = b.no_transaksi
									WHERE arf_number = '".$nofaktur."'");
        foreach ($getdate->result() as $data)
        {
            $tgl_awal = $data->min;
            $tgl_akhir = $data->max;
        }
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        $no = 0;
        $printer = $this->db->query("select p_kwitansi from zusers where kd_user = '".$kd_user."'")->row()->p_kwitansi;
        $Jabatan = $this->db->query("select setting from sys_setting where key_data = 'Jabatan_Print_Faktur_SP'")->row()->setting;
        $Nama = $this->db->query("select setting from sys_setting where key_data = 'Nama_Print_Faktur_SP'")->row()->setting;
        $NIP = $this->db->query("select setting from sys_setting where key_data = 'NIP_Print_Faktur_SP'")->row()->setting;
        $t1 = 4;
        $t3 = 30;
        $t2 = 36 - ($t3 + $t1);
        $today = Bulaninindonesia(date("d F Y"));
        $tgl_awal = Bulaninindonesia(date('d F Y', strtotime($tgl_awal)));
        $tgl_akhir = Bulaninindonesia(date('d F Y', strtotime($tgl_akhir)));
        $date = Bulaninindonesia(date('d F Y', strtotime($date)));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $NameRS . "\n"; $no++;
        $Data .= $Address. "\n"; $no++;
        $Data .= $TLP. "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($bold1."SURAT PENGANTAR".$bold0,30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($bold1."NOMOR : ".$bold0,30," ")."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= str_pad(" ", 50, " ") . str_pad("Kepada Yth", 30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($customer, 30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Di-", 30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Tempat", 30," ")."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "Dengan Hormat, "."\n"; $no++;
        $Data .= "Bersama ini kami kirimkan faktur tagihan biaya pengobatan karyawan/ti. "."\n"; $no++;
        $Data .= "Periode : ".$tgl_awal.' s/d '.$tgl_akhir."\n"; $no++;
        $Data .= "Faktur  : ".$nofaktur."\n"; $no++;
        $Data .= "Tanggal : ".$date."\n"; $no++;
        $Data .= "Sebesar : ".number_format($Total, 0, ',', '.')."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "Terbilang : ".terbilang($Total)."Rupiah"."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "Untuk Pembayaran tersebut agar saudara menyetorkan pada Rekening Bank yang telah"."\n"; $no++;
        $Data .= "ditunjuk dalam Faktur tersebut"."\n"; $no++;
        $Data .= "Demikian Disampaikan, Atas perhatianya diucapkan terima kasih"."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($Kota . ' , ' . $today, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($Jabatan, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($Nama, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("(---------------------)", 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($NIP, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= "--------------------------------------------------------------------------------"."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad($bold1."TANDA TERIMA".$bold0,80," ", STR_PAD_BOTH)."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= chr(27) . chr(33) . chr(8); $no++;
        $Data .= "Telah terima faktur tagihan biaya pengobatan karyawan/ti ".$customer."\n"; $no++;
        $Data .= "di ".$NameRS."\n"; $no++;
        $Data .= str_pad(" ", 10, " ") . "Faktur    :". $nofaktur."\n"; $no++;
        $Data .= str_pad(" ", 10, " ") . "Tanggal   :". $date ."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Yang Menerima", 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("(---------------------)", 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        if ($no < 67) {
            for ($i=$no; $i < 67; $i++) {
                $Data .= "\n";
            }
        }

        fwrite($handle, $Data);
        fclose($handle);
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
		}
        $strError = "sukses";
        
        
        }
        return $strError;
        
    }

     public function cetakFaktur($Params)
    {
        
        $nofaktur = $_POST['no_faktur'];
        $total = $_POST['total'];
        $strError = "";

        $kd_user = $this->session->userdata['user_id']['id'];
        $q = $this->db->query("SELECT a.*, b.customer
                                FROM acc_ar_faktur a
                                INNER JOIN customer b on a.cust_code = b.kd_customer
                                WHERE arf_number = '".$nofaktur."'");
        
        if($q->num_rows == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
        foreach ($q->result() as $data)
        {
            $Total = $data->amount;
            $date = $data->arf_date;
            $customer = $data->customer;
            $Jatuhtempo = $data->due_date;
        }
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        $printer = $this->db->query("select p_kwitansi from zusers where kd_user = '".$kd_user."'")->row()->p_kwitansi;
        $Jabatan = $this->db->query("select setting from sys_setting where key_data = 'Jabatan_Print_Faktur'")->row()->setting;
        $Nama = $this->db->query("select setting from sys_setting where key_data = 'Nama_Print_Faktur'")->row()->setting;
        $NIP = $this->db->query("select setting from sys_setting where key_data = 'NIP_Print_Faktur'")->row()->setting;
        $Bank = $this->db->query("select setting from sys_setting where key_data = 'Bank_Setoran_Faktur'")->row()->setting;
        $norek = $this->db->query("select setting from sys_setting where key_data = 'NOREK_Setoran_Faktur'")->row()->setting;
        $lam1 = $this->db->query("select setting from sys_setting where key_data = 'Tebusan1_Print_Faktur'")->row()->setting;
        $lam2 = $this->db->query("select setting from sys_setting where key_data = 'Tebusan2_Print_Faktur'")->row()->setting;
        $lam3 = $this->db->query("select setting from sys_setting where key_data = 'Tebusan3_Print_Faktur'")->row()->setting;
        $no = 0;
        $t1 = 15;
        $t3 = 20;
        $t2 = 75 - ($t3 + $t1);
        $today = Bulaninindonesia(date("d F Y"));
        $date = Bulaninindonesia(date('d F Y', strtotime($date)));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $NameRS . "\n"; $no++;
        $Data .= $Address. "\n"; $no++;
        $Data .= $TLP. "\n"; $no++;
        $Data .= $Kota. "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Faktur    : ".$nofaktur,30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Tanggal   : ".$date,30," ")."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Kepada Yth", 30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($customer, 30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Di-", 30," ")."\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("Tempat", 30," ")."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "--------------------------------------------------------------------------------"."\n"; $no++;
        $Data .= str_pad("Tanggal ", $t1, " ") . str_pad("Uraian", $t2, " ") . str_pad("Jumlah", $t3, " ", STR_PAD_LEFT) . "\n"; $no++;
        $Data .= "--------------------------------------------------------------------------------"."\n"; $no++;
        $queryDet = $this->db->query("SELECT a.arf_number,a.no_transaksi,b.tgl_transaksi, c.nama,d.nama_unit,a.jumlah
                                        FROM acc_ar_fak_trans  a
                                        inner join transaksi b on a.kd_kasir = b.kd_kasir and b.no_transaksi = a.no_transaksi
                                        inner join pasien c on c.kd_pasien = b.kd_pasien
                                        inner join unit d on d.kd_unit = b.kd_unit
                                        WHERE a.arf_number = '".$nofaktur."'
                                        order by c.nama")->result();
        $Data .= str_pad("", $t1, " ") . str_pad("Jumlah Biaya Pengobatan / Perawatan : ", $t2, " ") . str_pad("", $t3, " ", STR_PAD_LEFT) . "\n"; $no++;
        foreach ($queryDet as $line) { 
            $tgl = date_create($line->tgl_transaksi);
            $date =  date_format($tgl,"d/M/Y");
            $Data .= str_pad($date, $t1, " ") . str_pad($line->nama.' / '.$line->nama_unit, $t2, " ") . str_pad($jadi = number_format($line->jumlah, 0, ',', '.'), $t3, " ", STR_PAD_LEFT) . "\n"; $no++;
        }
        $Data .= "--------------------------------------------------------------------------------"."\n"; $no++;
        $Data .= str_pad("", $t1, " ") . str_pad("JUMLAH TOTAL", $t2, " ", STR_PAD_BOTH) . str_pad(number_format($Total, 0, ',', '.'), $t3, " ", STR_PAD_LEFT) . "\n"; $no++;
        $Data .= "--------------------------------------------------------------------------------"."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "Terbilang : "."\n"; $no++;
        $Data .= terbilang($Total)."Rupiah"."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($Kota . ' , ' . $today, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($Jabatan, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($Nama, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad("(---------------------)", 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= str_pad(" ", 50, " ") . str_pad($NIP, 30, " ", STR_PAD_BOTH) . "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++; 
        $Data .= "Tembusan : "."\n"; $no++;
        $Data .= "1. ".$lam1."\n"; $no++;
        $Data .= "2. ".$lam2."\n"; $no++;
        $Data .= "3. ".$lam3."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "--------------------------------------------------------------------------------"."\n"; $no++;
        $Data .= "\n"; $no++;
        $Data .= "Catatan "."\n"; $no++;
        $Data .= "- Perincian Terlampir "."\n"; $no++;
        $Data .= "- Jatuh Tempo Pembayaran : ".$Jatuhtempo."\n"; $no++;
        $Data .= "- Disetorkan Pada : ".$Bank."\n"; $no++;
        $Data .= "- Nomor Rekening : ".$Bank."\n"; $no++;
        if ($no < 67) {
            for ($i=$no; $i < 67; $i++) {
                $Data .= "\n";
            }
        }
        fwrite($handle, $Data);
        fclose($handle);
        
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            copy($file, $printer);  # Lakukan cetak
            unlink($file);
            # printer windows -> nama "printer" di komputer yang sharing printer
        } else{
            shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
        }
        $strError = "sukses";
        
        
        }
        return $strError;
        
    }

    // public function cek($Params)
    // {
    //     $strError = "";
    //     $kd_user = $this->session->userdata['user_id']['id'];
    //     $printer = $this->db->query("select p_kwitansi from zusers where kd_user = '".$kd_user."'")->row()->p_kwitansi;
    //     $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
    //     $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
    //     $handle = fopen($file, 'w');
    //     $condensed = Chr(27) . Chr(33) . Chr(4);
    //     $bold1 = Chr(27) . Chr(69);
    //     $bold0 = Chr(27) . Chr(70);
    //     $initialized = chr(27).chr(64);
    //     $condensed1 = chr(15);
    //     $condensed0 = chr(18);
    //     $Data  = $initialized;
    //     $Data .= $condensed1;
    //     $Data .= chr(27) . chr(33) . chr(8);
    //     for ($i=1; $i < 100; $i++) { 
    //         $Data .= $i."\n";
    //     }

    //     // $Data .= 'a'."\n";
    //     fwrite($handle, $Data);
    //     fclose($handle);
        
    //     if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    //         copy($file, $printer);  # Lakukan cetak
    //         unlink($file);
    //     //     # printer windows -> nama "printer" di komputer yang sharing printer
    //     } else{
    //         shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
    //     }
    //     $strError = "sukses";
        
        
        
    //     return $strError;
        
    // }
    
   
            
}




