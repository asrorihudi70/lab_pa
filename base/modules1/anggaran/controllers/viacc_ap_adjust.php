<?php

/**
 * @author
 * @copyright
 */
class viacc_ap_adjust extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $data=$Params[4];
        $query = $this->db->query("SELECT APA_NUMBER, APA_DATE, DUE_DATE, VEND_CODE, VENDOR,VendName, SUM(Debit) AS TotalDebit, SUM(Kredit) AS TotalKredit, Type, Notes,POSTED
									 FROM (
										SELECT     ART.APA_NUMBER, ART.APA_DATE, ART.DUE_DATE, VEND.KD_VENDOR AS VEND_CODE, VEND.VENDOR,VEND.KD_VENDOR||' - '||VEND.VENDOR AS VendName, 
											CASE WHEN ARD.IsDebit = 't' THEN SUM(ARD.Value) ELSE 0 END AS Debit, 
											CASE WHEN ARD.IsDebit = 'f' THEN SUM(ARD.Value) ELSE 0 END AS Kredit, ART.Notes,ART.Type,POSTED
										FROM ACC_APA_DETAIL AS ARD
										INNER JOIN ACC_APADJUST AS ART ON ART.APA_NUMBER = ARD.APA_NUMBER AND ART.APA_DATE = ARD.APA_DATE 
										INNER JOIN VENDOR AS VEND ON VEND.KD_VENDOR = ART.VEND_CODE
										INNER JOIN ACCOUNTS AS AK ON AK.Account = ARD.Account
										LEFT OUTER JOIN ACCOUNTS AS PAK ON PAK.Account = AK.Account
										$data
										GROUP BY ART.APA_NUMBER, ART.APA_DATE, ART.DUE_DATE, VEND.KD_VENDOR, VEND.VENDOR, ARD.IsDebit, ART.Notes, ART.Type,POSTED)
										AS x
										GROUP BY APA_NUMBER, APA_DATE,DUE_DATE, VEND_CODE,VENDOR,VendName, Type, Notes,POSTED
										ORDER BY APA_DATE DESC")->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['APA_NUMBER']=$data->apa_number;
			$HASIL[$i]['APA_DATE']=$data->apa_date;
			$HASIL[$i]['DUE_DATE']=$data->due_date;
			$HASIL[$i]['VEND_CODE']=$data->vend_code;
			$HASIL[$i]['VENDOR']=$data->vendor;
			$HASIL[$i]['VENDNAME']=$data->vendname;
			$HASIL[$i]['TOTALDEBIT']=$data->totaldebit;
			$HASIL[$i]['TOTALKREDIT']=$data->totalkredit;
			$HASIL[$i]['TYPE']=$data->type;
			$HASIL[$i]['NOTES']=$data->notes;
			$HASIL[$i]['POSTED']=$data->posted;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}';
    }

}

?>