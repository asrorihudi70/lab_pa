<?php

/**
 * @author
 * @copyright
 */
class viewaccountunit extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewaccountunit');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaccountunit->db->where($criteria, null, false);
        }
        $query = $this->tblviewaccountunit->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>