<?php

/**
 * @author
 * @copyright
 */
class viewimportlistfakturar extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
       $data=explode('#',$Params[4]);
		$kd_cust 	= $data[0];
		$kd_unit 	= $data[1];
		$tgl_awal 	= $data[2];
		$tgl_akhir 	= $data[3];
		$chkAll 	= $data[4];
		$chk1 		= $data[5];
		$chk2 		= $data[6];
		$chk3 		= $data[7];
		
		if ($chkAll == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (2)';
		}else if ($chk1 == 0 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1,2)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,3)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (2,3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}
        
        $query = $this->db->query("SELECT d.no_transaksi AS NO_TRANSAKSI,d.tgl_transaksi AS TGL_TRANSAKSI,d.shift AS SHIFT,d.kd_kasir AS KD_KASIR,d.Kd_Pay AS KD_PAY,
									k.KD_PASIEN ||' - ' || s.NAMA AS URAIAN, SUM(d.Jumlah) as HARGA,d.KD_USER 
									FROM (((Transaksi t INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
									INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay)
									INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk)
									inner join PASIEN s on k.KD_PASIEN=s.KD_PASIEN
									WHERE t.Kd_unit = '$kd_unit' AND k.Kd_Customer ='$kd_cust' --AND p.Kd_Customer = '$kd_cust' 
									AND ((d.Tgl_Transaksi between '$tgl_awal' and '$tgl_akhir'
											$criteriashift
										))
									and t.kd_unit||t.no_transaksi||to_char(d.tgl_transaksi,'YYYYMMDD')||d.shift::character varying  not in ( select aat.kd_unit||aat.no_transaksi||to_char(aat.tgl_transaksi,'YYYYMMDD')||aat.shift::character varying  
									From ACC_ARFAK_TRANS aat
									WHERE aat.Kd_unit = '$kd_unit' AND ((aat.Tgl_Transaksi between '$tgl_awal' and '$tgl_akhir' $criteriashift
										))
									)
									GROUP BY d.no_transaksi,d.tgl_transaksi,d.shift,d.kd_kasir,d.Kd_Pay, p.Uraian,d.KD_USER,k.KD_PASIEN,s.NAMA ")->result();
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($query) . '}';
    }

}

?>