<?php

/**
 * @author
 * @copyright
 */
class viewmasterrkat extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblrkat');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblrkat->db->where($criteria, null, false);
        }
        $query = $this->tblrkat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblviewrkatrdet');
            $criteria = $Params['query'];
            $this->tblviewrkatrdet->db->where($criteria, null, false);
        $query = $this->tblviewrkatrdet->GetRowList();
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>