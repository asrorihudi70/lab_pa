<?php

/**
 * @author
 * @copyright
 */
class viewmasterunitkerja extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewmasterunitkerja');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewmasterunitkerja->db->where($criteria, null, false);
        }
        $query = $this->tblviewmasterunitkerja->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $this->load->model('anggaran/tblviewmasterunitkerja');

        $data = array("plafond_plaf" => $Params["ANGGARAN"]);

        $criteria = $Params["query"];
        $this->tblviewmasterunitkerja->db->where($criteria, null, false);
        $query = $this->tblviewmasterunitkerja->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["kd_unit_kerja"] = $Params["KDPLAFON"];
            $data["tahun_anggaran_ta"] = $Params["TAHUN"];
            $result = $this->tblviewmasterunitkerja->Save($data);
        } else {
            $this->tblviewmasterunitkerja->db->where($criteria, null, false);
            $dataUpdate = array("kd_unit_kerja" => $Params["KDPLAFON"],"tahun_anggaran_ta" => $Params["TAHUN"]);
            $result = $this->tblviewmasterunitkerja->Update($data);
        }        
    
        if ($result > 0) {
                echo "{success: true}";
            } else {
                echo "{success: false}";
            }
    }

    public function delete($Params = null) {
        $criteria = $Params['query'];

        $this->load->model('anggaran/tblviewmasterunitkerja');

        $this->tblviewmasterunitkerja->db->where($criteria, null, false);

        $query = $this->tblviewmasterunitkerja->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewmasterunitkerja->db->where($criteria, null, false);
            $result = $this->tblviewmasterunitkerja->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }
}

?>