<?php

/**
 * @author
 * @copyright
 */
class viewpengesahanrkat extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewpengesahanrkat');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewpengesahanrkat->db->where($criteria, null, false);
        }
        $query = $this->tblviewpengesahanrkat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblrkat');

        $list1 = $Params['List'];
        $list = rtrim($list1,'|####|');
        $cut = explode('|####|', $list);
        // print_r($cut);
        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('@@@@', $cut[$i]);
            $data = array(                
                'disahkan_rkat' => 't',
            );
            $datacari = array(
                'kd_unit_kerja' => "'" . $res[0] . "'",
                'no_program_prog' => "'" . $res[4] . "'",
                'prioritas_rkat' => "'" . $res[3] . "'",
                'tahun_anggaran_ta' => "'" . $res[1] . "'",
            );
            $this->tblrkat->db->where($datacari, null, false);
            $query = $this->tblrkat->GetRowList();
            if ($query[1] != 0) {
                $this->tblrkat->db->where($datacari, null, false);
                $result = $this->tblrkat->Update($data);

                $this->load->model('anggaran/tblrkatp');
                $data1 = array(                
                'disahkan_rka' => 't',
                'jumlah' => $res[5]
                );

                $datacari1 = array(
                'kd_unit_kerja' => "'" . $res[0] . "'",
                'tahun_anggaran_ta' => "'" . $res[1] . "'",
                );

                $this->tblrkatp->db->where($datacari1, null, false);
                $query = $this->tblrkatp->GetRowList();
                if ($query[1] != 0) {
                    $this->tblrkatp->db->where($datacari1, null, false);
                    $result = $this->tblrkatp->Update($data1);
                    
                } else {
                    $data1['kd_unit_kerja'] = $res[0];
                    $data1['tahun_anggaran_ta'] = $res[1];
                    $result = $this->tblrkatp->Save($data1);
                }
             }else{}
        }

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }
    

}

?>