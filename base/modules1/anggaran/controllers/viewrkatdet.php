<?php

/**
 * @author
 * @copyright
 */
class viewrkatdet extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewrkat');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewrkat->db->where($criteria, null, false);
        }
        $query = $this->tblviewrkat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function GetPrioritas($unit, $tahun) {
        $retVal = 1;
        
        $this->load->model('anggaran/tblrkat');
        $this->tblrkat->db->where(" kd_unit_kerja = '" . $unit . "' and tahun_anggaran_ta = '" . $tahun . "'", null, false);
        $res = $this->tblrkat->GetRowList(0, 1, "DESC", "prioritas_rkat", "");
        if ($res[1] > 0) {
            $nm = $res[0][0]->PRIORITAS;
            $nomor = (int) $nm + 1;
            $retVal = $nomor;
        }
        return $retVal;
    }

    public function save($Params = null) {
        $this->load->model('anggaran/tblrkatdet');
         $list = $Params["LIST"];
        $cut = explode('@@@@', $list);
        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('<<>>', $cut[$i]);
            print_r($res);
            // $tot = $res[8] + $res[9] + $res[10] + $res[11] + $res[12] + $res[13] + $res[14] + $res[15] + $res[16] + $res[17] + $res[18] + $res[19];
            // $totall = +$tot;
            if ($res[7] == 'Pcs'){
                $satuan = 1;
            }elseif ($res[7] == 'Lembar') {
                 $satuan = 2;
            }elseif ($res[7] == 'Pcs') {
                 $satuan = 3;
            }else
            {
                $satuan = $res[6]; 
            }
            $data = array(
                // 'tahun_anggaran_ta' => $Params["Tahun"],
                // 'kd_unit_kerja' => $Params["Unit"],
                // 'prioritas_rkat' => $Params["Prioritas"],
                // 'no_program_prog' => $Params["Program"],
                // 'urut_rkat_det' => $res[1],   
                'account' => $res[2],         
                'kd_satuan_sat' => $satuan,
                'kuantitas_rkat_det' => $res[5],
                'biaya_satuan_rkat_det' => $res[6],
                'jmlh_rkat_det' => $res[8],
                'm1_rkat_det' => $res[9],
                'm2_rkat_det' => $res[10],
                'm3_rkat_det' => $res[11],
                'm4_rkat_det' => $res[12],
                'm5_rkat_det' => $res[13],
                'm6_rkat_det' => $res[14],
                'm7_rkat_det' => $res[15],
                'm8_rkat_det' => $res[16],
                'm9_rkat_det' => $res[17],
                'm10_rkakdet' => $res[18],
                'm11_rkakdet' => $res[19],
                'm12_rkakdet' => $res[20],
                'deskripsi_rkat_det' => $res[4]

            );
            $datacari = array(
                'tahun_anggaran_ta' => "'" . $Params["Tahun"] . "'",
                'kd_unit_kerja' => "'" . $Params["Unit"] . "'",
                'prioritas_rkat' => $Params["Prioritas"],
                'no_program_prog' =>  "'" . $Params["Program"] . "'",
                'urut_rkat_det' => $res[1],
            );
            $this->tblrkatdet->db->where($datacari, null, false);
            $query = $this->tblrkatdet->GetRowList();
            if ($query[1] != 0) {
                $this->tblrkatdet->db->where($datacari, null, false);
                $result = $this->tblrkatdet->Update($data);
            } else {
                $data["tahun_anggaran_ta"] = $Params["Tahun"];
                $data["kd_unit_kerja"] = $Params["Unit"];
                $data["prioritas_rkat"] = $Params["Prioritas"];
                $data["no_program_prog"] =  $Params["Program"];
                $data["urut_rkat_det"] = $res[1];
                $result = $this->tblrkatdet->Save($data);
            }
            // $tot = 0;
        }

        

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>