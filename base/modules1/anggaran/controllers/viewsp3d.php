<?php

/**
 * @author
 * @copyright
 */
class viewsp3d extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $this->load->model('anggaran/tblviewrkat');
        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewrkat->db->where($criteria, null, false);
        }
        $query = $this->tblviewrkat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function GetPrioritas($tahun) {
        $retVal = 1;
        
        $this->load->model('anggaran/tblsp3d');
        $this->tblsp3d->db->where("tahun_anggaran_ta = '" . $tahun . "'", null, false);
        $res = $this->tblsp3d->GetRowList(0, 1, "DESC", "tahun_anggaran_ta", "");
        if ($res[1] > 0) {
            $nm = $res[0][0]->NO_SP3D_RKAT;
            $nomor = (int) $nm + 1;
            $retVal = $nomor;
        }else{
            $retVal = 'SP3D-'.$tahun.'-00001';
        }
        return $retVal;
    }

    public function save($Params = null) {
        // print_r($Params);
        $this->load->model('anggaran/tblsp3d');

        if ($Params["NomorSp3d"] == '') {
            $tmpprioritas = $this->GetPrioritas($Params["TAHUN"]);
        }else
        {
            $tmpprioritas = $Params["NomorSp3d"];
        }
        

        $data = array("jumlah" => $Params["JUMLAH"]);

        $criteria = "kd_unit_kerja = '" . $Params["UNIT"] . "' AND tahun_anggaran_ta = '" . $Params["TAHUN"] . "' AND no_sp3d_rkat = '" . $tmpprioritas . "' AND tgl_sp3d_rkat = '" . $Params["TANGGAL"] . "'";

        $this->load->model('anggaran/tblsp3d');
        $this->tblsp3d->db->where($criteria, null, false);
        $query = $this->tblsp3d->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["no_sp3d_rkat"] = $tmpprioritas;
            $data["kd_unit_kerja"] = $Params["UNIT"];
            $data["tahun_anggaran_ta"] = $Params["TAHUN"];
            $data["tgl_sp3d_rkat"] = $Params["TANGGAL"];
            $result = $this->tblsp3d->Save($data);
        } else {
            $this->tblsp3d->db->where($criteria, null, false);
            $dataUpdate = array("jumlah" => $Params["JUMLAH"]);
            $result = $this->tblsp3d->Update($data);
        }

        $this->save_sp3d_trans($Params["TAHUN"],$Params["UNIT"],$tmpprioritas,$Params["TANGGAL"],$Params["PROG"],$Params["PRIO"],$Params["KEGIATAN"],$Params["JUMLAH"]);
        $this->save_sp3d_det($Params["TAHUN"],$Params["UNIT"],$tmpprioritas,$Params["TANGGAL"],$Params["PROG"],$Params["PRIO"],0,$Params["ACC"],$Params["KEGIATAN"]);

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function save_sp3d_trans($tahun,$unit,$nomor,$tanggal,$prog,$prio,$kegiatan,$jumlah) {
        // print_r($Params);
        $this->load->model('anggaran/tblsp3dtrans');

        $data = array("ket_sp3d_rkat" => $kegiatan, "jumlah_sp3d_rkat" => $jumlah );

        $criteria = "tahun_anggaran_ta = '" . $tahun . "' AND kd_unit_kerja = '" . $unit . "' AND no_sp3d_rkat = '" . $nomor . "' AND tgl_sp3d_rkat = '" . $tanggal . "' AND no_program_prog = '" . $prog . "' AND prioritas_sp3d_rkat = " . $prio . "";

        // $this->load->model('anggaran/tblsp3dtrans');
        $this->tblsp3dtrans->db->where($criteria, null, false);
        $query = $this->tblsp3dtrans->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["tahun_anggaran_ta"] = $tahun;
            $data["kd_unit_kerja"] = $unit;
            $data["no_sp3d_rkat"] = $nomor;
            $data["tgl_sp3d_rkat"] = $tanggal;
            $data["no_program_prog"] =  $prog;
            $data["prioritas_sp3d_rkat"] =  $prio;
            $result = $this->tblsp3dtrans->Save($data);
        } else {
            $this->tblsp3dtrans->db->where($criteria, null, false);
            $dataUpdate = array("jumlah" => $Params["JUMLAH"]);
            $result = $this->tblsp3dtrans->Update($data);
        }

        

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function save_sp3d_det($tahun,$unit,$nomor,$tanggal,$prog,$prio,$urut,$acc,$ket) {
        // print_r($Params);
        $this->load->model('anggaran/tblsp3ddet');
        if ($urut == 0)
        {
            $urut = 1;
        }

        $data = array("account" => $acc, "deskripsi" => $ket);

        $criteria = "tahun_anggaran_ta = '" . $tahun . "' AND kd_unit_kerja = '" . $unit . "' AND no_sp3d_rkat = '" . $nomor . "' AND tgl_sp3d_rkat = '" . $tanggal . "' AND no_program_prog = '" . $prog . "' AND prioritas_sp3d_rkat = " . $prio . " AND urut_sp3d_rkat_det = " . $urut . "";

        
        $this->tblsp3ddet->db->where($criteria, null, false);
        $query = $this->tblsp3ddet->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {

            $data["tahun_anggaran_ta"] = $tahun;
            $data["kd_unit_kerja"] = $unit;
            $data["no_sp3d_rkat"] = $nomor;
            $data["tgl_sp3d_rkat"] = $tanggal;
            $data["no_program_prog"] =  $prog;
            $data["prioritas_sp3d_rkat"] =  $prio;
            $data["urut_sp3d_rkat_det"] =  $urut;
            $result = $this->tblsp3ddet->Save($data);
        } else {
            $this->tblsp3ddet->db->where($criteria, null, false);
            $dataUpdate = array("jumlah" => $Params["JUMLAH"]);
            $result = $this->tblsp3ddet->Update($data);
        }

        

        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

}

?>