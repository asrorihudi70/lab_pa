<?php

/**
 * @author
 * @copyright
 */
class viimportitemopenar extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        
        $data					=explode('##@@##',$Params[4]);
		$jml_select				= $data[0];
		$data_kd_kasir			= explode('#',$data[1]);
		$data_no_trans			= explode('#',$data[2]);
		$data_tgl_trans			= explode('#',$data[3]);
		$data_shift_trans		= explode('#',$data[4]);
		$data_lainlain			= explode('#',$data[5]);
		$kd_cust				= $data_lainlain[0];
		$tgl_awal				= $data_lainlain[1];
		$tgl_akhir				= $data_lainlain[2];
		
		$query_union			='';
		$chkAll 				= $data_lainlain[3];
		$chk1 					= $data_lainlain[4];
		$chk2 					= $data_lainlain[5];
		$chk3 					= $data_lainlain[6];
		
		if ($chkAll == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (2)';
		}else if ($chk1 == 0 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 0){
			$criteriashift='AND d.Shift In (1,2)';
		}else if ($chk1 == 1 && $chk2 == 0 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,3)';
		}else if ($chk1 == 0 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (2,3)';
		}else if ($chk1 == 1 && $chk2 == 1 && $chk3 == 1){
			$criteriashift='AND d.Shift In (1,2,3)';
		}
		for ($i=0; $i < $jml_select; $i++){
			if ($i == 0){
				$ket_union='';
			}else{
				$ket_union='UNION';
			}
			$query_union			.= $ket_union." SELECT x.kd_unit,'' AS LINE,z.Item_Code AS ITEM_CODE,z.Item_Desc AS ITEM_DESC,z.Account AS ACCOUNT,
									z.Item_Desc AS DESCRIPTION,x.Amount AS VALUE FROM (
									SELECT k.kd_unit,d.Kd_Pay, p.Uraian, SUM(d.Jumlah) as Amount
									FROM (((Transaksi t INNER JOIN Detail_Bayar d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
									INNER JOIN Payment p ON d.Kd_Pay = p.Kd_Pay) 
									INNER JOIN Kunjungan k ON t.Kd_Unit = k.Kd_Unit AND t.Kd_Pasien = k.Kd_Pasien AND t.Urut_Masuk = k.Urut_Masuk AND t.Tgl_Transaksi = k.Tgl_Masuk) 
									WHERE t.Kd_kasir = '".$data_kd_kasir[$i]."' AND k.Kd_Customer = '".$kd_cust."' --AND p.Kd_Customer = '".$kd_cust."' 
									AND ((d.Tgl_Transaksi between '$tgl_awal'  AND '$tgl_akhir' 
									$criteriashift
									)  
									Or (d.Tgl_Transaksi  between '$tgl_awal'   AND '$tgl_akhir' 
									$criteriashift))  
									AND t.no_transaksi||'-'||to_char(d.tgl_transaksi,'DD/Mon/YYYY')||'-'||d.shift::character varying||'-'||t.kd_kasir 
									in ('".$data_no_trans[$i]."-".date('d/M/Y',strtotime($data_tgl_trans[$i]))."-".$data_shift_trans[$i]."-".$data_kd_kasir[$i]."') 
									GROUP BY k.kd_unit,d.Kd_Pay, p.Uraian 
									)x
									LEFT JOIN (
										SELECT KD_UNIT,KD_KASIR,i.* FROM Kasir_Unit ku
										INNER JOIN ACC_FAK_ITEM i on left(ku.KD_UNIT,1)=i.Item_Code
									)z on z.Kd_kasir='".$data_kd_kasir[$i]."' AND z.KD_UNIT=x.KD_UNIT ";
		}
		
		
        
        $query = $this->db->query($query_union)->result();
		$i=0;
		$HASIL=array();
		foreach($query as $data){
			$HASIL[$i]['LINE']=$data->line;
			$HASIL[$i]['ITEM_CODE']=$data->item_code;
			$HASIL[$i]['ITEM_DESC']=$data->item_desc;
			$HASIL[$i]['ACCOUNT']=$data->account;
			$HASIL[$i]['DESCRIPTION']=$data->description;
			$HASIL[$i]['VALUE']=$data->value;
			$i++;
		}
        echo '{success:true, totalrecords:' . count($query) . ', ListDataObj:' . json_encode($HASIL) . '}'; 
    }

}

?>