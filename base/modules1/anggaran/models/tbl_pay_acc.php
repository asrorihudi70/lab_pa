﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tbl_pay_acc extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select * from acc_payment";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->pay_code;
        $row->NAMA=$rec->payment;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $NAMA;
}