﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblsp3ddet extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_sp3d_rkat_det';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowtblsp3drkatkdet;
                
		$row->TAHUN=$rec->tahun_anggaran_ta;
        $row->UNIT=$rec->kd_unit_kerja;
        $row->NO=$rec->no_sp3d_rkat;
        $row->TGL=$rec->tgl_sp3d_rkat;
        $row->PROG=$rec->no_program_prog;
        $row->PRIO=$rec->prioritas_sp3d_rkat;
        $row->URUT=$rec->urut_sp3d_rkat_det;
        $row->ACC=$rec->account;
        $row->KET=$rec->deskripsi_sp3d_rkat_det;
        return $row;
	}
}
class Rowtblsp3drkatkdet
{
        public $TAHUN;
        public $UNIT;
        public $NO;
        public $TGL;
        public $PROG;
        public $PRIO;
        public $URUT;
        public $ACC;
        public $KET;
}