﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewfaktur_ap extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT 0::integer as ID,a.ARF_Number as REFF, a.Amount AS AMOUNT , 
							coalesce(a.vat,0) as AMOUNT_ADD,0::float as PAID,a.notes AS NOTES,
							a.ARF_DATE AS REFF_DATE,Amount-(paid+vat) as REMAIN
						FROM ACC_AR_FAKTUR a ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->ID=$rec->id;
        $row->REFF=$rec->reff;
        $row->AMOUNT=$rec->AMOUNT;
        $row->AMOUNT_ADD=$rec->amount_add;
        $row->PAID=$rec->paid;
        $row->NOTES=$rec->notes;
        $row->REFF_DATE=$rec->reff_date;
        $row->REMAIN=$rec->remain;
        return $row;
	}
}
class Rowprogram
{
        public $ID;
        public $REFF;
        public $AMOUNT;
        public $AMOUNT_ADD;
        public $PAID;
        public $NOTES;
        public $REFF_DATE;
        public $REMAIN;
}