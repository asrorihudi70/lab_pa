﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcmbunitfilterkasirunit extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT KD_UNIT, NAMA_UNIT FROM UNIT ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KD_UNIT=$rec->kd_unit;
        $row->NAMA_UNIT=$rec->nama_unit;
        return $row;
	}
}
class Rowprogram
{
        public $KD_UNIT;
        public $NAMA_UNIT;
}