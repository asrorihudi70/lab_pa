﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcmbunitkasir extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT DISTINCT U.KD_UNIT,U.KD_BAGIAN,U.KD_KELAS,U.NAMA_UNIT,U.PARENT,U.TYPE_UNIT,U.AKTIF, 
							U.SPESIAL,U.alias_sms AS ALIAS_SMS FROM UNIT U ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KD_UNIT=$rec->kd_unit;
        $row->KD_BAGIAN=$rec->kd_bagian;
        $row->KD_KELAS=$rec->kd_kelas;
        $row->NAMA_UNIT=$rec->nama_unit;
        $row->PARENT=$rec->parent;
        $row->TYPE=$rec->type_unit;
        $row->AKTIF=$rec->aktif;
        $row->SPESIAL=$rec->spesial;
        $row->ALIAS_SMS=$rec->alias_sms;
        return $row;
	}
}
class Rowprogram
{
        public $KD_UNIT;
        public $KD_BAGIAN;
        public $KD_KELAS;
        public $NAMA_UNIT;
        public $PARENT;
        public $TYPE;
        public $AKTIF;
        public $SPESIAL;
        public $ALIAS_SMS;
}