﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcomborekening extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="select DISTINCT  ma.MA_ID || ' ' || ma.MA_Name as MA_NAME, ma.ma_ID from acc_bd_mata ma";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewcomborekening;
                
		$row->KODE=$rec->ma_id;
        $row->NAMA=$rec->ma_name;
        return $row;
	}
}
class Rowtblviewcomborekening
{
        public $KODE;
        public $NAMA;
}