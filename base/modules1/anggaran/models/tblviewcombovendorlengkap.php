﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombovendorlengkap extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="SELECT kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT ,vendor || ' - ' || kd_vendor  AS VendorName FROM (
							SELECT 1 AS URUT, kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT FROM VENDOR WHERE (Vendor IS NOT NULL AND Vendor<>'') 
							UNION
							SELECT 2 AS URUT, kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT FROM VENDOR WHERE (Vendor IS NULL OR Vendor='')
						)x
						ORDER BY URUT ASC, Vendor ASC";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->Vend_Code=$rec->kd_vendor;
        $row->Vendor=$rec->vendor;
        $row->VendorName=$rec->vendorname;
        $row->DUE_DAY=$rec->term;
        $row->ACCOUNT=$rec->account;
        return $row;
	}
}
class Rowprogram
{
        public $Vend_Code;
        public $Vendor;
        public $VendorName;
        public $DUE_DAY;
        public $ACCOUNT;
}