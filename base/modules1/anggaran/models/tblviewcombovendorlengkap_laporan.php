﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewcombovendorlengkap_laporan extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="
						SELECT '000' as kd_vendor,0 as  urut, 'Semua' as vendor, '' as contact, '' as alamat, '' as kota,'' as kd_pos, '' as negara ,'' as telepon1,'' as telepon2,'' as FAX,0 as cr_limit,0 as term, '' as  ACCOUNT ,'Semua'  AS VendorName
						UNION
						SELECT kd_vendor, urut, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT ,vendor || ' - ' || kd_vendor  AS VendorName FROM (
							SELECT 1 AS URUT, kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT FROM VENDOR WHERE (Vendor IS NOT NULL AND Vendor<>'') 
							UNION
							SELECT 2 AS URUT, kd_vendor, Vendor,CONTACT,alamat,kota,kd_pos,negara,telepon1,telepon2,FAX,cr_limit,term, ACCOUNT FROM VENDOR WHERE (Vendor IS NULL OR Vendor='')
						)x
						ORDER BY URUT ASC, Vendor ASC";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_vendor;
        $row->VENDOR=$rec->vendor;
        $row->NAMA=$rec->vendorname;
        $row->TERM=$rec->term;
        $row->ACCOUNT=$rec->account;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $VENDOR;
        public $NAMA;
        public $TERM;
        public $ACCOUNT;
}