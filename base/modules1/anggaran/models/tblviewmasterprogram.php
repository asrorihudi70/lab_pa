﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewmasterprogram extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_program';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->NOPROG=$rec->no_program_prog;
        $row->PARENT=$rec->parent_prog;
        $row->NAMA=$rec->nama_program_prog;
        $row->LEVEL=$rec->level_prog;
        $row->TYPE=$rec->tipe_prog;
        $row->KODE=$rec->kode_program;
        $row->THN_AWAL=$rec->tahun_awal;
        $row->THN_AKHIR=$rec->tahun_akhir;
        $row->ALIAS=$rec->alias_noprogram;
        return $row;
	}
}
class Rowprogram
{
        public $NOPROG;
        public $PARENT;
        public $NAMA;
        public $LEVEL;
        public $TYPE;
        public $KODE;
        public $THN_AWAL;
        public $THN_AKHIR;
        public $ALIAS;
}