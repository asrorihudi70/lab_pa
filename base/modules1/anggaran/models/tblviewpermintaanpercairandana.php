﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewpermintaanpercairandana extends TblBase
{
	
	function __construct()
    {
        $this->TblName='acc_sp3d';
        TblBase::TblBase(true);

        $this->SqlQuery="select asp.no_sp3d_rkat, asp.tgl_sp3d_rkat,uk.nama_unit_kerja, asp.jumlah, '' as keterangan,
                        case 
                        when app_level_1 = 'true' and app_level_2 = true 
                        then 'true' else 'false' end as disahkan
                        from acc_sp3d asp
                        inner join unit_kerja uk on uk.kd_unit_kerja = asp.kd_unit_kerja";
    }


	function FillRow($rec)
	{
		$row=new Rowtblviewpermintaanpercairandana;
                
		$row->NO=$rec->no_sp3d_rkat;
        $row->TGL=$rec->tgl_sp3d_rkat;
        $row->NAMAUNIT=$rec->nama_unit_kerja;
        $row->JUMLAH=$rec->jumlah;
        $row->KET=$rec->keterangan;
        $row->DISAHKAN=$rec->disahkan;
        return $row;
	}
}
class Rowtblviewpermintaanpercairandana
{
        public $NO;
        public $TGL;
        public $NAMAUNIT;
        public $JUMLAH;
        public $KET;
        public $DISAHKAN;
}