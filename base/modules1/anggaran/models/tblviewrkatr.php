﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrkatr extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="
                            SELECT  RKA.KD_UNIT_KERJA, UK.NAMA_UNIT_KERJA,  RKA.TAHUN_ANGGARAN_TA,   RKA.DISAHKAN_RKAT
                            FROM  ACC_RKAT AS RKA   
                            INNER JOIN UNIT_KERJA AS UK ON UK.KD_UNIT_KERJA = RKA.KD_UNIT_KERJA
                        ";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODEUNIT=$rec->kd_unit_kerja;
        $row->NAMAUNIT=$rec->nama_unit_kerja;
        $row->TAHUN=$rec->tahun_anggaran_ta;
        if ($rec->disahkan_rkat == 'f')
        {
            $row->DISAHKAN = 'Belum Disahkan';
        }else{
            $row->DISAHKAN = 'Disahkan';
        }

        
        return $row;
	}
}
class Rowprogram
{
        public $KODEUNIT;
        public $NAMAUNIT;
        public $TAHUN;
        public $DISAHKAN;
}