﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewrkatrdet extends TblBase
{
	
	function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);

        $this->SqlQuery="
                            SELECT  RKA.KD_UNIT_KERJA, UK.NAMA_UNIT_KERJA,  RKA.TAHUN_ANGGARAN_TA, RKA.DISAHKAN_RKAT,rka.kegiatan_rkat,
                            rka.prioritas_rkat,rka.no_program_prog,ap.nama_program_prog,rka.is_revisi,rka.disahkan_rkat,art.jmlh_rkat_det,art.account
                            FROM  ACC_RKAT AS RKA   
                            left join acc_rkat_det art on RKA.tahun_anggaran_ta = art.tahun_anggaran_ta AND RKA.kd_unit_kerja = art.kd_unit_kerja AND RKA.no_program_prog = art.no_program_prog AND RKA.prioritas_rkat = art.prioritas_rkat
                            INNER JOIN UNIT_KERJA AS UK ON UK.KD_UNIT_KERJA = RKA.KD_UNIT_KERJA
                            inner join acc_program ap on ap.no_program_prog = rka.no_program_prog
                        ";
    }
    // ard.jmlh_rkat_det,
    // INNER join acc_rkat_det ard on ard.tahun_anggaran_ta = rka.tahun_anggaran_ta and ard.kd_unit_kerja = rka.kd_unit_kerja and ard.prioritas_rkat = rka.prioritas_rkat
	function FillRow($rec) 
	{
		$row=new Rowprogram;
                
		$row->KODEUNIT=$rec->kd_unit_kerja;
        $row->NAMAUNIT=$rec->nama_unit_kerja;
        $row->TAHUN=$rec->tahun_anggaran_ta;
        if ($rec->disahkan_rkat = 'f')
        {
            $row->DISAHKAN = 'false';
        }else{
            $row->DISAHKAN = 'true';
        }
        $row->KEGIATAN=$rec->kegiatan_rkat;
        // $row->JUMLAH=$rec->jmlh_rkat_det;
        $row->PRIORITAS=$rec->prioritas_rkat;
        $row->NO_PROG=$rec->no_program_prog;
        $row->NAMA_PROG=$rec->nama_program_prog;
        $row->REVISI=$rec->is_revisi;
        if ($rec->disahkan_rkat = null)
        {
            $row->JUMLAH = 0;
        }else{
            $row->JUMLAH = $rec->jmlh_rkat_det;;
        }
        $row->ACCOUNT=$rec->account;
        
        return $row;
	}
}
class Rowprogram
{
        public $KODEUNIT;
        public $NAMAUNIT;
        public $TAHUN;
        public $DISAHKAN;
        public $KEGIATAN;
        public $JUMLAH;
        public $PRIORITAS;
        public $NO_PROG;
        public $NAMA_PROG;
        public $REVISI;
        // public $TOT;
}