﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewunitkerja extends TblBase
{
	
	function __construct()
    {
        $this->TblName='unit_kerja';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }


	function FillRow($rec)
	{
		$row=new Rowprogram;
                
		$row->KODE=$rec->kd_unit_kerja;
        $row->PARENT=$rec->parent;
        $row->JENIS=$rec->kd_jns_unit;
        $row->NAMA=$rec->nama_unit_kerja;
        $row->STATUS=$rec->aktif;
        return $row;
	}
}
class Rowprogram
{
        public $KODE;
        public $PARENT;
        public $JENIS;
        public $NAMA;
        public $STATUS;
}