<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionInsertMrObat extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    } 
	 

	
	public function save_mrobat()
	{

			
			$jmllist=$_POST['jumlah'];
			for($i=0;$i<$jmllist;$i++)
			{
			$kd_satuan=$_POST['kd_satuan-'.$i];
			$kd_prd=$_POST['kd_prd-'.$i];
			$jml=$_POST['jml-'.$i];
			$kd_pasien=$_POST['kd_pasien'];
			$kd_unit=$_POST['kd_unit'];
			$tgl_masuk=$_POST['tgl_masuk'];
			$urut_masuk=$_POST['urut_masuk'];
			$urut=$this->db->query("select max(urut)+1 as urutmas from mr_obat where 
				kd_pasien='$kd_pasien'	
				and kd_unit='$kd_unit'
				and tgl_masuk='$tgl_masuk'
				and urut_masuk='$urut_masuk'
				")->row()->urutmas;
		    if ($urut=="")
			{
			$urut=1;
			}
				$ada=$this->db->query("select * from mr_obat where 
				kd_pasien='$kd_pasien'	
				and kd_unit='$kd_unit'
				and tgl_masuk='$tgl_masuk'
				and urut_masuk='$urut_masuk'
				and kd_prd='$kd_prd'
				")->result();
				if (count($ada)===0)
				{
					$query=$this->db->query(" insert into mr_obat values
					('$kd_pasien',
					'$kd_unit',
					'$tgl_masuk',
					'$urut_masuk',
					'".$_POST['tgl_out']."',
					$urut,
					$jml,
				   '$kd_prd',
				   '$kd_satuan'
					)
					");

				}else{
					
					$query=$this->db->query(" update mr_obat set jumlah=jumlah+$jml 
					where 
					kd_pasien='$kd_pasien'	
					and kd_unit='$kd_unit'
					and tgl_masuk='$tgl_masuk'
					and urut_masuk='$urut_masuk'
					and kd_prd='$kd_prd'
					");
				
				}
				
			}
			
			
		if($query)
		{
		echo "{success:true}";
		}else
		{
		echo "{success:false}";
		}
		
	}

	
	public function unposting_mrobat()
	{

			
			$jmllist=$_POST['jumlah'];
			for($i=0;$i<$jmllist;$i++)
			{
			$kd_satuan=$_POST['kd_satuan-'.$i];
			$kd_prd=$_POST['kd_prd-'.$i];
			$jml=$_POST['jml-'.$i];
			$kd_pasien=$_POST['kd_pasien'];
			$kd_unit=$_POST['kd_unit'];
			$tgl_masuk=$_POST['tgl_masuk'];
			$urut_masuk=$_POST['urut_masuk'];
			
				
					$query=$this->db->query(" update mr_obat set jumlah=jumlah-$jml 
					where 
					kd_pasien='$kd_pasien'	
					and kd_unit='$kd_unit'
					and tgl_masuk='$tgl_masuk'
					and urut_masuk='$urut_masuk'
					and kd_prd='$kd_prd'
					");

			
				
			}
			
			
		if($query)
		{
		echo "{success:true}";
		}else
		{
		echo "{success:false}";
		}
		
	}	
	
	
	public function retur_mrobat()
	{

		$jmllist=$_POST['jumlah'];
		$no_transaksi=$_POST['no_transaksi'];
		$kd_kasir=$_POST['kd_kasir'];
		$Query=$this->db->query("select * from transaksi where no_transaksi='".$no_transaksi."' and kd_kasir='".$kd_kasir."' limit 1 ")->result();
		if (count($Query)===0)
		{
			echo "{success:false}";
		}else{
		
		foreach($Query as $line)
		{
		$kd_pasien=$line->kd_pasien;
		$kd_unit=$line->kd_unit;
		$tgl_masuk=$line->tgl_transaksi;
		$urut_masuk=$line->urut_masuk;		
				
		}
		for($i=0;$i<$jmllist;$i++)
			{
		
			$kd_prd=$_POST['kd_prd-'.$i];
			$jml=$_POST['qty-'.$i];
		
					$query=$this->db->query(" update mr_obat set jumlah=jumlah-$jml 
					where 
					kd_pasien='$kd_pasien'	
					and kd_unit='$kd_unit'
					and tgl_masuk='$tgl_masuk'
					and urut_masuk='$urut_masuk'
					and kd_prd='$kd_prd'
					");

			
				
			}
		if($query)
		{
		echo "{success:true}";
		}else
		{
		echo "{success:false}";
		}
		
		
		}

			
		
	}	
	
	
	public function retur_unpost_mrobat()
	{

		$jmllist=$_POST['jumlah'];
		$no_transaksi=$_POST['no_transaksi'];
		$kd_kasir=$_POST['kd_kasir'];
		$Query=$this->db->query("select * from transaksi where no_transaksi='".$no_transaksi."' and kd_kasir='".$kd_kasir."' limit 1 ")->result();
		if (count($Query)===0)
		{
			echo "{success:false}";
		}else{
		
		foreach($Query as $line)
		{
		$kd_pasien=$line->kd_pasien;
		$kd_unit=$line->kd_unit;
		$tgl_masuk=$line->tgl_transaksi;
		$urut_masuk=$line->urut_masuk;		
				
		}
		for($i=0;$i<$jmllist;$i++)
			{
		
			$kd_prd=$_POST['kd_prd-'.$i];
			$jml=$_POST['qty-'.$i];
		
					$query=$this->db->query(" update mr_obat set jumlah=jumlah+$jml 
					where 
					kd_pasien='$kd_pasien'	
					and kd_unit='$kd_unit'
					and tgl_masuk='$tgl_masuk'
					and urut_masuk='$urut_masuk'
					and kd_prd='$kd_prd'
					");

			
				
			}
		if($query)
		{
		echo "{success:true}";
		}else
		{
		echo "{success:false}";
		}
		
		
		}

			
		
	}	
	

}
?>