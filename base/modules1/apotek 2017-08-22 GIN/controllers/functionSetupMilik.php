<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupMilik extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getMilikGrid(){
		$milik=$_POST['text'];
		if($milik == ''){
			$criteria="";
		} else{
			$criteria=" WHERE milik like '".$milik."%' ";
		}
		$result=$this->db->query("SELECT kd_milik, milik  
									FROM apt_milik $criteria ORDER BY kd_milik	
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdMilik(){
		$query=$this->db->query("SELECT MAX(kd_milik) AS kd_milik FROM apt_milik")->row();
		$KdMilik=$query->kd_milik;
		
		if($KdMilik != '' || $KdMilik != null){
			$newKdMilik=$KdMilik + 1;
		} else{
			$newKdMilik=1;
		}
		return $newKdMilik;
	}

	public function save(){
		$KdMilik = $_POST['KdMilik'];
		$Milik = $_POST['Milik'];
		
		if($KdMilik == ''){ //data baru
			$Ubah=0;
			$newKdMilik=$this->getKdMilik();
			$save=$this->saveMilik($newKdMilik,$Milik,$Ubah);
			$kode=$newKdMilik;
		} else{
			$Ubah=1;
			$save=$this->saveMilik($KdMilik,$Milik,$Ubah);
			$kode=$KdMilik;
		}
				
		if($save=='Ok'){
			echo "{success:true, kdmilik:'$kode'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdMilik = $_POST['KdMilik'];
		
		$query = $this->db->query("DELETE FROM apt_milik WHERE kd_milik='$KdMilik' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM apt_milik WHERE kd_milik='$KdMilik'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveMilik($KdMilik,$Milik,$Ubah){
		$strError = "";
		
		if($Ubah == 0){ //data baru
			$data = array("kd_milik"=>$KdMilik,
							"milik"=>$Milik
			);
			
			$result=$this->db->insert('apt_milik',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('apt_milik',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("milik"=>$Milik);
			
			$criteria = array("kd_milik"=>$KdMilik);
			$this->db->where($criteria);
			$result=$this->db->update('apt_milik',$dataUbah);
			
			//-----------Ubah to sq1 server Database---------------//
			_QMS_update('apt_milik',$dataUbah,$criteria);
			//-----------akhir Ubah ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>