<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionTutupShift extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getcurrentshift(){
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$query=$this->db->query("select shift from apt_shift_rsj where kd_unit_far='".$kdUnitFar."'")->row();
		$shift=$query->shift;
		if($query){
			echo "{success:true, shift:'$shift'}";
		} else{
			echo "{success:false}";
		}
	}
	
	
	public function cekTransaksi(){		
		$tanggal = $_POST['tanggal'];
		$shiftKe = $_POST['shiftKe'];
		$shiftSelanjutnya = $_POST['shiftSelanjutnya'];
		$besok = date('Y-m-d', strtotime(' +1 day'));
		
		if($shiftKe == '3' || $shiftKe == 3){
			$tgl=" and tgl_out in('".$tanggal."','".$besok."')";
		} else{
			$tgl=" and tgl_out='".$tanggal."'";
		}
		
		//cek resep
		$qResep=$this->db->query("SELECT o.no_resep, o.tgl_out, o.kd_pasienapt, o.nmpasien, o.no_bukti, 
									o.returapt, o.tutup, o.kd_unit, u.nama_unit
								FROM apt_barang_out o
									LEFT JOIN unit u on o.kd_unit=u.kd_unit
								WHERE o.tutup=0 ".$tgl." and o.returapt=0 and shiftapt=".$shiftKe."
									ORDER BY tgl_out desc						
								")->result();
		//cek retur
		$qRetur=$this->db->query("SELECT o.no_resep, o.tgl_out, o.kd_pasienapt, o.nmpasien, o.no_bukti, 
									o.returapt, o.tutup, o.kd_unit, u.nama_unit
								FROM apt_barang_out o
									LEFT JOIN unit u on o.kd_unit=u.kd_unit
								WHERE o.tutup=0 ".$tgl." and o.returapt=1 and shiftapt=".$shiftKe."
									ORDER BY tgl_out desc						
								")->result();
		$data=array();
		$data['qResep']=$qResep;
		$data['qRetur']=$qRetur;
		$reqRes=count($qResep);
		$reqRet=count($qRetur);
		$req=$reqRes+$reqRet;
		
		if(count($qRetur) > 0 || count($qResep) > 0 ){
			$pesan='Ok';
			echo'{success:true,totalrecords:'.$req.', ListDataObj:'.json_encode($data).',pesan:"$pesan"}';
		} else{
			echo"{success:true, pesan:'Tidak ada transaksi yg belum ditutup'}";
		}
		
	}
	
	function tutupShift(){
		$tanggal = $_POST['tanggal'];
		$shiftKe = $_POST['shiftKe'];
		$shiftSelanjutnya = $_POST['shiftSelanjutnya'];
		$kdUnitFar=$this->session->userdata['user_id']['aptkdunitfar'];
		$besok = date('Y-m-d', strtotime(' +1 day'));
		
		if($shiftKe == '3' || $shiftKe == 3){
			$tgl=" and tgl_out in('".$tanggal."','".$besok."')";
		} else{
			$tgl=" and tgl_out='".$tanggal."'";
		}
		
		//cek resep
		$qResep=$this->db->query("SELECT o.no_resep, o.tgl_out, o.kd_pasienapt, o.nmpasien, o.no_bukti, 
									o.returapt, o.tutup, o.kd_unit, u.nama_unit
								FROM apt_barang_out o
									LEFT JOIN unit u on o.kd_unit=u.kd_unit
								WHERE o.tutup=0 ".$tgl." and o.returapt=0 and shiftapt=".$shiftKe."
									ORDER BY tgl_out desc						
								")->result();
		//cek retur
		$qRetur=$this->db->query("SELECT o.no_resep, o.tgl_out, o.kd_pasienapt, o.nmpasien, o.no_bukti, 
									o.returapt, o.tutup, o.kd_unit, u.nama_unit
								FROM apt_barang_out o
									LEFT JOIN unit u on o.kd_unit=u.kd_unit
								WHERE o.tutup=0 ".$tgl." and o.returapt=1 and shiftapt=".$shiftKe."
									ORDER BY tgl_out desc						
								")->result();
								
		if(count($qRetur) > 0 || count($qResep) > 0){
			echo "{success:false, pesan:'Transaksi belum posting'}";
		} else{
			$data = array("shift"=>$shiftSelanjutnya,"lastdate"=>$tanggal);
			$criteria = array("kd_unit_far"=>$kdUnitFar);
			$this->db->where($criteria);
			$query=$this->db->update('apt_shift_rsj',$data);
			
			//-----------insert to sq1 server Database---------------//
			// _QMS_update('apt_shift_rsj',$data,$criteria);
			//-----------akhir insert ke database sql server----------------//
			
			if($query){
				echo '{success:true}';
			}else{
				echo "{success:false, pesan:'Error'}";
			}
		}
	}


}
?>