<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_kasir_summary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	
   	public function preview(){
		$common = $this->common;
		$param = json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 17);
		//print_r ($Split);
		/* //all shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
	/* 	//1 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1 */
		
		/* //2 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2	 */	
		
		/* //3 shift
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => RWJ/IGD
		[5] => Unit
		[6] => APOTIK
		[7] => APT
		[8] => operator
		[9] => Administrator
		[10] => 0
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
		/* //semua
		[0] => Tanggal
		[1] => 07/Aug/2015
		[2] => 07/Aug/2015
		[3] => asal pasien
		[4] => Semua
		[5] => Unit
		[6] => Semua
		[7] => Semua
		[8] => operator
		[9] => Semua
		[10] => Semua
		[11] => shift1
		[12] => 1
		[13] => shift2
		[14] => 2
		[15] => shift3
		[16] => 3 */
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$asalpasien = $Split[4];
			$unit = $Split[6];
			$user = $Split[9];
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
					
			/* where tgl_out between '' and ''
				and kd_unit=''
				and kd_unit_far=''
				and opr=
			ok	and shiftapt= */
			
			//-------------------------------- jumlah shift 1 -----------------------------------------------------------------------------------------------
			if (count($Split) === 13) //1 shif yang di pilih
			{
				if ($Split[12] == 3) {
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")) 
									   or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11];
				} else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")))";//untuk selain shif 4
					$shift = $Split[11];
				}

			//-------------------------------- jumlah shift 2 -----------------------------------------------------------------------------------------------
			} else if(count($Split) === 15){
				if ($Split[12] === 3 or $Split[14] === 3)
				{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")))";//untuk selain shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}

			//-------------------------------- jumlah shift 3 -----------------------------------------------------------------------------------------------	
			} else if(count($Split) === 17){ 
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].",".$Split[16].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = 'Semua Shift';
			}
			
			//----------------- asal pasien --------------------------------
			if($asalpasien == 'Semua'){
				$kriteriaUnit ="";
			} else if($asalpasien == 'RWJ/IGD'){
				$kriteriaUnit=" AND left(bo.kd_unit,1) in('2','3') ";
			} else if($asalpasien == 'RWI'){
				$kriteriaUnit=" AND left(bo.kd_unit,1)='1' ";
			}
			
			//------------------- user/operator ------------------------------
			if($user == 'Semua'){
				$kd_user="";
			} else{
				$kd_user=" AND bo.opr=".$Split[10]."";
			}
			
			//------------------ unit far -------------------------------
			if($unit == 'Semua'){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND bo.kd_unit_far='".$Split[7]."'";
			}
		}

	
		$queryHasil = $this->db->query( " SELECT 	Sum(Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End) as Sub_Jumlah, 
											Sum(Case When returapt=0 then Discount Else (-1)* Discount End) as Discount, 
											sum(Jasa) as Tuslah, sum(AdmRacik) as AdmRacik, Sum(admnci+AdmNCI_Racik) as Adm_NCI, sum(Tunai) as Tunai, Sum(Transfer) as Transfer, 
											sum(Kredit) as Kredit, sum(ITU) as ItemTunai, sum(ICH) as ItemTransfer, sum(ICB) as ItemKredit, 
											Sum(Case When returapt=0 then Jml_Bayar Else (-1)* Jml_Bayar End) as Jumlah, Sum(ITU)+Sum(ICH)+Sum(ICB) as ItemJumlah, 
											Sum(Tunai)+Sum(Transfer)+Sum(Kredit)- (Sum(Jasa)+Sum(AdmRacik)+Sum(admnci+AdmNCI_Racik)+Sum(Case When returapt=0 then Jml_Obat 
											Else (-1)* Jml_Obat End)- Sum(Case When returapt=0 then Discount Else (-1)* Discount End)) as Pembulatan 
										FROM Apt_Barang_Out bo 
											INNER JOIN 
												(
												SELECT Tgl_Out, No_Out, Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as Tunai, 
													sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit, 
													Sum(Case When Type_Data in (0,1) then 1 Else 0 End) as ITU, Sum(Case When Type_Data=2 then 1 Else 0 End) as ICH, 
													Sum(Case When Type_Data not In (0, 1, 2) then 1 Else 0 End) as ICB 
												FROM 
													(
													SELECT Tgl_Out, No_Out, Type_data, Case when DB_CR='f' Then Jumlah Else (-1)*Jumlah End as Jumlah
													FROM apt_Detail_Bayar db 
														INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE ".$ParamShift."  
													) det 
													GROUP BY Tgl_Out, No_Out
												) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::CHARACTER VARYING=y.No_Out
												WHERE tutup=1 
												".$kd_unit_far."
												".$kd_user."
												".$kriteriaUnit." ");
		$query = $queryHasil->result();
		// echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		
		 $html='';
		$html.="<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN SUMMARY KASIR</th>
						</tr>
						<tr>
							<th>".$tglAwal." s/d ".$tglAkhir."</th>
						</tr>
						<tr>
							<th>UNIT RAWAT : ".$asalpasien."</th>
						</tr>
						<tr>
							<th>".$unit."</th>
						</tr>
						<tr>
							<th align='left'>Kasir : ".$user."</th>
						</tr>
						<tr>
							<th align='left'>Shift : ".$shift."</th>
						</tr>
					</tbody>
				</table><br>";
		
		$html.='<table class="t1" style="overflow: wrap" cellpadding="3" >';
		if(count($query) == 0)
		{
			$html.="<tr>
						<td colspan='4' align='center'>Data tidak ada.</td>
					</tr>";
		}else 
		{									
			
			foreach ($query as $line) 
			{
				$html.="<tr>
							<td style='border-top:1px solid ;border-left:1px solid;' width='100'><b>Transaksi</b></td>
							<td style='border-top:1px solid' width='50'>:</td>
							<td style='border-top:1px solid; border-right:1px solid;' colspan='2' align='left' width='100'>".number_format($line->sub_jumlah,0, "." , ".")."</td>
						</tr>
						<tr>
							<td style='border-left:1px solid'><b>Discount</b></td>
							<td>:</td>
							<td colspan='2' align='left' style='border-right:1px solid;'>".number_format($line->discount,0, "." , ".")."</td>
							
						</tr>
						<tr>
							<td style='border-left:1px solid'><b>Tuslah</b></td>
							<td>:</td>
							<td colspan='2' align='left'  style='border-right:1px solid;'>".number_format($line->tuslah,0, "." , ".")."</td>
						</tr>
						<tr>
							<td style='border-left:1px solid'><b>Adm. Racik</b></td>
							<td>:</td>
							<td colspan='2' align='left'  style='border-right:1px solid;'>".number_format($line->admracik,0, "." , ".")."</td>
						</tr>
						<tr>
							<td style='border-left:1px solid'><b>Adm. NCI</b></td>
							<td>:</td>
							<td colspan='2' align='left'  style='border-right:1px solid;'>".number_format($line->adm_nci,0, "." , ".")."</td>
						</tr>
						<tr>
							<td style='border-left:1px solid; border-top:1px solid;'><b>Tunai</b></td>
							<td style='border-top:1px solid;'>:</td>
							<td style='border-top:1px solid;'>".number_format($line->tunai,0, "." , ".")."</td>
							<td style='border-right:1px solid; border-top:1px solid;' >".number_format($line->itemtunai,0, "." , ".")." Transaksi</td>
						</tr>
						<tr>
							<td style='border-left:1px solid'><b>Transfer</b></td>
							<td>:</td>
							<td >".number_format($line->transfer,0, "." , ".")."</td>
							<td  style='border-right:1px solid;'>".number_format($line->itemtransfer,0, "." , ".")." Transaksi</td>
						</tr>
						<tr>
							<td style='border-left:1px solid'><b>Kredit</b></td>
							<td>:</td>
							<td >".number_format($line->kredit,0, "." , ".")."</td>
							<td  style='border-right:1px solid;'>".number_format($line->itemkredit,0, "." , ".")." Transaksi</td>
						</tr>
						<tr>
							<td style='border-left:1px solid; border-bottom:1px solid; border-top:1px solid;'><b>Jumlah</b></td>
							<td style='border-bottom:1px solid; border-top:1px solid;'>:</td>
							<td style='border-bottom:1px solid; border-top:1px solid;'>".number_format($line->jumlah,0, "." , ".")."</td>
							<td style='border-right:1px solid; border-bottom:1px solid; border-top:1px solid;'>".number_format($line->itemjumlah,0, "." , ".")." Transaksi</td>
						</tr>";
				
			}
			
			$html.='</tbody></table>';
		}
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN KASIR SUMMARY',$html); 
		echo $html;
	}

	public function print_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 17);
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,4,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$asalpasien = $Split[4];
			$unit = $Split[6];
			$user = $Split[9];
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
					
			/* where tgl_out between '' and ''
				and kd_unit=''
				and kd_unit_far=''
				and opr=
			ok	and shiftapt= */
			
			//-------------------------------- jumlah shift 1 -----------------------------------------------------------------------------------------------
			if (count($Split) === 13) //1 shif yang di pilih
			{
				if ($Split[12] == 3) {
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")) 
									   or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11];
				} else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].")))";//untuk selain shif 4
					$shift = $Split[11];
				}

			//-------------------------------- jumlah shift 2 -----------------------------------------------------------------------------------------------
			} else if(count($Split) === 15){
				if ($Split[12] === 3 or $Split[14] === 3)
				{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}else{
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].")))";//untuk selain shif 4
					$shift = $Split[11].' Dan '.$Split[13];
				}

			//-------------------------------- jumlah shift 3 -----------------------------------------------------------------------------------------------	
			} else if(count($Split) === 17){ 
					$ParamShift = " ((tgl_bayar BETWEEN '".$tglAwal."' AND '".$tglAkhir."' AND shift in (".$Split[12].",".$Split[14].",".$Split[16].")) 
									  or (tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' AND shift=4))";//untuk shif 4
					$shift = 'Semua Shift';
			}
			
			//----------------- asal pasien --------------------------------
			if($asalpasien == 'Semua'){
				$kriteriaUnit ="";
			} else if($asalpasien == 'RWJ/IGD'){
				$kriteriaUnit=" AND left(bo.kd_unit,1) in('2','3') ";
			} else if($asalpasien == 'RWI'){
				$kriteriaUnit=" AND left(bo.kd_unit,1)='1' ";
			}
			
			//------------------- user/operator ------------------------------
			if($user == 'Semua'){
				$q_kd_user="";
			} else{
				$q_kd_user=" AND bo.opr=".$Split[10]."";
			}
			
			//------------------ unit far -------------------------------
			if($unit == 'Semua'){
				$kd_unit_far="";
			} else{
				$kd_unit_far=" AND bo.kd_unit_far='".$Split[7]."'";
			}
		}

		$queryHasil = $this->db->query( " SELECT 	Sum(Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End) as Sub_Jumlah, 
											Sum(Case When returapt=0 then Discount Else (-1)* Discount End) as Discount, 
											sum(Jasa) as Tuslah, sum(AdmRacik) as AdmRacik, Sum(admnci+AdmNCI_Racik) as Adm_NCI, sum(Tunai) as Tunai, Sum(Transfer) as Transfer, 
											sum(Kredit) as Kredit, sum(ITU) as ItemTunai, sum(ICH) as ItemTransfer, sum(ICB) as ItemKredit, 
											Sum(Case When returapt=0 then Jml_Bayar Else (-1)* Jml_Bayar End) as Jumlah, Sum(ITU)+Sum(ICH)+Sum(ICB) as ItemJumlah, 
											Sum(Tunai)+Sum(Transfer)+Sum(Kredit)- (Sum(Jasa)+Sum(AdmRacik)+Sum(admnci+AdmNCI_Racik)+Sum(Case When returapt=0 then Jml_Obat 
											Else (-1)* Jml_Obat End)- Sum(Case When returapt=0 then Discount Else (-1)* Discount End)) as Pembulatan 
										FROM Apt_Barang_Out bo 
											INNER JOIN 
												(
												SELECT Tgl_Out, No_Out, Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as Tunai, 
													sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit, 
													Sum(Case When Type_Data in (0,1) then 1 Else 0 End) as ITU, Sum(Case When Type_Data=2 then 1 Else 0 End) as ICH, 
													Sum(Case When Type_Data not In (0, 1, 2) then 1 Else 0 End) as ICB 
												FROM 
													(
													SELECT Tgl_Out, No_Out, Type_data, Case when DB_CR='f' Then Jumlah Else (-1)*Jumlah End as Jumlah
													FROM apt_Detail_Bayar db 
														INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE ".$ParamShift."  
													) det 
													GROUP BY Tgl_Out, No_Out
												) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::CHARACTER VARYING=y.No_Out 
												WHERE tutup=1 
												".$kd_unit_far."
												".$q_kd_user."
												".$kriteriaUnit."
										");

		$query = $queryHasil->result();
		// echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		
		 # SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 5)
			->setColumnLength(2, 15)
			->setColumnLength(3, 15)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 4,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 4,"left")
			->commit("header")
			->addColumn($telp, 4,"left")
			->commit("header")
			->addColumn($fax, 4,"left")
			->commit("header")
			->addColumn("", 4,"center")
			->commit("header")
			->addColumn("LAPORAN SUMMARY KASIR", 4,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($date1))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($date2))), 4,"center")
			->commit("header")
			->addColumn("UNIT RAWAT :".$asalpasien, 4,"center")
			->commit("header")
			->addColumn($unit, 4,"center")
			->commit("header")
			->addColumn("Kasir : ".$user, 4,"left")
			->commit("header")
			->addColumn("Shift : ".$shift, 4,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		
		if(count($query) == 0)
		{
			$tp	->addColumn("Data tidak ada", 4,"center")
				->commit("header");
		}else 
		{									
			
			foreach ($query as $line) 
			{
				$tp	->addColumn("Transaksi", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->sub_jumlah,0, "." , "."), 2,"left")
					->commit("header");
				$tp	->addColumn("Discount", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->discount,0, "." , "."), 2,"left")
					->commit("header");
				$tp	->addColumn("Tuslah", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->tuslah,0, "." , "."), 2,"left")
					->commit("header");
				$tp	->addColumn("Adm. Racik", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->admracik,0, "." , "."), 2,"left")
					->commit("header");	
				$tp	->addColumn("Adm. NCI", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->adm_nci,0, "." , "."), 2,"left")
					->commit("header");	
				$tp	->addColumn("Tunai", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->tunai,0, "." , "."), 1,"left")
					->addColumn(number_format($line->itemtunai,0, "." , ".")." Transaksi", 1,"right")
					->commit("header");	
				$tp	->addColumn("Transfer", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->transfer,0, "." , "."), 1,"left")
					->addColumn(number_format($line->itemtransfer,0, "." , ".")." Transaksi", 1,"right")
					->commit("header");	
				$tp	->addColumn("Kredit", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->kredit,0, "." , "."), 1,"left")
					->addColumn(number_format($line->itemkredit,0, "." , ".")." Transaksi", 1,"right")
					->commit("header");
				$tp	->addColumn("Jumlah", 1,"left")
					->addColumn(":", 1,"left")
					->addColumn(number_format($line->jumlah,0, "." , "."), 1,"left")
					->addColumn(number_format($line->itemjumlah,0, "." , "."). " Transaksi", 1,"right")
					->commit("header");
			}
			
		}
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 2,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		
		# End Data
		
		$file =  '/home/tmp/data_kasirsummary.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file); 
   	}
	
}
?>