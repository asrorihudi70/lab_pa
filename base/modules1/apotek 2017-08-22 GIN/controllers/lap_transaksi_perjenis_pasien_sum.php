<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_transaksi_perjenis_pasien_sum extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$array['customer']=$this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
												kontraktor B ON B.kd_customer=A.kd_customer  ORDER BY customer ASC ")->result();
		$array['unit_rawat']=$this->db->query("SELECT kd_unit AS id,nama_unit AS text FROM unit where kd_unit in('1','2','3') ORDER BY nama_unit ASC")->result();										
												
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getCustomer(){
   		
   		$result=$this->result;
   		$data=$this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
   		kontraktor B ON B.kd_customer=A.kd_customer WHERE UPPER(A.kd_customer) LIKE UPPER('%".$_POST['text']."%') OR UPPER(customer) LIKE UPPER('%".$_POST['text']."%') 
   				ORDER BY customer ASC LIMIT 10")->result();
   		$result->setData($data);
   		$result->end();
   	}
	
	public function preview(){
		$html='';
		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 16);
		// print_r ($Split);
		
		/* //3 shift
		[0] => Operator
		[1] => 0
		[2] => unit_rawat
		[3] => 1
		[4] => unit
		[5] => APT
		[6] => jenis_pasien
		[7] => 0000000001
		[8] => start_date
		[9] => 2015-8-7
		[10] => last_date
		[11] => 2015-8-7
		[12] => shift1
		[13] => true
		[14] => shift2
		[15] => true
		[16] => shift3
		[17] => true */
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[5];
			$tglAkhir = $Split[7];
			//$asalpasien = $Split[4];
			$kd_unit_far = $Split[3];
			$kd_user = $Split[1];
			//$kd_customer = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit rawat-----------------------------
		
			$arrayDataUR = $param->tmp_unit_rawat;
			$tmpkdUR ='';
			$tmp_t_kdUR ='';
			for ($i=0; $i < count($arrayDataUR); $i++) { 
				$tmp_t_kdUR.= $arrayDataUR[$i][0];
				$tmp_t_kdUR.= ",";
				$unit_rawat=$this->db->query("SELECT kd_unit FROM unit WHERE nama_unit='".$arrayDataUR[$i][0]."'")->row()->kd_unit;
				$tmpkdUR .= "'".$unit_rawat."',";
			}
			$tmpkdUR = substr($tmpkdUR, 0, -1);
			$tmp_t_kdUR = substr($tmp_t_kdUR, 0, -1);
			
			$qr_ur=" AND left(kd_unit,1) in (".$tmpkdUR.") ";
			//------------------------ kriteria jenis pasien -----------------------------
			
			$arrayDataJP = $param->tmp_jenis_pasien;
			$tmpkdJP ='';
			$tmp_t_kdJP ='';
			for ($i=0; $i < count($arrayDataJP); $i++) { 
				$tmp_t_kdJP.= $arrayDataJP[$i][0];
				$tmp_t_kdJP.= ",";
				$customer=$this->db->query("SELECT kd_customer FROM customer WHERE customer='".$arrayDataJP[$i][0]."'")->row()->kd_customer;
				$tmpkdJP .= "'".$customer."',";
			}
			$tmpkdJP = substr($tmpkdJP, 0, -1);
			$tmp_t_kdJP = substr($tmp_t_kdJP, 0, -1);
			
			$qr_jp=" AND c.kd_customer in (".$tmpkdJP.") ";
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user=" AND bo.opr='".$kd_user."' ";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
				$unit="";
			} else{
				$kd_unit_far=" AND bo.kd_unit_far='".$kd_unit_far."'";
				$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->kd_unit_far;
			}
			
			
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[9] == 'true' && $Split[11] == 'true' && $Split[13] == 'true'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2,3) 
								or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'true' && $Split[11] == 'true' && $Split[9] == 'false'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2,3) 
									or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[13] == 'true' && $Split[11] == 'false' && $Split[9] == 'true'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,3) 
									or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'false' && $Split[11] == 'true' && $Split[9] == 'true'){
				$ParamShift = "tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'false' && $Split[11] == 'false' && $Split[9] == 'true'){
				$ParamShift = "tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[13] == 'false' && $Split[11] == 'true' && $Split[9] == 'false'){
				$ParamShift = "tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[13] == 'true' && $Split[11] == 'false' && $Split[9] == 'false'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(3) 
								or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";
				$shift = '3';
			} 
		}

	
		$queryHasil = $this->db->query( " 	SELECT bo.kd_customer, max(customer) as customer, Sum(Case When returapt=0 then Jml_Obat Else  Jml_Obat End) AS Sub_Jumlah, 
												Sum(Discount) As Discount, Sum(Jasa) as Tuslah, sum(AdmRacik) as AdmRacik, Sum(AdmNCI+AdmNCI_Racik) as Adm_NCI, 
												Sum(Tunai) As Tunai, Sum(Kredit) As Kredit, Sum(Transfer) As Transfer 
											FROM Apt_Barang_Out bo 
												INNER JOIN customer c ON bo.kd_customer=c.kd_customer 
												inner JOIN --hrsnya inner join
													(
													SELECT Tgl_Out, No_Out, Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as Tunai, 
														sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, 
														sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit 
													FROM 
														(
														SELECT Tgl_Out, No_Out, Type_data, Case when DB_CR='f' Then Jumlah Else Jumlah End as Jumlah
														FROM apt_Detail_Bayar db 
															INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE (".$ParamShift.") 
														) det 
														GROUP BY Tgl_Out, No_Out
													) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out 
													WHERE tutup=1 
														".$kd_unit_far."
														".$kd_user."
														".$qr_ur."
														".$qr_jp."
													GROUP BY bo.kd_customer 
													ORDER BY customer ");
		$query = $queryHasil->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		$html.="
				<table  cellspacing='0' border='0'>
					<tbody>
						<tr>
							<th>LAPORAN TRANSAKSI PER JENIS PASIEN (SUMMARY)</th>
						</tr>
						<tr>
							<th>".$tmptglawal." s/d ".$tmptglakhir."</th>
						</tr>
						<tr>
							<th>Unit Rawat : ".$tmp_t_kdUR."</th>
						</tr>
						<tr>
							<th>Jenis Pasien: ".$tmp_t_kdJP."</th>
						</tr>
						<tr>
							<th align='left'>Kasir : ".$user."</th>
						</tr>
						<tr>
							<th align='left'>Shift : ".$shift."</th>
						</tr>
					</tbody>
				</table><br>";
			
		if(count($query) == 0)
		{
			$html.="<tr>
						<th colspan='11' align='center'>Data tidak ada</th>
					</tr>";
		}
		else {									
			$html.='<table class="t1" border = "1">
					<thead>
					  <tr>
							<th width ="20" align="center">No</td>
							<th width ="20" align="center">Kode</td>
							<th width ="40" align="center">Jenis Pasien</td>
							<th width ="30" align="center">Transaksi</td>
							<th width ="30" align="center">Discount</td>
							<th width ="30" align="center">Tuslah</td>
							<th width ="30" align="center">Racik</td>
							<th width ="30" align="center">Adm NCI</td>
							<th width ="30" align="center">Tunai</td>
							<th width ="30" align="center">Transfer</td>
							<th width ="30" align="center">Kredit</td>
					  </tr>
					</thead>';
					$no=1;
					$grand=0;
					$customer='';
					
					$grand_sub_jumlah=0; 
					$grand_discount=0; 
					$grand_tuslah=0; 
					$grand_admracik=0; 
					$grand_adm_nci=0; 
					$grand_tunai=0; 
					$grand_transfer=0; 
					$grand_kredit=0;
					foreach ($query as $line) 
					{
						
						$html.='<tr > 
									<td align="center" >'.$no.'.</td>
									<td >'.$line->kd_customer.'</td>
									<td >'.$line->customer.'</td>
									<td align="right">'.number_format($line->sub_jumlah,0,',','.').'</td>
									<td align="right">'.number_format($line->discount,0,',','.').'</td>
									<td align="right">'.number_format($line->tuslah,0,',','.').'</td>
									<td align="right">'.number_format($line->admracik,0,',','.').'</td>
									<td align="right">'.number_format($line->adm_nci,0,',','.').'</td>
									<td align="right">'.number_format($line->tunai,0,',','.').'</td>
									<td align="right">'.number_format($line->transfer,0,',','.').'</td>
									<td align="right">'.number_format($line->kredit,0,',','.').'</td>
								</tr>';
						$no++;
						
						
						/*----------------------- GRAND TOTAL ----------------------------------*/
						$grand_sub_jumlah = $grand_sub_jumlah + $line->sub_jumlah; 
						$grand_discount = $grand_discount + $line->discount; 
						$grand_tuslah = $grand_tuslah +  $line->tuslah; 
						$grand_admracik = $grand_admracik +  $line->admracik; 
						$grand_adm_nci = $grand_adm_nci + $line->adm_nci; 
						$grand_tunai = $grand_tunai + $line->tunai; 
						$grand_transfer = $grand_transfer + $line->transfer; 
						$grand_kredit = $grand_kredit + $line->kredit;
					}
				
					$html.="<tr>
								<td colspan='3' align='right'><b>Grand Total</b></td>
								<td align='right'>".number_format($grand_sub_jumlah,0,',','.')."</td>
								<td align='right'>".number_format($grand_discount,0,',','.')."</td>
								<td align='right'>".number_format($grand_tuslah,0,',','.')."</td>
								<td align='right'>".number_format($grand_admracik,0,',','.')."</td>
								<td align='right'>".number_format($grand_adm_nci,0,',','.')."</td>
								<td align='right'>".number_format($grand_tunai,0,',','.')."</td>
								<td align='right'>".number_format($grand_transfer,0,',','.')."</td>
								<td align='right'>".number_format($grand_kredit,0,',','.')."</td>
							</tr>";
			
		}		
		$html.="</table>";
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN TRANSAKSI PER JENIS PASIEN (SUMMARY)',$html);
		echo $html;  

	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
   		$param=json_decode($_POST['data']);
		$Params = $param->criteria;
		$Split = explode("##@@##", $Params, 14);
		$kd_user_p=$this->session->userdata['user_id']['id'];
		$user_p=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_p."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,11,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		if (count($Split) > 0 ){
			$tglAwal = $Split[5];
			$tglAkhir = $Split[7];
			//$asalpasien = $Split[4];
			$kd_unit_far = $Split[3];
			$kd_user = $Split[1];
			//$kd_customer = $Split[7];
			
			$date1 = str_replace('/', '-', $tglAwal);
			$date2 = str_replace('/', '-', $tglAkhir);
			$tmptglawal=date('d-M-Y',strtotime($tglAwal));
			$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
			$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
			$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
			
			if($kd_unit_far == ''){
				$unit='SEMUA UNIT';
			} else{
				$unit = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
			}
			
			if($kd_user == ''){
				$user = 'SEMUA OPERATOR';
			} else{
				$user = $this->db->query("select full_name from zusers where kd_user='".$kd_user."'")->row()->full_name;
			}
			
			//------------------------ kriteria unit rawat-----------------------------
		
			$arrayDataUR = $param->tmp_unit_rawat;
			$tmpkdUR ='';
			$tmp_t_kdUR ='';
			for ($i=0; $i < count($arrayDataUR); $i++) { 
				$tmp_t_kdUR.= $arrayDataUR[$i][0];
				$tmp_t_kdUR.= ",";
				$unit_rawat=$this->db->query("SELECT kd_unit FROM unit WHERE nama_unit='".$arrayDataUR[$i][0]."'")->row()->kd_unit;
				$tmpkdUR .= "'".$unit_rawat."',";
			}
			$tmpkdUR = substr($tmpkdUR, 0, -1);
			$tmp_t_kdUR = substr($tmp_t_kdUR, 0, -1);
			
			$qr_ur=" AND left(kd_unit,1) in (".$tmpkdUR.") ";
			//------------------------ kriteria jenis pasien -----------------------------
			
			$arrayDataJP = $param->tmp_jenis_pasien;
			$tmpkdJP ='';
			$tmp_t_kdJP ='';
			for ($i=0; $i < count($arrayDataJP); $i++) { 
				$tmp_t_kdJP.= $arrayDataJP[$i][0];
				$tmp_t_kdJP.= ",";
				$customer=$this->db->query("SELECT kd_customer FROM customer WHERE customer='".$arrayDataJP[$i][0]."'")->row()->kd_customer;
				$tmpkdJP .= "'".$customer."',";
			}
			$tmpkdJP = substr($tmpkdJP, 0, -1);
			$tmp_t_kdJP = substr($tmp_t_kdJP, 0, -1);
			
			$qr_jp=" AND c.kd_customer in (".$tmpkdJP.") ";
			//------------------------ kriteria user -----------------------------
			if($kd_user == ''){
				$kd_user="";
			} else{
				$kd_user=" AND bo.opr='".$kd_user."' ";
			}
			
			//------------------------ kriteria unit far -----------------------------
			if($kd_unit_far==''){
				$kd_unit_far="";
				$unit="";
			} else{
				$kd_unit_far=" AND bo.kd_unit_far='".$kd_unit_far."'";
				$unit=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->kd_unit_far;
			}
			
			
			//------------------------ kriteria shift-----------------------------
			//-------------------------------- semua -----------------------------------------------------------------------------------------------
			if($Split[9] == 'true' && $Split[11] == 'true' && $Split[13] == 'true'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2,3) 
								or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = 'Semua Shift';
			//-------------------------------- shift 3, shift 2-----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'true' && $Split[11] == 'true' && $Split[9] == 'false'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2,3) 
									or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '2 Dan 3';
			//-------------------------------- shift 3, shift 1 -----------------------------------------------------------------------------------------------		
			} else if ($Split[13] == 'true' && $Split[11] == 'false' && $Split[9] == 'true'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,3) 
									or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";//untuk shif 4
				$shift = '1 Dan 3';
			//-------------------------------- shift 1, shift 2 -----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'false' && $Split[11] == 'true' && $Split[9] == 'true'){
				$ParamShift = "tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1,2)";//untuk shif 4
				$shift = '1 Dan 2';
			
			//-------------------------------- shift 1 -----------------------------------------------------------------------------------------------	
			} else if ($Split[13] == 'false' && $Split[11] == 'false' && $Split[9] == 'true'){
				$ParamShift = "tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(1)";
				$shift = '1 ';
				
			//-------------------------------- shift 2 -----------------------------------------------------------------------------------------------
			} else if ($Split[13] == 'false' && $Split[11] == 'true' && $Split[9] == 'false'){
				$ParamShift = "tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(2)";
				$shift = '2';
				
			//-------------------------------- shift 3 -----------------------------------------------------------------------------------------------
			} else if ($Split[13] == 'true' && $Split[11] == 'false' && $Split[9] == 'false'){
				$ParamShift = "(tgl_bayar BETWEEN '".$date1."' AND '".$date2."' and shift in(3) 
								or tgl_bayar BETWEEN '".$tomorrow."' AND '".$tomorrow2."' and shift=4 )";
				$shift = '3';
			} 
		}
		
		$queryHead = $this->db->query( "SELECT bo.kd_customer, max(customer) as customer, Sum(Case When returapt=0 then Jml_Obat Else  Jml_Obat End) AS Sub_Jumlah, 
												Sum(Discount) As Discount, Sum(Jasa) as Tuslah, sum(AdmRacik) as AdmRacik, Sum(AdmNCI+AdmNCI_Racik) as Adm_NCI, 
												Sum(Tunai) As Tunai, Sum(Kredit) As Kredit, Sum(Transfer) As Transfer 
											FROM Apt_Barang_Out bo 
												INNER JOIN customer c ON bo.kd_customer=c.kd_customer 
												inner JOIN --hrsnya inner join
													(
													SELECT Tgl_Out, No_Out, Sum(Case When Type_Data in (0,1) Then Jumlah Else 0 End) as Tunai, 
														sum(Case When Type_Data=2 Then Jumlah Else 0 End) as Transfer, 
														sum(Case When Type_Data not In (0, 1, 2) Then Jumlah Else 0 End) as Kredit 
													FROM 
														(
														SELECT Tgl_Out, No_Out, Type_data, Case when DB_CR='f' Then Jumlah Else Jumlah End as Jumlah
														FROM apt_Detail_Bayar db 
															INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay 
														WHERE (".$ParamShift.") 
														) det 
														GROUP BY Tgl_Out, No_Out
													) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out::character varying=y.No_Out 
													WHERE tutup=1 
														".$kd_unit_far."
														".$kd_user."
														".$qr_ur."
														".$qr_jp."
													GROUP BY bo.kd_customer 
													ORDER BY customer
										 ");
		$query = $queryHead->result();
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 11)
			->setColumnLength(2, 15)
			->setColumnLength(3, 11)
			->setColumnLength(4, 11)
			->setColumnLength(5, 11)
			->setColumnLength(6, 11)
			->setColumnLength(7, 11)
			->setColumnLength(8, 11)
			->setColumnLength(9, 11)
			->setColumnLength(10, 11)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 11,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 11,"left")
			->commit("header")
			->addColumn($telp, 11,"left")
			->commit("header")
			->addColumn($fax, 11,"left")
			->commit("header")
			->addColumn("LAPORAN TRANSAKSI PER JENIS PASIEN (SUMMARY)", 11,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($date1))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($date2))), 11,"center")
			->commit("header")
			->addColumn("UNIT RAWAT : ".$tmp_t_kdUR , 11,"center")
			->commit("header")
			->addColumn("JENIS PASIEN: ".$tmp_t_kdJP , 11,"center")
			->commit("header")
			->addColumn("Kasir : ".$user , 11,"left")
			->commit("header")
			->addColumn("Shift : ".$shift , 11,"left")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Kode", 1,"left")
			->addColumn("Jenis Pasien ", 1,"left")
			->addColumn("Transaksi", 1,"right")
			->addColumn("Discount", 1,"right")
			->addColumn("Tuslah", 1,"right")
			->addColumn("Racik", 1,"right")
			->addColumn("Adm NCI", 1,"right")
			->addColumn("Tunai ", 1,"right")
			->addColumn("Transfer ", 1,"right")
			->addColumn("Kredit ", 1,"right")
			->commit("header");
			 
		if(count($query) > 0) {
			$no=1;
			$grand=0;
			$customer='';
			$grand_sub_jumlah=0; 
			$grand_discount=0; 
			$grand_tuslah=0; 
			$grand_admracik=0; 
			$grand_adm_nci=0; 
			$grand_tunai=0; 
			$grand_transfer=0; 
			$grand_kredit=0;
			foreach ($query as $line) 
			{
				$tp	->addColumn($no.".", 1,"right")
					->addColumn($line->kd_customer, 1,"left")
					->addColumn($line->customer, 1,"left")
					->addColumn(number_format($line->sub_jumlah,0,',','.'), 1,"right")
					->addColumn(number_format($line->discount,0,',','.'), 1,"right")
					->addColumn(number_format($line->tuslah,0,',','.'), 1,"right")
					->addColumn(number_format($line->admracik,0,',','.'), 1,"right")
					->addColumn(number_format($line->adm_nci,0,',','.'), 1,"right")
					->addColumn(number_format($line->tunai,0,',','.'), 1,"right")
					->addColumn(number_format($line->transfer,0,',','.'), 1,"right")
					->addColumn(number_format($line->kredit,0,',','.'), 1,"right")
					->commit("header");
				$no++;
				$grand_sub_jumlah = $grand_sub_jumlah + $line->sub_jumlah; 
				$grand_discount = $grand_discount + $line->discount; 
				$grand_tuslah = $grand_tuslah +  $line->tuslah; 
				$grand_admracik = $grand_admracik +  $line->admracik; 
				$grand_adm_nci = $grand_adm_nci + $line->adm_nci; 
				$grand_tunai = $grand_tunai + $line->tunai; 
				$grand_transfer = $grand_transfer + $line->transfer; 
				$grand_kredit = $grand_kredit + $line->kredit;
			}
			
			$tp	->addColumn("Grand Total :", 3,"right")
				->addColumn(number_format($grand_sub_jumlah,0,',','.'), 1,"right")
				->addColumn(number_format($grand_discount,0,',','.'), 1,"right")
				->addColumn(number_format($grand_tuslah,0,',','.'), 1,"right")
				->addColumn(number_format($grand_admracik,0,',','.'), 1,"right")
				->addColumn(number_format($grand_adm_nci,0,',','.'), 1,"right")
				->addColumn(number_format($grand_tunai,0,',','.'), 1,"right")
				->addColumn(number_format($grand_transfer,0,',','.'), 1,"right")
				->addColumn(number_format($grand_kredit,0,',','.'), 1,"right")
				->commit("header");
			
		}else {		
			$tp	->addColumn("Data tidak ada", 11,"center")
				->commit("header");
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user_p, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 8,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_transaksi_perjenis_pasien_sum.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_p."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	
	}
}
?>