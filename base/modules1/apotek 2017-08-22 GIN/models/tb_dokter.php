<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_dokter extends TblBase
{
        function __construct() {
			$this->TblName='dokter';
			TblBase::TblBase(true);

			$this->SqlQuery= "SELECT kd_dokter, nama FROM dokter ORDER BY nama";
        }

	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->NAMA=$rec->nama;

		return $row;
	}
}
class Rowdokter
{
    public $KD_DOKTER;
    public $NAMA;

}

?>