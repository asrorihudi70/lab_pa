﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_detailgridObatRetur extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="cito,kd_prd,nama_obat,kd_satuan,racik,harga_jual,harga_beli,jml,reduksi,dosis,admracik";
		$this->SqlQuery="SELECT distinct(o.kd_prd), o.cito, a.nama_obat, a.kd_satuan, o.racikan as racik, 
							o.harga_jual, o.harga_pokok as harga_beli, o.markup, o.jml_out as jml,  o.jml_out as qty,
							o.disc_det as reduksi, o.dosis, o.jasa, admracik,
							o.no_out, o.no_urut, o.tgl_out, o.kd_milik
						FROM apt_barang_out_detail o 
							left JOIN apt_barang_out_detail_gin og on og.no_out=o.no_out AND og.tgl_out=o.tgl_out AND og.kd_prd=o.kd_prd AND og.kd_milik=o.kd_milik AND og.no_urut=o.no_urut
							inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out 
							INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}
	/* FROM apt_barang_out_detail o
							INNER JOIN apt_barang_out_detail_gin og on og.no_out=o.no_out AND og.tgl_out=o.tgl_out AND og.kd_prd=o.kd_prd AND og.kd_milik=o.kd_milik AND og.no_urut=o.no_urut
							inner join apt_barang_out b on o.no_out=b.no_out and o.tgl_out=b.tgl_out 
							INNER JOIN apt_obat a on o.kd_prd = a.kd_prd 
							INNER JOIN apt_stok_unit_gin s ON og.kd_prd=s.kd_prd AND og.kd_milik=s.kd_milik AND og.gin=s.gin 
							AND o.kd_milik=s.kd_milik */

	function FillRow($rec)
	{
		$row=new RowdetailResepRWJ;
		
		$row->cito=$rec->cito;
		$row->kd_prd=$rec->kd_prd;
		$row->nama_obat=$rec->nama_obat;
		$row->kd_satuan=$rec->kd_satuan;
		$row->racik=$rec->racik;
		$row->harga_jual=$rec->harga_jual;
		$row->harga_beli=$rec->harga_beli;
		$row->markup=$rec->markup;
		$row->jml=$rec->jml;
		$row->reduksi=$rec->reduksi;
		$row->dosis=$rec->dosis;
		$row->jasa=$rec->jasa;
		$row->admracik=$rec->admracik;
		$row->no_out=$rec->no_out;
		$row->no_urut=$rec->no_urut;
		$row->tgl_out=$rec->tgl_out;
		$row->kd_milik=$rec->kd_milik;
		//$row->jml_stok_apt=$rec->jml_stok_apt;
		$row->qty=$rec->qty;
		return $row;
	}
}
class RowdetailResepRWJ
{
	public $cito;
	public $kd_prd;
	public $nama_obat;
	public $kd_satuan;
	public $racik;
	public $harga_jual;
	public $harga_beli;
	public $markup;
	public $jml;
	public $reduksi;
	public $dosis;
	public $jasa;
	public $admracik;
	public $no_out;
	public $no_urut;
	public $tgl_out;
	public $kd_milik;
	public $qty;
}



?>