﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_master_obat extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_prd,kd_satuan,satuan,nama_obat,ket_obat,kd_sat_besar,keterangan,kd_jns_obt,
						nama_jenis,generic,kd_sub_jns,sub_jenis,apt_kd_golongan,apt_golongan,
						fractions,mg,dpho,kd_pabrik,pabrik,aktif,kd_katagori";
		$this->SqlQuery="SELECT o.kd_prd,o.kd_satuan, sk.satuan,o.nama_obat,o.ket_obat,o.kd_sat_besar,o.fractions,
							sb.keterangan,o.kd_jns_obt,j.nama_jenis,o.generic,
							o.kd_sub_jns,sj.sub_jenis,o.apt_kd_golongan,g.apt_golongan,o.mg,o.dpho,
							o.kd_pabrik,p.pabrik,o.aktif,o.kd_katagori,o.kd_jns_terapi,o.formularium,o.generik,jt.jenis_terapi
						FROM apt_obat o
							LEFT JOIN apt_jenis_obat j on o.kd_jns_obt=j.kd_jns_obt
							LEFT JOIN apt_sub_jenis sj on o.kd_sub_jns=sj.kd_sub_jns
							LEFT JOIN apt_sat_besar sb on o.kd_sat_besar=sb.kd_sat_besar
							LEFT JOIN apt_satuan sk on o.kd_satuan=sk.kd_satuan
							LEFT JOIN apt_golongan g on o.apt_kd_golongan=g.apt_kd_golongan
							LEFT JOIN pabrik p on o.kd_pabrik=p.kd_pabrik
							LEFT JOIN apt_jenis_terapi jt on o.kd_jns_terapi=jt.kd_jns_terapi
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}

	function FillRow($rec)
	{
		$row=new Rowmasterobat;
		
		$row->KD_PRD=$rec->kd_prd;
		$row->KD_SATUAN=$rec->kd_satuan;
		$row->NAMA_OBAT=$rec->nama_obat;
		$row->KET_OBAT=$rec->ket_obat;
		$row->KD_SAT_BESAR=$rec->kd_sat_besar;
		$row->KD_JNS_OBT=$rec->kd_jns_obt;
		$row->GENERIC=$rec->generic;
		$row->KD_SUB_JNS=$rec->kd_sub_jns;
		$row->APT_KD_GOLONGAN=$rec->apt_kd_golongan;
		$row->FRACTIONS=$rec->fractions;
		$row->MG=$rec->mg;
		$row->DPHO=$rec->dpho;
		$row->KD_PABRIK=$rec->kd_pabrik;
		$row->PABRIK=$rec->pabrik;
		$row->AKTIF=$rec->aktif;
		$row->SATUAN=$rec->satuan;
		$row->KETERANGAN=$rec->keterangan;
		$row->NAMA_JENIS=$rec->nama_jenis;
		$row->SUB_JENIS=$rec->sub_jenis;
		$row->APT_GOLONGAN=$rec->apt_golongan;
		$row->KD_KATAGORI=$rec->kd_katagori;
		$row->KD_JNS_TERAPI=$rec->kd_jns_terapi;
		$row->GENERIK=$rec->generik;
		$row->FORMULARIUM=$rec->formularium;
		$row->JENIS_TERAPI=$rec->jenis_terapi;
		
		return $row;
	}
}
class Rowmasterobat
{
	public $KD_PRD;
	public $KD_SATUAN;
	public $NAMA_OBAT;
	public $KET_OBAT;
	public $KD_SAT_BESAR;
	public $KD_JNS_OBT;
	public $GENERIC;
	public $KD_SUB_JNS;
	public $APT_KD_GOLONGAN;
	public $FRACTIONS;
	public $MG;
	public $DPHO;
	public $KD_PABRIK;
	public $PABRIK;
	public $AKTIF;
	public $SATUAN;
	public $KETERANGAN;
	public $NAMA_JENIS;
	public $SUB_JENIS;
	public $APT_GOLONGAN;
	public $KD_KATAGORI;
	public $KD_JNS_TERAPI;
	public $FORMULARIUM;
	public $GENERIK;
	public $JENIS_TERAPI;
	
}



?>