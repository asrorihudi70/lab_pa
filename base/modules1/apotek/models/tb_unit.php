<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_unit extends TblBase
{
        function __construct() {
			$this->TblName='unit';
			TblBase::TblBase(true);

			$this->SqlQuery= "SELECT kd_unit, nama_unit FROM unit
								UNION
								SELECT '3' AS kd_unit,'IGD' AS nama_unit";
        }

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT=$rec->kd_unit;
		$row->NAMA_UNIT=$rec->nama_unit;

		return $row;
	}
}
class Rowunit
{
    public $KD_UNIT;
    public $NAMA_UNIT;

}

?>