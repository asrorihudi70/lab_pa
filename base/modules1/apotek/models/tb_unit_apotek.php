<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tb_unit_apotek extends TblBase
{
	function __construct() {
		$this->TblName  ='apt_unit';
		TblBase::TblBase(true);
		$this->SqlQuery = "SELECT kd_unit_far, nm_unit_far from apt_unit ORDER BY nm_unit_far ASC";
	}

	function FillRow($rec)
	{
		$row=new Rowunit;
		$row->KD_UNIT_FAR=$rec->kd_unit_far;
		$row->NM_UNIT_FAR=$rec->nm_unit_far;

		return $row;
	}
}
class Rowunit
{
    public $KD_UNIT_FAR;
    public $NM_UNIT_FAR;

}

?>