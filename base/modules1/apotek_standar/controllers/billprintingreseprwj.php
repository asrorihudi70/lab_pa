<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class billprintingreseprwj extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
    $this->load->view('main/index');
    }
    
    public function save($Params=NULL)
    {
       $strError = "";
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		
		$NoResep = $Params['NoResep'];
		$NoOut = $Params['NoOut'];
		$TglOut = $Params['TglOut'];
		$KdPasien = $Params['KdPasien'];
		$NamaPasien = $Params['NamaPasien'];
		$JenisPasien = $Params['JenisPasien'];
		$Poli = $Params['Poli'];
		$Dokter = $Params['Dokter'];
		$Total = $Params['Total'];
		$Tot = $Params['Tot'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$deskripsi=$this->db->query("select pt.deskripsi
									from apt_detail_bayar b
										inner join PAYMENT p on b.kd_pay=p.kd_pay
										inner join PAYMENT_TYPE pt on p.jenis_pay=pt.jenis_pay
									where no_out=".$NoOut." and tgl_out='".$TglOut."'")->row()->deskripsi;
		$user=$this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
	 
		//AWAL SCRIPT PRINT
		$printer=$Params['printer'];
		//$printer = $this->db->query("select setting from sys_setting where key_data = 'apt_default_printer_bill'")->row()->setting;
        $t1 = 4;
        $t3 = 20;
        $t2 = 60-($t3+$t1);       
        $format1 = date('d F Y', strtotime($TglOut));
        $today = date("d F Y");
        $Jam = date("G:i:s");
		$tglSekarang = date('d-M-Y G:i:s');
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;
        $Data .= $NameRS."\n";
        $Data .= $Address."\n";
        $Data .= "Phone : ".$TLP."\n";
		$Data .= "\n";
		$Data .= "                KWITANSI PEMBAYARAN OBAT \n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= "No. Tr     	: ".$NoOut."         ".$NoResep."\n";
        $Data .= "No. Medrec	: ".$KdPasien."\n";
        $Data .= "Nama      	: ".$NamaPasien."\n";
		$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
		$Data .= "Tanggal     	: ".$format1."\n";
		$Data .= "Poliklinik 	: ".$Poli."\n";
        $Data .= "Dokter     	: ".$Dokter."\n";
		$Data .= "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
        $Data .= "------------------------------------------------------------\n";
        
        $no = 0;
		$jmllist= $Params['jumlah'];
		for($i=0;$i<$jmllist;$i++){	
			$no++;
			$nama_obat = $Params['nama_obat-'.$i];
			$jml = $Params['jml-'.$i];
			$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
		}
        $Data .= "------------------------------------------------------------\n";
		//$Data .= str_pad("Total[".$deskripsi."]", 50," ",STR_PAD_LEFT).str_pad($Total, 20," ",STR_PAD_LEFT)."\n";
		$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
        $Data .= "\n";
        $Data .= "Terbilang :"."\n";
		$Data .= terbilang($Tot)." Rupiah \n";
        $Data .= "\n";
        $Data .= "\n";
        //$Data .= "\n";
		$Data .= "Kasir	: ".$kdUser." ".$user."\n";
		$Data .= "Date	: ".$tglSekarang."\n";
		$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
       /*  $Data .= str_pad(" ",30, " ").str_pad("(---------------------)", 30," ",STR_PAD_LEFT)."\n";
        $Data .= str_pad(" ",40, " ").str_pad($kdUser, 20," ",STR_PAD_BOTH)."\n";
        $Data .= str_pad("Jam : ".$Jam,30, " ").str_pad("Kasir : ".$kdUser,30, " ",STR_PAD_LEFT)."\n"; */
        fwrite($handle, $Data);
        fclose($handle);

			 
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." -r ".$file); # Lakukan cetak linux
		}
		
        if ($deskripsi)
        {
            echo '{success: true}';
        }
        else{
            echo '{success: false}';
        }
    } 
	
	public function cetak($Params)
    {
        $strError = "";
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
		
		$NoResep = $Params['NoResep'];
		$NoOut = $Params['NoOut'];
		$TglOut = $Params['TglOut'];
		$KdPasien = $Params['KdPasien'];
		$NamaPasien = $Params['NamaPasien'];
		$JenisPasien = $Params['JenisPasien'];
		$Poli = $Params['Poli'];
		$Dokter = $Params['Dokter'];
		$Total = $Params['Total'];
		$Tot = $Params['Tot'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$deskripsi=$this->db->query("select pt.deskripsi
									from apt_detail_bayar b
									inner join PAYMENT p on b.kd_pay=p.kd_pay
									inner join PAYMENT_TYPE pt on p.jenis_pay=pt.jenis_pay
									where no_out=".$NoOut." and tgl_out='".$TglOut."'")->row()->deskripsi;
		$user=$this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
	 
		//AWAL SCRIPT PRINT
		$printer = $this->db->query("select setting from sys_setting where key_data = 'rwi_default_printer_bill'")->row()->setting;
        $t1 = 4;
        $t3 = 20;
        $t2 = 60-($t3+$t1);       
        $format1 = date('d F Y', strtotime($TglOut));
        $today = date("d F Y");
        $Jam = date("G:i:s");
		$tglSekarang = date('d-M-Y G:i:s');
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;
        $Data .= $NameRS."\n";
        $Data .= $Address."\n";
        $Data .= "Phone : ".$TLP."\n";
		$Data .= "\n";
		$Data .= "                KWITANSI PEMBAYARAN OBAT \n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= "No. Tr     	: ".$NoOut."         ".$NoResep."\n";
        $Data .= "No. Medrec	: ".$KdPasien."\n";
        $Data .= "Nama      	: ".$NamaPasien."\n";
		$Data .= "Jenis Pasien	: ".$JenisPasien."\n";
		$Data .= "Tanggal     	: ".$format1."\n";
		$Data .= "Poliklinik 	: ".$Poli."\n";
        $Data .= "Dokter     	: ".$Dokter."\n";
		$Data .= "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= str_pad("No.", $t1," ").str_pad("Nama Obat", $t2," ").str_pad("Qty", $t3," ",STR_PAD_LEFT)."\n";
        $Data .= "------------------------------------------------------------\n";
        
        $no = 0;
		$jmllist= $Params['jumlah'];
		for($i=0;$i<$jmllist;$i++){	
			$no++;
			$nama_obat = $Params['nama_obat-'.$i];
			$jml = $Params['jml-'.$i];
			$Data .= str_pad($no, $t1," ").str_pad($nama_obat, $t2," ").str_pad($jml, $t3," ",STR_PAD_LEFT)."\n";
		}
        $Data .= "------------------------------------------------------------\n";
		//$Data .= str_pad("Total[".$deskripsi."]", 50," ",STR_PAD_LEFT).str_pad($Total, 20," ",STR_PAD_LEFT)."\n";
		$Data .= "Total	[".$deskripsi."] : ".$Total."\n";
        $Data .= "\n";
        $Data .= "Terbilang :"."\n";
		$Data .= terbilang($Tot)." Rupiah \n";
        $Data .= "\n";
        $Data .= "\n";
        //$Data .= "\n";
		$Data .= "Kasir	: ".$kdUser." ".$user."\n";
		$Data .= "Date	: ".$tglSekarang."\n";
		$Data .= "BUKTI PEMBAYARAN INI BERLAKU JUGA SEBAGAI KUITANSI \n";
       /*  $Data .= str_pad(" ",30, " ").str_pad("(---------------------)", 30," ",STR_PAD_LEFT)."\n";
        $Data .= str_pad(" ",40, " ").str_pad($kdUser, 20," ",STR_PAD_BOTH)."\n";
        $Data .= str_pad("Jam : ".$Jam,30, " ").str_pad("Kasir : ".$kdUser,30, " ",STR_PAD_LEFT)."\n"; */
        fwrite($handle, $Data);
        fclose($handle);

		//$print = shell_exec("lpr -P ".$printer." -r ".$file); 
		copy($file, "//192.168.0.88/epson lx-310 escp");  # Lakukan cetak
        unlink($file);
		
		$error = "sukses";
        return $error;

		/* if($print)
		{
			$error = "sukses";
		}
		else
		{
			$error = "gagal";
		} */
        /* copy($file, $printer);  # Lakukan cetak
        unlink($file);
        
        $error = "sukses"; */
        
		/* echo "print:".$printer."\n";
		echo "file:".$file; */

	//AKHIR	 	 
    }

}




