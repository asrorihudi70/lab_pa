<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_returpasienperfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   	
   	public function getCustomer(){
   		
   		$result=$this->result;
   		$data=$this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A INNER JOIN
   		kontraktor B ON B.kd_customer=A.kd_customer WHERE UPPER(A.kd_customer) LIKE UPPER('%".$_POST['text']."%') OR UPPER(customer) LIKE UPPER('%".$_POST['text']."%') 
   				ORDER BY customer ASC LIMIT 10")->result();
   		$result->setData($data);
   		$result->end();
   	}
	
	public function cetak(){
		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN RETUR PASIEN PERFAKTUR';
		$param=json_decode($_POST['data']);
		
		$unit=$param->unit;
		$user=$param->user;
		$unit_rawat=$param->unit_rawat;
		$tgl_awal=$param->tgl_awal;
		$tgl_akhir=$param->tgl_akhir;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tgl_awal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tgl_akhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tgl_awal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tgl_akhir)));
		
		

		if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 1";
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,2))
									and tutup=1 and returapt=1 ";
			$sh="Shift 1 dan 2";
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 1 dan 3";
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
			$criteriaShift="WHERE (bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (2))
									and tutup=1 and returapt=1 ";
			$sh="Shift 2";
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (2,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 2 dan 3";
		} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Shift 3";
		} else{
			$criteriaShift="WHERE ((bo.tgl_out between '".$tgl_awal."' and '".$tgl_akhir."' and shiftapt in (1,2,3)) 
									or (bo.tgl_out between '".$tomorrow."' and '".$tomorrow2."' and shiftapt=4)) 
									and tutup=1 and returapt=1 ";
			$sh="Semua Shift";
		}
		
		if($unit == ""){
			$criteriaunit="";
			$nm_unit_far = "Semua Unit";
		} else{
			$criteriaunit="AND kd_unit_far='".$unit."'";
			$nm_unit_far = $this->db->query("select nm_unit_far from apt_unit where kd_unit_far='".$unit."'")->row()->nm_unit_far;
		}
		
		if($user == ""){
			$criteriauser="";
			$nama_user="Semua Operator";
		} else{
			$criteriauser="AND bo.opr=".$user."";
			$nama_user= $this->db->query("select full_name from zusers where kd_user='".$user."'")->row()->full_name;
		}
		
		if($unit_rawat == 1){
			$asalpasien='Rawat Inap';
			$kd_unit="AND left(o.kd_unit,1)='1'";
		} else if($unit_rawat == 2){
			$asalpasien='Rawat Jalan';
			$kd_unit="AND left(o.kd_unit,1)='2'";
		} else if($unit_rawat == 3){
			$asalpasien='Inst. Gawat Darurat';
			$kd_unit="AND left(o.kd_unit,1)='3'";
		} else {
			$asalpasien='SEMUA UNIT';
			$kd_unit="";
		}
		
		$queryHead = $this->db->query( "SELECT bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, initcap(bo.nmpasien) as nama, nama_unit, dr.nama as nama_dokter
										FROM Apt_Barang_Out bo 
											INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
											INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
											INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
											INNER JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
											".$criteriaShift."
											".$criteriaunit."
											".$criteriauser."
											".$kd_unit."
										ORDER BY bo.tgl_out, bo.No_out ");
		$query = $queryHead->result();
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Unit Rawat : '.$nm_unit_far.'</th>
					</tr>
					<tr>
						<th>Unit : '.$asalpasien.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "0">
			<thead>
				<tr>
					<th align="left">Kasir : '.$nama_user.'</th>
				</tr>
				<tr>
					<th align="left">Shift : '.$sh.'</th>
				</tr>
			</thead>
			</table>
			<br>
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Tanggal</th>
					<th align="center">No. Tr</th>
					<th align="center">No. Resep</th>
					<th align="center">No. Medrec / Kode Obat</th>
					<th align="center">Nama Pasien / Unit / Dokter / Nama Obat</th>
					<th align="center">&nbsp; Satuan &nbsp;</th>
					<th align="center">&nbsp; Qty &nbsp;</th>
					<th align="center">&nbsp; Harga &nbsp;</th>
					<th align="center">&nbsp; Jumlah &nbsp;</th>
				</tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;
			$grand=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='<tbody>
						<tr>
							<th align="left">'.$no.'</th>
							<th align="left">'.tanggalstring($line->tgl_out).'</th>
							<th align="left">'.$line->no_out.'</th>
							<th align="left">'.$line->no_resep.'</th>
							<th align="left">'.$line->kd_pasienapt.'</th>
							<th align="left">'.$line->nama.' / '.$line->nama_unit.' / '.$line->nama_dokter.'</th>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>';
						  
				$queryBody = $this->db->query( "SELECT bo.No_Resep, bo.tgl_out, bo.no_out, bo.kd_pasienapt, bo.nmpasien as nama, nama_unit, dr.nama as nama_dokter, bod.kd_prd, o.nama_obat, o.kd_satuan, bod.Jml_out as jumlah, bod.Harga_jual, 
													bod.jml_out*bod.harga_jual as NilaiJual, bo.jasa as tuslah, bo.admracik,bo.discount,
													Case When jml_item = 0 Then 0 Else jml_bayar/ jml_item End as bayar  
												FROM Apt_Barang_Out bo 
												INNER JOIN Apt_barang_out_Detail bod on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out 
												INNER JOIN Apt_Obat o ON o.kd_prd=bod.kd_prd 
												INNER JOIN unit u ON bo.kd_unit=u.kd_unit 
												INNER JOIN (SELECT kd_dokter, nama FROM dokter UNION SELECT kd_dokter, Nama FROM apt_dokter_luar) dr ON bo.dokter=dr.kd_dokter   
												".$criteriaShift."
												".$criteriaunit."
												".$criteriauser."
												".$kd_unit."
												and bo.No_Resep='".$line->no_resep."'
												and bo.tgl_out='".$line->tgl_out."'
												and bo.no_out='".$line->no_out."'
												ORDER BY bo.tgl_out, bo.No_out ");
				$query2 = $queryBody->result();
				$tot_jumlah=0;
				$tot_disc=0;
				$tottusadm=0;
				$sub_total=0;
				foreach ($query2 as $line2) 
				{
					$html.='<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>&nbsp;&nbsp;&nbsp;'.$line2->kd_prd.'</td>
								<td>&nbsp;&nbsp;&nbsp;'.$line2->nama_obat.'</td>
								<td>'.$line2->kd_satuan.'</td>
								<td align="right">'.$line2->jumlah.'</td>
								<td align="right">'.number_format($line2->harga_jual,0,',','.').'&nbsp;</td>
								<td align="right">'.number_format($line2->nilaijual,0,',','.').'&nbsp;</td>
							</tr>';
					$tot_jumlah += $line2->nilaijual;	
					$tot_disc += $line2->discount;
					$tottusadm += $line2->tuslah + $line2->admracik;
					
				}
				$sub_total = $tot_jumlah - $tot_disc + $tottusadm;
				
				$html.='<tr> 
								<th width="" align="right" colspan="9">Jumlah </th>
								<th width="" align="right">'.number_format($tot_jumlah,0,',','.').'&nbsp;</th>
						</tr>
						<tr>
								<th width="" align="right" colspan="9">Discount(-) </th>
								<th width="" align="right">'.number_format($tot_disc,0,',','.').'&nbsp;</th>
						</tr>
						<tr> 
								<th width="" align="right" colspan="9">Tuslah + Adm.Racik </th>
								<th width="" align="right">'.number_format($tottusadm,0,',','.').'&nbsp;</th>
						</tr>
						<tr> 
								<th width="" align="right" colspan="9">Sub Total </th>
								<th width="" align="right">'.number_format($sub_total,0,',','.').'&nbsp;</th>
						</tr>
						<tr> 
								<th width="" align="right" colspan="10">&nbsp;</th>
						</tr>';
				
				$grand += $sub_total;
			}
			$html.='<tr> 
					<th width="" align="right" colspan="9"></th>
					<th width="" align="right"></th>
				</tr>
				<tr> 
					<th width="" align="right" colspan="9">Grand Total </th>
					<th width="" align="right">'.number_format($grand,0,',','.').'&nbsp;</th>
				</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Retur Pasien PerFaktur',$html);	
	}
}
?>