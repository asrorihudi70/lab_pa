<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_transaksipembayarandetail extends MX_Controller {
    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }
    	
	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getSelect(){
   		$result=$this->result;
   		if($_POST['jenis_pay']!=''){
   			$result->setData($this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment WHERE jenis_pay=".$_POST['jenis_pay']." ORDER BY uraian ASC")->result());
   		}
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['user']=$this->db->query("SELECT kd_user AS id,full_name AS text FROM zusers ORDER BY user_names ASC")->result();
   		$array['payment']=$this->db->query("SELECT jenis_pay AS id,deskripsi AS text FROM payment_type ORDER BY deskripsi ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr_ubnit_far='';
   		$qr_operator='';
   		$qr_pembayaran='';
   		$qr_unit_rawat='';
   		$qr_shift='';
   		$unitRawat='SEMUA';
   		$unit='';
   		$pembayaran='SEMUA';
   		$shift='';
   		$operator='SEMUA';
   		$qr='';
   		if($_POST['unit_rawat']!=''){
   			if($_POST['unit_rawat']==0){
   				$unitRawat='Inst. Rawat Darurat';
   			}else if($_POST['unit_rawat']==1){
   				$unitRawat='Rawat Inap';
   			}else if($_POST['unit_rawat']==2){
   				$unitRawat='Rawat Jalan';
   			}
   			$qr_unit_rawat=" AND left(kd_unit,1)='".$_POST['unit_rawat']."'";
   		}
   		if($_POST['pembayaran']!=''){
   			$pembayaran=$this->db->query("SELECT deskripsi FROM payment_type WHERE jenis_pay='".$_POST['pembayaran']."'")->row()->deskripsi;
   			$qr_pembayaran=' AND pt.Jenis_Pay='.$_POST['pembayaran'];
   			if($_POST['detail_bayar']!=''){
   				$qr_pembayaran.=" AND p.Kd_Pay='".$_POST['detail_bayar']."' ";
   			}
   		}
		if($_POST['operator']!=''){
			$qr_operator='AND bo.opr='.$_POST['operator'];
			$operator=$this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$_POST['operator']."'")->row()->user_names;
		}
   		if(isset($_POST['kd_unit'])){
   			$u='';
   			for($i=0;$i<count($_POST['kd_unit']) ; $i++){
   				if($u !=''){
   					$u.=', ';
   					$unit.=', ';
   				}
   				$unit.=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$_POST['kd_unit'][$i]."'")->row()->nm_unit_far;
   				$u.="'".$_POST['kd_unit'][$i]."'";
   			}
   			if(count($_POST['kd_unit'])>0){
   				$qr_ubnit_far=" AND bo.kd_unit_far in (".$u.")";
   			}
   		}else{
			$result->error();
			$result->setMessage('Unit Tidak Boleh Kosong');
			$result->end();   			
   		}
   		
   		$bshift=false;
   		$shift3=false;
   		if($_POST['shift1']=='true'){
   			$shift='1';
   			$bshift=true;
   		}
   		if($_POST['shift2']=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='2';
   			$bshift=true;
   		}
   		if($_POST['shift3']=='true'){
   			if($shift!='')$shift.=', ';
   			$shift.='3';
   			$shift3=true;
   			$bshift=true;
   		}
   		if($bshift==false){
   			$result->error();
   			$result->setMessage('Pilih Shift');
   			$result->end();
   		}
   		$qr_shift="((tgl_bayar BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND shift in (".$shift."))";
   		if($shift3==true){
   			$qr_shift.="or (tgl_bayar BETWEEN '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."' AND
   					 '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."' AND shift=4)";
   		}
   		$qr_shift.=')';
   		$mpdf=$common->getPDF('L','LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL');
   		
   		
   		/* $queri="SELECT *, JumlahPay-(Sub_Jumlah+Tuslah+AdmRacik+Adm_NCI-Discount) as Pembulatan
   		FROM
   		(SELECT Payment, bo.Tgl_out, bo.No_Out, no_Resep, no_bukti, kd_pasienapt, nmPasien,
   		Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as Sub_Jumlah,
   				Jasa as Tuslah, AdmRacik, AdmNCI+AdmNCI_Racik as Adm_NCI, Discount, JumlahPay
   				FROM Apt_Barang_Out bo
   				INNER JOIN
   				(SELECT kd_dokter, nama
   						FROM dokter
   						UNION
   						SELECT kd_dokter, nama
   						FROM apt_dokter_luar
   				)dr
   				ON bo.Dokter=dr.kd_dokter
   				INNER JOIN
   				(SELECT Tgl_Out, No_Out, p.Uraian as Payment,
   						Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
   						FROM apt_Detail_Bayar db
   						INNER JOIN (Payment p
   								INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
   						WHERE ".$qr_shift."
   						".$qr_pembayaran."
   						GROUP BY Tgl_Out, No_Out, p.uraian
   				) y
   				ON bo.tgl_out=y.Tgl_Out AND bo.No_Out=y.No_Out
   				WHERE tutup=1
   				".$qr_ubnit_far." ".$qr_operator." ".$qr_unit_rawat."
   		) x
   		ORDER BY Payment, tgl_out, no_out"; */
		
		$queri="SELECT *, JumlahPay-(Sub_Jumlah+Tuslah+AdmRacik+Adm_NCI-Discount) as Pembulatan
				FROM
					(SELECT Payment, bo.Tgl_out, bo.No_Out, no_Resep, no_bukti, kd_pasienapt, nmPasien,
						Case When returapt=0 then Jml_Obat Else (-1)* Jml_Obat End as Sub_Jumlah,
						Jasa as Tuslah, AdmRacik, AdmNCI+AdmNCI_Racik as Adm_NCI, Discount, JumlahPay,
						Case When returapt=0 then 0 Else bod.reduksi End as reduksi
					FROM Apt_Barang_Out bo
					INNER JOIN (SELECT kd_dokter, nama FROM dokter
								UNION
								SELECT kd_dokter, nama FROM apt_dokter_luar 
								)dr ON bo.Dokter=dr.kd_dokter
					INNER JOIN (SELECT Tgl_Out, No_Out, p.Uraian as Payment, Sum(Case when DB_CR=false Then Jumlah Else (-1)*Jumlah End) as JumlahPay
								FROM apt_Detail_Bayar db
									INNER JOIN (Payment p INNER JOIN Payment_Type pt ON p.jenis_pay=pt.jenis_pay) ON db.kd_pay=p.kd_pay
								WHERE ".$qr_shift."
								".$qr_pembayaran."
								GROUP BY Tgl_Out, No_Out, p.uraian 
								) y ON bo.tgl_out=y.Tgl_Out AND bo.No_Out=y.No_Out
					INNER JOIN (SELECT sum(disc_det) as reduksi,no_out,tgl_out from apt_barang_out_detail group by no_out,tgl_out) bod ON bod.no_out=bo.no_out AND bod.tgl_out=bo.tgl_out
   				WHERE tutup=1
   				".$qr_ubnit_far." ".$qr_operator." ".$qr_unit_rawat."
				) x
				ORDER BY Payment, tgl_out, no_out";
		
   		
   		$data=$this->db->query($queri)->result();
   		
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th>LAPORAN TRANSAKSI PER PEMBAYARAN DETAIL</th>
   					</tr>
   					<tr>
   						<th>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</th>
   					</tr>
   					<tr>
   						<th>UNIT RAWAT : ".$unitRawat."</th>
   					</tr>
   					<tr>
   						<th>".$unit."</th>
   					</tr>
   					<tr>
   						<th>JENIS PEMBAYARAN : ".$pembayaran."</th>
   					</tr>
   					<tr>
   						<th align='left'>OPERATOR : ".$operator."</th>
   					</tr>
   					<tr>
   						<th align='left'>SHIFT : ".$shift."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='30'>No.</th>
   						<th width='80'>Tanggal</th>
   						<th width='80'>No Resep</th>
				   		<th>Nama Pasien</th>
   						<th width='70'>Transaksi</th>
				   		<th width='70'>Discount</th>
						<th width='70'>Reduksi</th>
				   		<th width='70'>Tuslah</th>
		   				<th width='70'>Racik</th>
   						<th width='70'>SIM</th>
		   				<th width='70'>Jumlah Bayar</th>
   					</tr>
   				</thead>
   		");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
				$subRed=0;
				$sub1=0;
				$sub2=0;
				$sub3=0;
				$sub4=0;
				$sub5=0;
				$sub6=0;
				$grand1=0;
				$grand2=0;
				$grand3=0;
				$grand4=0;
				$grand5=0;
				$grand6=0;
 				$payment='';
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->payment!=$payment){
	   					$payment=$data[$i]->payment;
	   					if($i!=0){
	   						$mpdf->WriteHTML("
		   						<tr>
		   					   		<th align='right' colspan='4'>Sub Total</th>
		   					   		<th align='right'>".number_format($sub1,0,',','.')."</th>
		   							<th align='right'>".number_format($sub2,0,',','.')."</th>
									<th align='right'>".number_format($subRed,0,',','.')."</th>
		   							<th align='right'>".number_format($sub3,0,',','.')."</th>
		   							<th align='right'>".number_format($sub4,0,',','.')."</th>
		   							<th align='right'>".number_format($sub5,0,',','.')."</th>
		   							<th align='right'>".number_format($sub6,0,',','.')."</th>
		   						</tr>
		   					");
	   						$sub1=0;
	   						$sub2=0;
	   						$sub3=0;
	   						$sub4=0;
	   						$sub5=0;
	   						$sub6=0;
							$subRed=0;
	   					}
	   					$mpdf->WriteHTML("
	   						<tr>
	   					   		<th align='left' colspan='11'>".$data[$i]->payment."</th>
	   						</tr>
	   					");
	   				}
					$reduksi=pembulatanratusan((int)$data[$i]->reduksi);
	   				$sub1+=$data[$i]->sub_jumlah;
					$sub2+=$data[$i]->discount;
					$subRed+=$reduksi;
					$sub3+=$data[$i]->tuslah;
					$sub4+=$data[$i]->admracik;
					$sub5+=$data[$i]->adm_nci;
					$sub6+=$data[$i]->jumlahpay;
					$grand1+=$data[$i]->sub_jumlah;
					$grand2+=$data[$i]->discount;
					$grandRed+=$reduksi;
					$grand3+=$data[$i]->tuslah;
					$grand4+=$data[$i]->admracik;
					$grand5+=$data[$i]->adm_nci;
					$grand6+=$data[$i]->jumlahpay;
   					$mpdf->WriteHTML("
   						<tr>
   					   		<td align='center'>".($i+1)."</td>
   					   		<td align='center'>".date('d/m/Y', strtotime($data[$i]->tgl_out))."</td>
   						   	<td align='center'>".$data[$i]->no_resep."</td>
   							<td>".$data[$i]->nmpasien."</td>
   					   		<td align='right'>".number_format($data[$i]->sub_jumlah,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->discount,0,',','.')."</td>
   							<td align='right'>".number_format($reduksi,0,',','.')."</td>
							<td align='right'>".number_format($data[$i]->tuslah,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->admracik,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->adm_nci,0,',','.')."</td>
   							<td align='right'>".number_format($data[$i]->jumlahpay,0,',','.')."</td>
   						</tr>
   					");
					/* echo $reduksi." pembulatan<br>";
					echo (int)$data[$i]->reduksi." asli<br>"; */
	   			}
				
	   			$mpdf->WriteHTML("
   						<tr>
   					   		<th align='right' colspan='4'>Sub Total</th>
   					   		<th align='right'>".number_format($sub1,0,',','.')."</th>
   							<th align='right'>".number_format($sub2,0,',','.')."</th>
							<th align='right'>".number_format($subRed,0,',','.')."</th>
   							<th align='right'>".number_format($sub3,0,',','.')."</th>
   							<th align='right'>".number_format($sub4,0,',','.')."</th>
   							<th align='right'>".number_format($sub5,0,',','.')."</th>
   							<th align='right'>".number_format($sub6,0,',','.')."</th>
   						</tr>
   					");
	   			$mpdf->WriteHTML("
   						<tr>
   					   		<th align='right' colspan='4'>Grand Total</th>
   					   		<th align='right'>".number_format($grand1,0,',','.')."</th>
   							<th align='right'>".number_format($grand2,0,',','.')."</th>
   							<th align='right'>".number_format($grandRed,0,',','.')."</th>
							<th align='right'>".number_format($grand3,0,',','.')."</th>
   							<th align='right'>".number_format($grand4,0,',','.')."</th>
   							<th align='right'>".number_format($grand5,0,',','.')."</th>
   							<th align='right'>".number_format($grand6,0,',','.')."</th>
   						</tr>
   					");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapTransaksiPerPembayaranDetail';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>