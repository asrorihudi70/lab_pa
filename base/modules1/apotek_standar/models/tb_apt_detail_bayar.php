<?php

class tb_apt_detail_bayar extends TblBase
{
    function __construct()
    {
        $this->TblName='apt_detail_bayar';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewaptdetailbayar;
		
		$row->TGL_OUT=$rec->tgl_out;
		$row->NO_OUT=$rec->no_out;
		$row->URUT=$rec->urut;
		$row->TGL_BAYAR=$rec->tgl_bayar;
		$row->KD_PAY=$rec->kd_pay;
		$row->JUMLAH=$rec->jumlah;
		$row->SHIFT=$rec->shift;
		$row->KD_USER=$rec->kd_user;
		$row->JUMLAH_TERIMA_UANG=$rec->jumlah_terima_uang;
		
		return $row;
		  
    }

}

class Rowviewaptdetailbayar
{
	public $NO_OUT;
	public $TGL_OUT;
	public $URUT;
	public $TGL_BAYAR;
	public $KD_PAY;
	public $JUMLAH;
	public $SHIFT;
	public $KD_USER;
	public $JUMLAH_TERIMA_UANG;
}

?>
