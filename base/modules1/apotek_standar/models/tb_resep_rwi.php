﻿<?php
/**
 * @author Agung
 * @copyright 2008
 */


class tb_resep_rwi extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="status_posting, no_resep, no_out, tgl_out, kd_pasienapt, nmpasien, dokter, nama_dokter, kd_unit,
						nama_unit, apt_no_transaksi, tgl_transaksi,apt_kd_kasir, kd_customer,admracik, jumlah, jml_terima_uang, 
						sisa, customer, jenis_pasien,tgl_masuk,urut_masuk,kd_spesial,kd_unit_kamar";
		$this->SqlQuery="SELECT  distinct(o.no_resep),o.tutup as status_posting, o.no_out, o.tgl_out, o.kd_pasienapt, o.nmpasien, 
							o.dokter, d.nama as nama_dokter, o.kd_unit, u.nama_unit, o.apt_no_transaksi, t.tgl_transaksi,
							o.apt_kd_kasir, o.admracik,o.jasa, o.admprhs,o.admresep,
							o.no_kamar, k.nama_kamar,c.customer, o.kd_customer, case when ko.jenis_cust =0 then 'Perorangan'
								when ko.jenis_cust =1 then 'Perusahaan'
								when ko.jenis_cust =2 then 'Asuransi'
								end as jenis_pasien,
							kun.tgl_masuk,kun.urut_masuk,ngin.kd_spesial,ngin.kd_unit_kamar
							FROM
								apt_barang_out o
								inner join unit u on o.kd_unit=u.kd_unit
								inner join dokter d on o.dokter=d.kd_dokter
								inner JOIN kamar k on o.kd_unit=k.kd_unit and o.no_kamar=k.no_kamar
								inner JOIN customer c ON c.kd_customer = o.kd_customer
								inner join kontraktor ko on c.kd_customer=ko.kd_customer
								inner join transaksi t on t.no_transaksi=o.apt_no_transaksi and t.kd_kasir=o.apt_kd_kasir
								inner join kunjungan kun on t.kd_pasien=kun.kd_pasien and t.kd_unit=kun.kd_unit 
									and t.urut_masuk=kun.urut_masuk 
									and t.tgl_transaksi=kun.tgl_masuk 
									and t.batal=false
								inner join nginap ngin on kun.kd_unit=ngin.kd_unit and  ngin.kd_pasien=kun.kd_pasien and ngin.tgl_masuk=kun.tgl_masuk
									and ngin.urut_masuk=kun.urut_masuk and ngin.akhir=true
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowResepRWJ;
		
		$row->STATUS_POSTING=$rec->status_posting;
		$row->NO_RESEP=$rec->no_resep;
		$row->NO_OUT=$rec->no_out;
		$row->TGL_OUT=$rec->tgl_out;
		$row->KD_PASIENAPT=$rec->kd_pasienapt;
		$row->NMPASIEN=$rec->nmpasien;
		$row->DOKTER=$rec->dokter;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->KD_UNIT=$rec->kd_unit;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->APT_NO_TRANSAKSI=$rec->apt_no_transaksi;
		$row->TGL_TRANSAKSI=$rec->tgl_transaksi;
		$row->APT_KD_KASIR=$rec->apt_kd_kasir;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->ADMRACIK=$rec->admracik;
		$row->JASA=$rec->jasa;
		$row->ADMPRHS=$rec->admprhs;
		$row->ADMRESEP=$rec->admresep;
		$row->NO_KAMAR=$rec->no_kamar;
		$row->NAMA_KAMAR=$rec->nama_kamar;
		$row->CUSTOMER=$rec->customer;
		$row->JENIS_PASIEN=$rec->jenis_pasien;
		$row->TGL_MASUK=$rec->tgl_masuk;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->KD_UNIT_KAMAR=$rec->kd_unit_kamar;
		$row->KD_SPESIAL=$rec->kd_spesial;
		/* $row->JUMLAH=$rec->jumlah;
		$row->JML_TERIMA_UANG=$rec->jml_terima_uang;
		$row->SISA=$rec->sisa; */
		return $row;
	}
}
class RowResepRWJ
{
	public $STATUS_POSTING;
	public $NO_RESEP;
	public $NO_OUT;
	public $TGL_OUT;
	public $KD_PASIENAPT;
	public $NMPASIEN;
	public $DOKTER;
	public $NAMA_DOKTER;
	public $KD_UNIT;
	public $NAMA_UNIT;
	public $APT_NO_TRANSAKSI;
	public $TGL_TRANSAKSI;
	public $APT_KD_KASIR;
	public $KD_CUSTOMER;
	public $ADMRACIK;
	public $JASA;
	public $ADMPRHS;
	public $ADMRESEP;
	public $NO_KAMAR;
	public $NAMA_KAMAR;
	public $CUSTOMER;
	public $JENIS_PASIEN;
	public $TGL_MASUK;
	public $URUT_MASUK;
	public $KD_UNIT_KAMAR;
	public $KD_SPESIAL;
/* 	public $JUMLAH;
	public $JML_TERIMA_UANG;
	public $SISA; */
	
}



?>