<?php

/**
 * @author
 * @copyright
 */
class viewaskepdetresumeasuhan extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = null) {
        $this->load->model('ASKEP/tblviewaskepdetresumeasuhan');

        $criteria = $Params['query'];
        $this->tblviewaskepdetresumeasuhan->db->where($criteria, null, false);
        $query = $this->tblviewaskepdetresumeasuhan->GetRowList();
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>