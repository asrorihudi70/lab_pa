<?php

/**
 * @author
 * @copyright
 */
class viewaskeppengkajian extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskeppengkajian');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskeppengkajian->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskeppengkajian->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $criteria = $Params['query'];

        $data = array(
            "riwayat_utama" => $Params['RIWAYAT_UTAMA'],
            "riwayat_penyakit_sekarang" => $Params['RIWAYAT_PENYAKIT_SEKARANG'],
            "riwayat_penyakit_dahulu" => $Params['RIWAYAT_PENYAKIT_DAHULU'],
            "nafas_paten" => $Params['NAFAS_PATEN'],
            "nafas_obstruktif" => $Params['NAFAS_OBSTRUKTIF'],
            "nafas_jelas" => $Params['NAFAS_JELAS'],
            "nafas_pola_simetri" => $Params['NAFAS_POLA_SIMETRI'],
            "nafas_pola_asimetri" => $Params['NAFAS_POLA_ASIMETRI'],
            "nafas_suara_normal" => $Params['NAFAS_SUARA_NORMAL'],
            "nafas_suara_hipersonor" => $Params['NAFAS_SUARA_HIPERSONOR'],
            "nafas_menurun" => $Params['NAFAS_MENURUN'],
            "nafas_jenis_normal" => $Params['NAFAS_JENIS_NORMAL'],
            "nafas_jenis_tachypnoe" => $Params['NAFAS_JENIS_TACHYPNOE'],
            "nafas_jenis_cheynestokes" => $Params['NAFAS_JENIS_CHEYNESTOKES'],
            "nafas_jenis_retractive" => $Params['NAFAS_JENIS_RETRACTIVE'],
            "nafas_jenis_kusmaul" => $Params['NAFAS_JENIS_KUSMAUL'],
            "nafas_jenis_dispnoe" => $Params['NAFAS_JENIS_DISPNOE'],
            "nafas_rr" => $Params['NAFAS_RR'],
            "nafas_suara_wheezing" => $Params['NAFAS_SUARA_WHEEZING'],
            "nafas_suara_ronchi" => $Params['NAFAS_SUARA_RONCHI'],
            "nafas_suara_rales" => $Params['NAFAS_SUARA_RALES'],
            "nafas_evakuasi_cairan" => $Params['NAFAS_EVAKUASI_CAIRAN'],
            "nafas_jml_cairan" => $Params['NAFAS_JML_CAIRAN'],
            "nafas_warna_cairan" => $Params['NAFAS_WARNA_CAIRAN'],
            "nafas_masalah" => $Params['NAFAS_MASALAH'],
            "jantung_reguler" => $Params['JANTUNG_REGULER'],
            "jantung_irreguler" => $Params['JANTUNG_IRREGULER'],
            "jantung_s1s2_tunggal" => $Params['JANTUNG_S1S2_TUNGGAL'],
            "jantung_nyeri_dada" => $Params['JANTUNG_NYERI_DADA'],
            "jantung_nyeri_dada_ket" => $Params['JANTUNG_NYERI_DADA_KET'],
            "jantung_bunyi_murmur" => $Params['JANTUNG_BUNYI_MURMUR'],
            "jantung_bunyi_gallop" => $Params['JANTUNG_BUNYI_GALLOP'],
            "jantung_bunyi_bising" => $Params['JANTUNG_BUNYI_BISING'],
            "jantung_crt" => $Params['JANTUNG_CRT'],
            "jantung_akral_hangat" => $Params['JANTUNG_AKRAL_HANGAT'],
            "jantung_akral_dingin" => $Params['JANTUNG_AKRAL_DINGIN'],
            "jantung_peningkatan_jvp" => $Params['JANTUNG_PENINGKATAN_JVP'],
            "jantung_ukuran_cvp" => $Params['JANTUNG_UKURAN_CVP'],
            "jantung_warna_jaundice" => $Params['JANTUNG_WARNA_JAUNDICE'],
            "jantung_warna_sianotik" => $Params['JANTUNG_WARNA_SIANOTIK'],
            "jantung_warna_kemerahan" => $Params['JANTUNG_WARNA_KEMERAHAN'],
            "jantung_warna_pucat" => $Params['JANTUNG_WARNA_PUCAT'],
            "jantung_tekanan_darah" => $Params['JANTUNG_TEKANAN_DARAH'],
            "jantung_nadi" => $Params['JANTUNG_NADI'],
            "jantung_temp" => $Params['JANTUNG_TEMP'],
            "jantung_masalah" => $Params['JANTUNG_MASALAH'],
            "otot_sendi_bebas" => $Params['OTOT_SENDI_BEBAS'],
            "otot_sendi_terbatas" => $Params['OTOT_SENDI_TERBATAS'],
            "otot_kekuatan" => $Params['OTOT_KEKUATAN'],
            "otot_odema_ekstrimitas" => $Params['OTOT_ODEMA_EKSTRIMITAS'],
            "otot_kelainan_bentuk" => $Params['OTOT_KELAINAN_BENTUK'],
            "otot_krepitasi" => $Params['OTOT_KREPITASI'],
            "otot_masalah" => $Params['OTOT_MASALAH'],
            "syaraf_gcs_eye" => $Params['SYARAF_GCS_EYE'],
            "syaraf_gcs_verbal" => $Params['SYARAF_GCS_VERBAL'],
            "syaraf_gcs_motorik" => $Params['SYARAF_GCS_MOTORIK'],
            "syaraf_gcs_total" => $Params['SYARAF_GCS_TOTAL'],
            "syaraf_fisiologis_brachialis" => $Params['SYARAF_FISIOLOGIS_BRACHIALIS'],
            "syaraf_fisiologis_patella" => $Params['SYARAF_FISIOLOGIS_PATELLA'],
            "syaraf_fisiologis_achilles" => $Params['SYARAF_FISIOLOGIS_ACHILLES'],
            "syaraf_patologis_choddoks" => $Params['SYARAF_PATOLOGIS_CHODDOKS'],
            "syaraf_patologis_babinski" => $Params['SYARAF_PATOLOGIS_BABINSKI'],
            "syaraf_patologis_budzinzky" => $Params['SYARAF_PATOLOGIS_BUDZINZKY'],
            "syaraf_patologis_kernig" => $Params['SYARAF_PATOLOGIS_KERNIG'],
            "syaraf_masalah" => $Params['SYARAF_MASALAH'],
            "indra_pupil_isokor" => $Params['INDRA_PUPIL_ISOKOR'],
            "indra__pupil_anisokor" => $Params['INDRA__PUPIL_ANISOKOR'],
            "indra__konjungtiva_anemis" => $Params['INDRA__KONJUNGTIVA_ANEMIS'],
            "indra__konjungtiva_icterus" => $Params['INDRA__KONJUNGTIVA_ICTERUS'],
            "indra__konjungtiva_tidak" => $Params['INDRA__KONJUNGTIVA_TIDAK'],
            "indra_gangguan_dengar" => $Params['INDRA_GANGGUAN_DENGAR'],
            "indra_otorhea" => $Params['INDRA_OTORHEA'],
            "indra_bentuk_hidung" => $Params['INDRA_BENTUK_HIDUNG'],
            "indra_bentuk_hidung_ket" => $Params['INDRA_BENTUK_HIDUNG_KET'],
            "indra_gangguan_cium" => $Params['INDRA_GANGGUAN_CIUM'],
            "indra_rhinorhea" => $Params['INDRA_RHINORHEA'],
            "indra_masalah" => $Params['INDRA_MASALAH'],
            "kemih_kebersihan" => $Params['KEMIH_KEBERSIHAN'],
            "kemih_alat_bantu" => $Params['KEMIH_ALAT_BANTU'],
            "kemih_jml_urine" => $Params['KEMIH_JML_URINE'],
            "kemih_warna_urine" => $Params['KEMIH_WARNA_URINE'],
            "kemih_bau" => $Params['KEMIH_BAU'],
            "kemih_kandung_membesar" => $Params['KEMIH_KANDUNG_MEMBESAR'],
            "kemih_nyeri_tekan" => $Params['KEMIH_NYERI_TEKAN'],
            "kemih_gangguan_anuria" => $Params['KEMIH_GANGGUAN_ANURIA'],
            "kemih_gangguan_aliguria" => $Params['KEMIH_GANGGUAN_ALIGURIA'],
            "kemih_gangguan_retensi" => $Params['KEMIH_GANGGUAN_RETENSI'],
            "kemih_gangguan_inkontinensia" => $Params['KEMIH_GANGGUAN_INKONTINENSIA'],
            "kemih_gangguan_disuria" => $Params['KEMIH_GANGGUAN_DISURIA'],
            "kemih_gangguanhematuria" => $Params['KEMIH_GANGGUANHEMATURIA'],
            "kemih_masalah" => $Params['KEMIH_MASALAH'],
            
            "cerna_makan_habis" => $Params['CERNA_MAKAN_HABIS'],
            "cerna_makan_ket" => $Params['CERNA_MAKAN_KET'],
            "cerna_makan_frekuensi" => $Params['CERNA_MAKAN_FREKUENSI'],
            "cerna_jml_minum" => $Params['CERNA_JML_MINUM'],
            "cerna_jenis_minum" => $Params['CERNA_JENIS_MINUM'],
            "cerna_mulut_bersih" => $Params['CERNA_MULUT_BERSIH'],
            "cerna_mulut_kotor" => $Params['CERNA_MULUT_KOTOR'],
            "cerna_mulut_berbau" => $Params['CERNA_MULUT_BERBAU'],
            "cerna_mukosa_lembab" => $Params['CERNA_MUKOSA_LEMBAB'],
            "cerna_mukosa_kering" => $Params['CERNA_MUKOSA_KERING'],
            "cerna_mukosa_stomatitis" => $Params['CERNA_MUKOSA_STOMATITIS'],
            "cerna_tenggorokan_sakit" => $Params['CERNA_TENGGOROKAN_SAKIT'],
            "cerna_tenggorokan_nyeri_tekan" => $Params['CERNA_TENGGOROKAN_NYERI_TEKAN'],
            "cerna_tenggorokan_pembesaranto" => $Params['CERNA_TENGGOROKAN_PEMBESARANTO'],
            "cerna_perut_normal" => $Params['CERNA_PERUT_NORMAL'],
            "cerna_perut_distended" => $Params['CERNA_PERUT_DISTENDED'],
            "cerna_perut_meteorismus" => $Params['CERNA_PERUT_METEORISMUS'],
            "cerna_perut_nyeri_tekan" => $Params['CERNA_PERUT_NYERI_TEKAN'],
            "cerna_perut_lokasi_nyeri" => $Params['CERNA_PERUT_LOKASI_NYERI'],
            "cerna_peristaltik" => $Params['CERNA_PERISTALTIK'],
            "cerna_turgor_kulit" => $Params['CERNA_TURGOR_KULIT'],
            "cerna_pembesaran_hepar" => $Params['CERNA_PEMBESARAN_HEPAR'],
            "cerna_hematemesis" => $Params['CERNA_HEMATEMESIS'],
            "cerna_evakuasi_cairan_ascites" => $Params['CERNA_EVAKUASI_CAIRAN_ASCITES'],
            "cerna_jml_cairan_ascites" => $Params['CERNA_JML_CAIRAN_ASCITES'],
            "cerna_frek_bab" => $Params['CERNA_FREK_BAB'],
            "cerna_konsistensi" => $Params['CERNA_KONSISTENSI'],
            "cerna_bau_bab" => $Params['CERNA_BAU_BAB'],
            "cerna_warna_bab" => $Params['CERNA_WARNA_BAB'],
            "cerna_warna_cairan_ascites" => $Params['CERNA_WARNA_CAIRAN_ASCITES'],
            "cerna_masalah" => $Params['CERNA_MASALAH'],
            "endokrin_tyroid" => $Params['ENDOKRIN_TYROID'],
            "endokrin_hipoglikemia" => $Params['ENDOKRIN_HIPOGLIKEMIA'],
            "endokrin_luka_gangren" => $Params['ENDOKRIN_LUKA_GANGREN'],
            "endokrin__pus" => $Params['ENDOKRIN__PUS'],
            "endokrin_masalah" => $Params['ENDOKRIN_MASALAH'],
            "personal_mandi" => $Params['PERSONAL_MANDI'],
            "personal_sikatgigi" => $Params['PERSONAL_SIKATGIGI'],
            "personal_keramas" => $Params['PERSONAL_KERAMAS'],
            "personal_gantipakaian" => $Params['PERSONAL_GANTIPAKAIAN'],
            "personal_masalah" => $Params['PERSONAL_MASALAH'],
            "psikososial_orangdekat" => $Params['PSIKOSOSIAL_ORANGDEKAT'],
            "psikososial_kegiatan_ibadah" => $Params['PSIKOSOSIAL_KEGIATAN_IBADAH'],
            "psikososial_masalah" => $Params['PSIKOSOSIAL_MASALAH'],
            "terapi_penunjang" => $Params['TERAPI_PENUNJANG'],
        );

        $this->load->model('ASKEP/tblviewaskepdetpengkajian');

        $this->tblviewaskepdetpengkajian->db->where($criteria, null, false);

        $query = $this->tblviewaskepdetpengkajian->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepdetpengkajian->db->where($criteria, null, false);
            $result = $this->tblviewaskepdetpengkajian->Update($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        } else {
            $data["kd_pasien_kunj"] = $Params['KD_PASIEN_KUNJ'];
            $data["kd_unit_kunj"] = $Params['KD_UNIT_KUNJ'];
            $data["urut_masuk_kunj"] = $Params['URUT_MASUK_KUNJ'];
            $data["tgl_masuk_kunj"] = $Params['TGL_MASUK_KUNJ'];
            $result = $this->tblviewaskepdetpengkajian->Save($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        }
    }
    
    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewaskepdetpengkajian');

        $this->tblviewaskepdetpengkajian->db->where($criteria, null, false);

        $query = $this->tblviewaskepdetpengkajian->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepdetpengkajian->db->where($criteria, null, false);
            $result = $this->tblviewaskepdetpengkajian->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>