<?php

/**
 * @author
 * @copyright
 */
class viewaskepresumeasuhan extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskepresumeasuhan');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskepresumeasuhan->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskepresumeasuhan->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $criteria = $Params['query'];

        $data = array(
            "kd_status_pulang" => $Params['KD_STATUS_PULANG'],
            "kd_rujukan" => $Params['KD_RUJUKAN'],
            "diagnosa_akhir" => $Params['DIAGNOSA_AKHIR'],
            "masalah_keperawatan" => $Params['MASALAH_KEPERAWATAN'],
            "mobilisasi_jalan" => $Params['MOBILISASI_JALAN'],
            "mobilisasi_tongkat" => $Params['MOBILISASI_TONGKAT'],
            "mobilisasi_kursi_roda" => $Params['MOBILISASI_KURSI_RODA'],
            "mobilisasi_brankar" => $Params['MOBILISASI_BRANKAR'],
            "alat_tidakada" => $Params['ALAT_TIDAKADA'],
            "alat_bantu_kateter" => $Params['ALAT_BANTU_KATETER'],
            "alat_bantu_oksigen" => $Params['ALAT_BANTU_OKSIGEN'],
            "alat_bantu_infus" => $Params['ALAT_BANTU_INFUS'],
            "alat_bantu_ngt" => $Params['ALAT_BANTU_NGT'],
            "alat_bantu_lain" => $Params['ALAT_BANTU_LAIN'],
            "alat_bantu_ket" => $Params['ALAT_BANTU_KET'],
            "saran_tindakan" => $Params['SARAN_TINDAKAN'],
            "penyuluhan_makan" => $Params['PENYULUHAN_MAKAN'],
            "penyuluhan_obat" => $Params['PENYULUHAN_OBAT'],
            "penyuluhan_rawatluka" => $Params['PENYULUHAN_RAWATLUKA'],
            "penyuluhan_relaksasi" => $Params['PENYULUHAN_RELAKSASI'],
            "penyuluhan_batukefektif" => $Params['PENYULUHAN_BATUKEFEKTIF'],
            "penyuluhan_batukfisio" => $Params['PENYULUHAN_BATUKFISIO'],
            "penyuluhan_rawatbbl" => $Params['PENYULUHAN_RAWATBBL'],
            "penyuluhan_alat_bantu" => $Params['PENYULUHAN_ALAT_BANTU'],
            "penyuluhan_hearing_aid" => $Params['PENYULUHAN_HEARING_AID'],
            "penyuluhan_kateter" => $Params['PENYULUHAN_KATETER'],
            "penyuluhan_larangan" => $Params['PENYULUHAN_LARANGAN'],
            "penyuluhan_rawatpayudara" => $Params['PENYULUHAN_RAWATPAYUDARA'],
            "penyuluhan_larutanpk" => $Params['PENYULUHAN_LARUTANPK'],
            "penyuluhan_diet" => $Params['PENYULUHAN_DIET'],
            "penyuluhan_ngt" => $Params['PENYULUHAN_NGT'],
            "penyuluhan_infus" => $Params['PENYULUHAN_INFUS'],
            "penyuluhan_oksigen" => $Params['PENYULUHAN_OKSIGEN'],
            "penyuluhan_lain" => $Params['PENYULUHAN_LAIN'],
            "penyuluhan_ket" => $Params['PENYULUHAN_KET'],
        );

        $this->load->model('ASKEP/tblviewaskepresumeasuhan');

        $this->tblviewaskepresumeasuhan->db->where($criteria, null, false);

        $query = $this->tblviewaskepresumeasuhan->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepresumeasuhan->db->where($criteria, null, false);
            $result = $this->tblviewaskepresumeasuhan->Update($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        } else {
            $data["kd_pasien_kunj"] = $Params['KD_PASIEN_KUNJ'];
            $data["kd_unit_kunj"] = $Params['KD_UNIT_KUNJ'];
            $data["urut_masuk_kunj"] = $Params['URUT_MASUK_KUNJ'];
            $data["tgl_masuk_kunj"] = $Params['TGL_MASUK_KUNJ'];
            $result = $this->tblviewaskepresumeasuhan->Save($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        }
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewaskepresumeasuhan');

        $this->tblviewaskepresumeasuhan->db->where($criteria, null, false);

        $query = $this->tblviewaskepresumeasuhan->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepresumeasuhan->db->where($criteria, null, false);
            $result = $this->tblviewaskepresumeasuhan->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>