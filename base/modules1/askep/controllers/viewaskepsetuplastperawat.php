<?php

/**
 * @author
 * @copyright
 */
class viewaskepsetuplastperawat extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save() {
        try {
            $strQuery = "select kd_perawat from perawat order by kd_perawat desc limit 1";
            $query = $this->db->query($strQuery);
            $res = $query->result();
            if ($query->num_rows() > 0) {
                foreach ($res as $data) {

                    $tmpkode = $data->kd_perawat;
                    IF ($tmpkode === "" || $tmpkode === NULL) {
                        $kd_perawat = '001';
                    } else {
                        $kd_perawat = $tmpkode + 1;
                    }
                }
            }
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:' . $query->num_rows() . ', kode:' . json_encode($kd_perawat) . '}';
    }

}

?>