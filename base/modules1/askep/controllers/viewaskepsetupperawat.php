<?php

/**
 * @author
 * @copyright
 */
class viewaskepsetupperawat extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskepsetupperawat');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskepsetupperawat->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskepsetupperawat->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $list = $Params['LIST'];

        $this->load->model('ASKEP/tblviewaskepsetupperawat');

        $cut = explode('@@@@', $list);

        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('<<>>', $cut[$i]);
            if ($res[3] == "Aktif" || $res[3] == "true") {
                $res[3] = 1;
            } else {
                $res[3] = 0;
            }
            if ($res[4] == 'null') {
                $status = '';
            } else {
                $status = $res[4];
            }
//                echo $res[3];
            $data = array(
                'nama_perawat' => $res[2],
                'aktif' => $res[3],
                'nip' => $status,
            );
            $datacari = array(
                'kd_perawat' => "'" . $res[1] . "'",
            );
            $this->tblviewaskepsetupperawat->db->where($datacari, null, false);
            $query = $this->tblviewaskepsetupperawat->GetRowList();
            if ($query[1] != 0) {
                $this->tblviewaskepsetupperawat->db->where($datacari, null, false);
                $result = $this->tblviewaskepsetupperawat->Update($data);
            } else {
                $data["kd_perawat"] = $res[1];
                $data["nama_perawat"] = $res[2];
                $data["aktif"] = $res[3];
                $data["nip"] = $res[4];
                $result = $this->tblviewaskepsetupperawat->Save($data);
            }
        }
        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewaskepsetupperawat');

        $this->tblviewaskepsetupperawat->db->where($criteria, null, false);

        $query = $this->tblviewaskepsetupperawat->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepsetupperawat->db->where($criteria, null, false);
            $result = $this->tblviewaskepsetupperawat->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>