<?php

/**
 * @author
 * @copyright
 */
class viewaskepstatuspulang extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewaskepstatuspulang');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewaskepstatuspulang->db->where($criteria, null, false);
        }
        $query = $this->tblviewaskepstatuspulang->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $list = $Params['LIST'];

        $this->load->model('ASKEP/tblviewaskepstatuspulang');

        $cut = explode('@@@@', $list);

        for ($i = 0; $i < count($cut); $i++) {
            $res = explode('<<>>', $cut[$i]);

            $data = array(
                'status_pulang' => $res[2]
            );
            $datacari = array(
                'kd_status_pulang' => "'" . $res[1] . "'",
            );
            $this->tblviewaskepstatuspulang->db->where($datacari, null, false);
            $query = $this->tblviewaskepstatuspulang->GetRowList();
            if ($query[1] != 0) {
                $this->tblviewaskepstatuspulang->db->where($datacari, null, false);
                $result = $this->tblviewaskepstatuspulang->Update($data);
            } else {
                $data["kd_status_pulang"] = $res[1];
                $data["status_pulang"] = $res[2];
                $result = $this->tblviewaskepstatuspulang->Save($data);
            }
        }
        if ($result > 0) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewaskepstatuspulang');

        $this->tblviewaskepstatuspulang->db->where($criteria, null, false);

        $query = $this->tblviewaskepstatuspulang->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewaskepstatuspulang->db->where($criteria, null, false);
            $result = $this->tblviewaskepstatuspulang->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>