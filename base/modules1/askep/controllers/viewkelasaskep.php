<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class viewkelasaskep extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        $kd_spesial = $Params[4];

        try {
            if ($kd_spesial == '0') {
                $query = $this->db->query("select akelas.Kd_Unit,(akelas.Nama_unit) as Fieldjoin , akelas.kd_kelas  
                                        from Unit as akelas  
                                        inner join kelas as pkelas on pkelas.kd_kelas=akelas.kd_kelas 
                                        where akelas.kd_Unit || akelas.kd_kelas 
                                        in ( select Spc_unit.kd_Unit || Spc_unit.kd_kelas from Spc_unit)")->result();
            } else {
                $query = $this->db->query("select akelas.Kd_Unit,(akelas.Nama_unit) as Fieldjoin , akelas.kd_kelas  
                                        from Unit as akelas  
                                        inner join kelas as pkelas on pkelas.kd_kelas=akelas.kd_kelas 
                                        where akelas.kd_Unit || akelas.kd_kelas 
                                        in ( select Spc_unit.kd_Unit || Spc_unit.kd_kelas from Spc_unit where kd_spesial = " . $kd_spesial . ")")->result();
            }
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        echo '{success:true, ListDataObj:' . json_encode($query) . '}';
    }

}

?>
