<?php

/**
 * @author
 * @copyright
 */
class viewsoap extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewsoap');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewsoap->db->where($criteria, null, false);
        }
        $query = $this->tblviewsoap->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {
        $criteria = $Params['query'];
        $tmpjam = $Params['JAM_SOAP'];
        $tmphari = date('Y-m-d');
        
        $tmpjamsoap =  date("H:i:00", strtotime($tmpjam));
        $data = array(
            "tgl_soap" => $Params['TGL_SOAP'],
            "jam_soap" => $tmphari." ".$tmpjamsoap,
            "kd_perawat" => $Params['KD_PERAWAT'],
            "subject" => $Params['SUBJECT'],
            "object" => $Params['OBJECT'],
            "assusment" => $Params['ASSUSMENT'],
            "planing" => $Params['PLANING']
        );

        $this->load->model('ASKEP/tblviewsoap');

        $this->tblviewsoap->db->where($criteria, null, false);

        $query = $this->tblviewsoap->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewsoap->db->where($criteria, null, false);
            $result = $this->tblviewsoap->Update($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        } else {
            $data["kd_pasien_kunj"] = $Params['KD_PASIEN_KUNJ'];
            $data["kd_unit_kunj"] = $Params['KD_UNIT_KUNJ'];
            $data["urut_masuk_kunj"] = $Params['URUT_MASUK_KUNJ'];
            $data["tgl_masuk_kunj"] = $Params['TGL_MASUK_KUNJ'];
            $result = $this->tblviewsoap->Save($data);
            if ($result > 0) {
                echo "{success: true}";
            } else {
                echo '{success: false}';
            }
        }
    }
    
    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('ASKEP/tblviewsoap');

        $this->tblviewsoap->db->where($criteria, null, false);

        $query = $this->tblviewsoap->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewsoap->db->where($criteria, null, false);
            $result = $this->tblviewsoap->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>