<?php

/**
 * @author
 * @copyright
 */
class viewspesialisasilaporan extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('ASKEP/tblviewspesialisasilaporan');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewspesialisasilaporan->db->where($criteria, null, false);
        }
        $query = $this->tblviewspesialisasilaporan->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

}

?>