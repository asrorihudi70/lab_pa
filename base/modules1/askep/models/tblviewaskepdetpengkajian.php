﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepdetpengkajian extends TblBase {

//    function __construct() {
//        $this->StrSql = "kd_pasien_kunj, kd_unit_kunj, urut_masuk_kunj, tgl_masuk_kunj, riwayat_utama,riwayat_penyakit_sekarang, riwayat_penyakit_dahulu, nafas_paten, nafas_obstruktif, nafas_jelas, nafas_pola_simetri, nafas_pola_asimetri, nafas_suara_normal, nafas_suara_hipersonor,  nafas_menurun, nafas_jenis_normal, nafas_jenis_tachypnoe, nafas_jenis_cheynestokes, 
//                            nafas_jenis_retractive, nafas_jenis_kusmaul, nafas_jenis_dispnoe,  nafas_rr, nafas_suara_wheezing, nafas_suara_ronchi, nafas_suara_rales, nafas_evakuasi_cairan, nafas_jml_cairan, nafas_warna_cairan, nafas_masalah, jantung_reguler, jantung_irreguler, jantung_s1s2_tunggal,  jantung_nyeri_dada, jantung_nyeri_dada_ket, jantung_bunyi_murmur, jantung_bunyi_gallop, 
//                            jantung_bunyi_bising, jantung_crt, jantung_akral_hangat, jantung_akral_dingin, jantung_peningkatan_jvp, jantung_ukuran_cvp, jantung_warna_jaundice, jantung_warna_sianotik, jantung_warna_kemerahan, jantung_warna_pucat, jantung_tekanan_darah, jantung_nadi, jantung_temp, jantung_masalah, otot_sendi_bebas, otot_sendi_terbatas, otot_kekuatan, otot_odema_ekstrimitas, 
//                            otot_kelainan_bentuk, otot_krepitasi, otot_masalah, syaraf_gcs_eye, syaraf_gcs_verbal, syaraf_gcs_motorik, syaraf_gcs_total, syaraf_fisiologis_brachialis, syaraf_fisiologis_patella, syaraf_fisiologis_achilles, syaraf_patologis_choddoks, syaraf_patologis_babinski, syaraf_patologis_budzinzky, syaraf_patologis_kernig, syaraf_masalah, indra_pupil_isokor, indra__pupil_anisokor, indra__konjungtiva_anemis, 
//                            indra__konjungtiva_icterus, indra__konjungtiva_tidak, indra_gangguan_dengar, indra_otorhea, indra_bentuk_hidung, indra_bentuk_hidung_ket, indra_gangguan_cium, indra_rhinorhea, indra_masalah, kemih_kebersihan, 
//                            kemih_alat_bantu, kemih_jml_urine, kemih_warna_urine, kemih_bau, 
//                            kemih_kandung_membesar, kemih_nyeri_tekan, kemih_gangguan_anuria, 
//                            kemih_gangguan_aliguria, kemih_gangguan_retensi, kemih_gangguan_inkontinensia, 
//                            kemih_gangguan_disuria, kemih_gangguanhematuria, kemih_masalah, 
//                            cerna_makan_habis, cerna_makan_ket, cerna_makan_frekuensi, cerna_jml_minum, 
//                            cerna_jenis_minum, cerna_mulut_bersih, cerna_mulut_kotor, cerna_mulut_berbau, 
//                            cerna_mukosa_lembab, cerna_mukosa_kering, cerna_mukosa_stomatitis, 
//                            cerna_tenggorokan_sakit, cerna_tenggorokan_nyeri_tekan, cerna_tenggorokan_pembesaranto, 
//                            cerna_perut_normal, cerna_perut_distended, cerna_perut_meteorismus, 
//                            cerna_perut_nyeri_tekan, cerna_perut_lokasi_nyeri, cerna_peristaltik, 
//                            cerna_turgor_kulit, cerna_pembesaran_hepar, cerna_hematemesis, 
//                            cerna_evakuasi_cairan_ascites, cerna_jml_cairan_ascites, cerna_frek_bab, 
//                            cerna_konsistensi, cerna_bau_bab, cerna_warna_bab, cerna_masalah, 
//                            endokrin_tyroid, endokrin_hipoglikemia, endokrin_luka_gangren, 
//                            endokrin__pus, endokrin_masalah, personal_mandi, personal_sikatgigi, 
//                            personal_keramas, personal_gantipakaian, personal_masalah, psikososial_orangdekat, 
//                            psikososial_kegiatan_ibadah, psikososial_masalah, terapi_penunjang, 
//                            cerna_warna_cairan_ascites
//                           ";
//        $this->SqlQuery = "select * from askep_periksa";
//        $this->TblName = '';
//        TblBase::TblBase(true);
//    }
    
    function __construct()
    {
        $this->TblName='askep_periksa';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec) {
        $row = new Rowpengkajian;

        $row->KD_PASIEN_KUNJ = $rec->kd_pasien_kunj;
        $row->KD_UNIT_KUNJ = $rec->kd_unit_kunj;
        $row->URUT_MASUK_KUNJ = $rec->urut_masuk_kunj;
        $row->TGL_MASUK_KUNJ = $rec->tgl_masuk_kunj;
        $row->RIWAYAT_UTAMA = $rec->riwayat_utama;
        $row->RIWAYAT_PENYAKIT_SEKARANG = $rec->riwayat_penyakit_sekarang;
        $row->RIWAYAT_PENYAKIT_DAHULU = $rec->riwayat_penyakit_dahulu;
        
        $row->NAFAS_PATEN = $rec->nafas_paten;
        $row->NAFAS_OBSTRUKTIF = $rec->nafas_obstruktif;
        $row->NAFAS_JELAS = $rec->nafas_jelas;
        $row->NAFAS_POLA_SIMETRI = $rec->nafas_pola_simetri;
        $row->NAFAS_POLA_ASIMETRI = $rec->nafas_pola_asimetri;
        $row->NAFAS_SUARA_NORMAL = $rec->nafas_suara_normal;
        $row->NAFAS_SUARA_HIPERSONOR = $rec->nafas_suara_hipersonor;
        $row->NAFAS_MENURUN = $rec->nafas_menurun;
        $row->NAFAS_JENIS_NORMAL = $rec->nafas_jenis_normal;
        $row->NAFAS_JENIS_TACHYPNOE = $rec->nafas_jenis_tachypnoe;
        $row->NAFAS_JENIS_CHEYNESTOKES = $rec->nafas_jenis_cheynestokes;
        $row->NAFAS_JENIS_RETRACTIVE = $rec->nafas_jenis_retractive;
        $row->NAFAS_JENIS_KUSMAUL = $rec->nafas_jenis_kusmaul;
        $row->NAFAS_JENIS_DISPNOE = $rec->nafas_jenis_dispnoe;
        $row->NAFAS_RR = $rec->nafas_rr;
        $row->NAFAS_SUARA_WHEEZING = $rec->nafas_suara_wheezing;
        $row->NAFAS_SUARA_RONCHI = $rec->nafas_suara_ronchi;
        $row->NAFAS_SUARA_RALES = $rec->nafas_suara_rales;
        $row->NAFAS_EVAKUASI_CAIRAN = $rec->nafas_evakuasi_cairan;
        $row->NAFAS_JML_CAIRAN = $rec->nafas_jml_cairan;
        $row->NAFAS_WARNA_CAIRAN = $rec->nafas_warna_cairan;
        $row->NAFAS_MASALAH = $rec->nafas_masalah;
        
        $row->JANTUNG_REGULER = $rec->jantung_reguler;
        $row->JANTUNG_IRREGULER = $rec->jantung_irreguler;
        $row->JANTUNG_S1S2_TUNGGAL = $rec->jantung_s1s2_tunggal;
        $row->JANTUNG_NYERI_DADA = $rec->jantung_nyeri_dada;
        $row->JANTUNG_NYERI_DADA_KET = $rec->jantung_nyeri_dada_ket;
        $row->JANTUNG_BUNYI_MURMUR = $rec->jantung_bunyi_murmur;
        $row->JANTUNG_BUNYI_GALLOP = $rec->jantung_bunyi_gallop;
        $row->JANTUNG_BUNYI_BISING = $rec->jantung_bunyi_bising;
        $row->JANTUNG_CRT = $rec->jantung_crt;
        $row->JANTUNG_AKRAL_HANGAT = $rec->jantung_akral_hangat;
        $row->JANTUNG_AKRAL_DINGIN = $rec->jantung_akral_dingin;
        $row->JANTUNG_PENINGKATAN_JVP = $rec->jantung_peningkatan_jvp;
        $row->JANTUNG_UKURAN_CVP = $rec->jantung_ukuran_cvp;
        $row->JANTUNG_WARNA_JAUNDICE = $rec->jantung_warna_jaundice;
        $row->JANTUNG_WARNA_SIANOTIK = $rec->jantung_warna_sianotik;
        $row->JANTUNG_WARNA_KEMERAHAN = $rec->jantung_warna_kemerahan;
        $row->JANTUNG_WARNA_PUCAT = $rec->jantung_warna_pucat;
        $row->JANTUNG_TEKANAN_DARAH = $rec->jantung_tekanan_darah;
        $row->JANTUNG_NADI = $rec->jantung_nadi;
        $row->JANTUNG_TEMP = $rec->jantung_temp;
        $row->JANTUNG_MASALAH = $rec->jantung_masalah;
        
        $row->OTOT_SENDI_BEBAS = $rec->otot_sendi_bebas;
        $row->OTOT_SENDI_TERBATAS = $rec->otot_sendi_terbatas;
        $row->OTOT_KEKUATAN = $rec->otot_kekuatan;
        $row->OTOT_ODEMA_EKSTRIMITAS = $rec->otot_odema_ekstrimitas;
        $row->OTOT_KELAINAN_BENTUK = $rec->otot_kelainan_bentuk;
        $row->OTOT_KREPITASI = $rec->otot_krepitasi;
        $row->OTOT_MASALAH = $rec->otot_masalah;
        
        $row->SYARAF_GCS_EYE = $rec->syaraf_gcs_eye;
        $row->SYARAF_GCS_VERBAL = $rec->syaraf_gcs_verbal;
        $row->SYARAF_GCS_MOTORIK = $rec->syaraf_gcs_motorik;
        $row->SYARAF_GCS_TOTAL = $rec->syaraf_gcs_total;
        $row->SYARAF_FISIOLOGIS_BRACHIALIS = $rec->syaraf_fisiologis_brachialis;
        $row->SYARAF_FISIOLOGIS_PATELLA = $rec->syaraf_fisiologis_patella;
        $row->SYARAF_FISIOLOGIS_ACHILLES = $rec->syaraf_fisiologis_achilles;
        $row->SYARAF_PATOLOGIS_CHODDOKS = $rec->syaraf_patologis_choddoks;
        $row->SYARAF_PATOLOGIS_BABINSKI = $rec->syaraf_patologis_babinski;
        $row->SYARAF_PATOLOGIS_BUDZINZKY = $rec->syaraf_patologis_budzinzky;
        $row->SYARAF_PATOLOGIS_KERNIG = $rec->syaraf_patologis_kernig;
        $row->SYARAF_MASALAH = $rec->syaraf_masalah;
        
        $row->INDRA_PUPIL_ISOKOR = $rec->indra_pupil_isokor;
        $row->INDRA__PUPIL_ANISOKOR = $rec->indra__pupil_anisokor;
        $row->INDRA__KONJUNGTIVA_ANEMIS = $rec->indra__konjungtiva_anemis;
        $row->INDRA__KONJUNGTIVA_ICTERUS = $rec->indra__konjungtiva_icterus;
        $row->INDRA__KONJUNGTIVA_TIDAK = $rec->indra__konjungtiva_tidak;
        $row->INDRA_GANGGUAN_DENGAR = $rec->indra_gangguan_dengar;
        $row->INDRA_OTORHEA = $rec->indra_otorhea;
        $row->INDRA_BENTUK_HIDUNG = $rec->indra_bentuk_hidung;
        $row->INDRA_BENTUK_HIDUNG_KET = $rec->indra_bentuk_hidung_ket;
        $row->INDRA_GANGGUAN_CIUM = $rec->indra_gangguan_cium;
        $row->INDRA_RHINORHEA = $rec->indra_rhinorhea;
        $row->INDRA_MASALAH = $rec->indra_masalah;
        
        $row->KEMIH_KEBERSIHAN = $rec->kemih_kebersihan;
        $row->KEMIH_ALAT_BANTU = $rec->kemih_alat_bantu;
        $row->KEMIH_JML_URINE = $rec->kemih_jml_urine;
        $row->KEMIH_WARNA_URINE = $rec->kemih_warna_urine;
        $row->KEMIH_BAU = $rec->kemih_bau;
        $row->KEMIH_KANDUNG_MEMBESAR = $rec->kemih_kandung_membesar;
        $row->KEMIH_NYERI_TEKAN = $rec->kemih_nyeri_tekan;
        $row->KEMIH_GANGGUAN_ANURIA = $rec->kemih_gangguan_anuria;
        $row->KEMIH_GANGGUAN_ALIGURIA = $rec->kemih_gangguan_aliguria;
        $row->KEMIH_GANGGUAN_RETENSI = $rec->kemih_gangguan_retensi;
        $row->KEMIH_GANGGUAN_INKONTINENSIA = $rec->kemih_gangguan_inkontinensia;
        $row->KEMIH_GANGGUAN_DISURIA = $rec->kemih_gangguan_disuria;
        $row->KEMIH_GANGGUANHEMATURIA = $rec->kemih_gangguanhematuria;
        $row->KEMIH_MASALAH = $rec->kemih_masalah;
        
        $row->CERNA_MAKAN_HABIS = $rec->cerna_makan_habis;
        $row->CERNA_MAKAN_KET = $rec->cerna_makan_ket;
        $row->CERNA_MAKAN_FREKUENSI = $rec->cerna_makan_frekuensi;
        $row->CERNA_JML_MINUM = $rec->cerna_jml_minum;
        $row->CERNA_JENIS_MINUM = $rec->cerna_jenis_minum;
        $row->CERNA_MULUT_BERSIH = $rec->cerna_mulut_bersih;
        $row->CERNA_MULUT_KOTOR = $rec->cerna_mulut_kotor;
        $row->CERNA_MULUT_BERBAU = $rec->cerna_mulut_berbau;
        $row->CERNA_MUKOSA_LEMBAB = $rec->cerna_mukosa_lembab;
        $row->CERNA_MUKOSA_KERING = $rec->cerna_mukosa_kering;
        $row->CERNA_MUKOSA_STOMATITIS = $rec->cerna_mukosa_stomatitis;
        $row->CERNA_TENGGOROKAN_SAKIT = $rec->cerna_tenggorokan_sakit;
        $row->CERNA_TENGGOROKAN_NYERI_TEKAN = $rec->cerna_tenggorokan_nyeri_tekan;
        $row->CERNA_TENGGOROKAN_PEMBESARANTO = $rec->cerna_tenggorokan_pembesaranto;
        $row->CERNA_PERUT_NORMAL = $rec->cerna_perut_normal;
        $row->CERNA_PERUT_DISTENDED = $rec->cerna_perut_distended;
        $row->CERNA_PERUT_METEORISMUS = $rec->cerna_perut_meteorismus;
        $row->CERNA_PERUT_NYERI_TEKAN = $rec->cerna_perut_nyeri_tekan;
        $row->CERNA_PERUT_LOKASI_NYERI = $rec->cerna_perut_lokasi_nyeri;
        $row->CERNA_PERISTALTIK = $rec->cerna_peristaltik;
        $row->CERNA_TURGOR_KULIT = $rec->cerna_turgor_kulit;
        $row->CERNA_PEMBESARAN_HEPAR = $rec->cerna_pembesaran_hepar;
        $row->CERNA_HEMATEMESIS = $rec->cerna_hematemesis;
        $row->CERNA_EVAKUASI_CAIRAN_ASCITES = $rec->cerna_evakuasi_cairan_ascites;
        $row->CERNA_JML_CAIRAN_ASCITES = $rec->cerna_jml_cairan_ascites;
        $row->CERNA_FREK_BAB = $rec->cerna_frek_bab;
        $row->CERNA_KONSISTENSI = $rec->cerna_konsistensi;
        $row->CERNA_BAU_BAB = $rec->cerna_bau_bab;
        $row->CERNA_WARNA_BAB = $rec->cerna_warna_bab;
        $row->CERNA_MASALAH = $rec->cerna_masalah;
        
        $row->ENDOKRIN_TYROID = $rec->endokrin_tyroid;
        $row->ENDOKRIN_HIPOGLIKEMIA = $rec->endokrin_hipoglikemia;
        $row->ENDOKRIN_LUKA_GANGREN = $rec->endokrin_luka_gangren;
        $row->ENDOKRIN__PUS = $rec->endokrin__pus;
        $row->ENDOKRIN_MASALAH = $rec->endokrin_masalah;
        
        $row->PERSONAL_MANDI = $rec->personal_mandi;
        $row->PERSONAL_SIKATGIGI = $rec->personal_sikatgigi;
        $row->PERSONAL_KERAMAS = $rec->personal_keramas;
        $row->PERSONAL_GANTIPAKAIAN = $rec->personal_gantipakaian;
        $row->PERSONAL_MASALAH = $rec->personal_masalah;
        
        $row->PSIKOSOSIAL_ORANGDEKAT = $rec->psikososial_orangdekat;
        $row->PSIKOSOSIAL_KEGIATAN_IBADAH = $rec->psikososial_kegiatan_ibadah;
        $row->PSIKOSOSIAL_MASALAH = $rec->psikososial_masalah;
        
        $row->TERAPI_PENUNJANG = $rec->terapi_penunjang;
        
        $row->CERNA_WARNA_CAIRAN_ASCITES = $rec->cerna_warna_cairan_ascites;



        return $row;
    }

}

class Rowpengkajian {

    public $KD_PASIEN_KUNJ;
    public $KD_UNIT_KUNJ;
    public $URUT_MASUK_KUNJ;
    public $TGL_MASUK_KUNJ;
    public $RIWAYAT_UTAMA;
    public $RIWAYAT_PENYAKIT_SEKARANG;
    public $RIWAYAT_PENYAKIT_DAHULU;
    public $NAFAS_PATEN;
    public $NAFAS_OBSTRUKTIF;
    public $NAFAS_JELAS;
    public $NAFAS_POLA_SIMETRI;
    public $NAFAS_POLA_ASIMETRI;
    public $NAFAS_SUARA_NORMAL;
    public $NAFAS_SUARA_HIPERSONOR;
    public $NAFAS_MENURUN;
    public $NAFAS_JENIS_NORMAL;
    public $NAFAS_JENIS_TACHYPNOE;
    public $NAFAS_JENIS_CHEYNESTOKES;
    public $NAFAS_JENIS_RETRACTIVE;
    public $NAFAS_JENIS_KUSMAUL;
    public $NAFAS_JENIS_DISPNOE;
    public $NAFAS_RR;
    public $NAFAS_SUARA_WHEEZING;
    public $NAFAS_SUARA_RONCHI;
    public $NAFAS_SUARA_RALES;
    public $NAFAS_EVAKUASI_CAIRAN;
    public $NAFAS_JML_CAIRAN;
    public $NAFAS_WARNA_CAIRAN;
    public $NAFAS_MASALAH;
    public $JANTUNG_REGULER;
    public $JANTUNG_IRREGULER;
    public $JANTUNG_S1S2_TUNGGAL;
    public $JANTUNG_NYERI_DADA;
    public $JANTUNG_NYERI_DADA_KET;
    public $JANTUNG_BUNYI_MURMUR;
    public $JANTUNG_BUNYI_GALLOP;
    public $JANTUNG_BUNYI_BISING;
    public $JANTUNG_CRT;
    public $JANTUNG_AKRAL_HANGAT;
    public $JANTUNG_AKRAL_DINGIN;
    public $JANTUNG_PENINGKATAN_JVP;
    public $JANTUNG_UKURAN_CVP;
    public $JANTUNG_WARNA_JAUNDICE;
    public $JANTUNG_WARNA_SIANOTIK;
    public $JANTUNG_WARNA_KEMERAHAN;
    public $JANTUNG_WARNA_PUCAT;
    public $JANTUNG_TEKANAN_DARAH;
    public $JANTUNG_NADI;
    public $JANTUNG_TEMP;
    public $JANTUNG_MASALAH;
    public $OTOT_SENDI_BEBAS;
    public $OTOT_SENDI_TERBATAS;
    public $OTOT_KEKUATAN;
    public $OTOT_ODEMA_EKSTRIMITAS;
    public $OTOT_KELAINAN_BENTUK;
    public $OTOT_KREPITASI;
    public $OTOT_MASALAH;
    public $SYARAF_GCS_EYE;
    public $SYARAF_GCS_VERBAL;
    public $SYARAF_GCS_MOTORIK;
    public $SYARAF_GCS_TOTAL;
    public $SYARAF_FISIOLOGIS_BRACHIALIS;
    public $SYARAF_FISIOLOGIS_PATELLA;
    public $SYARAF_FISIOLOGIS_ACHILLES;
    public $SYARAF_PATOLOGIS_CHODDOKS;
    public $SYARAF_PATOLOGIS_BABINSKI;
    public $SYARAF_PATOLOGIS_BUDZINZKY;
    public $SYARAF_PATOLOGIS_KERNIG;
    public $SYARAF_MASALAH;
    public $INDRA_PUPIL_ISOKOR;
    public $INDRA__PUPIL_ANISOKOR;
    public $INDRA__KONJUNGTIVA_ANEMIS;
    public $INDRA__KONJUNGTIVA_ICTERUS;
    public $INDRA__KONJUNGTIVA_TIDAK;
    public $INDRA_GANGGUAN_DENGAR;
    public $INDRA_OTORHEA;
    public $INDRA_BENTUK_HIDUNG;
    public $INDRA_BENTUK_HIDUNG_KET;
    public $INDRA_GANGGUAN_CIUM;
    public $INDRA_RHINORHEA;
    public $INDRA_MASALAH;
    public $KEMIH_KEBERSIHAN;
    public $KEMIH_ALAT_BANTU;
    public $KEMIH_JML_URINE;
    public $KEMIH_WARNA_URINE;
    public $KEMIH_BAU;
    public $KEMIH_KANDUNG_MEMBESAR;
    public $KEMIH_NYERI_TEKAN;
    public $KEMIH_GANGGUAN_ANURIA;
    public $KEMIH_GANGGUAN_ALIGURIA;
    public $KEMIH_GANGGUAN_RETENSI;
    public $KEMIH_GANGGUAN_INKONTINENSIA;
    public $KEMIH_GANGGUAN_DISURIA;
    public $KEMIH_GANGGUANHEMATURIA;
    public $KEMIH_MASALAH;
    public $CERNA_MAKAN_HABIS;
    public $CERNA_MAKAN_KET;
    public $CERNA_MAKAN_FREKUENSI;
    public $CERNA_JML_MINUM;
    public $CERNA_JENIS_MINUM;
    public $CERNA_MULUT_BERSIH;
    public $CERNA_MULUT_KOTOR;
    public $CERNA_MULUT_BERBAU;
    public $CERNA_MUKOSA_LEMBAB;
    public $CERNA_MUKOSA_KERING;
    public $CERNA_MUKOSA_STOMATITIS;
    public $CERNA_TENGGOROKAN_SAKIT;
    public $CERNA_TENGGOROKAN_NYERI_TEKAN;
    public $CERNA_TENGGOROKAN_PEMBESARANTO;
    public $CERNA_PERUT_NORMAL;
    public $CERNA_PERUT_DISTENDED;
    public $CERNA_PERUT_METEORISMUS;
    public $CERNA_PERUT_NYERI_TEKAN;
    public $CERNA_PERUT_LOKASI_NYERI;
    public $CERNA_PERISTALTIK;
    public $CERNA_TURGOR_KULIT;
    public $CERNA_PEMBESARAN_HEPAR;
    public $CERNA_HEMATEMESIS;
    public $CERNA_EVAKUASI_CAIRAN_ASCITES;
    public $CERNA_JML_CAIRAN_ASCITES;
    public $CERNA_FREK_BAB;
    public $CERNA_KONSISTENSI;
    public $CERNA_BAU_BAB;
    public $CERNA_WARNA_BAB;
    public $CERNA_MASALAH;
    public $ENDOKRIN_TYROID;
    public $ENDOKRIN_HIPOGLIKEMIA;
    public $ENDOKRIN_LUKA_GANGREN;
    public $ENDOKRIN__PUS;
    public $ENDOKRIN_MASALAH;
    public $PERSONAL_MANDI;
    public $PERSONAL_SIKATGIGI;
    public $PERSONAL_KERAMAS;
    public $PERSONAL_GANTIPAKAIAN;
    public $PERSONAL_MASALAH;
    public $PSIKOSOSIAL_ORANGDEKAT;
    public $PSIKOSOSIAL_KEGIATAN_IBADAH;
    public $PSIKOSOSIAL_MASALAH;
    public $TERAPI_PENUNJANG;
    public $CERNA_WARNA_CAIRAN_ASCITES;

}
