﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepdetsmp1 extends TblBase {

    function __construct() {
        $this->StrSql = "kd_variabel, variabel, psbaru, pslama";
        $this->SqlQuery = "Select KD_VARIABEL, VARIABEL, '' as psbaru, '' as pslama
                            From ASKEP_VARIABEL_MUTU";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new RowSmp1;

        $row->KD_VARIABEL = $rec->kd_variabel;
        $row->VARIABEL = $rec->variabel;
        $row->PSBARU = $rec->psbaru;
        $row->PSLAMA = $rec->pslama;
        return $row;
    }

}

class RowSmp1 {

    public $KD_VARIABEL;
    public $VARIABEL;
    public $PSBARU;
    public $PSLAMA;

}
