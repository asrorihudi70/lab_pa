﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskeppengkajian extends TblBase {

    function __construct() {
        $this->StrSql = "kd_pasien,nama,alamat,kelas,spesialisasi,nama_kamar,kd_kelas,kd_unit_kamar,"
                . "     no_kamar,kd_unit,tgl_masuk,urut_masuk,tgl_inap,jam_inap,tgl_keluar,bed,kd_spesial,akhir,urut_nginap";
        $this->SqlQuery = " SELECT P.KD_PASIEN, P.NAMA, P.ALAMAT, KLS.KELAS,S.SPESIALISASI,  K.NAMA_KAMAR,kls.KD_KELAS, 
                            NG.kd_unit_kamar, NG.no_kamar,NG.kd_pasien,NG.kd_unit,NG.tgl_masuk::date,NG.urut_masuk,NG.tgl_inap::date,
                            NG.jam_inap::time,NG.tgl_keluar::date,NG.jam_keluar::time,NG.bed,NG.kd_spesial,NG.akhir,NG.urut_nginap
                            FROM NGINAP NG  
                            INNER JOIN PASIEN P ON NG.KD_PASIEN = P.KD_PASIEN  
                            INNER JOIN UNIT U ON NG.KD_UNIT = U.KD_UNIT  
                            INNER JOIN KELAS KLS ON U.KD_KELAS = KLS.KD_KELAS  
                            INNER JOIN SPESIALISASI S ON NG.KD_SPESIAL  = S.KD_SPESIAL  
                            INNER JOIN KAMAR K ON NG.NO_KAMAR = K.NO_KAMAR  and ng.KD_UNIT_KAMAR = K.KD_UNIT";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowpengkajian;

        $row->KD_PASIEN = $rec->kd_pasien;
        $row->NAMA = $rec->nama;
        $row->ALAMAT = $rec->alamat;
        $row->KELAS = $rec->kelas;
        $row->SPESIALISASI = $rec->spesialisasi;
        $row->NAMA_KAMAR = $rec->nama_kamar;
        $row->KD_KELAS = $rec->kd_kelas;
        $row->KD_UNIT_KAMAR = $rec->kd_unit_kamar;
        $row->NO_KAMAR = $rec->no_kamar;
        $row->KD_UNIT = $rec->kd_unit;
        $row->TGL_MASUK = $rec->tgl_masuk;
        $row->URUT_MASUK = $rec->urut_masuk;
        $row->TGL_INAP = $rec->tgl_inap;
        $row->JAM_INAP = $rec->jam_inap;
        $row->TGL_KELUAR = $rec->tgl_keluar;
        $row->BED = $rec->bed;
        $row->KD_SPESIAL = $rec->kd_spesial;
        $row->AKHIR = $rec->akhir;
        $row->URUT_NGINAP = $rec->urut_nginap;


        return $row;
    }

}

class Rowpengkajian {

    public $KD_PASIEN;
    public $NAMA;
    public $ALAMAT;
    public $KELAS;
    public $SPESIALISASI;
    public $NAMA_KAMAR;
    public $KD_KELAS;
    public $KD_UNIT_KAMAR;
    public $NO_KAMAR;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $TGL_INAP;
    public $JAM_INAP;
    public $TGL_KELUAR;
    public $BED;
    public $KD_SPESIAL;
    public $AKHIR;
    public $URUT_NGINAP;

}
