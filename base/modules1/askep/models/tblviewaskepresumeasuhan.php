﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepresumeasuhan extends TblBase {

    function __construct() {
        $this->TblName = 'askep_resume';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new RowResume;

        $row->KD_PASIEN_KUNJ = $rec->kd_pasien_kunj;
        $row->KD_UNIT_KUNJ = $rec->kd_unit_kunj;
        $row->URUT_MASUK_KUNJ = $rec->urut_masuk_kunj;
        $row->TGL_MASUK_KUNJ = $rec->tgl_masuk_kunj;
        $row->KD_STATUS_PULANG = $rec->kd_status_pulang;
        $row->KD_RUJUKAN = $rec->kd_rujukan;
        $row->DIAGNOSA_AKHIR = $rec->diagnosa_akhir;
        $row->MASALAH_KEPERAWATAN = $rec->masalah_keperawatan;
        $row->MOBILISASI_JALAN = $rec->mobilisasi_jalan;
        $row->MOBILISASI_TONGKAT = $rec->mobilisasi_tongkat;
        $row->MOBILISASI_KURSI_RODA = $rec->mobilisasi_kursi_roda;
        $row->MOBILISASI_BRANKAR = $rec->mobilisasi_brankar;
        $row->ALAT_TIDAKADA = $rec->alat_tidakada;
        $row->ALAT_BANTU_KATETER = $rec->alat_bantu_kateter;
        $row->ALAT_BANTU_OKSIGEN = $rec->alat_bantu_oksigen;
        $row->ALAT_BANTU_INFUS = $rec->alat_bantu_infus;
        $row->ALAT_BANTU_NGT = $rec->alat_bantu_ngt;
        $row->ALAT_BANTU_LAIN = $rec->alat_bantu_lain;
        $row->ALAT_BANTU_KET = $rec->alat_bantu_ket;
        $row->SARAN_TINDAKAN = $rec->saran_tindakan;
        $row->PENYULUHAN_MAKAN = $rec->penyuluhan_makan;
        $row->PENYULUHAN_OBAT = $rec->penyuluhan_obat;
        $row->PENYULUHAN_RAWATLUKA = $rec->penyuluhan_rawatluka;
        $row->PENYULUHAN_RELAKSASI = $rec->penyuluhan_relaksasi;
        $row->PENYULUHAN_BATUKEFEKTIF = $rec->penyuluhan_batukefektif;
        $row->PENYULUHAN_BATUKFISIO = $rec->penyuluhan_batukfisio;
        $row->PENYULUHAN_RAWATBBL = $rec->penyuluhan_rawatbbl;
        $row->PENYULUHAN_ALAT_BANTU = $rec->penyuluhan_alat_bantu;
        $row->PENYULUHAN_HEARING_AID = $rec->penyuluhan_hearing_aid;
        $row->PENYULUHAN_KATETER = $rec->penyuluhan_kateter;
        $row->PENYULUHAN_LARANGAN = $rec->penyuluhan_larangan;
        $row->PENYULUHAN_RAWATPAYUDARA = $rec->penyuluhan_rawatpayudara;
        $row->PENYULUHAN_LARUTANPK = $rec->penyuluhan_larutanpk;
        $row->PENYULUHAN_DIET = $rec->penyuluhan_diet;
        $row->PENYULUHAN_NGT = $rec->penyuluhan_ngt;
        $row->PENYULUHAN_INFUS = $rec->penyuluhan_infus;
        $row->PENYULUHAN_OKSIGEN = $rec->penyuluhan_oksigen;
        $row->PENYULUHAN_LAIN = $rec->penyuluhan_lain;
        $row->PENYULUHAN_KET = $rec->penyuluhan_ket;



        return $row;
    }

}

class RowResume {

    public $KD_PASIEN_KUNJ;
    public $KD_UNIT_KUNJ;
    public $URUT_MASUK_KUNJ;
    public $TGL_MASUK_KUNJ;
    public $KD_STATUS_PULANG;
    public $KD_RUJUKAN;
    public $DIAGNOSA_AKHIR;
    public $MASALAH_KEPERAWATAN;
    public $MOBILISASI_JALAN;
    public $MOBILISASI_TONGKAT;
    public $MOBILISASI_KURSI_RODA;
    public $MOBILISASI_BRANKAR;
    public $ALAT_TIDAKADA;
    public $ALAT_BANTU_KATETER;
    public $ALAT_BANTU_OKSIGEN;
    public $ALAT_BANTU_INFUS;
    public $ALAT_BANTU_NGT;
    public $ALAT_BANTU_LAIN;
    public $ALAT_BANTU_KET;
    public $SARAN_TINDAKAN;
    public $PENYULUHAN_MAKAN;
    public $PENYULUHAN_OBAT;
    public $PENYULUHAN_RAWATLUKA;
    public $PENYULUHAN_RELAKSASI;
    public $PENYULUHAN_BATUKEFEKTIF;
    public $PENYULUHAN_BATUKFISIO;
    public $PENYULUHAN_RAWATBBL;
    public $PENYULUHAN_ALAT_BANTU;
    public $PENYULUHAN_HEARING_AID;
    public $PENYULUHAN_KATETER;
    public $PENYULUHAN_LARANGAN;
    public $PENYULUHAN_RAWATPAYUDARA;
    public $PENYULUHAN_LARUTANPK;
    public $PENYULUHAN_DIET;
    public $PENYULUHAN_NGT;
    public $PENYULUHAN_INFUS;
    public $PENYULUHAN_OKSIGEN;
    public $PENYULUHAN_LAIN;
    public $PENYULUHAN_KET;

}
