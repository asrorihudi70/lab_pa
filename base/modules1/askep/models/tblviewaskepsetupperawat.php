﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepsetupperawat extends TblBase {

    function __construct() {
        $this->TblName = 'perawat';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new RowSperawat;

        $row->KODE = $rec->kd_perawat;
        $row->NAMA = $rec->nama_perawat;
        if ($rec->aktif === '1') {
            $TMPSTATUS = 'Aktif';
        }else
        {
            $TMPSTATUS = 'Tidak Aktif';
        }
        $row->STATUS = $TMPSTATUS;
        $row->NIP = $rec->nip;

        return $row;
    }

}

class RowSperawat {

    public $KODE;
    public $NAMA;
    public $STATUS;
    public $NIP;

}
