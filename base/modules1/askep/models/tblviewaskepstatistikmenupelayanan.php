﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepstatistikmenupelayanan extends TblBase {

    function __construct() {
        $this->StrSql = "tanggal,kd_unit,nama_unit,no_kamar,nama_kamar,spesialisasi,kd_perawat,perawat, kd_spesial,waktu,kd_waktu";
        $this->SqlQuery = " SELECT distinct ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, kmr.NAMA_KAMAR, sp.SPESIALISASI,  
                            ask.KD_PERAWAT, concat(p.NAMA_PERAWAT,' - ',NIP) as perawat ,sp.KD_SPESIAL, AW.WAKTU, ASK.KD_WAKTU   
                            FROM ASKEP_MUTU_LAYAN  ask  
                            inner join KAMAR kmr on ask.NO_KAMAR = kmr.NO_KAMAR AND ask.KD_UNIT = kmr.KD_UNIT   
                            inner join UNIT u on kmr.KD_UNIT  = u.KD_UNIT  
                            inner join KELAS kls on u.KD_KELAS = kls.KD_KELAS  
                            inner join Spc_unit su on u.KD_KELAS = su.KD_KELAS and u.KD_UNIT = su.KD_UNIT  
                            inner join SPESIALISASI sp on su.kd_spesial  = sp.KD_SPESIAL   
                            inner join PERAWAT p on ask.KD_PERAWAT = p.KD_PERAWAT  
                            inner JOIN ASKEP_WAKTU AW ON ASK.KD_WAKTU = AW.KD_WAKTU ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new RowSmp;

        $row->TANGGAL = $rec->tanggal;
        $row->KD_UNIT = $rec->kd_unit;
        $row->NAMA_UNIT = $rec->nama_unit;
        $row->NO_KAMAR = $rec->no_kamar;
        $row->NAMA_KAMAR = $rec->nama_kamar;
        $row->SPESIALISASI = $rec->spesialisasi;
        $row->KD_PERAWAT = $rec->kd_perawat;
        $row->PERAWAT = $rec->perawat;
        $row->KD_SPESIAL = $rec->kd_spesial;
        $row->WAKTU = $rec->waktu;
        $row->KD_WAKTU = $rec->kd_waktu;
        return $row;
    }

}

class RowSmp {
    
    public $TANGGAL;
    public $KD_UNIT;
    public $NAMA_UNIT;
    public $NO_KAMAR;
    public $NAMA_KAMAR;
    public $SPESIALISASI;
    public $KD_PERAWAT;
    public $PERAWAT;
    public $KD_SPESIAL;
    public $WAKTU;
    public $KD_WAKTU;

}
