﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaskepstatuspulang extends TblBase {

    function __construct() {
        $this->TblName = 'status_pulang';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new RowSstatuspulang;

        $row->KODE = $rec->kd_status_pulang;
        $row->NAMA = $rec->status_pulang;
        return $row;
    }

}

class RowSstatuspulang {

    public $KODE;
    public $NAMA;

}
