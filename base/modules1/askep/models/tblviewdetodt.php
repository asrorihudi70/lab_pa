﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewdetodt extends TblBase {

    function __construct() {
        $this->TblName = 'askep_observasi';
        TblBase::TblBase(true);

        $this->SqlQuery = "Select kd_pasien_kunj,kd_unit_kunj,urut_masuk_kunj,
                    tgl_masuk_kunj::date,tgl_observasi::date,jam_observasi::time,
                    ao.kd_perawat,nama_perawat,
                    tekanan_darah,nadi,detak_jantung,suhu,cvp,wsd,kesadaran,
                    perifer,oral,parenteral,muntah,ngt,bab,bak,pendarahan,tindakan_perawat
                    from askep_observasi ao 
                    inner join perawat p on p.kd_perawat = ao.kd_perawat ";
    }

    function FillRow($rec) {
        $row = new Rowodt;

        $row->KD_PASIEN_KUNJ = $rec->kd_pasien_kunj;
        $row->KD_UNIT_KUNJ = $rec->kd_unit_kunj;
        $row->URUT_MASUK_KUNJ = $rec->urut_masuk_kunj;
        $row->TGL_MASUK_KUNJ = $rec->tgl_masuk_kunj;
        $row->TGL_OBSERVASI = $rec->tgl_observasi;
        $row->JAM_OBSERVASI = $rec->jam_observasi;
        $row->KD_PERAWAT = $rec->kd_perawat;
        $row->NAMA_PERAWAT = $rec->nama_perawat;
        $row->TEKANAN_DARAH = $rec->tekanan_darah;
        $row->NADI = $rec->nadi;
        $row->DETAK_JANTUNG = $rec->detak_jantung;
        $row->SUHU = $rec->suhu;
        $row->CVP = $rec->cvp;
        $row->WSD = $rec->wsd;
        $row->KESADARAN = $rec->kesadaran;
        $row->PERIFER = $rec->perifer;
        $row->ORAL = $rec->oral;
        $row->PARENTERAL = $rec->parenteral;
        $row->MUNTAH = $rec->muntah;
        $row->NGT = $rec->ngt;
        $row->BAB = $rec->bab;
        $row->BAK = $rec->bak;
        $row->PENDARAHAN = $rec->pendarahan;
        $row->TINDAKAN_PERAWAT = $rec->tindakan_perawat;
        return $row;
    }

}

class Rowodt {

    public $KD_PASIEN_KUNJ;
    public $KD_UNIT_KUNJ;
    public $URUT_MASUK_KUNJ;
    public $TGL_MASUK_KUNJ;
    public $TGL_OBSERVASI;
    public $JAM_OBSERVASI;
    public $KD_PERAWAT;
    public $NAMA_PERAWAT;
    public $TEKANAN_DARAH;
    public $NADI;
    public $DETAK_JANTUNG;
    public $SUHU;
    public $CVP;
    public $WSD;
    public $KESADARAN;
    public $PERIFER;
    public $ORAL;
    public $PARENTERAL;
    public $MUNTAH;
    public $NGT;
    public $BAB;
    public $BAK;
    public $PENDARAHAN;
    public $TINDAKAN_PERAWAT;

}
