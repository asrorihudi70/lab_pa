<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewrencanaasuhandetail extends TblBase {

    function __construct() {
        $this->TblName = 'tblaskep_ncp';
        TblBase::TblBase(true);

        $this->SqlQuery = "select a.kd_pasien_kunj,a.kd_unit_kunj,a.urut_masuk_kunj,a.tgl_masuk_kunj::date,"
                . " a.tgl_ncp::date,a.jam::time,a.diagnosa,a.tujuan,a.rencana,a.kd_perawat,b.nama_perawat,"
                . " a.tgl_selesai::date "
                . " from askep_ncp a "
                . "inner join perawat b ON b.kd_perawat = a.kd_perawat ";
    }

    function FillRow($rec) {
        $row = new Rowpengkajian;

        $row->KD_PASIEN_KUNJ = $rec->kd_pasien_kunj;
        $row->KD_UNIT_KUNJ = $rec->kd_unit_kunj;
        $row->URUT_MASUK_KUNJ = $rec->urut_masuk_kunj;
        $row->TGL_MASUK_KUNJ = $rec->tgl_masuk_kunj;
        $row->TGL_NCP = $rec->tgl_ncp;
        $row->JAM = $rec->jam;
        $row->DIAGNOSA = $rec->diagnosa;
        $row->TUJUAN = $rec->tujuan;
        $row->RENCANA = $rec->rencana;
        $row->KD_PERAWAT = $rec->kd_perawat;
        $row->NAMA_PERAWAT = $rec->nama_perawat;
        $row->TGL_SELESAI = $rec->tgl_selesai;


        return $row;
    }

}

class Rowpengkajian {

    public $KD_PASIEN_KUNJ;
    public $KD_UNIT_KUNJ;
    public $URUT_MASUK_KUNJ;
    public $TGL_MASUK_KUNJ;
    public $TGL_NCP;
    public $JAM;
    public $DIAGNOSA;
    public $TUJUAN;
    public $RENCANA;
    public $KD_PERAWAT;
    public $NAMA_PERAWAT;
    public $TGL_SELESAI;

}
