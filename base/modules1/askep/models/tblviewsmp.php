﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewsmp extends TblBase {

    function __construct() {
        $this->TblName = 'askep_mutu_layan';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new Rowsmpr;

        $row->KD_VARIABEL = $rec->kd_variabel;
        $row->TANGGAL = $rec->tanggal;
        $row->KD_UNIT = $rec->kd_unit;
        $row->NO_KAMAR = $rec->no_kamar;
        $row->KD_WAKTU = $rec->kd_waktu;
        $row->KD_PERAWAT = $rec->kd_perawat;
        $row->PSBARU = $rec->psbaru;
        $row->PSLAMA = $rec->pslama;

        return $row;
    }

}

class Rowsmpr {

    public $KD_VARIABEL;
    public $TANGGAL;
    public $KD_UNIT;
    public $NO_KAMAR;
    public $KD_WAKTU;
    public $KD_PERAWAT;
    public $PSBARU;
    public $PSLAMA;

}
