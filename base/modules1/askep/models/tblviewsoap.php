﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewsoap extends TblBase {

    function __construct() {
        $this->TblName = 'askep_soap';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new Rowsoap;

        $row->KD_PASIEN_KUNJ = $rec->kd_pasien_kunj;
        $row->KD_UNIT_KUNJ = $rec->kd_unit_kunj;
        $row->URUT_MASUK_KUNJ = $rec->urut_masuk_kunj;
        $row->TGL_MASUK_KUNJ = $rec->tgl_masuk_kunj;
        $row->TGL_SOAP = $rec->tgl_soap;
        $row->JAM_SOAP = $rec->jam_soap;
        $row->KD_PERAWAT = $rec->kd_perawat;
        $row->SUBJECT = $rec->subject;
        $row->OBJECT = $rec->object;
        $row->ASSUSMENT = $rec->assusment;
        $row->PLANING = $rec->planing;

        return $row;
    }

}

class Rowsoap {

    public $KD_PASIEN_KUNJ;
    public $KD_UNIT_KUNJ;
    public $URUT_MASUK_KUNJ;
    public $TGL_MASUK_KUNJ;
    public $TGL_SOAP;
    public $JAM_SOAP;
    public $KD_PERAWAT;
    public $SUBJECT;
    public $OBJECT;
    public $ASSUSMENT;
    public $PLANING;

}
