﻿<?php
/**
 * @author HDHT
 * @copyright 2015
 */


class tblviewspesialisasi extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_spesial,spesialisasi,folio";
		$this->SqlQuery="SELECT * FROM ( select * from spesialisasi where kd_spesial <> 0   Union All  Select '0' as kd_spesial, 'SEMUA' as spesialisasi, '' as folio ) X   order by kd_spesial";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                
		$row->KD_SPESIAL=$rec->kd_spesial;
                $row->SPESIALISASI=$rec->spesialisasi;
                $row->FOLIO=$rec->folio;

                return $row;
	}
}
class Rowdokter
{
        public $KD_SPESIAL;
        public $SPESIALISASI;
        public $FOLIO;
}