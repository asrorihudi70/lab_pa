<?php

/**
 * @Author Ali
 * @copyright NCI 2015
 */


class functionDistribusiBHP extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridAwal(){
		if($_POST['nokeluar'] == ''){
			$criterianokeluar = "";
		} else{
			$criterianokeluar= "and d.no_keluar='".$_POST['nokeluar']."'";
		}
		
		if($_POST['tglawal'] == '' && $_POST['tglakhir'] == ''){
			$criteriaTgl = " d.tanggal = '".date('Y/m/d')."'";
		} else{
			$criteriaTgl = " d.tanggal between '".$_POST['tglawal']."' and '".$_POST['tglakhir']."'";
		}
		
		$result=$this->db->query("select d.no_keluar,d.kd_lokasi,d.kd_jns_lokasi,d.tanggal,d.update_stok,d.keterangan,jk.jns_lokasi,l.lokasi,
									d.no_spk,d.tgl_spk
									from inv_distribusi d
										inner join inv_jenis_lokasi jk on jk.kd_jns_lokasi=d.kd_jns_lokasi
										inner join inv_lokasi l on l.kd_lokasi=d.kd_lokasi and l.kd_jns_lokasi=d.kd_jns_lokasi
									WHERE ".$criteriaTgl."
									".$criterianokeluar."
									ORDER BY d.no_keluar asc
									LIMIT 50
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridBarangLoad(){
		$result=$this->db->query("select dd.NO_KELUAR,m.KD_INV,m.NO_URUT_BRG,m.NAMA_BRG,m.KD_SATUAN,
										coalesce( s.satuan,'-') as satuan,dd.JUMLAH as jumlah_total,dd.KETERANGAN,dd.NO_BARIS,dd.KD_STOK
									from INV_DETAIL_DISTR dd
										inner join INV_KARTU_STOK k on k.KD_STOK=dd.KD_STOK
										inner join INV_MASTER_BRG m on m.NO_URUT_BRG=k.NO_URUT_BRG
										left join INV_SATUAN s on s.KD_SATUAN=m.KD_SATUAN
									WHERE dd.NO_KELUAR='".$_POST['no_keluar']."'
									ORDER BY dd.no_baris
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getPONumber(){
		$result=$this->db->query("SELECT o.po_number,m.kd_inv,od.no_urut_brg,m.nama_brg,od.jml_order,od.harga, od.JML_ORDER*od.HARGA as jumlah
									FROM inv_order_det od
									INNER JOIN inv_order o ON o.po_number=od.po_number
									INNER JOIN inv_master_brg m ON m.no_urut_brg=od.no_urut_brg
									WHERE (o.po_number,od.no_urut_brg) not in(SELECT po_number,no_urut_brg FROM inv_trm_d)
									AND o.kd_vendor='".$_POST['kdvendor']."'
									AND upper(o.po_number) like upper('".$_POST['text']."%')
									ORDER BY o.po_number limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getGridBarang(){	
		$result=$this->db->query("Select nama_brg, no_urut_brg, kd_inv from inv_master_brg 
									where left(kd_inv,1)='8' and upper(nama_brg) like upper('".$_POST['nama_brg']."%') 
									order by nama_brg
						")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getGridStokBarang(){
		$tgl=date('Y-m-d');
		
		if($_POST['tglawal']!='' || $_POST['tglakhir'] !='' ){
			$tanggal=" '".$_POST['tglawal']."' and '".$_POST['tglakhir']."' ";
		} else{
			$tanggal=" '".$tgl."' and '".$tgl."'";
		}
		
		if($_POST['nama_brg'] !=''){
			$nama="and m.nama_brg like '".$_POST['nama_brg']."%'";
		} else{
			$nama="";
		}
		
		
		$result=$this->db->query("select k.kd_stok,k.tgl_posting,k.no_urut_brg,m.kd_inv,m.nama_brg,k.jumlah_total,m.kd_satuan, coalesce( s.satuan,'-') as satuan
									from inv_kartu_stok k
										inner join inv_master_brg m on m.no_urut_brg=k.no_urut_brg
										left join inv_satuan s on s.kd_satuan=m.kd_satuan
									where k.tgl_posting  between ".$tanggal."
										and k.jumlah_total > 0
										".$nama."
									order by k.kd_stok
						")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function cekPosting(){
		$result = $this->db->query("SELECT status_posting from inv_trm_g
									where no_terima='".$_POST['no_terima']."'")->row()->status_posting;
		if($result == 't' || $result == false){
			echo "{success:true}";
		} else{
			echo "{success:false}";
		}
	}
	
	function getNoBaris($NoKeluar){
		$query = $this->db->query("select no_baris From inv_detail_distr
									where no_keluar ='".$NoKeluar."' 
									order by no_baris desc limit 1");
		if(count($query->result())==0){
			$no_baris = 1;
		} else{
			$no_baris = $query->row()->no_baris;
			$no_baris = $no_baris+1;
		}
		return $no_baris;
	}
	
	function getNoKeluar(){
		//2015-10-0003
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("Select no_keluar from inv_distribusi  
									where left(no_keluar,7)='".$thisYear."-".$thisMonth."'  
									order by no_keluar desc limit 1");
		if(count($query->result()) == 0){
			$NoKeluar=$thisYear."-".$thisMonth."-0001";
		} else{
			$no=substr($query->row()->no_keluar,-4);
			$new=$no+1;
			if(strlen($new) == 1){
				$NoKeluar=$thisYear."-".$thisMonth."-000".$new;
			} else if(strlen($new) == 2){
				$NoKeluar=$thisYear."-".$thisMonth."-00".$new;
			} else if(strlen($new) == 3){
				$NoKeluar=$thisYear."-".$thisMonth."-0".$new;
			} else{
				$NoKeluar=$thisYear."-".$thisMonth."-".$new;
			}
		}
		return $NoKeluar;
	}
	
	
	public function save(){
		$this->db->trans_begin();
		
		$NoKeluar = $_POST['NoKeluar'];
		$KdJnsLokasi = $_POST['KdJnsLokasi'];
		$KdLokasi = $_POST['KdLokasi'];
		$Tanggal = $_POST['Tanggal'];
		$Keterangan = $_POST['Keterangan'];
		$NoSpk = $_POST['NoSpk'];
		$TglSpk = $_POST['TglSpk'];
		
		if($NoSpk == ''){
			$NoSpk='-';
		} else{
			$NoSpk=$NoSpk;
		}
		
		$save=$this->saveDistribusi($NoKeluar,$KdJnsLokasi,$KdLokasi,$Tanggal,$Keterangan,$NoSpk,$TglSpk);
				
		if($save){
			$this->db->trans_commit();
			echo "{success:true, nokeluar:'$save'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	function saveDistribusi($NoKeluar,$KdJnsLokasi,$KdLokasi,$Tanggal,$Keterangan,$NoSpk,$TglSpk){
		$strError = "";
		
		$newNoKeluar=$this->getNoKeluar();
		$jmllist= $_POST['jumlah'];
		
		//data baru
		if($NoKeluar == ''){
			$data = array("no_keluar"=>$newNoKeluar,
							"kd_lokasi"=>$KdLokasi,
							"tanggal"=>$Tanggal,
							"keterangan"=>$Keterangan,
							"update_stok"=>'false',
							"tgl_spk"=>$TglSpk,
							"no_spk"=>$NoSpk,
							"kd_jns_lokasi"=>$KdJnsLokasi
							);
			
			$result=$this->db->insert('inv_distribusi',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_distribusi',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$NoBaris=$this->getNoBaris($newNoKeluar);
					$kd_stok = $_POST['kd_stok-'.$i];
					$keterangangrid = $_POST['keterangan-'.$i];
					$jumlah_total = $_POST['jumlah_total-'.$i];
					
					$dataDet = array("no_keluar"=>$newNoKeluar,
								"kd_stok"=>$kd_stok,
								"no_baris"=>$NoBaris,
								"keterangan"=>$keterangangrid,
								"jumlah"=>$jumlah_total);
					
					$resultDet=$this->db->insert('inv_detail_distr',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_detail_distr',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$newNoKeluar;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{
			//edit
			for($i=0;$i<$jmllist;$i++){
				$kd_stok = $_POST['kd_stok-'.$i];
				$keterangangrid = $_POST['keterangan-'.$i];
				$jumlah_total = $_POST['jumlah_total-'.$i];
				$no_baris = $_POST['no_baris-'.$i];
				if($no_baris == ''){
					$no_baris=0;
				} else{
					$no_baris=$no_baris;
				}
				
				//cek jika data ada maka update
				$qCek = $this->db->query("Select * from inv_detail_distr where no_keluar='".$NoKeluar."' and kd_stok ='".$kd_stok."' and no_baris='".$no_baris."'")->result();
				if(count($qCek) > 0){
					$dataUbah = array("jumlah"=>$jumlah_total,"keterangan"=>$keterangangrid);
					$criteria = array("no_keluar"=>$NoKeluar,"kd_stok"=>$kd_stok,"no_baris"=>$no_baris);
					$this->db->where($criteria);
					$result=$this->db->update('inv_detail_distr',$dataUbah);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_detail_distr',$dataUbah,$criteria);
					//-----------akhir update ke database sql server----------------//
					$no=$NoKeluar;
				} else{
					$NoBaris=$this->getNoBaris($NoKeluar);
					$dataDet = array("no_keluar"=>$NoKeluar,
								"kd_stok"=>$kd_stok,
								"no_baris"=>$NoBaris,
								"keterangan"=>$keterangangrid,
								"jumlah"=>$jumlah_total);
					
					$result=$this->db->insert('inv_detail_distr',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('inv_detail_distr',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($result){
					$strError=$NoKeluar;
				}else{
					$strError='Error';
				}
			}
		}
		
		return $strError;
	}
	
	
	public function updatePosting(){
		$this->db->trans_begin();
		$tgl=date('Y-m-d');
		$no_keluar=$_POST['NoKeluar'];
		$jmllist= $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$kd_stok=$_POST['kd_stok-'.$i];
			$jumlah_total=$_POST['jumlah_total-'.$i];
			
			$dataHistory = array("kd_stok"=>$kd_stok,
						"no_keluar"=>$no_keluar,
						"jumlah"=>$jumlah_total);
			
			$resultHist=$this->db->insert('inv_hist_stok_out',$dataHistory);

			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_hist_stok_out',$dataHistory);
			//-----------akhir insert ke database sql server----------------//
			
			if($resultHist){
				$dataHist = array("update_stok"=>'true');
				$criteria = array("no_keluar"=>$no_keluar);
				$this->db->where($criteria);
				$resultDistr=$this->db->update('inv_distribusi',$dataHist);
				
				//-----------update to sq1 server Database---------------//
				_QMS_update('inv_distribusi',$dataHist,$criteria);
				//-----------akhir update ke database sql server----------------//
				if($resultDistr){
					$query=$this->db->query("select jumlah_total from inv_kartu_stok where kd_stok='".$kd_stok."'")->row()->jumlah_total;
					$jml=$query - $jumlah_total;
					$dataKartu = array("jumlah_total"=>$jml,"tgl_posting"=>$tgl);
					$criteria = array("kd_stok"=>$kd_stok);
					$this->db->where($criteria);
					$result=$this->db->update('inv_kartu_stok',$dataKartu);
					
					//-----------update to sq1 server Database---------------//
					_QMS_update('inv_kartu_stok',$dataKartu,$criteria);
					//-----------akhir update ke database sql server----------------//
				}
			}
		}
		if($result){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusDistribusi(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_detail_distr WHERE no_keluar='".$_POST['no_keluar']."' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_detail_distr WHERE no_keluar='".$_POST['no_keluar']."' ");
		//-----------akhir delete ke database sql server----------------//
		if($query){
			$qDet = $this->db->query("DELETE FROM inv_distribusi WHERE no_keluar='".$_POST['no_keluar']."' ");
		
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM inv_distribusi WHERE no_keluar='".$_POST['no_keluar']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($qDet){
				$hasil='Ok';
			} else{
				$hasil='error';
			}
		} else{
			$hasil='error';
		}
		
		if($hasil == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBarang(){
		$this->db->trans_begin();
		$query = $this->db->query("DELETE FROM inv_detail_distr 
									WHERE no_keluar='".$_POST['no_keluar']."' 
										AND kd_stok='".$_POST['kd_stok']."'
										AND no_baris=".$_POST['no_baris']."");
										
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_detail_distr 
									WHERE no_keluar='".$_POST['no_keluar']."' 
										AND kd_stok='".$_POST['kd_stok']."' 
										AND no_baris=".$_POST['no_baris']."");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
	}
	
	
	
}
?>