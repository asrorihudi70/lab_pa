<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupSatuan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getSatuanGrid(){
		$satuan=$_POST['text'];
		if($satuan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE upper(satuan) like upper('".$satuan."%')";
		}
		$result=$this->db->query("Select kd_satuan,satuan from  inv_satuan
									".$criteria."
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdSatuan(){
		$query = $this->db->query("select max(kd_satuan) as kd_satuan from inv_satuan");
		
		if(count($query->result()) > 0){
			$KdSatuan=$query->row()->kd_satuan;
			
			$newNo=$KdSatuan+1;
			//009
			if(strlen($newNo) == 1){
				$newKdSatuan='00'.$newNo;
			} else if(strlen($newNo) == 2){
				$newKdSatuan='0'.$newNo;
			} else {
				$newKdSatuan=$newNo;
			}
		} else{
			$newKdSatuan='0000000001';
		}
		
		return $newKdSatuan;
	}

	public function save(){
		$this->db->trans_begin();
		
		$KdSatuan = $_POST['KdSatuan'];
		$Satuan = $_POST['Satuan'];
		
		$Satuan=strtoupper($Satuan);
		
		$newKdSatuan=$this->getKdSatuan();
		
		if($KdSatuan == ''){//data baru
			$ubah=0;
			$save=$this->saveSatuan($newKdSatuan,$Satuan,$ubah);
			$kode=$newKdSatuan;
		} else{//data edit
			$ubah=1;
			$save=$this->saveSatuan($KdSatuan,$Satuan,$ubah);
			$kode=$KdSatuan;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, kdsatuan:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
				
	}
	
	public function delete(){
		$KdSatuan = $_POST['KdSatuan'];
		
		$query = $this->db->query("DELETE FROM inv_satuan WHERE kd_satuan='$KdSatuan' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_satuan WHERE kd_satuan='$KdSatuan'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveSatuan($KdSatuan,$Satuan,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_satuan"=>$KdSatuan,
							"satuan"=>$Satuan);
			
			$result=$this->db->insert('inv_satuan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_satuan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("satuan"=>$Satuan);
			
			$criteria = array("kd_satuan"=>$KdSatuan);
			$this->db->where($criteria);
			$result=$this->db->update('inv_satuan',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_satuan',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>