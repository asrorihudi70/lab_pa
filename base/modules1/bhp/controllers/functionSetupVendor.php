<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupVendor extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getVendorGrid(){
		$vendor=$_POST['text'];
		if($vendor == ''){
			$criteria="";
		} else{
			$criteria=" WHERE vendor like upper('".$vendor."%')";
		}
		$result=$this->db->query("select kd_vendor,vendor,contact,alamat,kota,telepon1,telepon2,fax,kd_pos,negara,term 
								  FROM inv_vendor ".$criteria."	
								  order by kd_vendor
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getKdVendor(){
		$query = $this->db->query("select max(kd_vendor) as kd_vendor from inv_vendor")->row();
		$KdVendor=$query->kd_vendor;
		
		if($query){
			$newNo=$KdVendor+1;
			//0000000237
			if(strlen($newNo) == 1){
				$newKdVendor='000000000'.$newNo;
			} else if(strlen($newNo) == 2){
				$newKdVendor='00000000'.$newNo;
			} else if(strlen($newNo) == 3){
				$newKdVendor='0000000'.$newNo;
			} else if(strlen(newNo) == 4){
				$newKdVendor='000000'.$newNo;
			} else if(strlen($newNo) == 5){
				$newKdVendor='00000'.$newNo;
			} else if(strlen($newNo) == 6){
				$newKdVendor='0000'.$newNo;
			} else if(strlen($newNo) == 7){
				$newKdVendor='000'.$newNo;
			} else if(strlen($newNo) == 8){
				$newKdVendor='00'.$newNo;
			} else if(strlen($newNo) == 9){
				$newKdVendor='0'.$newNo;
			} else{
				$newKdVendor=$newNo;
			}
		} else{
			$newKdVendor='0000000001';
		}
		
		return $newKdVendor;
	}

	public function save(){
		$this->db->trans_begin();
		
		$KdVendor = $_POST['KdVendor'];
		$Nama = $_POST['Nama'];
		$Kontak = $_POST['Kontak'];
		$Alamat = $_POST['Alamat'];
		$Kota = $_POST['Kota'];
		$KodePos = $_POST['KodePos'];
		$Negara = $_POST['Negara'];
		$Telepon = $_POST['Telepon'];
		$Telepon2 = $_POST['Telepon2'];
		$Fax = $_POST['Fax'];
		$Tempo = $_POST['Tempo'];
		
		$Nama=strtoupper($Nama);
		
		$newKdVendor=$this->getKdVendor();
		
		if($KdVendor == ''){//data baru
			$ubah=0;
			$save=$this->saveVendor($newKdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Tempo,$ubah);
			$kode=$newKdVendor;
		} else{//data edit
			$ubah=1;
			$save=$this->saveVendor($KdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Tempo,$ubah);
			$kode=$KdVendor;
		}
		
		if($save){
			$this->db->trans_commit();
			echo "{success:true, kdvendor:'$kode'}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdVendor = $_POST['KdVendor'];
		
		$query = $this->db->query("DELETE FROM inv_vendor WHERE kd_vendor='$KdVendor' ");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM inv_vendor WHERE kd_vendor='$KdVendor'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveVendor($KdVendor,$Nama,$Kontak,$Alamat,$Kota,$KodePos,$Negara,$Telepon,$Telepon2,$Fax,$Tempo,$ubah){
		$strError = "";
		
		if($ubah == 0){ //data baru
			$data = array("kd_vendor"=>$KdVendor,
							"vendor"=>$Nama,
							"contact"=>$Kontak,
							"alamat"=>$Alamat,
							"kota"=>$Kota,
							"telepon1"=>$Telepon,
							"telepon2"=>$Telepon2,
							"fax"=>$Fax,
							"kd_pos"=>$KodePos,
							"negara"=>$Negara,
							"beg_bal"=>0,
							"currents"=>0,
							"cr_limit"=>0,
							"finance"=>0,
							"term"=>$Tempo
			);
			
			$dataSql = array("kd_vendor"=>$KdVendor,
								"vendor"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo
			);
			
			$result=$this->db->insert('inv_vendor',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('inv_vendor',$dataSql);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("vendor"=>$Nama,
								"contact"=>$Kontak,
								"alamat"=>$Alamat,
								"kota"=>$Kota,
								"telepon1"=>$Telepon,
								"telepon2"=>$Telepon2,
								"fax"=>$Fax,
								"kd_pos"=>$KodePos,
								"negara"=>$Negara,
								"beg_bal"=>0,
								"currents"=>0,
								"cr_limit"=>0,
								"finance"=>0,
								"term"=>$Tempo
			);
			
			$dataUbahSql = array("vendor"=>$Nama,
									"contact"=>$Kontak,
									"alamat"=>$Alamat,
									"kota"=>$Kota,
									"telepon1"=>$Telepon,
									"telepon2"=>$Telepon2,
									"fax"=>$Fax,
									"kd_pos"=>$KodePos,
									"negara"=>$Negara,
									"beg_bal"=>0,
									"currents"=>0,
									"cr_limit"=>0,
									"finance"=>0,
									"term"=>$Tempo
			);
			
			$criteria = array("kd_vendor"=>$KdVendor);
			$this->db->where($criteria);
			$result=$this->db->update('inv_vendor',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_update('inv_vendor',$dataUbahSql,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>