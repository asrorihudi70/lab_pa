<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionStok extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridBarang(){
		$nama=$_POST['nama'];
		if($nama == ''){
			$criteria="";
		} else{
			$criteria=" and upper(B.nama_brg) like upper('".$nama."%')";
		} 
		$result=$this->db->query("select b.kd_inv, B.no_urut_brg, B.nama_brg, coalesce(S.satuan,'-') as satuan, L.Nama_sub,  b.min_stok , 
									coalesce((SELECT SUM(jumlah_total) From inv_kartu_stok WHERE no_urut_brg = B.no_urut_brg), 0)  AS jumlah 
							from inv_master_brg B 
								 left join inv_satuan S  on B.kd_satuan=S.kd_satuan 
								 inner join inv_kode L  on B.kd_inv=L.kd_inv  
								 where left(b.kd_inv,1) = '8'  and b.no_urut_brg in (select no_urut_brg from inv_kartu_stok)  
								 ".$criteria."
								 order by nama_brg
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridDetailStok(){
		$result=$this->db->query("Select k.*,coalesce(h.jumlah,0)as jumlah, coalesce(k.JUMLAH_TOTAL+h.JUMLAH,k.JUMLAH_TOTAL)as total from inv_kartu_stok k
									left join inv_hist_stok_out h on k.KD_STOK=h.KD_STOK
									where no_urut_brg='".$_POST['no_urut_brg']."'  
									order by k.tgl_posting, k.kd_stok
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getGridDetailStokFilter(){
		$bulan=substr($_POST['bulan'],0,2);
		$tahun=substr($_POST['bulan'],-4);
		
		$result=$this->db->query("Select k.*,coalesce(h.jumlah,0)as jumlah, coalesce(k.JUMLAH_TOTAL+h.JUMLAH,k.JUMLAH_TOTAL)as total from inv_kartu_stok k
									left join inv_hist_stok_out h on k.KD_STOK=h.KD_STOK
									where no_urut_brg='".$_POST['no_urut_brg']."'
									and EXTRACT(MONTH FROM tgl_posting)='".$bulan."' and EXTRACT(year FROM tgl_posting)='".$tahun."'
									order by k.tgl_posting, k.kd_stok
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
}
?>