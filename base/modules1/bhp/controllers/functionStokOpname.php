<?php

/**
 * @author Ali
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionStokOpname extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getGridAwal(){
		if($_POST['kdinv'] == ''){
			$criteriakode="";
		} else{
			$criteriakode=" and m.kd_inv like '".$_POST['kdinv']."%'";
		} 
		
		if($_POST['nama'] == ''){
			$criterianama="";
		} else{
			$criterianama=" and upper(m.nama_brg) like upper('".$_POST['nama']."%')";
		} 
		
		if($_POST['tglawal'] != '' && $_POST['tglakhir'] != ''){
			$criteriatgl=" where a.TGL_ADJUST between '".$_POST['tglawal']."' and '".$_POST['tglakhir']."'";
		} else{
			$criteriatgl=" where a.TGL_ADJUST='".date('Y-m-d')."'";
		}
		$result=$this->db->query("select  a.kd_stok,a.tgl_adjust,a.tgl_posting,a.adjust_qty,k.no_urut_brg,m.nama_brg,k.jumlah_total,
									m.kd_inv,m.min_stok,kd.nama_sub,s.satuan
								from inv_adjustment a
									inner join inv_kartu_stok k on k.kd_stok=a.kd_stok
									inner join inv_master_brg m on m.no_urut_brg=k.no_urut_brg
									inner join inv_kode kd on kd.kd_inv=m.kd_inv
									left join inv_satuan s on m.kd_satuan=s.kd_satuan
									".$criteriatgl."
									".$criterianama."
									".$criteriakode."
								order by m.kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getBarang(){
		$result=$this->db->query("SELECT imb.kd_inv, ik.nama_sub, iks.kd_stok,  iks.no_urut_brg, imb.nama_brg, ist.satuan, 
										iks.tgl_posting, imb.min_stok,  iks.jumlah_total 
									FROM inv_kartu_stok iks 
										INNER JOIN inv_master_brg imb ON iks.no_urut_brg = imb.no_urut_brg 
										INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv 
										LEFT JOIN inv_satuan ist ON imb.kd_satuan = ist.kd_satuan 
									WHERE upper(imb.nama_brg) LIKE upper('".$_POST['text']."%') 
									ORDER BY imb.nama_brg
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getGridDetailBarang(){
		$result=$this->db->query("SELECT imb.kd_inv, iks.kd_stok,  iks.no_urut_brg, imb.nama_brg, ist.satuan, 
										to_char(iks.tgl_posting, 'dd-mm-YYYY') as tgl_posting, imb.min_stok,  iks.jumlah_total, iks.jumlah_total as ADJUST_QTY
									FROM inv_kartu_stok iks 
										INNER JOIN inv_master_brg imb ON iks.no_urut_brg = imb.no_urut_brg 
										LEFT JOIN inv_satuan ist ON imb.kd_satuan = ist.kd_satuan 
									WHERE  imb.nama_brg='".$_POST['nama']."'
									ORDER BY imb.nama_brg
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	

	function getUrut($kd_stok,$TglAdjust,$TglPosting){
		$query = $this->db->query("SELECT urut FROM inv_adjustment  WHERE kd_stok ='".$kd_stok."' 
									AND tgl_adjust = '".$TglAdjust."' AND tgl_posting ='".$TglPosting."'");
		if(count($query->result()) > 0){
			$u=$query->row()->urut;
			$urut=$u+1;
		} else{
			$urut=1;
		}
		
		return $urut;
	}
	
	public function save(){
		$this->db->trans_begin();
		
		$TglPosting = $_POST['TglPosting'];
		$jmllist= $_POST['jumlah'];
		$TglAdjust = date('Y-m-d');
		$kdUser=$this->session->userdata['user_id']['id'] ;
		
		for($i=0;$i<$jmllist;$i++){
			$kd_stok = $_POST['kd_stok-'.$i];
			$adjust_qty = $_POST['adjust_qty-'.$i];
			$jumlah_total = $_POST['jumlah_total-'.$i];
			$urut=$this->getUrut($kd_stok,$TglAdjust,$TglPosting);
			
			if($adjust_qty != $jumlah_total){
				$qty = $adjust_qty - $jumlah_total;
				
				$data = array("kd_stok"=>$kd_stok,
								"tgl_adjust"=>$TglAdjust,
								"urut"=>$urut,
								"tgl_posting"=>$TglPosting,
								"opr"=>$kdUser,
								"adjust_qty"=>$qty
				);
				$result=$this->db->insert('inv_adjustment',$data);
				
				//-----------insert to sq1 server Database---------------//
				_QMS_insert('inv_adjustment',$data);
				//-----------akhir insert ke database sql server----------------//
				if($result){
					$dataUbah = array("jumlah_total"=>$adjust_qty);
					
					$criteria = array("kd_stok"=>$kd_stok);
					$this->db->where($criteria);
					$resultUpdate=$this->db->update('inv_kartu_stok',$dataUbah);
					
					//-----------insert to sq1 server Database---------------//
					_QMS_update('inv_kartu_stok',$dataUbah,$criteria);
					//-----------akhir insert ke database sql server----------------//
					if($resultUpdate){
						$hasil='Ok';
					} else{
						$hasil='Error';
					}
				} else{
					$hasil='Error';
				}
			}
		}
		
		if($hasil != 'Error'){
			$this->db->trans_commit();
			echo "{success:true}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
}
?>