<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_masterbarang extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakDaftarBarang(){		
		$common=$this->common;
   		$result=$this->result;
   		$title='DAFTAR BARANG PERSEDIAAN';
		$param=json_decode($_POST['data']);
		
		$judul=$param->judul;
		
		$queryHead = $this->db->query( "  SELECT  DISTINCT(imb.kd_inv), iv.nama_sub
											FROM inv_master_brg imb  
											INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											WHERE LEFT(iv.kd_inv,1)='8'  
											ORDER BY iv.nama_sub
										");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="5">'.$title.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="50">Reg.</th>
					<th width="100">Nama Barang</th>
					<th width="60">Satuan</th>
					<th width="50">Buffer Stok</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
				$html.='

				<tbody>
						<tr class="headerrow"> 
								<td width=""></td>
								<th width="" colspan="4" align="left">'.$line->nama_sub.' ('.$kd.')</th>
						</tr>

				';
				$queryHasil = $this->db->query( " SELECT  imb.kd_inv, imb.no_urut_brg, imb.nama_brg, isa.satuan,  iv.nama_sub, imb.min_stok 
													FROM inv_master_brg imb  
													INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													WHERE LEFT(iv.kd_inv,1)='8' and  iv.nama_sub='".$line->nama_sub."'
													ORDER BY iv.nama_sub, imb.nama_brg
												");
				$query2 = $queryHasil->result();
			
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='

					<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_urut_brg.'</td>
								<td width="">&nbsp;'.$line2->nama_brg.'</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
								<td width="" align="right">'.$line2->min_stok.'&nbsp;</td>
							</tr>

					';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : 02158574545</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : 02158574545</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : 02158574545</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</table>';
		$prop=array('foot'=>true);
		$name='Lap_PersediaanBarangBHP.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=".$name);
		echo $html;
		//$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
   	}
	
	public function cetakHargaBarang(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='DAFTAR HARGA BARANG PERSEDIAAN';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$bulan=$param->bulan;
		$jenis=$param->jenis;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($jenis == 'tanggal'){
			$criteria="WHERE pg.tgl_terima between '".$tglAwal."'  AND '".$tglAkhir."' ";
			$periode="Tanggal ".$awal." s/d ".$akhir."";
		} else{
			$criteria="WHERE (PG.TGL_TERIMA between (SELECT date_trunc('month', max(TGL_TERIMA)) - INTERVAL '".$bulan." month'
														FROM INV_TRM_G PG
														INNER JOIN INV_TRM_D TGD ON PG.NO_TERIMA=TGD.NO_TERIMA
														WHERE NO_URUT_BRG = IMB.NO_URUT_BRG)  
								AND(SELECT MAX(TGL_TERIMA) 
									FROM INV_TRM_G PG
									INNER JOIN INV_TRM_D TGD ON PG.NO_TERIMA = TGD.NO_TERIMA
									WHERE NO_URUT_BRG = IMB.NO_URUT_BRG))";
						
			$tgl=date('Y-m-d',strtotime(date('Y-m-d') . "-".$bulan." month"));
			$tgl=tanggalstring($tgl);
			$Split = explode("-", $tgl, 3);
			$nowbulan=$Split[1];
			$nowtahun=$Split[2];
			
			$periode="Periode ".$bulan." bulan terakhir sejak ".$nowbulan.", ".$nowtahun."";
		}
		
		
		$queryHead = $this->db->query( "  SELECT  DISTINCT(imb.kd_inv), iv.nama_sub
											FROM inv_kode iv 
												INNER JOIN inv_master_brg imb 
												INNER JOIN  inv_satuan isa ON imb.kd_satuan = isa.kd_satuan ON  iv.kd_inv = imb.kd_inv 
												LEFT OUTER JOIN  inv_trm_g pg 
												INNER JOIN inv_trm_d tgd  ON pg.no_terima = tgd.no_terima ON imb.no_urut_brg = tgd.no_urut_brg  
											".$criteria."
												and iv.kd_inv='".$KdInv."'  
											GROUP BY imb.kd_inv, imb.no_urut_brg, iv.nama_sub, imb.min_stok, imb.nama_brg, isa.satuan  
											ORDER BY iv.nama_sub
										");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$periode.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10" rowspan="2">No</th>
					<th width="50" rowspan="2">Reg.</th>
					<th width="100" rowspan="2">Nama Barang</th>
					<th width="60" rowspan="2">Satuan</th>
					<th width="50" rowspan="2">Buffer Stok</th>
					<th width="150" colspan="3">Harga Beli Barang</th>
			  </tr>
			  <tr>
					<th width="50">Terendah (Rp)</th>
					<th width="50">Tertinggi (Rp)</th>
					<th width="50">Rata-rata (Rp)</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				
				$html.='

				<tbody>
						<tr class="headerrow"> 
								<td width=""></td>
								<th width="" colspan="7" align="left">Kelompok : '.$line->nama_sub.'</th>
						</tr>

				';
				$queryHasil = $this->db->query( " SELECT imb.kd_inv, iv.nama_sub, imb.no_urut_brg, imb.min_stok, imb.nama_brg, isa.satuan,
														EXTRACT(month FROM max(pg.tgl_terima)) || ', ' || EXTRACT(year from max(pg.tgl_terima)) AS tgl,  
														coalesce(MIN(tgd.harga_beli), 0) AS harga_beli_bawah,  coalesce(MAX(tgd.harga_beli), 0) AS harga_beli_atas,  
														coalesce(AVG(tgd.harga_beli), 0) AS harga_beli_avg  
													FROM inv_kode IV 
														INNER JOIN inv_master_brg imb 
														INNER JOIN  inv_satuan isa ON imb.kd_satuan = isa.kd_satuan ON  iv.kd_inv = imb.kd_inv 
														LEFT OUTER JOIN  inv_trm_g pg 
														INNER JOIN inv_trm_d tgd  ON pg.no_terima = tgd.no_terima ON imb.no_urut_brg = tgd.no_urut_brg  
													".$criteria." 
													AND iv.kd_inv='".$KdInv."'  
													GROUP BY imb.kd_inv, imb.no_urut_brg, iv.nama_sub, imb.min_stok, imb.nama_brg, isa.satuan  
													ORDER BY iv.nama_sub, imb.nama_brg
												");
				$query2 = $queryHasil->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='

					<tbody>
							<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="" align="center">'.$line2->no_urut_brg.'</td>
								<td width="">&nbsp;'.$line2->nama_brg.'</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
								<td width="" align="right">'.$line2->min_stok.'&nbsp;</td>
								<td width="" align="right">'.number_format($line2->harga_beli_bawah,0,',','.').'&nbsp;</td>
								<td width="" align="right">'.number_format($line2->harga_beli_atas,0,',','.').'&nbsp;</td>
								<td width="" align="right">'.number_format($line2->harga_beli_avg,0,',','.').'&nbsp;</td>
							</tr>

					';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Daftar Barang BHP',$html);	
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>