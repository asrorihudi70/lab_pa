<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_stokpersediaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakDetail(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='DAFTAR BARANG PERSEDIAAN';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$queryBarang = $this->db->query( " SELECT distinct(ks.no_urut_brg), imb.nama_brg, iv.nama_sub,  
												imb.kd_inv, imb.min_stok
											FROM inv_master_brg imb  
												INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
												INNER JOIN inv_kartu_stok KS ON imb.no_urut_brg = ks.no_urut_brg  
												INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
												LEFT OUTER JOIN inv_hist_stok_out hso ON ks.kd_stok = hso.kd_stok  
											WHERE tgl_posting BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  AND iv.kd_inv='".$KdInv."'  
											ORDER by imb.nama_brg 
										");
		
		$query = $queryBarang->result();
		$html='';
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" rowspan="2">Tanggal</th>
					<th width="60" rowspan="2">No Stok</th>
					<th width="60" colspan="3">Jumlah Stok</th>
			  </tr>
			   <tr>
					<th width="60">In</th>
					<th width="60">Out</th>
					<th width="60">Saldo</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			$grandtotal=0;
			foreach ($query as $line) 
			{
				$queryBody = $this->db->query( " SELECT ks.tgl_posting, ks.no_urut_brg, ks.kd_stok, imb.nama_brg, iv.nama_sub,  
													imb.kd_inv, isa.satuan, imb.min_stok, ks.jumlah_total as jml_aktual,  
													coalesce(hso.jumlah,0) AS jml_out 
												FROM inv_master_brg imb  
													INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
													INNER JOIN inv_kartu_stok ks ON imb.no_urut_brg = ks.no_urut_brg  
													INNER JOIN inv_kode iv ON imb.kd_inv = iv.kd_inv  
													LEFT OUTER JOIN inv_hist_stok_out hso ON ks.kd_stok = hso.kd_stok  
												WHERE tgl_posting BETWEEN '".$tglAwal."' AND '".$tglAkhir."'  
													AND iv.kd_inv='".$KdInv."'
													AND KS.NO_URUT_BRG='".$line->no_urut_brg."'
												ORDER BY imb.nama_brg, ks.kd_stok, ks.tgl_posting  
										");
				$query2 = $queryBody->result();
				
				$subin=0;
				$subout=0;
				$subsaldo=0;
				foreach ($query2 as $line2) 
				{
					$no++;
					$jmlin=$line2->jml_aktual+$line2->jml_out;
					$html.='
					<tbody>
						<tr class="headerrow"> 
							<td width="" align="center">'.$no.'</td>
							<td width="">&nbsp;'.date('d-M-Y',strtotime($line2->tgl_posting)).'</td>
							<td width="">&nbsp;'.$line2->kd_stok.'</td>
							<td width="" align="right">'.$jmlin.'&nbsp;</td>
							<td width="" align="right">'.$line2->jml_out.' &nbsp;</td>
							<td width="" align="right">'.$line2->jml_aktual.'&nbsp;</td>
						</tr> 
						';
					
					$subin += $jmlin;
					$subout += $line2->jml_out;
					$subsaldo += $line2->jml_aktual;
				}
				$html.='<tr> 
							<th width="" rowspan="2"></th>
							<th width="" colspan="5" align="left">&nbsp;Barang : '.$line->nama_brg.'</th>
						</tr> 
						<tr> 
							<th width="" colspan="5" align="left">&nbsp;Stok Min : '.$line->min_stok.'</th>
						</tr> 
						';
			}
			$html.='
						<tr> 
							<th width=""></th>
							<th width="" colspan="2" align="right">Sub Total</th>
							<th width="" align="right">'.$subin.'&nbsp;</th>
							<th width="" align="right">'.$subout.'&nbsp;</th>
							<th width="" align="right">'.$subsaldo.'&nbsp;</th>
						</tr> 
						';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		
		//--------------------------------------TANDA TANGAN--------------------------------------------------------------------	
		$queryRS = $this->db->query("select * from db_rs")->result();
		foreach ($queryRS as $line) {
			$kota=$line->city;
			$namars=$line->name;
		}
		$t=tanggalstring(date('Y-m-d'));
		$nip='02158574545';
		
		$html.='<br><br><br>
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="150" align="right">'.$kota.', '.$t.'</th>
					</tr>
				</tbody>
			</table><br>';
		
		$html.='<table width="200" border="0">
				<tbody>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Medis</th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">Pengurus Barang Non Medis</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150" align="center">.........................<hr></th>
						<th width="150"></th>
						<th width="150" align="center">.........................<hr></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow"> 
						<th width="15"></th>
						<th width="150">&nbsp;</th>
						<th width="300" align="center">Direktur '.$namars.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr class="headerrow">
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15" align="center"></th>
						<th width="150">&nbsp;</th>
						<th width="150" align="center">.........................<hr width="50%"></th>
						<th width="150" align="center"></th>
						<th width="15">&nbsp;</th>
					</tr>
					<tr>
						<th width="15">&nbsp;</th>
						<th width="150">&nbsp;</th>
						<th width="150">NIP : '.$nip.'</th>
						<th width="150">&nbsp;</th>
						<th width="15">&nbsp;</th>
					</tr>

				<p>&nbsp;</p>
			';
		$html.='</tbody></table>';
		$html.='</table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Stok Detail BHP',$html);	
   	}
	
	public function cetakSummary(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='SUMMARY STOK BARANG PERSEDIAAN';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$jenis=$param->jenis;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($jenis == 'Detail'){
			$criteria="WHERE pg.tgl_terima between '".$tglAwal."'  AND '".$tglAkhir."' ";
		} else{
			$criteria="WHERE tgl_posting BETWEEN '".$tglAwal."'  AND '".$tglAkhir."'  ";
		}
		
		
		$queryHasil = $this->db->query( " SELECT imb.nama_brg, ks.no_urut_brg,  iv.nama_sub, imb.kd_inv,  isa.satuan , imb.min_stok,  
											SUM(ks.jumlah_total) AS jml_aktual, SUM(COALESCE(hso.jumlah, 0)) AS jml_out,  
											SUM(COALESCE(hso.jumlah, 0)+ks.jumlah_total) AS jml_in  
										FROM INV_MASTER_BRG IMB  
											INNER JOIN inv_satuan isa ON IMB.KD_SATUAN = ISA.KD_SATUAN  
											INNER JOIN inv_kartu_stok ks on imb.no_urut_brg = ks.no_urut_brg 
											INNER JOIN Inv_kode iv ON imb.kd_inv = iv.kd_inv 
											LEFT OUTER JOIN  inv_hist_stok_out hso ON ks.kd_stok = hso.kd_stok  
										".$criteria."
										AND iv.kd_inv='".$KdInv."' 
										GROUP BY imb.nama_brg, ks.no_urut_brg,  iv.nama_sub, imb.kd_inv,  isa.satuan, imb.min_stok  
										ORDER BY imb.nama_brg 
										");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		if(count($query) > 0){
			foreach ($query as $line) 
			{
				$namasub=$line->nama_sub;
			
				//8.01.03.08.014
				$kd_inv=$line->kd_inv;
				$a=substr($kd_inv,0,1);//8
				$b=substr($kd_inv,1,2);//01
				$c=substr($kd_inv,3,2);//03
				$d=substr($kd_inv,5,2);//08
				$e=substr($kd_inv,-3);//014
				
				$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
				
			}
			
		} else{
			$namasub='-';
			$kd='-';
		}
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10" rowspan="2">No</th>
					<th width="50" rowspan="2">Reg.</th>
					<th width="100" rowspan="2">Nama Barang</th>
					<th width="60" rowspan="2">Satuan</th>
					<th width="50" rowspan="2">Minimal Stok</th>
					<th width="150" colspan="3">Jumlah Stok</th>
			  </tr>
			  <tr>
					<th width="50">In</th>
					<th width="50">Out</th>
					<th width="50">Stok Real</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>
					<tr class="headerrow"> 
						<td width="" align="center">'.$no.'</td>
						<td width="" align="center">'.$line->no_urut_brg.'</td>
						<td width="">&nbsp;'.$line->nama_brg.'</td>
						<td width="">&nbsp;'.$line->satuan.'</td>
						<td width="" align="right">'.$line->min_stok.'&nbsp;</td>
						<td width="" align="right">'.$line->jml_in.'&nbsp;</td>
						<td width="" align="right">'.$line->jml_out.'&nbsp;</td>
						<td width="" align="right">'.$line->jml_aktual.'&nbsp;</td>
					</tr>

				';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Stok Persediaan (Summary) BHP',$html);	
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>