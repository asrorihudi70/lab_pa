<?php

class tb_lokasi extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_lokasi';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewlokasi;
		$row->kd_lokasi=$rec->kd_lokasi;
		$row->lokasi=$rec->lokasi;
		$row->jen_lokasi=$rec->jen_lokasi;
        $row->kd_jns_lokasi=$rec->kd_jns_lokasi;
        
        return $row;
    }

}

class Rowviewlokasi {
   public $kd_jns_lokasi;
   public $jns_lokasi;
}

?>
