<?php
class tb_proses_invkode extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "Select kd_inv,inv_kd_inv,nama_sub from INV_KODE";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_kode();
        $row->kd_inv=$rec->kd_inv;
        $row->inv_kd_inv=$rec->inv_kd_inv;
        $row->nama_sub=$rec->nama_sub;

        return $row;
    }

}

class Rowam_inv_kode
{
    public $kd_inv;
    public $inv_kd_inv;
    public $nama_sub;

}

class clsTreeRow
{

    public $id;
    public $text;
    public $leaf;
    public $expanded;
    public $parents;
    public $parents_name;
    public $type;
    public $children = array();

}


?>
