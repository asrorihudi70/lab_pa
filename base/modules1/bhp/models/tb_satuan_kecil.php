<?php

class tb_satuan_kecil extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_satuan_kecil';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewsatuankecil;
        $row->kd_satuan_kecil=$rec->kd_satuan_kecil;
		$row->satuan_kecil=$rec->satuan_kecil;
        
        return $row;
    }

}

class Rowviewsatuankecil {
   public $kd_satuan_kecil;
   public $satuan_kecil;
}

?>
