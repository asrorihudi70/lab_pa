<?php

class tb_vendor extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_vendor';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewsatuan;
        $row->kd_vendor=$rec->kd_vendor;
		$row->vendor=$rec->vendor;
        
        return $row;
    }

}

class Rowviewsatuan {
   public $kd_vendor;
   public $vendor;
}

?>
