<?php
/**
 * @author OLIB
 * @copyright 2008
 */

class tblviewdashboardcost extends TblBase
{
	
	function __construct()
	{
		$this->TblName='vischedulepm';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->SqlQuery=" select  
		case when ".$SQL->fnMonth('finish_date')."=1 then 'Januari'
		when ".$SQL->fnMonth('finish_date')."=1 then 'Januari'
		when ".$SQL->fnMonth('finish_date')."=2 then 'Pebruari'
		when ".$SQL->fnMonth('finish_date')."=3 then 'Maret'
		when ".$SQL->fnMonth('finish_date')."=4 then 'April'
		when ".$SQL->fnMonth('finish_date')."=5 then 'Mei'
		when ".$SQL->fnMonth('finish_date')."=6 then 'Juni'
		when ".$SQL->fnMonth('finish_date')."=7 then 'Juli'
		when ".$SQL->fnMonth('finish_date')."=8 then 'Agustus'
		when ".$SQL->fnMonth('finish_date')."=9 then 'September'
		when ".$SQL->fnMonth('finish_date')."=10 then 'Oktober'
		when ".$SQL->fnMonth('finish_date')."=11 then 'November'
		else 'Desember' end
		as bulan 
		,sum(last_cost) as jml,sum(cost) as jmlawal
         from ( 
         select     rc.finish_date, rc.last_cost, ac.cost
         from    am_result_cm as rc inner join
         am_work_order_cm as woc on rc.wo_cm_id = woc.wo_cm_id inner join
         am_schedule_cm as sc on woc.sch_cm_id = sc.sch_cm_id inner join
         am_approve_cm as ac on sc.app_id = ac.app_id
         union all 
         select  finish_date, last_cost ,last_cost as cost
         from am_result_pm  ) x   #where#
         group by ".$SQL->fnMonth('finish_date');
		
		
	
	}
	
	function FillRow($rec)
	{
		$row=new Rowviewdascost;
		$row->BULAN=$rec->bulan;
		$row->JML=$rec->jml;
		$row->JMLAWAL=$rec->jmlawal;
	
		return $row;
	}
}

class Rowviewdascost
{

PUBLIC $BULAN;
PUBLIC $JML;
PUBLIC $JMLAWAL;

}