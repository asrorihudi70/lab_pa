<?php
/**
 * @author OLIB
 * @copyright 2008
 */

class tblviewdashboardwo extends TblBase
{
	
	function __construct()
	{
		$this->TblName='vischedulepm';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->SqlQuery=   "  select category_id,category_name,count(*) as jml
        from (
        select  a.category_id, c.category_name, woc.wo_cm_id, woc.wo_cm_date as due_date
        from  am_work_order_cm as woc inner join
        am_schedule_cm as sc on woc.sch_cm_id = sc.sch_cm_id inner join
        am_approve_cm as ac on sc.app_id = ac.app_id inner join
        am_request_cm_detail as rcd on ac.req_id = rcd.req_id and ac.row_req = rcd.row_req inner join
        am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
        am_category as c on a.category_id = c.category_id inner join
        am_sch_cm_service as scs on woc.sch_cm_id = scs.sch_cm_id
        union all
        select  am.category_id,c.category_name, wop.wo_pm_id, wop.wo_pm_date as due_date 
        from am_wo_pm_service as wops inner join 
        am_work_order_pm as wop on wop.wo_pm_id=wops.wo_pm_id inner join
        am_asset_maint as am inner join am_schedule_pm 
        as sp on am.asset_maint_id = sp.asset_maint_id inner join am_category as c 
        on am.category_id = c.category_id inner join am_sch_pm_detail as spd on 
        sp.sch_pm_id = spd.sch_pm_id on wops.service_id = spd.service_id and  
        wops.category_id = spd.category_id and wops.sch_pm_id = spd.sch_pm_id and 
        wops.row_sch = spd.row_sch 
       
        )x  #where#
        group by category_id,category_name";
	}
	
function FillRow($rec)
	{
		$row=new Rowviewdaswo;
		$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->JML=$rec->jml;
		return $row;
	}
}

class Rowviewdaswo
{

public $CATEGORY_ID;
public $CATEGORY_NAME;
public $JML;


}