﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewtracking extends TblBase
{
	
	function __construct()
	{
		$this->TblName='viewtracking';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->StrSql="status_id,status_name,location_id,asset_main,location,req_date,req_id,problem,emp_id,emp_name,dept_id,dept_name,impact,req_finish_date,asset_maint_id,row_req,desc_req,desc_status,category_id,category_name"  
;
 $this->SqlQuery=" select      am_request_cm_detail.status_id,  am_status.status_name,  am_asset_maint.location_id, 
                       am_asset_maint.asset_maint_name ".$SQL->OPJoinStr()." ' - ' ".$SQL->OPJoinStr()." ".$SQL->fnSpace(50)." ".$SQL->OPJoinStr()."  am_request_cm_detail.asset_maint_id as asset_main, 
                       am_location.location,  am_request_cm.req_date,  am_request_cm.req_id,  am_request_cm_detail.problem, 
                       am_request_cm.emp_id,  am_employees.emp_name,  am_request_cm.dept_id,  am_department.dept_name, 
                       am_request_cm_detail.impact,  am_request_cm_detail.req_finish_date,  am_request_cm_detail.asset_maint_id, 
                       am_request_cm_detail.row_req,  am_request_cm_detail.desc_req,  am_request_cm_detail.desc_status, 
                       am_asset_maint.category_id,  am_category.category_name
from          am_request_cm inner join
                       am_request_cm_detail on  am_request_cm.req_id =  am_request_cm_detail.req_id inner join
                       am_status on  am_request_cm_detail.status_id =  am_status.status_id inner join
                       am_asset_maint on  am_request_cm_detail.asset_maint_id =  am_asset_maint.asset_maint_id inner join
                       am_location on  am_asset_maint.location_id =  am_location.location_id inner join
                       am_employees on  am_request_cm.emp_id =  am_employees.emp_id inner join
                       am_department on  am_request_cm.dept_id =  am_department.dept_id inner join
                       am_category on  am_asset_maint.category_id =  am_category.category_id";

		
	}


	function FillRow($rec)
	{
		$row=new Rowviewtracking;
				$row->STATUS_ID=$rec->status_id;
		$row->STATUS_NAME=$rec->status_name;
		$row->LOCATION_ID=$rec->location_id;
		$row->ASSET_MAIN=$rec->asset_main;
		$row->LOCATION=$rec->location;
		if ($rec->req_date!==null) 
		{
		$dt=new DateTime($rec->req_date);				
		$row->REQ_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->REQ_DATE='00010101'; } 
		$row->REQ_ID=$rec->req_id;
		$row->PROBLEM=$rec->problem;
		$row->EMP_ID=$rec->emp_id;
		$row->EMP_NAME=$rec->emp_name;
		$row->DEPT_ID=$rec->dept_id;
		$row->DEPT_NAME=$rec->dept_name;
		$row->IMPACT=$rec->impact;
		if ($rec->req_finish_date!==null) 
		{ 
		$dt=new DateTime($rec->req_finish_date);			
		$row->REQ_FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->REQ_FINISH_DATE='00010101'; } 
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->ROW_REQ=$rec->row_req;
		$row->DESC_REQ=$rec->desc_req;
		$row->DESC_STATUS=$rec->desc_status;
		$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;

		return $row;
	}
}
class Rowviewtracking
{
	public $STATUS_ID;
public $STATUS_NAME;
public $LOCATION_ID;
public $ASSET_MAIN;
public $LOCATION;
public $REQ_DATE;
public $REQ_ID;
public $PROBLEM;
public $EMP_ID;
public $EMP_NAME;
public $DEPT_ID;
public $DEPT_NAME;
public $IMPACT;
public $REQ_FINISH_DATE;
public $ASSET_MAINT_ID;
public $ROW_REQ;
public $DESC_REQ;
public $DESC_STATUS;
public $CATEGORY_ID;
public $CATEGORY_NAME;

}

?>