<?php
class tblviewtrackingappinfo extends TblBase
{
    function __construct()
    {
        $this->TblName='viewtrackingappinfo';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from (

            SELECT am_approve_cm.app_id, am_approve_cm.emp_id, am_approve_cm.req_id, am_approve_cm.row_req,
                am_approve_cm.app_due_date, am_approve_cm.app_finish_date, am_approve_cm.desc_app,
                am_approve_cm.is_ext_repair, am_approve_cm.cost, am_employees.emp_name,
                am_request_cm_detail.desc_status, am_request_cm_detail.status_id, am_status.status_name
            FROM am_approve_cm INNER JOIN
                am_request_cm_detail ON am_approve_cm.req_id = am_request_cm_detail.req_id AND
                am_approve_cm.row_req = am_request_cm_detail.row_req
                INNER JOIN am_employees ON am_approve_cm.emp_id = am_employees.emp_id 
                INNER JOIN am_status ON am_request_cm_detail.status_id = am_status.status_id
            ) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewtrackingappinfo;

        $row->APP_ID = $rec->app_id;
        $row->EMP_ID = $rec->emp_id;
        $row->ROW_REQ = $rec->row_req;
        $row->APP_DUE_DATE = $rec->app_due_date;
        $row->APP_FINISH_DATE = $rec->app_finish_date;
        $row->DESC_APP = $rec->desc_app;
        $row->IS_EXT_REPAIR = $rec->is_ext_repair;
        $row->COST = $rec->cost;
        $row->EMP_NAME = $rec->emp_name;
        $row->DESC_STATUS = $rec->desc_status;
        $row->STATUS_ID = $rec->status_id;
        $row->STATUS_NAME = $rec->status_name;                

        return $row;
    }

}

class Rowtblviewtrackingappinfo
{

    public $APP_ID;
    public $EMP_ID;
    public $REQ_ID;
    public $ROW_REQ;
    public $APP_DUE_DATE;
    public $APP_FINISH_DATE;
    public $DESC_APP;
    public $IS_EXT_REPAIR;
    public $COST;
    public $EMP_NAME;
    public $DESC_STATUS;
    public $STATUS_ID;
    public $STATUS_NAME;

}

?>
