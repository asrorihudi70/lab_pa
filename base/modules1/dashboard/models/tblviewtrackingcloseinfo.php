<?php
class tblviewtrackingcloseinfo extends TblBase
{
    function __construct()
    {
        $this->TblName='viewtrackingcloseinfo';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from (

            SELECT am_result_cm.result_cm_id, am_result_cm.wo_cm_id, am_result_cm.finish_date,
                am_result_cm.diff_finish_date, am_result_cm.last_cost, am_result_cm.diff_cost,
                am_result_cm.desc_result_cm, am_result_cm.reference, am_request_cm_detail.req_id,
                am_request_cm_detail.row_req
            FROM am_result_cm INNER JOIN
                am_work_order_cm ON am_result_cm.wo_cm_id = am_work_order_cm.wo_cm_id INNER JOIN
                am_schedule_cm ON am_work_order_cm.sch_cm_id = am_schedule_cm.sch_cm_id INNER JOIN
                am_approve_cm ON am_schedule_cm.app_id = am_approve_cm.app_id INNER JOIN
                am_request_cm_detail ON am_approve_cm.req_id = am_request_cm_detail.req_id AND
                am_approve_cm.row_req = am_request_cm_detail.row_req

            ) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewtrackingcloseinfo;

        $row->RESULT_CM_ID = $rec->result_cm_id;
        $row->WO_CM_ID = $rec->wo_cm_id;
        $row->FINISH_DATE = $rec->finish_date;
        $row->DIFF_FINISH_DATE = $rec->diff_finish_date;
        $row->LAST_COST = $rec->last_cost;
        $row->DIFF_COST = $rec->diff_cost;
        $row->DESC_RESULT_CM = $rec->desc_result_cm;
        $row->REFERENCE = $rec->reference;
        $row->REQ_ID = $rec->req_id;
        $row->ROW_REQ = $rec->row_req;
        
        return $row;
    }

}

class Rowtblviewtrackingcloseinfo
{

    public $RESULT_CM_ID;
    public $WO_CM_ID;
    public $FINISH_DATE;
    public $DIFF_FINISH_DATE;
    public $LAST_COST;
    public $DIFF_COST;
    public $DESC_RESULT_CM;
    public $REFERENCE;
    public $REQ_ID;
    public $ROW_REQ;

}

?>
