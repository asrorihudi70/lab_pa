<?php
class tblviewtrackingwoinfo extends TblBase
{
    function __construct()
    {
        $this->TblName='viewtrackingwoinfo';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from (

            SELECT am_work_order_cm.wo_cm_id, am_work_order_cm.emp_id, am_work_order_cm.sch_cm_id,
                am_work_order_cm.status_id, am_work_order_cm.wo_cm_date, am_work_order_cm.wo_cm_finish_date,
                am_work_order_cm.desc_wo_cm, am_employees.emp_name, am_request_cm_detail.req_id,
                am_request_cm_detail.row_req, am_status.status_name
            FROM am_work_order_cm INNER JOIN
                am_employees ON am_work_order_cm.emp_id = am_employees.emp_id INNER JOIN
                am_status ON am_work_order_cm.status_id = am_status.status_id INNER JOIN
                am_schedule_cm ON am_work_order_cm.sch_cm_id = am_schedule_cm.sch_cm_id INNER JOIN
                am_approve_cm ON am_schedule_cm.app_id = am_approve_cm.app_id INNER JOIN
                am_request_cm_detail ON am_approve_cm.req_id = am_request_cm_detail.req_id AND
                am_approve_cm.row_req = am_request_cm_detail.row_req

            ) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewtrackingwoinfo;

        $row->WO_CM_ID = $rec->wo_cm_id;
        $row->EMP_ID = $rec->emp_id;
        $row->SCH_CM_ID = $rec->sch_cm_id;
        $row->STATUS_ID = $rec->status_id;
        $row->WO_CM_DATE = $rec->wo_cm_date;
        $row->WO_CM_FINISH_DATE = $rec->wo_cm_finish_date;
        $row->DESC_WO_CM = $rec->desc_wo_cm;
        $row->EMP_NAME = $rec->emp_name;
        $row->REQ_ID = $rec->req_id;
        $row->ROW_REQ = $rec->row_req;
        $row->STATUS_NAME = $rec->status_name;
        
        return $row;
    }

}

class Rowtblviewtrackingwoinfo
{

    public $WO_CM_ID;
    public $EMP_ID;
    public $SCH_CM_ID;
    public $STATUS_ID;
    public $WO_CM_DATE;
    public $WO_CM_FINISH_DATE;
    public $DESC_WO_CM;
    public $EMP_NAME;
    public $REQ_ID;
    public $ROW_REQ;
    public $STATUS_NAME;

}

?>
