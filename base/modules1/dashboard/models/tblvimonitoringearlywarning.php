﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvimonitoringearlywarning extends TblBase
{
	
	function __construct()
	{
		$this->TblName='vimonitoringearlywarning';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->SqlQuery=" select category_id,category_name,sum(before) as before,sum(target) as target ,sum(after) as after
		from (
		select category_id,category_name,
		sum(case when status=1 then 1 else 0 end) as before, 
		sum(case when status=2 then 1 else 0 end) as target, 
		sum(case when status=3 then 1 else 0 end) as after 
		from ( select  ss.category_id, c.category_name, s.sch_due_date as due_date, ss.sch_cm_id, ss.service_id,
		case when ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())."  <= sc.act_day_before  
		and ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())."  >= (sc.act_day_after * - 1)  then 2 
		when ".$SQL->fnDateDiff('s.sch_due_date',$SQL->fnCurrDate())."  > sc.act_day_after then 3 else 1 end as status 
		from    am_schedule_cm as s inner join am_sch_cm_service as ss on s.sch_cm_id = ss.sch_cm_id 
		inner join am_category as c on ss.category_id = c.category_id 
		inner join am_service_category as sc on ss.service_id = sc.service_id and ss.category_id = sc.category_id 
		where  s.status_id = '2')x 
				#where#
		group by category_id,category_name 
		union all
		select category_id,category_name,
		sum(case when status=1 then 1 else 0 end) as before, 
		sum(case when status=2 then 1 else 0 end) as target, 
		sum(case when status=3 then 1 else 0 end) as after 
		from ( select  c.category_id, c.category_name, spd.due_date, spd.service_id, spd.sch_pm_id, spd.row_sch ,
		case when ".$SQL->fnDateDiff('due_date',$SQL->fnCurrDate())."  <= sc.act_day_before  
		and ".$SQL->fnDateDiff('due_date',$SQL->fnCurrDate())." >= (sc.act_day_after * - 1)  then 2 
		when ".$SQL->fnDateDiff('due_date',$SQL->fnCurrDate())." > sc.act_day_after then 3 else 1 end as status 
		from am_schedule_pm as sp inner join am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id 
		inner join am_category as c on spd.category_id = c.category_id 
		inner join am_service_category as sc on spd.service_id = sc.service_id and spd.category_id = sc.category_id 
		where  spd.status_id = '2')x 
				#where#
		group by category_id,category_name )z
		group by category_id,category_name";

	
	}

	
	function FillRow($rec)
	{
		$row=new Rowvimonitoringearlywarning;
				$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->BEFORE=$rec->before;
		$row->TARGET=$rec->target;
		$row->AFTER=$rec->after;

		return $row;
	}
}
class Rowvimonitoringearlywarning
{
	public $CATEGORY_ID;
public $CATEGORY_NAME;
public $BEFORE;
public $TARGET;
public $AFTER;

}

?>