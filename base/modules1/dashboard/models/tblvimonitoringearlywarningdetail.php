﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvimonitoringearlywarningdetail extends TblHelper
{
	
	function __construct()
	{
            $this->StrSql="status,path,id,category_id,category_name,asset_maint_id,asset_maint_name,status_id,due_date,service_pm_name,location,dept_name,location_id,dept_id,service_id" ;

            $this->SqlQuery=" select  
                case when datediff('day', x.due_date, CURRENT_DATE) <= sc.act_day_before and 
                    datediff('day', x.due_date, CURRENT_DATE) >= (sc.act_day_after * - 1)then 2
                    when datediff('day', x.due_date, CURRENT_DATE) > sc.act_day_after then 3 else 1 end as status, 
                case when datediff('day', x.due_date, CURRENT_DATE) <= sc.act_day_before and 
                    datediff('day', x.due_date, CURRENT_DATE) >= (sc.act_day_after * - 1) then sc.legend_target
                    when datediff('day', x.due_date, CURRENT_DATE) > sc.act_day_after
                        then sc.legend_after else sc.legend_before end as path, x.id, x.category_id,
                x.category_name, x.asset_maint_id, x.asset_maint_id || ' - ' || x.asset_maint_name as asset_maint_name,
                x.status_id, x.due_date, x.service_pm_name, x.location, x.dept_name, x.location_id, x.dept_id,
                x.service_id from
                (select  'pm' as id, c.category_id, c.category_name, sp.asset_maint_id, a.asset_maint_name,
                    spd.status_id, spd.due_date, s.service_name as service_pm_name, l.location, d.dept_name,
                    l.location_id, d.dept_id, s.service_id from am_schedule_pm as sp
                    inner join am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id
                    inner join am_asset_maint as a on sp.asset_maint_id = a.asset_maint_id 
                    inner join am_category as c on a.category_id = c.category_id 
                    inner join am_location as l on a.location_id = l.location_id
                    inner join am_department as d on a.dept_id = d.dept_id 
                    inner join am_service as s on spd.service_id = s.service_id
		union all
		select 'cm' as id, c.category_id, c.category_name, rcd.asset_maint_id, am.asset_maint_name, 
                    sc.status_id, sc.sch_due_date, s.service_name, l.location, d.dept_name, l.location_id,
                    d.dept_id, s.service_id from  am_schedule_cm as sc
                    inner join am_approve_cm as a on sc.app_id = a.app_id
                    inner join am_request_cm_detail as rcd on a.req_id = rcd.req_id and a.row_req = rcd.row_req
                    inner join am_asset_maint as am on rcd.asset_maint_id = am.asset_maint_id
                    inner join am_category as c on am.category_id = c.category_id
                    inner join am_location as l on am.location_id = l.location_id
                    inner join am_department as d on am.dept_id = d.dept_id
                    inner join am_sch_cm_service as b on sc.sch_cm_id = b.sch_cm_id
                    inner join am_service as s on b.service_id = s.service_id) as x
                 inner join am_service_category as sc on x.service_id = sc.service_id and x.category_id = sc.category_id ";

		$this->TblName='vimonitoringearlywarningdetail';
	}

	function SaveData( $DBConn, array $values)
	{
		
		return TblHelper::SaveData($DBConn,$values);
		
	}
	
	function UpdateData( $DBConn, array $values, $criteria='')
	{
		
		return TblHelper::UpdateData($DBConn,$values,$criteria);
		
	}
	function FillRow($rec)
	{
		$row=new Rowvimonitoringearlywarningdetail;
				$row->STATUS=$rec->status;
		$row->PATH=$rec->path;
		$row->ID=$rec->id;
		$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->ASSET_MAINT_NAME=$rec->asset_maint_name;
		$row->STATUS_ID=$rec->status_id;
		if ($rec->due_date!==null) 
		{
		$dt=new DateTime($rec->due_date);				
		$row->DUE_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->DUE_DATE='00010101'; } 
		$row->SERVICE_PM_NAME=$rec->service_pm_name;
		$row->LOCATION=$rec->location;
		$row->DEPT_NAME=$rec->dept_name;
		$row->LOCATION_ID=$rec->location_id;
		$row->DEPT_ID=$rec->dept_id;
		$row->SERVICE_ID=$rec->service_id;

		return $row;
	}
}
class Rowvimonitoringearlywarningdetail
{
	public $STATUS;
public $PATH;
public $ID;
public $CATEGORY_ID;
public $CATEGORY_NAME;
public $ASSET_MAINT_ID;
public $ASSET_MAINT_NAME;
public $STATUS_ID;
public $DUE_DATE;
public $SERVICE_PM_NAME;
public $LOCATION;
public $DEPT_NAME;
public $LOCATION_ID;
public $DEPT_ID;
public $SERVICE_ID;

}

?>