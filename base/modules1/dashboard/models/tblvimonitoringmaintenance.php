﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvimonitoringmaintenance extends TblBase
{
	
	function __construct()
	{
		$this->TblName='vimonitoringmaintenance';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->SqlQuery="
select category_id,category_name ,sum(request) as request,sum(schedule) as schedule,sum(wo) as wo,sum(result) as result
from ( 
select category_id,category_name,count(*) as request,0 as schedule,0 as wo,0 as result 
from ( 
select  c.category_id, c.category_name,  rc.req_date as due_date, rcd.req_id, rcd.row_req 
from  am_request_cm as rc inner join 
 am_request_cm_detail as rcd on rc.req_id = rcd.req_id inner join 
 am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join 
 am_category as c on a.category_id = c.category_id)x  
        #where#
group by category_id,category_name 
union all 
select category_id,category_name,0 as request,count(*) as schedule,0 as wo,0 as result 
from ( 
select  ss.category_id, c.category_name,  s.sch_due_date  as due_date, ss.sch_cm_id, ss.service_id 
from     am_schedule_cm as s inner join 
 am_sch_cm_service as ss on s.sch_cm_id = ss.sch_cm_id inner join 
 am_category as c on ss.category_id = c.category_id where  s.status_id = '1')x  
        #where#
group by category_id,category_name 
union all  
select category_id,category_name,0 as request,count(*) as schedule,0 as wo,0 as result 
from ( 
select  c.category_id, c.category_name, spd.due_date, spd.service_id, spd.sch_pm_id, spd.row_sch 
from  am_schedule_pm as sp inner join 
 am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id inner join 
 am_category as c on spd.category_id = c.category_id where  spd.status_id = '1')x  
        #where#
group by category_id,category_name 
union all 
select category_id,category_name,0 as request,0 as schedule,count(*) as wo,0 as result 
from ( 
select  c.category_id, c.category_name,  w.wo_cm_date  as due_date, w.wo_cm_id 
from  am_work_order_cm as w inner join 
 am_schedule_cm as s on w.sch_cm_id = s.sch_cm_id inner join 
 am_sch_cm_service as sc on s.sch_cm_id = sc.sch_cm_id inner join 
 am_category as c on sc.category_id = c.category_id 
group by c.category_id, c.category_name, w.wo_cm_date, w.wo_cm_id)x  
        #where#
group by category_id,category_name 
union all  
select category_id,category_name,0 as request,0 as schedule,count(*) as wo,0 as result 
from ( 
select  c.category_id, c.category_name,  w.wo_pm_date as due_date, w.wo_pm_id,s.service_id 
from  am_work_order_pm as w inner join 
 am_wo_pm_service as s on w.wo_pm_id = s.wo_pm_id inner join 
 am_category as c on s.category_id = c.category_id 
group by c.category_id, c.category_name, w.wo_pm_date, w.wo_pm_id,s.service_id)x  
        #where#


group by category_id,category_name 
union all 
select category_id,category_name,0 as request,0 as schedule,0 as wo,count(*) as result
from ( 
select  c.category_id, c.category_name,  r.finish_date as due_date, rs.result_cm_id, rs.service_id 
from     am_result_cm as r inner join 
 am_result_cm_service as rs on r.result_cm_id = rs.result_cm_id inner join 
 am_category as c on rs.category_id = c.category_id)x  
        #where#
group by category_id,category_name 
union all 
select category_id,category_name,0 as request,0 as schedule,0 as wo,count(*) as result
from ( 
select  c.category_id, c.category_name,  r.finish_date as due_date, rs.service_id, rs.result_pm_id 
from     am_result_pm as r inner join 
 am_result_pm_service as rs on r.result_pm_id = rs.result_pm_id inner join 
 am_category as c on rs.category_id = c.category_id)x  
        #where#
group by category_id,category_name 
)z group by category_id,category_name ";

		$this->TblName='vimonitoringmaintenance';
	}

	
	function FillRow($rec)
	{
		$row=new Rowvimonitoringmaintenance;
				$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->REQUEST=$rec->request;
		$row->SCHEDULE=$rec->schedule;
		$row->WO=$rec->wo;
		$row->RESULT=$rec->result;

		return $row;
	}
}
class Rowvimonitoringmaintenance
{
	public $CATEGORY_ID;
public $CATEGORY_NAME;
public $REQUEST;
public $SCHEDULE;
public $WO;
public $RESULT;

}

?>