<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class cetaksepugd extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
             $this->load->library('common');
    }
	 
	public function index()
	{

		$this->load->view('main/index');

	} 
	
	public function cetaksep(){
		$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', 'A4');//array(210,110));
		$mpdf->AddPage('P', // L - landscape, P - portrait
				'', '', '', '',
				1, // margin_left
				1, // margin right
				1, // margin top
				0, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
	 	$html='';
		$html.='<style>
	 			.normal{
					width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
   					border-collapse: none;
   					font-size: 12;
				}
				 
				td {

					border:0px solid;
    				text-justify: inter-word;
	 			}
					#one td {

					border:none;
    				text-justify: inter-word;
	 			}
				.bottom{
					border-bottom: none;
	 				font-size: 12px;
				}
	 			table{
	   				width: 100%;
					font-family: Times New Roman, Helvetica, sans-serif;
					 
   					border-collapse: none;
   					font-size: 13px;
   				}
				div{
	 				text-align: justify;
					border:1px none;
    				text-justify: inter-word;
	 			}
           </style>';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$telp.=' ,Fax. '.$rs->fax.'.';
				
		}
		
		$html.="<table style='font-size: 18;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr align=justify>
				<th border='0'; width='20'></th>
   				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='80' height='50'/>
   				</th>
				<th border='0'; width='2'></th>
				<th border='0'; width='2'></th>
				<th border='0'; width='2'></th>
   				<th align=center >
					<font style='font-size: 16px;font-family: Arial'><b>SURAT ELEGIBILITAS PESERTA</b></font><br>
					<font style='font-size: 11px;font-family: Arial'>".strtoupper($rs->name)." ".strtoupper($rs->city)."</font><br>
   				</th>
				<th border='0'; width='70'>
   					<img src='./ui/images/Logo/bpjs.png' width='240' height='40'/>
   				</th>
   			</tr>
   		</table>";
		
		$param=json_decode($_POST['data']);
		
		$KdInv=
		
		$nosep=$param->nosep;
		$tglsep=tanggalstring(date('Y-m-d'));
		$nokartu=$param->nokartu;
		$nama=$param->nama;
		$ttl=tanggalstring(date('Y-m-d',strtotime($param->ttl)));
		$jk=$param->jk;
		$poli=$param->poli;
		$asalfaskes='KLINIK MEDIKA ANTAPANI';
		$diagnosa=$param->diagnosa;
		$nomedrec=$param->nomedrec;
		$peserta='PEKERJA SEKTOR INFORMAL INDIVIDU';
		$jnsrawat=$param->jnsrawat;
		$klsrawat=$param->klsrawat;
		
		if($jk == 'Perempuan'){
			$jk='WANITA';
		} else{
			$jk='PRIA';
		}
		
		$html.='<br>
			<table width="862" border="0" cellspacing="0" style="font-size: 11;font-family: Arial;">
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">No. SEP</td>
				<td width="6">:</td>
				<td width="282">'.$nosep.'</td>
				<td width="26">&nbsp;</td>
				<td width="148">No. Mr</td>
				<td width="8">:</td>
				<td width="165">'.$nomedrec.'</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Tgl. SEP</td>
				<td width="6">:</td>
				<td width="282">'.$tglsep.'</td>
				<td width="26"></td>
				<td width="148"></td>
				<td width="8"></td>
				<td width="165"></td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">No. Kartu</td>
				<td width="6">:</td>
				<td width="282">'.$nokartu.'</td>
				<td width="26">&nbsp;</td>
				<td width="148">Peserta</td>
				<td width="8">:</td>
				<td width="165"; rowspan="2">'.$peserta.'</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Nama Peserta</td>
				<td width="6">:</td>
				<td width="282">'.$nama.'</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Tgl. Lahir</td>
				<td width="6">:</td>
				<td width="282">'.$ttl.'</td>
				<td width="26"></td>
				<td width="148"></td>
				<td width="8"></td>
				<td width="165"></td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Jns Kelamin</td>
				<td width="6">:</td>
				<td width="282">'.$jk.'</td>
				<td width="26">&nbsp;</td>
				<td width="148">COB</td>
				<td width="8">:</td>
				<td width="165"></td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Poli Tujuan</td>
				<td width="6">:</td>
				<td width="282">'.$poli.'</td>
				<td width="26">&nbsp;</td>
				<td width="148">Jns. Rawat</td>
				<td width="8">:</td>
				<td width="165">'.$jnsrawat.'</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">No. SEP</td>
				<td width="6">:</td>
				<td width="282">'.$nosep.'</td>
				<td width="26">&nbsp;</td>
				<td width="148">Kls. Rawat</td>
				<td width="8">:</td>
				<td width="165">'.$klsrawat.'</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Asal Faskes Tk. I</td>
				<td width="6">:</td>
				<td width="282">'.$asalfaskes.'</td>
				<td width="26"></td>
				<td width="148"></td>
				<td width="8"></td>
				<td width="165"></td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Diagnosa Awal</td>
				<td width="6">:</td>
				<td width="282">'.$diagnosa.'</td>
				<td width="26">&nbsp;</td>
				<td width="148">&nbsp;</td>
				<td width="8">&nbsp;</td>
				<td width="165">&nbsp;</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160">Catatan</td>
				<td width="6">:</td>
				<td width="282"></td>
				<td width="26">&nbsp;</td>
				<td width="148">Pasien / Keluarga</td>
				<td width="8"></td>
				<td width="165">Petugas BPJS</td>
			</tr>
			<tr>
				<th border="0"; width="3"></th>
				<td width="160"></td>
				<td width="6"></td>
				<td width="282"></td>
				<td width="26">&nbsp;</td>
				<td width="148">Pasien</td>
				<td width="8"></td>
				<td width="165">Kesehatan</td>
			</tr>
			 <tr>
				<th border="0"; width="3"></th>
				<td colspan="3";><font style="font-size: 9px;font-family: Arial"><i>*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan</i></td>
				<td width="26"></td>
				<td width="148">&nbsp;</td>
				<td width="8"></td>
				<td width="165">&nbsp;</td>
			</tr>
		    <tr>
			   <th border="0";></th>
			   <td colspan="3";><font style="font-size: 9px;font-family: Arial"><i>*SEP bukan sebagai bukti penjamin peserta</td>
			   <td></td>
		      <td>&nbsp;</td>
			   <td></td>
			   <td>&nbsp;</td>
  </tr>
		    <tr>
		      <th border="0";></th>
		      <td colspan="3";>&nbsp;</td>
		      <td></td>
		      <td>&nbsp;</td>
		      <td></td>
		      <td>&nbsp;</td>
  </tr>
		    <tr>
				<th border="0"; width="3"></th>
				<td colspan="3";>Catatan Ke 1</td>
				<td width="26"></td>
				<td width="148"><hr></td>
				<td width="8"></td>
				<td width="165"><hr></td>
			</tr> 
			</table>
		';
		
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="Cetakan SEP UGD.pdf"' );
		readfile ( 'original.pdf' );
	}
	
	 
}
?>