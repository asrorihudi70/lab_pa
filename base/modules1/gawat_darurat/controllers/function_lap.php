<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class function_lap extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}	

   	public function cetaklaporanIGD_RegisterSummary(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Pasien';
		$param=json_decode($_POST['data']);
		
		$params = array(
			'tgl_awal'  => $param->start_date,
			'tgl_akhir' => $param->last_date,
			'type_file' => $param->type_file,
			'kd_unit'   => $param->list_kd_unit,
		);
		$html = "";
		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
										where 
											u.kd_bagian='3' and  
											u.Kd_Unit in(".$params['kd_unit'].") and 
											k.tgl_masuk between  '".$params['tgl_awal']."' and  '".$params['tgl_akhir']."' 
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows() == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			if ($params['kd_unit']== "Semua" || $params['kd_unit']== ""){
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
			}else{
				$queryRS = $this->db->query("SELECT * from db_rs")->result();
			}
			$queryuser = $this->db->query("SELECT * from zusers where kd_user= "."'".$this->session->userdata['user_id']['id']."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

			if($params['type_file'] == 1){
				$html.="<style>
	                   .t1 {
	                           border: 1px solid black;
	                           border-collapse: collapse;
	                           font-family: Arial, Helvetica, sans-serif;
	                   }
					   .t1 tr th
					   {
						   font-weight:bold;
						   font-size:12px;
					   }
					    .t1 tr td
					   {
						   font-size:12px;
					   }
	                   .formarial {
	                           font-family: Arial, Helvetica, sans-serif;
	                   }
	                  
					   .t2 {
	                           font-family: Arial, Helvetica, sans-serif;
	                   }
						
						.t2 tr th
					   {
						   font-weight:bold;
						   font-size:14px;
					   }
	                   </style>";
			}else{
				$html.="<style>
	                   .t1 {
	                           border: 1px solid black;
	                           border-collapse: collapse;
	                           font-family: Arial, Helvetica, sans-serif;
	                   }
					   .t1 tr th
					   {
						   font-weight:bold;
						   font-size:12px;
					   }
					    .t1 tr td
					   {
						   font-size:12px;
					   }
	                   .formarial {
	                           font-family: Arial, Helvetica, sans-serif;
	                   }
	                  
					   .t2 {
	                           font-family: Arial, Helvetica, sans-serif;
	                   }
						
						.t2 tr th
					   {
						   font-weight:bold;
						   font-size:12px;
					   }
	                   </style>";
				
			}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.date_format(date_create($params['tgl_awal']), 'd/M/Y').' s/d '.date_format(date_create($params['tgl_akhir']), 'd/M/Y').'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" colspan="2">Kunjungan</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">Baru</th>
								<th align="center" width="50">Lama</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->br.'</td>
							<td align="right">'.$line->lm.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_br         += $line->br;
				$total_lm         += $line->lm;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right"><b>'.$total_br.'</b></td>
				<td align="right"><b>'.$total_lm.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';   
			$html.='</table>';
		}
		
		$prop=array('foot'=>true);
		if($params['type_file'] == 1){
			$name='Buku_Laporan_Summary_IGD.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }

    public function cetaklaporanIGD_Regisdet()
	{
		$common       = $this->common;
		$result       = $this->result;
		//$title      = 'Laporan Pasien Detail';
		$param        = json_decode($_POST['data']);
		
		$params = array(
			'tgl_awal'        => $param->start_date,
			'tgl_akhir'       => $param->last_date,
			'type_file'       => $param->type_file,
			'kd_unit'         => $param->list_kd_unit,
			'jenis_pasien'    => $param->jenis_pasien,
			'kd_customer'     => $param->kd_customer,
			'head_kel_pasien' => $param->head_kel_pasien,
		);

		$criteriaOrder = "kdpasien";
		/*if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "kdpasien";
		}else if(strtolower($order_by) == strtolower("Nama Pasien")){
			$criteriaOrder = "namapasien";
		}else{
			$criteriaOrder = "tglmas";
		}*/

		$html="";
		$criteria_customer = "";
		$criteria_unit     = "";
		$criteria_pasien   = "";
		$label_customer    = "SEMUA";
		if (strtolower($params['head_kel_pasien']) != "semua") {
			if (strtolower($params['kd_customer']) != "semua") {
				$criteria_customer 	= " and ktr.kd_customer = '".$params['kd_customer']."'";
				$label_customer  	= $this->db->query("SELECT * FROM customer where kd_customer = '".$params['kd_customer']."'")->row()->customer;
			}
		}

		if ($params['kd_unit'] != "") {
			$criteria_unit     = " and u.kd_unit in(".$params['kd_unit'].") ";
		}
		if (strtolower($params['jenis_pasien']) != "semua") {
			if (strtolower($params['jenis_pasien']) == "0") {
				$criteria_pasien = " and k.baru = 'true' ";
			}else{
				$criteria_pasien = " and k.baru = 'false' ";
			}
		}

		$kd_rs    = $this->session->userdata['user_id']['kd_rs'];
		$queryRS  = "select * from db_rs WHERE code='".$kd_rs."'";
		$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
		$no       = 0;

		while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
		{							   
			$telp='';
			$fax='';
			if(($line['phone1'] != null && $line['phone1'] != '')|| ($line['phone2'] != null && $line['phone2'] != '')){
				$telp='<br>Telp. ';
				$telp1=false;
				if($line['phone1'] != null && $line['phone1'] != ''){
					$telp1=true;
					$telp.=$line['phone1'];
				}
				if($line['phone2'] != null && $line['phone2'] != ''){
					if($telp1==true){
						$telp.='/'.$line['phone2'].'.';
					}else{
						$telp.=$line['phone2'].'.';
					}
				}else{
					$telp.='.';
				}
			}
			if($line['fax'] != null && $line['fax']  != ''){
				$fax='<br>Fax. '.$line['fax'] .'.';
			}
        }
		if($params['type_file'] == 1){
		$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		#HEADER TABEL LAPORAN
		$html.='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="15">Laporan Buku Registrasi Detail Rawat Jalan</th>
					</tr>
			';
		$html.='
					<tr>
						<th colspan="15">Customer : '.$label_customer.'</th>
					</tr>';
		$html.='
					<tr>
						<th colspan="15">Periode '.date_format(date_create($params['tgl_awal']), 'd/M/Y').' s/d '.date_format(date_create($params['tgl_akhir']), 'd/M/Y').'</th>
					</tr>
				</tbody>
			</table> <br>';
		$html.='
			<table class="t1" border = "1">
			<thead>
			 <tr>
				   <th align="center" width="24" rowspan="2">No. </th>
				   <th align="center" width="80" rowspan="2">No. Medrec</th>
				   <th align="center" width="210" rowspan="2">Nama Pasien</th>
				   <th align="center" width="220" rowspan="2">Alamat</th>
				   <th align="center" width="26" colspan="2">Kelamin</th>
				   <th align="center" width="100" rowspan="2">Umur</th>
				   <th align="center" colspan="2">Kunjungan</th>
				   <th align="center" width="82" rowspan="2">Pekerjaan</th>
				   <th align="center" width="68" rowspan="2">Tanggal Masuk</th>
				   <th align="center" width="63" rowspan="2">Rujukan</th>
				   <th align="center" width="30" rowspan="2">Jam Masuk</th>
				   <th align="center" width="100" rowspan="2">Kode Diagnosa</th>
				   <th align="center" width="63" rowspan="2">Diagnosa</th>
				   <th align="center" width="63" rowspan="2">Dokter</th>
				   <th align="center" width="63" rowspan="2">User</th>
			 </tr>
			 <tr>
				   <td align="center" width="37"><b>L</b></td>
				   <td align="center" width="37"><b>P</b></td>
				   <td align="center" width="37"><b>Baru</b></td>
				   <td align="center" width="37"><b>Lama</b></td>
			 </tr>
			</thead>';

		$fquery = pg_query("SELECT 
								u.kd_unit,
			    				u.nama_unit from unit u 
			    				left join kunjungan 
								on kunjungan.kd_unit = u.kd_unit 
								where 
								kd_bagian ='03' 
								and kunjungan.tgl_masuk between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'
								".$criteria_unit."
						   	group by u.kd_unit,u.nama_unit  order by nama_unit asc");
		$html.='<tbody>';
		$totBaru     = 0;
		$totLama     = 0;
		$totalpasien = 0;
		$totL        = 0;
		$totP        = 0;
		$totRujukan  = 0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
					$query = "SELECT k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||
									to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||'bln ' ||
									to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||'hari'
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'x'  else ''  end as pasienbar,
								case when k.Baru=false then 'x'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
								k.Jam_masuk as jammas,
								k.Tgl_masuk as tglmas,
								dr.nama as dokter, 
								c.customer as customer, 
								zu.user_names as username,
								getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								LEFT join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where 
									k.tgl_masuk between '".$params['tgl_awal']."' and '".$params['tgl_akhir']."' 
									and k.kd_unit = '".$f['kd_unit']."'
									".$criteria_customer."
									".$criteria_pasien."
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
								zu.user_names,k.urut_masuk
								Order By $criteriaOrder ";

		// echo $query;
       $result = pg_query($query) or die('Query failed: ' . pg_last_error());
       $i=1;

		   if(pg_num_rows($result) <= 0)
		   {
				$html.='';
		   }else{
				$html.='<tr><td colspan="17">'.$f['nama_unit'].'</td></tr>';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='x'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='x'){
						$totLama+=1;
					}
					if($line['rujukan']=='x'){
						$totRujukan+=1;
					}

					$html.='
							<tr class="headerrow"> 

							<td align="right">'.$i.'</td>

							<td  align="left" style="padding-left:5px;">'.$line['kdpasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['namapasien'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['alamatpas'].'</td>';
							/*<td align="center">'.$line['jk'].'</td>
							<td align="center"></td>*/
							if (strtolower($line['jk']) == "l") {
								$html .= "<td align='center'>X</td><td align='center'></td>";
								$totL++;
							}else{
								$html .= "<td align='center'></td><td align='center'>X</td>";
								$totP++;
							}
					$html.='<td  align="left" style="padding-left:5px;">'.$line['umur'].'</td>
							<td  align="center">'.strtoupper($line['pasienbar']).'</td>
							<td  align="center">'.strtoupper($line['pasienlama']).'</td>
							<td  align="left" style="padding-left:5px;">'.$line['pekerjaan'].'</td>
							<td  align="center">'.date('d.m.Y', strtotime($line['tglmas'])).'</td>
							<td  align="center">'.$line['rujukan'].'</td>
							<td  align="left" style="padding-left:5px;">'.date_format(date_create($line['jammas']), 'H:i:s').'</td>
							<td  align="left" style="padding-left:5px;">'.$line['kd_diagnosa'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['penyakit'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['dokter'].'</td>
							<td  align="left" style="padding-left:5px;">'.$line['username'].'</td>
							</tr>
							
					';
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$html.='<tr>
					<td colspan="3"><b>Total Pasien Daftar di '.$f['nama_unit'].' : </b></td>
					<td><b>'.$i.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totL.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totP.'</b></td>
					<td></td>
					<td align="center" style="padding:5px;"><b>'.$totBaru.'</b></td>
					<td align="center" style="padding:5px;"><b>'.$totLama.'</b></td>
					<td colspan="2"></td>
					<td align="center"><b>'.$totRujukan.'</b></td>
					<td colspan="5"></td>
					</tr>';
					$totL 		= 0;
					$totP 		= 0;
					$totBaru 	= 0;
					$totLama 	= 0;
					$totRujukan	= 0;
			}
        }
		$html.='<tr><td colspan="3"><b>Total Keseluruhan Pasien</b></td><td colspan="14"><b>\''.$totalpasien.'</b></td></tr>';
		// $html.='<tr><td colspan="3">&nbsp;</td><td colspan="12"><b>Baru : '.$totBaru.'/Lama :'.$totLama.'</b></td></tr>';
		$html.='</tbody></table>';
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($params['type_file'] == 1){
			$name='Laporan_Pasien_Detail.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Buku_register_igd_detail',$html);
		}
	}
	/*
	
		PENAMBAHAN LAPORAN BARU 
		OLEH HADAD AL GOJALI 
		2017 - 11 - 21
		*Harus tersedia template di Server
	 */
	public function laporan_tunai_per_komponen_det(){
		$common       = $this->common;
		$result       = $this->result;
		$param 	= json_decode($this->input->post('data'));
		$params = array(
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'shift_1' 		=> $param->shift1,
			'shift_2' 		=> $param->shift2,
			'shift_3' 		=> $param->shift3,
			'pendaftaran' 	=> $param->tindakan0,
			'tindakan' 		=> $param->tindakan1,
			'transfer' 		=> $param->tindakan2,
			'arr_kd_unit'	=> $param->tmp_kd_unit,
			'arr_kd_pay'	=> $param->tmp_payment,
			'type_file'		=> $param->type_file,
			'kd_customer'	=> $param->kd_customer,
			'pasien'		=> $param->pasien,
		);

		$criteria_shift = "";

		if ($params['shift_1'] === true || $params['shift_1'] == 'true') {
			$criteria_shift .= " 1, ";
		}

		if ($params['shift_2'] === true || $params['shift_2'] == 'true') {
			$criteria_shift .= " 2, ";
		}

		if ($params['shift_3'] === true || $params['shift_3'] == 'true') {
			$criteria_shift .= " 3, ";
		}

		$criteria_shift = substr($criteria_shift, 0, strlen($criteria_shift)-2);

		$criteria_tindakan = "";
		if ($params['pendaftaran'] === true || $params['pendaftaran'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas = '1' ";
		}

		if ($params['transfer'] === true || $params['transfer'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas = '9' ";
		}

		if ($params['tindakan'] === true || $params['tindakan'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas <> '1' and p.kd_klas <> '9' ";
		}

		$label_customer = "";
		if (strtolower($params['pasien']) != 'semua') {
			if ($params['pasien'] == 0 || $params['pasien'] == '0') {
				$label_customer = " Perseorangan ";
				if (strtolower($params['kd_customer']) != 'semua') {
					$label_customer .= "(".$this->get_data_customer($params['kd_customer']).")";
				}else{
					$label_customer .= "(Semua)";
				}
			}else if ($params['pasien'] == 1 || $params['pasien'] == '1') {
				$label_customer = " Perusahaan ";
				if (strtolower($params['kd_customer']) != 'semua') {
					$label_customer .= "(".$this->get_data_customer($params['kd_customer']).")";
				}else{
					$label_customer .= "(Semua)";
				}
			}else if ($params['pasien'] == 2 || $params['pasien'] == '2') {
				$label_customer = " Asuransi ";
				if (strtolower($params['kd_customer']) != 'semua') {
					$label_customer .= "(".$this->get_data_customer($params['kd_customer']).")";
				}else{
					$label_customer .= "(Semua)";
				}
			}else{
				$label_customer = " Semua ";
			}
		}else{
			$label_customer = " Semua ";
		}


		$query = "SELECT u.nama_unit, 
			t.no_transaksi, 
			ps.kd_pasien, 
			ps.nama, 
			p.deskripsi,  
			sum(x.jumlah) as jumlah,
			sum(c20) as c20,
			sum(c21) as c21,
			sum(c22) as c22,
			sum(c23) as c23,
			sum(c24) as c24,
			sum(c25) as c25,
			sum(c26) as c26,
			sum(c30) as c30,
			sum(jpl) as jpl
		FROM Kunjungan k INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
			INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
			INNER JOIN (SELECT dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dt.qty, 
				SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C20, 
				SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C21, 
				SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C22, 
				SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C23, 
				SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C24, 
				SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C25, 
				SUM(CASE WHEN dtc.kd_component =26 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C26, 
				SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah * dt.qty ELSE 0 END) as C30,  
				Sum(case when pc.kd_jenis=2 then dtc.jumlah * dt.qty else 0 end ) as jPL, 
				sum(dtc.Jumlah * dt.qty) as Jumlah  FROM (SELECT * FROM Detail_Bayar 
		WHERE kd_kasir='06' AND ((Tgl_Transaksi BETWEEN '".$params['start_date']."' AND '".$params['last_date']."' 
			AND Shift In ($criteria_shift))  OR  (Tgl_Transaksi BETWEEN '".$params['start_date']."' AND '".$params['last_date']."'  AND Shift = 4) ) 
			AND (KD_PAY in (".$params['arr_kd_pay'].")) ) db 
			INNER JOIN (Detail_TR_Bayar dtb 
			INNER JOIN (Detail_TR_Bayar_Component dtc  
			INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) ON dtc.kd_kasir = dtb.kd_kasir  
				and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi 
				and dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar) ON db.kd_kasir = dtb.kd_kasir 
				and db.no_transaksi = dtb.no_transaksi AND db.urut = dtb.urut_bayar AND db.tgl_transaksi = dtb.tgl_bayar 
			INNER JOIN Detail_Transaksi dt on dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
			GROUP BY dtb.kd_kasir, dtb.no_transaksi, dtb.urut, dtb.tgl_transaksi, dtb.Jumlah,dt.qty) x ON x.kd_kasir = dt.kd_kasir 
				AND x.no_transaksi = dt.no_transaksi and x.urut = dt.urut  and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
			WHERE 
				(t.ispay = 'true' And t.kd_Unit in (".$params['arr_kd_unit'].")  And t.kd_kasir = '06')  
				and (   
					$criteria_tindakan
				)
			GROUP BY U.Nama_Unit, t.No_transaksi, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi, k.Kd_Pasien  
		ORDER BY U.Nama_Unit, Ps.Nama, t.no_Transaksi";

		$result_query = $this->db->query($query);

		$nama = array();
		foreach ($result_query->result() as $result) {
			$nama[] = $result->kd_pasien;
		}

		$tmp_header   = implode('#', array_unique($nama));
		$array_header = explode("#", $tmp_header);
		$total_jasa_1  	= 0;
		$total_jasa_2  	= 0;
		$total_jasa_3  	= 0;
		$total_jasa_4  	= 0;
		$total_jasa_5  	= 0;
		$total_jasa_6  	= 0;
		$total_jasa_7  	= 0;
		$total_jasa_8  	= 0;
		$total_jasa_9  	= 0;
		$total_jasa_10 	= 0;
		$file = "Penerimaan tunai per komponen (IGD)";
		if ($result_query->num_rows() > 0) {

			if($params['type_file'] == 1){
				$url = "Doc Asset/format_laporan/".$file.".xls";

				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();

				$sharedStyle = new PHPExcel_Style();
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$baris = 6;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y'));
				$phpExcel->getActiveSheet()->setCellValue('A3', "Customer : ".$label_customer);
				for ($i=0; $i < count($array_header); $i++) { 
					$nama_pasien = $this->db->query("SELECT nama FROM pasien where kd_pasien = '".$array_header[$i]."'")->row()->nama;
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, ($i+1)." - ".$array_header[$i]." ".$nama_pasien);
					$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
					$phpExcel->getActiveSheet()
								->getStyle('A'.$baris.':B'.$baris)
								->getAlignment()
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

					$phpExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(
					 array(
					 'font' => array(
					 	'bold' => true
					 	),
					 )
					);

					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':L'.$baris);
					$no_body = 1;
					$sub_jasa_1  = 0;
					$sub_jasa_2  = 0;
					$sub_jasa_3  = 0;
					$sub_jasa_4  = 0;
					$sub_jasa_5  = 0;
					$sub_jasa_6  = 0;
					$sub_jasa_7  = 0;
					$sub_jasa_8  = 0;
					$sub_jasa_9  = 0;
					$sub_jasa_10 = 0;
					foreach ($result_query->result() as $result) {
						if ($result->kd_pasien == $array_header[$i]) {
							$phpExcel->getActiveSheet()->setCellValue('A'.((int)$baris+(int)$no_body), $no_body);
							$phpExcel->getActiveSheet()->setCellValue('B'.((int)$baris+(int)$no_body), $result->deskripsi);
							$phpExcel->getActiveSheet()->setCellValue('C'.((int)$baris+(int)$no_body), $result->c20);
							$phpExcel->getActiveSheet()->setCellValue('D'.((int)$baris+(int)$no_body), $result->c21);
							$phpExcel->getActiveSheet()->setCellValue('E'.((int)$baris+(int)$no_body), $result->c22);
							$phpExcel->getActiveSheet()->setCellValue('F'.((int)$baris+(int)$no_body), $result->c23);
							$phpExcel->getActiveSheet()->setCellValue('G'.((int)$baris+(int)$no_body), $result->c24);
							$phpExcel->getActiveSheet()->setCellValue('H'.((int)$baris+(int)$no_body), $result->c25);
							$phpExcel->getActiveSheet()->setCellValue('I'.((int)$baris+(int)$no_body), $result->c26);
							$phpExcel->getActiveSheet()->setCellValue('J'.((int)$baris+(int)$no_body), $result->c30);
							$phpExcel->getActiveSheet()->setCellValue('K'.((int)$baris+(int)$no_body), $result->jpl);
							$phpExcel->getActiveSheet()->setCellValue('L'.((int)$baris+(int)$no_body), $result->jumlah);
							$sub_jasa_1  += $result->c20;
							$sub_jasa_2  += $result->c21;
							$sub_jasa_3  += $result->c22;
							$sub_jasa_4  += $result->c23;
							$sub_jasa_5  += $result->c24;
							$sub_jasa_6  += $result->c25;
							$sub_jasa_7  += $result->c26;
							$sub_jasa_8  += $result->c30;
							$sub_jasa_9  += $result->jpl;
							$sub_jasa_10 += $result->jumlah;
							$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.((int)$baris+(int)$no_body).':L'.((int)$baris+(int)$no_body));
							$no_body++;
						}
					}
					$phpExcel->getActiveSheet()->setCellValue('A'.((int)$baris+(int)$no_body), "Sub Total");
					$phpExcel->getActiveSheet()->setCellValue('C'.((int)$baris+(int)$no_body), $sub_jasa_1);
					$phpExcel->getActiveSheet()->setCellValue('D'.((int)$baris+(int)$no_body), $sub_jasa_2);
					$phpExcel->getActiveSheet()->setCellValue('E'.((int)$baris+(int)$no_body), $sub_jasa_3);
					$phpExcel->getActiveSheet()->setCellValue('F'.((int)$baris+(int)$no_body), $sub_jasa_4);
					$phpExcel->getActiveSheet()->setCellValue('G'.((int)$baris+(int)$no_body), $sub_jasa_5);
					$phpExcel->getActiveSheet()->setCellValue('H'.((int)$baris+(int)$no_body), $sub_jasa_6);
					$phpExcel->getActiveSheet()->setCellValue('I'.((int)$baris+(int)$no_body), $sub_jasa_7);
					$phpExcel->getActiveSheet()->setCellValue('J'.((int)$baris+(int)$no_body), $sub_jasa_8);
					$phpExcel->getActiveSheet()->setCellValue('K'.((int)$baris+(int)$no_body), $sub_jasa_9);
					$phpExcel->getActiveSheet()->setCellValue('L'.((int)$baris+(int)$no_body), $sub_jasa_10);
					$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.((int)$baris+(int)$no_body).':L'.((int)$baris+(int)$no_body));

					$phpExcel->getActiveSheet()->getStyle('A'.((int)$baris+(int)$no_body).':L'.((int)$baris+(int)$no_body))->applyFromArray(
					 array(
					 'font' => array(
					 	'bold' => true
					 	),
					 )
					);

					$total_jasa_1  	+= $sub_jasa_1;
					$total_jasa_2  	+= $sub_jasa_2;
					$total_jasa_3  	+= $sub_jasa_3;
					$total_jasa_4  	+= $sub_jasa_4;
					$total_jasa_5  	+= $sub_jasa_5;
					$total_jasa_6  	+= $sub_jasa_6;
					$total_jasa_7  	+= $sub_jasa_7;
					$total_jasa_8  	+= $sub_jasa_8;
					$total_jasa_9  	+= $sub_jasa_9;
					$total_jasa_10 	+= $sub_jasa_10;
					// $baris += 1;
					$baris = ($baris+1)+$no_body;
					// $phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':L'.$baris);
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "Grand Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_jasa_1);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_jasa_2);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_jasa_3);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_jasa_4);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jasa_5);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jasa_6);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_jasa_7);
				$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $total_jasa_8);
				$phpExcel->getActiveSheet()->setCellValue('K'.$baris, $total_jasa_9);
				$phpExcel->getActiveSheet()->setCellValue('L'.$baris, $total_jasa_10);
				$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':L'.$baris);
				$phpExcel->getActiveSheet()->getStyle('A'.$baris.':L'.$baris)->applyFromArray(
				 array(
				 'font' => array(
				 	'bold' => true
				 	),
				 )
				);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.$file.'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$table = "";
				$table .= "<table width='100%' border='0' cellspacing='0'>";
				$table .= "<tr>";
				$table .= "<th align='center'>PENERIMAAN TUNAI PER KOMPONEN (IGD)</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th align='center'>Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y')."</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th align='center'>Customer : ".$label_customer."</th>";
				$table .= "</tr>";
				$table .= "</table>";
				$table .= "</br>";
				$table .= "</hr>";

				$table .= "<table width='100%' border='1' cellspacing='0'>";
				$table .= "<tr>";
				$table .= "<th width='10'>No</th>";
				$table .= "<th width='250'>Tindakan</th>";
				$table .= "<th width='25'>Jasa Dokter</th>";
				$table .= "<th width='25'>Jasa Perawat/Bidan</th>";
				$table .= "<th width='25'>Tindakan tdk langsung</th>";
				$table .= "<th width='25'>Operator</th>";
				$table .= "<th width='25'>Ops. RS</th>";
				$table .= "<th width='25'>Ops. Direksi</th>";
				$table .= "<th width='25'>BHP</th>";
				$table .= "<th width='25'>Jasa Sarana</th>";
				$table .= "<th width='25'>Jasa Pelayanan</th>";
				$table .= "<th width='25'>Total</th>";
				$table .= "</tr>";
				$no = 1;

				for ($i=0; $i < count($array_header); $i++) { 
					$nama_pasien = $this->db->query("SELECT nama FROM pasien where kd_pasien = '".$array_header[$i]."'")->row()->nama;
					$table .= "<tr>";
					$table .= "<td colspan='12' style='padding-left:10px;'><b>".($i+1)." - ".$array_header[$i]." ".$nama_pasien."</b></td>";
					$table .= "</tr>";
					$no_body = 1;

					$sub_jasa_1  = 0;
					$sub_jasa_2  = 0;
					$sub_jasa_3  = 0;
					$sub_jasa_4  = 0;
					$sub_jasa_5  = 0;
					$sub_jasa_6  = 0;
					$sub_jasa_7  = 0;
					$sub_jasa_8  = 0;
					$sub_jasa_9  = 0;
					$sub_jasa_10 = 0;
					foreach ($result_query->result() as $result) {
						if ($result->kd_pasien == $array_header[$i]) {
							$table .= "<tr>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='center'>".$no_body."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;'>".$result->deskripsi."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c20."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c21."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c22."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c23."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c24."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c25."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c26."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c30."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->jpl."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->jumlah."</td>";
							$table .= "</tr>";

							$sub_jasa_1  += $result->c20;
							$sub_jasa_2  += $result->c21;
							$sub_jasa_3  += $result->c22;
							$sub_jasa_4  += $result->c23;
							$sub_jasa_5  += $result->c24;
							$sub_jasa_6  += $result->c25;
							$sub_jasa_7  += $result->c26;
							$sub_jasa_8  += $result->c30;
							$sub_jasa_9  += $result->jpl;
							$sub_jasa_10 += $result->jumlah;
							$no_body++;
						}
					}
					$table .= "<tr>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='left' colspan='2'>Sub Total</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_1."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_2."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_3."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_4."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_5."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_6."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_7."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_8."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_9."</td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$sub_jasa_10."</td>";
					$table .= "</tr>";

					$total_jasa_1  	+= $sub_jasa_1;
					$total_jasa_2  	+= $sub_jasa_2;
					$total_jasa_3  	+= $sub_jasa_3;
					$total_jasa_4  	+= $sub_jasa_4;
					$total_jasa_5  	+= $sub_jasa_5;
					$total_jasa_6  	+= $sub_jasa_6;
					$total_jasa_7  	+= $sub_jasa_7;
					$total_jasa_8  	+= $sub_jasa_8;
					$total_jasa_9  	+= $sub_jasa_9;
					$total_jasa_10 	+= $sub_jasa_10;
				}
				$table .= "<tr>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='left' colspan='2'>Grand Total</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_1."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_2."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_3."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_4."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_5."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_6."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_7."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_8."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_9."</td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$total_jasa_10."</td>";
				$table .= "</tr>";
				$table .= "</table>";
				$this->common->setPdf('L',$file,$table);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}

	/*
	
		PENAMBAHAN LAPORAN BARU 
		OLEH HADAD AL GOJALI 
		2017 - 11 - 21
		*Harus tersedia template di Server
	 */
	public function laporan_tunai_per_komponen_sum(){
		$common       = $this->common;
		$result       = $this->result;
		$param 	= json_decode($this->input->post('data'));
		$params = array(
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'shift_1' 		=> $param->shift1,
			'shift_2' 		=> $param->shift2,
			'shift_3' 		=> $param->shift3,
			'pendaftaran' 	=> $param->tindakan0,
			'tindakan' 		=> $param->tindakan1,
			'transfer' 		=> $param->tindakan2,
			'arr_kd_unit'	=> $param->tmp_kd_unit,
			'arr_kd_pay'	=> $param->tmp_payment,
			'type_file'		=> $param->type_file,
			'kd_customer'	=> $param->kd_customer,
			'pasien'		=> $param->pasien,
		);

		$criteria_shift = "";

		if ($params['shift_1'] === true || $params['shift_1'] == 'true') {
			$criteria_shift .= " 1, ";
		}

		if ($params['shift_2'] === true || $params['shift_2'] == 'true') {
			$criteria_shift .= " 2, ";
		}

		if ($params['shift_3'] === true || $params['shift_3'] == 'true') {
			$criteria_shift .= " 3, ";
		}

		$criteria_shift = substr($criteria_shift, 0, strlen($criteria_shift)-2);

		$criteria_tindakan = "";
		if ($params['pendaftaran'] === true || $params['pendaftaran'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas = '1' ";
		}

		if ($params['transfer'] === true || $params['transfer'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas = '9' ";
		}

		if ($params['tindakan'] === true || $params['tindakan'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas <> '1' and p.kd_klas <> '9' ";
		}

		$label_customer = "";
		if (strtolower($params['pasien']) != 'semua') {
			if ($params['pasien'] == 0 || $params['pasien'] == '0') {
				$label_customer = " Perseorangan ";
				if (strtolower($params['kd_customer']) != 'semua') {
					$label_customer .= "(".$this->get_data_customer($params['kd_customer']).")";
				}else{
					$label_customer .= "(Semua)";
				}
			}else if ($params['pasien'] == 1 || $params['pasien'] == '1') {
				$label_customer = " Perusahaan ";
				if (strtolower($params['kd_customer']) != 'semua') {
					$label_customer .= "(".$this->get_data_customer($params['kd_customer']).")";
				}else{
					$label_customer .= "(Semua)";
				}
			}else if ($params['pasien'] == 2 || $params['pasien'] == '2') {
				$label_customer = " Asuransi ";
				if (strtolower($params['kd_customer']) != 'semua') {
					$label_customer .= "(".$this->get_data_customer($params['kd_customer']).")";
				}else{
					$label_customer .= "(Semua)";
				}
			}else{
				$label_customer = " Semua ";
			}
		}else{
			$label_customer = " Semua ";
		}

		$query = "SELECT 
			u.nama_unit, 
			p.deskripsi, 
			count(k.kd_pasien) as jml_pasien, 
			sum(dt.qty) as qty,  
			sum(x.jumlah * dt.qty) as jumlah,
			sum(c20)* dt.qty  as c20,
			sum(c21)* dt.qty  as c21,
			sum(c22)* dt.qty  as c22,
			sum(c23)* dt.qty  as c23,
			sum(c24)* dt.qty  as c24,
			sum(c25)* dt.qty  as c25,
			sum(c26)* dt.qty  as c26,
			sum(c30)* dt.qty  as c30,
			sum(jpl* dt.qty) as jpl
		from (select  
			dtbc.kd_kasir, 
			dtbc.no_transaksi, 
			dtbc.urut, 
			dtbc.tgl_transaksi,
			sum(case when dtbc.kd_component =20 then dtbc.jumlah else 0 end) as c20,
			sum(case when dtbc.kd_component =21 then dtbc.jumlah else 0 end) as c21,
			sum(case when dtbc.kd_component =22 then dtbc.jumlah else 0 end) as c22,
			sum(case when dtbc.kd_component =23 then dtbc.jumlah else 0 end) as c23,
			sum(case when dtbc.kd_component =24 then dtbc.jumlah else 0 end) as c24,
			sum(case when dtbc.kd_component =25 then dtbc.jumlah else 0 end) as c25,
			sum(case when dtbc.kd_component =26 then dtbc.jumlah else 0 end) as c26,
			sum(case when dtbc.kd_component =30 then dtbc.jumlah else 0 end) as c30,
			sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jpl,
			sum(dtbc.jumlah) as jumlah from detail_tr_bayar dtb 
				inner join detail_tr_bayar_component dtbc on dtb.kd_kasir = dtbc.kd_kasir and dtb.no_transaksi = dtbc.no_transaksi 
					and dtb.urut = dtbc.urut       and dtb.tgl_transaksi = dtbc.tgl_transaksi and dtb.kd_pay = dtbc.kd_pay 
					and dtb.urut_bayar = dtbc.urut_bayar and dtb.tgl_bayar = dtbc.tgl_bayar 
				inner join detail_bayar db on dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi 
					and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi 
				inner join produk_component pc on dtbc.kd_component = pc.kd_component 
		where dtb.kd_kasir='06' and ((db.tgl_transaksi BETWEEN '".$params['start_date']."' AND '".$params['last_date']."' 
			AND Shift In ($criteria_shift))  OR  (db.tgl_transaksi BETWEEN '".$params['start_date']."' AND '".$params['last_date']."'  AND Shift = 4))  
			and (dtb.kd_pay in (".$params['arr_kd_pay'].")) 
			group by dtbc.kd_kasir, dtbc.no_transaksi, dtbc.urut, dtbc.tgl_transaksi) x 
			inner join  transaksi t on x.kd_kasir = t.kd_kasir and x.no_transaksi = t.no_transaksi 
			inner join  kunjungan k on t.kd_pasien=k.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk 
			inner join  detail_transaksi dt on dt.kd_kasir = x.kd_kasir and dt.no_transaksi = x.no_transaksi   and dt.urut = x.urut  and dt.tgl_transaksi = x.tgl_transaksi 
			inner join unit u on u.kd_unit=t.kd_unit inner join produk p on p.kd_produk= dt.kd_produk 
			left join kontraktor ktr on ktr.kd_customer=k.kd_customer  
			where  t.kd_unit in (".$params['arr_kd_unit'].") 
			and (   
				$criteria_tindakan
			) 
			and t.ispay = 'true'  
			and t.kd_kasir = '06'  
		group by u.nama_unit, p.deskripsi , dt.qty  
		order by u.nama_unit";

		$result_query = $this->db->query($query);

		$nama_unit = array();
		foreach ($result_query->result() as $result) {
			$nama_unit[] = $result->nama_unit;
		}

		$tmp_header   = implode('#', array_unique($nama_unit));
		$array_header = explode("#", $tmp_header);
		$total_jasa_1  	= 0;
		$total_jasa_2  	= 0;
		$total_jasa_3  	= 0;
		$total_jasa_4  	= 0;
		$total_jasa_5  	= 0;
		$total_jasa_6  	= 0;
		$total_jasa_7  	= 0;
		$total_jasa_8  	= 0;
		$total_jasa_9  	= 0;
		$total_jasa_10 	= 0;
		$total_jasa_11 	= 0;
		$total_jasa_12	= 0;
		$file = "Penerimaan tunai per komponen - Summary (IGD)";
		if ($result_query->num_rows() > 0) {
			if($params['type_file'] == 1){
				$url = "Doc Asset/format_laporan/".$file.".xls";

				$numberFormat='#,##0';//'General','0.00','@';
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();

				$sharedStyle = new PHPExcel_Style();
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$baris = 6;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y'));
				$phpExcel->getActiveSheet()->setCellValue('A3', "Customer : ".$label_customer);
				for ($i=0; $i < count($array_header); $i++) { 
					// $nama_pasien = $this->db->query("SELECT nama FROM pasien where kd_pasien = '".$array_header[$i]."'")->row()->nama;
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, ($i+1)." - ".$array_header[$i]);
					$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
					$phpExcel->getActiveSheet()
								->getStyle('A'.$baris.':B'.$baris)
								->getAlignment()
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

					$phpExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(
						array(
						'font' => array(
							'bold' => true
							),
						)
					);

					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':N'.$baris);
					$no_body = 1;
					$sub_jasa_1  = 0;
					$sub_jasa_2  = 0;
					$sub_jasa_3  = 0;
					$sub_jasa_4  = 0;
					$sub_jasa_5  = 0;
					$sub_jasa_6  = 0;
					$sub_jasa_7  = 0;
					$sub_jasa_8  = 0;
					$sub_jasa_9  = 0;
					$sub_jasa_10 = 0;
					$sub_jasa_11 = 0;
					$sub_jasa_12 = 0;
					foreach ($result_query->result() as $result) {
						if ($result->nama_unit == $array_header[$i]) {
							$phpExcel->getActiveSheet()->setCellValue('A'.((int)$baris+(int)$no_body), $no_body);
							$phpExcel->getActiveSheet()->setCellValue('B'.((int)$baris+(int)$no_body), $result->deskripsi);
							$phpExcel->getActiveSheet()->setCellValue('C'.((int)$baris+(int)$no_body), $result->jml_pasien);
							$phpExcel->getActiveSheet()->setCellValue('D'.((int)$baris+(int)$no_body), $result->qty);
							$phpExcel->getActiveSheet()->setCellValue('E'.((int)$baris+(int)$no_body), $result->c20);
							$phpExcel->getActiveSheet()->setCellValue('F'.((int)$baris+(int)$no_body), $result->c21);
							$phpExcel->getActiveSheet()->setCellValue('G'.((int)$baris+(int)$no_body), $result->c22);
							$phpExcel->getActiveSheet()->setCellValue('H'.((int)$baris+(int)$no_body), $result->c23);
							$phpExcel->getActiveSheet()->setCellValue('I'.((int)$baris+(int)$no_body), $result->c24);
							$phpExcel->getActiveSheet()->setCellValue('J'.((int)$baris+(int)$no_body), $result->c25);
							$phpExcel->getActiveSheet()->setCellValue('K'.((int)$baris+(int)$no_body), $result->c26);
							$phpExcel->getActiveSheet()->setCellValue('L'.((int)$baris+(int)$no_body), $result->c30);
							$phpExcel->getActiveSheet()->setCellValue('M'.((int)$baris+(int)$no_body), $result->jpl);
							$phpExcel->getActiveSheet()->setCellValue('N'.((int)$baris+(int)$no_body), $result->jumlah);


							$sub_jasa_1  += $result->jml_pasien;
							$sub_jasa_2  += $result->qty;
							$sub_jasa_3  += $result->c20;
							$sub_jasa_4  += $result->c21;
							$sub_jasa_5  += $result->c22;
							$sub_jasa_6  += $result->c23;
							$sub_jasa_7  += $result->c24;
							$sub_jasa_8  += $result->c25;
							$sub_jasa_9  += $result->c26;
							$sub_jasa_10 += $result->c30;
							$sub_jasa_11 += $result->jpl;
							$sub_jasa_12 += $result->jumlah;
							$phpExcel->getActiveSheet()
										->getStyle('E'.((int)$baris+(int)$no_body).':N'.((int)$baris+(int)$no_body))
										->getNumberFormat()
										->setFormatCode($numberFormat);
							$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.((int)$baris+(int)$no_body).':N'.((int)$baris+(int)$no_body));
							$no_body++;
						}
					}
					$phpExcel->getActiveSheet()->setCellValue('A'.((int)$baris+(int)$no_body), "Sub Total");
					$phpExcel->getActiveSheet()->setCellValue('C'.((int)$baris+(int)$no_body), $sub_jasa_1);
					$phpExcel->getActiveSheet()->setCellValue('D'.((int)$baris+(int)$no_body), $sub_jasa_2);
					$phpExcel->getActiveSheet()->setCellValue('E'.((int)$baris+(int)$no_body), $sub_jasa_3);
					$phpExcel->getActiveSheet()->setCellValue('F'.((int)$baris+(int)$no_body), $sub_jasa_4);
					$phpExcel->getActiveSheet()->setCellValue('G'.((int)$baris+(int)$no_body), $sub_jasa_5);
					$phpExcel->getActiveSheet()->setCellValue('H'.((int)$baris+(int)$no_body), $sub_jasa_6);
					$phpExcel->getActiveSheet()->setCellValue('I'.((int)$baris+(int)$no_body), $sub_jasa_7);
					$phpExcel->getActiveSheet()->setCellValue('J'.((int)$baris+(int)$no_body), $sub_jasa_8);
					$phpExcel->getActiveSheet()->setCellValue('K'.((int)$baris+(int)$no_body), $sub_jasa_9);
					$phpExcel->getActiveSheet()->setCellValue('L'.((int)$baris+(int)$no_body), $sub_jasa_10);
					$phpExcel->getActiveSheet()->setCellValue('M'.((int)$baris+(int)$no_body), $sub_jasa_11);
					$phpExcel->getActiveSheet()->setCellValue('N'.((int)$baris+(int)$no_body), $sub_jasa_12);
					$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
					$phpExcel->getActiveSheet()
								->getStyle('E'.((int)$baris+(int)$no_body).':N'.((int)$baris+(int)$no_body))
								->getNumberFormat()
								->setFormatCode($numberFormat);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.((int)$baris+(int)$no_body).':N'.((int)$baris+(int)$no_body));
					$phpExcel->getActiveSheet()->getStyle('A'.((int)$baris+(int)$no_body).':N'.((int)$baris+(int)$no_body))->applyFromArray(
					 array(
					 'font' => array(
					 	'bold' => true
					 	),
					 )
					);

					$total_jasa_1  	+= $sub_jasa_1;
					$total_jasa_2  	+= $sub_jasa_2;
					$total_jasa_3  	+= $sub_jasa_3;
					$total_jasa_4  	+= $sub_jasa_4;
					$total_jasa_5  	+= $sub_jasa_5;
					$total_jasa_6  	+= $sub_jasa_6;
					$total_jasa_7  	+= $sub_jasa_7;
					$total_jasa_8  	+= $sub_jasa_8;
					$total_jasa_9  	+= $sub_jasa_9;
					$total_jasa_10 	+= $sub_jasa_10;
					$total_jasa_11 	+= $sub_jasa_11;
					$total_jasa_12 	+= $sub_jasa_12;
					// $baris += 1;
					$baris = ($baris+1)+$no_body;
					// $phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':L'.$baris);
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "Grand Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_jasa_1);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_jasa_2);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_jasa_3);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_jasa_4);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jasa_5);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jasa_6);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_jasa_7);
				$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $total_jasa_8);
				$phpExcel->getActiveSheet()->setCellValue('K'.$baris, $total_jasa_9);
				$phpExcel->getActiveSheet()->setCellValue('L'.$baris, $total_jasa_10);
				$phpExcel->getActiveSheet()->setCellValue('M'.$baris, $total_jasa_11);
				$phpExcel->getActiveSheet()->setCellValue('N'.$baris, $total_jasa_12);
				$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
				$phpExcel->getActiveSheet()
							->getStyle('E'.$baris.':N'.$baris)
							->getNumberFormat()
							->setFormatCode($numberFormat);
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':N'.$baris);
				$phpExcel->getActiveSheet()->getStyle('A'.$baris.':N'.$baris)->applyFromArray(
				 array(
				 'font' => array(
				 	'bold' => true
				 	),
				 )
				);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.$file.'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$table = "";
				$table .= "<table width='100%' border='0' cellspacing='0'>";
				$table .= "<tr>";
				$table .= "<th>Penerimaan Penerimaan Tunai Per Komponen - Summary</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th>Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y')."</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th>Customer : ".$label_customer."</th>";
				$table .= "</tr>";
				$table .= "</table>";
				$table .= "<table width='100%' border='1' cellspacing='0'>";
				$table .= "<tr>";
				$table .= "<th width='10'>No</th>";
				$table .= "<th width='250'>Tindakan</th>";
				$table .= "<th width='25'>Jml Pasien</th>";
				$table .= "<th width='25'>Jml Produk</th>";
				$table .= "<th width='25'>Jasa Dokter</th>";
				$table .= "<th width='25'>Jasa Perawat/Bidan</th>";
				$table .= "<th width='25'>Tindakan tdk langsung</th>";
				$table .= "<th width='25'>Operator</th>";
				$table .= "<th width='25'>Ops. RS</th>";
				$table .= "<th width='25'>Ops. Direksi</th>";
				$table .= "<th width='25'>BHP</th>";
				$table .= "<th width='25'>Jasa Sarana</th>";
				$table .= "<th width='25'>Jasa Pelayanan</th>";
				$table .= "<th width='25'>Total</th>";
				$table .= "</tr>";
				$no = 1;

				for ($i=0; $i < count($array_header); $i++) { 
					// $nama_pasien = $this->db->query("SELECT nama FROM pasien where kd_pasien = '".$array_header[$i]."'")->row()->nama;
					$table .= "<tr>";
					$table .= "<td colspan='12' style='padding-left:10px;'><b>".($i+1)." - ".$array_header[$i]."</b></td>";
					$table .= "</tr>";
					$no_body = 1;

					$sub_jasa_1  = 0;
					$sub_jasa_2  = 0;
					$sub_jasa_3  = 0;
					$sub_jasa_4  = 0;
					$sub_jasa_5  = 0;
					$sub_jasa_6  = 0;
					$sub_jasa_7  = 0;
					$sub_jasa_8  = 0;
					$sub_jasa_9  = 0;
					$sub_jasa_10 = 0;
					$sub_jasa_11 = 0;
					$sub_jasa_12 = 0;
					foreach ($result_query->result() as $result) {
						if ($result->nama_unit == $array_header[$i]) {
							$table .= "<tr>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='center'>".$no_body."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;'>".$result->deskripsi."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->jml_pasien."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->qty."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c20."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c21."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c22."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c23."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c24."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c25."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c26."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c30."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->jpl."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->jumlah."</td>";
							$table .= "</tr>";

							$sub_jasa_1  += $result->jml_pasien;
							$sub_jasa_2  += $result->qty;
							$sub_jasa_3  += $result->c20;
							$sub_jasa_4  += $result->c21;
							$sub_jasa_5  += $result->c22;
							$sub_jasa_6  += $result->c23;
							$sub_jasa_7  += $result->c24;
							$sub_jasa_8  += $result->c25;
							$sub_jasa_9  += $result->c26;
							$sub_jasa_10 += $result->c30;
							$sub_jasa_11 += $result->jpl;
							$sub_jasa_12 += $result->jumlah;
							$no_body++;
						}
					}
					$table .= "<tr>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='left' colspan='2'><b>Sub Total</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_1."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_2."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_3."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_4."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_5."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_6."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_7."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_8."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_9."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_10."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_11."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_12."</b></td>";
					$table .= "</tr>";

					$total_jasa_1  	+= $sub_jasa_1;
					$total_jasa_2  	+= $sub_jasa_2;
					$total_jasa_3  	+= $sub_jasa_3;
					$total_jasa_4  	+= $sub_jasa_4;
					$total_jasa_5  	+= $sub_jasa_5;
					$total_jasa_6  	+= $sub_jasa_6;
					$total_jasa_7  	+= $sub_jasa_7;
					$total_jasa_8  	+= $sub_jasa_8;
					$total_jasa_9  	+= $sub_jasa_9;
					$total_jasa_10 	+= $sub_jasa_10;
					$total_jasa_11 	+= $sub_jasa_11;
					$total_jasa_12 	+= $sub_jasa_12;
				}
				$table .= "<tr>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='left' colspan='2'><b>Grand Total</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_1."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_2."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_3."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_4."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_5."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_6."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_7."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_8."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_9."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_10."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_11."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_12."</b></td>";
				$table .= "</tr>";
				$table .= "</table>";
				$this->common->setPdf('L',$file,$table);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}
	/*
	
		PENAMBAHAN LAPORAN BARU 
		OLEH HADAD AL GOJALI 
		2017 - 11 - 22
		*Harus tersedia template di Server
	 */
	public function laporan_perhitungan_jasa_tindakan_dokter(){
		$common       = $this->common;
		$result       = $this->result;
		$param 	= json_decode($this->input->post('data'));
		$params = array(
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'shift_1' 		=> $param->shift1,
			'shift_2' 		=> $param->shift2,
			'shift_3' 		=> $param->shift3,
			'pendaftaran' 	=> false,
			'tindakan' 		=> false,
			'transfer' 		=> false,
			'arr_kd_unit'	=> $param->tmp_kd_unit,
			'arr_kd_pay'	=> $param->tmp_payment,
			'type_file'		=> $param->type_file,
		);

		$criteria_shift = "";

		if ($params['shift_1'] === true || $params['shift_1'] == 'true') {
			$criteria_shift .= " 1, ";
		}

		if ($params['shift_2'] === true || $params['shift_2'] == 'true') {
			$criteria_shift .= " 2, ";
		}

		if ($params['shift_3'] === true || $params['shift_3'] == 'true') {
			$criteria_shift .= " 3, ";
		}

		$criteria_shift = substr($criteria_shift, 0, strlen($criteria_shift)-2);

		$criteria_tindakan = "";
		if ($params['pendaftaran'] === true || $params['pendaftaran'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas = '1' ";
		}

		if ($params['transfer'] === true || $params['transfer'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas = '9' ";
		}

		if ($params['tindakan'] === true || $params['tindakan'] == 'true') {
			if (strlen($criteria_tindakan) > 0) {
				$criteria_tindakan .= " OR ";	
			}
			$criteria_tindakan .= " p.kd_klas <> '1' and p.kd_klas <> '9' ";
		}

		$query = "SELECT 
			u.nama_unit, 
			x.tgl_transaksi, 
			p.deskripsi,  
			max(dt.harga) as tarif,  
			dt.qty, 
			sum(x.jumlah) as jumlah,  
			dtx.kd_dokter, 
			dtx.nama as nama_dokter, 
			ps.kd_pasien, 
			ps.nama as nama_pasien, 
			x.no_transaksi, 
			sum(cx) as cx,
			sum(c20) as c20,
			sum(c21) as c21,
			sum(c22) as c22,
			sum(c23) as c23,
			sum(c24) as c24,
			sum(c25) as c25,
			sum(c30) as c30
		 from 
			kunjungan k 
			inner join transaksi t   on t.kd_pasien=k.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk  
			inner join detail_transaksi dt on  dt.no_transaksi = t.no_transaksi and dt.kd_kasir = t.kd_kasir and dt.kd_kasir ='06' and dt.folio in ('A','E') 
			left join (
				select 
					dtd.kd_kasir,
					dtd.no_transaksi,
					dtd.urut, 
					dtd.tgl_transaksi, 
					d.kd_dokter, 
					d.nama, 
					d.jenis_dokter           
				from detail_trdokter dtd 
			inner join dokter d on d.kd_dokter = dtd.kd_dokter) dtx on dtx.no_transaksi = dt.no_transaksi and dtx.kd_kasir = dt.kd_kasir and dtx.urut = dt.urut and dtx.tgl_transaksi = dt.tgl_transaksi 
			inner join (
				select 
					dtbc.kd_kasir, 
					dtbc.no_transaksi, 
					dtbc.urut, 
					dtbc.tgl_transaksi, 
					sum(case when dtbc.kd_component =20 then dtbc.jumlah * dt.qty else 0 end) as c20,
					sum(case when dtbc.kd_component =21 then dtbc.jumlah * dt.qty else 0 end) as c21,
					sum(case when dtbc.kd_component =22 then dtbc.jumlah * dt.qty else 0 end) as c22,
					sum(case when dtbc.kd_component =23 then dtbc.jumlah * dt.qty else 0 end) as c23,
					sum(case when dtbc.kd_component =24 then dtbc.jumlah * dt.qty else 0 end) as c24,
					sum(case when dtbc.kd_component =25 then dtbc.jumlah * dt.qty else 0 end) as c25,
					sum(case when dtbc.kd_component =30 then dtbc.jumlah * dt.qty else 0 end) as c30,
					sum(case when dtbc.kd_component in (20, 21, 22, 23, 24, 25) then dtbc.jumlah * dt.qty else 0 end) as cx,  
					sum(dtbc.jumlah * dt.qty) as jumlah 
				from detail_tr_bayar dtb 
				inner join detail_tr_bayar_component dtbc  on dtb.kd_kasir = dtbc.kd_kasir and dtb.no_transaksi = dtbc.no_transaksi and dtb.urut = dtbc.urut    and dtb.tgl_transaksi = dtbc.tgl_transaksi and dtb.kd_pay = dtbc.kd_pay    and dtb.urut_bayar = dtbc.urut_bayar and dtb.tgl_bayar = dtbc.tgl_bayar 
				inner join detail_bayar db on dtb.kd_kasir = db.kd_kasir    and dtb.no_transaksi = db.no_transaksi    and dtb.urut_bayar = db.urut       and dtb.tgl_bayar = db.tgl_transaksi 
				inner join produk_component pc on dtbc.kd_component = pc.kd_component
				inner join detail_transaksi dt on dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi =dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
				where 
				dtb.kd_kasir= '06' 
				and dtb.kd_pay in (".$params['arr_kd_pay'].")       
				and ((dtb.tgl_transaksi between '".$params['start_date']."'  and '".$params['last_date']."')     
				and db.shift in (".$criteria_shift.") or (dtb.tgl_transaksi between '".$params['start_date']."' and '".$params['last_date']."' and db.shift=4) )
				group by dtbc.kd_kasir, dtbc.no_transaksi, dtbc.urut, dtbc.tgl_transaksi 
			) x on x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi and x.urut = dt.urut   and x.tgl_transaksi = dt.tgl_transaksi   and  x.kd_kasir = t.kd_kasir and x.no_transaksi = t.no_transaksi 
			left join unit u on u.kd_unit=t.kd_unit 
			left join produk p on p.kd_produk= dt.kd_produk 
			left join kontraktor ktr on ktr.kd_customer=k.kd_customer 
			left join pasien ps on ps.kd_pasien=t.kd_pasien  
		where t.kd_unit in (".$params['arr_kd_unit'].") 
		group by u.nama_unit, x.tgl_transaksi, ps.kd_pasien, ps.nama, p.deskripsi,  k.kd_pasien, dtx.kd_dokter, dtx.nama, x.no_transaksi,dt.qty order by u.nama_unit, ps.nama, p.deskripsi ";

		$result_query = $this->db->query($query);

		$kode_pasien = array();
		foreach ($result_query->result() as $result) {
			$kode_pasien[] = $result->kd_pasien;
		}

		$tmp_header   = implode('#', array_unique($kode_pasien));
		$array_header = explode("#", $tmp_header);
		$total_jasa_1  	= 0;
		$total_jasa_2  	= 0;
		$total_jasa_3  	= 0;
		$total_jasa_4  	= 0;
		$total_jasa_5  	= 0;
		$total_jasa_6  	= 0;
		$total_jasa_7  	= 0;
		$total_jasa_8  	= 0;
		$total_jasa_9  	= 0;
		$total_jasa_10 	= 0;
		$total_jasa_11 	= 0;
		$total_jasa_12	= 0;

		if ($result_query->num_rows() > 0) {
			if($params['type_file'] == 1){
				$url = "Doc Asset/format_laporan/Laporan Perhitungan Jasa Tindakan Dokter (IGD).xls";

				$numberFormat='#,##0';//'General','0.00','@';
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();

				$sharedStyle = new PHPExcel_Style();
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$baris = 6;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y'));

				for ($i=0; $i < count($array_header); $i++) { 
					$nama_pasien = $this->db->query("SELECT nama FROM pasien where kd_pasien = '".$array_header[$i]."'")->row()->nama;
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, ($i+1)." - ".$array_header[$i]." ".$nama_pasien);
					$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
					$phpExcel->getActiveSheet()
								->getStyle('A'.$baris.':B'.$baris)
								->getAlignment()
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

					$phpExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(
						array(
						'font' => array(
							'bold' => true
							),
						)
					);

					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':M'.$baris);
					$no_body = 1;
					$sub_jasa_1  = 0;
					$sub_jasa_2  = 0;
					$sub_jasa_3  = 0;
					$sub_jasa_4  = 0;
					$sub_jasa_5  = 0;
					$sub_jasa_6  = 0;
					$sub_jasa_7  = 0;
					$sub_jasa_8  = 0;
					$sub_jasa_9  = 0;
					$sub_jasa_10 = 0;
					$sub_jasa_11 = 0;
					$sub_jasa_12 = 0;
					foreach ($result_query->result() as $result) {
						if ($result->kd_pasien == $array_header[$i]) {
							$phpExcel->getActiveSheet()->setCellValue('A'.((int)$baris+(int)$no_body), $no_body);
							$phpExcel->getActiveSheet()->setCellValue('B'.((int)$baris+(int)$no_body), $result->deskripsi);
							$phpExcel->getActiveSheet()->setCellValue('C'.((int)$baris+(int)$no_body), $result->tarif);
							$phpExcel->getActiveSheet()->setCellValue('D'.((int)$baris+(int)$no_body), $result->qty);
							$phpExcel->getActiveSheet()->setCellValue('E'.((int)$baris+(int)$no_body), $result->jumlah);
							$phpExcel->getActiveSheet()->setCellValue('F'.((int)$baris+(int)$no_body), $result->c20);
							$phpExcel->getActiveSheet()->setCellValue('G'.((int)$baris+(int)$no_body), $result->c21);
							$phpExcel->getActiveSheet()->setCellValue('H'.((int)$baris+(int)$no_body), $result->c22);
							$phpExcel->getActiveSheet()->setCellValue('I'.((int)$baris+(int)$no_body), $result->c23);
							$phpExcel->getActiveSheet()->setCellValue('J'.((int)$baris+(int)$no_body), $result->c24);
							$phpExcel->getActiveSheet()->setCellValue('K'.((int)$baris+(int)$no_body), $result->c25);
							$phpExcel->getActiveSheet()->setCellValue('L'.((int)$baris+(int)$no_body), $result->c30);
							$phpExcel->getActiveSheet()->setCellValue('M'.((int)$baris+(int)$no_body), $result->cx);


							$sub_jasa_1  += $result->tarif;
							$sub_jasa_2  += $result->qty;
							$sub_jasa_3  += $result->jumlah;
							$sub_jasa_4  += $result->c20;
							$sub_jasa_5  += $result->c21;
							$sub_jasa_6  += $result->c22;
							$sub_jasa_7  += $result->c23;
							$sub_jasa_8  += $result->c24;
							$sub_jasa_9  += $result->c25;
							$sub_jasa_10 += $result->c30;
							$sub_jasa_11 += $result->cx;
							// $sub_jasa_12 += $result->jumlah;
							/*$phpExcel->getActiveSheet()
										->getStyle('C'.((int)$baris+(int)$no_body).':M'.((int)$baris+(int)$no_body))
										->getNumberFormat()
										->setFormatCode($numberFormat);*/
							$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.((int)$baris+(int)$no_body).':M'.((int)$baris+(int)$no_body));
							$no_body++;
						}
					}
					$phpExcel->getActiveSheet()->setCellValue('A'.((int)$baris+(int)$no_body), "Sub Total");
					$phpExcel->getActiveSheet()->setCellValue('C'.((int)$baris+(int)$no_body), $sub_jasa_1);
					$phpExcel->getActiveSheet()->setCellValue('D'.((int)$baris+(int)$no_body), $sub_jasa_2);
					$phpExcel->getActiveSheet()->setCellValue('E'.((int)$baris+(int)$no_body), $sub_jasa_3);
					$phpExcel->getActiveSheet()->setCellValue('F'.((int)$baris+(int)$no_body), $sub_jasa_4);
					$phpExcel->getActiveSheet()->setCellValue('G'.((int)$baris+(int)$no_body), $sub_jasa_5);
					$phpExcel->getActiveSheet()->setCellValue('H'.((int)$baris+(int)$no_body), $sub_jasa_6);
					$phpExcel->getActiveSheet()->setCellValue('I'.((int)$baris+(int)$no_body), $sub_jasa_7);
					$phpExcel->getActiveSheet()->setCellValue('J'.((int)$baris+(int)$no_body), $sub_jasa_8);
					$phpExcel->getActiveSheet()->setCellValue('K'.((int)$baris+(int)$no_body), $sub_jasa_9);
					$phpExcel->getActiveSheet()->setCellValue('L'.((int)$baris+(int)$no_body), $sub_jasa_10);
					$phpExcel->getActiveSheet()->setCellValue('M'.((int)$baris+(int)$no_body), $sub_jasa_11);
					// $phpExcel->getActiveSheet()->setCellValue('N'.((int)$baris+(int)$no_body), $sub_jasa_12);
					$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
					/*$phpExcel->getActiveSheet()
								->getStyle('E'.((int)$baris+(int)$no_body).':M'.((int)$baris+(int)$no_body))
								->getNumberFormat()
								->setFormatCode($numberFormat);*/
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.((int)$baris+(int)$no_body).':M'.((int)$baris+(int)$no_body));
					$phpExcel->getActiveSheet()->getStyle('A'.((int)$baris+(int)$no_body).':M'.((int)$baris+(int)$no_body))->applyFromArray(
					 array(
					 'font' => array(
					 	'bold' => true
					 	),
					 )
					);

					$total_jasa_1  	+= $sub_jasa_1;
					$total_jasa_2  	+= $sub_jasa_2;
					$total_jasa_3  	+= $sub_jasa_3;
					$total_jasa_4  	+= $sub_jasa_4;
					$total_jasa_5  	+= $sub_jasa_5;
					$total_jasa_6  	+= $sub_jasa_6;
					$total_jasa_7  	+= $sub_jasa_7;
					$total_jasa_8  	+= $sub_jasa_8;
					$total_jasa_9  	+= $sub_jasa_9;
					$total_jasa_10 	+= $sub_jasa_10;
					$total_jasa_11 	+= $sub_jasa_11;
					// $total_jasa_12 	+= $sub_jasa_12;
					
					$baris += 1;
					$baris = ($baris)+$no_body;
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':M'.$baris);
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "Grand Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_jasa_1);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_jasa_2);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_jasa_3);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_jasa_4);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jasa_5);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jasa_6);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_jasa_7);
				$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $total_jasa_8);
				$phpExcel->getActiveSheet()->setCellValue('K'.$baris, $total_jasa_9);
				$phpExcel->getActiveSheet()->setCellValue('L'.$baris, $total_jasa_10);
				$phpExcel->getActiveSheet()->setCellValue('M'.$baris, $total_jasa_11);
				// $phpExcel->getActiveSheet()->setCellValue('N'.$baris, $total_jasa_12);
				$phpExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
				/*$phpExcel->getActiveSheet()
							->getStyle('E'.$baris.':N'.$baris)
							->getNumberFormat()
							->setFormatCode($numberFormat);*/
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':M'.$baris);
				$phpExcel->getActiveSheet()->getStyle('A'.$baris.':M'.$baris)->applyFromArray(
				 array(
				 'font' => array(
				 	'bold' => true
				 	),
				 )
				);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename=Penerimaan tunai per komponen - Summary (IGD).xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$table = "";
				$table .= "<table width='100%' border='0' cellspacing='0'>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Laporan Perhitungan Jasa Tindakan Dokter</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y')."</th>";
				$table .= "</tr>";
				$table .= "</br>";
				$table .= "</table>";
				$table .= "<table width='100%' border='1' cellspacing='0'>";
				$table .= "<tr>";
				$table .= "<th width='10'>No</th>";
				$table .= "<th width='250'>Tindakan</th>";
				$table .= "<th width='25'>Tarif</th>";
				$table .= "<th width='25'>Jml</th>";
				$table .= "<th width='25'>Total</th>";
				$table .= "<th width='25'>Operator</th>";
				$table .= "<th width='25'>Jasa Dokter</th>";
				$table .= "<th width='25'>Jasa Perawat/Bidan</th>";
				$table .= "<th width='25'>Indeks tdk langsung</th>";
				$table .= "<th width='25'>Ops. Installasi</th>";
				$table .= "<th width='25'>Ops. RS</th>";
				$table .= "<th width='25'>Ops. Direksi</th>";
				$table .= "<th width='25'>Total</th>";
				$table .= "</tr>";
				$no = 1;

				for ($i=0; $i < count($array_header); $i++) { 
					$nama_pasien = $this->db->query("SELECT nama FROM pasien where kd_pasien = '".$array_header[$i]."'")->row()->nama;
					$table .= "<tr>";
					$table .= "<td colspan='13' style='padding-left:10px;'><b>".($i+1)." - ".$array_header[$i]." ".$nama_pasien."</b></td>";
					$table .= "</tr>";
					$no_body = 1;

					$sub_jasa_1  = 0;
					$sub_jasa_2  = 0;
					$sub_jasa_3  = 0;
					$sub_jasa_4  = 0;
					$sub_jasa_5  = 0;
					$sub_jasa_6  = 0;
					$sub_jasa_7  = 0;
					$sub_jasa_8  = 0;
					$sub_jasa_9  = 0;
					$sub_jasa_10 = 0;
					$sub_jasa_11 = 0;
					$sub_jasa_12 = 0;
					foreach ($result_query->result() as $result) {
						if ($result->kd_pasien == $array_header[$i]) {
							$table .= "<tr>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='center'>".$no_body."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;'>".$result->deskripsi."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->tarif."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->qty."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->jumlah."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c20."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c21."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c22."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c23."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c24."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c25."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->c30."</td>";
							$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'>".$result->cx."</td>";
							$table .= "</tr>";

							$sub_jasa_1  += $result->tarif;
							$sub_jasa_2  += $result->qty;
							$sub_jasa_3  += $result->jumlah;
							$sub_jasa_4  += $result->c20;
							$sub_jasa_5  += $result->c21;
							$sub_jasa_6  += $result->c22;
							$sub_jasa_7  += $result->c23;
							$sub_jasa_8  += $result->c24;
							$sub_jasa_9  += $result->c25;
							$sub_jasa_10 += $result->c30;
							$sub_jasa_11 += $result->cx;
							// $sub_jasa_12 += $result->jumlah;
							$no_body++;
						}
					}
					$table .= "<tr>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='left' colspan='2'><b>Sub Total</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_1."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_2."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_3."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_4."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_5."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_6."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_7."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_8."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_9."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_10."</b></td>";
					$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_11."</b></td>";
					// $table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$sub_jasa_12."</b></td>";
					$table .= "</tr>";

					$total_jasa_1  	+= $sub_jasa_1;
					$total_jasa_2  	+= $sub_jasa_2;
					$total_jasa_3  	+= $sub_jasa_3;
					$total_jasa_4  	+= $sub_jasa_4;
					$total_jasa_5  	+= $sub_jasa_5;
					$total_jasa_6  	+= $sub_jasa_6;
					$total_jasa_7  	+= $sub_jasa_7;
					$total_jasa_8  	+= $sub_jasa_8;
					$total_jasa_9  	+= $sub_jasa_9;
					$total_jasa_10 	+= $sub_jasa_10;
					$total_jasa_11 	+= $sub_jasa_11;
					// $total_jasa_12 	+= $sub_jasa_12;
				}
				$table .= "<tr>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='left' colspan='2'><b>Grand Total</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_1."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_2."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_3."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_4."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_5."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_6."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_7."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_8."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_9."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_10."</b></td>";
				$table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_11."</b></td>";
				// $table .= "<td style='padding-left:5px;padding-right:5px;' align='right'><b>".$total_jasa_12."</b></td>";
				$table .= "</tr>";
				$table .= "</table>";
				$this->common->setPdf('P','Laporan Perhitungan Jasa Tindakan Dokter (IGD)',$table);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}

	/*
	
		PENAMBAHAN LAPORAN BARU 
		OLEH HADAD AL GOJALI 
		2017 - 11 - 23
		* Harus tersedia template di Server
	 */
	public function laporan_batal_transaksi(){
		$param 	= json_decode($this->input->post('data'));
		$params = array(
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'type_file'		=> $param->type_file,
			'order_by'		=> $param->order_by,
		);

		$criteria_order = "";
		if ($params['order_by'] == 'Medrec') {
			$criteria_order = "kd_pasien";
		}else{
			$criteria_order = "nama";
		}
		$file = "Laporan Batal Transaksi (IGD)";
		$query = "SELECT 
				kd_kasir, 
				no_transaksi, 
				tgl_transaksi, 
				kd_pasien, 
				nama, 
				kd_unit, 
				nama_unit, 
				kd_user, 
				kd_user_del, 
				shift, 
				shiftdel, 
				jumlah, 
				user_name, 
				tgl_batal, 
				ket,
				cast(jam_batal as time)as jam_batal 
			from 
				history_batal_trans 
			where 
			kd_kasir='06' and 
			Tgl_Batal Between '".$params['start_date']."' and '".$params['last_date']."' 
			ORDER BY $criteria_order ASC";
		$result_query = $this->db->query($query);
		if ($result_query->num_rows() > 0) {
			if($params['type_file'] == 1){
				$url      = "Doc Asset/format_laporan/".$file.".xls";
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();

				$sharedStyle = new PHPExcel_Style();
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$baris 	= 6;
				$no 	= 1;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y'));
				foreach ($result_query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->no_transaksi);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->kd_pasien);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->nama);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->nama_unit);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, date_format(date_create($result->tgl_batal), 'd-M-Y'));
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->jumlah);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->ket);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':H'.$baris);
					$no++;
					$baris++;
				}

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.$file.'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$table = "";
				
				$table .= "<table width='100%'>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Laporan Batal Transaksi</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y')."</th>";
				$table .= "</tr>";
				$table .= "</table>";

				$table .= "<table border='1' cellspacing='0' width='100%'>";
				$table .= "<thead>";
				$table .= "<tr>";
				$table .= "<th>NO</th>";
				$table .= "<th>No Transaksi</th>";
				$table .= "<th>Medrec</th>";
				$table .= "<th>Nama Pasien</th>";
				$table .= "<th>Unit</th>";
				$table .= "<th>Tgl Batal</th>";
				$table .= "<th>Jumlah</th>";
				$table .= "<th>Keterangan</th>";
				$table .= "</tr>";
				$table .= "</thead>";
				
				$table .= "<tbody>";
				$no = 1;
				foreach ($result_query->result() as $result) {
					$table .= "<tr>";
					$table .= "<td align='center'>".$no."</td>";
					$table .= "<td align='center'>".$result->no_transaksi."</td>";
					$table .= "<td align='center'>".$result->kd_pasien."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->nama."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->nama_unit."</td>";
					$table .= "<td align='right'>".date_format(date_create($result->tgl_batal), 'd-M-Y')."</td>";
					$table .= "<td align='right'>".$result->jumlah."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->ket."</td>";
					$table .= "</tr>";
					$no++;
				}
				$table .= "</tbody>";
				$table .= "</table>";
				$this->common->setPdf('P',$file,$table);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}
	/*
	
		PENAMBAHAN LAPORAN BARU 
		OLEH HADAD AL GOJALI 
		2017 - 11 - 23
		* Harus tersedia template di Server
	*/
	public function laporan_batal_kunjungan(){
		$param 	= json_decode($this->input->post('data'));
		$params = array(
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'type_file'		=> $param->type_file,
			// 'order_by'		=> $param->order_by,
			'tmp_kd_unit'	=> $param->tmp_kd_unit,
		);

		// $criteria_order = "";
		// if ($params['order_by'] == 'Medrec') {
		// 	$criteria_order = "kd_pasien";
		// }else{
		// 	$criteria_order = "nama";
		// }
		// 
		// 
		
		if (strtolower($param->tmp_kd_unit) != 'semua' && $param->tmp_kd_unit != '') {
			// $params['tmp_unit'] = substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			$criteria_unit = "AND kd_unit in (".$params['tmp_kd_unit'].")";
		}else{
			$criteria_unit = "";
		}

		$file = "Laporan Batal Kunjungan (IGD)";
		$query = "SELECT * FROM history_batal_kunjungan
			where 
			Tgl_Batal Between '".$params['start_date']."' and '".$params['last_date']."' $criteria_unit order by tgl_kunjungan ";
		$result_query = $this->db->query($query);
		if ($result_query->num_rows() > 0) {
			if($params['type_file'] == 1){
				$url      = "Doc Asset/format_laporan/".$file.".xls";
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();

				$sharedStyle = new PHPExcel_Style();
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$baris 	= 7;
				$no 	= 1;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y'));
				foreach ($result_query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->kd_pasien);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->nama);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->nama_unit);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, date_format(date_create($result->tgl_kunjungan), 'd-M-Y'));
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, date_format(date_create($result->tgl_batal), 'd-M-Y'));
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->username);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->ket);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':H'.$baris);
					$no++;
					$baris++;
				}

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.$file.'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$table = "";
				
				$table .= "<table width='100%'>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Laporan Batal Transaksi</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y')."</th>";
				$table .= "</tr>";
				$table .= "</table>";

				$table .= "<table border='1' cellspacing='0' width='100%'>";
				$table .= "<thead>";
				$table .= "<tr>";
					$table .= "<th rowspan='2'>NO</th>";
					$table .= "<th colspan='3'>Pasien</th>";
					$table .= "<th colspan='2'>Kunjungan</th>";
					$table .= "<th rowspan='2'>Petugas</th>";
					$table .= "<th rowspan='2'>Keterangan</th>";
				$table .= "</tr>";
				$table .= "<tr>";
					$table .= "<th width='75'>No Medrec</th>";
					$table .= "<th>Nama Pasien</th>";
					$table .= "<th width='75'>Nama Unit</th>";
					$table .= "<th width='75'>Tgl Kunjungan</th>";
					$table .= "<th width='75'>Tgl Batal</th>";
				$table .= "</tr>";
				$table .= "</thead>";
				
				$table .= "<tbody>";
				$no = 1;
				foreach ($result_query->result() as $result) {
					$table .= "<tr>";
					$table .= "<td align='center'>".$no."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->kd_pasien."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->nama."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->nama_unit."</td>";
					$table .= "<td style='padding-left:5px;'>".date_format(date_create($result->tgl_kunjungan), 'd-M-Y')."</td>";
					$table .= "<td style='padding-left:5px;'>".date_format(date_create($result->tgl_batal), 'd-M-Y')."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->username."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->ket."</td>";
					$table .= "</tr>";
					$no++;
				}
				$table .= "</tbody>";
				$table .= "</table>";
				$this->common->setPdf('P',$file,$table);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}

	/*
	
		PENAMBAHAN LAPORAN BARU 
		OLEH HADAD AL GOJALI 
		2017 - 11 - 23
		* Harus tersedia template di Server
	*/
	public function laporan_batal_pembayaran(){
		$param 	= json_decode($this->input->post('data'));
		$params = array(
			'start_date' 	=> $param->start_date,
			'last_date' 	=> $param->last_date,
			'type_file'		=> $param->type_file,
			// 'order_by'		=> $param->order_by,
			// 'tmp_kd_unit'	=> $param->tmp_kd_unit,
		);

		// $criteria_order = "";
		// if ($params['order_by'] == 'Medrec') {
		// 	$criteria_order = "kd_pasien";
		// }else{
		// 	$criteria_order = "nama";
		// }
		// 
		// 
		
		/*if (strtolower($param->tmp_kd_unit) != 'semua' && $param->tmp_kd_unit != '') {
			// $params['tmp_unit'] = substr($param->tmp_kd_unit, 0, strlen($param->tmp_kd_unit)-1);
			$criteria_unit = "AND kd_unit in (".$params['tmp_kd_unit'].")";
		}else{
			$criteria_unit = "";
		}*/

		$file = "Laporan Batal Pembayaran (IGD)";
		$query = " SELECT * FROM history_detail_bayar WHERE kd_kasir = '06' and tgl_batal between '".$params['start_date']."' and '".$params['last_date']."' ";
		$result_query = $this->db->query($query);
		if ($result_query->num_rows() > 0) {
			if($params['type_file'] == 1){
				$url      = "Doc Asset/format_laporan/".$file.".xls";
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();

				$sharedStyle = new PHPExcel_Style();
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$baris 	= 6;
				$no 	= 1;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y'));
				foreach ($result_query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->no_transaksi);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->kd_pasien);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->nama);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->nama_unit);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result->uraian);
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->user_name);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->jumlah);
					$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $result->ket);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);
					$no++;
					$baris++;
				}

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.$file.'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$table = "";
				
				$table .= "<table width='100%'>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Laporan Batal Transaksi</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th width='100%' align='center'>Periode ".date_format(date_create($params['start_date']), 'd-M-Y')." s/d ".date_format(date_create($params['last_date']), 'd-M-Y')."</th>";
				$table .= "</tr>";
				$table .= "</table>";

				$table .= "<table border='1' cellspacing='0' width='100%'>";
				$table .= "<thead>";
				$table .= "<tr>";
					$table .= "<th>NO</th>";
					$table .= "<th>No. Transaksi</th>";
					$table .= "<th>No. Medrec</th>";
					$table .= "<th>Nama Pasien</th>";
					$table .= "<th>Unit</th>";
					$table .= "<th>Uraian</th>";
					$table .= "<th>Petugas</th>";
					$table .= "<th>Jumlah</th>";
					$table .= "<th>Keterangan</th>";
				$table .= "</tr>";
				$table .= "</thead>";
				
				$table .= "<tbody>";
				$no = 1;
				foreach ($result_query->result() as $result) {
					$table .= "<tr>";
					$table .= "<td align='center'>".$no."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->no_transaksi."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->kd_pasien."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->nama."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->nama_unit."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->uraian."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->user_name."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->jumlah."</td>";
					$table .= "<td style='padding-left:5px;'>".$result->ket."</td>";
					$table .= "</tr>";
					$no++;
				}
				$table .= "</tbody>";
				$table .= "</table>";
				$this->common->setPdf('P',$file,$table);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}

	private function keterangan_tidak_ada_data(){
		echo "<h3>Tidak Ada Data</h3>";
	} 

	private function get_data_customer($kd_customer){
		return $this->db->query("SELECT customer FROM customer where kd_customer = '".$kd_customer."'")->row()->customer;
	} 
}
?>