<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData(){
		
		 $param=json_decode($_POST['data']);
		$html='';
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$td_t_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->setting;
   		$td_t_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->setting;
   		$td_t_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->setting;
   		
   		$td_n_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->setting;
   		$td_n_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->setting;
   		$td_n_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->setting;
   		
   		$td_i_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->setting;
   		$td_i_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->setting;
   		$td_i_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->setting;
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
   		$type_file = $param->type_file;
		$awal = tanggalstring($param->start_date);
		$akhir = tanggalstring($param->last_date);
   		
   		$q_dokter='';
   		if(isset($param->kd_dokter)){
   			$q_dokter=" AND k.kd_dokter='".$param->kd_dokter."' ";
   		}
		
   		$q_jenis='';
   		if($param->pasien!='Semua'){
   			$q_jenis=" AND Ktr.Jenis_cust=".$param->pasien." ";
   		}
		
   		$q_customer='';
		$customer='SEMUA';
   		if(isset($param->kd_customer)){
			if($param->kd_customer!='Semua') {
				$q_customer=" AND Ktr.kd_customer='".$param->kd_customer."' ";
				$customer=$this->db->query("select customer from customer where kd_customer='".$param->kd_customer."'")->row()->customer;
			} else {
				$q_customer="";
				$customer='SEMUA';
			}
   		}
		
        $q_unit='';
        $tmpKdUnit='';
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
        }
        $tmpKdUnit = substr($tmpKdUnit, 0, -1);  
   		if(!empty($param->tmp_kd_unit)){
            $q_unit = " t.Kd_Unit IN (".$tmpKdUnit.")";
   		}

		$q_payment = '';
		$tmpKdPay='';
		$arrayDataPay = $param->tmp_payment;
        for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
        }
        $tmpKdPay = substr($tmpKdPay, 0, -1);  
        if (!empty($param->tmp_payment)) {
            $q_payment = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
        }
		
   		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
            $q_shift=" ((dtb.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))     
         Or  (dtb.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) ) ";
   			$t_shift='SHIFT (1,2,3)';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.=" dtb.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dtb.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
				$t_shift ='SHIFT ('.$s_shift.')';
				$q_shift =$q_shift;
   			}
   		}

		$q_tindakan = "";
		$tindakan="";
		if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas not in('9','1')";
			$tindakan="TINDAKAN";
		}else if($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==true){
			$q_tindakan .= " AND kd_Klas in('9','1')";
			$tindakan="PENDAFTARAN DAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas = '1' ";
			$tindakan="PENDAFTARAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==false && $param->tindakan2==true) {
			$q_tindakan .= " AND kd_Klas = '9' ";
			$tindakan="TINDAKAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND kd_Klas not in('9')  ";
			$tindakan="PENDAFTARAN DAN TINDAKAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==true) {
			$q_tindakan .= " AND kd_Klas not in('1')  ";
			$tindakan="TINDAKAN DAN TRANSFER";
		}else{
			$q_tindakan .= "  ";
		}

		$result   = $this->db->query("Select distinct(k.kd_unit), U.Nama_Unit
										From Transaksi t
											INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
											INNER JOIN 
												(
												SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
												FROM Detail_Bayar db 
													INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
														and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
												WHERE  ".$q_shift.$q_payment."
													GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
												) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
											INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
										WHERE ".$q_unit." AND t.IsPay = 't'
											AND t.Kd_Kasir = '".$KdKasir."'
											".$q_customer.$q_tindakan."
										GROUP BY k.kd_unit,U.Nama_Unit
										ORDER BY U.Nama_Unit
									")->result();
		$html.='
			<table class="t2"  cellspacing="0" border="0" >
				
					<tr>
						<th colspan="7" align="center">LAPORAN PENERIMAAN '.$tindakan.' (Per Pasien)</th>
					</tr>
					<tr>
						<th colspan="7" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="7" align="center">KELOMPOK PASIEN ('.$customer.')</th>
					</tr>
					<tr>
						<th colspan="7" align="center">'.$t_shift.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					<tr>
						<th width='40' align='center'>NO.</th>
						<th width='80'>Unit</th>
						<th width='80'>NO. TRANSAKSI</th>
						<th width='80'>NO. MEDREC</th>
						<th width='250'>NAMA PASIEN</th>
						<th width='100'>JENIS PENERIMAAN</th>
						<th width='100' align='center'>JUMLAH</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='7'>Tidak Ada Data</td>
					</tr>";
		} else{
			$jumlahtotal=0;
			$no=0;
			foreach($result as $line){
				$noo=0;
				$no++;
				$html.="<tr>
						<td align='center'>".$no."</td>
						<td align='left' colspan='6'>".$line->nama_unit."</td>
					</tr>";
				$resultbody   = $this->db->query("SELECT x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, Max(ps.Nama)as Nama, py.Uraian, Sum(x.Jumlah) AS JUMLAH
												From Transaksi t
												INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
												INNER JOIN 
													(
													SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
													FROM Detail_Bayar db 
														INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
															and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
													WHERE  ".$q_shift.$q_payment."
														GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
													) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
												INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
													And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
												INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
											WHERE t.kd_unit='".$line->kd_unit."' AND t.IsPay = 't'
												AND t.Kd_Kasir = '".$KdKasir."'
												".$q_customer.$q_tindakan."
											GROUP BY x.Tgl_Transaksi, U.Nama_Unit, x.No_Transaksi, t.kd_Pasien, py.Uraian, Ps.Nama
											ORDER BY U.Nama_Unit, Ps.Nama, x.no_transaksi, t.kd_pasien, py.uraian, max(x.urut)
										")->result();	
				$jumlah=0;
				foreach($resultbody as $linebody){
					$noo++;
					$html.="<tr>
								<td align='center'></td>
								<td align='center'></td>
								<td align='center'>".$noo.".&nbsp;&nbsp;&nbsp;".$linebody->no_transaksi."</td>
								<td align='center'>".$linebody->kd_pasien."</td>
								<td>".$linebody->nama."</td>
								<td>".$linebody->uraian."</td>
								<td align='right'>".number_format($linebody->jumlah,0,'.',',')."</td>
							</tr>";
					$jumlah += $linebody->jumlah;
				}
				$html.="
					<tr>
						<th align='right' colspan='6' style='font-weight: bold;'>JUMLAH ".$line->nama_unit."</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlah,0,'.',',')."</th>
					</tr>";
				$jumlahtotal += $jumlah;
			}
			$html.="
					<tr>
						<th align='right' colspan='6' style='font-weight: bold;'>JUMLAH &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
					<tr>
						<th align='right' colspan='6' style='font-weight: bold;'>TOTAL &nbsp;</th>
						<th align='right' style='font-weight: bold;'>".number_format($jumlahtotal,0,'.',',')."</th>
					</tr>
				";
				
		}
   			
   				
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PENERIMAAN PER PASIEN PER JENIS PENERIMAAN',$html);
			echo $html;
		}
   	}

      function cekVariableUnit($tmpunit){
         $arrayData = $tmpunit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         $tmpunit = str_replace(strtolower("Array"), "", $tmpunit);
         return $tmpunit;
      }


      function test_cekVariableUnit(){
         $criteria="";
         $tmpunit = "";
         $arrayData = $param->tmp_kd_unit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         echo $tmpunit;
         //return $tmpunit;
      }

      function cekVariablePayment($payment){
         $criteria="";
         $tmpunit = "";
         $arrayData = $payment;
         for ($i=0; $i < count($payment); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         return $tmpunit;
      }
}
?>