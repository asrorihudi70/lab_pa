<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_perhitungan_jasa_tindakan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData(){
		
		 $param=json_decode($_POST['data']);
		$html='';
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$td_t_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->setting;
   		$td_t_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->setting;
   		$td_t_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->setting;
   		
   		$td_n_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->setting;
   		$td_n_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->setting;
   		$td_n_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->setting;
   		
   		$td_i_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->setting;
   		$td_i_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->setting;
   		$td_i_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->setting;
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		
   		$type_file = $param->type_file;
		$awal = tanggalstring($param->start_date);
		$akhir = tanggalstring($param->last_date);
   		
   		$q_dokter='';
   		if(isset($param->kd_dokter)){
			if($param->kd_dokter!='SEMUA') {
				$q_dokter=" AND k.kd_dokter='".$param->kd_dokter."' ";
			}else{
				$q_dokter="";
			}
   			
   		}
		
		$q_user='';
   		if(isset($param->kd_user)){
			if($param->kd_user!='SEMUA') {
				$q_user=" AND dt.kd_user='".$param->kd_user."' ";
			}else{
				$q_user="";
			}
   			
   		}
		
		
   		$q_customer='';
		$customer='SEMUA';
   		if(isset($param->kd_customer)){
			if($param->kd_customer!='Semua') {
				$q_customer=" AND k.kd_customer='".$param->kd_customer."' ";
				$customer=$this->db->query("select customer from customer where kd_customer='".$param->kd_customer."'")->row()->customer;
			} else {
				$q_customer="";
				$customer='SEMUA';
			}
   		}
		
        $q_unit='';
        $tmpKdUnit='';
        $arrayDataUnit = $param->tmp_kd_unit;
        for ($i=0; $i < count($arrayDataUnit); $i++) { 
           $tmpKdUnit .= "'".$arrayDataUnit[$i][0]."',";
        }
        $tmpKdUnit = substr($tmpKdUnit, 0, -1);  
   		if(!empty($param->tmp_kd_unit)){
            $q_unit = " t.Kd_Unit IN (".$tmpKdUnit.")";
   		}

		$q_payment = '';
		$tmpKdPay='';
		$arrayDataPay = $param->tmp_payment;
        for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
        }
        $tmpKdPay = substr($tmpKdPay, 0, -1);  
        if (!empty($param->tmp_payment)) {
            $q_payment = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
        }
		
   		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
            $q_shift=" AND ((dtb.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))     
         Or  (dtb.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) ) ";
   			$t_shift='SHIFT (1,2,3)';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.=" AND dtb.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (dtb.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
				$t_shift ='SHIFT ('.$s_shift.')';
				$q_shift =$q_shift;
   			}
   		}

		$q_tindakan = "";
		$tindakan="";
		if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND prod.kd_Klas not in('9','1')";
			$tindakan="TINDAKAN";
		}else if($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==true){
			$q_tindakan .= " AND prod.kd_Klas in('9','1')";
			$tindakan="PENDAFTARAN DAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==false && $param->tindakan2==false) {
			$q_tindakan .= " AND prod.kd_Klas = '1' ";
			$tindakan="PENDAFTARAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==false && $param->tindakan2==true) {
			$q_tindakan .= " AND prod.kd_Klas = '9' ";
			$tindakan="TINDAKAN TRANSFER";
		}else if ($param->tindakan0 ==true && $param->tindakan1==true && $param->tindakan2==false) {
			$q_tindakan .= " AND prod.kd_Klas not in('9')  ";
			$tindakan="PENDAFTARAN DAN TINDAKAN";
		}else if ($param->tindakan0 ==false && $param->tindakan1==true && $param->tindakan2==true) {
			$q_tindakan .= " AND prod.kd_Klas not in('1')  ";
			$tindakan="TINDAKAN DAN TRANSFER";
		}else{
			$q_tindakan .= "  ";
		}

		/* $result   = $this->db->query("Select distinct(k.kd_unit), U.Nama_Unit
										From Transaksi t
											INNER JOIN Detail_Transaksi dt ON t.kd_kasir = dt.kd_kasir AND t.No_Transaksi = dt.No_Transaksi
											INNER JOIN 
												(
												SELECT dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah 
												FROM Detail_Bayar db 
													INNER JOIN Detail_TR_Bayar dtb ON dtb.kd_kasir = db.kd_kasir and dtb.no_transaksi = db.no_transaksi
														and dtb.urut_bayar = db.urut and dtb.tgl_bayar = db.tgl_transaksi and dtb.kd_pay = db.kd_pay
												WHERE  ".$q_shift.$q_payment."
													GROUP BY dtb.Kd_Kasir, dtb.No_Transaksi, dtb.Urut, dtb.tgl_transaksi, dtb.kd_Pay, dtb.Jumlah
												) X ON dt.Kd_Kasir = X.Kd_Kasir AND dt.No_transaksi = X.No_Transaksi and dt.Urut = X.Urut
											INNER JOIN  Payment py On py.Kd_pay = x.Kd_Pay 
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit
												And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi 
											INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien 
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
											INNER JOIN Unit U ON K.Kd_Unit = U.Kd_Unit
											INNER JOIN Produk Pr ON dt.Kd_Produk = Pr.Kd_Produk
										WHERE ".$q_unit." AND t.IsPay = 't'
											AND t.Kd_Kasir = '".$KdKasir."'
											".$q_customer.$q_tindakan."
										GROUP BY k.kd_unit,U.Nama_Unit
										ORDER BY U.Nama_Unit
									")->result(); */
			$result   = $this->db->query("SELECT U.Nama_Unit, x.Tgl_Transaksi, p.Deskripsi,   Max(dt.Harga) as Tarif,  dt.Qty, 
											 COALESCE(SUM(x.Jumlah),0) as JUMLAH ,  dtx.Kd_Dokter, dtx.Nama AS Nama_Dokter, Ps.Kd_Pasien, Ps.Nama AS Nama_Pasien, x.No_Transaksi , 
											 SUM(CX) as CX, Sum(C20) as C20, Sum(C21) as C21, Sum(C22) as C22, Sum(C23) as C23, Sum(C24)as C24, Sum(C25) as C25, Sum(C30) as C30
										 FROM Kunjungan k INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
										 inner join detail_transaksi dt on  dt.No_Transaksi = t.No_Transaksi And dt.Kd_Kasir = t.Kd_Kasir and dt.Kd_kasir ='06' And dt.Folio in ('A','E') 
										 inner join produk prod on prod.kd_produk = dt.kd_produk
										 left join (select dtd.kd_kasir,dtd.No_transaksi,dtd.Urut, Dtd.Tgl_Transaksi , d.KD_Dokter, d.Nama, d.Jenis_Dokter           
										 from Detail_trDokter Dtd inner JOIN Dokter d ON d.Kd_Dokter = dtd.KD_Dokter) dtx on dtx.No_transaksi = dt.No_transaksi And dtx.Kd_Kasir = dt.Kd_Kasir And dtx.Urut = dt.Urut And dtx.Tgl_Transaksi = dt.Tgl_Transaksi INNER JOIN (SELECT dtbc.kd_kasir, dtbc.no_transaksi, dtbc.urut, dtbc.tgl_transaksi,  SUM(CASE WHEN dtbc.kd_component =20 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as C20, SUM(CASE WHEN dtbc.kd_component =21 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as C21,  SUM(CASE WHEN dtbc.kd_component =22 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as C22, SUM(CASE WHEN dtbc.kd_component =23 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as C23, SUM(CASE WHEN dtbc.kd_component =24 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as C24, SUM(CASE WHEN dtbc.kd_component =25 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as C25, SUM(CASE WHEN dtbc.kd_component =30 THEN dtbc.Jumlah * dt.qty ELSE 0 END) as  C30,  SUM(CASE WHEN dtbc.Kd_Component in (20, 21, 22, 23, 24, 25) Then dtbc.Jumlah * dt.qty ELSE 0 END) as CX,  Sum(dtbc.Jumlah * dt.qty) as Jumlah
										 FROM DETAIL_TR_BAYAR dtb Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc  ON dtb.KD_KASIR = dtbc.KD_KASIR AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT    AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY    AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR    AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI    AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
										 INNER JOIN Detail_Transaksi dt on dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi =dtb.no_transaksi and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi 
										WHERE dtb.kd_kasir= '".$KdKasir."' ".$q_payment."   ".$q_shift."       
										GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI ) x ON x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi and x.urut = dt.urut   and x.tgl_transaksi = dt.tgl_transaksi   and  X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi INNER JOIN Unit u On u.kd_unit=t.kd_unit INNER JOIN Produk p on p.kd_produk= dt.kd_produk LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien  
										WHERE   ".$q_unit." ".$q_customer." ".$q_tindakan." ".$q_dokter." ".$q_user."
										GROUP BY U.Nama_Unit, X.Tgl_Transaksi, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,  k.Kd_Pasien, dtx.Kd_Dokter, dtx.Nama, X.No_Transaksi,dt.qty ORDER BY U.Nama_Unit,dtx.kd_dokter, Ps.Nama, P.Deskripsi
									")->result();
		$html.='
			<table class="t2"  cellspacing="0" border="0" >
				
					<tr>
						<th colspan="7" align="center">LAPORAN PERHITUNGAN JASA TINDAKAN (DOKTER)</th>
					</tr>
					<tr>
						<th colspan="7" align="center">'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="7" align="center">KELOMPOK PASIEN ('.$customer.')</th>
					</tr>
					<tr>
						<th colspan="7" align="center">'.$t_shift.'</th>
					</tr>
			</table> <br>';
		$html.="<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
				<thead>
					<tr>
						<th width='40' align='center'>NO.</th>
						<th width='250'>Tindakan</th>
						<th width='80'>Tarif</th>
						<th width='60'>Jml</th>
						<th width='100'>Total</th>
						<th width='100'>Operator : ".$param->kd_user."</th>
						<th width='100'>Jasa Dokter</th>
						<th width='100'>Jasa Perawat/Bidan</th>
						<th width='100'>Index Tdk Langsung</th>
						<th width='100'>Ops Instalasi</th>
						<th width='100'>Ops RS</th>
						<th width='100'>Ops Direksi</th>
						<th width='100'>Total Jasa Pelayanan</th>
					</tr>
				</thead>
			";
		
		if(count($result)==0){
			$html.="<tr>
						<td align='center' colspan='13'>Tidak Ada Data</td>
					</tr>";
		} else{
			$jumlahtarif=0;
			$jumlahtotal=0;
			$jumlahcx=0;
			$jumlahc20=0;
			$jumlahc21=0;
			$jumlahc22=0;
			$jumlahc23=0;
			$jumlahc24=0;
			$jumlahc25=0;
			$jumlahc30=0;
			
			$all_jumlahtarif=0;
			$all_jumlahtotal=0;
			$all_jumlahcx=0;
			$all_jumlahc20=0;
			$all_jumlahc21=0;
			$all_jumlahc22=0;
			$all_jumlahc23=0;
			$all_jumlahc24=0;
			$all_jumlahc25=0;
			$all_jumlahc30=0;
			$no=0;
			$temp_kd_pasien='';
			$temp_unit='';
			$arr_unitrwj = array();
			for($i=0;$i<count($result);$i++){
				$arr_unitrwj ['unit'][$i] = $result[$i]->nama_unit;
			}
			$arr_unitrwj = array_unique($arr_unitrwj['unit']); # remove duplicate value
			$arr_unitrwj = array_values($arr_unitrwj); # reset index array$arr_unitrwj = array();
			
			$arr_dokterrwj = array();
			for($i=0;$i<count($result);$i++){
				$arr_dokterrwj ['dokter'][$i] = $result[$i]->nama_dokter;
			}
			$arr_dokterrwj = array_unique($arr_dokterrwj['dokter']); # remove duplicate value
			$arr_dokterrwj = array_values($arr_dokterrwj); # reset index array
			$arr_kdpasien = array();
			$arr_namapasien = array();
			for($i=0;$i<count($result);$i++){
				$arr_kdpasien ['kd_pasien'][$i] = $result[$i]->kd_pasien;
				$arr_namapasien ['nama'][$i] = $result[$i]->nama_pasien;
			}
			$arr_kdpasien = array_unique($arr_kdpasien['kd_pasien']); # remove duplicate value
			$arr_namapasien = array_unique($arr_namapasien['nama']); # remove duplicate value
			$arr_kdpasien = array_values($arr_kdpasien); # remove duplicate value
			$arr_namapasien = array_values($arr_namapasien); # remove duplicate value
			
			$all_subtotal_jumlah = 0;
			
			for($i=0;$i<count($arr_unitrwj);$i++){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="12" align="left">'.$arr_unitrwj[$i].'</th>
				</tr>
				';
				$nama='';
				$no2=0;
				$all_total_jumlah = 0;
				$dokter = '';
				
				for($j=0;$j<count($result);$j++){
					if($arr_unitrwj[$i] == $result[$j]->nama_unit && $nama != $result[$j]->nama_pasien){
						$no2++;
						$nama=$result[$j]->nama_pasien;
						$html.='
						<tr class="headerrow"> 
							<th width="">&nbsp;</th>
							<th width="" colspan="12" align="left">'.$no2.'. '.$result[$j]->kd_pasien.' ' .'-'. ' '.$result[$j]->nama_pasien.'</th>
						</tr>
						';
						$tindakan = '';
						for($k=0;$k<count($result);$k++){
							if($result[$j]->nama_unit == $result[$k]->nama_unit && $nama == $result[$k]->nama_pasien && $tindakan != $result[$k]->deskripsi){
								$html.='
								<tr> 
									<td width="">&nbsp;</td>
									<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$result[$k]->deskripsi.'</td>
									<td align="right"> '.number_format($result[$k]->tarif,0,'.',',').'</td>
									<td align="center"> '.$result[$k]->qty.'</td>
									<td align="right"> '.number_format($result[$k]->jumlah,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->cx,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c20,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c21,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c22,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c23,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c24,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c25,0,'.',',').'</td>
									<td align="right"> '.number_format($result[$k]->c30,0,'.',',').'</td>
								</tr>
								';
								
								$all_total_jumlah += $result[$k]->jumlah;
								$all_subtotal_jumlah += $result[$k]->jumlah;
								$jumlahtarif+=$result[$k]->tarif;
								$jumlahtotal+=$result[$k]->jumlah;
								$jumlahcx+=$result[$k]->cx;
								$jumlahc20+=$result[$k]->c20;
								$jumlahc21+=$result[$k]->c21;
								$jumlahc22+=$result[$k]->c22;
								$jumlahc23+=$result[$k]->c23;
								$jumlahc24+=$result[$k]->c24;
								$jumlahc25+=$result[$k]->c25;
								$jumlahc30+=$result[$k]->c30;
								
								$tindakan = $result[$k]->deskripsi;
							}
						}
					}
				}
				$html.='
					<tr> 
						<th width=""></th>
						<th width=""  align="right">Sub Total ('.$arr_unitrwj[$i].')</th>
						<th width=""  align="right">&nbsp;</th>
						<th width=""  align="right">&nbsp;</th>
						<th width=""  align="right">'.number_format($jumlahtotal,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahcx,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc20,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc21,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc22,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc23,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc24,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc25,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($jumlahc30,0,'.',',').'</th>';
				$all_jumlahtarif+=0;
				$all_jumlahtotal+=$jumlahtotal;
				$all_jumlahcx+=$jumlahcx;
				$all_jumlahc20+=$jumlahc20;
				$all_jumlahc21+=$jumlahc21;
				$all_jumlahc22+=$jumlahc22;
				$all_jumlahc23+=$jumlahc23;
				$all_jumlahc24+=$jumlahc24;
				$all_jumlahc25+=$jumlahc25;
				$all_jumlahc30+=$jumlahc30;
			}
			$html.='
					<tr> 
						<th width=""></th>
						<th width=""  align="right">Grand Total </th>
						<th width=""  align="right">&nbsp;</th>
						<th width=""  align="right">&nbsp;</th>
						<th width=""  align="right">'.number_format($all_jumlahtotal,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahcx,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc20,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc21,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc22,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc23,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc24,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc25,0,'.',',').'</th>
						<th width=""  align="right">'.number_format($all_jumlahc30,0,'.',',').'</th>';
			
				
		}
   			
   				
   		$html.="</table>";
        $prop=array('foot'=>true);
		# jika type file 1=excel 
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('C:M')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Penerimaan_PerpasienIGD'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_perhitungan_jasa_tindakan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' LAPORAN PERHITUNGAN JASA TINDAKAN',$html);
			echo $html;
		}
   	}

      function cekVariableUnit($tmpunit){
         $arrayData = $tmpunit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         $tmpunit = str_replace(strtolower("Array"), "", $tmpunit);
         return $tmpunit;
      }


      function test_cekVariableUnit(){
         $criteria="";
         $tmpunit = "";
         $arrayData = $param->tmp_kd_unit;
         for ($i=0; $i < count($arrayData); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         echo $tmpunit;
         //return $tmpunit;
      }

      function cekVariablePayment($payment){
         $criteria="";
         $tmpunit = "";
         $arrayData = $payment;
         for ($i=0; $i < count($payment); $i++) { 
            $tmpunit .= "'".$arrayData[$i][0]."',";
         }
         $tmpunit = substr($tmpunit, 0, -1);  
         return $tmpunit;
      }
}
?>