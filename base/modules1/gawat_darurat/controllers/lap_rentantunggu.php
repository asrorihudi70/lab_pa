<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rentantunggu extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getPoliklinik(){
		$result=$this->db->query("SELECT 1 as id,kd_unit,nama_unit FROM unit
									where parent ='2'
									UNION
									Select 0 as id,'000'as kd_unit, 'SEMUA' as nama_unit
									order by id,nama_unit")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetakIGDRentanTunggu(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN RENTAN TUNGGU';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$ctgl = "where k.tgl_masuk >= '".date('Y-m-d',strtotime($tglAwal))."' and k.tgl_masuk <= '".date('Y-m-d',strtotime($tglAkhir))."'";
		
		$queryHead = $this->db->query( "select k.kd_pasien,p.nama,k.kd_unit,u.nama_unit, k.jam_masuk, k.jam_berkas_masuk, k.jam_dilayani, k.jam_keluar
										from kunjungan k
											inner join pasien p on p.kd_pasien=k.kd_pasien
											inner join unit u on u.kd_unit=k.kd_unit
											".$ctgl."
											and k.kd_unit='3'
										order by p.nama");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th> INSTALASI GAWAT DARURAT</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="60" align="center">No. Medrec</th>
					<th width="80" align="center">Nama</th>
					<th width="60" align="center">Unit</th>
					<th width="40" align="center">Jam Daftar</th>
					<th width="40" align="center">Jam Berkas Datang(Scan Berkas)</th>
					<th width="40" align="center">Jam Dilayani</th>
					<th width="40" align="center">Jam Pulang</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
							<tr>
								<th align="left">'.$no.'</th>
								<th align="left">'.$line->kd_pasien.'</th>
								<th align="left">'.$line->nama.'</th>
								<th align="left">'.$line->nama_unit.'</th>
								<th align="center">'.substr($line->jam_masuk, -8).'</th>
								<th align="center">'.substr($line->jam_berkas_masuk, -8).'</th>
								<th align="center">'.substr($line->jam_dilayani, -8).'</th>
								<th align="center">'.substr($line->jam_keluar, -8).'</th>
							</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Rentan Tunggu',$html);	
   	}
	
}
?>