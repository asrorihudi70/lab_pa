<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_riwayatpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	public function cetakRiwayatKunjungan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='RIWAYAT PASIEN';
		$param=json_decode($_POST['data']);
		
		$kd_unit=$param->kd_unit;
		$kd_pasien=$param->kd_pasien;
		$tgl_masuk=$param->tgl_masuk;
		$kd_kasir=$param->kd_kasir;
		$urut_masuk=$param->urut_masuk;
		$no_transaksi=$param->no_transaksi;
		$kd_dokter=$param->kd_dokter;
		
		$pasien = $this->db->query("select *,age(tgl_lahir) as ages from pasien where kd_pasien='".$param->kd_pasien."'")->row();
		$unit = $this->db->query("select nama_unit from unit where kd_unit='".$param->kd_unit."'")->row()->nama_unit;
		$dokter = $this->db->query("select nama from dokter where kd_dokter='".$param->kd_dokter."'")->row()->nama;
		
		$result = $this->db->query("select * from kunjungan where kd_pasien='".$param->kd_pasien."' and tgl_masuk='".$param->tgl_masuk."'");
		
		
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th> PerTanggal Kunjungan</th>
					</tr>
				</tbody>
			</table><br>';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th width="40" align="left">Nama</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$pasien->nama.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th align="left">Umur</th>
						<th align="left">:</th>
						<th width="80" align="left">'.getUmur($pasien->ages).'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Alamat</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$pasien->alamat.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Tgl. Kunjungan</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.tanggalstring($tgl_masuk).'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Nama Unit</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$unit.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
					<tr>
						<th width="40" align="left">Dokter</th>
						<th width="5"  align="left">:</th>
						<th width="80" align="left">'.$dokter.'</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
						<th width="40">&nbsp;</th>
					</tr>
				</tbody>
			</table><br>';
		#=============================================================ISI============================================================#
		
		# ----------------------------------------------ANAMNESE---------------------------------------------------------------------#
		
		$queryanamnese = $this->db->query("select anamnese from kunjungan k
									where k.kd_pasien='".$kd_pasien."' 
										and k.kd_unit='".$kd_unit."' 
										and k.tgl_masuk='".$tgl_masuk."' 
										and k.urut_masuk='".$urut_masuk."'")->result();
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th align="left">ANAMNESE</th>
				</tr>
				<tr>
					<th align="left"></th>
				</tr>
				 <tr>
					<th align="center">Anamnese / Keluhan</th>
				  </tr>
			</thead>';
		if(count($queryanamnese) > 0){
			foreach($queryanamnese as $lineanamnese){
				$html.='<tbody>
							<tr>
								<td align="left">'.$lineanamnese->anamnese.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------ICD 10 / DIAGNOSA---------------------------------------------------------------------#
		$querydiagnosa = $this->db->query("SELECT mrp.*,p.penyakit,n.morfologi,k.sebab 
											FROM mr_penyakit mrp 
												LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
												LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit 
													AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit 
													AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
												LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit 
													AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit 
													AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
											WHERE mrp.kd_pasien = '".$kd_pasien."' 
												and mrp.kd_unit='".$kd_unit."' 
												and mrp.tgl_masuk = '".$tgl_masuk." '")->result();
		
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="4" align="left">ICD 10 / Diagnosa</th>
				</tr>
				<tr>
					<th colspan="4" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="80" align="center">Deskripsi Penyakit</th>
					<th width="60" align="center">Status diagnosa</th>
					<th width="60" align="center">Kasus</th>
				  </tr>
			</thead>';
		if(count($querydiagnosa) > 0){
			$no=0;
			foreach($querydiagnosa as $linediagnosa){
				$no++;
				$diagnosa='';
				$kasus='';
				
				if ($linediagnosa->stat_diag == 0) {
					$diagnosa ='Diagnosa Awal';
				} else if ($linediagnosa->stat_diag == 1) {
					$diagnosa = 'Diagnosa Utama';
				} else if ($linediagnosa->stat_diag == 2) {
					$diagnosa ='Komplikasi';
				} else if ($linediagnosa->stat_diag == 3) {
					$diagnosa = 'Diagnosa Sekunder';
				}
				if ($linediagnosa->kasus == 't') {
					$kasus =  'Baru';
				} else if ($linediagnosa->kasus == 'f') {
					$kasus =  'Lama';
				}
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$linediagnosa->penyakit.'</td>
								<td align="left">'.$diagnosa.'</td>
								<td align="left">'.$kasus.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# ----------------------------------------------ICD 9 / TINDAKAN---------------------------------------------------------------------#
		
		$querytindakan = $this->db->query("select mr.* , ic.deskripsi
									from mr_tindakan mr
									inner join icd_9 ic on ic.kd_icd9=mr.kd_icd9
									where mr.kd_pasien='".$kd_pasien."' 
										and mr.kd_unit='".$kd_unit."' 
										and mr.tgl_masuk='".$tgl_masuk."' 
										and mr.urut_masuk='".$urut_masuk."'")->result();
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">ICD 9 / TINDAKAN</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Deskripsi</th>
				  </tr>
			</thead>';
		if(count($querytindakan) > 0){
			$no=0;
			foreach($querytindakan as $linetindakan){
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$linetindakan->deskripsi.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------------------------OBAT------------------------------------------------------------------------#
		
		$queryobat = $this->db->query("select bod.*, bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='".$kd_pasien."' 
										and bo.kd_unit='".$kd_unit."' 
										and t.tgl_transaksi='".$tgl_masuk."' 
										and t.urut_masuk='".$urut_masuk."'
									order by o.nama_obat")->result();
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">OBAT</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Nama obat</th>
				  </tr>
			</thead>';
		if(count($queryobat) > 0){
			$no=0;
			foreach($queryobat as $lineobat){
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$lineobat->nama_obat.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------------------------LABORATORIUM----------------------------------------------------------------#
		
		$kd_unit_lab = $this->db->query("select setting from sys_setting where key_data='lab_default_kd_unit'")->row()->setting;
		$querylab = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
											case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
										From LAB_hasil 
											inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
											inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
												on LAB_Test.kd_Test = produk.Kd_Produk
											inner join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
											inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
											inner join transaksi t on t.kd_pasien=lab_hasil.kd_pasien
												and t.kd_unit=lab_hasil.kd_unit and t.urut_masuk=lab_hasil.urut_masuk
												and t.tgl_transaksi=lab_hasil.tgl_masuk
										where LAB_hasil.Kd_Pasien = '".$kd_pasien."' 
											And LAB_hasil.Tgl_Masuk = '".$tgl_masuk."'  
											and LAB_hasil.Urut_Masuk = '".$urut_masuk."'
											and LAB_hasil.kd_unit= '".$kd_unit_lab."' 
											and lab_hasil.kd_unit_asal='".$kd_unit."'
										order by lab_test.kd_lab,lab_test.kd_test asc")->result();
										
		
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="9" align="left">LABORATORIUM</th>
				</tr>
				<tr>
					<th colspan="9" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th width="20" align="center">Item Pemeriksaan</th>
					<th width="40" align="center">Pemeriksaan</th>
					<th width="40" align="center">Metode</th>
					<th width="20" align="center">Hasil</th>
					<th width="30" align="center">Normal</th>
					<th width="20" align="center">Ket. hasil</th>
					<th width="40" align="center">Satuan</th>
					<th width="40" align="center">Keterangan</th>
				  </tr>
			</thead>';
			
		if(count($querylab) > 0){
			$no=0;
			foreach($querylab as $linelab){
				$hasil = '';
				$ket = '';
				$metode = '';
				
				if ($linelab->hasil == 'null' || $linelab->hasil == null) {
					$hasil ='';
				} else {
					$hasil = $linelab->hasil;
				}
				
				if ($linelab->ket == 'null' || $linelab->ket == null || $linelab->ket == 'undefined') {
					$ket = '';
				} else {
					$ket = $linelab->ket;
				}
				
				if ($linelab->satuan == 'null' && $linelab->satuan == 'null') {
					$metode = '';
				} else {
					$metode = $linelab->metode;
				}
				if($linelab->judul_item == ''){
					$nomor='';
				} else{
					$no++;
					$nomor=$no;
				}
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$nomor.'</td>
								<td align="left">'.$linelab->judul_item.'</td>
								<td align="left">'.$linelab->item_test.'</td>
								<td align="center">'.$metode.'</td>
								<td align="left">&nbsp;'.$hasil.'</td>
								<td align="center">'.$linelab->normal.'</td>
								<td align="center">'.$linelab->ket_hasil.'</td>
								<td align="left">'.$linelab->satuan.'</td>
								<td align="left">'.$ket.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table><br>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		# -------------------------------------------------------RADIOLOGI-------------------------------------------------------------------#
		$kd_unit_rad = $this->db->query("select setting from sys_setting where key_data='rad_default_kd_unit'")->row()->setting;
		$queryrad = $this->db->query("select dt.*, p.deskripsi
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
										inner join unit_asal ua on ua.kd_kasir=t.kd_kasir and ua.no_transaksi=t.no_transaksi
										inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal
										inner join rad_hasil rh on rh.kd_pasien=t.kd_pasien and rh.kd_unit=t.kd_unit and rh.urut_masuk=t.urut_masuk and rh.tgl_masuk=t.tgl_transaksi
									where k.kd_pasien='".$kd_pasien."' 
										and k.kd_unit='".$kd_unit_rad."'
										and tr.kd_unit='".$kd_unit."'
										and k.tgl_masuk='".$tgl_masuk."' 
										and k.urut_masuk='".$urut_masuk."'")->result();
		
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				<tr>
					<th colspan="2" align="left">RADIOLOGI</th>
				</tr>
				<tr>
					<th colspan="2" align="left"></th>
				</tr>
				 <tr>
					<th width="5" align="center">No.</th>
					<th align="center">Deskripsi pemeriksaan radiologi</th>
				  </tr>
			</thead>';
		if(count($queryrad) > 0){
			$no=0;
			foreach($queryrad as $linerad){
				$no++;
				
				$html.='<tbody>
							<tr>
								<td align="center">'.$no.'</td>
								<td align="left">'.$linerad->deskripsi.'</td>
							</tr>';
			}
			$html.='<tbody></table><br>';
		} else{
			$html.='<tbody>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>
					<tbody>
					</table>';
		}
		# -----------------------------------------------------------------------------------------------------------------------------------#
		# -----------------------------------------------------------------------------------------------------------------------------------#
		
		
		
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Riwayat Pasien',$html);	
   	}
}
?>