<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_tindakanperpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakIGDTindakanPerPasien(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN TINDAKAN PERPASIEN';
		$param=json_decode($_POST['data']);
		
		$kd_unit=$param->kd_unit;
		$kd_customer=$param->kd_customer;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$ctgl = "where k.tgl_masuk >= '".date('Y-m-d',strtotime($tglAwal))."' and k.tgl_masuk <= '".date('Y-m-d',strtotime($tglAkhir))."'";
		if($kd_unit == 'Semua' || $kd_unit == ''){
			$cKdunit=" and u.parent not in('100','2')";
			$unit = 'SEMUA';
		} else{
			$cKdunit="and t.kd_unit='".$kd_unit."'";
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
		}
		
		if($kd_customer == 'SEMUA' || $kd_customer==''){
			$cKdcustomer = '';
			$customer='SEMUA';
		} else{
			$cKdcustomer = "and k.kd_customer='".$kd_customer."'";
			$customer = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
		}
		
		$queryHead = $this->db->query( "select distinct(t.kd_pasien),ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer
										from detail_transaksi dt 
											inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
											inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
											inner join produk p on p.kd_produk=dt.kd_produk
											inner join pasien ps on ps.kd_pasien=k.kd_pasien
											inner join dokter d on d.kd_dokter=k.kd_dokter
											inner join unit u on u.kd_unit=t.kd_unit
											inner join customer c on c.kd_customer=k.kd_customer
										WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
										".$cKdunit.$cKdcustomer."
										order by t.tgl_transaksi,ps.nama");
		$query = $queryHead->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th> Poli : '.$unit.'</th>
					</tr>
					<tr>
						<th> Jenis Pasien : '.$customer.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="60" align="center">No. Medrec</th>
					<th width="80" align="center">Nama</th>
					<th width="40" align="center">Tgl. Transaksi</th>
					<th width="60" align="center">Unit</th>
					<th width="40" align="center">Customer</th>
					<th width="40" align="center">Deskripsi tindakan</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
							<tr>
								<td align="left">'.$no.'</td>
								<td align="left">'.$line->kd_pasien.'</td>
								<td align="left">'.$line->nama.'</td>
								<td align="left">'.tanggalstring($line->tgl_transaksi).'</td>
								<td align="left">'.$line->nama_unit.'</td>
								<td align="left">'.$line->customer.'</td>
								<td></td>
							</tr>';
				$queryBody = $this->db->query( "select dt.*, t.kd_pasien,ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer,p.deskripsi
							from detail_transaksi dt 
								inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
								inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
								inner join produk p on p.kd_produk=dt.kd_produk
								inner join pasien ps on ps.kd_pasien=k.kd_pasien
								inner join dokter d on d.kd_dokter=k.kd_dokter
								inner join unit u on u.kd_unit=t.kd_unit
								inner join customer c on c.kd_customer=k.kd_customer
							WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
							and t.kd_unit='".$line->kd_unit."' and k.kd_customer='".$line->kd_customer."' and t.kd_pasien='".$line->kd_pasien."'
							order by ps.nama");
					$query2 = $queryBody->result();	
				foreach($query2 as $line2){
					$html.='<tr>
								<td></td>
								<td colspan="5"></td>
								<td align="left">'.$line2->deskripsi.'</td>
							</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Tindakan PerPasien',$html);	
   	}
	
}
?>