<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class view_daftarigd extends MX_Controller
{

    
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


	public function index()
	{
        $this->load->view('main/index');
    }


	function read($Params=null)
	{
		try
		{
                    $this->load->model('gawat_darurat/tb_igd');
                       if (strlen($Params[4])!==0)
                    {
                       $this->db->where(str_replace("~", "'", $Params[4]." offset ".$Params[0]." limit 200") ,null, false);
                    }
                    $res = $this->tb_igd->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                }
                catch(Exception $o)
		{
			echo 'Debug  fail ';

		}

	 	echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
	}
	public function saveDiagnosa($Params, $SchId){
		$this->db->trans_begin();
		$kdPasien = $Params["NoMedrec"];
		$kdUnit = $Params["Poli"];
		$Tgl =date('Y-m-d');
		$urut_masuk = 0;
		$kdPenyakit=$Params['KdDiagnosa'];
		$perawatan = 99;
		$tindakan = 99;
		$inser_batch=array();
		$this->db->query("DELETE FROM mr_penyakit WHERE kd_pasien='".$kdPasien."' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk='".$urut_masuk."'");
		$diagnosa = 0;
		$kasus = 'FALSE';
		//$zkasus = 0;

		$cekPenyakit=$this->db->query("SELECT COUNT(kd_penyakit)AS jum FROM mr_penyakit WHERE kd_penyakit='".$kdPenyakit."' AND kd_pasien='".$kdPasien."'")->row();
		if($cekPenyakit->jum>0){
			$kasus='TRUE';
		}


		$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
		$result = $urut->result();
		foreach ($result as $data){
			$Urutan = $data->geturutmrpenyakit;
		}
		$query = $this->db->query("select insertdatapenyakit('".$kdPenyakit."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",
		".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
		
		
		if($this->db->trans_status()==true){
			$this->db->trans_commit();
			//$strError = 'okeh';
		}else{
			$this->db->trans_rollback();
			//$strError = 'henteu';
		}	
		//return $strError;
	}
       /* public function save($Params=null)
        {
        	
            $mError = "";
            $AppId = "";
            $SchId = "";
            $Schunit = "";
            $Schtgl = "";
            $Schurut = "";
            $notrans = "";
			$this->db->trans_begin();
			//_QMS_Query('Begin Transaction');
            $mError = $this->SimpanPasien($Params, $SchId);
           if ($mError == "ada")
               {
               	if($Params['NonKunjungan']==='true'){
					  $this->db->trans_commit();
						echo '{success: true, KD_PASIEN: "' . $SchId . '"}';
				} else{ 
					  $mError = $this->SimpanKunjungan($Params, $SchId, $Schunit, $Schtgl, $Schurut);
					   if ($mError=="aya")
						{
						$mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans));
						if ($mError[0]=="sae")
							{	$this->db->trans_commit();
							
							echo '{success: true, KD_PASIEN: "'.$SchId.'",NoTrans: "' . $mError[1] . '"}';
						
							}
							else 
							{
							$this->db->trans_rollback();
							echo '{success: false}';
							}
						}
						else if ($mError=="cari_ada")
						{
						$this->db->trans_rollback();
						echo '{success: false, cari: true }';
						}
						else if($mError=="eror")
						{
						$this->db->trans_rollback();
						echo '{success: false, }';
						}
						else{
						$this->db->trans_rollback();
						echo '{success: false, }';
						}
					  } 

               }
		
        } */
	public function save($Params = null){
		//$db = $this->load->database('otherdb2',TRUE);
		$mError = "";
		$AppId = "";
		$SchId = "";
		$Schunit = "";
		$Schtgl = "";
		$Schurut = "";
		$notrans = "";
		$this->db->trans_begin();
		//$db->trans_begin();
		$mError = $this->SimpanPasien($Params, $SchId);
		if ($mError == "ada") {
			if ($Params['NonKunjungan'] === 'true') {
				$this->db->trans_commit();
				//$db->trans_commit();
				echo '{success: true, KD_PASIEN: "' . $SchId . '"}';
			} else {
				$antrian = $this->GetUrutKunjungan($Params["NoMedrec"], $Params["Poli"],date('Y-m-d')); 
				// $mError = $this->SimpanKunjungan($Params, $SchId, $Schunit, $Schtgl, $Schurut);
				$mError = $this->SimpanKunjungan($Params, $SchId, $antrian);
				if ($mError == "aya") {
					// $mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans));
					$mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $antrian, $notrans));
					if ($mError[0] == "sae") {
						$this->db->trans_commit();
						//$db->trans_commit();
						echo '{success: true, KD_PASIEN: "'.$SchId.'",NoTrans: "' . $mError[1] . '"}';
					} else {
						$this->db->trans_rollback();
						//$db->trans_rollback();
						echo '{success: false}';
					}
				} else if ($mError == "eror") {             
					$this->db->trans_rollback();
					//$db->trans_rollback();
					echo '{success: false, cari: true}';
				   
				} else {
					$this->db->trans_rollback();
					//$db->trans_rollback();
					echo "{success: false,message:'Pasien ini sudah berkunjung, Data tidak dapat disimpan'}";
				}
			}
		} else {
			$this->db->trans_rollback();
			//$db->trans_rollback();
			echo "{success: false,message:'Pasien tidak dapat disimpan.'}";
		}
	}
		
	public function SimpanPasien($Params, & $AppId = ""){
		$strError = "";
		$suku = 0;
		if ($Params["GolDarah"] === "") {
			$Params["GolDarah"] = 0;
		}
		if ($Params["KDKECAMATAN"] === "") {
			$tmpkecamatan = $Params["Kd_Kecamatan"];
			$tmpkabupaten = $Params["AsalPasien"];
			if ($Params["Pendidikan"]=='' || $Params["Pendidikan"]=='undefined'){
				$tmppendidikan =0;
			}else{
				$tmppendidikan = $Params["Pendidikan"];
			}
			if ($Params["Pekerjaan"]=='' || $Params["Pekerjaan"]=='undefined'){
				$tmppekerjaan =0;
			}else{
				$tmppekerjaan = $Params["Pekerjaan"];
			}
			$tmpagama = $Params["Agama"];
		} else {
			$tmpkecamatan = $Params["KDKECAMATAN"];
			$tmpkabupaten = $Params["KDKABUPATEN"];
			if ($Params["KDPENDIDIKAN"]=='' || $Params["KDPENDIDIKAN"]=='undefined'){
				$tmppendidikan =0;
			}else{
				$tmppendidikan = $Params["KDPENDIDIKAN"];
			}
			if ($Params["KDPEKERJAAN"]=='' || $Params["KDPEKERJAAN"]=='undefined'){
				$tmppekerjaan =0;
			}else{
				$tmppekerjaan = $Params["KDPEKERJAAN"];
			}
			$tmpagama = $Params["KDAGAMA"];
		}
		if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan') {
			$kode_asuransi = NULL;
		} else {
			$kode_asuransi = $Params["KD_Asuransi"];
		}
		if ($Params["NoAskes"] == "") {
			$tmpno_asuransi = $Params["NoSjp"];
		} else {
			$tmpno_asuransi = $Params["NoAskes"];
		}
		if ($Params["Kelurahan"] == "" or $Params["Kelurahan"] === NULL or $Params["Kelurahan"] === "Pilih Kelurahan...") {

			$intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
		} else {
			if (is_numeric($Params["Kelurahan"])) {
				$intkd_lurah = $Params["Kelurahan"];
			} else {
				$intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
			}
		}
		if ($Params["KdKelurahanKtpPasien"] == "" or $Params["KdKelurahanKtpPasien"] === NULL or $Params["KdKelurahanKtpPasien"] === "Pilih Kelurahan...") {
			$intkd_lurahktp = $this->GetKdLurah($kd_Kec = $Params["KDKECAMATANKTP"]);
		} else {
			$intkd_lurahktp = $Params["KdKelurahanKtpPasien"];
		}
		if ($Params["Pendidikan_Ayah"]=='' || $Params["Pendidikan_Ayah"]=='undefined'){
			$pendAyah=0;
		}else{
			$pendAyah=$Params["Pendidikan_Ayah"];
		}	
		if ($Params["Pendidikan_Ibu"]=='' || $Params["Pendidikan_Ibu"]=='undefined'){
			$pendIbu=0;
		}else{
			$pendIbu=$Params["Pendidikan_Ibu"];
		}
		if ($Params["Pekerjaan_Ayah"]=='' || $Params["Pekerjaan_Ayah"]=='undefined'){
			$pekerAyah=0;
		}else{
			$pekerAyah=$Params["Pekerjaan_Ayah"];
		}	
		if ($Params["Pekerjaan_Ibu"]=='' || $Params["Pekerjaan_Ibu"]=='undefined'){
			$pekerIbu=0;
		}else{
			$pekerIbu=$Params["Pekerjaan_Ibu"];
		}	
		if ($Params["Pendidikan_SuamiIstri"]=='' || $Params["Pendidikan_SuamiIstri"]=='undefined'){
			$pendSumis=0;
		}else{
			$pendSumis=$Params["Pendidikan_SuamiIstri"];
		}
		if ($Params["Pekerjaan_SuamiIstri"]=='' || $Params["Pekerjaan_SuamiIstri"]=='undefined'){
			$pekerSumis=0;
		}else{
			$pekerSumis=$Params["Pekerjaan_SuamiIstri"];
		}
		$nik_pasien=$Params["part_number_nik"];
		//echo 'awdwdawd';
		if ($Params["NoMedrec"] === "" || $Params["NoMedrec"] == "Automatic from the system...") {
			//echo 'cai1';
			$AppId = $this->GetIdRWJ();
			$Params["NoMedrec"]=$AppId;
			//echo $AppId;
		}else{
			//echo 'cai';
			$AppId = $Params["NoMedrec"];
		}
		if (substr($_POST['TglLahir'], 6,4) == '1900') {
      		$tmptgllahir = '1900-01-01 00:00:00.000';
      	}else{
			$tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $_POST['TglLahir'])));
      	}
		$data = array("kd_pasien" => $AppId, "nama" => $Params["NamaPasien"],
			"nama_keluarga" => $Params["NamaKeluarga"], "jenis_kelamin" => $Params["JenisKelamin"],
			"tempat_lahir" => $Params["Tempatlahir"], "tgl_lahir" => $tmptgllahir,
			"kd_agama" => $tmpagama, "gol_darah" => $Params["GolDarah"],
			"status_marita" => $Params["StatusMarita"], "wni" => $Params["StatusWarga"],
			"alamat" => $Params["Alamat"], "telepon" => $Params["TLPNPasien"],
			"kd_kelurahan" => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
			"kd_pekerjaan" => $tmppekerjaan, "pemegang_asuransi" => $Params["NamaPeserta"],
			"no_asuransi" => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
			"kd_suku" => $suku, "jabatan" => $Params["Jabatan"],
			"kd_perusahaan" => 0, "alamat_ktp" => $Params["AlamatKtpPasien"],
			"kd_kelurahan_ktp" => $intkd_lurahktp,
			"kd_pos_ktp" => $Params["KdPostKtpPasien"], "nama_ayah" => $Params["NamaAyahPasien"],
			"nama_ibu" => $Params["NamaIbuPasien"], "kd_pos" => $Params["KdposPasien"],
			"handphone" => $Params["HPPasien"],
			"email" => $Params["EmailPasien"],
			"suami_istri" => $Params["suami_istrinya"],
			"kd_pendidikan_ayah" => $pendAyah,
			"kd_pendidikan_ibu" => $pendIbu,
			"kd_pekerjaan_ayah" => $pekerAyah,
			"kd_pekerjaan_ibu" => $pekerIbu,
			"kd_pendidikan_suamiistri" => $pendSumis,
			"kd_pekerjaan_suamiistri" => $pekerSumis,
			"part_number_nik" => $nik_pasien 
		);
		if ($Params["StatusWarga"]=='false'){
			$wni=0;
		}else{
			$wni=1;
		}
		if ($Params["JenisKelamin"]=='false'){
			$jk=0;
		}else{
			$jk=1;
		}
		$datasqlsrv = array("kd_pasien" => $Params["NoMedrec"], "nama" => $Params["NamaPasien"],
			"nama_keluarga" => $Params["NamaKeluarga"], "jenis_kelamin" => $jk,
			"tempat_lahir" => $Params["Tempatlahir"], "tgl_lahir" => $tmptgllahir,
			"kd_agama" => $tmpagama, "gol_darah" => $Params["GolDarah"],
			"status_marita" => $Params["StatusMarita"], "wni" => $wni,
			"alamat" => $Params["Alamat"], "telepon" => $Params["TLPNPasien"],
			"kd_kelurahan" => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
			"kd_pekerjaan" => $tmppekerjaan, "pemegang_asuransi" => $Params["NamaPeserta"],
			"no_asuransi" => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
			"kd_suku" => $suku, "jabatan" => $Params["Jabatan"],
			"kd_perusahaan" => 1,  "kd_pos" => $Params["KdposPasien"]
		);
		//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER		  	
		$criteria = "kd_pasien = '" . $AppId . "'";
		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		//$query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
		//$query =$this->db->query("select * from pasien where $criteria ");
		//var_dump($query);
		//_QMS_insert('pasien', $datasqlsrv,$criteria);
		//$query = _QMS_QUERY('select * from kunjungan where '.$criteria);
		/*$query = _QMS_QUERY("select * from pasien where $criteria ");
		if (count($query->result()) == 0) {
			$datasqlsrv["kd_pasien"] = $AppId;
			_QMS_insert('pasien', $datasqlsrv);
			$strError = "ada";
		} else {
			_QMS_update('pasien', $datasqlsrv,$criteria);
			$strError = "ada";
		} */
		$query =$this->db->query("select * from pasien where $criteria ");
		if (count($query->result()) == 0) {
			$data["kd_pasien"] = $AppId;
			$result = $this->db->insert('pasien',$data);
			$strError = "ada";
		} else {
			$this->tb_pasien->db->where($criteria, null, false);
			$dataUpdate = array("nama" => $Params["NamaPasien"]);
			$result = $this->tb_pasien->Update($data);
			$strError = "ada";
		} 
		return $strError;
	}
		
		 /*  public function SimpanPasienDulu($Params, &$AppId="")
        {
            $strError = "";
            $suku = 0;
			
			   
          if($Params["GolDarah"] === "")
		  {
			  $Params["GolDarah"]='1';
		  }    
		if($Params["KDKECAMATAN"] === "")
            {
               // $tmppropinsi = $Params["Kelurahan"];
                $tmpkecamatan = $Params["Kd_Kecamatan"];
                $tmpkabupaten = $Params["AsalPasien"];
                if ($Params["Pendidikan"]=='' || $Params["Pendidikan"]=='undefined')
				{
					$tmppendidikan =0;
				}
				else
				{
					$tmppendidikan = $Params["Pendidikan"];
				}
                if ($Params["Pekerjaan"]=='' || $Params["Pekerjaan"]=='undefined')
				{
					$tmppekerjaan =0;
				}
				else
				{
					$tmppekerjaan = $Params["Pekerjaan"];
				}
                $tmpagama = $Params["Agama"];
                
            }else{
               // $tmppropinsi = $Params["KDPROPINSI"];
                $tmpkecamatan = $Params["KDKECAMATAN"];
                $tmpkabupaten = $Params["KDKABUPATEN"];
                if ($Params["KDPENDIDIKAN"]=='' || $Params["KDPENDIDIKAN"]=='undefined')
				{
					$tmppendidikan =0;
				}
				else
				{
					$tmppendidikan = $Params["KDPENDIDIKAN"];
				}
                if ($Params["KDPEKERJAAN"]=='' || $Params["KDPEKERJAAN"]=='undefined')
				{
					$tmppekerjaan =0;
				}
				else
				{
					$tmppekerjaan = $Params["KDPEKERJAAN"];
				}
                $tmpagama = $Params["KDAGAMA"];
                }
               
			     if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan')
                {
                    $kode_asuransi = NULL;
                }
                else
                {
                    $kode_asuransi = $Params["KD_Asuransi"];
                }
                if ($Params["NoAskes"] == "")
                    {
                    $tmpno_asuransi = $Params["NoSjp"];
                    }
                else
                    {
                    $tmpno_asuransi = $Params["NoAskes"];
                    }
					
				if($Params["Kelurahan"]=="" or $Params["Kelurahan"]===NULL or $Params["Kelurahan"]==="Pilih Kelurahan..." )
			   { 
			   
				   $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
            	 
				   }
				   else
				   {
					   if (is_numeric($Params["Kelurahan"]))
					   {
							$intkd_lurah= $Params["Kelurahan"]; 
					   }
					   else
					   {
						    $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
						   
					   } 
				   };       
          	
					
				if($Params["KdKelurahanKtpPasien"]=="" or $Params["KdKelurahanKtpPasien"]===NULL or $Params["KdKelurahanKtpPasien"]==="Pilih Kelurahan..." )
			  	   {
				   $intkd_lurahktp = $this->GetKdLurah($kd_Kec = $Params["KDKECAMATANKTP"]);
				   }
				   else
				   {
					   $intkd_lurahktp=$Params["KdKelurahanKtpPasien"] ;
				   };       
				if ($Params["Pendidikan_Ayah"]=='' || $Params["Pendidikan_Ayah"]=='undefined')
				{
					$pendAyah=0;
				}
				else
				{
					$pendAyah=$Params["Pendidikan_Ayah"];
				}
				if ($Params["Pendidikan_Ibu"]=='' || $Params["Pendidikan_Ibu"]=='undefined')
				{
					$pendIbu=0;
				}
				else
				{
					$pendIbu=$Params["Pendidikan_Ibu"];
				}
				
				if ($Params["Pekerjaan_Ayah"]=='' || $Params["Pekerjaan_Ayah"]=='undefined')
				{
					$pekerAyah=0;
				}
				else
				{
					$pekerAyah=$Params["Pekerjaan_Ayah"];
				}
				
				if ($Params["Pekerjaan_Ibu"]=='' || $Params["Pekerjaan_Ibu"]=='undefined')
				{
					$pekerIbu=0;
				}
				else
				{
					$pekerIbu=$Params["Pekerjaan_Ibu"];
				}
				
				if ($Params["Pendidikan_SuamiIstri"]=='' || $Params["Pendidikan_SuamiIstri"]=='undefined')
				{
					$pendSumis=0;
				}
				else
				{
					$pendSumis=$Params["Pendidikan_SuamiIstri"];
				}
				
				if ($Params["Pekerjaan_SuamiIstri"]=='' || $Params["Pekerjaan_SuamiIstri"]=='undefined')
				{
					$pekerSumis=0;
				}
				else
				{
					$pekerSumis=$Params["Pekerjaan_SuamiIstri"];
				}
				$nik_pasien=$Params["part_number_nik"];
				if ($Params["NoMedrec"] == " " || $Params["NoMedrec"] == "Automatic from the system...")
                {
                  $AppId = $this->GetIdRWJ();
				} else {$AppId = $Params["NoMedrec"];}
                $data = array("kd_pasien"=>$AppId,"nama"=>$Params["NamaPasien"],
                          "nama_keluarga"=>$Params["NamaKeluarga"],"jenis_kelamin"=>$Params["JenisKelamin"],
                          "tempat_lahir"=>$Params["Tempatlahir"],"tgl_lahir"=>$Params["TglLahir"],
                          "kd_agama"=>$tmpagama,"gol_darah"=>$Params["GolDarah"],
                          "status_marita"=>$Params["StatusMarita"],"wni"=>$Params["StatusWarga"],
                          "alamat"=>$Params["Alamat"],"telepon"=>$Params["TLPNPasien"],
                          "kd_kelurahan"=>$intkd_lurah,"kd_pendidikan"=>$tmppendidikan,
                          "kd_pekerjaan"=>$tmppekerjaan,"pemegang_asuransi"=>$Params["NamaPeserta"],
                          "no_asuransi"=>$tmpno_asuransi,"kd_asuransi"=>$kode_asuransi,
                          "kd_suku"=>$suku,"jabatan"=>$Params["Jabatan"],
						  "kd_perusahaan"=>0,"alamat_ktp"=>$Params["AlamatKtpPasien"],
						  "kd_kelurahan_ktp"=>$intkd_lurahktp,
						  "kd_pos_ktp"=>$Params["KdPostKtpPasien"],"nama_ayah"=>$Params["NamaAyahPasien"],
						  "nama_ibu"=>$Params["NamaIbuPasien"],"kd_pos"=>$Params["KdposPasien"],
						  "handphone"=>$Params["HPPasien"],
						  "email"=>$Params["EmailPasien"],"no_nik"=>$Params["NoNIK"],
						  "suami_istri" => $Params["suami_istrinya"],
						"kd_pendidikan_ayah" => $pendAyah,
						"kd_pendidikan_ibu" => $pendIbu,
						"kd_pekerjaan_ayah" => $pekerAyah,
						"kd_pekerjaan_ibu" => $pekerIbu,
						"kd_pendidikan_suamiistri" => $pendSumis,
						"kd_pekerjaan_suamiistri" => $pekerSumis,
						"part_number_nik" => $nik_pasien  
						  );
						  
			//DATA ARRAY UNTUK SAVE KE SQL SERVER
			 $datasql = array("kd_pasien"=>$AppId,"nama"=>$Params["NamaPasien"],
                          "nama_keluarga"=>$Params["NamaKeluarga"],"jenis_kelamin"=>$Params["JenisKelamin"],
                          "tempat_lahir"=>$Params["Tempatlahir"],"tgl_lahir"=>$Params["TglLahir"],
                          "agama"=>$tmpagama,"gol_darah"=>$Params["GolDarah"],
                          "status_marita"=>$Params["StatusMarita"],"wni"=>$Params["StatusWarga"],
                          "alamat"=>$Params["Alamat"],"telepon"=>$Params["TLPNPasien"],
                          "kd_kelurahan"=>$intkd_lurah,"kd_pendidikan"=>$tmppendidikan,
                          "kd_pekerjaan"=>$tmppekerjaan,"pemegang_asuransi"=>$Params["NamaPeserta"],
                          "no_asuransi"=>$tmpno_asuransi,"kd_asuransi"=>$kode_asuransi,
                          "jabatan"=>$Params["Jabatan"],
                          "kd_perusahaan"=>1);
			//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER		  	
						  
            if ($Params["NoMedrec"] == " " || $Params["NoMedrec"] == "Automatic from the system...")
                {
                  $AppId = $this->GetIdRWJ();
            } else {$AppId = $Params["NoMedrec"];}

            $criteria = "kd_pasien = '".$AppId."'";

            $this->load->model("rawat_jalan/tb_pasien");
            $this->tb_pasien->db->where($criteria, null, false);
            $query = $this->tb_pasien->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {
				
						
			//-----------insert to sq1 server Database---------------//
			/* $datasql["kd_pasien"] = $AppId;
			_QMS_insert('pasien',$datasql); */
			//-----------akhir insert ke database sql server----------------//
				
            /*$data["kd_pasien"] = $AppId;
            $result = $this->tb_pasien->Save($data);
			if($result)
			{
			$strError = "ada";
			}
			else
			{
			$strError = "error";
			}
			
            
            }else{
            $this->tb_pasien->db->where($criteria, null, false);
            $dataUpdate = array("nama"=>$Params["NamaPasien"]);
            $result = $this->tb_pasien->Update($data);
			
		
			
            $strError = "ada";
            }
            return $strError;
        } */

        public function GetKdLurah($kd_Kec)
        {
            //echo($kd_Kec);
            $intKdKec = $kd_Kec;
            $intKdLurah;
            
            $criteria = "where kec.kd_kecamatan=".$intKdKec." And (Kelurahan='DEFAULT' or Kelurahan='.' OR Kelurahan is null)";
            $this->load->model("rawat_jalan/tb_getkelurahan");
            $this->tb_getkelurahan->db->where($criteria, null, false);
            $query = $this->tb_getkelurahan->GetRowList( 0,1, "", "","");
            //print_r($query);
            
            if ($query[1]!=0)
            {
                if($query[0][0]->KD_KELURAHAN == '')
                {
                    $tmp_kdlurah = $this->GetLastKd_daerah(1);
                    $this->load->model("general/tb_kelurahan");

                    $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                    $result = $this->tb_kelurahan->Save($data);
                    $strError = $tmp_kdlurah;
                }else
                    {
                        $strError = $query[0][0]->KD_KELURAHAN;
                    }
            }
             else
                {
                if ($intKdKec == "")
                    {
                        $intKdKec = $this->GetLastKd_daerah(2);
                        $this->load->model("general/tbl_kecamatan");
                        $data = array("kd_kabupaten"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kecamatan"=>"DEFAULT");
                        $result = $this->tbl_kecamatan->Save($data);
                        $strError = $intKdKec;
                    }
                    else
                        {
                            $intKdKec = $kd_Kec;
                        }
                     $tmp_kdlurah = $this->GetLastKd_daerah(1);
                     $this->load->model("general/tb_kelurahan");
                     $data = array("kd_kelurahan"=>$tmp_kdlurah,"kd_kecamatan"=>$intKdKec,"kelurahan"=>"DEFAULT");
                     $result = $this->tb_kelurahan->Save($data);
                     $strError = $tmp_kdlurah;
                }        
            //echo($strError);
            return $strError;
        }

        public function GetLastKd_daerah($LevelLokasi)
        {

            if ($LevelLokasi == "1")
            {
                        $this->load->model('general/tb_case1');
                        $res = $this->tb_case1->GetRowList(0,1, "", "","");
                        if ($res[1]>0)
                            {
                                $nm = $res[0][0]->KODE;
                                $nomor = (int) $nm +1;
                            }
            }
            else if($LevelLokasi == "2")
                {
                        $this->load->model('general/tb_case2');
                        $res = $this->tb_case2->GetRowList(0,1, "", "","");
                        if ($res[1]>0)
                            {
                                $nm = $res[0][0]->KODE;
                                $nomor = (int) $nm +1;
                            }
                }
                 else if($LevelLokasi == "3")
                {
                            $this->load->model('general/tb_case3');
                            $res = $this->tb_case3->GetRowList(0,1, "", "","");
                            if ($res[1]>0)
                                {
                                    $nm = $res[0][0]->KODE;
                                    $nomor = (int) $nm +1;
                                }
                }
                 else if($LevelLokasi == "4")
                {
                                $this->load->model('general/tb_case4');
                                $res = $this->tb_case4->GetRowList(0,1, "", "","");
                                if ($res[1]>0)
                                    {
                                        $nm = $res[0][0]->KODE;
                                        $nomor = (int) $nm +1;
                                    }
                }
                return $nomor;
                //echo($nm);
        }

	public function SimpanKunjungan($Params, $SchId,$antrian){
		//$db = $this->load->database('otherdb2',TRUE);
		//echo $SchId;
		//echo json_encode($Params);
		$kunj = $this->db->query("select * from kunjungan where kd_pasien='".$SchId."' and kd_unit='".$Params["Poli"]."'");
		$Shiftbagian= $this->GetShiftBagian();
		$Shiftbagian= (int) $Shiftbagian;
		$kode_customer = $Params["KdCustomer"];
		$Params["NoMedrec"] = $SchId;
		$jam_kunj=$Params["JamKunjungan"];
		if($kunj->num_rows() == 0){
			$baru = 'true';
			$barusqlsrv=1;
		}else{
			$baru = 'false';
			$barusqlsrv=0;
		}
		// $antrian = $this->GetUrutKunjungan($Params["NoMedrec"], $Params["Poli"],date('Y-m-d'));  
		if ($Params["KdRujukan"] === '' || $Params["KdRujukan"] === 'Pilih Rujukan...'){
			$tmpkd_rujuk = 0;
		}  else {
			 $tmpkd_rujuk = $Params["KdRujukan"];
		}
		$strError = "";
		date_default_timezone_set("Asia/Jakarta");
		$JamKunjungan = gmdate("Y-m-d H:i:s", time()+60*60*7);
		$cari_kd_unit=$this->db->query("select setting from sys_setting where key_data='".$_POST['unitMana']."_default_kd_unit'")->row()->setting;
		$asal_pasien=$this->db->query("select kd_asal from asal_pasien where kd_unit='$cari_kd_unit'")->row()->kd_asal;
		$data = array("kd_pasien"=>$Params["NoMedrec"],"kd_unit"=>$Params["Poli"],"tgl_masuk"=>date('Y-m-d'),
		"urut_masuk"=>$antrian,"jam_masuk"=>$JamKunjungan,"cara_penerimaan"=>$Params["CaraPenerimaan"],
		"kd_rujukan"=>$tmpkd_rujuk,"asal_pasien"=>$asal_pasien,"kd_dokter"=>$Params["KdDokter"],
		"baru"=>$baru,"kd_customer"=>$kode_customer,"shift"=>$Shiftbagian,"karyawan"=>$Params["Karyawan"],
					  "kontrol"=>$Params["Kontrol"],"antrian"=>$Params["Antrian"],"no_surat"=>$Params["NoSurat"],
					  "alergi"=>$Params["Alergi"],
					  "anamnese"=>$Params["Anamnese"],"no_sjp"=>$Params["NoSjp"]);

		$AppId = $Params["NoMedrec"];
		$Schunit = $Params["Poli"];
		$Schtgl = date('Y-m-d');
		$Schurut= $antrian;          
		//$criteria = "kd_pasien = '".$AppId."' AND lunas = false ";//permintaan pak toni kalau mau masuk ugd harus lunas
		$criteria = "kd_pasien = '".$AppId."' AND kd_unit = '".$Schunit."' AND tgl_masuk = '".$Schtgl."' ";
		$this->load->model("rawat_jalan/tb_kunjungan_pasien");	
		$this->tb_kunjungan_pasien->db->where($criteria, null, false);
		//$kdkasir=$this->db->query("select  setting from sys_setting where key_data='default_kd_kasir_igd' ")->row()->setting; 
		//$query = $this->db->query("select  kd_pasien from transaksi where $criteria and kd_kasir='$kdkasir' ")->result(); 
		//$query = $this->db->query("select * from kunjungan where $criteria")->result(); 
		$query = $this->db->query('select * from kunjungan where '.$criteria);
		//if (count($query)==0|| $_POST["cek_ok_rwi"]==='true')
		if(count($query->result()) == 0 || count($query->result()) > 0 ){
			$result = $this->tb_kunjungan_pasien->Save($data);
				//-----------insert to sq1 server Database---------------//
			$datasql = array("kd_pasien" => $Params["NoMedrec"], "kd_unit" => $Params["Poli"],
				"tgl_masuk" => date('Y-m-d'), "urut_masuk" => $antrian, "jam_masuk" => $JamKunjungan,
				"cara_penerimaan" => $Params["CaraPenerimaan"],
				"asal_pasien" =>$asal_pasien, "kd_rujukan" => $tmpkd_rujuk, "kd_dokter" => $Params["KdDokter"],
				"baru" => $baru, "kd_customer" => $kode_customer, "shift" => 1,
				"karyawan" => $Params["Karyawan"], "kontrol" => $Params["Kontrol"],
				"antrian" => $Params["Antrian"], "no_surat" => $Params["NoSurat"]);
				//-----------akhir insert ke database sql server----------------//	
			$datasql["kd_pasien"] = $AppId;
			$data["kd_pasien"] = $AppId;
			if ($Params["Kontrol"]=='false'){
				$kontrolsrv=0;
			}else{
				$kontrolsrv=1;
			}
			/* $datasqlsrv =array("kd_pasien" => $Params["NoMedrec"], "kd_unit" => $Params["Poli"], "tgl_masuk" => date('Y-m-d'),
				"urut_masuk" => $antrian, "jam_masuk" => $JamKunjungan, "cara_penerimaan" => $Params["CaraPenerimaan"], "asal_pasien" =>$asal_pasien, 
				"kd_rujukan" =>$tmpkd_rujuk,
				"kd_dokter" => $Params["KdDokter"],
				"baru" => $barusqlsrv, "kd_customer" => $Params["KdCustomer"], "shift" => $Shiftbagian, "karyawan" => $Params["Karyawan"],
				"kontrol" => $kontrolsrv, "antrian" => $Params["Antrian"], "no_surat" => $Params["NoSurat"],
				"alergi" => $Params["Alergi"], "no_sjp" => $Params["NoSjp"]);
				_QMS_insert('kunjungan', $datasqlsrv); */
			if($result){
				if($Params["KelurahanPJ"]==="" or $Params["KelurahanPJ"]===NULL or
				 $Params["KelurahanPJ"]==="Pilih Kelurahan..."){
					$intkd_lurahktp = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
				}
				if($Params["NoSjp"]!=""){
					$result=$this->db->query("INSERT INTO sjp_kunjungan
					(kd_pasien,kd_unit,tgl_masuk,urut_masuk,no_sjp
					) values('".$Params["NoMedrec"]."','".$Params["Poli"]."','".date('Y-m-d')."',".$antrian.",
					'".$Params["NoSjp"]."')");	
				}
				if ($Params["NAMA_PJ"]==="" && $Params["KTP"]===""){
					$strError = "aya";	
				}else{
					$result=$this->db->query("
						INSERT INTO penanggung_jawab
						(kd_pasien,kd_unit,tgl_masuk,urut_masuk,kd_kelurahan,kd_pekerjaan,
						nama_pj ,alamat ,telepon ,kd_pos,hubungan,alamat_ktp,no_hp,
						kd_pos_ktp ,no_ktp,kd_pendidikan,email,tempat_lahir,tgl_lahir,status_marital,
						kd_agama,kd_kelurahan_ktp,wni,jenis_kelamin
						) values
						('".$Params["NoMedrec"]."','".$Params["Poli"]."','".date('Y-m-d')."',".$antrian.",
						".$Params["KelurahanPJ"].",".$Params["kdpekerjaanPj"]."
						,'".$Params["NAMA_PJ"]."','".$Params["AlamatPJ"]."','".$Params["TeleponPj"]."',
						'".$Params["PosPJ"]."',".$Params["HubunganPj"].",'".$Params["AlamatKtpPJ"]."',
						'".$Params["HpPj"]."','".$Params["KdPosKtp"]."','".$Params["KTP"]."',
						".$Params["KdPendidikanPj"].",'".$Params["EmailPenanggungjawab"]."'
						,'".$Params["tempatlahirPenanggungjawab"]."','".$Params["TgllahirPJ"]."',
						".$Params["StatusMaritalPenanggungjawab"].",1,".$Params["KelurahanKtpPJ"].",
						".$Params["WNIPJ"].",".$Params["JKpenanggungJwab"].")");	
					$strError = "aya";
				}
			}
		}else{	
			$strError='cari_ada';
		}
		return $strError;
	}

	public function SimpanTransaksi($Params, $SchId, $Schurut, $notrans){
		$_kduser = $this->session->userdata['user_id']['id'];
		$strError = "";
		$strcek = "";
		$strcekdata = "";
		$kd_unit = "";
		$appto = "";
		$kd_kasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$Params["NoMedrec"] = $SchId;
		// $Schurut = $this->GetAntrian($Params["NoMedrec"],$Params["Poli"],date('Y-m-d'),$Params["KdDokter"]);
		$Schkasir=  $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;	
		$noTrans=0;
		$loop=true;
		while($loop==true){
			$notrans = $this->GetIdTransaksi($kd_kasir);
			$this->load->model("general/tb_transaksi");
			$criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$Schkasir."'";
			//$this->tb_transaksi->db->where($criteria, null, false);
			//$query = $this->tb_transaksi->GetRowList(0, 1, "", "", "");
			//$query = _QMS_QUERY('select * from transaksi where '.$criteria)->result();
			$query = $this->db->query('select * from transaksi where '.$criteria)->result();
			if(count($query)==0){
				$loop=false;
			}
		}
//            $Params["UrutMasuk"] = $Schurut;
		$data = array("kd_kasir"=>$kd_kasir,
		  "no_transaksi"=>$notrans,
		  "kd_pasien"=>$Params["NoMedrec"],
		  "kd_unit"=>$Params["Poli"],
		  "tgl_transaksi"=>date('Y-m-d'),
		  "urut_masuk"=>$Schurut,
		  "tgl_co"=>NULL,
		  "co_status"=>"False",
		  "orderlist"=>NULL,
		  "ispay"=>"False",
		  "app"=>"False",
		  "kd_user"=>$_kduser,
		  "tag"=>NULL,
		  "lunas"=>"False",
		  "posting_transaksi"=>"true",
		  "tgl_lunas"=>NULL);
		$now=new DateTime();
		$datasqlsrv = array("kd_kasir" => $kd_kasir,
			"no_transaksi" => $notrans,
			"kd_pasien" => $Params["NoMedrec"],
			"kd_unit" => $Params["Poli"],
			"tgl_transaksi" => $now->format('Y-m-d'),
			"urut_masuk" => $Schurut,
			"tgl_co" => NULL,
			"co_status" => 0,
			"orderlist" => NULL,
			"ispay" => 0,
			"app" => 0,
			"kd_user" => $_kduser,
			"tag" => NULL,
			"lunas" => 0,
			//"posting_transaksi"=>1,
			"tgl_lunas" => NULL);
		if (count($query) == 0) { 
			//-----------insert to sq1 server Database---------------//
			//-----------akhir insert ke database sql server----------------
			//RadioRujukanPasien,PasienBaruRujukan	
			$data["no_transaksi"] = $notrans;
			//_QMS_insert('transaksi', $datasqlsrv);
			$result = $this->tb_transaksi->Save($data);
			$strError = "sae ".$notrans;
			if($result){
				//echo $Params['PasienBaruRujukan'];
				//echo $Params['JenisRujukan'];
				//$querygetappto=$this->db->query("Select getappto(False,".$Params['PasienBaruRujukan'].",".$Params['RadioRujukanPasien'].",False,False,'-1')")->result();				
				$querygetappto=$this->db->query("Select getappto(False,".$Params['PasienBaruRujukan'].",".$Params['RadioRujukanPasien'].",False,False,'".$Params['JenisRujukan']."')")->result();				
				$sqlPasienBaruRujukan=1;
				if($Params['PasienBaruRujukan']=='false'){
					$sqlPasienBaruRujukan=0;
				}
				$sqlRadioRujukanPasien=1;
				if($Params['RadioRujukanPasien']=='false'){
					$sqlRadioRujukanPasien=0;
				}
				$sqlJenisRujukan=1;
				if($Params['JenisRujukan']=='false'){
					$sqlJenisRujukan=0;
				}
				//$querygetapptoSQL = _QMS_Query("Select dbo.GetAppTo(0," . $sqlPasienBaruRujukan . "," . $sqlRadioRujukanPasien . ",0,0,'".$Params['JenisRujukan']."')  AS appto")->row();
				foreach($querygetappto as $getappto){
					$hasilgetappto = $getappto->getappto;
				}
				//$hasilgetapptoSql=$querygetapptoSQL->appto;
				if($hasilgetappto!=""){
					$querygetappto=$this->db->query("Select getappto(False,".$Params['PasienBaruRujukan'].",".$Params['RadioRujukanPasien'].",False,False,'".$Params['JenisRujukan']."')")->result();
					foreach($querygetappto as $getappto){
						$hasilgetappto = $getappto->getappto;
					}
					$Shiftbagian= $this->GetShiftBagian();
					$Shiftbagian= (int) $Shiftbagian;	
					$kdtarifcus=$this->db->query("Select getkdtarifcus('".$Params['KdCustomer']."')")->result();
					foreach($kdtarifcus as $getkdtarifcus){
						$KdTarifpasien = $getkdtarifcus->getkdtarifcus;
					}
					$gettglberlaku=$this->db->query("Select gettanggalberlakufromklas('$KdTarifpasien','".date('Y-m-d')."','".date('Y-m-d')."','71')")->result();
					foreach($gettglberlaku as $gettanggalberlakufromklas){
						$Tglberlaku_pasien = $gettanggalberlakufromklas->gettanggalberlakufromklas;
					}
					if($Tglberlaku_pasien=="" ||$Tglberlaku_pasien=="null" ){
						$Tglberlaku_pasien=date('Y-m-d');	
					}
					$sql="INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
						select  
							'$Schkasir',
							'$notrans',
							row_number() OVER () as rnum,
							'" . date('Y-m-d') . "',
							" . $_kduser . ",
							rn.kd_tarif,
							rn.kd_produk,
							rn.kd_unit,
							rn.tglberlaku,
							'true',
							'true',
							'A'
							,1,
							rn.tarifx,
							" . $Shiftbagian . ",
							'false',
							''  

							from(
							Select AutoCharge.appto,
							tarif.kd_tarif,
							AutoCharge.kd_produk,
							AutoCharge.kd_unit,
							max (tarif.tgl_berlaku) as tglberlaku,
							tarif.tarif as tarifx,
							row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
							From AutoCharge inner join tarif on 
							tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
							inner join produk on produk.kd_produk = tarif.kd_produk  
							Where 
							AutoCharge.kd_unit='" . $Params["Poli"] . "'
							and AutoCharge.appto in $hasilgetappto
							and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
							and tgl_berlaku <= '$Tglberlaku_pasien'
							group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku order by AutoCharge.kd_produk asc
							) as rn
							where rn = 1											
					";
					//left(AutoCharge.kd_unit,1)=left('" . $Params["Poli"] . "',1)
					$queryinsertdetailtransaksi = $this->db->query($sql);
						/* PERBARUAN */ 
					/* $sql_SS="INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
						select top 1 '$Schkasir',
							'$notrans',1, '" . date('Y-m-d') . "',
						" . $_kduser . ", rn.kd_tarif, 
						 rn.kd_produk, left(rn.kd_unit,1), rn.tglberlaku, 1, 1, 'A' ,1, rn.tarifx, " . $Shiftbagian . ", 0, '' 
						from( 
							Select AutoCharge.appto, tarif.kd_tarif, AutoCharge.kd_produk, 
						AutoCharge.kd_unit, max (tarif.tgl_berlaku) as tglberlaku, tarif.tarif as tarifx
						From AutoCharge inner join tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=left(autoCharge.kd_unit,1) 
						inner join produk on produk.kd_produk = tarif.kd_produk Where 
						AutoCharge.kd_unit='" . $Params["Poli"] . "'
						and AutoCharge.appto in $hasilgetapptoSql
						and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
						and tgl_berlaku <= '$Tglberlaku_pasien'
						group by AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,
						tarif.tgl_berlaku
						)
						as rn order by tglberlaku desc"; */
					//echo $sql_SS;
					//$queryinsertdetailtransaksisql=_QMS_Query($sql_SS);
					
					# CEK DETAIL_TRANSAKSI, JIKA KOSONG MAKA TIDAK INSERT DETAIL_COMPONENT & DETAIL_TRDOKTER
					$res = $this->db->query("select count(*) as jml from detail_transaksi where no_transaksi='$notrans' and kd_kasir='$Schkasir'")->row();
					//$ressql = _QMS_Query("select count(*) as jml from detail_transaksi where no_transaksi='$notrans' and kd_kasir='$Schkasir'")->row();
					if($res->jml > 0){
						$query = $this->db->query("
								select * from(
							Select row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
							tarif.kd_tarif,
							AutoCharge.kd_produk,
							AutoCharge.kd_unit,
							max (tarif.tgl_berlaku) as tglberlaku,
							max(tarif.tarif) as tarifx
							From AutoCharge inner join tarif on 
							tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
							inner join produk on produk.kd_produk = tarif.kd_produk  
							Where AutoCharge.kd_unit='" . $Params["Poli"] . "'
							and AutoCharge.appto in $hasilgetappto
							and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
							and tgl_berlaku <= '$Tglberlaku_pasien'
							group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
							tarif.kd_tarif order by AutoCharge.kd_produk asc) as  rn where rn.rnum=1
						");
						if ($query->num_rows() > 0){
							/*
								PEMBARUAN DEFAULT KODE COMPONENT DETAIL TR DOKTER
								OLEH 	: HADAD AL GOJALI
								TANGGAL : 2017-02-02
								ALASAN 	: KD KOMPONENT DI KASIH NILAI TETAP (DI PATOK)
							 */

							$KdTrDokterComp = $this->db->query("SELECT setting FROM sys_setting WHERE key_data='def_kdcomp_dettrdr'")->row()->setting;
							$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
							$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
							//echo $query->num_rows();
							foreach ($query->result() as $row){
								$kdprodukpasien=$row->kd_produk;
								$kdTarifpasien=$row->kd_tarif;
								$tglberlakupasien=$row->tglberlaku;
								$kd_unitpasein=$row->kd_unit;
								$urutpasein=$row->rnum;
								$query = $this->db->query("INSERT INTO Detail_Component 
									(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
									Select '$Schkasir','$notrans',$urutpasein,'".date('Y-m-d')."',kd_component, tarif as FieldResult,0
									From Tarif_Component 
									Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien");
								/* $sqlComponent="INSERT INTO Detail_Component 
									(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
									Select '$Schkasir','$notrans',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
									From Tarif_Component 
									Where left(KD_Unit,1)=left('$kd_unitpasein',1) And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien"; */
								//	echo $sqlComponent;
								//_QMS_Query($sqlComponent);
								if 	($query){
									$trDokter = $this->db->query("insert into detail_trdokter 
										Select '$Schkasir','$notrans',$urutpasein,'".$Params["KdDokter"]."','".date('Y-m-d')."',0,".$KdTrDokterComp.", 
										tarif as FieldResult,0,0,0 From Tarif_Component
										Where 
										(KD_Unit='$kd_unitpasein' 
										And Tgl_Berlaku='$tglberlakupasien'
										And KD_Tarif='$kdTarifpasien'
										And Kd_Produk=$kdprodukpasien 
										and kd_component = '$kdjasadok')
										 OR   (KD_Unit='$kd_unitpasein' 
										 And Tgl_Berlaku='$tglberlakupasien'
										 And KD_Tarif='$kdTarifpasien'
										 And Kd_Produk=$kdprodukpasien 
										and kd_component = '$kdjasaanas')");
									/* _QMS_Query("insert into detail_trdokter 
										Select '$Schkasir','$notrans',$urutpasein,'".$Params["KdDokter"]."','".date('Y-m-d')."',0,0, 
										tarif as FieldResult,0,0,0 From Tarif_Component
										Where 
										(left(KD_Unit,1)=left('$kd_unitpasein',1) 
										And Tgl_Berlaku='$tglberlakupasien'
										And KD_Tarif='$kdTarifpasien'
										And Kd_Produk=$kdprodukpasien 
										and kd_component = '$kdjasadok')
										 OR   (left(KD_Unit,1)=left('$kd_unitpasein',1) 
										 And Tgl_Berlaku='$tglberlakupasien'
										 And KD_Tarif='$kdTarifpasien'
										 And Kd_Produk=$kdprodukpasien 
										and kd_component = '$kdjasaanas')"); */
								}								
							}
						}
						$paid=$this->db->query("select auto_paid from zusers where kd_user = '".$_kduser."'")->row()->auto_paid;
						if($paid==1){
							//postgre
							$sqlDetailTransaksi="SELECT * FROM detail_transaksi WHERE kd_kasir='$Schkasir' AND no_transaksi='$notrans'";
							$resDetailTransaksi=$this->db->query($sqlDetailTransaksi)->result();
							$totBayar=0;
							if ($totBayar > 0) {
								for($i=0,$iLen=count($resDetailTransaksi); $i<$iLen;$i++){
									$totBayar+=((int)$resDetailTransaksi[$i]->qty*(int)$resDetailTransaksi[$i]->harga);
								}
								$sqlPay="SELECT kd_pay FROM payment WHERE kd_customer='".$Params["KdCustomer"]."'";
								$resPay=$this->db->query($sqlPay)->row();
								$sql="SELECT inserttrpembayaran('".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
									'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",true,'".$now->format('Y-m-d')."',1)";
								$this->db->query($sql);
								$sql="SELECT insertpembayaran('".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
									'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",true,'".$now->format('Y-m-d')."',1)";
								$this->db->query($sql);
								
								// //sql
								/* $sql="EXEC dbo.v5_inserttrpembayaran '".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
									'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",1";
								 _QMS_Query($sql);
								 $sql="EXEC dbo.v5_insertpembayaran '".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
									 '".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",1,'".$now->format('Y-m-d')."',1";
									// echo $sql;
								 _QMS_Query($sql); */
							}
							
						}
					}
				}	
			}
		}
		$sqlCekReg="select count(kd_pasien) as jum from reg_unit where kd_unit='".$Params["Poli"]."' AND kd_pasien='".$Params["NoMedrec"]."'";
		$arrCekReg=$this->db->query($sqlCekReg)->row()->jum;
		if($arrCekReg==0){
			$sqlReg="select max(no_register) as no from reg_unit where kd_unit='".$Params["Poli"]."'";
			$resReg = $this->db->query($sqlReg)->row()->no;
			$resReg++;
			$arrReg=array(
				'kd_pasien'=>$Params["NoMedrec"],
				'kd_unit'=>$Params["Poli"],
				'no_register'=>$resReg,
				'urut_masuk'=>0
			);
			$this->db->insert('reg_unit', $arrReg);
		}
		if ($Params['KdDiagnosa']<>''){
			$this->saveDiagnosa($Params, $SchId);
		}  
		return $strError;
	}

	private function GetAntrian($medrec,$Poli,$Tgl,$Dokter){
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
		 private function GetIdRWJ_sss() {

			  $ci =& get_instance();
			  $db = $ci->load->database('otherdb2',TRUE);
			  $sqlcounter = $db->query("select top 1 kd_pasien as kdpas from pasien Where kd_pasien Like '%-%' And isnumeric(replace(kd_pasien,'-',''))=1 Order By kd_pasien desc
			  ")->row();
			  $sqlvalsql = $sqlcounter->kdpas;
			  $sqlvalsql3 = str_replace("-", "",$sqlvalsql);
			  $sqlvalsql4 = (int)$sqlvalsql3 + 1;

			  if ($sqlvalsql4 < 999999)
			  {
			  	$sqlvalsql2=str_pad($sqlvalsql4, 8, "0", STR_PAD_LEFT);
			  }
			  else
			  {
			  	$sqlvalsql2=str_pad($sqlvalsql4, 7, "0", STR_PAD_LEFT);
			  }

			  $getnewmedrecsql = substr($sqlvalsql2, 0, 1). '-' .substr($sqlvalsql2, 1, 2). '-' .substr($sqlvalsql2, 3, 2). '-' .substr($sqlvalsql2, -2); 



			// $this->load->model('rawat_jalan/getmedrec');
			// $res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien", "");
			// if ($res[1] > 0) {
			// 	$nm = $res[0][0]->KD_PASIEN;
			// 	//penambahan 1 digit
			// 	$nomor = (int) $nm+1;


			// 	//echo("'$nomor'");
			// 	if ($nomor < 999999) {
			// 		$retVal = str_pad($nomor, 8, "0", STR_PAD_LEFT);
			// 	} else {
			// 		$retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
			// 	}
			// 	//memberikan '-' pada setiap baris angka
			// 	$getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
			// } else {
			// 	$strNomor = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
			// 	$getnewmedrec = $strNomor . "01";
			// }
			/* if ($nomor<$sqlvalsql4)
			  {
			  $getnewmedrec=$getnewmedrecsql ;
			  } */

			return $getnewmedrecsql;
		}
		
		private function GetIdRWJ() {

        /* $ci =& get_instance();
          $db = $ci->load->database('otherdb',TRUE);
          $sqlcounter = $db->query("select top 1 kd_pasien as kdpas from pasien Where kd_pasien Like '%-%' And isnumeric(replace(kd_pasien,'-',''))=1 Order By kd_pasien desc
          ")->row();
          $sqlvalsql = $sqlcounter->kdpas;
          $sqlvalsql3 = str_replace("-", "",$sqlvalsql);
          $sqlvalsql4 = $sqlvalsql3+1;
          if ($sqlvalsql4<999999)
          {
          $sqlvalsql2=str_pad($sqlvalsql4,8,"0",STR_PAD_LEFT);
          }
          else
          {
          $sqlvalsql2=str_pad($sqlvalsql4,7,"0",STR_PAD_LEFT);
          }
          $getnewmedrecsql = substr($sqlvalsql2, 0,1).'-'.substr($sqlvalsql2,2,2).'-'.substr($sqlvalsql2,4,2).'-'.substr($sqlvalsql2,-2); */


		//$db = $this->load->database('otherdb2',TRUE);
       // $this->load->model('rawat_jalan/getmedrec');
        //$res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien", "");
        $res = $this->db->query("Select replace(kd_pasien,'-','') as KD_PASIEN from pasien Where kd_pasien Like '%-%' Order By kd_pasien desc limit 1");
        if (count($res->result()) > 0) {
			
            $nm = $res->row()->kd_pasien;
            //penambahan 1 digit
			//echo (int) $nm+1; 
            $nomor = (int) $nm + 1;


            //echo("'$nomor'");
            if ($nomor < 999999) {
                $retVal = str_pad($nomor, 8, "0", STR_PAD_LEFT);
            } else {
                $retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
            }
            //memberikan '-' pada setiap baris angka
			/* echo $retVal.'<br/>';
			echo str_pad($nomor, 7, "0", STR_PAD_LEFT).'<br/>'; */
				
            $getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
			
			/* echo substr($retVal, 0, 1).'<br/>';
			echo substr($retVal, 1, 2).'<br/>';
			echo substr($retVal, 3, 2).'<br/>';
			echo substr($retVal, -2).'<br/>';
			echo $getnewmedrec.'<br/>'; */
		} else {
            $strNomor = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
            $getnewmedrec = $strNomor . "01";
        }
        /* if ($nomor<$sqlvalsql4)
          {
          $getnewmedrec=$getnewmedrecsql ;
          } */
		
        return $getnewmedrec;
    }
        /* private function GetIdRWJ()
        {

				
            $this->load->model('rawat_jalan/getmedrec');
            $res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->KD_PASIEN;
                //penambahan 1 digit
                $nomor = (int) $nm +1;
			

                //echo("'$nomor'");
                if ($nomor<999999)
                    {
                        $retVal=str_pad($nomor,8,"0",STR_PAD_LEFT);
                    }
                    else
                    {
                        $retVal=str_pad($nomor,7,"0",STR_PAD_LEFT);
                    }
                //memberikan '-' pada setiap baris angka
                $getnewmedrec = substr($retVal, 0,1).'-'.substr($retVal,1,2).'-'.substr($retVal,3,2).'-'.substr($retVal,-2);
            }else
            {
              $strNomor="0-00-".str_pad("00-",2,'0',STR_PAD_LEFT);
              $getnewmedrec=$strNomor."06";
            }
		
			
//          
            return $getnewmedrec;
        } */

        private function GetIdTransaksi($kd_kasir)
        {
			
			/*$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$sqlcounterkasir = $db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$sqlsqlcounterkasir2 = $sqlcounterkasir->counter;
			$sqlnotr = $sqlsqlcounterkasir2+1;
			*/
			//$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
			$no = $counter->counter;
			$retVal = $no+1;
			/*if($sqlnotr>$retVal)
			{
			$retVal=$sqlnotr;	
			}*/
			$query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
			$update = $this->db->query($query);
			
			//-----------insert to sq1 server Database---------------//
				//_QMS_query($query);
			//-----------akhir insert ke database sql server----------------//
			$retVal = str_pad($retVal, 7, "0", STR_PAD_LEFT);
        return $retVal;
        }

        private function GetUrutKunjungan($medrec, $unit, $tanggal)
        {
            $retVal = 0;
            if($medrec === "Automatic from the system..." || $medrec === " ")
            {
                $tmpmedrec = " ";
            }else {
                $tmpmedrec = $medrec;
            }
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $nomor = (int) $nm +1;
                $retVal=$nomor;
            }
            return $retVal;
        }
		
		
		private function GetShiftBagian()
        {
			
			/* $ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE); */
			$sqlbagianshift = $this->db->query("SELECT shift FROM BAGIAN_SHIFT  where KD_BAGIAN='3'")->row();
			$sqlbagianshift2 = $sqlbagianshift->shift;
			
			//$sqlbagianshift2=1;
				
        return $sqlbagianshift2;
        }
		

        public function CekdataAutoCharge($kd_unit, $appto)
        {
            $Params = "";
            $app = "";
            $pasien = "";
            $strcek = $this->readcekautocharge($Params);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";
                       
                try
		{
			$this->load->model('general/autocharge');
                        $this->autocharge->db->where('left(kd_unit,3)='.$kd_unit.'and AutoCharge.appto in'.$appto.'');
                        $res = $this->autocharge->GetRowList($app, $pasien);
                        if ($res[1]>0)
                            {
                                $nm = $res;
                            }else
                                {
                                $nm = "";
                            }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }

        public function CekdataTarif()
        {
            $Params = "";
            $app = "";
            $pasien = "";
            $strcek = $this->readcekautocharge($Params);
                if ($strcek !== "")
                {
                     $appto=$strcek;
                }
            $strError = "";
            $kd_unit="'"."202"."'";

                try
		{
			

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $nm;
        }

	function readcekautocharge($Params=null)
	{
            $strError = "";
            $app="false";
            $pasien="false";
            $rujukan="false";
            $kontrol="false";
            $kartu="false";
            $jenisrujukan="1";
		try
		{
			$strQuery = "Select GetAppTo"."(".$app.",".$pasien.",".$rujukan.",".$kontrol.",".$kartu.",".$jenisrujukan."::text".")";
                        echo($strQuery);
                        $query = $this->db->query($strQuery);
                        $res = $query->result();
                        if ($query->num_rows() > 0)
                        {
                            foreach($res as $data)
                            {
                            $curentshift = $data->getappto;
                            }
                        }

		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
                return $curentshift;
	}

	
    function getNoantrian($kd_unit){
    	$no = $this->db->query("select max(no_antrian) as no_antrian from antrian_poliklinik 
    							where tgl_transaksi='".date('Y-m-d')."' and kd_unit='".$kd_unit."'
    							order by no_antrian desc limit 1");
    	if(count($no->result()) > 0){
    		$no_antrian = $no->row()->no_antrian + 1;
    	} else{
    		$no_antrian = 1;
    	}
    	return $no_antrian;
    }
	
	function getHistoryKunjungan(){
		//$dbsqlsrv = $this->load->database('otherdb2',TRUE);
		if($_POST['kd_pasien']==''){
			$kd_pasien="";
		} else{
			$kd_pasien=$_POST['kd_pasien'];
		}
		# QUERY POSTGRES
		/* $result=$this->db->query("SELECT top 100 P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER
								FROM PASIEN P
										INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
										inner join customer c on c.kd_customer = k.kd_customer
										INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
										INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
								where P.KD_PASIEN = '".$kd_pasien."' ")->result(); */
		
		#QUERY SQLSERVER
		$result=$this->db->query("SELECT P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, 
									d.KD_DOKTER , K.TGL_KELUAR,c.CUSTOMER,TO_CHAR(K.JAM_MASUK, 'HH24:MI:SS') as JAM_MASUK
								FROM PASIEN P
										INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
										inner join customer c on c.kd_customer = k.kd_customer
										INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
										INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
								where P.KD_PASIEN = '".$kd_pasien."' 
								order by k.TGL_MASUK desc limit 100 ")->result(); 
		foreach ($result as $rec)
		{
		  $o=array();
		  $o['KD_PASIEN']=$rec->kd_pasien;
		  $o['KD_UNIT']=$rec->kd_unit;
		  $o['POLI']=$rec->nama_unit;
		  $o['URUT']=$rec->urut_masuk;
		  $o['TGL']=date("d-M-Y", strtotime($rec->tgl_masuk));
		  $o['DOKTER']=$rec->nama_dokter;
		  $o['KD_DOKTER']=$rec->kd_dokter;
		  $o['CUSTOMER']=$rec->customer;
		  $o['JAM_MASUK']=$rec->jam_masuk;
		  if ($rec->tgl_keluar==null)
		  {
		  	$o['TGL_KELUAR']='';
		  }else

		  {
		  	$o['TGL_KELUAR']=date("d-M-Y", strtotime($rec->tgl_keluar));
		  }
		  
          $list[]=$o;	
		}
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($list) . '}';
	}
	
}
?>			