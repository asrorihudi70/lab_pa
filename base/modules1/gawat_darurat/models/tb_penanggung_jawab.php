<?php

class tb_penanggung_jawab extends TblBase
{
    function __construct()
    {
        $this->TblName='penanggung_jawab';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewpenanggungjawab;
		
        $row->KD_PASIEN=$rec->kd_pasien;
		$row->KD_UNIT=$rec->KD_UNIT;
		$row->TGL_MASUK=$rec->TGL_MASUK;
		$row->URUT_MASUK=$rec->URUT_MASUK;
		$row->KD_PERUSAHAAN=$rec->KD_PERUSAHAAN;
		$row->KD_KELURAHAN=$rec->KD_KELURAHAN;
		$row->KD_PEKERJAAN=$rec->KD_PEKERJAAN;
		$row->NAMA_PJ=$rec->NAMA_PJ;
		$row->ALAMAT=$rec->ALAMAT;
		$row->KOTA=$rec->KOTA;
		$row->TELEPON=$rec->TELEPON;
		$row->KD_POS=$rec->KD_POS;
		$row->JABATAN=$rec->JABATAN;
		$row->HUBUNGAN=$rec->HUBUNGAN;
		$row->ALAMAT_KTP=$rec->ALAMAT_KTP; 
		$row->NO_HP=$rec->NO_HP; 
		$row->KD_POS_KTP=$rec->KD_POS_KTP; 
		$row->NO_KTP=$rec->NO_KTP; 
		$row->KD_KELURAHAN_KTP=$rec->KD_KELURAHAN_KTP;  
		$row->EMAIL=$rec->EMAIL; 


        return $row;
    }
}

class Rowviewpenanggungjawab
{
    public $KD_PASIEN;
	public $KD_UNIT; 
	public $TGL_MASUK; 
	public $URUT_MASUK;  
	public $KD_PERUSAHAAN;  
	public $KD_KELURAHAN; 
	public $KD_PEKERJAAN; 
	public $NAMA_PJ ;
	public $ALAMAT; 
	public $KOTA; 
	public $TELEPON; 
	public $KD_POS; 

}

?>
