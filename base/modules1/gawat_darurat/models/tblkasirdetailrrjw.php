<?php
class tblkasirdetailrrjw extends TblBase
{


    function __construct()
    {
        $this->TblName='detail_transaksi';
        TblBase::TblBase();

        $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowtblkasirdetailrrjw;
		
		
		$row->KD_KASIR=$rec->kd_kasir;
        $row->NO_TRANSAKSI=$rec->no_transaksi;
        $row->URUT=$rec->urut;
		$row->TGL_TRANSAKSI=$rec->tgl_transaksi;
		$row->KD_USER=$rec->kd_user;
		$row->KD_TARIF=$rec->kd_tarif;
        $row->KD_PRODUK=$rec->kd_produk; 
		$row->KD_UNIT=$rec->kd_unit; 
        $row->TGL_BERLAKU=$rec->tgl_berlaku;
		$row->CHARGE=$rec->charge;
		$row->ADJUST=$rec->adjust;
		$row->FOLIO=$rec->folio;
		$row->QTY=$rec->qty;
        $row->HARGA=$rec->harga;
		$row->SHIFT=$rec->shift;
       	$row->KD_DOKTER=$rec->kd_dokter;
		$row->KD_UNIT_TR=$rec->kd_unit_tr;
		$row->CITO=$rec->cito;
	    $row->JS=$rec->js;
		$row->JP=$rec->jp;
		$row->NO_FAKTUR=$rec->no_faktur;
		$row->FLAG=$rec->flag;
		$row->TAG=$rec->tag;
		$row->HRG_ASLI=$rec->hrg_asli;
	   	 $row->KD_CUSTOMER=$rec->kd_customer;
        return $row;
    }

}
class Rowtblkasirdetailrrjw
{	public $KD_KASIR;
    public $NO_TRANSAKSI;
    public $URUT;
	public $TGL_TRANSAKSI;
	public $KD_USER;
	public $KD_TARIF;
    public $KD_PRODUK;
	public $KD_UNIT;
    public $TGL_BERLAKU;
	public $CHARGE;
	public $ADJUST;
	public $FOLIO;
	public $QTY;
	public $HARGA;
	public $SHIFT;
	public $KD_DOKTER;
	public $KD_UNIT_TR;
	public $CITO;
	public $JS;
	public $JP;
	public $NO_FAKTUR;
	public $FLAG;
	public $TAG;
	public $HRG_ASLI;
	public $KD_CUSTOMER;
	
	//teu dipake
	 public $PROBLEM;
    public $IMPACT;
	public $STATUS_ID;
   // public $DESC_STATUS;

}

?>
