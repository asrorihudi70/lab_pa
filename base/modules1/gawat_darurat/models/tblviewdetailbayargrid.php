<?php
class tblviewdetailbayargrid extends TblBase
{
    function __construct()
    {
        $this->TblName='viewdetailbayargriid';
        TblBase::TblBase(true);
		$this->StrSql="kd_produk,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,tunai,dicount,piutang,kd_tarif";
		
        $this->SqlQuery= "SELECT * from (select produk.kp_produk,detail_transaksi.no_transaksi ,detail_transaksi.urut, 
COALESCE(SUM(detail_transaksi.harga * detail_transaksi.qty)/COUNT(detail_transaksi.urut) - SUM(detail_tr_bayar.jumlah),(detail_transaksi.harga * detail_transaksi.qty)  )  as tunai, 
COALESCE(SUM(detail_transaksi.harga * detail_transaksi.qty)/COUNT(detail_transaksi.urut) - SUM(detail_tr_bayar.jumlah),(detail_transaksi.harga * detail_transaksi.qty)  )  as piutang,
COALESCE(SUM(detail_transaksi.harga * detail_transaksi.qty)/COUNT(detail_transaksi.urut) - SUM(detail_tr_bayar.jumlah),(detail_transaksi.harga * detail_transaksi.qty)  )  as dicount,
detail_transaksi.kd_unit,detail_transaksi.kd_produk,produk.deskripsi,detail_transaksi.harga,detail_transaksi.flag,detail_transaksi.qty,detail_transaksi.tgl_berlaku,
detail_transaksi.kd_dokter,detail_transaksi.adjust,detail_transaksi.cito,detail_transaksi.kd_customer,detail_transaksi.tgl_transaksi,detail_transaksi.kd_tarif,detail_transaksi.kd_kasir,detail_transaksi.tag
from  detail_transaksi 
 left join detail_tr_bayar on 
detail_transaksi.no_transaksi = detail_tr_bayar.no_transaksi AND 
detail_transaksi.urut = detail_tr_bayar.urut AND
detail_transaksi.tgl_transaksi = detail_tr_bayar.tgl_transaksi AND
detail_transaksi.kd_kasir = detail_tr_bayar.kd_kasir 
  inner join
unit on detail_transaksi.kd_unit = unit.kd_unit 
  inner join
produk on detail_transaksi.kd_produk = produk.kd_produk 
GROUP BY 
detail_transaksi.urut,
 detail_transaksi.no_transaksi
,detail_transaksi.harga * detail_transaksi.qty,detail_transaksi.kd_unit,detail_transaksi.kd_produk,produk.kd_produk,detail_transaksi.harga,detail_transaksi.flag,
detail_transaksi.qty,
detail_transaksi.tgl_berlaku
,detail_transaksi.kd_dokter
,detail_transaksi.adjust
,detail_transaksi.cito
,detail_transaksi.kd_kasir
,detail_transaksi.kd_customer
,detail_transaksi.tgl_transaksi
,detail_transaksi.kd_tarif
) as  resdata";

    }
	/*
  left join detail_bayar on detail_transaksi.urut = detail_bayar.urut And  detail_transaksi.tgl_transaksi = detail_bayar.tgl_transaksi AND detail_transaksi.kd_kasir = detail_bayar.kd_kasir 
									  left join payment on detail_bayar.kd_pay = payment.kd_pay
									  left join payment_type on payment.jenis_pay = payment_type.jenis_pay
	*/

    function FillRow($rec)
    {
        $row=new Rowtblviewdetailbayargrid;

        $row->KP_PRODUK     = $rec->kp_produk;
        $row->KD_PRODUK     = $rec->kd_produk;
        $row->DESKRIPSI     = $rec->deskripsi;
        $row->KD_TARIF      = $rec->kd_tarif;
        $row->DESKRIPSI2    = $rec->deskripsi;
        $row->HARGA         = $rec->harga;
        $row->FLAG          = $rec->flag;
        $row->QTY           = $rec->qty;
        $row->TGL_BERLAKU   = $rec->tgl_berlaku;
        $row->NO_TRANSAKSI  = $rec->no_transaksi;
        $row->URUT          = $rec->urut;
        $row->ADJUST        = $rec->adjust;
        $row->KD_DOKTER     = $rec->kd_dokter;
        $row->KD_UNIT       = $rec->kd_unit;
        $row->CITO          = $rec->cito;
        $row->KD_CUSTOMER   = $rec->kd_customer;
        $row->TGL_TRANSAKSI = $rec->tgl_transaksi;
        //$row->TOTAL       = $rec->total;
        $row->BAYARTR       = $rec->tunai;
        $row->PIUTANG       = $rec->piutang;
        $row->DISCOUNT      = $rec->dicount;
        if($rec->tunai > 0){
            $row->TAG =true;
        }else{
            $row->TAG =false;
        }
		/*if($rec->tag=='t'){
			$row->TAG =false;
		}else{
			$row->TAG =true;
		}*/
		//$row->DISCOUNT = $rec->type_data;
        return $row;
    }

}

class Rowtblviewdetailbayargrid
{

    public $KP_PRODUK;
    public $KD_PRODUK;
    public $DESKRIPSI;
    public $KD_TARIF;
    public $DESKRIPSI2;
    public $HARGA;
    public $FLAG;
    public $QTY;
    public $TGL_BERLAKU;
    public $NO_TRANSAKSI;
    public $URUT;
    public $ADJUST;
    public $KD_DOKTER;
    public $KD_UNIT;
    public $CITO;
    public $KD_CUSTOMER;
	public $TGL_TRANSAKSI;
	//public $TOTAL;
		public $BAYARTR;
		public $DISCOUNT;
		public $PIUTANG;
		//public $TYPE_DATA;
}

?>
