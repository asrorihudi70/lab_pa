<?php
class tblviewkasirrwjdetail extends TblBase
{
    function __construct()
    {
        $this->TblName='viewgridkasirrwjdetail';
        TblBase::TblBase(true);
		$this->StrSql="kd_produk,deskripsi,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,total,tunai,dicount,Piutang";
		
        $this->SqlQuery= "select * from (
									select     detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user,
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli
									,detail_transaksi.harga * detail_transaksi.qty as total,
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer,  COALESCE((detail_transaksi.harga * detail_transaksi.qty) - detail_bayar.jumlah, detail_transaksi.harga * detail_transaksi.qty )   as tunai, COALESCE((detail_transaksi.harga * detail_transaksi.qty) - detail_bayar.jumlah, detail_transaksi.harga * detail_transaksi.qty )   as Piutang,
									COALESCE((detail_transaksi.harga * detail_transaksi.qty) - detail_bayar.jumlah, detail_transaksi.harga * detail_transaksi.qty )   as dicount
										from  detail_transaksi 
									inner join
									  produk on detail_transaksi.kd_produk = produk.kd_produk 
									  inner join
									  unit on detail_transaksi.kd_unit = unit.kd_unit 
									  left join
									  customer on detail_transaksi.kd_customer = customer.kd_customer 
									  left  join
									  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
									  left join detail_bayar on detail_transaksi.urut = detail_bayar.urut And  detail_transaksi.tgl_transaksi = detail_bayar.tgl_transaksi AND detail_transaksi.kd_kasir = detail_bayar.kd_kasir 	
									) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirrwjdetail;

        $row->KD_PRODUK = $rec->kd_produk;
        $row->DESKRIPSI = $rec-> deskripsi;
        $row->KD_TARIF = $rec->kd_tarif;
        $row->DESKRIPSI2 = $rec->deskripsi;
        $row->HARGA = $rec->harga;
        $row->FLAG = $rec->flag;
        $row->QTY = $rec->qty;
        $row->TGL_BERLAKU = $rec->tgl_berlaku;
        $row->NO_TRANSAKSI= $rec->no_transaksi;
        $row->URUT = $rec->urut;
        $row->ADJUST = $rec->adjust;
        $row->KD_DOKTER = $rec->kd_dokter;
        $row->KD_UNIT= $rec->kd_unit;
        $row->CITO = $rec->cito;
        $row->KD_CUSTOMER = $rec->kd_customer;
		$row->TGL_TRANSAKSI = $rec->tgl_transaksi;
		$row->TOTAL = $rec->total;
		$row->BAYARTR = $rec->tunai;
		$row->PIUTANG = $rec->piutang;
		$row->DISCOUNT = $rec->dicount;
        return $row;
    }

}

class Rowtblviewkasirrwjdetail
{

    public $KD_PRODUK;
    public $DESKRIPSI;
    public $KD_TARIF;
    public $DESKRIPSI2;
    public $HARGA;
    public $FLAG;
    public $QTY;
    public $TGL_BERLAKU;
    public $NO_TRANSAKSI;
    public $URUT;
    public $ADJUST;
    public $KD_DOKTER;
    public $KD_UNIT;
    public $CITO;
    public $KD_CUSTOMER;
	public $TGL_TRANSAKSI;
	public $TOTAL;
		public $BAYARTR;
		public $DISCOUNT;
		public $PIUTANG;
}

?>
