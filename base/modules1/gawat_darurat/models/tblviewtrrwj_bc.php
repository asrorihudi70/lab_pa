﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewtrrwj extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="customer,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,urut_masuk'";
		$this->SqlQuery="select  * from (
											
											select customer.customer,unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, dokter.nama as nama_dokter, 
											kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist, 
											knt.jenis_cust  
											from (((((unit 
											inner join kunjungan on kunjungan.kd_unit=unit.kd_unit)   
											inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
											inner join customer on customer.kd_customer= kunjungan.kd_customer)
											left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
											  
											inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
											and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk )as resdata ";
		$this->TblName='viewkasirrwj';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->KD_PASIEN=$rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI=$rec->tgl_transaksi;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
		
		return $row;
	}
}
class Rowdokter
{
	public $KD_UNIT;
    public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
}

?>