<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_cmb_dokter extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('general/Model_cmb_dokter');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function getCmbDefault(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params 	= array(

		);

		$result = $this->Model_cmb_dokter->getDataPerawat($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
			$resultData[$x]['NAMA']  		= $data['nama'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function getCmbDokterSpesialisasi(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$criter_unit = array(
			'kd_unit' 	=> $this->input->post('kd_unit'),
		);
		$params_unit = $this->Model_cmb_dokter->getDataUnit($criter_unit);
		$params 	= array(
			// 'dokter_inap.kd_unit' => $params_unit->row()->parent,
			//'dokter.jenis_dokter' => $this->input->post('jenis_dokter'),
			'dokter.status'       => 1,
		);

		if ($criter_unit['kd_unit'] != null) {
			if ($criter_unit['kd_unit'] == '711' || $criter_unit['kd_unit'] == '712' || $criter_unit['kd_unit'] == '713') {
				$params['dokter_inap.kd_unit'] = $this->input->post('kd_unit');
			}else{
				$params['dokter_inap.kd_unit'] = $params_unit->row()->parent;
			}

			if ($this->input->post('kd_job') == '2' || $this->input->post('kd_job') == '1') {
				$params['dokter.jenis_dokter'] = 1;
			}else{
				$params['dokter.jenis_dokter'] = 0;
			}
			
			$criteria_not_in = array(
				'kd_kasir'      => $this->input->post('kd_kasir'),
				'no_transaksi'  => $this->input->post('no_transaksi'),
				'urut'          => $this->input->post('urut'),
				'kd_unit'       => $params_unit->row()->parent,
				'kd_job'        => $this->input->post('kd_job'),
			);
			$params_not_in = array(
				'dokter_inap.kd_dokter' 	=> $this->Model_cmb_dokter->criteriaVisiteDokter($criteria_not_in),
			);

			$tmpNamaDokter = $this->input->post('txtDokter');

			$result = $this->Model_cmb_dokter->getDataDokterSpesial_revisi($params, $params_not_in);
			// unset($resultData);
			foreach ($result->result_array() as $data) {

				if (strlen($tmpNamaDokter)>0) {
					//$tmpNamaDokter = strtolower($data['nama']);

					if (stripos(strtolower($data['nama']), strtolower($tmpNamaDokter)) !== false) {
						$tmpNama = $data['nama'];
						$text = str_replace(strtolower($tmpNamaDokter), "<b>".strtolower($tmpNamaDokter)."</b>", strtolower($tmpNama));
						$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
						$resultData[$x]['NAMA']  		= $text;
						$x++;
					}else if(stripos($data['kd_dokter'], $tmpNamaDokter) !== false && is_numeric($tmpNamaDokter)){
						$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
						$resultData[$x]['NAMA']  		= $data['nama'];
						$x++;
					}
				}else{
					$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
					$resultData[$x]['NAMA']  		= $data['nama'];
					$x++;
				}
			}
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}


	public function getCmbDokterInapInt(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();

		$params 	= array(
			//'kd_spesial' => $this->input->post('kd_spesial'),
			'groups'     => $this->input->post('groups'),
		);
		$result = $this->Model_cmb_dokter->getDataDokterInapInt($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_JOB']       = $data['kd_job'];
			$resultData[$x]['KD_COMPONENT'] = $data['kd_component'];
			$resultData[$x]['LABEL']        = $data['label'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

	public function getCmbDokterVisite(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params 	= array(
			'kd_kasir'  	=> $this->input->post('kd_kasir'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'tag_int'       => $this->input->post('kd_produk'),
			'urut'       	=> $this->input->post('urut'),
		);

		$result = $this->Model_cmb_dokter->getDataDokterVisite($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_DOKTER'] = $data['kddokter'];
			$resultData[$x]['NAMA']      = $data['nama'];
			$resultData[$x]['JP']        = ($data['jp']-$data['vd_disc'])+$data['vd_markup'];
			$resultData[$x]['PERC']      = $data['prc'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}

}


?>
