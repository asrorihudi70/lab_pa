<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_cmb_perawat extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('general/Model_cmb_perawat');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function getCmbPerawat(){
		$x 			= 0;
		$resultData = array();
		$response 	= array();
		$params 	= array(
			'kd_kasir'      => $this->kd_kasir,
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'kd_pasien'     => $this->input->post('no_medrec'),
			'tgl_transaksi' => date('Y-m-d', strtotime($this->input->post('tgl_transaksi'))),
			'tgl_masuk'     => date('Y-m-d', strtotime($this->input->post('tgl_masuk'))),
			'kd_job'        => $this->input->post('kd_job'),
			'kd_unit_kamar' => $this->input->post('kd_unit_kamar'),
		);

		$result = $this->Model_cmb_perawat->getDataPerawat($params);
		foreach ($result->result_array() as $data) {
			$resultData[$x]['KD_DOKTER'] 	= $data['kd_dokter'];
			$resultData[$x]['NAMA']  		= $data['nama'];
			$x++;
		}
		$response['data'] = $resultData;
		echo json_encode($response);
	}		
}


?>
