<?php

/**
 * @author
 * @copyright
 */


class pendidikan extends MX_Controller {
    public function __construct(){
        //parent::Controller();
        parent::__construct();
        $this->load->model("general/m_pendidikan");
    }

    public function index(){
        $this->load->view('main/index');
    }

    public function combo($params = null){
        $query = $this->m_pendidikan->select($params);

        $data = array();
        foreach ($query->result() as $result) {
            $tmp_data = array();
            $tmp_data['KD_PENDIDIKAN'] = $result->kd_pendidikan;
            $tmp_data['PENDIDIKAN']    = $result->pendidikan;
            array_push($data, $tmp_data);
        }
        echo '{success:true, totalrecords:'.$query->num_rows().', ListDataObj:'.json_encode($data).'}';
    }
}



?>