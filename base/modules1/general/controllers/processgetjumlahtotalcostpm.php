<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class processgetjumlahtotalcostpm extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');		
    }

    public function read($Params) 
    {

        $this->load->model('pm/tblam_wo_pm_service');
        //$prm='';
        $dblJumlah = 0;
        $dblJumlah2 = 0;
        //$Total=0;
        $criPerson = "";
        $criPart = "";
        $strPart='';
        
        if (strlen($Params)>0)
        {
            $prm=explode('@^@',$Params);

            if (strlen($prm[0]) > 0 )
            {
                $criPerson=$prm[0];
                $criPerson = str_replace("~" ,"'",$criPerson);
                //$criteria = str_replace("@^@" ," ",$criteria);
                $criPerson = str_replace("where" ," ",$criPerson);
            }

            if (strlen($prm[1])> 0 )
            {
                $criPart = $prm[1];
                $criPart = str_replace("~" ,"'",$criPart);
                //$criteria = str_replace("@^@" ," ",$criteria);
                $criPart = str_replace("where" ," ",$criPart);
                
                $this->tblam_wo_pm_service->db->where($criPart, null, false);
                $query = $this->tblam_wo_pm_service->GetRowList( 0, 1000, "", "", "");

                if ($query[1] > 0)
                {
                    if ($query[1]==1)
                    {
                        $strPart = $strPart."'".$query[0][0]->SERVICE_ID."'";
                    } else {
                        for ($i=0;$i < $query[1];$i++)
                        {
                            if ($i < $query[1])
                            {
                                $strPart = $strPart."'".$query[0][$i]->SERVICE_ID."',";
                            } else $strPart = $strPart."'".$query[0][$i]->SERVICE_ID."'";
                        }
                    }
                    
//                    foreach ($query[0] as $x)
//                    {
//                        $strPart = $strPart."'".$x->SERVICE_ID."',";
//                    }
                }
            }            
        }

        //$Sql=$this->db;

        if (strlen($criPerson)>0 )
        {
            $this->load->model('pm/tblam_wo_pm_person');
            $this->tblam_wo_pm_person->db->where($criPerson,null,false);
            //$Sql->where($criPerson,null,false);
            //$res = $this->tblam_wo_pm_person->GetRowList('','');
            $res = $this->tblam_wo_pm_person->GetRowList(0,1000,'','','');
            if ($res[1]!=='0')
            {
                foreach ($res[0] as $x)
                {
                    $dblJumlah +=$x->COST;
                }
            }

            if (strlen($strPart)> 0)
            {
                $this->load->model('setup/tblam_service_parts');
                //$Sql->where("service_id in ( ".$strPart." )",null,false);
                $this->tblam_service_parts->db->where("service_id in ( ".$strPart." )",null,false);
                //$res = $this->tblam_service_parts->GetRowList('','');
                $res = $this->tblam_service_parts->GetRowList(0,1000,'','','');
                if ($res[1]!=='0')
                {
                    foreach ($res[0] as $x)
                    {
                        $dblJumlah2 +=$x->TOT_COST;
                    }
                }
            }
        } elseif (strlen($strPart)>0) {
            $this->load->model('setup/tblam_service_parts');
            $this->tblam_service_parts->db->where("service_id in ( ".$strPart." )",null,false);
            $res = $this->tblam_service_parts->GetRowList(0,1000,'','','');
            if ($res[1]!=='0')
            {
                foreach ($res[0] as $x)
                {
                    $dblJumlah2 +=$x->TOT_COST;
                }
            }
        }

        //$dblJumlah+=$Total;
        $total = $dblJumlah + $dblJumlah2;
        $res = '{success: true, TotalCost: '.$total.'}';
	echo $res;
    }


}



?>