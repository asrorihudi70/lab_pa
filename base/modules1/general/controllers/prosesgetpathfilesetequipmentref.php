<?php
class prosesgetpathfilesetequipmentref extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
       $this->load->view('main/index');
    }

    public function read($Params)
    {

        $this->load->model('general/tblam_reference');

        if (strlen($Params)>0)
        {
            $criteria = str_replace("~" ,"'",$Params);
            $this->tblam_reference->db->where($criteria,null,false);
        }

        $query = $this->tblam_reference->GetRowList( 0, 1, "", "", "");

        if ($query[1] > 0)
        {
            //Dim res = New With {.success = True, .Path = a.PATH_IMAGE, .RefId = a.REFF_ID}
            $res = '{success: true, Path: "'.$query[0][0]->PATH_IMAGE.'", RefId: "'.$query[0][0]->REFF_ID.'"}';

        } else $res = '{success: false}';


	echo $res;
    }
}

?>
