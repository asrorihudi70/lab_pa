<?php
/**
 * @author Ali
 * @copyright NCI 2011
 */


class prosesgettotalhistory extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
       $this->load->view('main/index');
    }

    public function read($Params)
    {

        $this->load->model('general/tblvihistoryasset');

        if (strlen($Params)>0)
        {
            $criteria = str_replace("~" ,"'",$Params);
            $this->tblvihistoryasset->db->where($criteria, null, false);
        }

        $query = $this->tblvihistoryasset->GetRowList( 0, 1000, "", "", "");

        if ($query[1] > 0)
        {
            $dblJumlah = 0;
            
            foreach ($query[0] as $x)
            {
                $dblJumlah += $x->LAST_COST;
            }
            //Dim res = New With {.success = True, .Total = dblJumlah}

            $res = '{success: true, Total: '.$dblJumlah.'}';
        }
        else $res = '{success: false}';

	echo $res;
    }
}

?>
