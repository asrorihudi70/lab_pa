<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class prosesgetvendorpmworkorder extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
    
    }	 

    public function index()
    {
            $this->load->view('main/index');            
    }
	     
    public function read($Params) 
    {    			
		
         $this->load->model('pm/tblam_wo_pm_service');

        if (strlen($Params)>0)
        {
            $criteria = str_replace("~" ,"'",$Params);
            $criteria = str_replace("@^@" ," ",$criteria);
            $criteria = str_replace("where" ," ",$criteria);
            $this->db->where($criteria,null,false);
        }

        $query = $this->tblam_wo_pm_service->GetRowList( 0, 1000, "", "", "");

        if ($query[1] > 0)
        {
            $dblJumlah = 0;
            
            foreach ($query[0] as $x)
            {
                $dblJumlah += $x->LAST_COST;
            }
            //Dim res = New With {.success = True, .Total = dblJumlah}

            $res = '{success: true, Total: '.$dblJumlah.'}';
        }
        else $res = '{success: false}';

	echo $res;
    }
}



?>