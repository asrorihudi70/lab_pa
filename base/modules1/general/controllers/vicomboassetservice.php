<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class vicomboassetservice extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
   {
        $this->load->model('general/tblvicomboassetservice');

        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblvicomboassetservice->db->where($criteria." OR asset_maint_id = 'xxx'", null, false);
        }

        $query = $this->tblvicomboassetservice->GetRowList( $Params[0], $Params[1], $Params[3], $Params[2], "");

        //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';

   }

}


?>
