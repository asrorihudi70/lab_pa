<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewlookupproduk extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }	 


    public function index()
    {
        $this->load->view('main/index');
    }
   
   public function read($Params=null)
   {

      if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
           // $this->tbl_kamar->db->where($criteria, null, false);
        }
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
		
			$kamar_all=array();
			$query=$this->db->query("select  
														row_number() OVER () as rnum,
														rn.jumlah,
														rn.manual, 
														rn.kp_produk,
														 rn.kd_kat,
														 rn.kd_klas,
														rn.klasifikasi, 
														rn.parent,
														rn.kd_tarif,
														rn.kd_produk,
														rn.deskripsi,
														rn.kd_unit,
														rn.nama_unit,
														 (rn.tglberlaku),
														rn.tarifx,
														rn.tgl_berakhir
														

														from(
														Select tr.jumlah,produk.manual, 
														produk.kp_produk,
														 produk.kd_kat, produk.kd_klas,
														klas_produk.klasifikasi, klas_produk.parent,
														tarif.kd_tarif,
														tarif.tgl_berakhir,
														produk.kd_produk,
														produk.deskripsi,
														tarif.kd_unit,
														unit.nama_unit,
														max (tarif.tgl_berlaku) as tglberlaku,
														tarif.tarif as tarifx,
														row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn
														From tarif 
														inner join produk on produk.kd_produk = tarif.kd_produk
														 inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
														 inner join unit on tarif.kd_unit = unit.kd_unit
														 inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
														 
														 left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,
														kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
														where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  
														group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk 
														AND tr.tgl_berlaku = tarif.tgl_berlaku  AND tr.kd_tarif=tarif.kd_tarif
														 
														 
														Where 
														$criteria and tarif.tgl_berlaku  <= '". date('Y-m-d') ."'
														group by 
														tarif.tgl_berlaku, produk.kd_produk,produk.manual, 
														produk.kp_produk,tarif.kd_unit,
														 produk.kd_kat, produk.kd_klas,tr.jumlah,
														klas_produk.klasifikasi, klas_produk.parent ,unit.nama_unit, produk.deskripsi,tarif.tgl_berakhir,
														tarif.kd_tarif,tarif.tarif order by produk.kd_produk asc
														) as rn 
														where rn = 1 order by rn.deskripsi asc ")->result();
			for($aj=0; $aj<count($query); $aj++)
			{
	

			$kamar_all1=array();
			$kamar_all1['TARIF']=$query[$aj]->tarifx;
			$kamar_all1['KLASIFIKASI']=$query[$aj]->klasifikasi;
			$kamar_all1['PERENT']=$query[$aj]->parent;
			
			$kamar_all1['TGL_BERAKHIR']=$query[$aj]->tgl_berakhir;
			$kamar_all1['KD_KAT']=$query[$aj]->kd_kat;
			$kamar_all1['KD_TARIF']=$query[$aj]->kd_tarif;
			
			
			
			$kamar_all1['KD_KLAS']=$query[$aj]->kd_klas;
			$kamar_all1['DESKRIPSI']=$query[$aj]->deskripsi;
			
			$kamar_all1['NAMA_UNIT']=$query[$aj]->nama_unit;
			$kamar_all1['KD_PRODUK']=$query[$aj]->kd_produk;
			$kamar_all1['TGL_BERLAKU']=$query[$aj]->tglberlaku;
			
			$kamar_all1['JUMLAH']=$query[$aj]->jumlah;
			
			$kamar_all[]=$kamar_all1;
			}
			echo'{success:true, totalrecords: '.count($query).', ListDataObj:'.json_encode($kamar_all).'}';
			   		   		   	
   }
   
}



?>