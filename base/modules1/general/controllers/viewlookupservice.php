<?php
class viewlookupservice extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }

   public function read($Params=null)
   {
        $this->load->model('general/tblam_service');

        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblam_service->db->where($criteria,null,false);
        }
        $query = $this->tblam_service->GetRowList( $Params[0], $Params[1], $Params[3], $Params[2], "");

        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';

   }

}


?>
