<?php

class autocharge extends TblBase
{
    function __construct()
    {
        
        $this->TblName='viewkunjungan';
        TblBase::TblBase(true);

        $this->SqlQuery= "Select AutoCharge.*,Produk.kp_produk,Produk.Kd_Klas
                          From AutoCharge inner join produk on produk.kd_produk=autoCharge.kd_Produk";
    }

    function FillRow($rec)
    {
        $row=new Rowviewautocharge;

          $row->KD_UNIT=$rec->kd_unit;
          $row->KD_PRODUK=$rec->kd_produk;
          $row->APPTO=$rec->appto;
          $row->SHIFT=$rec->shift;
          $row->KP_PRODUK=$rec->kp_produk;
          $row->KD_KLAS=$rec->kd_klas;
        return $row;
    }

}

class Rowviewautocharge
{
        public $KD_UNIT;
        public $KD_PRODUK;
        public $APPTO;
        public $SHIFT;
        public $KP_PRODUK;
        public $KD_KLAS;
}

?>
