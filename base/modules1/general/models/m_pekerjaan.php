<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class m_pekerjaan extends Model
{
	private $table = "pekerjaan";
	public function select($criteria = null){
		$this->db->select("*");
		if ($criteria != null) {
			$this->db->where($criteria);
		}
		$this->db->from($this->table);
		$this->db->order_by('pekerjaan', 'ASC');
		return $this->db->get();
	}
}

?>