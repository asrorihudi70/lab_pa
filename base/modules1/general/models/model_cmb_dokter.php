<?php 	
class model_cmb_dokter extends Model
{
	private $tbl_dokter      	= "dokter";
	private $tbl_dokter_spesial = "dokter_spesial";
	private $tbl_dokter_visite  = "visite_dokter";
	private $tbl_dokter_klinik 	= "dokter_klinik";
	private $tbl_pasien_inap_int= "dokter_inap_int";
	private $tbl_pasien_inap 	= "dokter_inap";
	private $tbl_unit 			= "unit";
	private $id_user         = "";
	private $dbSQL           = "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		======================================================================================================================
		======================================================== SQL QUERY ===================================================
		======================================================================================================================
	*/

		public function getDataDefaultSQL($criteria){
			$result = false;
			try {
				$result = $this->dbSQL->query("SELECT d.* FROM DOKTER d 
					INNER JOIN DOKTER_INAP i ON d.Kd_Dokter = i.Kd_Dokter 
					WHERE i.Kd_unit in (  
					select PARENT  from UNIT 
						where KD_UNIT in (  
							select top 1 KD_UNIT_KAMAR from NGINAP  where KD_PASIEN  = '".$criteria['kd_pasien']."'   and TGL_MASUK ='".$criteria['tgl_masuk']."'   and AKHIR =1 
						) 
					) 
					AND i.Kd_Dokter NOT IN ( SELECT Kd_Dokter FROM VISITE_DOKTER WHERE Kd_Kasir = '".$criteria['kd_kasir']."' AND No_Transaksi = '".$criteria['no_transaksi']."' AND Urut = 0 
					AND Tgl_Transaksi = '".$criteria['tgl_transaksi']."' AND Kd_Unit = '".$criteria['kd_unit_kamar']."' AND kd_Job = '".$criteria['kd_job']."' )
					and d.jenis_dokter =0 ORDER BY Nama ");
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}


		public function getDataDokterSpesialSQL($criteria){
			$result = false;
			try {
				$this->dbSQL->select(" dokter.kd_dokter as kd_dokter, dokter.nama as nama ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_dokter);
				$this->dbSQL->join($this->tbl_dokter_spesial, $this->tbl_dokter_spesial.".kd_dokter = ".$this->tbl_dokter.".kd_dokter ","INNER");
				$result = $this->dbSQL->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataDokterKlinik($criteria){
			$result = false;
			try {
				$this->db->select(" distinct(dokter.kd_dokter) as kd_dokter, dokter.nama as nama, dokter.jenis_dokter ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_dokter);
				$this->db->join($this->tbl_dokter_klinik, $this->tbl_dokter_klinik.".kd_dokter = ".$this->tbl_dokter.".kd_dokter ","INNER");
				$this->db->order_by("dokter.nama","ASC");
				$result = $this->db->get();
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataDokterSpesialSQL_revisi($criteria, $criteria_not_in){
			$result = false;
			try {
				$this->dbSQL->select(" dokter.kd_dokter as kd_dokter, dokter.nama as nama ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				if (isset($criteria_not_in)) {
					$this->dbSQL->where_not_in($criteria_not_in);
				}
				$this->dbSQL->from($this->tbl_dokter);
				$this->dbSQL->join($this->tbl_pasien_inap, $this->tbl_pasien_inap.".kd_dokter = ".$this->tbl_dokter.".kd_dokter ","INNER");
				$result = $this->dbSQL->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
		public function getDataDokterSpesial_revisi($criteria, $criteria_not_in){
			$result = false;
			try {
				$this->db->select(" dokter.kd_dokter as kd_dokter, dokter.nama as nama ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				if (isset($criteria_not_in)) {
					$this->db->where_not_in($criteria_not_in);
				}
				$this->db->from($this->tbl_dokter);
				$this->db->join($this->tbl_pasien_inap, $this->tbl_pasien_inap.".kd_dokter = ".$this->tbl_dokter.".kd_dokter ","INNER");
				$this->db->order_by($this->tbl_dokter.".nama" , "ASC");
				$result = $this->db->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataDokterVisitelSQL($criteria){
			$result = false;
			try {
				$this->dbSQL->select("*, dokter.kd_dokter as kddokter, dokter.nama as nama, visite_dokter.jp as jp, visite_dokter.prc as prc ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_dokter_visite);
				$this->dbSQL->join($this->tbl_dokter, $this->tbl_dokter.".kd_dokter = ".$this->tbl_dokter_visite.".kd_dokter ","INNER");
				$result = $this->dbSQL->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataDokterInapIntSQL($criteria){
			$result = false;
			try {
				$this->dbSQL->select("*, KD_JOB as kd_job, KD_COMPONENT as kd_component, LABEL as label ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_pasien_inap_int);
				$result = $this->dbSQL->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataUnitSQL($criteria){
			$result = false;
			try {
				$this->dbSQL->select("*, KD_UNIT as kd_unit, NAMA_UNIT as nama_unit, PARENT as parent ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_unit);
				$result = $this->dbSQL->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function criteriaVisiteDokterSQL($criteria){
			$result = false;
			$var    = array();
			try {
				$this->dbSQL->select(" KD_DOKTER as kd_dokter ");
				if (isset($criteria)) {
					$this->dbSQL->where($criteria);
				}
				$this->dbSQL->from($this->tbl_dokter_visite);
				$result = $this->dbSQL->get();

				foreach ($result->result_array() as $data) {
					array_push($var,$data['kd_dokter']);
					//$var = "'".$data['kd_dokter']."',".$var;
				}

				//$var = substr($var, 0,-1);
				
			} catch (Exception $e) {
				$result = false;
			}
			return $var;
		}
	/*
		======================================================================================================================
		==================================================== END SQL QUERY ===================================================
		======================================================================================================================
	*/
	/*
		======================================================================================================================
		==================================================== PG QUERY ===================================================
		======================================================================================================================
	*/

		public function getDataDokterInapInt($criteria){
			$result = false;
			try {
				$this->db->select("*, kd_job as kd_job, kd_component as kd_component, label as label ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_pasien_inap_int);
				$result = $this->db->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function getDataUnit($criteria){
			$result = false;
			try {
				$this->db->select(" * ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_unit);
				$result = $this->db->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function criteriaVisiteDokter($criteria){
			$result = false;
			$var    = array();
			try {
				$this->db->select(" kd_dokter ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_dokter_visite);
				$result = $this->db->get();

				foreach ($result->result_array() as $data) {
					array_push($var,$data['kd_dokter']);
					//$var = "'".$data['kd_dokter']."',".$var;
				}

				//$var = substr($var, 0,-1);
				
			} catch (Exception $e) {
				$result = false;
			}
			return $var;
		}
		
		public function getDataDokterVisite($criteria){
			$result = false;
			try {
				$this->db->select("*, dokter.kd_dokter as kddokter, dokter.nama as nama, visite_dokter.jp as jp, visite_dokter.prc as prc ");
				if (isset($criteria)) {
					$this->db->where($criteria);
				}
				$this->db->from($this->tbl_dokter_visite);
				$this->db->join($this->tbl_dokter, $this->tbl_dokter.".kd_dokter = ".$this->tbl_dokter_visite.".kd_dokter ","INNER");
				$result = $this->db->get();
				
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
}
?>