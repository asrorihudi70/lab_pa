<?php 	
class Model_cmb_perawat extends Model
{
	private $tbl_dokter      = "dokter";
	private $tbl_dokter_inap = "dokter_inap";
	private $tbl_kunjungan   = "kunjungan";
	private $tbl_pasien_inap = "pasien_inap";
	private $id_user         = "";
	private $dbSQL           = "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		======================================================================================================================
		======================================================== SQL QUERY ===================================================
		======================================================================================================================
	*/
		public function getDataPerawatSQL($criteria){
			$result = false;
			try {
				$result = $this->dbSQL->query("SELECT d.* FROM DOKTER d 
					INNER JOIN DOKTER_INAP i ON d.Kd_Dokter = i.Kd_Dokter 
					WHERE i.Kd_unit in (  
					select PARENT  from UNIT 
						where KD_UNIT in (  
							select top 1 KD_UNIT_KAMAR from NGINAP  where KD_PASIEN  = '".$criteria['kd_pasien']."'   and TGL_MASUK ='".$criteria['tgl_masuk']."'   and AKHIR =1 
						) 
					) 
					AND i.Kd_Dokter NOT IN ( SELECT Kd_Dokter FROM VISITE_DOKTER WHERE Kd_Kasir = '".$criteria['kd_kasir']."' AND No_Transaksi = '".$criteria['no_transaksi']."' AND Urut = 0 
					AND Tgl_Transaksi = '".$criteria['tgl_transaksi']."' AND Kd_Unit = '".$criteria['kd_unit_kamar']."' AND kd_Job = '".$criteria['kd_job']."' )
					and d.jenis_dokter =0 ORDER BY Nama ");
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
	/*
		======================================================================================================================
		==================================================== END SQL QUERY ===================================================
		======================================================================================================================
	*/

		public function getDataPerawat($criteria){
			$result = false;
			try {
				$result = $this->db->query("SELECT d.* FROM DOKTER d 
					INNER JOIN DOKTER_INAP i ON d.Kd_Dokter = i.Kd_Dokter 
					WHERE i.Kd_unit in (  
					select PARENT  from UNIT 
						where KD_UNIT in (  
							select KD_UNIT_KAMAR from NGINAP  where KD_PASIEN  = '".$criteria['kd_pasien']."'   and TGL_MASUK ='".$criteria['tgl_masuk']."' and AKHIR ='t' limit 1
						) 
					) 
					AND i.Kd_Dokter NOT IN ( SELECT Kd_Dokter FROM VISITE_DOKTER WHERE Kd_Kasir = '".$criteria['kd_kasir']."' AND No_Transaksi = '".$criteria['no_transaksi']."' AND Urut = 0 
					AND Tgl_Transaksi = '".$criteria['tgl_transaksi']."' AND Kd_Unit = '".$criteria['kd_unit_kamar']."' AND kd_Job = '".$criteria['kd_job']."' )
					and d.jenis_dokter =0 ORDER BY Nama ");
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}
}
?>