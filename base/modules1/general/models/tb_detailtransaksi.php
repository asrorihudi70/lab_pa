<?php

class tb_detailtransaksi extends TblBase
{
    function __construct()
    {

        $this->TblName='detail_transaksi';
        TblBase::TblBase(true);

        $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowviewtb_detailtransaksi;
         $row->KD_TRANSAKSI=$rec->no_transaksi;
         $row->KD_KASIR=$rec->kd_kasir;
         $row->TGL_TRANSAKSI=$rec->tgl_transaksi;
         $row->URUT=$rec->urut;
         $row->KD_TARIF=$rec->kd_tarif;
         $row->KD_PRODUK=$rec->kd_produk;
         $row->KD_UNIT=$rec->kd_unit;
         $row->TGL_BERLAKU=$rec->tgl_berlaku;
         $row->KD_USER=$rec->kd_user;
         $row->SHIFT=$rec->shift;
         $row->HARGA=$rec->harga;
         $row->QTY=$rec->qty;
         $row->FOLIO=$rec->folio;
         $row->TAG=$rec->tag;
         $row->KD_CUSTOMER=$rec->Kd_customer;
         $row->HRG_ASLI=$rec->hrg_asli;
         $row->KD_LOKET=$rec->kd_loket;
        return $row;
    }

}

class Rowviewtb_detailtransaksi
{
         public $KD_TRANSAKSI;
         public $KD_KASIR;
         public $TGL_TRANSAKSI;
         public $URUT;
         public $KD_TARIF;
         public $KD_PRODUK;
         public $KD_UNIT;
         public $TGL_BERLAKU;
         public $KD_USER;
         public $SHIFT;
         public $HARGA;
         public $QTY;
         public $FOLIO;
         public $TAG;
         public $KD_CUSTOMER;
         public $HRG_ASLI;
         public $KD_LOKET;
}

?>
