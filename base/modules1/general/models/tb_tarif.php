<?php

class tb_tarif extends TblBase
{
    function __construct()
    {

        $this->TblName='tarif';
        TblBase::TblBase(true);

        $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowviewtarif;

          $row->KD_TARIF=$rec->kd_tarif;
          $row->KD_PRODUK=$rec->kd_produk;
          $row->KD_UNIT=$rec->kd_unit;
          $row->TGL_BERLAKU=$rec->tgl_berlaku;
          $row->TARIF=$rec->tarif;
          $row->TGL_BERAKHIR=$rec->tgl_berakhir;
        return $row;
    }

}

class Rowviewtarif
{
        public $KD_TARIF;
        public $KD_PRODUK;
        public $KD_UNIT;
        public $TGL_BERLAKU;
        public $TARIF;
        public $TGL_BERAKHIR;
}

?>
