<?php

class tb_unit_asal extends TblBase
{
    function __construct()
    {
        $this->TblName='unit_asal';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewunitasal;

          $row->KD_KASIR=$rec->kd_kasir;
          $row->NO_TRANSAKSI=$rec->no_transaksi;
          $row->NO_TRANSAKSI_ASAL=$rec->no_transaksi_asal;
          $row->KD_KASIR_ASAL=$rec->kd_kasir_asal;
		  $row->ID_ASAL=$rec->id_asal;

          return $row;
    }

}

class Rowviewunitasal
{
    public $KD_KASIR;
    public $NO_TRANSAKSI;
    public $NO_TRANSAKSI_ASAL;
    public $KD_KASIR_ASAL;
	public $ID_ASAL;
}

?>
