<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_customerkontraktor extends TblBase {

    function __construct() {
        $this->StrSql = "kd_customer,customer,jenis_cust";
        $this->SqlQuery = "select kontraktor.kd_customer, kontraktor.jenis_cust, customer.customer 
                            from kontraktor inner join
                            customer on kontraktor.kd_customer = customer.kd_customer 
                            ";
        $this->TblName = 'viewcombokontraktorjoincustomer';
        //TblBase::TblBase();
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowcustomer;
        $row->KD_CUSTOMER = $rec->kd_customer;
        $row->CUSTOMER = $rec->customer;
        $row->JENISCUS = $rec->jenis_cust;
        return $row;
    }

}

class Rowcustomer {

    public $KD_CUSTOMER;
    public $CUSTOMER;
    public $JENISCUS;

}

?>