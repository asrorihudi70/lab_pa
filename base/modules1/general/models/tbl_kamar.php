<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_kamar extends TblBase
{

	function __construct()
	{
		$this->StrSql="kd_unit,no_kamar,nama_kamar,jumlah_bed,digunakan";
		TblBase::TblBase(true);
		$this->TblName='kamarxx';
		$this->SqlQuery= "select * from kamar ";
        
	}


	function FillRow($rec)
	{
		$row=new RowKamar;
		$row->KD_UNIT=$rec->kd_unit;
		$row->NO_KAMAR=$rec->no_kamar;
        $row->NAMA_KAMAR=$rec->nama_kamar;
		$row->JUMLAH_BED=$rec->jumlah_bed;
        $row->DIGUNAKAN=$rec->digunakan;

		return $row;
	}
}
class RowKamar
{
    public $KD_UNIT;
    public $NO_KAMAR;
    public $NAMA_KAMAR;
    public $JUMLAH_BED;
    public $DIGUNAKAN;
}

?>
