<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_kelasspesial extends TblBase
{
        function __construct()
        {
        $this->TblName='spc_kelas';
        TblBase::TblBase(true);

        $this->SqlQuery= "select sk.kd_kelas, kelas 
                            from spc_kelas sk
                            inner join kelas k ON k.kd_kelas = sk.kd_kelas";
        }

	function FillRow($rec)
	{
		$row=new RowKelasSpesial;
		$row->KD_KELAS=$rec->kd_kelas;
		$row->KELAS=$rec->kelas;

		return $row;
	}
}
class RowKelasSpesial
{
    public $KD_KELAS;
    public $KELAS;

}

?>