<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class tbl_ruangan extends TblBase
{
        function __construct()
        {
        $this->TblName='unit';
        TblBase::TblBase(true);

        $this->SqlQuery= "select u.kd_unit, nama_unit as nama, s.kd_spesial, k.kd_kelas
                            from unit u
                            inner join kelas k ON k.kd_kelas = u.kd_kelas
                            inner join spc_unit s On s.kd_unit = u.kd_unit";
        }

	function FillRow($rec)
	{
		$row=new RowRuangan;
		$row->KD_UNIT=$rec->kd_unit;
		$row->NAMA=$rec->nama;
           $row->KD_SPESIAL=$rec->kd_spesial;
		$row->KD_KELAS=$rec->kd_kelas;

		return $row;
	}
}
class RowRuangan
{
    public $KD_UNIT;
    public $NAMA;
    public $KD_SPESIAL;
    public $KD_KELAS;
}

?>