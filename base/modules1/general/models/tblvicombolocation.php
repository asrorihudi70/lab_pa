<?php
class tblvicombolocation extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT location_id, location
            FROM am_location
            UNION
            SELECT 'xxx' AS location_id, ' All' AS location

            ) as resdata";

        $this->TblName='vicombolocation';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblvicombolocation;

        $row->LOCATION_ID = $rec->location_id;
        $row->LOCATION = $rec->location;

        return $row;
    }
}

class Rowtblvicombolocation
{
    public $LOCATION_ID;
    public $LOCATION;
}

?>
