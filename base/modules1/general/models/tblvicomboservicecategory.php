<?php
class tblvicomboservicecategory extends TblBase
{
    function __construct()
    {
        $this->TblName='vicomboservicecategory';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (

            SELECT  am_service.service_id, am_service.service_name, am_service_category.category_id
            FROM am_service_category INNER JOIN
                am_service ON am_service_category.service_id = am_service.service_id

            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblvicomboservicecategory;

        $row->SERVICE_ID=$rec->service_id;
        $row->SERVICE_NAME=$rec->service_name;
        $row->CATEGORY_ID=$rec->category_id;

        return $row;
    }

}

class Rowtblvicomboservicecategory
{

    public $SERVICE_ID;
    public $SERVICE_NAME;
    public $CATEGORY_ID;

}
?>
