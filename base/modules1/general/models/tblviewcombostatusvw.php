<?php

class tblviewcombostatusvw extends TblBase
{
    function __construct()
    {
        $this->TblName='viewcombostatusvw';

        $this->SqlQuery= " select * from ( 
            SELECT status_id, status_name FROM am_status
            UNION 
            SELECT ' 9999' AS status_id, ' All' AS status_name
            ) as resdata ";

        TblBase::TblBase(true);
    }

    function FillRow($rec)
    {
        $row=new Rowviewcombostatusvw;

        $row->STATUS_ID=$rec->status_id;
        $row->STATUS_NAME=$rec->status_name;

        return $row;
    }

}

class Rowviewcombostatusvw
{
    public $STATUS_ID;
    public $STATUS_NAME;
}

?>