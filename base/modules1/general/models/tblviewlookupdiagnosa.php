<?php
class tblviewlookupdiagnosa extends TblBase
{
    function __construct()
    {
        $this->TblName='viewlookupdiagnosa';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
						select kd_penyakit, penyakit, '' as keterangan from penyakit 
            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblviewlookupdiagnosa;
        $row->KD_PENYAKIT=$rec->kd_penyakit;
		 $row->PENYAKIT=$rec->penyakit;
		 $row->KETERANGAN=$rec->keterangan;
		/*
        $row->PERENT=$rec->parent;
        $row->DESKRIPSI=$rec->deskripsi;
        $row->TGL_BERAKHIR=$rec->tgl_berakhir;
        $row->KLASIFIKASI=$rec->klasifikasi;
        $row->KD_KAT=$rec->kd_kat;
        $row->KD_KLAS=$rec->kd_klas;
        $row->NAMA_UNIT=$rec->nama_unit;
        $row->TARIF=$rec->tarif;
        $row->KD_TARIF=$rec->kd_tarif;
		$row->TGL_BERLAKU=$rec->tgl_berlaku;*/
        return $row;
    }

}

class Rowtblviewlookupdiagnosa
{
 public $KD_PENYAKIT;
 public $PENYAKIT;
 public $KETERANGAN;
/*
    public $TARIF;
    public $KLASIFIKASI;
    public $PERENT;
    public $TGL_BERAKHIR;
    public $KD_KAT;
    public $KD_TARIF;
    public $KD_KLAS;
    public $DESKRIPSI;
    public $YEARS;
    public $NAMA_UNIT;
   
	 public $TGL_BERLAKU;*/
}
?>
