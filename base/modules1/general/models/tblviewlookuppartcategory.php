<?php
class tblviewlookuppartcategory extends TblBase
{
    function __construct()
    {
        $this->TblName='viewlookuppartcategory';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
            SELECT am_spareparts.part_id, am_spareparts.part_name,
            am_spareparts.price, am_spareparts.desc_part,
            am_part_category.category_id
            FROM am_part_category
            INNER JOIN am_spareparts ON am_part_category.part_id = am_spareparts.part_id
            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblviewlookuppartcategory;

        $row->PART_ID=$rec->part_id;
        $row->PART_NAME=$rec->part_name;
        $row->PRICE=$rec->price;
        $row->DESC_PART=$rec->desc_part;
        $row->CATEGORY_ID=$rec->category_id;

        return $row;
    }

}

class Rowtblviewlookuppartcategory
{

    public $PART_ID;
    public $PART_NAME;
    public $PRICE;
    public $DESC_PART;
    public $CATEGORY_ID;

}

?>
