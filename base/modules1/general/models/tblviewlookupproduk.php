<?php
class tblviewlookupproduk extends TblBase
{
    function __construct()
    {
        $this->TblName='viewlookupasset';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from ( 
		select distinct produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, max (tarif.tgl_berlaku) as tglberlaku,max(tarif.tarif) as tarifx, tarif.tgl_berakhir from 
		 produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas
		 inner join tarif on produk.kd_produk = tarif.kd_produk 
		 inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
		 inner join unit on tarif.kd_unit = unit.kd_unit 
		 group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,
		 klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir order by produk.deskripsi asc
		 ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblviewlookupproduk;
        $row->KD_PRODUK=$rec->kd_produk;
        $row->PERENT=$rec->parent;
        $row->DESKRIPSI=$rec->deskripsi;
        $row->TGL_BERAKHIR=$rec->tgl_berakhir;
        $row->KLASIFIKASI=$rec->klasifikasi;
        $row->KD_KAT=$rec->kd_kat;
        $row->KD_KLAS=$rec->kd_klas;
        $row->NAMA_UNIT=$rec->nama_unit;
        $row->TARIF=$rec->tarifx;
        $row->KD_TARIF=$rec->kd_tarif;
		$row->TGL_BERLAKU=$rec->tglberlaku;
        return $row;
    }

}

class Rowtblviewlookupproduk
{
    public $TARIF;
    public $KLASIFIKASI;
    public $PERENT;
    public $TGL_BERAKHIR;
    public $KD_KAT;
    public $KD_TARIF;
    public $KD_KLAS;
    public $DESKRIPSI;
    public $YEARS;
    public $NAMA_UNIT;
    public $KD_PRODUK;
	 public $TGL_BERLAKU;
}
?>
