<?php

class tblviewlookupservicecategory extends TblBase
{
    function __construct()
    {
        $this->TblName='viewlookupservicecategory';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
            SELECT  am_service_category.service_id, am_service_category.category_id,
            am_service_category.type_service_id, am_service_category.is_aktif,
            am_service_category.interval, am_service_category.metering_assumptions,
            am_service_category.days_assumptions, am_service_category.flag,
            am_service.service_name
            FROM  am_service_category
            INNER JOIN am_service ON am_service_category.service_id = am_service.service_id

            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblviewlookupservicecategory;

        $row->SERVICE_ID=$rec->service_id;
        $row->CATEGORY_ID=$rec->category_id;
        $row->TYPE_SERVICE_ID=$rec->type_service_id;
        $row->IS_AKTIF=$rec->is_aktif;
        $row->INTERVAL=$rec->interval;
        $row->METERING_ASSUMPTIONS=$rec->metering_assumptions;
        $row->DAYS_ASSUMPTIONS=$rec->days_assumptions;
        $row->FLAG=$rec->flag;
        $row->SERVICE_NAME=$rec->service_name;

        return $row;
    }

}

class Rowtblviewlookupservicecategory
{

    public $SERVICE_ID;
    public $CATEGORY_ID;
    public $TYPE_SERVICE_ID;
    public $IS_AKTIF;
    public $INTERVAL;
    public $METERING_ASSUMPTIONS;
    public $DAYS_ASSUMPTIONS;
    public $FLAG;
    public $SERVICE_NAME;
    
}

?>
