<?php

class tblvihistoryasset extends TblBase
{
    function __construct()
    {
        $this->TblName='vihistoryasset';
        TblBase::TblBase(true);

        $this->SqlQuery= " select * from (
            SELECT  rcd.asset_maint_id, am.asset_maint_name, s.service_id, s.service_name,
            ars.desc_result_cm AS description, ars.finish_date, ars.last_cost,
            'CM' AS jenis, CASE WHEN a.is_ext_repair = true THEN 'External' ELSE 'Internal' END AS repair
            FROM  am_result_cm_service AS rcs
            INNER JOIN am_result_cm AS ars ON rcs.result_cm_id = ars.result_cm_id
            INNER JOIN am_work_order_cm AS awoc ON ars.wo_cm_id = awoc.wo_cm_id
            INNER JOIN am_schedule_cm AS scc ON awoc.sch_cm_id = scc.sch_cm_id
            INNER JOIN am_approve_cm AS a ON scc.app_id = a.app_id
            INNER JOIN am_request_cm_detail AS rcd ON a.req_id = rcd.req_id AND a.row_req = rcd.row_req
            INNER JOIN am_service AS s ON rcs.service_id = s.service_id
            INNER JOIN am_asset_maint AS am ON rcd.asset_maint_id = am.asset_maint_id
            UNION ALL
            SELECT sp.asset_maint_id, am.asset_maint_name, rps.service_id, s.service_name,
            arp.desc_result_pm, arp.finish_date, arp.last_cost, 'PM' AS jenis,
            CASE WHEN wpp.vendor_id IS NOT NULL THEN 'External' ELSE 'Internal' END AS repair
            FROM   am_work_order_pm AS wop
            INNER JOIN am_wo_pm_service AS wps ON wop.wo_pm_id = wps.wo_pm_id
            INNER JOIN am_schedule_pm AS sp
            INNER JOIN am_sch_pm_detail AS spd ON sp.sch_pm_id = spd.sch_pm_id
            INNER JOIN am_asset_maint AS am ON sp.asset_maint_id = am.asset_maint_id
            ON wps.sch_pm_id = spd.sch_pm_id AND wps.service_id = spd.service_id
            AND wps.category_id = spd.category_id AND wps.row_sch = spd.row_sch
            INNER JOIN am_wo_pm_person AS wpp ON wps.sch_pm_id = wpp.sch_pm_id
            AND wps.category_id = wpp.category_id AND wps.service_id = wpp.service_id
            AND wps.row_sch = wpp.row_sch AND wps.wo_pm_id = wpp.wo_pm_id
            RIGHT OUTER JOIN am_result_pm AS arp ON wop.wo_pm_id = arp.wo_pm_id
            RIGHT OUTER JOIN am_service AS s
            INNER JOIN am_result_pm_service AS rps ON s.service_id = rps.service_id 
            ON arp.result_pm_id = rps.result_pm_id
            GROUP BY sp.asset_maint_id, am.asset_maint_name, rps.service_id, s.service_name,
            arp.desc_result_pm, arp.finish_date, arp.last_cost, wpp.vendor_id 
            ) as resdata ";
    }

    function FillRow($rec)
    {
        $row=new Rowtblvihistoryasset;

        $row->ASSET_MAINT_ID=$rec->asset_maint_id;
        $row->ASSET_MAINT_NAME=$rec->asset_maint_name;
        $row->SERVICE_ID=$rec->service_id;
        $row->SERVICE_NAME=$rec->service_name;
        $row->DESCRIPTION=$rec->description;
        $row->FINISH_DATE=$rec->finish_date;
        $row->LAST_COST=$rec->last_cost;
        $row->Jenis=$rec->jenis;
        $row->Repair==$rec->repair;

        return $row;
    }

}

class Rowtblvihistoryasset
{

    public $ASSET_MAINT_ID;
    public $ASSET_MAINT_NAME;
    public $SERVICE_ID;
    public $SERVICE_NAME;
    public $DESCRIPTION;
    public $FINISH_DATE;
    public $LAST_COST;
    public $Jenis;
    public $Repair;

}

?>
