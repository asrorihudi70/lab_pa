<?php
class tblvihistoryinputlog extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (
            SELECT am_history_log_metering.hist_log_id, am_history_log_metering.asset_maint_id,
                am_history_log_metering.emp_id, am_history_log_metering.input_date,
                am_history_log_metering.meter, am_history_log_metering.desc_log,
                am_asset_maint.asset_maint_name, am_employees.emp_name
            FROM am_history_log_metering INNER JOIN am_asset_maint
                ON am_history_log_metering.asset_maint_id = am_asset_maint.asset_maint_id
                INNER JOIN am_employees
                ON am_history_log_metering.emp_id = am_employees.emp_id
            ) as resdata";

        $this->TblName='vihistoryinputlog';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblvihistoryinputlog;

        $row->HIST_LOG_ID = $rec->hist_log_id;
        $row->ASSET_MAINT_ID = $rec->asset_maint_id;
        $row->EMP_ID = $rec->emp_id;
        $row->INPUT_DATE = $rec->input_date;
        $row->METER = $rec->meter;
        $row->DESC_LOG = $rec->desc_log;
        $row->ASSET_MAINT_NAME = $rec->asset_maint_name;
        $row->EMP_NAME = $rec->emp_name;

        return $row;
    }
}

class Rowtblvihistoryinputlog
{
    public $HIST_LOG_ID;
    public $ASSET_MAINT_ID;
    public $EMP_ID;
    public $INPUT_DATE;
    public $METER;
    public $DESC_LOG;
    public $ASSET_MAINT_NAME;
    public $EMP_NAME;
    
}

?>
