<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class viewcombodeptallmodel extends Model
{

	function viewcombodeptallmodel()
	{
		parent::Model();
		$this->load->database();
	}    
	
	function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {
    	$Params="";

        if ($ar_filters != null)
        {
	        If ($ar_filters!="")
	        {						
				$Params = str_replace("~" ,"'",$ar_filters );
				$this->db->where($Params); 
			} 															            				
		                					    			                	    
        }

		

		$this->db->select("*");			
		$this->db->from('viewcombodeptall');
		$this->db->orderby("DEPT_ID", $sortdir);
		
        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }
        
		$query = $this->db->get();
		
		return $query;

    }
    
}
class Rowviewdepartmentall
{
	public $DEPT_ID;
	public $DEPT_NAME;
}


?>