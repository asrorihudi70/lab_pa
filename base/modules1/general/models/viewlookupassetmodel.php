<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class viewlookupassetmodel extends Model
{

	function viewlookupassetmodel()
	{
		parent::Model();
		$this->load->database();
	}    
	
	function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {
    	$Params="";

        if ($ar_filters != null)
        {
	        If ($ar_filters!="")
	        {						
				$Params = str_replace("~" ,"'",$ar_filters );
				$this->db->where($Params); 
			} 
										
			
        }
		
		$this->db->select("*");			
		$this->db->from('dbo.viewlookupasetmaint');
		$this->db->orderby("ASSET_MAINT_ID", $sortdir);
		
        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }
        
		$query = $this->db->get();
		
		return $query;

    }
    
}



?>