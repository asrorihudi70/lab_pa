<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionSetupSatuan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getSatuanGrid(){
		$satuan=$_POST['text'];
		if($satuan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE satuan like upper('".$satuan."%')";
		}
		$result=$this->db->query("SELECT kd_satuan,satuan  
									FROM gz_satuan $criteria ORDER BY kd_satuan
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function cekUbah($KdSatuan){
		$result=$this->db->query("SELECT kd_satuan
									FROM gz_satuan 
									WHERE kd_satuan='".$KdSatuan."'	
								")->result();
		if(count($result) > 0){
			$ubah=1;
		} else{
			$ubah=0;
		}
		return $ubah;
	}
	function getKodeOtomatisSetupSatuanBahan()
	{
		$query=$this->db->query("select max(kd_satuan) as kd_satuan from gz_satuan")->row();
			if ($query)
			{
				$otokod=$query->kd_satuan +1;
				if(strlen($otokod) == 1){
				$otokodAhliGz='0'.$otokod;
				} else{
				$otokodAhliGz=$otokod;
				}
			}
			else
			    $otokodAhliGz=1;       
		return $otokodAhliGz;
	}
	public function save(){
		$kdOtomatisSatuanSetupBahan = $this->getKodeOtomatisSetupSatuanBahan();
		$KdSatuan = $_POST['KdSatuan'];
		$NamaSatuan = $_POST['NamaSatuan'];
		$KdSatuan=strtoupper($KdSatuan);
		if ($KdSatuan=='')
		{
				$criteria = "kd_satuan = '".$kdOtomatisSatuanSetupBahan."'";
				$save=$this->saveSetupSatuanBahan($kdOtomatisSatuanSetupBahan,$NamaSatuan);
				$kode=$kdOtomatisSatuanSetupBahan;
		}
		else
		{
				$criteria = "kd_satuan = '".$KdSatuan."'";
				$save=$this->saveSetupSatuanBahan($KdSatuan,$NamaSatuan);
				$kode=$KdSatuan;
		}
		
		
				
		if($save){
			echo "{success:true, kodesatuan:'$kode'}";
		}else{
			echo "{success:false}";
		}
		
	}
	
	public function delete(){
		$KdSatuan = $_POST['KdSatuan'];
		
		$query = $this->db->query("DELETE FROM gz_satuan WHERE Kd_satuan='$KdSatuan' ");
		
		//-----------delete to sq1 server Database---------------//
		//_QMS_Query("DELETE FROM apt_unit WHERE kd_unit_far='$KdUnit'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	
	function saveSetupSatuanBahan($KdSatuan,$NamaSatuan){
		$strError = "";
		
		$Ubah=$this->cekUbah($KdSatuan);
		
		if($Ubah == 0){ //data baru
			$data = array("kd_satuan"=>$KdSatuan,
							"satuan"=>$NamaSatuan
			);
			
			$result=$this->db->insert('gz_satuan',$data);
		
			//-----------insert to sq1 server Database---------------//
			//_QMS_insert('apt_unit',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
			
		} else{ //data edit
			$dataUbah = array("satuan"=>$NamaSatuan);
			
			$criteria = array("kd_satuan"=>$KdSatuan);
			$this->db->where($criteria);
			$result=$this->db->update('gz_satuan',$dataUbah);
			
			//-----------insert to sq1 server Database---------------//
			//_QMS_update('apt_unit',$dataUbah,$criteria);
			//-----------akhir insert ke database sql server----------------//
			if($result){
				$strError='Ok';
			}else{
				$strError='Error';
			}
		}
		
		return $strError;
	}
	
}
?>