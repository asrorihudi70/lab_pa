<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_ordervendor extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakordervendor(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DAFTAR ORDER MAKANAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$kdVendor=$param->kdVendor;
		
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		
		
		if($kdVendor == ''){
			$criteriaVendor="";
		} else{
			$criteriaVendor="and v.kd_vendor = '".$kdVendor."'";
		}
		
		$criteriaTgl="where o.tgl_order between '".$tglAwal."' and '".$tglAkhir."'";
   		
		$queryHead = $this->db->query( "  select distinct(x.vendor ) as vendor
											From (
												select Vendor 
													from gz_order o 
													 inner join gz_order_detail_pasien od on o.no_order = od.no_order 
													 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
													 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
													 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
												".$criteriaTgl."
												".$criteriaVendor." 
												group by  Vendor
												 Union All 
												select Vendor 
													from gz_order o 
													 inner join gz_order_detail_karyawan od on o.no_order = od.no_order 
													 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
													 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
													 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
												".$criteriaTgl."
												".$criteriaVendor."  
												group by  Vendor 
												 Union All 
												select Vendor 
													from gz_order o 
													 inner join gz_order_detail_keluarga od on o.no_order = od.no_order 
													 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
													 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
													 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
												".$criteriaTgl."
												".$criteriaVendor." 
												group by  Vendor
												 Union All 
												select Vendor  
													from gz_order o 
													 inner join gz_order_detail_umum od on o.no_order = od.no_order 
													 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
													 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
													 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
												".$criteriaTgl."
												".$criteriaVendor." 
												group by  Vendor
											 ) x 
											 group by x.vendor

										");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10">No</th>
					<th width="50" align="center">Vendor</th>
					<th width="70" align="center">Tanggal</th>
					<th width="100" align="center">No Order</th>
					<th width="100" align="center">Petugas</th>
					<th width="10" align="center">Jenis Diet</th>
					<th width="40" align="center">Qty Minta</th>
					<th width="40" align="center">Qty Order</th>
					<th width="40" align="center">Selisih</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;		
			
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>
						<tr class="headerrow"> 
								<td width=""></td>
								<th width="" colspan="8" align="left">'.$line->vendor.'</th>
						</tr>

				';
				$queryHasil = $this->db->query( "  select x.no_order, x.tgl_order, x.vendor, x.petugas, jenis_diet, sum(x.jml_minta) as jml_minta, sum(qty_order) as qty_order, sum(qty_order) - sum(x.jml_minta) as selisih
													From (
														select o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet, sum(od.qty_minta) as jml_minta, sum(od.qty) as qty_order ,  sum(od.qty) - sum(od.qty_minta) as selisih
															from gz_order o 
															 inner join gz_order_detail_pasien od on o.no_order = od.no_order 
															 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
															 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
															 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
														".$criteriaTgl."
														".$criteriaVendor."
														and Vendor='".$line->vendor."'
														group by o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet 
														 Union All 
														select o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet, sum(od.qty_minta) as jml_minta, sum(od.qty) as qty_order ,  sum(od.qty) - sum(od.qty_minta) as selisih
															from gz_order o 
															 inner join gz_order_detail_karyawan od on o.no_order = od.no_order 
															 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
															 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
															 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
														".$criteriaTgl."
														".$criteriaVendor."
														and Vendor='".$line->vendor."'
														group by o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet 
														 Union All 
														select o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet, sum(od.qty_minta) as jml_minta, sum(od.qty) as qty_order ,   sum(od.qty) - sum(od.qty_minta) as selisih
															from gz_order o 
															 inner join gz_order_detail_keluarga od on o.no_order = od.no_order 
															 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
															 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
															 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
														".$criteriaTgl."
														".$criteriaVendor."
														and Vendor='".$line->vendor."'
														group by o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet 
														 Union All 
														select o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet, sum(od.qty_minta) as jml_minta, sum(od.qty) as qty_order ,  sum(od.qty) - sum(od.qty_minta) as selisih
															from gz_order o 
															 inner join gz_order_detail_umum od on o.no_order = od.no_order 
															 inner join gz_jenis_diet jd on jd.kd_jenis = od.kd_jenis 
															 inner join gz_vendor v on v.kd_vendor = o.kd_vendor 
															 inner join gz_petugas pt on pt.kd_petugas = o.kd_petugas 
														".$criteriaTgl."
														".$criteriaVendor."
														and Vendor='".$line->vendor."'
														group by o.no_order, o.tgl_order, Vendor, petugas, jd.jenis_diet
													 ) x 
													 group by x.no_order, x.tgl_order, x.vendor, x.petugas, jenis_diet

												");
				$query2 = $queryHasil->result();
				$no=0;
				foreach ($query2 as $line2) 
				{
					$no++;
					$html.='

					<tbody>
							<tr class="headerrow"> 
									<td width="" align="center">'.$no.'</td>
									<td width=""></td>
									<td width="">'.date('d-M-Y',strtotime($line2->tgl_order)).'</td>
									<td width="">'.$line2->no_order.'</td>
									<td width="">'.$line2->petugas.'</td>
									<td width="">'.$line2->jenis_diet.'</td>
									<td width="" align="right">'.$line2->jml_minta.'</td>
									<td width="" align="right">'.$line2->qty_order.'</td>
									<td width="" align="right">'.$line2->selisih.'</td>
							</tr>

					';
					$tot_minta+=$line2->jml_minta;
					$tot_order+=$line2->qty_order;
					$tot_selisih+=$line2->selisih;
				
				}
			}
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="right">Total</th>
					<th width="" align="right">'.$tot_minta.'</th>
					<th width="" align="right">'.$tot_order.'</th>
					<th width="" align="right">'.$tot_selisih.'</th>
				</tr>

			';		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Order Makanan Ke Vendor',$html);	
   	}
}
?>