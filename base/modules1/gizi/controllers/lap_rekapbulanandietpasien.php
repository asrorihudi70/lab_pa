<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rekapbulanandietpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakRekapBulanan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAPITULASI MAKANAN PERBULAN';
		$param=json_decode($_POST['data']);
		
		$bulanAwal=date('m',strtotime($param->bulanAwal));
		$bulanAkhir=date('m',strtotime($param->bulanAkhir));
		$tahun=date('Y',strtotime($param->tahun));
		
		$bulan1=date('M',strtotime($param->bulanAwal));
		$bulan2=date('M',strtotime($param->bulanAkhir));
		
		$criteria="WHERE EXTRACT(MONTH FROM GM.TGL_MINTA) BETWEEN '".$bulanAwal."' AND  '".$bulanAkhir."' AND EXTRACT(YEAR FROM GM.TGL_MINTA) = '".$tahun."'";
		
		$queryHasil = $this->db->query( "  SELECT X.JENIS_DIET, SUM(X.PAGI) as PAGI, SUM(X.SIANG) as SIANG, SUM(X.SORE) as SORE
											FROM (
											 select  GJ.JENIS_DIET,
												CASE WHEN GW.KD_WAKTU = '01' THEN COUNT(GMD.KD_PASIEN) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN COUNT(GMD.KD_PASIEN) ELSE 0 END as SIANG ,         
												CASE WHEN GW.KD_WAKTU = '03' THEN COUNT(GMD.KD_PASIEN) ELSE 0 END as SORE
											 from GZ_MINTA GM
											INNER JOIN GZ_MINTA_PASIEN GMP ON GM.NO_MINTA = GMP.NO_MINTA AND GM.KD_UNIT = GMP.KD_UNIT     
											INNER JOIN GZ_MINTA_PASIEN_DETAIL GMD ON GMP.NO_MINTA = GMD.NO_MINTA AND GMP.KD_PASIEN = GMD.KD_PASIEN     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMD.KD_JENIS     
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMD.KD_WAKTU
											 ".$criteria."     
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU     
											Union All
											 select  GJ.JENIS_DIET,         
												CASE WHEN GW.KD_WAKTU = '01' THEN COUNT(GMD.NIK) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN COUNT(GMD.NIK) ELSE 0 END as SIANG,         
												CASE WHEN GW.KD_WAKTU = '03' THEN COUNT(GMD.NIK) ELSE 0 END as SORE
											 from GZ_MINTA_KARYAWAN GM
											INNER JOIN GZ_MINTA_PER_KARYAWAN GMP ON GM.NO_MINTA = GMP.NO_MINTA     
											INNER JOIN GZ_MINTA_PER_KARYAWAN_DET GMD ON GMP.NO_MINTA = GMD.NO_MINTA AND GMP.NIK = GMD.NIK     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMD.KD_JENIS     
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMD.KD_WAKTU
											  ".$criteria."    
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU     
											Union All
											 select  GJ.JENIS_DIET,         
												CASE WHEN GW.KD_WAKTU = '01' THEN SUM(GMP.QTY) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN SUM(GMP.QTY) ELSE 0 END as SIANG,         
												CASE WHEN GW.KD_WAKTU = '03' THEN SUM(GMP.QTY) ELSE 0 END as SORE
											 from GZ_MINTA_KELUARGA GM
											INNER JOIN GZ_MINTA_KELUARGA_DET GMP ON GM.NO_MINTA = GMP.NO_MINTA     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMP.KD_JENIS     	
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMP.KD_WAKTU
											 ".$criteria."
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU     Union All
											 select  GJ.JENIS_DIET,         
												CASE WHEN GW.KD_WAKTU = '01' THEN SUM(GMP.QTY) ELSE 0 END as PAGI,         
												CASE WHEN GW.KD_WAKTU = '02' THEN SUM(GMP.QTY) ELSE 0 END as SIANG,         
												CASE WHEN GW.KD_WAKTU = '03' THEN SUM(GMP.QTY) ELSE 0 END as SORE
											 from GZ_MINTA_UMUM GM
											INNER JOIN GZ_MINTA_UMUM_DET GMP ON GM.NO_MINTA = GMP.NO_MINTA     
											INNER JOIN GZ_JENIS_DIET GJ ON GJ.KD_JENIS = GMP.KD_JENIS     
											INNER JOIN GZ_WAKTU GW ON GW.KD_WAKTU = GMP.KD_WAKTU
											  ".$criteria."     
											 GROUP BY GJ.JENIS_DIET, GW.KD_WAKTU
											)X
											GROUP BY X.JENIS_DIET

										");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$bulan1.'-'.$tahun.' s/d '.$bulan2.'-'.$tahun.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="25px">No</th>
					<th width="" align="center">Jenis Diet</th>
					<th width="" align="center">Makan Pagi</th>
					<th width="" align="center">Makan Siang</th>
					<th width="" align="center">Makan Malam</th>
			  </tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;		
			
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="">'.$line->jenis_diet.'</td>
								<td width="" align="right">'.$line->pagi.'</td>
								<td width="" align="right">'.$line->siang.'</td>
								<td width="" align="right">'.$line->sore.'</td>
						</tr>

				';
				$tot_pagi+=$line->pagi;
				$tot_siang+=$line->siang;
				$tot_sore+=$line->sore;
			}
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="2" align="right">Total</th>
					<th width="" align="right">'.$tot_pagi.'</th>
					<th width="" align="right">'.$tot_siang.'</th>
					<th width="" align="right">'.$tot_sore.'</th>
				</tr>

			';		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','REKAPITULASI MAKANAN PERBULAN',$html);
	}
}
?>