<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rekappemakaianbahan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakRekapPemakaianBahanGizi(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAP PEMAKAIAN BAHAN MAKANAN';
		$param=json_decode($_POST['data']);
		
		$tglAwal=date('d-M-Y',strtotime($param->tglAwal));
		$tglAkhir=date('d-M-Y',strtotime($param->tglAkhir));
		
		$queryHasil = $this->db->query( "select b.nama_bahan, s.satuan,
case when gpb.jenis_pakai=1 then sum(gpbd.qty) else 0 end as jml_p,
case when gpb.jenis_pakai=2 then sum(gpbd.qty) else 0 end as jml_pg,
case when gpb.jenis_pakai=3 then sum(gpbd.qty) else 0 end as jml_kp,
case when gpb.jenis_pakai=4 then sum(gpbd.qty) else 0 end as jml_u, gpb.keterangan
from gz_pakai_bahan gpb
inner join gz_pakai_bahan_detail gpbd on gpb.no_pakai=gpbd.no_pakai
inner join gz_bahan b on gpbd.kd_bahan=b.kd_bahan
inner join gz_satuan s on b.kd_satuan=s.kd_satuan
where gpb.tgl_pakai between '".$tglAwal."' and '".$tglAkhir."'
group by b.nama_bahan, s.satuan, gpb.jenis_pakai, gpb.keterangan

");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$tglAwal.' s/d '.$tglAkhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="25px" rowspan="2">No</th>
					<th width="" align="center" rowspan="2">Nama Bahan</th>
					<th width="" align="center" rowspan="2">Satuan</th>
					<th width="" align="center" colspan="4">Jenis Pemakaian</th>
					<th width="" align="center" rowspan="2">Jumlah</th>
					<th width="" align="center" rowspan="2">Keterangan</th>
			  </tr>
			  <tr>
				<th align="center">Pasien</td>
				<th align="center">Pegawai</td>
				<th align="center">Kel. Pasien</td>
				<th align="center">Umum</td>
			 </tr>
			</thead>
			';
			
		if(count($query) > 0) {
			$no=0;
			$tot_pagi=0;
			$tot_siang=0;
			$tot_sore=0;		
			
			foreach ($query as $line) 
			{
				//$tgl=date('d-M-Y',strtotime($line->tgl_minta));
				//$tgl=substr($tgl,0,-6);
				$no++;
				$jml = $line->jml_p+$line->jml_pg+$line->jml_kp+$line->jml_u;
				$html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="">'.$line->nama_bahan.'</td>
								<td width="" align="left">'.$line->satuan.'</td>
								<td width="" align="right">'.$line->jml_p.'</td>
								<td width="" align="right">'.$line->jml_pg.'</td>
								<td width="" align="right">'.$line->jml_kp.'</td>
								<td width="" align="right">'.$line->jml_u.'</td>
								<td width="" align="right">'.$jml.'</td>
								<td width="" align="left">'.$line->keterangan.'</td>
						</tr>

				';
			}
			/*$html.='
				<tr class="headerrow"> 
					<th width="" colspan="2" align="right">Total</th>
					<th width="" align="right">'.$tot_pagi.'</th>
					<th width="" align="right">'.$tot_siang.'</th>
					<th width="" align="right">'.$tot_sore.'</th>
				</tr>

			';*/		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','PEMAKAIAN BAHAN MAKANAN',$html);
	}
}
?>