<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_rekappenerimaanbahan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
  
   	
   	public function cetakPenerimaanBahan(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='REKAP PENERIMAAN BAHAN MAKANAN';
		$param=json_decode($_POST['data']);
		
		$bulan=date('m',strtotime($param->bulanTahun));
		$tahun=date('Y',strtotime($param->bulanTahun));
		
		if($bulan == '01'){
			$bln='Januari';
		} else if($bulan == '02'){
			$bln='Februari';
		} else if($bulan == '03'){
			$bln='Maret';
		} else if($bulan == '04'){
			$bln='April';
		} else if($bulan == '05'){
			$bln='Mei';
		} else if($bulan == '06'){
			$bln='Juni';
		} else if($bulan == '07'){
			$bln='Juli';
		} else if($bulan == '08'){
			$bln='Agustus';
		} else if($bulan == '09'){
			$bln='September';
		} else if($bulan == '10'){
			$bln='Oktober';
		} else if($bulan == '11'){
			$bln='November';
		} else {
			$bln='Desember';
		}
		
		$criteria="and  EXTRACT(MONTH FROM x.tgl_terima)='".$bulan."' and  EXTRACT(YEAR FROM x.tgl_terima)='".$tahun."'";
		
		$queryHasil = $this->db->query( "  select x.nama_bahan, x.satuan,
											x.t1,x.t2,x.t3,x.t4,x.t5,x.t6,x.t7,x.t8,x.t9,x.t10,x.t11,x.t12,x.t13,x.t14,x.t15,x.t16,x.t17,x.t18,x.t19,x.t20,x.t21,x.t22,x.t23,x.t24,x.t25,x.t26,x.t27,x.t28,x.t29,x.t30,
											(x.t1+x.t2+x.t3+x.t4+x.t5+x.t6+x.t7+x.t8+x.t9+x.t10+x.t11+x.t12+x.t13+x.t14+x.t15+x.t16+x.t17+x.t18+x.t19+x.t20+x.t21+x.t22+x.t23+x.t24+x.t25+x.t26+x.t27+x.t28+x.t29+x.t30)as jumlah 
										   from (
											select b.nama_bahan,s.satuan, gtb.posted, gtb.tgl_terima,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=1 then sum(gtd.qty) else 0 end as t1,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=2 then sum(gtd.qty) else 0 end as t2,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=3 then sum(gtd.qty) else 0 end as  t3,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=4 then sum(gtd.qty) else 0 end  as t4,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=5 then sum(gtd.qty) else 0 end  as t5,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=6 then sum(gtd.qty) else 0 end as t6,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=7 then sum(gtd.qty) else 0 end as t7,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=8 then sum(gtd.qty) else 0 end as t8,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=9 then sum(gtd.qty) else 0 end as t9,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=10 then sum(gtd.qty) else 0 end as t10,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=11 then sum(gtd.qty) else 0 end as t11,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=12 then sum(gtd.qty) else 0 end as t12,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=13 then sum(gtd.qty) else 0 end as t13,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=14 then sum(gtd.qty) else 0 end as t14,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=15 then sum(gtd.qty) else 0 end as t15,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=16 then sum(gtd.qty) else 0 end as t16,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=17 then sum(gtd.qty) else 0 end as t17,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=18 then sum(gtd.qty) else 0 end as t18,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=19 then sum(gtd.qty) else 0 end as t19,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=20 then sum(gtd.qty) else 0 end as t20,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=21 then sum(gtd.qty) else 0 end as t21,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=22 then sum(gtd.qty) else 0 end as t22,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=23 then sum(gtd.qty) else 0 end as t23,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=24 then sum(gtd.qty) else 0 end as t24,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=25 then sum(gtd.qty) else 0 end as t25,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=26 then sum(gtd.qty) else 0 end as t26,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=27 then sum(gtd.qty) else 0 end as t27,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=28 then sum(gtd.qty) else 0 end as t28,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=29 then sum(gtd.qty) else 0 end as t29,
												case when EXTRACT(DAY FROM gtb.tgl_terima)=30 then sum(gtd.qty) else 0 end t30
											from gz_terima_bahan gtb
												inner join gz_terima_bahan_detail gtd on gtb.no_terima=gtd.no_terima
												inner join gz_bahan b on gtd.kd_bahan=b.kd_bahan
												inner join gz_satuan s on b.kd_satuan=s.kd_satuan
											group by b.nama_bahan, s.satuan, gtb.tgl_terima, gtb.posted
											) x
											Where X.posted = 1
												".$criteria."
											order by x.tgl_terima

										");
		$query = $queryHasil->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Bulan '.$bln.' '.$tahun.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th width="10" rowspan="2">No</th>
					<th width="100" align="center" rowspan="2">Nama Bahan Makanan</th>
					<th width="40" align="center" rowspan="2">Satuan</th>
					<th width="40" align="center" colspan="30">Tanggal</th>
					<th width="40" align="center" rowspan="2">Jumlah</th>
			  </tr>
			   <tr>
					<th width="20" align="center">1</th>
					<th width="20" align="center">2</th>
					<th width="20" align="center">3</th>
					<th width="20" align="center">4</th>
					<th width="20" align="center">5</th>
					<th width="20" align="center">6</th>
					<th width="20" align="center">7</th>
					<th width="20" align="center">8</th>
					<th width="20" align="center">9</th>
					<th width="20" align="center">10</th>
					<th width="20" align="center">11</th>
					<th width="20" align="center">12</th>
					<th width="20" align="center">13</th>
					<th width="20" align="center">14</th>
					<th width="20" align="center">15</th>
					<th width="20" align="center">16</th>
					<th width="20" align="center">17</th>
					<th width="20" align="center">18</th>
					<th width="20" align="center">19</th>
					<th width="20" align="center">20</th>
					<th width="20" align="center">21</th>
					<th width="20" align="center">22</th>
					<th width="20" align="center">23</th>
					<th width="20" align="center">24</th>
					<th width="20" align="center">25</th>
					<th width="20" align="center">26</th>
					<th width="20" align="center">27</th>
					<th width="20" align="center">28</th>
					<th width="20" align="center">29</th>
					<th width="20" align="center">30</th>
			  </tr>
			</thead>';
			
		if(count($query) > 0) {
			$no=0;		
			
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>

						<tr class="headerrow"> 
								<td width="" align="center">'.$no.'</td>
								<td width="">'.$line->nama_bahan.'</td>
								<td width="">'.$line->satuan.'</td>
								<td width="" align="right">'.$line->t1.'</td>
								<td width="" align="right">'.$line->t2.'</td>
								<td width="" align="right">'.$line->t3.'</td>
								<td width="" align="right">'.$line->t4.'</td>
								<td width="" align="right">'.$line->t5.'</td>
								<td width="" align="right">'.$line->t6.'</td>
								<td width="" align="right">'.$line->t7.'</td>
								<td width="" align="right">'.$line->t8.'</td>
								<td width="" align="right">'.$line->t9.'</td>
								<td width="" align="right">'.$line->t10.'</td>
								<td width="" align="right">'.$line->t11.'</td>
								<td width="" align="right">'.$line->t12.'</td>
								<td width="" align="right">'.$line->t13.'</td>
								<td width="" align="right">'.$line->t14.'</td>
								<td width="" align="right">'.$line->t15.'</td>
								<td width="" align="right">'.$line->t16.'</td>
								<td width="" align="right">'.$line->t17.'</td>
								<td width="" align="right">'.$line->t18.'</td>
								<td width="" align="right">'.$line->t19.'</td>
								<td width="" align="right">'.$line->t20.'</td>
								<td width="" align="right">'.$line->t21.'</td>
								<td width="" align="right">'.$line->t22.'</td>
								<td width="" align="right">'.$line->t23.'</td>
								<td width="" align="right">'.$line->t24.'</td>
								<td width="" align="right">'.$line->t25.'</td>
								<td width="" align="right">'.$line->t26.'</td>
								<td width="" align="right">'.$line->t27.'</td>
								<td width="" align="right">'.$line->t28.'</td>
								<td width="" align="right">'.$line->t29.'</td>
								<td width="" align="right">'.$line->t30.'</td>
								<td width="" align="right">'.$line->jumlah.'</td>
						</tr>

				';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="34" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Rekap Penerimaan Bahan Makanan',$html);
	}
}
?>