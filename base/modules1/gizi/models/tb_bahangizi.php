<?php

class tb_bahangizi extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_bahan';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewbahan;
        $row->kd_bahan=$rec->kd_bahan;
		$row->nama_bahan=$rec->nama_bahan;
        
        return $row;
    }

}

class Rowviewbahan
{
   public $kd_bahan;
   public $nama_bahan;
   

}

?>
