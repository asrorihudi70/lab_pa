<?php

class tb_det_jenisdiet extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_det_jenisdiet';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewjenisdiet;
		$row->KD_JENIS_DETJENISDIET=$rec->kd_jenis;
		$row->KD_BAHAN_DETJENISDIET=$rec->kd_bahan;
		$row->KET_DETJENISDIET=$rec->keterangan;
        
        return $row;
    }

}

class Rowviewjenisdiet
{
   public $KD_JENIS_DETJENISDIET;
   public $KD_BAHAN_DETJENISDIET;
   public $KET_DETJENISDIET;

}

?>