﻿<?php
/**
 * @author M
 * @copyright 2008
 */


class tb_distribusipasien extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="no_distribusi, kd_petugas, petugas, kd_waktu, waktu, tgl_distribusi,kd_unit,nama_unit,no_minta";
		$this->SqlQuery="SELECT distinct(d.no_distribusi), d.kd_petugas, p.petugas, d.kd_waktu, w.waktu, d.tgl_distribusi,
											d.kd_unit, u.nama_unit, dd.no_minta
										FROM gz_distribusi d
											INNER JOIN gz_distribusi_detail dd on dd.no_distribusi=d.no_distribusi
											INNER JOIN gz_waktu w on w.kd_waktu=d.kd_waktu
											INNER JOIN gz_petugas p on p.kd_petugas=d.kd_petugas
											INNER JOIN unit u on u.kd_unit=d.kd_unit	
							 ";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new RowDistirbusipasien;
		
		$row->no_distribusi=$rec->no_distribusi;
		$row->kd_petugas=$rec->kd_petugas;
		$row->petugas=$rec->petugas;
		$row->kd_waktu=$rec->kd_waktu;
		$row->waktu=$rec->waktu;
		$row->tgl_distribusi=$rec->tgl_distribusi;
		$row->kd_unit=$rec->kd_unit;
		$row->nama_unit=$rec->nama_unit;
		$row->no_minta=$rec->no_minta;
		return $row;
	}
}
class RowDistirbusipasien
{
	public $no_distribusi;
	public $kd_petugas;
	public $petugas;
	public $kd_waktu;
	public $waktu;
	public $tgl_distribusi;
	public $kd_unit;
	public $nama_unit;
	public $no_minta;
	
}



?>