<?php

class tb_minta_umum_det extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_minta_umum_det';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewmintaumumdet;
		$row->no_minta=$rec->no_minta;
		$row->kd_jenis=$rec->kd_jenis;
		$row->kd_waktu=$rec->kd_waktu;
		$row->qty=$rec->qty;
		$row->harga=$rec->harga;
		$row->kd_petugas=$rec->kd_petugas;
        $row->realisasi=$rec->realisasi;
        return $row;
    }

}

class Rowviewmintaumumdet
{
   public $no_minta;
   public $kd_jenis;
   public $kd_waktu;
   public $qty;
   public $harga;
   public $kd_petugas;
   public $realisasi;

}

?>