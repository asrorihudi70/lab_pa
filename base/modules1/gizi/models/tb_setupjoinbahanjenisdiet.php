<?php

class tb_setupjoinbahanjenisdiet extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_bahan';
        TblBase::TblBase(true);

        $this->SqlQuery='select d.kd_jenis, d.kd_bahan, b.nama_bahan, b.kd_satuan, d.keterangan from gz_det_jenisdiet d inner join gz_bahan b on b.kd_bahan=d.kd_bahan';
    }

    function FillRow($rec)
    {
        $row=new Rowviewbahanjenisdiet;
        $row->KD_BAHAN_BAHANJENISDIET=$rec->kd_bahan;
		$row->NAMA_BAHANJENISDIET=$rec->nama_bahan;
		$row->KD_SATUAN_BAHANJENISDIET=$rec->kd_satuan;
		$row->KETERANGAN_BAHANJENISDIET=$rec->keterangan;
        return $row;
    }

}

class Rowviewbahanjenisdiet
{
   public $KD_BAHAN_BAHANJENISDIET;
   public $NAMA_BAHANJENISDIET;
   public $KD_SATUAN_BAHANJENISDIET;
   public $KETERANGAN_BAHANJENISDIET;
}

?>
