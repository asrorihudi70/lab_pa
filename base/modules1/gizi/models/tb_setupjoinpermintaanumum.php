<?php

class tb_setupjoinpermintaanumum extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_minta_umum';
        TblBase::TblBase(true);

        $this->SqlQuery='select gmu.no_minta, gw.waktu ,gmu.nama, gmu.alamat , gmu.tgl_minta, gmu.tgl_makan, gjd.jenis_diet, gmud.harga, gmud.qty as jumlah, gmud.harga * gmud.qty as totjumlah, gmu.keterangan
						from gz_minta_umum gmu 
						inner join gz_minta_umum_det gmud on gmu.no_minta = gmud.no_minta
						inner join gz_waktu gw on gmud.kd_waktu = gw.kd_waktu
						inner join gz_jenis_diet gjd on gmud.kd_jenis = gjd.kd_jenis';
    }

    function FillRow($rec)
    {
        $row=new Rowviewbahanjenisdiet;
        $row->no_minta=$rec->no_minta;
		$row->waktu=$rec->waktu;
		$row->nama=$rec->nama;
		$row->alamat=$rec->alamat;
		$row->tgl_minta=$rec->tgl_minta;
		$row->tgl_makan=$rec->tgl_makan;
		$row->jenis_diet=$rec->jenis_diet;
		$row->harga=$rec->harga;
		$row->jumlah=$rec->jumlah;
		$row->totjumlah=$rec->totjumlah;
		$row->keterangan=$rec->keterangan;
        return $row;
    }

}

class Rowviewbahanjenisdiet
{
   public $no_minta;
   public $waktu;
   public $nama;
   public $alamat;
   public $tgl_minta;
   public $tgl_makan;
   public $jenis_diet;
   public $harga;
   public $jumlah;
   public $totjumlah;
   public $keterangan;
}

?>
