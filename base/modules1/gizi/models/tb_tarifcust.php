<?php

class tb_tarifcust extends TblBase
{
    function __construct()
    {
        $this->TblName='gz_tarif_cust';
        TblBase::TblBase(true);

        $this->SqlQuery='';
    }

    function FillRow($rec)
    {
        $row=new Rowviewtarifcust;
        $row->KD_GOL_TARIFCUST=$rec->kd_gol;
		$row->KD_JENIS_TERIFCUST=$rec->kd_jenis;
		$row->HARGA_JUAL_TARIFCUST=$rec->harga_jual;
		$row->TAG_BERLAKU_TARIFCUST=$rec->tag_berlaku;
        
        return $row;
    }

}

class Rowviewtarifcust
{
   public $KD_GOL_TARIFCUST;
   public $KD_JENIS_TERIFCUST;
   public $HARGA_JUAL_TARIFCUST;
   public $TAG_BERLAKU_TARIFCUST;

}

?>
