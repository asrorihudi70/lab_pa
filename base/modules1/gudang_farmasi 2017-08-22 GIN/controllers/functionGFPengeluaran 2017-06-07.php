<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFPengeluaran extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	
   	public function unposting(){
   		$this->db->trans_begin();
   		$this->dbSQL->trans_begin();
		$strError="";
   		$result= $this->db->query("SELECT tgl_stok_out,kd_unit_cur,kd_unit_far,kd_milik,post_out FROM apt_stok_out WHERE no_stok_out='".$_POST['no_stok_out']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_stok_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_cur."' AND years=".((int)date("Y",strtotime($result->tgl_stok_out))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
    	if($result->post_out==1){
   			$cekstok=$this->db->query("Select A.kd_prd,A.jml_out,sum(C.jml_stok_apt) AS stok_far,B.kd_milik,B.kd_unit_far,B.kd_unit_cur,sum(D.jml_stok_apt) AS stok_cur
								FROM apt_stok_out_det A 
									INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out 
									INNER JOIN apt_stok_unit_gin C ON C.kd_unit_far=B.kd_unit_far AND C.kd_milik=B.kd_milik AND C.kd_prd=A.kd_prd 
									INNER JOIN apt_stok_unit_gin D ON D.kd_unit_far=B.kd_unit_cur AND D.kd_milik=B.kd_milik AND D.kd_prd=A.kd_prd AND D.gin=C.gin
								WHERE A.no_stok_out='".$_POST['no_stok_out']."' 
								GROUP BY A.kd_prd,A.jml_out,B.kd_milik,B.kd_unit_far,B.kd_unit_cur")->result();
			for($a=0; $a<count($cekstok); $a++){
				$paramsStokUnitTujuanSql = array(
					'kd_unit_far' 	=> $cekstok[$a]->kd_unit_far,
					'kd_prd' 		=> $cekstok[$a]->kd_prd,
					'kd_milik'		=> $cekstok[$a]->kd_milik
				);
				$rescekstoktujuansql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnitTujuanSql);
   				if($cekstok[$a]->jml_out > $cekstok[$a]->stok_far || ($cekstok[$a]->jml_out > $rescekstoktujuansql->row()->JML_STOK_APT)){
   					$jsonResult['processResult']='ERROR';
   					$jsonResult['processMessage']='Stok Obat kode produk "'.$cekstok[$a]->kd_prd.'" tidak Mencukupi.';
   					echo json_encode($jsonResult);
   					exit;
   				}
   			}
   			$apt_stok_out=array();
   			$apt_stok_out['post_out']= 0;
   			$criteria=array('no_stok_out'=>$_POST['no_stok_out']);
   			
   			$this->db->where($criteria);
   			$update_apt_stok_out=$this->db->update('apt_stok_out',$apt_stok_out);
   			
			# UPDATE STOK UNIT SQL SERVER
			$resdetails=$this->db->query("select * from apt_stok_out_det where no_stok_out='".$_POST['no_stok_out']."'")->result();
			if(count($resdetails)>0){
				for($i=0;$i<count($resdetails);$i++){
					# UPDATE STOK YANG MENERIMA
					$update_far_apt_stok_unitSQL=$this->dbSQL->query("UPDATE APT_STOK_UNIT SET JML_STOK_APT = JML_STOK_APT - ".$resdetails[$i]->jml_out."
													WHERE KD_UNIT_FAR='".$result->kd_unit_far."' AND KD_PRD='".$resdetails[$i]->kd_prd."' 
													AND KD_MILIK='".$result->kd_milik."'");
					
					# UPDATE STOK YANG MENGELUARKAN
					$update_cur_apt_stok_unitSQL=$this->dbSQL->query("UPDATE APT_STOK_UNIT SET JML_STOK_APT = JML_STOK_APT + ".$resdetails[$i]->jml_out."
													WHERE KD_UNIT_FAR='".$result->kd_unit_cur."' AND KD_PRD='".$resdetails[$i]->kd_prd."' 
													AND KD_MILIK='".$result->kd_milik."'");
				}
			}
			
			/*----------------------- UPDATE STOK ------------------------------ */
			if($update_apt_stok_out){
				$details=$this->db->query("select * from apt_stok_out_det_gin where no_stok_out='".$_POST['no_stok_out']."'")->result();
				if(count($details)>0){
					
					for($i=0;$i<count($details);$i++){
						
						$update_apt_stok_out_det_gin=$this->db->query("update apt_stok_out_det_gin set jml = jml - ".$details[$i]->jml."
													WHERE no_stok_out='".$_POST['no_stok_out']."' and kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$details[$i]->kd_prd."' 
													AND kd_milik='".$result->kd_milik."' and gin='".$details[$i]->gin."' and out_line=".$details[$i]->out_line."");
													
						if($update_apt_stok_out_det_gin){
							/* UPDATE APT_STOK_UNIT_GIN -> stok unit farmasi yg menerima obat dari yg mengeluarkannya */
							$a=substr($details[$i]->gin,0,4);
							$b=substr($details[$i]->gin,-5);
							$ginsql=$a.$b;
							$update_far_apt_stok_unit_gin=$this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$details[$i]->jml."
													WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$details[$i]->kd_prd."' 
													AND kd_milik='".$result->kd_milik."' and gin='".$details[$i]->gin."'");
							// $update_far_apt_stok_unit_ginSQL=$this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT - ".$details[$i]->jml."
													// WHERE KD_UNIT_FAR='".$result->kd_unit_far."' AND KD_PRD='".$details[$i]->kd_prd."' 
													// AND KD_MILIK='".$result->kd_milik."' AND GIN='".$ginsql."'");
							
							// if($update_far_apt_stok_unit_gin && $update_far_apt_stok_unit_ginSQL){
							if($update_far_apt_stok_unit_gin){
								/* UPDATE APT_STOK_UNIT_GIN -> stok unit farmasi yg mengeluarkan */
								$update_cur_apt_stok_unit_gin=$this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt + ".$details[$i]->jml."
													WHERE kd_unit_far='".$result->kd_unit_cur."' AND kd_prd='".$details[$i]->kd_prd."' 
													AND kd_milik='".$result->kd_milik."' and gin='".$details[$i]->gin."'");
								// $update_cur_apt_stok_unit_ginSQL=$this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT + ".$details[$i]->jml."
													// WHERE KD_UNIT_FAR='".$result->kd_unit_cur."' AND KD_PRD='".$details[$i]->kd_prd."' 
													// AND KD_MILIK='".$result->kd_milik."' AND GIN='".$ginsql."'");
													
								// if($update_cur_apt_stok_unit_gin && $update_cur_apt_stok_unit_ginSQL){
								if($update_cur_apt_stok_unit_gin){
									$strError='SUCCESS';
								} else{
									$strError='ERROR';
								}
							} else{
								$strError='ERROR';
							}
						} else{
							$strError='ERROR';
						}
					}
				}
			} else{
				$strError='ERROR';
			}
   		}
   		if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
   			$this->dbSQL->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$this->dbSQL->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
   	}
   	
   	public function postingSave(){
   		$this->db->trans_begin();
		$strError="";
   		$this->checkBulan();
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$date=new DateTime();
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get no_stok_out

		*/
			// $no_stok_out=$kdUnit.$this->db->query("Select CONCAT(
			// '/',
			// date_part('month', TIMESTAMP 'NOW()'),
			// '/',
			// substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			// '/',
			// substring(to_char(nextval('no_stok_out'),'00000') from 2 for 5)) AS code")->row()->code;
			
		$nomor_out=$this->db->query("select nomor_out from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_out+1;
		$no_stok_out=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_out,5,"0", STR_PAD_LEFT);	
		
		
   		$apt_stok_out=array();
   		$apt_stok_out['no_stok_out']= $no_stok_out;
   		$apt_stok_out['tgl_stok_out']= $_POST['tgl_stok_out'] ;
   		$apt_stok_out['post_out']= 1;
   		$apt_stok_out['remark']= $_POST['remark'];
   		$apt_stok_out['kd_milik']= $kdMilik;
   		$apt_stok_out['kd_unit_cur']= $kdUnit;
   		$apt_stok_out['kd_unit_far']= $_POST['kd_unit_far'];
   		$apt_stok_out['total_out']= $_POST['total_out'];
   		
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   			$cekstok=$this->db->query("SELECT kd_unit_far,kd_milik,kd_prd,sum(jml_stok_apt) as jml_stok_apt FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$kdUnit."' AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$kdMilik."' AND jml_stok_apt > 0 
											GROUP BY kd_unit_far,kd_milik,kd_prd");
   			if(count($cekstok->result())>0){
   				if($cekstok->row()->jml_stok_apt < $_POST['jml_out'][$i]){
   					$jsonResult['processResult']='ERROR';
   					$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
   					echo json_encode($jsonResult);
   					exit;
   				}
   			}else{
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
   				echo json_encode($jsonResult);
   				exit;
   			}
   		}
   		
   		$insert_apt_stok_out=$this->db->insert('apt_stok_out',$apt_stok_out);
		
		if($insert_apt_stok_out){
			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$jml=$_POST['jml_out'][$i];
				$jumlah=0;/* sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok */
				$tmp=0;/* sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya */
				
				/* CREATE APT_STOK_OUT_DET */
				$apt_stok_out_det=array();
				$apt_stok_out_det['no_stok_out']=$no_stok_out;
				$apt_stok_out_det['kd_prd']=$_POST['kd_prd'][$i];
				$apt_stok_out_det['kd_milik']=$kdMilik;
				$apt_stok_out_det['out_line']=$i+1;
				$apt_stok_out_det['jml_out']=$_POST['jml_out'][$i];
				
				$insert_apt_stok_out_det=$this->db->insert('apt_stok_out_det',$apt_stok_out_det);
				
				if($insert_apt_stok_out_det){
					$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$kdUnit."' 
											AND kd_prd='".$_POST['kd_prd'][$i]."' 
											AND kd_milik='".$kdMilik."'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
					for($j=0; $j<count($getgin);$j++) {
						$apt_stok_out_det_gin=array();
						
						/* UPDATE APT_STOK_UNIT_GIN -> STOK DARI unit farmasi yg MENGELUARKANNYA */
						if($jml >= $getgin[$j]->jml_stok_apt){
							$jml=$jml - $getgin[$j]->jml_stok_apt;
							$apt_stok_out_det_gin['jml_out']=$getgin[$j]->jml_stok_apt;
							
							$result_cur_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
											where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$kdUnit."' and kd_milik = ".$kdMilik."");
							$jumlah=$getgin[$j]->jml_stok_apt;
							$tmp += $jumlah;
						} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
							$apt_stok_out_det_gin['jml_out']=$jml;
							
							$result_cur_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
											where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$kdUnit."' and kd_milik = ".$kdMilik."");
							$jumlah=$jml;
							$tmp += $jumlah;
							if($tmp == $_POST['jml_out'][$i]){
								$jml=0;
							}
							//break;
						}
						
						/* UPDATE APT_STOK_UNIT_GIN -> STOK UNIT FARMASI yg MENERIMA obat DARI YG MENGELUARKANNYA */
						if($result_cur_apt_stok_unit_gin){
							/* cek gin far tujuan dari gin yg mengdistribusikan
							* jika tersedia makan di update -> jumlah STOK di UNIT TUJUAN ditambah dengan STOK unit yg di DISTRIBUSIKAN
							* jika tidak tersedia makan create sesuai gin dan jumlah stok yg di distribusikan 
							*/
							$getgin2=$this->db->query("SELECT * FROM apt_stok_unit_gin 
												WHERE kd_unit_far='".$_POST['kd_unit_far']."' 
													AND kd_prd='".$_POST['kd_prd'][$i]."' 
													AND kd_milik='".$kdMilik."'
													AND gin='".$getgin[$j]->gin."'
													--AND jml_stok_apt > 0 
												ORDER BY gin asc limit 1")->result();
							
							//for($k=0; $k<count($getgin2);$k++) {
								if(count($getgin2)>0 && $getgin[$j]->jml_stok_apt >= 0){
									$result_far_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt + ".$jumlah." 
														where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$_POST['kd_unit_far']."' and kd_milik = ".$kdMilik."");
								} else{
									$detobat=$this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd'][$i]."' and gin='".$getgin[$j]->gin."'")->row();
									if(count($detobat->result()) > 0){
										$batch=$detobat->row()->batch;
										$harga=$detobat->row()->hrg_beli_obt;
									} else{
										$batch="";
										$harga=0;
									}
									$apt_stok_unit_gin['gin']=$getgin[$j]->gin;
									$apt_stok_unit_gin['kd_unit_far']=$_POST['kd_unit_far'];
									$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd'][$i];
									$apt_stok_unit_gin['kd_milik']=$kdMilik;
									$apt_stok_unit_gin['jml_stok_apt']=$jumlah;
									$apt_stok_unit_gin['batch']=$getgin[$j]->batch;
									$apt_stok_unit_gin['harga']=$getgin[$j]->harga;
									
									$result_far_apt_stok_unit_gin=$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
								}
							//}
							
							/* CREATE APT_STOK_OUT_DET */	
							$detailsgin=$this->db->query("SELECT * FROM apt_stok_out_det_gin WHERE no_stok_out='".$no_stok_out."' AND out_line=".($i+1))->result();	
							
							$apt_stok_out_det_gin['kd_prd']=$_POST['kd_prd'][$i];
							$apt_stok_out_det_gin['kd_milik']=$kdMilik;
							$apt_stok_out_det_gin['out_line']=$i+1;
							$apt_stok_out_det_gin['gin']=$getgin[$j]->gin;
							$apt_stok_out_det_gin['kd_unit_far']=$kdUnit;
							$apt_stok_out_det_gin['jml']=$_POST['jml_out'][$i];
							
							if(count($detailsgin)>0){
								$array = array('no_stok_out =' => $no_stok_out, 'out_line =' => ($i+1));
								
								$this->db->where($array);
								$result_apt_stok_out_det_gin=$this->db->update('apt_stok_out_det_gin',$apt_stok_out_det_gin);
							}else{
								$apt_stok_out_det_gin['no_stok_out']=$no_stok_out;
								
								$result_apt_stok_out_det_gin=$this->db->insert('apt_stok_out_det_gin',$apt_stok_out_det_gin);
							}
							
							if($result_far_apt_stok_unit_gin && $result_apt_stok_out_det_gin){
								$strError='SUCCESS';
							} else{
								$strError='ERROR';
							}
						} else{
							$strError='ERROR';
						}
					}
				} else{
					$strError='ERROR';
				}
				
			}
			# UPDATE nomor_out di apt_unit
			if($strError=='SUCCESS'){
				$update_nomor_out = $this->db->query("update apt_unit set nomor_out =".$nomor_out." where kd_unit_far='".$kdUnit."'");
				if($update_nomor_out){
					$strError='SUCCESS';
				} else{
					$strError='ERROR';
				}
			}
		} else{
			$strError='ERROR';
		}
   		
   		if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   			$jsonResult['resultObject']=array('code'=>$no_stok_out);
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function postingUpdate(){
   		$this->db->trans_begin();
		$strError='';
   		$result= $this->db->query("SELECT tgl_stok_out,kd_unit_cur,kd_milik,post_out FROM apt_stok_out WHERE no_stok_out='".$_POST['no_stok_out']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_stok_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_cur."' AND years=".((int)date("Y",strtotime($result->tgl_stok_out))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
		
    	if($result->post_out==0){
   			$apt_stok_out=array();
   			$apt_stok_out['remark']= $_POST['remark'];
	    	$apt_stok_out['kd_unit_far']= $_POST['kd_unit_far'];
	    	$apt_stok_out['total_out']= $_POST['total_out'];
	    	$apt_stok_out['post_out']= 1;
	    	$apt_stok_out['status_sinkronisasi']= 0;
	    	
	    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
	    		$cekstok=$this->db->query("SELECT kd_unit_far,kd_milik,kd_prd,sum(jml_stok_apt)as jml_stok_apt
											FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_cur."' AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$result->kd_milik."' AND jml_stok_apt > 0 
											GROUP BY kd_unit_far,kd_milik,kd_prd");
				$paramsStokUnitSql = array(
					'kd_unit_far' 	=> $result->kd_unit_cur,
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $result->kd_milik
				);
				$rescekstoksql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnitSql);
				
	    		if(count($cekstok->result()) > 0 || $rescekstoksql->num_rows > 0){
	    			if(($cekstok->row()->jml_stok_apt < $_POST['jml_out'][$i]) || ($rescekstoksql->row()->JML_STOK_APT < $_POST['jml_out'][$i])){
	    				$jsonResult['processResult']='ERROR';
	    				$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
	    				echo json_encode($jsonResult);
	    				exit;
	    			}
	    		}else{
	    			$jsonResult['processResult']='ERROR';
	    			$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'][$i].'" tidak mencukupi.';
	    			echo json_encode($jsonResult);
	    			exit;
	    		}
	    	}
	    	$criteriahead=array('no_stok_out'=>$_POST['no_stok_out']);
	    	
			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$jml=$_POST['jml_out'][$i];
				$jumlah=0;/* sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok */
				$tmp=0;/* sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya */
				
				
				/* UPDATE or CREATE apt_stok_out_det */
				$details=$this->db->query("SELECT * FROM apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line=".($i+1))->result();
				$apt_stok_out_det=array();
				$apt_stok_out_det['kd_prd']=$_POST['kd_prd'][$i];
				$apt_stok_out_det['kd_milik']=$result->kd_milik;
				$apt_stok_out_det['out_line']=$i+1;
				$apt_stok_out_det['jml_out']=$_POST['jml_out'][$i];
				
				if(count($details)>0){
					$array = array('no_stok_out =' => $_POST['no_stok_out'], 'out_line =' => ($i+1));
					
					$this->db->where($array);
					$result_apt_stok_out_det=$this->db->update('apt_stok_out_det',$apt_stok_out_det);
				}else{
					$apt_stok_out_det['no_stok_out']=$_POST['no_stok_out'];
					
					$result_apt_stok_out_det=$this->db->insert('apt_stok_out_det',$apt_stok_out_det);
				}
				
				
				# UPDATE STOK UNIT YANG MENGELUARKAN
				$criteriaSQL = array(
					'kd_unit_far' 	=> $result->kd_unit_cur,
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $result->kd_milik,
				);
				$resstokunitsql = $this->M_farmasi->cekStokUnitSQL($criteriaSQL);
				$apt_stok_unitSQL=array('JML_STOK_APT'=>$resstokunitsql->row()->JML_STOK_APT - $_POST['jml_out'][$i]);
				$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaSQL, $apt_stok_unitSQL);
					
				# UPDATE STOK UNIT YANG MENERIMA	
				$criteriaUpdateSQL = array(
					'kd_unit_far' 	=> $_POST['kd_unit_far'],
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $result->kd_milik,
				);
				$resupdatestokunitsql = $this->M_farmasi->cekStokUnitSQL($criteriaUpdateSQL);
				if($resupdatestokunitsql->num_rows > 0){
					$apt_stok_unit_updateSQL=array('JML_STOK_APT'=>$resupdatestokunitsql->row()->JML_STOK_APT + $_POST['jml_out'][$i]);
					$successSQL = $this->M_farmasi->updateStokUnitSQL($criteriaUpdateSQL, $apt_stok_unit_updateSQL);
				} else{
					$params = array(
						'kd_unit_far' 	=> $_POST['kd_unit_far'],
						'kd_prd' 		=> $_POST['kd_prd'][$i],
						'kd_milik'		=> $result->kd_milik,
						'jml_stok_apt'	=> $_POST['jml_out'][$i],
						'min_stok'		=> 0
					);
					$successSQL = $this->M_farmasi->insertStokUnitSQL($params);
				}
				
				
				if($result_apt_stok_out_det){
					$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_cur."' 
												AND kd_prd='".$_POST['kd_prd'][$i]."' 
												AND kd_milik='".$result->kd_milik."'
												AND jml_stok_apt > 0 
											ORDER BY gin asc ")->result();
					
					for($j=0; $j<count($getgin);$j++) {
						if($tmp != $_POST['jml_out'][$i]){
							$a=substr($getgin[$j]->gin,0,4);
							$b=substr($getgin[$j]->gin,-5);
							$ginsql=$a.$b;
							
							// $getginSQL=$this->dbSQL->query("SELECT * FROM apt_stok_unit_gin 
											// WHERE kd_unit_far='".$result->kd_unit_cur."' 
												// AND kd_prd='".$_POST['kd_prd'][$i]."' 
												// AND kd_milik='".$result->kd_milik."'
												// AND jml_stok_apt > 0 
												// AND gin='".$ginsql."'
											// ORDER BY gin asc ")->row();
							$apt_stok_out_det_gin=array();
							
							/* UPDATE APT_STOK_UNIT_GIN -> stok dari kd unit farmasi yg mengeluarkannya */
							// if($jml >= $getgin[$j]->jml_stok_apt && $jml >= $getginSQL->JML_STOK_APT){
							if($jml >= $getgin[$j]->jml_stok_apt){
								$jml=$jml - $getgin[$j]->jml_stok_apt;
								$apt_stok_out_det_gin['jml']=$getgin[$j]->jml_stok_apt;
								
								$result_cur_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
												where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$result->kd_unit_cur."' and kd_milik = ".$result->kd_milik."");
								// $result_cur_apt_stok_unit_ginSQL = $this->dbSQL->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getginSQL->JML_STOK_APT." 
												// where gin ='".$ginsql."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$result->kd_unit_cur."' and kd_milik = ".$result->kd_milik."");
								
								$jumlah=$getgin[$j]->jml_stok_apt;
								$tmp += $jumlah;
							// } else if ($jml>0 && ($jml < $getgin[$j]->jml_stok_apt && $jml < $getginSQL->JML_STOK_APT)) {
							} else if ($jml > 0 && $jml < $getgin[$j]->jml_stok_apt) {
								$apt_stok_out_det_gin['jml']=$jml;
								
								$result_cur_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
												where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$result->kd_unit_cur."' and kd_milik = ".$result->kd_milik."");
								// $result_cur_apt_stok_unit_ginSQL = $this->dbSQL->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
												// where gin ='".$ginsql."' and kd_prd = '".$_POST['kd_prd'][$i]."' and kd_unit_far = '".$result->kd_unit_cur."' and kd_milik = ".$result->kd_milik."");
								
								$jumlah=$jml;
								$tmp += $jumlah;
								//break;
							}
							
							/* UPDATE APT_STOK_UNIT_GIN -> stok kd unit farmasi yg menerima obat dari yg mengeluarkannya */
							// if($result_cur_apt_stok_unit_gin && $result_cur_apt_stok_unit_ginSQL){
							if($result_cur_apt_stok_unit_gin){
								/* cek gin far tujuan dari gin yg mengdistribusikan
								* jika tersedia makan di update -> jumlah STOK di UNIT TUJUAN ditambah dengan STOK unit yg di DISTRIBUSIKAN
								* jika tidak tersedia makan create sesuai gin dan jumlah stok yg di distribusikan 
								*/
								$getgin2 = $this->db->query("SELECT * FROM apt_stok_unit_gin 
													WHERE kd_unit_far='".$_POST['kd_unit_far']."' 
														AND kd_prd='".$_POST['kd_prd'][$i]."' 
														AND kd_milik='".$result->kd_milik."'
														AND gin='".$getgin[$j]->gin."'
														--AND jml_stok_apt > 0 
													ORDER BY gin asc limit 1 ")->result();
								// $getginSQL2 = $this->dbSQL->query("SELECT top 1 * FROM apt_stok_unit_gin 
													// WHERE kd_unit_far='".$_POST['kd_unit_far']."' 
														// AND kd_prd='".$_POST['kd_prd'][$i]."' 
														// AND kd_milik='".$result->kd_milik."'
														// AND gin='".$ginsql."'
														// --AND jml_stok_apt > 0 
													// ORDER BY gin asc")->result();
								// if((count($getgin2)>0 && $getgin[$j]->jml_stok_apt >= 0) && (count($getginSQL2) > 0 && $getginSQL->JML_STOK_APT >= 0)){
								if(count($getgin2)>0 && $getgin[$j]->jml_stok_apt >= 0){
									$result_far_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt + ".$jumlah." 
														where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'][$i]."' 
															and kd_unit_far = '".$_POST['kd_unit_far']."' and kd_milik = ".$result->kd_milik."");
									// $result_far_apt_stok_unit_ginSQL = $this->dbSQL->query("UPDATE APT_STOK_UNIT_GIN SET JML_STOK_APT = JML_STOK_APT + ".$jumlah." 
														// WHERE GIN ='".$ginsql."' AND KD_PRD = '".$_POST['kd_prd'][$i]."' 
															// AND KD_UNIT_FAR = '".$_POST['kd_unit_far']."' AND KD_MILIK = ".$result->kd_milik."");
								} else{
									$detobat=$this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd'][$i]."' and gin='".$getgin[$j]->gin."'");
									if(count($detobat->result()) > 0){
										$batch=$detobat->row()->batch;
										$harga=$detobat->row()->hrg_beli_obt;
									} else{
										$batch="";
										$harga=0;
									}
									// $apt_stok_unit_gin['gin']=$getgin[$j]->gin;
									$apt_stok_unit_gin['kd_unit_far']=$_POST['kd_unit_far'];
									$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd'][$i];
									$apt_stok_unit_gin['kd_milik']=$result->kd_milik;
									$apt_stok_unit_gin['jml_stok_apt']=$jumlah;
									$apt_stok_unit_gin['batch']=$getgin[$j]->batch;
									$apt_stok_unit_gin['harga']=$getgin[$j]->harga;
									
									// $result_far_apt_stok_unit_gin=$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
									$result_far_apt_stok_unit_gin 	= $this->M_farmasi->insertStokUnitGin($apt_stok_unit_gin,$getgin[$j]->gin);
									// $result_far_apt_stok_unit_ginSQL = $this->M_farmasi->insertStokUnitGinSQL($apt_stok_unit_gin,$ginsql);
								}
								// if($result_far_apt_stok_unit_gin && $result_far_apt_stok_unit_ginSQL){
								if($result_far_apt_stok_unit_gin){
									$strError='SUCCESS';
								} else{
									$this->db->trans_rollback();
									$this->dbSQL->trans_rollback();
									$strError='ERROR';
									$jsonResult['processResult']='ERROR';
									$jsonResult['processMessage']='Error update stok unit yang menerima obat.';
									echo json_encode($jsonResult);
									exit;
								}
							} else{
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								$strError='ERROR';
								$jsonResult['processResult']='ERROR';
								$jsonResult['processMessage']='Error update stok unit yang mengeluarkan obat.';
								echo json_encode($jsonResult);
								exit;
							}

							/* UPDATE or CREATE apt_stok_out_det_gin */
							$detailsgin=$this->db->query("SELECT * FROM apt_stok_out_det_gin WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line=".($i+1)." and gin='".$getgin[$j]->gin."'")->result();
							if(count($detailsgin)>0){
								$array = array('no_stok_out =' => $_POST['no_stok_out'],'kd_prd' =>$_POST['kd_prd'][$i], 'out_line =' => ($i+1),'gin =' =>$getgin[$j]->gin);
								
								$this->db->where($array);
								$result_apt_stok_out_det_gin=$this->db->update('apt_stok_out_det_gin',$apt_stok_out_det_gin);
							}else{
								$apt_stok_out_det_gin['no_stok_out']=$_POST['no_stok_out'];
								$apt_stok_out_det_gin['kd_prd']=$_POST['kd_prd'][$i];
								$apt_stok_out_det_gin['kd_milik']=$result->kd_milik;
								$apt_stok_out_det_gin['out_line']=$i+1;
								$apt_stok_out_det_gin['gin']=$getgin[$j]->gin;
								$apt_stok_out_det_gin['kd_unit_far']=$result->kd_unit_cur;
								//$apt_stok_out_det_gin['jml']=$_POST['jml_out'][$i];
								
								$result_apt_stok_out_det_gin=$this->db->insert('apt_stok_out_det_gin',$apt_stok_out_det_gin);
							}
							if($result_apt_stok_out_det_gin){
								$strError='SUCCESS';
							} else{
								$this->db->trans_rollback();
								$this->dbSQL->trans_rollback();
								$strError='ERROR';
								$jsonResult['processResult']='ERROR';
								$jsonResult['processMessage']='Error simpan detail gin pengeluaran obat unit. Hubungi Admin!';
								echo json_encode($jsonResult);
								exit;
							}
						}
					}
					
					# UPDATE STOK UNIT SQL SERVER
				} else{
					$strError='ERROR';
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Error simpan detail pengeluaran obat unit.';
					echo json_encode($jsonResult);
					exit;
				}
			}
			
			
			
			if($strError == 'SUCCESS'){
				$this->db->where($criteriahead);
				$update_apt_stok_out=$this->db->update('apt_stok_out',$apt_stok_out);
				if($update_apt_stok_out){
					$strError = 'SUCCESS';
				} else{
					$strError = 'ERROR';
				}
			} else{
				$strError = 'ERROR';
			}
			
    	
			if ($strError == 'ERROR'){
   				$this->db->trans_rollback();
				$this->dbSQL->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
				$this->dbSQL->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function getObat(){
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result=$this->db->query("SELECT A.nama_obat,A.kd_prd,A.kd_sat_besar,A.fractions,C.harga_beli,
							C.kd_milik,sum( B.jml_stok_apt) as jml_stok_apt, asm.min_stok
						FROM apt_obat A 
							INNER JOIN apt_stok_unit_gin B ON B.kd_prd=A.kd_prd 
							INNER JOIN apt_produk C ON C.kd_prd=A.kd_prd 
							LEFT JOIN apt_stok_minimum asm on asm.kd_prd=A.kd_prd
						WHERE  A.aktif='t'
							AND B.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'  
							AND B.jml_stok_apt>0 AND upper(A.nama_obat) like upper('".$_POST['text']."%') 
							and C.kd_milik=".$kdMilik."
						GROUP BY A.nama_obat,A.kd_prd,A.kd_sat_besar,A.fractions,C.harga_beli ,asm.min_stok,C.kd_milik
						ORDER BY nama_obat limit 10");
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		echo json_encode($jsonResult);
   	}
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
	public function save(){
		$this->db->trans_begin();
    	$this->checkBulan();
    	$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    	$date=new DateTime();
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get no_stok_out

		*/
			// $no_stok_out=$kdUnit.$this->db->query("Select CONCAT(
			// '/',
			// date_part('month', TIMESTAMP 'NOW()'),
			// '/',
			// substring(to_char(date_part('year', TIMESTAMP 'NOW()'),'0000') from 4 for 5),
			// '/',
			// substring(to_char(nextval('no_stok_out'),'00000') from 2 for 5)) AS code")->row()->code;
		$nomor_out=$this->db->query("select nomor_out from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_out+1;
		$no_stok_out=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_out,5,"0", STR_PAD_LEFT);	
		
    	$apt_stok_out=array();
    	$apt_stok_out['no_stok_out']= $no_stok_out;
    	$apt_stok_out['tgl_stok_out']= $_POST['tgl_stok_out'] ;
    	$apt_stok_out['post_out']= 0;
    	$apt_stok_out['remark']= $_POST['remark'];
    	$apt_stok_out['kd_milik']= $kdMilik;
    	$apt_stok_out['kd_unit_cur']= $kdUnit;
    	$apt_stok_out['kd_unit_far']= $_POST['kd_unit_far'];
    	$apt_stok_out['total_out']= $_POST['total_out'];
    	
    	/*
    	 * insert postgre
    	 */
    	$this->db->insert('apt_stok_out',$apt_stok_out);
    	/*
    	 * insert sql server
    	 */
    	// _QMS_insert('apt_stok_out',$apt_stok_out);
    	
    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
    		$apt_stok_out_det=array();
    		$apt_stok_out_det['no_stok_out']=$no_stok_out;
    		$apt_stok_out_det['kd_prd']=$_POST['kd_prd'][$i];
    		$apt_stok_out_det['kd_milik']=$kdMilik;
    		$apt_stok_out_det['out_line']=$i+1;
    		$apt_stok_out_det['jml_out']=$_POST['jml_out'][$i];
			$apt_stok_out_det['hrg_beli_out']=$_POST['harga_beli'][$i];
    		$apt_stok_out_det['no_minta']=$_POST['no_minta'][$i];
    		/*
    		 * insert postgre
    		 */
    		$this->db->insert('apt_stok_out_det',$apt_stok_out_det);
    		if($_POST['no_minta'][$i]!="")
			{
				$this->db->query("update apt_ro_unit_det set qty_ordered='".$_POST['jml_out'][$i]."'
						where no_ro_unit='".$_POST['no_minta'][$i]."' and kd_prd='".$_POST['kd_prd'][$i]."'");	
			}	
			/*
    		 * insert sql server
    		 */
    		// _QMS_insert('apt_stok_out_det',$apt_stok_out_det);
    		
    	}
		
		# UPDATE nomor_out di apt_unit
		$update_nomor_out = $this->db->query("update apt_unit set nomor_out =".$nomor_out." where kd_unit_far='".$kdUnit."'");
    	
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
    	}else{
    		$this->db->trans_commit();
    		$jsonResult['processResult']='SUCCESS';
    		$jsonResult['resultObject']=array('code'=>$no_stok_out);
    	}
    	echo json_encode($jsonResult);
    }
    public function initList(){
    	$query='';
    	if($_POST['post']=='Belum Posting'){
    		$query='AND post_out=0';
    	}else if($_POST['post']=='Posting'){
    		$query='AND post_out=1';
    	}
    	 
    	$result=$this->db->query("SELECT A.post_out,A.no_stok_out,A.tgl_stok_out,B.nm_unit_far FROM apt_stok_out A INNER JOIN
			apt_unit B ON B.kd_unit_far=A.kd_unit_far WHERE
    		A.no_stok_out like'%".$_POST['no_stok_out']."%'
    		AND tgl_stok_out BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND upper(B.nm_unit_far)LIKE upper('".$_POST['unit']."%') AND
   			kd_unit_cur='".$this->session->userdata['user_id']['aptkdunitfar']."' 
    			
   		".$query);
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result->result();
    	echo json_encode($jsonResult);
    }
    public function deleteDetail(){
    	$this->db->trans_begin();
    	$result= $this->db->query("SELECT tgl_stok_out,kd_unit_cur,kd_milik,post_out FROM apt_stok_out WHERE no_stok_out='".$_POST['no_stok_out']."'")->row();
    	if($result->post_out==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_stok_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_cur."' AND years=".((int)date("Y",strtotime($result->tgl_stok_out))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	
		$cek = $this->db->query("Select * from apt_stok_out_det_gin where no_stok_out='".$_POST['no_stok_out']."' AND out_line=".$_POST['out_line'])->result();
		if(count($cek)>0){
			$this->db->query("DELETE FROM apt_stok_out_det_gin WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line=".$_POST['out_line']);
		}
		
		
    	/*
    	 * query postgre
    	 */
    	$this->db->query("DELETE FROM apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line=".$_POST['out_line']);
    	/*
    	 * query sql server
    	 */
    	// _QMS_query("DELETE FROM apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line=".$_POST['out_line']);
    	
    	$res=$this->db->query("SELECT * FROM apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line>".$_POST['out_line'])->result();
    	if(count($res)>0){
    		for($i=0; $i<count($res) ;$i++){
    			$det=array();
    			$det['out_line']=$res[$i]->out_line-1;
    			$criteria=array('no_stok_out'=>$_POST['no_stok_out'],'out_line'=>$res[$i]->out_line);
    			
    			/*
    			 * update postgre
    			 */
    			$this->db->where($criteria);
    			$this->db->update('apt_stok_out_det',$det);
    			/*
    			 * update sql server
    			 */
    			// _QMS_update('apt_stok_out_det',$det,$criteria);
    		}
    	}
    	if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   		}
		echo json_encode($jsonResult);
    }
    
    public function getForEdit(){
    	$result=$this->db->query("SELECT A.*,B.nm_unit_far FROM apt_stok_out A INNER JOIN
   				apt_unit B ON B.kd_unit_far=A.kd_unit_far
   				WHERE no_stok_out='".$_POST['no_stok_out']."'");
    	 
    	if(count($result->result())>0){
    		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
    		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    		$jsonResult['resultObject']=$result->row();
    		$det=$this->db->query("SELECT A.*,B.nama_obat,B.kd_sat_besar,B.fractions,sum(C.jml_stok_apt) as jml_stok_apt,A.jml_out AS qty,D.harga_beli 
					FROM apt_stok_out_det A INNER JOIN
   					apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
    				apt_stok_unit_gin C ON C.kd_prd=A.kd_prd AND C.kd_unit_far='".$result->row()->kd_unit_cur."' AND C.kd_milik='".$result->row()->kd_milik."' INNER JOIN
   					apt_produk D ON D.kd_prd=A.kd_prd AND D.kd_milik='".$result->row()->kd_milik."'
   					WHERE A.no_stok_out='".$_POST['no_stok_out']."'
					Group BY A.no_stok_out,A.kd_prd,A.kd_milik,A.out_line,A.jml_out,A.no_minta,A.line_minta,A.out_ket,A.hrg_beli_out,A.hrg_beli_def,B.nama_obat,B.kd_sat_besar,B.fractions,D.harga_beli ,A.jml_out")->result();
    		for($i=0; $i<count($det) ; $i++){
    			$det[$i]->jml_stok_apt+=$det[$i]->qty;
    		}
    		$jsonResult['listData']=$det;
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Data Tidak Ada.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
    public function initTransaksi(){
    	$this->checkBulan();
    	$jsonResult['processResult']='SUCCESS';
    	echo json_encode($jsonResult);
    }
    
	public function update(){
		$this->db->trans_begin();
    	$result= $this->db->query("SELECT tgl_stok_out,kd_unit_cur,kd_milik,post_out FROM apt_stok_out WHERE no_stok_out='".$_POST['no_stok_out']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_stok_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_cur."' AND years=".((int)date("Y",strtotime($result->tgl_stok_out))))->row();
    	if($period->month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    	if($result->post_out==0){
			$apt_stok_out=array();
	    	$apt_stok_out['remark']= $_POST['remark'];
	    	$apt_stok_out['kd_unit_far']= $_POST['kd_unit_far'];
	    	$apt_stok_out['total_out']= $_POST['total_out'];
	    	$criteria=array('no_stok_out'=>$_POST['no_stok_out']);
	    	
	    	/*
	    	 * update postgre
	    	 */
	    	$this->db->where($criteria);
	    	$this->db->update('apt_stok_out',$apt_stok_out);
	    	/*
	    	 * update sql server
	    	 */
	    	
	    	/*
	    	 * query postgre
	    	 */
	    	$this->db->query("DELETE from apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line>".count($_POST['kd_prd']));
	    	/*
	    	 * query sql server
	    	 */
	    	// _QMS_query("DELETE from apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line>".count($_POST['kd_prd']));
	    	
	    	for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
	    		$details=$this->db->query("SELECT * FROM apt_stok_out_det WHERE no_stok_out='".$_POST['no_stok_out']."' AND out_line=".($i+1))->result();
	    		$apt_stok_out_det=array();
	    		$apt_stok_out_det['kd_prd']=$_POST['kd_prd'][$i];
	    		$apt_stok_out_det['kd_milik']=$result->kd_milik;
	    		$apt_stok_out_det['out_line']=$i+1;
	    		$apt_stok_out_det['jml_out']=$_POST['jml_out'][$i];
	    		
	    		if(count($details)>0){
	    			$array = array('no_stok_out =' => $_POST['no_stok_out'], 'out_line =' => ($i+1));
	    			
	    			/*
	    			 * update postgre
	    			 */
					$this->db->where($array);
	    			$this->db->update('apt_stok_out_det',$apt_stok_out_det);
	    			/*
	    			 * update sql server
	    			 */
	    			// _QMS_update('apt_stok_out_det',$apt_stok_out_det,$array);
	    			
	    		}else{
	    			$apt_stok_out_det['no_stok_out']=$_POST['no_stok_out'];
	    			
	    			/*
	    			 * insert postgre
	    			 */
	    			$this->db->insert('apt_stok_out_det',$apt_stok_out_det);
	    			/*
	    			 * insert sql server
	    			 */
	    			// _QMS_insert('apt_stok_out_det',$apt_stok_out_det);
	    			
	    		}
	    	}
    		if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}
    	}else{
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
    	}
    	echo json_encode($jsonResult);
    }
}
?>