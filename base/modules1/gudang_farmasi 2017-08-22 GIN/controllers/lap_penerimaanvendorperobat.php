<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanvendorperobat extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$html='';
   		$qr='';
   		$vendor='SEMUA';
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   			$pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$param->vendor."'")->row();
   			$vendor=$pbf->vendor;
   		} else{
			$qr="";
		}
   		$queri="SELECT A.kd_prd,D.nama_obat,E.satuan,sum(A.jml_in_obt/A.frac) AS qty,C.harga_beli,
				sum((A.jml_in_obt/A.frac)*A.hrg_beli_obt)AS sub_total,
				sum(A.apt_disc_rupiah+((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
				sum(((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*10)AS ppn,
				sum(((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100)*10)+((A.jml_in_obt/A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah+((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))AS total
			FROM apt_obat_in_detail A 
				INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in 
				INNER JOIN apt_produk C ON C.kd_prd=A.kd_prd AND C.kd_milik=B.kd_milik 
				INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
				LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
			WHERE B.due_date BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			GROUP BY A.kd_prd,nama_obat,satuan,C.harga_beli
			ORDER BY D.nama_obat,E.satuan ASC";
   		
   		$data=$this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN BERDASARKAN VENDOR PER OBAT</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>TGL PENERIMAAN : ".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>VENDOR : ".$vendor."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='60'>Kode</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='60'>Sat</th>
   						<th width='60'>Qty</th>
				   		<th width='60'>Harga</th>
		   				<th width='70'>Sub Total</th>
		   				<th width='70'>Disc</th>
		   				<th width='70'>PPN</th>
		   				<th width='70'>Total</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
	   			$html.="<tr>
   						<th colspan='10' align='center'>Data tidak ada</th>
				   		</tr>";
	   		}else{
	   			$sub_total=0;
	   			$total=0;
	   			$disc=0;
	   			$ppn=0;
	   			for($i=0; $i<count($data); $i++){
	   				$sub_total+=$data[$i]->sub_total;
	   				$total+=$data[$i]->total;
	   				$disc+=$data[$i]->disc;
	   				$ppn+=$data[$i]->ppn;
	   				$html.="<tr>
	   					<td>".($i+1)."</td>
   						<td>".$data[$i]->kd_prd."</td>
   						<td>".$data[$i]->nama_obat."</td>
	   					<td align='center'>".$data[$i]->satuan."</td>
   						<td align='right'>".number_format($data[$i]->qty,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->disc,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   					</tr>";
	   			}
	   			$html.="<tr>
							<th colspan='6' align='right'>Grand total</td>
							<th align='right'>".number_format($sub_total,0,',','.')."</th>
							<th align='right'>".number_format($disc,0,',','.')."</th>
							<th align='right'>".number_format($ppn,0,',','.')."</th>
							<th align='right'>".number_format($total,0,',','.')."</th>
						</tr>";
	   		}
		$html.="</tbody></table>";
   		$common=$this->common;
		$this->common->setPdf('L','LAPORAN BERDASARKAN VENDOR PER OBAT',$html);
		echo $html;
   	}

	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
   		$qr='';
   		$vendor='SEMUA';
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   			$pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$param->vendor."'")->row();
   			$vendor=$pbf->vendor;
   		} else{
			$qr="";
		}
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN BERDASARKAN VENDOR PER OBAT", 9,"center")
			->commit("header")
			->addColumn("TGL PENERIMAAN : ".tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 9,"center")
			->commit("header")
			->addColumn("VENDOR : ".$vendor, 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		#QUERY HEAD
		$reshead=$this->db->query("SELECT A.kd_prd,D.nama_obat,E.satuan,sum(A.jml_in_obt/A.frac) AS qty,C.harga_beli,
				sum((A.jml_in_obt/A.frac)*A.hrg_beli_obt)AS sub_total,
				sum(A.apt_disc_rupiah+((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
				sum(((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*10)AS ppn,
				sum(((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100)*10)+((A.jml_in_obt/A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah+((((A.jml_in_obt/A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))AS total
			FROM apt_obat_in_detail A 
				INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in 
				INNER JOIN apt_produk C ON C.kd_prd=A.kd_prd AND C.kd_milik=B.kd_milik 
				INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
				LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
			WHERE B.due_date BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			GROUP BY A.kd_prd,nama_obat,satuan,C.harga_beli
			ORDER BY D.nama_obat,E.satuan ASC")->result();
			
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 30)
			->setColumnLength(3, 10)
			->setColumnLength(4, 5)
			->setColumnLength(5, 10)
			->setColumnLength(6, 10)
			->setColumnLength(7, 8)
			->setColumnLength(8, 8)
			->setColumnLength(9, 10)
			->setUseBodySpace(true);
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Kode", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Harga", 1,"right")
			->addColumn("Sub Total", 1,"right")
			->addColumn("Disc", 1,"right")
			->addColumn("PPN", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 10,"center")
				->commit("header");
		} else{
			$no = 0;
			$ppn =0;
			$disc =0;
			$subtotal =0;
			$total =0;
			$harga_beli =0;
			foreach ($reshead as $key) {
				$no++;					
					$tp	->addColumn($no, 1)
						->addColumn($key->kd_prd, 1,"left")
						->addColumn($key->nama_obat, 1,"left")
						->addColumn($key->satuan, 1,"left")
						->addColumn($key->qty, 1,"right")
						->addColumn(number_format($key->harga_beli,0, "," , "."), 1,"right")
						->addColumn(number_format($key->sub_total,0, "," , "."), 1,"right")
						->addColumn(number_format($key->disc,0, "," , "."), 1,"right")
						->addColumn(number_format($key->ppn,0, "," , "."), 1,"right")
						->addColumn(number_format($key->total,0, "," , "."), 1,"right")
						->commit("header");	
						
					$harga_beli += $key->harga_beli;
					$subtotal += $key->sub_total;
					$disc += $key->disc;
					$ppn += $key->ppn;
					$total += $key->total;
			}	
			$tp	->addColumn("Grand Total", 6,"right")
				->addColumn(number_format($subtotal,0, "," , "."), 1,"right")
				->addColumn(number_format($disc,0, "," , "."), 1,"right")
				->addColumn(number_format($ppn,0, "," , "."), 1,"right")
				->addColumn(number_format($total,0, "," , "."), 1,"right")
				->commit("footer");
		}
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/datapenerimaanvendorperobat.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
}
?>