<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_stokobatunit extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             // $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getObat(){
    	$result=$this->result;
    	$result->setData($this->db->query("SELECT kd_prd,nama_obat FROM apt_obat WHERE upper(nama_obat) like upper('".$_POST['text']."%') limit 10")->result());
    	$result->end();
    }
   	
   	public function getData(){
   		$result=$this->result;
   		$common=$this->common;
   		$kd_unit_far=$common->getKodeUnit();
   		$array=array();
   		$array['this_unit']=array('id'=>$kd_unit_far,'text'=>$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far);
   		$array['unit']=$this->db->query("SELECT kd_unit_far as id,nm_unit_far as text FROM apt_unit WHERE kd_unit_far not in('".$kd_unit_far."') ORDER BY nm_unit_far ASC")->result();
		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns as id,sub_jenis as text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
		$array['milik']=$this->db->query("SELECT kd_milik as id,milik as text FROM apt_milik ORDER BY milik ASC")->result();
   		
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
		$html='';
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		$qr.=" A.kd_unit_far in(".$tmpKdUnit.")";
   		if(isset($param->group)){
   			$group=false;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns='".$param->sub_jenis."'";
   		}
   		if($param->milik!=''){
   			$qr.=" AND A.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}
		if($param->kd_prd!=''){
   			$kdprd=" AND A.kd_prd='".$param->kd_prd."'";
   		}
		
   		$queri="SELECT C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli,sum(A.jml_stok_apt) as jml_stok_apt FROM apt_stok_unit_gin A INNER JOIN
   		apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
   		apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns INNER JOIN
   		apt_satuan D ON D.kd_satuan=B.kd_satuan INNER JOIN
   		apt_produk E ON E.kd_prd=A.kd_prd AND E.kd_milik=A.kd_milik
   		WHERE ".$qr.$kdprd."
		GROUP BY C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli
   		ORDER BY B.nama_obat,C.kd_sub_jns,A.kd_prd,A.kd_unit_far";
   		$data=$this->db->query($queri)->result();
   		$urut=0;
   		$obts=array();
   		$obats=array();
		$temp='';
   		for($i=0; $i<count($data); $i++){
   			$obat=array();
   			if(isset($obats[$temp])){
   				$obats[$obts[$data[$i]->kd_prd]][$data[$i]->kd_unit_far]+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total']+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total_harga']+=($data[$i]->jml_stok_apt*$data[$i]->harga_beli);
   			}else{
   				
				for ($j=0; $j < count($arrayDataUnit); $j++) { 
					$obat[$arrayDataUnit[$j][0]]=0;
				}
   				$obat[$data[$i]->kd_unit_far]=$data[$i]->jml_stok_apt;
   				$obat['harga']=$data[$i]->harga_beli;
   				$obat['total']=$data[$i]->jml_stok_apt;
   				$obat['total_harga']=$data[$i]->jml_stok_apt*$data[$i]->harga_beli;
   				$obat['satuan']=$data[$i]->satuan;
   				$obat['nama_obat']=$data[$i]->nama_obat;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['sub_jenis']=$data[$i]->sub_jenis;
   				$obat['kd_sub_jns']=$data[$i]->kd_sub_jns;
   				$obats[$urut]=$obat;
   				$obts[$data[$i]->kd_prd]=$urut;
				$temp = $obts[$data[$i]->kd_prd];
   				$urut++;
   			}
   		}
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN STOK OBAT PER UNIT</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y')."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Kode</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='70'>Sat</th>
   						<th width='80'>Harga</th>
   		";
   		
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$html.="<th width='50'>".$arrayDataUnit[$i][0]."</th>";
		}
		
   		$html.="<th width=70''>Jumlah</th><th width='100'>Jumlah(Rp)</th></tr></thead>";
   		if(count($data)==0){
   			$html.="<tr>
						<th colspan='".(7+count($arrayDataUnit))."' align='center'>Data tidak ada.</td>
					</tr>";
   		}else{
   			$html.="<tbody>";
   			$kd_sub_jenis='';
			$data=$obats;
			$total=0;
			$grand_total=0;
   			for($i=0; $i<count($data); $i++){
   				if($kd_sub_jenis!=$data[$i]['kd_sub_jns']){
   					$kd_sub_jenis=$data[$i]['kd_sub_jns'];
   					if($group==true){
   						if($i!=0){
   							$html.="
	   							<tr>
			   						<td align='right' colspan='".(6+count($arrayDataUnit))."'>Total</td>
	   								<td align='right'>".number_format($total,0,',','.')."</td>
	   							</tr>
			   				";
   						}
   						$html.="
		   					<tr>
		   						<th align='left' colspan='".(7+count($arrayDataUnit))."'>".$data[$i]['sub_jenis']."</th>
   							</tr>
		   				";
   					}
   					$total=0;
   				}
   				$html.="
   					<tr>
   						<td>".($i+1)."</td>
   						<td>".$data[$i]['kd_prd']."</td>
   						<td>".$data[$i]['nama_obat']."</td>
   						<td>".$data[$i]['satuan']."</td>
   						<td align='right'>".number_format($data[$i]['harga'],0,',','.')."</td>
   				";
				
				for ($j=0; $j < count($arrayDataUnit); $j++) { 
					$html.="<td align='right'>".number_format($data[$i][$arrayDataUnit[$j][0]],0,',','.')."</td>";
				}
   				$html.="
	   					<td align='right'>".number_format($data[$i]['total'],0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]['total_harga'],0,',','.')."</td>
   					</tr>
	   			";
   				$total+=$data[$i]['total_harga'];
   				$grand_total+=$data[$i]['total_harga'];
   			}
   			$html.="</tbody>";
   			if($group==true){
   				$html.="
	   				<tr>
			   			<td align='right' colspan='".(6+count($arrayDataUnit))."'>Total</td>
	   					<td align='right'>".number_format($total,0,',','.')."</td>
	   				</tr>
			   	";
   			}
   			$html.="
		   		<tr>
		   			<th align='right' colspan='".(6+count($arrayDataUnit))."'>Grand Total</th>
   					<th align='right'>".number_format($grand_total,0,',','.')."</th>
   				</tr>
		   	";
	   	}
   		$html.="</tbody></table>";
		$prop=array('foot'=>true);
		$baris=$baris+3;
		$print_area='A1:H'.$baris;
		$area_wrap='A1:H'.$baris;
		
		$common=$this->common;
		$this->common->setPdf('P','LAPORAN STOK OBAT PER UNIT',$html);
		
	}
	
	public function doPrint_excel(){
		$html='';
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		$qr.=" A.kd_unit_far in(".$tmpKdUnit.")";
   		if(isset($param->group)){
   			$group=false;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns='".$param->sub_jenis."'";
   		}
   		if($param->milik!=''){
   			$qr.=" AND A.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}
		if($param->kd_prd!=''){
   			$kdprd=" AND A.kd_prd='".$param->kd_prd."'";
   		}
		
		$excel = $param->excel;
   		$queri="SELECT C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli,sum(A.jml_stok_apt) as jml_stok_apt FROM apt_stok_unit_gin A INNER JOIN
   		apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
   		apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns INNER JOIN
   		apt_satuan D ON D.kd_satuan=B.kd_satuan INNER JOIN
   		apt_produk E ON E.kd_prd=A.kd_prd AND E.kd_milik=A.kd_milik
   		WHERE ".$qr.$kdprd."
		GROUP BY C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli
   		ORDER BY B.nama_obat,C.kd_sub_jns,A.kd_prd,A.kd_unit_far";
   		$data=$this->db->query($queri)->result();
   		$urut=0;
   		$obts=array();
   		$obats=array();
		$temp='';
   		for($i=0; $i<count($data); $i++){
   			$obat=array();
   			if(isset($obats[$temp])){
   				$obats[$obts[$data[$i]->kd_prd]][$data[$i]->kd_unit_far]+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total']+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total_harga']+=($data[$i]->jml_stok_apt*$data[$i]->harga_beli);
   			}else{
   				
				for ($j=0; $j < count($arrayDataUnit); $j++) { 
					$obat[$arrayDataUnit[$j][0]]=0;
				}
   				$obat[$data[$i]->kd_unit_far]=$data[$i]->jml_stok_apt;
   				$obat['harga']=$data[$i]->harga_beli;
   				$obat['total']=$data[$i]->jml_stok_apt;
   				$obat['total_harga']=$data[$i]->jml_stok_apt*$data[$i]->harga_beli;
   				$obat['satuan']=$data[$i]->satuan;
   				$obat['nama_obat']=$data[$i]->nama_obat;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['sub_jenis']=$data[$i]->sub_jenis;
   				$obat['kd_sub_jns']=$data[$i]->kd_sub_jns;
   				$obats[$urut]=$obat;
   				$obts[$data[$i]->kd_prd]=$urut;
				$temp = $obts[$data[$i]->kd_prd];
   				$urut++;
   			}
   		}
		$jml_kolom = 6+count($arrayDataUnit);
   		$html.="
   			<table  cellspacing='0' border='0'>
					<tr>
   						<th colspan='".$jml_kolom."' align='center' style='font-weight: bold;'>LAPORAN STOK OBAT PER UNIT</th>
   					</tr>
   					<tr>
   						<td colspan='".$jml_kolom."' align='center' style='font-weight: bold;'>".date('d M Y')."</td>
   					</tr>
   					<tr>
   						<td colspan='".$jml_kolom."' align='center' style='font-weight: bold;'>KEPEMILIKAN : ".$milik."</td>
   					</tr>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>Kode</th>
				   		<th width=''>Nama Obat</th>
				   		<th width='70'>Sat</th>
   						<th width='80'>Harga</th>
   		";
   		
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$html.="<th width='50'>".$arrayDataUnit[$i][0]."</th>";
		}
		
   		$html.="<th width=70''>Jumlah</th><th width='100'>Jumlah(Rp)</th></tr></thead>";
		$baris=0;
   		if(count($data)==0){
   			$html.="<tr>
						<th colspan='".(7+count($arrayDataUnit))."' align='center'>Data tidak ada.</th>
					</tr>";
   		}else{
   			$kd_sub_jenis='';
			$data=$obats;
			$total=0;
			$grand_total=0;
			
   			for($i=0; $i<count($data); $i++){
   				if($kd_sub_jenis!=$data[$i]['kd_sub_jns']){
   					$kd_sub_jenis=$data[$i]['kd_sub_jns'];
   					if($group==true){
   						if($i!=0){
   							$html.="
	   							<tr>
			   						<td align='right' colspan='".(6+count($arrayDataUnit))."'>Total</td>
	   								<td align='right'>".number_format($total,0,',',',')."</td>
	   							</tr>
			   				";
							
   						}
   						$html.="
		   					<tr>
		   						<th align='left' colspan='".(7+count($arrayDataUnit))."'>".$data[$i]['sub_jenis']."</th>
   							</tr>
		   				";
						
   					}
   					$total=0;
   				}
   				$html.="
   					<tr>
   						<td>".($i+1)."</td>
   						<td>".$data[$i]['kd_prd']."</td>
   						<td>".$data[$i]['nama_obat']."</td>
   						<td>".$data[$i]['satuan']."</td>
   						<td align='right'>".number_format($data[$i]['harga'],0,',',',')."</td>
   				";
				
				for ($j=0; $j < count($arrayDataUnit); $j++) { 
					$html.="<td align='right'>".number_format($data[$i][$arrayDataUnit[$j][0]],0,',',',')."</td>";
				}
   				$html.="
	   					<td align='right'>".number_format($data[$i]['total'],0,',',',')."</td>
   						<td align='right'>".number_format($data[$i]['total_harga'],0,',',',')."</td>
   					</tr>
	   			";
   				$total+=$data[$i]['total_harga'];
   				$grand_total+=$data[$i]['total_harga'];
				
   			}
   			
   			if($group==true){
   				$html.="
	   				<tr>
			   			<td align='right' colspan='".(6+count($arrayDataUnit))."'>Total</td>
	   					<td align='right'>".number_format($total,0,',',',')."</td>
	   				</tr>
			   	";
   			}
   			$html.="
		   		<tr>
		   			<th align='right' colspan='".(6+count($arrayDataUnit))."'>Grand Total</th>
   					<th align='right'>".number_format($grand_total,0,',',',')."</th>
   				</tr>
		   	";
		
	   	}
   		$html.="</table>";
		$prop=array('foot'=>true);
		$baris=count($data)+7;
		$print_area='A1:H'.$baris;
		$area_wrap='A1:H'.$baris;
		$name='Laporan_Stok_Obat_Per_Unit.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attschment; filename=".$name);
		echo $html;
		/* # Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
		# - Margin
		# - Print area
		# - Type font
		# - Paragraph alignment
		# - Password protected
		# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
		# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
		
		$table    = $html;
		// $table    = "<table><tr><td>".$baris."</td></tr></table>";

		# save $table inside temporary file that will be deleted later
		$tmpfile = tempnam(sys_get_temp_dir(), 'html');
		file_put_contents($tmpfile, $table);
	
		# Create object phpexcel
		$objPHPExcel     = new PHPExcel();
		
		# Fungsi untuk set PRINT AREA
		$objPHPExcel->getActiveSheet()
					->getPageSetup()
					->setPrintArea($print_area);
		# END Fungsi untuk set PRINT AREA			
					
		# Fungsi untuk set MARGIN
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setTop(0.4);
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setRight(0.1);
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setLeft(0.2);
		$objPHPExcel->getActiveSheet()
					->getPageMargins()->setBottom(0.1);
		# END Fungsi untuk set MARGIN
		
		# Fungsi untuk set TYPE FONT 
		$styleArrayHead = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 9,
				'name'  => 'Courier New'
			));
		$objPHPExcel->getActiveSheet()->getStyle('A1:H3')->applyFromArray($styleArrayHead);
		$styleArrayBody = array(
			'font'  => array(
				'size'  => 9,
				'name'  => 'Courier New'
			));
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
		
		$styleBorder = array(
			'borders' => array(
				'allborders' => array (
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$objPHPExcel->getActiveSheet()->getStyle('A6:H'.$baris)->applyFromArray($styleBorder); 
		# END Fungsi untuk set TYPE FONT 
					
		# Fungsi untuk set ALIGNMENT 
		$objPHPExcel->getActiveSheet()
					->getStyle('A1:H6')
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
					->getStyle('G7:H'.$baris)
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		// # END Fungsi untuk set ALIGNMENT 
		#Fungsi wraptext
		$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
					->getAlignment()->setWrapText(true); 
		# Fungsi untuk PROTECTED SHEET
		$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
		$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
		$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
		$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
		# Set Password
		$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
		# END fungsi PROTECTED SHEET
		
		#Fungsi mengatur lebar kolom
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()
					->getPageSetup()
					->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
		$objPHPExcel->getActiveSheet()
					->getPageSetup()
					->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		#Fungsi wraptext
		$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
					->getAlignment()->setWrapText(true); 
		$excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
		$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
		$objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

		unlink($tmpfile); # delete temporary file because it isn't needed anymore

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
		header('Content-Disposition: attachment;filename=Laporan_Stok_Obat_Per_Unit.xls'); # specify the download file name
		header('Cache-Control: max-age=0');

		# Creates a writer to output the $objPHPExcel's content
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$writer->save('php://output'); */
	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$group=true;
   		$milik='SEMUA';
		$param=json_decode($_POST['data']);
   		$unit='';
   		
   		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
   		$qr.=" A.kd_unit_far in(".$tmpKdUnit.")";
   		if(isset($param->group)){
   			$group=false;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns='".$param->sub_jenis."'";
   		}
   		if($param->milik!=''){
   			$qr.=" AND A.kd_milik='".$param->milik."'";
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik='".$param->milik."'")->row()->milik;
   		}
		if($param->kd_prd!=''){
   			$kdprd=" AND A.kd_prd='".$param->kd_prd."'";
   		}
		
   		$queri="SELECT C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli,sum(A.jml_stok_apt) as jml_stok_apt FROM apt_stok_unit_gin A INNER JOIN
   		apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN
   		apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns INNER JOIN
   		apt_satuan D ON D.kd_satuan=B.kd_satuan INNER JOIN
   		apt_produk E ON E.kd_prd=A.kd_prd AND E.kd_milik=A.kd_milik
   		WHERE ".$qr.$kdprd."
		GROUP BY C.sub_jenis,C.kd_sub_jns,A.kd_prd,B.nama_obat,A.kd_unit_far,D.satuan, E.harga_beli
   		ORDER BY B.nama_obat,C.kd_sub_jns,A.kd_prd,A.kd_unit_far";
   		$data=$this->db->query($queri)->result();
   		$urut=0;
   		$obts=array();
   		$obats=array();
		$temp='';
   		for($i=0; $i<count($data); $i++){
   			$obat=array();
   			if(isset($obats[$temp])){
   				$obats[$obts[$data[$i]->kd_prd]][$data[$i]->kd_unit_far]+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total']+=$data[$i]->jml_stok_apt;
   				$obats[$obts[$data[$i]->kd_prd]]['total_harga']+=($data[$i]->jml_stok_apt*$data[$i]->harga_beli);
   			}else{
   				
				for ($j=0; $j < count($arrayDataUnit); $j++) { 
					$obat[$arrayDataUnit[$j][0]]=0;
				}
   				$obat[$data[$i]->kd_unit_far]=$data[$i]->jml_stok_apt;
   				$obat['harga']=$data[$i]->harga_beli;
   				$obat['total']=$data[$i]->jml_stok_apt;
   				$obat['total_harga']=$data[$i]->jml_stok_apt*$data[$i]->harga_beli;
   				$obat['satuan']=$data[$i]->satuan;
   				$obat['nama_obat']=$data[$i]->nama_obat;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['kd_prd']=$data[$i]->kd_prd;
   				$obat['sub_jenis']=$data[$i]->sub_jenis;
   				$obat['kd_sub_jns']=$data[$i]->kd_sub_jns;
   				$obats[$urut]=$obat;
   				$obts[$data[$i]->kd_prd]=$urut;
				$temp = $obts[$data[$i]->kd_prd];
   				$urut++;
   			}
   		}
		
		# Create Data
		$jml_kolom = 7+count($arrayDataUnit);
		$tp = new TableText(145,$jml_kolom,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		 # SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 8)
			->setColumnLength(2, 15)
			->setColumnLength(3, 6)
			->setColumnLength(4, 10);
		$x =5;
		for ($i=0; $i < count($arrayDataUnit); $i++) {
			$tp->setColumnLength($x, 14);
			$x++;
		}
		$tp->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, $jml_kolom,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, $jml_kolom,"left")
			->commit("header")
			->addColumn($telp, $jml_kolom,"left")
			->commit("header")
			->addColumn($fax, $jml_kolom,"left")
			->commit("header")
			->addColumn("LAPORAN STOK OBAT PER UNIT", $jml_kolom,"center")
			->commit("header")
			->addColumn(date('d M Y'), $jml_kolom,"center")
			->commit("header")
			->addColumn("KEPEMILIKAN : ".$milik, $jml_kolom,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Kode", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Sat", 1,"left")
			->addColumn("Harga", 1,"left");
		$nama_kolom='';
		for ($i=0; $i < count($arrayDataUnit); $i++) {
			$tp->addColumn($arrayDataUnit[$i][0], 1,"left");
		}
		$tp	->addColumn("Jumlah", 1,"right")
			->addColumn("Jumlah(Rp)", 1,"right");
		$tp->commit("header");	
   		
   		if(count($data)==0){
			$tp	->addColumn("Data tidak ada", 7+count($arrayDataUnit),"center")
				->commit("header");
   		}else{
   			$kd_sub_jenis='';
			$data=$obats;
			$total=0;
			$grand_total=0;
   			for($i=0; $i<count($data); $i++){
   				if($kd_sub_jenis!=$data[$i]['kd_sub_jns']){
   					$kd_sub_jenis=$data[$i]['kd_sub_jns'];
   					if($group==true){
   						if($i!=0){
							$tp	->addColumn("Total", 6+count($arrayDataUnit),"right")
								->addColumn(number_format($total,0,',','.'), 1,"right")
								->commit("header");
   						}
						$tp	->addColumn($data[$i]['sub_jenis'], 7+count($arrayDataUnit),"left")
							->commit("header");
   					}
   					$total=0;
   				}
				$tp	->addColumn(($i+1), 1,"left")
					->addColumn($data[$i]['kd_prd'], 1,"left")
					->addColumn($data[$i]['nama_obat'], 1,"left")
					->addColumn($data[$i]['satuan'], 1,"left")
					->addColumn(number_format($data[$i]['harga'],0,',','.'), 1,"right");
   				
				for ($j=0; $j < count($arrayDataUnit); $j++) { 
					$tp	->addColumn(number_format($data[$i][$arrayDataUnit[$j][0]],0,',','.'), 1,"right");
				}
				$tp	->addColumn(number_format($data[$i]['total'],0,',','.'), 1,"right")
					->addColumn(number_format($data[$i]['total_harga'],0,',','.'), 1,"right")
					->commit("header");
   				$total+=$data[$i]['total_harga'];
   				$grand_total+=$data[$i]['total_harga'];
   			}
   			if($group==true){
				$tp	->addColumn("Total", 6+count($arrayDataUnit),"right")
					->addColumn(number_format($total,0,',','.'), 1,"right")
					->commit("header");
   				
   			}
			$tp	->addColumn("Grand Total", 6+count($arrayDataUnit),"right")
				->addColumn(number_format($grand_total,0,',','.'), 1,"right")
				->commit("header");
	   	}
			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 8,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_stok_obat_unit.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
	}
}
?>