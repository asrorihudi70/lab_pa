<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Tb_apt_unit extends TblBase
{
        function __construct() {
			$this->TblName='apt_unit';
			TblBase::TblBase(true);

			$this->SqlQuery= "SELECT kd_unit_far as kode, nm_unit_far as nama_unit FROM apt_unit ORDER BY nm_unit_far ";
        }

	function FillRow($rec)
	{
		$row=new Rowaptunit;
		$row->KODE=$rec->kode;
		$row->NAMA_UNIT=$rec->nama_unit;

		return $row;
	}
}
class Rowaptunit
{
    public $KODE;
    public $NAMA_UNIT;

}

?>