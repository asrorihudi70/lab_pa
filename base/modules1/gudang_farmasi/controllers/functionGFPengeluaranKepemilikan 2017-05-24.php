<?php

/**
 * @author M
 * @copyright NCI 2015
 */


//class main extends Controller {
class FunctionGFPengeluaranKepemilikan extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
			 $this->load->library('common');
    }
	 
	 public function index()
    {
        
		$this->load->view('main/index');

    }
	public function getDataGridAwal_order(){
		
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select aru.*,nm_unit_far,am.milik 
		from apt_ro_unit aru inner join  apt_unit au on au.kd_unit_far=aru.kd_unit_far
		inner join apt_milik am  on am.kd_milik=aru.kd_milik  where no_out not in
		(select distinct no_out  from apt_ro_unit_det where qty_ordered>0) 
		and aru.kd_unit_far_tujuan='".$unitfar."' limit 100")->result();
			
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getMilikTujuan(){
		$kd_milik = $this->session->userdata['user_id']['aptkdmilik'];
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select * from apt_milik
									where kd_milik not in(".$kd_milik.")
									order by milik ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getMilik(){
		$kd_milik = $this->session->userdata['user_id']['aptkdmilik'];
		$result=$this->db->query("select * from apt_milik
									order by milik ")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		$jsonResult['currentMilik']=$kd_milik;
		echo json_encode($jsonResult);
	}
	
	public function delete(){
		$deletegin=$this->db->query("delete from apt_out_milik_det_gin where no_out='".$_POST['no_out']."' 
									and kd_prd='".$_POST['kd_prd']."'");
		$delete=$this->db->query("delete from apt_out_milik_det where no_out='".$_POST['no_out']."' 
									and kd_prd='".$_POST['kd_prd']."' and out_urut=".$_POST['out_urut']."");
		if($delete)
		{
			echo"{success:true}";
		}else{
			echo"{success:false}";
		}
	}
	public function getDataGridAwal(){
		$cNokeluar="";
		if($_POST['no_keluar']!="")
		{
			$cNokeluar=" and lower(no_out) like '".$_POST['no_keluar']."%'";
		}
		if($_POST['tgl_out_awal'] !='' || $_POST['tgl_out_akhir'] !=''){
			$ctgl="(tgl_out) >= '".$_POST['tgl_out_awal']."' and (tgl_out) <= '".$_POST['tgl_out_akhir']."'";
		} else{
			$ctgl="tgl_out = '".date('Y-m-d')."'";
		}
		
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select aom.*,au.nm_unit_far,am.milik,m.milik as milik_tujuan,
									case when posting=0 then 'f' else 't' end as status_posting  
								from apt_out_milik aom
									inner join apt_unit au on au.kd_unit_far=aom.kd_unit_far
									inner join apt_milik am on am.kd_milik=aom.kd_milik
									inner join apt_milik m on m.kd_milik=aom.kd_milik_out
								where $ctgl
								$cNokeluar
								order by aom.no_out
								limit 100 ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function getDataGrid_detail(){
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		$result=$this->db->query("select aomd.kd_prd, ao.nama_obat,ao.fractions,ao.kd_sat_besar,aomd.kd_milik,
									aomd.out_urut,sum(asug.jml_stok_apt) as jml_stok_apt,aomd.jml_out 
									from apt_out_milik_det aomd
										inner join apt_obat ao on ao.kd_prd=aomd.kd_prd
										left join apt_stok_unit_gin asug on asug.kd_prd=aomd.kd_prd and asug.kd_milik=aomd.kd_milik and asug.kd_unit_far='".$kd_unit_far."'
										inner join apt_milik am on am.kd_milik=aomd.kd_milik
									where no_out='".$_POST['no_out']."' 
									group by aomd.kd_prd, ao.nama_obat,ao.fractions,ao.kd_sat_besar,aomd.kd_milik,aomd.out_urut,aomd.jml_out
									order by aomd.out_urut asc")->result();
		$arr=array();
		for($i=0;$i<count($result);$i++){
			$arr[$i]['kd_prd']			=$result[$i]->kd_prd;
			$arr[$i]['kd_milik']		=$result[$i]->kd_milik;
			$arr[$i]['nama_obat']		=$result[$i]->nama_obat;
			$arr[$i]['kd_sat_besar']	=$result[$i]->kd_sat_besar;
			$arr[$i]['qty']				=$result[$i]->jml_out;
			$arr[$i]['qty_b']			=$result[$i]->jml_out/$result[$i]->fractions;
			$arr[$i]['fractions']		=$result[$i]->fractions;
			$arr[$i]['out_urut']		=$result[$i]->out_urut;
			$arr[$i]['jml_stok_apt']	=$result[$i]->jml_stok_apt;
		}	
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($arr).'}';
	}
	
	public function getDataGrid_detail_pengeluaran(){
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
			$result=$this->db->query("select apr.no_out as no_minta,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,
				sum( B.jml_stok_apt) as jml_stok_apt,apr.qty from apt_ro_unit_det apr 
				inner JOIN APt_OBAT AO ON apr.KD_PRD=AO.KD_PRD 
				INNER JOIN apt_produk C ON C.kd_prd=AO.kd_prd 
				INNER JOIN apt_stok_unit_gin B ON B.kd_prd=AO.kd_prd 
				where no_out='".$_POST['no_minta']."' and B.kd_unit_far='".$unitfar."' 
				GROUP BY apr.no_out,apr.kd_prd,AO.NAMA_OBAT,AO.kd_sat_besar,AO.fractions,C.harga_beli,apr.qty
				order by AO.NAMA_OBAT asc")->result();
			
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}

	
	public function getstock_obat(){	
		$unitfar = $this->session->userdata['user_id']['aptkdunitfar'];
		$kd_milik=$_POST['kd_milik'];
			
		$result=$this->db->query("SELECT  MAX(GIN.KD_PRD)AS KD_PRD,AO.NAMA_OBAT ,
										MAX(AO.KD_SAT_BESAR)AS kd_sat_besar,AO.fractions,SUM(GIN.JML_STOK_APT) AS JML_STOK_APT,GIN.kd_milik 
								FROM apt_stok_unit_gin GIN
										INNER JOIN APt_OBAT AO ON GIN.KD_PRD=AO.KD_PRD
										INNER JOIN APT_PRODUK AP ON  AO.KD_PRD= AP.KD_PRD
								WHERE (AO.aktif='t' and KD_UNIT_FAR='".$unitfar."' AND AP.TAG_BERLAKU=0 
										and	lower(AO.NAMA_OBAT) like lower('".$_POST['text']."%') 
										and GIN.kd_milik=".$kd_milik.") 
										or (AO.aktif='t' and KD_UNIT_FAR='".$unitfar."' AND AP.TAG_BERLAKU=0
										and lower(GIN.KD_PRD) like lower('".$_POST['text']."%')
										and GIN.kd_milik=".$kd_milik.")
								GROUP BY GIN.KD_PRD,AO.NAMA_OBAT,GIN.kd_milik,AO.fractions
								ORDER BY AO.NAMA_OBAT
								LIMIT 10")->result();
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getNoKeluarKepemilikan(){
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$today=date('Y/m');
		$today=date('d-M-Y');
		$thisMonth=date('m');
		$nomor_out_milik=$this->db->query("select nomor_out_milik from apt_unit
									where kd_unit_far='".$kdUnit."' ")->row()->nomor_out_milik+1;
		$no_out=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_out_milik,4,"0", STR_PAD_LEFT);
		
		return $no_out;
	}
	public function save()
	{
		$kd_milik=isset($this->session->userdata['user_id']['aptkdmilik']);
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		if (isset ($this->session->userdata['user_id'])){
			if( $kd_milik=="" ||$kd_unit_far==""){	
				echo "{login_error:true}";
			}else{
				if($_POST['no_keluar']==""){
					$no_keluar=$this->getNoKeluarKepemilikan();
					$data=array(
						 "no_out"=>$no_keluar ,
						 "kd_milik"=>$_POST['kd_milik'],
						 "kd_unit_far"=>$kd_unit_far,
						 "tgl_out"=>$_POST['tgl_keluar'],
						 "kd_milik_out"=>$_POST['kd_milik_tujuan'],
						 "posting"=>0,
						 "remark"=>$_POST['remark']
					 ); 
					$simpan=$this->db->insert("apt_out_milik",$data);
					if($simpan) {
						for($i=0 ; $i<$_POST['jmlList'] ; $i++){
							$datadet=array(
							  "no_out"  =>$no_keluar,
							  "kd_milik"	=>$_POST['kd_milik'.$i],
							  "kd_prd"  	=>$_POST['kd_prd'.$i],
							  "out_urut"	=>$i+1,
							  "jml_out"		=>$_POST['qty'.$i],
							);
							$simpan_det=$this->db->insert("apt_out_milik_det",$datadet);
						}
						if($simpan_det)	{
							# UPDATE nomor_out_milik di apt_unit
							$update_nomor_out_milik= $this->db->query("update apt_unit set nomor_out_milik =".substr($no_keluar,-4)." where kd_unit_far='".$kd_unit_far."'");
							if($update_nomor_out_milik){
								echo "{simpan_det:true,no_keluar:'".$no_keluar."'}"; 
							} else{
								echo "{simpan:false}"; 
							}
						}else{
							echo "{simpan_det:false}"; 
						}
					 }else{
						echo "{simpan:false}"; 
					 }
				}else{	
					$no_keluar=$_POST['no_keluar'];
					for($i=0 ; $i<$_POST['jmlList'] ; $i++){
						$query_cari=$this->db->query("select * from apt_out_milik_det
							where no_out='$no_keluar' and kd_prd='".$_POST['kd_prd'.$i]."'")->result();
						if(count($query_cari)==0){
							$datadet=array(
							  "no_out" 		=>$no_keluar,
							  "kd_milik"	=>$_POST['kd_milik'.$i],
							  "kd_prd"  	=>$_POST['kd_prd'.$i],
							  "out_urut"	=>$_POST['out_urut'.$i],
							  "jml_out"		=>$_POST['qty'.$i],
							);
							$simpan_det=$this->db->insert("apt_out_milik_det",$datadet);
						} else{
							$simpan_det=$this->db->query("update apt_out_milik_det set jml_out='".$_POST['qty'.$i]."'
							where no_out='$no_keluar' and kd_prd='".$_POST['kd_prd'.$i]."' and out_urut=".$_POST['out_urut'.$i]."");
						}
					}
					
					if($simpan_det)	{
						echo "{simpan_det:true,no_keluar:'".$no_keluar."'}"; 
					}else{
						echo "{simpan_det:false}"; 
					}
				}
			}
		}else{
			echo "{login_error:true}";
		}
			
	}
	
	public function deletetrans(){
		$this->db->trans_begin();
		$kd_form=11;
		$kdUser = $this->session->userdata['user_id']['id'] ;
		$user_name = $this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$kdUser."'")->row()->user_names;
		$result = $this->db->query("SELECT no_out,kd_unit_far FROM apt_ro_unit WHERE no_out='".$_POST['no_out']."'")->row();
		$qcek=$this->db->query("select count(kd_form) as jml from apt_history_trans where kd_form='$kd_form'")->row()->jml+1;
		
		$data=array("kd_form"=>$kd_form,
					"no_faktur"=>$_POST['no_out'].'-'.$qcek,
					"kd_unit_far"=>$result->kd_unit_far,
					"tgl_del"=>date("Y-m-d"),
					"kd_customer"=>"",
					"nama_customer"=>"", 
					"kd_unit"=>"", 
					"kd_user_del"=>$kdUser,
					"user_name"=>$user_name,
					"jumlah"=>0,
					"ket_batal"=>$_POST['alasan']);
		
		$insert=$this->db->insert("apt_history_trans",$data);
		if($insert){
			$delete = $this->db->query("DELETE FROM apt_ro_unit WHERE no_out='".$_POST['no_out']."'");
			if($delete){
				$this->db->trans_commit();
				echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}
		
	}
	
	
	function posting(){
		$this->db->trans_begin();
		$strError='';
   		$result= $this->db->query("SELECT * FROM apt_out_milik WHERE no_out='".$_POST['no_keluar']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_out)))." as month 
									FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' 
									AND years=".((int)date("Y",strtotime($result->tgl_out))))->row();
    	# CEK PERIODE
		if($period->month==1){
    		echo "{success:false,pesan:'Periode Sudah Ditutup'}"; 
			exit;
    	}
		
    	if($result->posting==0){
			$apt_out_milik=array();
	    	$apt_out_milik['posting']= 1;
			# CEK STOK
	    	for($i=0 ; $i<count($_POST['jmlList']) ; $i++){
	    		$cekstok=$this->db->query("SELECT kd_unit_far,kd_milik,kd_prd,sum(jml_stok_apt)as jml_stok_apt
											FROM apt_stok_unit_gin 
											WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$_POST['kd_prd'.$i]."' 
												AND kd_milik='".$result->kd_milik."' AND jml_stok_apt > 0 
											GROUP BY kd_unit_far,kd_milik,kd_prd");
	    		if(count($cekstok->result())>0){
	    			if($cekstok->row()->jml_stok_apt<$_POST['qty'.$i]){
	    				$jsonResult['processResult']='ERROR';
	    				$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'.$i].'" tidak mencukupi.';
	    				echo "{success:false,pesan:'Obat dengan kode obat ".$_POST['kd_prd'.$i]." tidak mencukupi.'}"; 
	    				exit;
	    			}
	    		}else{
	    			$jsonResult['processResult']='ERROR';
	    			$jsonResult['processMessage']='Obat dengan kode obat "'.$_POST['kd_prd'.$i].'" tidak mencukupi.';
	    			echo "{success:false,pesan:'Obat dengan kode obat ".$_POST['kd_prd'.$i]." tidak mencukupi.'}"; 
					exit;
	    		}
	    	}
	    	$criteriahead=array('no_out'=>$_POST['no_keluar']);
	    	# END CEK STOK
			
			# CREATE OR UPDATE APT_OUT_MILIK_DET_GIN
			for($i=0 ; $i<$_POST['jmlList']; $i++){
				$jml=$_POST['qty'.$i];
				$jumlah=0; # sebagai variabel untuk menampung jml gin untuk disimpan sebagai stok
				$tmp=0; # sebagai variabel tampungan untuk pembanding jml gin yg kurang mencukupi stoknya
				
				# GET GIN
				$getgin=$this->db->query("SELECT * FROM apt_stok_unit_gin 
										WHERE kd_unit_far='".$result->kd_unit_far."' 
											AND kd_prd='".$_POST['kd_prd'.$i]."' 
											AND kd_milik='".$result->kd_milik."'
											AND jml_stok_apt > 0 
										ORDER BY gin asc ")->result();
				
				for($j=0; $j<count($getgin);$j++) {
					if($tmp != $_POST['qty'.$i]){
						$apt_out_milik_det_gin=array();
						
						# UPDATE APT_STOK_UNIT_GIN -> stok dari kd_milik yg mengeluarkannya
						
						# CEK STOK GIN 
						if($jml >= $getgin[$j]->jml_stok_apt){
							$jml=$jml - $getgin[$j]->jml_stok_apt;
							$apt_out_milik_det_gin['jml_out']=$getgin[$j]->jml_stok_apt;
							
							$result_cur_milik_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$getgin[$j]->jml_stok_apt." 
														where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'.$i]."' 
														and kd_unit_far = '".$result->kd_unit_far."' and kd_milik = ".$result->kd_milik."");
							$jumlah=$getgin[$j]->jml_stok_apt;
							$tmp += $jumlah;
						} else if ($jml>0 && $jml < $getgin[$j]->jml_stok_apt) {
							$apt_out_milik_det_gin['jml_out']=$jml;
							
							$result_cur_milik_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$jml." 
													where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'.$i]."' 
													and kd_unit_far = '".$result->kd_unit_far."' and kd_milik = ".$result->kd_milik."");
							$jumlah=$jml;
							$tmp += $jumlah;
							//break;
						}
						
						# UPDATE APT_STOK_UNIT_GIN -> stok kd_milik yg menerima obat dari yg mengeluarkannya
						if($result_cur_milik_apt_stok_unit_gin){
							# CEK GIN kepemilikan tujuan dari gin yg mengdistribusikan
							# jika tersedia makan di update -> jumlah STOK di KEPEMILIKAN TUJUAN ditambah dengan STOK KEPEMILIKAN yg di DISTRIBUSIKAN
							# jika tidak tersedia makan create sesuai gin dan jumlah stok yg di distribusikan 
							
							$getgin2=$this->db->query("SELECT * FROM apt_stok_unit_gin 
												WHERE kd_unit_far='".$result->kd_unit_far."' 
													AND kd_prd='".$_POST['kd_prd'.$i]."' 
													AND kd_milik='".$result->kd_milik_out."'
													AND gin='".$getgin[$j]->gin."'
													--AND jml_stok_apt > 0 
												ORDER BY gin asc limit 1 ")->result();
							if(count($getgin2)>0 && $getgin[$j]->jml_stok_apt >= 0){
								$result_far_apt_stok_unit_gin = $this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt + ".$jumlah." 
													where gin ='".$getgin[$j]->gin."' and kd_prd = '".$_POST['kd_prd'.$i]."' 
													and kd_unit_far = '".$result->kd_unit_far."' and kd_milik = ".$result->kd_milik_out."");
							} else{
								/* $detobat=$this->db->query("select * from apt_obat_in_detail where kd_prd='".$_POST['kd_prd'.$i]."' and gin='".$getgin[$j]->gin."'");
								if(count($detobat->result()) > 0){
									$batch=$detobat->row()->batch;
									$harga=$detobat->row()->hrg_beli_obt;
								} else{
									$batch="";
									$harga=0;
								} */
								$apt_stok_unit_gin['gin']=$getgin[$j]->gin;
								$apt_stok_unit_gin['kd_unit_far']=$result->kd_unit_far;
								$apt_stok_unit_gin['kd_prd']=$_POST['kd_prd'.$i];
								$apt_stok_unit_gin['kd_milik']=$result->kd_milik_out;
								$apt_stok_unit_gin['jml_stok_apt']=$jumlah;
								$apt_stok_unit_gin['batch']=$getgin[$j]->batch;
								$apt_stok_unit_gin['harga']=$getgin[$j]->harga;
								
								$result_far_apt_stok_unit_gin=$this->db->insert('apt_stok_unit_gin',$apt_stok_unit_gin);
							}
						} else{
							$strError='ERROR';
						}
						
						# UPDATE or CREATE apt_out_milik_det_gin
						$detailsgin=$this->db->query("SELECT * FROM apt_out_milik_det_gin WHERE no_out='".$_POST['no_keluar']."' AND out_urut=".($i+1)." and gin='".$getgin[$j]->gin."'")->result();
						
						if(count($detailsgin)>0){
							$array = array('no_out =' => $_POST['no_keluar'],'kd_prd' =>$_POST['kd_prd'.$i], 'out_urut =' => ($i+1),'gin =' =>$getgin[$j]->gin);
							
							$this->db->where($array);
							$result_apt_out_milik_det_gin=$this->db->update('apt_out_milik_det_gin',$apt_out_milik_det_gin);
						}else{
							$apt_out_milik_det_gin['no_out']=$_POST['no_keluar'];
							$apt_out_milik_det_gin['kd_prd']=$_POST['kd_prd'.$i];
							$apt_out_milik_det_gin['kd_milik']=$result->kd_milik;
							$apt_out_milik_det_gin['out_urut']=$i+1;
							$apt_out_milik_det_gin['gin']=$getgin[$j]->gin;
							$apt_out_milik_det_gin['kd_unit_far']=$result->kd_unit_far;
							$apt_out_milik_det_gin['jml_out']=$_POST['qty'.$i];
							
							
							$result_apt_out_milik_det_gin=$this->db->insert('apt_out_milik_det_gin',$apt_out_milik_det_gin);
						}
						
						if($result_apt_out_milik_det_gin){
							$strError='SUCCESS';
						} else{
							$strError='ERROR';
						}
					}
				}
				
			}
			
			if($strError == 'SUCCESS'){
				$this->db->where($criteriahead);
				$update_apt_out_milik=$this->db->update('apt_out_milik',$apt_out_milik);
				if($update_apt_out_milik){
					$strError = 'SUCCESS';
				} else{
					$strError = 'ERROR';
				}
			} else{
				$strError = 'ERROR';
			}
			
    	
			if ($strError == 'ERROR'){
   				$this->db->trans_rollback();
				echo "{success:false,pesan:'Kesalahan Pada Database, Hubungi Admin!'}"; 
   			}else{
   				$this->db->trans_commit();
				echo "{success:true}";
   			}
   		}else{
			echo "{success:false,pesan:'Sebelum Menyimpan Harap Unposting terlebih dahulu.'}"; 
   		}
	}
	
	function unposting(){
		$this->db->trans_begin();
		$strError="";
   		$result= $this->db->query("SELECT * from apt_out_milik WHERE no_out='".$_POST['no_keluar']."'")->row();
    	$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->tgl_out)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->tgl_out))))->row();
    	if($period->month==1){
			echo "{success:false,pesan:'Periode Sudah Ditutup.'}"; 
    		exit;
    	}
		
    	if($result->posting==1){
			# CEK STOK OBAT sebelum di posting
   			$cekstok=$this->db->query("select aom.no_out,aomdg.kd_prd,sum(aomdg.jml_out)as jml_out,sum(asug.jml_stok_apt)as stok_tujuan from apt_out_milik aom
										inner join apt_out_milik_det_gin aomdg on aomdg.no_out=aom.no_out
										inner join apt_stok_unit_gin asug on asug.kd_unit_far=aomdg.kd_unit_far and aomdg.kd_prd=asug.kd_prd 
											and aomdg.gin=asug.gin and asug.kd_milik=".$result->kd_milik_out."
										inner join apt_stok_unit_gin asugs on asugs.kd_unit_far=aomdg.kd_unit_far and aomdg.kd_prd=asugs.kd_prd 
											and aomdg.gin=asugs.gin and asugs.kd_milik=aomdg.kd_milik  
										where aom.no_out='".$_POST['no_keluar']."'
										group by aom.no_out,aomdg.kd_prd")->result();
			for($a=0; $a<count($cekstok); $a++){
   				if($cekstok[$a]->jml_out > $cekstok[$a]->stok_tujuan){
					echo "{success:false,pesan:'Gagal posting! Stok Obat kode produk ".$cekstok[$a]->kd_prd." tidak mencukupi/sudah dipakai.'}"; 
					exit;
   				}
   			}
			
			# UPDATE status posting
   			$apt_out_milik=array();
   			$apt_out_milik['posting']= 0;
   			$criteria=array('no_out'=>$_POST['no_keluar']);
   			
   			$this->db->where($criteria);
   			$update_apt_out_milik=$this->db->update('apt_out_milik',$apt_out_milik);
   			
			#----------------------- UPDATE STOK ------------------------------
			if($update_apt_out_milik){
				$details=$this->db->query("select * from apt_out_milik_det_gin where no_out='".$_POST['no_keluar']."'")->result();
				if(count($details)>0){
					
					for($i=0;$i<count($details);$i++){
						
						$update_apt_out_milik_det_gin=$this->db->query("update apt_out_milik_det_gin set jml_out = jml_out - ".$details[$i]->jml_out."
													WHERE no_out='".$_POST['no_keluar']."' and kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$details[$i]->kd_prd."' 
													AND kd_milik='".$result->kd_milik."' and gin='".$details[$i]->gin."' and out_urut=".$details[$i]->out_urut."");
													
						if($update_apt_out_milik_det_gin){
							# UPDATE APT_STOK_UNIT_GIN -> stok kd_milik yg menerima obat dari yg mengeluarkannya 
							$update_far_apt_stok_unit_gin=$this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt - ".$details[$i]->jml_out."
													WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$details[$i]->kd_prd."' 
													AND kd_milik='".$result->kd_milik_out."' and gin='".$details[$i]->gin."'");
							
							if($update_far_apt_stok_unit_gin){
								# UPDATE APT_STOK_UNIT_GIN -> stok kd_milik yg mengeluarkan
								$update_cur_apt_stok_unit_gin=$this->db->query("update apt_stok_unit_gin set jml_stok_apt = jml_stok_apt + ".$details[$i]->jml_out."
													WHERE kd_unit_far='".$result->kd_unit_far."' AND kd_prd='".$details[$i]->kd_prd."' 
													AND kd_milik='".$result->kd_milik."' and gin='".$details[$i]->gin."'");
								if($update_cur_apt_stok_unit_gin){
									$strError='SUCCESS';
								} else{
									$strError='ERROR';
								}
							} else{
								$strError='ERROR';
							}
						} else{
							$strError='ERROR';
						}
					}
				}
			} else{
				$strError='ERROR';
			}
   		}
   		if ($strError == 'ERROR'){
   			$this->db->trans_rollback();
			echo "{success:false,pesan:'Kesalahan Pada Database, Hubungi Admin!'}"; 
   		}else{
   			$this->db->trans_commit();
			echo "{success:true,pesan:'Unposting berhasil.'}"; 
   		}
	}
	
	
}