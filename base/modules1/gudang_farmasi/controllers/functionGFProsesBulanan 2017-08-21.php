<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFProsesBulanan extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('result');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	
   	public function doProcess(){
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		
   		$mutasi_this_month=$this->db->query("SELECT kd_prd FROM apt_mutasi WHERE kd_unit_far='".$kd_unit_far."' AND kd_milik='".$kdMilik."' AND years=".$_POST['year']." AND months=".$_POST['month'])->result();
   		
   		$periode_last_month=$this->common->getPeriodeLastMonth($_POST['year'],$_POST['month'],$kd_unit_far);
   		$periode_this_month=$this->common->getPeriodeThisMonth($_POST['year'],$_POST['month'],$kd_unit_far);
   		
   		if($periode_last_month==1){
   			if($periode_this_month==0){
   				$this->db->trans_begin();
   				for($i=0,$iLen=count($mutasi_this_month); $i<$iLen ; $i++){
   					$apt_mutasi=array();
   					$apt_mutasi['inqty']=0;
   					$apt_mutasi['outjualqty']=0;
   					$apt_mutasi['outqty']=0;
   					$apt_mutasi['inreturresep']=0;
   					$apt_mutasi['outhapus']=0;
   					$apt_mutasi['outreturpbf']=0;
   					$apt_mutasi['inunit']=0;
   					$apt_mutasi['outmilik']=0;
   					$apt_mutasi['adjustqty']=0;
   					$criteria=array('years'=>$_POST['year'],'months'=>$_POST['month'],'kd_prd'=>$mutasi_this_month[$i]->kd_prd,'kd_milik'=>$kdMilik,'kd_unit_far'=>$kd_unit_far);
   					
   					/*
   					 * update postgre
   					 */
   					$this->db->where($criteria);
   					$this->db->update('apt_mutasi',$apt_mutasi);
   					/*
   					 * update sql server
   					 */
   					_QMS_update('apt_mutasi',$apt_mutasi,$criteria);
   					
   				}
   				
   				$inqty=$this->db->query("SELECT A.kd_prd, sum(A.jml_in_obt) AS total FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON B.no_obat_in=A.no_obat_in WHERE EXTRACT(MONTH FROM B.tgl_obat_in)=".$_POST['month']." 
   					AND EXTRACT(YEAR FROM B.tgl_obat_in)=".$_POST['year']."  AND B.kd_unit_far='".$kd_unit_far."' AND B.posting=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('inqty', $inqty);
   				
   				$outqty=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_stok_out_det A 
					INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']." 
   					AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." AND B.kd_unit_cur='".$kd_unit_far."' AND B.post_out=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outqty', $outqty);
   				
   				$outjualqty=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_barang_out_detail A 
					INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE 
					EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']." 
					AND B.kd_unit_far='".$kd_unit_far."' AND B.tutup=1 AND returapt=0
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outjualqty', $outjualqty);
   				
   				$outreturresep=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_barang_out_detail A
					INNER JOIN apt_barang_out B ON B.no_out=A.no_out AND B.tgl_out=A.tgl_out WHERE
					EXTRACT(MONTH FROM B.tgl_out)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_out)=".$_POST['year']."
					AND B.kd_unit_far='".$kd_unit_far."' AND B.tutup=1  AND returapt=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('inreturresep', $outreturresep);
   				
   				$outhapus=$this->db->query("SELECT A.kd_prd, sum(A.qty_hapus) AS total FROM apt_hapus_det A 
					INNER JOIN apt_hapus B ON B.no_hapus=A.no_hapus WHERE 
   					EXTRACT(MONTH FROM B.hps_date)=".$_POST['month']." AND EXTRACT(YEAR FROM B.hps_date)=".$_POST['year']." 
					AND B.kd_unit_far='".$kd_unit_far."' AND B.post_hapus=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outhapus', $outhapus);
   				
   				$outreturpbf=$this->db->query("SELECT A.kd_prd, sum(A.ret_qty) AS total FROM apt_ret_det A 
					INNER JOIN apt_retur B ON B.ret_number=A.ret_number WHERE 
   					EXTRACT(MONTH FROM B.ret_date)=".$_POST['month']." AND EXTRACT(YEAR FROM B.ret_date)=".$_POST['year']."  
					AND B.kd_unit_far='".$kd_unit_far."' AND B.ret_post=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('outreturpbf', $outreturpbf);
   				
   				$inunit=$this->db->query("SELECT A.kd_prd, sum(A.jml_out) AS total FROM apt_stok_out_det A
					INNER JOIN apt_stok_out B ON B.no_stok_out=A.no_stok_out WHERE EXTRACT(MONTH FROM B.tgl_stok_out)=".$_POST['month']."
   					AND EXTRACT(YEAR FROM B.tgl_stok_out)=".$_POST['year']." AND B.kd_unit_far='".$kd_unit_far."' AND B.post_out=1
					group by A.kd_prd")->result();
   				
   				$this->doMutasi('inunit', $inunit);
   				
   				$adjust_qty=$this->db->query("SELECT A.kd_prd, sum(A.stok_akhir-A.stok_awal) AS total FROM apt_stok_opname_det A 
					INNER JOIN apt_stok_opname B ON B.no_so=A.no_so WHERE 
					EXTRACT(MONTH FROM B.tgl_so)=".$_POST['month']." AND EXTRACT(YEAR FROM B.tgl_so)=".$_POST['year']." 
					AND A.kd_unit_far='".$kd_unit_far."' AND B.approve=true
					group by A.kd_prd")->result();
   					
   				$this->doMutasi('adjustqty', $adjust_qty);
   				
   				if ($this->db->trans_status() === FALSE){
   					$this->db->trans_rollback();
   					$this->result->result='ERROR';
   					$this->result->message='Kesalahan Pada Database, Hubungi Admin.';
   				}else{
   					$this->db->trans_commit();
   					$this->result->message='Data Berhasil diMutasi.';
   				}
   			}else{
   				$this->result->result='ERROR';
   				$this->result->message='Periode Bulan Ini Sudah diTutup.';
   			}
   		}else{
   			$this->result->result='ERROR';
   			$data="Bulan ";
   			if($_POST['month']==1){
   				$data.='Desember Tahun '.($_POST['year']-1);
   			}else if($_POST['month']==2){
   				$data.='Januari Tahun '.$_POST['year'];
   			}else if($_POST['month']==3){
   				$data.='Februari Tahun '.$_POST['year'];
   			}else if($_POST['month']==4){
   				$data.='Maret Tahun '.$_POST['year'];
   			}else if($_POST['month']==5){
   				$data.='April Tahun '.$_POST['year'];
   			}else if($_POST['month']==6){
   				$data.='Mei Tahun '.$_POST['year'];
   			}else if($_POST['month']==7){
   				$data.='Juni Tahun '.$_POST['year'];
   			}else if($_POST['month']==8){
   				$data.='Juli Tahun '.$_POST['year'];
   			}else if($_POST['month']==9){
   				$data.='Agustus Tahun '.$_POST['year'];
   			}else if($_POST['month']==10){
   				$data.='September Tahun '.$_POST['year'];
   			}else if($_POST['month']==11){
   				$data.='Oktober Tahun '.$_POST['year'];
   			}else if($_POST['month']==12){
   				$data.='November Tahun '.$_POST['year'];
   			}
   			$data.=' Harap Tututp Periodenya terlebih dahulu.';
   			$this->result->message=$data;
   		}
   		$this->result->data=count($mutasi_this_month);
   		echo json_encode($this->result);
   	}
   	
   	public function getPeriode(){
   		$result=$this->db->query("SELECT * FROM periode_inv WHERE kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' AND years=".$_POST['year']);
   		if(count($result->result())>0){
   			$res=$result->row();
   			$months=array();
   			$month=array();
   			$month['month']='Januari';
   			$month['stat']=$res->m1;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Februari';
   			$month['stat']=$res->m2;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Maret';
   			$month['stat']=$res->m3;
   			$months[]=$month;
   			$month=array();
   			$month['month']='April';
   			$month['stat']=$res->m4;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Mei';
   			$month['stat']=$res->m5;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Juni';
   			$month['stat']=$res->m6;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Juli';
   			$month['stat']=$res->m7;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Agustus';
   			$month['stat']=$res->m8;
   			$months[]=$month;
   			$month=array();
   			$month['month']='September';
   			$month['stat']=$res->m9;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Oktober';
   			$month['stat']=$res->m10;
   			$months[]=$month;
   			$month=array();
   			$month['month']='November';
   			$month['stat']=$res->m11;
   			$months[]=$month;
   			$month=array();
   			$month['month']='Desember';
   			$month['stat']=$res->m12;
   			$months[]=$month;
   			
   			$this->result->data=$months;
   		}else{
   			$this->result->result='ERROR';
   			$this->result->message='Tidak Ada Data.';
   		}
   		
   		echo json_encode($this->result);
   	}
   	
   	private function doMutasi($fields,$arr){
   		$kd_unit_far=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		
   		for($i=0,$iLen=count($arr); $i<$iLen; $i++){
   			$cek=$this->db->query("SELECT ".$fields." FROM apt_mutasi WHERE years=".$_POST['year']." AND months=".$_POST['month']."
   							AND kd_prd='".$arr[$i]->kd_prd."' AND kd_milik='".$kdMilik."' AND kd_unit_far='".$kd_unit_far."'");
   			if($cek->num_rows()>0){
   				$apt_mutasi=array();
   				$apt_mutasi[$fields]=$cek->row()->$fields+$arr[$i]->total;
   				$criteria=array('years'=>$_POST['year'],'months'=>$_POST['month'],'kd_prd'=>$arr[$i]->kd_prd,'kd_milik'=>$kdMilik,'kd_unit_far'=>$kd_unit_far);
   					
   				/*
   				 * update postgre
   				 */
   				$this->db->where($criteria);
   				$this->db->update('apt_mutasi',$apt_mutasi);
   				/*
   				 * update sql server
   				 */
   				_QMS_update('apt_mutasi',$apt_mutasi,$criteria);
   				
   			}else{
   				$apt_mutasi=array();
   				$apt_mutasi['years']=$_POST['year'];
   				$apt_mutasi['months']=$_POST['month'];
   				$apt_mutasi['kd_prd']=$arr[$i]->kd_prd;
   				$apt_mutasi['kd_milik']=$kdMilik;
   				$apt_mutasi['kd_unit_far']=$kd_unit_far;
   				$apt_mutasi['saldo_awal']=0;
   				$apt_mutasi['inqty']=0;
   				$apt_mutasi['outqty']=0;
   				$apt_mutasi['inreturresep']=0;
   				$apt_mutasi['outhapus']=0;
   				$apt_mutasi['outreturpbf']=0;
   				$apt_mutasi['inunit']=0;
   				$apt_mutasi['outmilik']=0;
   				$apt_mutasi['adjustqty']=0;
   				$apt_mutasi[$fields]=$arr[$i]->total;
   				
   				/*
   				 * insert postgre
   				 */
   				$this->db->insert('apt_mutasi',$apt_mutasi);
   				/*
   				 * insert sql server
   				 */
   				_QMS_insert('apt_mutasi',$apt_mutasi);
   			}
   		}
   	}
}
?>