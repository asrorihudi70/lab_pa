<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class FunctionGFReturPBF extends  MX_Controller {		

    public $ErrLoginMsg='';
	private $dbSQL      = "";
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
		$this->load->model('M_farmasi');
		$this->load->model('M_farmasi_mutasi');
		// $this->dbSQL   = $this->load->database('otherdb2',TRUE);
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
   	
   	public function unposting(){
   		$result= $this->db->query("SELECT * FROM apt_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->ret_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->ret_date))))->row();
   		if($period->month==1){
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Periode Sudah Ditutup.';
   			echo json_encode($jsonResult);
   			exit;
   		}
		
   		if($result->ret_post==1){
   			$this->db->trans_begin();
   			// $this->dbSQL->trans_begin();
			$strError='';
   			$apt_retur=array();
   			$apt_retur['ret_post']= 0;
   			$criteriahead=array('ret_number'=>$_POST['ret_number']);
   			$this->db->where($criteriahead);
   			$this->db->update('apt_retur',$apt_retur);
   			$dets=$this->db->query("SELECT * FROM apt_ret_det WHERE ret_number='".$_POST['ret_number']."'")->result();
   			
			#UPDATE STOK
			for($i=0; $i<count($dets); $i++){
				$paramsStokUnit = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $dets[$i]->kd_prd,
					'kd_milik'		=> $result->kd_milik
				);
				// $rescekstoksql 		= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				$rescekstok 		= $this->M_farmasi->cekStokUnit($paramsStokUnit);
				// if($rescekstok->num_rows > 0 && $rescekstoksql->num_rows > 0 ){
				if($rescekstok->num_rows > 0){
					#UPDATE STOK SQL SERVER
					// $apt_stok_unit_sql		= array('jml_stok_apt' => $rescekstoksql->row()->JML_STOK_APT + $dets[$i]->ret_qty);
					// $updatestoksql 			= $this->M_farmasi->updateStokUnitSQL($paramsStokUnit,$apt_stok_unit_sql);
					
					#UPDATE STOK PG
					$apt_stok_unit			= array('jml_stok_apt' => $rescekstok->row()->jml_stok_apt + $dets[$i]->ret_qty);
					$updatestok 			= $this->M_farmasi->updateStokUnit($paramsStokUnit,$apt_stok_unit);
					
					# UPDATE APT_MUTASI
					// if($updatestoksql > 0 && $updatestok > 0){
					if($updatestok > 0){
						$arr = array(
							'kd_unit_far' 	=> $result->kd_unit_far,
							'kd_prd' 		=> $dets[$i]->kd_prd,
							'kd_milik'		=> $result->kd_milik
							
						);
						$val = array(
							'outreturpbf'	=> $dets[$i]->ret_qty
						);
						$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_retur_pbf_unposting($arr,$val);
						if($update_mutasi_stok){
							$strError='SUCCESS';
						} else{
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							$jsonResult['processResult']='ERROR';
							$jsonResult['processMessage']='Gagal update stok mutasi.';
							echo json_encode($jsonResult);
							exit;
						}
					} else{
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok.';
						echo json_encode($jsonResult);
						exit;
					}
				}
				
   			}
			
   			if($strError=='SUCCESS'){
				$this->db->trans_commit();
   				// $this->dbSQL->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
   			}else{
   				$this->db->trans_rollback();
   				// $this->dbSQL->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
			$jsonResult['processMessage']='Status transaksi belum diPosting. Unposting tidak dapat dilakukan.';
   		}
   		
   		echo json_encode($jsonResult);
   	}
   	
   	public function posting(){	
		$strError='';	
   		$result= $this->db->query("SELECT * FROM apt_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->ret_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->ret_date))))->row();
   		if($period->month==1){
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Periode Sudah Ditutup.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		if($result->ret_post==0){
   			$this->db->trans_begin();
   			// $this->dbSQL->trans_begin();
   			$apt_retur=array();
   			$apt_retur['remark']= $_POST['remark'];
   			$apt_retur['ret_post']= 1;
   			$apt_retur['status_sinkronisasi']= 0;
   			$prd=array();
			
			# CEK KETERSEDIAAN STOK
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$cekstok=$this->db->query("SELECT * FROM apt_stok_unit ast
										INNER JOIN apt_obat_in_detail ao on ao.kd_prd=ast.kd_prd AND ao.kd_milik=ast.kd_milik
											WHERE kd_unit_far='".$result->kd_unit_far."' 
												AND ast.kd_prd='".$_POST['kd_prd'][$i]."' 
												AND ast.kd_milik='".$result->kd_milik."'
												AND ao.no_obat_in='".$_POST['no_obat_in'][$i]."'");
				
				$paramsStokUnit = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $result->kd_milik
				);
				// $rescekstoksql 	= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				// if(($_POST['qty'][$i] > $cekstok->row()->jml_stok_apt) || ($_POST['qty'][$i] > $rescekstoksql->row()->JML_STOK_APT)){
				if($_POST['qty'][$i] > $cekstok->row()->jml_stok_apt){
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Stok Obat kode produk "'.$cekstok->result()[$i]->kd_prd.'" tidak Mencukupi.';
					echo json_encode($jsonResult);
					exit;
				}
   			}
   			$criteriahead=array('ret_number'=>$_POST['ret_number']);
   			
			# UPDATE STOK UNIT
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				$paramsStokUnit = array(
					'kd_unit_far' 	=> $result->kd_unit_far,
					'kd_prd' 		=> $_POST['kd_prd'][$i],
					'kd_milik'		=> $result->kd_milik
				);
				// $rescekstoksql 		= $this->M_farmasi->cekStokUnitSQL($paramsStokUnit);
				$rescekstok 		= $this->M_farmasi->cekStokUnit($paramsStokUnit);
				// if(count($unit->result())>0 && count($rescekstoksql->num_rows > 0)){
				if($rescekstok->num_rows > 0){
					# UPDATE STOK UNIT SQL SERVER
					// $apt_stok_unit_sql		= array('jml_stok_apt' => $rescekstoksql->row()->JML_STOK_APT - $_POST['qty'][$i]);
					// $update_stok_sql 		= $this->M_farmasi->updateStokUnitSQL($paramsStokUnit,$apt_stok_unit_sql);
					
					# UPDATE STOK UNIT PG
					$apt_stok_unit			= array('jml_stok_apt' => $rescekstok->row()->jml_stok_apt - $_POST['qty'][$i]);
					$update_stok	 		= $this->M_farmasi->updateStokUnit($paramsStokUnit,$apt_stok_unit);
					
					# UPDATE APT_MUTASI
					// if($update_stok_sql > 0 && $update_stok > 0){
					if( $update_stok > 0){
						$arr = array(
							'kd_unit_far' 	=> $result->kd_unit_far,
							'kd_prd' 		=> $_POST['kd_prd'][$i],
							'kd_milik'		=> $result->kd_milik,
							'outreturpbf'	=> $_POST['qty'][$i]
						);
						$update_mutasi_stok = $this->M_farmasi_mutasi->apt_mutasi_stok_retur_pbf_posting($arr);
						if($update_mutasi_stok > 0){
							$strError='SUCCESS';
						} else{
							$this->db->trans_rollback();
							// $this->dbSQL->trans_rollback();
							$jsonResult['processResult']='ERROR';
							$jsonResult['processMessage']='Gagal update stok mutasi.';
							echo json_encode($jsonResult);
							exit;
						}
					} else{
						$this->db->trans_rollback();
						// $this->dbSQL->trans_rollback();
						$jsonResult['processResult']='ERROR';
						$jsonResult['processMessage']='Gagal update stok.';
						echo json_encode($jsonResult);
						exit;
					}
				}
				
   			}
			
			if($update_mutasi_stok > 0){
				$this->db->where($criteriahead);
				$update_apt_retur=$this->db->update('apt_retur',$apt_retur);
				if($update_apt_retur){
					$this->db->trans_commit();
					// $this->dbSQL->trans_commit();
					$jsonResult['processResult']='SUCCESS';
					$jsonResult['processMessage']='Posting berhasil dilakukan.';
				} else{
					$this->db->trans_rollback();
					// $this->dbSQL->trans_rollback();
					$jsonResult['processResult']='ERROR';
					$jsonResult['processMessage']='Gagal update status posting.';
				}
			} else{
				$this->db->trans_rollback();
				// $this->dbSQL->trans_rollback();
				$jsonResult['processResult']='ERROR';
				$jsonResult['processMessage']='Gagal update stok mutasi.';
			}
   		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Status transaksi sudah diPosting. Posting tidak dapat dilakukan.';
   		}
   		echo json_encode($jsonResult);
   	}
   	
   	public function getObat(){
   		$result=$this->db->query("SELECT D.kd_milik,A.no_obat_in,A.tgl_exp,A.batch,A.kd_prd, A.jml_in_obt-
					(CASE WHEN (SELECT COUNT(*) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch)>0 THEN 
					(SELECT SUM(F.ret_qty) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch) ELSE 0 END)AS jml_in_obt,
					B.nama_obat,C.satuan,D.harga_beli, A.ppn_item,F.jml_stok_apt,G.milik,A.rcv_line
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan 
					INNER JOIN apt_produk D ON D.kd_prd=A.kd_prd and D.kd_milik=A.kd_milik
					INNER JOIN apt_obat_in E ON E.no_obat_in=A.no_obat_in 
					inner join apt_stok_unit F ON F.kd_prd=A.kd_prd and F.kd_milik=A.kd_milik and F.kd_unit_far=E.kd_unit_far
					inner join apt_milik G ON G.kd_milik=A.kd_milik
				WHERE B.aktif='t'
					AND E.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
					AND E.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." 
					AND upper(B.nama_obat) like upper('%".$_POST['text']."%')
					AND jml_in_obt-(CASE WHEN (SELECT COUNT(*) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch)>0 THEN 
					(SELECT SUM(F.ret_qty) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch) ELSE 0 END)>0 AND
					E.kd_vendor='".$_POST['pbf']."'
   				ORDER BY B.nama_obat ASC")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result;
   		echo json_encode($jsonResult);
   	}
   	
   	public function initTransaksi(){
   		$this->checkBulan();
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
   	
   	public function initList(){
   		// $query="SELECT A.ret_number,A.ret_date,B.vendor,A.ret_post FROM apt_retur A LEFT JOIN 
			// vendor B ON B.kd_vendor=A.kd_vendor WHERE
    		// A.ret_number like'%".$_POST['ret_number']."%' AND
    		// A.kd_vendor like '%".$_POST['kd_vendor']."%'
    		// AND ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND A.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." AND
   			// kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' ORDER BY A.ret_number ASC LIMIT ".$_POST['size']." OFFSET ".$_POST['start'];
   		// $queryTotal="SELECT COUNT(*) AS total FROM apt_retur A LEFT JOIN 
			// vendor B ON B.kd_vendor=A.kd_vendor WHERE
    		// A.ret_number like'%".$_POST['ret_number']."%' AND
    		// A.kd_vendor like '%".$_POST['kd_vendor']."%'
    		// AND ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND A.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." AND
   			// kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."'";
   		// $result=$this->db->query($query);
   		// $total=$this->db->query($queryTotal)->row();
   		// $jsonResult['processResult']='SUCCESS';
   		// $jsonResult['listData']=$result->result();
   		// $jsonResult['total']=$total->total;
   		// echo json_encode($jsonResult);
		$posting = '';
		if($_POST['status_posting'] == 'Belum Posting'){
			$posting = "and A.ret_post =0";
		} else if($_POST['status_posting'] == 'Posting'){
			$posting = "and A.ret_post =1";
		} else{
			$posting = '';
		}
   		$query="SELECT A.ret_number,A.ret_date,B.vendor,A.ret_post FROM apt_retur A LEFT JOIN 
			vendor B ON B.kd_vendor=A.kd_vendor WHERE
    		A.ret_number like'%".$_POST['ret_number']."%' AND
    		A.kd_vendor like '%".$_POST['kd_vendor']."%'
    		AND ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND A.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." AND
   			kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' ".$posting." ORDER BY A.ret_number ASC LIMIT ".$_POST['size']." OFFSET ".$_POST['start'];
   		$queryTotal="SELECT COUNT(*) AS total FROM apt_retur A LEFT JOIN 
			vendor B ON B.kd_vendor=A.kd_vendor WHERE
    		A.ret_number like'%".$_POST['ret_number']."%' AND
    		A.kd_vendor like '%".$_POST['kd_vendor']."%'
    		AND ret_date BETWEEN '".$_POST['startDate']."' AND '".$_POST['lastDate']."' AND A.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." AND
   			kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' ".$posting."";
   		$result=$this->db->query($query);
   		$total=$this->db->query($queryTotal)->row();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['listData']=$result->result();
   		$jsonResult['total']=$total->total;
   		echo json_encode($jsonResult);
   	}
   	
   	public function getForEdit(){
   		$result=$this->db->query("SELECT A.ret_number,A.kd_vendor,B.vendor,A.remark,A.ret_date,A.ret_post FROM apt_retur A INNER JOIN
				vendor B ON B.kd_vendor=A.kd_vendor
   				WHERE ret_number='".$_POST['ret_number']."'");
   	
   		if(count($result->result())>0){
   			$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   			$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   			$jsonResult['resultObject']=$result->row();
   			$det=$this->db->query("SELECT D.kd_milik,A.ret_number,A.no_obat_in,D.hrg_beli_obt as harga_beli,C.satuan,B.nama_obat,B.kd_prd,A.ret_qty AS qty,A.ret_expire AS tgl_exp,A.batch ,A.rcv_line,
										(SELECT SUM(H.jml_in_obt) FROM apt_obat_in_detail H WHERE H.no_obat_in=A.no_obat_in AND H.kd_prd=A.kd_prd AND H.batch=A.batch)-
										(SELECT SUM(G.ret_qty) FROM apt_ret_det G WHERE G.no_obat_in=A.no_obat_in AND G.kd_prd=A.kd_prd AND G.batch=A.batch) + A.ret_qty AS jml_in_obt,A.ret_line,A.ppn_item,A.ret_reduksi
								FROM apt_ret_det A 
									INNER JOIN apt_retur F ON F.ret_number=A.ret_number 
									INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
									LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan 
									INNER JOIN apt_obat_in_detail D on D.no_obat_in=A.no_obat_in and D.rcv_line=A.rcv_line
									INNER JOIN apt_stok_unit E ON E.kd_prd=A.kd_prd AND E.kd_unit_far=F.kd_unit_far AND E.kd_milik=F.kd_milik
   					WHERE A.ret_number='".$_POST['ret_number']."'
					GROUP BY D.kd_milik,A.ret_number,A.no_obat_in,D.hrg_beli_obt,C.satuan,B.nama_obat,B.kd_prd,A.ret_qty,A.ret_expire,A.batch,A.kd_prd,A.ret_line
					order by A.ret_line");
   			$jsonResult['listData']=$det->result();
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Data Tidak Ada.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		$jsonResult['processResult']='SUCCESS';
   		echo json_encode($jsonResult);
   	}
   	
	function checkBulan(){
    	$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
    	$common=$this->common;
    	$thisMonth=(int)date("m");
    	$thisYear=(int)date("Y");
    	$periode_last_month=$common->getPeriodeLastMonth($thisYear,$thisMonth,$kdUnit);
    	$periode_this_month=$common->getPeriodeThisMonth($thisYear,$thisMonth,$kdUnit);
    	
    	$result=$this->db->query("SELECT m".((int)date("m", strtotime("last month")))." as month FROM periode_inv WHERE kd_unit_far='".$kdUnit."' AND years=".date("Y", strtotime("last month")))->row();
    	if($periode_last_month==0){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan Lalu Harap Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}else if($periode_this_month==1){
    		$jsonResult['processResult']='ERROR';
    		$jsonResult['processMessage']='Periode Bulan ini sudah Ditutup.';
    		echo json_encode($jsonResult);
    		exit;
    	}
    }
   	
   	public function save(){
   		$this->checkBulan();
   		$kdMilik=$this->session->userdata['user_id']['aptkdmilik'];
   		$date=new DateTime();
		$thisMonth=date('m');
		$thisYear=substr((int)date("Y"), -2);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		/* 
			Edit by	: MSD
			Tgl		: 06-04-2017
			Ket		: Update Get ret_number

		*/
		/* GET NO RETUR FROM SQL
		$nomor_returSQL=$this->dbSQL->query("SELECT NOMOR_RETUR FROM APT_UNIT
									WHERE KD_UNIT_FAR='".$kdUnit."' ")->row()->NOMOR_RETUR+1;
		$ret_number=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_returSQL,5,"0", STR_PAD_LEFT);
   		
		*/
		$nomor_retur=$this->db->query("select nomor_retur from apt_unit where kd_unit_far='".$kdUnit."' ")->row()->nomor_retur+1;
		$ret_number=$kdUnit.'/'.$thisMonth.'/'.substr(date('Y'),-2).'/'.str_pad($nomor_retur,5,"0", STR_PAD_LEFT);
		$this->db->trans_begin();
   		$apt_retur=array();
   		$apt_retur['ret_number']= $ret_number;
   		$apt_retur['kd_vendor']= $_POST['kd_vendor'] ;
   		$apt_retur['ret_date']= $_POST['ret_date'];
   		$apt_retur['remark']= $_POST['remark'];
   		$apt_retur['kd_unit_far']= $kdUnit;
   		$apt_retur['kd_milik']= $kdMilik;
   		$apt_retur['ppn']= $_POST['ppn'];
   		
   		$this->db->insert('apt_retur',$apt_retur);
   		
   		for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
   			$apt_ret_det=array();
   			$apt_ret_det['ret_number']=$ret_number;
   			$apt_ret_det['no_obat_in']=$_POST['no_obat_in'][$i];
   			$apt_ret_det['ret_line']=$i+1;
   			$apt_ret_det['ret_qty']=$_POST['qty'][$i];
   			$apt_ret_det['kd_milik']= $kdMilik;
   			$apt_ret_det['kd_prd']=$_POST['kd_prd'][$i];
   			$apt_ret_det['ret_expire']=$_POST['tgl_exp'][$i];
   			$apt_ret_det['batch']=$_POST['batch'][$i];
   			$apt_ret_det['ppn_item']=$_POST['ppn_item'][$i];
   			$apt_ret_det['ret_reduksi']=$_POST['ret_reduksi'][$i];
   			$apt_ret_det['rcv_line']=$_POST['rcv_line'][$i];
   			
   			$this->db->insert('apt_ret_det',$apt_ret_det);
   		}
		# UPDATE nomor_retur di apt_unit
		$update_nomor_retur		 = $this->db->query("update apt_unit set nomor_retur =".$nomor_retur." where kd_unit_far='".$kdUnit."'");
		// $update_nomor_returSQL	 = $this->dbSQL->query("update apt_unit set nomor_retur =".$nomor_returSQL." where kd_unit_far='".$kdUnit."'");
   		
   		if ($this->db->trans_status() === FALSE){
   			$this->db->trans_rollback();
   			$jsonResult['processResult']='ERROR';
   		}else{
   			$this->db->trans_commit();
   			$jsonResult['processResult']='SUCCESS';
   			$jsonResult['resultObject']=array('code'=>$ret_number);
   			$jsonResult['listData']=$this->getListDetail($ret_number);
   			$jsonResult['totalLisData']=count($this->getListDetail($ret_number));
   		}
   		
   		echo json_encode($jsonResult);
   	}
   	public function update(){
   		$result= $this->db->query("SELECT * FROM apt_retur WHERE ret_number='".$_POST['ret_number']."'")->row();
   		$period=$this->db->query("SELECT m".((int)date("m",strtotime($result->ret_date)))." as month FROM periode_inv WHERE kd_unit_far='".$result->kd_unit_far."' AND years=".((int)date("Y",strtotime($result->ret_date))))->row();
   		if($period->month==1){
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Periode Sudah Ditutup.';
   			echo json_encode($jsonResult);
   			exit;
   		}
   		if($result->ret_post==0){
   			$this->db->trans_begin();
   			$apt_retur=array();
   			$apt_retur['remark']= $_POST['remark'];
   			$criteria=array('ret_number'=>$_POST['ret_number']);
   			
   			$this->db->where($criteria);
   			$this->db->update('apt_retur',$apt_retur);
   			
   			for($i=0 ; $i<count($_POST['kd_prd']) ; $i++){
				if($_POST['ret_line'][$i] == ''){
					$ret_line = $this->db->query("select max(ret_line) as ret_line 
												from apt_ret_det where ret_number='".$_POST['ret_number']."'");
					if(count($ret_line->result()) > 0){
						$ret_line = $ret_line->row()->ret_line + 1;
					} else{
						$ret_line = 1;
					}
				} else{
					$ret_line = $_POST['ret_line'][$i];
				}
   				$details=$this->db->query("SELECT * FROM apt_ret_det WHERE ret_number='".$_POST['ret_number']."' AND ret_line=".$ret_line)->result();
   				$apt_ret_det=array();
   				
	   			$apt_ret_det['ret_qty']		= $_POST['qty'][$i];
	   			$apt_ret_det['ret_reduksi']	= $_POST['ret_reduksi'][$i];
   		   
   				if(count($details)>0){
   					$array = array('ret_number =' => $_POST['ret_number'],'ret_line =' => $ret_line);
   					
   					$this->db->where($array);
   					$this->db->update('apt_ret_det',$apt_ret_det);
   				}else{
					$apt_ret_det['ret_number']	= $_POST['ret_number'];
					$apt_ret_det['no_obat_in']	= $_POST['no_obat_in'][$i];
   					$apt_ret_det['ret_line']	= $ret_line;
					$apt_ret_det['kd_milik']	= $result->kd_milik;
					$apt_ret_det['kd_prd']		= $_POST['kd_prd'][$i];
					$apt_ret_det['ret_expire']	= $_POST['tgl_exp'][$i];
					$apt_ret_det['batch']		= $_POST['batch'][$i];
					$apt_ret_det['ppn_item']	= $_POST['ppn_item'][$i];
					$apt_ret_det['rcv_line']	= $_POST['rcv_line'][$i];
   					
   					$this->db->insert('apt_ret_det',$apt_ret_det);
   				}
   		   
   			}
   			
   			if ($this->db->trans_status() === FALSE){
   				$this->db->trans_rollback();
   				$jsonResult['processResult']='ERROR';
   				$jsonResult['processMessage']='Kesalahan Pada Database, Hubungi Admin.';
   			}else{
   				$this->db->trans_commit();
   				$jsonResult['processResult']='SUCCESS';
				$jsonResult['listData']=$this->getListDetail($_POST['ret_number']);
				$jsonResult['totalLisData']=count($this->getListDetail($_POST['ret_number']));
   			}
   		}else{
   			$jsonResult['processResult']='ERROR';
   			$jsonResult['processMessage']='Sebelum Menyimpan Harap Unposting terlebih dahulu.';
   		}
   		echo json_encode($jsonResult);
   	}
	
	public function deletedetail(){
		$delete = $this->db->query("delete apt_ret_det where ret_number='".$_POST['ret_number']."' and kd_prd='".$_POST['kd_prd']."' and ret_line".$_POST['ret_line']."");
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	
	function getListDetail($ret_number){
		$result = $this->db->query("SELECT D.kd_milik,A.ret_number,A.no_obat_in,D.hrg_beli_obt as harga_beli,C.satuan,B.nama_obat,B.kd_prd,A.ret_qty AS qty,A.ret_expire AS tgl_exp,A.batch ,A.rcv_line,
										(SELECT SUM(H.jml_in_obt) FROM apt_obat_in_detail H WHERE H.no_obat_in=A.no_obat_in AND H.kd_prd=A.kd_prd AND H.batch=A.batch)-
										(SELECT SUM(G.ret_qty) FROM apt_ret_det G WHERE G.no_obat_in=A.no_obat_in AND G.kd_prd=A.kd_prd AND G.batch=A.batch) + A.ret_qty AS jml_in_obt,A.ret_line,A.ppn_item,A.ret_reduksi
								FROM apt_ret_det A 
									INNER JOIN apt_retur F ON F.ret_number=A.ret_number 
									INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
									LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan 
									--INNER JOIN apt_produk D ON D.kd_prd=A.kd_prd AND D.kd_milik=F.kd_milik 
									INNER JOIN apt_obat_in_detail D on D.no_obat_in=A.no_obat_in and D.rcv_line=A.rcv_line
									INNER JOIN apt_stok_unit E ON E.kd_prd=A.kd_prd AND E.kd_unit_far=F.kd_unit_far AND E.kd_milik=F.kd_milik
   					WHERE A.ret_number='".$ret_number."'
					GROUP BY D.kd_milik,A.ret_number,A.no_obat_in,D.hrg_beli_obt,C.satuan,B.nama_obat,B.kd_prd,A.ret_qty,A.ret_expire,A.batch,A.kd_prd,A.ret_line
					order by A.ret_line");
		return $result->result();
	}
	
	public function getListObat(){
		$kd_milik=$this->session->userdata['user_id']['aptkdmilik'];
   		$result=$this->db->query("SELECT D.kd_milik,A.no_obat_in,A.tgl_exp,A.batch,A.kd_prd, A.jml_in_obt-
					(CASE WHEN (SELECT COUNT(*) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch)>0 THEN 
					(SELECT SUM(F.ret_qty) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch) ELSE 0 END)AS jml_in_obt,
					B.nama_obat,C.satuan,D.harga_beli, A.ppn_item,F.jml_stok_apt,G.milik,A.rcv_line
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan C ON C.kd_satuan=B.kd_satuan 
					INNER JOIN apt_produk D ON D.kd_prd=A.kd_prd and D.kd_milik=A.kd_milik
					INNER JOIN apt_obat_in E ON E.no_obat_in=A.no_obat_in 
					inner join apt_stok_unit F ON F.kd_prd=A.kd_prd and F.kd_milik=A.kd_milik and F.kd_unit_far=E.kd_unit_far
					inner join apt_milik G ON G.kd_milik=A.kd_milik
				WHERE B.aktif='t'
					AND E.kd_unit_far='".$this->session->userdata['user_id']['aptkdunitfar']."' 
					AND E.kd_milik=".$this->session->userdata['user_id']['aptkdmilik']." 
					AND upper(B.nama_obat) like upper('%".$_POST['nama_obat']."%')
					AND jml_in_obt-(CASE WHEN (SELECT COUNT(*) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch)>0 THEN 
					(SELECT SUM(F.ret_qty) FROM apt_ret_det F WHERE F.kd_prd=A.kd_prd AND F.no_obat_in=A.no_obat_in AND F.batch=A.batch) ELSE 0 END)>0 AND
					E.kd_vendor='".$_POST['pbf']."'
   				ORDER BY B.nama_obat ASC")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
}
?>