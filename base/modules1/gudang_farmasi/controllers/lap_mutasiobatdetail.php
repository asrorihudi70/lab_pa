<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_mutasiobatdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit ORDER BY nm_unit_far ASC")->result();
   		$array['milik']=$this->db->query("SELECT kd_milik AS id,milik AS text FROM apt_milik ORDER BY milik ASC")->result();
   		$array['sub_jenis']=$this->db->query("SELECT kd_sub_jns AS id,sub_jenis AS text FROM apt_sub_jenis ORDER BY sub_jenis ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$u='';
   		$milik='SEMUA';
   		$subJenis='SEMUA';
   		$unit='';
		$html='';
		$param=json_decode($_POST['data']);
		$excel = $param->excel;
   		if($param->kd_milik!=''){
   			$qr.=" AND A.kd_milik=".$param->kd_milik;
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik=".$param->kd_milik)->row()->milik;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns=".$param->sub_jenis;
   			$subJenis=$this->db->query("SELECT sub_jenis FROM apt_sub_jenis WHERE kd_sub_jns=".$param->sub_jenis)->row()->sub_jenis;
   		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr.=" AND A.kd_unit_far in(".$tmpKdUnit.")";
   		// $mpdf=$common->getPDF('L','LAPORAN MUTASI OBAT DETAIL');
   
		/* $queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inmilik)as in4,sum(A.inreturresep+A.inunit+A.inqty+A.inmilik)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outmilik) as out4,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty+A.inmilik)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik)+A.adjustqty)AS jumlah
			FROM apt_mutasi A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$param->year." and months=".$param->month." ".$qr."
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
			ORDER BY C.sub_jenis,B.nama_obat "; */
		$queri="select * from (SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.saldo_awal) AS saldo, 
					sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inmilik)as in4,sum(A.inreturresep+A.inunit+A.inqty+A.inmilik)AS jml_in, 
					sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outmilik) as out4,sum(A.outjualqty)AS out5,
					sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik) AS jml_out, sum(A.adjustqty) AS adjust,
					sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty+A.inmilik)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik)+A.adjustqty)AS jumlah 
				FROM apt_mutasi A INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan 
				where years=".$param->year." and months=".$param->month." ".$qr."
				GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
				ORDER BY C.sub_jenis,B.nama_obat) Z where saldo <> 0 or in1<> 0 or in2<> 0 or in3<> 0 or in4<> 0 or jml_in<>0 or out1<>0  or out2<>0  or out3 <>0 or out4 <>0 or out5 <>0 or adjust <>0 or jumlah <>0";
   		
   		$data=$this->db->query($queri)->result();
   		
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='17'>LAPORAN MUTASI OBAT DETAIL</th>
   					</tr>
   					<tr>
   						<th colspan='17'>PERIODE : ".$common->getMonthByIndex($param->month-1)." ".$param->year." Kepemilikan Obat : ".$milik."</th>
   					</tr>
   					<tr>
   						<th colspan='17'>SUB JENIS OBAT : ".$subJenis." ".$param->year."</th>
   					</tr>
   					<tr>
   						<th colspan='17'>".$t_unit."</th>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   					<tr>
   						<th width='30' align='center' rowspan='2'>No.</th>
				   		<th rowspan='2'>Nama Obat</th>
   						<th width='50' rowspan='2'>Sat</th>
				   		<th width='70' rowspan='2'>Saldo Awal(Stok) </th>
				   		<th colspan='5'>Stok Masuk</th>
		   				<th colspan='6'>Stok Keluar</th>
   						<th width='60' rowspan='2'>Adjust</th>
		   				<th width='70' rowspan='2'>Saldo Akhir(Stok)</th>
   					</tr>
   					<tr>
   						<th width='40'>PBF</th>
				   		<th width='40'>Unit</th>
   						<th width='40'>R Resep</th>
   						<th width='40'>Milik</th>
		   				<th width='50'>Jumlah</th>
		   				<th width='40'>Unit</th>
   						<th width='40'>R PBF</th>
		   				<th width='40'>Hapus</th>
		   				<th width='40'>Rsp</th>
		   				<th width='40'>Milik</th>
		   				<th width='50'>Jumlah</th>
   					</tr>
   		";
	   		if(count($data)==0){
	   				$html.="<tr>
							<th colspan='15' align='center'>Data tidak ada.</td>
						</tr>";
	   		}else{
 				$grand_total1=0;
 				$grand_total2=0;
 				$sub='';
				$no=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->kd_sub_jns!=$sub){
	   					$sub=$data[$i]->kd_sub_jns;
	   					$html.="
	   						<tr>
	   					   		<th align='left' colspan='15'>".$data[$i]->sub_jenis."</th>
	   						</tr>
	   					";
						$no=1;
	   				}else{
						$no++;
					}
	   				$grand_total1+=$data[$i]->saldo;
	   				$grand_total2+=$data[$i]->jumlah;
   					$html.="
   						<tr>
   					   		<td align='center'>".($no)."</td>
   					   		<td>".$data[$i]->nama_obat."</td>
   						   	<td align='center'>".$data[$i]->satuan."</td>
   					   		<td align='right'>".$data[$i]->saldo."</td>
   							<td align='right'>".$data[$i]->in1."</td>
   							<td align='right'>".$data[$i]->in2."</td>
   							<td align='right'>".$data[$i]->in3."</td>
   							<td align='right'>".$data[$i]->in4."</td>
   							<td align='right'>".$data[$i]->jml_in."</td>
   							<td align='right'>".$data[$i]->out1."</td>
   							<td align='right'>".$data[$i]->out2."</td>
   							<td align='right'>".$data[$i]->out3."</td>
   							<td align='right'>".$data[$i]->out5."</td>
   							<td align='right'>".$data[$i]->out4."</td>
   							<td align='right'>".$data[$i]->jml_out."</td>
   							<td align='right'>".$data[$i]->adjust."</td>
   							<td align='right'>".$data[$i]->jumlah."</td>
   						</tr>
   					";
	   			}
	   			$html.="
	   				<tr>
   						<th colspan='3' align='right'>Grand Total</th>
   						<th align='right'>".$grand_total1."</th>
	   					<th colspan='12' align='right'>&nbsp;</th>
	   					<th align='right'>".$grand_total2."</th>
   					</tr>
	   			";
	   		}
   			$html.="</tbody></table>";
			
			if($excel == true){
				$prop=array('foot'=>true);
				$name='Laporan_Mutasi_Obat_Detail.xls';
				header("Content-Type: application/vnd.ms-excel");
				header("Expires: 0");
				header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
				header("Content-disposition: attschment; filename=".$name);
				echo $html;
			}else{
				$this->common->setPdf('L','LAPORAN MUTASI OBAT DETAIL',$html);
				echo $html;
			}
			
   	}
	
	public function doPrintDirect(){
   		ini_set('memory_limit', '1024M');
   		ini_set('max_execution_time', 300);
		ini_set('display_errors', '1');
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$u='';
   		$milik='SEMUA';
   		$subJenis='SEMUA';
   		$unit='';
		$html='';
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
		$param=json_decode($_POST['data']);
   		if($param->kd_milik!=''){
   			$qr.=" AND A.kd_milik=".$param->kd_milik;
   			$milik=$this->db->query("SELECT milik FROM apt_milik WHERE kd_milik=".$param->kd_milik)->row()->milik;
   		}
   		if($param->sub_jenis!=''){
   			$qr.=" AND C.kd_sub_jns=".$param->sub_jenis;
   			$subJenis=$this->db->query("SELECT sub_jenis FROM apt_sub_jenis WHERE kd_sub_jns=".$param->sub_jenis)->row()->sub_jenis;
   		}
		
		$arrayDataUnit = $param->tmp_unit;
		$tmpKdUnit ='';
		$t_unit ='';
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$unit=$this->db->query("SELECT kd_unit_far FROM apt_unit WHERE nm_unit_far='".$arrayDataUnit[$i][0]."'")->row()->kd_unit_far;
			$tmpKdUnit.="'".$unit."',";
   			$t_unit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit = substr($tmpKdUnit, 0, -1);
		$t_unit = substr($t_unit, 0, -1);
		$qr.=" AND A.kd_unit_far in(".$tmpKdUnit.")";
   		// $mpdf=$common->getPDF('L','LAPORAN MUTASI OBAT DETAIL');
   
		
		/* $queri="SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.saldo_awal) AS saldo,
			sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inmilik)as in4,sum(A.inreturresep+A.inunit+A.inqty+A.inmilik)AS jml_in,
			sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outmilik) as out4,sum(A.outjualqty)AS out5,sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik) AS jml_out,
			sum(A.adjustqty) AS adjust,sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty+A.inmilik)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik)+A.adjustqty)AS jumlah
			FROM apt_mutasi A
			INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd
			INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns
			INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan
			where years=".$param->year." and months=".$param->month." ".$qr."
			GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
			ORDER BY C.sub_jenis,B.nama_obat "; */
   		$queri="select * from (SELECT C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan,sum(A.saldo_awal) AS saldo, 
					sum(A.inqty)AS in1,sum(A.inunit)AS in2,sum(A.inreturresep)AS in3,sum(A.inmilik)as in4,sum(A.inreturresep+A.inunit+A.inqty+A.inmilik)AS jml_in, 
					sum(A.outqty)AS out1,sum(A.outreturpbf)AS out2,sum(A.outhapus)AS out3,sum(A.outmilik) as out4,sum(A.outjualqty)AS out5,
					sum(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik) AS jml_out, sum(A.adjustqty) AS adjust,
					sum(A.saldo_awal+(A.inreturresep+A.inunit+A.inqty+A.inmilik)-(A.outqty+A.outreturpbf+A.outhapus+A.outjualqty+A.outmilik)+A.adjustqty)AS jumlah 
				FROM apt_mutasi A INNER JOIN apt_obat B ON B.kd_prd=A.kd_prd INNER JOIN apt_sub_jenis C ON C.kd_sub_jns=B.kd_sub_jns INNER JOIN apt_satuan D ON D.kd_satuan=B.kd_satuan 
				where years=".$param->year." and months=".$param->month." ".$qr."
				GROUP BY C.kd_sub_jns,C.sub_jenis,B.nama_obat,D.satuan 
				ORDER BY C.sub_jenis,B.nama_obat) Z where saldo <> 0 or in1<> 0 or in2<> 0 or in3<> 0 or in4<> 0 or jml_in<>0 or out1<>0  or out2<>0  or out3 <>0 or out4 <>0 or out5 <>0 or adjust <>0 or jumlah <>0";
   		
   		$data=$this->db->query($queri)->result();
   		
		# Create Data
		$tp = new TableText(145,15,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 15)
			->setColumnLength(2, 6)
			->setColumnLength(3, 6)
			->setColumnLength(4, 8)
			->setColumnLength(5, 8)
			->setColumnLength(6, 8)
			->setColumnLength(7, 8)
			->setColumnLength(8, 8)
			->setColumnLength(9, 8)
			->setColumnLength(10, 8)
			->setColumnLength(11, 8)
			->setColumnLength(12, 8)
			->setColumnLength(13, 8)
			->setColumnLength(14, 8)
			->setColumnLength(15, 8)
			->setColumnLength(16, 6)
			->setUseBodySpace(true);
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 17,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 17,"left")
			->commit("header")
			->addColumn($telp, 17,"left")
			->commit("header")
			->addColumn($fax, 17,"left")
			->commit("header")
			->addColumn("LAPORAN MUTASI OBAT DETAIL", 17,"center")
			->commit("header")
			->addColumn("PERIODE : ".$common->getMonthByIndex($param->month-1)." ".$param->year." Kepemilikan Obat : ".$milik, 17,"center")
			->commit("header")
			->addColumn("SUB JENIS OBAT : ".$subJenis." ".$param->year, 17,"center")
			->commit("header")
			->addColumn($t_unit, 17,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		$tp	->addColumn("No.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Sat", 1,"left")
			->addColumn("Saldo Awal  (Stok)", 1,"right")
			->addColumn("Stok Masuk", 5,"center")
			->addColumn("Stok Keluar", 6,"center")
			->addColumn("Adjust", 1,"right")
			->addColumn("Saldo Akhir (Stok)", 1,"right")
			->commit("header");	
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("PBF", 1,"right")
			->addColumn("Unit", 1,"right")
			->addColumn("R Resep", 1,"right")
			->addColumn("Milik", 1,"right")
			->addColumn("Jumlah", 1,"right")
			->addColumn("Unit", 1,"right")
			->addColumn("R PBF", 1,"right")
			->addColumn("Hapus", 1,"right")
			->addColumn("Rsp", 1,"right")
			->addColumn("Milik", 1,"right")
			->addColumn("Jumlah", 1,"right")
			->addColumn("", 1,"right")
			->addColumn("", 1,"right")
			->commit("header");	
   		
	   		if(count($data)==0){
	   			$tp	->addColumn("Data tidak ada", 17,"center")
					->commit("header");
	   		}else{
 				$grand_total1=0;
 				$grand_total2=0;
 				$sub='';
				$no=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($data[$i]->kd_sub_jns!=$sub){
	   					$sub=$data[$i]->kd_sub_jns;
						$tp	->addColumn($data[$i]->sub_jenis, 17,"left")
							->commit("header");
						$no=1;
	   				}else{
						$no++;
					}
	   				$grand_total1+=$data[$i]->saldo;
	   				$grand_total2+=$data[$i]->jumlah;
					$tp	->addColumn($no.".", 1,"left")
						->addColumn($data[$i]->nama_obat, 1,"left")
						->addColumn($data[$i]->satuan, 1,"left")
						->addColumn($data[$i]->saldo, 1,"right")
						->addColumn($data[$i]->in1, 1,"right")
						->addColumn($data[$i]->in2, 1,"right")
						->addColumn($data[$i]->in3, 1,"right")
						->addColumn($data[$i]->in4, 1,"right")
						->addColumn($data[$i]->jml_in, 1,"right")
						->addColumn($data[$i]->out1, 1,"right")
						->addColumn($data[$i]->out2, 1,"right")
						->addColumn($data[$i]->out3, 1,"right")
						->addColumn($data[$i]->out5, 1,"right")
						->addColumn($data[$i]->out4, 1,"right")
						->addColumn($data[$i]->jml_out, 1,"right")
						->addColumn($data[$i]->adjust, 1,"right")
						->addColumn($data[$i]->jumlah, 1,"right")
						->commit("header");
	   			}
				$tp	->addColumn("Grand Total", 3,"right")
					->addColumn($grand_total1, 1,"right")
					->addColumn("", 12,"right")
					->addColumn($grand_total2, 1,"right")
					->commit("header");
	   		}
   			# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 8,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data_mutasi_obat_detail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
		
   	}
}
?>