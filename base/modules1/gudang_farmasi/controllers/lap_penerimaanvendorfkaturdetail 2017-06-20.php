<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanvendorfkaturdetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
   		$qr='';
		$html='';
   		$statuspembayaran='SEMUA';
   		$jatuhtempo='SEMUA';
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		// $mpdf=$common->getPDF('L','LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)');
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   		}
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
   		/* $queri="SELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,E.satuan,A.jml_in_obt,
			(A.hrg_beli_obt) - (((A.hrg_beli_obt/A.jml_in_obt)*10)/100) as hrg_beli_obt,(A.jml_in_obt*A.hrg_beli_obt)AS sub_total,
		(A.apt_disc_rupiah+(((A.jml_in_obt*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,((A.hrg_beli_obt)*0.1) AS ppn,
   		((((A.jml_in_obt*A.hrg_beli_obt)/100)*10)+(A.jml_in_obt*A.hrg_beli_obt)-(A.apt_disc_rupiah+(((A.jml_in_obt*A.hrg_beli_obt)/100))*A.apt_discount))AS total
		FROM apt_obat_in_detail A INNER JOIN
		apt_obat_in B ON A.no_obat_in=B.no_obat_in INNER JOIN
		vendor C ON C.kd_vendor=B.kd_vendor INNER JOIN
		apt_obat D ON D.kd_prd=A.kd_prd LEFT JOIN
		apt_satuan E ON E.kd_satuan=D.kd_satuan
		WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
		ORDER BY C.kd_vendor,A.no_obat_in ASC"; */
		
		/* $queri="SELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,
					E.satuan,(A.jml_in_obt / A.frac)as jml_in_obt,A.hrg_satuan,A.hrg_beli_obt,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)AS sub_total,
					(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
					((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)*10)/100)AS ppn,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)+((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)*10)/100)-
						(A.apt_disc_rupiah+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)AS total
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
					INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
					INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
				WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY C.vendor,A.no_obat_in,D.nama_obat ASC"; */
		
		$queri="SELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,
					E.satuan,(A.jml_in_obt / A.frac)as jml_in_obt,A.hrg_satuan,A.hrg_beli_obt,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)AS sub_total,
					(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
					((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100 AS ppn,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)+(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100)+B.materai AS total,
					B.materai
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
					INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
					INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
				WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY C.vendor,A.no_obat_in,D.nama_obat ASC";
   		
   		$data=$this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>STATUS PEMBAYARAN : ".$statuspembayaran."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>JATUH TEMPO : ".$jatuhtempo."</td>
   					</tr>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>No. Terima</th>
				   		<th width=''>No. Faktur</th>
				   		<th width=''>Tgl Terima</th>
   						<th width=''>Tgl Tempo</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>
		   				<th width='60'>Qty</th>
		   				<th width='60'>Harga</th>
		   				<th width='70'>Sub Total</th>
		   				<th width='70'>Disc</th>
   						<th width='70'>PPN</th>
   						<th width='70'>Total</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
	   			$html.="<tr>
   						<th colspan='13' align='center'>Data tidak ada</th>
				   		</tr>";
	   		}else{
	   			$no=0;
	   			$pbf='';
	   			$sub_disc=0;
	   			$sub_ppn=0;
	   			$sub_sub_total=0;
	   			$sub_total=0;
	   			$total=0;
	   			$big_total=0;
	   			$disc=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_disc=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$no_obat_in='';
	   			$vendor='';
				$materai = 0;
	   			for($i=0; $i<count($data); $i++){
	   				$sama=false;
	   				if($no_obat_in != $data[$i]->no_obat_in){
	   					$no_obat_in=$data[$i]->no_obat_in;
	   					$sama=true;
	   					$no++;
	   					if($i!=0){
	   						// $sub_total+=$data[$i]->materai;
	   						$html.="<tr>
   								<th colspan='9' align='right'>Materai</th>
	   							<td colspan='3'>&nbsp;</td>
	   							<th align='right'>".number_format(0,2,',','.')."</th>
	   						</tr>";
	   						$html.="<tr>
   								<th colspan='9' align='right'>Sub total</th>
	   							<th align='right'>".number_format($sub_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($sub_disc,2,',','.')."</th>
   								<th align='right'>".number_format($sub_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($sub_total,2,',','.')."</th>
	   						</tr>";
	   						$sub_disc=0;
	   						$sub_sub_total=0;
	   						$sub_ppn=0;
	   						$sub_total=0;
	   					}
	   				}
	   				if($pbf != $data[$i]->kd_vendor){
	   					if($i!=0){
	   						$html.="<tr>
   								<th colspan='9' align='right'>Total ".$vendor."</th>
	   							<th align='right'>".number_format($total,2,',','.')."</th>
   								<th align='right'>".number_format($disc,2,',','.')."</th>
   								<th align='right'>".number_format($ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($big_total,2,',','.')."</th>
	   						</tr>";
	   						$total=0;
	   						$big_total=0;
	   						$disc=0;
	   						$ppn=0;
	   						
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$html.="<tr>
   							<th colspan='13' align='left'>".$data[$i]->vendor."</th></tr>";
	   					
	   				}
	   				$html.="<tr>";
	   				if($sama==true){
	   					$html.="
	   						<td align='center'>".$no."</td>
	   						<td align='center'>".$data[$i]->no_obat_in."</td>
		   					<td align='center'>".$data[$i]->remark."</td>
	   						<td>".date('d/m/Y', strtotime($data[$i]->tgl_obat_in))."</td>
   							<td>".date('d/m/Y', strtotime($data[$i]->due_date))."</td>
	   						";
	   				}else{
	   					$html.="
	   						<td colspan='5'>&nbsp;</td>
	   						";
	   				}
	   				$sub_disc+=$data[$i]->disc;
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				// $sub_total+=$data[$i]->total;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$disc+=$data[$i]->disc;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_disc+=$data[$i]->disc;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
					$materai = $data[$i]->materai;
	   				$html.="
   						<td>".$data[$i]->nama_obat."</td>
   						<td align='center'>".$data[$i]->satuan."</td>
   						<td align='right'>".number_format($data[$i]->jml_in_obt,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->hrg_beli_obt,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->disc,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,2,',','.')."</td>
   					</tr>";
	   			}
	   			$html.="<tr>
   								<th colspan='9' align='right'>Materai</th>
	   							<td colspan='3'>&nbsp;</td>
	   							<th align='right'>".number_format($materai,2,',','.')."</th>
	   						</tr>";
	   			$html.="<tr>
   								<th colspan='9' align='right'>Sub total</th>
	   							<th align='right'>".number_format($sub_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($sub_disc,2,',','.')."</th>
   								<th align='right'>".number_format($sub_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($sub_total,2,',','.')."</th>
	   						</tr>";
	   			$html.="<tr>
   								<th colspan='9' align='right'>Total ".$vendor."</th>
	   							<th align='right'>".number_format($total,2,',','.')."</th>
   								<th align='right'>".number_format($disc,2,',','.')."</th>
   								<th align='right'>".number_format($ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($big_total,2,',','.')."</th>
	   						</tr>";
	   			$html.="<tr>
   								<th colspan='13' align='right'></td>
	   						</tr>
							<tr>
   								<th colspan='9' align='right'>Grand total</td>
	   							<th align='right'>".number_format($grand_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($grand_disc,2,',','.')."</th>
   								<th align='right'>".number_format($grand_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($grand_total,2,',','.')."</th>
	   						</tr>";
	   		}
   		$html.="</table>";
		#-----------------------TANDA TANGAN-------------------------------------------------------------
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		$html.='
			<br><br>
		
			<table border="0">
			<tbody>
				<tr class="headerrow"> 
					<td width="100" align="center">'.$ttd_left.'</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">' . $rs->city . ', ' . tanggalstring(date('Y-m-d')) . '</td>
				</tr>
				<tr class="headerrow">
					<td width="100" align="center">Petugas Gudang Farmasi</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">Penyimpan Barang Medis</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="headerrow"> 
					<td width="100" align="center">'.$user.'</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">(................................)</td>
				</tr>

			<p>&nbsp;</p>
			</tbody></table>
		';
		$common=$this->common;
		$this->common->setPdf('L','LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)',$html);
		echo $html;
   	}
	
	public function doPrintDirect_(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);

		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$statuspembayaran='SEMUA';
   		$jatuhtempo='SEMUA';
		$qr='';
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   		}
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 25)
			->setColumnLength(3, 5)
			->setColumnLength(4, 10)
			->setColumnLength(5, 5)
			->setColumnLength(6, 15)
			->setColumnLength(7, 15)
			->setColumnLength(8, 15)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)", 9,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 9,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 15)
			->setColumnLength(1, 2)
			->setColumnLength(2, 20)
			->setColumnLength(3, 1)
			->setColumnLength(4, 5)
			->setColumnLength(5, 1)
			->setColumnLength(6, 15)
			->setColumnLength(7, 2)
			->setColumnLength(8, 25)
			->setUseBodySpace(true);
		#QUERY HEAD
		$reshead=$this->db->query("SELECT distinct(A.no_obat_in),C.kd_vendor,C.vendor,B.due_date,B.remark,B.tgl_obat_in
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
					INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
					INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
				WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY A.no_obat_in ASC")->result();
			
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 20)
			->setColumnLength(6, 7)
			->setColumnLength(7, 5)
			->setColumnLength(8, 7)
			->setColumnLength(9, 10)
			->setColumnLength(10, 7)
			->setColumnLength(11, 8)
			->setColumnLength(12, 10)
			->setUseBodySpace(true);
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No.Terima", 1,"left")
			->addColumn("No.Faktur", 1,"left")
			->addColumn("Tgl.Terima", 1,"left")
			->addColumn("Tgl.Tempo", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Sat", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Harga", 1,"right")
			->addColumn("Sub Total", 1,"right")
			->addColumn("Disc", 1,"right")
			->addColumn("PPN", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 13,"center")
				->commit("header");
		} else{
			$no = 0;
			$grandppn =0;
			$granddisc =0;
			$grandsubtotal =0;
			$grandtotal =0;
			foreach ($reshead as $key) {
				$materai =0;
				$ppn =0;
				$disc =0;
				$subtotal =0;
				$total =0;
				$harga_beli =0;
				$no++;
				$tp	->addColumn("", 1)
					->addColumn($key->vendor, 12,"left")
					->commit("header");
				$tp	->addColumn(($no).".", 1)
					->addColumn($key->no_obat_in, 1,"left")
					->addColumn($key->remark, 1,"left")
					->addColumn(date('d-M-Y', strtotime($key->tgl_obat_in)), 1,"left")
					->addColumn(date('d-M-Y', strtotime($key->due_date)), 1,"left")
					->addColumn("", 8,"left")
					->commit("header");
				$res=$this->db->query("sSELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,
										E.satuan,(A.jml_in_obt / A.frac)as jml_in_obt,A.hrg_satuan,A.hrg_beli_obt,
										((A.jml_in_obt / A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)AS sub_total,
										(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
										((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100 AS ppn,
										((A.jml_in_obt / A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)+
											(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100)+B.materai AS total,
										B.materai
									FROM apt_obat_in_detail A 
										INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
										INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
										INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
										LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
									WHERE B.no_obat_in='".$key->no_obat_in."' 
										and B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' 
										AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
									ORDER BY D.nama_obat ASC")->result();
				foreach ($res as $line) {					
					$tp	->addColumn("", 1)
						->addColumn("", 4)
						->addColumn($line->nama_obat, 1,"left")
						->addColumn($line->satuan, 1,"left")
						->addColumn($line->jml_in_obt, 1,"right")
						->addColumn(number_format($line->hrg_beli_obt,2, "," , "."), 1,"right")
						->addColumn(number_format($line->sub_total,2, "," , "."), 1,"right")
						->addColumn(number_format($line->disc,2, "," , "."), 1,"right")
						->addColumn(number_format($line->ppn,2, "," , "."), 1,"right")
						->addColumn(number_format($line->total,2, "," , "."), 1,"right")
						->commit("header");	
						
					$harga_beli += $line->hrg_beli_obt;
					$subtotal += $line->sub_total;
					$disc += $line->disc;
					$ppn += $line->ppn;
					$total += $line->total;
					$materai = $line->materai;
					
				}
				
				$tp	->addColumn("MATERAI", 9,"right")
					->addColumn("", 3,"right")
					->addColumn(number_format($materai,2, "," , "."), 1,"right")
					->commit("footer")
					->addColumn("SUB TOTAL", 9,"right")
					->addColumn(number_format($subtotal,2, "," , "."), 1,"right")
					->addColumn(number_format($disc,2, "," , "."), 1,"right")
					->addColumn(number_format($ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($total,2, "," , "."), 1,"right")
					->commit("footer")
					->addColumn("Total ".$key->vendor, 9,"right")
					->addColumn(number_format($subtotal,2, "," , "."), 1,"right")
					->addColumn(number_format($disc,2, "," , "."), 1,"right")
					->addColumn(number_format($ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($total,2, "," , "."), 1,"right")
					->commit("footer");
				
				$grandsubtotal += $subtotal;
				$granddisc += $disc;
				$grandppn += $ppn;
				$grandtotal += $total;
			}	
			$tp	->addColumn("Grand Total", 9,"right")
				->addColumn(number_format($grandsubtotal,2, "," , "."), 1,"right")
				->addColumn(number_format($granddisc,2, "," , "."), 1,"right")
				->addColumn(number_format($grandppn,2, "," , "."), 1,"right")
				->addColumn(number_format($grandtotal,2, "," , "."), 1,"right")
				->commit("footer");
		}
		
		
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		
			
		$tp	->addColumn($ttd_left, 3,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')),6,"right")
			->commit("footer")
			->addColumn("Petugas Gudang Farmasi", 3,"left")
			->addColumn($ttd_right,6,"right")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($user, 3,"left")
			->addColumn("(....................)", 6,"right")
			->commit("footer");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/datapenerimaanvendordetail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		// shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,13,'',0,false);

		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$statuspembayaran='SEMUA';
   		$jatuhtempo='SEMUA';
		$qr='';
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   		}
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
		
		
		
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 11)
			->setColumnLength(4, 11)
			->setColumnLength(5, 20)
			->setColumnLength(6, 7)
			->setColumnLength(7, 5)
			->setColumnLength(8, 7)
			->setColumnLength(9, 10)
			->setColumnLength(10, 7)
			->setColumnLength(11, 8)
			->setColumnLength(12, 10)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 13,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 13,"left")
			->commit("header")
			->addColumn($telp, 13,"left")
			->commit("header")
			->addColumn($fax, 13,"left")
			->commit("header")
			->addColumn("LAPORAN BERDASARKAN VENDOR PER FAKTUR (DETAIL)", 13,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 13,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		
		#QUERY HEAD
		/* $reshead=$this->db->query("SELECT distinct(A.no_obat_in),C.kd_vendor,C.vendor,B.due_date,B.remark,B.tgl_obat_in
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
					INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
					INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
				WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY A.no_obat_in ASC")->result(); */
		$queri="SELECT C.kd_vendor,C.vendor,B.no_obat_in,B.due_date,B.remark,B.tgl_obat_in,D.nama_obat,
					E.satuan,(A.jml_in_obt / A.frac)as jml_in_obt,A.hrg_satuan,A.hrg_beli_obt,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)AS sub_total,
					(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount) AS disc,
					((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100 AS ppn,
					((A.jml_in_obt / A.frac)*A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount)+(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100)+B.materai AS total,
					B.materai
				FROM apt_obat_in_detail A 
					INNER JOIN apt_obat_in B ON A.no_obat_in=B.no_obat_in 
					INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor 
					INNER JOIN apt_obat D ON D.kd_prd=A.kd_prd 
					LEFT JOIN apt_satuan E ON E.kd_satuan=D.kd_satuan
				WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
				ORDER BY C.vendor,A.no_obat_in,D.nama_obat  ASC  ";
   		
   		$data=$this->db->query($queri)->result();	
	
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No.Terima", 1,"left")
			->addColumn("No.Faktur", 1,"left")
			->addColumn("Tgl.Terima", 1,"left")
			->addColumn("Tgl.Tempo", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Sat", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Harga", 1,"right")
			->addColumn("Sub Total", 1,"right")
			->addColumn("Disc", 1,"right")
			->addColumn("PPN", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
		if(count($data) < 0){
			$tp	->addColumn("Data tidak ada", 13,"center")
				->commit("header");
		} else{
			$no=0;
	   			$pbf='';
	   			$sub_disc=0;
	   			$sub_ppn=0;
	   			$sub_sub_total=0;
	   			$sub_total=0;
	   			$total=0;
	   			$big_total=0;
	   			$disc=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_disc=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$no_obat_in='';
	   			$vendor='';
				$materai = 0;
	   			for($i=0; $i<count($data); $i++){
	   				$sama=false;
	   				if($no_obat_in != $data[$i]->no_obat_in){
	   					$no_obat_in=$data[$i]->no_obat_in;
	   					$sama=true;
	   					$no++;
	   					if($i!=0){
	   						// $sub_total+=$data[$i]->materai;
							
							$tp	->addColumn("Materai", 9,"right")
								->addColumn("", 3,"right")
								->addColumn(number_format(0,2, "," , "."), 1,"right")
								->commit("header");	
	   						
							$tp	->addColumn("Sub total", 9,"right")
								->addColumn(number_format($sub_sub_total,2, "," , "."), 1,"right")
								->addColumn(number_format($sub_disc,2, "," , "."), 1,"right")
								->addColumn(number_format($sub_ppn,2, "," , "."), 1,"right")
								->addColumn(number_format($sub_total,2, "," , "."), 1,"right")
								->commit("header");	
	   						$sub_disc=0;
	   						$sub_sub_total=0;
	   						$sub_ppn=0;
	   						$sub_total=0;
	   					}
	   				}
	   				if($pbf != $data[$i]->kd_vendor){
	   					if($i!=0){
							$tp	->addColumn("Total", 9,"right")
								->addColumn(number_format($total,2, "," , "."), 1,"right")
								->addColumn(number_format($disc,2, "," , "."), 1,"right")
								->addColumn(number_format($ppn,2, "," , "."), 1,"right")
								->addColumn(number_format($big_total,2, "," , "."), 1,"right")
								->commit("header");	
	   						$total=0;
	   						$big_total=0;
	   						$disc=0;
	   						$ppn=0;
	   						
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
						$tp	->addColumn($data[$i]->vendor, 13,"left")
							->commit("header");	
	   					
	   				}
	   				
						
	   				if($sama==true){
						$tp	->addColumn($no, 1,"left")
							->addColumn($data[$i]->no_obat_in, 1,"left")
							->addColumn($data[$i]->remark, 1,"left")
							->addColumn(date('d/m/Y', strtotime($data[$i]->tgl_obat_in)), 1,"left")
							->addColumn(date('d/m/Y', strtotime($data[$i]->due_date)), 1,"left");	
	   				}else{
	   					$tp	->addColumn("", 5,"right");	
	   				}
	   				$sub_disc+=$data[$i]->disc;
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				// $sub_total+=$data[$i]->total;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$disc+=$data[$i]->disc;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_disc+=$data[$i]->disc;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
					$materai = $data[$i]->materai;
					
					$tp	->addColumn($data[$i]->nama_obat, 1,"left")
						->addColumn($data[$i]->satuan, 1,"left")
						->addColumn(number_format($data[$i]->jml_in_obt,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->hrg_beli_obt,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->sub_total,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->disc,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->ppn,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->total,2, "," , "."), 1,"right")
						->commit("header");	
	   			}
				$tp	->addColumn("Materai", 9,"right")
					->addColumn("", 3,"right")
					->addColumn(number_format($materai,2, "," , "."), 1,"right")
					->commit("header");	
	   			$tp	->addColumn("Sub total", 9,"right")
					->addColumn(number_format($sub_sub_total,2, "," , "."), 1,"right")
					->addColumn(number_format($sub_disc,2, "," , "."), 1,"right")
					->addColumn(number_format($sub_ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($sub_total,2, "," , "."), 1,"right")
					->commit("header");	
				$tp	->addColumn("Total ".$vendor, 9,"right")
					->addColumn(number_format($total,2, "," , "."), 1,"right")
					->addColumn(number_format($disc,2, "," , "."), 1,"right")
					->addColumn(number_format($ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($big_total,2, "," , "."), 1,"right")
					->commit("header");	
				$tp	->addColumn("", 13,"right")
					->commit("header");	
				$tp	->addColumn("Grand total ", 9,"right")
					->addColumn(number_format($grand_sub_total,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_disc,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_total,2, "," , "."), 1,"right")
					->commit("header");	
		}
		
		
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		
			
		$tp	->addColumn($ttd_left, 3,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')),9,"right")
			->commit("footer")
			->addColumn("Petugas Gudang Farmasi", 3,"left")
			->addColumn($ttd_right,9,"right")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($user, 3,"left")
			->addColumn("(....................)", 9,"right")
			->commit("footer");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 9,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/datapenerimaanvendordetail.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
}
?>