<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaanvendorfkatursummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
		$html='';
   		$statuspembayaran='SEMUA';
   		$jatuhtempo='SEMUA';
		$param=json_decode($_POST['data']);
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
   		
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   		}
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   			}
   		}
   		/* $queri="select c.vendor,A.no_obat_in, b.kd_vendor,B.tgl_obat_in,B.remark,B.due_date,B.materai,
				(sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))AS sub_total,
				(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount)) AS disc,
				(sum(((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*10))AS ppn,
				((sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))+(sum(((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*10))+B.materai)AS total
			from apt_obat_in_detail A
				inner join  apt_obat_in B ON A.no_obat_in=B.no_obat_in  
				INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor
			WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			group by 
			 A.no_obat_in,b.kd_vendor,c.vendor,tgl_obat_in,remark,due_date,materai
			 order by  C.vendor,A.no_obat_in ASC"; */
   		
		$queri="select c.vendor,A.no_obat_in, b.kd_vendor,B.tgl_obat_in,B.remark,B.due_date,B.materai,
				(sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))AS sub_total,
				(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount)) AS disc,
				(sum(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100)) AS ppn,
				((sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))+
					(sum(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100))
					+B.materai)AS total,
				B.materai
			from apt_obat_in_detail A
				inner join  apt_obat_in B ON A.no_obat_in=B.no_obat_in  
				INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor
			WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			group by 
			 A.no_obat_in,b.kd_vendor,c.vendor,tgl_obat_in,remark,due_date,materai
			 order by  C.vendor,A.no_obat_in ASC";
		
   		$data=$this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN BERDASARKAN VENDOR PER FAKTUR (SUMMARY)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>STATUS PEMBAYARAN : ".$statuspembayaran."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>JATUH TEMPO : ".$jatuhtempo."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>Nama Vendor</th>
				   		<th width=''>No. Terima</th>
   						<th width=''>No. Faktur</th>
				   		<th width=''>Tgl Terima</th>
   						<th width=''>Tgl Tempo</th>
		   				<th width='70'>Sub Total</th>
		   				<th width='70'>Disc</th>
   						<th width='70'>PPN</th>
   						<th width='70'>Materai</th>
   						<th width='70'>Total</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
	   			$html.="<tr>
   						<th colspan='11' align='center'>Data tidak ada</th>
				   		</tr>";
	   		}else{
	   			$no=0;
	   			$pbf='';
	   			$total=0;
	   			$big_total=0;
	   			$disc=0;
	   			$materai=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_disc=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$grand_materai=0;
	   			$no_obat_in='';
	   			$vendor='';
				$sub_disc =0;
				$sub_sub_total =0;
				$sub_ppn =0;
				$sub_total =0;
	   			for($i=0; $i<count($data); $i++){
	   				if($pbf != $data[$i]->kd_vendor){
	   					$no++;
	   					if($i!=0){
	   						$html.="<tr>
   								<td colspan='6' align='right'>Total</td>
	   							<td align='right'>".number_format($total,2,',','.')."</td>
   								<td align='right'>".number_format($disc,2,',','.')."</td>
   								<td align='right'>".number_format($ppn,2,',','.')."</td>
	   							<td align='right'>".number_format($materai,2,',','.')."</td>
	   							<td align='right'>".number_format($big_total,2,',','.')."</td>
	   						</tr>";
	   						$total=0;
	   						$big_total=0;
	   						$disc=0;
	   						$ppn=0;
	   						$materai=0;
							$sub_disc =0;
							$sub_sub_total =0;
							$sub_ppn =0;
							$sub_total =0;
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$html.="<tr>
	   						<th>".$no."</th>
   							<th colspan='10' align='left'>".$data[$i]->vendor."</th>
	   							</tr>";
	   					
	   				}
	   				$html.="<tr>";
	   				$sub_disc+=$data[$i]->disc;
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$disc+=$data[$i]->disc;
	   				$materai+=$data[$i]->materai;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_disc+=$data[$i]->disc;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
	   				$grand_materai=$data[$i]->materai;
	   				$html.="
	   						<td align='center' colspan='2'>&nbsp;</td>
		   					<td align='center'>".$data[$i]->no_obat_in."</td>
	   						<td align='center'>".$data[$i]->remark."</td>
	   						<td>".date('d/m/Y', strtotime($data[$i]->tgl_obat_in))."</td>
   							<td>".date('d/m/Y', strtotime($data[$i]->due_date))."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->disc,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,2,',','.')."</td>
	   					<td align='right'>".number_format($data[$i]->materai,2,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,2,',','.')."</td>
   					</tr>";
	   			}
	   			$html.="<tr>
   								<td colspan='6' align='right'>Total</td>
	   							<td align='right'>".number_format($total,2,',','.')."</td>
   								<td align='right'>".number_format($disc,2,',','.')."</td>
   								<td align='right'>".number_format($ppn,2,',','.')."</td>
	   							<td align='right'>".number_format($materai,2,',','.')."</td>
	   							<td align='right'>".number_format($big_total,2,',','.')."</td>
	   						</tr>";
	   			$html.="<tr>
   								<th colspan='6' align='right'>Grand total</td>
	   							<th align='right'>".number_format($grand_sub_total,2,',','.')."</th>
   								<th align='right'>".number_format($grand_disc,2,',','.')."</th>
   								<th align='right'>".number_format($grand_ppn,2,',','.')."</th>
	   							<th align='right'>".number_format($grand_materai,2,',','.')."</th>
	   							<th align='right'>".number_format($grand_total,2,',','.')."</th>
	   						</tr>";
	   		}
		$html.="</tbody></table>";
		#-----------------------TANDA TANGAN-------------------------------------------------------------
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		$html.='
			<br><br>
		
			<table border="0">
			<tbody>
				<tr class="headerrow"> 
					<td width="100" align="center">'.$ttd_left.'</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">' . $rs->city . ', ' . tanggalstring(date('Y-m-d')) . '</td>
				</tr>
				<tr class="headerrow">
					<td width="100" align="center">Petugas Gudang Farmasi</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">Penyimpan Barang Medis</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="headerrow"> 
					<td width="100" align="center">'.$user.'</td>
					<td width="300">&nbsp;</td>
					<td width="300">&nbsp;</td>
					<td width="150" align="center">(................................)</td>
				</tr>

			<p>&nbsp;</p>
			</tbody></table>
		';
   		$common=$this->common;
		$this->common->setPdf('L','LAPORAN BERDASARKAN VENDOR PER FAKTUR (SUMMARY)',$html);
		echo $html;
   	}
	
	public function doPrintDirect_(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$qr='';
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,11,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		if($param->tempo !=''){
   			if($param->tempo==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
   		
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   		}
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   			}
   		}
		
		
		
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 7)
			->setColumnLength(2, 15)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 13)
			->setColumnLength(6, 10)
			->setColumnLength(7, 10)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 13)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 10,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 10,"left")
			->commit("header")
			->addColumn($telp, 10,"left")
			->commit("header")
			->addColumn($fax, 10,"left")
			->commit("header")
			->addColumn("LAPORAN BERDASARKAN VENDOR PER FAKTUR (SUMMARY)", 10,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 10,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		
		#QUERY HEAD
		$reshead=$this->db->query("select c.vendor,A.no_obat_in, b.kd_vendor,B.tgl_obat_in,B.remark,B.due_date,B.materai
									from apt_obat_in_detail A
										inner join  apt_obat_in B ON A.no_obat_in=B.no_obat_in  
										INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor
									WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  
										".$qr."  AND B.kd_unit_far='".$kdUnit."'
									group by A.no_obat_in,b.kd_vendor,c.vendor,tgl_obat_in,remark,due_date,materai
									order by  C.vendor,A.no_obat_in ASC")->result();
			
		
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Vendor", 1,"left")
			->addColumn("No.Terima", 1,"left")
			->addColumn("No.Faktur", 1,"left")
			->addColumn("Tgl.Terima", 1,"left")
			->addColumn("Tgl.Tempo", 1,"left")
			->addColumn("Sub Total", 1,"right")
			->addColumn("Disc", 1,"right")
			->addColumn("Materai", 1,"right")
			->addColumn("PPN", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
		
		$no = 0;
		$grandppn =0;
		$granddisc =0;
		$grandsubtotal =0;
		$grandmaterai =0;
		$grandtotal =0;
		if(count($reshead) < 0){
			$tp	->addColumn("Data tidak ada", 11,"center")
				->commit("header");
		} else{
			$vendor='';
			foreach ($reshead as $key) {
				$materai =0;
				$ppn =0;
				$disc =0;
				$subtotal =0;
				$total =0;
				$harga_beli =0;
				
				if($vendor != $key->vendor ){
					$no++;
					$tp	->addColumn(($no).".", 1)
						->addColumn($key->vendor, 10,"left")
						->commit("header");
				}
				$vendor=  $key->vendor;
				$tp	->addColumn("", 1)
					->addColumn("", 1,"left")
					->addColumn($key->no_obat_in, 1,"left")
					->addColumn($key->remark, 1,"left")
					->addColumn(date('d-M-Y', strtotime($key->tgl_obat_in)), 1,"left")
					->addColumn(date('d-M-Y', strtotime($key->due_date)), 1,"left")
					->addColumn("", 5,"left")
					->commit("header");
				$res=$this->db->query("select c.vendor,A.no_obat_in, b.kd_vendor,B.tgl_obat_in,B.remark,B.due_date,B.materai,
										(sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))AS sub_total,
										(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount)) AS disc,
										(sum(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100)) AS ppn,
										((sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))+
											(sum(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100))+
											B.materai)AS total,
										B.materai
									from apt_obat_in_detail A
										inner join  apt_obat_in B ON A.no_obat_in=B.no_obat_in  
										INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor
									WHERE B.no_obat_in='".$key->no_obat_in."' and B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
									group by A.no_obat_in,b.kd_vendor,c.vendor,tgl_obat_in,remark,due_date,materai
									order by  C.vendor,A.no_obat_in ASC")->result();
				foreach ($res as $line) {					
					$tp	->addColumn("", 1)
						->addColumn("", 5)
						->addColumn(number_format($line->sub_total,2, "," , "."), 1,"right")
						->addColumn(number_format($line->disc,2, "," , "."), 1,"right")
						->addColumn(number_format($line->ppn,2, "," , "."), 1,"right")
						->addColumn(number_format($line->materai,2, "," , "."), 1,"right")
						->addColumn(number_format($line->total,2, "," , "."), 1,"right")
						->commit("header");	
						
					$subtotal += $line->sub_total;
					$disc += $line->disc;
					$ppn += $line->ppn;
					$materai = $line->materai;
					$total += $line->total;
					
				}
				
				$tp	->addColumn("", 1,"right")
					->addColumn("Total", 5,"right")
					->addColumn(number_format($subtotal,2, "," , "."), 1,"right")
					->addColumn(number_format($disc,2, "," , "."), 1,"right")
					->addColumn(number_format($ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($materai,2, "," , "."), 1,"right")
					->addColumn(number_format($total,2, "," , "."), 1,"right")
					->commit("footer");
				
				$grandsubtotal += $subtotal;
				$granddisc += $disc;
				$grandppn += $ppn;
				$grandmaterai += $materai;
				$grandtotal += $total;
			}	
			$tp	->addColumn("Grand Total", 6,"right")
				->addColumn(number_format($grandsubtotal,2, "," , "."), 1,"right")
				->addColumn(number_format($granddisc,2, "," , "."), 1,"right")
				->addColumn(number_format($grandppn,2, "," , "."), 1,"right")
				->addColumn(number_format($grandmaterai,2, "," , "."), 1,"right")
				->addColumn(number_format($grandtotal,2, "," , "."), 1,"right")
				->commit("footer");
		}
		
		
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		
			
		$tp	->addColumn($ttd_left, 3,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')),6,"right")
			->commit("footer")
			->addColumn("Petugas Gudang Farmasi", 3,"left")
			->addColumn($ttd_right,6,"right")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($user, 3,"left")
			->addColumn("(....................)", 6,"right")
			->commit("footer");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 4,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/datapenerimaanvendorsummary.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		// shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$qr='';
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,11,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		if($param->tempo !=''){
   			if($param->tempo==1){
   				$jatuhtempo='SUDAH JATUH TEMPO';
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$jatuhtempo='BELUM JATUH TEMPO';
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$statuspembayaran='SUDAH BAYAR';
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   				$statuspembayaran='BELUM BAYAR';
   			}
   		}
   		
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   		}
   		if($param->tempo !=''){
   			if($param->tempo==1){
   				$qr.=" AND B.due_date<'".date('Y-m-d')."'";
   			}else{
   				$qr.=" AND B.due_date>'".date('Y-m-d')."'";
   			}
   		}
   		if($param->bayar !=''){
   			if($param->bayar==1){
   				$qr.=" AND B.bayar='t'";
   			}else{
   				$qr.=" AND B.bayar='f'";
   			}
   		}
		
		
		
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 7)
			->setColumnLength(2, 15)
			->setColumnLength(3, 13)
			->setColumnLength(4, 13)
			->setColumnLength(5, 13)
			->setColumnLength(6, 10)
			->setColumnLength(7, 10)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 13)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 11,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 11,"left")
			->commit("header")
			->addColumn($telp, 11,"left")
			->commit("header")
			->addColumn($fax, 11,"left")
			->commit("header")
			->addColumn("LAPORAN BERDASARKAN VENDOR PER FAKTUR (SUMMARY)", 11,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 11,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		
		#QUERY HEAD
		$data=$this->db->query("select c.vendor,A.no_obat_in, b.kd_vendor,B.tgl_obat_in,B.remark,B.due_date,B.materai,
				(sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))AS sub_total,
				(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount)) AS disc,
				(sum(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100)) AS ppn,
				((sum((A.jml_in_obt / A.frac)*A.hrg_beli_obt))-(sum(((A.apt_disc_rupiah)+((((A.jml_in_obt / A.frac)*A.hrg_beli_obt))/100))*A.apt_discount))+
					(sum(((((A.jml_in_obt / A.frac) * A.hrg_beli_obt)-(A.apt_disc_rupiah + ((((A.jml_in_obt / A.frac)*A.hrg_beli_obt)/100))*A.apt_discount))*A.ppn_item)/100))
					+B.materai)AS total,
				B.materai
			from apt_obat_in_detail A
				inner join  apt_obat_in B ON A.no_obat_in=B.no_obat_in  
				INNER JOIN vendor C ON C.kd_vendor=B.kd_vendor
			WHERE B.tgl_obat_in BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.posting=1  ".$qr."  AND B.kd_unit_far='".$kdUnit."'
			group by 
			 A.no_obat_in,b.kd_vendor,c.vendor,tgl_obat_in,remark,due_date,materai
			 order by  C.vendor,A.no_obat_in ASC")->result();
			
		
			
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("Vendor", 1,"left")
			->addColumn("No.Terima", 1,"left")
			->addColumn("No.Faktur", 1,"left")
			->addColumn("Tgl.Terima", 1,"left")
			->addColumn("Tgl.Tempo", 1,"left")
			->addColumn("Sub Total", 1,"right")
			->addColumn("Disc", 1,"right")
			->addColumn("Materai", 1,"right")
			->addColumn("PPN", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
		
		$no = 0;
		$grandppn =0;
		$granddisc =0;
		$grandsubtotal =0;
		$grandmaterai =0;
		$grandtotal =0;
		if(count($data) < 0){
			$tp	->addColumn("Data tidak ada", 11,"center")
				->commit("header");
		} else{
			$no=0;
	   			$pbf='';
	   			$total=0;
	   			$big_total=0;
	   			$disc=0;
	   			$materai=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_disc=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$grand_materai=0;
	   			$no_obat_in='';
	   			$vendor='';
				$sub_disc =0;
				$sub_sub_total =0;
				$sub_ppn =0;
				$sub_total =0;
	   			for($i=0; $i<count($data); $i++){
	   				if($pbf != $data[$i]->kd_vendor){
	   					$no++;
	   					if($i!=0){
							$tp	->addColumn("Total", 6,"right")
								->addColumn(number_format($total,2, "," , "."), 1,"right")
								->addColumn(number_format($disc,2, "," , "."), 1,"right")
								->addColumn(number_format($ppn,2, "," , "."), 1,"right")
								->addColumn(number_format($materai,2, "," , "."), 1,"right")
								->addColumn(number_format($big_total,2, "," , "."), 1,"right")
								->commit("header");	
	   						
	   						$total=0;
	   						$big_total=0;
	   						$disc=0;
	   						$ppn=0;
	   						$materai=0;
							$sub_disc =0;
							$sub_sub_total =0;
							$sub_ppn =0;
							$sub_total =0;
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					
	   					$tp	->addColumn($no, 1,"left")
							->addColumn($data[$i]->vendor, 10,"left")
							->commit("header");	
	   				}
	   				
	   				$sub_disc+=$data[$i]->disc;
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$disc+=$data[$i]->disc;
	   				$materai+=$data[$i]->materai;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_disc+=$data[$i]->disc;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
	   				$grand_materai=$data[$i]->materai; 
					
					$tp ->addColumn("", 2,"right")
						->addColumn($data[$i]->no_obat_in, 1,"left")
						->addColumn($data[$i]->remark,1 ,"left")
						->addColumn(date('d/m/Y', strtotime($data[$i]->tgl_obat_in)), 1,"left")
						->addColumn(date('d/m/Y', strtotime($data[$i]->due_date)), 1,"left")
						->addColumn(number_format($data[$i]->sub_total,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->disc,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->ppn,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->materai,2, "," , "."), 1,"right")
						->addColumn(number_format($data[$i]->total,2, "," , "."), 1,"right")
						->commit("header");	 
	   				
	   			}
				$tp	->addColumn("Total", 6,"right")
					->addColumn(number_format($total,2, "," , "."), 1,"right")
					->addColumn(number_format($disc,2, "," , "."), 1,"right")
					->addColumn(number_format($ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($materai,2, "," , "."), 1,"right")
					->addColumn(number_format($big_total,2, "," , "."), 1,"right")
					->commit("header");	
	   			$tp	->addColumn("Grand Total", 6,"right")
					->addColumn(number_format($grand_sub_total,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_disc,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_ppn,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_materai,2, "," , "."), 1,"right")
					->addColumn(number_format($grand_total,2, "," , "."), 1,"right")
					->commit("header");	
		}
		
		
		$ttd_left=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left'")->row()->setting;
		$ttd_jabatan=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_jabatan'")->row()->setting;
		$ttd_left_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_name'")->row()->setting;
		$ttd_left_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_left_nip'")->row()->setting;
		$ttd_right=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right'")->row()->setting;
		$ttd_right_name=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_name'")->row()->setting;
		$ttd_right_nip=$this->db->query("SELECT setting FROM sys_setting WHERE key_data='apt_cetakan_bill_ttd_right_nip'")->row()->setting;
		
		
			
		$tp	->addColumn($ttd_left, 3,"left")
			->addColumn($rs->city.", ".tanggalstring(date('Y-m-d')),8,"right")
			->commit("footer")
			->addColumn("Petugas Gudang Farmasi", 3,"left")
			->addColumn($ttd_right,8,"right")
			->commit("footer")
			->addLine("footer")
			->addLine("footer")
			->addLine("footer")
			->addColumn($user, 3,"left")
			->addColumn("(....................)", 8,"right")
			->commit("footer");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 4,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 6,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/datapenerimaanvendorsummary.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
}
?>