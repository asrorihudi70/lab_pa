<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_pengeluaranunitobat extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$array=array();
   		$array['unit']=$this->db->query("SELECT kd_unit_far AS id,nm_unit_far AS text FROM apt_unit WHERE kd_unit_far not in('".$kdUnit."') ORDER BY nm_unit_far ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
   		$qr='';
   		$u='';
   		for($i=0;$i<count($_POST['kd_unit']) ; $i++){
   			if($u !=''){
   				$u.=',';
   			}
   			$u.="'".$_POST['kd_unit'][$i]."'";
   		}
   		if(count($_POST['kd_unit'])>0){
   			$qr=" AND B.kd_unit_far in(".$u.")";
   		}
   		$mpdf=$common->getPDF('L','LAPORAN PENGELUARAN KE UNIT PER OBAT');
   		$queri="SELECT D.kd_jns_obt,G.nama_jenis,D.kd_prd,B.remark,B.no_stok_out,B.tgl_stok_out,C.kd_unit_far,D.nama_obat,E.satuan,D.fractions,
   		A.jml_out AS qty,F.harga_beli,(F.harga_beli*A.jml_out) AS total
   		FROM apt_stok_out_det A INNER JOIN
   		apt_stok_out B ON B.no_stok_out=A.no_stok_out INNER JOIN
   		apt_unit C ON C.kd_unit_far=B.kd_unit_far INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=A.kd_prd AND F.kd_milik=B.kd_milik INNER JOIN
   		apt_jenis_obat G ON G.kd_jns_obt=D.kd_jns_obt
   		WHERE B.tgl_stok_out BETWEEN '".$_POST['start_date']."' AND '".$_POST['last_date']."' AND B.post_out=1  ".$qr."  AND B.kd_unit_cur='".$kdUnit."'
   		ORDER BY D.kd_jns_obt,B.no_stok_out,D.nama_obat";
   		
   		$data=$this->db->query($queri)->result();
   		
   		$unitList=array();
   		$stop=false;
   		for($i=0; $i<count($data); $i++){
   			$aa=$data[$i]->kd_unit_far;
   			$ada=false;
   			for($j=0; $j<count($unitList); $j++){
   				if($unitList[$j]==$data[$i]->kd_unit_far){
   					$ada=true;
   				}
   			}
   			if($ada==false && count($unitList)<9){
   				$unitList[]=$data[$i]->kd_unit_far;
   			}else if(count($unitList)==9 && $stop==false){
   				$unitList[]='lain-lain';
   				$stop=true;
   			}
   			if($ada==false && count($unitList)>=9){
   				$aa='lain-lain';
   			}
   			$data[$i]->$aa=$data[$i]->qty;
   		}
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$u=$this->db->query("SELECT nm_unit_far FROM apt_unit WHERE kd_unit_far='".$kdUnit."'")->row();
   		$nm_unit_far=$u->nm_unit_far;
   		$mpdf->WriteHTML("
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENGELUARAN KE UNIT PER OBAT</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>UNIT : ".$nm_unit_far."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width='80'>No. Faktur</th>
   						<th width='80'>Remark</th>
				   		<th width='80'>Kode</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>
		   				<th width='60'>Harga</th>
   		");
   		for($i=0; $i<count($unitList); $i++){
   			$mpdf->WriteHTML("
   				<th width='30'>".$unitList[$i]."</th>
   			");
   		}
		   		$mpdf->WriteHTML("
	   						<th width='70'>Total</th>
	   					</tr>
	   				</thead>
   				");
	   		if(count($data)==0){
	   			$result->error();
	   			$result->setMessage('Data tidak Ada');
	   			$result->end();
	   		}else{
				$jenis='';
				$total=0;
				$grand_total=0;
	   			for($i=0; $i<count($data); $i++){
	   				if($jenis != $data[$i]->kd_jns_obt){
	   					$jenis=$data[$i]->kd_jns_obt;
	   					if($i!=0){
	   						$mpdf->WriteHTML("<tr>
   							<td colspan='".(7+count($unitList))."' align='right'>Total</td>
   							<td align='right'>".number_format($total,0,',','.')."</td>
   							</tr>");
	   						$total=0;
	   					}
	   					$mpdf->WriteHTML("<tr>
   							<th colspan='".(8+count($unitList))."' align='left'>".$data[$i]->nama_jenis."</th></tr>");
	   				}
	   				$mpdf->WriteHTML("<tr>");
	   				$mpdf->WriteHTML("
	   						<td align='center'>".($i+1)."</td>
	   						<td align='center'>".$data[$i]->no_stok_out."</td>
		   					<td align='center'>".$data[$i]->remark."</td>
	   						<td>".$data[$i]->kd_prd."</td>
   							<td>".$data[$i]->nama_obat."</td>
	   						<td>".$data[$i]->satuan."</td>
	   						<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
	   				");
	   				$total+=$data[$i]->total;
	   				$grand_total+=$data[$i]->total;
	   				for($j=0; $j<count($unitList); $j++){
	   					$li=$unitList[$j];
	   					$isi=0;
	   					if(isset($data[$i]->$li)){
	   						$isi=$data[$i]->$li;
	   					}
	   					$mpdf->WriteHTML("
			   				<td align='right'>".number_format($isi,0,',','.')."</td>
			   			");
	   				}
	   				$mpdf->WriteHTML("
	   						<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
	   					</tr>
   				");
	   			}
	   			$mpdf->WriteHTML("<tr>
   							<td colspan='".(7+count($unitList))."' align='right'>Total</td>
   							<td align='right'>".number_format($total,0,',','.')."</td>
   							</tr>");
	   			$mpdf->WriteHTML("<tr>
   							<th colspan='".(7+count($unitList))."' align='right'>Grand Total</th>
   							<th align='right'>".number_format($grand_total,0,',','.')."</th>
   							</tr>");
	   		}
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPengeluaranKeUnitPerObat';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$result->setData(base_url().$tmpbase.$datenow.$tmpname.'.pdf');
   		$result->end();
   	}
}
?>