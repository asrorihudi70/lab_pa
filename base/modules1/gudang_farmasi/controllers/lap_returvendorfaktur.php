<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_returvendorfaktur extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		
   		$array=array();
   		$array['vendor']=$this->db->query("SELECT kd_vendor as id,vendor as text FROM vendor ORDER BY vendor ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
   
   	public function doPrint(){
   		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		$html='';
   		$qr='';
   		$vendor='SEMUA';
   		
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   			$pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$param->vendor."'")->row();
   			$vendor=$pbf->vendor;
   		}
   		$queri="SELECT C.kd_vendor,C.vendor,B.ret_number,B.ret_date,A.no_obat_in,A.kd_prd,D.nama_obat,E.satuan,A.ret_qty,F.harga_beli,
   		(F.harga_beli*A.ret_qty) AS sub_total,(((F.harga_beli*A.ret_qty)/100)*10) AS ppn,((F.harga_beli*A.ret_qty)+(((F.harga_beli*A.ret_qty)/100)*10))AS total
   		FROM apt_ret_det A INNER JOIN
   		apt_retur B ON B.ret_number=A.ret_number INNER JOIN
   		vendor C ON C.kd_vendor=B.kd_vendor INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=D.kd_prd AND F.kd_milik=B.kd_milik
   		WHERE B.ret_date BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.ret_post=1  ".$qr." AND B.kd_unit_far='".$kdUnit."'
   		ORDER BY C.vendor,B.ret_date,D.nama_obat ASC";
   		$data=$this->db->query($queri)->result();
   		$html.="
   			<table  cellspacing='0' border='0'>
   				<tbody>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN RETUR VENDOR PER FAKTUR</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>".date('d M Y', strtotime($param->start_date))." s/d ".date('d M Y', strtotime($param->last_date))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;'>VENDOR : ".$vendor."</td>
   					</tr>
   				</tbody>
   			</table><br>
   			<table border='1'>
   				<thead>
   					<tr>
   						<th width='40' align='center'>No.</th>
				   		<th width=''>No. Retur</th>
				   		<th width=''>Tgl Retur</th>
				   		<th width=''>No.Terima</th>
   						<th width=''>Kode</th>
				   		<th width=''>Nama Obat</th>
		   				<th width='60'>Sat</th>
		   				<th width='60'>Qty</th>
		   				<th width='60'>Harga</th>
		   				<th width='70'>Sub Total</th>
   						<th width='70'>PPN</th>
   						<th width='70'>Total</th>
   					</tr>
   				</thead>
   				";
	   		if(count($data)==0){
				$html.="<tr>
							<th colspan='12' align='center'>Data tidak ada.</td>
						</tr>";
	   		}else{
	   			$no=0;
	   			$pbf='';
	   			$sub_ppn=0;
	   			$sub_sub_total=0;
	   			$sub_total=0;
	   			$total=0;
	   			$big_total=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$no_obat_in='';
	   			$vendor='';
	   			for($i=0; $i<count($data); $i++){
	   				$sama=false;
	   				if($no_obat_in != $data[$i]->ret_number){
	   					$no_obat_in=$data[$i]->ret_number;
	   					$sama=true;
	   					$no++;
	   					if($i!=0){
	   						$sub_total+=$data[$i]->materai;
	   						$html.="<tr>
   								<td colspan='9' align='right'>Sub total</td>
	   							<td align='right'>".number_format($sub_sub_total,0,',','.')."</td>
   								<td align='right'>".number_format($sub_ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($sub_total,0,',','.')."</td>
	   						</tr>";
	   						$sub_sub_total=0;
	   						$sub_ppn=0;
	   						$sub_total=0;
	   					}
	   				}
	   				if($pbf != $data[$i]->kd_vendor){
	   					if($i!=0){
	   						$html.="<tr>
   								<td colspan='9' align='right'>Total ".$vendor."</td>
	   							<td align='right'>".number_format($total,0,',','.')."</td>
   								<td align='right'>".number_format($ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
	   						</tr>";
	   						$total=0;
	   						$big_total=0;
	   						$ppn=0;
	   						
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$html.="<tr>
   							<th colspan='12' align='left'>".$data[$i]->vendor."</th></tr>";
	   					
	   				}
	   				$html.="<tr>";
	   				if($sama==true){
	   					$html.="
	   						<td align='center'>".$no."</td>
	   						<td align='center'>".$data[$i]->ret_number."</td>
		   					<td align='center'>".date('d/m/Y', strtotime($data[$i]->ret_date))."</td>
	   						<td>".$data[$i]->no_obat_in."</td>
   							
	   						";
	   				}else{
	   					$html.="
	   						<td colspan='4'>&nbsp;</td>
	   						";
	   				}
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
	   				$html.="
	   					<td>".$data[$i]->kd_prd."</td>
   						<td>".$data[$i]->nama_obat."</td>
   						<td align='center'>".$data[$i]->satuan."</td>
   						<td align='right'>".number_format($data[$i]->ret_qty,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->harga_beli,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->sub_total,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->ppn,0,',','.')."</td>
   						<td align='right'>".number_format($data[$i]->total,0,',','.')."</td>
   					</tr>";
	   			}
	   			$html.="<tr>
   								<td colspan='9' align='right'>Sub total</td>
	   							<td align='right'>".number_format($sub_sub_total,0,',','.')."</td>
   								<td align='right'>".number_format($sub_ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($sub_total,0,',','.')."</td>
	   						</tr>";
	   			$html.="<tr>
   								<td colspan='9' align='right'>Total ".$vendor."</td>
	   							<td align='right'>".number_format($total,0,',','.')."</td>
   								<td align='right'>".number_format($ppn,0,',','.')."</td>
	   							<td align='right'>".number_format($big_total,0,',','.')."</td>
	   						</tr>";
	   			$html.="<tr>
   								<th colspan='9' align='right'>Grand total</td>
	   							<th align='right'>".number_format($grand_sub_total,0,',','.')."</th>
   								<th align='right'>".number_format($grand_ppn,0,',','.')."</th>
	   							<th align='right'>".number_format($grand_total,0,',','.')."</th>
	   						</tr>";
	   		}
		$html.="</tbody></table>";
   		$common=$this->common;
		$this->common->setPdf('L','LAPORAN RETUR VENDOR PER FAKTUR',$html);
		echo $html;
   	}

	public function doPrintDirect(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(216,12,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
   		$qr='';
   		$vendor='SEMUA';
   		
   		if($param->vendor!=''){
   			$qr.=" AND B.kd_vendor='".$param->vendor."'";
   			$pbf=$this->db->query("SELECT vendor FROM vendor WHERE kd_vendor='".$param->vendor."'")->row();
   			$vendor=$pbf->vendor;
   		}
		
		# SET JUMLAH KOLOM HEADER
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 15)
			->setColumnLength(2, 10)
			->setColumnLength(3, 15)
			->setColumnLength(4, 10)
			->setColumnLength(5, 18)
			->setColumnLength(6, 6)
			->setColumnLength(7, 5)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 10)
			->setColumnLength(11, 10)
			->setUseBodySpace(true);
			
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 12,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 12,"left")
			->commit("header")
			->addColumn($telp, 12,"left")
			->commit("header")
			->addColumn($fax, 12,"left")
			->commit("header")
			->addColumn("LAPORAN RETUR VENDOR PER FAKTUR", 12,"center")
			->commit("header")
			->addColumn(tanggalstring(date('Y-m-d', strtotime($param->start_date))) ." s/d ".tanggalstring(date('Y-m-d', strtotime($param->last_date))), 12,"center")
			->commit("header")
			->addColumn("VENDOR : ".$vendor, 12,"center")
			->commit("header")
			->addSpace("header")
			->addLine("header");
		
		#QUERY HEAD
		$reshead="SELECT C.kd_vendor,C.vendor,B.ret_number,B.ret_date,A.no_obat_in,A.kd_prd,D.nama_obat,E.satuan,A.ret_qty,F.harga_beli,
   		(F.harga_beli*A.ret_qty) AS sub_total,(((F.harga_beli*A.ret_qty)/100)*10) AS ppn,((F.harga_beli*A.ret_qty)+(((F.harga_beli*A.ret_qty)/100)*10))AS total
   		FROM apt_ret_det A INNER JOIN
   		apt_retur B ON B.ret_number=A.ret_number INNER JOIN
   		vendor C ON C.kd_vendor=B.kd_vendor INNER JOIN
   		apt_obat D ON D.kd_prd=A.kd_prd INNER JOIN
   		apt_satuan E ON E.kd_satuan=D.kd_satuan INNER JOIN
   		apt_produk F ON F.kd_prd=D.kd_prd AND F.kd_milik=B.kd_milik
   		WHERE B.ret_date BETWEEN '".$param->start_date."' AND '".$param->last_date."' AND B.ret_post=1  ".$qr." AND B.kd_unit_far='".$kdUnit."'
   		ORDER BY C.vendor,B.ret_date,D.nama_obat ASC";
		$data=$this->db->query($reshead)->result();	
		
		$tp	->addColumn("NO.", 1,"center")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("No. Retur", 1,"left")
			->addColumn("Tgl Retur", 1,"left")
			->addColumn("No. Terima", 1,"left")
			->addColumn("Kode", 1,"left")
			->addColumn("Nama Obat", 1,"left")
			->addColumn("Satuan", 1,"left")
			->addColumn("Qty", 1,"right")
			->addColumn("Harga", 1,"right")
			->addColumn("Sub Total", 1,"right")
			->addColumn("PPN", 1,"right")
			->addColumn("Total", 1,"right")
			->commit("header");
		if(count($data) < 0){
			$tp	->addColumn("Data tidak ada", 12,"center")
				->commit("header");
		} else{
			$no=0;
	   			$pbf='';
	   			$sub_ppn=0;
	   			$sub_sub_total=0;
	   			$sub_total=0;
	   			$total=0;
	   			$big_total=0;
	   			$ppn=0;
	   			$grand_sub_total=0;
	   			$grand_ppn=0;
	   			$grand_total=0;
	   			$no_obat_in='';
	   			$vendor='';
	   			for($i=0; $i<count($data); $i++){
	   				$sama=false;
	   				if($no_obat_in != $data[$i]->ret_number){
	   					$no_obat_in=$data[$i]->ret_number;
	   					$sama=true;
	   					$no++;
	   					if($i!=0){
	   						// $sub_total+=$data[$i]->materai;
							$tp	->addColumn("Sub total", 9, "right")
								->addColumn(number_format($sub_sub_total,0,',','.'), 1, "right")
								->addColumn(number_format($sub_ppn,0,',','.'), 1, "right")
								->addColumn(number_format($sub_total,0,',','.'), 1, "right")
								->commit("header");	
	   						$sub_sub_total=0;
	   						$sub_ppn=0;
	   						$sub_total=0;
	   					}
	   				}
	   				if($pbf != $data[$i]->kd_vendor){
	   					if($i!=0){
							$tp	->addColumn("Total ".$vendor, 9, "right")
								->addColumn(number_format($total,0,',','.'), 1, "right")
								->addColumn(number_format($ppn,0,',','.'), 1, "right")
								->addColumn(number_format($big_total,0,',','.'), 1, "right")
								->commit("header");	
	   						$total=0;
	   						$big_total=0;
	   						$ppn=0;
	   						
	   					}
	   					$pbf=$data[$i]->kd_vendor;
	   					$vendor=$data[$i]->vendor;
	   					$tp	->addColumn('', 1, "center")
							->addColumn($data[$i]->vendor, 11, "left");
	   				}
	   				$tp ->commit("header");	
	   				if($sama==true){
							$tp	->addColumn($no, 1, "center")
								->addColumn($data[$i]->ret_number, 1, "left")
								->addColumn(date('d/m/Y', strtotime($data[$i]->ret_date)), 1, "center")
								->addColumn($data[$i]->no_obat_in, 1, "left");
	   				}else{
							$tp	->addColumn("", 4, "left");
	   				}
				
	   				$sub_sub_total+=$data[$i]->sub_total;
	   				$sub_ppn+=$data[$i]->ppn;
	   				$sub_total+=$data[$i]->total;
	   				$big_total+=$data[$i]->total;
	   				$total+=$data[$i]->sub_total;
	   				$ppn+=$data[$i]->ppn;
	   				$grand_sub_total+=$data[$i]->sub_total;
	   				$grand_ppn+=$data[$i]->ppn;
	   				$grand_total+=$data[$i]->total;
					$tp	->addColumn($data[$i]->kd_prd, 1, "left")
						->addColumn($data[$i]->nama_obat, 1, "left")
						->addColumn($data[$i]->satuan, 1, "left")
						->addColumn(number_format($data[$i]->ret_qty,0,',','.'), 1, "right")
						->addColumn(number_format($data[$i]->harga_beli,0,',','.'), 1, "right")
						->addColumn(number_format($data[$i]->sub_total,0,',','.'), 1, "right")
						->addColumn(number_format($data[$i]->ppn,0,',','.'), 1, "right")
						->addColumn(number_format($data[$i]->total,0,',','.'), 1, "right")
						->commit("header");
	   			}
				$tp	->addColumn('Subtotal', 9, "right")
					->addColumn(number_format($sub_sub_total,0,',','.'), 1, "right")
					->addColumn(number_format($sub_ppn,0,',','.'), 1, "right")
					->addColumn(number_format($sub_total,0,',','.'), 1, "right")
					->commit("header");
				$tp	->addColumn('Total '.$vendor, 9, "right")
					->addColumn(number_format($total,0,',','.'), 1, "right")
					->addColumn(number_format($ppn,0,',','.'), 1, "right")
					->addColumn(number_format($big_total,0,',','.'), 1, "right")
					->commit("header");
				$tp	->addColumn('Grand Total', 9, "right")
					->addColumn(number_format($grand_sub_total,0,',','.'), 1, "right")
					->addColumn(number_format($grand_ppn,0,',','.'), 1, "right")
					->addColumn(number_format($grand_total,0,',','.'), 1, "right")
					->commit("header");					
	   		
	   		}
			$no = 0;
			$ppn =0;
			$disc =0;
			$subtotal =0;
			$total =0;
			$harga_beli =0;
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 5,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/data.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);

		// unlink($file);
   	}
	
}
?>