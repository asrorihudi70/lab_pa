<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewstokmin extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='DAFTAR STOK OBAT MINIMUM';
		$param=json_decode($_POST['data']);
		
		$tanggal=tanggalstring(date('Y-m-d'));
		$kd_unit_far = $this->session->userdata['user_id']['aptkdunitfar'];
		
		$unitfar=$this->db->query("SELECT nm_unit_far from apt_unit where kd_unit_far='".$kd_unit_far."'")->row()->nm_unit_far;
		
		$queryBody = $this->db->query( "SELECT s.kd_milik,s.kd_prd,p.minstok,st.jml_stok_apt,o.nama_obat,m.milik
										FROM apt_stok_unit_gin s
											INNER JOIN (SELECT sum(jml_stok_apt) as jml_stok_apt,kd_prd,kd_milik,kd_unit_far FROM apt_stok_unit_gin 
														WHERE kd_unit_far='".$kd_unit_far."' GROUP BY kd_prd,kd_milik,kd_unit_far) st on st.kd_milik=s.kd_milik 
												and st.kd_prd=s.kd_prd and st.kd_unit_far=s.kd_unit_far
											INNER JOIN apt_produk p on p.kd_milik=s.kd_milik and p.kd_prd=s.kd_prd
											INNER JOIN apt_obat o on o.kd_prd=s.kd_prd
											INNER JOIN apt_milik m on m.kd_milik=s.kd_milik
										WHERE s.kd_unit_far='".$kd_unit_far."' and st.jml_stok_apt <= p.minstok
										GROUP BY s.kd_milik,s.kd_prd,p.minstok,st.jml_stok_apt,o.nama_obat,m.milik");
		$query = $queryBody->result();	
		
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6"> Tanggal '.$tanggal.'</th>
					</tr>
					<tr>
						<th colspan="6"> Unit Farmasi : '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5" align="center">No</th>
					<th width="60" align="center">Kepemilikan</th>
					<th width="60" align="center">Kd Prd</th>
					<th width="80" align="center">Nama Obat</th>
					<th width="40" align="center">Min Stok</th>
					<th width="40" align="center">Stok Tersedia</th>
				  </tr>
			</thead>';
		if(count($query) > 0) {
			$no=0;
			//$grandnilai=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
						<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->milik.'</td>
							<td>'.$line->kd_prd.'</td>
							<td>'.$line->nama_obat.'</td>
							<td width="" align="right">'.$line->minstok.'</td>
							<td width="" align="right"><font color="red">'.$line->jml_stok_apt.'</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="5" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$name='Minimum_Stok_'.date("dMY").'.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=".$name);
		echo $html;
		// $this->common->setPdf('P','Daftar Stok Obat Hampir Habis',$html);	
		// echo $html;
   	}
	
	
}
?>