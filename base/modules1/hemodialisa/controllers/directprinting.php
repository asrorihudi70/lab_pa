<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class directprinting extends MX_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->library('session');
    }
	 
    public function index(){
          $this->load->view('main/index',$data=array('controller'=>$this));
    }

    public function cetakBill() {
		$bataskertas= new Pilihkertas;
        $strError = "";
        $logged = $this->session->userdata('user_id');
		$si_user = $this->session->userdata['user_id']['id'];
		
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            $NameRS = $query[0][0]->NAME;
            $Address = $query[0][0]->ADDRESS;
            $TLP = $query[0][0]->PHONE1;
            $Kota = $query[0][0]->CITY;
        } else {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        $no_transaksi = $_POST["No_TRans"];
        $kd_kasir = $_POST["kd_kasir"];
        $criteria = "no_transaksi = '" . $no_transaksi . "' and kd_kasir='".$kd_kasir."'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            $Medrec = $query[0][0]->KD_PASIEN;
            $Status = $query[0][0]->STATUS;
            $Dokter = $query[0][0]->DOKTER;
            $Nama = $query[0][0]->NAMA;
            $Alamat = $query[0][0]->ALAMAT;
            $Poli = $query[0][0]->UNIT;
            $Notrans = $query[0][0]->NO_TRANSAKSI;
            $Tgl = $query[0][0]->TGL_TRANS;
            $KdUser = $query[0][0]->KD_USER;
            $uraian = $query[0][0]->DESKRIPSI;
            $jumlahbayar = $query[0][0]->JUMLAH;
        } else {
            //$NoNota = "";
            $Medrec = "";
            $Status = "";
            $Dokter = "";
            $Nama = "";
            $Alamat = "";
            $Poli = " ";
            $Notrans = "";
            $Tgl = "";
        }
		
		//$printer = $_POST['printer'];
        $printer = $this->db->query("select p_bill from zusers where kd_user = '$si_user'")->row()->p_bill;
		
        $t1 = 4;
        $t3 = 20;
        $t4 = 5;
        $t2 = 55 - ($t3 + $t1 + $t4);
        $format1 = Bulaninindonesia(date('d F Y', strtotime($Tgl)));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file = tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
		$feed=$bataskertas->PageLength('laporan/2');
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
		$condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27) . chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data = $initialized;
		$Data .=$feed;
        $Data .= $condensed1;
        $Data .= $NameRS . "\n";
        $Data .= $Address . "\n";
        $Data .= "Phone : " . $TLP . "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= "No. Trans  : " . $Notrans . "          " . "Tgl       : " . $format1 . "\n";
        $Data .= "No. Medrec : " . $Medrec . "\n";
        $Data .= "Status P.  : " . $Status . "\n";
        $Data .= "Dokter     : " . $Dokter . "\n";
        $Data .= "Nama       : " . $Nama . "\n";
        $Data .= "Alamat     : " . $Alamat . "\n";
        $Data .= "" . $Poli . "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= str_pad("No.", $t1, " ") . str_pad("Uraian", $t2, " ") . str_pad("Sub Total", $t3, " ", STR_PAD_LEFT) . "\n";
        $Data .= "------------------------------------------------------------\n";

        $queryDet = $this->db->query("select p.deskripsi, dt.harga, dt.urut from detail_transaksi dt inner join produk p on dt.kd_produk = p.kd_produk where no_transaksi = '" . $no_transaksi . "' and kd_kasir='".$kd_kasir."' order by deskripsi")->result();
        $no = 0;
        foreach ($queryDet as $line) {
            $no++;
            $Data .= str_pad($no, $t1, " ") . str_pad($line->deskripsi, $t2, " ") . str_pad($jadi = number_format($line->harga, 0, ',', '.'), $t3, " ", STR_PAD_LEFT) . "\n";
        }
        $Data .= "------------------------------------------------------------\n";
        $queryJum = $this->db->query("select sum(harga) as total from (select harga from detail_transaksi where no_transaksi = '" . $Notrans . "' and kd_kasir='".$kd_kasir."') as x")->result();
        foreach ($queryJum as $line) {
            $Data .= str_pad("Total", 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->total, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $queryTotJum = $this->db->query("select uraian,jumlah from detail_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $Notrans . "' and kd_kasir='".$kd_kasir."'")->result();
        foreach ($queryTotJum as $line) {
            $Data .= str_pad($line->uraian, 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->jumlah, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $Data .= "\n";
        $Data .= str_pad(" ", 30, " ") . str_pad($Kota . ' , ' . $today, 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad(" ", 30, " ") . str_pad("(---------------------)", 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 20, " ", STR_PAD_BOTH) . "\n";
        $Data .= "\n";
        $Data .= str_pad("Jam : " . $Jam, 30, " ") . str_pad("Operator : " . $logged['username'], 30, " ", STR_PAD_LEFT) . "\n";
		$Data .= "\n";
		$Data .= $formfeed;
		fwrite($handle, $Data);
		fclose($handle);
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			echo '{success:true}';
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			
			shell_exec("lpr -P " . $printer . " " . $file); # Lakukan cetak linux
			echo '{success:true}';
			// echo $Data;
		}
		
        $error = "sukses";
        return $error;
    }
	
	public function cetakKwitansi() {
        $strError = "";
        $no_transaksi = $_POST["No_TRans"];
		// $printer = $_POST["printer"];
		$si_user = $this->session->userdata['user_id']['id'];
		$printer = $this->db->query("select p_kwitansi from zusers where kd_user = '$si_user'")->row()->p_kwitansi;		
        $Total = "";
        $q = $this->db->query("select * from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '" . $no_transaksi . "' and jenis_pay = 1");
        $queryjumlah = $this->db->query("select sum(jumlah) as jumlah from (select * from detail_bayar db
                                        inner join payment p on db.kd_pay = p.kd_pay
                                        where no_transaksi = '" . $no_transaksi . "' and jenis_pay = 1) as x");

        if ($q->num_rows == 0) {
            $strError = '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        } else {
            foreach ($queryjumlah->result() as $data) {
                $Total = $data->jumlah;
            }
            $logged = $this->session->userdata('user_id');

            $this->load->model('general/tb_dbrs');
            $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
            if ($query[1] != 0) {
                $NameRS = $query[0][0]->NAME;
                $Address = $query[0][0]->ADDRESS;
                $TLP = $query[0][0]->PHONE1;
                $Kota = $query[0][0]->CITY;
            } else {
                $NameRS = "";
                $Address = "";
                $TLP = "";
            }

            $criteria = "no_transaksi = '" . $no_transaksi . "'";
            $paramquery = "where no_transaksi = '" . $no_transaksi . "'";
            $this->load->model('general/tb_cekdetailtransaksi');
            $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
            $query = $this->tb_cekdetailtransaksi->GetRowList(0, 1, "", "", "");
            if ($query[1] != 0) {
                $Medrec = $query[0][0]->KD_PASIEN;
                $Status = $query[0][0]->STATUS;
                $Dokter = $query[0][0]->DOKTER;
                $Nama = $query[0][0]->NAMA;
                $Alamat = $query[0][0]->ALAMAT;
                $Poli = $query[0][0]->UNIT;
                $Notrans = $query[0][0]->NO_TRANSAKSI;
                $Tgl = $query[0][0]->TGL_TRANS;
                $KdUser = $query[0][0]->KD_USER;
                $uraian = $query[0][0]->DESKRIPSI;
                $jumlahbayar = $query[0][0]->JUMLAH;
            } else {
                $Medrec = "";
                $Status = "";
                $Dokter = "";
                $Nama = "";
                $Alamat = "";
                $Poli = " ";
                $Notrans = "";
                $Tgl = "";
            }

            

            $format1 = date('d F Y', strtotime($Tgl));
            $today = Bulaninindonesia(date("d F Y"));
            $Jam = date("G:i:s");
            $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
            $file = tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
            $handle = fopen($file, 'w');
            $condensed = Chr(27) . Chr(33) . Chr(4);
            $bold1 = Chr(27) . Chr(69);
            $bold0 = Chr(27) . Chr(70);
            $initialized = chr(27) . chr(64);
            $condensed1 = chr(15);
            $condensed0 = chr(18);
            $Data = $initialized;
            $Data .= $condensed1;
            $Data .= chr(27) . chr(87) . chr(49);
            $Data .= $NameRS . "\n";
            $Data .= "\n";
            $Data .= $Address . "\n";
            $Data .= "\n";
            $Data .= "Phone : " . $TLP . "\n";
            $Data .= "\n";
            $Data .= str_pad($bold1 . "KWITANSI" . $bold0, 62, " ", STR_PAD_BOTH) . "\n";
            $Data .= "\n";
            $Data .= str_pad("No. Transaksi : " . $no_transaksi, 31, " ") . str_pad("No. Medrec : " . $Medrec, 31, " ") . "\n";
            $Data .= "\n";
            $Data .= str_pad("Telah Terima Dari : " . $Nama, 30, " ") . "\n";
            $Data .= "\n";
            $Data .= "Banyaknya uang terbilang : " . terbilang($Total) . 'Rupiah' . "\n";
            $Data .= "\n";
            $Data .= "Untuk pembayaran Pemeriksaan untuk Pasien : " . $Nama . "\n";
            $Data .= "\n";
            $Data .= "Unit yang dituju : " . $Poli . "\n";
            $Data .= "\n";
            $Data .= "Jumlah Rp. " . number_format($Total, 0, ',', '.') . "\n";
            $Data .= "\n";
            $Data .= str_pad(" ", 31, " ") . str_pad($Kota . ' , ' . $today, 31, " ", STR_PAD_LEFT) . "\n";
            $Data .= "\n";
            $Data .= "\n\n\n\n\n\n\n\n\n\n\n\n\n";
			
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				fwrite($handle, $Data);
				fclose($handle);
				shell_exec("lpr -P " . $printer . " " . $file);# Lakukan cetak linux
				// echo $Data;
			}
			
            $strError = "{success:true}";
        }
		
        echo $strError;
    }

}
