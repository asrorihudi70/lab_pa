<?php

/**
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionHasilHemodialisa extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	public function getGridHasilHemodialisa(){
		$nama	=	$_POST['nama'];
		$kode	=	$_POST['kode'];
		$tgl_awal = $_POST['tgl_awal'];
		$tgl_akhir = $_POST['tgl_akhir'];
		if($nama=='')
		{
			$criteria="";
		}else{
			$criteria=" and upper (p.nama) like upper('".$nama."%')";
		}

		if($kode==''){
			$criteria2="";
		}else {
			$criteria2=" and upper (k.kd_pasien) like upper('".$kode."%')";
		}
		
		if ($tgl_awal == ''){
			$criteria3="";
		}else{
			$criteria3=" and (hdk.tgl_masuk) >= '".$tgl_awal."'";
		}
		
		if ($tgl_akhir == ''){
			$criteria4="";
		}else{
			$criteria4=" and (hdk.tgl_masuk) <= '".$tgl_akhir."'";
		}
		
		$result=$this->db->query("select hdk.*,p.nama,p.tgl_lahir,p.jenis_kelamin, p.alamat,p.gol_darah,k.kd_dokter,d.nama as nama_dokter,t.no_transaksi,t.kd_kasir, tr.kd_unit as kd_unit_asal, u.nama_unit as nama_unit_asal, tr.no_transaksi as no_transaksi_asal, tr.kd_kasir as kd_kasir_asal, kl.kd_dokter as kd_dokter_asal, dr.nama as nama_dokter_asal
									from hd_kunjungan hdk
								inner join kunjungan k on k.kd_pasien=hdk.kd_pasien and k.kd_unit=hdk.kd_unit and k.tgl_masuk=hdk.tgl_masuk and k.urut_masuk=hdk.urut_masuk
								inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
								inner join unit_asal ua on ua.no_transaksi=t.no_transaksi and ua.kd_kasir=t.kd_kasir
								inner join transaksi tr on ua.no_transaksi_asal=tr.no_transaksi and ua.kd_kasir_asal=tr.kd_kasir
								inner join kunjungan kl on tr.kd_pasien =kl.kd_pasien and tr.kd_unit=kl.kd_unit and tr.tgl_transaksi=kl.tgl_masuk and tr.urut_masuk=kl.urut_masuk
								inner join dokter dr on kl.kd_dokter=dr.kd_dokter
								inner join pasien p on p.kd_pasien=hdk.kd_pasien
								inner join dokter d on d.kd_dokter=k.kd_dokter
								inner join unit u on u.kd_unit=tr.kd_unit where p.nama <> '' $criteria $criteria2 $criteria3 $criteria4")->result();
									
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getHasilHDGrid(){
		$kd_pasien=$_POST['text'];
		
		if($kd_pasien == ''){
			$criteria="";
		} else{
			$criteria=" WHERE k.kd_pasien like upper('".$kd_pasien."%')";
		}
		$result=$this->db->query("select hdk.*,p.nama,p.tgl_lahir,p.alamat,p.gol_darah,k.kd_dokter,d.nama as nama_dokter,t.no_transaksi,t.kd_kasir, tr.kd_unit as kd_unit_asal, u.nama_unit as nama_unit_asal, tr.no_transaksi as no_transaksi_asal, tr.kd_kasir as kd_kasir_asal, kl.kd_dokter, dr.nama as nama_dokter_asal
									from hd_kunjungan hdk
								inner join kunjungan k on k.kd_pasien=hdk.kd_pasien and k.kd_unit=hdk.kd_unit and k.tgl_masuk=hdk.tgl_masuk and k.urut_masuk=hdk.urut_masuk
								inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
								inner join unit_asal ua on ua.no_transaksi=t.no_transaksi and ua.kd_kasir=t.kd_kasir
								inner join transaksi tr on ua.no_transaksi_asal=tr.no_transaksi and ua.kd_kasir_asal=tr.kd_kasir
								inner join kunjungan kl on tr.kd_pasien =kl.kd_pasien and tr.kd_unit=kl.kd_unit and tr.tgl_transaksi=kl.tgl_masuk and tr.urut_masuk=kl.urut_masuk
								inner join dokter dr on kl.kd_dokter=dr.kd_dokter
								inner join pasien p on p.kd_pasien=hdk.kd_pasien
								inner join dokter d on d.kd_dokter=k.kd_dokter
								inner join unit u on u.kd_unit=tr.kd_unit $criteria		
								")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getUmur()
	{
		 $tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $_POST['TanggalLahir'])));
        $strQuery = "select age(timestamp '".$tmptgllahir."')";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
             foreach($res as $data)
             {
                $umur = $data->age;
             }
         echo $umur;
		}
         return $umur;
	}
	
	public function getGridHD()
	{
		$no_dia = $_POST['NoDia'];
		$kd_dia = $_POST['KdDia'];
		$result = $this->db->query("select d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa, a.no_urut, a.nilai, e.no_ref, e.deskripsi 
									from hd_item_kunjungan a
									inner join hd_item_dia b on a.kd_item=b.kd_item::integer and a.kd_dia = b.kd_dia 
									inner join hd_dia c on b.kd_dia = c.kd_dia
									inner join hd_item d on b.kd_item = d.kd_item
									left join hd_ref e on d.kd_item=e.kd_item and a.no_ref=e.no_ref
									where a.no_dia='$no_dia' and c.kd_dia='$kd_dia' group by 
									d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa , a.no_urut, a.nilai, e.no_ref, e.deskripsi order by d.kd_item;")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function saveHasilHD()
	{
		$no_dia = $_POST['NoDia'];
		$kd_dia = $_POST['KdDia'];
		$jmllist= $_POST['jml'];
		
		for($i=0;$i<$jmllist;$i++){
			$kd_item = $_POST['kd_item-'.$i];
			$no_urut = $_POST['no_urut-'.$i];
			$nilai = $_POST['nilai-'.$i];
			$data = array("nilai"=>$nilai);
			
			$criteria = array("no_dia"=>$no_dia,"kd_dia"=>$kd_dia,"no_urut"=>$no_urut,"kd_item"=>$kd_item);
			$this->db->where($criteria);
			$result=$this->db->update('hd_item_kunjungan',$data);
		}
		
		if($result){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function CetakHasilHD()
	{
		$common=$this->common;
   		$result=$this->result;
		$title='HASIL PEMERIKSAAN HEMODIALISA';
		$param=json_decode($_POST['data']);
		
		$tglmasuk = $param->Tgl;
        $kdpasien = $param->KdPasien;
        $NoDia = $param->NoDia;
        $NamaPasien = $param->Nama;
        $JK = $param->JenisKelamin;
        $Ttl = $param->Ttl;
        $Umur = $param->Umur;
        $Alamat = $param->Alamat;
        $Poli = $param->Poli;
        $Dokter = $param->Dokter;
        $urut = $param->urutmasuk;
        $NamaUnitAsal = $param->NamaUnitAsal;
        $KdDokterAsal = $param->KdDokterAsal;
        $JamMasuk = $param->JamMasuk;
        $NamaDokterAsal = $param->NamaDokterAsal;
        $tmpJamMasuk = substr($JamMasuk, 11, 5);

        if ($NamaUnitAsal == '' || $NamaUnitAsal == 'null') {
            $NamaUnitAsal = $Poli;
        }
		
		//ambil judul unit
        $kd_bagian = $this->db->query("select * from unit where nama_unit='" . $NamaUnitAsal . "'")->row()->kd_bagian;
        if ($kd_bagian = 1) {
            $judulunit = "Kelas Ruang";
        } else if ($kd_bagian = 2) {
            $judulunit = "Poliklinik";
        } else {
            $judulunit = "Laboratorium";
        }

   
		//ambil data hasil HD
        $Params = "where hd_kunjungan.kd_pasien = '$kdpasien' and hd_kunjungan.tgl_masuk = '$tglmasuk' and hd_kunjungan.urut_masuk = $urut";
        $q = $this->db->query("select * from hd_kunjungan " . $Params)->result();

        
            $queryHasil_pradialisa = $this->db->query("select d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa, a.no_urut, a.nilai, e.no_ref, e.deskripsi
											from hd_item_kunjungan a
											inner join hd_item_dia b on a.kd_item=b.kd_item::integer and a.kd_dia = b.kd_dia 
											inner join hd_dia c on b.kd_dia = c.kd_dia
											inner join hd_item d on b.kd_item = d.kd_item
											left join hd_ref e on d.kd_item=e.kd_item and a.no_ref=e.no_ref
											inner join hd_kunjungan f on a.no_dia = f.no_dia
											where f.no_dia='$NoDia' and f.tgl_masuk ='$tglmasuk' and f.urut_masuk='$urut' and a.kd_dia='0' group by 
											d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa , a.no_urut, a.nilai, e.no_ref, e.deskripsi order by d.kd_item ");
            $query_pradialisa = $queryHasil_pradialisa->result();
			
			$queryHasil_dialisa = $this->db->query("select d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa, a.no_urut, a.nilai, e.no_ref, e.deskripsi
											from hd_item_kunjungan a
											inner join hd_item_dia b on a.kd_item=b.kd_item::integer and a.kd_dia = b.kd_dia 
											inner join hd_dia c on b.kd_dia = c.kd_dia
											inner join hd_item d on b.kd_item = d.kd_item
											left join hd_ref e on d.kd_item=e.kd_item and a.no_ref=e.no_ref
											inner join hd_kunjungan f on a.no_dia = f.no_dia
											where f.no_dia='$NoDia' and f.tgl_masuk ='$tglmasuk' and f.urut_masuk='$urut' and a.kd_dia='1' group by 
											d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa , a.no_urut, a.nilai, e.no_ref, e.deskripsi order by d.kd_item ");
            $query_dialisa = $queryHasil_dialisa->result();
			
			$queryHasil_pascadialisa = $this->db->query("select d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa, a.no_urut, a.nilai, e.no_ref, e.deskripsi
											from hd_item_kunjungan a
											inner join hd_item_dia b on a.kd_item=b.kd_item::integer and a.kd_dia = b.kd_dia 
											inner join hd_dia c on b.kd_dia = c.kd_dia
											inner join hd_item d on b.kd_item = d.kd_item
											left join hd_ref e on d.kd_item=e.kd_item and a.no_ref=e.no_ref
											inner join hd_kunjungan f on a.no_dia = f.no_dia
											where f.no_dia='$NoDia' and f.tgl_masuk ='$tglmasuk' and f.urut_masuk='$urut' and a.kd_dia='2' group by 
											d.kd_item, d.item_hd, d.type_item, c.kd_dia, c.dialisa , a.no_urut, a.nilai, e.no_ref, e.deskripsi order by d.kd_item ");
            $query_pascadialisa = $queryHasil_pascadialisa->result();
			
            $queryRS = $this->db->query("select * from db_rs")->row();
			$kota=$queryRS->city;
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
          
            $date = date("d-M-Y / H:i:s");
           
            $html="<h2 class='formarial' align='center'>Laporan Hasil Pemeriksaan Hemodialisa</h2>";
            $html.='<table width="1039" border="" class="detail">
										<tr>
										  <td >Tanggal</td>
										  <td>:</td>
										  <td colspan="4">' . $tglmasuk . '</td>
										  <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										  <td >No Reg</td>
										  <td >:</td>
										  <td ></td>
										</tr>
										<tr>
										  <td>No Medrec</td>
										  <td>:</td>
										  <td colspan="4">' . $kdpasien . '</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										</tr>
										<tr>
										  <td>Nama Pasien</td>
										  <td>:</td>
										  <td colspan="4">' . $NamaPasien . '</td>
										  <td>&nbsp;</td>
										  <td>Dokter Pengirim</td>
										  <td>:</td>
										  <td>' . $NamaDokterAsal . '</td>
										</tr>
										<tr>
										  <td>Jenis Kelamin</td>
										  <td>:</td>
										  <td colspan="4">' . $JK . '</td>
										  <td>&nbsp;</td>
										  <td>Dokter Penanggung Jawab</td>
										  <td>:</td>
										  <td>' . $Dokter . '</td>
										</tr>
										<tr>
										  <td>Umur</td>
										  <td>:</td>
										  <td >' . $Umur . '</td>
										  <td >Tgl Lahir</td>
										  <td >:</td>
										  <td >' . $Ttl . '</td>
										  <td>&nbsp;</td>
										  <td>Jam Pemeriksaan</td>
										  <td>:</td>
										  <td>' . $tmpJamMasuk . '</td>
										</tr>
										<tr>
										  <td>Alamat</td>
										  <td>:</td>
										  <td colspan="4">' . $Alamat . '</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										</tr>
										<tr>
										  <td>' . $judulunit . '</td>
										  <td>:</td>
										  <td colspan="4">' . $NamaUnitAsal . '</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td colspan="4">&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
										</tr>
								  </table>';
			
			if(count($q)==0){
				$html.='
					   <tbody>
			  
							<tr class="headerrow"> 
								<td width="" colspan="4"></th>
							</tr>
						<p>&nbsp;</p>
				   ';
			}else{
				$html.="<h3 class='formarial' align='left'>Pradialisa</h3>";
					$html.='<table style="overflow: wrap" class="t1" border = "1">
							<thead>
								<tr>
									<th width="85" align="center">Kode Item</td>
									<th width="170"  align="center">Item</td>
									<th width="170"  align="center">Test</td>
									<th width=""  align="center">Hasil</td>
								</tr>
							</thead>
							';					
				foreach ($query_pradialisa as $line) {
					$kd_item = $line->kd_item;
					$item_hd = $line->item_hd;
					$test = $line->deskripsi;
					$hasil = $line->nilai;
					
					$html.='
					   <tbody>
							<tr class="headerrow"> 
								<td >&nbsp;' . $kd_item .  '</th>
								<td  align="center">' . $item_hd . '</td>
								<td  align="center">' . $test . '</td>
								<td  align="left">&nbsp;' . $hasil . '</td>
							</tr>
						<p>&nbsp;</p>
				   ';
				}
				
			    $html.='</tbody></table>';
				$html.="<h3 class='formarial' align='left'>Dialisa</h3>";
					$html.='<table style="overflow: wrap" class="t1" border = "1">
							<thead>
								<tr>
									<th width="85" align="center">Kode Item</td>
									<th width="170" align="center">Item</td>
									<th width="170" align="center">Test</td>
									<th width="" align="center">Hasil</td>
								</tr>
							</thead>
							';
				foreach ($query_dialisa as $line) {
					$kd_item = $line->kd_item;
					$item_hd = $line->item_hd;
					$test = $line->deskripsi;
					$hasil = $line->nilai;
				
					$html.='
						   <tbody>
				  
								<tr class="headerrow"> 
									<td >&nbsp;' . $kd_item .  '</th>
									<td  align="center">' . $item_hd . '</td>
									<td  align="center">' . $test . '</td>
									<td align="left">&nbsp;' . $hasil . '</td>
								</tr>
							<p>&nbsp;</p>
					   ';
				}
				$html.='</tbody></table>';
				
				$html.="<h3 class='formarial' align='left'>Pascadialisa</h3>";
					$html.='<table style="overflow: wrap" class="t1" border = "1">
							<thead>
								<tr>
									<th  width="85" align="center">Kode Item</td>
									<th  width="170" align="center">Item</td>
									<th  width="170" align="center">Test</td>
									<th  align="center">Hasil</td>
								</tr>
							</thead>
							';
				foreach ($query_pascadialisa as $line) {
					$kd_item = $line->kd_item;
					$item_hd = $line->item_hd;
					$test = $line->deskripsi;
					$hasil = $line->nilai;
					$html.='
						   <tbody>
								<tr class="headerrow"> 
									<td>&nbsp;' . $kd_item .  '</th>
									<td align="center">' . $item_hd . '</td>
									<td  align="center">' . $test . '</td>
									<td align="left">&nbsp;' . $hasil . '</td>
								</tr>
							<p>&nbsp;</p>
					   ';
				}
			}
          
            $html.='</tbody></table>';
            //-----------------------TANDA TANGAN-------------------------------------------------------------
            $html.='<br><br>';
            $html.='
								<table width="" border="0" class="detail">
								<tbody>
									<tr class="headerrow"> 
										<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;</td>
										<td  align="center">' . $kota . ', ' . $tglmasuk . '</td>
									</tr>
									<tr class="headerrow">
										<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;</td>
										<td  align="center">Pemeriksa,</td>
									</tr>
									<tr>
										<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr class="headerrow"> 
										<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td>&nbsp;</td>
										<td  align="center">' . $Dokter . '</td>
									</tr>

								<p>&nbsp;</p>

							';
            $html.='</tbody></table>';
			 
       
		$prop=array('foot'=>true);
		// echo $html;
		$this->common->setPdf('P','Cetakan Hasil Pemeriksaan Hemodialisa',$html);
           
	}
}
?>