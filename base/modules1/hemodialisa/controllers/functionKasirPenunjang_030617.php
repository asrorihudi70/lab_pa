<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


 
//class main extends Controller {
class functionKasirPenunjang extends  MX_Controller {		

   public $ErrLoginMsg='';
    public function __construct(){
            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index(){
          $this->load->view('main/index',$data=array('controller'=>$this));
    }
	
	public function GetPasienTranferHD() 
	{
        $kd_pasien = $_POST['kd_pasien'];
        $namaunitasal = $_POST['namaunitasal'];
		#Variabel sebagai parameter pembeda asal pasien dari rwj/rwi/ird
		$kd_unit_asal = $this->db->query("select * from unit where nama_unit='$namaunitasal'")->row()->kd_bagian; 
		$strQuery = "	SELECT Unit.Nama_Unit as namaunit, Pasien.Nama as namapasien,
							unit_kamar.Nama_Unit||'-'||kin.nama_kamar as namaunitkamar,
							nginap.kd_spesial,
							nginap.kd_unit_kamar as kodeunitkamar,
							nginap.no_kamar,
							Pasien.ALamat, kunjungan.Kd_pasien as kdpas,
							Transaksi.Tgl_Transaksi as tgtr, Dokter.Nama as Nama_Dokter, 
							Kunjungan.*, Transaksi.NO_TRANSAKSI as notransaksi, TRANSAKSI.Kd_Kasir as kdkas, 
							TRANSAKSI.co_status, Unit.kd_kelas, Dokter.Kd_Dokter,kunjungan.kd_unit as kdunit, Transaksi.ORDERLIST, knt.Jenis_Cust
						From ((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit)
									   INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien) 
									   INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter) 
									   Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer) 
							INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit and 
										transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.Urut_masuk=kunjungan.Urut_masuk
							LEFT JOIN  nginap  On nginap.kd_Pasien = kunjungan.kd_pasien and nginap.kd_unit =kunjungan.Kd_Unit and nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.Urut_masuk=kunjungan.Urut_masuk
										and nginap.akhir=true
							LEFT JOIN unit unit_kamar on unit_kamar.kd_unit=nginap.kd_unit_kamar
							LEFT JOIN kamar_induk kin on kin.no_kamar=nginap.no_kamar
						WHERE 	Transaksi.Co_status ='false' 
								and Transaksi.tgl_co is null 
								and unit.kd_bagian = '".$kd_unit_asal."'
								and kunjungan.kd_pasien='".$kd_pasien."' 
						ORDER BY Transaksi.Tgl_Transaksi desc limit 1 "; 
		
        $query = $this->db->query($strQuery)->result();
		$string = "";
        if (count($query) > 0)
        {
		 
			foreach($query as $data)
			{
                $string .= "<>".$data->namaunit ;
				$string .= "<>".$data->namapasien;
				$string .= "<>".$data->notransaksi;
				$string .= "<>".date('Y-m-d',strtotime($data->tgtr));
				$string .= "<>".$data->kdpas;
				$string .= "<>".$data->kdunit;
				$string .= "<>".$data->kdkas;
				$string .= "<>".$data->kodeunitkamar;
				$string .= "<>".$data->namaunitkamar;
				$string .= "<>".$data->kd_spesial;
				$string .= "<>".$data->no_kamar;
				
			}
            echo $string;
		}	
        return $string; 
    }
	function getDetailTransfer(){
		$sisa = 0;
		$no_transaksi = $_POST['notr'];
		$kdks = $_POST['kdkasir']; // KD KASIR DARI UNIT HD
		//$kdks  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		$totalpembayaran = $this->db->query("select sum(harga*qty) as total from detail_transaksi where no_transaksi='".$no_transaksi."' and kd_kasir='".$kdks."'")->row()->total;
		$totalsudahbayar = $this->db->query("select sum(jumlah) as jumlah from detail_bayar where no_transaksi='".$no_transaksi."' and kd_kasir='".$kdks."'")->row()->jumlah;
		if($totalsudahbayar == '' || $totalsudahbayar == 0){
			$totalsudahbayar = 0;
		}
		$sisa = $totalpembayaran-$totalsudahbayar;
		if($sisa < 0){
			$sisa = 0;
		}
		
		echo "{totalpembayaran:".$totalpembayaran.", totalsudahbayar:".$totalsudahbayar.", sisa:".$sisa.", kdks:".$kdks."}";
	}
	public function UpdateKdCustomer()
	 {
		 
		 $KdTransaksi= $_POST['TrKodeTranskasi'];
		 $KdUnit= $_POST['KdUnit'];
		 $KdDokter= $_POST['KdDokter'];
		 $TglTransaksi= $_POST['TglTransaksi'];
		 $KdCustomer= $_POST['KDCustomer'];
		 $NoSjp= $_POST['KDNoSJP'];
		 $NoAskes= $_POST['KDNoAskes'];
		 $KdPasien = $_POST['TrKodePasien'];
		 
		 $query = $this->db->query("select updatekdcostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoAskes."','".$NoSjp."','".$KdCustomer."')");
		 $res = $query->result();
		 
		
		 
		 if($res)
		 {
			 echo "{success:true}";
		 }
		 else
		 {
			  echo "{success:false}";
		 }
	 }
	 
	 
	   public function KonsultasiPenataJasa()
   {

	 
	   
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $kdUnitAsal = $_POST['KdUnitAsal'];
	   $kdDokterAsal = $_POST['KdDokterAsal'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
	   $kdKasir = "01";
	   $kdTransaksi = $this->GetIdTransaksi();
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   
	   $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."','".$kdCostumer."','".$kdKasir."','".$kdTransaksi."','".$kdUnitAsal."','".$kdDokterAsal."',".$antrian.")");
	   $res = $query->result();
	   
	
	 
	   if($res)
	   {
		   echo '{success: true}';
	   }
	   else
	   {
		   echo '{success: false}';
	   }
   } 
   
   
     private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
    private function GetIdTransaksi()
        {
         
		   $counter = $this->db->query("select counter from kasir where kd_kasir = '01'")->row();
			$no = $counter->counter;
			$retVal2 = $no+1;
			$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kd_kasir'");
     return $retVal2;
    }
	
	
	public function savePembayaran()
	{
		// $this->db->trans_begin();
		$kdKasir = $_POST['kdKasir'];
		$notransaksi = $_POST['TrKodeTranskasi'];
		$tgltransaksi = $_POST['Tgl'];
		$shift = $_POST['Shift'];
		$flag = $_POST['Flag'];
		$tglbayar = date('Y-m-d');
		$list = $_POST['List'];
		$jmllist = $_POST['JmlList'];
		$_kdunit = $_POST['kdUnit'];
		$_Typedata = $_POST['Typedata'];
		$_kdpay=$_POST['bayar'];
		$bayar =$_POST['Totalbayar'];
		$total = str_replace('.','',$bayar); 
		$_kduser = $this->session->userdata['user_id']['id'];
		
		#1. GET URUT DETAIL_BAYAR
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$notransaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		}else{
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
                
		$a = explode("##[[]]##",$list);
		if(count($a) > 1)
		{
			for($i=0;$i<=count($a)-1;$i++){				
				$b = explode("@@##$$@@",$a[$i]);
				for($k=0;$k<=count($b)-1;$k++){			
					$_kdproduk = $b[1];
					$_qty = $b[2];
					$_harga = $b[3];
					$_urut = $b[5];
				
					if($_Typedata == 0){
						$harga = $b[6];
					}else if($_Typedata == 1){
						$harga = $b[8];	
					}else if ($_Typedata == 3){
						$harga = $b[7];	
					}
				}
				#2. GET URUT DETAIL_TR_BAYAR
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r){
					$urutanbayar = $r->geturutbayar;
				}
				
				#3. INSERT INTO DETAIL_BAYAR
				$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",
													'',".$shift.",'TRUE','".$tglbayar."',0)");
			
				/*  _QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$total.",
				 '',".$shift.",1"); */
				
				#4. INSERT INTO DETAIL_TR_BAYAR & DETAIL_TR_BAYAR_COMPONENT
				$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'".$tgltransaksi."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',
										".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();

				/* _QMS_Query("exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'".$tgltransaksi."',".$_kduser.",'".$_kdunit."','".$_kdpay."',".$harga.",'',
				 ".$shift.",1,'".$tglbayar."',".$urut_detailbayar."");
				 */
						
			}
		}else{
			$b = explode("@@##$$@@",$list);
			
			for($k=0;$k<=count($b)-1;$k++)
			{
				$_kdproduk = $b[1];
				$_qty = $b[2];
				$_harga = $b[3];
				//$_kdpay = $b[4];
				$_urut = $b[5];
				
				if($_Typedata == 0){
					$harga = $b[6];
				}
				if($_Typedata == 1){
					$harga = $b[8];	
				}
				if ($_Typedata == 3){
					$harga = $b[7];	
				}
				
				#2. GET URUT DETAIL_TR_BAYAR				
				$urut = $this->db->query("select geturutbayar('$kdKasir','".$notransaksi."','".$tgltransaksi."')")->result();
				foreach ($urut as $r)
				{
					$urutanbayar = $r->geturutbayar;
				}
			}
		
			#3. INSERT INTO DETAIL_BAYAR
			$pay_query = $this->db->query("select inserttrpembayaran('$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			".$total.",'',".$shift.",'TRUE','".$tglbayar."',0)");

			/*  _QMS_Query("exec dbo.V5_inserttrpembayaran '$kdKasir','".$notransaksi."',".$urut_detailbayar.",'".$tglbayar."',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			".$total.",'',".$shift.",1");
			 */	
			
			#4. INSERT INTO DETAIL_TR_BAYAR & DETAIL_TR_BAYAR_COMPONENT			
			$pembayaran = $this->db->query("select insertpembayaran('$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			".$harga.",'',".$shift.",'TRUE','".$tglbayar."',".$urut_detailbayar.")")->result();
			/*  _QMS_Query("exec dbo.V5_insertpembayaran '$kdKasir','".$notransaksi."',".$_urut.",'$tgltransaksi',".$_kduser.",'".$_kdunit."','".$_kdpay."',
			".$harga.",'',".$shift.",1,'".$tglbayar."',".$urut_detailbayar."");   */
		}
						
		if($pembayaran)
		{
			echo '{success:true}';	
		}else{
			echo '{success:false}';	
		}
	}
	
	public function saveDiagnosa()
	{
		
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		
	
		$a = explode("##[[]]##",$list);

		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
							if($b[$k] == 'Diagnosa Awal')
							{
								$diagnosa = 0;
							}
							else if($b[$k] == 'Diagnosa Utama')
							{
								$diagnosa = 1;
							}
				
							else if($b[$k] == 'Komplikasi')
							{
								$diagnosa = 2;
							}
							else if($b[$k] == 'Diagnosa Sekunder')
							{
								$diagnosa = 3;
							}
							else if($b[$k] == 'Baru')
							{
								$kasus = 'TRUE';
								$zkasus = 1;
							}
							else if($b[$k] == 'Lama')
							{
								$kasus = 'FALSE';
								$zkasus = 0;
							}
			}
				$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
				$result = $urut->result();
				foreach ($result as $data)
				{
					$Urutan = $data->geturutmrpenyakit;
				}
			
				$query = $this->db->query("select insertdatapenyakit('".$b[1]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
				
			
		
		}
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}
	}
        
    public function deletedetail_bayar()
	{
		$_kduser = $this->session->userdata['user_id']['id'];
		$kdKasir = $_POST['KDkasir'];	
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$Urut =$_POST['Urut'];
		$TmpTglBayar = explode(' ',$_POST['TrTglbayar']);
		$TglBayar = $TmpTglBayar[0];
		$Kodepay=$_POST['Kodepay'];
		$KodeUnit=$_POST['KodeUnit'];
		$NamaPasien=$_POST['NamaPasien'];
		$Namaunit=$_POST['Namaunit'];
		$Tgltransaksi=explode(' ',$_POST['Tgltransaksi']);
		$Kodepasein=$_POST['Kodepasein'];
		$Uraian=$_POST['Uraian'];
		$keterangan=$_POST['keterangan'];
		$jumlahbayar=str_replace('.','',$_POST['Jumlah']);
		$this->db->trans_begin();
		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
								inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
									and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
								inner join zusers zu on zu.kd_user=t.kd_user
							where kd_kasir='".$kdKasir."' 
								and no_transaksi='".$TrKodeTranskasi."' 
								and tgl_transaksi='".$_POST['Tgltransaksi']."'
							")->row();	
		$param_insert_history_detail_bayar = array(
											'kd_kasir'		=>$kdKasir,
											'no_transaksi'	=>$TrKodeTranskasi,
											'tgl_transaksi'	=>$_POST['Tgltransaksi'],
											'kd_pasien'		=>$Kodepasein,
											'nama'			=>$NamaPasien,
											'kd_unit'		=>$KodeUnit,
											'nama_unit'		=>$Namaunit,
											'kd_pay'		=>$Kodepay,
											'uraian'		=>$Uraian,
											'kd_user'		=>$res->kd_user,
											'kd_user_del'	=>$_kduser,
											'shift'			=>$res->shift,
											'shiftdel'		=>$this->db->query("select shift from bagian_shift where kd_bagian='72'")->row()->shift,
											'user_name'		=>$res->user_names,
											'jumlah'		=>$jumlahbayar,
											'tgl_batal'		=>date('Y-m-d'),
											'ket'			=>$keterangan,
										);
		$insert		= $this->db->insert("history_detail_bayar",$param_insert_history_detail_bayar);	
		if( $_POST['Uraian']==='transfer' || $_POST['Uraian']==='TRANSFER'){
				$queryselect_tranfer=$this->db->query("select * from transfer_bayar where kd_kasir='$kdKasir'
				and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."' ")->result();
				_QMS_Query("select * from transfer_bayar where kd_kasir='$kdKasir'
				and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."' "); 
				if(count($queryselect_tranfer)===1)
				{
					for($x=0;  $x<count($queryselect_tranfer); $x++)
					{
						$kdkasirtujuan=$queryselect_tranfer[$x]->det_kd_kasir;
						$no_transaksitujuan=$queryselect_tranfer[$x]->det_no_transaksi;
						$uruttujuan=$queryselect_tranfer[$x]->det_urut;
						$tgl_transaksitujuan=$queryselect_tranfer[$x]->det_tgl_transaksi;
					}
					$Querydelete_tujuan=$this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
						and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'");
						_QMS_Query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
						and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'"); 
					if($Querydelete_tujuan){
						$Querydelete_tujuan=$this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
						and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
						_QMS_Query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
						and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'"); 
						if($Querydelete_tujuan){
								$query=$this->db->query(
								"select hapusdetailbayar('$kdKasir','".$TrKodeTranskasi."',".$Urut.",'".$TglBayar."')");
								_QMS_Query("exec dbo.V5_hapus_detail_bayar '$kdKasir','".$TrKodeTranskasi."',".$Urut.",'".$TglBayar."'");
							if($query){
								$this->db->trans_commit();
								echo "{success:true}";
							}else{
								$this->db->trans_rollback();
								echo "{success:false}";
							}
						}else{	
							$this->db->trans_rollback();
								echo "{success:false}";
						}
					}else{
						$this->db->trans_rollback();
						echo "{success:false}";
					}
				}else{
					$this->db->trans_rollback();
					echo "{success:false}";
				}
		}else{	
			$query=$this->db->query("select hapusdetailbayar('$kdKasir','".$TrKodeTranskasi."',".$Urut.",'".$TglBayar."')");
			_QMS_Query("exec dbo.V5_hapus_detail_bayar '$kdKasir','".$TrKodeTranskasi."',".$Urut.",'".$TglBayar."'");
			if($query){
				$this->db->trans_commit();
				echo "{success:true}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
									
		}
	}
	
public function savedetailpenyakit()
	{
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$Tgl =date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		//$urut_masuk = $_POST['UrutMasuk'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=0;$k<=count($b)-1;$k++)
			{
			//echo($b[$k]);
			
			}
				$urut = $this->db->query("select geturutdetailtransaksi('01','".$TrKodeTranskasi."','".$Tgl."') ");
				$result = $urut->result();
				foreach ($result as $data)
				{ 
				if($b[6]==0 || $b[6]=='' )
				{
					$Urutan = $data->geturutdetailtransaksi;
				}else{
					$Urutan=$b[6];
				}
				//echo($Urutan);
				}
				//echo();
			//(   charge,adjust,folio,qty,harga,shift,tag)
				$query = $this->db->query("select insert_detail_transaksi
				('01',
				'".$TrKodeTranskasi."',
				".$Urutan.",
				'".$Tgl."',
				 0,
				'".$b[5]."',
				".$b[1].",
				'".$KdUnit."',
				'".$b[3]."',
				'false',
				'true',
				'',
				".$b[2].",
				".$b[4].",
				".$Shift.",
				'false'
				
				)
				");	
				
				
		

		}
				
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}
			
	} 
	
	public function ubah_co_status_transksi ()
	{
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KDkasir=$_POST['KDkasir'];
		$query = $this->db->query(" update transaksi set co_status='true' ,  tgl_co='".date("Y-m-d")."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='$KDkasir'");	
		

		
		if($query)
		{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}
	 
	}
	public function GetRecordPenyakit($kd,$nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%".$kd."%' OR penyakit ilike '%".$nama."%' ");
		$count = $query->num_rows();
		return $count;
	}
		
    public function saveTransfer()
	{	
		$KASIR_SYS_WI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$Tglasal=$_POST['Tglasal'];
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$Kdcustomer=$_POST['Kdcustomer'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$_POST['Shift'];
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$_POST['KasirRWI'];
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$tglhariini=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$this->db->trans_begin();
				
		$det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		}
	
		#1. INSERT DETAIL BAYAR
		$pay_query = $this->db->query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
					values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'A',$Shift1,'true')");	
			
		if($pay_query )
		{
			#2. INSERT DETAIL_TR_BAYAR
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
				
			if($detailTrbayar)
			{	
				#3. UPDATE STATUS LUNAS
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				
				if($statuspembayaran)
				{
					#4. INSERT DETAIL_TR_BAYAR_COMPONENT
					$detailtrcomponet = $this->db->query("
						insert into detail_tr_bayar_component
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select 	'$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
								'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
							FROM Detail_Component dc
								INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
								INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
									and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasirIGD' AND dc.No_Transaksi ='$TrKodeTranskasi' ORDER BY dc.Kd_Component");	
								
					if($detailtrcomponet )
					{	
						#5. GET URUT DI DETAIL TRANSAKSI
						$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1";
						$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
						if(pg_num_rows($resulthasilurut) <= 0)
						{
							$uruttujuan=1;
						}else{
							while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
							{
								$uruttujuan = $line['urutan'];
							}
						}
						
						#6. GET TARIF CUSTOMER
						$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
						
						#7. GET KD_PRODUK
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
						
						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
						
						
						#8. GET TGL_BERLAKU						
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();
						
						

						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlakuunit;
							
						}
						
						if($tanggalberlaku==''){
							echo "{success:false,message:'Produk Transfer Belum tersedia, Hubungi IPDE.'}";	
							exit;
						}
											
						
						#9. INSERT DETAIL_TRANSAKSI TUJUAN
						$detailtransaksitujuan = $this->db->query
						("	INSERT INTO detail_transaksi
									(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
									tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
									'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
									$total,$Shift1,'false','$TrKodeTranskasi')
						");	
						
						if($detailtransaksitujuan )	
						{
							#10. INSERT DETAIL COMPONENT TUJUAN
							$detailcomponentujuan = $this->db->query
							("	INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
									select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
										from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
											and Kd_Kasir = '$KDkasirIGD' group by kd_component order by kd_component");	
											  
							if($detailcomponentujuan )
							{ 	
								#11. INSERT TRANSFER BAYAR
								$tranferbyr = $this->db->query("
									INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
																det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
											values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
													'$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								
								
								if($tranferbyr){
									if ( $KASIR_SYS_WI==$KASIRRWI){
										$trkamar = $this->db->query("
											INSERT INTO detail_tr_kamar VALUES
												('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."',
													'".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
										if($trkamar ){
											$this->db->trans_commit();
											echo '{success:true}';
										}else{
											$this->db->trans_rollback();
											echo '{success:false, pesan:"detail_tr_kamar"}';	
										}
									}else{
										$this->db->trans_commit();
										echo '{success:true}';
									}
								}else{ 
										$this->db->trans_rollback();
										echo '{success:false, pesan:"transfer_bayar"}';	
								}
							}else{
								$this->db->trans_rollback();
								echo '{success:false,pesan:"detail_component tujuan"}';	
							}
						} else {
							$this->db->trans_rollback();
							echo '{success:false,, pesan:"detail_transaksi tujuan"}';	
						}
					} else {
						$this->db->trans_rollback();
						echo '{success:false,, pesan:"detail_tr_bayar_component"}';	
					}

				} else {
					$this->db->trans_rollback();
					echo '{success:false,pesan:"update status transaksi"}';	
				}
			} else {
				$this->db->trans_rollback();
				echo '{success:false, pesan:"detail_tr_bayar"}';	
			}
		} else {
			$this->db->trans_rollback();
			echo '{success:false,pesan:"detail_bayar"}';	
			
		}
	}
	
	public function saveTransferHD()
	{	
		$namaunitasal = $_POST['namaunitasal'];
		$kd_bagian = $this->db->query("select * from unit where nama_unit='$namaunitasal'")->row()->kd_bagian; 
		$KASIR_SYS_RWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		if ($kd_bagian == 1){
			$KASIR_SYS=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		}else if ($kd_bagian == 2){
			$KASIR_SYS=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		}else if ($kd_bagian == 3){
			$KASIR_SYS=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		}
		$KDkasir=$_POST['KDkasir'];
		$Tglasal=$_POST['Tglasal'];
        $KDunittujuan=$_POST['KDunittujuan'];
		$Kdcustomer=$_POST['Kdcustomer'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;//$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$_POST['Shift'];
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KDKasirAsal=$_POST['KDKasirAsal'];
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$tglhariini=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$this->db->trans_begin();
				
		$det_query = "select COALESCE(urut+1,1) as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urut desc limit 1";
		$resulthasil = pg_query($det_query) or die('Query failed: ' . pg_last_error());
		if(pg_num_rows($resulthasil) <= 0)
		{
			$urut_detailbayar=1;
		}else{
			while ($line = pg_fetch_array($resulthasil, null, PGSQL_ASSOC)) 
			{
				$urut_detailbayar = $line['urutan'];
			}
		}
	
		#1. INSERT DETAIL BAYAR
		$pay_query = $this->db->query(" insert into detail_bayar 
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
					values ('$KDkasir','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'A',$Shift1,'true')");	
		
		$pay_query_SQL= _QMS_Query(" insert into detail_bayar 
							(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
							values ('$KDkasir','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'A',$Shift1,1)");						
		if($pay_query && $pay_query_SQL )
		{
			#2. INSERT DETAIL_TR_BAYAR
			$detailTrbayar = $this->db->query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasir' AND NO_TRANSAKSI='$TrKodeTranskasi'");
			$detailTrbayar_SQL = _QMS_Query("	insert into detail_tr_bayar 
				(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasir' AND NO_TRANSAKSI='$TrKodeTranskasi'");		
			if($detailTrbayar && $detailTrbayar_SQL)
			{	
				#3. UPDATE STATUS LUNAS
				$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasir','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
				$sql="EXEC dbo.updatestatustransaksi '$KDkasir','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer'";
				$statuspembayaran_sql=_QMS_Query($sql);
				if($statuspembayaran)
				{
					#4. INSERT DETAIL_TR_BAYAR_COMPONENT
					$detailtrcomponet = $this->db->query("
						insert into detail_tr_bayar_component
								(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select 	'$KDkasir','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
								'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
							FROM Detail_Component dc
								INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
								INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
									and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasir' AND dc.No_Transaksi ='$TrKodeTranskasi' ORDER BY dc.Kd_Component");	
					$detailtrcomponet_SQL = _QMS_Query("
						insert into detail_tr_bayar_component
							(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
						Select 	'$KDkasir','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
								'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
							FROM Detail_Component dc
								INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
								INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
									and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
						WHERE dc.Kd_Kasir = '$KDkasir' AND dc.No_Transaksi ='$TrKodeTranskasi' ORDER BY dc.Kd_Component");	
					if($detailtrcomponet && $detailtrcomponet_SQL )
					{	
						#5. GET URUT DI DETAIL TRANSAKSI
						$urutquery ="select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1";
						$resulthasilurut = pg_query($urutquery) or die('Query failed: ' .pg_last_error());
						if(pg_num_rows($resulthasilurut) <= 0)
						{
							$uruttujuan=1;
						}else{
							while ($line = pg_fetch_array($resulthasilurut, null, PGSQL_ASSOC)) 
							{
								$uruttujuan = $line['urutan'];
							}
						}
						
						#6. GET TARIF CUSTOMER
						$getkdtarifcus=$this->db->query("select getkdtarifcus('$Kdcustomer')")->result();
						foreach($getkdtarifcus as $xkdtarifcus)
						{
							$kdtarifcus = $xkdtarifcus->getkdtarifcus;
						}
						
						#7. GET KD_PRODUK
						$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
						FROM Produk_Charge pc 
						INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
						WHERE  left(kd_unit,length(kd_unit))='$KdUnit'  order by pc.kd_unit asc limit 1")->result();
						
						foreach($getkdproduk as $det1)
						{
							$kdproduktranfer = $det1->kdproduk;
							$kdUnittranfer = $det1->unitproduk;
						}
						
						
						#8. GET TGL_BERLAKU	POSTGRE					
						$gettanggalberlaku=$this->db->query("SELECT gettanggalberlakuunit
						('$kdtarifcus','$tgltransfer','$tglhariini',$kdproduktranfer,'$kdUnittranfer')")->result();
						
						foreach($gettanggalberlaku as $detx)
						{
							$tanggalberlaku = $detx->gettanggalberlakuunit;
							
						}
						
						# GET TGL_BERLAKU	SQL	
						$gettanggalberlaku_SQL=_QMS_Query("
						select distinct top 1  tgl_berlaku 
							from Tarif inner join 
							(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
							on tarif.kd_produk =produk.kd_produk 
							where kd_tarif = '$kdtarifcus'
							and tgl_berlaku <='$tgltransfer'
							and (tgl_berakhir >='$tglhariini'
							or tgl_berakhir is null)
							and kd_unit='$kdUnittranfer'
							and produk.kd_produk='$kdproduktranfer' order by Tgl_Berlaku desc ")->result();
						foreach($gettanggalberlaku_SQL as $detx)
						{
							$tanggalberlaku_SQL = $detx->tgl_berlaku;
							
						}
						
						if($tanggalberlaku=='' && $tanggalberlaku_SQL==''){
							echo "{success:false,message:'cek tanggal berlaku produk!',produk:'$kdproduktranfer',tgl_berlaku:'$tanggalberlaku'}";	
							exit;
						}
						
						#9. INSERT DETAIL_TRANSAKSI TUJUAN
						
						$cek_unit = $KDunittujuan[0];
						// echo $cek_unit;
						#kondisi untuk unit tujuan rawat inap
						if($cek_unit == 1 ){
							$q_insert_detail="	INSERT INTO detail_transaksi
										(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
										tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur, kd_unit_tr)
								VALUES('$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
										'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','E',1,
										$total,$Shift1,'false','$TrKodeTranskasi', '$KDunittujuan')";
							$detailtransaksitujuan = $this->db->query($q_insert_detail);
							$detailtransaksitujuan_SQL = _QMS_Query
								("INSERT INTO detail_transaksi
										(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
										tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur, kd_unit_tr)
									VALUES('$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
										'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,1,'E',1,
										$total,$Shift1,0,'$TrKodeTranskasi','$KDunittujuan')
								");	
							// echo "inap";
						}
						else{
							// echo "bukan inap";
							$detailtransaksitujuan = $this->db->query
							("	INSERT INTO detail_transaksi
										(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
										tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
								VALUES('$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
										'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku','true','true','',1,
										$total,$Shift1,'false','$TrKodeTranskasi')
							");	
							
							$detailtransaksitujuan_SQL = _QMS_Query
							("INSERT INTO detail_transaksi
									(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
									tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur, kd_unit_tr)
								VALUES('$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
									'$_kduser', '$kdtarifcus','$kdproduktranfer','$kdUnittranfer','$tanggalberlaku_SQL',1,1,'',1,
									$total,$Shift1,0,'$TrKodeTranskasi','$KDunittujuan')
							");	
						}
						
						if($detailtransaksitujuan && $detailtransaksitujuan_SQL)	
						{
							#10. INSERT DETAIL COMPONENT TUJUAN
							$detailcomponentujuan = $this->db->query
							("	INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
									select '$KDKasirAsal','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
										from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
											and Kd_Kasir = '$KDkasir' group by kd_component order by kd_component");	
							$detailcomponentujuan_SQL = _QMS_Query
							("	INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
									select '$KDKasirAsal','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,sum(jumlah) as jumlah,0
										from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi'
										and Kd_Kasir = '$KDkasir' group by kd_component order by kd_component");							  
									  
							if($detailcomponentujuan && $detailcomponentujuan_SQL)
							{ 	
								#11. INSERT TRANSFER BAYAR
								$tranferbyr = $this->db->query("
									INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
																det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
											values ('$KDkasir','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
													'$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								$tranferbyr_SQL = _QMS_Query("
									INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
																det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan)
											values ('$KDkasir','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
													'$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");
								
								if($tranferbyr){
									if ( $KASIR_SYS==$KASIR_SYS_RWI){
										$trkamar = $this->db->query("
											INSERT INTO detail_tr_kamar VALUES
												('$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."',
													'".$_POST['nokamar']."','".$_POST['kdspesial']."')");
										$trkamar_SQL = _QMS_Query("
											INSERT INTO detail_tr_kamar VALUES
												('$KDKasirAsal', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','".$_POST['kodeunitkamar']."',
													'".$_POST['nokamar']."','".$_POST['kdspesial']."')");	
																	
										if($trkamar && $trkamar_SQL ){
											$this->db->trans_commit();
											echo "{success:true, no_transaksi:'$TrKodeTranskasi'}";
										}else{
											$this->db->trans_rollback();
											echo '{success:false, pesan:"detail_tr_kamar"}';	
										}
									}else{
										$this->db->trans_commit();
										echo "{success:true, no_transaksi:'$TrKodeTranskasi'}";
									}
								}else{ 
										$this->db->trans_rollback();
										echo '{success:false, pesan:"transfer_bayar"}';	
								}
							}else{
								$this->db->trans_rollback();
								echo '{success:false,pesan:"detail_component tujuan"}';	
							}
						} else {
							$this->db->trans_rollback();
							echo '{success:false, pesan:"detail_transaksi tujuan"}';	
						}
					} else {
						$this->db->trans_rollback();
						echo '{success:false, pesan:"detail_tr_bayar_component"}';	
					}

				} else {
					$this->db->trans_rollback();
					echo '{success:false,pesan:"update status transaksi"}';	
				}
			} else {
				$this->db->trans_rollback();
				echo '{success:false, pesan:"detail_tr_bayar"}';	
			}
		} else {
			$this->db->trans_rollback();
			echo '{success:false,pesan:"detail_bayar"}';	
			
		}
	}
	public function getKdPayTransfer(){
		$kd_pay=$this->db->query("select setting from sys_setting where key_data='sys_kd_pay_transfer'")->row()->setting;
		echo "{success:true, kdpay:'$kd_pay'}";
	}
	
	public function GetPasienTranfer() 
	{
		$kdtr = $_POST['notr'];
		$kdkasir = $_POST['kdkasir'];
		$strQuery = "SELECT Unit.Nama_Unit as namaunit, Pasien.Nama as namapasien, Pasien.ALamat, kunjungan.Kd_pasien as kdpas, Transaksi.Tgl_Transaksi as tgtr, Dokter.Nama as Nama_Dokter, Kunjungan.*, 
					Transaksi.NO_TRANSAKSI as notransaksi, TRANSAKSI.Kd_Kasir as kdkas, TRANSAKSI.co_status, Unit.kd_kelas, Dokter.Kd_Dokter, kunjungan.kd_unit as kdunit,
					unit_kamar.Nama_Unit||'-'||kin.nama_kamar as namaunitkamar,
										ngin.kd_spesial,
										ngin.kd_unit_kamar as kodeunitkamar,
										ngin.no_kamar,
					Transaksi.ORDERLIST, knt.Jenis_Cust
				From ((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit)
						INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien) 
						INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter)
						Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer)
					INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit 
						and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.Urut_masuk=kunjungan.Urut_masuk 
					left join nginap ngin on  ngin.kd_Pasien = kunjungan.kd_pasien and ngin.kd_unit =kunjungan.Kd_Unit and ngin.tgl_masuk=kunjungan.tgl_masuk and ngin.Urut_masuk=kunjungan.Urut_masuk and akhir=true
					left join unit unit_kamar on unit_kamar.kd_unit=ngin.kd_unit_kamar
					left join kamar_induk kin on kin.no_kamar=ngin.no_kamar
				where Transaksi.Co_status ='false' 
					and Transaksi.kd_kasir = '$kdkasir' 
					and Transaksi.no_transaksi='$kdtr' 
					and Transaksi.lunas='false'
				order by Transaksi.Tgl_Transaksi desc limit 1 ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
		
		$string = "";
		if ($query->num_rows() > 0)
		{
			//$string = "";
			foreach($res as $data)
			{
                $string .= "<>".$data->namaunit ;
				$string .= "<>".$data->namapasien;
				$string .= "<>".$data->notransaksi;
				$string .= "<>".$data->tgtr;
				$string .= "<>".$data->kdpas;
				$string .= "<>".$data->kdunit;
				$string .= "<>".$data->kdkas;
				$string .= "<>".$data->kodeunitkamar;
				$string .= "<>".$data->namaunitkamar;
				$string .= "<>".$data->kd_spesial;
				$string .= "<>".$data->no_kamar;
            }
            echo $string;
		}	
		return $string;
    }
	
	public function GettotalTranfer() {
        $kdtr = $_POST['notr'];
		/* if ($_POST['kd_kasir']=='igd')
		{
			$kdkasir  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_igd'")->row()->setting;
		}else if ($_POST['kd_kasir']=='rwj')
		{
			$kdkasir  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		}else if ($_POST['kd_kasir']=='rwi')
		{
			$kdkasir  = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		} */
		$kdks = $_POST['kd_kasir'];
        $strQuery = "select sum(harga*qty) as total from detail_transaksi where no_transaksi='".$kdtr."' and kd_kasir='".$kdks."'";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string = $data->total ;
				
             }
         echo $string;
		 }	
         return $string;
    }
	
	public function getPrinter(){	
		$o = shell_exec("lpstat -d -p");
		$res = explode("\n", $o);
		$i = 0;
		foreach ($res as $r) {
			$active = 0;
			if (strpos($r, "printer") !== FALSE) {
				$r = str_replace("printer ", "", $r);
				if (strpos($r, "is idle") !== FALSE)
					$active = 1;

				$r = explode(" ", $r);

				$printers[$i]['name'] = $r[0];
				$printers[$i]['active'] = $active;
				$i++;
			}
		}
		//var_dump($printers);
		//var_dump('{success:true, totalrecords:'.count($printers).', ListDataObj:'.json_encode($printers).'}') ;
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$printers;
		echo json_encode($jsonResult);	
	}
}

?>