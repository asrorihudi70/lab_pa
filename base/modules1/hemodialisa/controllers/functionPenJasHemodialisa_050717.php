<?php
/**

 * @author Ali
 * Editing by M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPenJasHemodialisa extends  MX_Controller {

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
			 // $this->load->database('otherdb2',TRUE);
	}

	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}
	
	public function getDokterDialisa(){
		$kd_dokter_dialisa = $this->db->query("select * from sys_setting where key_data='hd_default_kd_dokter'")->row()->setting;
		$result=$this->db->query("select kd_dokter,nama from dokter where kd_dokter='$kd_dokter_dialisa'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	public function getProduk(){	
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		
		$kd_unit_asal = $_POST['kd_unit_asal'];
		$kd_customer = $_POST['kd_customer'];
		
		#KD_UNIT diambil dari fungsi gettarifmir yang akan digunakan sebagai parameter pengambilan produk dan tarif
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'hd_kd_produk_hd'")->row()->setting;
		if($kd_unit_asal == '251'){
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'hd_kd_produk_hd_pav'")->row()->setting;
		}
		$kdUnit = $this->db->query("select gettarifmir('$kd_unit_asal', '$kd_customer', '$kdklasproduk')")->row()->gettarifmir; 
		// $kdUnit = $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
		$query="select * from (
								SELECT PRODUK.DESKRIPSI,produk.kp_produk,PRODUK.kd_klas, TARIF.TARIF as harga, TARIF.KD_PRODUK, TARIF.KD_UNIT ,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty
								FROM PRODUK 
									INNER JOIN TARIF ON PRODUK.KD_PRODUK = TARIF.KD_PRODUK 
								where tarif.kd_unit = '72' and tarif.kd_Tarif= '".$row->kd_tarif."'  
									and tarif.tgl_berlaku =  (select max(trf.tgl_berlaku) 
												  from tarif trf where trf.tgl_berlaku <=('". date('Y-m-d') ."')   
													and (tgl_berakhir>=('". date('Y-m-d') ."') or Tgl_berakhir is null) and trf.kd_produk=tarif.kd_produk and trf.kd_unit=tarif.kd_Unit and trf.kd_tarif=tarif.kd_tarif) 
									and PRODUK.kd_produk='1' 
								Union 
								SELECT PRODUK.DESKRIPSI,produk.kp_produk,PRODUK.kd_klas, TARIF.TARIF as harga, TARIF.KD_PRODUK, TARIF.KD_UNIT ,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty
								FROM PRODUK 
									INNER JOIN TARIF ON PRODUK.KD_PRODUK = TARIF.KD_PRODUK 
								where tarif.kd_unit = '".$kdUnit."' and tarif.kd_Tarif= '".$row->kd_tarif."'  
									and tarif.tgl_berlaku =  (select max(trf.tgl_berlaku) 
												  from tarif trf where trf.tgl_berlaku <=('". date('Y-m-d') ."')   
													and (tgl_berakhir>=('". date('Y-m-d') ."') or Tgl_berakhir is null) and trf.kd_produk=tarif.kd_produk and trf.kd_unit=tarif.kd_Unit and trf.kd_tarif=tarif.kd_tarif) 
									and LEFT(produk.kd_klas,2) = '".$kdklasproduk."'  
								Union 
								Select Produk.Deskripsi,produk.kp_produk,PRODUK.KD_KLAS,tarif.Tarif as harga,TARIF.KD_PRODUK,tarif.kd_unit,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty   
								from produk 
									inner join (Tarif inner join unit on unit.kd_unit=tarif.kd_unit) on Tarif.kd_produk=produk.kd_produk  
								Where Unit.kd_unit='".$kdUnit."' and tarif.kd_tarif= '".$row->kd_tarif."' and produk.kd_klas = '".$kdklasproduk."'  
									and tarif.tgl_berlaku =  (select max(trf.tgl_berlaku) 
												  from tarif trf where trf.tgl_berlaku <=('". date('Y-m-d') ."')    
													and trf.kd_produk=tarif.kd_produk and trf.kd_unit=tarif.kd_Unit and trf.kd_tarif=tarif.kd_tarif) 
									and tarif.Kd_Produk not in (Select Kd_Produk From Tarif  Where left(Unit.kd_unit,1)='".$kdUnit."' and tarif.tgl_berlaku<= ('". date('Y-m-d') ."')    ) 
								--Union 
								--Select produk.Deskripsi,produk.kp_produk,PRODUK.KD_KLAS,tarif.Tarif as harga,TARIF.KD_PRODUK,tarif.kd_unit,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty    
								--from produk 
								--	inner join ( Tarif inner join unit on unit.kd_unit=tarif.kd_unit  ) on Tarif.kd_produk=produk.kd_produk 
								--Where Unit.kd_unit='".$kdUnit."' and tarif.kd_tarif= '".$row->kd_tarif."' and produk.kd_klas= '".$kdklasproduk."' and tarif.tgl_berlaku <= ('". date('Y-m-d') ."')   
								--Union
								--SELECT PRODUK.DESKRIPSI,produk.kp_produk,PRODUK.kd_klas, TARIF.TARIF as harga, TARIF.KD_PRODUK, TARIF.KD_UNIT ,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty
								--FROM PRODUK 
								--	INNER JOIN TARIF ON PRODUK.KD_PRODUK = TARIF.KD_PRODUK 
								--where left(Tarif.Kd_unit,7) in ('".$kdUnit."') And tarif.kd_Tarif= '".$row->kd_tarif."' and tarif.tgl_berlaku <= ('". date('Y-m-d') ."')
								--and (tarif.tgl_bERAKHIR >=('". date('Y-m-d') ."') or tarif.tgl_berakhir is null) and produk.kd_klas = '".$kdklasproduk."'  
								
								) as data
								where lower(data.deskripsi)= lower('".$_POST['text']."') or lower(data.kp_produk::character varying) = lower('".$_POST['text']."')
							order by data.kd_produk,data.deskripsi,data.tgl_Berlaku,data.kd_unit asc limit 10 ";
		$result=$this->db->query($query)->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function getProduk_kasir(){	
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'hd_kd_produk_hd'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
	 	$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		
		
		$kd_unit_asal = $this->db->query("select * from unit WHERE nama_unit='".$_POST['nama_unit_asal']."'")->row()->kd_unit;
		$kd_customer = $_POST['kd_customer'];
		$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'hd_kd_produk_hd'")->row()->setting;
		if($kd_unit_asal == '251'){
			$kdklasproduk  = $this->db->query("select setting from sys_setting where key_data = 'hd_kd_produk_hd_pav'")->row()->setting;
		}
		#KD_UNIT diambil dari fungsi gettarifmir yang akan digunakan sebagai parameter pengambilan produk dan tarif
		$kdUnit = $this->db->query("select gettarifmir('$kd_unit_asal', '$kd_customer', '$kdklasproduk')")->row()->gettarifmir; 
		// $kdUnit = $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
		$query="select * from (
								SELECT PRODUK.DESKRIPSI,produk.kp_produk,PRODUK.kd_klas, TARIF.TARIF as harga, TARIF.KD_PRODUK, TARIF.KD_UNIT ,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty
								FROM PRODUK 
									INNER JOIN TARIF ON PRODUK.KD_PRODUK = TARIF.KD_PRODUK 
								where tarif.kd_unit = '72' and tarif.kd_Tarif= '".$row->kd_tarif."'  
									and tarif.tgl_berlaku =  (select max(trf.tgl_berlaku) 
												  from tarif trf where trf.tgl_berlaku <=('". date('Y-m-d') ."')   
													and (tgl_berakhir>=('". date('Y-m-d') ."') or Tgl_berakhir is null) and trf.kd_produk=tarif.kd_produk and trf.kd_unit=tarif.kd_Unit and trf.kd_tarif=tarif.kd_tarif) 
									and PRODUK.kd_produk='1' 
								Union 
								SELECT PRODUK.DESKRIPSI,produk.kp_produk,PRODUK.kd_klas, TARIF.TARIF as harga, TARIF.KD_PRODUK, TARIF.KD_UNIT ,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty
								FROM PRODUK 
									INNER JOIN TARIF ON PRODUK.KD_PRODUK = TARIF.KD_PRODUK 
								where tarif.kd_unit = '".$kdUnit."' and tarif.kd_Tarif= '".$row->kd_tarif."'  
									and tarif.tgl_berlaku =  (select max(trf.tgl_berlaku) 
												  from tarif trf where trf.tgl_berlaku <=('". date('Y-m-d') ."')   
													and (tgl_berakhir>=('". date('Y-m-d') ."') or Tgl_berakhir is null) and trf.kd_produk=tarif.kd_produk and trf.kd_unit=tarif.kd_Unit and trf.kd_tarif=tarif.kd_tarif) 
									and LEFT(produk.kd_klas,2) = '".$kdklasproduk."'  
								Union 
								Select Produk.Deskripsi,produk.kp_produk,PRODUK.KD_KLAS,tarif.Tarif as harga,TARIF.KD_PRODUK,tarif.kd_unit,tarif.tgl_berlaku,produk.kd_kat, tarif.kd_tarif, 1 as qty   
								from produk 
									inner join (Tarif inner join unit on unit.kd_unit=tarif.kd_unit) on Tarif.kd_produk=produk.kd_produk  
								Where Unit.kd_unit='".$kdUnit."' and tarif.kd_tarif= '".$row->kd_tarif."' and produk.kd_klas = '".$kdklasproduk."'  
									and tarif.tgl_berlaku =  (select max(trf.tgl_berlaku) 
												  from tarif trf where trf.tgl_berlaku <=('". date('Y-m-d') ."')    
													and trf.kd_produk=tarif.kd_produk and trf.kd_unit=tarif.kd_Unit and trf.kd_tarif=tarif.kd_tarif) 
									and tarif.Kd_Produk not in (Select Kd_Produk From Tarif  Where left(Unit.kd_unit,1)='".$kdUnit."' and tarif.tgl_berlaku<= ('". date('Y-m-d') ."')    ) 
								
								) as data
								where lower(data.deskripsi) = lower('".$_POST['text']."') or lower(data.kp_produk::character varying) = lower('".$_POST['text']."')
							order by data.kd_produk,data.deskripsi,data.tgl_Berlaku,data.kd_unit asc limit 10 ";
		$result=$this->db->query($query)->result();
		
		$array_produk= array();
		for($i=0; $i<count($result);$i++){
			$array_produk[$i]['DESKRIPSI'] = $result[$i]->deskripsi;
			$array_produk[$i]['KP_PRODUK'] = $result[$i]->kp_produk;
			$array_produk[$i]['KD_KLAS'] = $result[$i]->kd_klas;
			$array_produk[$i]['HARGA'] = $result[$i]->harga;
			$array_produk[$i]['KD_PRODUK'] = $result[$i]->kd_produk;
			$array_produk[$i]['TGL_BERLAKU'] = $result[$i]->tgl_berlaku;
			$array_produk[$i]['KD_KAT'] = $result[$i]->kd_kat;
			$array_produk[$i]['KD_TARIF'] = $result[$i]->kd_tarif;
			$array_produk[$i]['KD_UNIT'] = $result[$i]->kd_unit;
			$array_produk[$i]['QTY'] = $result[$i]->qty;
		}
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$array_produk;
		echo json_encode($jsonResult);
		// echo '{success:true, totalrecords:'.count($array_produk).', ListDataObj:'.json_encode($array_produk).'}' ; 
		// echo '{success:true, input:'.$kd_unit_asal.'}' ; 
		
	}
	public function update_dokter()
	{
	$this->db->trans_begin();
	$result=$this->db->query("update kunjungan set kd_dokter='".$_POST['KdDokter']."', kd_customer='".$_POST['TmpCustoLama']."'
	where kd_pasien = '".$_POST['KdPasien']."' AND kd_unit = '".$_POST['KdUnit']."' AND tgl_masuk = '".$_POST['TglTransaksiAsal']."' AND urut_masuk=".$_POST['urutmasuk']." ");
	if($result)
	{	
	
		$result2=$this->db->query("update detail_trdokter set kd_dokter='".$_POST['KdDokter']."'
		where kd_kasir = '".$_POST['KdKasirAsal']."' AND no_transaksi = '".$_POST['KdTransaksi']."'");
		if($result2)
			{	
					$this->db->trans_commit();
					echo "{success:true, notrans:'".$_POST['KdTransaksi']."', kdPasien:'".$_POST['KdPasien']."'}";
			}else{
			
			$this->db->trans_rollback();
			echo "{success:false}";
			}
	} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}

	
	}
	
	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter)
	{

		$result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='".$KdUnit."'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='".$KdUnit."'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=1;
		}
		
		return $urut;
	}

   private function GetIdTransaksi($kd_kasir)
	{
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
		$no = $counter->counter;
		$retVal = $no+1;
		$query = "update kasir set counter=$retVal,lastdate='".date('Y-m-d')."' where kd_kasir='$kd_kasir'";
		$update = $this->db->query($query);
		
		$counter_sql = _QMS_Query("select counter from kasir where kd_kasir = '$kd_kasir' ")->row();
		$no_sql = $counter_sql->counter;
		$retVal_sql = $no_sql+1;
		$query_sql = _QMS_Query("update kasir set counter=$retVal_sql,lastdate='".date('Y-m-d')."' where kd_kasir='$kd_kasir' ");
		 
		return $retVal;
	}
	
	public function GetKodeAsalPasien($cUnit)
	{	
		$cKdUnitAsal = "";
		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$cKdUnitAsal = $data->kd_asal;
		}
		if ($cKdUnitAsal != "")
		{
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

	public function GetIdAsalPasien($KdUnit)
	{
		$IdAsal = "";
		$result = $this->db->query("Select * From unit Where Kd_unit=  left('".$KdUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$IdAsal = $data->kd_unit;
		}

		return $IdAsal;
	}


	public function GetKodeKasirPenunjang($cUnit, $cKdUnitAsal)
	{
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='".$cUnit."' and kd_asal='".$cKdUnitAsal."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}

	public function save()
	{
		$this->db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting; # kd_unit lab
		$kd_input_hd = $_POST['kd_unit_hd'];
		
		//$JenisTrans = $_POST['JenisTrans'];
		//$Tgl = gmdate("Y-m-d", time()+60*60*7);
		$Tgl = $_POST['Tgl_transaksi'];
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdDokter_temp = $_POST['KdDokter'];
		
		//kd dokter Tulus Lumaksono, dr. Sp PD = 321
		if($KdDokter_temp == "Tulus Lumaksono, dr. Sp PD"){
			$KdDokter = $this->db->query("select * from dokter where nama='$KdDokter_temp'")->row()->kd_dokter;
		}else{
			$KdDokter = $KdDokter_temp;
		}
		// echo $KdDokter;
		$pasienBaru=$_POST["pasienBaru"];# variabel untuk kunjungan langsung
		
		$Shift =$_POST['Shift'];
		$list = json_decode($_POST['List']);
		$unitasal = $_POST['KdUnit'];# kd_unit asal
		$TmpNotransAsal = $_POST['TmpNotransaksi']; # no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal']; # Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama']; # kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		
		if($TglTransaksiAsal==''){
			$TglTransaksiAsal=$Tgl;
		} else{
			$TglTransaksiAsal=$TglTransaksiAsal;
		}
		
		# *** jika kunjungan langsung ***
		if ($KdPasien == '' && $pasienBaru ==1){
			$KdPasien = $this->GetKdPasien();
			$savepasien= $this->SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi);
		}else {
			$KdPasien = $KdPasien;
		}

		if($KdCusto==''){
			$KdCusto=$TmpCustoLama;
		}else{
			$KdCusto=$KdCusto;
		}
		
		if($unitasal == ''){
			$unitasal=$KdUnit;
		} else{
			$unitasal=$unitasal;
		}

		if($pasienBaru == 0){
			if(substr($unitasal, 0, 1) == '1'){ 
				# RWI
				$IdAsal=2;
			} else if(substr($unitasal, 0, 1) == '2'){ 
				# RWJ
				$IdAsal=1;
			} else if(substr($unitasal, 0, 1) == '3'){ 
				# UGD
				$IdAsal=3;
			}
		} else{
			$IdAsal=1;
		}
		$kdkasirpasien = $this->GetKdKasir($KdUnit,$IdAsal);
		$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
		$notrans_tmp = $this->GetIdTransaksi($kdkasirpasien);
		$notrans_tmp2 = strlen((string)$notrans_tmp);
		if ($notrans_tmp2 < 7){
			$notrans = "0".$notrans_tmp;
		}else{
			$notrans = $notrans_tmp ;
		}
		$UrutDia = $this->GetUrutDia($KdPasien,$KdUnit,$Tgl,$urut);
		$NoDia = $this->GetNoDia();
		if( $_POST['KdUnit'] == '251'){
			$KdUnit_asal = $_POST['KdUnit'];
		}else{
			$KdUnit_asal = $KdUnit;
		}
		
		$simpankunjunganhd = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
		if($simpankunjunganhd == 'success'){
			$simpanhdkunjungan = $this->SimpanHDKunjungan($KdUnit,$KdPasien,$Tgl,$urut,$UrutDia,$NoDia);
			if($simpanhdkunjungan == 'success'){
				$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$KdUnit,$Tgl,$urut);
				if($hasil == 'success'){
					$detail= $this->detailsaveall($NoDia,$TmpNotransAsal,$list,$notrans,$Tgl,$kdkasirpasien,$unitasal,$Shift,$KdUnit_asal,$KdPasien,$urut,$TglTransaksiAsal,$KdDokter);
					if($detail){
						
						if($unitasal != '' && substr($unitasal, 0, 1) =='1'){
							# *** jika bersal dari rawat inap ***
							$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$unitasal,$Kamar,$KdSpesial);
						} else{
							$simpanunitasalinap='Ok';
						}
						
						if($pasienBaru == 0){
							# *** jika bukan Pasien baru/kunjungan langsung ****
							$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
						}else{
							$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
						} 
						
						if($simpanunitasall == 'Ok'){
							$str='Ok';
							
						} else{
							$str='error_unitasal';
						}
					} else{
						$str='error_detail';
					}					
				} else{
					$str='error_transaksi';
				}
			} else{
				$str='error_kunjunganhd';
			}
		} else{
			$str='error_kunjungan';
		}
		
		
		// echo $str; 
		// $nama_unit = $this->db->query("select nama_unit from unit where kd_unit='".$KdUnit."'")->row()->nama_unit;
		if ($str == 'Ok'){
			$this->db->trans_commit();
			echo "{success:true, notrans:'$notrans', kdPasien:'$KdPasien'}";
		} else{
			$this->db->trans_rollback();
			echo "{success:false, error:$str}";
		}

	}

	private function detailsaveall($NoDia,$TmpNotransAsal,$list,$notrans,$Tgl,$kdkasirpasien,$unit,$Shift,$KdUnit,$KdPasien,$urut,$TglTransaksiAsal,$KdDokter){
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$kdUser=$this->session->userdata['user_id']['id'] ;
		/* $lab_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_lab_cito_pk'")->row()->setting; */
		
		$j=0;
		for($i=0;$i<count($list);$i++){
			$kd_produk=$list[$i]->KD_PRODUK;
			$qty=$list[$i]->QTY;
			$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
			$tgl_berlaku=$list[$i]->TGL_BERLAKU;
			$kd_tarif=$list[$i]->KD_TARIF;
			$harga=$list[$i]->HARGA;
			
			/* if (isset($list[$i]->cito))
			{
				$cito=$list[$i]->cito;
				if($cito=='Ya')
				{
					$cito='1';
					$harga=$list[$i]->HARGA;
					$hargacito = (((int) $harga) * ((int)$lab_cito_pk))/100;
					$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
				
				}
				else if($cito=='Tidak')
				{
					$cito='0';
					$harga=$list[$i]->HARGA;
				}
			} else
			{
				$cito='0';
				$harga=$list[$i]->HARGA;
			} */
			
			
			$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
			# *** insert detail_transaksi ***
			$query = $this->db->query("select insert_detail_transaksi
									('".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
										'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$KdUnit."',
										'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false' ) ");
										
			#insert detail transaksi SQL
			$query_sql = _QMS_Query("exec dbo.V5_insert_detail_transaksi 
									'".$kdkasirpasien."', 
									'".$notrans."',
									".$urutdetailtransaksi.",
									'".$Tgl."',
									".$kdUser.",
									'".$kd_tarif."',
									".$kd_produk.",
									'".$KdUnit."',
									'".$tgl_berlaku."',
									'0',
									'0',
									'',
									".$qty.",
									".$harga.",
									".$Shift.",
									'0',
									''  ");
			
			# 1. insert detail_transaksi
			# 2. insert detail_component (berdasarkan kd produk) => 1 produk memiliki banyak komponen
			
			/* if($cito==='1')
			{
				$query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'"); 
				$query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."' "); 
			} */
			
			$qsql=$this->db->query(" select * from detail_component 
										where kd_kasir='".$kdkasirpasien."' 
											and no_transaksi='".$notrans."'
											and urut=".$urutdetailtransaksi."
											and tgl_transaksi='".$Tgl."'")->result();
			foreach($qsql as $line){
				$qkd_kasir=$line->kd_kasir; //p
				$qno_transaksi=$line->no_transaksi;
				$qurut=$line->urut;//p
				$qtgl_transaksi=$line->tgl_transaksi;//p
				$qkd_component=$line->kd_component;
				$qtarif=$line->tarif;
			}
			
			if($query){
				$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
						where (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') 
							AND kd_unit ='".$KdUnit."' AND kd_produk='".$kd_produk."' AND	tgl_berlaku='".$tgl_berlaku."'
							AND kd_tarif='".$kd_tarif."' 
						group by tarif")->result();
				foreach($ctarif as $ct)
				{
					if($ct->jumlah != 0)
					{
						$trDokter = $this->db->query("insert into detail_trdokter select '".$qkd_kasir."','".$notrans."',
							'".$qurut."','".$KdDokter."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 
							WHERE NOT EXISTS (
								SELECT * FROM detail_trdokter WHERE   
									kd_kasir= '".$qkd_kasir."' AND
									tgl_transaksi='".$qtgl_transaksi."' AND
									urut='".$qurut."' AND
									kd_dokter = '".$KdDokter."' AND
									no_transaksi='".$notrans."' )");
					}
				}
				
				if($kd_produk != '1'){
					# *** insert Hemodialisa hasil ***
					$query = $this->db->query("insert into hd_item_kunjungan 
												(no_dia, kd_item, kd_dia, no_urut,no_ref, nilai, tanggal)
												select $NoDia,hid.kd_item::integer,hid.kd_dia,row_number() over (order by hid.kd_dia,hid.kd_item asc) as no_urut,
												hr.no_ref,'','$Tgl'
											from hd_item_dia hid
												inner join hd_item hi on hi.kd_item=hid.kd_item::character varying
												inner join hd_ref hr on hr.kd_item=hid.kd_item::character varying
											order by hid.kd_dia,hid.kd_item ");
				}
			}
		}
		
		return $query;
	}

	public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
    {
		$strError = "";
		date_default_timezone_set("Asia/Jakarta");
		$JamKunjungan = gmdate("Y-m-d H:i:s", time()+60*60*7);
		$data = array("kd_pasien"=>$kdpasien, "kd_unit"=>$unit, "tgl_masuk"=>$Tgl,
					  "kd_rujukan"=>"0", "urut_masuk"=>$urut, "jam_masuk"=>$JamKunjungan,
					  "kd_dokter"=>$kddokter, "shift"=>$Shift, "kd_customer"=>$KdCusto,
					  "karyawan"=>"0", "no_sjp"=>$NoSJP, "keadaan_masuk"=>0,
					  "keadaan_pasien"=>0, "cara_penerimaan"=>99, "asal_pasien"=>$IdAsal,
					  "cara_keluar"=>0, "baru"=>$pasienBaru, "kontrol"=>"0" );

		$query = $this->db->query("select * from kunjungan where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."");
		
		if (count($query->result())==0){
			$result = $this->db->insert('kunjungan',$data);
		}else{
			$result = $this->db->query("update kunjungan set kd_dokter='$kddokter', kd_customer='$KdCusto'
				where kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut." ");
		}
		
		if($result){
			$strError = "success";
		} else{
			$strError = "error";
		}
		
        return $strError;
    }
	
	public function SimpanHDKunjungan($KdUnit,$kdpasien,$TglMasukB,$UrutMasuk,$UrutDia,$NoDia)
    {
		$strError = "";
		date_default_timezone_set("Asia/Jakarta");
		$JamMasuk = gmdate("Y-m-d H:i:s", time()+60*60*7);
		
		
		$data = array("kd_pasien"=>$kdpasien, "kd_unit"=>$KdUnit, "tgl_masuk"=>$TglMasukB,
					  "urut_masuk"=>$UrutMasuk, "jam_masuk"=>$JamMasuk,
					  "urut_dia"=>$UrutDia, "no_dia"=>$NoDia);

		$query = $this->db->query("select * from hd_kunjungan where kd_pasien = '".$kdpasien."' AND kd_unit = '".$KdUnit."' AND tgl_masuk = '".$TglMasukB."' AND urut_masuk=".$UrutMasuk."");
		
		if (count($query->result())==0){
			$result = $this->db->insert('hd_kunjungan',$data);
			if($result){
				$strError = "success";
			} else{
				$strError = "error";
			}
		}
		
        return $strError;
    }


	public function SimpanTransaksi($kdkasirpasien,$notrans,$kdpasien,$unit,$Tgl,$urut)
	{
		$strError = "";
		$kduser = $this->session->userdata['user_id']['id'];
		$data = array("kd_kasir"=>$kdkasirpasien, "no_transaksi"=>$notrans, "kd_pasien"=>$kdpasien, 
					  "kd_unit"=>$unit, "tgl_transaksi"=>$Tgl, "urut_masuk"=>$urut, "tgl_co"=>NULL,
					  "co_status"=>"False", "orderlist"=>NULL, "ispay"=>"False", "app"=>"False",
					  "kd_user"=>$kduser, "tag"=>NULL, "lunas"=>"False", "tgl_lunas"=>NULL,
					  "posting_transaksi"=>"True");
					  
		$criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirpasien."'";
		$query = $this->db->query("select * from transaksi where no_transaksi = '".$notrans."' AND kd_kasir = '".$kdkasirpasien."' ")->result();
		// echo count($query);
		if (count($query)==0){
			$result = $this->db->insert('transaksi',$data);
			
			if($result){
				$strError = "success";
			} else{
				$strError = "error";
			}
		}
		return $strError;
	}

	public function SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien, "nama"=>$NmPasien, "jenis_kelamin"=>$JK,
							"tgl_lahir"=>$Ttl, "gol_darah"=>$GolDarah, "alamat"=>$Alamat,
							"no_asuransi"=>$NoAskes, "pemegang_asuransi"=>$NamaPesertaAsuransi);
		$query = $this->db->query("select * from pasien where kd_pasien = '".$KdPasien."'")->result();
		if (count($query) == 0){
			$result = $this->db->insert('pasien',$data);
			if($result){
				$strError = "success";
			} else{
				$strError = "error";
			}
		}
		return $strError;
	}

	public function SimpanMrLab($KdPasien,$unit,$Tgl,$urut)
	{
		$strError = "";
			$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$unit,
							"tgl_masuk"=>$Tgl,
							"urut_masuk"=>$urut
						);

		$result = $this->db->insert('mr_lab',$data);
		if($result){
			$strError='success';
		} else{
			$strError='error';
		}

		return $strError;
	}

	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		// $this->load->model("general/tb_unit_asalinap");
		// $result = $this->tb_unit_asal->Save($data);
		//-----------insert to sq1 server Database---------------//
		// _QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//


		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}


	public function GetKdPasien()
	{
		$kdPasien="";
		$inisial = 'HD';
		$res = $this->db->query("Select kd_pasien from pasien where LEFT(kd_pasien,2) = '$inisial' ORDER BY kd_pasien desc limit 1")->result();
		foreach($res as $line){
			$kdPasien=$line->kd_pasien;
		}

		if ($kdPasien != ""){
			$nm = $kdPasien;

			# HD00001
			# penambahan 1 digit
			$no = substr($nm,-5);
			$nomor = (int) $no +1;
			if (strlen($nomor) == 1){
				$nomedrec = $inisial."0000". $nomor;
			}else if(strlen($nomor) == 2){
				$nomedrec = $inisial."000". $nomor;
			}else if(strlen($nomor) == 3){
				$nomedrec = $inisial."00". $nomor;
			}else if(strlen($nomor) == 4){
				$nomedrec = $inisial."0". $nomor;
			}else if(strlen($nomor) == 5){
				$nomedrec = $inisial. $nomor;
			}
			$getnewmedrec = $nomedrec;
		}else{
			$getnewmedrec= $inisial."00001";
		}
		return $getnewmedrec;
	}
	
	public function GetNoDia(){
		$thisYear = date('Y');
		$thisMonth = date('m');
		$str = $thisYear.$thisMonth;
		$res = $this->db->query("select no_dia from hd_kunjungan where EXTRACT(MONTH FROM tgl_masuk) ='".$thisMonth."' 
								and EXTRACT(YEAR FROM tgl_masuk) ='".$thisYear."' order by no_dia desc limit 1");
		if(count($res->result()) > 0){
			$no = substr($res->row()->no_dia,6);
			$nodia = $no + 1;
			$newNoDia = $str.str_pad($nodia,6,"0", STR_PAD_LEFT);
		} else{
			$newNoDia = $str."000001";
		}
		return $newNoDia;
	}
	
	public function GetUrutDia($KdPasien,$KdUnit,$Tgl,$urut){
		$res = $this->db->query("select urut_dia from hd_kunjungan where kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk=".$urut." ");
		if(count($res->result()) > 0){
			$urut_dia = $res->row()->urut_dia + 1;
		} else{
			$urut_dia = 1;
		}
		
		return $urut_dia;
	}


	public function ubahlabhasil(){
		$KdPasien = $_POST['KdPasien'];
		$Tgl =date("Y-m-d");
		$TglMasuk =$_POST['TglMasuk'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['Unit'];
		$KdDokter = $_POST['KdDokter'];
		$UrutM = $_POST['UrutMasuk'];
		$TglHasil = $_POST['TglHasil'];
		$JamHasil = date('Y-m-d h:i:s');


			$urut = $this->GetAntrian($KdPasien, $unit,$TglMasuk,$KdDokter);
			$a = explode("##[[]]##",$list);
				for($i=0;$i<=count($a)-1;$i++)
				{

						$b = explode("@@##$$@@",$a[$i]);
						$hasil=$b[3];
						$kethasil='';
						
						$ren = explode(" - ",$b[6]);
						//10 - 15
						if(count($ren) > 1){
							$min=$ren[0];
							$max=$ren[1];
							//echo $min;
							if(is_numeric($min) && is_numeric($b[3])){
								if($b[3] <>''){
									
									
									if(($hasil >= $min) && ($hasil <= $max)){
										$kethasil='N';
									} else if($hasil > $max){
										$kethasil='H';
									} else if($hasil < $min){
										$kethasil='L';
									} 
									
									
								} else if ($b[3] ==''){
									
									$kethasil='';
									
								}
								
							
								
							} else{
								$kethasil='';
							}

						}
					
						

						$query = $this->db->query("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='".$TglHasil."', jam_selesai_hasil='".$JamHasil."', ket_hasil='".$kethasil."'
													where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
													");

						//-----------insert to sq1 server Database---------------//
						$sql=("update lab_hasil set hasil='$b[3]', ket='$b[4]', tgl_selesai_hasil='".$TglHasil."', jam_selesai_hasil='".$JamHasil."'
													where kd_lab='$b[5]' and kd_test='$b[1]' and kd_produk='$b[5]' and kd_pasien='$KdPasien' and tgl_masuk='$TglMasuk' and urut_masuk=$UrutM
													");
						_QMS_Query($sql);
						//-----------akhir insert ke database sql server----------------//

				}
				if($query)
				{
					echo "{success:true}";
				}else{
					echo "{success:false}";
				}

	}

	private function GetUrutKunjungan($medrec, $unit, $tanggal)
	{
		$retVal = 1;
		if($medrec === "Automatic from the system..." || $medrec === " ")
		{
			$tmpmedrec = " ";
		}else {
			$tmpmedrec = $medrec;
		}
		$this->load->model('general/tb_getantrian');
		$this->tb_getantrian->db->where(" kd_pasien = '".$tmpmedrec."' and kd_unit = '".$unit."'and tgl_masuk = '".$tanggal."'order by urut_masuk desc Limit 1", null, false);
		$res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
		if ($res[1]>0)
		{
			$nm = $res[0][0]->URUT_MASUK;
			$nomor = (int) $nm +1;
			$retVal=$nomor;
		}
		return $retVal;
	}
		
	public function getDokterPengirim(){
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$res=$this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."') AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row();
		$nama = $res->nama;
		$kd_dokter = $res->kd_dokter;
		if($nama){
			echo "{success:true,nama:'$nama',kd_dokter:'$kd_dokter'}";
		} else{
			echo "{success:false}";
		}
	}

		
	public function getPasien(){
		$tanggal=$_POST['tanggal'];
		$asal_pasien=$_POST['asal_pasien'];
		$tanggal2 = str_replace('/', '-', $tanggal);
		
		$xtanggal=$_POST['tanggal2'];
		$xtanggal2 = str_replace('/', '-', $xtanggal);
		
		$k_no_transaksi='';
		$k_no_medrec='';
		$k_nama_pasien='';
		if($_POST['a'] == 0 || $_POST['a'] == '0'){
			$no_transaksi = " and tr.no_transaksi like '".$_POST['text']."%'";
			$nama = "";
			$kd_pasien = "";
		} else if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " and pasien.kd_pasien like '".$_POST['text']."%'";
			$nama = "";
			$no_transaksi = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and lower(pasien.nama) like lower('".$_POST['text']."%')";
			$kd_pasien = "";
			$no_transaksi = "";
		}
		
		$k_asal_pasien='';
		$k_unit_kunjungan='';
		if($asal_pasien == ''){
			$k_asal_pasien='';
		}else{
			if($asal_pasien == '3' || $asal_pasien == 3){
				#PASIEN IRD HANYA UNIT IRD HEMODIALISA
				$k_asal_pasien=" and kunjungan.kd_unit= '305' "; 
			}else if($asal_pasien == '2' || $asal_pasien == 2){
				#PASIEN RWJ HANYA UNIT HEMODIALISA PAV
				$k_asal_pasien=" and kunjungan.kd_unit in ('251') ";
			}else if ($asal_pasien == '1' || $asal_pasien == 1){
				#PASIEN RAWAT INAP SELURUH UNIT
				$k_asal_pasien=" and left(kunjungan.kd_unit,1)='1' ";
			}
			
		}
		//$query= $this->db->query("")->result();
		

		$result=$this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar,  ka.nama_kamar,nginap.kd_spesial,s.spesialisasi,nginap.akhir,
									case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
									pasien.kd_pasien||'   '||u.nama_unit as kode
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
										left join spesialisasi s on s.kd_spesial = nginap.kd_spesial
										left join kamar ka on ka.no_kamar = nginap.no_kamar
										inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
										inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
									)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE 
								--left(kunjungan.kd_unit,1) in ('1','2','3','7') and 
								--kunjungan.kd_unit in ('251','305') 
								tr.tgl_transaksi >='".$xtanggal2."' and tr.tgl_transaksi <='".$tanggal2."' 
								".$k_asal_pasien."
								".$kd_pasien."
								".$no_transaksi."
								".$nama."
								ORDER BY tr.no_transaksi desc limit 10						
							")->result();
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getPasien_NoMedrec(){
		$tanggal=$_POST['tanggal'];
		$asal_pasien=$_POST['asal_pasien'];
		$tanggal2 = str_replace('/', '-', $tanggal);
		
		$xtanggal=$_POST['tanggal2'];
		$xtanggal2 = str_replace('/', '-', $xtanggal);
		
		$kd_pasien = " and pasien.kd_pasien = '".$_POST['no_medrec']."'";
	
		if($asal_pasien == ''){
			$k_asal_pasien='';
		}else{
			if($asal_pasien == '3' || $asal_pasien == 3){
				#PASIEN IRD HANYA UNIT IRD HEMODIALISA
				$k_asal_pasien=" and kunjungan.kd_unit= '305' "; 
			}else if($asal_pasien == '2' || $asal_pasien == 2){
				#PASIEN RWJ HANYA UNIT HEMODIALISA PAV
				$k_asal_pasien=" and kunjungan.kd_unit in ('251') ";
			}else if ($asal_pasien == '1' || $asal_pasien == 1){
				#PASIEN RAWAT INAP SELURUH UNIT
				$k_asal_pasien=" and left(kunjungan.kd_unit,1)='1' ";
			}
		}
		//$query= $this->db->query("")->result();
		
		$result=$this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
									to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar,  ka.nama_kamar,nginap.kd_spesial,s.spesialisasi,nginap.akhir,
									case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
									pasien.kd_pasien||'   '||u.nama_unit as kode
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
										left join spesialisasi s on s.kd_spesial = nginap.kd_spesial
										left join kamar ka on ka.no_kamar = nginap.no_kamar
										inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
										inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
									)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE 
								tr.tgl_transaksi >='".$xtanggal2."' and tr.tgl_transaksi <='".$tanggal2."' 
								".$k_asal_pasien."
								".$kd_pasien."
								ORDER BY tr.no_transaksi desc limit 10						
							")->result();
					 
			echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getItemPemeriksaan(){
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
		$kdkasir = $_POST['kd_kasir'];
		$no_transaksi=$_POST['no_transaksi'];
		if($no_transaksi == ""){
			$where="";
		} else{
			$where=" where no_transaksi='".$no_transaksi."' And kd_kasir ='$kdkasir'";
		}
		
		
		$result=$this->db->query(" select * from detail_transaksi dt
										inner join produk p on p.kd_produk=dt.kd_produk
									where dt.no_transaksi='".$no_transaksi."' And dt.kd_kasir ='".$kdkasir."' AND dt.kd_unit='".$KdUnit."' order by dt.urut
								  ")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
function saveTrDokter($listtrdokter,$kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut)
	{
		//save jasa dokter
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 
			foreach ($listtrdokter as $arr) 
			{
			//var_dump($arr);
			
			if($arr->kd_job == 'Dokter')
			{
				$kd_job = 1;
			}
			else
			{
				$kd_job = 2;
			}

			$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
			where 
			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
			kd_unit ='".$KdUnit."' AND
			kd_produk='".$arr->kd_produk."' AND
			tgl_berlaku='".$arr->tgl_berlaku."' AND
			kd_tarif='".$arr->kd_tarif."' group by tarif")->result();
			
			foreach($ctarif as $ct)
			{
			if($ct->jumlah != 0)
			{
				
				$trDokter = $this->db->query("insert into detail_trdokter select '".$kdkasirasalpasien."','".$notrans."','".$Schurut."','".$arr->kd_dokter."','".$Tgl."',0,'".$kd_job."',".$ct->tarif.",0,0,0 WHERE
    NOT EXISTS (
        SELECT * FROM detail_trdokter WHERE   
			kd_kasir= '".$kdkasirasalpasien."' AND
			tgl_transaksi='".$Tgl."' AND
			urut='".$Schurut."' AND
			kd_dokter = '".$arr->kd_dokter."' AND
			no_transaksi='".$notrans."'
    )");
			}
			}
			}
				
				//akhir save jasa dokter
				
	}

	
	
	public function getCurrentShiftHd(){
		$kdUnit=$this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
		$kd_bagian = $this->db->query("select kd_bagian from unit where kd_unit = '".$kdUnit."'")->row()->kd_bagian;
		$query=$this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,bs.lastdate,to_char(bs.lastdate,'YYYY-mm-dd')as lastdate 
									FROM bagian_shift bs
									INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									WHERE b.kd_bagian='".$kd_bagian."'")->row();
		$shift=$query->shift;
		$lastdate=$query->lastdate;
		if($shift == 3){
			$today=date('Y-m-d');
			$yesterday=date("Y-m-d", strtotime("yesterday"));
			if($lastdate != $today || $lastdate==$yesterday){
				$shift=4;
			} else{
				$shift=$shift;
			}
		} else{
			$shift=$shift;
		}
		
		echo $shift;
	}
	
	public function cekProduk(){
		$query=$this->db->query("select kd_lab, kd_test, normal, item_test,satuan, case when normal=' ' and satuan=' ' then 'Normal Kosong' 
									when normal='' and satuan='' then 'Normal Kosong' 
									when normal !='' and satuan!='' then 'Normal' 
									when normal !=' ' and satuan!=' ' then 'Normal' end as ket 
								from lab_test where kd_lab=".$_POST['kd_lab']." and kd_test !=0 order by kd_test")->result();
		$totkosong=0;
		$totisi=0;
		
		if(count($query) > 0){
			foreach($query as $x){
				if($x->ket == 'Normal Kosong'){
					$kosong = 1;
					$totkosong += $kosong;
				} else{
					$isi=1;
					$totisi += $isi;
				}
			}

		} else{
			$totkosong=100;
		}
		
		//var_dump($totkosong);
		if($totkosong < 1){
			echo '{success:false}';
		} else{
			echo '{success:true}';
		}
	}
	
	public function getKdUnit(){
		$kd_unit = $this->db->query("select setting from sys_setting where key_data='hd_default_kd_unit'")->row()->setting;
		echo "{success:true, kd_unit:'$kd_unit'}";
	}
	
	function gantidokter(){
		
		$KdKasir=$_POST['KdKasir'];
		$KdDokter=$_POST['KdDokter'];
		$KdPasien=$_POST['KdPasien'];
		$KdUnit=$_POST['KdUnit'];
		$Tgl=$_POST['Tgl'];
		$UrutMasuk=$_POST['UrutMasuk'];
		$NoTrans=$_POST['NoTrans'];
		$KdDokterGanti=$_POST['KdDokterGanti'];
		$cek = $this->db->query("select * from kunjungan where kd_dokter='".$KdDokter."' and kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk=$UrutMasuk")->result();
		if (count($cek) > 0){
			$update_kunjungan=$this->db->query("update kunjungan set kd_dokter='".$KdDokterGanti."' where
										kd_pasien='".$KdPasien."' and kd_unit='".$KdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk=$UrutMasuk");
			$update_detail_trdokter=$this->db->query("update Detail_TRDokter set kd_dokter = '".$KdDokterGanti."' where no_transaksi = '".$NoTrans."' and kd_kasir = '".$KdKasir."' " );
		}
		if ($update_kunjungan && $update_detail_trdokter)
		{
			echo '{success: true}';
		} else {
			echo '{success: false}';
		}
	}
	
	function getKelpas(){
		$kd_cust=$_POST['kd_cust'];
		$query= $this->db->query("select kd_customer,jenis_cust, 
									case when 
										jenis_cust='0' 
									then 'Perseorangan'
									when 
										jenis_cust='1' 
									then 'Perusahaan'
									when 
										jenis_cust='2' 
									then 'Asuransi'
									 end as kelpas from kontraktor where kd_customer='".$kd_cust."'")->row();
		$kelpas =$query->kelpas;
		echo "{success:true, kelpas:'$kelpas'}";
	}
	
	function UpdateKdCustomer(){
		$TrKodePasien = $_POST['TrKodePasien'];
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$KdDokter = $_POST['KdDokter'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$KDCustomer = $_POST['KDCustomer'];
		$KDNoSJP = $_POST['KDNoSJP'];
		$KDNoAskes = $_POST['KDNoAskes'];
		$UrutMasuk = $_POST['UrutMasuk'];
		$query = $this->db->query("select updatekdcostumer('".$TrKodePasien."','".$KdUnit."','".$TglTransaksi."','".$KDNoAskes."','".$KDNoSJP."','".$KDCustomer."')");
		$res = $query->result();
		if($res)
		{
		 echo "{success:true}";
		}
		else
		{
		  echo "{success:false}";
		}
	}
	
	public function GetKdKasir($KdUnit,$IdAsal){
		$KdKasir=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$KdUnit' and kd_asal='$IdAsal'")->row()->kd_kasir;
		return $KdKasir;
	}
	
	public function getTransaksiLama(){
		$no_transaksi = $_POST['no_transaksi'];
		$tgl_transaksi = $_POST['tgl_transaksi'];
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
		$query=$this->db->query("select * from(     
								   select distinct tr.no_transaksi,cus.customer,ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal
											,u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter,tr.cito
											,d.nama as nama_dokter,tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, u.kd_kelas,k.kd_customer,k.urut_masuk
											,k.kd_unit, knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment, payment_type.jenis_pay,payment.kd_pay
											,payment_type.type_data, tr.posting_transaksi,(select count(flag) from detail_transaksi where dt.no_transaksi=tr.no_transaksi AND dt.flag=1 ) as flag
									from transaksi tr
									LEFT JOIN detail_transaksi dt on tr.no_transaksi = dt.no_transaksi
									INNER JOIN pasien p on p.kd_pasien=tr.kd_pasien
									INNER JOIN unit  u on u.kd_unit=tr.kd_unit
									INNER JOIN kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
									INNER JOIN customer cus on cus.kd_customer= k.kd_customer
									LEFT  JOIN kontraktor knt on knt.kd_customer=k.kd_customer
									INNER JOIN payment on payment.kd_customer = k.kd_customer
									INNER JOIN payment_type on payment.jenis_pay = payment_type.jenis_pay
									INNER JOIN dokter d on d.kd_dokter= k.kd_dokter
									LEFT JOIN unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
									INNER JOIN transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
									inner join unit  uu on uu.kd_unit=tra.kd_unit
									)as resdata where tgl_transaksi='".$tgl_transaksi."' and kd_unit='".$KdUnit."' and no_transaksi='".$no_transaksi."' ")->result();
		echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
	}
	
	public function getCek_Kunjungan(){
		$kd_pasien = $_POST['kd_pasien'];
		$tgl_masuk = date('Y-m-d');
		$KdUnit = $this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
		$query=$this->db->query(" select * from kunjungan where kd_pasien='".$kd_pasien."' and tgl_masuk='".$tgl_masuk."' and kd_unit='".$KdUnit."'")->result();
		$cek_kunjungan = count($query);
		echo '{success:true, cek_kunjungan:'.$cek_kunjungan.'}';
	}
}

?>