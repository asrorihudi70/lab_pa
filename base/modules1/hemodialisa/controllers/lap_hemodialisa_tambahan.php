<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_hemodialisa_tambahan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUser(){
		$result=$this->db->query("SELECT kd_user,user_names,full_name
									FROM zusers
									UNION 
									Select '9999' as kd_user, ' Semua' as user_names, 'Semua' as full_name Order By full_name asc")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}

	public function transaksi_detail(){
   		$title='Laporan Transaksi Detail';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($user != 'Semua'){
			$criteriaUserB=" and db.kd_user='".$user."' ";
			$criteriaUserT=" and dt.kd_user='".$user."' ";
		} else{
			$criteriaUserB="";
			$criteriaUserT="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
	
		if($shift == 'All'){
			$criteriaShiftB="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShiftB="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShiftB="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShiftB="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShiftB="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShiftB="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShiftB="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
			$queryhead_pemeriksaan=$this->db->query("select distinct namapasien from (
												Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi, 'Penerimaan' as Head  
														from Detail_bayar db   
															inner join payment py on db.kd_pay=py.kd_Pay   
															inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
															inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
															left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
															inner join customer c on k.kd_customer=c.kd_Customer  
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShiftB."
																".$criteriaCustomer."
																".$criteriaUserB."
																".$crtiteriaAsalPasien."
																and pyt.Type_Data <=3
																and u.kd_bagian in ('72') 
												 union 	select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,(Dt.QTY* Dt.Harga) as jumlah,Dt.QTY,Dt.Urut,  
													Dt.tgl_transaksi,  'Pemeriksaan' as Head    
														from detail_transaksi DT   
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShift."
																".$criteriaCustomer."
																".$criteriaUserT."
																".$crtiteriaAsalPasien."
																and (u.kd_bagian in ('72') )  
																order by No_transaksi
											) Z where HEAD='Pemeriksaan' ");
		$queryhead_penerimaan=$this->db->query("select distinct namapasien from (
												Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi, 'Penerimaan' as Head  
														from Detail_bayar db   
															inner join payment py on db.kd_pay=py.kd_Pay   
															inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
															inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
															left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
															inner join customer c on k.kd_customer=c.kd_Customer  
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShiftB."
																".$criteriaCustomer."
																".$criteriaUserB."
																".$crtiteriaAsalPasien."
																and pyt.Type_Data <=3
																and u.kd_bagian in ('72') 
												 union 	select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,(Dt.QTY* Dt.Harga) as jumlah,Dt.QTY,Dt.Urut,  
													Dt.tgl_transaksi,  'Pemeriksaan' as Head    
														from detail_transaksi DT   
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShift."
																".$criteriaCustomer."
																".$criteriaUserT."
																".$crtiteriaAsalPasien."
																and u.kd_bagian in ('72')     order by No_transaksi
											) Z where HEAD='Penerimaan' ");
		$query_pemeriksaan = $queryhead_pemeriksaan->result();	
		$query_penerimaan = $queryhead_penerimaan->result();	
		// echo '{success:true, totalrecords:'.count($query_pemeriksaan).', ListDataObj:'.json_encode($query_pemeriksaan).'}';
		// echo '{success:true, totalrecords:'.count($query_penerimaan).', ListDataObj:'.json_encode($query_penerimaan).'}';
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="6">'.$title.' '.$asal_pasien.'</th>
				</tr>';
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="6">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="6">'.$namabulan.'</th>
					</tr>';
		}
		$html.='<tr>
					<th colspan="6">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="6">Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table  border = "1" >
			<thead>
				 <tr>
					<th width="10" align="center" > No.</th>
					<th width ="250" align="center" > Pasien</th>
					<th width ="200" align="center" > Pemeriksaan</th>
					<th align="center" > Qty</th>
					<th align="center" > Jumlah </th>
					<th align="center" > User </th>
				  </tr>
			</thead>';
		$baris=0;
		 if(count($query_pemeriksaan) > 0 || count($query_penerimaan) > 0) {
			$no=1;
			$html.='<tr>
						<td align="center">1.</td>
						<td>Pemeriksaan</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					 </tr>';
			$baris++;
			foreach ($query_pemeriksaan as $line) 
			{
				$namapasien=$line->namapasien;
				$html.='<tr>
							<td></td>
							<td>'.$no.'. &nbsp;'.$namapasien.'</td>
							<td></td><td></td><td></td><td></td>
						 </tr>';
				$baris++;
				$querybody_pemeriksaan=$this->db->query("select * from (
												Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi, 'Penerimaan' as Head  
														from Detail_bayar db   
															inner join payment py on db.kd_pay=py.kd_Pay   
															inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
															inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
															left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
															inner join customer c on k.kd_customer=c.kd_Customer  
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShiftB."
																".$criteriaCustomer."
																".$criteriaUserB."
																".$crtiteriaAsalPasien."
																and pyt.Type_Data <=3
																and u.kd_bagian in ('72') 
												 union 	select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,(Dt.QTY* Dt.Harga) as jumlah,Dt.QTY,Dt.Urut,  
													Dt.tgl_transaksi,  'Pemeriksaan' as Head    
														from detail_transaksi DT   
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShift."
																".$criteriaCustomer."
																".$criteriaUserT."
																".$crtiteriaAsalPasien."
																and u.kd_bagian in ('72')   
											) Z 
												where HEAD='Pemeriksaan' 
												and namapasien='$namapasien' 
												order by bayar,kd_pay,No_transaksi 
												")->result();
					// echo '{success:true, totalrecords:'.count($querybody_pemeriksaan).', ListDataObj:'.json_encode($querybody_pemeriksaan).'}';
				$jumlah =0;
				foreach ($querybody_pemeriksaan as $line2){
					$html.=' <tr>
								<td></td><td></td>
								<td>'.$line2->deskripsi.'</td>
								<td align="center">&nbsp;'.$line2->qty.'</td>
								<td  align="right">'.number_format($line2->jumlah,0, "," , ",").'</td>
								<td align="center">&nbsp;'.$line2->kd_user.'</td>
							  </tr>';
					$jumlah = $jumlah+$line2->jumlah;
					$baris++;
				}
				
				
				$html.='<tr>
							<td></td><td></td><td align="right"><b>Jumlah</b></td><td></td><td align="right">'.number_format($jumlah,0, "," , ",").'</td><td></td>
						</tr>';
				$no++;
				$baris++;
			}
			
			$no_b=1;
			$html.='<tr>
						<td align="center">2.</td>
						<td>Penerimaan</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					 </tr>';
			foreach ($query_penerimaan as $line3) 
			{
				$baris++;
				$namapasien2=$line3->namapasien;
				$html.='<tr>
							<td></td>
							<td>'.$no_b.'. &nbsp;'.$namapasien2.'</td>
							<td></td><td></td><td></td><td></td>
						 </tr>';
				$querybody_penerimaan=$this->db->query("select * from (
												Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													db.Kd_Pay ,py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi, 'Penerimaan' as Head  
														from Detail_bayar db   
															inner join payment py on db.kd_pay=py.kd_Pay   
															inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay     
															inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
															left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
															inner join customer c on k.kd_customer=c.kd_Customer  
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShiftB."
																".$criteriaCustomer."
																".$criteriaUserB."
																".$crtiteriaAsalPasien."
																and pyt.Type_Data <=3
																and u.kd_bagian in ('72') 
												 union 	select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  
													Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,(Dt.QTY* Dt.Harga) as jumlah,Dt.QTY,Dt.Urut,  
													Dt.tgl_transaksi,  'Pemeriksaan' as Head    
														from detail_transaksi DT   
															inner join produk prd on DT.kd_produk=prd.kd_produk   
															inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
															inner join unit u on u.kd_unit=t.kd_unit    
															inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk   
															inner join customer c on k.kd_customer=c.kd_Customer   
															left join kontraktor knt on c.kd_customer=knt.kd_Customer     
															inner join pasien p on t.kd_pasien=p.kd_pasien  
																".$criteriaShift."
																".$criteriaCustomer."
																".$criteriaUserT."
																".$crtiteriaAsalPasien."
																and u.kd_bagian in ('72') 
											) Z 
												where HEAD='Penerimaan' 
												and namapasien='$namapasien2' 
												order by bayar,kd_pay,No_transaksi 
												")->result();
					// echo '{success:true, totalrecords:'.count($querybody_pemeriksaan).', ListDataObj:'.json_encode($querybody_pemeriksaan).'}';
				$jumlah_b=0;
				foreach ($querybody_penerimaan as $line4){
					$html.=' <tr>
								<td></td><td></td>
								<td>'.$line4->deskripsi.'</td>
								<td align="center">&nbsp;'.$line4->qty.'</td>
								<td  align="right">'.number_format($line4->jumlah,0, "," , ",").'</td>
								<td align="center">&nbsp;'.$line4->kd_user.'</td>
							  </tr>';
					$jumlah_b = $jumlah_b+$line4->jumlah;
					$baris++;
				}
				
				
				$html.='<tr>
							<td></td><td></td><td align="right"><b>Jumlah</b></td><td></td><td align="right">'.number_format($jumlah_b,0, "," , ",").'</td><td></td>
						</tr>';
				$no_b++;
				$baris++;
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="6" align="center">Data tidak ada</td>
				</tr>
			
			';		
			$baris++;
		}
		 
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+8;
		$print_area='A1:F'.$baris;
		$area_wrap='B8:F'.$baris;
		$area_kanan='D8:E'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:F7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:F7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:F7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(32);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:F'.$baris)->applyFromArray($styleBorder);
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanTransaksiDetail.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P','Laporan Transaksi Detail',$html);
		
		} 
   	 
	}
	
	function transaksi_summary(){
		$title='Laporan Transaksi Summary';
		$param=json_decode($_POST['data']);
		
		$asal_pasien=$param->asal_pasien;
		$user=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$type_file=$param->type_file;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$tomorrow = date('d-M-Y',strtotime($tglAwal . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($tglAkhir . "+1 days"));
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$explode_nama_bln = explode('-',$awal);
		$namabulan = $explode_nama_bln[1]." ".$explode_nama_bln[2];
		
		$kd_unit= $this->db->query("select setting from sys_setting  where key_data='hd_default_kd_unit'")->row()->setting;
		
		//-----------------------get kode kasir unit-------------------------------------
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='$kd_unit' and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 'Semua'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
		} else if($asal_pasien == 'RWJ'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirRwj."')";
		} else if($asal_pasien == 'IGD'){
			$crtiteriaAsalPasien=" and t.kd_kasir in ('".$KdKasirIGD."')";
		}else if($asal_pasien == 'RWI'){
			$crtiteriaAsalPasien=" and t.kd_kasir='".$KdKasirRwi."'";
		}
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
		}
		
		if($tipe != 'Semua'){
			$criteriaCustomer=" and knt.kd_customer='".$kdcustomer."'";
		} else{
			$criteriaCustomer="";
		}
		
		if($user != 'Semua'){
			$criteriaUserB=" and db.kd_user='".$user."' ";
			$criteriaUserT=" and dt.kd_user='".$user."' ";
		} else{
			$criteriaUserB="";
			$criteriaUserT="";
		}
		
		if($shift == 'All'){
			$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2,3)
									AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (1,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="where  (dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >= ".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (2,3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="where  (((dt.Tgl_Transaksi >=".$tAwal."  and dt.Tgl_Transaksi <= ".$tAkhir.") and dt.shift in (3)
										AND not (dt.Tgl_Transaksi = ".$tAwal." and  dt.shift=4 ))  
										or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		if($shift == 'All'){
			$criteriaShiftB="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2,3)
									AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
									or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
			$sh="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShiftB="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1)";
				$sh="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShiftB="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,2)";
				$sh="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShiftB="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (1,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShiftB="where  (db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2)";
				$sh="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShiftB="where  (((db.Tgl_Transaksi >= ".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (2,3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShiftB="where  (((db.Tgl_Transaksi >=".$tAwal."  and db.Tgl_Transaksi <= ".$tAkhir.") and db.shift in (3)
										AND not (db.Tgl_Transaksi = ".$tAwal." and  db.shift=4 ))  
										or ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '".$tomorrow."' and '".$tomorrow2."'))";
				$sh="Shift 3";
			} 
		}
		
		
		$query=$this->db->query("Select X.BAYAR,X.Kd_CUSTOMER,X.CUSTOMER,SUM(P) AS JML_PASIEN,SUM(JML) AS JML_TR   
									from ( Select 1 AS BAYAR,k.Kd_CUSTOMER,c.CUSTOMER,1 as P,SUM(db.Jumlah) AS JML   
										from Detail_bayar  db   inner join payment py on db.kd_pay=py.kd_Pay       
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay        
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir       
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk       
													inner join customer c on k.kd_customer=c.kd_Customer       
													left join kontraktor knt on c.kd_customer=knt.kd_Customer             
													inner join pasien p on t.kd_pasien=p.kd_pasien       
									".$criteriaShiftB."
									 and pyt.Type_Data <=3 
									".$criteriaCustomer."
									".$criteriaUserB."
									".$crtiteriaAsalPasien."
									and u.kd_bagian in ('72') 
									GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR  ) X GROUP BY x.bayar,X.KD_CUSTOMER, X.CUSTOMER 
								union SELECT X.Tagih,X.KD_CUSTOMER,X.CUSTOMER,0,SUM(JML)  
									FROM (select 0 as Tagih,k.KD_CUSTOMER,c.CUSTOMER,0 as p,SUM(Dt.QTY* Dt.Harga) as jml       
										from detail_transaksi DT inner join produk prd on DT.kd_produk=prd.kd_produk       
													inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir      
													inner join unit u on u.kd_unit=t.kd_unit        
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk  
													inner join customer c on k.kd_customer=c.kd_Customer     
													left join kontraktor knt on c.kd_customer=knt.kd_Customer            
													inner join pasien p on t.kd_pasien=p.kd_pasien  
									".$criteriaShift."
									".$criteriaCustomer."
									".$criteriaUserT."
									".$crtiteriaAsalPasien."
									and u.kd_bagian in ('72') 
									
									GROUP BY k.Kd_CUSTOMER,c.CUSTOMER,t.NO_TRANSAKSI,t.KD_KASIR  ) X GROUP BY x.tagih,X.KD_CUSTOMER, X.CUSTOMER ")->result();
		
		// echo '{success:true, totalrecords:'.count($query).', ListDataObj:'.json_encode($query).'}';
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tr>
					<th colspan="5">'.$title.' '.$asal_pasien.'</th>
				</tr>';
				
		if($periode == 'tanggal'){
			$html.='<tr>
						<th colspan="5">'.$awal.' s/d '.$akhir.'</th>
					</tr>';
		}else{
			$html.='<tr>
						<th colspan="5">'.$namabulan.'</th>
					</tr>';
		}
		$html.='<tr>
					<th colspan="5">'.$sh.'</th>
				</tr>
				<tr>
					<th colspan="5">Kelompok '.$tipe.' ('.$customer.')</th>
				</tr>
				
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1">
			<thead>
				 <tr>
					<th align="center" width="20">No</th>
					<th align="center" width="100"> Kelompok Pasien</th>
					<th align="center" width="50"> Jumlah Pasien</th>
					<th align="center" width="80"> Pemeriksaan</th>
					<th align="center" width="80"> Penerimaan </th>
				  </tr>
			</thead>';
		$baris=0;
		 if(count($query)> 0) {
			
			$no=1;
			$total_jml_pasien=0;
			$total_jml_tr_pemeriksaan=0;
			$total_jml_tr_penerimaan=0;
			foreach ($query as $line){
				$html.='<tr>
							<td>'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td align="right">'.$line->jml_pasien.'</td>';
				#pemeriksaan
				if($line->bayar == 0){
					$html.='<td align="right">'.number_format($line->jml_tr,0, "," , ",").'</td><td></td>';
					$total_jml_tr_pemeriksaan = $total_jml_tr_pemeriksaan + $line->jml_tr;
				}else{
					$html.='<td></td><td align="right">'.number_format($line->jml_tr,0, "," , ",").'</td>';
					$total_jml_tr_penerimaan = $total_jml_tr_penerimaan + $line->jml_tr;
				}
				$html.='</tr>';
				$no++;
				$total_jml_pasien = $total_jml_pasien + $line->jml_pasien;
				
			}
			$selisih = $total_jml_tr_pemeriksaan - $total_jml_tr_penerimaan;
			$html.='<tr>
						<td colspan="2" align="right">Total</td>
						<td align="right">'.number_format($total_jml_pasien,0, "," , ",").'</td>
						<td align="right">'.number_format($total_jml_tr_pemeriksaan,0, "," , ",").'</td>
						<td align="right">'.number_format($total_jml_tr_penerimaan,0, "," , ",").'</td>
					
					</tr>';
			$html.='<tr>
						<td colspan="2" align="right" >Selisih</td>
						<td></td>
						<td align="right">'.number_format($selisih,0, "," , ",").'</td>
						<td></td>
					</tr>';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="5" align="center">Data tidak ada</td>
				</tr>
			';		
			$baris++;
		}
		 
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+11;
		$print_area='A1:E'.$baris;
		$area_wrap='B9:C'.$baris;
		// $area_wrap='A8:F'.$baris;
		$area_kanan='C8:E'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:E7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:E7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A7:E7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
			// $objPHPExcel->getActiveSheet()
						// ->getPageSetup()
						// ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:E'.$baris)->applyFromArray($styleBorder);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanTransaksiSummary.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P','Laporan Transaksi Summary',$html);
		
		}
	}
	
}
?>