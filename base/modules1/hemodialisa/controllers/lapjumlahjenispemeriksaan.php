<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lapjumlahjenispemeriksaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             // $this->load->library('result');
             // $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakJumlahJenisPemeriksaan() {
		set_time_limit ( 1000 );
   		$title='LAPORAN JUMLAH JENIS PEMERIKSAAN';
		$param=json_decode($_POST['data']);

		$excel = $param->excel;
		$tglAwal = $param->tglAwal;
		$tglAkhir = $param->tglAkhir;
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tmptglawal = date('d-M-Y', strtotime($tglAwal));
		$tmptglakhir = date('d-M-Y', strtotime($tglAkhir));
		$tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
		$html='';
		
		$kd_unit= $this->db->query("select setting from sys_setting where key_data='hd_default_kd_unit'")->row()->setting;
		
		if($shift == 'All'){
			$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (1,2,3))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
			$shift="Semua Shift";
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (1)))  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (1,2)))  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( 1,3 ))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In (2)))  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( 2,3 ))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift = "AND ((db.Tgl_Transaksi between '" . $tglAwal . "' And  '" . $tglAkhir . "'  And db.Shift In ( 3 ))  
								Or  (db.Tgl_Transaksi between '" . $tomorrow . "'  And '" . $tomorrow2 . "'  And k.Shift=4) )  
								And t.kd_Unit = '".$kd_unit."' ";
				$shift="Shift 3";
			} 
		}
		
        

        $queryHasil = $this->db->query(" SELECT dt.KD_PRODUK, p.DESKRIPSI ,SUM(CASE WHEN K.ASAL_PASIEN = '2' THEN (DT.Qty) ELSE 0 END) as RWI, 
															 SUM(CASE WHEN K.ASAL_PASIEN = '1' THEN (DT.Qty) ELSE 0 END) as RWJ, 
															 SUM(CASE WHEN K.ASAL_PASIEN = '3' THEN (DT.Qty) ELSE 0 END) as IGD, COALESCE(Sum(x.Jumlah), 0) as jml 
											FROM Kunjungan k 
												INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi 
													And k.Urut_Masuk=t.Urut_Masuk  
												INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
												INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
												INNER JOIN 
													(SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, 
													dtc.urut_bayar, dtc.tgl_bayar, sum(dtc.Jumlah ) as Jumlah
													FROM ((Detail_TR_Bayar dtb 
														INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi 
															and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and dtc.kd_pay = dtb.kd_pay 
															and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar) 
														INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
													GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  dtc.urut_bayar, dtc.tgl_bayar, 
														dtb.Jumlah) x 
												ON x.kd_kasir = db.kd_kasir and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
													and x.tgl_bayar = db.tgl_transaksi and x.kd_kasir = dt.kd_kasir  and x.no_transaksi = dt.no_transaksi 
													and x.urut = dt.urut and x.tgl_transaksi = dt.tgl_transaksi 
												INNER JOIN Unit u On u.kd_unit=t.kd_unit 
												INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
												INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
											WHERE dt.kd_produk not in ('1235555') 
												" . $ParamShift . "
											GROUP BY dt.kd_produk, p.Deskripsi 
											ORDER BY  Deskripsi 
											
										  ");


        $query = $queryHasil->result();
        
       $baris=0;
            //-------------------------JUDUL ------------------------------------------------
            $html.='
				<table border="0">
					<tr>
						<th colspan="6">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="6">' .$tmptglawal. ' s/d ' .$tmptglakhir. '</th>
					</tr>
					<tr>
						<th colspan="6">' .$shift. '</th>
					</tr>
				</table>';

            //-------------------------ISI------------------------------------------------
        $html.='
					<table border = "1" >
					<thead>
					  <tr>
							<td align="center">No</td>
							<td align="center">Jenis Pemeriksaan</td>
							<td align="center">RWJ</td>
							<td align="center">RWI</td>
							<td align="center">IGD</td>
							<td align="center">Jumlah</td>
					  </tr>
					</thead> ';
		$baris=0;
		if (count($query) > 0){	
			$no = 0;
            $grand = 0;
            $totrwj = 0;
            $totrwi = 0;
            $totigd = 0;
            $totjml = 0;
            $subtotalharga = 0;
            foreach ($query as $line) {
				$baris++;
                $no++;
                $deskripsi = $line->deskripsi;
                $rwj = $line->rwj;
                $rwi = $line->rwi;
                $igd = $line->igd;
                $jml = $line->jml;
                $totalharga = ($rwj + $rwi + $igd) * $jml;

				$html.='
					<tr class="headerrow"> 
								<td align="center">' . $no . '</td>
								<td align="Left">' . $deskripsi . '</td>
								<td align="right">' . $rwj . '</td>
								<td align="right">' . $rwi . '</td>
								<td align="right">' . $igd . '</td>
								<td align="right">' . number_format($jml, 0, ',', ',') . '</td>
						</tr> ';
                $subtotalharga += $totalharga;
                $totrwj += $rwj;
                $totrwi += $rwi;
                $totigd += $igd;
                $totjml += $jml;
            }
            $html.='
				<tr> 
					<th align="right" colspan="2">Grand Total</th>
					<th align="right">' . $totrwj . '</th>
					<th align="right">' . $totrwi . '</th>
					<th align="right">' . $totigd . '</th>
					<th align="right">' . number_format($totjml, 0, ',', ',') . '</th>
				</tr> ';
		} else{
			 $html.='
				<tr> 
					<th  colspan="6">Data tidak ada</th>
				</tr> ';
		}
		$html.='</table>'; 
		// echo $html;
		$prop=array('foot'=>true);
		$baris=$baris+6;
		$print_area='A1:F'.$baris;
		$area_wrap='B5:F'.$baris;
		// $area_wrap='A8:F'.$baris;
		$area_kanan='C6:F'.$baris;
		if($excel == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			// $html_="<table><tr><td>test</td></tr></table>";
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:F5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:G5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
			// $objPHPExcel->getActiveSheet()
						// ->getPageSetup()
						// ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A5:F'.$baris)->applyFromArray($styleBorder);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapJumlahJenisPemeriksaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			$common=$this->common;
			$this->common->setPdf('P','Laporan Jumlah Jenis Pemeriksaan',$html);
			
		}
		
    }
	
	
}
?>