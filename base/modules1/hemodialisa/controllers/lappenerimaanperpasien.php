<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lappenerimaanperpasien extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getSelect(){
   		$result=$this->result;
   		$result->setData($this->db->query("SELECT A.kd_customer AS id,customer AS text FROM customer A
			INNER JOIN kontraktor B ON B.kd_customer=A.kd_customer WHERE B.jenis_cust=".$_POST['cust']." ORDER BY customer ASC")->result());
   		$result->end();
   	}
   	
   	public function getData(){
   		$result=$this->result;
   		$array=array();
   		$array['cust']=$this->db->query("SELECT kd_pay AS id,uraian AS text FROM payment ORDER BY uraian ASC")->result();
   		$result->setData($array);
   		$result->end();
   	}
	
   
   	public function doPrintPerpasien(){
		$common=$this->common;
		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$kd_customer = $param->kd_customer;
		$jenis_cust  = $param->jenis_cust;
		$tglAkhir  = $param->last_date;
		$tglAwal  = $param->start_date;
		$shift1  = $param->shift1;
		$shift2  = $param->shift2;
		$shift3  = $param->shift3;
		$kd_pay = substr($param->kd_pay, 0, -1);
		$html='';
		
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tmptglawal=date('d-M-Y',strtotime($tglAwal));
		$tmptglakhir=date('d-M-Y',strtotime($tglAkhir));
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		$kd_unit= $this->db->query("select setting from sys_setting where key_data='hd_default_kd_unit'")->row()->setting;
		
		if($kd_customer == ''){
			$paramKd_customer="";
		} else{
			$paramKd_customer=" and c.kd_Customer='".$kd_customer."'";
		}
		
		if($jenis_cust == ''){
			$paramJenis_cust="";
			$kel='SEMUA JENIS PASIEN';
		} else{
			$jenis_cust--;
			$paramJenis_cust=" and ktr.jenis_Cust=".$jenis_cust."";
			
			if($jenis_cust==0){
   				$kel='PERORANGAN';
   			}else if($jenis_cust==1){
   				$kel='PERUSAHAAN';
   			}else if($jenis_cust==2){
   				$kel='ANSURANSI';
   			}
		}
		
		//--------------------------------------------shift 3-------------------------------------------------------------
		if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='3';
		
		//--------------------------------------------shift 2-------------------------------------------------------------
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (2))) ";
			$shift='2';
			
		//--------------------------------------------shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (1))) ";
			$shift='1';
			
		//--------------------------------------------shift 3, shift 2-------------------------------------------------------------
		} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,2))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='2,3';
		
		//--------------------------------------------shift 3, shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,1))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) ) ";
			$shift='1,3';
		
		//--------------------------------------------shift 2, shift 1-------------------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (2,1))) ";
			$shift='1,2';
			
		//--------------------------------------------shift 1, shift 2, shift 3----------------------------------------------------
		} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'true'){
			$paramShift="Where t.kd_unit='".$kd_unit."'
								And ((db.tgl_transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And db.Shift In (3,2,1))  
								Or  (db.Tgl_Transaksi between '".$tomorrow."'  And '".$tomorrow2."'  And db.Shift=4) )";
			$shift='1,2,3';
		} 
		
		$qr_pay='';
		$qr_pay=" And (db.Kd_Pay in (".$kd_pay."))";
		
		/* if(isset($_POST['kd_pay'])){
   			$u='';
   			for($i=0;$i<count($_POST['kd_pay']) ; $i++){
   				if($u !=''){
   					$u.=', ';
   				}
   				$u.="'".$_POST['kd_pay'][$i]."'";
   			}
   			if(count($_POST['kd_pay'])>0){
   				$qr_pay=" And (db.Kd_Pay in (".$u."))";
   			}
   		}else{
   			$result->error();
   			$result->setMessage('Cara Pembayaran Tidak Boleh Kosong.');
   			$result->end();
   		} */
		
		$queryHasil = $this->db->query( " Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) as Nama, py.Uraian,  
												case when max(py.Kd_pay) in ('TU') Then Sum(Jumlah) Else 0 end as TU,  
												case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as SSD,  
												case when max(py.Kd_pay) in ('KR') Then Sum(Jumlah) Else 0 end as KR,  
												case when max(py.Kd_pay) not in ('KR') And  max(py.Kd_pay) not in ('TU') Then Sum(Jumlah) Else 0 end as PT, 
												c.customer  
											From (Transaksi t 
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
												INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
												INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk 
													And k.tgl_masuk=t.tgl_Transaksi  
												INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
												INNER JOIN Customer c on c.kd_customer=k.kd_customer  
												LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer 
												".$paramShift."
												".$paramJenis_cust."
												".$paramKd_customer."
												".$qr_pay."
												Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian, c.customer  
												Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut) 
											
										  ");

		//$mpdf=$common->getPDF('P','LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)');	
		$query = $queryHasil->result();
		//-------------------------JUDUL ------------------------------------------------
		$html.='
			<table border="0">
				<tbody>
					<tr>
						<th>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
					</tr>
					<tr>
						<th>'.$tmptglawal.' s/d '.$tmptglakhir.'</th>
					</tr>
					<tr>
						<th>'.$kel.' SHIFT '.$shift.'</th>
					</tr>
				</tbody>
			</table><br> ';
		//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
		$html.='
			<table class="t1" border = "1" style="overflow: wrap">
			<thead>
			  <tr>
					<th width="" align="center">NO</td>
					<th width="" align="center">NO TRANSAKSI</td>
					<th width="" align="center">NO. MEDREC</td>
					<th width="" align="center">NAMA PASIEN</td>
					<th width="" align="center">JENIS PENERIMAAN</td>
					<th width="" align="center">JUMLAH</td>
			  </tr>
			</thead> ';
		
		
		if(count($query) == 0)
		{
			$html.='
			<tbody>
			  <tr>
					<th colspan="6">Data ridak ada</td>
			  </tr>
			</tbody> ';
		}
		else {												
			$no=0;
			$grand=0;
			$totTU=0;
			$totPT=0;
			$totSSD=0;
			$totKR=0;
			$jumlah=0;
			foreach ($query as $line) 
			{
				$no++;       
				$no_transaksi=$line->no_transaksi;
				$tgl_transaksi=$line->tgl_transaksi;
				$kd_pasien=$line->kd_pasien;
				$nama = $line->nama;
				$uraian = $line->uraian;
				$tu = $line->tu;
				$ssd = $line->ssd;
				$kr = $line->kr;
				$pt = $line->pt;
				$jumlah=$tu + $pt + $ssd;
				
				$html.='
					<tbody>
						<tr class="headerrow"> 
								<td width="20" align="center">'.$no.'</td>
								<td width="40" align="center">'.$no_transaksi.'</td>
								<td width="40" align="center">'.$kd_pasien.'</td>
								<td width="80" align="left">'.$nama.'</td>
								<td width="70" align="left">'.$uraian.'</td>
								<td width="50" align="right">'.number_format($jumlah,0,',','.').'</td>
						</tr>

				';
				$grand +=$jumlah;
				$totTU +=$tu;
				$totPT +=$pt;
				$totSSD +=$ssd;
				$totKR +=$kr;
			}
			$html.='
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Jumlah</th>
						<th width="" align="right">'.number_format($grand,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Tunai</th>
						<th width="" align="right">'.number_format($totTU,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Piutang</th>
						<th width="" align="right">'.number_format($totPT,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Subsidi</th>
						<th width="" align="right">'.number_format($totSSD,0,',','.').'</th>
				</tr>
				
				<tr class="headerrow"> 
						<th width="" align="right" colspan="5">Total Penerimaan Kredit</th>
						<th width="" align="right">'.number_format($totKR,0,',','.').'</th>
				</tr> ';
							
		}	
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Penerimaan PerPasien PerJenis Penerimaan',$html);		
   	}
	
}
?>