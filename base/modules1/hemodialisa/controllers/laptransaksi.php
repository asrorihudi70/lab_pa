<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class laptransaksi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function getUser(){
		$result=$this->db->query("SELECT '1' as id,kd_user,user_names,full_name
									FROM zusers
									UNION 
									Select '0' as id,'9999' as kd_user, ' Semua' as user_names, 'Semua' as full_name Order By id,full_name asc")->result();
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function cetakTransaksi() {
        $common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN TRANSAKSI HEMODIALISA';
		$param=json_decode($_POST['data']);
		
		$asalpasien=$param->asal_pasien;
		$kdUser=$param->user;
		$periode=$param->periode;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$customer=$param->customer;
		$kdcustomer=$param->kdcustomer;
		$tipe=$param->tipe;
		$tglAwalAsli = $param->tglAwal;
		$tglAkhirAsli = $param->tglAkhir;
		$periode = $param->periode;
		
		$shift=$param->shift;
		$shift1=$param->shift1;
		$shift2=$param->shift2;
		$shift3=$param->shift3;
		
		$ParamShift2 = "";
		$ParamShift3 = "";
		
		$username = $this->db->query("select full_name from zusers where kd_user='".$kdUser."'")->row()->full_name;
		$res = $this->db->query("select setting from sys_setting where key_data='hd_default_kd_unit'")->row();
		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='hd_default_kd_kasir'")->row()->setting;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->setting."' and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->setting."' and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit='".$res->setting."' and kd_asal='3'")->row()->kd_kasir;
		
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y',strtotime($date2 . "+1 days"));
		
		/* $awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir))); */
		
		if($periode == 'tanggal'){
			$tAwal="'".$tglAwal."'";
			$tAkhir="'".$tglAkhir."'";
			$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		} else{
			$tAwal="date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date)";
			$tAkhir="date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval";
			$awal=tanggalstring(date('Y-m-d',strtotime($this->db->query(" select date_trunc('month', '".date('Y-m-d',strtotime($tglAwal))."'::date) as tawal")->row()->tawal)));
			$akhir=tanggalstring(date('Y-m-d',strtotime($this->db->query("select date_trunc('month', '".date('Y-m-d',strtotime($tglAkhir))."'::date)+'1month'::interval-'1day'::interval as takhir")->row()->takhir)));
		}

		$pisahtgl = explode("/", $tglAkhir, 3);
		if (count($pisahtgl) == 2) {

			$bulan = $pisahtgl[0];
			$tahun = $pisahtgl[1];
			$tmpbulan = $bulan;
			if ($bulan == 'Jan') {
				$bulan = '1';
			} elseif ($bulan == 'Feb') {
				$bulan = '2';
			} elseif ($bulan == 'Mar') {
				$bulan = '3';
			} elseif ($bulan == 'Apr') {
				$bulan = '4';
			} elseif ($bulan == 'May') {
				$bulan = '5';
			} elseif ($bulan == 'Jun') {
				$bulan = '6';
			} elseif ($bulan == 'Jul') {
				$bulan = '7';
			} elseif ($bulan == 'Aug') {
				$bulan = '8';
			} elseif ($bulan == 'Sep') {
				$bulan = '9';
			} elseif ($bulan == 'Oct') {
				$bulan = '10';
			} elseif ($bulan == 'Nov') {
				$bulan = '11';
			} elseif ($bulan == 'Dec') {
				$bulan = '12';
			}

			$jmlhari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$tglAkhir = $tahun . '-' . $bulan . '-' . $jmlhari;
			$tglAwal = '01/' . $tglAwal;
		}

		if ($asalpasien == 'Semua') {
			$tmpasalpasien = 'Semua Pasien';
		} else if ($asalpasien == 'RWJ/IGD') {
			$tmpasalpasien = 'Rawat Jalan / Gawat Darurat';
		} else if ($asalpasien == 'RWI') {
			$tmpasalpasien = 'Rawat Inap';
		} else {
			$tmpasalpasien = $asalpasien;
		}

		if ($tglAkhirAsli == $tglAkhir) {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tglAkhirAsli;
		} else {
			$tmptglawal = $tglAwalAsli;
			$tmptglakhir = $tmpbulan . '/' . $tahun;
		}
		
		if($shift == 'All'){
			$ParamShift2 = " WHERE pyt.Type_Data <=3 
								AND (((db.Tgl_Transaksi >= " . $tAwal . "  and db.Tgl_Transaksi <= " . $tAkhir . ") 
								AND db.shift in (1,2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
								OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))
														 ";
			$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
								and dt.shift in (1,2,3) AND not (dt.Tgl_Transaksi = '" . $tglAwal . "' and  dt.shift=4 ))
								or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))";
			$shift = 'Semua Shift';
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =1 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =1 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 1";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,2) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))) ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,2) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))  ";
				$shift="Shift 1 dan 2";
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . ") 
									AND db.shift in (1,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))
														  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (1,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))   ";
				$shift="Shift 1 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =2 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 )))";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =2 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 )))"; //untuk shif 4
				$shift = "Shift 2";
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <= " . $tAkhir . "') 
									AND db.shift in (2,3) AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . "  and dt.Tgl_Transaksi <= " . $tAkhir . ") 
									and dt.shift in (2,3) AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "' ))  ";
				$shift="Shift 2 dan 3";
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$ParamShift2 = " WHERE pyt.Type_Data <=3 
									AND (((db.Tgl_Transaksi >= " . $tAwal . " and db.Tgl_Transaksi <=  " . $tAkhir . ") 
									AND db.shift =3 AND not (db.Tgl_Transaksi = " . $tAwal . " and  db.shift=4 ))
									OR ( db.SHIFT=4 AND db.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$ParamShift3 = " WHERE (((dt.Tgl_Transaksi >= " . $tAwal . " and dt.Tgl_Transaksi <= " . $tAkhir . ")
									and dt.shift =3 AND not (dt.Tgl_Transaksi = " . $tAwal . " and  dt.shift=4 ))
									or ( dt.SHIFT=4 AND dt.TGL_TRANSAKSI between '" . $tomorrow . "' and '" . $tomorrow2 . "'))"; //untuk shif 4
				$shift="Shift 3";
			} 
		}
	   
		//---------------------------------jika kelompok pasien = SEMUA-------------------------------------------
		if ($customer == 'Semua') {
			$kriteria_customer1 = "";
			$kriteria_customer2 = "";
		} else {
			$kriteria_customer1 = " AND k.kd_customer = '" . $kdcustomer . "'";
			$kriteria_customer2 = " AND k.kd_customer = '" . $kdcustomer . "'";
		}

		if ($asalpasien == 'Semua') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."')
												AND db.kd_user = " . $kdUser . "";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."')
												AND dt.kd_user = " . $kdUser . "   ";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwj."','".$KdKasirRwi."')
												AND dt.kd_user = " . $kdUser . "";
		} else if ($asalpasien == 'RWJ/IGD') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND db.kd_user = " . $kdUser . "
												AND left(tr.kd_unit,1) in ('2','3')";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND left(tr.kd_unit,1) in ('2','3')
												AND dt.kd_user = " . $kdUser . " ";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirIGD."')
												AND dt.kd_user = " . $kdUser . "
												And left (p.kd_pasien,2) <> 'HD'";
		} else if ($asalpasien == 'RWI') {
			$kriteria1 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND db.kd_user = " . $kdUser . "
												AND left(tr.kd_unit,1) = '1'";

			$kriteria2 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND left(tr.kd_unit,1) = '1'
												AND dt.kd_user = " . $kdUser . "";
			$kriteria3 = " AND t.kd_kasir in ('".$KdKasirRwi."')
												AND dt.kd_user = " . $kdUser . "";
		} else {
			$kriteria1 = " AND t.kd_kasir in ('".$kd_kasir."')
												AND db.kd_user = " . $kdUser . "
												And left (p.kd_pasien,2)='HD'";

			$kriteria2 = " AND t.kd_kasir in ('".$kd_kasir."')
												And left (p.kd_pasien,2)='HD'
												AND dt.kd_user = " . $kdUser . "";
			$kriteria3 = " AND t.kd_kasir in ('".$kd_kasir."')
												AND dt.kd_user = " . $kdUser . "
												And left (p.kd_pasien,2)='HD'";
		}

        $queryHasil = $this->db->query(" 
											Select p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "" . $kriteria_customer1 . "" . $kriteria1 . "

											union 

											Select p.kd_pasien,(p.kd_pasien ||' '|| p.nama) as namaPasien
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "" . $kriteria_customer2 . "" . $kriteria3 . "


											");
        $query = $queryHasil->result();
		
		$html="";
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table border="0">
				<tbody>
					<tr>
						<th>Laporan Transaksi Harian ' . $tmpasalpasien . '</th>
					</tr>
					<tr>
						<th>Periode dari ' . $awal . ' s/d ' . $akhir . '</th>
					</tr>
					<tr>
						<th>Kelompok ' . $customer . ' ( ' . $shift . ' )</th>
					</tr>
					<tr>
						<th>Operator ' . ucwords($username) . '</th>
					</tr>
				</tbody>
			</table><br>';
		//---------------ISI-----------------------------------------------------------------------------------	
		$html.='
				<table border = "1" >
				<thead>
				  <tr>
						<th width="10">No</td>
						<th width="80" align="center">Pasien</td>
						<th width="100" align="center">Pemeriksaan</td>
						<th width="40" align="center">Qty</td>
						<th width="70" align="center">Jumlah Penerimaan</td>
						<th width="70" align="center">Jumlah Pemeriksaan</td>
						<th width="70" align="center">User</td>
				  </tr>
				</thead>';
        if (count($query) == 0) {
			$html.='
					<tbody>
						<tr>
							<th colspan="7">Data tidak ada</th>
						</tr> 
					</tbody>
				</table>';
        } else {
			$no=0;
			$grandpenerimaan = 0;
			$grandpemeriksaan = 0;
            foreach ($query as $line) {
                $no++;

                $html.='
						<tbody>
							<tr >
								<td align="center">' . $no . '</td>
								<th width="100" align="left" colspan="6">' . $line->namapasien . '</th>
							</tr> ';
                $kdPasien = $line->kd_pasien;
                $queryPenerimaan = $this->db->query(" 
											Select 1 as Bayar,t.No_transaksi ,t.kd_kasir ,(p.kd_pasien ||' '|| p.nama) as namaPasien,  db.Kd_Pay,
													py.Uraian as deskripsi,db.kd_user,db.Jumlah,  1 as QTY,db.Urut, db.tgl_transaksi,  'Penerimaan' as Head, k.kd_customer, tr.kd_unit
													from Detail_bayar db   
													inner join payment py on db.kd_pay=py.kd_Pay   
													inner join payment_type pyt on py.jenis_pay=pyt.jenis_pay
													inner join transaksi t on t.no_transaksi=db.no_transaksi and t.kd_kasir=db.kd_kasir   
													left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
													inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal    
													inner join unit u on u.kd_unit=t.kd_unit    
													inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk    
													inner join customer c on k.kd_customer=c.kd_Customer  
													left join kontraktor knt on c.kd_customer=knt.kd_Customer
													inner join pasien p on t.kd_pasien=p.kd_pasien    

											" . $ParamShift2 . "
											" . $kriteria_customer1 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria1 . "
											order by bayar, kd_pay,No_transaksi
											");
				$queryPemerikasaan = $this->db->query(" 
											select 0 as Tagih,t.No_Transaksi,t.kd_kasir,(p.kd_pasien ||' '|| p.nama) as namaPasien,  Dt.Kd_tarif,prd.Deskripsi,Dt.kd_user,
											(Dt.QTY* Dt.Harga) + CASE WHEN((SELECT SUM(Harga * QTY) 
																	FROM Detail_Bahan 
																	WHERE kd_kasir=t.kd_kasir
																	and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut)) IS NULL THEN 0 Else 
																			(SELECT SUM(Harga * QTY)  
																			FROM Detail_Bahan 
																			WHERE kd_kasir=t.kd_kasir
																			and no_transaksi=dt.no_transaksi and tgl_transaksi=dt.tgl_transaksi and urut=dt.urut) END as jumlah, 
											Dt.QTY,Dt.Urut, Dt.tgl_transaksi,  'Pemeriksaan' as Head, k.kd_customer, tr.kd_unit
											from detail_transaksi DT   
											inner join produk prd on DT.kd_produk=prd.kd_produk   
											inner join transaksi t on t.no_transaksi=dT.no_transaksi and t.kd_kasir=dT.kd_kasir   
											inner join unit u on u.kd_unit=t.kd_unit    
											inner join kunjungan k on t.kd_pasien = k.kd_pasien and t.kd_unit=k.kd_unit and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
											left join unit_asal ua on t.no_transaksi=ua.no_transaksi and t.kd_kasir=ua.kd_kasir
											inner join transaksi tr on tr.no_transaksi=ua.no_transaksi_asal and tr.kd_kasir=ua.kd_kasir_asal  
											inner join customer c on k.kd_customer=c.kd_Customer   
											left join kontraktor knt on c.kd_customer=knt.kd_Customer
											inner join pasien p on t.kd_pasien=p.kd_pasien 

											" . $ParamShift3 . "
											" . $kriteria_customer2 . "
											AND p.kd_pasien='" . $kdPasien . "'
											" . $kriteria2 . "
											");
                $query3 = $queryPenerimaan->result();
				$query2 = $queryPemerikasaan->result();
				$noo = 0;
				
				/* Query Pemeriksaan*/
				$sub_jumlah_pemeriksaan=0;
				foreach ($query2 as $line) {
					$noo++;
                    $namapasien = $line->namapasien;
                    $deskripsi = $line->deskripsi;
                    $qty = $line->qty;
                    $jumlah = $line->jumlah;
                    $kd_user = $line->kd_user;
					$sub_jumlah_pemeriksaan +=$jumlah;
                    $queryuser2 = $this->db->query("select full_name from zusers where kd_user= '$kd_user'")->row();
                    $namauser = $queryuser2->full_name;
					
						$html.='
						<tbody>
							<tr> 
								<td> </td>
								<th colspan="6" align="left">' . $line->head . '</th>
							</tr>
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="right">' . number_format($jumlah, 0, ',', '.') . '</td>
								<td width="70" align="center">' . ucwords($namauser) . '</td>
							</tr>
						<p>&nbsp;</p> ';               
                }
				
				/* Query Penerimaan*/
				$sub_jumlah_penerimaan=0;
                foreach ($query3 as $line) {
					$noo++;
                    $namapasien = $line->namapasien;
                    $deskripsi = $line->deskripsi;
                    $qty = $line->qty;
                    $jumlah = $line->jumlah;
                    $kd_user = $line->kd_user;
					$sub_jumlah_penerimaan +=$jumlah;
                    $queryuser2 = $this->db->query("select full_name from zusers where kd_user= '$kd_user'")->row();
                    $namauser = $queryuser2->full_name;
					
					$html.='
						<tbody>
							<tr> 
								<td> </td>
								<th colspan="6" align="left">' . $line->head . '</th>
							</tr>
							<tr> 
								<td> </td>
								<td> </td>
								<td width="100" colspan>' . $deskripsi . '</td>
								<td width="40" align="center">' . $qty . '</td>
								<td width="70" align="right">' . number_format($jumlah, 0, ',', '.') . '</td>
								<td width="70" align="center">-</td>
								<td width="70" align="center">' . ucwords($namauser) . '</td>
							</tr>
						<p>&nbsp;</p> ';     
                }
				
                $html.='
					<tr>
						<td width="10"> </td>
						<th align="right" colspan="3">Sub Total</th>
						<th width="70" align="right">' . number_format($sub_jumlah_penerimaan, 0, ',', '.') . '</th>
						<th width="70" align="right">' . number_format($sub_jumlah_pemeriksaan, 0, ',', '.') . '</th>
						<td width="70"> </td>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>';
                $grandpenerimaan += $sub_jumlah_penerimaan;
				$grandpemeriksaan += $sub_jumlah_pemeriksaan;
            }
            $html.='
				<tr>
					<th align="right" colspan="4">GRAND TOTAL</th>
					<th align="right" width="70">' . number_format($grandpenerimaan, 0, ',', '.') . '</th>
					<th align="right" width="70">' . number_format($grandpemeriksaan, 0, ',', '.') . '</th>
					<td width="70"> </td>
				</tr> ';
            $html.='</tbody></table>';
        }
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Laporan Transaksi Hemodialisa',$html);	
    }
	
}
?>