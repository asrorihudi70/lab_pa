<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewdetailkasirhemodialisa extends MX_Controller {
	public  $gkdbagian;
	public  $Status;
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
		date_default_timezone_set("Asia/Jakarta");
        try
        {
			$Status='false';
			$kdunit=$this->db->query("select setting from sys_setting where key_data = 'hd_default_kd_unit'")->row()->setting;
			$kdbagian=$this->db->query("select kd_bagian from unit where kd_unit='$kdunit'")->row()->kd_bagian;
			$this->load->model('kasir_penunjang/tblviewkasirlebrad');
			if (strlen($Params[4])!==0)
			{
				$this->db->where(str_replace("~", "'"," ".$Params[4]. " and kd_bagian =  '".$kdbagian."'   order by cito desc,no_transaksi,tgl_transaksi asc  limit 200 "  ) ,null, false) ;
				$res = $this->tblviewkasirlebrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}	else{
				$this->db->where(str_replace("~", "'", "lunas = 'false' and kd_bagian =  '".$kdbagian."'  and tgl_transaksi in('".date('Y-m-d 00:00:00')."') order by cito desc,no_transaksi,tgl_transaksi asc limit 200  ") ,null, false) ;
				$res = $this->tblviewkasirlebrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			}
        }
        catch(Exception $o)
        {
            echo '{success: false}';
        }
        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
    }
}

?>