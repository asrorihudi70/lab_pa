<?php

/**
 * @Author Agung, Asep
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionPembukuanInv extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	//strupper
	function getDataAutoKomplitTanah()
	{
		
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' ";
		}
		//$opsitambahbaru=$_POST['no_register'];
		$result=$this->db->query("SELECT distinct(INV.*), IT.LUAS_TANAH, IT.ALAMAT, IT.KETERANGAN,  ID.SUMBER_DANA, IP.PEROLEHAN, IKO.KONDISI,  IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB, ISA.SATUAN,  IV.VENDOR, IHT.HAK_TANAH, 
									 IT.ID_HAK_TANAH, IT.PENGGUNAAN,  ITI.TGL_TERIMA FROM INV_INVENTARIS INV  
									 INNER JOIN INV_TANAH IT ON INV.NO_REGISTER=IT.NO_REGISTER  
									 INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI  
									 INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN  
									 INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA  
									 INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
									 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  
									 INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA  
									 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR  
									 LEFT JOIN INV_HAK_TANAH IHT ON IHT.ID_HAK_TANAH = IT.ID_HAK_TANAH 
									 LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG 
									 $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataDetailAutoCompletePanelTanah()
	{
		
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' and INR.NO_REGISTER<>''";
		}
		//$opsitambahbaru=$_POST['no_register'];
		$result=$this->db->query("SELECT INV.*, IT.LUAS_TANAH, IT.ALAMAT, IT.KETERANGAN,  ID.SUMBER_DANA, IP.PEROLEHAN, IKO.KONDISI,  IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB, ISA.SATUAN,  IV.VENDOR, IHT.HAK_TANAH, 
									 IT.ID_HAK_TANAH, IT.PENGGUNAAN, INR.NO_REG , ITI.TGL_TERIMA FROM INV_INVENTARIS INV  
									 INNER JOIN INV_TANAH IT ON INV.NO_REGISTER=IT.NO_REGISTER  
									 INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI  
									 INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN  
									 INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA  
									 INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
									 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  
									 INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA  
									 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR  
									 LEFT JOIN INV_HAK_TANAH IHT ON IHT.ID_HAK_TANAH = IT.ID_HAK_TANAH 
									 LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG 
									 $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitBangunan()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' ";
		}
		$result=$this->db->query(" SELECT distinct(INV.*), IB.THN_BANGUN, IB.THN_SELESAI, IB.THN_GUNA, IB.JML_LANTAI,  IB.JML_LUAS, IB.KETERANGAN,
									 ID.SUMBER_DANA, IP.PEROLEHAN,  IKO.KONDISI, IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB,  
									 IKB.KATEGORI, ISA.SATUAN,  IV.VENDOR, IB.KD_STATUS_TANAH, IB.LUAS_TANAH, IST.STATUS_TANAH,
									 IB.BETON, IB.NO_KODE_TANAH, IB.ALAMAT_TANAH,  ITI.TGL_TERIMA
									 FROM INV_INVENTARIS INV  
									 INNER JOIN INV_BANGUNAN IB ON INV.NO_REGISTER=IB.NO_REGISTER  
									 INNER JOIN INV_KATEGORI_B IKB ON IB.KD_KATEGORI=IKB.KD_KATEGORI  
									 INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI  
									 INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN  
									 INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA  
									 INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
									 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  
									 INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA  
									 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR  
									 LEFT JOIN INV_STATUS_TANAH IST ON IST.KD_STATUS_TANAH = IB.KD_STATUS_TANAH 
									 LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG 
									 $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataDetailAutoCompletePanelBangunan()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' and INR.NO_REGISTER<>''";
		}
		$result=$this->db->query(" SELECT INV.*, IB.THN_BANGUN, IB.THN_SELESAI, IB.THN_GUNA, IB.JML_LANTAI,  IB.JML_LUAS, IB.KETERANGAN,
									 ID.SUMBER_DANA, IP.PEROLEHAN,  IKO.KONDISI, IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB, INR.NO_REG, 
									 IKB.KATEGORI, ISA.SATUAN,  IV.VENDOR, IB.KD_STATUS_TANAH, IB.LUAS_TANAH, IST.STATUS_TANAH,
									 IB.BETON, IB.NO_KODE_TANAH, IB.ALAMAT_TANAH,  ITI.TGL_TERIMA
									 FROM INV_INVENTARIS INV  
									 INNER JOIN INV_BANGUNAN IB ON INV.NO_REGISTER=IB.NO_REGISTER  
									 INNER JOIN INV_KATEGORI_B IKB ON IB.KD_KATEGORI=IKB.KD_KATEGORI  
									 INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI  
									 INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN  
									 INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA  
									 INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
									 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN  
									 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  
									 INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA  
									 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR  
									 LEFT JOIN INV_STATUS_TANAH IST ON IST.KD_STATUS_TANAH = IB.KD_STATUS_TANAH 
									 LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG 
									 $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitAngkutan()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' ";
		}
		$result=$this->db->query(" SELECT distinct(INV.*), IA.NO_RANGKA, IA.NO_POL, IA.PABRIK, IA.NO_MESIN,
								   IA.NO_STNK, IA.NO_SERI, IA.CC, IA.THN_BUAT, IA.KETERANGAN, 
								   IA.KD_MERK, ID.SUMBER_DANA, IP.PEROLEHAN,  IKO.KONDISI,
								   IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB,IM.KD_MERK,  IM.MERK_TYPE, IV.VENDOR 
								   FROM INV_INVENTARIS INV  
								   INNER JOIN INV_ANGKUTAN IA ON INV.NO_REGISTER=IA.NO_REGISTER  
								   INNER JOIN INV_MERK IM ON IA.KD_MERK=IM.KD_MERK  
								   INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI  
								   INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN  
								   INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA  
								   INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
								   INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  
								   INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA  
								   INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR  
								   LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG  
									 $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataDetailAutoCompletePanelAngkutan()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' and INR.NO_REGISTER<>'' ";
		}
		$result=$this->db->query(" SELECT INV.*, IA.NO_RANGKA, IA.NO_POL, IA.PABRIK, IA.NO_MESIN,
								   IA.NO_STNK, IA.NO_SERI, IA.CC, IA.THN_BUAT, IA.KETERANGAN, 
								   IA.KD_MERK, ID.SUMBER_DANA, IP.PEROLEHAN,INR.NO_REG ,  IKO.KONDISI,
								   IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB,IM.KD_MERK, IM.MERK_TYPE, IV.VENDOR 
								   FROM INV_INVENTARIS INV  
								   INNER JOIN INV_ANGKUTAN IA ON INV.NO_REGISTER=IA.NO_REGISTER  
								   INNER JOIN INV_MERK IM ON IA.KD_MERK=IM.KD_MERK  
								   INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI  
								   INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN  
								   INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA  
								   INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG  
								   INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV  
								   INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA  
								   INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR  
								   LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG  
									 $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitMerkTipeAngkutan()
	{
		$merktype=$_POST['text'];
		if($merktype == ''){
			$criteria="";
		} else{
			$criteria=" WHERE MERK_TYPE like upper('".$merktype."%') and LEFT(KD_MERK,1)='1' ";
		}
		$result=$this->db->query("  SELECT * FROM INV_MERK
								   $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitBarangLain()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' ";
		}
		$result=$this->db->query("  SELECT distinct(INV.*), IBL.SPESIFIKASI, IBL.JUMLAH, IBL.KETERANGAN, 
									 ID.SUMBER_DANA, IP.PEROLEHAN, ISA.SATUAN, 
									 IKO.KONDISI, IMB.NAMA_BRG, IMB.KD_INV, IK.NAMA_SUB, 
									 IM.MERK_TYPE, IBL.KD_MERK, 
									 IV.VENDOR FROM INV_INVENTARIS INV 
									 left JOIN INV_BRG_LAIN IBL ON INV.NO_REGISTER=IBL.NO_REGISTER 
									 left JOIN INV_MERK IM ON IBL.KD_MERK=IM.KD_MERK 
									 INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI 
									 INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN 
									 INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA 
									 INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
									 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN 
									 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV 
									 INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA 
									 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR 
									 LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG  
								   $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataDetailAutoCompletePanelBarangLain()
	{
		$noregister=$_POST['text'];
		if($noregister == ''){
			$criteria="";
		} else{
			$criteria=" WHERE INV.NO_REGISTER like '".$noregister."%' and INR.NO_REGISTER<>'' ";
		}
		$result=$this->db->query(" SELECT INV.*, IBL.SPESIFIKASI, IBL.JUMLAH, IBL.KETERANGAN, 
									 ID.SUMBER_DANA, IP.PEROLEHAN, ISA.SATUAN, 
									 IKO.KONDISI, IMB.NAMA_BRG,INR.NO_REG, IMB.KD_INV, IK.NAMA_SUB, 
									 IM.MERK_TYPE, IBL.KD_MERK, 
									 IV.VENDOR FROM INV_INVENTARIS INV 
									 left JOIN INV_BRG_LAIN IBL ON INV.NO_REGISTER=IBL.NO_REGISTER 
									 left JOIN INV_MERK IM ON IBL.KD_MERK=IM.KD_MERK 
									 INNER JOIN INV_KONDISI IKO ON INV.KD_KONDISI=IKO.KD_KONDISI 
									 INNER JOIN INV_PEROLEHAN IP ON INV.KD_PEROLEHAN=IP.KD_PEROLEHAN 
									 INNER JOIN INV_DANA ID ON INV.KD_DANA=ID.KD_DANA 
									 INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
									 INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN=ISA.KD_SATUAN 
									 INNER JOIN INV_KODE IK ON IMB.KD_INV=IK.KD_INV 
									 INNER JOIN INV_TRM_I ITI ON INV.NO_TERIMA=ITI.NO_TERIMA 
									 INNER JOIN INV_VENDOR IV ON ITI.KD_VENDOR=IV.KD_VENDOR 
									 LEFT JOIN INV_NO_REG INR ON INR.NO_TERIMA = INV.NO_TERIMA AND INR.NO_URUT_BRG = INV.NO_URUT_BRG  
								   $criteria
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitMerkTipeBarangLain()
	{
		$merktype=$_POST['text'];
		if($merktype == ''){
			$criteria="";
		} else{
			$criteria=" WHERE MERK_TYPE like upper('".$merktype."%') and LEFT(KD_MERK,1)='2' ";
		}
		$result=$this->db->query("  SELECT * FROM INV_MERK
								   $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	function getDataAutoKomplitSatuanBarangLain()
	{
		$satuan=$_POST['text'];
		if($satuan == ''){
			$criteria="";
		} else{
			$criteria=" WHERE SATUAN like upper('".$satuan."%')";
		}
		$result=$this->db->query("  SELECT * FROM INV_SATUAN
								   $criteria limit 10
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	public function getnoterimaotomatis()
	{
		$noterimaoto=$_POST['no_terima'];
		$result=$this->db->query("select * from inv_trm_i where no_terima like '".$noterimaoto."%' ")->result();
		$newNo=count($result)+1;
		if(strlen($newNo) == 1){
			$NoTerima='000'.$newNo;
		} else if(strlen($newNo) == 2){
			$NoTerima='00'.$newNo;
		} else if(strlen($newNo) == 3){
			$NoTerima='0'.$newNo;
		} else{
			$NoTerima=$newNo;
		}
		$otonoterima=$noterimaoto.'-'.$NoTerima;
		echo '{success:true, totalrecords:'.count($result).', noterimaotomatis:'.json_encode($otonoterima).', ListDataObj:'.json_encode($result).'}';
	}
	public function getDataGridAwalLookUp(){
		$result=$this->db->query("SELECT iti.no_terima, iv.kd_vendor, iv.vendor , iti.tgl_terima, iti.tgl_posting , iti.no_faktur 
									,itid.no_urut_brg, iti.no_spk, iti.tgl_spk ,imb.kd_inv , imb.nama_brg 
									, itid.no_urut , isat.satuan , itid.jumlah_in, itid.harga_beli
									, itid.jumlah_in * itid.harga_beli as sub_total,
									CASE 
										WHEN iti.status_posting=0 THEN 'No'
										WHEN iti.status_posting=1 THEN 'Yes'
									END AS status, iti.keterangan
									FROM inv_trm_i iti
									left join inv_trm_i_det itid on iti.no_terima=itid.no_terima
									left join inv_vendor iv on iti.kd_vendor=iv.kd_vendor
									left join inv_master_brg imb on itid.no_urut_brg=imb.no_urut_brg
									left join inv_satuan isat on imb.kd_satuan=isat.kd_satuan
									where iti.no_terima='".$_POST['text']."'
									ORDER BY itid.no_urut asc	
									Limit 50
									")->result();
		foreach ($result as $row)
		{
			$noterima = $row->no_terima;
			$vendor= $row->vendor;
			$nofaktur= $row->no_faktur;
			$nospk= $row->no_spk;
			$tglspk= $row->tgl_spk;
			$ket= $row->keterangan;
			$nourut = $row->no_urut;
			$status = $row->status;
		}
		echo '{success:true, noterima:'.json_encode($noterima).', vendor:'.json_encode($vendor).',nourut:'.json_encode($nourut).', nofaktur:'.json_encode($nofaktur).', status:'.json_encode($status).', nospk:'.json_encode($nospk).', tglspk:'.json_encode($tglspk).' , ket:'.json_encode($ket).' , totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	public function getBarang(){			
		$result=$this->db->query("SELECT * from inv_master_brg
									where kd_inv like  '".$_POST['text']."%'
									ORDER BY no_urut_brg")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	public function getSatuan(){			
		$result=$this->db->query("SELECT * from inv_satuan
									where satuan like upper('".$_POST['text']."%')
									ORDER BY kd_satuan")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	public function getGridBahanLoad(){
		$result=$this->db->query("SELECT mb.no_minta, mb.kd_bahan, b.nama_bahan, b.kd_satuan,s.satuan, mb.qty, mb.ket_spek
									FROM gz_minta_bahan_detail mb
										INNER JOIN gz_bahan b on b.kd_bahan=mb.kd_bahan
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE mb.no_minta='".$_POST['no_minta']."'
									ORDER BY mb.kd_bahan, b.nama_bahan
									")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getBahan(){	
		$result=$this->db->query("SELECT b.kd_bahan, b.nama_bahan, b.kd_satuan, s.satuan, 0 as qty
									FROM gz_bahan b
										INNER JOIN gz_satuan s on s.kd_satuan=b.kd_satuan
									WHERE upper(b.nama_bahan) like upper('".$_POST['text']."%')
									ORDER BY b.kd_bahan limit 10
						")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	function getNoMintaBahan(){
		//MB201509001
		$thisMonth=(int)date("m");
		$thisYear=(int)date("Y");
		if(strlen($thisMonth) == 1){
			$thisMonth='0'.$thisMonth;
		} else{
			$thisMonth=$thisMonth;
		}
		
		$query = $this->db->query("SELECT no_minta FROM gz_minta_bahan where EXTRACT(MONTH FROM tgl_minta) =   ".$thisMonth." 
									and EXTRACT(year FROM tgl_minta) = '".$thisYear."' order by no_minta desc limit 1")->row();
		
		if($query){
			$no_minta=substr($query->no_minta,-3);
			$newNo=$no_minta+1;
			if(strlen($newNo) == 1){
				$NoMinta='MB'.$thisYear.$thisMonth.'00'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoMinta='MB'.$thisYear.$thisMonth.'0'.$newNo;
			} else{
				$NoMinta='MB'.$thisYear.$thisMonth.$newNo;
			}
		} else{
			$NoMinta='MB'.$thisYear.$thisMonth.'001';
		}
		
		return $NoMinta;
	}
	
	public function cekTerimaBahan(){
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		
		if(count($qCek) > 0){
			echo "{success:false}";
		} else{
			echo "{success:true}";
		}
	}
	
	public function save(){
		$noregister=$_POST['NoRegister'];
		$panel=$_POST['Panel'];
		if (strlen($noregister)==4)
		{
			$result=$this->db->query("select * from inv_inventaris where no_register like '".$noregister."%' ")->result();
			$hasil_q=count($result);
			$newNo=count($result)+1;
			if(strlen($newNo) == 1){
				$NoRegisterOto='000'.$newNo;
			} else if(strlen($newNo) == 2){
				$NoRegisterOto='00'.$newNo;
			} else if(strlen($newNo) == 3){
				$NoRegisterOto='0'.$newNo;
			} else{
				$NoRegisterOto=$newNo;
			}
			$HasilNoRegisterOto=$noregister.$NoRegisterOto;
		}
		else
		{
			$result=$this->db->query("select * from inv_inventaris where no_register='".$noregister."' ")->result();
			$hasil_q=count($result);
			$HasilNoRegisterOto=$noregister;
		}
		//tanah
		if ($panel=='tanah')
		{
			$KodeKelompok = $_POST['KdKelompok'];
			$Urutan = $_POST['Urutan'];
			$LuasTanah = $_POST['LuasTanah'];
			$Alamat = $_POST['Alamat'];
			$HakTanah = $_POST['HakTanah'];
			$TglBuku = $_POST['TglBuku'];
			$Kelompok = $_POST['Kelompok'];
			$Tanah = $_POST['Tanah'];
			$NoSertifikat=$_POST['NoSertifikat'];
			$TglSertifikat = $_POST['TglSertifikat'];
			$DigunakanUntuk = $_POST['DigunakanUntuk'];
			$NoTerima = $_POST['NoTerima'];
			$Perolehan = $_POST['Perolehan'];
			$TglPerolehan=$_POST['TglPerolehan'];
			$SumberDana = $_POST['SumberDana'];
			$TglTerima = $_POST['TglTerima'];
			$Vendor = $_POST['Vendor'];
			$Kondisi = $_POST['Kondisi'];
			$Harga=$_POST['Harga'];
			$Pengurus = $_POST['Pengurus'];
			$AlamatPengurus = $_POST['AlamatPengurus'];
			$Keterangan=$_POST['Keterangan'];
			$rec=$_POST['jumlahrecord'];
			$this->db->trans_begin();
			$query=$this->db->query("select * from inv_inventaris where no_register = '".$HasilNoRegisterOto."'");
			$hasil=count($query->result());
			$q_sisa=$this->db->query("SELECT iti.no_terima,itid.no_urut_brg, itid.no_urut , isat.satuan , itid.jumlah_in, itid.sisa
											FROM inv_trm_i iti
											left join inv_trm_i_det itid on iti.no_terima=itid.no_terima
											left join inv_vendor iv on iti.kd_vendor=iv.kd_vendor
											left join inv_master_brg imb on itid.no_urut_brg=imb.no_urut_brg
											left join inv_satuan isat on imb.kd_satuan=isat.kd_satuan
											where iti.no_terima='".$NoTerima."' and itid.no_urut_brg='".$Urutan."'
											ORDER BY itid.no_urut asc ")->result();	
			foreach ($q_sisa as $row)
			{
				$sisa = $row->sisa;	
			}
			$hasilsisa=$sisa-$rec;
			if ($sisa<>0)
			{
				if($hasil==0)
				{ 
					$simpan_inventaris = $this->db->query("insert into inv_inventaris (no_register,no_urut_brg,kd_perolehan,kd_dana,kd_kondisi,no_terima,tgl_buku,hrg_buku,tgl_perolehan,nama_pengurus,alamat_pengurus,no_dokumen,tgl_dokumen)
												values('".$HasilNoRegisterOto."','".$Urutan."','".$Perolehan."','".$SumberDana."','".$Kondisi."','".$NoTerima."','".$TglBuku."','".$Harga."','".$TglPerolehan."','".$Pengurus."','".$AlamatPengurus."','".$NoSertifikat."','".$TglSertifikat."')");
					$simpan_tanah = $this->db->query("insert into inv_tanah (no_register,kd_kondisi,luas_tanah,alamat,keterangan,id_hak_tanah,penggunaan)
												values('".$HasilNoRegisterOto."','".$Kondisi."','".$LuasTanah."','".$Alamat."','".$Keterangan."','".$HakTanah."','".$DigunakanUntuk."')");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				else
				{
					$simpan_inventaris = $this->db->query("update inv_inventaris set no_urut_brg='".$Urutan."' , kd_perolehan='".$Perolehan."', kd_dana='".$SumberDana."' , kd_kondisi='".$Kondisi."', no_terima='".$NoTerima."', tgl_buku='".$TglBuku."', hrg_buku='".$Harga."', tgl_perolehan='".$TglPerolehan."', nama_pengurus='".$Pengurus."', alamat_pengurus='".$AlamatPengurus."', no_dokumen='".$NoSertifikat."', tgl_dokumen='".$TglSertifikat."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_tanah = $this->db->query("update inv_tanah set kd_kondisi='".$Kondisi."', luas_tanah='".$LuasTanah."', alamat='".$Alamat."', keterangan='".$Keterangan."', id_hak_tanah='".$HakTanah."', penggunaan='".$DigunakanUntuk."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				for ($awal=0;$awal < $rec;$awal++)
				{
					$noRegBarang=$_POST['NoRegBarang-'.$awal];
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."' ");
					$hasil2=count($query2->result());
					$result = $this->db->query("update inv_no_reg set no_register='".$HasilNoRegisterOto."', tag_buku='1' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."' ");	
				}
				
				if($simpan_inventaris && $simpan_tanah && $simpan_penerimaan)
				{
				$this->db->trans_commit();
				echo '{success: true, simpan: true, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
				else
				{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
			}
			else
			{
				echo '{success: false, sisa: '.json_encode($sisa).'}';
			}
		}
		//bangunan
		else if ($panel=='bangunan')
		{
			$KodeKelompok = $_POST['KdKelompok'];
			$Urutan = $_POST['Urutan'];
			$LuasLantai=$_POST['LuasLantai'];
			$JumlahLantai=$_POST['JumlahLantai'];
			$KonstBeton=$_POST['KonstBeton'];
			$LuasTanah = $_POST['LuasTanah'];
			$Alamat = $_POST['Alamat'];
			$TglBuku = $_POST['TglBuku'];
			$Kelompok = $_POST['Kelompok'];
			$Bangunan = $_POST['Bangunan'];
			$Dokumen=$_POST['Dokumen'];
			$ThnAwal=$_POST['ThnAwal'];
			$ThnAkhir=$_POST['ThnAkhir'];
			$StatusTanah=$_POST['StatusTanah'];
			$ThnGuna=$_POST['ThnGuna'];
			$TglDokumen = $_POST['TglDokumen'];
			$NoSertifikat=$_POST['NoSertifikat'];
			$Kategori=$_POST['Kategori'];
			$NoTerima = $_POST['NoTerima'];
			$Perolehan = $_POST['Perolehan'];
			$TglPerolehan=$_POST['TglPerolehan'];
			$SumberDana = $_POST['SumberDana'];
			$TglTerima = $_POST['TglTerima'];
			$Vendor = $_POST['Vendor'];
			$Kondisi = $_POST['Kondisi'];
			$Harga=$_POST['Harga'];
			$Pengurus = $_POST['Pengurus'];
			$AlamatPengurus = $_POST['AlamatPengurus'];
			$Keterangan=$_POST['Keterangan'];
			$rec=$_POST['jumlahrecord'];
			$this->db->trans_begin();
			$query=$this->db->query("select * from inv_inventaris where no_register = '".$HasilNoRegisterOto."'");
			$hasil=count($query->result());
			$q_sisa=$this->db->query("SELECT iti.no_terima,itid.no_urut_brg, itid.no_urut , isat.satuan , itid.jumlah_in, itid.sisa
											FROM inv_trm_i iti
											left join inv_trm_i_det itid on iti.no_terima=itid.no_terima
											left join inv_vendor iv on iti.kd_vendor=iv.kd_vendor
											left join inv_master_brg imb on itid.no_urut_brg=imb.no_urut_brg
											left join inv_satuan isat on imb.kd_satuan=isat.kd_satuan
											where iti.no_terima='".$NoTerima."' and itid.no_urut_brg='".$Urutan."'
											ORDER BY itid.no_urut asc ")->result();	
			foreach ($q_sisa as $row)
			{
				$sisa = $row->sisa;	
			}
			$hasilsisa=$sisa-$rec;
			if ($sisa<>0)
			{
				if($hasil==0)
				{ 
					$simpan_inventaris = $this->db->query("insert into inv_inventaris (no_register,no_urut_brg,kd_perolehan,kd_dana,kd_kondisi,no_terima,tgl_buku,hrg_buku,tgl_perolehan,nama_pengurus,alamat_pengurus,no_dokumen,tgl_dokumen)
												values('".$HasilNoRegisterOto."','".$Urutan."','".$Perolehan."','".$SumberDana."','".$Kondisi."','".$NoTerima."','".$TglBuku."','".$Harga."','".$TglPerolehan."','".$Pengurus."','".$AlamatPengurus."','".$Dokumen."','".$TglDokumen."')");
					$simpan_bangunan = $this->db->query("insert into inv_bangunan (no_register,kd_kategori,thn_bangun,thn_selesai,thn_guna,jml_lantai,jml_luas,keterangan,luas_tanah,kd_status_tanah,no_kode_tanah,alamat_tanah,beton)
												values('".$HasilNoRegisterOto."','".$Kategori."','".$ThnAwal."','".$ThnAkhir."','".$ThnGuna."','".$JumlahLantai."','".$LuasLantai."','".$Keterangan."','".$LuasTanah."','".$StatusTanah."','".$NoSertifikat."','".$Alamat."','".$KonstBeton."')");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				else
				{
					$simpan_inventaris = $this->db->query("update inv_inventaris set no_urut_brg='".$Urutan."' , kd_perolehan='".$Perolehan."', kd_dana='".$SumberDana."' , kd_kondisi='".$Kondisi."', no_terima='".$NoTerima."', tgl_buku='".$TglBuku."', hrg_buku='".$Harga."', tgl_perolehan='".$TglPerolehan."', nama_pengurus='".$Pengurus."', alamat_pengurus='".$AlamatPengurus."', no_dokumen='".$NoSertifikat."', tgl_dokumen='".$TglDokumen."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_bangunan = $this->db->query("update inv_bangunan set kd_kategori='".$Kategori."', thn_bangun='".$ThnAwal."', thn_selesai='".$ThnAkhir."', thn_guna='".$ThnGuna."', jml_lantai='".$JumlahLantai."', jml_luas='".$LuasLantai."',keterangan='".$Keterangan."', luas_tanah='".$LuasTanah."', kd_status_tanah='".$StatusTanah."', no_kode_tanah='".$NoSertifikat."', alamat_tanah='".$Alamat."', beton='".$KonstBeton."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				for ($awal=0;$awal < $rec;$awal++)
				{
					$noRegBarang=$_POST['NoRegBarang-'.$awal];
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."'");
					$hasil2=count($query2->result());
					$result = $this->db->query("update inv_no_reg set no_register='".$HasilNoRegisterOto."', tag_buku='1' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."' ");	
				}
				
				if($simpan_inventaris && $simpan_bangunan && $simpan_penerimaan)
				{
				$this->db->trans_commit();
				echo '{success: true, simpan: true, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
				else
				{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
			}
			else
			{
				echo '{success: false, sisa: '.json_encode($sisa).'}';
			}
		}
		//angkutan
		else if ($panel=='angkutan')
		{
			$KodeKelompok = $_POST['KdKelompok'];
			$Urutan = $_POST['Urutan'];
			$KodeMerk=$_POST['KodeMerk'];
			$MerkTipe=$_POST['MerkTipe'];
			$ThnProduksi=$_POST['ThnProduksi'];
			$NoSeri=$_POST['NoSeri'];
			$NoMesin = $_POST['NoMesin'];
			$TglBuku = $_POST['TglBuku'];
			$Kelompok = $_POST['Kelompok'];
			$NamaAlat = $_POST['NamaAlat'];
			$Dokumen=$_POST['Dokumen'];
			$TglDokumen = $_POST['TglDokumen'];
			$Pabrik=$_POST['Pabrik'];
			$NoRangka=$_POST['NoRangka'];
			$CC=$_POST['CC'];
			$NoPol=$_POST['NoPol'];
			$NoBPKB=$_POST['NoBPKB'];
			$NoTerima = $_POST['NoTerima'];
			$Perolehan = $_POST['Perolehan'];
			$TglPerolehan=$_POST['TglPerolehan'];
			$SumberDana = $_POST['SumberDana'];
			$TglTerima = $_POST['TglTerima'];
			$Vendor = $_POST['Vendor'];
			$Kondisi = $_POST['Kondisi'];
			$Harga=$_POST['Harga'];
			$Pengurus = $_POST['Pengurus'];
			$AlamatPengurus = $_POST['AlamatPengurus'];
			$Keterangan=$_POST['Keterangan'];
			$rec=$_POST['jumlahrecord'];
			$this->db->trans_begin();
			$query=$this->db->query("select * from inv_inventaris where no_register = '".$HasilNoRegisterOto."'");
			$hasil=count($query->result());
			$q_sisa=$this->db->query("SELECT iti.no_terima,itid.no_urut_brg, itid.no_urut , isat.satuan , itid.jumlah_in, itid.sisa
											FROM inv_trm_i iti
											left join inv_trm_i_det itid on iti.no_terima=itid.no_terima
											left join inv_vendor iv on iti.kd_vendor=iv.kd_vendor
											left join inv_master_brg imb on itid.no_urut_brg=imb.no_urut_brg
											left join inv_satuan isat on imb.kd_satuan=isat.kd_satuan
											where iti.no_terima='".$NoTerima."' and itid.no_urut_brg='".$Urutan."'
											ORDER BY itid.no_urut asc ")->result();	
			foreach ($q_sisa as $row)
			{
				$sisa = $row->sisa;	
			}
			$hasilsisa=$sisa-$rec;
			if ($sisa<>0)
			{
				if($hasil==0)
				{ 
					$simpan_inventaris = $this->db->query("insert into inv_inventaris (no_register,no_urut_brg,kd_perolehan,kd_dana,kd_kondisi,no_terima,tgl_buku,hrg_buku,tgl_perolehan,nama_pengurus,alamat_pengurus,no_dokumen,tgl_dokumen)
												values('".$HasilNoRegisterOto."','".$Urutan."','".$Perolehan."','".$SumberDana."','".$Kondisi."','".$NoTerima."','".$TglBuku."','".$Harga."','".$TglPerolehan."','".$Pengurus."','".$AlamatPengurus."','".$Dokumen."','".$TglDokumen."')");
					$simpan_angkutan = $this->db->query("insert into inv_angkutan (no_register,kd_merk,no_rangka,no_pol,pabrik,no_mesin,no_stnk,no_seri,cc,keterangan,thn_buat)
												values('".$HasilNoRegisterOto."','".$KodeMerk."','".$NoRangka."','".$NoPol."','".$Pabrik."','".$NoMesin."','".$NoBPKB."','".$NoSeri."','".$CC."','".$Keterangan."','".$ThnProduksi."')");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				else
				{
					$simpan_inventaris = $this->db->query("update inv_inventaris set no_urut_brg='".$Urutan."' , kd_perolehan='".$Perolehan."', kd_dana='".$SumberDana."' , kd_kondisi='".$Kondisi."', no_terima='".$NoTerima."', tgl_buku='".$TglBuku."', hrg_buku='".$Harga."', tgl_perolehan='".$TglPerolehan."', nama_pengurus='".$Pengurus."', alamat_pengurus='".$AlamatPengurus."', no_dokumen='".$NoSertifikat."', tgl_dokumen='".$TglDokumen."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_angkutan = $this->db->query("update inv_angkutan set kd_merk='".$KodeMerk."', no_rangka='".$NoRangka."', no_pol='".$NoPol."', pabrik='".$Pabrik."', no_mesin='".$NoMesin."', no_stnk='".$NoBPKB."',no_seri='".$NoSeri."', cc='".$CC."', keterangan='".$Keterangan."', thn_buat='".$ThnProduksi."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				for ($awal=0;$awal < $rec;$awal++)
				{
					$noRegBarang=$_POST['NoRegBarang-'.$awal];
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."'");
					$hasil2=count($query2->result());
					$result = $this->db->query("update inv_no_reg set no_register='".$HasilNoRegisterOto."', tag_buku='1' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."' ");	
				}
				
				if($simpan_inventaris && $simpan_angkutan && $simpan_penerimaan)
				{
				$this->db->trans_commit();
				echo '{success: true, simpan: true, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
				else
				{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
			}
			else
			{
				echo '{success: false, sisa: '.json_encode($sisa).'}';
			}
		}
		//baranglain
		else if ($panel=='baranglain')
		{
			$KodeKelompok = $_POST['KdKelompok'];
			$Urutan = $_POST['Urutan'];
			$KodeMerk=$_POST['KodeMerk'];
			$MerkTipe=$_POST['MerkTipe'];
			$Jumlah=$_POST['Jumlah'];
			$Spesifikasi=$_POST['Spesifikasi'];
			$TglBuku = $_POST['TglBuku'];
			$Kelompok = $_POST['Kelompok'];
			$Barang = $_POST['Barang'];
			$Dokumen=$_POST['Dokumen'];
			$TglDokumen = $_POST['TglDokumen'];
			$Satuan=$_POST['Satuan'];
			$NoTerima = $_POST['NoTerima'];
			$Perolehan = $_POST['Perolehan'];
			$TglPerolehan=$_POST['TglPerolehan'];
			$SumberDana = $_POST['SumberDana'];
			$TglTerima = $_POST['TglTerima'];
			$Vendor = $_POST['Vendor'];
			$Kondisi = $_POST['Kondisi'];
			$Harga=$_POST['Harga'];
			$Pengurus = $_POST['Pengurus'];
			$AlamatPengurus = $_POST['AlamatPengurus'];
			$Keterangan=$_POST['Keterangan'];
			$rec=$_POST['jumlahrecord'];
			$this->db->trans_begin();
			$query=$this->db->query("select * from inv_inventaris where no_register = '".$HasilNoRegisterOto."'");
			$hasil=count($query->result());
			$q_sisa=$this->db->query("SELECT iti.no_terima,itid.no_urut_brg, itid.no_urut , isat.satuan , itid.jumlah_in, itid.sisa
											FROM inv_trm_i iti
											left join inv_trm_i_det itid on iti.no_terima=itid.no_terima
											left join inv_vendor iv on iti.kd_vendor=iv.kd_vendor
											left join inv_master_brg imb on itid.no_urut_brg=imb.no_urut_brg
											left join inv_satuan isat on imb.kd_satuan=isat.kd_satuan
											where iti.no_terima='".$NoTerima."' and itid.no_urut_brg='".$Urutan."'
											ORDER BY itid.no_urut asc ")->result();	
			foreach ($q_sisa as $row)
			{
				$sisa = $row->sisa;	
			}
			$hasilsisa=$sisa-$rec;
			if ($sisa<>0)
			{
				if($hasil==0)
				{ 
					$simpan_inventaris = $this->db->query("insert into inv_inventaris (no_register,no_urut_brg,kd_perolehan,kd_dana,kd_kondisi,no_terima,tgl_buku,hrg_buku,tgl_perolehan,nama_pengurus,alamat_pengurus,no_dokumen,tgl_dokumen)
												values('".$HasilNoRegisterOto."','".$Urutan."','".$Perolehan."','".$SumberDana."','".$Kondisi."','".$NoTerima."','".$TglBuku."','".$Harga."','".$TglPerolehan."','".$Pengurus."','".$AlamatPengurus."','".$Dokumen."','".$TglDokumen."')");
					$simpan_barang_lain = $this->db->query("insert into inv_brg_lain (no_register,kd_merk,no_seri,spesifikasi,jumlah,keterangan)
												values('".$HasilNoRegisterOto."','".$KodeMerk."','','".$Spesifikasi."','".$Jumlah."','".$Keterangan."')");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				else
				{
					$simpan_inventaris = $this->db->query("update inv_inventaris set no_urut_brg='".$Urutan."' , kd_perolehan='".$Perolehan."', kd_dana='".$SumberDana."' , kd_kondisi='".$Kondisi."', no_terima='".$NoTerima."', tgl_buku='".$TglBuku."', hrg_buku='".$Harga."', tgl_perolehan='".$TglPerolehan."', nama_pengurus='".$Pengurus."', alamat_pengurus='".$AlamatPengurus."', no_dokumen='".$NoSertifikat."', tgl_dokumen='".$TglDokumen."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_barang_lain = $this->db->query("update inv_brg_lain set kd_merk='".$KodeMerk."',no_seri='', spesifikasi='".$Spesifikasi."', jumlah='".$Jumlah."', keterangan='".$Keterangan."' where no_register='".$HasilNoRegisterOto."' ");
					$simpan_penerimaan= $this->db->query("update inv_trm_i_det set sisa='".$hasilsisa."' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' ");
				}
				for ($awal=0;$awal < $rec;$awal++)
				{
					$noRegBarang=$_POST['NoRegBarang-'.$awal];
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."'");
					$hasil2=count($query2->result());
					$result = $this->db->query("update inv_no_reg set no_register='".$HasilNoRegisterOto."', tag_buku='1' where no_terima='".$NoTerima."' and no_urut_brg='".$Urutan."' and no_reg='".$noRegBarang."' ");	
				}
				
				if($simpan_inventaris && $simpan_barang_lain && $simpan_penerimaan)
				{
				$this->db->trans_commit();
				echo '{success: true, simpan: true, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
				else
				{
				$this->db->trans_rollback();
				echo '{success: false, simpan: false, records: '.json_encode($rec).' , sisa: '.json_encode($sisa).', noregister:'.json_encode($HasilNoRegisterOto).'}';
				}
			}
			else
			{
				echo '{success: false, sisa: '.json_encode($sisa).'}';
			}
		}
			
	}
	public function posting(){
		$NoTerima = $_POST['p_noterima'];
		$StatusPosting = 1;
		$rec=$_POST['jumlahrecord'];	
		$query=$this->db->query("select * from inv_trm_i where no_terima = '".$NoTerima."'");
		$hasil=count($query->result());
		//$query = $this->tb_minta_umum_det->GetRowList(0,1,"","","");
			if($hasil==0)
			{ 
				echo '{rec: false, ';
			}
			else
			{
				$simpan = $this->db->query("update inv_trm_i set status_posting='".$StatusPosting."' where no_terima='".$NoTerima."' ");
				for ($awal=0;$awal < $rec;$awal++)
				{
					$kd_kelompok=$_POST['kodeKelompokTerimaInv-'.$awal];
					$no_urut_brg=$_POST['noUrutBrgTerimaInv-'.$awal];
					$criteria = "no_terima = '".$NoTerima."' ";
					$query2=$this->db->query("select * from inv_no_reg where no_terima = '".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");
					$hasil2=count($query2->result());
					
					$newNo=$hasil+$awal;
					if(strlen($newNo) == 1){
						$NoReg='00000'.$newNo;
					} else if(strlen($newNo) == 2){
						$NoReg='0000'.$newNo;
					} else if(strlen($newNo) == 3){
						$NoReg='000'.$newNo;
					} else if(strlen($newNo) == 4){
						$NoReg='00'.$newNo;
					} else if(strlen($newNo) == 5){
						$NoReg='0'.$newNo;
					}
					  else{
						$NoReg=$newNo;
					}
					$RegNoReg=$kd_kelompok.$NoReg;
					
					if($hasil2==0)
					{ 	
						$result = $this->db->query("insert into inv_no_reg (no_terima,no_urut_brg,no_reg,tag_buku,kd_cek_status)
												values('".$NoTerima."','".$no_urut_brg."','".$RegNoReg."',0,0)");	
					}
					else
					{
						$result = $this->db->query("update inv_no_reg set no_reg='".$RegNoReg."' where no_terima='".$NoTerima."' and no_urut_brg='".$no_urut_brg."' ");	
					}
				}
				
				echo '{rec: true, ';
			}
		
			if($result && $simpan)
			{
			echo 'success: true, simpan: true, records: '.json_encode($rec).' }';
			}
			else
			{
			echo '{success: false, simpan: false, records: '.json_encode($rec).'}';
			}
	}
	public function deleteSemuaPenerimaan(){
		//$this->db->trans_begin();
		$no_terima = $_POST['no_terima'];
		//CEK JIKA PERMINTAAN DIET SUDAH DIORDER
		/*$qCek = $this->db->query("SELECT * FROM gz_order_detail 
										WHERE no_minta='".$no_terima."'")->result();
		if(count($qCek) <> 0){
				echo "{success:false,order:'true'}";
		} else {*/
			$qDelDetail = $this->db->query("delete from inv_trm_i_det
								where no_terima='".$no_terima."' ");
			if($qDelDetail){
				
				$qDelUmum = $this->db->query("delete from inv_trm_i
								where no_terima='".$no_terima."' ");
				if ($qDelDetail && $qDelUmum)
				{
					echo "{success:true}";
				}
			} else{
				echo "{success:false}";
			}
		//}
	}
	public function deletePenerimaan(){
		//$this->db->trans_begin();
		
		$no_terima = $_POST['no_terima'];
		$no_urut_brg = $_POST['no_urut_brg'];
		$qCek = $this->db->query("SELECT * FROM inv_trm_i_det
								WHERE no_terima='".$no_terima."' 
								AND no_urut_brg='".$no_urut_brg."'")->result();
		if(count($qCek) <> 0){
			$qDelDetail = $this->db->query("delete from inv_trm_i_det 
								where no_terima='".$no_terima."' and no_urut_brg='".$no_urut_brg."' ");
			if($qDelDetail){
					echo "{success:true, checking:true}";
				}else{
					echo "{success:false, checking:true}";
				}
		} else {
			echo "{success:false, checking:false}";
		}
	
	}
	
	function saveBarang($NoTerima,$kdVendor,$TglTerima,$NoFaktur,$StatusPosting,$Keterangan,$NoSpk,$TglSpk){
		$this->db->trans_begin();
		$strError = "";
		
		$noMinta=$this->getNoMintaBahan();
		$jmllist= $_POST['jumlah'];
		
		if($NoMintaAsal == ''){ //data baru
			$data = array("no_minta"=>$noMinta,
							"tgl_minta"=>$TglMinta,
							"jenis_minta"=>$JenisMinta );
			
			$result=$this->db->insert('gz_minta_bahan',$data);
		
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('gz_minta_bahan',$data);
			//-----------akhir insert ke database sql server----------------//
			
			if($result){
				for($i=0;$i<$jmllist;$i++){
					$kd_bahan = $_POST['kd_bahan-'.$i];
					$qty = $_POST['qty-'.$i];
					$ket_spek = strtoupper($_POST['ket_spek-'.$i]);
					
					$dataDet = array("no_minta"=>$noMinta,
								"kd_bahan"=>$kd_bahan,
								"qty"=>$qty,
								"ket_spek"=>$ket_spek);
					
					$resultDet=$this->db->insert('gz_minta_bahan_detail',$dataDet);
			
					//-----------insert to sq1 server Database---------------//
					_QMS_insert('gz_minta_bahan_detail',$dataDet);
					//-----------akhir insert ke database sql server----------------//
				}
				if($resultDet){
					$strError=$noMinta;
				}else{
					$strError='Error';
				}
			} else{
				$strError='Error';
			}
		} else{ //data edit
			for($i=0;$i<$jmllist;$i++){
				$kd_bahan = $_POST['kd_bahan-'.$i];
				$qty = $_POST['qty-'.$i];
				$ket_spek = $_POST['ket_spek-'.$i];
				
				$dataUbah = array("qty"=>$qty,"ket_spek"=>$ket_spek);
				$criteria = array("no_minta"=>$NoMintaAsal,"kd_bahan"=>$kd_bahan);
				$this->db->where($criteria);
				$result=$this->db->update('gz_minta_bahan_detail',$dataUbah);
				
				//-----------insert to sq1 server Database---------------//
				_QMS_update('gz_minta_bahan_detail',$dataUbah,$criteria);
				//-----------akhir insert ke database sql server----------------//
			}
			if($resultDet){
				$strError=$NoMintaAsal;
			}else{
				$strError='Error';
			}
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		} else{
			$this->db->trans_commit();
		}
		
		return $strError;
	}
	
	public function hapusPermintaan(){
		//cek jika data sudah diterima
		$qCek = $this->db->query("SELECT * from gz_terima_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ")->result();
		if(count($qCek) > 0){
			$hasil='error';
		} else{
			$query = $this->db->query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			
			//-----------delete to sq1 server Database---------------//
			_QMS_Query("DELETE FROM gz_minta_bahan_detail WHERE no_minta='".$_POST['no_minta']."' ");
			//-----------akhir delete ke database sql server----------------//
			if($query){
				$qDet = $this->db->query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
			
				//-----------delete to sq1 server Database---------------//
				_QMS_Query("DELETE FROM gz_minta_bahan WHERE no_minta='".$_POST['no_minta']."' ");
				//-----------akhir delete ke database sql server----------------//
				if($qDet){
					$hasil='Ok';
				} else{
					$hasil='error';
				}
			} else{
				$hasil='error';
			}
		}
		
		if($hasil == 'Ok'){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
	public function hapusBarisGridBahan(){
		$query = $this->db->query("DELETE FROM gz_minta_bahan_detail 
									WHERE no_minta='".$_POST['no_minta']."' 
										AND kd_bahan='".$_POST['kd_bahan']."'");
		
		//-----------delete to sq1 server Database---------------//
		_QMS_Query("DELETE FROM gz_minta_bahan_detail 
					WHERE no_minta='".$_POST['no_minta']."' 
						AND kd_bahan='".$_POST['kd_bahan']."'");
		//-----------akhir delete ke database sql server----------------//
		
		if($query){
			echo "{success:true}";
		}else{
			echo "{success:false}";
		}
	}
	
}
?>