<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_inventarismutasidetail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetakBertambah(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DETAIL MUTASI INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$Lokasi=$param->Lokasi;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($KdInv == ''){
			$criteriakd="";
		} else{
			$criteriakd="AND IMB.KD_INV='".$KdInv."'";
		}
		
		
		
		$queryHead = $this->db->query( "SELECT * FROM (
											SELECT im.no_mutasi, im.tgl_mutasi, imb.kd_inv, imb.no_urut_brg, 
												imb.nama_brg, idm.jml_masuk AS jml_mutasi, isa.satuan, 
												ik.nama_sub, im.jen_mutasi, im.keterangan, inv.no_register, 
												(SELECT (CASE WHEN LEFT(inv.kd_kondisi,1)='1' THEN IT.LUAS_TANAH 
														WHEN LEFT(inv.kd_kondisi,1)='2' THEN IB.JML_LUAS 
														WHEN LEFT(inv.kd_kondisi,1)='3' THEN 1 
														WHEN LEFT(inv.kd_kondisi,1)='4' THEN JUMLAH END ) as JML
												from inv_inventaris iv 
													left join inv_angkutan ia on iv.no_register=ia.no_register 
													left join inv_bangunan ib on iv.no_register=ib.no_register 
													left join inv_tanah it on iv.no_register=it.no_register 
													left join inv_brg_lain ibl on iv.no_register=ibl.no_register 
												where iv.no_register=INV.NO_REGISTER) AS jumlah_inv 
											FROM INV_DET_MUTASI IDM 
												INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER = INV.NO_REGISTER 
												INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG = IMB.NO_URUT_BRG 
												INNER JOIN INV_KODE IK ON IMB.KD_INV = IK.KD_INV 
												INNER JOIN INV_MUTASI IM ON IDM.NO_MUTASI = IM.NO_MUTASI 
												INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN = ISA.KD_SATUAN 
											WHERE IM.TGL_MUTASI BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
												AND IM.JEN_MUTASI=1 
												".$criteriakd."
											UNION all
											SELECT IM.NO_MUTASI, IM.TGL_MUTASI, IMB.KD_INV, IMB.NO_URUT_BRG, 
												IMB.NAMA_BRG, IDM.JML_KURANG AS JML_MUTASI, ISA.SATUAN, 
												IK.NAMA_SUB, IM.JEN_MUTASI, IM.KETERANGAN, INV.NO_REGISTER, 
												(JML_AKTUAL_LAMA - IDM.JML_KURANG) AS jumlah_inv 
											FROM INV_DET_MUTASI IDM 
												INNER JOIN INV_INVENTARIS INV ON IDM.NO_REGISTER = INV.NO_REGISTER 
												INNER JOIN INV_MASTER_BRG IMB ON INV.NO_URUT_BRG=IMB.NO_URUT_BRG 
												INNER JOIN INV_KODE IK ON IMB.KD_INV = IK.KD_INV 
												INNER JOIN INV_SATUAN ISA ON IMB.KD_SATUAN = ISA.KD_SATUAN 
												INNER JOIN INV_MUTASI IM ON IDM.NO_MUTASI = IM.NO_MUTASI 
											WHERE IM.TGL_MUTASI BETWEEN '".$tglAwal."' AND '".$tglAkhir."' 
												AND IM.JEN_MUTASI=3 
												".$criteriakd."
										) as mutasi
										ORDER BY TGL_MUTASI, NO_MUTASI, KD_INV,NAMA_BRG");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="60" colspan="2">Mutasi</th>
					<th width="60" rowspan="2">Kd. Kelompok</th>
					<th width="60" rowspan="2">Urutan</th>
					<th width="140" rowspan="2">Nama Barang</th>
					<th width="40" rowspan="2">Inventaris</th>
					<th width="60" colspan="3">Jumlah Barang</th>
					<th width="60" rowspan="2">Satuan</th>
				</tr>
				<tr>
					<th width="40">No</th>
					<th width="40">Tanggal</th>
					<th width="40">Bertambah</th>
					<th width="40">Berkurang</th>
					<th width="40">Saldo</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$jml_mutasi=$line->jml_mutasi;
				$jen_mutasi=$line->jen_mutasi;
				if($jen_mutasi == 1){
					$bertambah=$jml_mutasi;
					$berkurang=0;
				} else{
					$berkurang=$jml_mutasi;
					$bertambah=0;
				}
				$html.='<tr class="headerrow"> 
							<td width="" align="center">'.$no.'</td>
							<td width="" >&nbsp;'.$line->no_mutasi.'</td>
							<td width="" >&nbsp;'.date('d-M-Y',strtotime($line->tgl_mutasi)).'&nbsp;</td>
							<td width="">&nbsp;'.$line->kd_inv.'</td>
							<td width="">&nbsp;'.$line->no_urut_brg.'&nbsp;</td>
							<td width="">'.$line->nama_brg.'&nbsp;</td>
							<td width="">&nbsp;'.$line->no_register.'</td>
							<td width="" align="right">'.$bertambah.'&nbsp;</td>
							<td width="" align="right">'.$berkurang.'&nbsp;</td>
							<td width="" align="right">'.$line->jumlah_inv.'&nbsp;</td>
							<td width="">&nbsp;'.$line->satuan.'</td>
						</tr>';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Mutasi Detail',$html);	
		$html.='</table>';
   	}
	
	public function cetakPindah(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DETAIL MUTASI INVENTARIS';
		$param=json_decode($_POST['data']);
		
		$KdInv=$param->KdInv;
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$Lokasi=$param->Lokasi;
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if($KdInv==''){
			$criteriaKd=""; 
			$namasub='Semua';
			$kd='-';
		} else{
			$criteriaKd="AND IMB.KD_INV='".$KdInv."'";
			$q = $this->db->query( " SELECT kd_inv,nama_sub from inv_kode where kd_inv='".$KdInv."'");
			if(count($q->result()) > 0){
				foreach ($q->result() as $linee) 
				{
					$namasub=$linee->nama_sub;
				
					//8.01.03.08.014
					$kd_inv=$linee->kd_inv;
					$a=substr($linee->kd_inv,0,1);//8
					$b=substr($linee->kd_inv,1,2);//01
					$c=substr($linee->kd_inv,3,2);//03
					$d=substr($linee->kd_inv,5,2);//08
					$e=substr($linee->kd_inv,-3);//014
					
					$kd=$a.'.'.$b.'.'.$c.'.'.$d.'.'.$e;
					
				}
				
			} else{
				$namasub='-';
				$kd='-';
			}
		}
		
		if($Lokasi == '999999 - SEMUA' || $Lokasi == '999999'){
			$criterialokasi="";
			$qLokasi="Semua";
		} else{
			$criterialokasi="AND IDM.No_Ruang_Lama ='".$Lokasi."'";
			$qLokasi = $this->db->query( " SELECT IR.No_Ruang, L.Lokasi  
										FROM INV_RUANG IR 
											INNER JOIN  INV_LOKASI L ON IR.KD_LOKASI = L.KD_LOKASI 
										where no_ruang='".$Lokasi."'")->row()->lokasi;
		}
		
		
		$queryHead = $this->db->query( "SELECT distinct(idm.no_mutasi),im.tgl_mutasi, im.keterangan
										FROM inv_det_mutasi idm  
											INNER JOIN inv_inventaris inv ON idm.no_register = inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_mutasi im ON idm.no_mutasi = im.no_mutasi  
										WHERE TGL_MUTASI BETWEEN '".$tglAwal."'  AND '".$tglAkhir."'  AND im.jen_mutasi=2 
										".$criteriaKd."
										".$criterialokasi."
										ORDER BY idm.no_mutasi, im.tgl_mutasi");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN------------------------------------------------
		
		
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>Tanggal '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th>Kelompok Barang : '.$namasub.' ('.$kd.')</th>
					</tr>
					<tr>
						<th>Lokasi Asal : '.$qLokasi.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
				<tr>
					<th width="10" rowspan="2">No</th>
					<th width="10" rowspan="2">No<br>urut</th>
					<th width="60" colspan="2">Mutasi</th>
					<th width="60" rowspan="2">Kd. Kelompok</th>
					<th width="60" rowspan="2">Urutan</th>
					<th width="140" rowspan="2">Nama Barang</th>
					<th width="40" rowspan="2">Inventaris</th>
					<th width="60" colspan="2">Lokasi</th>
					<th width="60" rowspan="2">Jumlah Pindah</th>
					<th width="60" rowspan="2">Satuan</th>
				</tr>
				<tr>
					<th width="40">No</th>
					<th width="40">Tanggal</th>
					<th width="40">Lama</th>
					<th width="40">Baru</th>
				</tr>
			</thead>';
		if(count($query) > 0) {
			
			$noo=0;
			foreach ($query as $line) 
			{
				$no=0;
				$noo++;
				$html.='<tr class="headerrow"> 
								<th width="" align="center">'.$noo.'</th>
								<th width="" align="center"></th>
								<th width="" >&nbsp;'.$line->no_mutasi.'</th>
								<th width="" >&nbsp;'.date('d-M-Y',strtotime($line->tgl_mutasi)).'&nbsp;</th>
								<th width="" colspan="8">&nbsp;</th>
						</tr>';
				
				$queryBody = $this->db->query( "SELECT im.tgl_mutasi, idm.no_mutasi, im.keterangan, imb.kd_inv, imb.no_urut_brg,   
											idm.jml_pindah, idm.jml_aktual_lama, isa.satuan, ik.nama_sub,  imb.nama_brg, idm.no_ruang_lama,  
											(SELECT l.lokasi 
												FROM inv_ruang ir 
												INNER JOIN  inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi  
												WHERE ir.no_ruang = idm.no_ruang_lama) AS lokasi_lama,  idm.no_ruang_baru,  
											(SELECT l.lokasi 
												FROM inv_ruang ir 
												INNER JOIN  inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi  
												WHERE ir.no_ruang = idm.no_ruang_baru) AS lokasi_baru  
										FROM inv_det_mutasi idm  
											INNER JOIN inv_inventaris inv ON idm.no_register = inv.no_register  
											INNER JOIN inv_master_brg imb ON inv.no_urut_brg = imb.no_urut_brg  
											INNER JOIN inv_kode ik ON imb.kd_inv = ik.kd_inv  
											INNER JOIN inv_satuan isa ON imb.kd_satuan = isa.kd_satuan  
											INNER JOIN inv_mutasi im ON idm.no_mutasi = im.no_mutasi  
										WHERE TGL_MUTASI ='".$line->tgl_mutasi."'  AND im.jen_mutasi=2 
										".$criteriaKd."
										".$criterialokasi."
										AND idm.no_mutasi='".$line->no_mutasi."'
										ORDER BY im.no_mutasi, im.tgl_mutasi, lokasi_lama, imb.kd_inv, imb.nama_brg");
				$query2 = $queryBody->result();
				
				foreach ($query2 as $line2) 
				{
					$no++;
					if($line2->lokasi_lama == ''){
						$lama='-';
					} else{
						$lama=$line2->lokasi_lama;
					}
					
					if($line2->lokasi_baru == ''){
						$baru='-';
					} else{
						$baru=$line2->lokasi_baru;
					}
					
					$html.='<tr class="headerrow"> 
								<td width="" align="center"></td>
								<td width="" align="center">'.$no.'</td>
								<td width="" colspan="2">&nbsp;</td>
								<td width="">&nbsp;'.$line2->kd_inv.'</td>
								<td width="">&nbsp;'.$line2->no_urut_brg.'&nbsp;</td>
								<td width="">'.$line2->nama_brg.'&nbsp;</td>
								<td width="">&nbsp;</td>
								<td width="">&nbsp;'.$lama.'</td>
								<td width="">&nbsp;'.$baru.'</td>
								<td width="" align="right">'.$line2->jml_pindah.'&nbsp;</td>
								<td width="">&nbsp;'.$line2->satuan.'</td>
							</tr>';
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="12" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Inventari Mutasi Detail',$html);	
		$html.='</table>';
   	}
	
	public function getGridLookUpBarang(){
		$result=$this->db->query("Select kd_inv,inv_kd_inv,nama_sub from INV_KODE where inv_kd_inv='".$_POST['inv_kd_inv']."' order by kd_inv
								")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getLokasiAsal(){
		$result=$this->db->query("SELECT ir.no_ruang, ir.no_ruang || ' - ' || l.lokasi as lokasi
									FROM inv_ruang ir 
										INNER JOIN  inv_lokasi l ON ir.kd_lokasi = l.kd_lokasi 
									UNION  
									SELECT '999999' AS kd_lokasi, '999999 - SEMUA' AS lokasi ORDER BY lokasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
}
?>