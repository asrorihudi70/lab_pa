<?php
class tb_cekkondisiruangtempatinventaris extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from inv_cek_ruang";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_ruang();
        $row->no_reg_ruang=$rec->no_reg_ruang;
        $row->tgl_cek=$rec->tgl_cek;
		$row->jml_b=$rec->jml_b;
		$row->jml_rr=$rec->jml_rr;
        $row->jml_rb=$rec->jml_rb;
		$row->keterangan=$rec->keterangan;
		$row->no_baris=$rec->no_baris;
        return $row;
    }

}

class Rowam_inv_ruang
{
    public $no_reg_ruang;
    public $tgl_cek;
	public $jml_b;
	public $jml_rr;
    public $jml_rb;
	public $keterangan;
	public $no_baris;
    
}
?>
