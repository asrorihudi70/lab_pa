<?php
class tb_getcmbkondisipembukuan extends TblBase
{
    function __construct()
    {
        $this->TblName='inv_kondisi';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT kd_kondisi, kondisi FROM inv_kondisi ";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_kondisitanah();
        $row->kd_kondisi=$rec->kd_kondisi;
        $row->kondisi=$rec->kondisi;
        return $row;
    }

}

class Rowam_inv_kondisitanah
{
    public $kd_kondisi;
    public $kondisi;
}
?>
