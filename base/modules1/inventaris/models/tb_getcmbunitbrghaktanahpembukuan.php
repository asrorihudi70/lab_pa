<?php
class tb_getcmbunitbrghaktanahpembukuan extends TblBase
{
    function __construct()
    {
        $this->TblName='INV_HAK_TANAH';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT ID_HAK_TANAH, HAK_TANAH FROM INV_HAK_TANAH  ORDER BY ID_HAK_TANAH";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_haktanah();
        $row->id_hak_tanah=$rec->id_hak_tanah;
        $row->hak_tanah=$rec->hak_tanah;
        return $row;
    }

}

class Rowam_inv_haktanah
{
    public $id_hak_tanah;
    public $hak_tanah;
}
?>
