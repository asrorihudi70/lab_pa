<?php
class tb_getcmbunitbrgstatustanahbangunanpembukuan extends TblBase
{
    function __construct()
    {
        $this->TblName='INV_STATUS_TANAH';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT KD_STATUS_TANAH, STATUS_TANAH FROM INV_STATUS_TANAH ORDER BY KD_STATUS_TANAH";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_statustanah();
        $row->kd_status_tanah=$rec->kd_status_tanah;
        $row->status_tanah=$rec->status_tanah;
        return $row;
    }

}

class Rowam_inv_statustanah
{
    public $kd_status_tanah;
    public $status_tanah;
}
?>
