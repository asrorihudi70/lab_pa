<?php
class tb_golbarang extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "SELECT 'SEMUA'::TEXT AS gol, '0' ::TEXT AS kd
							union all 
							SELECT distinct( CASE LEFT(IK.KD_INV,1)WHEN '1' THEN 'BARANG TIDAK BERGERAK' 
														  WHEN '2' THEN 'BARANG BERGERAK' 
														  WHEN '3' THEN 'HEWAN, IKAN DAN TANAMAN'
														  Else '-' END)  as  gol, LEFT(IK.KD_INV,1) as kd
							FROM  INV_MASTER_BRG IMB 
								INNER JOIN INV_SATUAN ISA  ON IMB.KD_SATUAN = ISA.KD_SATUAN  
								INNER JOIN INV_KODE IK  ON IMB.KD_INV = IK.KD_INV  
							WHERE LEFT(IK.KD_INV,1)<>'8'  
							ORDER BY kd
";
    }

    function FillRow($rec)
    {
        $row=new Row_golbarang();
        $row->kd=$rec->kd;
        $row->gol=$rec->gol;

        return $row;
    }

}

class Row_golbarang
{
    public $kd;
    public $gol;

}

?>
