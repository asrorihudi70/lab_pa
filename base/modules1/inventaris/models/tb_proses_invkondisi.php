<?php
class tb_proses_invkondisi extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "Select kd_kondisi,inv_kd_kondisi,kondisi from INV_KONDISI";
    }

    function FillRow($rec)
    {
        $row=new Rowam_inv_kode();
        $row->kd_kondisi=$rec->kd_kondisi;
        $row->inv_kd_kondisi=$rec->inv_kd_kondisi;
        $row->kondisi=$rec->kondisi;

        return $row;
    }

}

class Rowam_inv_kode
{
    public $kd_kondisi;
    public $inv_kd_kondisi;
    public $kondisi;

}

class clsTreeRow
{

    public $id;
    public $text;
    public $leaf;
    public $expanded;
    public $parents;
    public $parents_name;
    public $type;
    public $children = array();

}


?>
