<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_BukuRegisterSummaryOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	public function cetak()
    {
		$common=$this->common;
   		$result=$this->result;
   		$title='Laporan Summary Pasien Radiologi';
		$param=json_decode($_POST['data']);
		
		$TglAwal = $param->tglAwal;
		$TglAkhir = $param->tglAkhir;
		//$JmlList =  $param->JmlList;
		//$type_file = $param->Type_File;
		$html="";
		//ambil list kd_unit
		$u="";
		/* for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		} */
		
		$UserID = '0';
		$tglsum = $TglAwal;
		$tglsummax = $TglAkhir;
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		
		/* $tmpunit = explode(',', $u);
		$criteria = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "".$tmpunit[$i].",";
		}
		$criteria = substr($criteria, 0, -1); */

		$q = $this->db->query("SELECT 
								x.Nama_Unit as namaunit2,  
								Count(X.KD_Pasien) as jumlahpasien , 
								Sum(lk) as lk, 
								Sum(pr) as pr, 
								Sum(br) as br, 
								Sum(Lm)as lm, 
								Sum(PHB)as phb, 
								Sum(nPHB) as nphb, 
								sum(PERUSAHAAN)as perusahaan,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum,
								x.Kd_Unit as kdunit
								  From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
										Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
										Case When k.Baru           = true Then 1 else 0 end as Br,
										Case When k.Baru           = false Then 1 else 0 end as Lm,
										Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
										Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										case when ktr.jenis_cust   = 1 then 1 else 0 end as PERUSAHAAN,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
										case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
										case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
										case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
										INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
										Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.Kd_Unit IN ('711','712','713') and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."' 
									 ) x Group By x.kd_unit,x.Nama_Unit  Order By x.Nama_Unit asc");
        if($q->num_rows == 0)
        {
            $html.='';
        }else {
			$query = $q->result();
			if ($u== "Semua" || $u== "")
			{
				$kd_rs=$this->session->userdata['user_id']['kd_rs'];	
				$queryRS = $this->db->query("SELECT * from db_rs WHERE code='".$kd_rs."'")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as 
												lk, Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
													Case When k.Baru           = true Then 1 else 0 end as Br,
													Case When k.Baru           = false Then 1 else 0 end as Lm,
													Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												  From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
													 ) x")->result();*/
			}else{
				$queryRS = $this->db->query("select * from db_rs")->result();
				/*$queryjumlah= $this->db->query("SELECT   
												Count(X.KD_Pasien) as jumlahpasien , 
												Sum(lk) as lk, 
												Sum(pr) as pr, 
												Sum(br) as br, 
												Sum(Lm)as lm, 
												Sum(PHB)as phb, 
												Sum(nPHB) as nphb,  
												sum(PERUSAHAAN)as perusahaan,  
												sum(pbi) as pbi, 
												sum(non_pbi) as non_pbi, 
												sum(jamkesda) as jamkesda, 
												sum(lain_lain) as lain_lain,
												sum(umum) as umum 
												From (
												Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
												Case When ps.Jenis_Kelamin = true Then 1 else 0 end as Lk,
												Case When ps.Jenis_Kelamin = false Then 1 else 0 end as Pr, 
												Case When k.Baru           = true Then 1 else 0 end as Br,
												Case When k.Baru           = false Then 1 else 0 end as Lm,
												Case When k.kd_Customer    = '0000000001' Then 1 Else 0 end as PHB,  
												Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
												case when ktr.jenis_cust   =1 then 1 else 0 end as PERUSAHAAN, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
												case when ktr.jenis_cust   =2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
												case when ktr.jenis_cust   =2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
												case when ktr.jenis_cust   =0 then 1 else 0 end as umum
												From Unit u
												INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
												 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
												 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='2' and  u.Kd_Unit in($criteria) and k.tgl_masuk between  '".$tglsum."' and  '".$tglsummax."'
												 ) x")->result();*/
			}
			$queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
			
			foreach ($queryuser as $line) {
			   $kduser = $line->kd_user;
			   $nama = $line->user_names;
			}
			
			$no = 0;
			foreach ($queryRS as $line) {
				$telp='';
				$fax='';
				if(($line->phone1 != null && $line->phone1 != '')|| ($line->phone2 != null && $line->phone2 != '')){
					$telp='<br>Telp. ';
					$telp1=false;
					if($line->phone1 != null && $line->phone1 != ''){
						$telp1=true;
						$telp.=$line->phone1;
					}
					if($line->phone2 != null && $line->phone2 != ''){
						if($telp1==true){
							$telp.='/'.$line->phone2.'.';
						}else{
							$telp.=$line->phone2.'.';
						}
					}else{
						$telp.='.';
					}
				}
				if($line->fax != null && $line->fax != ''){
					$fax='<br>Fax. '.$line->fax.'.';
				}
					   
			}

		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
			#HEADER TABEL LAPORAN
			$html.='
				<table class="t2" cellspacing="0" border="0">
					<tbody>
						<tr>
							<th colspan="13">Laporan Summary Pasien Kamar Operasi</th>
						</tr>
						<tr>
							<th colspan="13">Periode '.$awal.' s/d '.$akhir.'</th>
						</tr>
					</tbody>
				</table> <br>';
			$html.='
					<table class="t1" width="600" border = "1">
						<tr>
								<th align ="center" width="24"  rowspan="2">No. </th>
								<th align ="center" width="137" rowspan="2">Nama Unit </th>
								<th align ="center" width="50"  rowspan="2">Jumlah Pasien</th>
								<th align ="center" colspan="2">Jenis kelamin</th>
								<th align ="center" width="68" rowspan="2">Perusahaan</th>
								<th align ="center" width="68" colspan="4">Asuransi</th>
								<th align ="center" width="68" rowspan="2">Umum</th>
							</tr>
							<tr>    
								<th align="center" width="50">L</th>
								<th align="center" width="50">P</th>
								<th align="center" width="50">BPJS NON PBI</th>
								<th align="center" width="50">BPJS PBI</th>
								<th align="center" width="50">TM/ PM JAMKESDA</th>
								<th align="center" width="50">LAIN-LAIN</th>
							</tr>  
						';

			$total_pasien     = 0;
			$total_lk         = 0;
			$total_pr         = 0;
			$total_br         = 0;
			$total_lm         = 0;
			$total_perusahaan = 0;
			$total_non_pbi    = 0;
			$total_pbi        = 0;
			$total_jamkesda   = 0;
			$total_lain       = 0;
			$total_umum       = 0;
			foreach ($query as $line) 
			{
			   $no++;
			   //$tanggal = $line->tgl_lahir;
			   //$tglhariini = date('d/F/Y', strtotime($tanggal));
			   $html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
						</tr>';
				$total_pasien     += $line->jumlahpasien;
				$total_lk         += $line->lk;
				$total_pr         += $line->pr;
				$total_perusahaan += $line->perusahaan;
				$total_non_pbi    += $line->non_pbi;
				$total_pbi        += $line->pbi;
				$total_jamkesda   += $line->jamkesda;
				$total_lain       += $line->lain_lain;
				$total_umum       += $line->umum;
			}
			$html.='
			<tr>
				<td colspan="2" align="right"><b> Jumlah</b></td>
				<td align="right"><b>'.$total_pasien.'</b></td>
				<td align="right"><b>'.$total_lk.'</b></td>
				<td align="right"><b>'.$total_pr.'</b></td>
				<td align="right">'.$total_perusahaan.'</td>
				<td align="right">'.$total_non_pbi.'</td>
				<td align="right">'.$total_pbi.'</td>
				<td align="right">'.$total_jamkesda.'</td>
				<td align="right">'.$total_lain.'</td>
				<td align="right"><b>'.$total_umum.'</b></td>
			</tr>';
			/*foreach ($queryjumlah as $line) 
			{
				$html.='
						<tr>
							<td colspan="2" align="right"><b> Jumlah</b></td>
							<td align="right"><b>'.$line->jumlahpasien.'</b></td>
							<td align="right"><b>'.$line->lk.'</b></td>
							<td align="right"><b>'.$line->pr.'</b></td>
							<td align="right"><b>'.$line->br.'</b></td>
							<td align="right"><b>'.$line->lm.'</b></td>
							<td align="right">'.$line->perusahaan.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right"><b>'.$line->umum.'</b></td>
						</tr>';
			} */     
			$html.='</table>';
		}  
        //echo $html;
		
		$prop=array('foot'=>true);
		//jika type file 1=excel 
		if($type_file == 1){
			$name='Laporan_Summary_Pasien_OK.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan Summary Pasien',$html);
		}
    }
   	public function cetak2(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN BUKU REGISTER SUMMARY OPERASI';
		$param=json_decode($_POST['data']);
		
		$awal=date('d-M-Y',strtotime($param->tglAwal));
		$akhir=date('d-M-Y',strtotime($param->tglAkhir));
		$cariawal=date('Y-m-d',strtotime($param->tglAwal));
		$cariakhir=date('Y-m-d',strtotime($param->tglAkhir));
        $tomorrow = date('d-M-Y', strtotime($awal . "+1 days"));
        $tomorrow2 = date('d-M-Y', strtotime($akhir . "+1 days"));
        
		$kriteria = "kd_unit IN ('711','712','713') and (tgl_masuk >= '" . $awal . "' and tgl_masuk <= '" . $akhir . "') or (tgl_masuk >= '" . $tomorrow . "' and tgl_masuk <= '" . $tomorrow2 . "')";
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
		
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td  align="center" rowspan="2"><strong>No</strong></td>
				<td  align="center" rowspan="2"><strong>Tgl Masuk</strong></td>
				<td  align="center" rowspan="2"><strong>Kamar Operasi</strong></td>
				<td  align="center" colspan="3"><strong>Asal Pasien</strong></td>
				<td  align="center" colspan="3"><strong>Kel. Pasien</strong></td>
			  </tr>
			  <tr>
				<td  align="center"><strong>RWI</strong></td>
				<td  align="center"><strong>RWJ</strong></td>
				<td  align="center"><strong>IGD</strong></td>
				<td  align="center"><strong>Perorangan</strong></td>
				<td  align="center"><strong>Perusahaan</strong></td>
				<td  align="center"><strong>Asuransi</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("select distinct kd_unit,tgl_masuk, nama_kamar ,  RWI , RWJ , IGD, Perorangan, Perusahaan , Asuransi 
									from (select  distinct okk.tgl_masuk,   kam.nama_kamar
										, count(case when left(okkm.kd_unit,1)='1' then 1 end) as RWI
										, count(case when left(okkm.kd_unit,1)='2' then 1 end) as RWJ
										, count(case when left(okkm.kd_unit,1)='3' then 1 end) as IGD
										
										, count(case when knt.jenis_cust='0' then 1  end) as Perorangan
										, count(case when knt.jenis_cust='1' then 1  end) as Perusahaan
										, count(case when knt.jenis_cust='2' then 1  end) as Asuransi,
										k.kd_unit
										from ok_kunjungan okk 
										inner join kunjungan k on okk.kd_pasien=k.kd_pasien and okk.kd_unit=k.kd_unit and okk.tgl_masuk=k.tgl_masuk and okk.urut_masuk = k.urut_masuk 
										inner join ok_kunjungan_kmr okkm on okkm.no_register=okk.no_register
										inner join kamar kam on kam.no_kamar=okkm.no_kamar
										INNER JOIN Customer c on k.kd_customer = c.kd_customer
										inner join kontraktor knt on knt.kd_customer = c.kd_customer 
										inner join transaksi tr on okk.kd_pasien = tr.kd_pasien and okk.kd_unit = tr.kd_unit and okk.tgl_masuk = tr.tgl_transaksi and okk.urut_masuk = tr.urut_masuk
										inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
										inner join produk pr on dt.kd_produk = pr.kd_produk
										left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
										left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
										left join unit u on tr2.kd_unit = u.kd_unit
										group by okk.tgl_masuk , kam.nama_kamar,k.kd_unit


										) as data 
													where ".$kriteria."
													group by kd_unit,tgl_masuk , nama_kamar , RWI , RWJ , IGD , Perorangan, Perusahaan , Asuransi
													
													order by tgl_masuk , nama_kamar
													
													")->result();
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='
				
				<tbody>
				<tr>
					<td align="center">' . $no . '</td>
                                                    <td width="80" align="center">' .date('d-M-Y',strtotime($line->tgl_masuk)) . '</td>
                                                    <td width="100">' .$line->nama_kamar . '</td>
                                                    <td width="100" align="center">' . $line->rwi . '</td>
                                                    <td width="100" align="center">' . $line->rwj . '</td>
                                                    <td width="100" align="center">' . $line->igd . '</td>
                                                    <td width="100" align="center">' . $line->perorangan  . '</td>
                                                    <td width="100" align="center">' . $line->perusahaan  . '</td>
                                                    <td width="100" align="center">' . $line->asuransi  . '</td>
					  </tr>
				';
			} 		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="8" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Buku Register Summary Operasi',$html);	
   	}
}
?>