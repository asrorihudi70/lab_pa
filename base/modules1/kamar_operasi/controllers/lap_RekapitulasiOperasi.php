<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RekapitulasiOperasi extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakRekapok(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAPITULASI OPERASI';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		$cariawal=date('Y-m-d',strtotime($tglAwal));
		$cariakhir=date('Y-m-d',strtotime($tglAkhir));
		
		
		//$query = $queryHead->result();
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td align="center"><strong>No</strong></td>
				<td align="center"><strong>Nama</strong></td>
				<td align="center"><strong>Jenis Kelamin</strong></td>
				<td align="center"><strong>Umur</strong></td>
				<td align="center"><strong>No Register</strong></td>
				<td align="center"><strong>Ruang</strong></td>
				<td align="center"><strong>Dokter Bedah</strong></td>
				<td align="center"><strong>Dokter Anastesi</strong></td>
				<td align="center"><strong>Diag Pre Operatif</strong></td>
				<td align="center"><strong>Diagnosa Post Operatif</strong></td>
				<td align="center"><strong>Jenis Anastesi</strong></td>
				<td align="center"><strong>Teknis anastesi</strong></td>
				<td align="center"><strong>premidikasi</strong></td>
				<td align="center"><strong>tindakan</strong></td>
				<td align="center"><strong>Tgl Operasi</strong></td>
				<td align="center"><strong>Jam anastesi mulai</strong></td>
				<td align="center"><strong>Jam Anastesi selesai</strong></td>
				<td align="center"><strong>Durasi</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("select distinct(ps.nama),ps.jenis_kelamin,extract(year from cast(age(ps.tgl_lahir) as interval))||' Tahun' as umur,
									kmr.nama_kamar as ruang, 
									okd.no_register, okk.tgl_op_mulai as tgl_operasi, 
									okk.jam_an_mulai, okk.jam_an_selesai ,
									CAST(okk.jam_an_selesai AS TIMESTAMP) - CAST(okk.jam_an_mulai AS TIMESTAMP) as lama_an
								from transaksi tr
									inner join ok_trans_det okd on tr.kd_kasir=okd.kd_kasir and tr.no_transaksi=okd.no_transaksi 
									inner join ok_kunjungan okk on okk.no_register=okd.no_register 
									inner join ok_hasil_pem ohp on ohp.no_register=okk.no_register 
									inner join produk otm on otm.kd_produk::varchar=ohp.kd_tindakan 
									left join ok_dokter okdr on ohp.no_register=okdr.no_register and ohp.Kd_jenis_op=okdr.Kd_jenis_op and ohp.kd_tindakan = okdr.kd_tindakan and ohp.kd_sub_spc = okdr.kd_sub_spc 
									left join ok_perawat okpr on ohp.no_register=okpr.no_register and ohp.Kd_jenis_op=okpr.Kd_jenis_op and ohp.kd_tindakan = okpr.kd_tindakan and ohp.kd_sub_spc = okpr.kd_sub_spc and okpr.no_register = okdr.no_register  
									inner join ok_jadwal_ps ojp on ojp.tgl_op = okk.tgl_op_mulai and ojp.jam_op = okk.jam_op_mulai and okk.kd_pasien = ojp.kd_pasien and okk.kd_unit = ojp.kd_unit 
									inner Join 
									(
										select no_register, max(cast(case when ohi.kd_item='001' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Pre_Operatif, 
											 max(cast(case when ohi.kd_item='002' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Post_Operatif, 
											 max(cast(case when ohi.kd_item='003' then ohi.hasil_item else '' end as varchar(255))) as Jenis_Anasthesi, 
											 max(cast(case when ohi.kd_item='004' then ohi.hasil_item else '' end as varchar(255))) as Teknis_Anasthesi , 
											 max(cast(case when ohi.kd_item='005' then ohi.hasil_item else '' end as varchar(255))) as Premidikasi
										from ok_hasil_item ohi 
										where kd_gol_item = '1' 
										group by No_register
									) x 
									on okd.no_register=x.no_register 
									inner join pasien ps on tr.kd_pasien=ps.kd_pasien 
									inner join kamar kmr on okd.no_kamar=kmr.no_kamar 
									left join dokter dr on dr.kd_dokter=okdr.kd_dokter 
									left join perawat prw on prw.kd_perawat=okpr.kd_perawat 
									where tr.tgl_transaksi between '$cariawal' and '$cariakhir'
									group by ps.nama, ps.jenis_kelamin, ps.tgl_lahir, okd.no_register, kmr.nama_kamar , dr.nama, okdr.kd_job, prw.nama_perawat, --okpr.kd_gol_item, 
										Diagnosa_Pre_Operatif, Diagnosa_Post_Operatif, Jenis_Anasthesi, Teknis_Anasthesi, Premidikasi, otm.deskripsi, okk.tgl_op_mulai , okk.jam_an_mulai, 
										okk.jam_an_selesai, okk.jam_an_mulai , okk.jam_an_selesai")->result();
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$noregistercari=$line->no_register;
				$no++;
					if ($line->jenis_kelamin == 'f')
					{
						$jk='Perempuan';
					}
					else{
						$jk='Laki-laki';
					}
				$html.='
				
				<tbody>
				<tr>
					<td>'.$no.'&nbsp;</td>
					<td><br/>&nbsp;<strong>'.$line->nama.'</strong>&nbsp;</td>
					<td>&nbsp;'.$jk.'&nbsp;</td>
					<td>&nbsp;'.$line->umur.'&nbsp;</td>
					<td>'.$line->no_register.'&nbsp;</td>
					<td>'.$line->ruang.'&nbsp;</td>
				'; 
				
				$queryAll=$this->db->query("select ps.nama,ps.jenis_kelamin,extract(year from cast(age(ps.tgl_lahir) as interval))||' Tahun' as umur,
									okd.no_register,kmr.nama_kamar as ruang, 
									case when okdr.kd_job =1 then dr.nama else '' end as dokter_bedah, 
									case when okdr.kd_job =2 then dr.nama else '' end as dokter_an, 
									diagnosa_pre_operatif, diagnosa_post_operatif, jenis_anasthesi, teknis_anasthesi, premidikasi, otm.deskripsi, okk.tgl_op_mulai as tgl_operasi, 
									okk.jam_an_mulai, okk.jam_an_selesai ,
									CAST(okk.jam_an_selesai AS TIMESTAMP) - CAST(okk.jam_an_mulai AS TIMESTAMP) as lama_an
								from transaksi tr
									inner join ok_trans_det okd on tr.kd_kasir=okd.kd_kasir and tr.no_transaksi=okd.no_transaksi 
									inner join ok_kunjungan okk on okk.no_register=okd.no_register 
									inner join ok_hasil_pem ohp on ohp.no_register=okk.no_register 
									inner join produk otm on otm.kd_produk::varchar=ohp.kd_tindakan 
									left join ok_dokter okdr on ohp.no_register=okdr.no_register and ohp.Kd_jenis_op=okdr.Kd_jenis_op and ohp.kd_tindakan = okdr.kd_tindakan and ohp.kd_sub_spc = okdr.kd_sub_spc 
									left join ok_perawat okpr on ohp.no_register=okpr.no_register and ohp.Kd_jenis_op=okpr.Kd_jenis_op and ohp.kd_tindakan = okpr.kd_tindakan and ohp.kd_sub_spc = okpr.kd_sub_spc and okpr.no_register = okdr.no_register  
									inner join ok_jadwal_ps ojp on ojp.tgl_op = okk.tgl_op_mulai and ojp.jam_op = okk.jam_op_mulai and okk.kd_pasien = ojp.kd_pasien and okk.kd_unit = ojp.kd_unit 
									inner Join 
									(
										select no_register, max(cast(case when ohi.kd_item='001' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Pre_Operatif, 
											 max(cast(case when ohi.kd_item='002' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Post_Operatif, 
											 max(cast(case when ohi.kd_item='003' then ohi.hasil_item else '' end as varchar(255))) as Jenis_Anasthesi, 
											 max(cast(case when ohi.kd_item='004' then ohi.hasil_item else '' end as varchar(255))) as Teknis_Anasthesi , 
											 max(cast(case when ohi.kd_item='005' then ohi.hasil_item else '' end as varchar(255))) as Premidikasi
										from ok_hasil_item ohi 
										where kd_gol_item = '1' 
										group by No_register
									) x 
									on okd.no_register=x.no_register 
									inner join pasien ps on tr.kd_pasien=ps.kd_pasien 
									inner join kamar kmr on okd.no_kamar=kmr.no_kamar 
									left join dokter dr on dr.kd_dokter=okdr.kd_dokter 
									left join perawat prw on prw.kd_perawat=okpr.kd_perawat 
									where tr.tgl_transaksi between '$cariawal' and '$cariakhir' and okk.no_register='$noregistercari'  
									group by ps.nama, ps.jenis_kelamin, ps.tgl_lahir, okd.no_register, kmr.nama_kamar , dr.nama, okdr.kd_job, prw.nama_perawat, --okpr.kd_gol_item, 
										Diagnosa_Pre_Operatif, Diagnosa_Post_Operatif, Jenis_Anasthesi, Teknis_Anasthesi, Premidikasi, otm.deskripsi, okk.tgl_op_mulai , okk.jam_an_mulai, 
										okk.jam_an_selesai, okk.jam_an_mulai , okk.jam_an_selesai
									 ")->result();
				if (count($queryAll)>1)
				{
					$dokterbedah='';
						$dokteran='';
						$diagnosapreop='';
						$diagnosapostop='';
						$jenisanastesi='';
						$teknisanastesi='';
						$premidikasi='';
						$deskripsi='';
					foreach ($queryAll as $line1) 
					{
						/* $dokterbedah.=$line1->dokter_bedah.' ; ';
						$dokteran.=$line1->dokter_an.' ; ';
						$diagnosapreop.=$line1->diagnosa_pre_operatif.' ; ';
						$diagnosapostop.=$line1->diagnosa_post_operatif.' ; ';
						$jenisanastesi.=$line1->jenis_anasthesi.' ; ';
						$teknisanastesi.=$line1->teknis_anasthesi.' ; ';
						$premidikasi.=$line1->premidikasi.' ; ';
						$deskripsi.=$line1->deskripsi.' ; '; */
						
						if ($line1->dokter_bedah=='')
						{
							$dokterbedah='-';
						}
						else
						{
							$dokterbedah.=$line1->dokter_bedah.' ; ';
						}
						if ($line1->dokter_an=='')
						{
							$dokteran='-';
						}
						else
						{
							$dokteran.=$line1->dokter_an.' ; ';
						}
						if ($line1->diagnosa_pre_operatif=='')
						{
							$diagnosapreop='-';
						}
						else
						{
							$diagnosapreop.=$line1->diagnosa_pre_operatif.' ; ';
						}
						if ($line1->diagnosa_post_operatif=='')
						{
							$diagnosapostop='-';
						}
						else
						{
							$diagnosapostop.=$line1->diagnosa_post_operatif.' ; ';
						}
						if ($line1->jenis_anasthesi=='')
						{
							$jenisanastesi='-';
						}
						else
						{
							$jenisanastesi.=$line1->jenis_anasthesi.' ; ';
						}
						if ($line1->teknis_anasthesi=='')
						{
							$teknisanastesi='-';
						}
						else
						{
							$teknisanastesi.=$line1->teknis_anasthesi.' ; ';
						}
						if ($line1->premidikasi=='')
						{
							$premidikasi='-';
						}
						else
						{
							$premidikasi.=$line1->premidikasi.' ; ';
						}
						$deskripsi.=$line1->deskripsi.' ; ';
					}
				}
				else
				{
					foreach ($queryAll as $line1) 
					{
						if ($line1->dokter_bedah=='')
						{
							$dokterbedah='-';
						}
						else
						{
							$dokterbedah=$line1->dokter_bedah;
						}
						if ($line1->dokter_an=='')
						{
							$dokteran='-';
						}
						else
						{
							$dokteran=$line1->dokter_an;
						}
						if ($line1->diagnosa_pre_operatif=='')
						{
							$diagnosapreop='-';
						}
						else
						{
							$diagnosapreop=$line1->diagnosa_pre_operatif;
						}
						if ($line1->diagnosa_post_operatif=='')
						{
							$diagnosapostop='-';
						}
						else
						{
							$diagnosapostop=$line1->diagnosa_post_operatif;
						}
						if ($line1->jenis_anasthesi=='')
						{
							$jenisanastesi='-';
						}
						else
						{
							$jenisanastesi=$line1->jenis_anasthesi;
						}
						if ($line1->teknis_anasthesi=='')
						{
							$teknisanastesi='-';
						}
						else
						{
							$teknisanastesi=$line1->teknis_anasthesi;
						}
						if ($line1->premidikasi=='')
						{
							$premidikasi='-';
						}
						else
						{
							$premidikasi=$line1->premidikasi;
						}
						$deskripsi=$line1->deskripsi;
					}
				}
				$html.='
						<td>'.$dokterbedah.'&nbsp;</td>
						<td>'.$dokteran.'&nbsp;</td>
						<td>&nbsp;'.$diagnosapreop.'&nbsp;</td>
						<td>&nbsp;'.$diagnosapostop.'&nbsp;</td>
						<td>&nbsp;'.$jenisanastesi.'&nbsp;</td>
						<td>&nbsp;'.$teknisanastesi.'&nbsp;</td>
						<td>&nbsp;'.$premidikasi.'&nbsp;</td>
						<td>'.$deskripsi.'&nbsp;</td>
						<td width="100">'.date('d-M-Y',strtotime($line->tgl_operasi)).'&nbsp;</td>
						<td width="100">'.date('d-M-Y H:i',strtotime($line->jam_an_mulai)).'&nbsp;</td>
						<td width="100">'.date('d-M-Y H:i',strtotime($line->jam_an_selesai)).'&nbsp;</td>
						<td>&nbsp;'.$line->lama_an.'&nbsp;</td>
					  </tr>
				';
			}		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="18" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Rekapitulasi Operasi',$html);	
   	}
	
	
	public function cetakRekapokExcel(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAPITULASI OPERASI';
		$param=json_decode($_POST['data']);
		
		$tglAwal=$param->tglAwal;
		$tglAkhir=$param->tglAkhir;
		$awal=date('d-M-Y',strtotime($tglAwal));
		$akhir=date('d-M-Y',strtotime($tglAkhir));
		$cariawal=date('Y-m-d',strtotime($tglAwal));
		$cariakhir=date('Y-m-d',strtotime($tglAkhir));
		
		
		//$query = $queryHead->result();
		$html='';
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
					<tr>
						<th>'.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th></th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			<tr>
				<td align="center"><strong>No</strong></td>
				<td align="center"><strong>Nama</strong></td>
				<td align="center"><strong>Jenis Kelamin</strong></td>
				<td align="center"><strong>Umur</strong></td>
				<td align="center"><strong>No Register</strong></td>
				<td align="center"><strong>Ruang</strong></td>
				<td align="center"><strong>Dokter Bedah</strong></td>
				<td align="center"><strong>Dokter Anastesi</strong></td>
				<td align="center"><strong>Diag Pre Operatif</strong></td>
				<td align="center"><strong>Diagnosa Post Operatif</strong></td>
				<td align="center"><strong>Jenis Anastesi</strong></td>
				<td align="center"><strong>Teknis anastesi</strong></td>
				<td align="center"><strong>premidikasi</strong></td>
				<td align="center"><strong>tindakan</strong></td>
				<td align="center"><strong>Tgl Operasi</strong></td>
				<td align="center"><strong>Jam anastesi mulai</strong></td>
				<td align="center"><strong>Jam Anastesi selesai</strong></td>
				<td align="center"><strong>Durasi</strong></td>
			  </tr>
			</thead>';
		$query=$this->db->query("select distinct(ps.nama),ps.jenis_kelamin,extract(year from cast(age(ps.tgl_lahir) as interval))||' Tahun' as umur,
									kmr.nama_kamar as ruang, 
									okd.no_register, okk.tgl_op_mulai as tgl_operasi, 
									okk.jam_an_mulai, okk.jam_an_selesai ,
									CAST(okk.jam_an_selesai AS TIMESTAMP) - CAST(okk.jam_an_mulai AS TIMESTAMP) as lama_an
								from transaksi tr
									inner join ok_trans_det okd on tr.kd_kasir=okd.kd_kasir and tr.no_transaksi=okd.no_transaksi 
									inner join ok_kunjungan okk on okk.no_register=okd.no_register 
									inner join ok_hasil_pem ohp on ohp.no_register=okk.no_register 
									inner join produk otm on otm.kd_produk::varchar=ohp.kd_tindakan 
									left join ok_dokter okdr on ohp.no_register=okdr.no_register and ohp.Kd_jenis_op=okdr.Kd_jenis_op and ohp.kd_tindakan = okdr.kd_tindakan and ohp.kd_sub_spc = okdr.kd_sub_spc 
									left join ok_perawat okpr on ohp.no_register=okpr.no_register and ohp.Kd_jenis_op=okpr.Kd_jenis_op and ohp.kd_tindakan = okpr.kd_tindakan and ohp.kd_sub_spc = okpr.kd_sub_spc and okpr.no_register = okdr.no_register  
									inner join ok_jadwal_ps ojp on ojp.tgl_op = okk.tgl_op_mulai and ojp.jam_op = okk.jam_op_mulai and okk.kd_pasien = ojp.kd_pasien and okk.kd_unit = ojp.kd_unit 
									inner Join 
									(
										select no_register, max(cast(case when ohi.kd_item='001' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Pre_Operatif, 
											 max(cast(case when ohi.kd_item='002' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Post_Operatif, 
											 max(cast(case when ohi.kd_item='003' then ohi.hasil_item else '' end as varchar(255))) as Jenis_Anasthesi, 
											 max(cast(case when ohi.kd_item='004' then ohi.hasil_item else '' end as varchar(255))) as Teknis_Anasthesi , 
											 max(cast(case when ohi.kd_item='005' then ohi.hasil_item else '' end as varchar(255))) as Premidikasi
										from ok_hasil_item ohi 
										where kd_gol_item = '1' 
										group by No_register
									) x 
									on okd.no_register=x.no_register 
									inner join pasien ps on tr.kd_pasien=ps.kd_pasien 
									inner join kamar kmr on okd.no_kamar=kmr.no_kamar 
									left join dokter dr on dr.kd_dokter=okdr.kd_dokter 
									left join perawat prw on prw.kd_perawat=okpr.kd_perawat 
									where tr.tgl_transaksi between '$cariawal' and '$cariakhir'
									group by ps.nama, ps.jenis_kelamin, ps.tgl_lahir, okd.no_register, kmr.nama_kamar , dr.nama, okdr.kd_job, prw.nama_perawat, --okpr.kd_gol_item, 
										Diagnosa_Pre_Operatif, Diagnosa_Post_Operatif, Jenis_Anasthesi, Teknis_Anasthesi, Premidikasi, otm.deskripsi, okk.tgl_op_mulai , okk.jam_an_mulai, 
										okk.jam_an_selesai, okk.jam_an_mulai , okk.jam_an_selesai")->result();
		if(count($query) > 0) {
			$no=0;
			foreach ($query as $line) 
			{
				$noregistercari=$line->no_register;
				$no++;
					if ($line->jenis_kelamin == 'f')
					{
						$jk='Perempuan';
					}
					else{
						$jk='Laki-laki';
					}
				$html.='
				
				<tbody>
				<tr>
					<td>'.$no.'&nbsp;</td>
					<td><br/>&nbsp;<strong>'.$line->nama.'</strong>&nbsp;</td>
					<td>&nbsp;'.$jk.'&nbsp;</td>
					<td>&nbsp;'.$line->umur.'&nbsp;</td>
					<td>'.$line->no_register.'&nbsp;</td>
					<td>'.$line->ruang.'&nbsp;</td>
				'; 
				
				$queryAll=$this->db->query("select ps.nama,ps.jenis_kelamin,extract(year from cast(age(ps.tgl_lahir) as interval))||' Tahun' as umur,
									okd.no_register,kmr.nama_kamar as ruang, 
									case when okdr.kd_job =1 then dr.nama else '' end as dokter_bedah, 
									case when okdr.kd_job =2 then dr.nama else '' end as dokter_an, 
									diagnosa_pre_operatif, diagnosa_post_operatif, jenis_anasthesi, teknis_anasthesi, premidikasi, otm.deskripsi, okk.tgl_op_mulai as tgl_operasi, 
									okk.jam_an_mulai, okk.jam_an_selesai ,
									CAST(okk.jam_an_selesai AS TIMESTAMP) - CAST(okk.jam_an_mulai AS TIMESTAMP) as lama_an
								from transaksi tr
									inner join ok_trans_det okd on tr.kd_kasir=okd.kd_kasir and tr.no_transaksi=okd.no_transaksi 
									inner join ok_kunjungan okk on okk.no_register=okd.no_register 
									inner join ok_hasil_pem ohp on ohp.no_register=okk.no_register 
									inner join produk otm on otm.kd_produk::varchar=ohp.kd_tindakan 
									left join ok_dokter okdr on ohp.no_register=okdr.no_register and ohp.Kd_jenis_op=okdr.Kd_jenis_op and ohp.kd_tindakan = okdr.kd_tindakan and ohp.kd_sub_spc = okdr.kd_sub_spc 
									left join ok_perawat okpr on ohp.no_register=okpr.no_register and ohp.Kd_jenis_op=okpr.Kd_jenis_op and ohp.kd_tindakan = okpr.kd_tindakan and ohp.kd_sub_spc = okpr.kd_sub_spc and okpr.no_register = okdr.no_register  
									inner join ok_jadwal_ps ojp on ojp.tgl_op = okk.tgl_op_mulai and ojp.jam_op = okk.jam_op_mulai and okk.kd_pasien = ojp.kd_pasien and okk.kd_unit = ojp.kd_unit 
									inner Join 
									(
										select no_register, max(cast(case when ohi.kd_item='001' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Pre_Operatif, 
											 max(cast(case when ohi.kd_item='002' then ohi.hasil_item else '' end as varchar(255))) as Diagnosa_Post_Operatif, 
											 max(cast(case when ohi.kd_item='003' then ohi.hasil_item else '' end as varchar(255))) as Jenis_Anasthesi, 
											 max(cast(case when ohi.kd_item='004' then ohi.hasil_item else '' end as varchar(255))) as Teknis_Anasthesi , 
											 max(cast(case when ohi.kd_item='005' then ohi.hasil_item else '' end as varchar(255))) as Premidikasi
										from ok_hasil_item ohi 
										where kd_gol_item = '1' 
										group by No_register
									) x 
									on okd.no_register=x.no_register 
									inner join pasien ps on tr.kd_pasien=ps.kd_pasien 
									inner join kamar kmr on okd.no_kamar=kmr.no_kamar 
									left join dokter dr on dr.kd_dokter=okdr.kd_dokter 
									left join perawat prw on prw.kd_perawat=okpr.kd_perawat 
									where tr.tgl_transaksi between '$cariawal' and '$cariakhir' and okk.no_register='$noregistercari'  
									group by ps.nama, ps.jenis_kelamin, ps.tgl_lahir, okd.no_register, kmr.nama_kamar , dr.nama, okdr.kd_job, prw.nama_perawat, --okpr.kd_gol_item, 
										Diagnosa_Pre_Operatif, Diagnosa_Post_Operatif, Jenis_Anasthesi, Teknis_Anasthesi, Premidikasi, otm.deskripsi, okk.tgl_op_mulai , okk.jam_an_mulai, 
										okk.jam_an_selesai, okk.jam_an_mulai , okk.jam_an_selesai
									 ")->result();
				if (count($queryAll)>1)
				{
					$dokterbedah='';
						$dokteran='';
						$diagnosapreop='';
						$diagnosapostop='';
						$jenisanastesi='';
						$teknisanastesi='';
						$premidikasi='';
						$deskripsi='';
					foreach ($queryAll as $line1) 
					{
						/* $dokterbedah.=$line1->dokter_bedah.' ; ';
						$dokteran.=$line1->dokter_an.' ; ';
						$diagnosapreop.=$line1->diagnosa_pre_operatif.' ; ';
						$diagnosapostop.=$line1->diagnosa_post_operatif.' ; ';
						$jenisanastesi.=$line1->jenis_anasthesi.' ; ';
						$teknisanastesi.=$line1->teknis_anasthesi.' ; ';
						$premidikasi.=$line1->premidikasi.' ; ';
						$deskripsi.=$line1->deskripsi.' ; '; */
						
						if ($line1->dokter_bedah=='')
						{
							$dokterbedah='-';
						}
						else
						{
							$dokterbedah.=$line1->dokter_bedah.' ; ';
						}
						if ($line1->dokter_an=='')
						{
							$dokteran='-';
						}
						else
						{
							$dokteran.=$line1->dokter_an.' ; ';
						}
						if ($line1->diagnosa_pre_operatif=='')
						{
							$diagnosapreop='-';
						}
						else
						{
							$diagnosapreop.=$line1->diagnosa_pre_operatif.' ; ';
						}
						if ($line1->diagnosa_post_operatif=='')
						{
							$diagnosapostop='-';
						}
						else
						{
							$diagnosapostop.=$line1->diagnosa_post_operatif.' ; ';
						}
						if ($line1->jenis_anasthesi=='')
						{
							$jenisanastesi='-';
						}
						else
						{
							$jenisanastesi.=$line1->jenis_anasthesi.' ; ';
						}
						if ($line1->teknis_anasthesi=='')
						{
							$teknisanastesi='-';
						}
						else
						{
							$teknisanastesi.=$line1->teknis_anasthesi.' ; ';
						}
						if ($line1->premidikasi=='')
						{
							$premidikasi='-';
						}
						else
						{
							$premidikasi.=$line1->premidikasi.' ; ';
						}
						$deskripsi.=$line1->deskripsi.' ; ';
					}
				}
				else
				{
					foreach ($queryAll as $line1) 
					{
						if ($line1->dokter_bedah=='')
						{
							$dokterbedah='-';
						}
						else
						{
							$dokterbedah=$line1->dokter_bedah;
						}
						if ($line1->dokter_an=='')
						{
							$dokteran='-';
						}
						else
						{
							$dokteran=$line1->dokter_an;
						}
						if ($line1->diagnosa_pre_operatif=='')
						{
							$diagnosapreop='-';
						}
						else
						{
							$diagnosapreop=$line1->diagnosa_pre_operatif;
						}
						if ($line1->diagnosa_post_operatif=='')
						{
							$diagnosapostop='-';
						}
						else
						{
							$diagnosapostop=$line1->diagnosa_post_operatif;
						}
						if ($line1->jenis_anasthesi=='')
						{
							$jenisanastesi='-';
						}
						else
						{
							$jenisanastesi=$line1->jenis_anasthesi;
						}
						if ($line1->teknis_anasthesi=='')
						{
							$teknisanastesi='-';
						}
						else
						{
							$teknisanastesi=$line1->teknis_anasthesi;
						}
						if ($line1->premidikasi=='')
						{
							$premidikasi='-';
						}
						else
						{
							$premidikasi=$line1->premidikasi;
						}
						$deskripsi=$line1->deskripsi;
					}
				}
				$html.='
						<td>'.$dokterbedah.'&nbsp;</td>
						<td>'.$dokteran.'&nbsp;</td>
						<td>&nbsp;'.$diagnosapreop.'&nbsp;</td>
						<td>&nbsp;'.$diagnosapostop.'&nbsp;</td>
						<td>&nbsp;'.$jenisanastesi.'&nbsp;</td>
						<td>&nbsp;'.$teknisanastesi.'&nbsp;</td>
						<td>&nbsp;'.$premidikasi.'&nbsp;</td>
						<td>'.$deskripsi.'&nbsp;</td>
						<td width="100">'.date('d-M-Y',strtotime($line->tgl_operasi)).'&nbsp;</td>
						<td width="100">'.date('d-M-Y H:i',strtotime($line->jam_an_mulai)).'&nbsp;</td>
						<td width="100">'.date('d-M-Y H:i',strtotime($line->jam_an_selesai)).'&nbsp;</td>
						<td>&nbsp;'.$line->lama_an.'&nbsp;</td>
					  </tr>
				';
			}		
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="18" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		//----------------------------------------tampilkan data ke browser--------------------------------------------------------------------------------------------------
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$name='Lap_RekapitulasiOperasi.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=".$name);
		echo $html;
   	}
}
?>