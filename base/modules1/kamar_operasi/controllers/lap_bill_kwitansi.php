<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_bill_kwitansi extends MX_Controller {
	private $dbSQL 		= false;
	private $kd_user 	= '';
    public function __construct(){
        parent::__construct();
		$this->load->library('session');
		$this->load->helper('file');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_visite_dokter');
		$this->load->model('Tbl_data_pasien');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_pasien_inap');
		$this->load->model('Tbl_data_nginap');
		$this->load->model('Tbl_data_spesialisasi');
		$this->kd_user = $this->session->userdata['user_id']['id'];
    }	 

	public function index(){
        $this->load->view('main/index');       
   	}

	public function Cetak($print = null)
	{        
		$erorkwitansi = '';
		$erorbill     = '';
    	$params 	= array(
			'spesialisasi' 			=> $this->input->post('Spesialisasi'),
			'detail_spesialisasi' 	=> $this->input->post('dtlspesialisasi'),
			'tgl_pulang' 			=> $this->input->post('Tglpulang'),
			'jenis_print' 			=> $this->input->post('jenisprint'),
			'folio' 				=> $this->input->post('folio'),
			'no_transaksi' 			=> $this->input->post('notrans'),
			'kd_kasir' 				=> $this->input->post('KdKasir'),
			'nama' 					=> $this->input->post('nama'),
			'kd_pasien' 			=> $this->input->post('kdpasien'),
			'reff' 					=> $this->input->post('reff'),
			'tgl_masuk' 			=> $this->input->post('TglMasuk'),
			'kd_unit' 				=> $this->input->post('kdunit'),
			'kd_unit_kamar' 		=> $this->input->post('kdunit_kamar'),
			'co_status' 			=> $this->input->post('costat'),
			'cetakan' 				=> $this->input->post('cetakan'),
			'cetak_kwitansi'		=> $this->input->post('cetakkwitansi'),
    	);

        if ($params['cetakan'] === 'Billing' || $params['cetakan'] == '0') {
        	if ($params['cetak_kwitansi'] === 'false') {
        		$erorbill = $this->cetakBiling($params, $print);
        		if ($erorbill === "sukses") {
        			echo '{success: true}';
        		}else{
        			echo '{success: false}';
        		}
        	}else{
				$erorbill = $this->cetakBiling($params, $print);
				if ($erorbill === 'sukses') {
					$erorkwitansi =  $this->cetakkwitansi($params);

					if ($erorkwitansi === "sukses") {
						echo '{success: true}';
					}else{
						echo '{success: false}';
					}
				}else{
					echo '{success: false}';
				}
        		
        	}
        }else{
        	$erorkwitansi =  $this->cetakkwitansi($params);
        	if ($erorkwitansi === "sukses") {
				echo '{success: true}';
    		}else{
    			echo '{success: false}';
    		}
		}
    }
	

	public function preview_billing($kd_pasien=null, $kd_unit=null, $tgl_masuk=null, $kd_kasir=null, $no_transaksi=null, $co_status=null, $print=null){
		$query_transaksi = $this->db->query("SELECT * from transaksi WHERE no_transaksi = '".$no_transaksi."' and kd_kasir = '".$kd_kasir."'");
		if ($query_transaksi->num_rows() > 0) {
			if ($query_transaksi->row()->co_status == 't' || $query_transaksi->row()->co_status === true) {
				$tmp_co_status = 'true';
			}else{
				$tmp_co_status = 'false';
			}
		}else{
			$tmp_co_status = 'false';
		}

		$params = array(
			'kd_pasien' 	=> $kd_pasien, 
			'kd_unit' 		=> $kd_unit, 
			'tgl_masuk'		=> $tgl_masuk, 
			'kd_kasir' 		=> $kd_kasir, 
			'no_transaksi' 	=> $no_transaksi, 
			'co_status' 	=> $tmp_co_status, 
			// 'co_status' 	=> $co_status, 
		);
		$this->cetakBiling($params, $print);
		$content ="
		<code>
			<pre>".htmlspecialchars(file_get_contents("http://192.168.1.25/medismart/data_billing_ok.txt"))."</pre>
		</code>";
	    echo $content;
	}
	
	private function cetakBiling($params, $print = null){
		$tp         = new TableText(132,9,'',0,false);
		$setpage 	= new Pilihkertas;
		$user 				= $this->db->query("select user_names from zusers where kd_user = '".$this->kd_user."'")->row()->user_names;
		$TmpTotNonPaviliun 	= 0;
		$TmpTotPaviliun 	= 0;
		$TmpTotNonPaviliun_int 	= 0;
		$TmpTotPaviliun_int 	= 0;
		$data_rs = array();
		$data_rs = $this->data_rs();
		$data_pasien = array();

		$today       = date("d F Y");
		$Jam         = date("G:i:s");

		if ($print == null) {
			$tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
			ini_set('display_errors', '1');
			$bold1       = Chr(27) . Chr(69);
			$bold0       = Chr(27) . Chr(70);
			$initialized = chr(27) . chr(64);
			$condensed   = Chr(27) . Chr(33) . Chr(4);
			$condensed1  = chr(15);
			$condensed0  = chr(18);
			$condensed2  = Chr(27).Chr(33).Chr(32);
			$condensed4  = Chr(27).Chr(33).Chr(24);
			$condensed5  = chr(27).chr(33).chr(8);
		}else{
			$bold1       ="";
			$bold0       ="";
			$initialized ="";
			$condensed   ="";
			$condensed1  ="";
			$condensed0  ="";
			$condensed2  ="";
			$condensed4  ="";
			$condensed5  ="";
		}

		$criteriaParams = array(
			'kd_pasien' 	=> $params['kd_pasien'],
		);
		$queryDataPasien 	= $this->Tbl_data_pasien->getDataSelectedPasien($criteriaParams);
		unset($criteriaParams);
		$criteriaParams = array(
			'kd_pasien' => $params['kd_pasien'],
			'tgl_masuk' => $params['tgl_masuk'],
			'kd_unit'   => $params['kd_unit'],
		);

		$queryDataKunjungan = $this->Tbl_data_kunjungan->getDataSelectedKunjungan($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
		);

		$queryDataTransaksi = $this->Tbl_data_transaksi->getTransaksi($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_pasien' 		=> $params['kd_pasien'],
			//'kd_unit_kamar' 	=> $params['kd_unit_kamar'],
			'akhir' 			=> 'true',
			'tgl_masuk' 		=> $params['tgl_masuk'],
		);
		$queryDataPasienInap = $this->Tbl_data_nginap->selectNginap($criteriaParams);

		if ($queryDataPasienInap->num_rows() > 0) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_spesial' 	=> $queryDataPasienInap->row()->kd_spesial,
			);
			$queryDataSpesialisasi = $this->Tbl_data_spesialisasi->get($criteriaParams);
			$tmp_spesialisasi 	= $queryDataSpesialisasi->row()->spesialisasi;
			// $tmp_no_kamar 		= $queryDataSpesialisasi->row()->no_kamar;
		}else{
			$tmp_spesialisasi 	= "";
			// $tmp_no_kamar 		= "";
		}

		//select * from kamar where no_kamar = '659'
		$criteriaCustom = array(
			'field_criteria'=> 'no_kamar',
			// 'value_criteria'=> $tmp_no_kamar,
			'table' 		=> 'kamar',
			'field_return' 	=> 'nama_kamar',
		);
		if ($queryDataPasienInap->num_rows() > 0) {
			$criteriaCustom['value_criteria'] = $queryDataPasienInap->row()->no_kamar;
			$queryNamaKamar = $this->Tbl_data_transaksi->getCustom($criteriaCustom);
		}else{
			$queryNamaKamar = "";
		}
		

		unset($criteriaCustom);
		$criteriaCustom = array(
			'field_criteria'=> 'kd_customer',
			'value_criteria'=> $queryDataKunjungan->row()->kd_customer,
			'table' 		=> 'customer',
			'field_return' 	=> 'customer',
		);
		$queryNamaCustomer = $this->Tbl_data_transaksi->getCustom($criteriaCustom);

		$query_asal_inap 	= $this->db->query("SELECT * FROM unit_asal WHERE kd_kasir='".$params['kd_kasir']."' and no_transaksi ='".$params['no_transaksi']."'");
		$query_trans_ok 	= $this->db->query("SELECT * from transaksi where kd_kasir='".$query_asal_inap->row()->kd_kasir."' and no_transaksi ='".$query_asal_inap->row()->no_transaksi."'");
		$query_trans_asal 	= $this->db->query("SELECT * from transaksi where kd_kasir='".$query_asal_inap->row()->kd_kasir_asal."' and no_transaksi ='".$query_asal_inap->row()->no_transaksi_asal."'");
		$query_kunjungan_ok = $this->db->query("SELECT * FROM ok_jadwal_ps ojs 
												INNER JOIN ok_jadwal oj ON ojs.tgl_op = oj.tgl_op AND ojs.jam_op = oj.jam_op AND ojs.kd_unit = oj.kd_unit 
												--LEFT JOIN ok_kunjungan ok ON ok.kd_pasien = ojs.kd_pasien AND ok.tgl_op_mulai = ojs.tgl_op AND ok.kd_unit = ojs.kd_unit 
												WHERE ojs.kd_pasien = '".$params['kd_pasien']."' 
												--and ojs.tgl_op = '2017-07-19' 
												and ojs.kd_unit = '".$query_trans_ok->row()->kd_unit."' order by ojs.tgl_op desc limit 1");
		$query_kamar 		= $this->db->query("SELECT * FROM kamar WHERE no_kamar='".$query_kunjungan_ok->row()->no_kamar."'");
		$query_tindakan		= $this->db->query("SELECT * FROM ok_tindakan_medis WHERE kd_tindakan='".$query_kunjungan_ok->row()->kd_tindakan."'");
		/*unset($criteriaCustom);
		$criteria = " kd_kasir = '".$params['kd_kasir']."' and no_transaksi = '".$params['no_transaksi']."' ";
		$criteriaCustom = array(
			'field_criteria'=> $criteria,
			'value_criteria'=> null,
			'table' 		=> 'asal_inap ',
			'field_return' 	=> null,
		);
		$query_asal_inap = $this->Tbl_data_transaksi->getCustom($criteriaCustom);
		
		unset($criteriaCustom);
		$criteriaCustom = array(
			'field_criteria'=> " kd_kasir = '".$query_asal_inap->row()->kd_kasir."' and no_transaksi = '".$query_asal_inap->row()->no_transaksi."' ",
			'value_criteria'=> null,
			'table' 		=> 'transaksi ',
			'field_return' 	=> null,
		);
		$query_transaksi_asal = $this->Tbl_data_transaksi->getCustom($criteriaCustom);*/

        $tmpTanggalLahir 	= $this->db->query("SELECT getumur('".date('Y-m-d')."', '".date_format(date_create($queryDataPasien->row()->tgl_lahir), 'Y-m-d')."')")->row()->getumur;
        $tmpTanggalLahir 	= str_replace("years","Tahun",$tmpTanggalLahir);
        $tmpTanggalLahir 	= str_replace("mons","Bulan",$tmpTanggalLahir);
        $tmpTanggalLahir 	= str_replace("days","Hari", $tmpTanggalLahir);
		# SET JUMLAH KOLOM BODY
		
		# SET HEADER REPORT
		if ($print == null) {
			$tp	->addColumn(chr(27).chr(33).chr(24).$bold1."NO TRANSAKSI : ".$params['kd_kasir']."-".$params['no_transaksi'].chr(27).chr(33).chr(8).$condensed1, 9,"right")
				->commit("header")
				->addSpace("header")
				->addColumn($data_rs['rs_name'], 9,"left")
				->commit("header")
				->addColumn($data_rs['rs_address'].", ".$data_rs['rs_city'], 9,"left")
				->commit("header")
				->addColumn($data_rs['telp'], 9,"left")
				->commit("header")
				->addColumn($data_rs['fax'], 9,"left")
				->commit("header")
				->addColumn(chr(27).chr(33).chr(24)."PERINCIAN BIAYA TINDAKAN OPERASI / PERSALINAN".chr(27).chr(33).chr(8).$bold0.$condensed1, 8,"center")
				->commit("body");
				
		}else{
			$tp	->addColumn("NO TRANSAKSI : ".$params['kd_kasir']."-".$params['no_transaksi'], 9,"right")
				->commit("header")
				->addSpace("header")
				->addColumn($data_rs['rs_name'], 9,"left")
				->commit("header")
				->addColumn($data_rs['rs_address'].", ".$data_rs['rs_city'], 9,"left")
				->commit("header")
				->addColumn($data_rs['telp'], 9,"left")
				->commit("header")
				->addColumn($data_rs['fax'], 9,"left")
				->commit("header")
				->addColumn("PERINCIAN BIAYA TINDAKAN OPERASI / PERSALINAN", 8,"center")
				->commit("body");
				
		}

		$tp ->setColumnLength(0, 23); 	//DESKRIPSI
		$tp ->addColumn("===================================================================================================================================", 9,"left")
			->commit("body");
		
		# SET JUMLAH KOLOM HEADER
		

		/*$tp ->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setUseBodySpace(true);*/

		/*$tp ->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 50);*/

		$tp ->setColumnLength(0, 21) 	//DESKRIPSI
			->setColumnLength(1, 51) 	//ISI
			->setColumnLength(2, 21) 	//DESKRIPSI		
			->setColumnLength(3, 31); 	//ISI
		

		$tp	->addColumn("No. Medrec        : ", 1,"left")
			->addColumn($queryDataPasien->row()->kd_pasien, 1,"left")
			->addColumn("Kelompok Pasien   : ", 1,"left")
			->addColumn($queryNamaCustomer, 1,"left")
			->commit("body");

		$tp	->addColumn("Nama pasien       : ", 1,"left")
			->addColumn($queryDataPasien->row()->nama, 1,"left")
			->addColumn("Ruang /Kelas      : ", 1,"left")
			->addColumn($query_kamar->row()->nama_kamar, 1,"left")
			->commit("body");
/*
		$tp	->addColumn("Umur              : ", 1,"left")
			->addColumn(substr($tmpTanggalLahir, 0, strlen($tmpTanggalLahir)-1), 1,"left")
			->commit("body");
*/

		if ($queryDataPasien->row()->jenis_kelamin == 't' || $queryDataPasien->row()->jenis_kelamin == true) {
			$tp	->addColumn("Jenis kelamin     : ", 1,"left")
				->addColumn("Laki - laki", 1,"left")
				->addColumn("Tanggal dan Jam   : ", 1,"left")
				->addColumn(date_format(date_create($query_kunjungan_ok->row()->tgl_op), 'Y-m-d')." ".date_format(date_create($query_kunjungan_ok->row()->jam_op), 'H:i:s'), 1,"left")
				->commit("body");
		}else{
			$tp	->addColumn("Jenis kelamin     : ", 1,"left")
				->addColumn("Perempuan", 1,"left")
				->addColumn("Tanggal dan Jam   : ", 1,"left")
				->addColumn(date_format(date_create($query_kunjungan_ok->row()->tgl_op), 'Y-m-d')." ".date_format(date_create($query_kunjungan_ok->row()->jam_op), 'H:i:s'), 1,"left")
				->commit("body");
		}
		

		$tp	->addColumn("Alamat            : ", 1,"left")
			->addColumn($queryDataPasien->row()->alamat, 1,"left")
			->addColumn("Spesialisasi      : ", 1,"left")
			->addColumn($tmp_spesialisasi, 1,"left")
			->commit("body");

		$query_transaksi = $this->db->query("SELECT * from transaksi WHERE no_transaksi = '".$params['no_transaksi']."' and kd_kasir = '".$params['kd_kasir']."'");
		if ($query_transaksi->num_rows() > 0) {
			if ($query_transaksi->row()->co_status == 't' || $query_transaksi->row()->co_status === true) {
				$params['co_status'] = 'true';
			}else{
				$params['co_status'] = 'false';
			}
		}else{
			$params['co_status'] = 'false';
		}

		$tp	->addColumn("Tanggal Lahir     : ", 1,"left")
			->addColumn(substr($tmpTanggalLahir, 0, strlen($tmpTanggalLahir)-1), 1,"left")
			->commit("body");

		/*$tp	->addColumn("Kelompok Pasien   : ", 1,"left")
			->addColumn($queryNamaCustomer, 1,"left")
			->commit("body");*/

		/*$tp	->addColumn("Ruang /Kelas      : ", 1,"left")
			->addColumn($query_kamar->row()->nama_kamar, 1,"left")
			->commit("body");*/

		/*$tp	->addColumn("Tanggal dan Jam   : ", 1,"left")
			->addColumn(date_format(date_create($query_trans_asal->row()->tgl_transaksi), 'Y-m-d')." ".date_format(date_create($query_kunjungan_ok->row()->jam_op), 'H:i:s'), 1,"left")
			->commit("body");*/

		/*$tp	->addColumn("Spesialisasi      : ", 1,"left")
			->addColumn($tmp_spesialisasi, 1,"left")
			->commit("body");
*/
		$tp ->setColumnLength(0, 32) 	//DESKRIPSI
			->setColumnLength(1, 0);

/*
		$tp ->setColumnLength(0, 23); 	//DESKRIPSI*/
		$tp ->addColumn("===================================================================================================================================", 9,"left")
			->commit("body");
        $query = $this->db->query("SELECT distinct header,urut,kd_unit_tr, kd_unit from getallbillkasirrwi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['co_status']."')  WHERE kd_unit = '".$query_trans_asal->row()->kd_unit."' order by urut ASC");
			
		$tp ->setColumnLength(0, 75) 	//DESKRIPSI
			->setColumnLength(1, 5) 	//RP
			->setColumnLength(2, 20) 	//RP 1		
			->setColumnLength(3, 5) 	//RP
			->setColumnLength(4, 20);
		$no = 0;

       
        foreach ($query->result() as $line) {
        	$no++;
        	if ($line->header != 'NULL') {
	        	$tp	->addColumn($line->header, 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", 1,"left")
					->commit("body");

				$querydetail = $this->db->query("SELECT * from getallbillkasirrwi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['co_status']."') where header = '".$line->header."' 
					AND kd_unit_tr='".$line->kd_unit_tr."' 
					AND urut='".$line->urut."' 
					AND header not in ('NULL') order by urut ASC");

				foreach ($querydetail->result() as $linedetail) 
				{

					$totaldetail = $linedetail->tarif * $linedetail->qty;
					$TmpTotNonPaviliun += $totaldetail;
					if (strtolower($linedetail->desk_quantity) == 'true') {
						$desk_quantity = "Hari";
					}else{
						$desk_quantity = "X";
					}
					if ($linedetail->header != "NULL") {
						$spacing = "     ";
					}else{
						$spacing = "";
					}
					$tmpDataTarif = number_format($linedetail->tarif,0,'.',',');
					$tmpDataTotal = number_format($totaldetail,0,'.',',');
		        	$tp	->addColumn($spacing.$linedetail->deskripsi." ".$linedetail->qty." ".$desk_quantity, 1,"left")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTarif, 1,"right")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTotal, 1,"right")
						->commit("body");


						if (strtolower($linedetail->detail_dr) == 'true') {
							$querycekdokter = $this->db->query("SELECT DISTINCT(d.nama), COUNT(d.nama) as jumlah from 
								detail_transaksi dt 
								inner join
								visite_dokter v on dt.kd_kasir = v.kd_kasir AND dt.no_transaksi = v.no_transaksi AND dt.tgl_transaksi = v.tgl_transaksi AND dt.urut = v.urut 
								inner join dokter d ON d.kd_dokter = v.kd_dokter 
								where dt.kd_kasir = '".$linedetail->kd_kasir."' and dt.no_transaksi = '".$linedetail->no_transaksi."' and dt.kd_unit='".$linedetail->kd_unit."' and dt.kd_produk = '".$linedetail->kd_produk."' 
								group by d.nama 
								order by d.nama asc");
							foreach ($querycekdokter->result() as $linedokter){
								if (strtolower($linedetail->detail_dr) == 'true') {
									$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn(" ", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}else{
						        	$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}
								//$Data .= "<br>";
								//$Data .= $linedokter->nama."<br>";
							}
						}
				}
        	}else{

				$querydetail = $this->db->query("SELECT * from getallbillkasirrwi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['co_status']."') where header = '".$line->header."' AND kd_unit_tr='".$line->kd_unit_tr."' 
					AND urut='".$line->urut."' AND header ='NULL' and urut='".$line->urut."' order by urut ASC");

				foreach ($querydetail->result() as $linedetail) 
				{

					$totaldetail = $linedetail->tarif * $linedetail->qty;
					$TmpTotNonPaviliun += $totaldetail;
					if (strtolower($linedetail->desk_quantity) == 'true') {
						$desk_quantity = "Hari";
					}else{
						$desk_quantity = "X";
					}
					if ($linedetail->header != "NULL") {
						$spacing = "     ";
					}else{
						$spacing = "";
					}
					$tmpDataTarif = number_format($linedetail->tarif,0,'.',',');
					$tmpDataTotal = number_format($totaldetail,0,'.',',');
		        	$tp	->addColumn($spacing.$linedetail->deskripsi." ".$linedetail->qty." ".$desk_quantity, 1,"left")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTarif, 1,"right")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTotal, 1,"right")
						->commit("body");


						if (strtolower($linedetail->dokter) == 'true') {
							$querycekdokter = $this->db->query("SELECT DISTINCT(d.nama), COUNT(d.nama) as jumlah from 
								detail_transaksi dt 
								inner join
								visite_dokter v on dt.kd_kasir = v.kd_kasir AND dt.no_transaksi = v.no_transaksi AND dt.tgl_transaksi = v.tgl_transaksi AND dt.urut = v.urut 
								inner join dokter d ON d.kd_dokter = v.kd_dokter 
								where dt.kd_kasir = '".$linedetail->kd_kasir."' and dt.no_transaksi = '".$linedetail->no_transaksi."' and dt.kd_unit='".$linedetail->kd_unit."' and dt.kd_produk = '".$linedetail->kd_produk."' 
								group by d.nama 
								order by d.nama asc");
							foreach ($querycekdokter->result() as $linedokter){
								if (strtolower($linedetail->detail_dr) == 'true') {
									$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn(" ", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}else{
						        	$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}
								//$Data .= "<br>";
								//$Data .= $linedokter->nama."<br>";
							}
						}
				}
        	}
		}

		$TmpTotPaviliun_int = $TmpTotPaviliun;
		/*if ($TmpTotPaviliun_int != 0) {
			$tp ->setColumnLength(0, 102) 	//DESKRIPSI
				->setColumnLength(1, 5) 	//RP
				->setColumnLength(2, 20);

			$TmpTotPaviliun = number_format($TmpTotPaviliun,0,'.',',');
		    $tp	->addColumn("SUB TOTAL PAVILIUN", 1,"right")
				->addColumn(" Rp. ", 1,"left")
				->addColumn($TmpTotPaviliun, 1,"right")
				->commit("body");
		}*/


        $query = $this->db->query("SELECT distinct header,urut,kd_unit_tr from getallbillkasirrwi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['co_status']."')  WHERE kd_unit not like '".$query_trans_asal->row()->kd_unit."' order by urut ASC");
			
		$tp ->setColumnLength(0, 75) 	//DESKRIPSI
			->setColumnLength(1, 5) 	//RP
			->setColumnLength(2, 20) 	//RP 1		
			->setColumnLength(3, 5) 	//RP
			->setColumnLength(4, 20);
		$no = 0;

        foreach ($query->result() as $line) {
        	$no++;
        	if ($line->header != 'NULL') {
	        	$tp	->addColumn($line->header, 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", 1,"left")
					->addColumn("", 1,"left")
					->commit("body");

				$querydetail = $this->db->query("SELECT * from getallbillkasirrwi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['co_status']."') where header = '".$line->header."' 
					AND kd_unit_tr='".$line->kd_unit_tr."' 
					AND urut='".$line->urut."' 
					AND header not in ('NULL') order by urut ASC");

				foreach ($querydetail->result() as $linedetail) 
				{

					$totaldetail = $linedetail->tarif * $linedetail->qty;
					$TmpTotNonPaviliun += $totaldetail;
					if (strtolower($linedetail->desk_quantity) == 'true') {
						$desk_quantity = "Hari";
					}else{
						$desk_quantity = "X";
					}
					if ($linedetail->header != "NULL") {
						$spacing = "     ";
					}else{
						$spacing = "";
					}
					$tmpDataTarif = number_format($linedetail->tarif,0,'.',',');
					$tmpDataTotal = number_format($totaldetail,0,'.',',');
		        	$tp	->addColumn($spacing.$linedetail->deskripsi." ".$linedetail->qty." ".$desk_quantity, 1,"left")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTarif, 1,"right")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTotal, 1,"right")
						->commit("body");


						if (strtolower($linedetail->dokter) == 'true') {
							$querycekdokter = $this->db->query("SELECT DISTINCT(d.nama), COUNT(d.nama) as jumlah from 
								detail_transaksi dt 
								inner join
								visite_dokter v on dt.kd_kasir = v.kd_kasir AND dt.no_transaksi = v.no_transaksi AND dt.tgl_transaksi = v.tgl_transaksi AND dt.urut = v.urut 
								inner join dokter d ON d.kd_dokter = v.kd_dokter 
								where dt.kd_kasir = '".$linedetail->kd_kasir."' and dt.no_transaksi = '".$linedetail->no_transaksi."' and dt.kd_unit='".$linedetail->kd_unit."' and dt.kd_produk = '".$linedetail->kd_produk."' 
								group by d.nama
								order by d.nama asc");
							foreach ($querycekdokter->result() as $linedokter){
								if (strtolower($linedetail->detail_dr) == 'true') {
									$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn(" ", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}else{
						        	$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}
								//$Data .= "<br>";
								//$Data .= $linedokter->nama."<br>";
							}
						}
				}
        	}else{

				$querydetail = $this->db->query("SELECT * from getallbillkasirrwi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['co_status']."') where header = '".$line->header."' AND kd_unit_tr='".$line->kd_unit_tr."' 
					AND urut='".$line->urut."' AND header ='NULL' and urut='".$line->urut."' order by urut ASC");

				foreach ($querydetail->result() as $linedetail) 
				{

					$totaldetail = $linedetail->tarif * $linedetail->qty;
					$TmpTotNonPaviliun += $totaldetail;
					if (strtolower($linedetail->desk_quantity) == 'true') {
						$desk_quantity = "Hari";
					}else{
						$desk_quantity = "X";
					}
					if ($linedetail->header != "NULL") {
						$spacing = "     ";
					}else{
						$spacing = "";
					}
					$tmpDataTarif = number_format($linedetail->tarif,0,'.',',');
					$tmpDataTotal = number_format($totaldetail,0,'.',',');
		        	$tp	->addColumn($spacing.$linedetail->deskripsi." ".$linedetail->qty." ".$desk_quantity, 1,"left")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTarif, 1,"right")
						->addColumn(" Rp. ", 1,"left")
						->addColumn($tmpDataTotal, 1,"right")
						->commit("body");


						if (strtolower($linedetail->dokter) == 'true') {
							$querycekdokter = $this->db->query("SELECT DISTINCT(d.nama), COUNT(d.nama) as jumlah from 
								detail_transaksi dt 
								inner join
								visite_dokter v on dt.kd_kasir = v.kd_kasir AND dt.no_transaksi = v.no_transaksi AND dt.tgl_transaksi = v.tgl_transaksi AND dt.urut = v.urut 
								inner join dokter d ON d.kd_dokter = v.kd_dokter 
								where dt.kd_kasir = '".$linedetail->kd_kasir."' and dt.no_transaksi = '".$linedetail->no_transaksi."' and dt.kd_unit='".$linedetail->kd_unit."' and dt.kd_produk = '".$linedetail->kd_produk."' 
								group by d.nama
								order by d.nama asc");
							foreach ($querycekdokter->result() as $linedokter){
								if (strtolower($linedetail->detail_dr) == 'true') {
									$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn(" ", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}else{
						        	$tp	->addColumn($spacing."     ".$linedokter->nama." ".$linedokter->jumlah." X", 1,"left")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->addColumn("", 1,"left")
										->addColumn("", 1,"right")
										->commit("body");
								}
								//$Data .= "<br>";
								//$Data .= $linedokter->nama."<br>";
							}
						}
				}
        	}
		}

		$TmpTotNonPaviliun_int = $TmpTotNonPaviliun;
		/*if ($TmpTotNonPaviliun_int !=0) {
			$TmpTotNonPaviliun = number_format($TmpTotNonPaviliun,0,'.',',');
			$tp ->setColumnLength(0, 102) 	//DESKRIPSI
				->setColumnLength(1, 5) 	//RP
				->setColumnLength(2, 20);
		    $tp	->addColumn("SUB TOTAL NON PAVILIUN", 1,"right")
				->addColumn(" Rp. ", 1,"left")
				->addColumn($TmpTotNonPaviliun, 1,"right")
				->commit("body");
		}*/

		$tp ->setColumnLength(0, 23); 	//DESKRIPSI
		$tp	->addColumn("-----------------------------------------------------------------------------------------------------------------------------------".$bold1, 9,"left")
			->commit("body");


		$tp ->setColumnLength(0, 75) 	//DESKRIPSI
			->setColumnLength(1, 5) 	//RP
			->setColumnLength(2, 20) 	//RP 1		
			->setColumnLength(3, 5) 	//RP
			->setColumnLength(4, 20);


	    $tp	->addColumn($user, 1,"left")
			->addColumn("", 1,"left")
			->addColumn("TOTAL BIAYA", 1,"right")
			->addColumn(" Rp. ", 1,"left")
			->addColumn(number_format((int)$TmpTotNonPaviliun_int+(int)$TmpTotPaviliun_int,0,'.',','), 1,"right")
			->commit("body");

		$queryTotJum = $this->db->query("SELECT uraian,jumlah,urut from detail_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $params['no_transaksi'] . "'  order by urut asc");

		$tp ->setColumnLength(0, 75) 	//DESKRIPSI
			->setColumnLength(1, 5) 	//RP
			->setColumnLength(2, 20) 	//RP 1		
			->setColumnLength(3, 5) 	//RP
			->setColumnLength(4, 20);
        foreach ($queryTotJum->result() as $line) {

		    $tp	->addColumn("", 1,"left")
				->addColumn("", 1,"left")
				->addColumn($line->uraian, 1,"right")
				->addColumn(" Rp. ", 1,"left")
				->addColumn(number_format($line->jumlah,0,'.',','), 1,"right")
				->commit("body");
            //$Data .= str_pad($line->uraian, 110, " ", STR_PAD_LEFT) . str_pad(number_format($line->jumlah, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }


		$tp ->setColumnLength(0, 75) 	//DESKRIPSI
			->setColumnLength(1, 5) 	//RP
			->setColumnLength(2, 10) 	//RP 1		
			->setColumnLength(3, 5) 	//RP
			->setColumnLength(4, 30);

		$tp	->addSpace("body")
			// ->addColumn("Keterangan Rangkap : ", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn($data_rs['rs_city'] . ' , ' . date("d F Y"), 1,"right")
			->commit("body");

		$tp ->addColumn("", 1,"left")	
		 //->addColumn("- Lembar 1 : Pasien", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			// ->addColumn("Petugas Rincian Biaya Pasien", 1,"right")
			 ->commit("body");

		 $tp	->addColumn("", 1,"left")
		   //->addColumn("- Lembar 2 : Keuangan", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"right")
			 ->commit("body");

		 $tp ->addColumn("", 1,"left")
		 //->addColumn("- Lembar 3 : Rekam Medis", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->addColumn("", 1,"left")
			 ->commit("body");

		$tp	->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("Kasir Kamar Operasi", 1,"right")
			->commit("body");


		$tp ->setColumnLength(0, 133);
		// $tp	->addLine("footer")
			// ->addColumn("NB : Mohon maaf apabila ada lembar tagihan yang belum tertagihkan dalam perincian ini, akan ditagihkan kemudian".$bold0, 1,"left")
			// ->commit("footer");
		$tp	->addLine("footer")
			->addColumn("Banyaknya Uang : ".$bold1.terbilang((int)$TmpTotNonPaviliun_int+(int)$TmpTotPaviliun_int)." Rupiah".$bold0, 1,"left")
			->commit("footer");

       /* $Data .= str_pad("Keterangan Rangkap : ", 65, " ") . str_pad($Kota . ' , ' . $today, 65, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad("- Lembar 1 : Pasien       ", 65, " ") .str_pad("Petugas Rincian Biaya Pasien", 65, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad("- Lembar 2 : Keuangan     ", 65, " ") ."\n";
        $Data .= str_pad("- Lembar 3 : Rekam Medis  ", 65, " ") ."\n";
        $Data .= str_pad("", 65-8, " ") . str_pad("Kasir Rawat Inap", 65, " ", STR_PAD_LEFT) . "\n";*/


		$data = $tp->getText();
		/*if ( !write_file(APPPATH.'/files/data_billing.txt', $data)){
		     echo 'Unable to write the file';
		}*/
		$fp = fopen("data_billing_ok.txt","wb");
		fwrite($fp,$data);
		fclose($fp);
		$file =  'data_billing_ok.txt';  # nama file temporary yang akan dicetak
		//$handle = fopen('data_billing.txt', 'w');
		/*$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;*/
		//echo $Data;
		//fwrite($handle, $data);
		//
		//
		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed    = chr(12); # mengeksekusi $feed
		$bold1       = Chr(27) . Chr(69); # Teks bold
		$bold0       = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$margin      = chr(27) . chr(78). chr(90);
		$margin_off  = chr(27) . chr(79);
		
		// $fast_mode	 = chr(27) . chr(120) . chr(48); # fast print / low quality
		// $fast_print_on	 = chr(27) . chr(115). chr(1); # fast print on
		// $low_quality_off = chr(120) . chr(1); # low quality

		$Data  = $initialized;
		$Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
		// $Data .= $fast_mode; # fast print / low quality
		// $Data .= $low_quality_off; # low quality
		// $Data .= $fast_print_on; # fast print on / enable
		$Data .= $condensed1;
		$Data .= $margin;
		$Data .= $data;
		//$Data .= $margin_off;
		$Data .= $formfeed;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$this->kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		
		if ($print == null) {
	        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
		}
       	return 'sukses';	
	}

	public function cetakkwitansi($params){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$tp 		= new TableText(132,9,'',0,false);
		$setpage 	= new Pilihkertas;
		$resultPG 	= false;
		$resultSQL 	= false;
		$strError = "";
		$no_transaksi = $_POST["notrans"];
        $Total = 0;

        $kd_user = $this->session->userdata['user_id']['id'];
        $q = $this->db->query("SELECT SUM(db.jumlah) as total_jumlah from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '".$params['no_transaksi']."' and db.kd_pay in (SELECT kd_pay FROM payment where jenis_pay = '1')");//-- and jenis_pay = 1
        
        $queryNota = $this->dbSQL->query("SELECT MAX(no_nota) as no_nota from nota_bill where kd_kasir = '".$params['kd_kasir']."'");
        if ($queryNota->num_rows() > 0) {
        	$no_nota = (int)$queryNota->row()->no_nota + 1;
        }else{
        	$no_nota = 1;
        }

        $labelNota = "0000000";
        $labelNota = substr($labelNota, strlen($no_nota)).$no_nota;
        //echo $labelNota;kdunit
        $paramsInsert = array(
			'jumlah' 		=> 0,
			'kd_kasir' 		=> $this->input->post('KdKasir'),
			'no_transaksi' 	=> $this->input->post('notrans'),
			'no_nota' 		=> $no_nota,
			'kd_user' 		=> $this->session->userdata['user_id']['id'],
			'kd_unit' 		=> $this->input->post('kdunit'),
			'tgl_cetak' 	=> date('Y-m-d'),
			'jenis' 		=> 'TO',
        );
        $resultPG 	= $this->db->insert('nota_bill', $paramsInsert);
        $resultSQL 	= $this->dbSQL->insert('nota_bill', $paramsInsert);
        if($q->num_rows() == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
/*
        foreach ($q->result() as $data)
        {
            $Total = $data->jumlah;
        }*/

		$Total = $q->row()->total_jumlah;
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        
        $criteria = "no_transaksi = '".$no_transaksi."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
			$Medrec      = $query[0][0]->KD_PASIEN;
			$Status      = $query[0][0]->STATUS;
			$Dokter      = $query[0][0]->DOKTER;
			$Nama        = $query[0][0]->NAMA;
			$Alamat      = $query[0][0]->ALAMAT;
			$Poli        = $query[0][0]->UNIT;
			$Notrans     = $query[0][0]->NO_TRANSAKSI;
			$Tgl         = $query[0][0]->TGL_TRANS;
			$KdUser      = $query[0][0]->KD_USER;
			$uraian      = $query[0][0]->DESKRIPSI;
			$jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
        }
        $waktu = explode(" ",$Tgl);
        $tanggal = $waktu[0];
		$printer=$this->db->query("select p_kwitansi from zusers where kd_user='".$this->kd_user."'")->row()->p_kwitansi;//'EPSON-LX-310-ME';
        $nosurat=$this->db->query("select setting from sys_setting where key_data = 'no_surat_default_kwitansi'")->row()->setting;

        $t1 = 4;
        $t3 = 30;
        $t2 = 36 - ($t3 + $t1);
        // $printer = "192.168.0.39\Epson-LX-310-ip39-17-10-2017";       
		$format1     = date('d F Y', strtotime($Tgl));
		$today       = Bulaninindonesia(date("d F Y"));
		$Jam         = date("G:i:s");
		$tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file        =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$bold1       = Chr(27) . Chr(69);
		$bold0       = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$condensed2  = Chr(27).Chr(33).Chr(32);


		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$feed        = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed    = chr(12); # mengeksekusi $feed
		$bold1       = Chr(27) . Chr(69); # Teks bold
		$bold0       = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$margin      = chr(27) . chr(78). chr(90);
		$margin_off  = chr(27) . chr(79);


        $Data  = $initialized;
		$Data .= $setpage->PageLength('laporan'); # PEMANGGILAN UKURAN KERTAS (UKURAN KERTAS BIASA DIBAGI 3)
        $Data .= $condensed1;
		$Data .= $margin;
        $Data .= chr(27) . chr(87) . chr(49);
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad($bold1.$condensed2."K W I T A N S I".$condensed1.$bold0,62," ",STR_PAD_BOTH)."\n\n";
        $Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $bold0;
        //$Data .= str_pad("No. Kwitansi                 : ".$bold1.$labelNota,40," ").str_pad($nosurat,40," ",STR_PAD_LEFT)."\n";
        $Data .= str_pad("No. Kwitansi                 : ".chr(27).chr(33).chr(24).$bold1.$labelNota.chr(27).chr(33).chr(8),40," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("No. Transaksi                : ".chr(27).chr(33).chr(24).$bold1.$no_transaksi.chr(27).chr(33).chr(8),40," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("No. Medrec                   : ".$bold1.$Medrec,40," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("Telah Terima Dari            : ".$bold1.$Nama, 40," ")."\n";
        $Data .= $bold0;
        if ($Total != 0 || $Total != "") {
        	$Data .= "Banyaknya uang               : Rp. ".chr(27).chr(33).chr(24).$bold1.number_format($Total,0,'.',',').chr(27).chr(33).chr(8).$bold0."\n";
        }
        $Data .= "\n";
        $Data .= "Untuk Pembayaran Biaya Rawat Inap RSUD dr. Soedono Madiun Madiun "."\n";
        $Data .= "a/n ".$Nama."   Pada Tanggal ".$tanggal."\n";
        $Data .= "Banyaknya uang terbilang     : ".$bold1.terbilang($Total)." Rupiah".$bold0."\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($Kota . ' , ' . $today, 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("Bendahara", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("RSUD DR. SOEDOONO MADIUN", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("(---------------------)", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 40, " ", STR_PAD_BOTH) . "\n";
        $Data .= $formfeed;
        fwrite($handle, $Data);
        fclose($handle);
		//echo $Data;
		//
		if (($resultPG>0 || $resultPG===true) && ($resultSQL>0 || $resultSQL===true)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
		}
        
        //copy($file, $printer);  # Lakukan cetak
        //unlink($file);
        
        //echo $file;
        $strError = "sukses";
        
        
        }
        return $strError;		
	}

	private function data_rs(){
		$response = array();
		$kd_rs = $this->session->userdata['user_id']['kd_rs'];
		$rs    = $this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$response['telp']  	= '';
		$response['fax']   	= '';
		$response['telp1'] 	= '';
		$response['rs_name']= '';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$response['telp'] 	= 'Telp. ';
			$response['telp1'] 	= false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$response['telp1'] 	= 	true;
				$response['telp'] 	.= 	$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($response['telp1']==true){
					$response['telp'] .= '/'.$rs->phone2.'.';
				}else{
					$response['telp'] .= $rs->phone2.'.';
				}
			}else{
				$$response['telp'].='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$response['fax']='Fax. '.$rs->fax.'.';
		}
		$response['rs_name'] 	= $rs->name;
		$response['rs_address'] = $rs->address;
		$response['rs_city'] 	= $rs->city;
		return $response;
	}

	private function data_pasien($params){
		$criteriaParams = array(
			'kd_pasien' 	=> $params['kd_pasien'],
		);
		$query 	= $this->Tbl_data_pasien->getDataSelectedPasien($criteriaParams);
		return $query->result_array();
	}
}
?>