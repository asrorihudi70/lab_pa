<?php
class tb_getcmbisikelompokpasienok extends TblBase
{
    function __construct()
    {
        $this->TblName='';
        TblBase::TblBase(true);
        $this->SqlQuery= "select customer.kd_customer,customer.customer, case when kontraktor.jenis_cust=0 then 'Perorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as jenis_cust
							from customer 
							inner join kontraktor on customer.kd_customer=kontraktor.kd_customer";
    }

    function FillRow($rec)
    {
        $row=new Rowam_pasien();
        $row->kd_customer=$rec->kd_customer;
		$row->customer=$rec->customer;
        return $row;
    }

}

class Rowam_pasien
{
    public $kd_customer;
    public $customer;
}
?>
