<?php
class tb_getitempemitempemeriksaan extends TblBase
{
    function __construct()
    {
        $this->TblName='item_pemeriksaan';
        TblBase::TblBase(true);
        $this->SqlQuery= "select * from item_pemeriksaan order by kd_item asc";
    }

    function FillRow($rec)
    {
        $row=new Rowam_item_pemeriksaan();
        $row->kd_item=$rec->kd_item;
		$row->nama_item=$rec->item;
        return $row;
    }

}

class Rowam_item_pemeriksaan
{
    public $kd_item;
    public $nama_item;
}
?>
