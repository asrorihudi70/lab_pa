﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewkasirlebrad extends TblBase
{
	
	function __construct()// jenis_pay 
	{
		$this->StrSql="customer,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,urut_masuk',posting_transaksi,uraian,cara_bayar
		,ket_payment,kd_pay,jenis_pay,type_data,lunas,co_status,kd_kasir,namaunitasal,kd_kasir_asal,no_transaksi_asal";
		$this->SqlQuery="
				select * from(     select tr.no_transaksi,cus.customer,ua.kd_kasir_asal,ua.no_transaksi_asal,uu.nama_unit as namaunitasal,
									   u.nama_unit,u.kd_bagian, p.nama, p.alamat, tr.kd_pasien as kode_pasien, d.kd_dokter
									   ,d.nama as nama_dokter,
									   tr.tgl_transaksi, tr.lunas, tr.kd_kasir, tr.co_status, 
									   u.kd_kelas,k.kd_customer,k.urut_masuk,k.kd_unit, 
									   knt.jenis_cust,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,
									   payment_type.jenis_pay,payment.kd_pay,payment_type.type_data, 
									   tr.posting_transaksi from transaksi tr  
									   inner join pasien p on p.kd_pasien=tr.kd_pasien
									   inner join unit  u on u.kd_unit=tr.kd_unit
									   inner join kunjungan k on k.kd_pasien=tr.kd_pasien and k.kd_unit=tr.kd_unit and
									   tr.tgl_transaksi=k.tgl_masuk and tr.urut_masuk=k.urut_masuk  
									   inner join customer cus on cus.kd_customer= k.kd_customer
									   left  join kontraktor knt on knt.kd_customer=k.kd_customer
									   inner join payment on payment.kd_customer = k.kd_customer
									   inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
									   inner join dokter d on d.kd_dokter= k.kd_dokter
									   left join unit_asal ua on ua.kd_kasir=tr.kd_kasir and ua.no_transaksi=tr.no_transaksi
									   inner join transaksi tra on tra.no_transaksi=ua.no_transaksi_asal and tra.kd_kasir=ua.kd_kasir_asal
									   inner join unit  uu on uu.kd_unit=tra.kd_unit
									
									   
									   )as resdata  ";
		$this->TblName='viewdetailkasirpenunjang';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->KD_PASIEN=$rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI=$rec->tgl_transaksi;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
//		$row->FLAG=$rec->flag;
		$row->KET_PAYMENT=$rec->ket_payment;
		$row->CARA_BAYAR=$rec->cara_bayar;
		$row->KD_PAY=$rec->kd_pay;
		$row->JENIS_PAY=$rec->jenis_pay;
		$row->TYPE_DATA=$rec->type_data;
		$row->POSTING=$rec->posting_transaksi;
		$row->LUNAS=$rec->lunas;
		$row->CO_STATUS=$rec->co_status;
		$row->KD_KASIR=$rec->kd_kasir;
		$row->NAMAUNITASAL=$rec->namaunitasal;
		$row->KD_KASIR_ASAL=$rec->kd_kasir_asal;
		$row->NO_TRANSAKSI_ASAL=$rec->no_transaksi_asal;
		return $row;
	}
}
class Rowdokter
{
	public $KD_UNIT;
        public $NAMA_UNIT;
        public $NAMA;
	public $ALAMAT;
        public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
//	public $FLAG;
	public $KET_PAYMENT;
	public $CARA_BAYAR;
	public $KD_PAY;
	public $JENIS_PAY;
	public $TYPE_DATA;
	public $POSTING;
	public $LUNAS;
	public $CO_STATUS;
	public $KD_KASIR;
	public $NAMAUNITASAL;
	public $KD_KASIR_ASAL;
	public $NO_TRANSAKSI_ASAL;
}

?>