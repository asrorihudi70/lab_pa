<?php

/**
 * @author Asep
 * @copyright NCI 2015
 */

//class main extends Controller {
class functionApproveAkad extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct(){
		parent::__construct();
      	$this->load->library('session');
      	$this->load->library('common');
    }
	 
 	public function index(){
		$this->load->view('main/index');
   	} 
	
	public function getLookupFakturPiutang(){ 
   		$result=$this->db->query("SELECT x.*FROM (
									SELECT 1 as ID, ARF_Number as nomor, Amount-paid as amount,0 as Paid,0 as remain,arf_date as tanggal
										FROM ACC_AR_FAKTUR 
									WHERE Cust_Code = '".$_POST['kd_customer']."' AND Amount <> Paid AND Posted = 't' 
									UNION 
									SELECT 2 as ID, ARA_Number as nomor, 
										CASE WHEN Type = 1 THEN ((Amount-paid) * -1) ELSE Amount END as Amount,  
										CASE WHEN Type = 1 THEN (Paid * -1) ELSE Paid END  as Paid,
										0 as remain,
										ara_date as tanggal
									FROM ACC_ARAdjust 
									WHERE Cust_Code = '".$_POST['kd_customer']."' AND Amount <> Paid AND Posted = 't'
								) x 
								ORDER BY x.nomor ")->result();
   		// return $result->result();
		/* $arraydata = array();
		$totalbayar = $_POST['pembayaran'];
		for($i=0;$i<count($result);$i++){
			$arraydata[$i]['id'] = $result[$i]->id;
			$arraydata[$i]['nomor'] = $result[$i]->nomor;
			$arraydata[$i]['amount'] = $result[$i]->amount;
			$arraydata[$i]['tanggal'] = $result[$i]->tanggal;
			
			if($totalbayar > $result[$i]->amount){
				$arraydata[$i]['paid'] = $result[$i]->amount;
				$arraydata[$i]['remain'] = $result[$i]->amount - $arraydata[$i]['paid'];
				$totalbayar = (int)$totalbayar - (int)$result[$i]->amount;
				
			} else{
				$arraydata[$i]['paid'] = $totalbayar;
				$arraydata[$i]['remain'] = $result[$i]->amount - $totalbayar;
			}
		} */
		echo "{success:true, listData:".json_encode($result).", total_record:'".count($result)."'}";
   	}
	
	public function getLookupFakturHutang(){ 
   		$result=$this->db->query("SELECT x.* FROM (
									SELECT 1 as ID, APF_Number as nomor, Amount-Paid as amount,0 as Paid,0 as remain,apf_date as tanggal
										FROM ACC_AP_FAKTUR 
									WHERE vend_Code = '".$_POST['kd_vendor']."' AND Amount <> Paid AND Posted = 't' 
									UNION 
									SELECT 2 as ID, APA_Number as nomor, 
										CASE WHEN Type = 1 THEN ((Amount-Paid) * -1) ELSE Amount END as Amount,  
										CASE WHEN Type = 1 THEN (Paid * -1) ELSE Paid END  as bayar,
										0 as remain,
										apa_date as tanggal
									FROM ACC_APAdjust
									WHERE vend_Code = '".$_POST['kd_vendor']."' AND Amount <> Paid AND Posted = 't'
								) x 
								ORDER BY x.nomor ")->result();
		echo "{success:true, listData:".json_encode($result).", total_record:'".count($result)."'}";
   	}
	
	public function getARFINumber(){
		$date = date('Ymd');
		$res = $this->db->query("select arfi_number from acc_arfak_int where left(arfi_number,3)='PAR' and date='".date('Y-m-d')."' order by arfi_number desc limit 1");
		if($res->num_rows == 0){
			$arfinumber = 'PAR/'.$date."/001";
		} else{
			$arfino = substr($res->row()->arfi_number,-3);
			$arfino = (int)$arfino + 1;
			$arfinumber = 'PAR/'.$date."/".str_pad($arfino,3,"0",STR_PAD_LEFT);
		}
		return $arfinumber;
	}
	
	public function getAPFINumber(){
		$date = date('Ymd');
		$res = $this->db->query("select apfi_number from acc_apfak_int where left(apfi_number,3)='PAP' and date='".date('Y-m-d')."' order by apfi_number desc limit 1");
		if($res->num_rows == 0){
			$apfinumber = 'PAP/'.$date."/001";
		} else{
			$apfino = substr($res->row()->apfi_number,-3);
			$apfino = (int)$arfino + 1;
			$apfinumber = 'PAP/'.$date."/".str_pad($apfino,3,"0",STR_PAD_LEFT);
		}
		return $apfinumber;
	}
	
	public function savePiutang(){
    	$this->db->trans_begin();
    	
		# GET DATA FROM JS
		$csar_number 	= $_POST['csar_number'];
		$csar_date	 	= $_POST['csar_date'];	
		
		# PARAM acc_arfak_int
		$acc_arfak_int = array(
			'csar_number' 	=> $csar_number,
			'csar_date'		=> $csar_date,
			'arfi_number'	=> $this->getARFINumber(),
		);
				
		# CEK UPDATE ATAU INSERT
		for($i=0 ; $i<$_POST['jumlah'] ; $i++){
			if($_POST['pilih-'.$i] == true || $_POST['pilih-'.$i] == 'true'){
				$acc_arfak_int['arf_number']	= $_POST['nomor-'.$i];
				$acc_arfak_int['date']			= $_POST['tanggal-'.$i];
				$acc_arfak_int['amount']		= $_POST['amount-'.$i];
				$acc_arfak_int['paid']			= $_POST['paid-'.$i];
				$acc_arfak_int['remain']		= $_POST['remain-'.$i];
				
				$save_arfak_int   = $this->db->insert('acc_arfak_int', $acc_arfak_int);
				if($save_arfak_int){
					# UPDATE JUMLAH PAID FAKTUR
					$getpaid = $this->db->query("select paid from acc_ar_faktur where arf_number='".$_POST['nomor-'.$i]."'")->row()->paid;
					$criteria_paid = array(
						'arf_number' 	=> $_POST['nomor-'.$i]
					);
					$value_paid = array('paid'=>$getpaid + $_POST['paid-'.$i]);		
					$this->db->where($criteria_paid);
					$update_accarfaktur= $this->db->update('acc_ar_faktur',$value_paid);
				} else{
					$this->db->trans_rollback();
					echo "{success:false, pesan:'Gagal simpan posting!'}";
					exit;
				}
			}
		}
		if($update_accarfaktur > 0 ){
			# UPDATE STATUS POSTING
			$criteria = array(
				'csar_number' 	=> $csar_number
			);
			$value = array('posted'=>'t');		
			$this->db->where($criteria);
			$update_acccsar = $this->db->update('acc_csar',$value);
			if($update_acccsar > 0){
				$this->db->trans_commit();
				echo "{success:true, pesan:'Data berhasil disimpan.'}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false, pesan:'Gagal mengubah status posting penerimaan piutang!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false, pesan:'Gagal simpan jumlah pembayaran faktur!'}";
			exit;
		}
	}
	
	public function saveHutang(){
    	$this->db->trans_begin();
    	
		# GET DATA FROM JS
		$csap_number 	= $_POST['csap_number'];
		$csap_date	 	= $_POST['csap_date'];	
		
		# PARAM acc_arfak_int
		$acc_apfak_int = array(
			'csap_number' 	=> $csap_number,
			'csap_date'		=> $csap_date,
			'apfi_number'	=> $this->getAPFINumber(),
		);
				
		# CEK UPDATE ATAU INSERT
		for($i=0 ; $i<$_POST['jumlah'] ; $i++){
			if($_POST['pilih-'.$i] == true || $_POST['pilih-'.$i] == 'true'){
				$acc_apfak_int['apf_number']	= $_POST['nomor-'.$i];
				$acc_apfak_int['date']			= $_POST['tanggal-'.$i];
				$acc_apfak_int['amount']		= $_POST['amount-'.$i];
				$acc_apfak_int['paid']			= $_POST['paid-'.$i];
				$acc_apfak_int['remain']		= $_POST['remain-'.$i];
				
				$save_arfak_int   = $this->db->insert('acc_apfak_int', $acc_apfak_int);
				if($save_arfak_int){
					# UPDATE JUMLAH PAID FAKTUR
					$getpaid = $this->db->query("select paid from acc_ap_faktur where apf_number='".$_POST['nomor-'.$i]."'")->row()->paid;
					$criteria_paid = array(
						'apf_number' 	=> $_POST['nomor-'.$i]
					);
					$value_paid = array('paid'=>$getpaid + $_POST['paid-'.$i]);		
					$this->db->where($criteria_paid);
					$update_accapfaktur= $this->db->update('acc_ap_faktur',$value_paid);
				} else{
					$this->db->trans_rollback();
					echo "{success:false, pesan:'Gagal simpan posting!'}";
					exit;
				}
			}
		}
		if($update_accapfaktur > 0 ){
			# UPDATE STATUS POSTING
			$criteria = array(
				'csap_number' 	=> $csap_number
			);
			$value = array('posted'=>'t');		
			$this->db->where($criteria);
			$update_acccsap = $this->db->update('acc_csap',$value);
			if($update_acccsap > 0){
				$this->db->trans_commit();
				echo "{success:true, pesan:'Data berhasil disimpan.'}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false, pesan:'Gagal mengubah status posting penerimaan piutang!'}";
				exit;
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false, pesan:'Gagal simpan jumlah pembayaran faktur!'}";
			exit;
		}
	}
	
}
?>