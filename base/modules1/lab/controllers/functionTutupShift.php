<?php

/**
 * @author Ali
 * @editor M
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionTutupShift extends  MX_Controller {		

    public $ErrLoginMsg='';
		
    public function __construct()
    {
		parent::__construct();
		$this->load->library('session');
    }
	 
	public function index()
    {
		$this->load->view('main/index');
    } 
	
	public function getCurrentShiftLab(){
		$query=$this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,to_char(bs.lastdate,'YYYY-mm-dd')as lastdate
									FROM bagian_shift bs
									INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									WHERE b.bagian='Laboratorium'")->row();
		$shift=$query->shift;
		
		echo $shift;
	}
	
	function getMaxkdbagian()
	{
		if(isset($_POST['command']))
		{
			$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Laboratorium'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag) {
				$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
		}
	}
	
	function getKdBagian(){
		$kd_bagian=$this->db->query("select * from bagian where bagian='Laboratorium'")->row()->kd_bagian;
		
		return $kd_bagian;
	}
	
	
	function tutupShift(){
		$tanggal = $_POST['tanggal'];
		$shiftKe = $_POST['shiftKe'];
		$shiftSelanjutnya = $_POST['shiftSelanjutnya'];
		$kdBagian = $this->getKdBagian();
		$besok = date('Y-m-d', strtotime(' +1 day'));
		
		
		$data = array("shift"=>$shiftSelanjutnya,"lastdate"=>$tanggal);
		$criteria = array("kd_bagian"=>$kdBagian);
		$this->db->where($criteria);
		$query=$this->db->update('bagian_shift',$data);
		
		//-----------insert to sq1 server Database---------------//
		_QMS_update('bagian_shift',$data,$criteria);
		//-----------akhir insert ke database sql server----------------//
		
		if($query){
			echo '{success:true}';
		}else{
			echo "{success:false}";
		}
	}


}
?>