<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_LABRegisterLaboratorium extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	public function cetak(){
		
		
        $UserID = '0';
		$title='LAP BUKU REGISTER LABORATORIUM';
		$param=json_decode($_POST['data']);
		
        $tglAwal 	= date('Y-m-d', strtotime(str_replace('/', '-', $param->tgl_awal)));
		$tglAkhir 	= date('Y-m-d', strtotime(str_replace('/', '-', $param->tgl_akhir)));
		$asalpasien = $param->asal_pasien;
		$kdCustomer = $param->kd_customer;
		$kelompok 	= $param->kelompok;
		$shift 		= $param->all_shift;
		$shift1 	= $param->shift_1;
		$shift2 	= $param->shift_2;
		$shift3 	= $param->shift_3;
		$date1 = str_replace('/', '-', $tglAwal);
		$date2 = str_replace('/', '-', $tglAkhir);
		$tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
		$tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit= "('44','45')";//$this->db->query("select kd_unit from unit where nama_unit='Laboratorium PA'")->row()->kd_unit;
		$ParamShift="k.kd_unit IN ('41','42','43')";
		$type_file = $param->type_file;
		
		if ($shift == true || ($shift_1 == true && $shift_2 == true && $shift_3 == true)) { //All shift
			$ParamShift .= " AND (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,2,3))
							or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
			$shift = 'Semua Shift';
		} else {
			if($shift_1 == true && $shift_2 == false && $shift_3 == false){
				$ParamShift .= " AND (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1))) ";
				$shift = 'Shift 1';
			} else if($shift_1 == true && $shift_2 == true && $shift_3 == false){
				$ParamShift .= " AND (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,2))) ";
				$shift = 'Shift 1 dan 2';
			} else if($shift_1 == true && $shift_2 == false && $shift_3 == true){
				$ParamShift .= " AND(k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (1,3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 1 dan 3';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == false){
				$ParamShift .= " AND (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (2))) ";
				$shift = 'Shift 2';
			} else if($shift_1 == false && $shift_2 == true && $shift_3 == true){
				$ParamShift .= " AND (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (2,3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 2 dan 3';
			} else if($shift_1 == false && $shift_2 == false && $shift_3 == true){
				$ParamShift .= " AND (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (3))
								or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
				$shift = 'Shift 3';
			}
		}
		
		
		$criteria = " WHERE ".$ParamShift;
		if ($asalpasien == 'Semua' || $asalpasien == 1) {
			$criteriaAsalPasien = "";
		}else if($asalpasien == 'RWJ/IGD' || $asalpasien == 2){
			$criteriaAsalPasien = " AND tr.kd_unit in (".$kd_unit.") And left(u.kd_unit,1) in ('2','3')";
		}else if($asalpasien == 'RWI' || $asalpasien == 3){
			$criteriaAsalPasien = " AND tr.kd_unit in (".$kd_unit.") And left(u.kd_unit,1) in ('1')";
		}else{
			$criteriaAsalPasien = " AND tr.kd_unit in (".$kd_unit.")";
		}

		if (strtolower($kelompok) == 'semua') {
			$criteriaKelompok 	= "";
		}else{
			$criteriaKelompok 	= " AND c.kd_customer = '".$kdCustomer."'";
		}
		
		/*
        if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") " . $ParamShift . "";
            } else {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") " . $ParamShift . " And c.kd_customer = '$kdCustomer'";
            }
        } else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift . "";
            } else {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift . " And c.kd_customer = '$kdCustomer'";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") And left(u.kd_unit,1)='1'
                                            " . $ParamShift . "";
            } else {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") And left(u.kd_unit,1)='1'
                                            " . $ParamShift . "  And c.kd_customer = '$kdCustomer'";
            }
        } else {
            if ($kdCustomer == 'Semua') {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift . " ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit in (".$kd_unit.") And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift . " And c.kd_customer = '$kdCustomer'";
            }
        }*/

        //echo $kriteria;
        $queryHead = $this->db->query("SELECT k.tgl_masuk,u.kd_unit, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													inner join unit u on tr2.kd_unit = u.kd_unit
											" . $criteria . "
											" . $criteriaAsalPasien . "
											" . $criteriaKelompok . "
											group by k.tgl_masuk,u.kd_unit,p.kd_pasien, k.no_sjp,u.nama_unit, c.customer
											order by k.tgl_masuk desc, nama asc");

		
        $query = $queryHead->result();
		
		/* $queryHasil = $this->db->query(" 
											select k.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . "
											order by k.tgl_masuk desc, nama asc,  Deskripsi
										  ");

		
        $query2 = $queryHasil->result(); */
		$html = "";
		//-------------JUDUL-----------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
					<tr>
						<th colspan="12">'.$title.'</th>
					</tr>
					<tr>
						<th colspan="12">'.$awal.' - '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="12">Pasien '.$asalpasien.'</th>
					</tr>
					<tr>
						<th colspan="12">'.$shift.'</th>
					</tr>
			</table> <br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		 $html.="
				<table border='1'>
					<thead>
					<tr>
						<th width='10'>No</th>
						<th width='70' align='center'>Tanggal Masuk</th>
						<th width='100' align='center'>Nama</th>
						<th width='60' align='center'>Umur</th>
						<th width='60' align='center'>Pemeriksaan</th>
						<th width='100' align='center'>Alamat</th>
						<th width='80' align='center'>No Medrec</th>
						<th width='70' align='center'>No Asuransi</th>
						<th width='70' align='center'>No SJP</th>
						<th width='70' align='center'>No Reg</th>
						<th width='70' align='center'>Unit</th>
						<th width='100' align='center'>Jenis Customer</th>
					</tr>
					</thead>
					<tbody>
					"; 
				
		if (count($query) == 0) {
             $html.='
					<tr> 
						<td align="center" colspan="12">Data tidak ada</td>
					</tr>';
			// $jmllinearea=count($query)+5;
        } else {
			$no=0;
			// $jmllinearea=count($query);
            foreach ($query as $line) {
				$tmpumur     = "";
            	$tmpumur = $line->umur;
            	$tmpumur = str_replace("years", "th", $tmpumur);
            	$tmpumur = str_replace("mons", "bln", $tmpumur);
            	$tmpumur = str_replace("days", "hr", $tmpumur);
                /*$Split1 = explode(" ", $line->umur, 6);
                //print_r ($usia);
                if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
                    $tmpumur = $tmp1 . 'th';
                } else if (count($Split1) == 4) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mon') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                } else {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                }*/
				//"2015-05-13 00:00:00"
				$tmptglmasuk = substr($line->tgl_masuk, 0, 10);
				$no++;
				$nama        = $line->nama;
				$alamat      = $line->alamat;
				$kdpasien    = $line->kd_pasien;
				$noasuransi  = $line->no_asuransi;
				$nosjp       = $line->no_sjp;
				$noreg       = $line->noreg;
				$kodeunit    = $line->kd_unit;
				$namaunit    = $line->nama_unit;
				$customer    = $line->customer;
				$queryHasil  = $this->db->query("SELECT k.tgl_masuk, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as NoReg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $criteria . "
											" . $criteriaAsalPasien . "
											" . $criteriaKelompok . "
											and p.kd_pasien='".$kdpasien."'
											and u.kd_unit = '".$kodeunit."'
											order by k.tgl_masuk desc, nama asc,  Deskripsi
										  ");

		
				$query2 = $queryHasil->result();
				/*if($type_file == 1){
					$deskripsi ='';
					foreach ($query2 as $line2) {
						$deskripsi.='<tr><td>'.$line2->deskripsi.'</td></tr>';
						//$deskripsi .= $line2->deskripsi."<br/>";
						
					}
					$deskripsi.='';
					$rowspan=count($query2)+1;
					 $html.='
							<tr> 
								<td align="center" rowspan="'.$rowspan.'">' . $no . '</td>
								<td align="center" rowspan="'.$rowspan.'">' . $tmptglmasuk . '</td>
								<td rowspan="'.$rowspan.'">' . $nama . '</td>
								<td align="center" rowspan="'.$rowspan.'">' . $tmpumur . '</td>
								<td>&nbsp;</td>
								<td rowspan="'.$rowspan.'">' . $alamat . '</td>
								<td rowspan="'.$rowspan.'">' . $kdpasien . '</td>
								<td rowspan="'.$rowspan.'">' . $noasuransi . '</td>
								<td rowspan="'.$rowspan.'">' . $nosjp . '</td>
								<td rowspan="'.$rowspan.'">' . $noreg . '</td>
								<td rowspan="'.$rowspan.'">' . $namaunit . '</td>
								<td rowspan="'.$rowspan.'">' . $customer . '</td>
							</tr>'.$deskripsi; 
				}else{*/
					$deskripsi ="";
					foreach ($query2 as $line2) {
						//$deskripsi.='<tr><td>'.$line2->deskripsi.'</td></tr>';
						$deskripsi .= $line2->deskripsi."<br/>";
						
					}
					/*
					$html.='
							<tr> 
								<td align="center">' . $no . '</td>
								<td align="center">' . $tmptglmasuk . '</td>
								<td>' . $nama . '</td>
								<td align="center"></td>
								<td>' . $deskripsi . '</td>
								<td>' . $alamat . '</td>
								<td>' . $kdpasien . '</td>
								<td>' . $noasuransi . '</td>
								<td>' . $nosjp . '</td>
								<td>' . $noreg . '</td>
								<td>' . $namaunit . '</td>
								<td>' . $customer . '</td>
							</tr>';*/
					$html .= "<tr>";
					$html .= "<td valign='top' align='center' style='padding-left:10px;'>".$no."</td>";
					$html .= "<td valign='top' align='center' style='padding-left:10px;'>".$tmptglmasuk."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$nama."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$tmpumur."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$deskripsi."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$alamat."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$kdpasien."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$noasuransi."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$nosjp."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$noreg."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$namaunit."</td>";
					$html .= "<td valign='top' align='left' style='padding-left:10px;'>".$customer."</td>";
					$html .= "</tr>";
				// }
				// $jmllinearea += 1;
				
            }
			// $jmllinearea = $jmllinearea+1;
        }
		// $jmllinearea = $jmllinearea+1;
		$html.="</tbody></table>";
		// echo $html;
		if($type_file == true){
			$name = "Laporan Registrasi Lab Klinik_".date('d/M/Y').".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;
		}else{
			$common = $this->common;
			$this->common->setPdf('L','Laporan Registrasi Lab Klinik',$html);   
		}
	}
	public function cetak1() {
		$common=$this->common;
        $result=$this->result;
        $title='LAPORAN REGISTRASI LAB';
        $UserID = '0';
        $param=json_decode($_POST['data']);
    	$html='';
        $Split = explode("##@@##", $param, 13);
        //print_r ($Split);
        /*
          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Semua
          [5] => NULL
          [6] => NULL
          [7] => shift1
          [8] => 1
          [9] => shift2
          [10] => 2
          [11] => shift3
          [12] => 3

          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Perseorangan
          [5] => Perorangan
          [6] => 0000000001
          [7] => shift1
          [8] => 1

          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Perseorangan
          [5] => Perorangan
          [6] => 0000000001
          [7] => shift1
          [8] => 1
          [9] => shift2
          [10] => 2

          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => Pasien
          [3] => Semua
          [4] => Perseorangan
          [5] => Perorangan
          [6] => 0000000001
          [7] => shift1
          [8] => 1
          [9] => shift2
          [10] => 2
          [11] => shift3
          [12] => 3


          [0] => 10/Jul/2015
          [1] => 13/Jul/2015
          [2] => RWJ/IGD
          [3] => Perseorangan
          [4] => Perorangan
          [5] => 0000000001
          [6] => shift1
          [7] => 1
         */
        if (count($Split) > 0) {
            $tglAwal = $Split[0];
            $tglAkhir = $Split[1];
            $asalpasien = $Split[3];
            $kelPasien = $Split[4];
            $kdCustomer = $Split[6];
            $date1 = str_replace('/', '-', $tglAwal);
            $date2 = str_replace('/', '-', $tglAkhir);
            $tomorrow = date('d-M-Y', strtotime($date1 . "+1 days"));
            $tomorrow2 = date('d-M-Y', strtotime($date2 . "+1 days"));
            $ParamShift2 = "";
            $ParamShift3 = "";
			$kduser = $this->session->userdata['user_id']['id'];
			$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
			$key="'4";
			$kd_unit='';
			if (strpos($carikd_unit,","))
			{
				
				$pisah_kata=explode(",",$carikd_unit);
				for($i=0;$i<count($pisah_kata);$i++)
				{
					$cek_kata=stristr($pisah_kata[$i],$key);
					if ($cek_kata != '' || $cek_kata != null)
					{
						$kd_unit=$cek_kata;
					}
				}
				
			}else
			{
				$kd_unit= $carikd_unit;
			}

            if (count($Split) === 9) { //1 shif yang di pilih
                if ($Split[8] == 3) {//shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . ")) ";
                    $shift = $Split[7];
                }
            } else if (count($Split) == 11) { //2 shif yang di pilih
                if ($Split[8] == 3 or $Split[10] === 3) {
                    //shif 3, shif 4
                    $ParamShift2 = "And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "))";
                    $ParamShift3 = "or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4))";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                } else {
                    $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . ")) ";
                    $shift = $Split[7] . ' Dan ' . $Split[9];
                }
            } else {
                $ParamShift2 = " And (k.tgl_masuk >= '" . $tglAwal . "' and k.tgl_masuk <= '" . $tglAkhir . "' And (k.Shift In (" . $Split[8] . "," . $Split[10] . "," . $Split[12] . "))";
                $ParamShift3 = " or (k.tgl_masuk >= '" . $tomorrow . "' and k.tgl_masuk <= '" . $tomorrow2 . "' And k.Shift = 4)";
                $shift = 'Semua Shift';
            }
        }
        if ($asalpasien == 'Semua') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' " . $ParamShift2 . " " . $ParamShift3 . ") ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' " . $ParamShift2 . " " . $ParamShift3 . " And c.kd_customer = '$kdCustomer')";
            }
        } else if ($asalpasien == 'RWJ/IGD') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' And left(u.kd_unit,1) in ('2','3')
                                            " . $ParamShift2 . " " . $ParamShift3 . ") And c.kd_customer = '$kdCustomer'";
            }
        } else if ($asalpasien == 'RWI') {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '41' And left(u.kd_unit,1)='1'
                                            " . $ParamShift2 . " " . $ParamShift3 . ")  And c.kd_customer = '$kdCustomer'";
            }
        } else {
            if ($kdCustomer == 'NULL') {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift2 . " " . $ParamShift3 . ") ORDER BY nama, Deskripsi";
            } else {
                $kriteria = " WHERE tr.kd_unit = '$kd_unit' And left (p.kd_pasien,2)='LB'
                                            " . $ParamShift2 . " " . $ParamShift3 . ") And c.kd_customer = '$kdCustomer'";
            }
        }

        //echo $kriteria;
        $queryHasil = $this->db->query(" 
											select k.tgl_masuk,u.kd_unit, upper(p.nama) as nama,p.tgl_lahir, age(p.tgl_lahir) as Umur, pr.deskripsi, upper(p.alamat) as alamat, p.kd_pasien, p.no_asuransi, k.no_sjp, '' as no_reg, u.nama_unit, c.customer
											from kunjungan k 
													INNER JOIN pasien p on p.kd_pasien = k.kd_pasien
													INNER JOIN Customer c on k.kd_customer = c.kd_customer
													inner join transaksi tr on k.kd_pasien = tr.kd_pasien and k.kd_unit = tr.kd_unit and k.tgl_masuk = tr.tgl_transaksi and k.urut_masuk = tr.urut_masuk
													inner join detail_transaksi dt on dt.kd_kasir = tr.kd_kasir and dt.no_transaksi= tr.no_transaksi
													inner join produk pr on dt.kd_produk = pr.kd_produk
													left join unit_asal ua on ua.kd_kasir = tr.kd_kasir and ua.no_transaksi = tr.no_transaksi
													left join transaksi tr2 on tr2.kd_kasir = ua.kd_kasir_asal and tr2.no_transaksi = ua.no_transaksi_asal
													left join unit u on tr2.kd_unit = u.kd_unit
											" . $kriteria . "
											group by k.tgl_masuk,u.kd_unit,p.kd_pasien, k.no_sjp,u.nama_unit, c.customer
											order by k.tgl_masuk desc, nama asc
										  ");


        $query = $queryHasil->result();
        if (count($query) == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $queryRS = $this->db->query("select * from db_rs")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }

            $no = 0;
            $type_file        =0;
            /*if($type_file == 1){
            $html.="<style>
                         .t1 {
                                 border: 1px solid black;
                                 border-collapse: collapse;
                                 font-family: Arial, Helvetica, sans-serif;
                         }
                 .t1 tr th
                 {
                   font-weight:bold;
                   font-size:12px;
                 }
                  .t1 tr td
                 {
                   font-size:12px;
                 }
                         .formarial {
                                 font-family: Arial, Helvetica, sans-serif;
                         }
                        
                 .t2 {
                                 font-family: Arial, Helvetica, sans-serif;
                         }
                
                .t2 tr th
                 {
                   font-weight:bold;
                   font-size:14px;
                 }
                         </style>";
          }else{
            $html.="<style>
                         .t1 {
                                 border: 1px solid black;
                                 border-collapse: collapse;
                                 font-family: Arial, Helvetica, sans-serif;
                         }
                 .t1 tr th
                 {
                   font-weight:bold;
                   font-size:12px;
                 }
                  .t1 tr td
                 {
                   font-size:12px;
                 }
                         .formarial {
                                 font-family: Arial, Helvetica, sans-serif;
                         }
                        
                 .t2 {
                                 font-family: Arial, Helvetica, sans-serif;
                         }
                
                .t2 tr th
                 {
                   font-weight:bold;
                   font-size:12px;
                 }
                         </style>";
            
          }*/
            /*$this->load->library('m_pdf');
            $this->m_pdf->load();*/
            //-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
            /*$mpdf = new mPDF('utf-8', array(297, 210));

            $mpdf->SetDisplayMode('fullpage');*/

            //-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
            /*$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (0) Admin',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'C' => array(
                        'content' => "Tgl/Jam : " . $date . "",
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );*/


            /*$mpdf->SetFooter($arr);
            $mpdf->SetTitle('LAP BUKU REGISTER LABORATORIUM');
            $mpdf->WriteHTML("
												<style>
												.t1 {
														border: 1px solid black;
														border-collapse: collapse;
														font-size: 35;
														font-family: Arial, Helvetica, sans-serif;
												}
												.formarial {
														font-family: Arial, Helvetica, sans-serif;
												}
												h1 {
														font-size: 12px;
												}

												h2 {
														font-size: 10px;
												}

												h3 {
														font-size: 8px;
												}

												table2 {
														border: 1px solid white;
												}
												.one {border-style: dotted solid dashed double;}
												</style>
                            ");*/
            //-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $html='
                <table class="t2" cellspacing="0" border="0">
                  <tbody>
                    <tr>
                      <th colspan="8">'.$title.'<br>
                    </tr>
                    <tr>
                      <th colspan="8"> Periode '.$tglAwal.' s/d '.$tglAkhir.'</th>
                    </tr>
                    <tr>
                      <th colspan="8"> Pasien ' . $asalpasien . '</th>
                    </tr>
                    <tr>
                      <th colspan="8"> '.$shift.'</th>
                    </tr>
                  </tbody>
                </table><br>';
                
               /* $mpdf->WriteHTML("
												<table width='1000' cellspacing='0' border='0'>
													 <tr>
														 <td width='76'>
														 <img src='./ui/images/Logo/LOGORSBA.png' width='76' height='74' />
														 </td>
														 <td>
														 <b><font style='font-size: 12px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
														 <font style='font-size: 9px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
														 <font style='font-size: 9px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
														 <font style='font-size: 9px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
														 </td>
													 </tr>
												 </table>
								");*/
            }

            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            /*$mpdf->WriteHTML("<h1 class='formarial' align='center'>BUKU REGISTER LABORATORIUM</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $tglAwal . " - " . $tglAkhir . "</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>Pasien " . $asalpasien . "</h2>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $shift . "</h3>");*/
            //-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
            $html.='
                    <table class="t2" border = "1">
                    <thead>
                      <tr>
                            <th >No</td>
                            <th  align="center">Tanggal Masuk</td>
                            <th  align="center">Nama</td>
                            <th  align="center">Umur</td>
                            <th  align="center">Pemeriksaan</td>
                            <th  align="center">Alamat</td>
                            <th  align="center">No Medrec</td>
                            <th  align="center">Unit</td>
                            <th  align="center">Jenis Customer</td>
                      </tr>
                    </thead>

                            ';
            foreach ($query as $line) {
                $Split1 = explode(" ", $line->umur, 6);
                //print_r ($usia);
                if (count($Split1) == 6) {
                    $tmp1 = $Split1[0];
                    $tmpumur = $tmp1 . 'th';
                } else if (count($Split1) == 4) {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    $tmp3 = $Split1[2];
                    $tmp4 = $Split1[3];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mon') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                } else {
                    $tmp1 = $Split1[0];
                    $tmp2 = $Split1[1];
                    if ($tmp2 == 'years') {
                        $tmpumur = $tmp1 . 'th';
                    } else if ($tmp2 == 'mons') {
                        $tmpumur = $tmp1 . 'bl';
                    } else if ($tmp2 == 'days') {
                        $tmpumur = $tmp1 . 'hr';
                    }
                }
                //"2015-05-13 00:00:00"
                $tmptglmasuk = substr($line->tgl_masuk, 0, 10);
                $no++;
                $nama = $line->nama;
                $deskripsi = $line->deskripsi;
                $alamat = $line->alamat;
                $kdpasien = $line->kd_pasien;
                $noasuransi = $line->no_asuransi;
                $nosjp = $line->no_sjp;
                $noreg = $line->no_reg;
                $namaunit = $line->nama_unit;
                $customer = $line->customer;

                $html.='

                                    <tbody>

                                            <tr class="headerrow"> 
                                                    <td align="center">' . $no . '</td>
                                                    <td align="center">' . $tmptglmasuk . '</td>
                                                    <td >' . $nama . '</td>
                                                    <td  align="center">' . $tmpumur . '</td>
                                                    <td >' . $deskripsi . '</td>
                                                    <td >' . $alamat . '</td>
                                                    <td >' . $kdpasien . '</td>
                                                    <td >' . $namaunit . '</td>
                                                    <td >' . $customer . '</td>
                                            </tr>

                                    <p>&nbsp;</p>

                                    ';
            }
            $html.='</tbody></table>';
            $prop=array('foot'=>true);
            
            /*if($type_file == 1){
              $name=' Lap_Registrasi.xls';
              header("Content-Type: application/vnd.ms-excel");
              header("Expires: 0");
              header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
              header("Content-disposition: attschment; filename=".$name);
            }else{*/
              $this->common->setPdf('L','Lap.Registrasi Lab',$html); 
            //}
            /*echo $html;
            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/Lab/';
            $tmpname = time() . 'LAB';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "", title : "Laporan Hasil Lab", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';*/
        }
        echo $res;
    }
	
}
?>