<?php
class tblviewgridhasillab extends TblBase
{
    function __construct()
    {
        $this->TblName='tblviewgridhasillab';
        TblBase::TblBase(true);
		$this->StrSql="klasifikasi, deskripsi, kd_lab, kd_test, item_test, satuan,ket_hasil, normal, kd_metode, hasil, ket, kd_unit_asal,nama_unit_asal, urut, metode,judul_item";
		
        $this->SqlQuery= "select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
								case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
							From LAB_hasil 
							inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
							inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
							on LAB_Test.kd_Test = produk.Kd_Produk
							inner join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
							inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewgridhasillab;
		
		$row->KLASIFIKASI = $rec->klasifikasi;
		$row->DESKRIPSI = $rec->deskripsi;
		$row->KD_LAB = $rec->kd_lab;
		$row->KD_TEST = $rec->kd_test;
		$row->ITEM_TEST = $rec->item_test;
		$row->SATUAN = $rec->satuan;
		$row->NORMAL = $rec->normal;
		/* $row->NORMAL_W = $rec->normal_w;
		$row->NORMAL_A = $rec->normal_a;
		$row->NORMAL_B = $rec->normal_b;
		$row->COUNTABLE = $rec->countable;
		$row->MAX_M = $rec->max_m;
		$row->MIN_M = $rec->min_m;
		$row->MAX_F = $rec->max_f;
		$row->MIN_F = $rec->min_f;
		$row->MAX_A = $rec->max_a;
		$row->MIN_A = $rec->min_a;
		$row->MAX_B = $rec->max_b;
		$row->MIN_B = $rec->min_b; */
		$row->KD_METODE = $rec->kd_metode;
		$row->JUDUL_ITEM = $rec->judul_item;
		$row->KD_UNIT_ASAL = $rec->kd_unit_asal;
		$row->NAMA_UNIT_ASAL = $rec->nama_unit_asal;
		$row->URUT = $rec->urut;
		$row->KET_HASIL = $rec->ket_hasil;
		
		if($rec->hasil=='null'){
			$row->HASIL = '';
		}else{
			$row->HASIL = $rec->hasil;
		}
		$row->KET = $rec->ket;
		if($rec->ket=='null'||$rec->ket=='undefined'){
			$row->KET = '';
		}else{
			$row->KET = $rec->ket;
		}
		if($rec->satuan == ' ' && $rec->normal == ' '){
			$row->METODE = '';
		} else{
			$row->METODE = $rec->metode;
		}
		
        return $row;
    }

}

class Rowtblviewgridhasillab
{
		public $KLASIFIKASI;
		public $DESKRIPSI;
		public $KD_LAB;
		public $KD_TEST;
		public $ITEM_TEST;
		public $SATUAN;
		public $NORMAL;
		/* public $NORMAL_W;
		public $NORMAL_A;
		public $NORMAL_B;
		public $COUNTABLE;
		public $MAX_M;
		public $MIN_M;
		public $MAX_F;
		public $MIN_F;
		public $MAX_A;
		public $MIN_A;
		public $MAX_B;
		public $MIN_B; */
		public $KD_METODE;
		public $HASIL;
		public $KET;
		public $KD_UNIT_ASAL;
		public $NAMA_UNIT_ASAL;
		public $URUT;
		public $METODE;
		public $JUDUL_ITEM;
		public $KET_HASIL;
}

?>
