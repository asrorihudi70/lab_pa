﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewrwjpenjaslab extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_pasien, no_transaksi, nama, alamat, nama_unit, masuk, tgl_lahir, 
						jenis_kelamin, gol_darah, kd_customer, customer, dokter, kd_dokter, kd_unit, kd_kasir, co_status, kd_user, tgl_transaksi";
		$this->SqlQuery="SELECT pasien.kd_pasien, tr.no_transaksi, pasien.NAMA, pasien.Alamat, u.Nama_unit as nama_unit, 
                                kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, customer.customer,
                                dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, tr.kd_Kasir,tr.tgl_transaksi, tr.posting_transaksi,
                                tr.co_status, tr.kd_user
                                FROM pasien 
                                INNER JOIN (( kunjungan  
                                inner join ( transaksi tr inner join unit u on u.kd_unit=tr.kd_unit)  
                                on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi 
                                inner join customer on customer.kd_customer = kunjungan.kd_customer)   
                                INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter)ON kunjungan.kd_pasien=pasien.kd_pasien";
		$this->TblName='';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
                $row->KD_PASIEN=$rec->kd_pasien;
                $row->NO_TRANSAKSI=$rec->no_transaksi;
                $row->NAMA=$rec->nama;
                $row->ALAMAT=$rec->alamat;
                $row->NAMA_UNIT=$rec-> nama_unit;
                $row->MASUK=$rec->masuk;
                $row->TGL_LAHIR=$rec-> tgl_lahir;
                $row->JENIS_KELAMIN=$rec->jenis_kelamin;
                $row->GOL_DARAH=$rec->gol_darah;
                $row->KD_CUSTOMER=$rec->kd_customer;
				$row->CUSTOMER=$rec->customer;
                $row->DOKTER=$rec->dokter;
                $row->KD_DOKTER=$rec->kd_dokter;
                $row->KD_UNIT=$rec->kd_unit;
                $row->KD_KASIR=$rec->kd_kasir;
                $row->CO_STATUS=$rec->co_status;
                $row->KD_USER=$rec->kd_user;
				$row->POSTING_TRANSAKSI=$rec->posting_transaksi;
				$row->TGL_TRANSAKSI=$rec->tgl_transaksi;
		
		return $row;
	}
}
class Rowdokter
{
        public $KD_PASIEN;
        public $NO_TRANSAKSI;
        public $NAMA;
        public $ALAMAT;
        public $NAMA_UNIT;
        public $MASUK;
        public $TGL_LAHIR;
        public $JENIS_KELAMIN;
        public $GOL_DARAH;
        public $KD_CUSTOMER;
		public $CUSTOMER;
        public $DOKTER;
        public $KD_DOKTER;
        public $KD_UNIT;
        public $KD_KASIR;
        public $CO_STATUS;
        public $KD_USER;
		public $POSTING_TRANSAKSI;
		public $TGL_TRANSAKSI;
}

?>