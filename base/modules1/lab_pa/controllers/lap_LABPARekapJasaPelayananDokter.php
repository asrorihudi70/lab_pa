<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_LABPARekapJasaPelayananDokter extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN REKAP JASA PELAYANAN DOKTER';
		$param=json_decode($_POST['data']);
		
		$type_file				=$param->type_file;
		$kd_poli    			=$param->kd_poli;
		//$pelayananPendaftaran   =$param->pelayananPendaftaran;
		//$pelayananTindak    	=1;//$param->pelayananTindak;
		$shift       			= $param->shift;
		$shift1         		= $param->shift1;
		$shift2         		= $param->shift2;
		$shift3         		= $param->shift3;
		$kd_dokter    			=$param->kd_dokter;
		$tglAwal   				=$param->tglAwal;
		$tglAkhir  				=$param->tglAkhir;
		//$kd_customer  			=$param->kd_kelompok;
		$kd_profesi  			=$param->kd_profesi;
		$html					='';
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kata_kd_unit='';
		if (strpos($carikd_unit,","))
		{
			$pisah_kata=explode(",",$carikd_unit);
			$qu='';
			for($i=0;$i<count($pisah_kata);$i++)
			{
				
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$qu.=$cek_kata.',';
				}
			}
			$kd_unit=substr($qu,0,-1);
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		/* if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){ */
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit in (".$kd_unit."))";
		/* } else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		} */
		$asal_pasien = $param->pasien;
		$cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."')   and t.kd_pasien not like 'LB%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$nama_asal_pasien = 'Langsung';
		}
		/* $asal_pasien = $param->pasien;
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('25','26','27')";
			$nama_asal_pasien = 'Asal Pasien (Semua)';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('25')";
			$nama_asal_pasien = 'Asal Pasien (RWJ)';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('25')";
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('26')";
			$nama_asal_pasien = 'Asal Pasien (RWI)';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('26')";
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('27')";
			$nama_asal_pasien = 'Asal Pasien (IGD)';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('27')";
		}else if($asal_pasien == 4){
			$crtiteriaAsalPasien="and t.kd_kasir in('25')";
			$nama_asal_pasien = 'Langsung';
			$crtiteriaAsalPasienDT="and xdt.kd_kasir in ('25')";
		} */
		if($kd_profesi == 1 ){
			$criteria_profesi="";
			$profesi="Dokter";			
		} else {
			$criteria_profesi="";			
			$profesi="Perawat";			
		}

		/* if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer_far="";
			$customerfar='SEMUA KELOMPOK CUSTOMER';
		} else{
			$ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
			$customerfar=""; 
			$customerfar.="KELOMPOK CUSTOMER " ; 
			$customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		} */
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$ckd_customer_far=" And k.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$ckd_customer_far="";
		}
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter_far="";
			$dokterfar='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter_far=" AND d.kd_dokter='".$kd_dokter."'";
			$dokterfar=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}

		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)!='4'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit in ('".$kd_poli."')";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit in ('".$kd_poli."')")->row()->nama_unit); 
		}
		if($shift == 'All'){
			$criteriaShift="and (dt.Shift In ( 1,2,3)) 
							   Or  (dt.Shift= 4)";
			
		} else{
			if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1))";
			} else if($shift1 == 'true' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 1,2))";
			
			} else if($shift1 == 'true' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 1,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'false'){
				$criteriaShift="and (dt.Shift In ( 2))";
				
			} else if($shift1 == 'false' && $shift2 == 'true' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In ( 2,3))";
				
			} else if($shift1 == 'false' && $shift2 == 'false' && $shift3 == 'true'){
				$criteriaShift="and (dt.Shift In (3))";
			} 
		}
		$queryHead = $this->db->query(
			"
			 Select distinct(d.Nama) as nama_dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
										Where 
											t.lunas='t'	".$crtiteriaAsalPasien." 	
											and u.kd_unit in(".$kd_unit.")
											And (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$criteriaShift."	
											".$ckd_dokter_far." ".$ckd_customer_far." ".$criteria." ".$ckd_unit_far." ".$criteria_profesi."	
																						
										Group By d.Nama, dtd.kd_Dokter 
										Order By nama_dokter, dtd.kd_Dokter 
                                        "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="11">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="11"> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="11"> Laporan Pelayanan '.$dokterfar.' dari '.$customerfar.' dan '.$unitfar.'</th>
					</tr>
					
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$opstind=$this->db->query("select setting from sys_setting where key_data='opstindakan'")->row()->setting;
		$pphnya=$this->db->query("select setting from sys_setting where key_data='pphdokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
		
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="13%" align="center">Dokter</th>
					<th width="13%" align="center">Jasa Konsul</th>
					<th width="13%" align="center">PPH ('.$pphnya.'%)</th>
					<th width="13%" align="center">Jml Konsul Net</th>
					<th width="13%" align="center">Jasa Tindakan</th>
					<th width="13%" align="center">OPS ('.$opstind.'%)</th>
					<th width="13%" align="center">PPH ('.$pphnya.'%)</th>
					<th width="13%" align="center">Jml Tindakan Net</th>
					<th width="13%" align="center">Penerimaan</th>
				  </tr>
			';
			//echo count($query);
			$html.="";
		if(count($queryHead->result()) > 0) {
		$jmllinearea=count($query);
			$no=0;

			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pph_tindakan = 0;
			$all_total_tindakan_net = 0;
			$all_total_tindakan = 0;
			$all_total_pph = 0;
			$all_total_all = 0;
			foreach($query as $line){
				$no++;
				$queryBody = $this->db->query( 
					"SELECT
					 x.kd_pasien,
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						k.kd_pasien,
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(case when xdt.KD_COMPONENT in ('20') THEN max(xdt.Tarif ) else 0 end) as JPDok, 
						(CASE WHEN xdt.kd_component in ('38') THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									tc.kd_component, 
									Sum(Qty) as Qty, 
									max(tc.tarif) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut 
								And dtd.Tgl_Transaksi=dt.Tgl_Transaksi
								INNER JOIN detail_component tc ON dt.no_transaksi = tc.no_transaksi
										AND dt.kd_kasir = tc.kd_kasir
										AND dt.tgl_transaksi = tc.tgl_transaksi
										AND dt.urut = tc.urut
								INNER JOIN detail_tr_bayar dtb ON dt.Kd_Kasir = dtb.Kd_kasir
														AND dt.No_Transaksi = dtb.no_Transaksi
														AND dt.Tgl_Transaksi = dtb.tgl_Transaksi
														AND dt.Urut = dtb.Urut
								Where 
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." ".$criteriaShift." 										
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, tc.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						where d.kd_dokter='".$line->kd_dokter."' ".$crtiteriaAsalPasien."  AND t.kd_unit in(".$kd_unit.") ".$criteria_profesi."
						".$ckd_customer_far."
						Group By k.kd_pasien,Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama,x.kd_pasien, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);
				/* $queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(case when xdt.KD_COMPONENT in ('20') THEN max(xdt.Tarif ) else 0 end) as JPDok, 
						(CASE WHEN xdt.kd_component in ('38') THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut 
								And dtd.Tgl_Transaksi=dt.Tgl_Transaksi
								INNER JOIN detail_tr_bayar dtb ON dt.Kd_Kasir = dtb.Kd_kasir
								AND dt.No_Transaksi = dtb.no_Transaksi
								AND dt.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dt.Urut = dtb.Urut
								INNER JOIN detail_tr_bayar_component dtbc ON dtbc.Kd_Kasir = dtb.Kd_kasir
								AND dtbc.No_Transaksi = dtb.no_Transaksi
								AND dtbc.Tgl_Transaksi = dtb.tgl_Transaksi
								AND dtbc.Urut = dtb.Urut
								Where 
								(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." ".$criteriaShift." 										
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						where d.kd_dokter='".$line->kd_dokter."' ".$crtiteriaAsalPasien."  AND t.kd_unit in(".$kd_unit.") ".$criteria_profesi."
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				); */
										
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pph = 0;
				$total_all = 0;
				$total_tindakan = 0;
				$total_pph_tindakan = 0;
				$total_tindakan_net = 0;

				foreach ($query2 as $line2) 
				{
				
					$noo++;
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jpdok;
					$total_jumlah += $line2->jpdok*$line2->jml_pasien;
					$total_pph += ((($line2->jpdok*$line2->jml_pasien)/100)*$opstind);
					$total_tindakan += $line2->total_as-((($line2->jpdok*$line2->jml_pasien)/100)*$opstind);
					$total_pph_tindakan += ($line2->jpdok*$line2->jml_pasien) * $pphnya;
					$total_tindakan_net += ($line2->jpdok*$line2->jml_pasien)-(($line2->jpdok*$line2->jml_pasien)*$pphnya);
					/* $total_pph += (($line2->jumlah_as)*$opstind);
					$total_tindakan += $line2->total_as-(($line2->jumlah_as)*$opstind);
					$total_pph_tindakan += (( ($line2->total_as-(($line2->jumlah_as)*$opstind)) )*$pphnya);
					$total_tindakan_net += ( $line2->total_as-(($line2->jumlah_as)*$opstind))-(( ($line2->total_as-(($line2->jumlah_as)*$opstind)) )*$pphnya); */
					/* $html.="<td style='padding-left:10px;'>".$noo." - ".$line2->deskripsi."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jumlah_as)."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah((($line2->jumlah_as/100)*$opstind))."</td>";
					$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->total_as-(($line2->jumlah_as/100)*$opstind))."</td>"; */
				
				}

				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty += $total_qty;
				$all_total_jpdok += $total_jpdok;
				$all_total_jumlah += $total_jumlah;
				$all_total_pph += $total_pph;
				$all_total_pph_tindakan += $total_jumlah*$pphnya;
				$all_total_tindakan_net += $total_tindakan_net;
				$all_total_tindakan += $total_tindakan;

				$html.="<tr>";
				$html.='<td style="padding-left:10px;">'.$no.'</td>';
				$html.='<td align="left" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>';
				//$html.="<td style='padding-right:10px;' align='right'><b>".$total_jml_pasien."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_jumlah)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph)."</b></td>";
				//$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_pph_tindakan)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($total_tindakan_net)."</b></td>";
				$html.="</tr>";
				$jmllinearea += 1;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pph = 0;
				$total_tindakan = 0;
				$total_pph_tindakan = 0;
				$total_tindakan_net =0;
			}
			$html.="<tr>";
			$html.="<td></td>";
			$html.="<td align='right' style='padding-right:10px;'><b>TOTAL</b></td>";
			//$html.="<td style='padding-right:10px;' align='right'><b>".$all_total_jml_pasien."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>0</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_jumlah)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pph)."</b></td>";
			//$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_pph_tindakan)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan_net)."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah($all_total_tindakan_net)."</b></td>";
			$html.="</tr>";
			$jmllinearea = $jmllinearea+1;
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="11" align="center">Data tidak ada</th>
				</tr>

			';	
			$jmllinearea=count($query)+5;			
		} 
		$jmllinearea = $jmllinearea+1;
		$html.='</table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			
			$name='lap_pasien_detail.xls';
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:K'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('C1:J100')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C7:K7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle('C:D')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setWrapText(true);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setWrapText(true);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setWrapText(true); */
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'Z'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('rekap_jasa_dokter'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=lap_rekap_jasa_dokter_pa.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		
		}else{
			$this->common->setPdf('L','Lap.Rekap Jasa Pelayanan Dokter',$html);	
		}
		echo $html;
   	}
	
}
?>