<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_LABPenerimaanTunaiPerKomponenSummary extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	function cetak(){
		$common=$this->common;
   		$result=$this->result;
   		$title='Tunai Perkomponen Summary ';
		$param=json_decode($_POST['data']);
		
		 
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		$html='';	
   		
		/*Parameter Unit*/
		// $tmpKdUnit="";
		/*$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}

		$tmpKdUnit = substr($tmpKdUnit, 0, -1);*/
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		$kriteria_unit = " And (t.kd_unit in (".$kd_unit.")) ";
		$kriteria_simple_unit= "kd_unit in(".$kd_unit.")";
		$asal_pasien = $param->asal_pasien;
		/* $cekKdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='1'");
		$cekKdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='2'");
		$cekKdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit in ($kd_unit) and kd_asal='3'");
		if (count($cekKdKasirRwj->result())==0){
			$KdKasirRwj='';
		}else{
			$KdKasirRwj=$cekKdKasirRwj->row()->kd_kasir;
		}
		
		if (count($cekKdKasirRwi->result())==0){
			$KdKasirRwi='';
		}else{
			$KdKasirRwi=$cekKdKasirRwi->row()->kd_kasir;
		}
		
		if (count($cekKdKasirIGD->result())==0){
			$KdKasirIGD='';
		}else{
			$KdKasirIGD=$cekKdKasirIGD->row()->kd_kasir;
		}
		if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwi."')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirIGD."')";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
			$crtiteriakodekasir="dtb.kd_kasir in ('".$KdKasirRwj."')";
		} */	
		if($kd_unit == "'44'"){
			# "Laboratorium PA Pav.";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24','31')";
				$crtiteriakodekasir="dtb.kd_kasir in ('24','31')";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('24')  and t.kd_pasien not like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('24')";
				$nama_asal_pasien = 'RWJ PAV';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('31')";
				$crtiteriakodekasir="dtb.kd_kasir in ('31')";
				$nama_asal_pasien = 'RWI PAV';
			} else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('24') and t.kd_pasien like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('24')";
				$nama_asal_pasien = 'Langsung';
			}
		} else if($kd_unit == "'45'"){
			# "Laboratorium PA";
			if($asal_pasien == 0 || $asal_pasien == 'Semua'){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25','26','27')";
				$crtiteriakodekasir="dtb.kd_kasir in ('25','26','27)";
				$nama_asal_pasien = 'Semua Pasien';
			} else if($asal_pasien == 1){
				$crtiteriaAsalPasien="and t.kd_kasir in ('25')  and t.kd_pasien not like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('25')";
				$nama_asal_pasien = 'RWJ';
			} else if($asal_pasien == 2){
				$crtiteriaAsalPasien="and t.kd_kasir in('26')";
				$crtiteriakodekasir="dtb.kd_kasir in ('26')";
				$nama_asal_pasien = 'RWI';
			}else if($asal_pasien == 3){
				$crtiteriaAsalPasien="and t.kd_kasir in('27')";
				$crtiteriakodekasir="dtb.kd_kasir in ('27)";
				$nama_asal_pasien = 'IGD';
			}else if($asal_pasien == 4){
				$crtiteriaAsalPasien="and t.kd_kasir in('25') and t.kd_pasien like 'LB%'";
				$crtiteriakodekasir="dtb.kd_kasir in ('25')";
				$nama_asal_pasien = 'Langsung';
			}
		}
		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
		$kriteria_bayar2 = " And (dtb.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		/* $kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		} */
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		//$kriteria_bayar = " ";
		
		/*Parameter Customer*/
		
		/*Parameter Customer*/
		if(strlen($param->tmp_kd_customer) > 0){
			$_tmp_kd_customer = substr($param->tmp_kd_customer, 0 , strlen($param->tmp_kd_customer)-1);
			$customerx=" And Ktr.kd_Customer in (".$_tmp_kd_customer.") ";
		}else{
			$customerx="";
		}
		
		/*$arrayDataCustomer = $param->tmp_kd_customer;
		if($arrayDataCustomer == ''){
			$customerx=" ";
			//$customer='Semua ';
		}else{
			$tmpKdCustomer="";
			for ($i=0; $i < count($arrayDataCustomer); $i++) { 
				$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
			}
			$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
			$customerx=" And Ktr.kd_Customer in (".$tmpKdCustomer.") ";
			//$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		}*/
		
		
		
		/*Parameter Shift*/
		$q_shift='';
   		$t_shift='';
   		/*if($param->shift0=='true'){
   			 $q_shift=" ((tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (1,2,3))		
			Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				 $q_shift=" And ".$q_shift;
   			}
   		}*/
		
		/*Parameter ShiftDB*/
		$q_shiftB='';
   		$t_shiftB='';
   		if($param->shift0=='true'){
   			 $q_shiftB="AND ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (1,2,3))		
			Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   			$t_shiftB='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shiftB='';
   				if($param->shift1=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shiftB.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shiftB!='')$s_shiftB.=',';
   					$s_shift.='3';
   				}
   				$q_shiftB.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shiftB="(".$q_shift." Or  (db.tgl_transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.shift=4) )	";
   				}
   				$t_shiftB='Shift '.$s_shiftB;
   				 $q_shiftB=" And ".$q_shiftB;
   			}
   		}
		/*Parameter Tindakan*/
		$kriteria_tindakan='';
		$t_tindakan='';
		/* if($param->tindakan0=='true' ){ 
			if($param->tindakan1=='true'){
				 $kriteria_tindakan =" and (( p.kd_klas = '1' ) or ( p.kd_klas <> '1' and p.kd_klas <> '9')) ";
				 $t_tindakan='Laporan Penerimaan Pendaftaran dan Tindakan Rawat Jalan';
			}else{
				 $kriteria_tindakan =  "and ( p.kd_klas = '1' ) ";
				 $t_tindakan='Laporan Penerimaan Pendaftaran';
			}
		}else if($param->tindakan1=='true'){  
			 $kriteria_tindakan ="  and ( p.kd_klas <> '1' and p.kd_klas <> '9')  ";
			 $t_tindakan='Laporan Penerimaan Tindakan Rawat Jalan';
		}else{
			$kriteria_tindakan='';
			 $t_tindakan='';
		} */
		/* if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			
			$kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 ";
			$t_tindakan='Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			$kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') ";
			$t_tindakan='Tindakan';
		}else{
			$kriteria_tindakan="";
			$t_tindakan='Pendaftaran dan Tindakan';
		} */
		/* if($param->tindakan0 ==true && $param->tindakan1==false)
		{
			/* $kriteria_tindakan =" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
								   INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')	 "; 
			$kriteria_tindakan =" and p.kd_klas IN ('1')	 ";
			$t_tindakan='Laporan Penerimaan Pendaftaran';
		}else if($param->tindakan0 ==false && $param->tindakan1 ==true){
			/* $kriteria_tindakan =" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk
									From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2') "; 
			$kriteria_tindakan =" and p.kd_klas not IN ('1')	 ";
			$t_tindakan='Laporan Penerimaan Tindakan Rawat Jalan';
		}else{
			$kriteria_tindakan="";
			 $t_tindakan='Laporan Penerimaan Pendaftaran dan Tindakan Rawat Jalan';
		} */
		
		/*Parameter Operator*/
		// $kd_user = $param->kd_user;
		$kd_user = '';
		$kriteria_user ='';
		$user='';
		/*if($kd_user == 'Semua'){
			 $kriteria_user ='';
			 $user='Semua';
		}else{
			$kriteria_user =" and kd_user= '$kd_user' ";
			$user=$this->db->query("Select * From zusers Where kd_user='$kd_user'")->row()->full_name;
		}*/
		
		/*Parameter Transfer*/
		/* $kriteria_transfer='';
		$transfer_lab =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_lab'")->row()->setting;
		$transfer_rad =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_rad'")->row()->setting;
		$transfer_apt =$this->db->query("Select * From sys_setting Where key_data='kd_transfer_apt'")->row()->setting;
		if($param->transfer0=='true'){ 
			if($param->transfer1=='true'){ 
				if($param->transfer2=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) "; //lab,rad,apt
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";//lab,rad
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab) or p.kd_produk = $transfer_apt ) ";//lab,apt
			}else{
				$kriteria_transfer=" and p.kd_produk in ($transfer_lab)    ";//lab
			}
		}else if($param->transfer1=='true'){  
			if($param->transfer0=='true'){ 
				if($param->transfer1=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad  ) ";
				}
			}else if($param->transfer2=='true'){
				$kriteria_transfer=" and ( p.kd_produk = $transfer_rad' or p.kd_produk = $transfer_apt ) ";
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_rad    ";
			}
		}else if($param->transfer2=='true'){  
			if($param->transfer0=='true'){ 
				if($param->transfer1=='true'){ 
					$kriteria_transfer=" and ( p.kd_produk in ($transfer_lab)  or p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}else{
					$kriteria_transfer=" and (  p.kd_produk = $transfer_rad or p.kd_produk = $transfer_apt ) ";
				}
			}else{
				$kriteria_transfer=" and p.kd_produk = $transfer_apt    ";
			}
		} */
		
		//query untuk kolom
		$query_kolom = $this->db->query(" SELECT Distinct dc.kd_Component as kd_Component, max(pc.Component) as komponent
											From Produk_Component pc 	
											INNER JOIN Detail_Component dc On dc.kd_Component=pc.Kd_Component 
											INNER JOIN Detail_Transaksi dt On dc.kd_kasir=dt.kd_kasir And dc.No_Transaksi=dt.No_Transaksi And dc.Tgl_Transaksi=dt.tgl_Transaksi And dc.Urut=dt.urut 
											INNER JOIN Produk p on p.kd_Produk=dt.kd_Produk  
											INNER JOIN Transaksi t on t.kd_Kasir=dt.kd_kasir And t.no_transaksi=dt.no_Transaksi 
											INNER JOIN Detail_Bayar db on t.kd_Kasir=db.kd_kasir And t.no_transaksi=db.no_Transaksi  
											Where dc.kd_component <> '36'  $crtiteriaAsalPasien   
											$kriteria_unit  $q_shiftB
											Group by dc.kd_Component  Order by kd_Component ")->result();
		// echo '{success:true, totalrecords:'.count($query_kolom).', listData:'.json_encode($query_kolom).'}';
		$arr_kd_component = array(); //array menampung kd_component
		if(count($query_kolom) > 0){
			//proses menampung kd_component
			$i=0;
			foreach($query_kolom as $line){
				$arr_kd_component[$i] = $line->kd_component;
				$i++;
			}
			$jml_kolom=count($query_kolom)+6;
		}else{
			$jml_kolom=6;
		}
		$type_file = $param->type_file;
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		// -------------JUDUL-----------------------------------------------
			$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						
						<tr>
							<th colspan='.$jml_kolom.' style='.$font_style.'>'.$title.'<br>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan='.$jml_kolom.'>'.$nama_asal_pasien.'</th>
						</tr>
					</tbody>
				</table><br>';
			
			//---------------ISI-----------------------------------------------------------------------------------
			$html.='
				<table class="t1" border = "1" cellpadding="2">
				<thead>
					 <tr>
						<th align="center" width="40px">No</th>
						<th align="center">Poliklinik/Tindakan</th>
						<th align="center">Jumlah Pasien</th>
						<th align="center">Jumlah Produk</th>';
				foreach($query_kolom as $line){
					$html.='<th align="center">'.$line->komponent.'</th>';
				}
			$html.='<th align="center" width="80px">Jasa Pelayanan</th><th align="center" width="80px">Total</th>
					  </tr>
				</thead><tbody>';
				
		
		$query_poli = $this->db->query(" SELECT distinct nama_unit from
													( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
														 sum(x.jPL)as jPL
													From 
														(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
															dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
															 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
														 FROM DETAIL_TR_BAYAR dtb
															 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
															 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
															 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
														 WHERE    
															$crtiteriakodekasir  And										--> kd_kasir RWJ
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
															--kriteria pembayaran
															$kriteria_bayar
															and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
														 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
														 ) x
														 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
														 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
														 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
														 INNER JOIN Unit u On u.kd_unit=t.kd_unit
														 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
														 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
													 where 
														(p.kd_klas is not null)   
														$kriteria_unit 
														$crtiteriaAsalPasien
														$customerx
														And t.IsPay = 't'
														and dt.kd_produk <> '417'  
													GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
													ORDER BY U.Nama_Unit
													)Y 
												 ")->result();
		//echo '{success:true, totalrecords:'.count($query_poli).', listData:'.json_encode($query_poli).'}';
		if(count($query_poli)>0){
			$no_unit = 1;
			$u=0;
			$jum_produk=0;
			
			foreach($query_poli as $line){
				$nama_unit = $line->nama_unit;
				$html.='<tr>
							<td align="center" width="40px">'.$no_unit.' </td>
							<td  >'.$nama_unit.'</td>';
				for($j=0; $j<=count($query_kolom)+3;$j++){
					$html.='<td></td>';
				}		
				$html.='</tr>';
				$query_tindakan = $this->db->query(" SELECT distinct deskripsi,jml_pasien,qty from
													( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, coalesce(SUM(x.Jumlah),0) as JUMLAH,
														 sum(x.jPL)as jPL
													From 
														(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, sum(dtbc.Jumlah) as jumlah,
															 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
														 FROM DETAIL_TR_BAYAR dtb
															 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
															 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
															 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
														 WHERE    
															$crtiteriakodekasir  And										--> kd_kasir RWJ
															(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	
															
															$kriteria_bayar
															and dtb.jumlah <> 0					
														 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty
														 ) x
														 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
														 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
														 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
														 INNER JOIN Unit u On u.kd_unit=t.kd_unit
														 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
														 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
													 where 
														(p.kd_klas is not null)   
														$kriteria_unit 
														$crtiteriaAsalPasien
														$customerx
														And t.IsPay = 't'
														and dt.kd_produk <> '417'  
													GROUP BY U.Nama_Unit, p.Deskripsi
													ORDER BY U.Nama_Unit
													)Y where nama_unit='$nama_unit' order by Deskripsi
												 ")->result();
				//echo '{success:true, totalrecords:'.count($query_tindakan).', listData:'.json_encode($query_tindakan).'}';
				$p=0; //counter tindakan
				$jumlah_produk=0;
				$jum_total = 0;
				$jum_jpl = 0;
				$da=0;
				foreach($query_tindakan as $line2){
					$deskripsi = $line2->deskripsi;
					$jml_pasien = $line2->jml_pasien;
					$qty = $line2->qty;
					$html.='<tr>
								<td ></td>
								<td  > - '.$deskripsi.'</td>
								<td  align="right">'.$jml_pasien.'</td>
								<td  align="right">'.$qty.'</td>';
					$query_tindakan_poli = $this->db->query(" SELECT * from
																( SELECT U.Nama_Unit, p.Deskripsi, Count(k.Kd_Pasien)as Jml_Pasien, SUM(dt.Qty) as Qty, x.kd_component, coalesce(SUM(x.Jumlah),0) as JUMLAH,
																	 sum(x.jPL)as jPL
																From 
																	(SELECT  dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI,
																		dtbc.kd_component, sum(dtbc.Jumlah) as jumlah,
																		 Sum(case when pc.kd_jenis=2 then dtbc.jumlah else 0 end ) as jPL  
																	 FROM DETAIL_TR_BAYAR dtb
																		 Inner Join DETAIL_TR_BAYAR_COMPONENT dtbc        ON dtb.KD_KASIR = dtbc.KD_KASIR       AND dtb.NO_TRANSAKSI = dtbc.NO_TRANSAKSI AND dtb.URUT = dtbc.URUT       AND dtb.TGL_TRANSAKSI = dtbc.TGL_TRANSAKSI AND dtb.KD_PAY = dtbc.KD_PAY       AND dtb.URUT_BAYAR = dtbc.URUT_BAYAR And dtb.TGL_BAYAR = dtbc.TGL_BAYAR
																		 Inner Join DETAIL_BAYAR db ON dtb.KD_KASIR = db.KD_KASIR       AND dtb.NO_TRANSAKSI = db.NO_TRANSAKSI AND dtb.URUT_BAYAR = db.URUT       AND dtb.TGL_BAYAR = db.Tgl_Transaksi
																		 inner join detail_transaksi dt ON dt.kd_kasir = dtb.kd_kasir and dt.no_transaksi = dtb.no_transaksi  and dt.urut = dtb.urut and dt.tgl_transaksi = dtb.tgl_transaksi  INNER JOIN Produk_Component pc ON dtbc.kd_component = pc.kd_component
																	 WHERE    
																		$crtiteriakodekasir  And										--> kd_kasir RWJ
																		(dtb.TGL_BAYAR between '$tgl_awal'   and  '$tgl_akhir')	--> tgl bayar dari detail_tr_bayar
																		--kriteria pembayaran
																		$kriteria_bayar
																		and dtb.jumlah <> 0					--> yang jumlah pembayarannya tidak 0 
																	 GROUP BY dtbc.KD_KASIR, dtbc.NO_TRANSAKSI, dtbc.URUT, dtbc.TGL_TRANSAKSI, dt.qty,dtbc.kd_component
																	 ) x
																	 INNER JOIN  Transaksi T on X.kd_kasir = t.kd_kasir and X.no_transaksi = t.no_transaksi
																	 INNER JOIN  Kunjungan K On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
																	 INNER JOIN  Detail_transaksi dt On dt.KD_KASIR = x.KD_KASIR AND dt.NO_TRANSAKSI = x.NO_TRANSAKSI   AND dt.Urut = x.Urut  And dt.Tgl_Transaksi = x.Tgl_Transaksi
																	 INNER JOIN Unit u On u.kd_unit=t.kd_unit
																	 INNER JOIN Produk p on p.kd_produk= dt.kd_produk
																	 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
																 where 
																	(p.kd_klas is not null)   
																	$kriteria_unit 
																	$crtiteriaAsalPasien
																	$customerx
																	And t.IsPay = 't'
																	and dt.kd_produk <> '417'  
																GROUP BY U.Nama_Unit, p.Deskripsi, x.kd_component
																ORDER BY U.Nama_Unit
																)Y where nama_unit='$nama_unit' and deskripsi='$deskripsi' order by kd_component 
												 ")->result();
					//echo '{success:true, totalrecords:'.count($query_tindakan_poli).', listData:'.json_encode($query_tindakan_poli).'}';	
					
					$k=0;
					$jumlah_jpl=0;
					$arr_data_component = array(); //arr tampung data komponent
					foreach($query_tindakan_poli as $line3){
						//var_dump( $line3->jumlah);
						//unset($arr_data_component);
						$arr_data_component[$k] ['kd_component']= $line3->kd_component;
						$arr_data_component[$k] ['jumlah']= $line3->jumlah;
						$jumlah_jpl=$jumlah_jpl+$line3->jpl;
						$k++;
					}
					//mengisi kolom nilai komponen
					for($l=0; $l<count($arr_kd_component); $l++){
						$cari_kd_component = $this->searchForId($arr_kd_component[$l],$arr_data_component);
						//var_dump( $cari_kd_component);
						if($cari_kd_component != null){									
							$tampung_data_component[$l] = $cari_kd_component;
						}else{
							$tampung_data_component[$l] =0;
						}
					}
					
					//hitung nilai komponen per tindakan
					$total_component=array_sum($tampung_data_component);
					for($j=0; $j<count($arr_kd_component);$j++){
						$html.='<td align="right">'.number_format($tampung_data_component[$j],0, "." , ",").'</td>';
						$arr_tamp_jumlah[$p][$j] = $tampung_data_component[$j];
					}
					$html.='<td align="right">'.number_format($jumlah_jpl,0, "." , ",").'</td>';
					$total_semua = $total_component;
					$html.='<td align="right">'.number_format($total_semua,0, "." , ",").'</td></tr>';
					$html.='</tr>';
				
					$p++;
					$jumlah_produk = $jumlah_produk + $qty;
					$jum_total = $jum_total + $total_semua;
					$jum_jpl = $jum_jpl +$jumlah_jpl; 
					$da++;
					
					
				}
				$jum_produk = $jum_produk +$jumlah_produk;
				if($p == 1){
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$arr_tamp_sub[$x]= $arr_tamp_jumlah[0][$x];
					}
					$html.='<tr>
							<td align="right" colspan="2" > Sub Total</td>';
					$html.='<td></td><td align="right">'.$jumlah_produk.'</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				}else{
					//transpos array
					array_unshift($arr_tamp_jumlah, null);
					$arr_tamp_jumlah = call_user_func_array('array_map', $arr_tamp_jumlah);
					for($x = 0; $x<count($arr_kd_component) ;$x++){
						$tmp_nilai=0;
						for($y=0; $y<$p ;$y++){
							$tmp_nilai= $tmp_nilai+$arr_tamp_jumlah[$x][$y];
						}
						$arr_tamp_sub[$x]= $tmp_nilai;
					}
					$html.='<tr>
							<td align="right" colspan="2" > Sub Total</td>';
					$html.='<td></td><td align="right">'.$jumlah_produk.'</td>';
					for($j=0; $j<count($query_kolom);$j++){
						$html.='<td align="right">'.number_format($arr_tamp_sub[$j],0, "." , ",").'</td>';
						$arr_tamp_grand[$u][$j]= $arr_tamp_sub[$j];
						
					}	
					$arr_tamp_grand[$u][count($query_kolom)]=$jum_jpl;
					$arr_tamp_grand[$u][count($query_kolom)+1]=$jum_total;
					$html.='<td align="right">'.number_format($jum_jpl,0, "." , ",").'</td>';
					$html.='<td align="right">'.number_format($jum_total,0, "." , ",").'</td></tr>';
				}
				$no_unit++;
				$u++;
			}
			if($u==1){
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$arr_tamp_grand_tot[$x]= $arr_tamp_grand[0][$x];
				}
				$html.='<tr><td align="right" colspan="2" > GRAND TOTAL</td>';
				$html.='<td></td><td align="right">'.$jum_produk.'</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
			}else{
				array_unshift($arr_tamp_grand, null);
				$arr_tamp_grand = call_user_func_array('array_map', $arr_tamp_grand);
				for($x = 0; $x<=count($arr_kd_component)+1 ;$x++){
					$tmp_nilai2=0;
					for($y=0; $y<$u ;$y++){
						$tmp_nilai2= $tmp_nilai2+$arr_tamp_grand[$x][$y];
					}
					$arr_tamp_grand_tot[$x]= $tmp_nilai2;
				}
				$html.='<tr><td align="right" colspan="2" > GRAND TOTAL</td>';
				$html.='<td></td><td align="right">'.$jum_produk.'</td>';
				for($j=0; $j<=count($query_kolom)+1;$j++){
					$html.='<td align="right">'.number_format($arr_tamp_grand_tot[$j],0, "." , ",").'</td>';
				}
				$html.="</tr>";
			}
		}else{
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan='.$jml_kolom.' align="center">Data tidak ada</th>
				</tr>';		
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name='Laporan_Tunai_Perkomponen_Summary.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
			
		}else{
			$this->common->setPdf('L','Laporan Tunai Perkomponen Summary',$html);	
			//echo $html;
		}
		
	}
	public function cetak_lama(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN PENERIMAAN TUNAI PER KOMPONEN SUMMARY';
		$param=json_decode($_POST['data']);
		
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		$_kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		$key="'4";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		$tmpKdPay="";
		$type_file = 0;
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		$kel_pas = $param->pasien;
		if($kel_pas== -1)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 0){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 1){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 2){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		/* if($kel_pas!= -1){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' && $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		} */
		
		
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=".$kd_unit." and kd_asal='3'")->row()->kd_kasir;
		if($asal_pasien == -1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."','".$KdKasirRwi."','".$KdKasirIGD."')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 0){
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien not like 'LB%'";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirRwi."'";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir='".$KdKasirIGD."'";
			$nama_asal_pasien = 'IGD';
		}else{
			$crtiteriaAsalPasien="and t.kd_kasir in ('".$KdKasirRwj."') and t.kd_pasien like 'LB%'";
		}
		
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
		$_kduser = $this->session->userdata['user_id']['id'];
		$kd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$_kduser."'")->row()->kd_unit;
		//$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_unit=".$kd_unit.")";
		
		$queryHead = $this->db->query(
			"
			 SELECT U.Nama_Unit
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")  
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit
                "
		);
		
		$queryBody = $this->db->query(
			"
			 SELECT U.Nama_Unit,count(k.Kd_Pasien) as Jml_Pasien, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi,
						COALESCE(SUM(x.Jumlah),0) as JUMLAH , 
						Sum(C20) as C20, Sum(C21) as C21, 
						Sum(C22) as C22, Sum(C23) as C23, 
						Sum(C24) as C24, Sum(C25) as C25, 
						Sum(C30) as C30 
				FROM Kunjungan k 
				INNER JOIN Transaksi t   On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit   And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk  
				LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
				INNER JOIN unit on unit.kd_unit=t.kd_unit  
				INNER JOIN Detail_Transaksi dt ON dt.kd_kasir = t.kd_kasir and dt.no_transaksi = t.no_transaksi 
				INNER JOIN Detail_Bayar db ON db.kd_kasir = dt.kd_kasir and db.no_transaksi = dt.no_transaksi 
				INNER JOIN 
				(
					SELECT dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi,  dtc.kd_pay, dtc.urut_bayar, dtc.tgl_bayar, 
						SUM(CASE WHEN dtc.kd_component =20 THEN dtc.Jumlah ELSE 0 END) as C20, 
						SUM(CASE WHEN dtc.kd_component =21 THEN dtc.Jumlah ELSE 0 END) as C21,  
						SUM(CASE WHEN dtc.kd_component =22 THEN dtc.Jumlah ELSE 0 END) as C22,  
						SUM(CASE WHEN dtc.kd_component =23 THEN dtc.Jumlah ELSE 0 END) as C23, 
						SUM(CASE WHEN dtc.kd_component =24 THEN dtc.Jumlah ELSE 0 END) as C24, 
						SUM(CASE WHEN dtc.kd_component =25 THEN dtc.Jumlah ELSE 0 END) as C25,  
						SUM(CASE WHEN dtc.kd_component =30 THEN dtc.Jumlah ELSE 0 END) as C30,  dtb.Jumlah  
					FROM (
						(
						Detail_TR_Bayar dtb 
						INNER JOIN Detail_TR_Bayar_Component dtc ON dtc.kd_kasir = dtb.kd_kasir  and dtc.no_transaksi = dtb.no_transaksi and dtc.urut = dtb.urut  and dtc.tgl_transaksi = dtb.tgl_transaksi and
							   dtc.kd_pay = dtb.kd_pay and dtc.urut_bayar = dtb.urut_bayar and dtc.tgl_bayar = dtb.tgl_bayar
						) 
						INNER JOIN Produk_Component pc ON dtc.kd_component = pc.kd_component) 
						GROUP BY dtc.kd_kasir, dtc.no_transaksi, dtc.urut, dtc.tgl_transaksi, dtc.kd_pay,  
							dtc.urut_bayar, dtc.tgl_bayar, dtb.Jumlah
				) x ON x.kd_kasir = db.kd_kasir 
					and x.no_transaksi = db.no_transaksi and x.urut_bayar = db.urut  
					and x.tgl_bayar = db.tgl_transaksi 
					and x.kd_kasir = dt.kd_kasir  
					and x.no_transaksi = dt.no_transaksi 
					and x.urut = dt.urut 
					and x.tgl_transaksi = dt.tgl_transaksi 
				INNER JOIN Unit u On u.kd_unit=t.kd_unit 
				INNER JOIN Produk p on p.kd_produk= dt.kd_produk 
				LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
				INNER JOIN Payment Py ON db.Kd_Pay = Py.Kd_Pay 
				INNER JOIN Pasien ps On ps.kd_pasien=t.Kd_pasien   
				WHERE ".$q_shift."  And t.kd_Unit in (".$kd_unit.")
					  ".$kriteria_bayar."
					 ".$crtiteriaAsalPasien." 
					  and unit.kd_bagian=4  
					  GROUP BY U.Nama_Unit, Ps.Kd_Pasien, Ps.Nama, p.Deskripsi
                "
		);
		
		if($type_file == 1){
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:14px;
				   }
                   </style>";
		}else{
			$html.="<style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-family: Arial, Helvetica, sans-serif;
                   }
				   .t1 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
				    .t1 tr td
				   {
					   font-size:12px;
				   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                  
				   .t2 {
                           font-family: Arial, Helvetica, sans-serif;
                   }
					
					.t2 tr th
				   {
					   font-weight:bold;
					   font-size:12px;
				   }
                   </style>";
			
		}
		$query = $queryHead->result();			
		$query2 = $queryBody->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table class="t2" cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="8">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="8"> Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="8">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$all_total_jml_pasien = 0;
		$all_total_jml_produk = 0;
		$all_total_jasa_dok = 0;
		$all_total_jasa_perawat = 0;
		$all_total_indeks_tdk_langsung = 0;
		$all_total_ops_instalasi = 0;
		$all_total_ops_rs = 0;
		$all_total_ops_direksi = 0;
		$all_total_jasa_sarana = 0;
		$all_total_jumlah = 0;
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		$html.='
			<table class="t1" width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="33%" align="center">Tindakan</th>
					<th width="13%" align="center">Jumlah Pasien</th>
					<th width="13%" align="center">Jumlah Produk</th>
					<th width="13%" align="center">Jasa Dokter</th>
					<th width="13%" align="center">Jasa Perawat/Bidan</th>
					<th width="13%" align="center">Indeks Tidak Langsung</th>
					<th width="13%" align="center">Ops. Instalasi</th>
					<th width="13%" align="center">Ops. RS</th>
					<th width="13%" align="center">Ops. Direksi</th>
					<th width="13%" align="center">Jasa Sarana</th>
					<th width="13%" align="center">Total</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($queryHead->result()) > 0) {
			$no=0;
			foreach($query as $line){
				$no++;
				$html.='
				<tr class="headerrow"> 
					<th width="">'.$no.'</th>
					<th width="" colspan="11" align="left">'.$line->nama_unit.'</th>
				</tr>
				';
				$no2=0;
				 foreach($query2 as $line2){
						$all_total_jml_pasien += $line2->jml_pasien;
						$all_total_jml_produk += $line2->jml_pasien;
						$all_total_jasa_dok += $line2->c20;
						$all_total_jasa_perawat += $line2->c21;
						$all_total_indeks_tdk_langsung += $line2->c22;
						$all_total_ops_instalasi += $line2->c23;
						$all_total_ops_rs += $line2->c24;
						$all_total_ops_direksi += $line2->c25;
						$all_total_jasa_sarana += $line2->c30;
						$all_total_jumlah += $line2->jumlah;
						$html.='
						<tr> 
							<td width="">&nbsp;</th>
							<td width="" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$line2->deskripsi.' </td>
							<td width="" align="left">'.$line2->jml_pasien.' </td>
							<td width="" align="left">'.$line2->jml_pasien.' </td>
							<td width="" align="left">'.$this->rupiah($line2->c20).' </td>
							<td width="" align="left">'.$this->rupiah($line2->c21).' </td>
							<td width="" align="left">'.$this->rupiah($line2->c22).' </td>
							<td width="" align="left">'.$this->rupiah($line2->c23).' </td>
							<td width="" align="left">'.$this->rupiah($line2->c24).' </td>
							<td width="" align="left">'.$this->rupiah($line2->c25).' </td>
							<td width="" align="left">'.$this->rupiah($line2->c30).' </td>
							<td width="" align="left">'.$this->rupiah($line2->jumlah).' </td>
							
							</tr>
						';
					
					
				} 
				
			}
			
			$html.='
					<tr> 
						<th width="" colspan="2">&nbsp;</th>
						<th width="" align="left">'.$this->rupiah($all_total_jml_pasien).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jml_produk).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_dok).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_perawat).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_indeks_tdk_langsung).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_instalasi).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_rs).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_ops_direksi).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jasa_sarana).' </th>
						<th width="" align="left">'.$this->rupiah($all_total_jumlah).' </th>
						
						</tr>
					';
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="10" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$name=' Lap_Penerimaan_Tunai_PerKomponen_Sum.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
		}else{
			$this->common->setPdf('L','Lap. Penerimaan Tunai Per Komponen Summary',$html);	
		}
		echo $html;
   	}
	function searchForId($id, $array) {
	   foreach ($array as $key => $val) {
		   if ($val['kd_component'] === $id) {
			   return $val['jumlah'];
		   }
	   }
	   return null;
	}
	
}
?>