<?php

class function_lab_sql extends  MX_Controller {
	
	public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = null) {
         $this->db->trans_begin();

         if($_POST['Modul']=='rwj' || $_POST['Modul']=='igd' || $_POST['Modul']=='rwi'){
          $unitasal =  $_POST['KdUnit'];
         }else{
          $unitasal=$this->db->query("select setting from sys_setting where key_data = 'rad_default_kd_unit'")->row()->setting;
         }

         $KdPasien = $Params["KdPasien"];     
         $KdUnit_t = $Params["KdUnitTujuan"]; 
         $Tgl = $Params["Tgl"];
         $urut = $Params["URUT"];
         $unitasal = $Params['KdUnit'];
         $IdAsal = '';
         $list = json_decode($_POST['List']);
         $listtrdokter = json_decode($_POST['listTrDokter']);
         $KdKasirAsal = $_POST['KdKasirAsal'];
         $Kamar = $_POST['Kamar'];
         $KdSpesial = $_POST['KdSpesial'];
         $TglTransaksiAsal = $_POST['TglTransaksiAsal'];

         if($KdUnit_t=='' || $TglTransaksiAsal==''){
			//$KdUnit='41';
			
			$TglTransaksiAsal=$Tgl;
		} else{
			//$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}

         if(substr($unitasal, 0, 1) == '1'){
             # RWI
             $IdAsal=1;
         } else if(substr($unitasal, 0, 1) == '2'){
             # RWJ
             $IdAsal=0;
         } else if(substr($unitasal, 0, 1) == '3'){
             # UGD
             $IdAsal=0;
         }else
         {
             // $IdAsal = $this->GetIdAsalPasien($unit);
         }
         $pasienBaru = 0;
         $Shift=$Params['Shift'];
         $KdTransaksi = $_POST['KdTransaksi'];
         $kunjungan_lab = $_POST['kunjungan_lab'];

         $datakunjungan = $this->db->query("select * from kunjungan where kd_pasien = '$KdPasien' and kd_unit = '$KdUnit_t' and tgl_masuk = '$Tgl' and urut_masuk = $urut");
         foreach ($datakunjungan->result() as $rec)
         {
           $KdPasien=$rec->kd_pasien;
           $KdUnit=$rec->kd_unit;
           $Tgl=$rec->tgl_masuk;
           $urut=$rec->urut_masuk;
           $KdDokter=$rec->kd_dokter;
           $Shift=$rec->shift;
           $KdCusto=$rec->kd_customer;
           $NoSJP=$rec->no_sjp;
           $JamKunjungan=$rec->jam_masuk;
         }

         $datatransaksi = $this->db->query("select * from transaksi where kd_pasien = '$KdPasien' and kd_unit = '$KdUnit' and tgl_transaksi = '$Tgl' and urut_masuk = $urut");
         foreach ($datatransaksi->result() as $rec)
         {
           $kdkasir=$rec->kd_kasir;
           $notrans=$rec->no_transaksi;
           $kdpasien=$rec->kd_pasien;
           $unit=$rec->kd_unit;
           $Tgl=$rec->tgl_transaksi;
           $urut=$rec->urut_masuk;
         }

         $simpankeunitasal='';
		if ($kunjungan_lab=='Baru')
		{
			$simpankunjunganlab = $this->simpankunjungan($KdPasien,$KdUnit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru,$JamKunjungan);
         	if ($simpankunjunganlab == 'aya'){
            $simpanmrlabb= $this->SimpanMrLab($KdPasien,$KdUnit,$Tgl,$urut);
            if($simpanmrlabb == 'Ok'){
              
                 $hasil = $this->SimpanTransaksi($kdkasir,$notrans,$kdpasien,$unit,$Tgl,$urut);
                 if($hasil == 'sae'){
                 	if($unitasal != '' && substr($unitasal, 0, 1) =='1'){
							# jika bersal dari rawat inap
							$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasir,$notrans,$unitasal,$Kamar,$KdSpesial);
						} else{
							$simpanunitasalinap='Ok';
						}
                    	$detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasir,$unitasal,$Shift,$unit,$kdpasien,$urut,$TglTransaksiAsal);
                    if($detail){
                    	if ($simpankeunitasal=='ya')
							{
								if($pasienBaru == 0){
								# jika bukan Pasien baru/kunjungan langsung
								    $simpanunitasall = $this->SimpanUnitAsal($kdkasir,$notrans,$notrans,$KdKasirAsal,$IdAsal);
								}else{
									$simpanunitasall = $this->SimpanUnitAsal($kdkasir,$notrans,$notrans,$kdkasirpasien,$IdAsal);
								}								
							}else
							{
								$simpanunitasall = 'Ok';
							}

							if($simpanunitasalinap == 'Ok' && $simpanunitasall == 'Ok'){
									$str='Ok';
								} else{
									$str='error';
								}
                    }else{
                    	$str='error';
                    }

                 }else{
                    $str='error';
                 }
            } else{
                $str='error';
            }
         } else{
             $str='error';
         }
         if ($simpanmrlabb == 'Ok'){
				$this->db->trans_commit();
				echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}else{
			$detail= $this->detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasir,$unitasal,$Shift,$unit,$kdpasien,$urut,$TglTransaksiAsal);
			if($detail){
			$this->db->trans_commit();
			echo "{success:true}";
			} else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		}

            
    }

    public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru,$JamKunjungan)
    {
        $strError = "";
        $data = array("kd_pasien"=>$kdpasien,
                      "kd_unit"=>$unit,
                      "tgl_masuk"=>$Tgl,
                      "kd_rujukan"=>"0",
                      "urut_masuk"=>$urut,
                      "jam_masuk"=>$JamKunjungan,
                      "kd_dokter"=>$kddokter,
                      "shift"=>$Shift,
                      "kd_customer"=>$KdCusto,
                      "karyawan"=>"0",
                      "no_sjp"=>$NoSJP,
                      "keadaan_masuk"=>0,
                      "keadaan_pasien"=>0,
                      "cara_penerimaan"=>99,
                      "asal_pasien"=>$IdAsal,
                      "cara_keluar"=>0,
                      "baru"=>$pasienBaru,
                      "kontrol"=>"0"
                   );

        $criterianya = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

        $query=_QMS_Query("select * from kunjungan where ".$criterianya)->result();

        if (count($query)==0)
        {
             $result= _QMS_insert('KUNJUNGAN', $data,$criterianya);
             if ($result)
             {
                 $strError = "aya";
             }else{
                 $strError = "teu aya";
             }            
        }else{
        
            $result=_QMS_update('KUNJUNGAN', $data,$criterianya);
            $strError = "aya";
        }
        return $strError;
    }

    public function SimpanMrLab($KdPasien,$unit,$Tgl,$urut)
   {
     $strError = "";
     $data = array("kd_pasien"=>$KdPasien,
               "kd_unit"=>$unit,
               "tgl_masuk"=>$Tgl,
               "urut_masuk"=>$urut
             );

     //-----------insert to sq1 server Database---------------//
     $result = _QMS_insert('mr_lab',$data);
     //-----------akhir insert ke database sql server----------------//
     if($result){
       $strError='Ok';
     } else{
       $strError='error';
     }

     return $strError;
   }

   public function SimpanTransaksi($kdkasir,$notrans,$kdpasien,$unit,$Tgl,$urut)
   {
      $strError = "";
      $data = array(
             "kd_kasir"=>$kdkasir,
             "no_transaksi"=>$notrans,
             "kd_pasien"=>$kdpasien,
             "kd_unit"=>$unit,
             "tgl_transaksi"=>$Tgl,
             "urut_masuk"=>$urut,
             "tgl_co"=>NULL,
             "co_status"=>0,
             "orderlist"=>NULL,
             "ispay"=>0,
             "app"=>0,
             "kd_user"=>$this->session->userdata['user_id']['id'],
             "tag"=>NULL,
             "lunas"=>0,
             "tgl_lunas"=>NULL);

       $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasir."'";

       $query=_QMS_Query("SELECT * FROM TRANSAKSI WHERE ".$criteria)->result();
        
       if (count($query)==0)
       {
         $result = _QMS_insert('TRANSAKSI', $data,$criteria);
         if($result){
           $strError = "sae";
         } else{
           $strError = "eror";
         }
       }
       else
       {
         $strError = "sae";
       }
       return $strError;
   }

   private function detailsaveall($listtrdokter,$list,$notrans,$Tgl,$kdkasir,$unitasal,$Shift,$unit,$kdpasien,$urut,$Tglasal){
     $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
     $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
     $lab_cito_pk = $this->db->query("select setting from sys_setting where key_data = 'sys_lab_cito_pk'")->row()->setting;
     $kdUser=$this->session->userdata['user_id']['id'];
     $urutlabhasil=1;
     $j=0;
    for($i=0;$i<count($list);$i++){
      $kd_produk=$list[$i]->KD_PRODUK;
      $qty=$list[$i]->QTY;
      $tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
      $tgl_berlaku=$list[$i]->TGL_BERLAKU;
      if (isset($list[$i]->cito))
      {
        $cito=$list[$i]->cito;
        if($cito=='Ya')
        {$cito='1';
        $harga=$list[$i]->HARGA;
        $hargacito = (((int) $harga) * ((int)$lab_cito_pk))/100;
        $harga=((int)$list[$i]->HARGA)+((int)$hargacito);
        
        }
        else if($cito=='Tidak')
        {$cito='0';
        $harga=$list[$i]->HARGA;
        }
      } else
      {
       $cito='0';
       $harga=$list[$i]->HARGA;
      }
      $kd_tarif=$list[$i]->KD_TARIF;

      $criteria = "kd_kasir='$kdkasir' and no_transaksi='$notrans' and tgl_transaksi='$Tgl' and kd_produk='$kd_produk'";

      $cekDetailTrx=  $query=_QMS_Query("SELECT * FROM detail_transaksi WHERE ".$criteria)->result();
      if (count($cekDetailTrx)==0)
      {
        $urut = $i+1;
        $urutdetailtransaksi =  $this->db->query("select geturutdetailtransaksi('$kdkasir','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
        $sql = "exec dbo.V5_insert_detail_transaksi '".$kdkasir."', '".$notrans."',".$urut.",
          '".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$list[$i]->kd_unit."',
          '".$tgl_berlaku."',0,0,'',".$qty.",".$harga.",".$Shift.",0,''";
        $query = _QMS_Query($sql);
      }else{
		$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasir','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
		$query=false;			
      }
        if($cito==='1')
        {
         $query = _QMS_Query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasir."' and no_transaksi='".$notrans."'
         and urut=".$urut." and tgl_transaksi='".$Tgl."'");
          $query = _QMS_Query("update transaksi set cito=1 where kd_kasir='".$kdkasir."' and no_transaksi='".$notrans."'");
        } //tutup cito
	        $qsql=$this->db->query(" select * from detail_component 
	                    where kd_kasir='".$kdkasir."' 
	                      and no_transaksi='".$notrans."'
	                      and urut=".$urut."
	                      and tgl_transaksi='".$Tgl."'")->result();
	      foreach($qsql as $line){
	        $qkd_kasir=$line->kd_kasir;
	        $qno_transaksi=$line->no_transaksi;
	        $qurut=$line->urut;
	        $qtgl_transaksi=$line->tgl_transaksi;
	        $qkd_component=$line->kd_component;
	        $qtarif=$line->tarif;
	      
	      }

	      if($query){
	        $ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
	        where 
	        (kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
	        kd_unit ='".$unitasal."' AND
	        kd_produk='".$kd_produk."' AND
	        tgl_berlaku='".$tgl_berlaku."' AND
	        kd_tarif='".$kd_tarif."' group by tarif")->result();
	        foreach($ctarif as $ct)
	        {
	          if($ct->jumlah != 0)
	          {
	            $trDokter = _QMS_Query("insert into detail_trdokter select '03','".$notrans."'
	            ,'".$urut."','".$_POST['KdDokter']."','".$Tgl."',0,0,".$ct->tarif.",0,0,0 WHERE
	              NOT EXISTS (
	                SELECT * FROM detail_trdokter WHERE   
	                  kd_kasir= '03' AND
	                  tgl_transaksi='".$Tgl."' AND
	                  urut='".$urut."' AND
	                  kd_dokter = '".$_POST['KdDokter']."' AND
	                  no_transaksi='".$notrans."'
	              )");
	          }
	        }

	        // $query = _QMS_Query("insert into lab_hasil 
         //            (kd_lab, kd_test, kd_produk, kd_pasien,kd_unit, tgl_masuk, urut_masuk, 
         //             urut, kd_unit_asal, tgl_masuk_asal, kd_metode)
                     
         //              select $kd_produk,kd_test,$kd_produk,'$kdpasien','$unit','$Tgl',$urut,
         //                row_number() over (order by kd_test asc)
         //                +(select count(urut) from lab_hasil where kd_pasien='$kdpasien'
         //                and kd_unit='$unit' and tgl_masuk='$Tgl' and urut_masuk=$urut) as rownum,
         //              $unit,'$Tglasal',0
         //            from lab_produk where kd_produk=$kd_produk and kd_lab=$kd_produk
         //            ");
	    }
    } //tutup perulangan
    
    return $query;
  }

  	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);
		$result= _QMS_insert('UNIT_ASAL', $data);
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}
        return $strError;
	}

	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result= _QMS_insert('unit_asalinap', $data);
		

		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}

}