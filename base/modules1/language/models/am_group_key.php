<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_group_key extends Model
{

    function am_group_key()
    {
            parent::Model();
            $this->load->database();
    }


    function read($id)
    {
            //$this->db->where('GROUP_KEY_ID', $id);
            //$query = $this->db->get('dbo.AM_GROUP_KEY');
            $this->db->where('group_key_id', $id);
            $query = $this->db->get('am_group_key');

            return $query;
    }

    function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {

        if ($ar_filters != null)
        {
            if (is_array($ar_filters))
            {
                foreach ($ar_filters as $field => $value)
                {
                    $this->db->where(strtolower($field), $value);
                }
            }
        }

        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }

        if ($ar_sort != null)
        {
            if (is_array($ar_sort))
            {
                foreach ($ar_sort as $field) {
                    $this->db->orderby(strtolower($field), $sortdir);
                }
            } else $this->db->orderby(strtolower ($ar_sort), $sortdir);
        }

        //$query = $this->db->get('dbo.AM_GROUP_KEY');
        $query = $this->db->get('am_group_key');

        return $query;
    }


    function readAll()
    {
        //$query = $this->db->get('dbo.AM_GROUP_KEY');
        $query = $this->db->get('am_group_key');

        return $query;
    }

    // akan diganti dengan tblhelper

    function create($data)
    {
        $this->db->set('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
        $this->db->set('GROUP_KEY', $data['GROUP_KEY']);
        $this->db->set('GROUP_KEY_DESC', $data['GROUP_KEY_DESC']);
        $this->db->insert('dbo.AM_GROUP_KEY');

        return $this->db->affected_rows();
    }


    function update($id, $data)
    {
        $this->db->where('GROUP_KEY_ID', $data['GROUP_KEY_ID']);
        $this->db->set('GROUP_KEY', $data['GROUP_KEY']);
        $this->db->set('GROUP_KEY_DESC', $data['GROUP_KEY_DESC']);
        $this->db->update('dbo.AM_GROUP_KEY');

        return $this->db->affected_rows();
    }

    function delete($id)
    {
        $this->db->where('GROUP_KEY_ID', $id);
        $this->db->delete('dbo.AM_GROUP_KEY');

        return $this->db->affected_rows();
    }

}


?>