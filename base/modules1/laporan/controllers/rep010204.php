<?php
 /**
 * @author HDHT
 * @copyright 2015
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class rep010204 extends ReportClass
{

     function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
     {
        $lng=new GlobalVarLanguageReport;
        $lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCMRequest());
        foreach($lang[0] as $ln)
        {
                $Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
        }
        
        
        $Sql=$this->db;
        
        //mengambil kd_pasien dan kd_unit dari $param kiriman extjs
        $tmpmedrec = substr($param, 12, 10);
        $tmpkdunit = substr($param, 37, 3);
        //----------------------------------------------------------------------
        
        //pancarian laporan berdasarkan parameter yang sudah di tentukan
        $Sql->where('kd_pasien = '."'".$tmpmedrec."'");
        $Sql->where('kd_unit = '."'".$tmpkdunit."'");
        //----------------------------------------------------------------------
        
              
        $this->load->model('laporan/tblprintstatus');
        $res=$this->tblprintstatus->GetRowList($VConn, $param, $Skip  ,$Take   ,$SortDir, $Sort);

        if ($res[1]!=='0') {
        $Rtpparams['rptfile']=Setting::ReportFilePath().'PrintStatus';
        $Rtpparams['tmpfile']= time().'Tmp.pdf';
        $Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
        $Rtpparams['dest']='F';
        $Rtpparams['RS']=Setting::Comname();
        $Rtpparams['Medrec']=$tmpmedrec;
        $Rtpparams['namapasien']=$res[0][0]->NAMA;
        $Rtpparams['umur']=$res[0][0]->HARI;
        $Rtpparams['jk']=$res[0][0]->JENIS_KELAMIN;
        $Rtpparams['pekerjaan']=$res[0][0]->PEKERJAAN;
        $Rtpparams['alamat']=$res[0][0]->ALAMATLKP;
        $Rtpparams['tgllahir']=$res[0][0]->TGL_LAHIR;
        $Rtpparams['agama']=$res[0][0]->AGAMA;
        $Rtpparams['dokterjaga']=$res[0][0]->DOKTERJAGA;
        $Rtpparams['namapj']=$res[0][0]->NAMA_PJ;
        $Rtpparams['pekerjaanpj']=$res[0][0]->PEKERJAANPENANGGUNGJAWAB;
        $Rtpparams['tlppj']=$res[0][0]->TELEPONPENANGGUNGJAWAB;
        $Rtpparams['alamatpj']=$res[0][0]->ALAMATPENANGGUNGJAWAB;
        $rdl = new RDLPdf;
        $rdl->pathFile=Setting::ReportFilePath();
        $RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
        
        $RetVal[0]='Contoh 1';
        $RetVal[1]='PHPRdl1';
        $RetVal[2]=$Rtpparams['name'];

        $res= '{ success : true, msg : "", id : "1010204", title : "Laporan Pasien", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
        }
        else
        {
            $res= '{ success : false, msg : "No Record Found"}';
        }
        
        return $res;
    }
    
}
?>