<?php

/**
 * @author Fully
 * @copyright 2010
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep910005 extends ReportClass
{
	

	function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	{
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportWOPMForm()); 
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		$Split = explode("##@@##", $param,  4);
		 
		$Sql=$this->db;
		/// parsing parameternya
		
		$Split = explode("###1###", $param); //Split(param, "###1###", 2)
                $strAsset = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###2###", $param); //Split(strTmp, "###2###", 2)
                $strProblem = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###3###", $param); //Split(strTmp, "###3###", 2)
                $dTglStart = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###4###", $param,  2); //Split(strTmp, "###4###", 2)
                $strId = $Split[0];
                $criteria = " WHERE WO_PM_ID='".$strId."'";
          
		 
		$this->load->model('laporan/tblvirptworkorderpmform');
	 	$res=$this->tblvirptworkorderpmform->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0') {
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'WorkOrderPMForm';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']=Setting::Comphone();
		$Rtpparams['Asset']= $strAsset;
		$Rtpparams['Problem']= $strProblem;
		$Rtpparams['StartDate']= $dTglStart;
                $Rtpparams['WODate']= $dTglStart;
                $Rtpparams['IdWo']= $dTglStart;
                $Rtpparams['pic']= $dTglStart;
                $Rtpparams['WOFinishDate']= $dTglStart;
                $Rtpparams['Notes']=  '';
		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);

		$res= '{ success : true, msg : "", id : "910005", title : "Work Order Maintenance", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';}
                else
                {$res= '{ success : false, msg : "No Records Found !!"}';}
	 	return $res;

	 }


}


?>