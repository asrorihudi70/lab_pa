<?php

/**
 * @author Fully
 * @copyright 2009
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep920003 extends ReportClass
{
	

	 function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	 {
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportScheduleCM()); 
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		
		$Sql=$this->db;
		/// parsing parameternya
		$Split = explode("##@@##", $param,  4);
		if (count($Split) > 0 )
		{
            if (count($Split) > 2 )
			{
				$tgl = $Split[0];
        		$tgl2 = $Split[1];
                $Sql->where('due_date >=',$tgl);
                $Sql->where('due_date <=',$tgl2);
                if ($Split[2] === "" Or $Split[2]=== "xxx")
				{
                    $strDepartment = "Semua Department";
				}
                else
                {    
                    $Sql->where('dept_id',$Split[2]);
                    $strDepartment = "Department : (" .$Split[2].") ".$Split[3];
				}
            }
            else
            {   
                $strDepartment = "Semua Department";
            }
             
        }
        if ($tgl === $tgl2 )
		{
			$strTahun = "Periode  :  ".$tgl;
		}
		else
		{
			$strTahun = "Periode  :  ".$tgl."  s/d  ".$tgl2;
		}
          		
		$this->load->model('laporan/tblvirptschedulecm');
	 	$res=$this->tblvirptschedulecm->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0') {
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'ScheduleCM';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']=Setting::Comphone();
		$Rtpparams['tgl']= $strTahun;

		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);

		$res= '{ success : true, msg : "", id : "920003", title : "Corrective Maintenace Schedule", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';

                }
                else
                {
                    $res= '{ success : false, msg : "No Record Found"}';
                }
                echo $res;

	 }


}


?>