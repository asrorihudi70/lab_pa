<?php

/**
 * @author Fully
 * @copyright 2009
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep920007 extends ReportClass
{
	

	 function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	 {
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCMRequestForm()); 
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		
		
		$Sql=$this->db;
		/// parsing parameternya
		$Split = explode("###1###", $param,  2);


                $strNoRequest = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###2###", $strTmp,  2);

                $dTgl = $Split[0];
                $strTmp =$Split[1];
                $Split = explode("###3###", $strTmp,  2);
               // mSplit = Split(strTmp, "###3###", 2)
                $strRequester = $Split[0];
                $strTmp = $Split[1];
                $Split = explode("###4###", $strTmp,  2);
                $strDept =$Split[0];
                //criteria = " WHERE REQ_ID='" & strNoRequest & "'"

		$Sql->where('req_id',$strNoRequest);
		$this->load->model('laporan/tblvirptrequestcm');
	 	$res=$this->tblvirptrequestcm->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0')
		{
		
			$Rtpparams['rptfile']=Setting::ReportFilePath().'RequestCMForm';
			$Rtpparams['tmpfile']= time().'Tmp.pdf';
			$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
			$Rtpparams['dest']='F';
			$Rtpparams['COMPANY']=Setting::Comname();
			$Rtpparams['ADDRESS']=Setting::Comaddress();
			$Rtpparams['TELP']=Setting::Comphone();
			$Rtpparams['RequestID']= $strNoRequest;
			$Rtpparams['RequestDate']= $dTgl;
			$Rtpparams['Requester']=$strRequester;
			$Rtpparams['Department']= $strDept;
			
			$rdl = new RDLPdf;
			$rdl->pathFile=Setting::ReportFilePath();
			$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
			$RetVal[0]='Contoh 1';
			$RetVal[1]='PHPRdl1';
			$RetVal[2]=$Rtpparams['name'];
			$res= '{ success : true, msg : "", id : "920001", title : "Biodata Pegawai", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
                }
                else
                {
                    $res= '{ success : false, msg : "No Record Found"}';
                }
                echo $res;

	 }


}


?>