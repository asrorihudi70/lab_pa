<?php
/**
 * @author Fully
 * @copyright 2009
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep920008 extends ReportClass
{
	
	 function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	 {
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCMApprovalForm());
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		$Sql=$this->db;
		
		$this->load->model('laporan/tblvirptapproval');
		$crit=str_replace('~',"'",$param);
		$this->db->where($crit);
	 	$res=$this->tblvirptapproval->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0')
                {
                    $Rtpparams['rptfile']=Setting::ReportFilePath().'ApprovalForm';
                    $Rtpparams['tmpfile']= time().'Tmp.pdf';
                    $Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
                    $Rtpparams['dest']='F';
                    $Rtpparams['COMPANY']=Setting::Comname();
                    $Rtpparams['ADDRESS']=Setting::Comaddress();
                    $Rtpparams['TELP']=Setting::Comphone();

                    $rdl = new RDLPdf;
                    $rdl->pathFile=Setting::ReportFilePath();
                    $RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);
                    $RetVal[0]='Contoh 1';
                    $RetVal[1]='PHPRdl1';
                    $RetVal[2]=$Rtpparams['name'];
                    $res= '{ success : true, msg : "", id : "920008", title : "Approval", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
                }
                else
                {
                    $res= '{ success : false, msg : "Records Not Found" }';
                }
	 	return $res;

	 }


}


?>