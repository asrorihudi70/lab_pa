<?php

/**
 * @author Fully
 * @copyright 2010
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep940007 extends ReportClass
{
	
	function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	{
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCostHistorySumMonitoring());
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		
		
		$Sql=$this->db;
               

                $mSplit = explode("@#@",$param, 2);
                If (count($mSplit) > 0 )
                {
                    $mSplit2 = explode( "##@@##",$mSplit[0], $mSplit[1]);
                    If ($mSplit2[0] === "" || $mSplit2[0] === "xxx" )
                    {
                        //$criteria = "where Years = " & (int)($mSplit2[2]) & " ";

                        $Sql->where('years =',$mSplit2[2],'');
                        $strTahun = "Tahun : ".$mSplit2[2];
                        $strDepartment = "Semua Unit Kerja";
                    }
                    else
                    {
                         $Sql->where('years =',$mSplit2[2],'');
                         $Sql->where('dept_id =',$mSplit2[0],'');
                         //criteria = "where Years = " & CInt(mSplit2(2)) & " and Dept_Id = '" & mSplit2(0) & "'"
                         $strTahun = "Tahun : ".$mSplit2[2];
                        $strDepartment = "Unit Kerja : ".$mSplit2[1]." (".$mSplit2[0].")";
                    }
                }
		
		 
		$this->load->model('laporan/tblvirpthistorycostsummary');
	 	$res=$this->tblvirpthistorycostsummary->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0') {
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'HistoryCostSummary';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']=Setting::Comphone();
		$Rtpparams['tahun']= $strTahun;

		
		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);

		$res= '{ success : true, msg : "", id : "940007", title : "History Cost Summary", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';
                } else {
                    $res= '{ success : false, msg : "No Records Found"}';
                }
	 	return $res;

	 }


}


?>