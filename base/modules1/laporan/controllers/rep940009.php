<?php

/**
 * @author Fully
 * @copyright 2010
 */
//require_once 'AppCommonLib.php';
//require '../OLIB/OLIB.RdlPdf.php';
require 'GlobalVarLanguageReport.php';

class Rep940009 extends ReportClass
{
	

	function CreateReport($VConn, $UserID,$param,$Skip  ,$Take   ,$SortDir, $Sort)
	{
		$lng=new GlobalVarLanguageReport;
		$lang= $lng->getLanguageReport($VConn,GlobalVarLanguageReport::GetArrLangReportCostMaintennaceMonitoringDetail());
		foreach($lang[0] as $ln)
		{
			$Rtpparams[$ln->LIST_KEY]=$ln->LABEL;
		}
		

		$Sql=$this->db;
            $Split = explode("@#@", $param,  2);
            if (count($Split) > 0 )
            {

                $Split2 = explode("##@@##", $Split[0],  $Split[1]);
                $tgl = $Split2[2];
                $tgl2 = $Split2[1];
                $Sql->where($Sql->fnYear('finish_date').'=',$tgl,false );
                //$Sql->where('dates',$tgl2,'<=');
                if ($Split2[1] === "All" )
                {
                $strTahun = "Tahun : ".$Split2[2];
                $strDepartment = "Semua Department";
                }
                else
                {
                $Sql->where('a.dept_id',$Split[1]);
                $strDepartment = "Department : (" .$Split2[1].") ".$Split[1];
                }


            }

      
			$strTahun = "Periode  :  ".$tgl;
		
          
		 
		$this->load->model('laporan/tblvirptcostmaintenancedetil');
	 	$res=$this->tblvirptcostmaintenancedetil->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
		if ($res[1]!=='0') {
	 	$Rtpparams['rptfile']=Setting::ReportFilePath().'BiayaPemeliharaanDetil';
		$Rtpparams['tmpfile']= time().'Tmp.pdf';
	 	$Rtpparams['name']= Setting::TmpPath().$Rtpparams['tmpfile'];
	 	$Rtpparams['dest']='F';
		$Rtpparams['COMPANY']=Setting::Comname();
		$Rtpparams['ADDRESS']=Setting::Comaddress();
		$Rtpparams['TELP']=Setting::Comphone();
		$Rtpparams['tahun']= $strTahun;

		
		$rdl = new RDLPdf;
		$rdl->pathFile=Setting::ReportFilePath();
		$RptEngine=$rdl->CreateReport( $res[0],$Rtpparams['rptfile'],$Rtpparams);

		$res= '{ success : true, msg : "", id : "940009", title : "Maintenance Cost Detail", url : "'.Setting::TmpUrl().$Rtpparams['tmpfile'].'"}';}
                else
                {
                    $res= '{ success : false, msg : "No Records Found"}';
                }
	 	return $res;

	 }


}


?>