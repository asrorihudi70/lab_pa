<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

include_once('PHPJasperXML.php');

class seting
{
    const nmMsgReportFound = "nmMsgReportFound";
    const server="localhost";
    const db="prim";
    const user="postgres";
    const pass="nci";
    const version="0.7c";
    const pgport=5432;
    
}

class rptall extends MX_Controller {

    const nmMsgReportFound = "nmMsgReportFound";
    const server="192.168.0.204";
    const db="prim";
    const user="postgres";
    const pass="nci";
    const version="0.7c";
    const pgport=5432;

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');            		
    }

    //Request CM Form
    public function rpt920007($Params, $UserID)
    {
        
        List($strNoRequest, $strTmp) = explode("###1###", $Params);

        List($dTgl, $strTmp) = explode("###2###", $strTmp);

        List($strRequester, $strTmp) = explode("###3###", $strTmp);

        List($strDept, $strTmp) = explode("###4###", $strTmp);
        
        $strLanguageID= $this->getIDLanguage($UserID);

        $arr = $this->GetArrLangReportCMRequestForm();
        //echo '<script type="text/javascript">alert("'.realpath(APPPATH).'");</script>';

        $mResult = $this->getLanguageReport($arr, $strLanguageID);

        $this->load->model("laporan/virptrequestcmmodel");

        $query = $this->load->virptrequestcmmodel->read("REQ_ID = '".$strNoRequest."'");

        $strTitle = "";
        
        if ($query->num_rows() > 0)
	{
            // coba load library nya apa bisa dari libraries atau emang hrs direct
            //
            //include_once('fpdf.php');
            //include_once('PHPJasperXML.inc.php');

            $xml =  simplexml_load_file(base_url()."rpt/RequestCMForm.jrxml");

            //echo '<script type="text/javascript">alert("'.$xml.'");</script>';

            //$this->load->library('PHPJasperXML');
            $PHPJasperXML = new PHPJasperXML();
            
            //$this->PHPJasperXML->debugsql= false;
            $PHPJasperXML->debugsql = false;


            $arrCompany = $this->getCompany();

            if (count($mResult)>0)
            {
                $strTitle = $mResult["nmHeaderCMRequestForm"]; //$mResult["LABEL"];
            }
            
            $PHPJasperXML->arrayParameter = array("COMPANY"=>$arrCompany["COMPANY"],
                "ADDRESS"=>$arrCompany["ADDRESS"], "TELP"=>$arrCompany["TELP"],
                "nmHeaderCMRequestForm"=>$mResult["nmHeaderCMRequestForm"],
                "nmRequestID"=>$mResult["nmReqIDCMReqForm"],
                "nmRequesterCMReqForm"=>$mResult["nmRequesterCMReqForm"],
                "nmReqDateCMReqForm"=>$mResult["nmReqDateCMReqForm"],
                "nmDeptCMReqForm"=>$mResult["nmDeptCMReqForm"],
                "RequestID"=>$strNoRequest,
                "Requester"=>$strRequester,
                "RequestDate"=>$dTgl,
                "Department"=>$strDept,
                "nmAssetCodeCMReqForm"=>$mResult["nmAssetCodeCMReqForm"],
                "nmAssetNameCMReqForm"=>$mResult["nmAssetNameCMReqForm"],
                "nmProblemCMReqForm"=>$mResult["nmProblemCMReqForm"],
                "nmDescCMReqForm"=>$mResult["nmDescCMReqForm"],
                "nmTargetDateCMReqForm"=>$mResult["nmTargetDateCMReqForm"],
                "nmImpactCMReqForm"=>$mResult["nmImpactCMReqForm"],
                "strNoRequest"=>$strNoRequest);

            //echo '<script type="text/javascript">alert("'.$PHPJasperXML->arrayParameter["COMPANY"].'");</script>';
            
            $PHPJasperXML->xml_dismantle($xml);

            $ff= new seting();            
            $transferDBtoArray = $PHPJasperXML->transferDBtoArray($ff::server, $ff::user, $ff::pass, $ff::db, "psql");

            //$this->PHPJasperXML->transferDBtoArray($server, $user, $password, $db, "psql");

            //$PHPJasperXML->outpage("I", base_url().'report/RequestCMForm.pdf');    //page output method I:standard output  D:Download file
            //
            
            $dpath= realpath(APPPATH);
            $PHPJasperXML->outpage("F", $dpath.'\report\RequestCMForm.pdf');
            //$PHPJasperXML->outpage("F", "RequestCMForm.pdf");
            //$PHPJasperXML->outpage("D");

            //$this->PHPJasperXML->outpage("I",base_url().'report/RequestCMForm.pdf');

            //Dim res = New With {.success = True, .msg = "", .id = "920007", .title = strTitle, .url = "./Report/" & "RequestCMForm.pdf"}
            echo '{success: true, id: "920007", title: "'.$strTitle.'", url: "'.base_url().'base/report/RequestCMForm.pdf"}';

        } else {

            if (count($mResult)>0)
            {
                $strTitle = $mResult["LABEL"];
            }

            echo '{success: false, msg: "'.$this->getReportNotFound($strLanguageID).'", id: "", title: "'.$strTitle.'", url: ""}';
            //Dim res = New With {.success = False, .msg = mBahasa.getReportNotFound(), .id = "", .title = strTitle, .url = "" & ""}
        }

        
    }

    public function rpt950012()
    {
        
    }

    public function rpt920001($Params)
    {

    }

    private function getIDLanguage($Kd_User)
    {

    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();						

        $strIDLanguage="";

        if ($query->num_rows() > 0)	
        {				
            $row = $query->row();
            $strIDLanguage=$row->LANGUAGE_ID;
        }

        return $strIDLanguage;
    }

    private function getCompany() //index()// read()
    {

    	$this->load->model('setup/am_company');
	$query = $this->am_company->readAll();

        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            $arr = array("COMPANY"=>$row->COMP_NAME,
                "ADDRESS"=>$row->COMP_ADDRESS.' '.$row->COMP_CITY.' '.$row->COMP_POS_CODE,
                "TELP"=>'Telp. '.$row->COMP_TELP.' Fax. '.$row->COMP_FAX);
            
        } else $arr = array("COMPANY"=>"PT. NCI","ADDRESS"=>"Pelajar Pjuang 45","TELP"=>"022 7317077");

        return $arr;

    }

    private function getLanguageReport($arr, $strLanguageID)
    {               
        $strList="";

        for ($i=0;$i<count($arr);$i+=1)
        {
            if ($i == count($arr)-1)
            {
                $strList .= "'".$arr[$i]."'";
            } else $strList .= "'".$arr[$i]."', ";
        }

        $strCriteria = "\"LANGUAGE_ID\" = '".$strLanguageID."'";

        if ($strList <> "")
            $strCriteria .= " AND \"LIST_KEY\" IN ( ".$strList." )";
        
        $mListLanguange=array();

        // ambil dulu model vilanguagreport
        $this->load->model('laporan/vilanguagereportmodel');

        $query = $this->load->vilanguagereportmodel->read($strCriteria);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row)
            {
                //$mListLanguange[]=$row;
                $mListLanguange[$row['LIST_KEY']]= $row['LABEL'];
                //$mListLanguange= array($row['LIST_KEY']=>$row['LABEL']);
                
            }
            //echo '<script type="text/javascript">alert("'.$mListLanguange["nmHeaderCMRequestForm"].'");</script>';
            
            
        }
         
        return $mListLanguange;

    }

    private function getReportNotFound($strLanguageID)
    {

        $strParams="LANGUAGE_ID = '".strLanguageID."' AND LIST_KEY = '".$nmMsgReportFound."'";

        $this->load->model('main/vilanguage');
        $query=$this->load->vilanguage->readParam($strParams);

        $retVal="";

        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            $retVal=$row->LABEL;
        }

        return $retVal;
    }


    private function GetArrLangReportSetLoc()
    {
        return array("nmHeaderSetLoc","nmIDSetLoc","nmLocationSetLoc");
    }

    private function GetArrLangReportSetDept()
    {
        return array("nmHeaderSetDept","nmIDSetDept","nmDeptSetDept");
    }

    private function GetArrLangReportSetCat()
    {
        return array("nmHeaderSetCat","nmIDSetCat","nmCatSetCat");

    }

    private function GetArrLangReportSetAset()
    {
        return array("nmHeaderSetAset","nmDepSetAset","nmIDSetAset",
            "nmNameSetAset","nmCatSetAset","nmLocSetAset","nmYearSetAset",
            "nmDescSetAset");        
    }

    private function GetArrLangReportSetPart()
    {
        return array("nmHeaderSetPart","nmNumberSetPart","nmNameSetPart",
            "nmPriceSetPart","nmDescSetPart");
    }    

    private function GetArrLangReportSetEmp()
    {
        return array("nmHeaderSetEmp","nmIDSetEmp","nmNameSetEmp",
            "nmAddressSetEmp","nmCitySetEmp","nmPosCodeSetEmp",
            "nmStateSetEmp","nmPhone1SetEmp","nmPhone2SetEmp",
            "nmEmailSetEmp");        
    }

    private function GetArrLangReportSetVend()
    {
        return array("nmHeaderSetVend","nmIDSetVend","nmNameSetVend",
            "nmContact1SetVend","nmContact2SetVend","nmAddressSetVend",
            "nmCitySetVend","nmPosCodeSetVend","nmCountrySetVend",
            "nmPhone1SetVend","nmPhone2SetVend");
    }

    private function GetArrLangReportSetService()
    {
        return array("nmHeaderSetServ","nmIDSetServ","nmNameSetServ");        
    }    

    private function GetArrLangReportCMRequestForm()
    {
        return array("nmHeaderCMRequestForm","nmReqIDCMReqForm",
            "nmRequesterCMReqForm","nmReqDateCMReqForm","nmDeptCMReqForm",
            "nmAssetCodeCMReqForm","nmAssetNameCMReqForm","nmProblemCMReqForm",
            "nmDescCMReqForm","nmTargetDateCMReqForm","nmImpactCMReqForm");        
    }

    private function GetArrLangReportCMRequest()
    {
        return array("nmHeaderCMReq","nmReqDateCMReq","nmRequsterCMReq",
            "nmAssetCodeCMReq","nmAssetNameCMReq","nmProblemCMReq",
            "nmDescCMReq","nmTargetDateCMReq","nmGroupDeptCMReq");
    }

    private function GetArrLangReportCMApprovalForm()
    {
        return array("nmHeaderAppFrom","nmHeaderReqInfoAppForm",
            "nmReqIDAppForm","nmRequesterAppForm","nmReqDateAppForm",
            "nmDeptAppForm","nmHeaderAsmaInfoAppForm","nmAsMaAppForm",
            "nmProblemAppForm","nmLocationAppForm","nmTargetDateAppForm",
            "nmHeaderAppAppForm","nmApproverAppForm","nmRepairAppForm",
            "nmVendorAppForm","nmCostRateAppForm","nmStartDateAppForm",
            "nmNotesAppForm","nmStatusAppForm","nmStatusDescAppForm",
            "nmFinishDateAppForm");
    }

    private function GetArrLangReportCMApproval()
    {
        return array("nmHeaderApp","nmAsMaApp","nmProblemApp",
            "nmRequesterApp","nmTargetDateApp","nmStatusApp",
            "nmApproverApp");
    }

    private function  GetArrLangReportScheduleCM()
    {
        return array("nmHeaderCMSchedule","nmRepairCMSchedule",
            "nmDescCMSchedule","nmRequesterCMSchedule",
            "nmDueDateCMSchedule","nmGroupDeptCMSchedule",
            "nmGroupAssetCMSchedule");
    }

    private function GetArrLangReportWOCM()
    {
        return array("nmHeaderWOCM","nmDeptWOCM","nmIDWOCM",
            "nmAsetWOCM","nmDateIssuedWOCM","nmDateCompWOCM",
            "nmVendorWOCM","nmSupervisorWOCM");        
    }
    
    private function GetArrLangReportWOCMForm()
    {
        return array("nmHeaderWOCMForm","nmSchInfoWOCMForm",
            "nmWOInfoWOCMForm","nmServiceWOCMForm","nmPersonWOCMForm",
            "nmCostWOCMForm","nmPartWOCMForm","nmQtyWOCMForm",
            "nmUnitCostWOCMForm","nmTotCostCMForm","nmDescWOCMForm",
            "nmNotesWOCMForm","nmAsetWOCMForm","nmProblemWOCMForm",
            "nmStartDateSchWOCMForm","nmFinishDateSchWOCMForm",
            "nmIDWOCMForm","nmDateWOCMForm","nmFinishDateWOCMForm",
            "nmPICWOCMForm");
    }

    private function GetArrLangReportResultCM()
    {
        return array("nmHeaderCMJobClose","nmGroupDeptCMJobClose",
            "nmIDCMJobClose","nmFinishDateCMJobClose","nmServiceCMJobClose",
            "nmReffCMJobClose","nmCostCMJobClose","nmAssetCMJobClose");
    }

    private function GetArrLangReportResultCMForm()
    {
        return array("nmHeaderResultCMForm","nmSchInfoResultCMForm",
            "nmWOInfoResultCMForm","nmResultInfoResultCMForm",
            "nmServiceResultCMForm","nmPersonResultCMForm",
            "nmCostResultCMForm","nmPartResultCMForm",
            "nmQtyResultCMForm","nmUnitCostResultCMForm",
            "nmTotCostResultCMForm","nmDescResultCMForm",
            "nmNotesResultCMForm","nmAsetResultCMForm",
            "nmStartDateSchResultCMForm","nmWODateResultCMForm",
            "nmFinishDateResultCMForm","nmPICResultCMForm",
            "nmFinalCostResultCMForm");
    }

    private function GetArrLangReportHistoryCM()
    {
        return array("nmHeaderCMHistory","nmDeptCMHistory",
            "nmAsetCMHistory","nmDateCMHistory","nmServiceCMHistory",
            "nmCostCMHistory");
    }

    private function GetArrLangReportSchedulePM()
    {
        return array("nmHeaderPMSchedule","nmGroupDeptPMSchedule",
            "nmGroupAssetPMSchedule","nmServicePMSchedule",
            "nmDescPMSchedule","nmDueDatePMSchedule");        
    }

    private function GetArrLangReportWOPM()
    {
        return array("nmHeaderWOPM","nmDeptWOPM","nmIDWOPM",
            "nmAsetWOPM","nmDateIssuedWOPM","nmDateFinishWOPM",
            "nmVendorWOPM","nmSupervisorWOPM");
    }

    private function GetArrLangReportWOPMForm()
    {
        return array("nmHeaderWOPMForm","nmSchInfoWOPMForm",
            "nmWOInfoWOPMForm","nmServiceWOPMForm","nmPersonWOPMForm",
            "nmCostWOPMForm","nmPartWOPMForm","nmQtyWOPMForm",
            "nmUnitCostWOPMForm","nmTotCostWOPMForm","nmDescWOPMForm",
            "nmNotesWOPMForm","nmAsetWOPMForm","nmProblemWOPMForm",
            "nmStartDateSchWOPMForm","nmFinishDateSchWOPMForm",
            "nmIDWOPMForm","nmDateWOPMForm","nmFinishDateWOPMForm",
            "nmPICWOPMForm");
    }

    private function GetArrLangReportResultPM()
    {
        return array("nmHeaderResultPM","nmDeptResultPM","nmIDResultPM",
            "nmAsetResultPM","nmDateFinishResultPM","nmServiceResultPM",
            "nmReffResultPM","nmCostResultPM");        
    }

    private function GetArrLangReportResultPMForm()
    {
        return array("nmHeaderResultPMForm","nmSchInfoResultPMForm",
            "nmWOInfoResultPMForm","nmResultInfoResultPMForm",
            "nmServiceResultPMForm","nmPersonResultPMForm",
            "nmCostResultPMForm","nmPartResultPMForm","nmQtyResultPMForm",
            "nmUnitCostResultPMForm","nmTotCostResultPMForm",
            "nmDescResultPMForm","nmNotesResultPMForm","nmAsetResultPMForm",
            "nmStartDateSchResultPMForm","nmWODateResultPMForm",
            "nmFinishDateResultPMForm","nmPICResultPMForm",
            "nmFinalCostResultPMForm");
    }

    private function GetArrLangReportHistoryPM()
    {
        return array("nmHeaderPMHistory", "nmDeptPMHistory","nmAsetPMHistory",
            "nmDatePMHistory","nmServicePMHistory","nmCostPMHistory");
    }

    private function GetArrLangReportCostHistoryMonitoring()
    {
        return array("nmHeaderRptCostHistory","nmCatRptCostHistory",
            "nmDeptRptCostHistory","nmAsetRptCostHistory",
            "nmBlnMayRptCostHistory","nmBlnAugRptCostHistory",
            "nmBlnOctRptCostHistory","nmBlnNovRptCostHistory",
            "nmBlnDecRptCostHistory");        
    }

    private function  GetArrLangReportPartHistoryMonitoring()
    {
        return array("nmHeaderRptPartHistory","nmDeptRptPartHistory",
            "nmAsetRptPartHistory","nmDateRptPartHistory",
            "nmPartsRptPartHistory","nmQtyRptPartHistory",
            "nmUnitCostRptPartHistory","nmTotCostRptPartHistory");
    }

    private function  GetArrLangReportCostHistorySumMonitoring()
    {
        return array("nmHeaderRptCostHistorySum","nmDeptRptCostHistorySum",
            "nmAsetRptCostHistorySum","nmPMRptCostHistorySum",
            "nmCMRptCostHistorySum","nmPartRptCostHistorySum",
            "nmLaborRptCostHistorySum","nmTotalRptCostHistorySum");
    }

    private function  GetArrLangReportCostMaintennaceMonitoring()
    {
        return array("nmHeaderRptBiayaPemeliharaan","nmDeptRptBiayaPemeliharaan");        
    }

    private function  GetArrLangReportCostMaintennaceMonitoringDetail()
    {
        return array("nmHeaderRptBiayaPemeliharaanDtl",
            "nmDeptRptBiayaPemeliharaanDtl","nmCatRptBiayaPemeliharaanDtl");
    }
}
?>