﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirpthistorycm extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virpthistorycm';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="department,asset,datecm,services,totcost,dept_id,asset_maint_id"  
;
$this->SqlQuery=" select * from ( select     d.dept_name as department, a.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, s.sch_due_date as datecm, 
                      rcd.problem as services, r.last_cost as totcost, d.dept_id, a.asset_maint_id
from          am_result_cm as r inner join
                       am_work_order_cm as w on w.wo_cm_id = r.wo_cm_id inner join
                       am_schedule_cm as s on s.sch_cm_id = w.sch_cm_id inner join
                       am_approve_cm as ap on ap.app_id = s.app_id inner join
                       am_request_cm_detail as rcd on rcd.req_id = ap.req_id and rcd.row_req = ap.row_req inner join
                       am_asset_maint as a on a.asset_maint_id = rcd.asset_maint_id inner join
                       am_department as d on d.dept_id = a.dept_id
group by d.dept_name, a.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name, s.sch_due_date, rcd.problem, r.last_cost, d.dept_id, a.
asset_maint_id ) as resdata ";
		
	}

	
	function FillRow($rec)
	{
		$row=new Rowvirpthistorycm;
				$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		if ($rec->datecm!==null) 
		{
		$dt=new DateTime($rec->datecm);					
		$row->DATECM=$dt->format('Ymd'); } 
		 else 
		{ $row->DATECM='00010101'; } 
		$row->SERVICES=$rec->services;
		$row->TOTCOST=$rec->totcost;
		$row->DEPT_ID=$rec->dept_id;
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;

		return $row;
	}
}
class Rowvirpthistorycm
{
	public $DEPARTMENT;
public $ASSET;
public $DATECM;
public $SERVICES;
public $TOTCOST;
public $DEPT_ID;
public $ASSET_MAINT_ID;

}

?>