﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirpthistorypm extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="department,asset,datepm,services,totcost,dept_id,asset_maint_id"  ;
		$this->TblName='virpthistorypm';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="  select     d.dept_name as department, a.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, rp.finish_date as datepm, 
		am_service.service_name as services, rp.last_cost as totcost, d.dept_id, a.asset_maint_id
		from          am_result_pm_service inner join
		am_result_pm as rp inner join
		am_work_order_pm as wo on rp.wo_pm_id = wo.wo_pm_id on 
		am_result_pm_service.result_pm_id = rp.result_pm_id inner join
		am_service on  am_result_pm_service.service_id =  am_service.service_id inner join
		am_wo_pm_service on wo.wo_pm_id =  am_wo_pm_service.wo_pm_id inner join
		am_schedule_pm as sp inner join
		am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id inner join
		am_asset_maint as a on sp.asset_maint_id = a.asset_maint_id inner join
		am_department as d on a.dept_id = d.dept_id on  am_wo_pm_service.sch_pm_id = spd.sch_pm_id and 
		am_wo_pm_service.category_id = spd.category_id and  am_wo_pm_service.service_id = spd.service_id and 
		am_wo_pm_service.row_sch = spd.row_sch #where#
		group by d.dept_name, a.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name, rp.finish_date,  am_service.service_name, rp.last_cost, d.
		dept_id, a.asset_maint_id ";

		
	}


	function FillRow($rec)
	{
		$row=new Rowvirpthistorypm;
		$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		if ($rec->datepm!==null) 
		{ 
		$dt=new DateTime($rec->datepm);			
		$row->DATEPM=$dt->format('Ymd'); } 
		 else 
		{ $row->DATEPM='00010101'; } 
		$row->SERVICES=$rec->services;
		$row->TOTCOST=$rec->totcost;
		$row->DEPT_ID=$rec->dept_id;
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;

		return $row;
	}
}
class Rowvirpthistorypm
{
	public $DEPARTMENT;
public $ASSET;
public $DATEPM;
public $SERVICES;
public $TOTCOST;
public $DEPT_ID;
public $ASSET_MAINT_ID;

}

?>