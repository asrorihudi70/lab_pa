﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptjobclosecm extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptjobclosecm';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="result_cm_id,finish_date,asset,services,reference,last_cost,category_id,dept_name,dept_id" ;
		$this->SqlQuery=" select * from (select  d.dept_name, d.dept_id,r.result_cm_id, r.finish_date, 
		rcd.asset_maint_id ".$this->SQL->OPJoinStr()."' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset,  
		getallserviceresultcm(r.result_cm_id) as services, r.reference, r.last_cost, a.category_id
		from am_result_cm as r inner join
		am_work_order_cm as w on r.wo_cm_id = w.wo_cm_id inner join
		am_schedule_cm as sc on w.sch_cm_id = sc.sch_cm_id inner join
		am_approve_cm as app on sc.app_id = app.app_id inner join
		am_request_cm_detail as rcd on app.req_id = rcd.req_id and app.row_req = rcd.row_req inner join
		am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
		am_department as d on a.dept_id = d.dept_id
		group by  d.dept_id,d.dept_name, r.result_cm_id, r.finish_date, r.last_cost, r.reference, 
		rcd.asset_maint_id, a.asset_maint_name, a.category_id
		order by  d.dept_id,d.dept_name ) as resdata ";
		
	}

	function SaveData( $DBConn, array $values)
	{
		
		return TblHelper::SaveData($DBConn,$values);
		
	}
	
	function UpdateData( $DBConn, array $values, $criteria='')
	{
		
		return TblHelper::UpdateData($DBConn,$values,$criteria);
		
	}
	function FillRow($rec)
	{
		$row=new Rowvirptjobclosecm;
				$row->RESULT_CM_ID=$rec->result_cm_id;
		if ($rec->finish_date!==null) 
		{
		$dt=new DateTime($rec->finish_date);					
		$row->FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->FINISH_DATE='00010101'; } 
		$row->ASSET=$rec->asset;
		$row->SERVICES=$rec->services;
		$row->REFERENCE=$rec->reference;
		$row->LAST_COST=$rec->last_cost;
		$row->CATEGORY_ID=$rec->category_id;
		$row->DEPT_NAME=$rec->dept_name;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirptjobclosecm
{
	public $RESULT_CM_ID;
public $FINISH_DATE;
public $ASSET;
public $SERVICES;
public $REFERENCE;
public $LAST_COST;
public $CATEGORY_ID;
public $DEPT_NAME;
public $DEPT_ID;

}

?>