﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptrequestcm extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptrequestcm';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="department,tgl_request,requester,asset_code,asset_name,problem,description,target_date,dept_id,emp_id,impact,desc_status,status_name,status_id,req_id,row_req"  
;
		$this->SqlQuery="select * from ( select     d.dept_name as department, rc.req_date as tgl_request, e.emp_name as requester, rcd.asset_maint_id as asset_code, 
             a.asset_maint_name as asset_name, rcd.problem, rcd.desc_req as description, rcd.req_finish_date as 
			target_date, d.dept_id, 
                      rc.emp_id, rcd.impact, rcd.desc_status,  am_status.status_name,  am_status.status_id, rcd.req_id, rcd.row_req
from          am_request_cm as rc inner join
                       am_request_cm_detail as rcd on rc.req_id = rcd.req_id inner join
                       am_department as d on rc.dept_id = d.dept_id inner join
                       am_employees as e on rc.emp_id = e.emp_id inner join
                       am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
                       am_status on rcd.status_id =  am_status.status_id) as resdata ";
		
	}

	
	function FillRow($rec)
	{
		$row=new Rowvirptrequestcm;
				$row->DEPARTMENT=$rec->department;
		if ($rec->tgl_request!==null) 
		{
		$dt=new DateTime($rec->tgl_request);		
		$row->TGL_REQUEST=$dt->format('Ymd'); } 
		 else 
		{ $row->TGL_REQUEST='00010101'; } 
		$row->REQUESTER=$rec->requester;
		$row->ASSET_CODE=$rec->asset_code;
		$row->ASSET_NAME=$rec->asset_name;
		$row->PROBLEM=$rec->problem;
		$row->DESCRIPTION=$rec->description;
		if ($rec->target_date!==null) 
		{ 
		$dt=new DateTime($rec->target_date);
		$row->TARGET_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->TARGET_DATE='00010101'; } 
		$row->DEPT_ID=$rec->dept_id;
		$row->EMP_ID=$rec->emp_id;
		$row->IMPACT=$rec->impact;
		$row->DESC_STATUS=$rec->desc_status;
		$row->STATUS_NAME=$rec->status_name;
		$row->STATUS_ID=$rec->status_id;
		$row->REQ_ID=$rec->req_id;
		$row->ROW_REQ=$rec->row_req;

		return $row;
	}
}
class Rowvirptrequestcm
{
	public $DEPARTMENT;
public $TGL_REQUEST;
public $REQUESTER;
public $ASSET_CODE;
public $ASSET_NAME;
public $PROBLEM;
public $DESCRIPTION;
public $TARGET_DATE;
public $DEPT_ID;
public $EMP_ID;
public $IMPACT;
public $DESC_STATUS;
public $STATUS_NAME;
public $STATUS_ID;
public $REQ_ID;
public $ROW_REQ;

}

?>