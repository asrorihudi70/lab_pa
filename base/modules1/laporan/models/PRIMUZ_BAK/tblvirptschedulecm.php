﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptschedulecm extends TblBase
{
	
	function __construct()
	{	
		$this->TblName='virptschedulecm';
		TblBase::TblBase(true);
		$SQL=$this->db;
		$this->StrSql="years, department, asset, repair, description,requester, due_date, dept_id";
		$this->SqlQuery=" select * from (
		select ".$SQL->fnYear("sc.sch_due_date")." as years, d.dept_name as department, a.asset_maint_id ".$SQL->OPJoinStr()." ' - ' ".$SQL->OPJoinStr()." 
		a.asset_maint_name as asset, rcd.problem as repair, rcd.desc_req as description, am_employees.emp_name as requester,
		sc.sch_due_date as due_date, d.dept_id
		from am_schedule_cm as sc inner join
		am_approve_cm as ac on sc.app_id = ac.app_id inner join
		am_request_cm_detail as rcd on ac.req_id = rcd.req_id inner join
		am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
		am_department as d on a.dept_id = d.dept_id inner join
		am_employees on sc.emp_id =  am_employees.emp_id ) as resdata ";
		
	}


	function FillRow($rec)
	{
		$row=new Rowvirptschedulecm;
				$row->YEARS=$rec->years;
		$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		$row->REPAIR=$rec->repair;
		$row->DESCRIPTION=$rec->description;
		$row->REQUESTER=$rec->requester;
		if ($rec->due_date!==null) 
		{
		$dt=new DateTime($rec->due_date);		
		$row->DUE_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->DUE_DATE='00010101'; } 
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirptschedulecm
{
	public $YEARS;
public $DEPARTMENT;
public $ASSET;
public $REPAIR;
public $DESCRIPTION;
public $REQUESTER;
public $DUE_DATE;
public $DEPT_ID;

}

?>