﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptworkorderpmform extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="wo_pm_id,service_name,person,costperson,part_name,qty,unit_cost,tot_cost,instruction,wo_pm_date,wo_pm_finish_date,desc_wo_pm,emp_name"  
;
		$this->TblName='virptworkorderpmform';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery=" select   * from (select  wo.wo_pm_id, sv.service_name,  getallpersonservicewopm(wo.wo_pm_id, wops.sch_pm_id, sv.service_id, wops.category_id, 
                      wops.row_sch) as person,  getallcostpersonservicewopm(wo.wo_pm_id, wops.sch_pm_id, sv.service_id, wops.category_id, 
                      wops.row_sch) as costperson, sp.part_name, svp.qty, svp.unit_cost, svp.tot_cost, svp.instruction, wo.wo_pm_date, 
                      wo.wo_pm_finish_date, wo.desc_wo_pm, e.emp_name
from          am_employees as e inner join
                       am_work_order_pm as wo inner join
                       am_wo_pm_service as wops on wo.wo_pm_id = wops.wo_pm_id inner join
                       am_service as sv on wops.service_id = sv.service_id on e.emp_id = wo.emp_id left outer join
                       am_spareparts as sp right outer join
                       am_service_parts as svp on sp.part_id = svp.part_id on wops.category_id = svp.category_id and 
                      wops.service_id = svp.service_id ) as resdata ";
		
	}


	function FillRow($rec)
	{
		$row=new Rowvirptworkorderpmform;
				$row->WO_PM_ID=$rec->wo_pm_id;
		$row->SERVICE_NAME=$rec->service_name;
		$row->PERSON=$rec->person;
		$row->COSTPERSON=$rec->costperson;
		$row->PART_NAME=$rec->part_name;
		$row->QTY=$rec->qty;
		$row->UNIT_COST=$rec->unit_cost;
		$row->TOT_COST=$rec->tot_cost;
		$row->INSTRUCTION=$rec->instruction;
		if ($rec->wo_pm_date!==null) 
		{ 
		$dt=new DateTime($rec->wo_pm_date);		
		$row->WO_PM_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->WO_PM_DATE='00010101'; } 
		if ($rec->wo_pm_finish_date!==null) 
		{ 
		$dt=new DateTime($rec->wo_pm_finish_date);		
		$row->WO_PM_FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->WO_PM_FINISH_DATE='00010101'; } 
		$row->DESC_WO_PM=$rec->desc_wo_pm;
		$row->EMP_NAME=$rec->emp_name;

		return $row;
	}
}
class Rowvirptworkorderpmform
{
	public $WO_PM_ID;
public $SERVICE_NAME;
public $PERSON;
public $COSTPERSON;
public $PART_NAME;
public $QTY;
public $UNIT_COST;
public $TOT_COST;
public $INSTRUCTION;
public $WO_PM_DATE;
public $WO_PM_FINISH_DATE;
public $DESC_WO_PM;
public $EMP_NAME;

}

?>