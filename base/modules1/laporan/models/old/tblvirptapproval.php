﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptapproval extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptapproval';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="app_id,emp_id,req_id,row_req,app_due_date,app_finish_date,desc_app,is_ext_repair,cost,approver,asset_maint_id,status_id,problem,req_date,dept_name,asset_maint_name,location,status_name,desc_status,req_finish_date,asset,repair,dept_id,category_id,category_name,requester,vendor"  
;
		$this->SqlQuery = " select * from (
		select   app.app_id, app.emp_id, app.req_id, app.row_req, app.app_due_date, app.app_finish_date, app.desc_app, app.is_ext_repair, 
		app.cost, e.emp_name as approver, rcd.asset_maint_id, rcd.status_id, rcd.problem, rc.req_date, d.dept_name, a.asset_maint_name, 
		l.location, st.status_name, rcd.desc_status, rcd.req_finish_date, rcd.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, 
		case when is_ext_repair = 'true' then 'external' else 'internal' end as repair, d.dept_id, c.category_id, c.
		category_name, e2.emp_name as requester,  am_vendors.vendor
		from am_vendors right outer join
		am_sch_cm_serv_person on  am_vendors.vendor_id =  am_sch_cm_serv_person.vendor_id right outer join
		am_sch_cm_service on  am_sch_cm_serv_person.service_id =  am_sch_cm_service.service_id and 
		am_sch_cm_serv_person.category_id =  am_sch_cm_service.category_id and 
		am_sch_cm_serv_person.sch_cm_id =  am_sch_cm_service.sch_cm_id right outer join
		am_schedule_cm on  am_sch_cm_service.sch_cm_id =  am_schedule_cm.sch_cm_id right outer join
		am_approve_cm as app inner join
		am_employees as e on app.emp_id = e.emp_id inner join
		am_request_cm_detail as rcd on app.req_id = rcd.req_id and app.row_req = rcd.row_req inner join
		am_request_cm as rc on rcd.req_id = rc.req_id inner join
		am_employees as e2 on rc.emp_id = e2.emp_id inner join
		am_department as d on rc.dept_id = d.dept_id inner join
		am_asset_maint as a on rcd.asset_maint_id = a.asset_maint_id inner join
		am_location as l on a.location_id = l.location_id inner join
		am_status as st on rcd.status_id = st.status_id inner join
		am_category as c on a.category_id = c.category_id on  am_schedule_cm.app_id = app.app_id
		group by app.app_id, app.emp_id, app.req_id, app.row_req, app.app_due_date, app.app_finish_date, app.desc_app, app.cost, 
		e.emp_name, rcd.asset_maint_id, rcd.status_id, rcd.problem, rc.req_date, d.dept_name, a.asset_maint_name, l.location,
		st.status_name, rcd.desc_status, rcd.req_finish_date, rcd.asset_maint_id ".$this->SQL->OPJoinStr()." ' - ' ".$this->SQL->OPJoinStr()." a.asset_maint_name, d.dept_id,
		c.category_id, c.category_name, e2.emp_name, app.is_ext_repair,  am_vendors.vendor) as resdata " ;
		
	}


	function FillRow($rec)
	{
		$row=new Rowvirptapproval;
				$row->APP_ID=$rec->app_id;
		$row->EMP_ID=$rec->emp_id;
		$row->REQ_ID=$rec->req_id;
		$row->ROW_REQ=$rec->row_req;
		if ($rec->app_due_date!==null) 
		{
$dt=new DateTime($rec->app_due_date);					
		$row->APP_DUE_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->APP_DUE_DATE='00010101'; } 
		if ($rec->app_finish_date!==null) 
		{ 
		$dt=new DateTime($rec->app_finish_date);			
		$row->APP_FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->APP_FINISH_DATE='00010101'; } 
		$row->DESC_APP=$rec->desc_app;
		$row->IS_EXT_REPAIR=$rec->is_ext_repair;
		$row->COST=$rec->cost;
		$row->APPROVER=$rec->approver;
		$row->ASSET_MAINT_ID=$rec->asset_maint_id;
		$row->STATUS_ID=$rec->status_id;
		$row->PROBLEM=$rec->problem;
		if ($rec->req_date!==null) 
		{
$dt=new DateTime($rec->req_date);					
		$row->REQ_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->REQ_DATE='00010101'; } 
		$row->DEPT_NAME=$rec->dept_name;
		$row->ASSET_MAINT_NAME=$rec->asset_maint_name;
		$row->LOCATION=$rec->location;
		$row->STATUS_NAME=$rec->status_name;
		$row->DESC_STATUS=$rec->desc_status;
		if ($rec->req_finish_date!==null) 
		{ 
		$dt=new DateTime($rec->req_finish_date);			
		$row->REQ_FINISH_DATE=$dt->format('Ymd'); } 
		 else 
		{ $row->REQ_FINISH_DATE='00010101'; } 
		$row->ASSET=$rec->asset;
		$row->REPAIR=$rec->repair;
		$row->DEPT_ID=$rec->dept_id;
		$row->CATEGORY_ID=$rec->category_id;
		$row->CATEGORY_NAME=$rec->category_name;
		$row->REQUESTER=$rec->requester;
		$row->VENDOR=$rec->vendor;

		return $row;
	}
}
class Rowvirptapproval
{
	public $APP_ID;
public $EMP_ID;
public $REQ_ID;
public $ROW_REQ;
public $APP_DUE_DATE;
public $APP_FINISH_DATE;
public $DESC_APP;
public $IS_EXT_REPAIR;
public $COST;
public $APPROVER;
public $ASSET_MAINT_ID;
public $STATUS_ID;
public $PROBLEM;
public $REQ_DATE;
public $DEPT_NAME;
public $ASSET_MAINT_NAME;
public $LOCATION;
public $STATUS_NAME;
public $DESC_STATUS;
public $REQ_FINISH_DATE;
public $ASSET;
public $REPAIR;
public $DEPT_ID;
public $CATEGORY_ID;
public $CATEGORY_NAME;
public $REQUESTER;
public $VENDOR;

}

?>