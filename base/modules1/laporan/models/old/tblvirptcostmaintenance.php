﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptcostmaintenance extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="dept_id,dept_name,total,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nop,des"  
;
		$this->TblName='virptcostmaintenance';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select     dept_id, dept_name, sum(last_cost) as total, 
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 1 then last_cost else 0 end) as jan, 
            sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 2 then last_cost else 0 end) as feb, 
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 3 then last_cost else 0 end) as mar, 
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 4 then last_cost else 0 end) as apr, 
            sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 5 then last_cost else 0 end) as mei, 
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 6 then last_cost else 0 end) as jun, 
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 7 then last_cost else 0 end) as jul, 
            sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 8 then last_cost else 0 end) as agu,
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 9 then last_cost else 0 end) as sep,
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 10 then last_cost else 0 end) as okt, 
            sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 11 then last_cost else 0 end) as nop, 
			sum(case when ".$this->SQL->fnMonth("x.finish_date")." = 12 then last_cost else 0 end) as des
			from (select a.dept_id, d.dept_name, r.last_cost, r.finish_date from am_result_pm as r inner join
			am_work_order_pm as w on r.wo_pm_id = w.wo_pm_id inner join
			am_wo_pm_service as ws on w.wo_pm_id = ws.wo_pm_id inner join
			am_sch_pm_detail as sd on ws.service_id = sd.service_id and ws.category_id = sd.
			category_id and ws.sch_pm_id = sd.sch_pm_id and ws.row_sch = sd.row_sch inner join
			am_schedule_pm as s on sd.sch_pm_id = s.sch_pm_id inner join
			am_asset_maint as a on s.asset_maint_id = a.asset_maint_id inner join
			am_department as d on a.dept_id = d.dept_id #where#
			union all
			select a.dept_id, d.dept_name, r.last_cost, r.finish_date
			from am_result_cm as r inner join
			am_work_order_cm as w on r.wo_cm_id = w.wo_cm_id inner join
			am_schedule_cm as s on w.sch_cm_id = s.sch_cm_id inner join
			am_approve_cm as ap on s.app_id = ap.app_id inner join
			am_request_cm_detail as rd on ap.req_id = rd.req_id and ap.row_req = rd.row_req 
			inner join
			am_asset_maint as a on rd.asset_maint_id = a.asset_maint_id inner join
			am_department as d on a.dept_id = d.dept_id #where# ) as x
			
			group by dept_id, dept_name";
	
		
	}


	function FillRow($rec)
	{
		$row=new Rowvirptcostmaintenance;
				$row->DEPT_ID=$rec->dept_id;
		$row->DEPT_NAME=$rec->dept_name;
		$row->TOTAL=$rec->total;
		$row->JAN=$rec->jan;
		$row->FEB=$rec->feb;
		$row->MAR=$rec->mar;
		$row->APR=$rec->apr;
		$row->MEI=$rec->mei;
		$row->JUN=$rec->jun;
		$row->JUL=$rec->jul;
		$row->AGU=$rec->agu;
		$row->SEP=$rec->sep;
		$row->OKT=$rec->okt;
		$row->NOP=$rec->nop;
		$row->DES=$rec->des;

		return $row;
	}
}
class Rowvirptcostmaintenance
{
	public $DEPT_ID;
public $DEPT_NAME;
public $TOTAL;
public $JAN;
public $FEB;
public $MAR;
public $APR;
public $MEI;
public $JUN;
public $JUL;
public $AGU;
public $SEP;
public $OKT;
public $NOP;
public $DES;

}

?>