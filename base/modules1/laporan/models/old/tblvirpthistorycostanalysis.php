﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirpthistorycostanalysis extends TblBase
{
	
	function __construct()
	{
		
		$this->StrSql="years,category,department,asset,jan,feb,mar,apr,may,jun,jul,aug,sept,okt,nop,dec,category_id,dept_id"  
;
		$this->TblName='virpthistorycostanalysis';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select * from ( select ".$this->SQL->fnYear('x.finish_date')." as years, c.category_name as category, d.dept_name as departement,
		x.asset_maint_id ".$this->SQL->OPJoinStr()." a.asset_maint_name as asset, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 1 then x.last_cost else 0 end) as jan,
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 2 then x.last_cost else 0 end) as feb, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 3 then x.last_cost else 0 end) as mar, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 4 then x.last_cost else 0 end) as apr,
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 5 then x.last_cost else 0 end) as may,
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 6 then x.last_cost else 0 end) as jun,
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 7 then x.last_cost else 0 end) as jul, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 8 then x.last_cost else 0 end) as aug, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 9 then x.last_cost else 0 end) as sept, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 10 then x.last_cost else 0 end) as okt,
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 11 then x.last_cost else 0 end) as nop, 
		sum(case when ".$this->SQL->fnMonth('x.finish_date')." = 12 then x.last_cost else 0 end) as dec, 
		a.category_id, a.dept_id
		from 
		(select  sp.asset_maint_id, rp.last_cost, rp.finish_date
		from   am_result_pm as rp inner join
		am_work_order_pm as wo on rp.wo_pm_id = wo.wo_pm_id inner join
		am_wo_pm_service on wo.wo_pm_id =  am_wo_pm_service.wo_pm_id inner join
		am_schedule_pm as sp inner join
		am_sch_pm_detail as spd on sp.sch_pm_id = spd.sch_pm_id on  am_wo_pm_service.
		sch_pm_id = spd.sch_pm_id and 
		am_wo_pm_service.category_id = spd.category_id and  am_wo_pm_service.service_id = 
		spd.service_id and  am_wo_pm_service.row_sch = spd.row_sch 
		union all
		select  req.asset_maint_id, rc.last_cost, rc.finish_date
		from  am_result_cm as rc inner join
		am_work_order_cm as wo on rc.wo_cm_id = wo.wo_cm_id inner join
		am_schedule_cm as sc on wo.sch_cm_id = sc.sch_cm_id inner join
		am_approve_cm as ac on sc.app_id = ac.app_id inner join
		am_request_cm_detail as req on ac.req_id = req.req_id and ac.row_req = req.row_req )
		as x inner join
		am_asset_maint as a on x.asset_maint_id = a.asset_maint_id inner join
		am_department as d on d.dept_id = a.dept_id inner join
		am_category as c on c.category_id = a.category_id #where#
		group by x.finish_date, c.category_name, d.dept_name, x.asset_maint_id, a.asset_maint_name, a.category_id, a.dept_id ) as resdata";

		
	}


	function FillRow($rec)
	{
		$row=new Rowvirpthistorycostanalysis;
				$row->YEARS=$rec->years;
		$row->CATEGORY=$rec->category;
		$row->DEPARTEMENT=$rec->departement;
		$row->ASSET=$rec->asset;
		$row->JAN=$rec->jan;
		$row->FEB=$rec->feb;
		$row->MAR=$rec->mar;
		$row->APR=$rec->apr;
		$row->MAY=$rec->may;
		$row->JUN=$rec->jun;
		$row->JUL=$rec->jul;
		$row->AUG=$rec->aug;
		$row->SEPT=$rec->sept;
		$row->OKT=$rec->okt;
		$row->NOP=$rec->nop;
		$row->DEC=$rec->dec;
		$row->CATEGORY_ID=$rec->category_id;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirpthistorycostanalysis
{
	public $YEARS;
public $CATEGORY;
public $DEPARTEMENT;
public $ASSET;
public $JAN;
public $FEB;
public $MAR;
public $APR;
public $MAY;
public $JUN;
public $JUL;
public $AUG;
public $SEPT;
public $OKT;
public $NOP;
public $DEC;
public $CATEGORY_ID;
public $DEPT_ID;

}

?>