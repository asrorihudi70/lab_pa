﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptjobclosepmform extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="result_cm_id,service_name,person,costperson,part_name,qty,unit_cost,tot_cost,desc_rslt"  
;
		$this->TblName='virptjobclosepmform';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->SqlQuery="select  * from (select   rc.result_pm_id, sv.service_name,  getallpersonservicepm(rc.result_pm_id, rcs.service_id, rcs.category_id) as person, 
                       getallcostpersonservicepm(rc.result_pm_id, rcs.service_id, rcs.category_id) as costperson, s.part_name, rcsp.
qty, rcsp.unit_cost, rcsp.tot_cost, rcsp.desc_rslt
from          am_service as sv inner join
                       am_result_pm as rc inner join
                       am_result_pm_service as rcs on rc.result_pm_id = rcs.result_pm_id on sv.service_id = rcs.service_id left 
outer join
                       am_spareparts as s inner join
                       am_result_pm_serv_part as rcsp on s.part_id = rcsp.part_id on rcs.result_pm_id = rcsp.result_pm_id and 
                      rcs.service_id = rcsp.service_id and rcs.category_id = rcsp.category_id ) as resdata ";

	}

	function SaveData( $DBConn, array $values)
	{
		
		return TblHelper::SaveData($DBConn,$values);
		
	}
	
	function UpdateData( $DBConn, array $values, $criteria='')
	{
		
		return TblHelper::UpdateData($DBConn,$values,$criteria);
		
	}
	function FillRow($rec)
	{
		$row=new Rowvirptjobclosecmform;
				$row->RESULT_PM_ID=$rec->result_pm_id;
		$row->SERVICE_NAME=$rec->service_name;
		$row->PERSON=$rec->person;
		$row->COSTPERSON=$rec->costperson;
		$row->PART_NAME=$rec->part_name;
		$row->QTY=$rec->qty;
		$row->UNIT_COST=$rec->unit_cost;
		$row->TOT_COST=$rec->tot_cost;
		$row->DESC_RSLT=$rec->desc_rslt;

		return $row;
	}
}
class Rowvirptjobclosecmform
{
	public $RESULT_PM_ID;
public $SERVICE_NAME;
public $PERSON;
public $COSTPERSON;
public $PART_NAME;
public $QTY;
public $UNIT_COST;
public $TOT_COST;
public $DESC_RSLT;

}

?>