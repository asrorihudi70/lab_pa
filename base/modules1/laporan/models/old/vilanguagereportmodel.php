<?php
/**
 * @author Ali
 * @copyright NCI 2010
 */

class vilanguagereportmodel extends Model
{

    function vilanguagereportmodel()
    {
        parent::Model();
        $this->load->database();
    }

    function read($criteria)
    {
        $this->db->where($criteria);
        $query = $this->db->get("dbo.viLanguageReport");

        return $query;
    }

}

?>
