﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptrwjs extends TblBase
{

	function __construct()
	{
		$this->TblName='virptrwjs';
		TblBase::TblBase(true);
		$this->SQL=$this->db;
		$this->StrSql="umum,gigi,gizi,syaraf,vtc,interna,kandungan,anak,bedah,tht,mata,kulit_kelamin,
                                kia,jiwa,dm,jantung";
                
		$this->SqlQuery="select
                                sum(x.umum)   as UMUM,
                                sum(x.Gigi)   as Gigi,
                                sum(x.Gizi)   as Gizi,
                                sum(x.Syaraf) as Syaraf,
                                sum(x.VTC) as VTC,
                                sum(x.Interna) as Interna,
                                sum(x.Kandungan) as Kandungan,
                                sum(x.Anak) as Anak,
                                sum(x.Bedah) as Bedah,
                                sum(x.THT) as THT,
                                sum(x.Mata) as Mata,
                                sum(x.Kulit_Kelamin) as Kulit_Kelamin,
                                sum(x.KIA) as KIA,
                                sum(x.Jiwa) as Jiwa,
                                sum(x.DM) as DM,
                                sum(x.Jantung) as Jantung
                            from
                            (
                            select
                                    case when u.kd_unit = '201' then count(k.kd_pasien) else 0 end as Umum,
                                    case when u.kd_unit = '202' then count(k.kd_pasien) else 0 end as Gigi,
                                    case when u.kd_unit = '203' then count(k.kd_pasien) else 0 end as Gizi,
                                    case when u.kd_unit = '204' then count(k.kd_pasien) else 0 end as Syaraf,
                                    case when u.kd_unit = '205' then count(k.kd_pasien) else 0 end as VTC,
                                    case when u.kd_unit = '206' then count(k.kd_pasien) else 0 end as Interna,
                                    case when u.kd_unit = '207' then count(k.kd_pasien) else 0 end as Kandungan,
                                    case when u.kd_unit = '208' then count(k.kd_pasien) else 0 end as Anak,
                                    case when u.kd_unit = '209' then count(k.kd_pasien) else 0 end as Bedah,
                                    case when u.kd_unit = '210' then count(k.kd_pasien) else 0 end as THT,
                                    case when u.kd_unit = '211' then count(k.kd_pasien) else 0 end as Mata,
                                    case when u.kd_unit = '212' then count(k.kd_pasien) else 0 end as Kulit_Kelamin,
                                    case when u.kd_unit = '213' then count(k.kd_pasien) else 0 end as KIA,
                                    case when u.kd_unit = '214' then count(k.kd_pasien) else 0 end as Jiwa,
                                    case when u.kd_unit = '215' then count(k.kd_pasien) else 0 end as DM,
                                    case when u.kd_unit = '216' then count(k.kd_pasien) else 0 end as Jantung

                            from Unit u
                            inner join kunjungan k on k.kd_unit = u.kd_unit
                            group by u.kd_unit, k.kd_unit, u.nama_unit
                            ) x";

	}


	function FillRow($rec)
    {
        $row=new Rowviewrwjs;

          $row->UMUM=$rec->umum;
          $row->GIGI=$rec->gigi;
          $row->GIZI=$rec->gizi;
          $row->SYARAF=$rec->syaraf;
          $row->VTC=$rec->vtc;
          $row->INTERNA=$rec->interna;
          $row->KANDUNGAN=$rec->kandungan;
          $row->ANAK=$rec->anak;
          $row->BEDAH=$rec->bedah;
          $row->THT=$rec->tht;
          $row->MATA=$rec->mata;
          $row->KULIT_KELAMIN=$rec->kulit_kelamin;
          $row->KIA=$rec->kia;
          $row->JIWA=$rec->jiwa;
          $row->DM=$rec->dm;
          $row->JANTUNG=$rec->jantung;

          
        return $row;
    }

}

class Rowviewrwjs
{
          public $UMUM;
          public $GIGI ;
          public $GIZI ;
          public $SYARAF;
          public $VTC ;
          public $INTERNA ;
          public $KANDUNGAN ;
          public $ANAK ;
          public $BEDAH ;
          public $THT ;
          public $MATA ;
          public $KULIT_KELAMIN ;
          public $KIA ;
          public $JIWA;
          public $DM ;
          public $JANTUNG;
}

?>