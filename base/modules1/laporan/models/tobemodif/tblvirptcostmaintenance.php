﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptcostmaintenance extends TblBase
{
	
	function __construct()
	{	$this->TblName='virptcostmaintenance';
	
		$this->SQL=$this->db;
		$this->StrSql="dept_id,dept_name,total,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nop,des"  
;		
		
	}

	function FillRow($rec)
	{
		$row=new Rowvirptcostmaintenance;
				$row->DEPT_ID=$rec->dept_id;
		$row->DEPT_NAME=$rec->dept_name;
		$row->TOTAL=$rec->total;
		$row->JAN=$rec->jan;
		$row->FEB=$rec->feb;
		$row->MAR=$rec->mar;
		$row->APR=$rec->apr;
		$row->MEI=$rec->mei;
		$row->JUN=$rec->jun;
		$row->JUL=$rec->jul;
		$row->AGU=$rec->agu;
		$row->SEP=$rec->sep;
		$row->OKT=$rec->okt;
		$row->NOP=$rec->nop;
		$row->DES=$rec->des;

		return $row;
	}
}
class Rowvirptcostmaintenance
{
	public $DEPT_ID;
public $DEPT_NAME;
public $TOTAL;
public $JAN;
public $FEB;
public $MAR;
public $APR;
public $MEI;
public $JUN;
public $JUL;
public $AGU;
public $SEP;
public $OKT;
public $NOP;
public $DES;

}

?>