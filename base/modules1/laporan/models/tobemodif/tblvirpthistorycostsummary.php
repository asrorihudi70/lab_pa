﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirpthistorycostsummary extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="years,department,asset,partcostpm,laborcostpm,extcostpm,partcostcm,laborcostcm,extcostcm,total,dept_id"  
;
		$this->TblName='virpthistorycostsummary';
				 TblBase::TblBase();
		 $this->SQL=$this->db;
	}

	function FillRow($rec)
	{
		$row=new Rowvirpthistorycostsummary;
				$row->YEARS=$rec->years;
		$row->DEPARTMENT=$rec->department;
		$row->ASSET=$rec->asset;
		$row->PARTCOSTPM=$rec->partcostpm;
		$row->LABORCOSTPM=$rec->laborcostpm;
		$row->EXTCOSTPM=$rec->extcostpm;
		$row->PARTCOSTCM=$rec->partcostcm;
		$row->LABORCOSTCM=$rec->laborcostcm;
		$row->EXTCOSTCM=$rec->extcostcm;
		$row->TOTAL=$rec->total;
		$row->DEPT_ID=$rec->dept_id;

		return $row;
	}
}
class Rowvirpthistorycostsummary
{
	public $YEARS;
public $DEPARTMENT;
public $ASSET;
public $PARTCOSTPM;
public $LABORCOSTPM;
public $EXTCOSTPM;
public $PARTCOSTCM;
public $LABORCOSTCM;
public $EXTCOSTCM;
public $TOTAL;
public $DEPT_ID;

}

?>