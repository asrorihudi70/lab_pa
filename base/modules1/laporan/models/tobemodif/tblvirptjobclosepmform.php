﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblvirptjobclosepmform extends TblBase
{
	
	function __construct()
	{
		$this->TblName='virptjobclosepmform';
		TblBase::TblBase();
		$this->StrSql="result_pm_id,service_name,person,costperson,part_name,qty,unit_cost,tot_cost,desc_rslt"  
;
		
	}

	function SaveData( $DBConn, array $values)
	{
		
		return TblHelper::SaveData($DBConn,$values);
		
	}
	
	function UpdateData( $DBConn, array $values, $criteria='')
	{
		
		return TblHelper::UpdateData($DBConn,$values,$criteria);
		
	}
	function FillRow($rec)
	{
		$row=new Rowvirptjobclosepmform;
				$row->RESULT_PM_ID=$rec->result_pm_id;
		$row->SERVICE_NAME=$rec->service_name;
		$row->PERSON=$rec->person;
		$row->COSTPERSON=$rec->costperson;
		$row->PART_NAME=$rec->part_name;
		$row->QTY=$rec->qty;
		$row->UNIT_COST=$rec->unit_cost;
		$row->TOT_COST=$rec->tot_cost;
		$row->DESC_RSLT=$rec->desc_rslt;

		return $row;
	}
}
class Rowvirptjobclosepmform
{
	public $RESULT_PM_ID;
public $SERVICE_NAME;
public $PERSON;
public $COSTPERSON;
public $PART_NAME;
public $QTY;
public $UNIT_COST;
public $TOT_COST;
public $DESC_RSLT;

}

?>