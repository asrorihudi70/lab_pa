<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller_kunjungan extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $resultQuery            = false;
	private $AppId                  = "";
	private $no_medrec              = "";

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_detail_tr_bayar');
		$this->load->model('Tbl_data_detail_tr_bayar_component');
		$this->load->model('Tbl_data_transfer_bayar');
		$this->load->model('Tbl_data_detail_tr_kamar');
		$this->tgl_now = date("Y-m-d");

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}

	public function get_custom_data(){
		$select 	= $this->input->post('select');
		$where 		= $this->input->post('where');
		$field 		= $this->input->post('field');
		$criteria 	= $this->input->post('criteria');
		$table 		= $this->input->post('table');

		$query = $this->db->query("SELECT ".$select." FROM ".$table." WHERE ".$where."");
		
		echo json_encode($query->result());
	}

	public function get_harga_data(){
		$select 	= $this->input->post('select');
		$where 		= $this->input->post('where');
		$field 		= $this->input->post('field');
		$criteria 	= $this->input->post('criteria');
		$table 		= $this->input->post('table');

		$parameter 	= $this->input->post('parameter');
		if (isset($parameter)) {
			$tmp_params	= substr($parameter, 0, strlen($parameter)-1);
			$criteriaParams = " AND ".$field." in (".$tmp_params.")";
		}else{
			$criteriaParams = "";
		}

		/*if (isset($parameter)) {
			for($i = 0; $i<count($parameter); $i++){
				$tmp_params .= "'".$parameter[$i]."',";
			}
			$tmp_params = substr($tmp_params, 0, strlen($tmp_params)-1);
		}*/
		$query = $this->db->query("SELECT ".$select." FROM ".$table." WHERE ".$where."".$criteriaParams);
		
		echo json_encode($query->result());
	}

}
?>
