<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller_transfer extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $resultQuery            = false;
	private $AppId                  = "";
	private $no_medrec              = "";

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_detail_tr_bayar');
		$this->load->model('Tbl_data_detail_tr_bayar_component');
		$this->load->model('Tbl_data_transfer_bayar');
		$this->load->model('Tbl_data_detail_tr_kamar');
		$this->tgl_now = date("Y-m-d");

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}

	public function simpan_transfer(){
		$response = array();
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$params = array(
			'kd_kasir' 				=> $this->input->post('KDkasirIGD'),
			'no_transaksi' 			=> $this->input->post('TrKodeTranskasi'),
			'kd_unit' 				=> $this->input->post('KdUnit'),
			'kd_pay' 				=> $this->input->post('Kdpay'),
			'total_bayar' 			=> str_replace('.','', $this->input->post('Jumlahtotal')),
			'tgl_asal' 				=> $this->input->post('Tglasal'),
			'shift' 				=> $this->input->post('Shift'),
			'no_transaksi_tujuan' 	=> $this->input->post('TRKdTransTujuan'),
			'kd_pasien_tujuan' 		=> $this->input->post('KdpasienIGDtujuan'),
			'tgl_transaksi_tujuan'	=> $this->input->post('TglTranasksitujuan'),
			'kd_unit_tujuan'		=> $this->input->post('KDunittujuan'),
			'alasan' 				=> $this->input->post('KDalasan'),
			'kd_kasir_tujuan'		=> $this->input->post('KasirRWI'),
			'kd_customer' 			=> $this->input->post('Kdcustomer'),
			'kd_unit_kamar' 		=> $this->input->post('kodeunitkamar'),
			'kd_spesial' 			=> $this->input->post('kdspesial'),
			'no_kamar' 				=> $this->input->post('nokamar'),
			'tgl_transfer' 			=> date_format(date_create($this->input->post('tgltransfer')), 'Y-m-d'),
			'jumlah' 				=> $this->input->post('jumlah'),
		);
		

		$criteriaTable 	= array(
			'table' 			=> 'sys_setting',
			'field_select' 		=> 'setting',
			'field_criteria' 	=> 'key_data',
			'value_criteria' 	=> 'default_kd_kasir_rwi',
			'field_return' 		=> 'setting',
		);
		$params['kd_kasir_rwi'] = $this->Tbl_data_detail_bayar->getCustom($criteriaTable);

		$criteriaGetUrut 	= array(
			'table' 			=> 'detail_bayar',
			'field_select' 		=> '*',
			'field_criteria' 	=> array(
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_kasir' 		=> $params['kd_kasir']
			),
		);
		$queryGetUrut 	= $this->Tbl_data_detail_component->getCustom($criteriaGetUrut);
		if ($queryGetUrut->num_rows() > 0) {
			$params['urut_bayar'] 	= (int)$queryGetUrut->row()->urut + 1;
		}else{
			$params['urut_bayar'] 	= 1;
		}

		/* 	================================================================================================ PROSES SIMPAN DETAIL BAYAR ======================================== */
		$this->resultQuery = $this->saveDetailBayar($params);

		if ($this->resultQuery === true || $this->resultQuery > 0) {
			$paramsItem = array();
			for($i=0;$i<$params['jumlah'];$i++){
				unset($paramsInsert);
				$criteriaGetDetailTransaksi 	= array(
					'table' 			=> 'detail_transaksi',
					'field_select' 		=> '*',
					'field_criteria' 	=> array(
						'no_transaksi' 	=> $params['no_transaksi'],
						'kd_kasir' 		=> $params['kd_kasir'],
						'urut'			=> $this->input->post('urut-'.$i),
					),
				);

				$criteriaGetDataDetailBayar 	= array(
					'table' 			=> 'detail_bayar',
					'field_select' 		=> '*',
					'field_criteria' 	=> array(
						'no_transaksi' 	=> $params['no_transaksi'],
						'kd_kasir' 		=> $params['kd_kasir'],
						'tgl_transaksi'	=> $this->tgl_now,
					),
				);

				$queryGetDetailTransaksi 	= $this->Tbl_data_detail_component->getCustom($criteriaGetDetailTransaksi);
				$queryGetDetailBayar 		= $this->Tbl_data_detail_component->getCustom($criteriaGetDataDetailBayar);
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $queryGetDetailTransaksi->row()->urut,
					'tgl_transaksi' => $queryGetDetailTransaksi->row()->tgl_transaksi,
					'kd_pay' 		=> $params['kd_pay'],
					'urut_bayar' 	=> $queryGetDetailBayar->row()->urut,
					'tgl_bayar' 	=> $queryGetDetailBayar->row()->tgl_transaksi,
					'jumlah' 		=> $this->input->post('harga-'.$i),
				);

				if ($this->input->post('harga-'.$i)!=0) {
				/* 	================================================================================================ PROSES SIMPAN DETAIL TR BAYAR ======================================== */
					$this->resultQuery = $this->saveDetailTRBayar($paramsInsert);
					$response['message'] 	= "Bagian detail tr bayar";
				}


				if ($this->input->post('harga-'.$i)!=0 && ($this->resultQuery === true || $this->resultQuery >0)){

					$criteriaGetDataDetailBayar_component 	= array(
						'table' 			=> 'detail_component',
						'field_select' 		=> '*',
						'field_criteria' 	=> array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut' 			=> $queryGetDetailTransaksi->row()->urut,
							'tgl_transaksi' => $queryGetDetailTransaksi->row()->tgl_transaksi,
						),
					);

					$this->dbSQL->query("EXEC V5_updatestatustransaksi '".$params['kd_kasir']."', '".$params['no_transaksi']."',".$queryGetDetailTransaksi->row()->urut.", '".$queryGetDetailTransaksi->row()->tgl_transaksi."'");
					$this->db->query("SELECT updatestatustransaksi('".$params['kd_kasir']."', '".$params['no_transaksi']."',".$queryGetDetailTransaksi->row()->urut.", '".$queryGetDetailTransaksi->row()->tgl_transaksi."')");

					$queryGetDetailBayar_component 		= $this->Tbl_data_detail_component->getCustom($criteriaGetDataDetailBayar_component);
					unset($paramsInsert);
					foreach ($queryGetDetailBayar_component->result() as $data) {
						$paramsInsert = array(
							'kd_kasir' 		=> $data->kd_kasir,
							'no_transaksi' 	=> $data->no_transaksi,
							'urut' 			=> $data->urut,
							'tgl_transaksi' => $data->tgl_transaksi,
							'kd_pay' 		=> $params['kd_pay'],
							'urut_bayar' 	=> $queryGetDetailBayar->row()->urut,
							'tgl_bayar' 	=> $queryGetDetailBayar->row()->tgl_transaksi,
							'kd_component' 	=> $data->kd_component,
							'jumlah' 		=> $data->tarif,
						);
				/* 	================================================================================================ PROSES SIMPAN DETAIL TR BAYAR COMPONENT ======================================== */
						$this->resultQuery = $this->saveDetailTRBayarComponent($paramsInsert);
						$response['message'] 	= "Bagian detail tr bayar component";
						if ($this->resultQuery != true || $this->resultQuery == 0) {
							$this->resultQuery = false;
							break;
						}
					}
				}
				if ($this->resultQuery === false || $this->resultQuery == 0) {
					$this->resultQuery = false;
					break;
				}
			}
		}else{
			$this->resultQuery = false;
		}
		
		
		/* 	================================================================================================ PROSES SIMPAN DETAIL TRANSAKSI TUJUAN ======================================== */
		if ($this->resultQuery === true || $this->resultQuery > 0) {
			unset($criteriaTable);
			$criteriaTable = array(
				'table' 			=> 'detail_transaksi',
				'field_select' 		=> ' MAX(urut) as urutan',
				'field_criteria' 	=> array(
					'no_transaksi' 	=> $params['no_transaksi_tujuan'],
					'kd_kasir' 		=> $params['kd_kasir_rwi']
				),
				//'field_return' 		=> 'urutan',
			);

			$query_tujuan = $this->Tbl_data_detail_bayar->getCustom($criteriaTable);
			if ($query_tujuan->num_rows() > 0) {
				$params['urut_tujuan'] 	= (int)$query_tujuan->row()->urutan+1;
			}else{
				$params['urut_tujuan'] 	= 1;
			}

			unset($criteriaTable);
			$params['kd_tarif_cus'] = $this->db->query("SELECT getkdtarifcus('".$params['kd_customer']."')")->row()->getkdtarifcus;

			if ($params['kd_kasir'] == '06') {
				$tmp_kd_bagian = '3';
			}else if ($params['kd_kasir'] == '01') {
				$tmp_kd_bagian = '2';
			}else{
				$tmp_kd_bagian = '1';
			}
			$params['kd_produk_transfer'] = $this->db->query("
				SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
				FROM Produk_Charge pc 
				INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
				WHERE kd_unit='".$tmp_kd_bagian."' limit 1
			")->row()->kdproduk;
/*
			$params['tgl_berlaku'] = $this->db->query("SELECT gettanggalberlakuunit (
				'".$params['kd_tarif_cus']."',
				'".$params['tgl_transfer']."',
				'".$this->tgl_now."',
				".$params['kd_produk_transfer'].",
				'".substr($params['kd_unit'], 0, 1)."'
			)")->row()->gettanggalberlakuunit;*/

			$params['tgl_berlaku'] = $this->dbSQL->query("
				SELECT distinct top 1 tgl_berlaku  as tgl_berlaku
				from Tarif inner join 
				(produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas)
				on tarif.kd_produk =produk.kd_produk 
				where kd_tarif = '".$params['kd_tarif_cus']."'
				and tgl_berlaku <='".$params['tgl_transfer']."'
				and (tgl_berakhir >='".$this->tgl_now."'
				or tgl_berakhir is null)
				and kd_unit='".substr($params['kd_unit'], 0, 1)."'
				and produk.kd_produk='".$params['kd_produk_transfer']."' 
				order by Tgl_Berlaku desc --limit 1		
			")->row()->tgl_berlaku;
			unset($paramsInsert);

			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir_rwi'],
				'no_transaksi' 	=> $params['no_transaksi_tujuan'],
				'urut' 			=> $params['urut_tujuan'],
				'tgl_transaksi' => $params['tgl_transfer'],
				'kd_user' 		=> $this->id_user,
				'kd_tarif' 		=> $params['kd_tarif_cus'],
				'kd_produk' 	=> $params['kd_produk_transfer'],
				//'kd_unit' 		=> $params['kd_unit'],
				'tgl_berlaku' 	=> date_format(date_create($params['tgl_berlaku']), 'Y-m-d'),
				'charge' 		=> true,
				'adjust' 		=> true,
				'folio' 		=> 'E',
				'qty' 			=> 1,
				'harga' 		=> $params['total_bayar'],
				'shift' 		=> $params['shift'],
				'tag' 			=> false,
				'no_faktur' 	=> $params['no_transaksi'],
				'kd_unit_tr' 	=> $params['kd_unit_kamar'],
			);
			if (substr($params['kd_unit'], 0, 1) == '3') {
				$paramsInsert['kd_unit'] = $params['kd_unit'];
			}else{
				$paramsInsert['kd_unit'] = '2';
			}

			$this->resultQuery = $this->saveDetailTransaksi($paramsInsert);
			$response['message'] 	= "Bagian detail Transaksi";
		}else{
			$this->resultQuery = false;
		}


		/* 	================================================================================================ PROSES SIMPAN DETAIL TR BAYAR COMPONENT ======================================== */
		if ($this->resultQuery === true || $this->resultQuery > 0) {
			unset($paramsInsert);
			unset($queryGetDetailBayar_component);

			$queryGetDetailBayar_component = $this->db->query("SELECT kd_component as kd_component,sum(jumlah) as jumlah, 0 as disc FROM detail_tr_bayar_component where no_transaksi='".$params['no_transaksi']."'
			and Kd_Kasir = '".$params['kd_kasir']."' group by kd_component order by kd_component");

			foreach ($queryGetDetailBayar_component->result() as $data) {
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir_rwi'],
					'no_transaksi' 	=> $params['no_transaksi_tujuan'],
					'urut' 			=> $params['urut_tujuan'],
					'tgl_transaksi' => $params['tgl_transfer'],
					'kd_component' 	=> $data->kd_component,
					'tarif' 		=> $data->jumlah,
					'disc' 			=> $data->disc,
				);
				//$response['detail'] = $queryGetDetailBayar_component->result();
			/* 	================================================================================================ PROSES SIMPAN DETAIL COMPONENT ======================================== */
				$this->resultQuery = $this->saveDetailComponent($paramsInsert);
				$response['message'] 	= "Bagian detail component";
				if ($this->resultQuery != true || $this->resultQuery == 0) {
					$this->resultQuery = false;
					break;
				}
			}
		}else{
			$this->resultQuery = false;
		}

		/* 	================================================================================================ PROSES SIMPAN TRANSFER BAYAR ======================================== */
		if ($this->resultQuery === true || $this->resultQuery > 0) {
			unset($paramsInsert);
			unset($criteriaTable);
			$paramsInsert = array(
				'kd_kasir'			=> $params['kd_kasir'],
				'no_transaksi'		=> $params['no_transaksi'],
				'urut'				=> $params['urut_bayar'],
				'tgl_transaksi'		=> $params['tgl_transfer'],
				'det_kd_kasir'		=> $params['kd_kasir_rwi'],
				'det_no_transaksi'	=> $params['no_transaksi_tujuan'],
				'det_urut'			=> $params['urut_tujuan'],
				'det_tgl_transaksi'	=> $params['tgl_transfer'],
				'alasan'			=> $params['alasan'],
			);
			$this->resultQuery = $this->saveTransferBayar($paramsInsert);
			$response['message'] 	= "Bagian transfer bayar";
		}else{
			$this->resultQuery = false;
		}

		/* 	================================================================================================ PROSES SIMPAN DETAIL TR KAMAR ======================================== */
		if ($this->resultQuery === true || $this->resultQuery > 0) {
			unset($paramsInsert);
			unset($criteriaTable);
			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir_rwi'],
				'no_transaksi' 	=> $params['no_transaksi_tujuan'],
				'urut' 			=> $params['urut_tujuan'],
				'tgl_transaksi' => $params['tgl_transfer'],
				'kd_unit' 		=> $params['kd_unit_kamar'],
				'no_kamar' 		=> $params['no_kamar'],
				'kd_spesial' 	=> $params['kd_spesial'],
			);
			$this->resultQuery = $this->saveDetailTRKamar($paramsInsert);
			$response['message'] 	= "Bagian detail tr kamar";
		}else{
			$this->resultQuery = false;
		}

		//$this->resultQuery = false;
		if ($this->resultQuery === true || $this->resultQuery > 0) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
			$response['status'] = false;
		}
		echo json_encode($response);
	}	

	/*
		=====================================================================================================================================
		SAVE DETAIL BAYAR ===================================================================================================================
		=====================================================================================================================================
	 */
	
	private function saveDetailBayar($params){
		$resultPG 	= false;
		$resultPG 	= false;
		$resultSQL 	= false;

		$criteriaGetUrut 	= array(
			'table' 			=> 'detail_bayar',
			'field_criteria' 	=> array(
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_kasir' 		=> $params['kd_kasir']
			),
		);
		$queryGetUrut 	= $this->Tbl_data_detail_component->getCustom($criteriaGetUrut);
		if ($queryGetUrut->num_rows() > 0) {
			$tmpUrut 	= (int)$queryGetUrut->row()->urut + 1;
		}else{
			$tmpUrut 	= 1;
		}

		$paramsInsert = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'urut' 			=> $tmpUrut,
			'tgl_transaksi' => $this->tgl_now,
			'kd_user' 		=> $this->id_user,
			'kd_unit' 		=> $params['kd_unit'],
			'kd_pay' 		=> $params['kd_pay'],
			'jumlah' 		=> $params['total_bayar'],
			'folio' 		=> 'A',
			'shift' 		=> $params['shift'],
		);

		$resultPG 	= $this->Tbl_data_detail_bayar->insertDetailBayar($paramsInsert);
		$resultSQL 	= $this->Tbl_data_detail_bayar->insertDetailBayar_SQL($paramsInsert);
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return false;
		}
	}

	/*
		=====================================================================================================================================
		SAVE DETAIL TR BAYAR ================================================================================================================
		=====================================================================================================================================
	 */
	
	private function saveDetailTRBayar($params){
		$resultPG 	= false;
		$resultSQL 	= false;

		$resultPG 	= $this->Tbl_data_detail_tr_bayar->insertDetailTRBayar($params);
		$resultSQL 	= $this->Tbl_data_detail_tr_bayar->insertDetailTRBayar_SQL($params);
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return false;
		}
	}

	/*
		=====================================================================================================================================
		SAVE DETAIL TR BAYAR COMPONENT ======================================================================================================
		=====================================================================================================================================
	 */
	

	private function saveDetailTRBayarComponent($params){
		$resultPG 	= false;
		$resultSQL 	= false;
		$resultPG 	= $this->Tbl_data_detail_tr_bayar_component->insertDetailTRBayarComponent($params);
		$resultSQL 	= $this->Tbl_data_detail_tr_bayar_component->insertDetailTRBayarComponent_SQL($params);

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return true;
		}
	}

	/*
		=====================================================================================================================================
		SAVE DETAIL COMPONENT ===============================================================================================================
		=====================================================================================================================================
	 */
	

	private function saveDetailComponent($params){
		$resultPG 	= false;
		$resultSQL 	= false;
		$resultPG 	= $this->Tbl_data_detail_component->insertComponent($params);
		$resultSQL 	= $this->Tbl_data_detail_component->insertComponent_SQL($params);

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return true;
		}
	}

	/*
		=====================================================================================================================================
		SAVE DETAIL TRANSAKSI ===============================================================================================================
		=====================================================================================================================================
	 */
	

	private function saveDetailTransaksi($params){
		$resultPG 	= false;
		$resultSQL 	= false;

		if ($params['charge'] === true) {
			$params['charge'] = 'true';
		}else{
			$params['charge'] = 'false';
		}

		if ($params['adjust'] === true) {
			$params['adjust'] = 'true';
		}else{
			$params['adjust'] = 'false';
		}

		if ($params['tag'] === true) {
			$params['tag'] = 'true';
		}else{
			$params['tag'] = 'false';
		}

		$resultPG 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi($params);
		
		if ($params['charge'] === true) {
			$params['charge'] = 1;
		}else{
			$params['charge'] = 0;
		}

		if ($params['adjust'] === true) {
			$params['adjust'] = 1;
		}else{
			$params['adjust'] = 0;
		}

		if ($params['tag'] === true) {
			$params['tag'] = 1;
		}else{
			$params['tag'] = 0;
		}
		$resultSQL 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi_SQL($params);
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return true;
		}
	}

	/*
		=====================================================================================================================================
		SAVE TRANSFER BAYAR =================================================================================================================
		=====================================================================================================================================
	 */
	

	private function saveTransferBayar($params){
		$resultPG 	= false;
		$resultSQL 	= false;
		$resultPG 	= $this->Tbl_data_transfer_bayar->insert($params);
		$resultSQL 	= $this->Tbl_data_transfer_bayar->insert_SQL($params);

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return true;
		}
	}

	/*
		=====================================================================================================================================
		SAVE DETAIL TR KAMAR ================================================================================================================
		=====================================================================================================================================
	 */
	

	private function saveDetailTRKamar($params){
		$resultPG 	= false;
		$resultSQL 	= false;
		$resultPG 	= $this->Tbl_data_detail_tr_kamar->insert($params);
		$resultSQL 	= $this->Tbl_data_detail_tr_kamar->insert_SQL($params);

		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			return true;
		}else{
			return true;
		}
	}
}

?>
