<?php

/**
 * @author inull
 * @copyright 2010
 */

class VIEWCOMBODEPT extends ControllerBase    
{ 
	
	function GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort)
	{
		
		try
		{
			
			$this->LoadModel('tables/tblam_department');
			$res = $this->tblam_department->GetRowList($VConn, $param,$Skip  ,$Take   ,$SortDir, $Sort);
			return $res;		
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
		
	 	return $res;
		
	}


	 function SaveData($VConn,$param)
	 {
		$this->LoadModel('tables/tblam_department');
				$Arr['dept_id']=$_REQUEST['DEPT_ID'];
		$Arr['dept_name']=$_REQUEST['DEPT_NAME'];

		$Arr=array_filter( $Arr , "ArrayUtils::NullElement"  );
	 	$res=$this->tblam_department->SaveData($VConn,$Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
	 	return $res;
	 }
	 
	 function UpdateData($VConn, $param)
	 {
		$this->LoadModel('tables/tblam_department');
				$Arr['dept_id']=$_REQUEST['DEPT_ID'];
		$Arr['dept_name']=$_REQUEST['DEPT_NAME'];

		$Arr=array_filter( $Arr , "ArrayUtils::NullElement"  );
	 	$res=$this->tblam_department->UpdateData($VConn,$Arr, $param); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
	 	return $res;
	 }	
	 
	 function DeleteData($VConn, $param)
	 {
		$this->LoadModel('tables/tblam_department');
	 	$res=$this->tblam_department->DeleteData($VConn,$param); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
	 	return $res;
	 }	

	
} //VIEWCOMBODEPT

?>