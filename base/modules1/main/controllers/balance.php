<?php
class balance extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session','url');
    }
	 
    public function cetak()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));

      $var = $this->uri->segment(4,0);
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
		  
           $mpdf= new mPDF('utf-8' ,array(200,400));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
          // $mpdf->SetFooter($arr);
		  
		  $style = "<style>
		  body
		  {
		  
		  }
		  table
		  {
			  border-collapse:collapse;
			  border:0px solid #000;
			  width:600px;
			  height:5px;
			  font-size:12px;
			  float:left;
		  }
		  
		  table tr td
		  {
			  padding:1px 5px;
			  border:0px solid #000;
		  }
		  
		  .first
		  {
			  width : 250px;
		  }
		  
		  .middle_long
		  {
			  width:400px;
		  }
		  
		  .long 
		  {
			  width : 400px;
		  }
		  
		  .short
		  {
			  width:122px;
			  padding:0px !important; 
		  }
		  
		  .head
		  {
			  float:left;
			  width:100px;
			  height:5px;
		  }
		  
		  .bordered
		  {
			  float:left;
			  width:100px;
			  height:20px;
			  padding:2px;
			  border:1px solid #000;
		  }
		  
		   .no_bordered
		  {
			  float:left;
			   width:5px;
			  height:2spx;
			  padding:2px;
			  border:none;
		  }
		  
		  
		  </style>
		  ";
		 
		   
						$mpdf->WriteHTML('<html>
						<head>'.$style.'</head>
						<body>');
							
						$mpdf->WriteHTML("
						<table width='380' border='0' style='border:none !important'>
						<tr style='border:none !important'>
						<td style='border:none !important' width='230'><h1 align='center'>RSU Bhakti Asih</h1></td>
						</tr>
						<tr style='border:none !important'>
						<td style='border:none !important' width='210'><h1 align='center'>Jl. Raden Saleh No. 10 Tangerang</h1></td>
						</tr>
						<tr style='border:none !important'>
						<td style='border:none !important' width='210'><h1 align='center'>Phone: (021) 7305662 Fax: (021) 7305652</h1></td>
						</tr>
						
						
							</table>
							<p align='center'><b>      BALANCE SHEET   </b></p>
							<p align='center'><b> As At January 30, 2012  </b></p>
					
							");
							
							
							
							
						$varqurey1="(DB0-CR0+DB1-CR1)";
						$varqurey0="(CR0-DB0+CR1-DB1)";
						$thn="'2012'";
						$query ="SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
						CASE a.Groups WHEN 1 THEN $varqurey1 ELSE  $varqurey0 END as duit--,
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND
						Levels in (3) AND Years = $thn and Parent='11' order by acc asc ";
						
						
						$mpdf->WriteHTML('	&nbsp;<td align="left"> <b> AKTIVA</b>  </td>' );
						$mpdf->WriteHTML('	&nbsp;&nbsp;<td align="left">  AKTIVA LANCAR </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
								while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
								{
								$mpdf->WriteHTML('<tr> ');
								$mpdf->WriteHTML("<td align='left'>&nbsp;&nbsp;&nbsp;"  .$line['name']."</td>
												<td align='right'>Rp. "  . number_format($line['duit'] , 2 , ',' , '.' )."</td>
												</tr>
												<p>&nbsp;</p>");
								$i++; 
								}
								$i--;
								}$mpdf->WriteHTML('</table>');
									for($x=0; $x<=140; $x++)
										{
										$vargaris .= "-";		
										}
								
								
						$mpdf->WriteHTML('<td align="left">'.$vargaris.' </td>' );
						$gethartalancar = $this->db->query("
						SELECT CASE a.Groups WHEN 1 THEN $varqurey1 ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='11' ");

						foreach($gethartalancar->result() as $datalancar)
						{
						$duitlancar = $datalancar->duit;	
						}
						 	for($x=0; $x<=110; $x++)
								{
								$varspasi .= "&nbsp;";			
								}

						$mpdf->WriteHTML('<td align="right">Total Aktiva Lancar  '.$varspasi.' Rp.'.number_format($duitlancar,2,',','.').' </td>' );
						$mpdf->WriteHTML('<tr> </tr>' );
						
						
						
						
						
						$query = "
						SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='12' order by acc asc ";
						$mpdf->WriteHTML('	&nbsp;<td align="left"> <b> </b>  </td>' );
						$mpdf->WriteHTML('	&nbsp;&nbsp;<td align="left">  AKTIVA LANCAR LAINYA </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
						$mpdf->WriteHTML('<tr> ');
						
						$mpdf->WriteHTML('					
										<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
										<td align="left">Rp. '  .format_number($line['duit'],2,',','.').'</td>
										</tr>
										<p>&nbsp;</p>
						');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris5 .= "-";			
								}
						$mpdf->WriteHTML('<td align="left">'.$vargaris5.' </td>' );
						$gethartalancar2 = $this->db->query("
						SELECT CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='12' ");

						 foreach($gethartalancar2->result() as $duitlancarlain)
						 {
						 $duitlancarlain = $duitlancarlain->duit;
						 }
						 	for($x=0; $x<=120; $x++)
								{
								$varspasi5 .= "&nbsp;";			
								}

						$mpdf->WriteHTML('<td align="left">Total Aktiva Lancar Lainya '.$varspasi5.' Rp.'.number_format($duitlancarlain,2,',','.').' </td>' );
						$mpdf->WriteHTML('<tr> </tr>' );
				
				
		
		
		
		
						$query = "
						SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='13' order by acc asc ";
						$mpdf->WriteHTML('
										<p>&nbsp;</p>
										<td align="left">  AKTIVA TETAP</td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {
                        $aaa="";
                        $string = "";

						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
							$mpdf->WriteHTML('<tr> ');
							$mpdf->WriteHTML('
											<td align="left">&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '. number_format($line['duit'],2,',','.').'</td>
											</tr>
											<p>&nbsp;</p>
											');$i++; 
						}
						
							$i--;
                        }	$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris2 .= "-";			
								}
						$mpdf->WriteHTML('<td align="left">'.$vargaris2.' </td>' );
						$gethartatetap = $this->db->query("
						SELECT CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='13' ");

						 foreach($gethartatetap->result() as $datatetap)
						 {
							 $duittetap = $datatetap->duit;
						 }
						 	for($x=0; $x<=110; $x++)
								{
								$varspasi2 .= "&nbsp;";			
								}
						$mpdf->WriteHTML('<td align="right">Total Aktiva Tetap '.$varspasi2.' Rp.'.number_format($duittetap,2,',','.').' </td>' );		
						$mpdf->WriteHTML("</body></html>");				
							

						$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
							FROM ACCOUNTS a 
							INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3)
							AND Years = $thn and Parent='14' order by acc asc ";
								
							
						$mpdf->WriteHTML('	
						<p>&nbsp;</p>
						<td align="left">  AKTIVA TAK BERWUJUD  </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {
                        $aaa="";
                        $string = "";
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
							$mpdf->WriteHTML('<tr> ');
							$mpdf->WriteHTML('
											<td align="left">&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '  .number_format($line['duit'],2,',','.').'</td>
											</tr>
											<p>&nbsp;</p>');
						$i++; 
						}
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris3 .= "-";
								}
								
								
						$mpdf->WriteHTML('<td align="left">'.$vargaris3.' </td>' );
						$gethartatakberwujud = $this->db->query("SELECT CASE a.Groups WHEN 1 THEN 
						$varqurey1 ELSE  $varqurey0 END as duit--,
						FROM ACCOUNTS a INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn 
						and v.Account='14' ");
						foreach($gethartatakberwujud->result() as $hartatakberwujud)
						 {
						  $hartatakberwujud = $hartatakberwujud->duit;
						 }
							for($x=0; $x<=100; $x++)
								{
								$varspasi3 .= "&nbsp;";			
								}
						$mpdf->WriteHTML('<td align="right">Total Aktiva Tak Berwujud '.$varspasi3.' Rp.'.number_format($hartatakberwujud,2,',','.').' </td>' );		

			
						$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
							FROM ACCOUNTS a 
							INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='15' order by acc asc ";
		
			
			
						$mpdf->WriteHTML('
						<p>&nbsp;</p>
						<td align="left">  AKTIVA LAIN-LAIN   </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
						$mpdf->WriteHTML('<tr> ');
						$mpdf->WriteHTML('
											<td align="left">&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '  .number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris4 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
						$mpdf->WriteHTML('<td align="left">'.$vargaris4.' </td>' );
						$gethartatakberwujud = $this->db->query("SELECT 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn
						and v.Account='15' ");
						 foreach($gethartatakberwujud->result() as $hartaleaseng)
						 {
							 $hartaleaseng = $hartaleaseng->duit;
						 }
						 for($x=0; $x<=127; $x++)
								{
								$varspasi6 .= "&nbsp;";	
								}
						$mpdf->WriteHTML('<td align="right">Total Aktiva Lain-lain   '.$varspasi6.' Rp.'.number_format($hartaleaseng,2,',','.').' </td>' );		
			
			
			
			
			
			
			
						$query = "
						SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--,
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (3) 
						AND Years = $thn and Parent='16' order by acc asc ";

						$mpdf->WriteHTML('	
							<p>&nbsp;</p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						<td align="left">  AKTIVA LEASING   </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {
                        $aaa="";
                        $string = "";

						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
						$mpdf->WriteHTML('<tr> ');
						$mpdf->WriteHTML('
										<td align="left">&nbsp;&nbsp;'  .$line['name'].'</td>
										<td align="right">Rp. '  .number_format($line['duit'],2,',','.').'</td>
										</tr>
										<p>&nbsp;</p>
							');$i++; 
						}$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris7 .= "-";			
								}
						$mpdf->WriteHTML('<td align="left">'.$vargaris7.' </td>' );
						$gethartalainlain2 = $this->db->query("
							SELECT CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
							FROM ACCOUNTS a 
							INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn 
							and v.Account='15' ");

						 foreach($gethartalainlain2->result() as $hartalainlainya)
						 {
							 $hartalainlainya = $hartalainlainya->duit;
							
						 }for($x=0; $x<=120; $x++)
								{
								$varspasi7 .= "&nbsp;";			
								}
			
						$mpdf->WriteHTML('<td align="right"> Total Aktiva Leasing '.$varspasi7.' Rp. '.number_format($hartalainlainya,2,',','.').' </td>' );		
			
			
			
						$gethartatotal = $this->db->query("
						SELECT CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--,  
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (1) AND Years = $thn and v.Account='1' ");
						foreach($gethartatotal->result() as $hartatotal)
						{
						$hartatotal = $hartatotal->duit;
						}
						$mpdf->WriteHTML('&nbsp;&nbsp;<td align="right"> <b>Total Aktiva</b> '.$varspasi7.' Rp. '.number_format($hartatotal,2,',','.').' </td>' );
			
			
			
			
			
						$query = "	SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='21' order by acc asc ";
					
						$mpdf->WriteHTML('	
										<p>&nbsp;</p>
										&nbsp;<td align="left"> <b> KEWAJIBAN</b>  </td>' );
						$mpdf->WriteHTML('&nbsp;&nbsp;<td align="left">    KEWAJIBAN JANGKA PENDEK </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {
						$aaa="";
                        $string = "";
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
						$mpdf->WriteHTML('<tr> ');
						$mpdf->WriteHTML('<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
										  <td align="right">Rp. '.number_format($line['duit'],2,',','.').'</td>
										  </tr>
										<p>&nbsp;</p>
										');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris8 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
						$mpdf->WriteHTML('<td align="left">'.$vargaris8.' </td>' );
						$gethutangpendek = $this->db->query("
						SELECT CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='21' ");

						 foreach($gethutangpendek->result() as $datahutangpendek)
						 {
							 $duithutangpendek = $datahutangpendek->duit;
							
						 }
						 	for($x=0; $x<=90; $x++)
								{
								$varspasi8 .= "&nbsp;";			
								
								}
			$mpdf->WriteHTML('<td align="left"> Total Kewajiban Jangka Pendek'.$varspasi8.' Rp.'.number_format($duithutangpendek,2,',','.').' </td>' );		
			
			
			
			
				$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='21' order by acc asc ";
						
						
			
				$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='22' order by acc asc ";
						
					
						$mpdf->WriteHTML('	<p>&nbsp;</p>
						<td align="left">    KEWAJIBAN JANGKA PANJANG </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
								


							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	
										

											
											<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. ' .number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris10 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris10.' </td>' );
					$gethutangpanjang = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='22' ");

						 foreach($gethutangpanjang->result() as $datahutangpanjang)
						 {
							 $duithutangpanjang = $datahutangpanjang->duit;
							
						 }
						 	for($x=0; $x<=90; $x++)
								{
								$varspasi10 .= "&nbsp;";			
								
								}
			$mpdf->WriteHTML('<td align="left"> Total Kewajiban Jangka Panjang'.$varspasi10.' Rp.'.number_format($duithutangpanjang,2,',','.').' </td>' );		

			
			
			
			
				$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='23' order by acc asc ";
						
					
						$mpdf->WriteHTML('	<p>&nbsp;</p>
						<td align="left">      HUTANG SEWA GUNA USAHA </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
								


							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	

											<td align="left">&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp.'.number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris11 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris11.' </td>' );
					$gethutangpanjang = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='23' ");

						 foreach($gethutangpanjang->result() as $datahutangpanjang)
						 {
							 $duithutangpanjang = $datahutangpanjang->duit;
							
						 }
						 	for($x=0; $x<=100; $x++)
								{
								$varspasi11 .= "&nbsp;";			
								
								}
			$mpdf->WriteHTML('<td align="right"> Total Hutang Sewa Guna'.$varspasi11.' Rp.'.number_format($duithutangpanjang,2,',','.').' </td>' );		

			
			
			
			
			
			
			$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='24' order by acc asc ";
						
					
						$mpdf->WriteHTML('	<p>&nbsp;</p>
						<td align="left">        HUTANG KONTRIBUSI </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
								


							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	
										

											
											<td align="left">&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp.'.number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris12 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris12.' </td>' );
					$gethutangkontribusi = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='24' ");

						 foreach($gethutangkontribusi->result() as $datahutangkontribusi)
						 {
							 $datahutangkontribusi = $datahutangkontribusi->duit;
							
						 }
						 	for($x=0; $x<=110; $x++)
								{
								$varspasi12 .= "&nbsp;";			
								
								}
			$mpdf->WriteHTML('<td align="right"> Total Hutang Kontribusi'.($varspasi12).' Rp.'.number_format($datahutangkontribusi,2,',','.').' </td>' );		
			$gethutangtotal = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (1) AND Years = $thn and v.Account='2' ");

						 foreach($gethutangtotal->result() as $hutangtotal)
						 {
							 $hutangtotal = $hutangtotal->duit;
							
						 }
						 	for($x=0; $x<=113; $x++)
								{
								$varspasi20 .= "&nbsp;";			
								
								}
					$mpdf->WriteHTML('&nbsp;&nbsp;<td align="right"> <b>Total Kewajiban </b> '.$varspasi20.' Rp.'.number_format($hutangtotal,2,'.',',').' </td>' );
			
			
					$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--,  
							FROM ACCOUNTS a 
							INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='31' order by acc asc ";
						
			
						$mpdf->WriteHTML('	
							<p>&nbsp;</p>
							&nbsp;<td align="left"> <b> EKUITAS</b>  </td>' );
						$mpdf->WriteHTML('	&nbsp;&nbsp;<td align="left">  Modal Usaha </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
							$mpdf->WriteHTML('<tr> ');
							$mpdf->WriteHTML('
											<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '  .number_format($line['duit'],2,',','.').'</td>
									</tr>
									<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris14 .= "-";			
								}
					$mpdf->WriteHTML('<td align="left">'.$vargaris14.' </td>' );
					$gethutangpendek = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='31' ");

						 foreach($gethutangpendek->result() as $datahutangpendek)
						 {
							 $duithutangpendek = $datahutangpendek->duit;
							
						 }
						 	for($x=0; $x<=110; $x++)
								{
								$varspasi14 .= "&nbsp;";			
								
								}
			$mpdf->WriteHTML('<td align="right"> Total Modal Usaha'.$varspasi14.' Rp.'.number_format($duithutangpendek,2,',','.').' </td>' );		
			
			$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='32' order by acc asc ";
						

						$mpdf->WriteHTML('<p>&nbsp;</p>
						&nbsp;&nbsp;<td align="left">  SAHAM INVESTOR LAIN </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	
										

											
											<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '.number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris15 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris15.' </td>' );
					$gethutangpendek = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='32' ");

						 foreach($gethutangpendek->result() as $datahutangpendek)
						 {
							 $duithutangpendek = $datahutangpendek->duit;
							
						 }
						 	for($x=0; $x<=120; $x++)
								{
								$varspasi15 .= "&nbsp;";			
								
								}
						$mpdf->WriteHTML('<td align="right"> Total Saham Inventory Lain'.$varspasi15.' Rp.'.number_format($duithutangpendek,2,',','.').' </td>' );		
		
		
		
		
		
		$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
							FROM ACCOUNTS a 
							INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='33' order by acc asc ";
						
		
			$mpdf->WriteHTML('<p>&nbsp;</p>
				&nbsp;&nbsp;<td align="left">  PRIVE </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
								


							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	
										

											
											<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '.number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris16 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris16.' </td>' );
					$gethutangpendek = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='33' ");

						 foreach($gethutangpendek->result() as $datahutangpendek)
						 {
							 $duithutangpendek = $datahutangpendek->duit;
							
						 }
						 	for($x=0; $x<=128; $x++)
								{
								$varspasi16 .= "&nbsp;";			
								
								}
			$mpdf->WriteHTML('<td align="right"> Total Prive'.$varspasi16.' Rp.'.number_format($duithutangpendek,2,',','.').' </td>' );		
			
			
			
				$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='35' order by acc asc ";
						
		
			$mpdf->WriteHTML('<p>&nbsp;</p>
				&nbsp;&nbsp;<td align="left">    PENARIKAN DANA PRIBADI </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
								


							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	
										

											
											<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '  .number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris17 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris17.' </td>' );
					$gethutangpendek = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='35' ");

						 foreach($gethutangpendek->result() as $datahutangpendek)
						 {
							 $duithutangpendek = $datahutangpendek->duit;
							
						 }
						 	for($x=0; $x<=120; $x++)
								{
								$varspasi17 .= "&nbsp;";			
								
								}
						$mpdf->WriteHTML('<td align="left"> Total Penarikan Dana Pribadi'.$varspasi17.' Rp.'.number_format($duithutangpendek,2,',','.').' </td>' );
						$mpdf->WriteHTML('<tr> </tr>' );
			
			
			
			
			
							$query = "
							SELECT v.Account as acc, a.Name, a.Type, a.Levels, a.Parent, a.Groups, 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
							FROM ACCOUNTS a 
							INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (3) AND Years = $thn and Parent='39' order by acc asc ";
						
		
						$mpdf->WriteHTML('<p>&nbsp;</p>
						&nbsp;&nbsp;<td align="left">    LABA DITAHAN </td>' );
                        $mpdf->WriteHTML('<table>');
                        $result = pg_query($query) or die('Query: ' . pg_last_error());
                        $i=1;
					
                        if(pg_num_rows($result) <= 0)
                        {
                        $mpdf->WriteHTML('');
                        }
                        else
                        {

                        $aaa="";
                        $string = "";
						
                        
						while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
						{
								


							$mpdf->WriteHTML('<tr> ');
		
							$mpdf->WriteHTML('	
										

											
											<td align="left">&nbsp;&nbsp;&nbsp;'  .$line['name'].'</td>
											<td align="right">Rp. '  .number_format($line['duit'],2,',','.').'</td>
									
									</tr>
							<p>&nbsp;</p>
							');

						$i++; 
						}
						
						$i--;
                        }$mpdf->WriteHTML('</table>');
							for($x=0; $x<=140; $x++)
								{
								$vargaris18 .= "-";			
								//$mpdf->WriteHTML(''.$var.'<td align="left">---------------------- "</td>' );
			
							}
					$mpdf->WriteHTML('<td align="left">'.$vargaris18.' </td>' );
					$gethutangpendek = $this->db->query("
						SELECT 
							CASE a.Groups WHEN 1 THEN $varqurey1 
							ELSE  $varqurey0 END as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum$varqurey1
						--	else sum $varqurey0 END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
							WHERE Groups IN (1, 2, 3)  AND Levels in (2) AND Years = $thn and v.Account='39' ");

						 foreach($gethutangpendek->result() as $datahutangpendek)
						 {
							 $duithutangpendek = $datahutangpendek->duit;
							
						 }
						 	for($x=0; $x<=110; $x++)
								{
								$varspasi18 .= "&nbsp;";			
								
								}
						$mpdf->WriteHTML('<td align="right"> Total Laba ditahan'.$varspasi18.' Rp.'.number_format($duithutangpendek,2,',','.').' </td>' );
						$mpdf->WriteHTML('<tr> </tr>' );
			
					
						$getbebanEkuitas= $this->db->query("
						SELECT sum( 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE $varqurey0 END )as duit--, 
					--x = CASE a.Groups WHEN 1 THEN sum(DB0-CR0+DB1-CR1+DB2-CR2+DB3-CR3+DB4-CR4+DB5-CR5+DB6-CR6)
					--	else sum(CR0-DB0+CR1-DB1+CR2-DB2+CR3-DB3+CR4-DB4+CR5-DB5+CR6-DB6) END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (1) AND Years = '2012' and v.Account in('3')
							");

						 foreach($getbebanEkuitas->result() as $dataEkuitas)
						 {
							 $dataEkuitas = $dataEkuitas->duit;
							
						 }
						 	for($x=0; $x<=125; $x++)
								{
								$varspasi21 .= "&nbsp;";			
								}
					$mpdf->WriteHTML('<td align="left"> Total  Ekuitas '.$varspasi21.'Rp.'.number_format($dataEkuitas,2,',','.').' </td>' );		
			
			
			
			
			
			
			
						$getbebantotalkabeh = $this->db->query("
						SELECT sum( 
						CASE a.Groups WHEN 1 THEN $varqurey1 
						ELSE $varqurey0 END )as duit--, 
						--x = CASE a.Groups WHEN 1 THEN sum(DB0-CR0+DB1-CR1+DB2-CR2+DB3-CR3+DB4-CR4+DB5-CR5+DB6-CR6)
					--	else sum(CR0-DB0+CR1-DB1+CR2-DB2+CR3-DB3+CR4-DB4+CR5-DB5+CR6-DB6) END 
						FROM ACCOUNTS a 
						INNER JOIN Acc_Value v ON v.Account = a.Account  
						WHERE Groups IN (1, 2, 3)  AND Levels in (1) AND Years = '2012' and v.Account in('2','3')
							");

						 foreach($getbebantotalkabeh->result() as $databebantotalkabeh)
						 {
							 $duitbebantotalkabeh = $databebantotalkabeh->duit;
							
						 }
						 	for($x=0; $x<=97; $x++)
								{
								$varspasi19 .= "&nbsp;";			
								}
			$mpdf->WriteHTML('<td align="left"> <b>Total Kewajiban & Ekuitas</b> '.$varspasi19.'Rp.'.number_format($duitbebantotalkabeh,2,',','.').' </td>' );		
			
			$mpdf->WriteHTML("</body></html>");		
							
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output("formulir.pdf" ,'I');
exit;
	
	
}

}

?>