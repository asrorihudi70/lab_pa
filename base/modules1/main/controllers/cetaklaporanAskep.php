<?php

class cetaklaporanAskep extends MX_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('session');
    }

    public function index() {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }

    public function cetaklaporan() {

        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID, $Params);
    }

    // laporan KIUP
    public function LapMutuLayanan($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 8);

        if (count($Split) > 0) {
            $tgl = $Split[0];
            $tgl2 = $Split[1];
            $spesialisasi = $Split[2];
            $kelas = $Split[3];
            $kamar = $Split[4];
            $tmpspesialisasi = $Split[5];
            $tmpkelas = $Split[6];
            $tmpkamar = $Split[7];
        } else {
            
        }
        if ($tgl === $tgl2) {
            $kriteria = "Periode " . $tgl2;
        } else {
            $kriteria = "Periode " . $tgl . " s/d " . $tgl2;
        }

        $Param = "WHERE su.kd_spesial='" . $spesialisasi . "'   and ask.KD_UNIT='" . $kelas . "'   and ask.NO_KAMAR='" . $kamar . "' and ask.TANGGAL between '" . $tgl . "'   and '" . $tgl2 . "' order by ask.TANGGAL";

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $q = $this->db->query("select distinct ask.TANGGAL 
                                FROM ASKEP_MUTU_LAYAN  ask  
                                inner join KAMAR kmr on ask.NO_KAMAR = kmr.NO_KAMAR  AND ask.KD_UNIT = kmr.KD_UNIT  
                                inner join UNIT u on kmr.KD_UNIT  = u.KD_UNIT  
                                inner join KELAS kls on u.KD_KELAS = kls.KD_KELAS  
                                inner join Spc_unit su on u.KD_KELAS = su.KD_KELAS and u.KD_UNIT = su.KD_UNIT  
                                inner join SPESIALISASI sp on su.kd_spesial  = sp.KD_SPESIAL  
                                inner join PERAWAT p on ask.KD_PERAWAT = p.KD_PERAWAT  
                                INNER JOIN ASKEP_WAKTU AW ON ASK.KD_WAKTU = AW.KD_WAKTU  
                                inner join ASKEP_VARIABEL_MUTU asb on ask.KD_VARIABEL  = asb.KD_VARIABEL " . $Param);

        if ($q->num_rows == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $query = $q->result();
            $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
            <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t2 {
                        border: 0px;
                        font-size: 10px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .detail {
                        font-size: 17px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 13px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                
            </style>
           ");
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $logors = base_url() . "ui/images/Logo/LogoRs.png";
                $mpdf->WriteHTML("
               <table width='1000' cellspacing='0' border='0'>
                        <tr>
                                <td width='76'>
                                <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                </td>
                                <td>
                                <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                </td>
                        </tr>
                </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Mutu Layanan</h1>");
            $mpdf->WriteHTML("<h2 class='formarial' align='center'>" . $kriteria . "</h2>");
            $mpdf->WriteHTML('<table class="t2" border = "0">
                                <tr>
                                  <td width="80"><strong>Spesialisasi</strong></td>
                                  <td width="8">:</td>
                                  <td width="150">' . $tmpspesialisasi . '</td>
                                  <td width="80"><strong>Kelas</strong></td>
                                  <td width="8">:</td>
                                  <td width="150">' . $tmpkelas . '</td>
                                  <td width="80"><strong>Kamar</strong></td>
                                  <td width="8">:</td>
                                  <td width="150">' . $tmpkamar . '</td>
                                </tr>
                              </table>');
            $mpdf->WriteHTML('
            <table class="t1" border = "1">
             <thead>
                 <tr>
                     <td width="30" rowspan="2">No.</td>
                     <td width="500" rowspan="2">Variable/Sasaran</td>
                     <td width="100" colspan="2" align="center">Pagi</td>
                     <td width="100" colspan="2" align="center">Sore</td>
                     <td width="100" colspan="2" align="center">Malam</td>
                 </tr>
                 <tr>
                     <td width="50" align="center">B</td>
                     <td width="50" align="center">L</td>
                     <td width="50" align="center">B</td>
                     <td width="50" align="center">L</td>
                     <td width="50" align="center">B</td>
                     <td width="50" align="center">L</td>
                 </tr>
           ');
            foreach ($query as $line) {
                $no++;
                $date = date_create($line->tanggal);
                $tmptgl = date_format($date, "d/M/Y");
                $mpdf->WriteHTML('
                            <tr>
                             <td width="30">' . $no . '</td>
                             <td colspan="7"><strong>' . $tmptgl . '</strong></td>
                         </tr> 
                    </thead>
                    <tfoot>

                    </tfoot>

                   ');
                $Param2 = "WHERE su.kd_spesial='" . $spesialisasi . "'   and ask.KD_UNIT='" . $kelas . "'   and ask.NO_KAMAR='" . $kamar . "' and ask.TANGGAL = '" . $line->tanggal . "' order by ask.TANGGAL,asb.VARIABEL";
                $q2 = $this->db->query("SELECT ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, 
                                kmr.NAMA_KAMAR, sp.SPESIALISASI,  ask.KD_PERAWAT,  
                                concat(p.NAMA_PERAWAT,' - ',NIP) as perawat,sp.KD_SPESIAL, AW.WAKTU, ASK.KD_WAKTU,  asb.VARIABEL,  
                                Case when AW.KD_WAKTU=1 then  ask.PSBARU ELSE 0 end as PSBARU_PAGI,  
                                Case when AW.KD_WAKTU=1 then  ask.PSLAMA ELSE 0  end as  PSLAMA_PAGI,  
                                Case when AW.KD_WAKTU=2 then  ask.PSBARU  ELSE 0 end as PSBARU_SORE,  
                                Case when AW.KD_WAKTU=2 then  ask.PSLAMA  ELSE 0 end as PSLAMA_SORE,  
                                Case when AW.KD_WAKTU=3 then  ask.PSBARU  ELSE 0 end as PSBARU_MALAM,  
                                Case when AW.KD_WAKTU=3 then  ask.PSLAMA  ELSE 0 end as PSLAMA_MALAM   
                                FROM ASKEP_MUTU_LAYAN  ask  
                                inner join KAMAR kmr on ask.NO_KAMAR = kmr.NO_KAMAR  AND ask.KD_UNIT = kmr.KD_UNIT  
                                inner join UNIT u on kmr.KD_UNIT  = u.KD_UNIT  
                                inner join KELAS kls on u.KD_KELAS = kls.KD_KELAS  
                                inner join Spc_unit su on u.KD_KELAS = su.KD_KELAS and u.KD_UNIT = su.KD_UNIT  
                                inner join SPESIALISASI sp on su.kd_spesial  = sp.KD_SPESIAL  
                                inner join PERAWAT p on ask.KD_PERAWAT = p.KD_PERAWAT  
                                INNER JOIN ASKEP_WAKTU AW ON ASK.KD_WAKTU = AW.KD_WAKTU  
                                inner join ASKEP_VARIABEL_MUTU asb on ask.KD_VARIABEL  = asb.KD_VARIABEL " . $Param2);
                $query2 = $q2->result();
                foreach ($query2 as $line2) {
                    $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                            <td width="30"></td>
                            <td width="100">' . $line2->variabel . '</td>
                            <td align="right">' . $line2->psbaru_pagi . '</td>
                            <td align="right">' . $line2->pslama_pagi . '</td>
                            <td align="right">' . $line2->psbaru_sore . '</td>
                            <td align="right">' . $line2->pslama_sore . '</td>
                            <td align="right">' . $line2->psbaru_malam . '</td>
                            <td align="right">' . $line2->pslama_malam . '</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
                }
            }

            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/';
            $tmpname = time() . 'ASKEPLapMutulayanan';
//            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');
            $mpdf->Output();

            $res = '{ success : true, msg : "", id : "170201", title : "Laporan Mutu Layanan", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function LapRencanaAsuhan($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 10);
//        print_r($Split);

        if (count($Split) > 0) {
            $kd_pasien = $Split[0];
            $kdunit = $Split[1];
            $urut = $Split[2];
            $tgl = $Split[3];
            $kdpasien = $Split[4];
            $namapasien = $Split[5];
            $alamat = $Split[6];
            $spesial = $Split[7];
            $kelas = $Split[8];
            $kamar = $Split[9];
        } else {
            
        }

        $Param = "Where kd_pasien_kunj = '" . $kd_pasien . "' and kd_unit_kunj = '" . $kdunit . "' and urut_masuk_kunj = " . $urut . " and tgl_masuk_kunj = '" . $tgl . "'";

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $q = $this->db->query("select tgl_ncp::date , jam::time, diagnosa,tujuan,rencana, p.nama_perawat,tgl_selesai::date
                                from askep_ncp ncp
                                inner join perawat p on p.kd_perawat = ncp.kd_perawat " . $Param);

        if ($q->num_rows == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $query = $q->result();
            $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
            <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t2 {
                        border: 0px;
                        font-size: 10px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .detail {
                        font-size: 17px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 13px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                
            </style>
           ");
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $logors = base_url() . "ui/images/Logo/LogoRs.png";
                $mpdf->WriteHTML("
               <table width='1000' cellspacing='0' border='0'>
                        <tr>
                                <td width='76'>
                                <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                </td>
                                <td>
                                <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                </td>
                        </tr>
                </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'><u>RENCANA ASUHAN KEPERAWATAN (NCP)</u></h1>");
            $mpdf->WriteHTML('<table width="786" border = "0" class="t2">
                                <tr>
                                    <td width="94" height="23"><strong>No. Medrec</strong></td>
                                    <td width="10">:</td>
                                    <td width="306">' . $kdpasien . '</td>
                                    <td width="31">&nbsp;</td>
                                    <td width="90" height="23"><strong>Spesialisasi </strong></td>
                                    <td width="10">:</td>
                                    <td width="215">' . $spesial . '</td>
                                </tr>
                                <tr>
                                  <td><strong>Nama</strong></td>
                                  <td>:</td>
                                  <td>' . $namapasien . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kamar</strong></td>
                                    <td>:</td>
                                    <td>' . $kamar . '</td>
                                </tr>
                                <tr>
                                  <td height="23"><strong>Alamat</strong></td>
                                  <td>:</td>
                                  <td>' . $alamat . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kelas</strong></td>
                                    <td>:</td>
                                    <td>' . $kelas . '</td>
                                </tr>
                              </table>');
            $mpdf->WriteHTML('
           <table class="t1" border = "1">
            <thead>
                <tr>
                    <td width="100" align="center"><strong>Tanggal/Jam</strong></td>
                    <td width="150" align="center"><strong>Diagnosa Perawatan</strong></td>
                    <td width="150" align="center"><strong>Tujuan/Kriteria Hasil</strong></td>
                    <td width="150" align="center"><strong>Rencana Tindakan</strong></td>
                    <td width="100" align="center"><strong>Perawat</strong></td>
                    <td width="100" align="center"><strong>Tgl. Masalah Selesai</strong></td>
                </tr>
            </thead>
                <tfoot>

                </tfoot>
           ');

            foreach ($query as $line) {
                $tmpdate = DateTime::createFromFormat('Y-m-d', $line->tgl_ncp);
                $realtmpdate = date_format($tmpdate, "d/M/Y");
                $tmpdate2 = DateTime::createFromFormat('Y-m-d', $line->tgl_selesai);
                $realtmpdate2 = date_format($tmpdate, "d/M/Y");
                $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow">
                            <td width="100">' . $realtmpdate . ' ' . substr($line->jam, 0, 5) . '</td>
                            <td width="100">' . $line->diagnosa . '</td>
                            <td width="100">' . $line->tujuan . '</td>
                            <td width="100">' . $line->rencana . '</td>
                            <td width="100">' . $line->nama_perawat . '</td>
                            <td width="100">' . $realtmpdate2 . '</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
            }


            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/';
            $tmpname = time() . 'ASKEPLapMutulayanan';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "170201", title : "Laporan Mutu Layanan", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function LapSOAP($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 10);

        if (count($Split) > 0) {
            $kd_pasien = $Split[0];
            $kdunit = $Split[1];
            $urut = $Split[2];
            $tgl = $Split[3];
            $kdpasien = $Split[4];
            $namapasien = $Split[5];
            $alamat = $Split[6];
            $spesial = $Split[7];
            $kelas = $Split[8];
            $kamar = $Split[9];
        } else {
            
        }

        $Param = "Where kd_pasien_kunj = '" . $kd_pasien . "' and kd_unit_kunj = '" . $kdunit . "' and urut_masuk_kunj = " . $urut . " and tgl_masuk_kunj = '" . $tgl . "'";

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $q = $this->db->query("select tgl_soap::date as tgl,jam_soap::time as jam, subject as s,object as o,assusment as a,planing as p,nama_perawat 
                                from askep_soap sp
                                inner join perawat p on p.kd_perawat = sp.kd_perawat " . $Param);

        if ($q->num_rows == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $query = $q->result();
            $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(297, 210));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
            <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t2 {
                        border: 0px;
                        font-size: 10px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .detail {
                        font-size: 17px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 13px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                
            </style>
           ");
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $logors = base_url() . "ui/images/Logo/LogoRs.png";
                $mpdf->WriteHTML("
               <table width='1000' cellspacing='0' border='0'>
                        <tr>
                                <td width='76'>
                                <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                </td>
                                <td>
                                <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                </td>
                        </tr>
                </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'><u>CATATAN PERKEMBANGAN PASIEN (SOAP)</u></h1>");
            $mpdf->WriteHTML('<table width="786" border = "0" class="t2">
                                <tr>
                                    <td width="94" height="23"><strong>No. Medrec</strong></td>
                                    <td width="10">:</td>
                                    <td width="306">' . $kdpasien . '</td>
                                    <td width="31">&nbsp;</td>
                                    <td width="90" height="23"><strong>Spesialisasi </strong></td>
                                    <td width="10">:</td>
                                    <td width="215">' . $spesial . '</td>
                                </tr>
                                <tr>
                                  <td><strong>Nama</strong></td>
                                  <td>:</td>
                                  <td>' . $namapasien . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kamar</strong></td>
                                    <td>:</td>
                                    <td>' . $kamar . '</td>
                                </tr>
                                <tr>
                                  <td height="23"><strong>Alamat</strong></td>
                                  <td>:</td>
                                  <td>' . $alamat . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kelas</strong></td>
                                    <td>:</td>
                                    <td>' . $kelas . '</td>
                                </tr>
                              </table>');
            $mpdf->WriteHTML('
           <table class="t1" border = "1">
            <thead>
                <tr>
                    <td width="100" align="center"><strong>Tanggal/Jam</strong></td>
                    <td width="200" align="center"><strong>Subject</strong></td>
                    <td width="200" align="center"><strong>Object</strong></td>
                    <td width="200" align="center"><strong>Assusment</strong></td>
                    <td width="200" align="center"><strong>Planing</strong></td>
                    <td width="150" align="center"><strong>Perawat</strong></td>
                </tr>
            </thead>
                <tfoot>

                </tfoot>
           ');

            foreach ($query as $line) {
                $tmpdate = DateTime::createFromFormat('Y-m-d', $line->tgl);
                $realtmpdate = date_format($tmpdate, "d/M/Y");
//                $tmpdate2 = DateTime::createFromFormat('Y-m-d', $line->tgl_selesai);
//                $realtmpdate2 = date_format($tmpdate2, "d/M/Y");
                $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow">
                            <td width="100">' . $realtmpdate . ' ' . substr($line->jam, 0, 5) . '</td>
                            <td width="100">' . $line->s . '</td>
                            <td width="100">' . $line->o . '</td>
                            <td width="100">' . $line->a . '</td>
                            <td width="100">' . $line->p . '</td>
                            <td width="100">' . $line->nama_perawat . '</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
            }


            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/';
            $tmpname = time() . 'ASKEPLapSOAP';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "170202", title : "Laporan Perkembangan pasien", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function LapObservasiTindakan($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 10);

        if (count($Split) > 0) {
            $kd_pasien = $Split[0];
            $kdunit = $Split[1];
            $urut = $Split[2];
            $tgl = $Split[3];
            $kdpasien = $Split[4];
            $namapasien = $Split[5];
            $alamat = $Split[6];
            $spesial = $Split[7];
            $kelas = $Split[8];
            $kamar = $Split[9];
        } else {
            
        }

        $Param = "Where kd_pasien_kunj = '" . $kd_pasien . "' and kd_unit_kunj = '" . $kdunit . "' and urut_masuk_kunj = " . $urut . " and tgl_masuk_kunj = '" . $tgl . "'";

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $q = $this->db->query("select tgl_observasi::date as tgl,jam_observasi::time as jam, nama_perawat,
                                tekanan_darah as td,nadi as n,detak_jantung as p,suhu as s,cvp,
                                wsd,kesadaran,perifer,oral, parenteral,muntah,ngt,bab,bak,pendarahan,tindakan_perawat
                                from askep_observasi ao
                                inner join perawat p on p.kd_perawat = ao.kd_perawat " . $Param);

        if ($q->num_rows == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $query = $q->result();
            $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(297, 210));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
            <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                        word-wrap: break-word;
                }
                .t2 {
                        border: 0px;
                        font-size: 10px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .detail {
                        font-size: 17px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 13px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                
            </style>
           ");
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $logors = base_url() . "ui/images/Logo/LogoRs.png";
                $mpdf->WriteHTML("
               <table width='1000' cellspacing='0' border='0'>
                        <tr>
                                <td width='76'>
                                <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                </td>
                                <td>
                                <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                </td>
                        </tr>
                </table>
            ");
            }

            $mpdf->WriteHTML("<h1 class='formarial' align='center'><u>OBSERVASI DAN TINDAKAN KEPERAWATAN</u></h1>");
            $mpdf->WriteHTML(' <br /><table width="786" border = "0" class="t2">
                                <tr>
                                    <td width="94" height="23"><strong>No. Medrec</strong></td>
                                    <td width="10">:</td>
                                    <td width="306">' . $kdpasien . '</td>
                                    <td width="31">&nbsp;</td>
                                    <td width="90" height="23"><strong>Spesialisasi </strong></td>
                                    <td width="10">:</td>
                                    <td width="215">' . $spesial . '</td>
                                </tr>
                                <tr>
                                  <td><strong>Nama</strong></td>
                                  <td>:</td>
                                  <td>' . $namapasien . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kamar</strong></td>
                                    <td>:</td>
                                    <td>' . $kamar . '</td>
                                </tr>
                                <tr>
                                  <td height="23"><strong>Alamat</strong></td>
                                  <td>:</td>
                                  <td>' . $alamat . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kelas</strong></td>
                                    <td>:</td>
                                    <td>' . $kelas . '</td>
                                </tr>
                              </table>');
            $mpdf->WriteHTML('
                <br />
           <table class="t1" border = "1">
            <thead>
                <tr>
                <td width="90" rowspan="2" align="center"><strong>Tanggal/Jam</strong></td>
                <td width="30" rowspan="2" align="center"><strong>TD</strong></td>
                <td width="30" rowspan="2" align="center"><strong>N</strong></td>
                <td width="30" rowspan="2" align="center"><strong>P</strong></td>
                <td width="30" rowspan="2" align="center"><strong>S</strong></td>
                <td width="30" rowspan="2" align="center"><strong>CVP</strong></td>
                <td width="30" rowspan="2" align="center" ><p><strong>WSD/</strong></p>
                  <p><strong>DRAIN</strong></p></td>
                <td width="90" rowspan="2" align="center"><strong>Kesadaran</strong></td>
                <td width="30" rowspan="2" align="center"><strong>Perifer</strong></td>
                <td height="24" colspan="2" align="center"><strong>Intake</strong></td>
                <td colspan="5" align="center"><strong>OUTPUT</strong></td>
                <td width="150" rowspan="2" align="center"><strong>Perawat</strong></td>
                <td width="200" rowspan="2 align="center""><strong>Tindakan Keperawatan</strong></td>
              </tr>
              <tr>
                <td width="30" align="center">Oral</td>
                <td width="30" align="center">Parental</td>
                <td width="30" align="center">muntah</td>
                <td width="30" align="center">NGT</td>
                <td width="30" align="center">BAB</td>
                <td width="30" align="center">BAK</td>
                <td width="30" align="center">Pendarahan</td>
              </tr>
            </thead>
                <tfoot>

                </tfoot>
           ');

            foreach ($query as $line) {
                $tmpdate = DateTime::createFromFormat('Y-m-d', $line->tgl);
                $realtmpdate = date_format($tmpdate, "d/M/Y");
                $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow">
                            <td>' . $realtmpdate . ' ' . substr($line->jam, 0, 5) . '</td>
                            <td>' . $line->td . '</td>
                            <td>' . $line->n . '</td>
                            <td>' . $line->p . '</td>
                            <td>' . $line->s . '</td>
                            <td>' . $line->cvp . '</td>
                            <td>' . $line->wsd . '</td>
                            <td>' . $line->kesadaran . '</td>
                            <td>' . $line->perifer . '</td>
                            <td>' . $line->oral . '</td>
                            <td>' . $line->parenteral . '</td>
                            <td>' . $line->muntah . '</td>
                            <td>' . $line->ngt . '</td>
                            <td>' . $line->bab . '</td>
                            <td>' . $line->bak . '</td>
                            <td>' . $line->pendarahan . '</td>
                            <td>' . $line->nama_perawat . '</td>
                            <td>' . $line->tindakan_perawat . '</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
            }


            $mpdf->WriteHTML('</tbody></table>');
            $tmpbase = 'base/tmp/';
            $tmpname = time() . 'ASKEPLapOTK';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "170202", title : "Laporan Observasi Dan Tindakan", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function LapAsuhanKeperawatan($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 10);

        if (count($Split) > 0) {
            $kd_pasien = $Split[0];
            $kdunit = $Split[1];
            $urut = $Split[2];
            $tgl = $Split[3];
            $kdpasien = $Split[4];
            $namapasien = $Split[5];
            $alamat = $Split[6];
            $spesial = $Split[7];
            $kelas = $Split[8];
            $kamar = $Split[9];
        } else {
            
        }

        $Param = "Where kd_pasien_kunj = '" . $kd_pasien . "' and kd_unit_kunj = '" . $kdunit . "' and urut_masuk_kunj = " . $urut . " and tgl_masuk_kunj = '" . $tgl . "'";

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $q = $this->db->query("SELECT 
                kd_pasien_kunj, kd_unit_kunj, urut_masuk_kunj, tgl_masuk_kunj, 
                ar.kd_status_pulang, sp.status_pulang, ar.kd_rujukan, r.rujukan, diagnosa_akhir, masalah_keperawatan, 
                mobilisasi_jalan,mobilisasi_tongkat,mobilisasi_kursi_roda,mobilisasi_brankar,
                CASE WHEN alat_tidakada = 't' THEN 'V' else '&nbsp;&nbsp;' END as alat_tidakada,
                CASE WHEN alat_bantu_kateter = 't' THEN 'V' else '&nbsp;&nbsp;' END as alat_bantu_kateter,
                CASE WHEN alat_bantu_oksigen = 't' THEN 'V' else '&nbsp;&nbsp;' END as alat_bantu_oksigen,
                CASE WHEN alat_bantu_infus = 't' THEN 'V' else '&nbsp;&nbsp;' END as alat_bantu_infus,
                CASE WHEN alat_bantu_ngt = 't' THEN 'V' else '&nbsp;&nbsp;' END as alat_bantu_ngt,
                CASE WHEN alat_bantu_lain = 't' THEN 'V' else '&nbsp;&nbsp;' END as alat_bantu_lain,
                alat_bantu_ket,saran_tindakan,
                CASE WHEN penyuluhan_makan = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_makan,
                CASE WHEN penyuluhan_obat = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_obat,
                CASE WHEN penyuluhan_rawatluka = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_rawatluka,
                CASE WHEN penyuluhan_relaksasi = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_relaksasi,
                CASE WHEN penyuluhan_batukefektif = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_batukefektif,
                CASE WHEN penyuluhan_batukfisio = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_batukfisio,
                CASE WHEN penyuluhan_rawatbbl = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_rawatbbl,
                CASE WHEN penyuluhan_alat_bantu = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_alat_bantu,
                CASE WHEN penyuluhan_hearing_aid = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_hearing_aid,
                CASE WHEN penyuluhan_kateter = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_kateter,
                CASE WHEN penyuluhan_larangan = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_larangan,
                CASE WHEN penyuluhan_rawatpayudara = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_rawatpayudara,
                CASE WHEN penyuluhan_larutanpk = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_larutanpk,
                CASE WHEN penyuluhan_diet = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_diet,
                CASE WHEN penyuluhan_ngt = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_ngt,
                CASE WHEN penyuluhan_infus = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_infus,
                CASE WHEN penyuluhan_oksigen = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_oksigen,
                CASE WHEN penyuluhan_lain = 't' THEN 'V' else '&nbsp;&nbsp;' END as penyuluhan_lain,
                penyuluhan_ket
                FROM askep_resume ar
                INNER JOIN rujukan r on ar.kd_rujukan::int = r.kd_rujukan
                INNER JOIN status_pulang sp on ar.kd_status_pulang::smallint = sp.kd_status_pulang " . $Param);

        if ($q->num_rows == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $query = $q->result();
            $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
            <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t2 {
                        border: 0px;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t3{
                    border: 0px;
                    font-size: 10px;
                    font-family: Arial, Helvetica, sans-serif;
                }
                .detail {
                        font-size: 17px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 13px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                .box {
                        float: left;
                        padding: 10px 15px;
                        border: 1px solid #000;
                        margin-right: 10px;
                        height:20px;
                        width:20px;
                }
                
            </style>
           ");
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $logors = base_url() . "ui/images/Logo/LogoRs.png";
                $mpdf->WriteHTML("
               <table width='1000' cellspacing='0' border='0'>
                        <tr>
                                <td width='76'>
                                <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                </td>
                                <td>
                                <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                </td>
                        </tr>
                </table>
            ");
            }

            $mpdf->WriteHTML("<br /><h1 class='formarial' align='center'><u>RESUME ASUHAN KEPERAWATAN</u></h1>");

            $mpdf->WriteHTML('<br /><table width="786" border = "0" class="t2">
                                <tr>
                                    <td width="94" height="23"><strong>No. Medrec</strong></td>
                                    <td width="10">:</td>
                                    <td width="306">' . $kdpasien . '</td>
                                    <td width="31">&nbsp;</td>
                                    <td width="90" height="23"><strong>Spesialisasi </strong></td>
                                    <td width="10">:</td>
                                    <td width="215">' . $spesial . '</td>
                                </tr>
                                <tr>
                                  <td><strong>Nama</strong></td>
                                  <td>:</td>
                                  <td>' . $namapasien . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kamar</strong></td>
                                    <td>:</td>
                                    <td>' . $kamar . '</td>
                                </tr>
                                <tr>
                                  <td height="23"><strong>Alamat</strong></td>
                                  <td>:</td>
                                  <td>' . $alamat . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kelas</strong></td>
                                    <td>:</td>
                                    <td>' . $kelas . '</td>
                                </tr>
                              </table>');
            foreach ($query as $line) {
                $tmpmobilitas = '';
                if ($line->mobilisasi_jalan == 't') {
                    $tmpmobilitas = 'Jalan';
                } else if ($line->mobilisasi_tongkat == 't') {
                    $tmpmobilitas = 'Tongkat';
                } else if ($line->mobilisasi_kursi_roda == 't') {
                    $tmpmobilitas = 'Kursi roda';
                } else if ($line->mobilisasi_brankar == 't') {
                    $tmpmobilitas = 'Brankar';
                } else {
                    $tmpmobilitas = '';
                }
                $mpdf->WriteHTML('
                <br />
                <table class="t3" border = "0">
                <tr>
                    <td>Pasien dirujuk ke</td>
                    <td>:</td>
                    <td>' . $line->rujukan . '</td>
                </tr>
                <tr>
                    <td width="150" height="23">Diagnosa saat pulang</td>
                    <td width="14">:</td>
                    <td>' . $line->diagnosa_akhir . '</td>
                </tr>
                <tr>
                    <td width="150">Masalah keperawatan saat di rawat</td>
                    <td width="14">:</td>
                    <td>' . $line->masalah_keperawatan . '</td>
                </tr>
                <tr>
                    <td>Status pulang</td>
                    <td>:</td>
                    <td>' . $line->status_pulang . '</td>
                </tr>
                </table>');
                $mpdf->WriteHTML('
                <br />
                <table class="t3" border = "0">
                <tr>
                    <td colspan="8"><strong><u>Kondisi Pasien Saat Pulang</u></strong></td>
                </tr>
                <tr>
                    <td width="150" height="23">Mobilitas</td>
                    <td width="14">:</td>
                    <td colspan="6">' . $tmpmobilitas . '</td>
                </tr>
                <tr>
                    <td width="150" height="20">Alat Bantu Yang Terpasang</td>
                    <td width="14">:</td>
                    <td width="32"><div class="box">&nbsp;&nbsp;' . $line->alat_tidakada . '&nbsp;&nbsp;</div></td>
                    <td width="80">Tidak Ada</td>
                    <td width="32"><div class="box">&nbsp;&nbsp;' . $line->alat_bantu_kateter . '&nbsp;&nbsp;</div></td>
                    <td width="80">Kateter</td>
                    <td width="32"><div class="box">&nbsp;&nbsp;' . $line->alat_bantu_oksigen . '&nbsp;&nbsp;</div></td>
                    <td width="80">Oksigen</td>
                    <td width="32"><div class="box">&nbsp;&nbsp;' . $line->alat_bantu_infus . '&nbsp;&nbsp;</div></td>
                    <td width="80">Infus</td>
                </tr>
                <tr>
                    <td width="150" height="20">&nbsp;</td>
                    <td width="14">&nbsp;</td>
                    <td width="32"><div class="box">&nbsp;&nbsp;' . $line->alat_bantu_ngt . '&nbsp;&nbsp;</div></td>
                    <td width="80">NGT</td>
                    <td width="32"><div class="box">&nbsp;&nbsp;' . $line->alat_bantu_lain . '&nbsp;&nbsp;</div></td>
                    <td width="80">lain - Lain</td>
                </tr>
                <tr>
                    <td width="150" height="20">Saran/Tindakan yang perlu di lanjutkan</td>
                    <td width="14">:</td>
                    <td colspan="6">' . $line->saran_tindakan . '</td>
                </tr>
                </table>');
                $mpdf->WriteHTML('
                <br />
                <table width="686" border = "0" class="t3">
                <tr>
                    <td colspan="9"><strong><u>Penyulusan kesehatan yang telah diberikan perihal</u></strong></td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_makan . '&nbsp;&nbsp;</div></td>
                  <td width="263">Pemberian makan/minum</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_alat_bantu . '&nbsp;&nbsp;</div></td>
                  <td width="152">Menggunakan alat bantu</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_diet . '&nbsp;&nbsp;</div></td>
                  <td width="109">Pengaturan diet</td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_obat . '&nbsp;&nbsp;</div></td>
                  <td width="263">pemberian Obat</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_hearing_aid . '&nbsp;&nbsp;</div></td>
                  <td width="152">Hearing Aid</td>
                  <td><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_ngt . '&nbsp;&nbsp;</div></td>
                  <td>NGT</td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_rawatluka . '&nbsp;&nbsp;</div></td>
                  <td width="263">Perawatan Luka</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_kateter . '&nbsp;&nbsp;</div></td>
                  <td width="152">Kateter</td>
                  <td><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_infus . '&nbsp;&nbsp;</div></td>
                  <td>Infus</td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_relaksasi . '&nbsp;&nbsp;</div></td>
                  <td width="263">Melakukan teknik relaksasi</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_larangan . '&nbsp;&nbsp;</div></td>
                  <td width="152">Pantangan/Larangan</td>
                  <td><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_oksigen . '&nbsp;&nbsp;</div></td>
                  <td>Oksigen</td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_batukefektif . '&nbsp;&nbsp;</div></td>
                  <td width="263">Batuk efektif</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_rawatpayudara . '&nbsp;&nbsp;</div></td>
                  <td width="152">Perawatan payudara</td>
                  <td><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_lain . '&nbsp;&nbsp;</div></td>
                  <td>Lain - Lain</td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_batukfisio . '&nbsp;&nbsp;</div></td>
                  <td width="263">Batuk fisioterapi</td>
                  <td width="23"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_larutanpk . '&nbsp;&nbsp;</div></td>
                  <td colspan="3">Membuat larutan PK/Bethadine untuk kebersihan vulva</td>
                </tr>
                <tr>
                  <td width="23" height="20"><div class="box">&nbsp;&nbsp;' . $line->penyuluhan_rawatbbl . '&nbsp;&nbsp;</div></td>
                  <td colspan="5">Perawatan Bayi Batu Lahir (12 pokok pemenuhan kebutuhan bayi)</td>
                </tr>
                
                </table>');
            }
            $tmpbase = 'base/tmp/';
            $tmpname = time() . 'ASKEPLapMutulayanan';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "170201", title : "Laporan Mutu Layanan", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

    public function LapPengkajianKeperawatan($UserID, $Params) {
        $UserID = '0';
        $Split = explode("##@@##", $Params, 10);

        if (count($Split) > 0) {
            $kd_pasien = $Split[0];
            $kdunit = $Split[1];
            $urut = $Split[2];
            $tgl = $Split[3];
            $kdpasien = $Split[4];
            $namapasien = $Split[5];
            $alamat = $Split[6];
            $spesial = $Split[7];
            $kelas = $Split[8];
            $kamar = $Split[9];
        } else {
            
        }

        $Param = "Where kd_pasien_kunj = '" . $kd_pasien . "' and kd_unit_kunj = '" . $kdunit . "' and urut_masuk_kunj = " . $urut . " and tgl_masuk_kunj = '" . $tgl . "'";

        $this->load->library('m_pdf');
        $this->m_pdf->load();

        $q = $this->db->query("
            select kd_pasien_kunj, kd_unit_kunj, urut_masuk_kunj, tgl_masuk_kunj,
            --Riwayat Pasien
            riwayat_utama,
            riwayat_penyakit_sekarang,
            riwayat_penyakit_dahulu,


            --AirWay'n Breathing
            CASE WHEN nafas_paten = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_paten, 
            CASE WHEN nafas_obstruktif = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_obstruktif, 
            CASE WHEN nafas_jelas = 't' THEN 'Ya' else 'Tidak' END as nafas_jelas, 
            CASE WHEN nafas_pola_simetri = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_pola_simetri, 
            CASE WHEN nafas_pola_asimetri = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_pola_asimetri, 
            CASE WHEN nafas_suara_normal = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_suara_normal, 
            CASE WHEN nafas_suara_hipersonor = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_suara_hipersonor, 
            CASE WHEN nafas_menurun = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_menurun, 
            CASE WHEN nafas_jenis_normal = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_jenis_normal, 
            CASE WHEN nafas_jenis_tachypnoe = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_jenis_tachypnoe, 
            CASE WHEN nafas_jenis_cheynestokes = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_jenis_cheynestokes, 
            CASE WHEN nafas_jenis_retractive = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_jenis_retractive, 
            CASE WHEN nafas_jenis_kusmaul = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_jenis_kusmaul, 
            CASE WHEN nafas_jenis_dispnoe = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_jenis_dispnoe, 

            nafas_rr, -- string 

            CASE WHEN nafas_suara_wheezing = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_suara_wheezing,
            CASE WHEN nafas_suara_ronchi = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_suara_ronchi,
            CASE WHEN nafas_suara_rales = 't' THEN 'V' else '&nbsp;&nbsp;' END as nafas_suara_rales,
            CASE WHEN nafas_evakuasi_cairan = 't' THEN 'Ya' else 'Tidak' END as nafas_evakuasi_cairan,

            nafas_jml_cairan, nafas_warna_cairan, nafas_masalah, -- string 

            -- Circulation
            CASE WHEN jantung_reguler = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_reguler,
            CASE WHEN jantung_irreguler = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_irreguler,
            CASE WHEN jantung_s1s2_tunggal = 't' THEN 'Ya' else 'Tidak' END as jantung_s1s2_tunggal,

            CASE WHEN jantung_nyeri_dada = 't' THEN 'Ya' else 'Tidak' END as jantung_nyeri_dada,
            
            jantung_nyeri_dada_ket, -- string 

            CASE WHEN jantung_bunyi_murmur = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_bunyi_murmur,
            CASE WHEN jantung_bunyi_gallop = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_bunyi_gallop,
            CASE WHEN jantung_bunyi_bising = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_bunyi_bising,

            CASE WHEN jantung_crt = 't' THEN '<3 Dtk' else '>3 Dtk' END as jantung_crt,


            CASE WHEN jantung_akral_hangat = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_akral_hangat,
            CASE WHEN jantung_akral_dingin = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_akral_dingin,

            CASE WHEN jantung_peningkatan_jvp = 't' THEN 'Ya' else 'Tidak' END as jantung_peningkatan_jvp,

            jantung_ukuran_cvp, -- string 

            CASE WHEN jantung_warna_jaundice = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_warna_jaundice,
            CASE WHEN jantung_warna_sianotik = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_warna_sianotik,
            CASE WHEN jantung_warna_kemerahan = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_warna_kemerahan,
            CASE WHEN jantung_warna_pucat = 't' THEN 'V' else '&nbsp;&nbsp;' END as jantung_warna_pucat,

            jantung_tekanan_darah, jantung_nadi, jantung_temp, jantung_masalah, -- string 

            --musculo
            CASE WHEN otot_sendi_bebas = 't' THEN 'V' else '&nbsp;&nbsp;' END as otot_sendi_bebas,
            CASE WHEN otot_sendi_terbatas = 't' THEN 'V' else '&nbsp;&nbsp;' END as otot_sendi_terbatas,

            CASE WHEN otot_kekuatan = 't' THEN 'Ya' else 'Tidak' END as otot_kekuatan,
            CASE WHEN otot_odema_ekstrimitas = 't' THEN 'Ya' else 'Tidak' END as otot_odema_ekstrimitas,
            CASE WHEN otot_kelainan_bentuk = 't' THEN 'Ya' else 'Tidak' END as otot_kelainan_bentuk,
            CASE WHEN otot_krepitasi = 't' THEN 'Ya' else 'Tidak' END as otot_krepitasi,

            otot_masalah, -- string 

            -- saraf
            CASE WHEN syaraf_gcs_eye = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_gcs_eye,
            CASE WHEN syaraf_gcs_verbal = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_gcs_verbal,
            CASE WHEN syaraf_gcs_motorik = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_gcs_motorik,

            syaraf_gcs_total, -- string 

            CASE WHEN syaraf_fisiologis_brachialis = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_fisiologis_brachialis,
            CASE WHEN syaraf_fisiologis_patella = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_fisiologis_patella,
            CASE WHEN syaraf_fisiologis_achilles = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_fisiologis_achilles,
            CASE WHEN syaraf_patologis_choddoks = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_patologis_choddoks,
            CASE WHEN syaraf_patologis_babinski = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_patologis_babinski,
            CASE WHEN syaraf_patologis_budzinzky = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_patologis_budzinzky,
            CASE WHEN syaraf_patologis_kernig = 't' THEN 'V' else '&nbsp;&nbsp;' END as syaraf_patologis_kernig,

            syaraf_masalah, -- string 

            -- Pengindraan
            CASE WHEN indra_pupil_isokor = 't' THEN 'V' else '&nbsp;&nbsp;' END as indra_pupil_isokor,
            CASE WHEN indra__pupil_anisokor = 't' THEN 'V' else '&nbsp;&nbsp;' END as indra__pupil_anisokor,
            CASE WHEN indra__konjungtiva_anemis = 't' THEN 'V' else '&nbsp;&nbsp;' END as indra__konjungtiva_anemis,
            CASE WHEN indra__konjungtiva_icterus = 't' THEN 'V' else '&nbsp;&nbsp;' END as indra__konjungtiva_icterus,
            CASE WHEN indra__konjungtiva_tidak = 't' THEN 'V' else '&nbsp;&nbsp;' END as indra__konjungtiva_tidak,

            CASE WHEN indra_gangguan_dengar = 't' THEN 'Ya' else 'Tidak' END as indra_gangguan_dengar,
            CASE WHEN indra_otorhea = 't' THEN 'Ya' else 'Tidak' END as indra_otorhea,
            CASE WHEN indra_bentuk_hidung = 't' THEN 'Ya' else 'Tidak' END as indra_bentuk_hidung,

            indra_bentuk_hidung_ket, -- string 

            CASE WHEN indra_gangguan_cium = 't' THEN 'Ya' else 'Tidak' END as indra_gangguan_cium,
            CASE WHEN indra_rhinorhea = 't' THEN 'Ya' else 'Tidak' END as indra_rhinorhea,

            indra_masalah, -- string 


            CASE WHEN kemih_kebersihan = 't' THEN 'Bersih' else 'Kotor' END as kemih_kebersihan,
            CASE WHEN kemih_alat_bantu = 't' THEN 'Ya' else 'Tidak' END as kemih_alat_bantu,

            kemih_jml_urine, kemih_warna_urine, kemih_bau, -- string 

            CASE WHEN kemih_kandung_membesar = 't' THEN 'Ya' else 'Tidak' END as kemih_kandung_membesar,
            CASE WHEN kemih_nyeri_tekan = 't' THEN 'Ya' else 'Tidak' END as kemih_nyeri_tekan,

            CASE WHEN kemih_gangguan_anuria = 't' THEN 'V' else '&nbsp;&nbsp;' END as kemih_gangguan_anuria,
            CASE WHEN kemih_gangguan_aliguria = 't' THEN 'V' else '&nbsp;&nbsp;' END as kemih_gangguan_aliguria,
            CASE WHEN kemih_gangguan_retensi = 't' THEN 'V' else '&nbsp;&nbsp;' END as kemih_gangguan_retensi,
            CASE WHEN kemih_gangguan_inkontinensia = 't' THEN 'V' else '&nbsp;&nbsp;' END as kemih_gangguan_inkontinensia,
            CASE WHEN kemih_gangguan_disuria = 't' THEN 'V' else '&nbsp;&nbsp;' END as kemih_gangguan_disuria,
            CASE WHEN kemih_gangguanhematuria = 't' THEN 'V' else '&nbsp;&nbsp;' END as kemih_gangguanhematuria,

            kemih_masalah, -- string 

            CASE WHEN cerna_makan_habis = 't' THEN 'Habis' else 'Tidak' END as cerna_makan_habis, 

            cerna_makan_ket, cerna_makan_frekuensi, cerna_jml_minum, cerna_jenis_minum,  -- string 

            CASE WHEN cerna_mulut_bersih = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_mulut_bersih,
            CASE WHEN cerna_mulut_kotor = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_mulut_kotor,
            CASE WHEN cerna_mulut_berbau = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_mulut_berbau,
            CASE WHEN cerna_mukosa_lembab = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_mukosa_lembab,
            CASE WHEN cerna_mukosa_kering = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_mukosa_kering,
            CASE WHEN cerna_mukosa_stomatitis = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_mukosa_stomatitis,
            CASE WHEN cerna_tenggorokan_sakit = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_tenggorokan_sakit,
            CASE WHEN cerna_tenggorokan_nyeri_tekan = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_tenggorokan_nyeri_tekan,
            CASE WHEN cerna_tenggorokan_pembesaranto = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_tenggorokan_pembesaranto,
            CASE WHEN cerna_perut_normal = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_perut_normal,
            CASE WHEN cerna_perut_distended = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_perut_distended,
            CASE WHEN cerna_perut_meteorismus = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_perut_meteorismus,
            CASE WHEN cerna_perut_nyeri_tekan = 't' THEN 'V' else '&nbsp;&nbsp;' END as cerna_perut_nyeri_tekan,

            cerna_perut_lokasi_nyeri, cerna_peristaltik,

            CASE WHEN cerna_turgor_kulit = 't' THEN 'Ya' else 'Tidak' END as cerna_turgor_kulit,
            CASE WHEN cerna_pembesaran_hepar = 't' THEN 'Ya' else 'Tidak' END as cerna_pembesaran_hepar,
            CASE WHEN cerna_hematemesis = 't' THEN 'Ya' else 'Tidak' END as cerna_hematemesis,
            CASE WHEN cerna_evakuasi_cairan_ascites = 't' THEN 'Ya' else 'Tidak' END as cerna_evakuasi_cairan_ascites,

            cerna_jml_cairan_ascites, cerna_warna_cairan_ascites, -- string
            cerna_frek_bab,cerna_konsistensi, cerna_bau_bab, cerna_warna_bab, cerna_masalah, -- string

            -- endrokrin
            CASE WHEN endokrin_tyroid = 't' THEN 'Ya' else 'Tidak' END as endokrin_tyroid,
            CASE WHEN endokrin_hipoglikemia = 't' THEN 'Ya' else 'Tidak' END as endokrin_hipoglikemia,
            CASE WHEN endokrin_luka_gangren = 't' THEN 'Ya' else 'Tidak' END as endokrin_luka_gangren,
            CASE WHEN endokrin__pus = 't' THEN 'Ya' else 'Tidak' END as endokrin__pus,

            endokrin_masalah, -- string

            --personal
            personal_mandi, -- string
            personal_sikatgigi,  -- string
            personal_keramas, -- string
            personal_gantipakaian, -- string
            personal_masalah,  -- string

            -- sosial
            psikososial_orangdekat,  -- string
            psikososial_kegiatan_ibadah, -- string
            psikososial_masalah, -- string

            --penunjang
            terapi_penunjang -- string


            from askep_periksa " . $Param);

        if ($q->num_rows == 0) {
            $res = '{ success : false, msg : "No Records Found"}';
        } else {
            $query = $q->result();
            $queryRS = $this->db->query("select * from db_rs order by code asc limit 1")->result();
            $queryuser = $this->db->query("select * from zusers where kd_user= " . "'" . $UserID . "'")->result();
            foreach ($queryuser as $line) {
                $kduser = $line->kd_user;
                $nama = $line->user_names;
            }
            $no = 0;
            $mpdf = new mPDF('utf-8', array(210, 297));

            $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
            $mpdf->pagenumPrefix = 'Hal : ';
            $mpdf->pagenumSuffix = '';
            $mpdf->nbpgPrefix = ' Dari ';
            $mpdf->nbpgSuffix = '';
            $date = date("d-M-Y / H:i:s");
            $arr = array(
                'odd' => array(
                    'L' => array(
                        'content' => 'Operator : (' . $kduser . ') ' . $nama,
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'R' => array(
                        'content' => '{PAGENO}{nbpg}',
                        'font-size' => 8,
                        'font-style' => '',
                        'font-family' => 'serif',
                        'color' => '#000000'
                    ),
                    'line' => 0,
                ),
                'even' => array()
            );
            $mpdf->SetFooter($arr);

            $mpdf->WriteHTML("
            <style>
                .t1 {
                        border: 1px solid black;
                        border-collapse: collapse;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t2 {
                        border: 0px;
                        font-size: 12px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .t3{
                    border: 0px;
                    font-size: 10px;
                    font-family: Arial, Helvetica, sans-serif;
                }
                .detail {
                        font-size: 17px;
                        font-family: Arial, Helvetica, sans-serif;
                }
                .formarial {
                        font-family: Arial, Helvetica, sans-serif;
                }
                h1 {
                        font-size: 13px;
                }

                h2 {
                        font-size: 10px;
                }

                h3 {
                        font-size: 8px;
                }

                table2 {
                        border: 1px solid white;
                }
                .box {
                        float: left;
                        padding: 10px 15px;
                        border: 1px solid #000;
                        margin-right: 10px;
                        height:20px;
                        width:20px;
                }
                
            </style>
           ");
            foreach ($queryRS as $line) {
                if ($line->phone2 == null || $line->phone2 == '') {
                    $telp = $line->phone1;
                } else {
                    $telp = $line->phone1 . " / " . $line->phone2;
                }
                if ($line->fax == null || $line->fax == '') {
                    $fax = "";
                } else {
                    $fax = "Fax. " . $line->fax;
                }
                $logors = base_url() . "ui/images/Logo/LogoRs.png";
                $mpdf->WriteHTML("
               <table width='1000' cellspacing='0' border='0'>
                        <tr>
                                <td width='76'>
                                <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
                                </td>
                                <td>
                                <b><font style='font-size: 15px; font-family: Arial, Helvetica, sans-serif;'>" . $line->name . "</font></b><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>" . $line->address . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. " . $telp . "</font><br>
                                <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
                                </td>
                        </tr>
                </table>
            ");
            }

            $mpdf->WriteHTML("<br /><h1 class='formarial' align='center'><u>Pengkajian Keperawatan</u></h1>");

            $mpdf->WriteHTML('<br /><table width="786" border = "0" class="t2">
                                <tr>
                                    <td width="94" height="23"><strong>No. Medrec</strong></td>
                                    <td width="10">:</td>
                                    <td width="306">' . $kdpasien . '</td>
                                    <td width="31">&nbsp;</td>
                                    <td width="90" height="23"><strong>Spesialisasi </strong></td>
                                    <td width="10">:</td>
                                    <td width="215">' . $spesial . '</td>
                                </tr>
                                <tr>
                                  <td><strong>Nama</strong></td>
                                  <td>:</td>
                                  <td>' . $namapasien . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kamar</strong></td>
                                    <td>:</td>
                                    <td>' . $kamar . '</td>
                                </tr>
                                <tr>
                                  <td height="23"><strong>Alamat</strong></td>
                                  <td>:</td>
                                  <td>' . $alamat . '</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Kelas</strong></td>
                                    <td>:</td>
                                    <td>' . $kelas . '</td>
                                </tr>
                              </table>');
            foreach ($query as $line) {
                $tmpmobilitas = '';
                if ($line->mobilisasi_jalan == 't') {
                    $tmpmobilitas = 'Jalan';
                } else if ($line->mobilisasi_tongkat == 't') {
                    $tmpmobilitas = 'Tongkat';
                } else if ($line->mobilisasi_kursi_roda == 't') {
                    $tmpmobilitas = 'Kursi roda';
                } else if ($line->mobilisasi_brankar == 't') {
                    $tmpmobilitas = 'Brankar';
                } else {
                    $tmpmobilitas = '';
                }
                $mpdf->WriteHTML('
                <br />
            <table class="t2" border = "0">
                <tr>
                    <td height="20" colspan="3"><strong>Riwayat Pasien<hr style="margin-top:-1px;" /></strong></td>
                </tr>
                <tr>
                    <td width="198">Keluhan Utama</td>
                    <td width="3">:</td>
                    <td width="551">' . $line->riwayat_utama . '</td>
                </tr>
                <tr>
                    <td>Riwayat Penyakit Sekarang</td>
                    <td>:</td>
                    <td>' . $line->riwayat_penyakit_sekarang . '</td>
                </tr>
                <tr>
                    <td>Riwayat Penyakit terdahulu</td>
                    <td>:</td>
                    <td>' . $line->riwayat_penyakit_dahulu . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td height="20" width="752" colspan="3"><strong>Pemeriksaan Fisik<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>AirWayn Breathing<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Jalan Napas</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_paten . '&nbsp;&nbsp;</div></td>
                    <td width="80">Paten</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_obstruktif . '&nbsp;&nbsp;</div></td>
                    <td width="80">Obstruktuif</td>
                </tr>
                <tr>
                    <td width="175" height="20">Jelas</td>
                    <td width="3">:</td>
                    <td width="23">' . $line->nafas_jelas . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Pola Napas</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_pola_simetri . '&nbsp;&nbsp;</div></td>
                    <td width="80">Simetris</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_pola_asimetri . '&nbsp;&nbsp;</div></td>
                    <td width="80">Asimetris</td>
                </tr>
                <tr>
                    <td width="175" height="20">Suara lapang Paru</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_suara_normal . '&nbsp;&nbsp;</div></td>
                    <td width="80">Normal/Sonor</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_suara_hipersonor . '&nbsp;&nbsp;</div></td>
                    <td width="80">Hypersonor</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_menurun . '&nbsp;&nbsp;</div></td>
                    <td width="80">Menurun</td>
                </tr>
                <tr>
                    <td width="175" height="20">Jenis</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_jenis_normal . '&nbsp;&nbsp;</div></td>
                    <td width="80">Normal</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_jenis_tachypnoe . '&nbsp;&nbsp;</div></td>
                    <td width="80">Tachypnoe</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_jenis_cheynestokes . '&nbsp;&nbsp;</div></td>
                    <td width="80">Cheyne Stokes</td>
                </tr>
                <tr>
                    <td width="175" height="20"></td>
                    <td width="3"></td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_jenis_retractive . '&nbsp;&nbsp;</div></td>
                    <td width="80">Retractive</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_jenis_kusmaul . '&nbsp;&nbsp;</div></td>
                    <td width="80">Kusmaul</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_jenis_dispnoe . '&nbsp;&nbsp;</div></td>
                    <td width="80">Dispnoe</td>
                </tr>
                <tr>
                    <td width="175" height="20">RR</td>
                    <td width="3">:</td>
                    <td width="23">' . $line->nafas_rr . '</td>
                    <td width="80">x/mnt</td>
                </tr>
                <tr>
                    <td width="175" height="20">Suara nafas tambahan</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_suara_wheezing . '&nbsp;&nbsp;</div></td>
                    <td width="80">Wheezing</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_suara_ronchi . '&nbsp;&nbsp;</div></td>
                    <td width="80">Ronchi</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->nafas_suara_rales . '&nbsp;&nbsp;</div></td>
                    <td width="80">Rales</td>
                </tr>
            </table>
                
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Evakuasi Cairan Pleura</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->nafas_evakuasi_cairan . '</td>
                    <td width="20">Jumlah</td>
                    <td width="3">:</td>
                    <td width="63">' . $line->nafas_jml_cairan . ' cc</td>
                    <td width="20">Warna</td>
                    <td width="3">:</td>
                    <td width="94">' . $line->nafas_warna_cairan . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->nafas_masalah . '</td>
                </tr>
            </table>
            <br />
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Circulation<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Irama jantung</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_reguler . '&nbsp;&nbsp;</div></td>
                    <td width="80">Reguler</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_irreguler . '&nbsp;&nbsp;</div></td>
                    <td width="50">Ireguler</td>
                    <td>&nbsp;</td>
                    <td width="100" colspan="2">S1/S2 Tunggal:</td>
                    <td>' . $line->jantung_s1s2_tunggal . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Nyeri Dada</td>
                    <td width="3">:</td>
                    <td width="23">' . $line->jantung_nyeri_dada . '</td>
                </tr>
                <tr>
                    <td>Jelaskan</td>
                    <td width="3">:</td>
                    <td colspan="9">' . $line->jantung_nyeri_dada_ket . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Bunyi jantung tambahan</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_bunyi_murmur . '&nbsp;&nbsp;</div></td>
                    <td width="80">Murmur</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_bunyi_gallop . '&nbsp;&nbsp;</div></td>
                    <td width="80">Gallop</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_bunyi_bising . '&nbsp;&nbsp;</div></td>
                    <td width="80">Bising sistolik</td>
                </tr>
                <tr>
                    <td>CRT</td>
                    <td width="3">:</td>
                    <td colspan="9">' . $line->jantung_crt . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Akral</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_akral_hangat . '&nbsp;&nbsp;</div></td>
                    <td width="80">Hangat</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_akral_dingin . '&nbsp;&nbsp;</div></td>
                    <td width="80">Dingin</td>
                </tr>
            </table>
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Peningkatan JVP</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->jantung_peningkatan_jvp . '</td>
                    <td width="20">Ukuran</td>
                    <td width="3">:</td>
                    <td width="63">' . $line->jantung_ukuran_cvp . ' cmHg</td>
                </tr>
            </table>
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Warna Kulit</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_warna_jaundice . '&nbsp;&nbsp;</div></td>
                    <td width="85">Jaudice</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_warna_sianotik . '&nbsp;&nbsp;</div></td>
                    <td width="80">Sianotik</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_warna_kemerahan . '&nbsp;&nbsp;</div></td>
                    <td width="80">Kemerahan</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->jantung_warna_pucat . '&nbsp;&nbsp;</div></td>
                    <td width="80">Pucat</td>
                </tr>
            </table>
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Tekanan Darah</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->jantung_tekanan_darah . ' mmHg</td>
                    <td width="20">Nadi</td>
                    <td width="3">:</td>
                    <td width="75">' . $line->jantung_nadi . ' x/mnt</td>
                    <td width="20">Temp</td>
                    <td width="3">:</td>
                    <td width="94">' . $line->jantung_temp . ' C</td>
                </tr>
            </table>
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->jantung_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Musculo<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Kemampuan Pergerakan Sendi</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->otot_sendi_bebas . '&nbsp;&nbsp;</div></td>
                    <td width="85">Bebas</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->otot_sendi_terbatas . '&nbsp;&nbsp;</div></td>
                    <td width="80">Terbatas</td>
                </tr>
            </table>

            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Kekuatan Otot</td>
                    <td width="3">:</td>
                    <td colspan="2">' . $line->otot_kekuatan . '</td>
                </tr>
                <tr>
                  <td>Odema ekstrimitas</td>
                  <td width="3">:</td>
                  <td colspan="2">' . $line->otot_odema_ekstrimitas . '</td>
                </tr>
                <tr>
                  <td>Kelainan bentuk</td>
                  <td>:</td>
                  <td colspan="2">' . $line->otot_kelainan_bentuk . '</td>
                </tr>
                <tr>
                  <td>Krepitas</td>
                  <td>:</td>
                  <td colspan="2">' . $line->otot_krepitasi . '</td>
                </tr>
                <tr>
                  <td>Masalah</td>
                  <td>:</td>
                  <td colspan="2">' . $line->otot_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Saraf<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">GCS</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_gcs_eye . '&nbsp;&nbsp;</div></td>
                    <td width="80">Eye</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_gcs_verbal . '&nbsp;&nbsp;</div></td>
                    <td width="80">Verval</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_gcs_motorik . '&nbsp;&nbsp;</div></td>
                    <td width="80">Motorik</td>
                </tr>
                <tr>
                    <td width="175" height="20">Bebas</td>
                    <td width="3">:</td>
                    <td width="23">' . $line->syaraf_gcs_total . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Reflek fisiologis</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_fisiologis_brachialis . '&nbsp;&nbsp;</div></td>
                    <td width="80">Brachialis</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_fisiologis_patella . '&nbsp;&nbsp;</div></td>
                    <td width="80">Patella</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_fisiologis_achilles . '&nbsp;&nbsp;</div></td>
                    <td width="80">Archilles</td>
                </tr>
                <tr>
                    <td width="175" height="20">Reflek patologis</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_patologis_choddoks . '&nbsp;&nbsp;</div></td>
                    <td width="80">Choddoks</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_patologis_babinski . '&nbsp;&nbsp;</div></td>
                    <td width="80">Babinski</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_patologis_budzinzky . '&nbsp;&nbsp;</div></td>
                    <td width="80">Budzinky</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->syaraf_patologis_kernig . '&nbsp;&nbsp;</div></td>
                    <td width="80">Kerning</td>
                </tr>
                <tr>
                    <td height="20">masalah</td>
                    <td>:</td>
                    <td colspan="8">' . $line->syaraf_masalah . '</td>
                </tr>
            </table>
            
<!-- <pagebreak /> -->
            <br />
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Pengindraan<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Pupil</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->indra_pupil_isokor . '&nbsp;&nbsp;</div></td>
                    <td width="80">Isokor</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->indra__pupil_anisokor . '&nbsp;&nbsp;</div></td>
                    <td width="80">Anisokor</td>
                </tr>
                <tr>
                    <td width="175" height="20">Konjungtiva/sklera</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->indra__konjungtiva_anemis . '&nbsp;&nbsp;</div></td>
                    <td width="80">Anemis</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->indra__konjungtiva_icterus . '&nbsp;&nbsp;</div></td>
                    <td width="80">Icterus</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->indra__konjungtiva_tidak . '&nbsp;&nbsp;</div></td>
                    <td width="80">Tidak</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Gangguan Pendengaran</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->indra_gangguan_dengar . '</td>
                    <td width="20">Otorhea</td>
                    <td width="3">:</td>
                    <td width="63">' . $line->indra_otorhea . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Bentuk Hidung</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->indra_bentuk_hidung . '</td>
                    <td width="20">Jelaskan</td>
                    <td width="3">:</td>
                    <td width="63">' . $line->indra_bentuk_hidung_ket . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Gangguan penciuman</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->indra_gangguan_cium . '</td>
                    <td width="20">Rhinorhea</td>
                    <td width="3">:</td>
                    <td width="63">' . $line->indra_rhinorhea . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->indra_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Perkemihan<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Kebersihan</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->kemih_kebersihan . '</td>
                    <td width="80">Alat Bantu</td>
                    <td width="3">:</td>
                    <td width="55">' . $line->kemih_alat_bantu . '</td>
                    <td width="20"></td>
                    <td width="3"></td>
                    <td width="63"></td>
                </tr>
                <tr>
                    <td >Urine</td>
                    <td>:</td>
                    <td >' . $line->kemih_jml_urine . ' cc/hr</td>
                    <td >Warna</td>
                    <td >:</td>
                    <td >' . $line->kemih_warna_urine . '</td>
                    <td >Bau</td>
                    <td >:</td>
                    <td >' . $line->kemih_bau . '</td>
                </tr>
                <tr>
                    <td >Kandung Kencing</td>
                    <td >:</td>
                    <td >Membesar ' . $line->kemih_kandung_membesar . '</td>
                    <td >Nyeri Tekan</td>
                    <td >:</td>
                    <td >' . $line->kemih_nyeri_tekan . '</td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Gangguan</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->kemih_gangguan_anuria . '&nbsp;&nbsp;</div></td>
                    <td width="80">Anuria</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->kemih_gangguan_aliguria . '&nbsp;&nbsp;</div></td>
                    <td width="80">Oliguria</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->kemih_gangguan_retensi . '&nbsp;&nbsp;</div></td>
                    <td width="80">Retensi</td>
                </tr>
                <tr>
                    <td width="175" height="20">&nbsp;</td>
                    <td width="3">&nbsp;</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->kemih_gangguan_inkontinensia . '&nbsp;&nbsp;</div></td>
                    <td width="80">Inkontinensia</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->kemih_gangguan_disuria . '&nbsp;&nbsp;</div></td>
                    <td width="80">Disuria</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->kemih_gangguanhematuria . '&nbsp;&nbsp;</div></td>
                    <td width="80">Hematuria</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->kemih_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Pencernaan<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Porsi Makan</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->cerna_makan_habis . '</td>
                    <td width="20">Keterangan</td>
                    <td width="3">:</td>
                    <td width="100">' . $line->cerna_makan_ket . ' Porsi</td>
                    <td width="20">Frekuensi</td>
                    <td width="3">:</td>
                    <td width="63">' . $line->cerna_makan_frekuensi . ' x/hr</td>
                </tr>
                <tr>
                    <td >Minum</td>
                    <td>:</td>
                    <td >' . $line->cerna_jml_minum . ' cc/hr</td>
                    <td >Jenis</td>
                    <td >:</td>
                    <td colspan="4">' . $line->cerna_jenis_minum . '</td>
                </tr>
            </table>
            
            <table class="t2" border = "0">
                <tr>
                    <td width="175" height="20">Mulut</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_mulut_bersih . '&nbsp;&nbsp;</div></td>
                    <td width="80">Bersih</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_mulut_kotor . '&nbsp;&nbsp;</div></td>
                    <td width="80">Kotor</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_mulut_berbau . '&nbsp;&nbsp;</div></td>
                    <td width="80">Berbau</td>
                </tr>
                <tr>
                    <td width="175" height="20">Mukosa</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_mukosa_lembab . '&nbsp;&nbsp;</div></td>
                    <td width="80">Lembab</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_mukosa_kering . '&nbsp;&nbsp;</div></td>
                    <td width="80">Kering</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_mukosa_stomatitis . '&nbsp;&nbsp;</div></td>
                    <td width="80">Stomatis</td>
                </tr>
                <tr>
                    <td width="175" height="20">Tenggorokan</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_tenggorokan_sakit . '&nbsp;&nbsp;</div></td>
                    <td width="80">Sakit/Sulit Menelan</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_tenggorokan_nyeri_tekan . '&nbsp;&nbsp;</div></td>
                    <td width="80">Nyeri Tekan</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_tenggorokan_pembesaranto . '&nbsp;&nbsp;</div></td>
                    <td width="100">Pembesaran tonsil</td>
                </tr>
                <tr>
                    <td width="175" height="20">Perut</td>
                    <td width="3">:</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_perut_normal . '&nbsp;&nbsp;</div></td>
                    <td width="80">Normal</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_perut_distended . '&nbsp;&nbsp;</div></td>
                    <td width="80">Distended</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_perut_meteorismus . '&nbsp;&nbsp;</div></td>
                    <td width="100">Meteorismus</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">&nbsp;</td>
                    <td width="3">&nbsp;</td>
                    <td width="23"><div class="box">&nbsp;&nbsp;' . $line->cerna_perut_nyeri_tekan . '&nbsp;&nbsp;</div></td>
                    <td width="80">Nyeri Tekan</td>
                    <td width="41">Lokasi</td>
                    <td width="10">:</td>
                    <td width="200">' . $line->cerna_perut_lokasi_nyeri . '</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Peristaltik</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->cerna_peristaltik . ' x/mnt</td>
                    <td width="81">Tugor Kulit</td>
                    <td width="10">:</td>
                    <td colspan="4">' . $line->cerna_turgor_kulit . '</td>
                </tr>
                <tr>
                    <td >Pembesaran hepar</td>
                    <td>:</td>
                    <td >' . $line->cerna_pembesaran_hepar . '</td>
                    <td >Hematesis</td>
                    <td >:</td>
                    <td width="35" >' . $line->cerna_hematemesis . '</td>
                    <td width="15" ></td>
                    <td width="6" ></td>
                    <td width="46" ></td>
                </tr>
                <tr>
                    <td >Evakuasi Cairan</td>
                    <td>:</td>
                    <td >' . $line->cerna_evakuasi_cairan_ascites . '</td>
                    <td >Jumlah</td>
                    <td >:</td>
                    <td width="70" >' . $line->cerna_jml_cairan_ascites . ' cc</td>
                    <td width="15" >Warna</td>
                    <td width="6" >:</td>
                    <td width="46" >' . $line->cerna_warna_cairan_ascites . '</td>
                </tr>
                <tr>
                    <td >Buang air Besar</td>
                    <td>:</td>
                    <td >' . $line->cerna_frek_bab . ' x/hr</td>
                    <td >Konsintensi</td>
                    <td >:</td>
                    <td width="70" >' . $line->cerna_konsistensi . '</td>
                    <td width="15" >Bau</td>
                    <td width="6" >:</td>
                    <td width="70" >' . $line->cerna_bau_bab . '</td>
                    <td width="15" >Warna</td>
                    <td width="6" >:</td>
                    <td width="46" >' . $line->cerna_warna_bab . '</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->cerna_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Endokrin<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Tyroid</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->endokrin_tyroid . '</td>
                    <td width="20">Hiper/hipoglikernia</td>
                    <td width="3">:</td>
                    <td width="100">' . $line->endokrin_hipoglikemia . '</td>
                    <td width="20"></td>
                    <td width="3"></td>
                    <td width="63"></td>
                </tr>
                <tr>
                    <td >Luka Gangren</td>
                    <td>:</td>
                    <td >' . $line->endokrin_luka_gangren . '</td>
                    <td >Pus</td>
                    <td >:</td>
                    <td colspan="4">' . $line->endokrin__pus . '</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->endokrin_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Personal - Hygiene<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">mandi</td>
                    <td width="3">:</td>
                    <td width="105">' . $line->personal_mandi . '</td>
                    <td width="20">Keramas</td>
                    <td width="3">:</td>
                    <td width="100">' . $line->personal_keramas . '</td>
                    <td width="20"></td>
                    <td width="3"></td>
                    <td width="63"></td>
                </tr>
                <tr>
                    <td >Sikat Gigi</td>
                    <td>:</td>
                    <td >' . $line->personal_sikatgigi . '</td>
                    <td >Ganti Pakaian</td>
                    <td >:</td>
                    <td colspan="4">' . $line->personal_gantipakaian . '</td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->personal_masalah . '</td>
                </tr>
            </table>
            
            <br />
            
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Psiko-Sosial Spiritual<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Orang Paling Dekat</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->psikososial_orangdekat . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Kegiatan Ibadah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->psikososial_kegiatan_ibadah . '</td>
                </tr>
                <tr>
                    <td width="175" height="20">Masalah</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->psikososial_masalah . '</td>
                </tr>
            </table>
            
<!-- <pagebreak /> -->
            <br />
            <table class="t2" border = "0">
                <tr>
                    <td colspan="3"><strong>Data Penunjang (Lab, Foto, USG, DLL)<hr style="margin-top:-1px;" /></strong></td>
                </tr>
            </table>
            
            <table border = "0" class="t2">
                <tr>
                    <td width="175" height="20">Terapi</td>
                    <td width="3">:</td>
                    <td colspan="7">' . $line->terapi_penunjang . '</td>
                </tr>
            </table>
                ');
            }
            $tmpbase = 'base/tmp/';
            $tmpname = time() . 'ASKEPLapMutulayanan';
            $mpdf->Output($tmpbase . $tmpname . '.pdf', 'F');

            $res = '{ success : true, msg : "", id : "170201", title : "Laporan Mutu Layanan", url : "' . base_url() . $tmpbase . $tmpname . '.pdf' . '"' . '}';
        }


        echo $res;
    }

}

?>