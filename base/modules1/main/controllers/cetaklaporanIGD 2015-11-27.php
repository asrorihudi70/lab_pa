<?php
class cetaklaporanIGD extends  MX_Controller {
     public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {
//          $this->load->view('laporan/Rep010205',$data=array('controller'=>$this));
    }
    
    public function cetaklaporan()
    {
        
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";
        $this->$ModuleID($UserID,$Params);
        
    }

        // laporan KIUP
    public function rep020201($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tgl = $Split[0];
                            $tgl2 = $Split[1];
                            $sort = $Split[2];
                                                          
                                $Param = "Where K.Tgl_Masuk Between "."'".$tgl."'"." AND "."'".$tgl2."'"." AND  left(u.KD_Unit, 1) = '3'  
                                        GROUP BY K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,p.Jenis_Kelamin,a.Agama,pk.pekerjaan  
                                        ORDER BY ".$sort."  , Kab.Kabupaten, Kec.Kecamatan";
                }
                else {
                        
                }
                if ($tgl === $tgl2)
                {
                    $kriteria = "Periode ".$tgl;
                }else
                {
                    $kriteria = "Periode ".$tgl." s/d ".$tgl2;
                }
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        
        $q = $this->db->query("SELECT K.kd_Pasien, P.Nama, P.Tgl_Lahir, P.Alamat,  Kab.Kabupaten, Kec.kecamatan  ,Case When p.Jenis_Kelamin='t' Then 'L' Else 'P' end as JK, a.Agama,pk.pekerjaan -- k.tgl_masuk
        From Kunjungan k 
        INNER JOIN Unit u On u.kd_Unit=k.kd_Unit 
        INNER JOIN Pasien p 
        INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan 
        INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan 
        INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten 
        INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi ON P.Kd_pasien=k.Kd_pasien 
        INNER JOIN Agama a On a.kd_agama=p.kd_agama  
        INNER JOIN Pekerjaan Pk On Pk.kd_pekerjaan=p.kd_pekerjaan ".$Param);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>KARTU INDENTITAS UTAMA PASIEN IGD</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
               <th width="28" height="28">No. </th>
               <th width="80" align="center">Kode Pasien</th>

               <th width="220" align="center">Nama</div></th>
               <th width="80" align="center">Tgl Lahir</div></th>
               <th width="220" align="center">Alamat</div></th>
               <th width="92" align="center">Kecamatan</div></th>
               <th width="108" align="center">Kabupaten </div></th>
               <th width="40" align="center">JK</div></th>
               <th width="117" align="center">Agama</div></th>
               <th width="108" align="center">Pekerjaan</div></th>
           </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td>'.$line->kd_pasien.'</td>
                           <td>'.$line->nama.'</td>
                           <td>'.$tglhariini.'</td>
                           <td>'.$line->alamat.'</td>
                           <td>'.$line->kabupaten.'</td>
                           <td>'.$line->kecamatan.'</td>
                           <td align="center">'.$line->jk.'</td>
                           <td>'.$line->agama.'</td>
                           <td>'.$line->pekerjaan.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'KIUPIGD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "020201", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    // Laporan Perdaerah
    public function rep020207($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		 
		if (count($Split) > 0 )
		{
                            $tmptgl = $Split[0];
                            $sort = $Split[1];
                            $tmptgl1 = explode("-",$tmptgl);
                            $bln = $tmptgl1[0];
                            $thn = $tmptgl1[1];
                            if ($sort === 'Kab') {
                                $sort = 'Kab.Kabupaten';
                            }elseif ($sort === 'Kec') {
                                $sort = 'Kec.Kecamatan';
                            }elseif ($sort === 'Kel') {
                                $sort = 'Kel.Kelurahan';
                            }else{
                                $sort = 'Prop.Propinsi';
                            }
                            
                            switch ($bln) {
                                case '01': $textbln = 'Januari';
                                    break;
                                case '02': $textbln = 'Febuari';
                                    break;
                                case '03': $textbln = 'Maret';
                                    break;
                                case '04': $textbln = 'April';
                                    break;
                                case '05': $textbln = 'Mei';
                                    break;
                                case '06': $textbln = 'Juni';
                                    break;
                                case '07': $textbln = 'Juli';
                                    break;
                                case '08': $textbln = 'Agustus';
                                    break;
                                case '09': $textbln = 'September';
                                    break;
                                case '10': $textbln = 'Oktober';
                                    break;
                                case '11': $textbln = 'November';
                                    break;
                                case '12': $textbln = 'Desember';
                            }
                                                          
                            $Param = "Where u.Kd_Bagian in (3,11)  And date_part('month',Tgl_Masuk)=".$bln." And date_part('Year',tgl_Masuk)=".$thn.")
                                          x  Group By Daerah Order By Daerah";
                }
                $kriteria = "Periode ".$textbln.' '.$thn;
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Daerah, Sum(PBaru) as PBaru, Sum(PLama)as PLama,Sum(LBaru)as LBaru, Sum(LLama) LLama,   Sum(PBaru)  + Sum(PLama) as JumlahP, Sum(lBaru)  + Sum(LLama) as JumlahL 
                                From (
                                Select  ".$sort." as Daerah, 
                                case When k.Baru='t' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PBaru, 
                                case When k.Baru='t' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LBaru, 
                                case When k.Baru='f' And p.Jenis_Kelamin='f' Then 1 Else 0 End as PLama, 
                                case When k.Baru='f' And p.Jenis_Kelamin='t' Then 1 Else 0 End as LLama
                                From (Kunjungan k INNER JOIN Unit u On u.kd_Unit=k.kd_Unit) 
                                INNER JOIN ((((Pasien p INNER JOIN Kelurahan kel ON kel.Kd_Kelurahan=p.Kd_Kelurahan) 
                                INNER JOIN Kecamatan kec ON kec.kd_kecamatan=kel.kd_Kecamatan) 
                                INNER JOIN Kabupaten kab ON Kab.Kd_Kabupaten=kec.Kd_Kabupaten) 
                                INNER JOIN Propinsi prop ON prop.kd_Propinsi=kab.Kd_Propinsi) ON P.Kd_pasien=k.Kd_pasien ".$Param;
        $q = $this->db->query($tmpquery);
        
        
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        
        $query = $q->result();
        $queryjumlah= $this->db->query('select sum(PBaru) as totPBaru, sum(PLama) as totPlama, sum(lbaru) as totLBaru, sum(LLama) as totLLama, sum(JumlahP) as totJumlahP, sum(JumlahL) as totJumlahL  
                        from ('.$tmpquery.') as total')->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Indeks Kunjungan Per Daerah IGD</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
                <tr>
                    <td width="29" rowspan="2">No. </td>
                    <td width="250" rowspan="2">Nama Daerah</td>
                    <td colspan="2" align="center">pasien Lama</td>
                    <td colspan="2" align="center">Pasien Baru</td>
                    <td colspan="2" align="center">Jumlah</td>
                    <td width="119" align="center" rowspan="2">Total</td>
                </tr>
                <tr>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                    <td width="50" align="center">L</td>
                    <td width="50" align="center">P</td>
                </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');
           foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
            <tfoot>
                    <tr>
                        <td colspan="2" align="right">Jumlah : </td>
                        <td align="center">'.$line->totllama.'</td>
                        <td align="center">'.$line->totplama.'</td>
                        <td align="center">'.$line->totlbaru.'</td>
                        <td align="center">'.$line->totpbaru.'</td>
                        <td align="center">'.$line->totjumlahl.'</td>
                        <td align="center">'.$line->totjumlahp.'</td>
                        <td align="center"></td>
                    </tr>
            </tfoot>
        ');
               }
           
           foreach ($query as $line) 
               {
                   $no++;
                    $jumlah = $line->jumlahp + $line->jumlahl;
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="250">'.$line->daerah.'</td>
                           <td width="50" align="center">'.$line->llama.'</td>
                           <td width="50" align="center">'.$line->plama.'</td>
                           <td width="50" align="center">'.$line->lbaru.'</td>
                           <td width="50" align="center">'.$line->pbaru.'</td>
                           <td width="50" align="center">'.$line->jumlahl.'</td>
                           <td width="50" align="center">'.$line->jumlahp.'</td>
                           <td width="50" align="center">'.$jumlah.'</td>
                       </tr>
                       
                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8" align="center">NIP. </td>
          </tr>
          <tr style="border:none !important">
            <td height="84" colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="2" align="center">NIP. </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">NIP. </td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'LPIGDPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "020207", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    //Laporan Pershift det
    public function ref020203($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        
                        if (count($Split) === 7)
                        {
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                            $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Asuransi')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = "Where  u.kd_Bagian in (3,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmphquery = $this->db->query("select u.kd_unit, u.nama_unit ,c.customer, c.kd_customer from unit u
                        inner join kunjungan k on k.kd_unit = u.kd_unit
                        inner join customer c on c.kd_customer = k.kd_customer
                        inner join kontraktor knt on knt.kd_customer = k.kd_customer
                        where u.kd_Bagian in (3,11)  "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                . " group by u.kd_unit, u.nama_unit ,c.customer, c.kd_customer"
                . " order by nama_unit asc")->result();
        
        $tmpquery = "select u.Nama_Unit ,c.customer , ps.Kd_Pasien,ps.Nama ,ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK, 
                                    (select age(k.Tgl_Masuk ,  ps.Tgl_Lahir)) as Umur,
			            case when k.baru = 't' then 'X' else '' end as Baru,
                                    case when k.Baru = 'f' then 'X' else '' end as Lama,
				    pk.pekerjaan, prs.perusahaan, zu.user_names , k.tgl_masuk , zu.user_names as namauser,
                                    k.kd_unit, k.kd_customer,
                                    case When k.Shift=4 Then 3 else k.shift end as Shift
                                    From ((pasien ps 
                                    LEFT JOIN perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                                    left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                                    inner join  ((Kunjungan k 
                                    LEFT JOIN Kontraktor knt On knt.kd_Customer=k.Kd_Customer) 
                                    INNER JOIN unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien   
                                    inner join customer c on k.kd_customer =c.kd_customer 
                                    inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit      
                                    and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk 
                                    inner join zusers zu ON zu.kd_user = t.kd_user ";
        $q = $this->db->query($tmpquery.$Param);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Instalasi Gawat Darurat Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
            <td align="center" width="24" rowspan="2"><strong>no.</strong></td>
            <td align="center" width="137" rowspan="2"><strong>No. Medrec</strong></td>
            <td align="center" width="137" rowspan="2"><strong>Nama Pasien</strong></td>
            <td align="center" width="273" rowspan="2"><strong>Alamat</strong></td>
            <td align="center" width="26" rowspan="2"><strong>JK</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Umur</strong></td>
            <td align="center" colspan="2"><strong>kunjungan</strong></td>
            <td align="center" width="82" rowspan="2"><strong>Pekerjaan</strong></td>
            <td align="center" width="68" rowspan="2"><strong>user</strong></td>
            <td align="center" width="63" rowspan="2"><strong>shift</strong></td>
          </tr>
          <tr>
            <td  width="37"><strong>Baru</strong></td>
            <td  width="39"><strong>Lama</strong></td>
          </tr>
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($tmphquery as $line) 
               {
                   
                   
                  $tmpparam2 = " where u.kd_Bagian in (3,11) and u.kd_unit =  '".$line->kd_unit."' and c.kd_customer = '".$line->kd_customer."'"
                                . $Paramplus
                                . "And ((Tgl_masuk= "."'".$tgl."'".""
                                . $ParamShift2
                                . $ParamShift3
                                . "Order By k.kd_unit, k.KD_PASIEN";
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
 else {
      $mpdf->WriteHTML('
                   <tbody>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->nama_unit.'</strong></td>
                        </tr>
                        <tr>
                            <td border="0" style="border:none !important" colspan="11"><strong>'.$line->customer.'</strong></td>
                        </tr>');
                   foreach ($dquery->result() as $d)
                   {
                       $no++;
                       $mpdf->WriteHTML(' <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50">'.$d->kd_pasien.'</td>
                           <td width="50">'.$d->nama.'</td>
                           <td width="50">'.$d->alamat.'</td>
                           <td width="50" align="center">'.$d->jk.'</td>
                           <td width="50">'.$d->umur.'</td>
                           <td width="50" align="center">'.$d->baru.'</td>
                           <td width="50" align="center">'.$d->lama.'</td>
                           <td width="50">'.$d->pekerjaan.'</td>
                           <td width="50">'.$d->user_names.'</td>
                           <td width="50" align="center">'.$d->shift.'</td>
                        </tr>');
                   }
 }

                  $mpdf->WriteHTML(' <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DIGDPD';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010206", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    //Laporan Pershift sum
    public function ref020204($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    if (count($Split) >= 5)
                    {
                        $tgl = $Split[0];
                        $date1 = str_replace('/', '-', $tgl);
                        $tomorrow = date('d-M-Y',strtotime($date1 . "+1 days"));
                        $ParamShift2 = " ";
                        $ParamShift3 = " ";
                        $Paramplus = " ";
                        $shift1 = " ";
                        $shift2 = " ";
                        $shift3 = " ";
                        if (count($Split) === 7)
                        {
                            $shift1 = $Split[5];
                            
                            if ($Split[6] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }else
                            {
                                $ParamShift2 = " And Shift In (".$Split[6].")))  ";
                            }
                        }elseif (count($Split) === 9)
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            if ($Split[6] === 3 or $Split[8] === 3)
                            {
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8]."))  ";
                              $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                            }
                              $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].")))  ";
                        }else
                        {
                            $shift1 = $Split[5];
                            $shift2 = $Split[7];
                            $shift3 = $Split[9];;
                            $ParamShift2 = " And Shift In (".$Split[6].",".$Split[8].",".$Split[10]."))  ";
                            $ParamShift3 = "Or  (Tgl_masuk= "."'".$tomorrow."'"."  And Shift=4) )  ";
                        }    
//                            $tmpkdunit = $Split[1];
                            $kdunit = $Split[2];
                            $tmpkelpas = $Split[3];
                            $kelpas = $Split[4];
//                            $tmpshift1 = $Split[5];
//                            $shift1 = $Split[6];
//                            $tmpshift2 = $Split[7];
//                            $shift2 = $Split[8];
//                            $tmpshift3 = $Split[9];
//                            $shift3 = $Split[10];
                            
                            
                            
                            if ($kdunit !== 'NULL')
                                {
                                    $Paramplus = " and k.kd_Unit = "."'".$kdunit."'"." ";
                                }  else {
                                $Param = " ";
                                }
                            if ($tmpkelpas !== 'Semua')
                                {
                                    if ($tmpkelpas === 'Umum')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=0 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Perusahaan')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=1 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }elseif ($tmpkelpas === 'Askes')
                                    {
                                        $Paramplus = $Paramplus." and Knt.Jenis_cust=2 ";
                                        if ($kelpas !== 'NULL')
                                        {
                                            $Paramplus = $Paramplus." and knt.kd_Customer= "."'".$kelpas."'"." ";
                                        }
                                    }
                                }else {
                                    $Param = $Paramplus." ";
                                    $tmpkelpas = "Semua Kelompok Pasien";
                                }
                    }
                    $kriteria = "Periode ".$tgl;
                    $Param = " "
                                    . $Paramplus
                                    . "And ((Tgl_masuk= "."'".$tgl."'".""
                                    . $ParamShift2
                                    . $ParamShift3
                                    . "Order By k.kd_unit, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmpquery = "Select Nama_unit, Sum(s1) as S1, Sum(s2) as S2, Sum(s3) as S3, Sum(s1) + Sum(s2) +Sum(s3) as Total
                 From
                (Select u.Nama_Unit,u.kd_unit,
                Case When k.shift=1 Then 1 else 0 end as S1,
                Case When k.shift=2 Then 1 else 0 end as S2,
                Case When k.shift=3 Or k.Shift=4 Then 1 Else 0 end as S3
                From (Kunjungan k LEFT JOIN Kontraktor knt On Knt.kd_Customer=k.Kd_Customer)
                INNER JOIN Unit u On u.kd_Unit=k.kd_Unit
                WHERE u.kd_Bagian in (3,11) ".$Param."
                ) x 
                Group By kd_Unit, Nama_Unit
                Order By kd_Unit, Nama_Unit ";
        $q = $this->db->query($tmpquery);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
        $queryjumlah= $this->db->query('select sum(s1) as tots1, sum(s2) as tots2, sum(s3) as tots3, sum(Total) as tottot from ('.$tmpquery.') as total')->result();
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Summary Daftar Pasien Rawat Jalan Pershift</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>Kelompok Pasien : ".$tmpkelpas." </h2>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $tmpcolspan = 0;
           if ($shift3 !== " ")
           {
               $tmpHshift3 = '<td align="center" width="100"><strong>'.$shift3.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
               $tmpFshift3 = '<td align="center"><strong>'.$line->tots3.'</strong></td>';
               }
               $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom3 = "<!-- ";
               $Tkom3 = "-->";
           }
           if ($shift2 !== " ")
           {
               $tmpHshift2 = '<td align="center" width="100"><strong>'.$shift2.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift2 = '<td align="center"><strong>'.$line->tots2.'</strong></td>';
               }
                $tmpcolspan = $tmpcolspan + 1;
           }else
           {
               $kom2 = "<!-- ";
               $Tkom2 = "-->";
           }
           if($shift1 !== " ")
           {
               $tmpHshift1 = '<td width="100" height="23" align="center"><strong>'.$shift1.'</strong></td>';
               foreach ($queryjumlah as $line) 
               {
                $tmpFshift1 = '<td align="center"><strong>'.$line->tots1.'</strong></td>';
               }
               
               $tmpcolspan = $tmpcolspan + 1;
           }else
                {
                    $kom1 = "<!-- ";
                    $Tkom1 = "-->";
                }
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
              <tr>
                <td align="center" width="37" rowspan="2"><strong>no. </strong></td>
                <td align="center" width="400" rowspan="2"><strong>Nama Unit</strong></td>
                <td height="23" colspan="'.$tmpcolspan.'" align="center"><strong>Jumlah Pasien</strong></td>
                <td align="center" width="129" rowspan="2"><strong>Total</strong></td>
              </tr>
              <tr>
                '.$tmpHshift1.'
                '.$tmpHshift2.'
                '.$tmpHshift3.'
              </tr>
           </thead>
          
           ');
           foreach ($queryjumlah as $line) 
               {
                    $mpdf->WriteHTML('
                        <tfoot>
                                 <tr>
                                   
                                    <td colspan="2" align="center">Total Pasien :</td>
                                    '.$tmpFshift1.'
                                    '.$tmpFshift2.'
                                    '.$tmpFshift3.'
                                    <td align="center"><strong>'.$line->tottot.'</strong></td>
                                 </tr>
                        </tfoot>
                    ');
           }
           foreach ($query as $line) 
               {
                   $no++;
                   $mpdf->WriteHTML('
                   <tbody>
                        <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                           <td width="50" align="left">'.$line->nama_unit.'</td>
                           '.$kom1.'<td width="50" align="center">'.$line->s1.'</td>'.$Tkom1.'
                           '.$kom2.'<td width="50" align="center">'.$line->s2.'</td>'.$Tkom2.'
                           '.$kom3.'<td width="50" align="center">'.$line->s3.'</td>'.$Tkom3.'
                           <td width="50" align="center">'.$line->total.'</td>
                        </tr>

                   <p>&nbsp;</p>

                   ');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $mpdf->WriteHTML('
        <table width="983" border="0" style="border:none !important">
         <tr style="border:none !important">
             <td width="83" align="right">&nbsp;</td>
             <td width="106" align="center">&nbsp;</td>
             <td width="110" align="center">&nbsp;</td>
             <td width="104" align="center">&nbsp;</td>
             <td width="120" align="center">&nbsp;</td>
             <td width="116" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
             <td width="146" align="center">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="8" align="center">NIP. </td>
          </tr>
          <tr style="border:none !important">
            <td height="84" colspan="8">&nbsp;</td>
          </tr>
          <tr style="border:none !important">
            <td colspan="2" align="center">NIP. </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">NIP. </td>
          </tr>
        </table>  
          ');
           
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DIGDPSS';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      
           
           $res= '{ success : true, msg : "", id : "020204", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
    
    //laporan perperujuk
    public function ref020206($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  11);
//        print_r($Split);
		if (count($Split) > 0 )
		{
                    $tgl1 = $Split[0];
                    $tgl2 = $Split[1];
                    $tmprujuk = $Split[2];
                    $rujuk = $Split[3];
                    $tmprujukandari = $Split[4];
                    $rujukandari = $Split[5];
                    $tmpjenis = $Split[6];
                    $jenis = $Split[7];
                    $unit = $Split[8];
                    
                    $criteria = "";
                    $tmpunit = explode(',',$unit);
                    for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
                    $criteria = substr($criteria, 0, -1);
                    if ($jenis == 3)
                    {
                        $tmptambahParam = "and baru = 'f' ";
                    }else if ($jenis == 2)
                    {
                        $tmptambahParam = "and baru = 't' ";
                    }else
                    {
                        $tmptambahParam = " ";
                    }
                    if ($tgl1 === $tgl2)
                    {
                        $kriteria = "Periode ".$tgl1;
                    }else
                    {
                        $kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
                    }
                    if ($rujukandari == -1)
                    {
                        if($rujuk == -1)
                        {
                            $tmptambahParamrujuk = " ";
                        }  else {
                            $tmptambahParamrujuk = " and k.kd_Rujukan = ".$rujuk." ";    
                        }
                    }else
                    {
                        if($rujuk == -1)
                        {
                            $tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." ";
                        }  else {
                            $tmptambahParamrujuk = " AND k.Cara_Penerimaan=".$rujukandari." AND k.kd_Rujukan = ".$rujuk." ";    
                        }
                    }
                     $Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                            . $tmptambahParam
                            . $tmptambahParamrujuk
                            . "and k.kd_Unit in ($criteria)  "
                            . "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
//                    echo $Param;
                }
                
                
                
        $this->load->library('m_pdf');
        $this->m_pdf->load();
        $tmphquery = $this->db->query("select k.kd_rujukan, r.rujukan from kunjungan k
                                    inner join rujukan r on r.kd_rujukan = k.kd_rujukan
                                    Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                    . $tmptambahParam
                                    . $tmptambahParamrujuk
                                    . "and k.kd_Unit in ($criteria)  "                                      
                                    . " group by k.kd_rujukan, r.rujukan"
                                    . " order by r.rujukan asc")->result();
                    
        
        $tmpquery = "Select u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
                        ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
                        (select age(k.Tgl_Masuk, ps.Tgl_Lahir) as Umur), 
                        case when k.baru = 't' then 'X' else '' end as Baru,
                        case when k.Baru = 'f' then 'X' else '' end as Lama,
                        pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
                        c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

                        From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                        left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                        inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
                        LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
        $q = $this->db->query($tmpquery.$Param);
        
             
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
        $queryRS = $this->db->query("select * from db_rs")->result();
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        
           $mpdf= new mPDF('utf-8', array(297,210));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".$logors." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Daftar Unit Gawat Darurat PerRujukan</h1>");
           $mpdf->WriteHTML("<h2 class='formarial' align='center'>".$kriteria."</h2>");
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
            <tr>
              <td align="center" width="24" rowspan="2">no. </td>
              <td align="center" width="137" rowspan="2">No. Medrec </td>
              <td align="center" width="137" rowspan="2">Nama Pasien</td>
              <td align="center" width="273" rowspan="2">Alamat</td>
              <td align="center" width="26" rowspan="2">JK</td>
              <td align="center" width="82" rowspan="2">Umur</td>
              <td align="center" colspan="2">kunjungan</td>
              <td align="center" width="82" rowspan="2">Pekerjaan</td>
              <td align="center" width="68" rowspan="2">Customer</td>
              <td align="center" width="63" rowspan="2">Rujukan</td>
            </tr>
            <tr>
              <td align="center" width="37">Baru</td>
              <td align="center" width="39">Lama</td>
            </tr>
          </thead>
           <tfoot>

           </tfoot>
           ');

          foreach ($tmphquery as $line) 
               {
                  $tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
                                . $tmptambahParam
                                . " and k.kd_Rujukan = ".$line->kd_rujukan.""
                                . "and k.kd_Unit in ($criteria) "
                                . "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
                  
                   $dquery =  $this->db->query($tmpquery.$tmpparam2);
                  // echo $tmpquery.$tmpparam2;
                   $no = 0;
                   if($dquery->num_rows == 0)
                   {
                       $mpdf->WriteHTML('');
                   }
                    else {
                         $mpdf->WriteHTML('
                                           <tbody>
                                           <tr>
                                               <td border="0" style="border:none !important" colspan="11">'.$line->rujukan.'</td>
                                           </tr>
                                           ');
                                      foreach ($dquery->result() as $d)
                                      {
                                          $no++;
                                          $Split1 = explode(" ", $d->umur, 6);
                                            if (count($Split1) == 6)
                                            {
                                                $tmp1 = $Split1[0];
                                                $tmp2 = $Split1[1];
                                                $tmp3 = $Split1[2];
                                                $tmp4 = $Split1[3];
                                                $tmp5 = $Split1[4];
                                                $tmp6 = $Split1[5];
                                                $tmpumur = $tmp1.'th';
                                            }else if (count($Split1) == 4)
                                            {
                                                $tmp1 = $Split1[0];
                                                $tmp2 = $Split1[1];
                                                $tmp3 = $Split1[2];
                                                $tmp4 = $Split1[3];
                                                if ($tmp2 == 'years')
                                                {
                                                   $tmpumur = $tmp1.'th';
                                                }else if ($tmp2 == 'mon')
                                                {
                                                   $tmpumur = $tmp1.'bl';
                                                }
                                                else if ($tmp2 == 'days')
                                                {
                                                   $tmpumur = $tmp1.'hr';
                                                }

                                            }  else {
                                                $tmp1 = $Split1[0];
                                                $tmp2 = $Split1[1];
                                                if ($tmp2 == 'years')
                                                {
                                                   $tmpumur = $tmp1.'th';
                                                }else if ($tmp2 == 'mons')
                                                {
                                                   $tmpumur = $tmp1.'bl';
                                                }
                                                else if ($tmp2 == 'days')
                                                {
                                                   $tmpumur = $tmp1.'hr';
                                                }
                                            }
                                          $mpdf->WriteHTML('

                                               <tr class="headerrow"> 
                                                   <td align="right">'.$no.'</td>
                                                   <td width="50">'.$d->kd_pasien.'</td>
                                                   <td width="50">'.$d->nama.'</td>
                                                   <td width="50">'.$d->alamat.'</td>
                                                   <td width="50" align="center">'.$d->jk.'</td>
                                                   <td width="50" align="center">'.$tmpumur.'</td>
                                                   <td width="50" align="center">'.$d->baru.'</td>
                                                   <td width="50" align="center">'.$d->lama.'</td>
                                                   <td width="50">'.$d->pekerjaan.'</td>
                                                   <td width="50">'.$d->customer.'</td>
                                                   <td width="50" align="center">'.$d->textrujukan.'</td>
                                               </tr>');
                                      }
                    }
               
                  $mpdf->WriteHTML(' <p>&nbsp;</p>');
               }
           $mpdf->WriteHTML('</tbody></table>');
           $tmpbase = 'base/tmp/';
//           $datenow = date("dmY");
           $tmpname = time().'DIGDPP';
           $mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "1010208", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }
	
    public function rep020202($UserID,$Params)
    {
        $UserID = '0';
       $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
         $tglsummax = $Split[1];
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		 $criteria="";
		 $tmpunit = explode(',', $Split[3]);
		   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
		$feildkriteria="sp_u.kd_unit";
		$criteria = substr($criteria, 0, -1);
            $pasientype=$Split[2];
			if($pasientype=='kabeh')
			{
			$pasientype="";
			}
			else
			{
			$pasientype=" and k.Baru='".$Split[2]."'";
			};
        $q = $this->db->query("Select x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
							  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum ,x.Kd_Unit as kdunit
							  From (
							  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
							  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
							   Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
							   Case When k.Baru=true Then 1 else 0 end as Br,
							   Case When k.Baru=false Then 1 else 0 end as Lm,
							   Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
							   Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
							   case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
							   ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
								case when ktr.jenis_cust=0 then 1 else 0 end as umum
								From Unit u
								INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
								 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
								 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in( $criteria) $pasientype
								 ) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
		
        
        if($q->num_rows == 0)
        {
            $res= '{ success : false, msg : "No Records Found"}';
        }
        else {
     
        $query = $q->result();
	
        $queryRS = $this->db->query("select * from db_rs")->result();
		    $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
										  Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
										  From (
										  Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
										  Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
										  Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
										  Case When k.Baru=true Then 1 else 0 end as Br,
										  Case When k.Baru=false Then 1 else 0 end as Lm,
										  Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
										  Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
										  case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
										  ,case when ktr.jenis_cust=2 then 1 else 0 end as ASKES , 
										  case when ktr.jenis_cust=0 then 1 else 0 end as umum
										  From Unit u
											INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
											 Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
											 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='3' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in($criteria)
											 ) x")->result();
		
        $queryuser = $this->db->query("select * from zusers where kd_user= "."'".$UserID."'")->result();
        foreach ($queryuser as $line) {
           $kduser = $line->kd_user;
           $nama = $line->user_names;
        }
        $no = 0;
            
           $mpdf= new mPDF('utf-8', array(210,297));

           $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
           $mpdf->pagenumPrefix = 'Hal : ';
           $mpdf->pagenumSuffix = '';
           $mpdf->nbpgPrefix = ' Dari ';
           $mpdf->nbpgSuffix = '';
           $date = date("d-M-Y / H:i:s");
           $arr = array (
             'odd' => array (
               'L' => array (
                 'content' => 'Operator : ('.$kduser.') '.$nama,
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'C' => array (
                 'content' => "Tgl/Jam : ".$date."",
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'R' => array (
                 'content' => '{PAGENO}{nbpg}',
                 'font-size' => 8,
                 'font-style' => '',
                 'font-family' => 'serif',
                 'color'=>'#000000'
               ),
               'line' => 0,
             ),
             'even' => array ()
           );
           $mpdf->SetFooter($arr);

           $mpdf->WriteHTML("
           <style>
           .t1 {
               border: 1px solid black;
               border-collapse: collapse;
               font-size: 11px;
               font-family: Arial, Helvetica, sans-serif;
           }
           .formarial {
               font-family: Arial, Helvetica, sans-serif;
           }
           h1 {
               font-size: 12px;
           }

           h2 {
               font-size: 10px;
           }

           h3 {
               font-size: 8px;
           }

           table2 {
               border: 1px solid white;
           }
           </style>
           ");
           foreach ($queryRS as $line) {
               $logors =  base_url()."ui/images/Logo/LogoRs.png";
               $mpdf->WriteHTML("
               <table width='380' border='0' style='border:none !important'>
                   <tr style='border:none !important'>
                       <td style='border:none !important' width='76' rowspan='3'><img src=".base_url()."ui/images/Logo/LogoRs.png"." width='76' height='74' /></td>
                       <td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line->name."</h1></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><p><h2 class='formarial' align='left'>".$line->address."</h1></p></td>
                   </tr>
                   <tr style='border:none !important'>
                       <td style='border:none !important'><h3 class='formarial' align='left'>".$line->phone1."".$line->phone2."".$line->phone3."".$line->fax." - ".$line->zip."</h1></td>
                   </tr>
               </table>
            ");

               }

           $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Summary Pasien Rawat Inap </h1>");
		   $mpdf->WriteHTML("<h3 class='formarial' align='center'> Tanggal :".$tglsum." s/d ".$tglsummax."</h1>");
          
           $mpdf->WriteHTML('
           <table class="t1" border = "1">
           <thead>
           <tr>
					<td align="center" width="24" rowspan="2">no. </td>
			
					<td align="center" width="250" rowspan="2">Nama Unit </td>
					<td align="center" width="50" rowspan="2">Jumlah Pasien</td>
					  <td align="center" colspan="2">enis kelamin</td>
					<td align="center" colspan="2">kunjungan</td>
					
					<td align="center" width="68" rowspan="2">Perusahaan</td>
					<td align="center" width="68" rowspan="2">Askes</td>
					<td align="center" width="68" rowspan="2">Umum</td>
		</tr>
				 <tr>    
					<td align="center" width="50">L</td>
					<td align="center" width="50">P</td>
					<td align="center" width="50">Baru</td>
					<td align="center" width="50s">Lama</td>
				 </tr>  
           </thead>
           <tfoot>

           </tfoot>
           ');

           foreach ($query as $line) 
               {
                   $no++;
                   $tanggal = $line->tgl_lahir;
                   $tglhariini = date('d/F/Y', strtotime($tanggal));
                   $mpdf->WriteHTML('
                   <tbody>
                       <tr class="headerrow"> 
                           <td align="right">'.$no.'</td>
                          
                           <td align="left">'.$line->namaunit2.'</td>
						    <td align="right">'.$line->jumlahpasien.'</td>
                             <td align="right">'.$line->lk.'</td>
                          <td align="right">'.$line->pr.'</td>
                           <td align="right">'.$line->br.'</td>
                           <td align="right">'.$line->lm.'</td>
                           <td align="right">'.$line->perusahaan.'</td>
                           <td align="right">'.$line->askes.'</td>
                          <td align="right">'.$line->umum.'</td>
                       </tr>

                   <p>&nbsp;</p>

                   ');
               }
					   $mpdf->WriteHTML('</tbody>');
						foreach ($queryjumlah as $line) 
						   {
								$mpdf->WriteHTML('
						<tfoot>
								<tr>
										<td colspan="2" align="right"> Jumlah </td>
										<td align="right">'.$line->jumlahpasien.'</td>
										<td align="right">'.$line->lk.'</td>
									   <td align="right">'.$line->pr.'</td>
									   <td align="right">'.$line->br.'</td>
									   <td align="right">'.$line->lm.'</td>
									   <td align="right">'.$line->perusahaan.'</td>
									   <td align="right">'.$line->askes.'</td>
									   <td align="right">'.$line->umum.'</td>
								</tr>
						</tfoot>
					');
					$mpdf->WriteHTML('</table>');
						  }      
           $tmpbase = 'base/tmp/';
           $datenow = date("dmY");
           $tmpname = time().'SumPasienRWI';
           $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

           $res= '{ success : true, msg : "", id : "030202", title : "Laporan Pasien", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
            }  
        
        
        echo $res;
    }

  
    public function rep030201($UserID,$Params)
    {
        $UserID = '0';
        $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
        $tglsummax = $Split[1];
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		$queryRS = "select * from db_rs";
		$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
		$no = 0;
		$mpdf= new mPDF('utf-8', array(297,210));
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i:s");
			$arr = array (
			  'odd' => array (
				'L' => array (
				  'content' => 'Operator : (0) Admin',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'C' => array (
				  'content' => "Tgl/Jam : ".$date."",
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'R' => array (
				  'content' => '{PAGENO}{nbpg}',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'line' => 0,
			  ),
			  'even' => array ()
			);
		$mpdf->SetFooter($arr);
		$mpdf->SetTitle('LAPDETAILPASIEN');
		$mpdf->WriteHTML("
		<style>
		.t1 {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px;
			font-family: Arial, Helvetica, sans-serif;
		}
		.formarial {
			font-family: Arial, Helvetica, sans-serif;
		}
		h1 {
			font-size: 12px;
		}

		h2 {
			font-size: 10px;
		}

		h3 {
			font-size: 8px;
		}

		table2 {
			border: 1px solid white;
		}
		</style>
		");
			while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
				{
				$mpdf->WriteHTML("
				<table width='380' border='0' style='border:none !important'>
					<tr style='border:none !important'>
						<td style='border:none !important' width='76' rowspan='3'><img src='file:///C|/xampp/htdocs/test/gambar/LogoRs.png' width='76' height='74' /></td>
						<td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line['name']."</h1></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><p><h2 class='formarial' align='left'>".$line['address']."</h1></p></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><h3 class='formarial' align='left'>".$line['phone1']."".$line['phone2']."".$line['phone3']."".$line['fax']." - ".$line['zip']."</h1></td>
					</tr>
				</table>
			 ");

				}
  
				$mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Registrasi Detail Rawat Jalan</h1>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
				$mpdf->WriteHTML('
				<table class="t1" border = "1">
				<thead>
				  <tr>
					<td align="center" width="24" rowspan="2">no. </td>
				   
					<td align="center" width="80" rowspan="2">No. Medrec</td>
					<td align="center" width="210" rowspan="2">Nama Pasien</td>
					<td align="center" width="220" rowspan="2">Alamat</td>
					<td align="center" width="26" rowspan="2">JK</td>
					<td align="center" width="30" rowspan="2">Umur</td>
					<td align="center" colspan="2">kunjungan</td>
					<td align="center" width="82" rowspan="2">Pekerjaan</td>
					<td align="center" width="68" rowspan="2">Tanggal Masuk</td>
					<td align="center" width="63" rowspan="2">Rujukan</td>
					<td align="center" width="30" rowspan="2">Jam Masuk</td>
					<td align="center" width="100" rowspan="2">Nama Dokter</td>
					<td align="center" width="63" rowspan="2">Customer</td>
					<td align="center" width="63" rowspan="2">User</td>
				  </tr>
				  <tr>
					<td align="center" width="37">Baru</td>
					<td align="center" width="37">Lama</td>
				  </tr>
				</thead>

				');

			$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
			on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."'
			group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
");
			$mpdf->WriteHTML('<tbody>');

			while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
			{

				
			


			$query = "
			Select k.kd_unit as kdunit,
			 u.nama_unit as namaunit, 
			ps.kd_Pasien as kdpasien,
			ps.nama  as namapasien,
			ps.alamat as alamatpas, 
			case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
			,date_part('year',age(ps.Tgl_Lahir)) as umur,
			case when k.Baru=true then 'x'  else ''  end as pasienbar,
			case when k.Baru=false then 'x'  else ''  end as pasienlama,
			 pk.pekerjaan as pekerjaan, 
			prs.perusahaan as perusahaan,  
			case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
			k.Jam_masuk as jammas,
			k.Tgl_masuk as tglmas,
			dr.nama as dokter, 
			c.customer as customer, 
			zu.user_names as username
			From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
			left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
			inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
			on k.Kd_Pasien = ps.Kd_Pasien  
			 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
			 INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
			 and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  inner join zusers zu ON zu.kd_user = t.kd_user 
			 where u.kd_unit='".$f['kd_unit']."' and tgl_masuk between '".$tglsum."' and '".$tglsummax."'
			 group by 
			 k.kd_unit,k.KD_PASIEN,
			u.Nama_Unit, 
			ps.Kd_Pasien,
			ps.Nama ,
			ps.Alamat, 
			ps.jenis_kelamin,
			ps.Tgl_Lahir,
			k.Baru, pk.pekerjaan, 
			prs.perusahaan,  k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, zu.user_names
				  Order By k.kd_unit, k.KD_PASIEN
			";
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;
			
				if(pg_num_rows($result) <= 0)
				{
					$mpdf->WriteHTML('');
				}
				else
				{
				$mpdf->WriteHTML('<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>');
			while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
				
					$mpdf->WriteHTML('
					
					
					
						<tr class="headerrow"> 
						
							<td align="right">'.$i.'</td>
						 
							<td width="50" align="left">'.$line['kdpasien'].'</td>
							<td width="50" align="left">'.$line['namapasien'].'</td>
							<td width="50" align="left">'.$line['alamatpas'].'</td>
							 <td width="50" align="center">'.$line['jk'].'</td>
							<td width="50" align="center">'.$line['umur'].'</td>
							<td width="50" align="center">'.$line['pasienbar'].'</td>
						   
							<td width="50" align="center">'.$line['pasienlama'].'</td>
							<td width="50" align="left">'.$line['pekerjaan'].'</td>
							<td width="50" align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							<td width="50" align="left">'.$line['rujukan'].'</td>
							<td width="50" align="left">'.date('h:m', strtotime($line['jammas'])).'</td>
								<td width="50" align="left">'.$line['dokter'].'</td>
								<td width="50" align="left">'.$line['customer'].'</td>
								<td width="50" align="left">'.$line['username'].'</td>
								
						</tr>
				   
					<p>&nbsp;</p>

					');
					 $i++;
				}
				
				$i--;
				$mpdf->WriteHTML('<tr><td colspan="3">Total Pasien Daftar di '.$f['nama_unit'].' : </td><td colspan="12">'.$i.'</td></tr>');
				}
			   //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
				}
			  $mpdf->WriteHTML('</tbody></table>');
			 
					   $tmpbase = 'base/tmp/';
					   $datenow = date("dmY");
					   $tmpname = time().'DetailPasienRWJ';
					   $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

					   $res= '{ success : true, msg : "", id : "030201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
						  
        
        
        echo $res;
    }   
   
    public function rep020108($UserID,$Params)
    {
        $UserID = 0;
        $Split = explode("##@@##", $Params,  4);
		$tglsum = $Split[0];
        $tglsummax = $Split[1];
		$criteria="";
		$tmpunit = explode(',', $Split[3]);
		   for($i=0;$i<count($tmpunit);$i++)
                    {
                    $criteria .= "'".$tmpunit[$i]."',";
                    }
		$criteria = substr($criteria, 0, -1);			
        $this->load->library('m_pdf');
        $this->m_pdf->load();
		$queryRS = "select * from db_rs";
		$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
		$no = 0;
		$mpdf= new mPDF('utf-8', array(297,210));
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i:s");
			$arr = array (
			  'odd' => array (
				'L' => array (
				  'content' => 'Operator : (0) Admin',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'C' => array (
				  'content' => "Tgl/Jam : ".$date."",
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'R' => array (
				  'content' => '{PAGENO}{nbpg}',
				  'font-size' => 8,
				  'font-style' => '',
				  'font-family' => 'serif',
				  'color'=>'#000000'
				),
				'line' => 0,
			  ),
			  'even' => array ()
			);
		$mpdf->SetFooter($arr);
		$mpdf->SetTitle('LAPDETAILPASIEN');
		$mpdf->WriteHTML("
		<style>
		.t1 {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 11px;
			font-family: Arial, Helvetica, sans-serif;
		}
		.formarial {
			font-family: Arial, Helvetica, sans-serif;
		}
		h1 {
			font-size: 12px;
		}

		h2 {
			font-size: 10px;
		}

		h3 {
			font-size: 8px;
		}

		table2 {
			border: 1px solid white;
		}
		</style>
		");
			while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
				{
				$mpdf->WriteHTML("
				<table width='380' border='0' style='border:none !important'>
					<tr style='border:none !important'>
						<td style='border:none !important' width='76' rowspan='3'><img src='file:///C|/xampp/htdocs/test/gambar/LogoRs.png' width='76' height='74' /></td>
						<td style='border:none !important' width='294'><h1 class='formarial' align='left'>".$line['name']."</h1></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><p><h2 class='formarial' align='left'>".$line['address']."</h1></p></td>
					</tr>
					<tr style='border:none !important'>
						<td style='border:none !important'><h3 class='formarial' align='left'>".$line['phone1']."".$line['phone2']."".$line['phone3']."".$line['fax']." - ".$line['zip']."</h1></td>
					</tr>
				</table>
			 ");

				}
  
				$mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Registrasi Detail Gawat Darurat</h1>");
				$mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
				$mpdf->WriteHTML('
				<table class="t1" border = "1">
				<thead>
				  <tr>
					<td align="center" width="24" rowspan="2">no. </td>
				   
					<td align="center" width="80" rowspan="2">No. Medrec</td>
					<td align="center" width="210" rowspan="2">Nama Pasien</td>
					<td align="center" width="220" rowspan="2">Alamat</td>
					<td align="center" width="26" rowspan="2">JK</td>
					<td align="center" width="30" rowspan="2">Umur</td>
					<td align="center" colspan="2">kunjungan</td>
					<td align="center" width="82" rowspan="2">Pekerjaan</td>
					<td align="center" width="68" rowspan="2">Tanggal Masuk</td>
					<td align="center" width="63" rowspan="2">Rujukan</td>
					<td align="center" width="30" rowspan="2">Jam Masuk</td>
					<td align="center" width="100" rowspan="2">Nama Dokter</td>
					<td align="center" width="63" rowspan="2">Customer</td>
					<td align="center" width="63" rowspan="2">User</td>
				  </tr>
				  <tr>
					<td align="center" width="37">Baru</td>
					<td align="center" width="37">Lama</td>
				  </tr>
				</thead>

				');

			$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
			on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='3' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($criteria)
			group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
");
			$mpdf->WriteHTML('<tbody>');

			while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
			{

				
			
if ( $Split[2]=='kosong')
{
			$query = "
			Select k.kd_unit as kdunit,
			 u.nama_unit as namaunit, 
			ps.kd_Pasien as kdpasien,
			ps.nama  as namapasien,
			ps.alamat as alamatpas, 
			case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
			,date_part('year',age(ps.Tgl_Lahir)) as umur,
			case when k.Baru=true then 'x'  else ''  end as pasienbar,
			case when k.Baru=false then 'x'  else ''  end as pasienlama,
			 pk.pekerjaan as pekerjaan, 
			prs.perusahaan as perusahaan,  
			case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
			k.Jam_masuk as jammas,
			k.Tgl_masuk as tglmas,
			dr.nama as dokter, 
			c.customer as customer, 
			zu.user_names as username
			From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
			left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
			inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
			on k.Kd_Pasien = ps.Kd_Pasien  
			 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
			 INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
			 and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  inner join zusers zu ON zu.kd_user = t.kd_user 
			 where u.kd_unit='".$f['kd_unit']."' and tgl_masuk between '".$tglsum."' and '".$tglsummax."' 
			 group by 
			 k.kd_unit,k.KD_PASIEN,
			u.Nama_Unit, 
			ps.Kd_Pasien,
			ps.Nama ,
			ps.Alamat, 
			ps.jenis_kelamin,
			ps.Tgl_Lahir,
			k.Baru, pk.pekerjaan, 
			prs.perusahaan,  k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, zu.user_names
				  Order By k.kd_unit, k.KD_PASIEN
			";
	}
	else 
		{
					$query = "
					Select k.kd_unit as kdunit,
					 u.nama_unit as namaunit, 
					ps.kd_Pasien as kdpasien,
					ps.nama  as namapasien,
					ps.alamat as alamatpas,
					k.Baru	,			
					case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk
					,date_part('year',age(ps.Tgl_Lahir)) as umur,
					case when k.Baru=true then 'x'  else ''  end as pasienbar,
					case when k.Baru=false then 'x'  else ''  end as pasienlama,
					 pk.pekerjaan as pekerjaan, 
					prs.perusahaan as perusahaan,  
					case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
					k.Jam_masuk as jammas,
					k.Tgl_masuk as tglmas,
					dr.nama as dokter, 
					c.customer as customer, 
					zu.user_names as username
					From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
					left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
					inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
					on k.Kd_Pasien = ps.Kd_Pasien  
					 LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
					 INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
					 and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  inner join zusers zu ON zu.kd_user = t.kd_user 
					 where u.kd_unit='".$f['kd_unit']."' and tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$Split[2]."'
					 group by 
					 k.kd_unit,k.KD_PASIEN,
					u.Nama_Unit, 
					ps.Kd_Pasien,
					ps.Nama ,
					ps.Alamat, 
					ps.jenis_kelamin,
					ps.Tgl_Lahir,
					k.Baru, pk.pekerjaan, 
					prs.perusahaan,  k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, zu.user_names
						  Order By k.kd_unit, k.KD_PASIEN
					";
			};
	
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;
			
				if(pg_num_rows($result) <= 0)
				{
					$mpdf->WriteHTML('');
				}
				else
				{
				$mpdf->WriteHTML('<tr><td colspan="15">'.$f['nama_unit'].'</td></tr>');
			while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
				
					$mpdf->WriteHTML('
					
					
					
						<tr class="headerrow"> 
						
							<td align="right">'.$i.'</td>
						 
							<td width="50" align="left">'.$line['kdpasien'].'</td>
							<td width="50" align="left">'.$line['namapasien'].'</td>
							<td width="50" align="left">'.$line['alamatpas'].'</td>
							 <td width="50" align="center">'.$line['jk'].'</td>
							<td width="50" align="center">'.$line['umur'].'</td>
							<td width="50" align="center">'.$line['pasienbar'].'</td>
						   
							<td width="50" align="center">'.$line['pasienlama'].'</td>
							<td width="50" align="left">'.$line['pekerjaan'].'</td>
							<td width="50" align="center">'.  date('d.m.Y', strtotime($line['tglmas'])).'</td>
							<td width="50" align="left">'.$line['rujukan'].'</td>
							<td width="50" align="left">'.date('h:m', strtotime($line['jammas'])).'</td>
								<td width="50" align="left">'.$line['dokter'].'</td>
								<td width="50" align="left">'.$line['customer'].'</td>
								<td width="50" align="left">'.$line['username'].'</td>
								
						</tr>
				   
					<p>&nbsp;</p>

					');
					 $i++;
				}
				
				$i--;
				$mpdf->WriteHTML('<tr><td colspan="3">Total Pasien Daftar di '.$f['nama_unit'].' : </td><td colspan="12">'.$i.'</td></tr>');
				}
			   //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
				}
			  $mpdf->WriteHTML('</tbody></table>');
			 
					   $tmpbase = 'base/tmp/';
					   $datenow = date("dmY");
					   $tmpname = time().'DetailPasienRWJ';
					   $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

					   $res= '{ success : true, msg : "", id : "020108", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';
						  
        
        
        echo $res;
    }
    
    public function rep020209($UserID,$Params){
    	$UserID = '0';
    	$Split = explode("##@@##", $Params, 12);
    	//print_r ($Split);
    	/*
    	 [0] => NoCheked
    	 [1] => 4
    	 [2] => Akta
    	 [3] => 219
    	 [4] => Adelia, Bidan
    	 [5] => LIA
    	 [6] => 05/Jul/2015
    	 [7] => 08/Jul/2015
    	 [8] => Semua
    	 [9] => Kelpas
    	[10] => NULL */
    		
    	if (count($Split) > 0 ){
    		$autocas = $Split[1];
    		$unit = $Split[2];
    		$kdunit = $Split[3];
    		$dokter = $Split[4];
    		$kddokter = $Split[5];
    		$tglAwal = $Split[6];
    		$tglAkhir = $Split[7];
    		$kelPasien = $Split[9];
    		$kdCustomer = $Split[10];
    		//echo $kdCustomer;
    
    			
    		if($dokter =='Dokter' || $kddokter =='Semua'){
    			$paramdokter="";
    		} else{
    			$paramdokter =" and dtd.kd_Dokter='$kddokter'";
    		}
    			
    		if($kelPasien=='Semua' || $kdCustomer==NULL){
    			$paramcustomer="";
    		} else{
    			$paramcustomer =" and k.Kd_Customer ='$kdCustomer'";
    		}
    			
    		if($unit =='Unit' || $kdunit=='Semua'){
    			$paramunit='';
    		} else{
    			$paramunit=" and t.kd_unit ='$kdunit' ";
    		}
    			
    		if($autocas == 1){
    			$params=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
    		} else if($autocas == 2){
    			$params=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='3')";
    		} else if($autocas == 3 || $autocas == 4){
    			$params="";
    		}
    			
    	}
    	/* , max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
    		Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah,dtd.kd_Dokter */
    	$queryHasil = $this->db->query( "
    			Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
    			From Detail_TRDokter dtd
    			INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
    			INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
    			INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
    			INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
    			INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
    			INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
    			INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
    			INNER JOIN unit u On u.kd_unit=t.kd_unit
    			Where t.ispay='1'
    			And dt.Kd_kasir='06'
    			And Folio in ('A','E')
    			and u.kd_bagian='3'
    			And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
    			And dt.Qty * dtd.JP >0
    			".$paramdokter."".$paramcustomer."".$params."".$paramunit."
										Group By d.Nama, dtd.kd_Dokter
										Order By Dokter, dtd.kd_Dokter
    
    
                                      ");
    
    
    	$query = $queryHasil->result();
    	if(count($query) == 0)
    	{
    		$res= '{ success : false, msg : "No Records Found"}';
    	} else {
    		$queryRS = $this->db->query("select * from db_rs")->result();
    		$queryuser = $this->db->query("select * from zusers where kd_user= '0'")->result();
    		foreach ($queryuser as $line) {
    			$kduser = $line->kd_user;
    			$nama = $line->user_names;
    		}
    
    		$no = 0;
    		$this->load->library('m_pdf');
    		$this->m_pdf->load();
    		//-------------------------MENGATUR TAMPILAN HALAMAN KERTAS------------------------------------------------------------------------
    		$mpdf= new mPDF('utf-8', 'a4');
    		$mpdf->SetDisplayMode('fullpage');
    			
    		//-------------------------MENGATUR TAMPILAN BOTTOM HASIL LABORATORIUM(HALAMAN DLL)------------------------------------------------
    		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
    		$mpdf->pagenumPrefix = 'Hal : ';
    		$mpdf->pagenumSuffix = '';
    		$mpdf->nbpgPrefix = ' Dari ';
    		$mpdf->nbpgSuffix = '';
    		$date = date("d-M-Y / H:i:s");
    		$arr = array (
    				'odd' => array (
    						'L' => array (
    								'content' => 'Operator : (0) Admin',
    								'font-size' => 8,
    								'font-style' => '',
    								'font-family' => 'serif',
    								'color'=>'#000000'
    						),
    						'C' => array (
    								'content' => "Tgl/Jam : ".$date."",
    								'font-size' => 8,
    								'font-style' => '',
    								'font-family' => 'serif',
    								'color'=>'#000000'
    						),
    						'R' => array (
    								'content' => '{PAGENO}{nbpg}',
    								'font-size' => 8,
    								'font-style' => '',
    								'font-family' => 'serif',
    								'color'=>'#000000'
    						),
    						'line' => 0,
    				),
    				'even' => array ()
    		);
    
    			
    		$mpdf->SetFooter($arr);
    		$mpdf->SetTitle('LAP JASA PELAYANAN DOKTER');
    		$mpdf->WriteHTML("
												<style>
												.t1 {
													border: 1px solid black;
													border-collapse: collapse;
													font-size: 20;
													font-family: Arial, Helvetica, sans-serif;
												}
												.formarial {
													font-family: Arial, Helvetica, sans-serif;
												}
												h1 {
													font-size: 12px;
												}
    
												h2 {
													font-size: 10px;
												}
    
												h3 {
													font-size: 8px;
												}
    
												table2 {
													border: 1px solid white;
												}
												.one {border-style: dotted solid dashed double;}
												</style>
							");
    
    		//-------------------------MENGATUR TAMPILAN HEADER KOP HASIL LABORATORIUM(NAMA RS DAN ALAMAT)------------------------------------------------
    		foreach ($queryRS as $line)//while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC))
    		{
    			if($line->phone2 == null || $line->phone2 == ''){
    				$telp=$line->phone1;
    			}else{
    				$telp=$line->phone1." / ".$line->phone2;
    			}
    			if($line->fax == null || $line->fax == ''){
    				$fax="";
    			} else{
    				$fax="Fax. ".$line->fax;
    			}
    			$mpdf->WriteHTML("
								<table width='1000' cellspacing='0' border='0'>
								   	 <tr>
									   	 <td width='76'>
									   	 <img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
									   	 </td>
									   	 <td>
									   	 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line->name."</font></b><br>
									   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line->address."</font><br>
									   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telp. ".$telp."</font><br>
									   	 <font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>$fax</font>
									   	 </td>
								   	 </tr>
							   	 </table>
				");
    
    		}
    
    		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
    		$mpdf->WriteHTML("<h1 class='formarial' align='center'>LAPORAN JASA PELAYANAN DOKTER PER PASIEN</h1>");
    		$mpdf->WriteHTML("<h1 class='formarial' align='center'>RSU BHAKTI ASIH</h1>");
    		$mpdf->WriteHTML("<h2 class='formarial' align='center'>$tglAwal s/d $tglAkhir</h2>");
    		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
    		$mpdf->WriteHTML('
												<table class="t1" border = "1" style="overflow: wrap">
												<thead>
												  <tr>
													<th width="30">No</td>
													<th width="600" align="center">DOKTER/PASIEN</th>
													<th width="200" align="right">JP. DOKTER</th>
													<th width="80" align="right">PAJAK</th>
													<th width="200" align="right">JUMLAH</th>
												  </tr>
												</thead>
    
							');
    		//	echo json_encode($query);
    		foreach ($query as $line)
    		{
    
    			$no++;
    			$mpdf->WriteHTML('
    
													<tbody>
							
														<tr class="headerrow">
															<th>'.$no.'</th>
															<td width="200" align="left" colspan="4">'.$line->dokter.'</td>
														</tr>
									
								');
    			$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
    			$dok=$qdok->kd_dokter;
    
    			$queryHasil2 = $this->db->query( "
    					Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
    					Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
    					From Detail_TRDokter dtd
    					INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
    					INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
    					INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
    					INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
    					INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
    					INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
    					INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
    					INNER JOIN unit u On u.kd_unit=t.kd_unit
    					Where t.ispay='1'
    					And dt.Kd_kasir='06'
    					And Folio in ('A','E')
    					and u.kd_bagian='3'
    					And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
    					And dt.Qty * dtd.JP >0
    					And dtd.kd_Dokter='$dok'
    					".$paramcustomer."".$params."".$paramunit."
										Group By d.Nama, p.Kd_pasien
										Order By Dokter, p.Kd_pasien
    
    
						                                      ");
    			$query2 = $queryHasil2->result();
    			$noo=0;
    			$sub_jumlah=0;
    			$sub_pph=0;
    			$sub_jd=0;
    			foreach ($query2 as $line2)
    			{
    					
    				$noo++;
    				$sub_jumlah+=$line2->jumlah;
    				$sub_pph +=$line2->pph;
    				$sub_jd+=$line2->jd;
    				$mpdf->WriteHTML('
															<tr class="headerrow">
																<td width="50"> </td>
																<td width="400">'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
																<td width="200" align="right">'.number_format($line2->jd,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->pph,0,',','.').'</td>
																<td width="200" align="right">'.number_format($line2->jumlah,0,',','.').'</td>
															</tr>
    
														');
    			}
    			$mpdf->WriteHTML('
    
														<tr class="headerrow">
															<th align="right" colspan="2">Sub Total</th>
															<th width="200" align="right">'.number_format($sub_jd,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_pph,0,',','.').'</th>
															<th width="200" align="right">'.number_format($sub_jumlah,0,',','.').'</th>
														</tr>
    
									');
    			$jd += $sub_jd;
    			$pph += $sub_pph;
    			$grand += $sub_jumlah;
    		}
    		$mpdf->WriteHTML('
								            <tr class="headerrow">
								                <th align="right" colspan="2">GRAND TOTAL</th>
												<th width="200" align="right">'.number_format($jd,0,',','.').'</th>
												<th width="200" align="right">'.number_format($pph,0,',','.').'</th>
												<th align="right" width="200">'.number_format($grand,0,',','.').'</th>
								            </tr>
    
					    	');
    
    		$mpdf->WriteHTML('</tbody></table>');
    		$tmpbase = 'base/tmp/';
    		$tmpname = time().'IGD';
    		$mpdf->Output($tmpbase.$tmpname.'.pdf', 'F');
    			
    		$res= '{ success : true, msg : "", id : "", title : "Laporan Jasa Pelayanan Dokter", url : "'.base_url().$tmpbase.$tmpname.'.pdf'.'"'.'} ';
    	}
    	echo $res;
    }
	
	
	
	
	public function rep020211($UserID,$Params)
    {   
	$logors =  base_url()."ui/images/Logo/LOGORSBA.png";
           $UserID = 0;
           $Split = explode("##@@##", $Params,  10);
                   $tglsum = $Split[0];
				$tglsummax = $Split[1];
				
				 $tglsumx=$Split[0]+strtotime("+1 day");
				$tglsum2= date("Y-m-d", $tglsumx);
				$tglsummaxx=$Split[1]+strtotime("+1 day");
				$tglsummax2= date("Y-m-d", $tglsummaxx);
				
				if($Split[2]==="Semua" || $Split[2]==="undefined")
				{
				$kdUNIT2="";
				$kdUNIT="";
				}else
				{
				$kdUNIT2=" and t.kd_unit='".$Split[2]."'";
				
				$kdUNIT=" and kd_unit='".$Split[2]."'";
				}
				
				if($Split[3]==="Semua")
				{
				$jniscus="";
				}
				else{
				$jniscus=" and  Ktr.Jenis_cust=".$Split[3]."";
				}
				
				
				if ($Split[4]==="NULL")
				{
				$customerx="";
				}
				else
				{
				$customerx=" And Ktr.kd_Customer='".$Split[4]."'";
				}
				if (count($Split)===9)
				{
				$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
				if ($Split[7]=== "ya")
				{
				$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
				INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$pendaftaranx="";
				}
				
			    if ($Split[8]=== "ya")
				{
				$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
				              From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$tindakanx="";
				}
				}
				else {
					if ($Split[6]=== "ya")
				{
				$pendaftaranx=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a 
				INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$pendaftaranx="";
				}
				
			    if ($Split[7]=== "ya")
				{
				$tindakanx=" and x.KD_PRODUK NOT IN (Select distinct Kd_Produk
				              From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='2')";
				}else
				{
				$tindakanx="";
				}
				$Shift4x="";
				}
				$shiftx = substr($Split[5], 0, -1);
			
					
                   $criteria="";
                   $tmpunit = explode(',', $Split[3]);
                      for($i=0;$i<count($tmpunit);$i++)
                       {
                       $criteria .= "'".$tmpunit[$i]."',";
                       }
					$criteria = substr($criteria, 0, -1);
					$this->load->library('m_pdf');
					$this->m_pdf->load();
					$queryRS = "select * from db_rs";
					$resultRS = pg_query($queryRS) or die('Query failed: ' . pg_last_error());
					$no = 0;
					$mpdf= new mPDF('utf-8', 'A4');
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumPrefix = 'Hal : ';
					$mpdf->pagenumSuffix = '';
					$mpdf->nbpgPrefix = ' Dari ';
					$mpdf->nbpgSuffix = '';
					$date = date("d-M-Y / H:i:s");
                           $arr = array (
                             'odd' => array (
                                   'L' => array (
                                     'content' => 'Operator : (0) Admin',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'C' => array (
                                     'content' => "Tgl/Jam : ".$date."",
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'R' => array (
                                     'content' => '{PAGENO}{nbpg}',
                                     'font-size' => 8,
                                     'font-style' => '',
                                     'font-family' => 'serif',
                                     'color'=>'#000000'
                                   ),
                                   'line' => 0,
                             ),
                             'even' => array ()
                           );
                   $mpdf->SetFooter($arr);
                   $mpdf->SetTitle('LAPDETAILPASIEN');
                   $mpdf->WriteHTML("
                   <style>
                   .t1 {
                           border: 1px solid black;
                           border-collapse: collapse;
                           font-size: 11px;
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   .formarial {
                           font-family: Arial, Helvetica, sans-serif;
                   }
                   h1 {
                           font-size: 12px;
                   }

                   h2 {
                           font-size: 10px;
                   }

                   h3 {
                           font-size: 8px;
                   }

                   table2 {
                           border: 1px solid white;
                   }
                   </style>
                   ");
                           while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) 
                                   {
                                   $mpdf->WriteHTML("
								
									 <table width='1000' cellspacing='0' border='0'>
													 <tr>
													 <td width='76'>
													 <img src='./ui/images/Logo/LOGORSBA.png' width='76' height='74' />
													 </td>
													 <td>
													 <b><font style='font-size: 16px; font-family: Arial, Helvetica, sans-serif;'>".$line['name']."</font></b><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>".$line['address']."</font><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Telepon : ".$line['phone1']."".$line['phone2']."".$line['phone3']." - ".$line['zip']."</font><br>
														<font style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;'>Fax :".$line['fax']."</font>
													 </td>
													 </tr>
													 </table>
                            ");
                                   }

                                   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Laporan Transaksi</h1>");
								   $mpdf->WriteHTML("<h1 class='formarial' align='center'>Per Poli Per Produk</h1>");
                                   $mpdf->WriteHTML("<h2 class='formarial' align='center'>Periode '".$tglsum."' s/d '".$tglsummax."'</h2>");
                                   $mpdf->WriteHTML('
                                   <table class="t1" border = "1">
                                    <thead>
                                     <tr class="headerrow">
                                           <th align="center" width="24" rowspan="2">Poli. </th>
                                           <th align="center" width="300" rowspan="2">Tindakan</th> 
                                            <th align="center" width="90" rowspan="2"> Pasien</th>
                                            <th align="center" width="90" rowspan="2">Produk</th>
										    <th align="center" width="90" rowspan="2">(Rp.)</th>
                                          
                                     </tr>
                                    
                                   </thead>

                                   ');
			
                           $fquery = pg_query("select kd_unit,nama_unit from unit where kd_bagian ='03' $kdUNIT 
                           group by kd_unit,nama_unit  order by nama_unit asc
							");
                           $mpdf->WriteHTML('<tbody>');

                           while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
                           {

					$query = "Select U.Nama_Unit as namaunit, p.deskripsi as keterangan, Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='06' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> 'T1'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'						
							And left(k.kd_Unit,1) ='3'	
							And k.kd_Unit ='".$f['kd_unit']."'								
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx							
							Group By u.Nama_Unit, p.Deskripsi		
							Order by U.Nama_Unit, p.Deskripsi ";
                           $result = pg_query($query) or die('Query failed: ' . pg_last_error());
                           $i=1;

                                   if(pg_num_rows($result) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
                                    $mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr><th width="90" align="right">'.$f['nama_unit'].'</th></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');	
									$mpdf->WriteHTML('<tr class="headerrow"></tr>');
									$pasientotal=0;
									$Jumlahtindakan=0;
									$duittotal=0;
									while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
                                   {
										$pasientotal+=$line['totalpasien'];
										$Jumlahtindakan+=$line['jumlahtindakan'];
										$duittotal+=$line['duittotal'];
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">'.$i.'</td>
                                                           <td width="300" align="left">'.$line['keterangan'].'</td>
                                                           <td width="90" align="right">'. $line['totalpasien'].'</td>
														    <td width="90" align="right">'.$line['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								    $mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><td colspan="2">Sub Total '.$f['nama_unit'].' : </td><td colspan="1" align="right">'.$pasientotal.'</td><td colspan="1" align="right">'.$Jumlahtindakan.'</td><td colspan="1" align="right">'.substr(number_format($duittotal,2,',','.'),0,-3).'</td></tr>');						  
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									$mpdf->WriteHTML('<tr><p>&nbsp;</p></tr>');
									
						
							
							
							
								}
                              //$mpdf->WriteHTML('<tr><td colspan="15"></td></tr>');
                                }
						     $query2 = "Select  Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From (
							(
							(
							Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk
							) 
							INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
							From  detail_transaksi Where kd_kasir ='06' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
							$Shift4x)  
							Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x
							ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
							) 
							INNER JOIN Unit u On u.kd_unit=t.kd_unit
							) 
							INNER JOIN Produk p on p.kd_produk=x.kd_produk 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN 
							(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> 'T1'
							group by  kd_kasir,no_transaksi 
							) db 
							On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
							Where t.ispay='true'		
														
							And left(k.kd_Unit,1) ='3'	
							$kdUNIT2
							$jniscus				
							$customerx
							$pendaftaranx	
							$tindakanx";
								 $result2 = pg_query($query2) or die('Query failed: ' . pg_last_error());
									$i=1;

                                   if(pg_num_rows($result2) <= 0)
                                   {
                                           $mpdf->WriteHTML('');
                                   }
                                   else
                                   {
									
										while ($line2 = pg_fetch_array($result2, null, PGSQL_ASSOC)) 
                                   {
										
										
                                           $mpdf->WriteHTML('



                                                   <tr class="headerrow"> 

                                                           <td align="right">Grand Total</td>
                                                           <td width="300" align="left"></td>
                                                           <td width="90" align="right">'.$line2['totalpasien'].'</td>
														    <td width="90" align="right">'.$line2['jumlahtindakan'].'</td>
                                                            <td width="90" align="right">'.substr(number_format($line2['duittotal'],2,',','.'),0,-3).'</td>
                                      

                                                   </tr>

                                       

                                           ');
                                            $i++;
                                   }

                                   $i--;
								   
                                   }
                             $mpdf->WriteHTML('</tbody></table>');

                                              $tmpbase = 'base/tmp/';
                                              $datenow = date("dmY");
                                              $tmpname = time().'TRANSAKSIPasienIGD';
                                              $mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');      

                                              $res= '{ success : true, msg : "", id : "010201", title : "Laporan Pasien Detail", url : "'.base_url().$tmpbase.$datenow.$tmpname.'.pdf'.'"'.'}';



           echo $res;
    }

    
   
   
}

?>