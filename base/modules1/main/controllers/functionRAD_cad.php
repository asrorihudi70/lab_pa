<?php
/**

 * @author Agung
 * Editing by HDHT
 * @copyright NCI 2015
 */


//class main extends Controller {
class functionRAD extends  MX_Controller {		

	public $ErrLoginMsg='';
	public function __construct()
	{

			parent::__construct();
			 $this->load->library('session');
	}
 
	public function index()
	{
		  $this->load->view('main/index',$data=array('controller'=>$this));
	}

	private function GetAntrian($KdPasien,$KdUnit,$Tgl,$Dokter)
	{
	   $result=$this->db->query("select * from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='5'
									and tgl_masuk='".$Tgl."'")->result();
		if(count($result) > 0){
			$urut_masuk=$this->db->query("select max(urut_masuk) as urut_masuk from kunjungan 
								where kd_pasien='".$KdPasien."' 
									and kd_unit='5'
									and tgl_masuk='".$Tgl."'")->row()->urut_masuk;
			$urut=$urut_masuk+1;
		} else{
			$urut=1;
		}
		
		return $urut;
	}
   
	private function GetIdTransaksi($kdkasirpasien)
	{
		$counter = $this->db->query("select counter from kasir where kd_kasir = '$kdkasirpasien'")->row();
		$no = $counter->counter;
		$retVal = $no+1;
	/*	if($sqlnotr>$retVal)
		{
			$retVal=$sqlnotr;	
		}*/
		$query = "update kasir set counter=$retVal where kd_kasir='$kdkasirpasien'";
		$update = $this->db->query($query);
		
		//-----------insert to sq1 server Database---------------//
			_QMS_query($query);
		//-----------akhir insert ke database sql server----------------//
		return $retVal;
    }
		
	
	public function GetKodeKasirPenunjang($kdunit,$cUnit)
	{
		$result = $this->db->query("Select * From Kasir_Unit Where Kd_unit='5' and kd_asal= '".$cUnit."'")->result();
		foreach ($result as $data)
		{
			$kodekasirpenunjang = $data->kd_kasir;
		}
		return $kodekasirpenunjang;
	}
	
	public function GetKodeAsalPasien($cUnit)
	{	$cKdUnitAsal = "";

		$result = $this->db->query("Select * From Asal_Pasien Where Kd_unit=  left('".$cUnit."', 1)")->result();

		foreach ($result as $data)
		{
			$cKdUnitAsal = $data->kd_asal;
		}
		
		if ($cKdUnitAsal != "")
		{
		   $kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}else{
			//jika kunjungan langsung atau kd_unit_asal=''
			$cKdUnitAsal='1';
			$kodekasirpenunjang = $this->GetKodeKasirPenunjang($cUnit, $cKdUnitAsal);
		}
		return $kodekasirpenunjang;
	}

	public function savedetailpenyakit()
	{	$this->db->trans_begin();
		$KdTransaksi = $_POST['KdTransaksi'];
		$KdPasien = $_POST['KdPasien'];
		$TglTransaksiAsal = $_POST['TglTransaksiAsal'];
		$NmPasien = $_POST['NmPasien'];
		$Ttl = $_POST['Ttl'];
		$Alamat = $_POST['Alamat'];
		$JK = $_POST['JK'];
		$GolDarah = $_POST['GolDarah'];
		$KdUnit = $_POST['KdUnit'];
		$KdDokter = $_POST['KdDokter'];
		$pasienBaru=$_POST["pasienBaru"];//variabel untuk kunjungan langsung
		$Tgl = date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list = json_decode($_POST['List']);
		$jmlfield =$_POST['JmlField'];
		$jmlList = $_POST['JmlList'];
		$unit = $_POST['unit'];//kd_unit rad
		/* $KdProduk = $_POST['KdProduk'];
		$KdLab = $KdProduk;
		$KdTes = $KdProduk; */
		$rad_cito = $this->db->query("select setting from sys_setting where key_data = 'sys_rad_cito'")->row()->setting;
		$TmpNotransAsal = $_POST['TmpNotransaksi'];//no transaksi asal jika bukan kunjungan langsung
		$KdKasirAsal = $_POST['KdKasirAsal'];//Kode kasir asal
		$KdCusto = $_POST['KdCusto'];
		$TmpCustoLama = $_POST['TmpCustoLama'];//kd customer jika jenis transaksi lama
		$NamaPesertaAsuransi = $_POST['NamaPesertaAsuransi'];
		$NoAskes = $_POST['NoAskes'];
		$NoSJP = $_POST['NoSJP'];
		$KdSpesial = $_POST['KdSpesial'];
		$Kamar = $_POST['Kamar'];
		$kdUser=$this->session->userdata['user_id']['id'] ;
		$listtrdokter= json_decode($_POST['listTrDokter']);
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		if($KdUnit=='')
		{
			$KdUnit='5';
			$TglTransaksiAsal=$Tgl;
		}else
		{
			$KdUnit=$KdUnit;
			$TglTransaksiAsal=$TglTransaksiAsal;
		}

		if ($KdPasien == '' && $pasienBaru ==1)
		{	//jika kunjungan langsung
			$KdPasien = $this->GetIdRad();
			$savepasien= $this->SimpanPasien($KdPasien,$NmPasien,$Ttl,$Alamat,$JK,$GolDarah,$NoAskes,$NamaPesertaAsuransi);
		}else 
		{
		$KdPasien = $KdPasien;
		}
		if($KdCusto=='')
		{
		$KdCusto=$TmpCustoLama;
		}
		else
		{
		$KdCusto=$KdCusto;
		}
		$kdkasirpasien = $this->GetKodeAsalPasien($KdUnit);
		$urut = $this->GetAntrian($KdPasien,$KdUnit,$Tgl,$KdDokter);
		$notrans = $this->GetIdTransaksi($kdkasirpasien);
		if($pasienBaru == 0)
		{
			if(substr($KdUnit, 0, 1) == '1')
			{//RWI
				$IdAsal=1;
			} else if(substr($KdUnit, 0, 1) == '2')
			{//RWJ
				$IdAsal=0;
			} else if(substr($KdUnit, 0, 1) == '3')
			{//UGD
				$IdAsal=0;
			}
		} else
		{
			$IdAsal=2;
		}
		$simpankunjungan = $this->simpankunjungan($KdPasien,$unit,$Tgl,$urut,$KdDokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru);
		if($simpankunjungan==="aya")
		{
		 $simpanmrrad= $this->SimpanMrRad($KdPasien,$unit,$Tgl,$urut);
		 if( $simpanmrrad === "Ok")
		{
			$hasil = $this->SimpanTransaksi($kdkasirpasien,$notrans,$KdPasien,$unit,$Tgl,$urut);
			if($hasil ==="sae")
			{
				if($KdUnit != '' && substr($KdUnit, 0, 1) =='1'){//jika bersal dari rawat inap
						$simpanunitasalinap = $this->SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial);
						if($simpanunitasalinap==="Ok")
						{
							if( $pasienBaru == 0){//jika bukan Pasien baru/kunjungan langsung
								$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
							}else{
								$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
							}if($simpanunitasall==="Ok")
								{
										for($i=0;$i<count($list);$i++){
											$kd_produk=$list[$i]->KD_PRODUK;
											$qty=$list[$i]->QTY;
											$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
											$tgl_berlaku=$list[$i]->TGL_BERLAKU;
											if (isset($list[$i]->cito))
											{
												$cito=$list[$i]->cito;
												if($cito=='Ya')
												{
												$cito='1';
												$harga=$list[$i]->HARGA;
												$hargacito = (((int) $harga) * ((int)$rad_cito))/100;
												$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
												
												}
												else if($cito=='Tidak')
												{
												$cito='0';
												$harga=$list[$i]->HARGA;
												}
											} else
											{
												$cito='0';
												$harga=$list[$i]->HARGA;
											}
											$kd_tarif=$list[$i]->KD_TARIF;
											$urutdetailtransaksi = $this->db->query("select
											geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
											$u=$i+1;											//insert detail_transaksi
											$query = $this->db->query("select insert_detail_transaksi
											(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
												'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$unit."',
												'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false')"); 
											if($cito==='1')
											{
											 $query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
											 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'"); 
											  $query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'"); 
											}
											$qsql=$this->db->query(" select * from detail_component 
																	where kd_kasir='".$kdkasirpasien."' 
																	and no_transaksi='".$notrans."'
																	and urut=".$urutdetailtransaksi."
																	and tgl_transaksi='".$Tgl."'")->result();
											
											foreach($qsql as $line)
											{
												$qkd_kasir=$line->kd_kasir;
												$qno_transaksi=$line->no_transaksi;
												$qurut=$line->urut;
												$qtgl_transaksi=$line->tgl_transaksi;
												$qkd_component=$line->kd_component;
												$qtarif=$line->tarif;
											}
											if($query){
												$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component 
																			where 
																			(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
																			kd_unit ='".$unit."' AND
																			kd_produk='".$kd_produk."' AND
																			tgl_berlaku='".$tgl_berlaku."' AND
																			kd_tarif='".$kd_tarif."' group by tarif")->result();
																			foreach($ctarif as $ct)
																			{
																				if($ct->jumlah != 0)
																				{
																					$trDokter = $this->db->query("insert into detail_trdokter select '02','".$notrans."'
																					,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 WHERE
																					NOT EXISTS (
																					SELECT * FROM detail_trdokter WHERE   
																					kd_kasir= '02' AND
																					tgl_transaksi='".$qtgl_transaksi."' AND
																					urut='".$qurut."' AND
																					kd_dokter = '".$_POST['KdDokter']."' AND
																					no_transaksi='".$notrans."')");
																				}
																			}
												$query2 = $this->db->query("Insert into rad_hasil (kd_test,  kd_pasien,kd_unit, tgl_masuk, urut_masuk,  urut,kd_unit_asal,keluhan) 
												values  (".$kd_produk.",'".$KdPasien."','".$unit."','".$Tgl."',".$urut.",".$u.",'".$KdUnit."','')");
												if($query2)
												{
												echo '{success: true, KD_PASIEN: "'.$KdPasien.'", NO_TRANS: "'.$notrans.'", KASIR: "'.$kdkasirpasien.'"}';
												} 
												else
												{
												echo "{success:false}";
												}
											}
											else
											{
											echo "{success:false}";
											}
										}
									
								}else
								{
								 echo "{success:false}";
								}
						}else
						{
						echo "{success:false}";
						}
				}else{
						if( $pasienBaru == 0){//jika bukan Pasien baru/kunjungan langsung
							$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal);
						}else{
							$simpanunitasall = $this->SimpanUnitAsal($kdkasirpasien,$notrans,$notrans,$kdkasirpasien,$IdAsal);
						} if($simpanunitasall==="Ok")
							{
								for($i=0;$i<count($list);$i++){
									$kd_produk=$list[$i]->KD_PRODUK;
									$qty=$list[$i]->QTY;
									$tgl_transaksi=$list[$i]->TGL_TRANSAKSI;
									$tgl_berlaku=$list[$i]->TGL_BERLAKU;
									if (isset($list[$i]->cito))
											{
												$cito=$list[$i]->cito;
												if($cito=='Ya')
												{
												$cito='1';
												$harga=$list[$i]->HARGA;
												$hargacito = (((int) $harga) * ((int)$rad_cito))/100;
												$harga=((int)$list[$i]->HARGA)+((int)$hargacito);
												}
												else if($cito=='Tidak')
												{
												$cito='0';
												$harga=$list[$i]->HARGA;
												}
											} else
											{
											 $cito='0';
											 $harga=$list[$i]->HARGA;
											}
									$kd_tarif=$list[$i]->KD_TARIF;
									$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('$kdkasirpasien','".$notrans."','".$Tgl."') as urutdetail")->row()->urutdetail;
									$u=$i+1;
									$query = $this->db->query("select insert_detail_transaksi
									(	'".$kdkasirpasien."', '".$notrans."',".$urutdetailtransaksi.",
										'".$Tgl."',".$kdUser.",'".$kd_tarif."',".$kd_produk.",'".$unit."',
										'".$tgl_berlaku."','false','false','',".$qty.",".$harga.",".$Shift.",'false')"); 
									if($cito==='1')
									{
									 $query = $this->db->query("update detail_transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'
									 and urut=".$urutdetailtransaksi." and tgl_transaksi='".$Tgl."'"); 
									  $query = $this->db->query("update transaksi set cito=1 where kd_kasir='".$kdkasirpasien."' and no_transaksi='".$notrans."'"); 
									}
									$qsql=$this->db->query(" select * from detail_component 
															where kd_kasir='".$kdkasirpasien."' 
															and no_transaksi='".$notrans."'
															and urut=".$urutdetailtransaksi."
															and tgl_transaksi='".$Tgl."'")->result();
									
									foreach($qsql as $line)
									{
										$qkd_kasir=$line->kd_kasir;
										$qno_transaksi=$line->no_transaksi;
										$qurut=$line->urut;
										$qtgl_transaksi=$line->tgl_transaksi;
										$qkd_component=$line->kd_component;
										$qtarif=$line->tarif;
									}
									
									if($query)
									{
										$ctarif = $this->db->query("select count(kd_component) as jumlah,tarif from tarif_component where 
													(kd_component = '".$kdjasadok."' OR  kd_component = '".$kdjasaanas."') AND
													kd_unit ='".$unit."' AND
													kd_produk='".$kd_produk."' AND
													tgl_berlaku='".$tgl_berlaku."' AND
													kd_tarif='".$kd_tarif."' group by tarif")->result();
													foreach($ctarif as $ct)
													{
														if($ct->jumlah != 0)
														{
															$trDokter = $this->db->query("insert into detail_trdokter select '02','".$notrans."'
															,'".$qurut."','".$_POST['KdDokter']."','".$qtgl_transaksi."',0,0,".$ct->tarif.",0,0,0 WHERE
															NOT EXISTS (
															SELECT * FROM detail_trdokter WHERE   
															kd_kasir= '02' AND
															tgl_transaksi='".$qtgl_transaksi."' AND
															urut='".$qurut."' AND
															kd_dokter = '".$_POST['KdDokter']."' AND
															no_transaksi='".$notrans."')");
														}
													}
										$query2 = $this->db->query("Insert into rad_hasil (kd_test,  kd_pasien,kd_unit, tgl_masuk, urut_masuk,  urut,kd_unit_asal,keluhan) 
										values  (".$kd_produk.",'".$KdPasien."','".$unit."','".$Tgl."',".$urut.",".$u.",'".$KdUnit."','')");
										if($query2)
										{
										echo '{success: true, KD_PASIEN: "'.$KdPasien.'", NO_TRANS: "'.$notrans.'", KASIR: "'.$kdkasirpasien.'"}';
										} 
										else
										{
										echo "{success:false}";
										}
									}
									else
									{
									echo "{success:false}";
									}
								}

							}else
							{
							echo "{success:false}";
							}
					}
			}
			else
			{
			echo "{success:false}";
			}
			
		}
		else
		{
		 echo "{success:false}";
		}
		}else
		{
		echo "{success:false}";
		}
		
	if ($this->db->trans_status() === FALSE)
		{
		$this->db->trans_rollback();
		}
		else
		{
		$this->db->trans_commit();
		} 
	
	}
	

	
	public function SimpanUnitAsal($kdkasirpasien,$notrans,$TmpNotransAsal,$KdKasirAsal,$IdAsal)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"no_transaksi_asal"=>$TmpNotransAsal,
							"kd_kasir_asal"=>$KdKasirAsal,
							"id_asal"=>$IdAsal
						);

		$this->load->model("general/tb_unit_asal");
		$result = $this->tb_unit_asal->Save($data);
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}


        return $strError;
	}
	
	public function SimpanUnitAsalInap($kdkasirpasien,$notrans,$KdUnit,$Kamar,$KdSpesial)
	{
		$strError = "";
			$data = array("kd_kasir"=>$kdkasirpasien,
							"no_transaksi"=>$notrans,
							"kd_unit"=>$KdUnit,
							"no_kamar"=>$Kamar,
							"kd_spesial"=>$KdSpesial
						);
		$result=$this->db->insert('unit_asalinap',$data);
		
		//-----------insert to sq1 server Database---------------//
		_QMS_insert('unit_asalinap',$data);
		//-----------akhir insert ke database sql server----------------//
		if ($result){
			$strError = "Ok";
		}else{
			$strError = "eror, Not Ok";
		}  return $strError;
	}
	
	
	public function SimpanMrRad($KdPasien,$unit,$Tgl,$urut)
	{
		$strError = "";
		$data = array("kd_pasien"=>$KdPasien,
							"kd_unit"=>$unit,
							"tgl_masuk"=>$Tgl,
							"urut_masuk"=>$urut
						);
		$result=$this->db->insert('mr_rad',$data);
		//-----------insert to sq1 server Database---------------//
		if($result)
		{
		 $strError = "Ok";
		}else{
			  $strError = "eror, Not Ok";
			 }
		return $strError;
	}
        
        
    public function SimpanPasien($kdpasien,$namaPasien,$tglLahirPasien,$alamatPasien,$jkPasien,$goldarPasien,$NoAskes,$NamaPesertaAsuransi)
	{
		$strError = "";
		$suku = 0;         
		
			$data = array("kd_pasien"=>$kdpasien,
					"nama"=>$namaPasien,
					"jenis_kelamin"=>$jkPasien,
					"tgl_lahir"=>$tglLahirPasien,
					"gol_darah"=>$goldarPasien,
					"alamat"=>$alamatPasien,
					"no_asuransi"=>$NoAskes,
					"pemegang_asuransi"=>$NamaPesertaAsuransi,
					"kd_kelurahan"=>0,
					"kd_pendidikan"=>0,
					"kd_pekerjaan"=>0,
					"kd_suku"=>$suku,
					"kd_perusahaan"=>0);
			
			 $datasql = array("kd_pasien"=>$kdpasien,"nama"=>$namaPasien,
					"nama_keluarga"=>"","jenis_kelamin"=>$jkPasien,
					"tempat_lahir"=>"","tgl_lahir"=>$tglLahirPasien,
					"gol_darah"=>$goldarPasien,"status_marita"=>0,
					"wni"=>0,"alamat"=>$alamatPasien,
					"telepon"=>"","kd_kelurahan"=>0,
					"kd_pendidikan"=>0,"kd_pekerjaan"=>0,
					"kd_perusahaan"=>0,
					"no_asuransi"=>$NoAskes,
					"pemegang_asuransi"=>$NamaPesertaAsuransi);
			
		

		$criteria = "kd_pasien = '".$kdpasien."'";

		$this->load->model("rawat_jalan/tb_pasien");
		$this->tb_pasien->db->where($criteria, null, false);
		$query = $this->tb_pasien->GetRowList( 0,1, "", "","");
		if ($query[1]==0)
		{
			$data["kd_pasien"] = $kdpasien;
			$result = $this->tb_pasien->Save($data);
			
			//-----------insert to sq1 server Database---------------//
			_QMS_insert('Pasien',$datasql);
			//-----------akhir insert ke database sql server----------------//
			$strError = "ada";
		}
		return $strError;
	}

	private function GetIdRad()
	{
		$kdpasien = "";
		$res = $this->db->query("select kd_pasien from pasien where left(kd_pasien,2) = 'RD'  Order By kd_pasien desc Limit 1")->result();
		
		foreach ($res as $line) {
			$kdpasien = $line->kd_pasien;
		}
		if ($kdpasien != "")
		{
			$nm = $kdpasien;                
			
			$retVal= substr($nm, -5);
			$nomor = (int) $retVal +1;
			$getnewmedrec = "RD".$nomor;
		}else
		{
		  $strNomor= "RD000";
		  $getnewmedrec=$strNomor."01";
		}
		return $getnewmedrec;
	}
	
        
		public function SimpanKunjungan($kdpasien,$unit,$Tgl,$urut,$kddokter,$Shift,$KdCusto,$NoSJP,$IdAsal,$pasienBaru)
		{
			//echo "masuk";
				$strError = "";
				$tmpkdcusto = '0000000001';
				//echo $tmpkdcusto;
				$JamKunjungan = date('Y-m-d h:i:s');
				$data = array("kd_pasien"=>$kdpasien,
							  "kd_unit"=>$unit,
							  "tgl_masuk"=>$Tgl,
							  "kd_rujukan"=>"0",
							  "urut_masuk"=>$urut,
							  "jam_masuk"=>$JamKunjungan,
							  "kd_dokter"=>$kddokter,
							  "shift"=>$Shift,
							  "kd_customer"=>$KdCusto,
							  "karyawan"=>"0",
							  "no_sjp"=>$NoSJP,
							  "keadaan_masuk"=>0,
							  "keadaan_pasien"=>0,
							  "cara_penerimaan"=>99,
							  "asal_pasien"=>$IdAsal,
							  "cara_keluar"=>0,
							  "baru"=>$pasienBaru,
							  "kontrol"=>"0"
							  );

				
				//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER

				$criteria = "kd_pasien = '".$kdpasien."' AND kd_unit = '".$unit."' AND tgl_masuk = '".$Tgl."' AND urut_masuk=".$urut."";

				$this->load->model("rawat_jalan/tb_kunjungan_pasien");
				$this->tb_kunjungan_pasien->db->where($criteria, null, false);
				$query = $this->tb_kunjungan_pasien->GetRowList( 0,1, "", "","");
				if ($query[1]==0)
				{
					//print_r($datasql);
					$result = $this->tb_kunjungan_pasien->Save($data);
					//-----------insert to sq1 server Database---------------//
					if ($result)
					{
					$strError = "aya";
					}else{
						 $strError = "eror";
						 }
				}else{
					 $strError = "eror";
					 }
			return $strError;
		}


        public function SimpanTransaksi($kdkasirasalpasien,$notrans,$kdpasien,$KdUnit,$Tgl,$Schurut)
        {            
            $kdpasien;
			$unit;
			$Tgl;
			$strError = "";
            $data = array("kd_kasir"=>$kdkasirasalpasien,
                          "no_transaksi"=>$notrans,
                          "kd_pasien"=>$kdpasien,
                          "kd_unit"=>$KdUnit,
                          "tgl_transaksi"=>$Tgl,
                          "urut_masuk"=>$Schurut,
                          "tgl_co"=>NULL,
                          "co_status"=>"False",
                          "orderlist"=>NULL,
                          "ispay"=>"False",
                          "app"=>"False",
                          "kd_user"=>"0",
                          "tag"=>NULL,
                          "lunas"=>"False",
                          "tgl_lunas"=>NULL,
						  "posting_transaksi"=>"True");
						//AKHIR DATA ARRAY UNTUK SAVE KE SQL SERVER
            $criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$kdkasirasalpasien."'";
            $this->load->model("general/tb_transaksi");
            $this->tb_transaksi->db->where($criteria, null, false);
            $query = $this->tb_transaksi->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {          
                $result = $this->tb_transaksi->Save($data);
				//-----------insert to sq1 server Database---------------//
                if($result){
					$strError = "sae";
				} else{
					$strError = "error";
				}
            }
            return $strError;
        }
        
	public function getDokterPengirim(){
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$nama=$this->db->query("SELECT DTR.NAMA, DTR.KD_DOKTER
								FROM TRANSAKSI 
									INNER JOIN UNIT_ASAL ON UNIT_ASAL.NO_TRANSAKSI = TRANSAKSI.NO_TRANSAKSI and UNIT_ASAL.KD_KASIR = TRANSAKSI.KD_KASIR
									INNER JOIN TRANSAKSI t2 ON t2.NO_TRANSAKSI = UNIT_ASAL.NO_TRANSAKSI_ASAL and UNIT_ASAL.KD_KASIR_ASAL = t2.KD_KASIR
									INNER JOIN KUNJUNGAN ON T2.KD_PASIEN = KUNJUNGAN.KD_PASIEN AND T2.KD_UNIT = KUNJUNGAN.KD_UNIT AND 
										T2.TGL_TRANSAKSI = KUNJUNGAN.TGL_MASUK AND T2.URUT_MASUK = KUNJUNGAN.URUT_MASUK 
									INNER JOIN DOKTER DTR ON DTR.KD_DOKTER = KUNJUNGAN.KD_DOKTER
								WHERE     (TRANSAKSI.NO_TRANSAKSI = '".$no_transaksi."') AND (TRANSAKSI.KD_KASIR = '".$kd_kasir."')")->row()->nama;
		if($nama){
			echo "{success:true,nama:'$nama'}";
		} else{
			echo "{success:false}";
		}
	}
		
	public function getPasien(){
		$tanggal=date('d-M-Y',strtotime($_POST['tanggal']));
		$tanggal2 = str_replace('/', '-', $tanggal);
		$yesterday = date('d-M-Y',strtotime($tanggal2 . "-3 days"));
		
		if($_POST['a'] == 0 || $_POST['a'] == '0'){
			$no_transaksi = " and tr.no_transaksi like '".$_POST['text']."%'";
			$nama = "";
			$kd_pasien = "";
		} else if($_POST['a'] == 1 || $_POST['a'] == '1'){
			$kd_pasien = " and pasien.kd_pasien like '".$_POST['text']."%'";
			$nama = "";
			$no_transaksi = "";
		} else if($_POST['a'] == 2 || $_POST['a'] == '2'){
			$nama = " and upper(pasien.nama) like upper('%".$_POST['text']."%')";
			$kd_pasien = "";
			$no_transaksi = "";
		}
		
		
		$result=$this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, 
									kunjungan.TGL_MASUK AS MASUK, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
									dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir,
									to_char(tr.tgl_transaksi,'dd - mm - yy') as tgl, tr.tgl_transaksi, tr.posting_transaksi,tarif_cust.kd_tarif,
									tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
														case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien 
								FROM pasien 
									LEFT JOIN (
										( kunjungan  
										LEFT join ( transaksi tr 
												INNER join unit u on u.kd_unit=tr.kd_unit)  
										on kunjungan.kd_pasien=tr.kd_pasien 
											and kunjungan.kd_unit= tr.kd_unit 
											and kunjungan.tgl_masuk=tr.tgl_transaksi 
											and kunjungan.urut_masuk = tr.urut_masuk
										LEFT join customer on customer.kd_customer = kunjungan.kd_customer
										left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
											and nginap.kd_unit=kunjungan.kd_unit 
											and nginap.tgl_masuk=kunjungan.tgl_masuk 
											and nginap.urut_masuk=kunjungan.urut_masuk 
											and nginap.akhir='t'
											inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
											inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
										)   
										LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
										LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
										LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
										LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
										)
									ON kunjungan.kd_pasien=pasien.kd_pasien  
								WHERE left(kunjungan.kd_unit,1) in ('1','2','3','5')
								and tr.tgl_transaksi >='".$yesterday."' and tr.tgl_transaksi <='".$tanggal."' 
								".$kd_pasien."
								".$no_transaksi."
								".$nama."
								ORDER BY tr.no_transaksi desc limit 10								
							")->result();
							
					 
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			echo json_encode($jsonResult);
	}
	
	public function getItemPemeriksaan(){
		
		$no_transaksi 	= $_POST['no_transaksi'];
		$asal_unit 		= $_POST['asal_unit'];
		$kd_unit 		= substr($this->db->query("SELECT * from unit where nama_unit='".$asal_unit."'")->row()->kd_unit, 0, 1);
		$key_data 	= $this->db->query("SELECT * from sys_setting WHERE key_data='lab_default_kd_unit'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$kd_kasir = "";
		if ($kd_unit==1) {
			$kd_kasir 	= $this->db->query("SELECT getkodekasirpenunjangrwi('".$key_data."')")->row()->getkodekasirpenunjangrwi;
		}else if ($kd_unit==2) {
			$kd_kasir 	= $this->db->query("SELECT getkodekasirpenunjangrwj('".$key_data."')")->row()->getkodekasirpenunjangrwj;
		}else{
			$kd_kasir 	= $this->db->query("SELECT getkodekasirpenunjangigd('".$key_data."')")->row()->getkodekasirpenunjangigd;
		}

		if($no_transaksi == ""){
			$where="";
		} else{
			//$where=" where no_transaksi='".$no_transaksi."' AND kd_kasir ='".$kd_kasir."'";
			$where=" where no_transaksi='".$no_transaksi."' AND kd_kasir ='04'";
		}

		$result=$this->db->query(" select * from (
									select     detail_transaksi.kd_kasir, detail_transaksi.urut, detail_transaksi.no_transaksi, detail_transaksi.tgl_transaksi, detail_transaksi.kd_user, 
									detail_transaksi.kd_tarif, detail_transaksi.kd_produk, detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, 
									detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, detail_transaksi.kd_unit_tr, 
									detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli, 
									detail_transaksi.kd_customer, produk.deskripsi, customer.customer, dokter.nama,tr.jumlah, unit.kd_bagian
								    from  detail_transaksi 
									inner join
								  produk on detail_transaksi.kd_produk = produk.kd_produk 
								  inner join
								  unit on detail_transaksi.kd_unit = unit.kd_unit 
								  left join
								  customer on detail_transaksi.kd_customer = customer.kd_customer 
								  left  join
								  dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
								  
								  left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku from tarif_component 
 where kd_component = '".$kdjasadok."' or kd_component = '".$kdjasaanas."'  
 group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as tr ON tr.kd_unit=detail_transaksi.kd_unit 
 AND tr.kd_produk=detail_transaksi.kd_produk AND tr.tgl_berlaku = detail_transaksi.tgl_berlaku  AND tr.kd_tarif=detail_transaksi.kd_tarif
								  ) as resdata 
								  $where
								  ")->result();
		echo '{success:true, asal_unit:'.json_encode($kd_unit).',key_data:'.json_encode($key_data).', kd_kasir:'.json_encode($kd_kasir).', totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function getCurrentShiftRad(){
		$query=$this->db->query("SELECT bs.shift,b.kd_bagian,b.bagian,to_char(bs.lastdate,'YYYY-mm-dd')as lastdate
									FROM bagian_shift bs
									INNER JOIN bagian b ON b.kd_bagian=bs.kd_bagian
									WHERE b.bagian='Radiologi'")->row();
		$shift=$query->shift;
		$lastdate=$query->lastdate;
		if($shift == 3){
			$today=date('Y-m-d');
			$yesterday=date("Y-m-d", strtotime("yesterday"));
			if($lastdate != $today || $lastdate==$yesterday){
				$shift=4;
			} else{
				$shift=$shift;
			}
		} else{
			$shift=$shift;
		}
		
		echo $shift;
	}
	
	public function update_dokter(){
		$this->db->trans_begin();
		$result=$this->db->query("update kunjungan set kd_dokter='".$_POST['KdDokter']."', kd_customer='".$_POST['TmpCustoLama']."'
									where kd_pasien = '".$_POST['KdPasien']."' 
										AND kd_unit = '".$_POST['KdUnit']."' 
										AND tgl_masuk = '".$_POST['TglTransaksiAsal']."' 
										AND urut_masuk=".$_POST['urutmasuk']." ");
										
		if($result) {	
			$result2=$this->db->query("update detail_trdokter set kd_dokter='".$_POST['KdDokter']."'
										where kd_kasir = '".$_POST['KdKasirAsal']."' 
											AND no_transaksi = '".$_POST['KdTransaksi']."'");
			if($result2) {	
				$this->db->trans_commit();
				echo "{success:true, notrans:'".$_POST['KdTransaksi']."', kdPasien:'".$_POST['KdPasien']."'}";
			}else{
				$this->db->trans_rollback();
				echo "{success:false}";
			}
		} else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}

	}
	
}

?>