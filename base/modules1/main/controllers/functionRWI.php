<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class functionRWI extends  MX_Controller {		

	private $setup_db_sql  = false;
	public $ErrLoginMsg ='';
	private $dbSQL      = "";
    public function __construct()
    {

		$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		}
			parent::__construct();
			$this->load->library('session');
			$this->load->model('M_pembayaran');
			$this->dbSQL = $this->load->database('otherdb2',TRUE);
			$this->load->model("M_produk");
			$this->load->model('Tbl_data_transaksi');
			$this->load->model('Tbl_data_kunjungan');
			$this->load->model('Tbl_data_nginap');
			$this->load->model('Tbl_data_pasien_inap');
			$this->load->model('Tbl_data_detail_tr_kamar');
			$this->load->model('Tbl_data_kamar');
			$this->load->model('Tbl_data_dokter');
			$this->load->model('Tbl_data_customer');
			$this->load->model('Tbl_data_detail_transaksi');
			$this->load->model('Tbl_data_detail_component');
			$this->load->model('Tbl_data_detail_bayar');
			$this->load->model('Tbl_data_tarif_component');
			$this->load->model('Tbl_data_tarif');
			$this->load->model('Tbl_data_visite_dokter');
    }
	 
    public function index()
    {
          $this->load->view('main/index',$data=array('controller'=>$this));
    }
    
	public function cekpassword_rwi(){
    	$q=$this->db->query("select * from sys_setting where key_data='rwi_password_batal_transaksi_kasir' and setting=md5('".$_POST['passDulu']."') ")->result();
		if ($q)
		{
			echo '{success:true}';
		}
		else{
			echo '{success:false}';
		}
    }
	public function UpdateKdCustomer()
	 {
		 
		$KdTransaksi  = $_POST['TrKodeTranskasi'];
		$KdUnit       = $_POST['KdUnit'];
		$KdDokter     = $_POST['KdDokter'];
		$TglTransaksi = $_POST['TglTransaksi'];
		$KdCustomer   = $_POST['KDCustomer'];
		$NoSjp        = $_POST['KDNoSJP'];
		$NoAskes      = $_POST['KDNoAskes'];
		$KdPasien     = $_POST['TrKodePasien'];
		 
       $query = $this->db->query("select updatekdcostumer('".$KdPasien."','".$KdUnit."','".$TglTransaksi."','".$NoSjp."','".$NoAskes."','".$KdCustomer."')");
		 $res = $query->result();
		 if($res)
		 {
			 echo "{success:true}";
		 }
		 else
		 {
			  echo "{success:false}";
		 }
	 }
	 
	 public function kelaskamar()
	{
    	$result=$this->db->query("select unit.nama_unit||'-'||kamar.nama_kamar as kelas_kamar,kelas.kelas as kelas,kamar.nama_kamar  as kamar from kamar
								inner join unit on unit.kd_unit=kamar.kd_unit
								inner join kelas on kelas.kd_kelas=unit.kd_kelas where lower(kamar.nama_kamar) like lower('".$_POST['kode']."%') or 
								lower(unit.nama_unit) like lower('".$_POST['kode']."%')")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	public function KonsultasiPenataJasa()
        {
					$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
                    $kdTransaksi = $this->GetIdTransaksi($kdKasir);

                    $kdUnit = $_POST['KdUnit'];
                    $kdDokter = $_POST['KdDokter'];
                    $kdUnitAsal = $_POST['KdUnitAsal'];
                    $kdDokterAsal = $_POST['KdDokterAsal'];
                    $tglTransaksi = $_POST['TglTransaksi'];
                    $kdCostumer = $_POST['KDCustomer'];
                    $kdPasien = $_POST['KdPasien'];
                    $antrian = $this->GetAntrian($kdPasien,$kdUnit,$tglTransaksi,$kdDokter);
                   

                    if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                         {
                                 list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                                 $ctgl= strtotime($tgl.$bln.$thn);
                                 $tgl_masuk=date("Y-m-d",$ctgl);
                             }

                    $query = $this->db->query("select insertkonsultasitindaklanjut('".$kdPasien."','".$kdUnit."','".$tgl_masuk."',
										'".$kdDokter."','".$kdCostumer."','".$kdKasir."','".$kdTransaksi."','".$kdUnitAsal."',
										'".$kdDokterAsal."',".$antrian.",0)");# 0 adalah cara_penerimaan ketika pasien di konsul ke poli lain
                    $res = $query->result();


                    if($res)
                    {
                            echo '{success: true}';
                    }
                    else
                    {
                            echo '{success: false}';
                    }
            } 
        
        private function GetAntrian($medrec,$Poli,$Tgl,$Dokter)
        {
            $retVal = 1;
            $this->load->model('general/tb_getantrian');
            $this->tb_getantrian->db->where(" kd_pasien = '".$medrec."' and kd_unit = '".$Poli."' and tgl_masuk = '".$Tgl."'and kd_dokter = '".$Dokter."'", null, false);
            $res = $this->tb_getantrian->GetRowList( 0, 1, "DESC", "no_transaksi",  "");
            if ($res[1]>0)
            {
                $nm = $res[0][0]->URUT_MASUK;
                $retVal=$nm;
            }
            return $retVal;
        }
   
        /* private function GetIdTransaksi()
        {
            $strNomor="0".str_pad("00",2,'0',STR_PAD_LEFT);
            $retVal=$strNomor."0001";

            $this->load->model('general/tb_transaksi');
            $this->tb_transaksi->db->where("substring(no_transaksi,1,3) = '".$strNomor."'", null, false);
            $res = $this->tb_transaksi->GetRowList( 0, 1, "DESC", "no_transaksi",  "");

            if ($res[1]>0)
            {
                $nm = substr($res[0][0]->NO_TRANSAKSI, -4);
                $nomor = (int) $nm +1;
                $retVal=$strNomor.str_pad($nomor,4,"00000",STR_PAD_LEFT);
           }
     return $retVal;
    } */
	
		private function GetIdTransaksi($kd_kasir){
			$kd_kasir_rwi= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
			   $counter = $this->db->query("select counter from kasir where kd_kasir = '$kd_kasir_rwi'")->row();
				$no = $counter->counter;
				$retVal2 = $no+1;
				$strNomor='';
				$update = $this->db->query("update kasir set counter=$retVal2 where kd_kasir='$kd_kasir'");
					$retVal=$strNomor.str_pad($retVal2,7,"000000",STR_PAD_LEFT);
			return $retVal;
		}
	

	/*
		PERBARUAN SAVE PEMBAYARAN
		OLEH 	: HDHT 
		TANGGAL : 2017 - 03 - 10
		ALASAN 	: DI SATUKAN PEMBAYARAN KE POSTGRESQL DAN SQLSERVER

	*/
    public function savePembayaran()
	{
		if(isset($_POST['KdPay'])){
			$kd_pay = $this->db->query("select kd_pay from payment where uraian='".$_POST['KdPay']."' or kd_pay='".$_POST['KdPay']."'")->row()->kd_pay;
		} else{
			$kd_pay = $_POST['bayar'];
		}
		$this->M_pembayaran->savePembayaran(
			'default_kd_kasir_rwi', 
			$_POST['TrKodeTranskasi'],
			$_POST['Tgl'],
			$this->GetShiftBagian(),
			$_POST['Flag'],
			date('Y-m-d'),
			$_POST['List'],
			$_POST['JmlList'],
			$_POST['kdUnit'],
			$_POST['Typedata'],
			$kd_pay,
			//$_POST['bayar'],
			$_POST['Totalbayar'],
			$this->session->userdata['user_id']['id']
		);

	}
	
	public function saveDiagnosa()
	{
		
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		
	
		$a = explode("##[[]]##",$list);

		for($i=0;$i<=count($a)-1;$i++)
		{

			$b = explode("@@##$$@@",$a[$i]);
			for($k=1;$k<=count($b)-1;$k++)
			{
							if($b[$k] == 'Diagnosa Awal')
							{
								$diagnosa = 0;
							}
							else if($b[$k] == 'Diagnosa Utama')
							{
								$diagnosa = 1;
							}
				
							else if($b[$k] == 'Komplikasi')
							{
								$diagnosa = 2;
							}
							else if($b[$k] == 'Diagnosa Sekunder')
							{
								$diagnosa = 3;
							}
							else if($b[$k] == 'Baru')
							{
								$kasus = 'TRUE';
							}
							else if($b[$k] == 'Lama')
							{
								$kasus = 'FALSE';
							}
			}
				$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
				$result = $urut->result();
				foreach ($result as $data)
				{
					$Urutan = $data->geturutmrpenyakit;
				}
			
				$query = $this->db->query("select insertdatapenyakit('".$b[1]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");	
			
		
		}
			if($query)
				{
				echo "{success:true}";
				}
				else
				{
					echo "{success:false}";
				}
	}
	
	// public function deletedetail_bayar()
	// {
	// $kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
	// $TrKodeTranskasi = $_POST['TrKodeTranskasi'];
	// $Urut =$_POST['Urut'];
	// $query=$this->db->query(
	// 					 "  select hapusdetailbayar('$kdKasir',
	// 					 '".$TrKodeTranskasi."',
	// 					 ".$Urut.")"
	// 					);
	// 					if($query)
	// 					{
	// 			echo "{success:true}";
	// 			}
	// 			else
	// 			{
	// 				echo "{success:false}";
	// 			}
	// }

	public function deletedetail_bayar()
	{
		$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		
		$_kduser = $this->session->userdata['user_id']['id'];
		$kdKasir =  $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;	
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$Urut =$_POST['Urut'];
		$TmpTglBayar = explode(' ',$_POST['TrTglbayar']);
		$TglBayar = $TmpTglBayar[0];
		$Kodepay=$_POST['Kodepay'];
		$KodeUnit=$_POST['KodeUnit'];
		$NamaPasien=$_POST['NamaPasien'];
		$Namaunit=$_POST['Namaunit'];
		$Tgltransaksi=explode(' ',$_POST['Tgltransaksi']);
		$Kodepasein=$_POST['Kodepasein'];
		$Uraian=$_POST['Uraian'];
		$Alasan=$_POST['alasan'];
		$jumlahbayar=str_replace('.','',$_POST['Jumlah']);
		$this->db->trans_begin();
		
		$res = $this->db->query("select k.shift,t.kd_user,zu.user_names from kunjungan k
									inner join transaksi t on t.kd_pasien=k.kd_pasien and t.kd_unit=k.kd_unit 
										and t.tgl_transaksi=k.tgl_masuk and t.urut_masuk=k.urut_masuk
									inner join zusers zu on zu.kd_user=t.kd_user
								where kd_kasir='".$kdKasir."' 
									and no_transaksi='".$TrKodeTranskasi."' 
									and tgl_transaksi='".$_POST['Tgltransaksi']."'
								")->row();	
		$param_insert_history_detail_bayar = array(
												'kd_kasir'		=>$kdKasir,
												'no_transaksi'	=>$TrKodeTranskasi,
												'tgl_transaksi'	=>$_POST['Tgltransaksi'],
												'kd_pasien'		=>$Kodepasein,
												'nama'			=>$NamaPasien,
												'kd_unit'		=>$KodeUnit,
												'nama_unit'		=>$Namaunit,
												'kd_pay'		=>$Kodepay,
												'uraian'		=>$Uraian,
												'kd_user'		=>$res->kd_user,
												'kd_user_del'	=>$_kduser,
												'shift'			=>$res->shift,
												'shiftdel'		=>$this->db->query("select shift from bagian_shift where kd_bagian='1'")->row()->shift,
												'user_name'		=>$res->user_names,
												'jumlah'		=>$jumlahbayar,
												'tgl_batal'		=>date('Y-m-d'),
												'ket'			=>$Alasan,
											);
		$insert		= $this->db->insert("history_detail_bayar",$param_insert_history_detail_bayar);		
		$insertSQL	= $db->insert("history_detail_bayar",$param_insert_history_detail_bayar);		
			if( $_POST['Uraian']==='transfer' || $_POST['Uraian']==='TRANSFER')
				{
					$queryselect_tranfer=$this->db->query("select * from transfer_bayar where kd_kasir='$kdKasir'
							and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."' ")->result();
					if(count($queryselect_tranfer)===1)
					{
						for($x=0;  $x<count($queryselect_tranfer); $x++)
						{
							$kdkasirtujuan=$queryselect_tranfer[$x]->det_kd_kasir;
							$no_transaksitujuan=$queryselect_tranfer[$x]->det_no_transaksi;
							$uruttujuan=$queryselect_tranfer[$x]->det_urut;
							$tgl_transaksitujuan=$queryselect_tranfer[$x]->det_tgl_transaksi;
						}
						$totaldttrx=$this->db->query("select sum(qty * harga) as total from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
							and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
						$totaldtbyr=$this->db->query("select sum(jumlah) as total from detail_bayar where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
							and tgl_transaksi='$tgl_transaksitujuan'")->row()->total;
							
						if ($totaldttrx==$totaldtbyr)
						{
							echo "{success:false , pesan:'LUNAS'}";
						}else
						{
							$Querydelete_tujuan=$this->db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
								and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'");
							$Querydelete_tujuansql=$db->query(" delete from transfer_bayar where  kd_kasir='$kdKasir'
								and no_Transaksi='".$TrKodeTranskasi."'and urut=".$Urut." and tgl_transaksi = '".$TglBayar."'");
							if($Querydelete_tujuan && $Querydelete_tujuansql)
							{
								$Querydelete_tujuan=$this->db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
								and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
								$Querydelete_tujuansql=$db->query("delete from detail_transaksi where kd_kasir='$kdkasirtujuan' and no_transaksi='$no_transaksitujuan' 
								and urut=$uruttujuan and tgl_transaksi='$tgl_transaksitujuan'");
								if($Querydelete_tujuan)
								{
									$query=$this->db->query( "select hapusdetailbayar('$kdKasir', '".$TrKodeTranskasi."',
																".$Urut.",'".$TglBayar."')");
									$querysql=$db->query( "exec dbo.V5_hapus_detail_bayar '$kdKasir', '".$TrKodeTranskasi."',
																".$Urut.",'".$TglBayar."'");
									
									if($query && $querysql) {									
										$updatetrans = $this->db->query("update transaksi set lunas='f', ispay='f' where kd_kasir='$kdKasir'
																	and no_Transaksi='".$TrKodeTranskasi."'");
										$updatetranssql = $db->query("update transaksi set lunas=0, ispay=0 where kd_kasir='$kdKasir'
																	and no_Transaksi='".$TrKodeTranskasi."'");
										if($updatetrans && $updatetranssql){
											$this->db->trans_commit();
											$db->trans_commit();
											echo "{success:true}";
										} else{
											$this->db->trans_rollback();
											$db->trans_rollback();
											echo "{success:false}";
										}
									}
									else{
										$this->db->trans_rollback();
										$db->trans_rollback();
										echo "{success:false}";
									}
								} else {	
									$this->db->trans_rollback();
									$db->trans_rollback();
									echo "{success:false}";
								}
							} else {
								$this->db->trans_rollback();
								$db->trans_rollback();
								echo "{success:false}";
							}
						}
						
					}else {
						$this->db->trans_rollback();
						$db->trans_rollback();
						echo "{success:false}";
					}
				}else {	
					$query=$this->db->query( "select hapusdetailbayar('$kdKasir', '".$TrKodeTranskasi."', ".$Urut.",'".$TglBayar."')");
					$querysql=$db->query( "exec dbo.V5_hapus_detail_bayar '$kdKasir', '".$TrKodeTranskasi."', ".$Urut.",'".$TglBayar."'");
					if($query && $querysql) {									
						$updatetrans = $this->db->query("update transaksi set lunas='f', ispay='f' where kd_kasir='$kdKasir'
													and no_Transaksi='".$TrKodeTranskasi."'");
						$updatetranssql = $db->query("update transaksi set lunas=0, ispay=0 where kd_kasir='$kdKasir'
													and no_Transaksi='".$TrKodeTranskasi."'");
						if($updatetrans && $updatetranssql){
							$this->db->trans_commit();
							$db->trans_commit();
							echo "{success:true}";
						} else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo "{success:false}";
						}
					} else{
						$this->db->trans_rollback();
						$db->trans_rollback();
						echo "{success:false}";
					}
				}
				
	}

	public function detail_obatrwi()
	{
		$Tgl = date("Y-m-d");
		$urut=$this->db->query('SELECT id_mrresep from mr_resep order by id_mrresep desc limit 1')->row();
		$urut=substr($urut->id_mrresep,8,12);
		$sisa=4-count(((int)$urut+1));
		$real=date('Ymd');
		for($i=0; $i<$sisa ; $i++)
		{
		$real.="0";
		}
		$real.=((int)$urut+1);
		$urut=$real;
		$result=$this->db->query("SELECT COUNT(*) AS jumlah FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
								tgl_masuk='".$_POST['Tgl1']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
		if($result->jumlah>0)
		{
			$result=$this->db->query("SELECT id_mrresep FROM mr_resep WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
			tgl_masuk='".$_POST['Tgl1']."' AND urut_masuk='".$_POST['urut_masuk']."'")->row();
			$urut=$result->id_mrresep;
			$update=$this->db->query("update mr_resep set order_mng=false WHERE kd_pasien ='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['KdUnit']."' AND 
			tgl_masuk='".$_POST['Tgl1']."' AND urut_masuk='".$_POST['urut_masuk']."'");
		}else
		{
			$kd_dokter=$this->db->query("SELECT kd_dokter FROM dokter WHERE nama='".$this->session->userdata['user_id']['username']."'")->row();
			$kd='0';
			if(isset($kd_dokter->kd_dokter))
			{
				$kd=$kd_dokter->kd_dokter;
			}
			$mr_resep=array();
			$mr_resep['kd_pasien']=$_POST['kd_pasien'];
			$mr_resep['kd_unit']=$_POST['KdUnit'];
			$mr_resep['tgl_masuk']=$_POST['Tgl1'];
			$mr_resep['urut_masuk']=$_POST['urut_masuk'];
			$mr_resep['kd_dokter']=$kd;
			$mr_resep['id_mrresep']=$urut;
			$mr_resep['cat_racikan']='';
			$mr_resep['tgl_order']=$Tgl;
			$mr_resep['dilayani']=0;
			$this->db->insert('mr_resep',$mr_resep);
		}
		$result=$this->db->query("SELECT id_mrresep,urut, kd_prd FROM mr_resepdtl WHERE id_mrresep=".$urut)->result();
		for($i=0; $i<count($result); $i++)
		{
			$ada=false;
			for($j=0; $j<$_POST['jmlObat']; $j++)
			{
				if($result[$i]->urut==($j+1) && $result[$i]->kd_prd==$_POST['kd_prd'.$j])
				{
					$ada=true;
				}
			}
		
		}
		for($i=0; $i<$_POST['jmlObat']; $i++)
		{
			$status=0;
			if($_POST['urut'.$i]==0 ||$_POST['urut'.$i]=='' ||$_POST['urut'.$i]=='undefined')
			{
			$urut_order=($i+1);
			}else
			{
			$urut_order=$_POST['urut'.$i];
			}
			if($_POST['verified'.$i]=='Not Verified')
			{
				$status=1;
			}
			$result=$this->db->query("SELECT insertmr_resepdtl(".$urut.",".$urut_order.",'".$_POST['kd_prd'.$i]."',".$_POST['jumlah'.$i].",'".$_POST['cara_pakai'.$i]."',0,'".$_POST['kd_dokter'.$i]."',".$status.",
			".$_POST['racikan'.$i].") ");
		}
		if($result)
		{
		echo "{success:true}";
		}
		else
		{
		echo "{success:false}";
		}	

}
    public function savedetailpenyakit()
	{	
		$kdKasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$this->db->trans_begin();
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		 date_default_timezone_set('Asia/Jakarta');
		$Tgl =gmdate("Y-m-d", time()+60*60*7);
		$Shift =$this->GetShiftBagian();
		//$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];
		//urut_masuk
		if ($_POST['Tgl']=='')
		{
			$Tgl=$_POST['Tgl'];
		}
		else
		{
			$Tgl =$_POST['Tgl'];
		}
		$query_ambilkamar=$this->db->query("select * from nginap  where kd_pasien='".$_POST['kd_pasien']."' and kd_unit ='".$_POST['KdUnit']."' and urut_masuk=".$_POST['urut_masuk']."
		--and tgl_masuk='".$Tgl."'
		and akhir=true and tgl_keluar isnull")->result();
		foreach ($query_ambilkamar as $datainap)
				{ 
				$kd_unit_kamar=$datainap->kd_unit_kamar;
				$no_kamar=$datainap->no_kamar;
				$kd_spesial=$datainap->kd_spesial;
				}
			$urut = $this->db->query("select geturutdetailtransaksi('$kdKasir','".$TrKodeTranskasi."','".$Tgl."') ");
			$result = $urut->result();
				foreach ($result as $data)
				{ 
					if($_POST['Urut']==0 || $_POST['Urut']=='' )
					{
						$Urutan = $data->geturutdetailtransaksi;
					}
					else
					{
						if ($_POST['Tgl']=='')
						{
							$Tgl=$_POST['Tgl1'];
						}
						else
						{
							$Tgl =$_POST['Tgl'];
						}
						$Urutan =$_POST['Urut'];
					}
				}
				//echo $Tgl;
				//echo "qty nya : ".$Tgl;
				$query = $this->db->query("select insert_detail_transaksi
				('$kdKasir','".$TrKodeTranskasi."',".$Urutan.",
				'".$Tgl."', '".$this->session->userdata['user_id']['id']."','".$_POST['KD_TARIF']."',
				".$_POST['KD_PRODUK'].",'".$KdUnit."',
				'".$_POST['TGLBERLAKU']."',
				'false','true','',".$_POST['QTY'].",
				".$_POST['HARGA'].",".$Shift.",'false'
				)");	
			
		$query_detail_trkamar=$this->db->query("select * from detail_tr_kamar where kd_kasir='$kdKasir' and no_transaksi='".$TrKodeTranskasi."' and urut='".$Urutan."' and tgl_transaksi ='".$Tgl."'")->result();
		if (count($query_detail_trkamar)==0)
		{
				$query = $this->db->query("insert into detail_tr_kamar values
				('$kdKasir','".$TrKodeTranskasi."',".$Urutan.",
				'".$Tgl."','".$kd_unit_kamar."','".$no_kamar."',
				'".$kd_spesial."')");	
		}
		
		/* //--------Query Untuk Insert / Update Ke mr_tindakan ------------------------\\
		$query_cek_kunjungan=$this->db->query("select * from kunjungan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' and kd_dokter='".$_POST['kdDokter']."' ")->result();
		$query_mr_tindakan=$this->db->query("select max(urut) from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
		$c_urutTindakan=count($query_mr_tindakan);
		$urutTindakan=0;
		if ($c_urutTindakan<>0)
		{
			$max=$this->db->query("select max(urut) as jml from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->row()->jml;
			$urutTindakan=$max+1;
		}
		$cek_klas=$this->db->query("select * from produk pr inner join klas_produk kp on pr.kd_klas=kp.kd_klas where pr.kd_produk='".$_POST['KD_PRODUK']."'")->row()->kd_klas;
			if ($cek_klas<>'9' && $cek_klas<>'1')
			{
				foreach ($query_cek_kunjungan as $datatindakan)
				{
					$q_cek_mrtind=$this->db->query("select * from mr_tindakan where kd_produk='".$_POST['KD_PRODUK']."' and kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$KdUnit."' ")->result();
					$jmlProd=count($q_cek_mrtind);
					if ($jmlProd==0)
					{
						$q_mr_tind = $this->db->query("insert into mr_tindakan values ('".$_POST['KD_PRODUK']."','".$_POST['kd_pasien']."','$KdUnit',
						'$datatindakan->tgl_masuk',$datatindakan->urut_masuk,$urutTindakan,'".$Tgl."','".$TrKodeTranskasi."','$kdKasir')");	
						$urutTindakan+=1;
					}
					
				}
			} */	
			
			if($query)
				{
					$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
					$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
					$getcounkomponent=$this->db->query("Select count(kd_component) as compo, sum(tarif) as total from detail_component where kd_kasir='$kdKasir' and 
					no_transaksi='".$TrKodeTranskasi."' and urut=$Urutan and tgl_transaksi='".$Tgl."' and kd_component in('".$kdjasadok."','".$kdjasaanas."') ")->result();
															foreach($getcounkomponent as $getcounkomponent_1)
															{
															$countcompo = $getcounkomponent_1->compo;
															$sumtarif = $getcounkomponent_1->total;
															}
															if($countcompo>0)
																	{
																	echo "{success:true, compo:true, tarif:".$sumtarif."}";
																	$this->db->trans_commit();
																	}
															else{
																echo"{success:true}";
																$this->db->trans_commit();
																}
															
				}
				else
				{
					echo "{success:false}";
				    $this->db->trans_rollback();
				}
				
			
	} 
	
private function GetShiftBagian()
{
			
		/*	$ci =& get_instance();
			$db = $ci->load->database('otherdb',TRUE);
			$sqlbagianshift = $db->query("SELECT  CONVERT(VARCHAR(3), shift)AS SHIFTBAG FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row();
			$sqlbagianshift2 = $sqlbagianshift->SHIFTBAG;*/
		
			//$sqlbagianshift2=1;
			$sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->shift;
			$lastdate = $this->db->query("SELECT   to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->lastdate;
				$datnow= date('Y-m-d');
			if($lastdate<>$datnow && $sqlbagianshift==='3')
			{
			$sqlbagianshift2 = '4';
			}else{
			$sqlbagianshift2 = $sqlbagianshift;
			}
			
        return $sqlbagianshift2;
}	
public function savetrdokter()
{		
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$KdProd = $_POST['KD_PRODUK'];
		if ($_POST['TGLTRANSAKSI']===''||$_POST['TGLTRANSAKSI']==='null')
		{
		$Tgl =date("Y-m-d");
		}
		else
		{
		$Tgl =$_POST['TGLTRANSAKSI'];
		}
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{
			$kdKomp	= $_POST['KD_KOMPONEN-'.$i];
			$prc	= $_POST['PRC-'.$i];
			$b = explode("@@##$$@@",$a[$i]);
			for($k=0;$k<=count($b)-1;$k++)
			{
			}
			$c = explode("-",$b[1]);
			$d = explode("-",$b[4]);
			$result=$this->db->query("select * from visite_dokter WHERE no_transaksi='".$TrKodeTranskasi."' AND urut='".$_POST['Urut']."' 
			AND kd_kasir='$KASIRRWI' AND tgl_transaksi='$Tgl'" )->result();
			$hasil=array();
			if(count($result)>0)
			{
				$jumlah_dokter=count($result)+1;
				$query = $this->db->query("insert into visite_dokter( kd_kasir,no_transaksi , urut, tgl_transaksi , line,kd_dokter , kd_unit,tag_int, tag_char,kd_job , prc, jp )values
						('$KASIRRWI','".$TrKodeTranskasi."',".$_POST['Urut'].",'".$Tgl."','".$jumlah_dokter."','$c[0]','".$KdUnit."','".$KdProd."','".$kdKomp."','$d[0]','".$prc."',$b[3])");
			}
			else
			{
				$query = $this->db->query("insert into visite_dokter( kd_kasir, no_transaksi , urut,tgl_transaksi ,line,kd_dokter , kd_unit,tag_int,tag_char, kd_job , prc, jp )
						  values('$KASIRRWI','".$TrKodeTranskasi."',".$_POST['Urut'].",'".$Tgl."','1','$c[0]','".$KdUnit."','".$KdProd."','".$kdKomp."','$d[0]','".$prc."',$b[3])");
			}
			/* $query = $this->db->query("update detail_component set tarif=$b[3] where kd_kasir='$KASIRRWI' and no_transaksi='".$TrKodeTranskasi."' 
			and urut='".$_POST['Urut']."' and tgl_transaksi='".$Tgl."' and kd_component='".$kdKomp."' ");
		 */
		}if($query)
		{
			echo "{success:true}";
		}
		else
		{
			echo "{success:false}";
		}												
	} 
	
	public function cekKeVisiteDokterDiawal()
	{
		if ($_POST['no_transaksi']=='')
		{
			$jsonResult=array();
			$jsonResult['processResult']='NOT SUCCESS';
			$jsonResult['listData']='Kosong';
			$jsonResult['jumlah']=0;
			echo json_encode($jsonResult);
		}
		else
		{
			$result=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->result();
			$result2=$this->db->query("select * from produk where kd_produk='".$_POST['kd_produk']."'")->row()->kd_klas;
			$kd_produk=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->row()->tag_int;
			$urut=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->row()->urut;
			$jml=count($result);
			if ($result)
			{
				$jsonResult=array();
				$jsonResult['processResult']='SUCCESS';
				$jsonResult['listData']=$result;
				$jsonResult['jumlah']=$jml;
				$jsonResult['kd_produk']=$kd_produk;
				$jsonResult['urut']=$urut;
			}
			else{
				$jsonResult=array();
				$jsonResult['processResult']='NOT SUCCESS';
				$jsonResult['listData']='kosong';
				$jsonResult['jumlah']=$jml;
			}
			
			$jsonResult['kd_klas']=$result2;
			if ($kd_produk)
			{
				$jsonResult['kd_produk']=$kd_produk;
			}
			else{
				$jsonResult['kd_produk']='kosong';
			}
			/* if ($urut)
			{
				$jsonResult['urut']=$urut;
			}
			else{
				$jsonResult['urut']='kosong';
			} */
			echo json_encode($jsonResult);
		}
		
	}
	public function getVisiteDokterDiawal()
	{
		if ($_POST['no_transaksi']=='')
		{
			$jsonResult=array();
			$jsonResult['processResult']='NOT SUCCESS';
			$jsonResult['listData']='Kosong';
			$jsonResult['jumlah']=0;
			echo json_encode($jsonResult);
		}
		else
		{
		$result=$this->db->query("select d.nama as kd_nama, vd.urut, vd. tgl_transaksi from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' --and vd.tag_int='".$_POST['kd_produk']."' 
									and vd.urut='".$_POST['urut']."'")->result();
		/* $kd_klas=substr($_POST['kd_klas'], 0,2);
			if ($kd_klas=='61')
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and vd.tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=1")->result();
			}
			else
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter 
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and vd.tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=0")->result();
			} */
		$jml=count($result);
		$nm_dok='';
		if ($jml==1)
		{
			foreach ($result as $line)
			{
				$nm_dok.=$line->kd_nama;
			}
		}
		else
		{
			foreach ($result as $line)
			{
				$nm_dok.=$line->kd_nama.';';
			}
		}
		
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['jumlah']=$jml;
		$jsonResult['nama_dok']=$nm_dok;
    	echo json_encode($jsonResult);
		}
	}
	public function cekKeVisiteDokter()
	{
		if ($_POST['no_transaksi']=='')
		{
			$jsonResult=array();
			$jsonResult['processResult']='NOT SUCCESS';
			$jsonResult['listData']='Kosong';
			$jsonResult['jumlah']=0;
			echo json_encode($jsonResult);
		}
		else
		{
			$result=$this->db->query("select * from visite_dokter where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' and tag_int='".$_POST['kd_produk']."' and urut='".$_POST['urut']."'")->result();
			$result2=$this->db->query("select * from produk where kd_produk='".$_POST['kd_produk']."'")->row()->kd_klas;
			$jml=count($result);
			$jsonResult=array();
			$jsonResult['processResult']='SUCCESS';
			$jsonResult['listData']=$result;
			$jsonResult['jumlah']=$jml;
			$jsonResult['kd_klas']=$result2;
			echo json_encode($jsonResult);
		}
		
	}
	public function getVisiteDokter()
	{
		$kd_klas=substr($_POST['kd_klas'], 0,2);
			if ($kd_klas=='61')
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."' 
									and tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=1")->result();
			}
			else
			{
				$result=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
									inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter 
									inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
									where no_transaksi='".$_POST['no_transaksi']."' and tgl_transaksi='".$_POST['tgl_transaksi']."'
									and tag_int='".$_POST['kd_produk']."' and vd.urut='".$_POST['urut']."' and dii.groups=0")->result();
			}
		$iniTarif=0;
		foreach ($result as $a)
		{
			$iniTarif+=$a->jpp;
		}
		$jml=count($result);
		$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['jumlah']=$jml;
		$jsonResult['iniTarif']=$iniTarif;
    	echo json_encode($jsonResult);
	}
	public function hapustrdokter()
	{	
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		$KdUnit = $_POST['KdUnit'];
		$Tgl =date("Y-m-d");
		$Shift =$_POST['Shift'];
		$list =$_POST['List'];
		$jmlfield =$_POST['JmlField']; 
		$jmlList = $_POST['JmlList'];

		$a = explode("##[[]]##",$list);
		for($i=0;$i<=count($a)-1;$i++)
		{
			$b = explode("@@##$$@@",$a[$i]);
			for($k=0;$k<=count($b)-1;$k++)
			{
			}
			$c = explode("-",$b[1]);
			$d = explode("-",$b[4]);
			$query = $this->db->query("delete from detail_trdokter where kd_kasir='$KASIRRWI'and 
					  no_transaksi ='".$TrKodeTranskasi."'  and
					  urut=".$_POST['Urut']."
					  and tgl_transaksi='".$Tgl."'
					  and kd_dokter= '$c[0]' 
					");
	
		}
		if($query)
		{
		echo "{success:true}";
		}
		else
		{
		echo "{success:false}";
		}												
	} 
	public function ubah_co_status_transksi()
	{
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TrKodeTranskasi = $_POST['TrKodeTranskasi'];
		/* $KDcus = $_POST['KDcus'];
		$TrKDUnit = $_POST['TrKDUnit'];
		$TrURUTMasuk = $_POST['TrURUTMasuk'];
		$TrNOkamar = $_POST['TrNOkamar'];
		$TrKDkelas = $_POST['TrKDkelas'];
		$TrTgl = $_POST['TrTgl'];
		$TrKdPasien = $_POST['TrKdPasien'];
		$TrUnit_kamar = $_POST['TrUnit_kamar'];
		$Trtapungalasan = $_POST['Trtapungalasan'];
		$kdunit = $_POST['KDUnit']; */
		$kdKasir =$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$query = $this->db->query(" update transaksi set  ispay='true', co_status='true' ,  tgl_co='".date("Y-m-d")."' where no_transaksi='".$TrKodeTranskasi ."' and kd_kasir='".$kdKasir."'");	
		if($query)
		{
		echo "{success:true}";
		/* $querykamar=  $this->db->query("Update Nginap Set Tgl_Keluar = '".date("Y-m-d H:m:s")."' , Akhir='true' Where Kd_Pasien = '" .$TrKdPasien."' And 
		Kd_Unit = '".$TrKDUnit."' And Tgl_Masuk = '".$TrTgl."' And Urut_Masuk = " .$TrURUTMasuk. " 
		And Kd_Unit_Kamar = '" .$TrUnit_kamar. "' And No_Kamar = '".$TrNOkamar ."' And Tgl_Keluar IS NULL");
			if($querykamar)
			{
				$querykunjungan= $this->db->query("Update Kunjungan Set Tgl_Keluar = '" .date("Y-m-d"). "', Keadaan_Pasien = " .$Trtapungalasan.", Jam_Keluar = '" .date("Y-m-d H:m:s"). "' 
				Where Kd_Pasien = '".$TrKdPasien. "' And Kd_Unit = '" .$TrKDUnit."' And Urut_Masuk = " .$TrURUTMasuk. " And Tgl_Keluar IS NULL ");
				if($querykunjungan)
				{
					$baru = $this->db->query("update kamar set digunakan = digunakan -1 Where kd_unit='$TrUnit_kamar' AND no_kamar='$TrNOkamar'");
					$baru2 = $this->db->query("update kamar_induk set digunakan = digunakan -1 Where no_kamar='$TrNOkamar'");
					$querypasien=$this->db->query("Delete From Pasien_Inap Where Kd_Kasir = '$KASIRRWI' And No_Transaksi = '".$TrKodeTranskasi."'");
					if($querypasien)
					{
					echo "{success:true}";
					}
					else
					{
					echo "{success:false}";
					}
				}
				else
				{
				echo "{success:false}";
				}
			}
			else 
			{
			echo "{success:false}";
			} */		
		}
		else
		{
		echo "{success:false}";
		}
	 
	}
        
	public function GetRecordPenyakit($kd,$nama)
	{
		$query = $this->db->query("SELECT kd_penyakit,penyakit from penyakit where kd_penyakit ilike '%".$kd."%' OR penyakit ilike '%".$nama."%' ");
		$count = $query->num_rows();
		return $count;
	}
        
    
	public function getTindakan(){
		$result=$this->db->query("select id_status,catatan from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' 
		AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		$hasil=array();
		if(count($result)>0){
			$hasil['id_status']=$result[0]->id_status;
			$hasil['catatan']=$result[0]->catatan;
		}else{
			$hasil['id_status']=-1;
			$hasil['catatan']='';
		}
		
		echo "{success:true,echo:".json_encode($hasil)."}";
	}
        
    public function saveTindakan()
	{
		$this->db->trans_begin();
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$urut=$this->db->query('select id_rwirujukan AS code FROM mr_rwi_rujukan order by id_rwirujukan desc limit 1')->row();
		$id='';
		if(isset($urut->code)){
			$urut=substr($urut->code,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$id=$real;
		}else{
			$id=date('Ymd').'0001';
		}
		
		$count=$this->db->query("select * from mr_rwi_rujukan WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND
		tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($count)>0){
			$id=$count[0]->id_rwirujukan;
			$this->db->where('id_rwirujukan',$id);
			$mr_rwi_rujukan=array();
			$mr_rwi_rujukan['id_status']=$_POST['id_status'];
			$mr_rwi_rujukan['catatan']=$_POST['catatan'];
			if(isset($_POST['kd_unit_tujuan'])){
				$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
			}
			$this->db->update('mr_rwi_rujukan',$mr_rwi_rujukan);
		}else
		{
			$mr_rwi_rujukan=array();
			$mr_rwi_rujukan['kd_pasien']=$_POST['kd_pasien'];
			$mr_rwi_rujukan['kd_unit']=$_POST['kd_unit'];
			$mr_rwi_rujukan['tgl_masuk']=$_POST['tgl_masuk'];
			$mr_rwi_rujukan['urut_masuk']=$_POST['urut_masuk'];
			$mr_rwi_rujukan['id_rwirujukan']=$id;
			$mr_rwi_rujukan['id_status']=$_POST['id_status'];
			$mr_rwi_rujukan['catatan']=$_POST['catatan'];
			if(isset($_POST['kd_unit_tujuan'])){
				$mr_rwi_rujukan['kd_unit_tujuan']=$_POST['kd_unit_tujuan'];
			}
			$this->db->insert('mr_rwi_rujukan',$mr_rwi_rujukan);
		}

		$data = array();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        $today = date('Y-m-d H:i:s');
        $kd_pasien = $this->input->post('kd_pasien');
        $kd_unit = $this->input->post('kd_unit');
        $tgl_masuk = $this->input->post('tgl_masuk');
        $urut_masuk = $this->input->post('urut_masuk');
        $tglkeluar = $this->input->post('tglkeluar');
        $carakeluar = $this->input->post('carakeluar');
		if($carakeluar==='' )
		{
		$carakeluar=0;
		}
        $keadaanakhir = $this->input->post('keadaanakhir');
        $sebabmati = $this->input->post('sebabmati');
		$query_cari_kunjungan=$this->db->query("
			select * from kunjungan 
			where kd_pasien = '$kd_pasien' and
				  kd_unit = '$kd_unit' and 
                  tgl_masuk = '$tgl_masuk' and
				  urut_masuk = $urut_masuk and
				  tgl_keluar  isnull 
				  ")->result();
		if(count($query_cari_kunjungan)==0)
		{
		echo "{success:false,cari: true}";
		}
		else
		{		
			$datacari = array(
				'kd_pasien' => $kd_pasien,
				'kd_unit' => $kd_unit,
				'tgl_masuk' => $tgl_masuk,
				'urut_masuk' => $urut_masuk,
			);
			$TrNOkamar=$_POST['no_kamar'];
			$TrUnit_kamar=$_POST['kd_unit_kamar'];
			$TrKodeTranskasi=$_POST['no_transaksi'];
			$data = array(
				'cara_keluar' => $carakeluar,
				'keadaan_pasien' => $keadaanakhir,
				'sebabmati' => $sebabmati,
				'tgl_keluar' => $tglkeluar,
				'jam_keluar' => $today
			);
			$this->db->where($datacari);
			$this->db->update('kunjungan', $data);
			$querykamar=  $this->db->query("Update Nginap Set Tgl_Keluar = '".date("Y-m-d H:m:s")."' , Akhir='true' Where Kd_Pasien = '" .$kd_pasien."' And 
			Kd_Unit = '".$kd_unit."' And Tgl_Masuk = '".$tgl_masuk."' And Urut_Masuk = " .$urut_masuk. " 
			And Kd_Unit_Kamar = '" .$TrUnit_kamar. "' And No_Kamar = '".$TrNOkamar ."' And Tgl_Keluar IS NULL");
			if($querykamar)
			{
				$baru = $this->db->query("update kamar set digunakan = digunakan -1 Where kd_unit='$TrUnit_kamar' AND no_kamar='$TrNOkamar'");
				$baru2 = $this->db->query("update kamar_induk set digunakan = digunakan -1 Where no_kamar='$TrNOkamar'");
				$querypasien=$this->db->query("Delete From Pasien_Inap Where Kd_Kasir = '$KASIRRWI' And No_Transaksi = '".$TrKodeTranskasi."'");
				if($querypasien)
				{
					$regpulang=$this->regonline_pasienpulang($_POST['kd_pasien']);
					if($regpulang){
						$this->db->trans_commit();
						echo "{success:true}";
					} else{
						$this->db->trans_rollback();
						echo "{success:false}";
					}
					
				}
				else
				{
					$this->db->trans_rollback();
					echo "{success:false}";
				} 
			}
			else 
			{
				$this->db->trans_rollback();
				echo "{success:false}";
			} 
		}
		//echo "{success:true}";
	}
        
        public function saveDiagnosaPoliklinik(){
		$this->db->trans_begin();
		$kdPasien = $_POST['KdPasien'];
		$kdUnit = $_POST['KdUnit'];
		$Tgl =$_POST['Tgl'];
		$jmlList = $_POST['JmlList'];
		$urut_masuk = $_POST['UrutMasuk'];
		$perawatan = 99;
		$tindakan = 99;
		$inser_batch=array();
		_QMS_Query("DELETE FROM mr_penyakit WHERE kd_pasien='".$kdPasien."' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk='".$urut_masuk."'");
		for($i=0;$i<$jmlList;$i++){
			if($_POST['STAT_DIAG'.$i] == 'Diagnosa Awal'){
				$diagnosa = 0;
			}else if($_POST['STAT_DIAG'.$i]  == 'Diagnosa Utama'){
				$diagnosa = 1;
			}else if($_POST['STAT_DIAG'.$i]  == 'Komplikasi'){
				$diagnosa = 2;
			}else if($_POST['STAT_DIAG'.$i] == 'Diagnosa Sekunder'){
				$diagnosa = 3;
			}
			if($_POST['KASUS'.$i] == 'Baru'){
				$kasus = 'TRUE';
				$zkasus = 1;
			}else if($_POST['KASUS'.$i] == 'Lama'){
				$kasus = 'FALSE';
				$zkasus = 0;
			}
			$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
			$result = $urut->result();
			foreach ($result as $data){
				$Urutan = $data->geturutmrpenyakit;
			}
			$query = $this->db->query("select insertdatapenyakit('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
			if($_POST['NOTE'.$i]==1){
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$query = $this->db->query("select insertneoplasma('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.",".$Urutan.",'".$_POST['DETAIL'.$i]."')");
			}else if($_POST['NOTE'.$i]==2){
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$query = $this->db->query("select insertkecelakaan('".$_POST['KD_PENYAKIT'.$i]."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.",".$Urutan.",'".$_POST['DETAIL'.$i]."')");
			}else if($_POST['NOTE'.$i]==0){
				$this->db->query("DELETE FROM neoplasma WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
				$this->db->query("DELETE FROM kecelakaan WHERE kd_penyakit='".$_POST['KD_PENYAKIT'.$i]."' AND kd_pasien='".$kdPasien."' AND
				kd_unit='".$kdUnit."' AND tgl_masuk='".$Tgl."' AND urut_masuk=".$urut_masuk);
			}
		}
		if($this->db->trans_status()==true){
			$this->db->trans_commit();
			echo "{success:true,echo:".$this->db->trans_status()."}";
		}else{
			$this->db->trans_rollback();
			echo "{success:false}";
		}	
	}
        
        public function deletelaboratorium(){
		$result=$this->db->query("select * from mr_labkonsul WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND tgl_masuk='".$_POST['tgl_masuk']."' AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($result)>0){
			$id=$result[0]->id_labkonsul;
			$this->db->query("DELETE FROM mr_labkonsuldtl WHERE id_labkonsul='".$id."' AND kd_produk='".$_POST['kd_produk']."'");
		}
		echo "{success:true}";
	}
        
        public function savelaboratorium(){
		//create id
		$urut=$this->db->query('select id_labkonsul AS code FROM mr_labkonsul order by id_labkonsul desc limit 1')->row();
		$id='';
		if(isset($urut->code)){
			$urut=substr($urut->code,8,12);
			$sisa=4-count(((int)$urut+1));
			$real=date('Ymd');
			for($i=0; $i<$sisa ; $i++){
				$real.="0";
			}
			$real.=((int)$urut+1);
			$id=$real;
		}else{
			$id=date('Ymd').'0001';
		}
		$count=$this->db->query("select * from mr_labkonsul WHERE kd_pasien='".$_POST['kd_pasien']."' AND kd_unit='".$_POST['kd_unit']."' AND tgl_masuk='".$_POST['tgl_masuk']."' 
		AND urut_masuk=".$_POST['urut_masuk'])->result();
		if(count($count)>0){
			$id=$count[0]->id_labkonsul;
		}else{
			$mr_labkonsul=array();
			$mr_labkonsul['kd_pasien']=$_POST['kd_pasien'];
			$mr_labkonsul['kd_unit']=$_POST['kd_unit'];
			$mr_labkonsul['tgl_masuk']=$_POST['tgl_masuk'];
			$mr_labkonsul['urut_masuk']=$_POST['urut_masuk'];
			$mr_labkonsul['id_labkonsul']=$id;
			$this->db->insert('mr_labkonsul',$mr_labkonsul);
		}
		$count=$this->db->query("delete from mr_labkonsuldtl WHERE id_labkonsul='".$id."' ");
		for($i=0; $i<$_POST['jum'] ; $i++){
				$mr_labkonsuldtl=array();
				$mr_labkonsuldtl['id_labkonsul']=$id;
				$mr_labkonsuldtl['kd_lab']=$_POST['kd_lab'.$i];
				$mr_labkonsuldtl['kd_produk']=$_POST['kd_produk'.$i];
				$r=$this->db->query("select * from dokter where nama='".$this->session->userdata['user_id']['username']."'")->result();
				if(count($r)>0){
					$code=$r[0]->kd_dokter;
				}else{
					$code='0';
				}
				$mr_labkonsuldtl['kd_dokter']=$code;
				$this->db->insert('mr_labkonsuldtl',$mr_labkonsuldtl);
		}
		echo "{success:true}";
	}
        
 public function saveTransfer()
	{
        $KDunittujuan=$_POST['KDunittujuan'];
		$KDkasirIGD=$_POST['KDkasirIGD'];
		$TrKodeTranskasi=$_POST['TrKodeTranskasi'];
		$KdUnit=$_POST['KdUnit'];
		$Kdpay=$_POST['Kdpay'];
		$total = str_replace('.','',$_POST['Jumlahtotal']); 
		$Shift1=$_POST['Shift'];
		$TglTranasksitujuan=$_POST['TglTranasksitujuan'];
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$TRKdTransTujuan=$_POST['TRKdTransTujuan'];
		$_kduser = $this->session->userdata['user_id']['id'];
		$tgltransfer=date("Y-m-d");
		$KDalasan =$_POST['KDalasan'];
		$det_query = $this->db->query("select urut+1 as urutan from detail_bayar where no_transaksi = '$TrKodeTranskasi' order by urutan desc limit 1")->result();
		foreach($det_query as $det)
		{
			$urut_detailbayar = $det->urutan;
		}
			if($urut_detailbayar=="")
			{
			$urut_detailbayar=1;
			}
			$pay_query = $this->db->query(" insert into detail_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_user,kd_unit,kd_pay,jumlah,folio,shift,status_bayar) 
			values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer','$_kduser','$KdUnit','$Kdpay',$total,'',$Shift1,'true')");	
						
			if($pay_query)
			{
				$detailTrbayar = $this->db->query("	insert into detail_tr_bayar (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,jumlah) 
				SELECT kd_kasir,no_transaksi,urut,tgl_transaksi,'$Kdpay',$urut_detailbayar,'$tgltransfer',harga *qty AS total FROM detail_transaksi
				WHERE KD_KASIR='$KDkasirIGD' AND NO_TRANSAKSI='$TrKodeTranskasi'");
				if($detailTrbayar)
				{	
					$statuspembayaran = $this->db->query("Select updatestatustransaksi('$KDkasirIGD','$TrKodeTranskasi', $urut_detailbayar, '$tgltransfer')");	
					if($statuspembayaran)
					{
					$detailtrcomponet = $this->db->query("insert into detail_tr_bayar_component
					(kd_kasir,no_transaksi,urut,tgl_transaksi,kd_pay,urut_bayar,tgl_bayar,kd_component,jumlah)
					Select '$KDkasirIGD','$TrKodeTranskasi',dt.urut,dt.Tgl_Transaksi,'$Kdpay',$urut_detailbayar,
					'$tgltransfer',dc.Kd_Component,((dt.Harga *dt.qty)/ dt.Harga) * dc.Tarif  as bayar
					FROM Detail_Component dc
					INNER JOIN Produk_Component pc ON dc.Kd_Component = pc.Kd_Component
					INNER JOIN Detail_Transaksi dt ON dc.kd_kasir = dt.kd_kasir and dc.no_transaksi = dt.no_transaksi 
					and dc.urut = dt.urut and dc.tgl_transaksi = dt.tgl_transaksi
					WHERE dc.Kd_Kasir = '$KDkasirIGD'
					AND dc.No_Transaksi ='$TrKodeTranskasi'
					ORDER BY dc.Kd_Component");	
						if($detailtrcomponet)
						{	
						
							$urutquery = $this->db->query("select urut+1 as urutan from detail_transaksi where no_transaksi = '$TRKdTransTujuan' order by urutan desc limit 1")->result();
							foreach($urutquery as $det)
							{
							$uruttujuan = $det->urutan;
							}
							if($uruttujuan=="")
							{
							$uruttujuan=1;
							}
							$getkdproduk = $this->db->query("SELECT pc.kd_Produk as kdproduk,pc.kd_unit as unitproduk
							FROM Produk_Charge pc 
							INNER JOIN produk p ON pc.kd_Produk = p.kd_Produk 
							WHERE left(kd_unit, 2)='2'")->result();
							foreach($getkdproduk as $det)
							{
							$kdproduktranfer = $det->kdproduk;
							$kdUnittranfer = $det->unitproduk;
							}
							$detailtransaksitujuan = $this->db->query("
							INSERT INTO detail_transaksi
							(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
							VALUES('$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer',
							'$_kduser', 'TU','$kdproduktranfer','$kdUnittranfer','2014-03-01','true','true','',1,$total,$Shift1,'false','$TrKodeTranskasi')
							");	
							if($detailtransaksitujuan)	
							{
								$detailcomponentujuan = $this->db->query
								("INSERT INTO Detail_Component (Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								select '$KASIRRWI','$TRKdTransTujuan',$uruttujuan,'$tgltransfer',kd_component,jumlah,0
								from detail_tr_bayar_component where no_transaksi='$TrKodeTranskasi' and Kd_Kasir = '$KDkasirIGD'");	
								   if($detailcomponentujuan)
								   { 
									$tranferbyr = $this->db->query("INSERT INTO transfer_bayar (kd_kasir, no_transaksi, Urut,Tgl_Transaksi,
									det_kd_kasir,det_no_transaksi, det_urut,det_tgl_transaksi,alasan) values ('$KDkasirIGD','$TrKodeTranskasi',$urut_detailbayar,'$tgltransfer',
									'$KASIRRWI', '$TRKdTransTujuan', $uruttujuan,'$tgltransfer','$KDalasan')");																  
									if($tranferbyr)
									{
									 echo '{success:true}';
									}
									else
									{ 
									 echo '{success:false}';	
									}
								   }
								   else
								   {
								    echo '{success:false}';	
								   }
							}
							else
							{
							 echo '{success:false}';	
							}
						}
						else
						{
						 echo '{success:false}';	
						}
					}								
					else
					{
					 echo '{success:false}';	
					}
				}
				else
				{
				echo '{success:false}';	
				}
			}
			else
			{
			echo '{success:false}';	
			}
			
	
		
	}
        
        public function getproduk()
        {
		 $kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		 $row=$this->db->query("select kd_tarif from tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row();
		 
    	$result=$this->db->query(
		"SELECT row_number() OVER () as rnum,rn.*, 1 as qty 
         from(
         SELECT produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, produk.manual,
		 produk.kp_produk, produk.kd_kat, produk.kd_klas, klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, 
		 max (tarif.tgl_berlaku) as tglberlaku,tarif.tarif as tarifx, tarif.tgl_berakhir,tr.jumlah,
         row_number() over(partition by produk.kd_produk order by tarif.tgl_berlaku desc) as rn 
         from produk inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
         inner join tarif on produk.kd_produk = tarif.kd_produk 
         inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
         inner join unit on tarif.kd_unit = unit.kd_unit 
         left join (select case when count(kd_component) > 0 then 'Ada' else 'Tidak Ada' end as jumlah ,kd_tarif,kd_produk,kd_unit,tgl_berlaku
         from tarif_component where kd_component = '".$kdjasadok."' or kd_component = '". $kdjasaanas."' group by kd_tarif,kd_produk,kd_unit,tgl_berlaku) as 
         tr ON tr.kd_unit=tarif.kd_unit AND tr.kd_produk=tarif.kd_produk AND tr.tgl_berlaku = tarif.tgl_berlaku AND 
         tr.kd_tarif=tarif.kd_tarif 
         Where tarif.kd_unit ='".$_POST['kd_unit']."' and lower(tarif.kd_tarif)=LOWER('".$row->kd_tarif."') 
		 and upper(produk.deskripsi) like upper('%".$_POST['text']."%') or CAST(produk_unit.kd_produk AS TEXT) LIKE '%".$_POST['text']."%'
         and tarif.tgl_berlaku <= '".date('Y-m-d')."'
         group by produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit,
         unit.nama_unit, produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas,tr.jumlah,
         klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berakhir ,tarif.tarif,produk.kd_produk,tarif.tgl_berlaku   
         order by produk.deskripsi asc ) 
         as rn where rn = 1 order by rn.deskripsi asc limit 10"
		 )->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }

    public function getProdukdeskripsi()
        {
		 $kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row()->kd_tarif;
		 		// $kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '".$_POST['text']."'");
		 		$tgl_now 	= date("Y-m-d");
		 		$q_unit 	= "";
		 		if (isset($_POST['kd_unit'])) {
		 			$q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
		 		}
		 		$q_customer = "";
		 		if (isset($_POST['kd_unit'])) {
		 			$q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
		 		}
		 		$q_text 	= "";
		 		if (isset($_POST['kd_unit'])) {
		 			$q_text = "  lower(deskripsi) like lower('%".$_POST['text']."%')";
		 		}
		 		$row = $this->db->query("select kd_tarif from tarif_cust ".$q_customer)->row();
		 		
		/*$sql = "SELECT 
			row_number() OVER () as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_unit = '".$_POST['kd_unit']."' 
					AND (c.kd_customer = '".$_POST['kd_customer']."' OR t.kd_tarif='".$kd_tarif."') 
					AND ".$q_kp_produk."
					AND t.tgl_berlaku < '".$tgl_now."'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.tgl_berlaku DESC";*/
		 		$sql = "SELECT 
		 			row_number() OVER () as rnum,
		 			rn.* 
		 			FROM (
		 			SELECT 
		 				1 as qty,
		 				u.kd_unit,
		 				u.kd_bagian, 
		 				u.kd_kelas, 
		 				u.nama_unit,
		 				p.kd_produk,
		 				p.kp_produk,
		 				p.deskripsi,
		 				t.kd_tarif,
		 				t.tgl_berlaku,
		 				t.tgl_berakhir,
		 				t.tarif,
		 				c.kd_customer,
		 				c.customer,
		 				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
		 				
		 			FROM 
		 				unit u 
		 				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
		 				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
		 				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
		 				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
		 				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
		 				WHERE 
		 					u.kd_unit = '".$_POST['kd_unit']."' 
		 					AND (c.kd_customer = '".$_POST['kd_customer']."' OR t.kd_tarif='".$kd_tarif."') 
		 					AND ".$q_text."
		 					AND t.tgl_berlaku < '".$tgl_now."'
		 			) as rn 
		 		WHERE rn = 1
		 		ORDER BY rn.tgl_berlaku DESC LIMIT 10";
		 		 
		 		$result     = $this->db->query($sql)->result();
		 		$jsonResult=array();
		 		$jsonResult['processResult']='SUCCESS';
		 		$jsonResult['listData']=$result;
		 		echo json_encode($jsonResult);
    }
	
	public function gettrdokter()
	{
    	$result=$this->db->query("select kd_dokter,nama from dokter where LOWER(kd_dokter) like LOWER('%".$_POST['text']."%') or 
		LOWER(nama) like LOWER('%".$_POST['text']."%')limit 20")->result();
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
    	echo json_encode($jsonResult);
    }
	
	public function getdokter_inap_int($a)
    {
		$kd_klas=substr($a, 0,2);
			if ($kd_klas=='61')
			{
				$result=$this->db->query("select * from dokter_inap_int where LOWER(label) like LOWER('%".$_POST['text']."%') and groups=1 limit 20")->result();
			}
			else
			{
				$result=$this->db->query("select * from dokter_inap_int where LOWER(label) like LOWER('%".$_POST['text']."%') and groups=0 limit 20")->result();
			}
    	$jsonResult=array();
    	$jsonResult['processResult']='SUCCESS';
    	$jsonResult['listData']=$result;
		$jsonResult['coba']=substr($a, 0,2);
    	echo json_encode($jsonResult);
    }
	
	public function cari_trdokter ()
	{	
		
		$KASIRRWI=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		$getcounkomponent=$this->db->query("Select count(kd_component) as compo, sum(tarif) as total from detail_component where kd_kasir='$KASIRRWI' and 
		no_transaksi='".$_POST['no_transaksi']."' and urut=".$_POST['urut']." and tgl_transaksi='".$_POST['tgl_transaksi']."' and kd_component in('".$kdjasadok."','". $kdjasaanas."') ")->result();
		foreach($getcounkomponent as $getcounkomponent_1)
		{
		$countcompo = $getcounkomponent_1->compo;
		$sumtarif = $getcounkomponent_1->total;
		}
		if($countcompo>0)
		{
		 echo "{success:true, compo:true, tarif:".$sumtarif."}";
		}
		else
		{
		 echo"{success:false}";
		}
		
	}
	public function batal_transaksi()
	{
		$this->db->trans_begin();
		try
		{
			$kd_kasir  =$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwi'")->row()->setting;
			$kd_pasien = $this->input->post('kdPasien');
			$kd_unit  = $this->input->post('kdUnit');
			$kd_dokter = $this->input->post('kdDokter');
			$tgl_trans = $this->input->post('tglTrans');
			$no_trans = $this->input->post('noTrans');
			$kd_customer = $this->input->post('kdCustomer');
			$keterangan = $this->input->post('Keterangan');
	
			$_kduser = $this->session->userdata['user_id']['id'];
			$username = $this->session->userdata['user_id']['username'];
			
			$notrans = $this->GetIdTransaksi($kd_kasir);
			$Schurut = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
    
			$nama = $this->db->query("select nama from pasien where kd_pasien='".$kd_pasien."'")->row()->nama;
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
			
			
			/* //transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			
			echo $notrans;
			echo "<br>";
			echo $no_trans;
			echo "<br>";
			echo $notransbaru;
			 */
			date_default_timezone_set("Asia/Jakarta");
			$datahistory = array(
				"kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"nama"=>$nama,
				"kd_unit"=>$kd_unit,
				"nama_unit"=>$unit,
				"kd_user"=>$_kduser,
				"kd_user_del"=>$_kduser,
				"shift"=>$this->GetShiftBagian(),
				"shiftdel"=>$this->GetShiftBagian(),
				"user_name"=>$username,
				"tgl_batal"=>date('Y-m-d'),
				"ket"=>$keterangan,
				"jam_batal"=>gmdate("d/M/Y H:i:s", time()+60*61*7)
			);
	
			$inserthistory = $this->db->insert('history_batal_trans',$datahistory);
	
	
			$data = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notrans,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurut,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL,
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
				"kd_kasir_asal"=>$kd_kasir,
				"no_transaksi_asal"=>$no_trans,
				"posting_transaksi"=>"True"
			);
						  
			$insert = $this->db->insert('transaksi',$data);		
			
			
			//transaksi baru setelah dicancel
			$notransbaru = $this->GetIdTransaksi($kd_kasir);
			$Schurutbaru = $this->GetAntrian($kd_pasien,$kd_unit,date('Y-m-d'),$kd_dokter);
			
			$databaru = array("kd_kasir"=>$kd_kasir,
				"no_transaksi"=>$notransbaru,
				"kd_pasien"=>$kd_pasien,
				"kd_unit"=>$kd_unit,
				"tgl_transaksi"=>$tgl_trans,
				"urut_masuk"=>$Schurutbaru,
				"tgl_co"=>NULL,
				"co_status"=>"False", 
				"orderlist"=>NULL,
				"ispay"=>"False",
				"app"=>"False",
				"kd_user"=>$_kduser,
				"tag"=>NULL,
				"lunas"=>"False",
				"tgl_lunas"=>NULL
			);
			
			$insertbaru = $this->db->insert('transaksi',$databaru);	
			//akhir tambah transaksi nomor baru	
			
	
			$dataUpdate = array(
				"batal"=>'True',
				"tgl_batal"=>date('Y-m-d'),
			);
	
			$update = $this->db->where('no_transaksi',$no_trans);
			$update = $this->db->where('kd_kasir',$kd_kasir);
			$update = $this->db->update('transaksi',$dataUpdate);				  
			// var_dump($data);
						 
						 
			if($insert)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				$date = date('Y-m-d');  
				$dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					-1*jumlah as jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					-1*sisa as sisa,
					-1*total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					-1*jumlah as jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notrans',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component
					-1*jumlah as jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
			}
			
			if($insertbaru)
			{
				$detail_transaksi = $this->db->query("insert into detail_transaksi
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_user,
					kd_tarif,
					kd_produk,
					kd_unit,
					tgl_berlaku,
					charge,
					adjust,
					folio,
					qty,
					harga,
					shift,
					kd_dokter,
					kd_unit_tr,
					cito,
					js,
					jp,
					no_faktur,
					flag,
					tag,
					hrg_asli,
					kd_customer,
					kd_loket
					from detail_transaksi where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  
				$detail_component = $this->db->query("insert into detail_component
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_component,
					tarif,
					disc,
					markup
					from detail_component where kd_kasir='".$kd_kasir."' AND no_transaksi ='".$no_trans."'");
				  $date = date('Y-m-d');
				/* $dtl_bayar = $this->db->query("insert into detail_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					$_kduser,
					kd_unit,
					kd_pay,
					jumlah,
					folio,
					shift,
					status_bayar,
					deposit,
					kd_dokter,
					tag,
					verified,
					kd_user_ver,
					tgl_ver,
					sisa as sisa,
					total_dibayar as total_dibayar,
					reff,
					pembayar,
					alamat,
					untuk,
					no_kartu
					from detail_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				
				$dtl_tr_bayar = $this->db->query("insert into detail_tr_bayar 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					jumlah
					from detail_tr_bayar where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				");
				
				$dtl_tr_bayar_component = $this->db->query("insert into detail_tr_bayar_component 
					select 
					'$kd_kasir',
					'$notransbaru',
					urut,
					tgl_transaksi,
					kd_pay,
					urut_bayar,
					tgl_bayar,
					kd_component,
					jumlah
					from detail_tr_bayar_component where kd_kasir ='".$kd_kasir."' AND no_transaksi = '".$no_trans."'
				"); */
				
			} 
				
				
				
			 if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				echo '{success:false}';
			}
			else
			{
				$this->db->trans_commit(); 
				echo "{success:true, notrans:'$notrans',notransbaru:'$notransbaru', urutmasuk:'$Schurut', urutmasukbaru:'$Schurutbaru'}";
			} 
		} 
		catch (Exception $e) 
		{
			echo $e->getMessage();
		}
	}
	
	function regonline_pasienpulang($kd_pasien){
		$strError = "";
		$pasien=$this->common->db2->query("SELECT kd_pasien FROM rs_pasien_rwi WHERE kd_pasien='".$kd_pasien."'");
		
		if(count($pasien->result()) != 0){
			$pasienpulang=$this->common->db2->query("Delete from rs_pasien_rwi where kd_pasien='".$kd_pasien."'");
			if($pasienpulang){
				$strError = "sukses";
			} else{
				$strError = "eror";
			}
		} else{
			$strError = "eror";
		}
		
		return $strError;
		
	}
	
	public function getTotKunjungan(){
		$kd_bagian='1';
		$total = $this->db->query("select count(*)as total from kunjungan k
									inner join unit u on u.kd_unit=k.kd_unit
									where k.tgl_masuk='".date('Y-m-d')."' and u.kd_bagian='".$kd_bagian."'");
		if(count($total->result()) > 0){
			$totalkunjungan = $total->row()->total;
		} else{
			$totalkunjungan = 0;
		}
		
		echo "{success:true, totalkunjungan:'".$totalkunjungan." "."'}";
	}
	
	public function getIcd9(){
		$result = $this->db->query("select kd_icd9, deskripsi from icd_9 
									where (upper(deskripsi) like upper('".$_POST['text']."%') or  upper(kd_icd9) like upper('".$_POST['text']."%'))
									order by deskripsi asc")->result();
		
		$jsonResult=array();
		$jsonResult['processResult']='SUCCESS';
		$jsonResult['listData']=$result;
		echo json_encode($jsonResult);
	}
	
	public function saveIcd9(){
		$mrtindakan=array();
		$mrtindakan['kd_pasien']=$_POST['kd_pasien'];	
		$mrtindakan['kd_unit']=$_POST['kd_unit'];
		$mrtindakan['tgl_masuk']=$_POST['tgl_masuk'];
		$mrtindakan['urut_masuk']=$_POST['urut_masuk'];
		$mrtindakan['no_transaksi']=$_POST['no_transaksi'];
		$mrtindakan['tgl_tindakan']=date('Y-m-d');
		$mrtindakan['kd_kasir']=$_POST['kd_kasir'];
		
		$jmllist = $_POST['jumlah'];
		for($i=0;$i<$jmllist;$i++){
			$cek = $this->db->query("select * from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
								and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']." 
								and urut=".$_POST['urut-'.$i]." and kd_icd9='".$_POST['kd_icd9-'.$i]."'")->result();
			if(count($cek) == 0){
				$mrtindakan['kd_icd9']=$_POST['kd_icd9-'.$i];
				$mrtindakan['urut']=$_POST['urut-'.$i];
				$save=$this->db->insert('mr_tindakan',$mrtindakan);
			}
		}
		
		if($save){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
		
	}
	
	public function viewgridicd9(){
		$result = $this->db->query("select mr.*,i.deskripsi 
									from mr_tindakan mr
										inner join icd_9 i on i.kd_icd9=mr.kd_icd9
									where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
									and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']." ORDER BY urut")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function hapusBarisGridIcd(){
		$delete = $this->db->query("delete from mr_tindakan where kd_pasien='".$_POST['kd_pasien']."' and kd_unit='".$_POST['kd_unit']."' 
									and tgl_masuk='".$_POST['tgl_masuk']."' and urut_masuk=".$_POST['urut_masuk']." 
									and urut=".$_POST['urut']." and kd_icd9='".$_POST['kd_icd9']."'");
		if($delete){
			echo '{success:true}';
		} else{
			echo '{success:false}';
		}
	}
	
	public function viewgridriwayatkunjungan(){
		$result = $this->db->query("select k.kd_pasien,p.nama,k.tgl_masuk,k.kd_unit,u.nama_unit,k.*,t.kd_kasir
									from kunjungan k
										inner join pasien p on p.kd_pasien=k.kd_pasien
										inner join unit u on u.kd_unit=k.kd_unit
										inner join transaksi t on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
									where k.kd_pasien='".$_POST['kd_pasien']."'
									order by tgl_masuk,jam_masuk asc")->result();
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatdiagnosa(){
		$result = $this->db->query("SELECT mrp.*,p.penyakit,n.morfologi,k.sebab 
									FROM mr_penyakit mrp 
										LEFT JOIN penyakit p ON mrp.kd_penyakit=p.kd_penyakit 
										LEFT JOIN neoplasma n ON n.kd_penyakit=mrp.kd_penyakit AND n.kd_pasien=mrp.kd_pasien AND n.kd_unit=mrp.kd_unit AND n.tgl_masuk=mrp.tgl_masuk AND n.tgl_masuk=mrp.tgl_masuk 
										LEFT JOIN kecelakaan k ON k.kd_penyakit=mrp.kd_penyakit AND k.kd_pasien=mrp.kd_pasien AND k.kd_unit=mrp.kd_unit AND k.tgl_masuk=mrp.tgl_masuk AND k.tgl_masuk=mrp.tgl_masuk 
									WHERE mrp.kd_pasien = '".$_POST['kd_pasien']."' 
										and mrp.kd_unit='".$_POST['kd_unit']."' 
										and mrp.tgl_masuk = '".$_POST['tgl_masuk']."'")->result();
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			$row[$i]['penyakit'] = $result[$i]->penyakit;
			$row[$i]['kd_penyakit'] = $result[$i]->kd_penyakit;
			
            if ($result[$i]->stat_diag == 0) {
                $row[$i]['stat_diag'] ='Diagnosa Awal';
            } else if ($result[$i]->stat_diag == 1) {
				$row[$i]['stat_diag'] = 'Diagnosa Utama';
            } else if ($result[$i]->stat_diag == 2) {
                $row[$i]['stat_diag'] ='Komplikasi';
            } else if ($result[$i]->stat_diag == 3) {
                $row[$i]['stat_diag'] = 'Diagnosa Sekunder';
            }
            if ($result[$i]->kasus == 't') {
				$row[$i]['kasus'] =  'Baru';
            } else if ($result[$i]->kasus == 'f') {
                $row[$i]['kasus'] =  'Lama';
            }
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($row).'}';
	}
	
	
	public function viewgridriwayattindakan(){
		$result = $this->db->query("select dt.*, p.deskripsi
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
									where k.kd_pasien='".$_POST['kd_pasien']."' 
										and k.kd_unit='".$_POST['kd_unit']."' 
										and k.tgl_masuk='".$_POST['tgl_masuk']."' 
										and k.urut_masuk='".$_POST['urut_masuk']."'
										and t.kd_kasir='".$_POST['kd_kasir']."'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatobat(){
		$result = $this->db->query("select bod.*, bo.kd_pasienapt, o.nama_obat
									from apt_barang_out_detail bod
										inner join apt_barang_out bo on bo.no_out=bod.no_out and bo.tgl_out=bod.tgl_out
										inner join transaksi t on t.no_transaksi=bo.apt_no_transaksi and t.kd_kasir=bo.apt_kd_kasir 
											and t.kd_pasien=bo.kd_pasienapt and t.kd_unit=bo.kd_unit
										inner join apt_obat o on o.kd_prd=bod.kd_prd
									where bo.kd_pasienapt='".$_POST['kd_pasien']."' 
										and bo.kd_unit='".$_POST['kd_unit']."' 
										and t.tgl_transaksi='".$_POST['tgl_masuk']."' 
										and t.urut_masuk='".$_POST['urut_masuk']."'
									order by o.nama_obat")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatlab(){
		$result = $this->db->query("select Klas_Produk.Klasifikasi,Produk.Deskripsi,LAB_test.*,LAB_hasil.hasil, LAB_hasil.ket_hasil,LAB_hasil.ket, LAB_hasil.kd_unit_asal,unit.nama_unit as nama_unit_asal, LAB_hasil.Urut, lab_metode.metode, 
										case when lab_test.kd_test = 0 then lab_test.item_test when lab_test.kd_test <> 0 then '' end as Judul_Item
									From LAB_hasil 
										inner join LAB_test on LAB_test.kd_test=LAB_hasil.kd_Test and lab_test.kd_lab=lab_hasil.kd_lab   
										inner join (produk inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)
											on LAB_Test.kd_Test = produk.Kd_Produk
										inner join lab_metode on lab_metode.kd_metode=LAB_hasil.kd_metode
										inner join unit on unit.kd_unit=lab_hasil.kd_unit_asal
									where LAB_hasil.Kd_Pasien = '".$_POST['kd_pasien']."' 
										And LAB_hasil.Tgl_Masuk = '".$_POST['tgl_masuk']."'  
										and LAB_hasil.Urut_Masuk = '".$_POST['urut_masuk']."'
										and LAB_hasil.kd_unit= '".$_POST['kd_unit']."'  
									order by lab_test.kd_lab,lab_test.kd_test asc")->result();
		
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			$row[$i]['klasifikasi'] = $result[$i]->klasifikasi;
			$row[$i]['deskripsi'] = $result[$i]->deskripsi;
			$row[$i]['kd_lab'] = $result[$i]->kd_lab;
			$row[$i]['kd_test'] = $result[$i]->kd_test;
			$row[$i]['item_test'] = $result[$i]->item_test;
			$row[$i]['satuan'] = $result[$i]->satuan;
			$row[$i]['normal'] = $result[$i]->normal;
			$row[$i]['kd_metode'] = $result[$i]->kd_metode;
			$row[$i]['judul_item'] = $result[$i]->judul_item;
			$row[$i]['kd_unit_asal'] = $result[$i]->kd_unit_asal;
			$row[$i]['nama_unit_asal'] = $result[$i]->nama_unit_asal;
			$row[$i]['ket_hasil'] = $result[$i]->ket_hasil;
			$row[$i]['urut'] = $result[$i]->urut;
			
            if ($result[$i]->hasil == 'null' || $result[$i]->hasil == null) {
                $row[$i]['hasil'] ='';
            } else {
				$row[$i]['hasil'] = $result[$i]->hasil;
            }
			
            if ($result[$i]->ket == 'null' || $result[$i]->ket == null || $result[$i]->ket == 'undefined') {
				$row[$i]['ket'] = '';
            } else {
                $row[$i]['ket'] = $result[$i]->ket;
            }
			
			if ($result[$i]->satuan == 'null' && $result[$i]->normal == 'null') {
				$row[$i]['metode'] = '';
            } else {
                $row[$i]['metode'] = $result[$i]->metode;
            }
		}
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewgridriwayatrad(){
		$result = $this->db->query("select dt.*, p.deskripsi
									from detail_transaksi dt
										inner join transaksi t on t.no_transaksi=dt.no_transaksi and t.kd_kasir=dt.kd_kasir
										inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi 
											and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
										inner join produk p on p.kd_produk=dt.kd_produk
									where k.kd_pasien='".$_POST['kd_pasien']."' 
										and k.kd_unit='".$_POST['kd_unit']."' 
										and k.tgl_masuk='".$_POST['tgl_masuk']."' 
										and k.urut_masuk='".$_POST['urut_masuk']."'
										and t.kd_kasir='".$_POST['kd_kasir']."'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	public function viewanamnese(){
		$result = $this->db->query("select anamnese from kunjungan k
									where k.kd_pasien='".$_POST['kd_pasien']."' 
										and k.kd_unit='".$_POST['kd_unit']."' 
										and k.tgl_masuk='".$_POST['tgl_masuk']."' 
										and k.urut_masuk='".$_POST['urut_masuk']."'");
		if(count($result->result()) > 0){
			$anamnese = $result->row()->anamnese;
		} else{
			$anamnese = "";
		}
		
		echo "{success:true, anamnese:'$anamnese'}";
	}
	/*
	
		PERBARUAN GET PRODUK / TINDAKAN
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017 - 01 - 09
	 */
	public function getProdukKey(){
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row()->kd_tarif;
		$kp_produk  = $this->db->query("SELECT kp_produk, kd_produk FROM produk WHERE kp_produk = '".$_POST['text']."'");
		$tgl_now 	= date("Y-m-d");
		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
		}
		$q_text 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_text = "  and produk.kp_produk='".$_POST['text']."' ";
		}

		$q_kp_produk= "";
		if ($kp_produk->num_rows() > 0) {
			$q_kp_produk = " p.kp_produk = '".$kp_produk->row()->kp_produk."'";
		}else{
			$q_kp_produk = " p.kd_produk = '".$_POST['text']."'";
		}
		$row = $this->db->query("select kd_tarif from tarif_cust ".$q_customer)->row();
		$sql = "SELECT 
			row_number() OVER () as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_unit = '".$_POST['kd_unit']."' 
					AND (c.kd_customer = '".$_POST['kd_customer']."' OR t.kd_tarif='".$kd_tarif."') 
					AND ".$q_kp_produk."
					AND t.tgl_berlaku < '".$tgl_now."'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.tgl_berlaku DESC";
		 
		$jsonResult = array();
		$data       = array();		
		$result     = $this->db->query($sql)->row();
		if ($result) {
			$data['deskripsi'] 		= $result->deskripsi;
			$data['kd_produk'] 	 	= $result->kd_produk;
			$data['kp_produk'] 	 	= $result->kp_produk;
			$data['kd_tarif'] 	 	= $result->kd_tarif;
			$data['kd_unit'] 	 	= $result->kd_unit;
			$data['nama_unit'] 	 	= $result->nama_unit;
			$data['rn'] 	 		= $result->rn;
			$data['rnum'] 	 		= $result->rnum;
			$data['tarifx'] 	 	= $result->tarif;
			$data['tgl_berakhir'] 	= $result->tgl_berakhir;
			$data['tgl_berlaku'] 	= $result->tgl_berlaku;
			$text = strtolower($data['deskripsi']);
			if (stripos($text, "konsul") !== false) {
				$data['status_konsultasi'] 	= true;
			}else{
				$data['status_konsultasi'] 	= false;
			}
			$jsonResult['processResult'] =   'SUCCESS';
		}else{
	    	$jsonResult['processResult'] 	= 'FAILED';
	    	$data = null;
		}
    	$jsonResult['listData'] 		= $data;
    	echo json_encode($jsonResult);
    }

	public function insertDataProduk(){
		/*
			PERBARUAN INSERT DETAIL TRANSAKSI
			OLEH 	: HADAD & DONNY
			TANGGAL : 2017 - 02 -28
		 */
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$result 	= array();
		$result['nama_dokter'] = "";

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=>  $_POST['no_transaksi'],
		);
		$queryPasienInap = $this->Tbl_data_pasien_inap->selectPasienInap($criteriaParams);

		$params = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=> $_POST['no_transaksi'],
			'kd_produk'     => $_POST['kd_produk'],
			'urut' 			=> $_POST['urut'],
			'kd_tarif' 		=> $_POST['kd_tarif'],
			'tgl_transaksi'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_transaksi']))),
			'tgl_berlaku'	=> date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_berlaku']))),
		);


		$paramsUrut = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=> $_POST['no_transaksi'],
			//'kd_unit_tr1' 	=> $_POST['unit'],
		);

		$result['kp_produk'] = $this->db->query("SELECT * from produk where kp_produk='".$this->input->post('kd_produk')."' OR kd_produk='".$this->input->post('kd_produk')."'")->row()->kp_produk;
		if ($this->input->post('fungsi') === false || $this->input->post('fungsi') == 'false') {
			$setter['kd_produk']     = $_POST['kd_produk'];
			$setter['qty']           = $_POST['quantity'];
			$setter['tgl_transaksi'] = $_POST['tgl_transaksi'];
			$paramsCriteria = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_produk'     => $params['kd_produk'],
				'urut' 			=> $params['urut'],
				'kd_tarif' 		=> $params['kd_tarif'],
			);
			// $qty 		= $this->M_produk->cekDetailTransaksi($params)->row()->qty;
			$success    = $this->M_produk->updateDetailTransaksi($paramsCriteria, $setter);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$successSQL = $this->M_produk->updateDetailTransaksiSQL($paramsCriteria, $setter);
			}else{
				$successSQL = true;
			}

			if (($success>0 || $success===true)&&($successSQL>0 || $successSQL === true)) {
				//$this->db->trans_commit();
				//$this->dbSQL->trans_commit();
				$result['status'] = true;
				$result['action'] = "update";
			}else{
				//$this->db->trans_rollback();
				//$this->dbSQL->trans_rollback();
				$result['status'] = false;
				$result['action'] = "update";
			}
		}else{
			$params['kd_unit'] 		= $queryPasienInap->row()->kd_unit;
			$params['kd_unit_tr'] 	= $queryPasienInap->row()->kd_unit;
			//$urutPG 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
			// DISINI
			// if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			// 	$urut 	= $this->M_produk->cekMaxUrutSQL($paramsUrut)->row()->max_urut;
			// }else{
			$criteriaDetail = array(
				'kd_kasir' 		=> $_POST['kd_kasir'],
				'no_transaksi' 	=> $_POST['no_transaksi'],
				'urut' 			=> $_POST['urut'],
			); 
			$query_get_urut = $this->M_produk->cekDetailTransaksi($criteriaDetail);
			if ($query_get_urut->num_rows() > 0) {
				$urut 	= $this->M_produk->cekMaxUrut($paramsUrut)->row()->max_urut;
				$params['urut'] 	= (int)$urut+1;
			}else{
				$params['urut'] 	= $_POST['urut'];
			}
			
			$params['folio1']         = "A";
			$params['qty']           = $_POST['quantity'];
			$params['harga']         = $_POST['harga'];

			$paramsInsert = array(
				'kd_kasir' 		=> $params['kd_kasir'],
				'no_transaksi' 	=> $params['no_transaksi'],
				'kd_produk' 	=> $params['kd_produk'],
				'urut' 			=> $params['urut'],
				'kd_unit' 		=> $params['kd_unit_tr'],
				'kd_unit_tr' 	=> $params['kd_unit_tr'],
				'kd_tarif' 		=> $params['kd_tarif'],
				'tgl_transaksi' => $params['tgl_transaksi'],
				'tgl_berlaku' 	=> $params['tgl_berlaku'],
				'folio' 		=> $this->input->post("folio"),
				'qty' 			=> $params['qty'],
				'harga' 		=> $params['harga'],
				'kd_user' 		=> $this->session->userdata['user_id']['id'],
			);

			if ($this->input->post("folio") == "E") {
				$paramsInsert['no_faktur'] = $this->input->post('no_faktur');
			}

			$result['komponen'] 		= $this->cekKomponen($params['kd_produk'], $params['kd_tarif']);
			$success         = $this->M_produk->insertDetailTransaksi($paramsInsert);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$successSQL     = $this->M_produk->insertDetailTransaksiSQL($paramsInsert);
			}else{
				$successSQL 	= true;
			}
			if ($success>0 && ($successSQL>0 || $successSQL === true)) {
				/*
					detail_tr_kamar 
				*/

				$criteriapasien['kd_kasir'] 	= $_POST['kd_kasir'];
				$criteriapasien['no_transaksi'] = $_POST['no_transaksi'];
				$getDatapasien  = $this->M_produk->getDatapasien($criteriapasien);

				$criteriagetDatakunjunganpasien['kd_pasien'] 	= $getDatapasien->row()->kd_pasien;
				$criteriagetDatakunjunganpasien['kd_unit']		= $params['kd_unit_tr'];
				$getDatakunjunganpasien  = $this->M_produk->getDatakunjunganpasien($criteriagetDatakunjunganpasien);


				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
				);
				$getDatakamar   = $this->Tbl_data_pasien_inap->selectPasienInap($criteriaParams);
				unset($criteriaParams);
				$criteriaParams = array(
					'kd_kasir' 		=> $_POST['kd_kasir'],
					'no_transaksi' 	=> $_POST['no_transaksi'],
					'kd_unit' 		=> $params['kd_unit_tr'],
					'no_kamar' 		=> $getDatakamar->row()->no_kamar,
					'kd_spesial' 	=> $getDatakamar->row()->kd_spesial
				);

				$queryPG 	= $this->Tbl_data_detail_tr_kamar->getMaxUrut($criteriaParams);
				if ($queryPG->num_rows() > 0) {
					$urut_detail_tr_kamar = (int)$queryPG->row()->urut_max + 1;
				}else{
					$urut_detail_tr_kamar = 1;
				}

				$params_detail_tr_kamar = array(
					'kd_kasir' 		=> $_POST['kd_kasir'],
					'no_transaksi' 	=> $_POST['no_transaksi'],
					'tgl_transaksi' => date('Y-m-d',strtotime(str_replace('/','-', $_POST['tgl_transaksi']))),
					'urut' 			=> $params['urut'],
					'kd_unit' 		=> $getDatakamar->row()->kd_unit,
					'no_kamar' 		=> $getDatakamar->row()->no_kamar,
					'kd_spesial' 	=> $getDatakamar->row()->kd_spesial
				);
				//$successdetail_tr_kamar = $this->M_produk->insertDetailTrKamar($params_detail_tr_kamar);
				$success 	= $this->Tbl_data_detail_tr_kamar->insert($params_detail_tr_kamar);
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$successSQL	= $this->Tbl_data_detail_tr_kamar->insert_SQL($params_detail_tr_kamar);
				}else{
					$successSQL = true;
				}
				/* 
					End Saving Detail Kamar 
				*/

				$params_tarif 	= array(
					'kd_unit' 		=> $params['kd_unit_tr'],
					'kd_tarif' 		=> $params['kd_tarif'],
					'tgl_berlaku'	=> $params['tgl_berlaku'],
					'kd_produk' 	=> $params['kd_produk'],
				);
				$result_data 	= $this->M_produk->getDataTarifComponent($params_tarif);

				$params_delete = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi'	=> $params['tgl_transaksi'],
					'urut'			=> $params['urut'],
				);
				$deleteDetail    = $this->M_produk->deleteDetailComponent($params_delete);
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$deleteDetailSQL = $this->M_produk->deleteDetailComponentSQL($params_delete);
				}else{
					$deleteDetailSQL = true;
				}

					foreach ($result_data->result_array() as $data) {
						$params_detail_component = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'no_transaksi' 	=> $params['no_transaksi'],
							'tgl_transaksi'	=> $params['tgl_transaksi'],
							'urut'			=> $params['urut'],
							'tarif'			=> $data['tarif'],
							'kd_component'	=> $data['kd_component'],
						);
						$success    = $this->M_produk->insertDetailComponent($params_detail_component);
						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							$successSQL = $this->M_produk->insertDetailComponentSQL($params_detail_component);
						}else{
							$successSQL = true;
						}
					}

					if ($success>0 && ($successSQL>0 || $successSQL === true)) {

						if (!empty($_POST['kd_job']) || isset($_POST['kd_job'])) {
							$params['kd_job']	= $_POST['kd_job'];
							$result['dokter']   = true;
						}else{
							$params['kd_job'] 	= "";
							$result['dokter'] 	= false;
						}

						if (!empty($_POST['kd_dokter']) || isset($_POST['kd_dokter'])) {
							$params['kd_dokter']	= $_POST['kd_dokter'];
						}else{
							$params['kd_dokter'] 	= "";
						}

						if ($params['kd_job']!="" && $params['kd_dokter']!="") {
							/*
								GET DATA DOKTER INAP
							 */
							$criteriaDokterInap['kd_job'] = $params['kd_job'];
							$resultVisiteDokter   = $this->M_produk->getDataDokterInap_int($criteriaDokterInap);
							/*
								GET DATA TARIF COMPONENT
							 */
							$criteriaTarifComponent = array(
								'kd_component' => $resultVisiteDokter->row()->kd_component,
								'kd_tarif'     => $params['kd_tarif'],
								'kd_produk'    => $params['kd_produk'],
								//'kd_unit'      => $params['kd_unit_tr'],
								//'tgl_berlaku'  => $params['tgl_berlaku'],
							);
							$resultTarifComponent = $this->M_produk->getDataTarifComponent($criteriaTarifComponent);

							if ($resultTarifComponent->num_rows() == 0) {
								unset($criteriaTarifComponent);
								$criteriaTarifComponent = array(
									// 'kd_component' => '21',
									'kd_tarif'     => $params['kd_tarif'],
									'kd_produk'    => $params['kd_produk'],
									'kd_unit'      => $params['kd_unit_tr'],
									//'tgl_berlaku'  => $params['tgl_berlaku'],
								);
								$resultTarifComponent = $this->M_produk->getDataTarifComponent($criteriaTarifComponent);
							}
							/*
								GET DATA TARIF LINE TERAKHIR DI VISITE DOKTER
							 */
							$criteriaVisiteDokter = array(
								'kd_kasir'      => $params['kd_kasir'],
								'no_transaksi'  => $params['no_transaksi'],
								/*'urut'          => $params['urut'],*/
								'tgl_transaksi' => $params['tgl_transaksi'],
								'kd_unit'       => $params['kd_unit_tr'],
							);
							
							$line = $this->M_produk->getMaxLineVisiteDokter($criteriaVisiteDokter);
							$criteriaUnit['kd_unit'] = $params['kd_unit_tr'];
							/*if (isset($resultTarifComponent->row()->tarif)) {
								$tarif = $resultTarifComponent->row()->tarif;
							}else{
								$tarif = 0;
							}*/
							$parent_unit = $this->M_produk->getDataUnit($criteriaUnit)->row()->parent;
								$tmpKdDokter = explode("-",$params['kd_dokter']);
								//$result['tmpKdDokter'] = $result['tmpKdDokter'][1];
								for($i=0;$i<=count($tmpKdDokter)-1;$i++){
									$queryJenisDokter = $this->db->query("SELECT jenis_dokter FROM dokter where kd_dokter = '".$tmpKdDokter[$i]."'")->row()->jenis_dokter;
									if ($queryJenisDokter == 1) {
										$query = $this->db->query("SELECT tarif from tarif_component 
											WHERE 
											kd_component = '20' 
											and kd_tarif = '".$params['kd_tarif']."'
											and kd_produk = '".$params['kd_produk']."'
											and kd_unit = '".$params['kd_unit']."'
											and tgl_berlaku = '".$params['tgl_berlaku']."'
										");
										if ($query->num_rows() > 0) {
											$tarif = $query->row()->tarif;
										}else{
											$tarif = 0;
										}
										$tag_char = "20";
									}else{
										$query = $this->db->query("SELECT tarif from tarif_component 
											WHERE 
											kd_component = '21' 
											and kd_tarif = '".$params['kd_tarif']."'
											and kd_produk = '".$params['kd_produk']."'
											and kd_unit = '".$params['kd_unit']."'
											and tgl_berlaku = '".$params['tgl_berlaku']."'
										");
										if ($query->num_rows() > 0) {
											$tarif = $query->row()->tarif;
										}else{
											$tarif = 0;
										}
										$tag_char = "21";
									}

									$paramsDataVisiteDokter = array(
										'kd_kasir'      => $params['kd_kasir'],
										'no_transaksi'  => $params['no_transaksi'],
										'urut'          => $params['urut'],
										'tgl_transaksi' => $params['tgl_transaksi'],
										'line'          => (int)$line->row()->line+1,
										'kd_dokter'     => $tmpKdDokter[$i],
										'kd_unit'       => $parent_unit,
										'tag_int'       => $params['kd_produk'],
										'tag_char'      => $tag_char,
										// 'jp'            => (int)$tarif*((int)$resultVisiteDokter->row()->prc/100),
										'jp'            => (int)$tarif,
										'kd_job'        => $resultVisiteDokter->row()->kd_job,
										'prc'           => $resultVisiteDokter->row()->prc,
										'vd_markup'     => 0,
										'vd_disc'       => 0,
									);
									if ($query->num_rows() > 0) {
										if ($result['komponen'] === true) {
											$criteriaParams = array(
												'kd_dokter' 	=> $tmpKdDokter[$i],
											);

											$queryDokter = $this->Tbl_data_dokter->selectDataDokter($criteriaParams);
											if ($queryDokter->num_rows() > 0) {
												$result['nama_dokter'] .= $queryDokter->row()->nama.",";
											}else{
												$result['nama_dokter'] = "";
											}

											$success    = $this->M_produk->insertVisiteDokter($paramsDataVisiteDokter);
											if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
												$successSQL = $this->M_produk->insertVisiteDokterSQL($paramsDataVisiteDokter);
											}else{
												$successSQL = true;
											}
										}else{
											$success    = true;
											$successSQL = true;
										}
									}else{
		                                $success    = true;
										$successSQL = true;
									}
								}

								$result['nama_dokter'] = substr($result['nama_dokter'], 0, strlen($result['nama_dokter'])-1);
						}
						
						if (($success>0 || $success===true)&&($successSQL>0 || $successSQL === true)) {
							$result['status'] = true;
							$result['action'] = "insert";
							$result['urut']   = $params['urut'];
						}else{
							$result['status'] = false;
						}
					}else{
						$result['status'] = false;
					}
			}else{
				$result['status'] = false;
				$result['action'] = "insert";
			}
		}

		$paramsCekDokter = array(
			'kd_kasir' 		=> $params['kd_kasir'],
			'no_transaksi' 	=> $params['no_transaksi'],
			'tgl_transaksi'	=> $params['tgl_transaksi'],
			'urut'			=> $params['urut'],
		);

		/*if (isset($_POST['kd_dokter'])) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_dokter' 	=> $_POST['kd_dokter'],
			);

			$queryDokter = $this->Tbl_data_dokter->selectDataDokter($criteriaParams);
			if ($queryDokter->num_rows() > 0) {

				$result['nama_dokter'] = $queryDokter->row()->nama;
			}else{
				$result['nama_dokter'] = "";
			}
		}*/

		$result['jumlah_penangan'] 	= $this->total_dokter_penangan($paramsCekDokter);
		$result['no_transaksi'] 	= $params['no_transaksi'];
		if (($success>0 || $success===true)&&($successSQL>0 || $successSQL === true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($result);
	}

	private function total_dokter_penangan($params){
		return $this->db->query("SELECT COUNT(kd_dokter) as count FROM visite_dokter WHERE kd_kasir='".$params['kd_kasir']."' 
			AND no_transaksi='".$params['no_transaksi']."' 
			AND tgl_transaksi='".$params['tgl_transaksi']."' 
			AND urut='".$params['urut']."'")->row()->count;
	}

	public function hapusDataProduk(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$result 	= array();

		$data 		= array(
			//'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'urut' 			=> $this->input->post('urut'),
			'tgl_transaksi'	=> $this->input->post('tgl_transaksi'),
		);

		$paramsDetailTransaksi = array(
			'kd_produk' 	=> $this->input->post('kd_produk'),
			'no_transaksi' 	=> $data['no_transaksi'],
			'kd_kasir' 		=> $data['kd_kasir'],
			'tgl_transaksi'	=> $data['tgl_transaksi'],
		);
		$urut 		= $this->M_produk->cekDetailTransaksi($paramsDetailTransaksi);
		/* ($urut->num_rows() > 0) {
			$data['urut'] 	= $urut->row()->urut;
		}*/

		// $delete 	= $this->M_produk->deleteDetailComponent($data);
		// $deleteSQL 	= $this->M_produk->deleteDetailComponentSQL($data);
		$delete 	= true;
		$deleteSQL 	= true;

		if ($delete>0 && $deleteSQL>0) {
			$delete 	= $this->M_produk->deleteDetailTransaksi($data);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$deleteSQL 	= $this->M_produk->deleteDetailTransaksiSQL($data);
			}else{
				$deleteSQL 	= true;
			}
			if ($delete>0 && $deleteSQL>0) {
				$this->db->trans_commit();
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$this->dbSQL->trans_commit();
				}
				$result['SQL'] 	= $deleteSQL;
				$result['POSTGRE'] 	= $delete;
				$result['tahap'] 	= "detail transaksi";
				$result['status'] 	= true;
			}else{
				$result['SQL'] 	= $deleteSQL;
				$result['POSTGRE'] 	= $delete;
				if ($deleteSQL>0 || $deleteSQL === true) {
					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						$this->dbSQL->trans_commit();
					}
				}else if ($delete>0) {
					$this->db->trans_commit();
				}else{
					$this->db->trans_rollback();
					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						$this->dbSQL->trans_rollback();
					}
				}
				$result['tahap'] 	= "detail transaksi";
				$result['status'] 	= false;
			}
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$result['tahap'] 	= "detail komponen";
			$result['status'] 	= false;
		}

		echo json_encode($result);
	}

	/* 
		PERBARUAN GANTI KELOMPOK PASIEN 
		OLEH 	; HADAD
		TANGGAL : 2017 - 02 - 17
	 */

	public function UpdateGantiKelompok(){		
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$Kdcustomer 	= $_POST['KDCustomer'];
		$KdNoSEP 		= $_POST['KDNoSJP'];
		$KdNoAskes 		= $_POST['KDNoAskes'];
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$resultQuery = 0;
		$kd_unit = $this->db->query("SELECT * from nginap WHERE kd_unit_kamar='".$KdUnit."' AND kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."'")->row()->kd_unit;
		$result 	= $this->db->query("
			UPDATE kunjungan 
			SET kd_customer = '".$Kdcustomer."', 
			no_sjp='".$KdNoSEP."' 
			WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' 
			AND kd_unit='".$kd_unit."' 
			AND urut_masuk='".$UrutMasuk."'");
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$resultQuery 	= $this->dbSQL->query("UPDATE kunjungan SET 
				kd_customer = '".$Kdcustomer."', 
				no_sjp='".$KdNoSEP."' 
				WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' 
				AND kd_unit='".$kd_unit."' 
				AND urut_masuk='".$UrutMasuk."'");
		}else{
			$resultQuery = true;
		}

		if(($result>0 || $result===true) && ($resultQuery>0 || $resultQuery===true)){
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			//echo "Sukses";
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			//echo "Failed";
		}
		$criteriaParams = array(
			'kd_customer' => $_POST['KDCustomer'],
		);
		echo '{success:'.$result.', totalrecords:'.count($resultQuery).', ListDataObj:'.json_encode($resultQuery).', customer:"'.$this->Tbl_data_customer->selectDataCustomer($criteriaParams)->row()->customer.'"}';
	}

	/* 
		PERBARUAN GANTI DOKTER
		OLEH 	; HADAD
		TANGGAL : 2017 - 02 - 17
	*/
 
	public function UpdateGantiDokter(){		
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$result    = false;
		$resultSQL = false;
		$KdPasien 		= $_POST['KdPasien'];
		$TglMasuk 		= $_POST['TglMasuk'];
		$KdUnit 		= $_POST['KdUnit'];
		$UrutMasuk 		= $_POST['UrutMasuk'];
		$KdDokter 		= $_POST['KdDokter'];
		$KdDokterAsal	= 	$this->db->query("SELECT 
								kd_dokter 
								FROM kunjungan 
								WHERE 
								kd_pasien='".$KdPasien."' 
								AND tgl_masuk='".$TglMasuk."' 
								AND kd_unit='".$KdUnit."' 
								AND urut_masuk='".$UrutMasuk."'")->row()->kd_dokter;

		$result      = $this->db->query("UPDATE kunjungan SET kd_dokter = '".$KdDokter."' WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");

		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$resultSQL 	= $this->dbSQL->query("UPDATE kunjungan SET kd_dokter = '".$KdDokter."' WHERE kd_pasien='".$KdPasien."' AND tgl_masuk='".$TglMasuk."' AND kd_unit='".$KdUnit."' AND urut_masuk='".$UrutMasuk."'");
		}else{
			$resultSQL 	= true;
		}

		if (($result > 0 || $result===true) && ($resultSQL > 0 || $resultSQL===true)) {
			$resultNoTr = $this->db->query("SELECT no_transaksi FROM transaksi WHERE kd_pasien='".$KdPasien."' AND tgl_transaksi='".$TglMasuk."' AND kd_unit='".$KdUnit."'");
			if ($resultNoTr->num_rows() > 0) {
				$result    = $this->db->query("UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal."'");
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$resultSQL = $this->dbSQL->query("UPDATE detail_trdokter SET kd_dokter='".$KdDokter."' WHERE no_transaksi='".$resultNoTr->row()->no_transaksi."' AND kd_dokter='".$KdDokterAsal."'");
				}else{
					$resultSQL = true;
				}
			}else{
				$result 	= false;
				$resultSQL 	= false;
			}
		}else{
			$result 	= false;
			$resultSQL 	= false;
		}

		$criteriaParams = array(
			'kd_dokter' 	=> $_POST['KdDokter'],
		);


		if (($result > 0 || $result===true) && ($resultSQL > 0 || $resultSQL===true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).', nama_dokter:"'.$this->Tbl_data_dokter->selectDataDokter($criteriaParams)->row()->nama.'"}';
	}
	
	/* 
		CREATE	: MSD
		TGL		: 14-03-2017
		KET		: FITUR DEPOSIT
	*/
	
	function getKeteranganDeposit(){
		$ket = $this->db->query("select setting from sys_setting where key_data='rwi_keterangan_deposit'");
		if(count($ket->result()) > 0){
			echo "{success:true, ket:'".$ket->row()->setting."'}";
		} else{
			echo "{success:false}";
		}
	}
	
	function savedeposit(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$kd_pay       = $this->db->query("select setting from sys_setting where key_data='rwi_default_kd_pay_deposit'")->row()->setting;
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir     = $_POST['kd_kasir'];
		$tgl_bayar    = $_POST['tgl_bayar'];
		$kd_unit      = $_POST['kd_unit'];
		$jumlah       = $_POST['jumlah'];
		$shift        = $this->GetShiftBagian();
		$kd_user      = $this->session->userdata['user_id']['id'];
		
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$no_transaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		} else {
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
		
		$savepg = $this->db->query("select inserttrpembayaran('$kd_kasir','".$no_transaksi."',
					".$urut_detailbayar.",'".$tgl_bayar."',".$kd_user.",'".$kd_unit."','".$kd_pay."',".$jumlah.",'',".$shift.",'TRUE','".$tgl_bayar."',0)");

		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$savesql = $this->dbSQL->query("exec dbo.V5_inserttrpembayaran '$kd_kasir','".$no_transaksi."',".$urut_detailbayar.",
					'".$tgl_bayar."',".$kd_user.",'".$kd_unit."','".$kd_pay."',".$jumlah.",'',".$shift.",0,'".$tgl_bayar."',0");
		}else{
			$savesql = 1;
		}
		if($savepg && $savesql){
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			echo "{success:true}";
		} else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			echo "{success:false}";
		}				
	}
	
	function gettotalbayar(){
		$res = $this->db->query("select (select sum(harga*qty)from detail_transaksi where no_transaksi = '".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."') as totaltagihan,
								(select sum(jumlah) from detail_bayar where no_transaksi = '".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."') as jumlah");
		$totaltagihan=0;
		$jumlah=0;
		$sisabayar=0;
		if(count($res->result())>0){
			$totaltagihan=$res->row()->totaltagihan;
			if($res->row()->jumlah == NULL){
				$jumlah=0;
				$sisabayar=$res->row()->totaltagihan;
			} else{
				$jumlah=$res->row()->jumlah;
				$sisabayar=$res->row()->totaltagihan-$res->row()->jumlah;
			}
		}				
		echo "{success:true,totaltagihan:'".$totaltagihan."',jumlah:'".$jumlah."',sisabayar:'".$sisabayar."'}";					
	}
	
	function getrefreshdetailpembayaran(){
		$result = $this->db->query("SELECT T.KD_KASIR,T.NO_TRANSAKSI,T.KD_PASIEN,T.KD_UNIT,
									T.TGL_TRANSAKSI,DT.HARGA,DT.QTY,DT.TOTALITEM,
									(((".str_replace('.','',$_POST['total'])."-DB.JUMLAH)/".str_replace('.','',$_POST['total']).")*DT.HARGA) AS JUMLAH FROM TRANSAKSI T 
								INNER JOIN ( SELECT SUM(JUMLAH) AS JUMLAH,KD_KASIR,NO_TRANSAKSI 
										FROM DETAIL_BAYAR 
										WHERE no_transaksi='".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."' GROUP BY KD_KASIR,NO_TRANSAKSI) DB ON DB.KD_KASIR=T.KD_KASIR AND DB.NO_TRANSAKSI=T.NO_TRANSAKSI
								INNER JOIN (SELECT SUM(HARGA*QTY) AS TOTALITEM,KD_KASIR,NO_TRANSAKSI, HARGA,QTY 
										FROM DETAIL_TRANSAKSI 
										WHERE no_transaksi='".$_POST['no_transaksi']."' and kd_kasir='".$_POST['kd_kasir']."' GROUP BY KD_KASIR,NO_TRANSAKSI,HARGA,QTY) DT ON DT.KD_KASIR=T.KD_KASIR AND DT.NO_TRANSAKSI=T.NO_TRANSAKSI
								where T.no_transaksi='".$_POST['no_transaksi']."' and T.kd_kasir='".$_POST['kd_kasir']."'
								GROUP BY T.KD_KASIR,T.NO_TRANSAKSI,T.KD_PASIEN,T.KD_UNIT,T.TGL_TRANSAKSI,DT.HARGA,DT.QTY,DB.JUMLAH,DT.TOTALITEM")->result();
		$row=array();
		for($i=0; $i<count($result) ;$i++){
			// $row[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
			$row[$i]['HARGA'] = $result[$i]->harga;
			$row[$i]['QTY'] = $result[$i]->qty;
			$row[$i]['BAYARTR'] = $result[$i]->jumlah;
			$row[$i]['DISCOUNT'] = $result[$i]->jumlah;
			$row[$i]['PIUTANG'] = $result[$i]->jumlah;
		}
		echo "{success:true,ListDataObj:'".json_encode($row)."', total:'".count($result)."'}";
	}
	
	
	/* ==== AKHIR FITUR DEPOSIT ==== */

	/* ---------------------------- Penambahan Proses Discount untuk pembayaran di kasir RWI -----------------------------------------------------------
		Oleh 	: HDHT
		Tanggal : 15 maret 2017
		Tempat 	: Bandung
	*/

	function getJumlahTagihan(){		
		$kd_kasir = $_POST['KODE'];
		$no_transaksi = $_POST['NO'];
		
		$tagihan = $this->db->query("SELECT gettagihan('".$kd_kasir."','".$no_transaksi."')");
		$pembayaran = $this->db->query("SELECT getpembayaran('".$kd_kasir."','".$no_transaksi."')");
		
		if(count($tagihan->result()) > 0 and count($pembayaran->result()) > 0){
			$a = $tagihan->row()->gettagihan;
			$b = $pembayaran->row()->getpembayaran;
			$c = (int)$a - (int)$b;
			echo "{success:true, ket:'".$c."'}";
		} else{
			echo "{success:false}";
		}	
	}

	function savediscount(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$kd_pay = $this->db->query("select setting from sys_setting where key_data='rwi_default_kd_pay_discount'")->row()->setting;
		$no_transaksi = $_POST['no_transaksi'];
		$kd_kasir = $_POST['kd_kasir'];
		$tgl_bayar = date('Y-m-d 00:00:00');
		$kd_unit = $_POST['kd_unit'];
		$jumlah = $_POST['jumlah'];
		$shift = $this->GetShiftBagian();
		$kd_user = $this->session->userdata['user_id']['id'];
		$persen = $_POST['persen'];
		
		$det_query = $this->db->query("select max(urut) as urutan from detail_bayar where no_transaksi = '$no_transaksi'");
		if ($det_query->num_rows==0)
		{
			$urut_detailbayar = 1;
		} else {
			foreach($det_query->result() as $det)
			{
				$urut_detailbayar = $det->urutan+1;
			}
		}
		
		$savepg = $this->db->query("select inserttrpembayaran('$kd_kasir','".$no_transaksi."',
					".$urut_detailbayar.",'".$tgl_bayar."',".$kd_user.",'".$kd_unit."','".$kd_pay."',".$jumlah.",'',".$shift.",'TRUE','".$tgl_bayar."',0)");
		
		$savesql = $this->dbSQL->query("exec dbo.V5_inserttrpembayaran '$kd_kasir','".$no_transaksi."',".$urut_detailbayar.",
					'".$tgl_bayar."',".$kd_user.",'".$kd_unit."','".$kd_pay."',".$jumlah.",'',".$shift.",0,'".$tgl_bayar."',0");
		if($savepg && $savesql){
			$cekdetail = $this->db->query("SELECT * from (select detail_transaksi.no_transaksi ,detail_transaksi.urut, 
					COALESCE(SUM(detail_transaksi.harga * detail_transaksi.qty)/COUNT(detail_transaksi.urut) - SUM(detail_tr_bayar.jumlah),(detail_transaksi.harga * detail_transaksi.qty)  )  as tunai, 
					COALESCE(SUM(detail_transaksi.harga * detail_transaksi.qty)/COUNT(detail_transaksi.urut) - SUM(detail_tr_bayar.jumlah),(detail_transaksi.harga * detail_transaksi.qty)  )  as piutang,
					COALESCE(SUM(detail_transaksi.harga * detail_transaksi.qty)/COUNT(detail_transaksi.urut) - SUM(detail_tr_bayar.jumlah),(detail_transaksi.harga * detail_transaksi.qty)  )  as dicount,
					detail_transaksi.kd_unit,detail_transaksi.kd_produk,produk.deskripsi,detail_transaksi.harga,detail_transaksi.flag,detail_transaksi.qty,detail_transaksi.tgl_berlaku,
					detail_transaksi.kd_dokter,detail_transaksi.adjust,detail_transaksi.cito,detail_transaksi.kd_customer,detail_transaksi.tgl_transaksi,detail_transaksi.kd_tarif,detail_transaksi.kd_kasir
				from  detail_transaksi 
				left join detail_tr_bayar on 
					detail_transaksi.no_transaksi = detail_tr_bayar.no_transaksi AND 
					detail_transaksi.urut = detail_tr_bayar.urut AND
					detail_transaksi.tgl_transaksi = detail_tr_bayar.tgl_transaksi AND
					detail_transaksi.kd_kasir = detail_tr_bayar.kd_kasir 
				inner join
					unit on detail_transaksi.kd_unit = unit.kd_unit 
				inner join
					produk on detail_transaksi.kd_produk = produk.kd_produk 
				GROUP BY 
					detail_transaksi.urut,
					detail_transaksi.no_transaksi,
					detail_transaksi.harga * detail_transaksi.qty,
					detail_transaksi.kd_unit,
					detail_transaksi.kd_produk,
					produk.kd_produk,detail_transaksi.harga,
					detail_transaksi.flag,
					detail_transaksi.qty,
					detail_transaksi.tgl_berlaku,
					detail_transaksi.kd_dokter,
					detail_transaksi.adjust,
					detail_transaksi.cito,
					detail_transaksi.kd_kasir,
					detail_transaksi.kd_customer,
					detail_transaksi.tgl_transaksi,
					detail_transaksi.kd_tarif
				) as  resdata where kd_kasir = '".$kd_kasir."' and no_transaksi = '".$no_transaksi."'
				");
			$result = $cekdetail->result();
			$urut = 0;
			foreach ($result as $data)
			{
				$urut ++;
				$tmpjumlah = $data->harga;
				$jumlah = ((int)$tmpjumlah * (float)$persen) / 100;
				$savepg = $this->db->query("select insertpembayaran('$kd_kasir','".$no_transaksi."',
					".$urut.",'".$tgl_bayar."',".$kd_user.",'".$kd_unit."','".$kd_pay."',".$jumlah.",'',".$shift.",'TRUE','".$tgl_bayar."',".$urut_detailbayar.")");
				$savesql = $this->dbSQL->query("exec dbo.V5_insertpembayaran '$kd_kasir','".$no_transaksi."',".$urut.",
					'".$tgl_bayar."',".$kd_user.",'".$kd_unit."','".$kd_pay."',".$jumlah.",'',".$shift.",0,'".$tgl_bayar."',".$urut_detailbayar."");
				
			}

			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			echo "{success:true}";
		} else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			echo "{success:false}";
		}				
	}

	/*------------------------------------- Akhir Proses Discont -------------------------------------*/

	/*------------------------------------- Proses Posting Manual ------------------------------------
		Oleh 	: HDHT
		Tanggal : 15 maret 2017
		Tempat 	: Bandung
		Fungsi	: Proses Posting Manual
	*/
	function savePostingManual(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$resultSQL = false;
		$resultPG  = false;
		$response  = array();
		$paramsInsert  		= array();
		$criteriaParams  	= array();
		$no_transaksi = $this->input->post('no_transaksi');
		$kd_kasir     = $this->input->post('kd_kasir');
		$tgl_bayar    = date('Y-m-d 00:00:00');
		$kd_produk    = $this->input->post('KODE');
		$jumlah       = $this->input->post('jumlah');
		$list         = json_decode($_POST['list']);
		$kd_user      = $this->session->userdata['user_id']['id'];
		$faktur       = $this->input->post('faktur');
		$kd_unit      = $this->input->post('unit');
		$Shift        = $this->db->query("select shift from bagian_shift where kd_bagian='1'")->row()->shift;

		$params = array(
			'no_transaksi' => $this->input->post('no_transaksi'),
			'kd_kasir'     => $this->input->post('kd_kasir'),
			'tgl_bayar'    => date('Y-m-d 00:00:00'),
			'kd_produk'    => $this->input->post('KODE'),
			'jumlah'       => $this->input->post('jumlah'),
			'list'         => json_decode($_POST['list']),
			'kd_user'      => $this->session->userdata['user_id']['id'],
			'faktur'       => $this->input->post('faktur'),
			'kd_unit'      => $this->input->post('unit'),
			'shift'        => $this->db->query("select shift from bagian_shift where kd_bagian='1'")->row()->shift,
		);

			$urutdetailtransaksi = $this->db->query("select geturutdetailtransaksi('".$params['kd_kasir']."','".$params['no_transaksi']."','".$params['tgl_bayar']."') as urutdetail")->row()->urutdetail;
			$tglberlaku          = $this->db->query("select tgl_berlaku from tarif where kd_produk = '".$params['kd_produk']."' and kd_tarif = 'TU' and kd_unit = '1'")->row()->tgl_berlaku;
			

			$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'urut' 			=> $urutdetailtransaksi,
					'tgl_transaksi' => $params['tgl_bayar'],
					'kd_user' 		=> $params['kd_user'],
					'kd_tarif' 		=> 'TU',
					'kd_produk' 	=> $params['kd_produk'],
					'kd_unit' 		=> '1',
					'tgl_berlaku' 	=> $tglberlaku,
					'charge' 		=> 'f',
					'adjust' 		=> 'f',
					'folio' 		=> 'E',
					'qty' 			=> 1,
					'harga' 		=> $params['jumlah'],
					'shift' 		=> $params['shift'],
					'kd_unit_tr' 	=> $params['kd_unit'],
					'no_faktur' 	=> $params['faktur'],
					'tag' 			=> 'f',
			);

			$resultPG 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
			$paramsInsert['charge'] 	= 0;
			$paramsInsert['adjust'] 	= 0;
			$paramsInsert['tag'] 		= 0;
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi_SQL($paramsInsert);
			}else{
				$resultSQL 	= true;
			}

			$response['tahap'] = "Detail transaksi";


			if (($resultPG>0 || $resultPG===true) && ($resultSQL>0 || $resultSQL === true)) {
				for($i=0;$i<count($list);$i++){
					$kd_component = $list[$i]->KD_COMPONENT;
					$harga        = $list[$i]->TARIF;
					
					unset($paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $urutdetailtransaksi,
						'tgl_transaksi' => $params['tgl_bayar'],
						'kd_component' 	=> $kd_component,
						'tarif' 		=> $harga,
					);

					$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						$resultSQL = $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
					}else{
						$resultSQL = true;
					}
					$response['tahap'] = "Detail compnent";

					if (($resultPG == 0 || $resultPG === false) && ($resultSQL == 0 || $resultSQL === false)) {
						$resultPG 	= false;
						$resultSQL 	= false;
						break;
					}
				}
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}

			if (($resultPG>0 || $resultPG===true) && ($resultSQL>0 || $resultSQL === true)) {
				unset($paramsInsert);
				unset($criteriaParams);
				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
				);

				$getDatakamar   = $this->Tbl_data_pasien_inap->selectPasienInap($criteriaParams);
				$paramsInsert = array(
					'kd_kasir' 		=> $params['kd_kasir'],
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date('Y-m-d',strtotime(str_replace('/','-', $params['tgl_bayar']))),
					'urut' 			=> $urutdetailtransaksi,
					'kd_unit' 		=> $getDatakamar->row()->kd_unit,
					'no_kamar' 		=> $getDatakamar->row()->no_kamar,
					'kd_spesial' 	=> $getDatakamar->row()->kd_spesial
				);
				$resultPG 	= $this->Tbl_data_detail_tr_kamar->insert($paramsInsert);
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$resultSQL	= $this->Tbl_data_detail_tr_kamar->insert_SQL($paramsInsert);
				}else{
					$resultSQL	= true;
				}
			}else{
				$resultPG 	= false;
				$resultSQL	= false;
			}

		if (($resultPG>0 || $resultPG===true) && ($resultSQL>0 || $resultSQL === true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['status'] = false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
					
	}




	/*------------------------------------- Akhir Proses Posting Manual-------------------------------*/

	/* 
		CREATE	: MSD
		TGL		: 16-03-2017
		KET		: REKAPITULASI TINDAKAN RWI

	*/
	
	function getRekapitulasiTindakan(){
		/*$result = $this->db->query("select  detail_transaksi.kd_produk, detail_transaksi.kd_unit,sum(detail_transaksi.harga*detail_transaksi.qty) as harga, sum(detail_transaksi.qty) as qty, detail_transaksi.no_faktur, produk.deskripsi
									from  detail_transaksi 
										inner join produk on detail_transaksi.kd_produk = produk.kd_produk 
										inner join unit on detail_transaksi.kd_unit = unit.kd_unit 
										left join (select count(visite_dokter.kd_dokter) as jumlah_dokter,no_transaksi,urut,tgl_transaksi,kd_kasir 
												from visite_dokter 
												group by no_transaksi,urut,tgl_transaksi,kd_kasir ) as d ON d.no_transaksi = detail_transaksi.no_transaksi 
											AND d.kd_kasir = detail_transaksi.kd_kasir AND d.urut = detail_transaksi.urut AND d.tgl_transaksi = detail_transaksi.tgl_transaksi
										left join customer on detail_transaksi.kd_customer = customer.kd_customer 
										left  join dokter on detail_transaksi.kd_dokter = dokter.kd_dokter
										inner join zusers on detail_transaksi.kd_user::character varying=zusers.kd_user
									where detail_transaksi.no_transaksi='".$_POST['no_transaksi']."' and detail_transaksi.kd_kasir='".$_POST['kd_kasir']."'
									group by detail_transaksi.kd_produk, detail_transaksi.kd_unit, detail_transaksi.no_faktur, produk.deskripsi
									order by produk.deskripsi
									")->result();*/
		$result = $this->db->query("SELECT * FROM getallbillkasirrwi('".$this->input->post('kd_kasir')."', '".$this->input->post('no_transaksi')."', ".$this->input->post('co_status').")");
		$response = array();
		$x = 0;
 		//kd_produk kd_unit harga qty no_faktur deskripsi
		foreach ($result->result() as $data) {
			$response[$x]['kd_produk'] 	= $this->db->query("SELECT kd_produk, kp_produk FROM produk WHERE kd_produk = '".$data->kd_produk."'")->row()->kp_produk;
			$response[$x]['deskripsi'] 	= $data->deskripsi;
			$response[$x]['kd_unit'] 	= $data->kd_unit;
			$response[$x]['qty'] 		= $data->qty;
			$response[$x]['harga'] 		= (int)$data->tarif*(int)$data->qty;
			$response[$x]['no_faktur'] 	= $data->no_faktur;
			$x++;
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($response).'}';
	}
	/* ========================= AKHIR REKAPITULASI ========================================== */

	function getdatakunjunganpasien(){
		$criteriagetDatakunjunganpasien['kd_pasien'] 	= $_POST['kd_pasien'];
		$criteriagetDatakunjunganpasien['kd_unit']		= $_POST['kd_unit'];
		$criteriagetDatakunjunganpasien['tgl_masuk']	= $_POST['tgl_masuk'];
		$getDatakunjunganpasien  = $this->M_produk->getDatakunjunganpasien($criteriagetDatakunjunganpasien);
		echo "{success: true, jam :'".$getDatakunjunganpasien->row()->jam_masuk."'}";
	}

	/*
		PERBARUAN UPDATE PASIEN PULANG 
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017 - 04 - 06
		ALASAN 	: UPDATE PASIEN PULANG SEBELUMNYA MASIH ADA ERROR DAN BELUM NGEUPDATE QUANTITY DIGUNAKAN PADA TABLE KAMAR 
	 */
	function UpdatePasienPulangKasirRWI(){
		$response = array();
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$criteriakunjungan= array(
			'kd_pasien'     => $_POST['kd_pasien'],
			'kd_unit' 		=> $_POST['kd_unit'],
			'tgl_masuk'		=> $_POST['tgl_masuk'],
			'urut_masuk'	=> $_POST['urut_masuk']
		);

		$dataupdatekunjungan = array(
			'tgl_keluar'     => $_POST['tglkeluar'],
			'jam_keluar' 	 => '1900-01-01 '.date_format(date_create($_POST['jamkeluar']), 'H:i:s'),
		);

		$criteriaParams = array(
			'kd_kasir' 		=> $_POST['kd_kasir'],
			'no_transaksi' 	=> $_POST['notrans'],
		);

		$queryTransaksi = $this->Tbl_data_transaksi->getTransaksi($criteriaParams);
		$response['tahap'] = 'Cek data transaksi';

		/*
			================================================================================================================================================= 
			====================================================== VALIDASI PULANG DENGAN STATUS LUNAS DAN BALANCE =========================================== 
			================================================================================================================================================= 
		 */
		$query_tagihan    = $this->db->query("SELECT * from gettagihan('".$criteriaParams['kd_kasir']."', '".$criteriaParams['no_transaksi']."')");
		$query_pembayaran = $this->db->query("SELECT * from getpembayaran('".$criteriaParams['kd_kasir']."', '".$criteriaParams['no_transaksi']."')");
		$tagihan    = 0;
		$pembayaran = 0;
		if ($query_tagihan->num_rows() > 0) {
			$tagihan    = $query_tagihan->row()->gettagihan;
		}
		if ($query_pembayaran->num_rows() > 0) {
			$pembayaran    = $query_pembayaran->row()->getpembayaran;
		}

		if (((int)$tagihan == (int)$pembayaran) && $queryTransaksi->row()->lunas === "t") {
			$query 		= true;
			$querysql 	= true;
			$response['status_lunas'] = true; 
		}else{
			$query 		= false;
			$querysql 	= false;
			$response['status_lunas'] = false;
		}
		/*
			================================================================================================================================================= 
			====================================================== VALIDASI PULANG DENGAN STATUS LUNAS DAN BALANCE ========================================== 
			================================================================================================================================================= 
		 */
		
		if(($query > 0 || $query === true) && ($querysql > 0 || $querysql === true)){
			$getDatakamar   = $this->Tbl_data_pasien_inap->selectPasienInap($criteriaParams);

			$query    = $this->Tbl_data_kunjungan->update($criteriakunjungan, $dataupdatekunjungan);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$querysql = $this->Tbl_data_kunjungan->updateSql($criteriakunjungan, $dataupdatekunjungan);
			}else{
				$querysql = true;
			}
			$response['tahap'] = 'Update data kunjungan';

			$response['status_postgre'] = $query; 
			$response['status_SQL']     = $querysql; 
		}else{
			$query 		= false;
			$querysql 	= false;
		}
		
		if(($query > 0 || $query === true) && ($querysql > 0 || $querysql === true)){
			$criteriaTransaksi= array(
			'kd_kasir'      => $_POST['kd_kasir'],
			'no_transaksi'  => $_POST['notrans'],
			'kd_pasien'     => $_POST['kd_pasien'],
			'kd_unit' 		=> $_POST['kd_unit'],
			'tgl_transaksi'	=> $_POST['tgl_masuk'],
			'urut_masuk'	=> $_POST['urut_masuk'],
			);

			$dataupdateTransaksi = array(
				'tgl_co'     => $_POST['tglkeluar'],
				'co_status'  => 'true',
				'ispay'  	 => 'true',
			);
			$query = $this->Tbl_data_transaksi->updateTransaksi($criteriaTransaksi, $dataupdateTransaksi);
			$dataupdateTransaksi['co_status']	= 1;
			$dataupdateTransaksi['ispay']		= 1;
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$querysql = $this->Tbl_data_transaksi->updateTransaksi_SQL($criteriaTransaksi, $dataupdateTransaksi);
			}else{
				$querysql = true;
			}
			$response['tahap'] = 'Update data transaksi';

			$response['status_postgre'] = $query; 
			$response['status_SQL'] = $querysql; 
		}else{
			$query    = false;
			$querysql = false;
		}

		if(($query > 0 || $query === true) && ($querysql > 0 || $querysql === true)){
			unset($criteriaParams);
			$criteriaParams = array(
				'no_kamar'     	=> $getDatakamar->row()->no_kamar,
				'kd_unit'		=> $getDatakamar->row()->kd_unit,
			);
			$dataKamar  = $this->Tbl_data_kamar->select($criteriaParams);
			$paramsUpdate = array(
				'digunakan' 	=> (int)$dataKamar->row()->digunakan - 1,
			);
			$query    = $this->Tbl_data_kamar->update($criteriaParams, $paramsUpdate);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$querysql = $this->Tbl_data_kamar->update_SQL($criteriaParams, $paramsUpdate);
			}else{
				$querysql = true;
			}
			$response['tahap'] = 'Update data kamar';

			$response['status_postgre'] = $query; 
			$response['status_SQL'] = $querysql; 
		}else{
			$query    = false;
			$querysql = false;
		}

		if(($query > 0 || $query === true) && ($querysql > 0 || $querysql === true)){
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien'     => $_POST['kd_pasien'],
				'kd_unit_kamar'	=> $getDatakamar->row()->kd_unit,
				'tgl_masuk'		=> $_POST['tgl_masuk'],
				'urut_masuk'	=> $_POST['urut_masuk'],
			);
			$query    = $this->Tbl_data_nginap->updateNginap($criteriaParams, $dataupdatekunjungan);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$querysql = $this->Tbl_data_nginap->updateNginap_SQL($criteriaParams, $dataupdatekunjungan);
			}else{
				$querysql = true;
			}
			$response['tahap'] = 'Update data nginap';

			$response['status_postgre'] = $query; 
			$response['status_SQL'] = $querysql; 
		}else{
			$query    = false;
			$querysql = false;
		}

		if(($query > 0 || $query === true) && ($querysql > 0 || $querysql === true)){
			unset($criteriaParams);
			$criteriaParams= array(
				'kd_kasir'    	=> $_POST['kd_kasir'],
				'no_transaksi'  => $_POST['notrans'],
				'kd_unit' 		=> $getDatakamar->row()->kd_unit,
			);
			$query    = $this->Tbl_data_pasien_inap->deletePasienInap($criteriaParams);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$querysql = $this->Tbl_data_pasien_inap->deletePasienInap_SQL($criteriaParams);
			}else{
				$querysql = true;
			}
			$response['tahap'] = 'Update data pasien inap';

			$response['status_postgre'] = $query; 
			$response['status_SQL'] = $querysql; 
		}else{
			$query    = false;
			$querysql = false;
		}

		if(($query > 0 || $query === true) && ($querysql > 0 || $querysql === true)){
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['status'] = false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}
	/*
		CEK NOMINAL 
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017 - 04 - 07
		ALASAN 	: UNTUK MENGISI FORM KETERANGAN NOMINAL YANG SUDAH DI BAYAR DAN SISA
	 */
	
	function cek_cominal_pembayaran(){
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;

		$criteriaParams = array(
			'field_select' 		=> 'SUM(jumlah) as jumlah',
			//'value_criteria' 	=> ,
			'field_criteria' 	=> array(
				'kd_kasir' 		=> $this->input->post('kd_kasir'),
				'no_transaksi' 	=> $this->input->post('no_transaksi'),
			),
			'table' 			=> 'detail_bayar',
			//'field_return' 		=> 'jumlah',
		);
		$detail_bayar = $this->Tbl_data_detail_bayar->getCustom($criteriaParams);
		if ($detail_bayar->row()->jumlah != 0 || $detail_bayar->row()->jumlah != '') {
			$response['telah_membayar'] = $detail_bayar->row()->jumlah;
		}else{
			$response['telah_membayar'] = 0;
		}

		unset($criteriaParams);
		$criteriaParams = array(
			'field_select' 		=> 'SUM(harga*qty) as jumlah',
			//'value_criteria' 	=> ,
			'field_criteria' 	=> array(
				'kd_kasir' 		=> $this->input->post('kd_kasir'),
				'no_transaksi' 	=> $this->input->post('no_transaksi'),
			),
			'table' 			=> 'detail_transaksi',
			//'field_return' 		=> 'jumlah',
		);
		$detail_transaksi = $this->Tbl_data_detail_bayar->getCustom($criteriaParams);
		if ($detail_transaksi->num_rows() > 0) {
			$response['harus_membayar'] = $detail_transaksi->row()->jumlah;
		}else{
			$response['harus_membayar'] = 0;
		}
		echo json_encode($response);
	}

	/*
		CEK TINDAKAN TRANSFER 
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017 - 04 - 07
		ALASAN 	: UNTUK MENGISI MENGISI DETAIL TRANSAKSI DARI SQL KE POSTGRE
	 */
	
	
	function cek_transfer_tindakan(){
		$this->dbSQL->trans_commit();
		$this->db->trans_commit();
		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		$params = array(
			'kd_kasir' 		=> $this->input->post('kd_kasir'),	
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),		
		);

		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],	
			'no_transaksi' 	=> $params['no_transaksi'],
			//'tgl_transaksi1' => $params['tgl_transaksi'],
		);

		$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);
		foreach ($querySQL->result() as $data) {

			$criteriaTarif = array(
				'kd_tarif' 		=> $data->KD_TARIF,
				'kd_produk' 	=> $data->KD_PRODUK,
				'kd_unit'	 	=> $data->KD_UNIT,
				'tgl_berlaku' 	=> date_format(date_create($data->TGL_BERLAKU), 'Y-m-d'),
			);
			
			$queryTarif = $this->Tbl_data_tarif->getTarif($criteriaTarif);
			if ($queryTarif->num_rows() == 0) {
				unset($queryTarif);
				$queryTarif = $this->Tbl_data_tarif->getTarif_SQL($criteriaTarif);
				foreach ($queryTarif->result() as $result) {
					$paramsInsertTarif = array(
						'kd_tarif' 		=> $data->KD_TARIF,
						'kd_produk' 	=> $data->KD_PRODUK,
						'kd_unit'	 	=> $data->KD_UNIT,
						'tgl_berlaku' 	=> date_format(date_create($data->TGL_BERLAKU), 'Y-m-d'),
						'tarif'	 		=> $result->TARIF,
						'tgl_berakhir'	=> $result->TGL_BERAKHIR,
					);
					$resultPG = $this->Tbl_data_tarif->insert($paramsInsertTarif);
				}
			}

			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],	
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
				'urut' 			=> $data->urut,
			);

			
			unset($queryPG);
			$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
			if ($queryPG->num_rows() == 0) {
				unset($paramsInsert);
				$paramsInsert = array(
					'kd_kasir' 		=> $data->KD_KASIR,
					'no_transaksi' 	=> $data->NO_TRANSAKSI,
					'urut' 			=> $data->URUT,
					'tgl_transaksi' => date_format(date_create($data->TGL_TRANSAKSI), 'Y-m-d'),
					'kd_user' 		=> $data->KD_USER,
					'kd_tarif' 		=> $data->KD_TARIF,
					'kd_produk' 	=> $data->KD_PRODUK,
					'kd_unit' 		=> $data->KD_UNIT,
					'tgl_berlaku' 	=> date_format(date_create($data->TGL_BERLAKU), 'Y-m-d'),
					//'charge' 		=> $data->CHARGE,
					//'adjust' 		=> $data->ADJUST,
					'folio' 		=> $data->FOLIO,
					'qty' 			=> $data->QTY,
					'harga' 		=> $data->HARGA,
					'shift' 		=> $data->SHIFT,
					'kd_dokter' 	=> $data->KD_DOKTER,
					'kd_unit_tr' 	=> $data->KD_UNIT_TR,
					'cito' 			=> $data->CITO,
					'js' 			=> $data->JS,
					'jp' 			=> $data->JP,
					'no_faktur' 	=> $data->NO_FAKTUR,
				);

				if ($data->CHARGE == '0' || $data->CHARGE == 0) {
					$paramsInsert['charge'] = 'false';
				}else{
					$paramsInsert['charge'] = 'true';
				}

				if ($data->ADJUST == '0' || $data->ADJUST == 0) {
					$paramsInsert['adjust'] = 'false';
				}else{
					$paramsInsert['adjust'] = 'true';
				}

				$resultPG = $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
				if ($resultPG !== true || $resultPG == 0) {
					$resultPG 	= false;
					$resultSQL 	= false;
					break;
				}else{
					$resultSQL 	= true;
					//$resultPG 
				}
			}else{
				$resultPG 	= true;
				$resultSQL 	= true;
			}

			$queryPG = $this->Tbl_data_visite_dokter->get($criteriaParams);
			if ($queryPG->num_rows() == 0) {
				$querySQL = $this->Tbl_data_visite_dokter->get_SQL($criteriaParams);
				foreach ($querySQL->result() as $result) {
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],	
						'no_transaksi' 	=> $params['no_transaksi'],
						'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
						'urut' 			=> $data->urut,
						'line' 			=> $result->LINE,
						'kd_dokter'		=> $result->KD_DOKTER,
						'kd_unit'		=> $result->KD_UNIT,
						'tag_int'		=> $result->TAG_INT,
						'tag_char'		=> $result->TAG_CHAR,
						'jp'			=> $result->JP,
						'kd_job'		=> $result->KD_JOB,
						'prc'			=> $result->PRC,
					);

					$resultPG = $this->Tbl_data_visite_dokter->insert($paramsInsert);
				}
			}else{
				$resultPG 	= true;
				$resultSQL 	= true;
			}
			
			if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
				unset($criteriaParams);
				unset($queryPG);
				unset($querySQL);

				$criteriaParams = array(
					'kd_kasir' 		=> $params['kd_kasir'],	
					'no_transaksi' 	=> $params['no_transaksi'],
					'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
					'urut' 			=> $data->urut,
				);

				$criteriaTarifComponent = array(
					'kd_tarif' 	=> $data->KD_TARIF,
					'kd_produk' => $data->KD_PRODUK,
					'kd_unit' 	=> $data->KD_UNIT,
				);
				$kd_component 	= $this->Tbl_data_tarif_component->get($criteriaTarifComponent);
				$querySQL 		= $this->Tbl_data_detail_component->getDetailComponent_SQL($criteriaParams);
				$queryPG 		= $this->Tbl_data_detail_component->getDetailComponent($criteriaParams);
				
				/*if ($kd_component->num_rows() > 0) {
					unset($paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $params['kd_kasir'],
						'tarif' 		=> $data->HARGA,
						'no_transaksi' 	=> $params['no_transaksi'],
						'urut' 			=> $data->urut,
						'tgl_transaksi' => date_format(date_create($data->tgl_transaksi), 'Y-m-d'),
						'disc' 			=> 0,
						'markup' 		=> 0,
						'kd_component' 	=> $kd_component->row()->kd_component,
					);

					if ($querySQL->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->KD_COMPONENT;
						// $resultSQL = $this->Tbl_data_detail_component->insertComponent_SQL($paramsInsert);
						$resultSQL = true;
					}else{
						$resultSQL = true;
					}

					if ($queryPG->num_rows() === 0) {
						//$paramsInsert['kd_component'] = $kd_component->row()->kd_component;
						$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);
					}else{
						$resultPG = true;
					}

				}*/
				if ($querySQL->num_rows() > 0) {
					foreach ($querySQL->result() as $data_component) {
						unset($criteriaTarifComponent);
						$criteriaTarifComponent = array(
							'kd_kasir' 		=> $params['kd_kasir'],
							'kd_component' 	=> $data_component->KD_COMPONENT,
							'tgl_transaksi' => date_format(date_create($data_component->TGL_TRANSAKSI), 'Y-m-d'),
							'no_transaksi' 	=> $params['no_transaksi'],
							'urut' 			=> $data_component->URUT,
						);
						$queryPG 		= $this->Tbl_data_detail_component->getDetailComponent($criteriaTarifComponent);

						if ($queryPG->num_rows() == 0) {
							unset($paramsInsert);
							$paramsInsert = array(
								'kd_kasir' 		=> $params['kd_kasir'],
								'tarif' 		=> $data_component->TARIF,
								'no_transaksi' 	=> $params['no_transaksi'],
								'urut' 			=> $data_component->URUT,
								'tgl_transaksi' => date_format(date_create($data_component->TGL_TRANSAKSI), 'Y-m-d'),
								'disc' 			=> $data_component->Disc,
								'markup' 		=> $data_component->Markup,
								'kd_component' 	=> $data_component->KD_COMPONENT,
							);
							$resultPG = $this->Tbl_data_detail_component->insertComponent($paramsInsert);

							if ($resultPG != true || $resultPG == 0) {
								$resultPG 	= false;
								$resultSQL 	= false;
								break;
							}
						}
					}
				}
			}else{
				$resultPG 	= false;
				$resultSQL 	= false;
			}
		}

		/*
			SINKRONISASI HAPUS TINDAKAN KE POSTGRE
			============================================================================================================================
		 */
		
		/*unset($criteriaParams);
		unset($queryPG);
		unset($querySQL);
		$criteriaParams = array(
			'kd_kasir' 		=> $params['kd_kasir'],	
			'no_transaksi' 	=> $params['no_transaksi'],
			//'tgl_transaksi' => $params['tgl_transaksi'],
		);

		$queryPG = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaParams);
		foreach ($queryPG->result() as $data) {
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $params['kd_kasir'],	
				'no_transaksi' 	=> $params['no_transaksi'],
				'tgl_transaksi' => $data->tgl_transaksi,
				'urut' 			=> $data->urut,
			);
			$querySQL = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi_SQL($criteriaParams);

			if ($querySQL->num_rows() === 0 && (substr($data->kd_unit, 0, 2) != '72' || substr($data->kd_unit, 0, 2) != 72)) {
				// $resultPG = $this->Tbl_data_detail_transaksi->deleteDetailTransaksi($criteriaParams);
				$resultPG = true;

				if ($resultPG === true || $resultPG > 0) {
					$resultSQL = true;
				}else{
					break;
					$resultPG 	= false;
					$resultSQL 	= false;
				}
			}
		}*/

		/*
			SINKRONISASI HAPUS TINDAKAN KE POSTGRE
			============================================================================================================================
		 */
		if (($resultPG === true || $resultPG > 0) && ($resultSQL === true || $resultSQL > 0)) {
			$this->dbSQL->trans_commit();
			$this->db->trans_commit();
			$response['no_transaksi'] 	= $params['no_transaksi'];
			$response['status'] 		= true;
		}else{
			$this->dbSQL->trans_rollback();
			$this->db->trans_rollback();
			$response['status'] = false;
		}
		echo json_encode($response);
	}
	
	
	private function cekKomponen($kd_produk, $kd_tarif){
		$queryCekKomponent 			= $this->db->query("SELECT * FROM tarif_component WHERE kd_produk = '".$kd_produk."' and kd_tarif = '".$kd_tarif."' and kd_component in ('20', '21','27','28','29','31')");
		if ($queryCekKomponent->num_rows() > 0) {
			$result 			= true;
		}else{
			$result 			= false;
		}
		return $result;
	}

	public function list_laporan_lain_lain(){
		$response = array();
		$criteria = array(
			'aktif' 	=> 'true',
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("laporan_lain_lain_rwi");
		$result = $this->db->get();
		$response['data'] = $result->result();
		echo json_encode($response);
	}

	public function list_order_by_lain_lain(){
		$response = array();
		$criteria = array(
			'kd_sp' 	=> $this->input->post('kd_sp'),
		);
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("laporan_lain_lain_rwi");
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			$index 	= 0;
			$data 	= array();
			$tmp_column  = str_replace(" as col", "", str_replace(",", "", $result->row()->columns));
			$data_column = explode(" ",$tmp_column);

			for($i=0;$i<count($data_column);$i++){
				$data[$i]['columns'] = str_replace("_", " ", str_replace($i+1, "", $data_column[$i]));
			}
		}
		$response['data'] = $data;
		// die();
		// $response['data'] = $result->result();
		echo json_encode($response);
	}

	public function get_data_rujukan(){
		$response = array();
		$criteria = array(
			'kd_rujukan' 	=> $this->input->post('kd_rujukan'),
		);

		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from("rujukan");
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			$response['kd_rujukan'] = $result->row()->kd_rujukan;
			$response['rujukan']    = $result->row()->rujukan;
			$response['alamat']     = $result->row()->alamat;
			$response['kota']       = $result->row()->kota;

		}
		// die();
		// $response['data'] = $result->result();
		echo json_encode($response);
	}
	/*
		=======================================================================================
		PENAMBAHAN FUNGSI LOOKUP TINDAKAN
		OLEH 	: HADAD AL GOJALI
		TANGGAL : 2017-11-10
		=======================================================================================
	 */
	
	
	public function getLookUpProdukList(){
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$_POST['kd_customer']."'")->row()->kd_tarif;
		$kdjasadok  = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		$kdjasaanas = $this->db->query("SELECT setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		//$kd_unit 	= $_POST['kd_unit'];
		$kp_produk 	= '';
		$deskripsi 	= '';
		$kd_klas 	= $this->input->post('kd_klas');
		$tgl_now 	= date("Y-m-d");
		$q_unit 	= "";
		if (isset($_POST['kd_unit'])) {
			$q_unit = " left(tarif.kd_unit,1) ='".substr($_POST['kd_unit'],0,1)."' and ";
		}
		$q_customer = "";
		if (isset($_POST['kd_unit'])) {
			$q_customer = "  WHERE kd_customer='".$_POST['kd_customer']."' ";
		}

		$nama_unit = $this->db->query("SELECT nama_unit FROM unit where kd_unit = '".$_POST['kd_unit']."'")->row()->nama_unit;
    	/* $row 		= $this->db->query("SELECT kd_tarif from tarif_cust WHERE kd_customer='".$kd_customer."'")->row();
		$kd_tarif  	= $this->db->query("SELECT kd_tarif FROM tarif_cust WHERE kd_customer='".$kd_customer."'")->row()->kd_tarif; */
		$sql="SELECT 
			row_number() OVER () as rnum,
			rn.* 
			FROM (
			SELECT 
				1 as qty,
				u.kd_unit,
				p.kd_klas,
				u.kd_bagian, 
				u.kd_kelas, 
				u.nama_unit,
				p.kd_produk,
				p.kp_produk,
				p.deskripsi,
				t.kd_tarif,
				t.tgl_berlaku,
				t.tgl_berakhir,
				t.tarif,
				c.kd_customer,
				c.customer,
				row_number() over(partition by p.kd_produk order by t.tgl_berlaku desc) as rn 
				
			FROM 
				unit u 
				INNER JOIN produk_unit pu ON u.kd_unit = pu.kd_unit 
				INNER JOIN produk p ON p.kd_produk = pu.kd_produk 
				INNER JOIN tarif t ON t.kd_produk = p.kd_produk AND t.kd_unit = u.kd_unit
				INNER JOIN tarif_cust tc ON tc.kd_tarif = t.kd_tarif 
				INNER JOIN customer c ON c.kd_customer = tc.kd_customer 
				WHERE 
					u.kd_bagian = '".substr($this->input->post('kd_unit'), 0, 1)."'
					and u.kd_unit='".$this->input->post('kd_unit')."'
					AND (c.kd_customer = '".$this->input->post('kd_customer')."' OR t.kd_tarif='".$this->input->post('kd_tarif')."') 
					AND	p.kd_klas='".$this->input->post('kd_klas')."'
					AND t.tgl_berlaku < '".date('Y-m-d')."'
			) as rn 
		WHERE rn = 1
		ORDER BY rn.tgl_berlaku DESC";

    	$result = $this->db->query($sql);
		
		$data       = array();		
		$jsonResult = array();
		$i          = 0;
		foreach ($result->result_array() as $row){


			if (stripos(strtolower($row['deskripsi']), strtolower($deskripsi)) !== false) {
				$tmp_deskripsi            = $row['deskripsi'];
				$text                     = str_replace(strtolower($deskripsi), "<b>".strtolower($deskripsi)."</b>", strtolower($tmp_deskripsi));
				$data[$i]['kd_klas']      = $row['kd_klas'];
				$data[$i]['kd_produk']    = $row['kd_produk'];
				$data[$i]['deskripsi']    = strtoupper($text);
				$data[$i]['kp_produk']    = $row['kp_produk'];
				$data[$i]['kd_tarif']     = $row['kd_tarif'];
				$data[$i]['kd_unit']      = $row['kd_unit'];
				$data[$i]['nama_unit']    = $row['nama_unit'];
				$data[$i]['rn']           = $row['rn'];
				$data[$i]['rnum']         = $row['rnum'];
				$data[$i]['tarifx']       = $row['tarif'];
				$data[$i]['tgl_berlaku']  = $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] = $row['tgl_berakhir'];
				$text                     = strtolower($data[$i]['deskripsi']);
				$text                     = substr($data[$i]['kd_klas'],0,1);
				if ($text == '3' || $text=='6') {
					$data[$i]['status_konsultasi'] 	= true;
				}else{
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			}else if(stripos(strtolower($row['kp_produk']), strtolower($kp_produk)) !== false){
				$tmp_kd_produk 					= $row['kp_produk'];
				$text 							= str_replace(strtolower($kp_produk), "<b>".strtolower($kp_produk)."</b>", strtolower($tmp_kd_produk));
				$data[$i]['kp_produk'] 			= strtoupper($text);
				$data[$i]['kd_klas'] 			= $row['kd_klas'];
				$data[$i]['deskripsi']  		= $row['deskripsi'];
				$data[$i]['kd_produk'] 	 		= $row['kd_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = strtolower($data[$i]['deskripsi']);
				$text = substr($data[$i]['kd_klas'],0,1);
				if ($text == '3' || $text=='6') {
					$data[$i]['status_konsultasi'] 	= true;
				}else{
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			}else if ($kp_produk == "" && $deskripsi == "") {
				$data[$i]['deskripsi'] 			= $row['deskripsi'];
				$data[$i]['kd_klas'] 			= $row['kd_klas'];
				$data[$i]['kd_produk'] 	 		= $row['kd_produk'];
				$data[$i]['kp_produk'] 	 		= $row['kp_produk'];
				$data[$i]['kd_tarif'] 	 		= $row['kd_tarif'];
				$data[$i]['kd_unit'] 	 		= $row['kd_unit'];
				$data[$i]['nama_unit'] 	 		= $row['nama_unit'];
				$data[$i]['rn'] 	 			= $row['rn'];
				$data[$i]['rnum'] 	 			= $row['rnum'];
				$data[$i]['tarifx'] 	 		= $row['tarif'];
				$data[$i]['tgl_berlaku'] 		= $row['tgl_berlaku'];
				$data[$i]['tgl_berakhir'] 		= $row['tgl_berakhir'];
				$text = substr($data[$i]['kd_klas'],0,1);
				if ($text == '3' || $text=='6') {
					$data[$i]['status_konsultasi'] 	= true;
				}else{
					$data[$i]['status_konsultasi'] 	= false;
				}
				$i++;
			}
        }
    	$jsonResult['processResult'] 	= 'SUCCESS';
    	$jsonResult['data'] 			= $data;
    	echo json_encode($jsonResult);
    }


	private function childMasterProduk($klas_produks){
		$res=array();
		for($i=0,$iLen=count($klas_produks); $i <$iLen ; $i ++) {
			$klas_produk = $klas_produks[$i];
			$a           = array();
			$a['text']   = $klas_produk->klasifikasi;
			$a['id']     = $klas_produk->kd_klas;
			$childs      = $this->db->query("SELECT kd_klas,klasifikasi FROM klas_produk WHERE parent='".$klas_produk->kd_klas."'")->result();
			if(count($childs) > 0) {
				$a['children']=$this->childMasterProduk($childs);
				$a['expanded']=false;
				// $a['deleted']=true;
			}else{
				// $a['children']=array();
				$a['leaf'] = true;
				// $a['deleted']=false;
			}
			$res[]=$a;
		}
		return $res;
	}

	public function tree_master(){
		$response      = array();
		$klas_produks = $this->db->query("SELECT kd_klas,klasifikasi FROM klas_produk WHERE parent=''")->result();
		$tree=$this->childMasterProduk($klas_produks);
		$json=array();
		// $json['d']=$tree;
		$response['text']     = ".";
		$response['id']       = "";
		$response['children'] = $tree;
		echo json_encode($response['children']);
	}

	public function get_tarif_produk_bayi(){
		$response = array();
		$params = array(
			'produk.kd_produk' 	=> str_replace("'", "", $this->db->query("SELECT setting FROM sys_setting WHERE key_data = 'kd_transfer_rwi_bayi'")->row()->setting),
			'tarif.kd_tarif' 	=> $this->input->post('kd_tarif'),
			'tarif.kd_unit' 	=> $this->input->post('kd_unit'),
		);

		$this->db->select(" tarif.tgl_berlaku, produk.kd_klas, tarif.tarif , tarif.kd_produk ");
		$this->db->from("tarif");
		$this->db->where($params);
		$this->db->join("produk", " produk.kd_produk = tarif.kd_produk ", "INNER");
		$result = $this->db->get();

		if ($result->num_rows() > 0) {
			$response['status']       = true;
			$response['data']        = $result->result();
			$response['tgl_berlaku'] = $result->row()->tgl_berlaku;
			$response['kd_klas']     = $result->row()->kd_klas;
			$response['tarif']       = $result->row()->tarif;
			$response['kd_produk']   = $result->row()->kd_produk;
			// $response['urut']   	 = $this->db->query("SELECT max(urut) as urut from detail_transaksi where no_transaksi = '".$this->input->post('no_transaksi')."' and kd_kasir = '".$this->input->post('kd_kasir')."'")->row()->urut;

			$query_urut = $this->db->query("SELECT max(urut) as urut from detail_transaksi where no_transaksi = '".$this->input->post('no_transaksi')."' and kd_kasir = '".$this->input->post('kd_kasir')."'");
			if ($query_urut->num_rows() > 0) {
				$response['urut'] = $query_urut->row()->urut;
			}else{
				$response['urut'] = 1;
			}
		}else{
			$response['status']       = false;
		}
		echo json_encode($response);
	}

	public function insert_unit_transfer(){
		$response 	= array();
		$this->db->trans_begin();
		$query 		= true;

		$params = array(
			'kd_kasir'          => $this->input->post("kd_kasir"),
			'no_transaksi'      => $this->input->post("no_transaksi"),
			'kd_kasir_asal'     => $this->input->post("kd_kasir_asal"),
			'no_transaksi_asal' => $this->input->post("no_transaksi_asal"),
			'id_asal' 			=> 0,
		);
		$query = $this->db->insert("unit_asal", $params);
		if ($query === true || $query > 0) {
			$response['status'] = true;
			$this->db->trans_commit();
		}else{
			$response['status'] = false;
			$this->db->trans_rollback();
		}
		$this->db->close();
		echo json_encode($response);
	}

	public function insert_transfer_bayar(){
		$response 	= array();
		$this->db->trans_begin();
		$query 		= true;

		$params = array(
			'kd_kasir'          => $this->input->post("kd_kasir"),
			'no_transaksi'      => $this->input->post("no_transaksi"),
			'kd_kasir_asal'     => $this->input->post("kd_kasir_asal"),
			'no_transaksi_asal' => $this->input->post("no_transaksi_asal"),
			'id_asal' 			=> 0,
		);
		$query = $this->db->insert("unit_asal", $params);
		if ($query === true || $query > 0) {
			$response['status'] = true;
			$this->db->trans_commit();
		}else{
			$response['status'] = false;
			$this->db->trans_rollback();
		}
		$this->db->close();
		echo json_encode($response);
	}
}

?>