<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_lap extends  MX_Controller {		
	private $cw_medrec      = 10;
	private $cw_no_transaksi= 15;
	private $cw_nama_pasien = 25;
	private $cw_tanggal 	= 15;
	private $cw_nominal 	= 20;

	private $jam;
	private $dbSQL;
	private $kd_kasir;
	private $id_user;
    public $ErrLoginMsg='';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
		$this->load->model('Tbl_data_transaksi');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
		$this->id_user 	= $this->session->userdata['user_id']['id'];
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

	}

	public function lap_index_pekerjaan(){
		$html         = "";
		$nama_file    = "Lap. Index Pekerjaan";
		
		$param        = json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA		
		$tgl_awal     = $param->tgl_awal;
		$tgl_akhir    = $param->tgl_akhir;
		$kd_bagian    = $param->bagian;
		$type_file    = $param->type_file;
		$kd_pekerjaan = $param->pekerjaan;
		$criteria 		= "";

		if (strtolower($kd_pekerjaan) != 'semua' && strlen($kd_pekerjaan) > 0) {
			$criteria = " where kd_pekerjaan = '".(int)$kd_pekerjaan."' ";
		}
		// echo $kd_pekerjaan;die();
		$query = $this->db->query("SELECT * FROM query_index_pekerjaan('".date_format(date_create($tgl_awal),'Y-m-d')."', '".date_format(date_create($tgl_akhir),'Y-m-d')."', '".$kd_bagian."') ".$criteria);

		if ($query->num_rows() > 0) {
			if ($type_file == '0') {
				$html .= "<table width='100%' border='0' cellspacing='0'>";
				$html .= "<tr>";
				$html .= "<th>lap. Index Pekerjaan</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th>Periode : ".date_format(date_create($tgl_awal),'Y-m-d')." s/d ".date_format(date_create($tgl_akhir), 'Y-m-d')."</th>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "</br>";
				$html .= "</hr>";
				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
					$html .= "<th rowspan='2'>No</th>";
					$html .= "<th rowspan='2'>Pekerjaan</th>";
					$html .= "<th colspan='2'>Pasien Baru</th>";
					$html .= "<th colspan='2'>Pasien Lama</th>";
					$html .= "<th colspan='2'>Jumlah</th>";
					$html .= "<th rowspan='2'>Total</th>";
				$html .= "</tr>";
				
				$html .= "<tr>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
				$html .= "</tr>";
				$html .= "</thead>";

				$html .= "<tbody>";
				$no = 1;
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				foreach ($query->result() as $result) {
					$html .= "<tr>";
					$html .= "<td>".$no."</td>";
					$html .= "<td>".$result->pekerjaan."</td>";
					$html .= "<td>".$result->lbaru."</td>";
					$html .= "<td>".$result->pbaru."</td>";
					$html .= "<td>".$result->llama."</td>";
					$html .= "<td>".$result->plama."</td>";
					$html .= "<td>".$result->jumlahl."</td>";
					$html .= "<td>".$result->jumlahp."</td>";
					$html .= "<td>".((int)$result->jumlahp + (int)$result->jumlahl)."</td>";
					$html .= "</tr>";

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;

					$no++;
				}
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td>Total</td>";
				$html .= "<td>".$total_lbaru."</td>";
				$html .= "<td>".$total_pbaru."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_jumlahl."</td>";
				$html .= "<td>".$total_jumlahp."</td>";
				$html .= "<td>".$total_seluruh."</td>";
				$html .= "</tr>";
				$html .= "</tbody>";
				$html .= "</table>";
				$this->common->setPdf('L',$nama_file,$html);
			}else{
				$url         = "Doc Asset/format_laporan/".$nama_file.".xls";
				$baris       = 7;
				$no          = 1;
				
				$phpExcel    = new PHPExcel();
				$sharedStyle = new PHPExcel_Style();
				$phpExcel    = PHPExcel_IOFactory::load($url);
				$sheet       = $phpExcel->getActiveSheet();
				// $phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d'));
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal),'Y-m-d')." s/d ".date_format(date_create($tgl_akhir),'Y-m-d'));
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				foreach ($query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->pekerjaan);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->lbaru);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->pbaru);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->llama);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result->plama);
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->jumlahl);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->jumlahp);
					$phpExcel->getActiveSheet()->setCellValue('I'.$baris, ((int)$result->jumlahp + (int)$result->jumlahl));
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;
					$total_seluruh += ((int)$result->jumlahp + (int)$result->jumlahl);

					$no++;
					$baris++;
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "");
				$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_lbaru);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_pbaru);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_llama);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_plama);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jumlahl);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jumlahp);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_seluruh);
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $nama_file).'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}	
	}

	public function lap_index_pendidikan(){
		$html         = "";
		$nama_file    = "Lap. Index Pendidikan";
		
		$param        = json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA		
		$tgl_awal     = $param->tgl_awal;
		$tgl_akhir    = $param->tgl_akhir;
		$kd_bagian    = $param->bagian;
		$type_file    = $param->type_file;
		$kd_pendidikan= $param->pendidikan;
		$criteria 		= "";

		if (strtolower($kd_pendidikan) != 'semua' && strlen($kd_pendidikan) > 0) {
			$criteria = " where kd_pendidikan = '".(int)$kd_pendidikan."' ";
		}
		// echo $kd_pekerjaan;die();
		$query = $this->db->query("SELECT * FROM query_index_pendidikan('".date_format(date_create($tgl_awal),'Y-m-d')."', '".date_format(date_create($tgl_akhir),'Y-m-d')."', '".$kd_bagian."') ".$criteria);

		if ($query->num_rows() > 0) {
			if ($type_file == '0') {
				$html .= "<table width='100%' border='0' cellspacing='0'>";
				$html .= "<tr>";
				$html .= "<th align='center'>Lap. Index Pendidikan</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th align='center'>Periode : ".date_format(date_create($tgl_awal),'Y-m-d')." s/d ".date_format(date_create($tgl_akhir), 'Y-m-d')."</th>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
					$html .= "<th rowspan='2'>No</th>";
					$html .= "<th rowspan='2'>Pendidikan</th>";
					$html .= "<th colspan='2'>Pasien Baru</th>";
					$html .= "<th colspan='2'>Pasien Lama</th>";
					$html .= "<th colspan='2'>Jumlah</th>";
					$html .= "<th rowspan='2'>Total</th>";
				$html .= "</tr>";
				
				$html .= "<tr>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
				$html .= "</tr>";
				$html .= "</thead>";

				$html .= "<tbody>";
				$no = 1;
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				foreach ($query->result() as $result) {
					$html .= "<tr>";
					$html .= "<td>".$no."</td>";
					$html .= "<td>".$result->pendidikan."</td>";
					$html .= "<td>".$result->lbaru."</td>";
					$html .= "<td>".$result->pbaru."</td>";
					$html .= "<td>".$result->llama."</td>";
					$html .= "<td>".$result->plama."</td>";
					$html .= "<td>".$result->jumlahl."</td>";
					$html .= "<td>".$result->jumlahp."</td>";
					$html .= "<td>".((int)$result->jumlahp + (int)$result->jumlahl)."</td>";
					$html .= "</tr>";

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;
					$total_seluruh += ((int)$result->jumlahp + (int)$result->jumlahl);

					$no++;
				}
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td>Total</td>";
				$html .= "<td>".$total_lbaru."</td>";
				$html .= "<td>".$total_pbaru."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_jumlahl."</td>";
				$html .= "<td>".$total_jumlahp."</td>";
				$html .= "<td>".$total_seluruh."</td>";
				$html .= "</tr>";
				$html .= "</tbody>";
				$html .= "</table>";
				$this->common->setPdf('L',$nama_file,$html);
			}else{
				$url         = "Doc Asset/format_laporan/".$nama_file.".xls";
				$baris       = 7;
				$no          = 1;
				
				$phpExcel    = new PHPExcel();
				$sharedStyle = new PHPExcel_Style();
				$phpExcel    = PHPExcel_IOFactory::load($url);
				$sheet       = $phpExcel->getActiveSheet();
				// $phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d'));
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal),'Y-m-d')." s/d ".date_format(date_create($tgl_akhir),'Y-m-d'));
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				foreach ($query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->pendidikan);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->lbaru);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->pbaru);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->llama);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result->plama);
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->jumlahl);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->jumlahp);
					$phpExcel->getActiveSheet()->setCellValue('I'.$baris, ((int)$result->jumlahp + (int)$result->jumlahl));
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;
					$total_seluruh += ((int)$result->jumlahp + (int)$result->jumlahl);

					$no++;
					$baris++;
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "");
				$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_lbaru);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_pbaru);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_llama);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_plama);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jumlahl);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jumlahp);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_seluruh);
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $nama_file).'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}	
	}

	private function keterangan_tidak_ada_data(){
		echo "<h3>Tidak Ada Data</h3>";
	} 
}
?>