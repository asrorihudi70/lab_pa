<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class main extends  MX_Controller {		

    public $ErrLoginMsg='';
    public function __construct()
    {

            parent::__construct();
             $this->load->library('session');
    }
	 
    public function index()
    {

 
          $this->load->view('main/index',$data=array('controller'=>$this));

    }
    function Login()
    {
           
			
            $p1  =$this->input->post('UserName');
            if (strlen($p1)!==0) {
                $p2  =$this->input->post('Password');
                $this->load->model("userman/tblzusers");
                $this->db->where("user_names",$p1);
                //$this->db->where("password",md5($p2)); tidak bisa di compare dlm database ueey
               // $this->db->where("password",p2);
                $res = $this->tblzusers->GetRowList();
                if ($res[1]!=='0' )
                {
                    $cpwd=md5($p2);
                    if (trim($res[0][0]->PASSWORD)===$cpwd)
                    {
                        $data = array(
						'id' => $res[0][0]->KD_USER,
                       'username'  => $p1,
                       'logged_in'  => TRUE,
                        'logtime'  => time(),
						'currentshift' => $this->getcurrentshift()
						 );
                        // session_cache_expire(0.5);
                        $this->session->set_userdata( 'user_id',$data);
                        //
                        //$this->session->sess_expiration(0);
						//redirect('main/index');
                        //  $this->load->view('main/index');
                         $this->load->view('main/index',$data=array('controller'=>$this));
                    }
                    else
                    {
                        //$this->ErrLoginMsg="Password Tidak Dikenal";
                         //$this->load->view('main/index',$data=array('controller'=>$this));
						 $data['error'] = "Password Anda Tidak Tepat";
                         $this->load->view('main/index',$data=array('controller'=>$this,'error'=>'Maaf, Password Anda tidak benar'));
						 
                    }
                }
                else
                {
                      //redirect('main/index');
                     // $this->load->view('main/index');
                   // $this->ErrLoginMsg="Nama User Tidak Dikenal";
                     //$this->load->view('main/index',$data=array('controller'=>$this));
					  $data['error'] = "User Tidak Terdaftar";
                     $this->load->view('main/index',$data=array('controller'=>$this,'error'=>'Maaf, Username Anda Belum Terdaftar'));
                }
            }
             else {
                   // $this->ErrLoginMsg="Nama User dan password harus diisi";
                    //$this->load->view('main/index',$data=array('controller'=>$this));
					$data['error'] = "Nama User dan password harus diisi";
                    $this->load->view('main/index',$data=array('controller'=>$this,'error'=>'Nama User dan Password harus diisi'));
					
            }
    }

	public function val_currentship()
	{
	$res = $this->session->userdata['user_id'];
	var_dump($res);
	}
	
	public function getNameUser()
	{
	$res = $this->session->userdata['user_id']['username'];
	echo $res;	
	}

    public function getKdUser()
    {
    $res = $this->session->userdata['user_id']['id'];
    echo $res;  
    }
    
	
	function getkdbagian()
	{
	$query_kdbagian = $this->db->query("select bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Jalan'");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianrwj = $kdbag->kd_bagian;
			}
			return $kdbagianrwj;
	}
	
	function getMaxkdbagian()
	{
	if(isset($_POST['command']))
	{
	$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Jalan'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag)
			{
			$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
	}
	}
	
	   //function untuk mengambil nilai shift         
     function getcurrentshift()
    {   
	if(isset($_POST['command']))
	{
	  $kdbagianrwj = $this->getkdbagian();
	  $strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                $curentshift = $data->getcurrentshift;
				
             }
            }
         echo $curentshift;
	}
	else
	{
	$kdbagianrwj = $this->getkdbagian();
	$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                $curentshift = $data->getcurrentshift;
				
             }
            }
         return $curentshift;
	}
    }
    //----------------END------------------------
	
	
	
	
   function Logoff()
    {
          $logged = $this->session->userdata('user_id');
          if ($logged)
          {
              if ($logged['logtime']===0)
              {
                  $this->ErrLoginMsg="Anda terlalu lama tidak melakukan aktifitas , silakan login ulang untuk melanjutkan.";
              }
          }
         $this->session->unset_userdata('user_id');

         $this->load->view('main/index',$data=array('controller'=>$this));
     
    }
    // checking user logged user by registered session
    function _is_logged_in()
    {
        //return TRUE;
        
        $logged = $this->session->userdata('user_id');
        if ($logged)
        {
            
            $elapsed=time()-$logged['logtime'];
            if ($elapsed<=900)
            {
                $logged['logtime']=time();
                return true;
            }
       
            else
            {

                $logged['logtime']=0;
                return false;
            }
        }
        else
        {
           $logged['logtime']=0;
            return FALSE;
        }
    }


    private function CRUDLoadController($target, $Params, $method)
    {
        //format text pengenal;sintak controller(module/controller/method)

        $loadFile=file_get_contents( base_url()."ui/jslist/DataAccessList.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);

        foreach ($tambahdelimiter as $rowDA)
        {
            if (trim($rowDA)!='')
            {
                list($pengenal, $sintak)= explode(';',$rowDA);
                if (trim($target)==trim($pengenal))
                {
                    // load controller
                    list($mod, $ctrl)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    $this->$ctrl->$method($Params);
                }
            }
        }
				    	
    }

	
   public function KonsultasiPenataJasa()
   {

	   $kdTransaksi = $this->GetIdTransaksi();
	   $kdUnit = $_POST['KdUnit'];
	   $kdDokter = $_POST['KdDokter'];
	   $tglTransaksi = $_POST['TglTransaksi'];
	   $kdCostumer = $_POST['KDCustomer'];
	   $kdPasien = $_POST['KdPasien'];
	   $kdKasir = "01";
	   
	   if ($tglTransaksi!="" and $tglTransaksi !="undefined")
                {
                        list($tgl,$bln,$thn)= explode('/',$tglTransaksi,3);
                        $ctgl= strtotime($tgl.$bln.$thn);
                        $tgl_masuk=date("Y-m-d",$ctgl);
                    }
	   
	   $query = $this->db->query("select insertkonsultasi('".$kdPasien."','".$kdUnit."','".$tgl_masuk."','".$kdDokter."','".$kdCostumer."','".$kdKasir."','".$kdTransaksi."')");
	   $res = $query->result();
	   if($res)
	   {
		   echo '{success: true}';
	   }
	   else
	   {
		   echo '{success: false}';
	   }
   } 
   
   
    private function GetIdTransaksi()
    {
        $strNomor="0".str_pad("00",2,'0',STR_PAD_LEFT);
        $retVal=$strNomor."0001";

        $this->load->model('general/tb_transaksi');
        $this->tb_transaksi->db->where("substring(no_transaksi,1,3) = '".$strNomor."'", null, false);
        $res = $this->tb_transaksi->GetRowList( 0, 1, "DESC", "no_transaksi",  "");

        if ($res[1]>0)
        {
            $nm = substr($res[0][0]->NO_TRANSAKSI, -4);
            $nomor = (int) $nm +1;
            $retVal=$strNomor.str_pad($nomor,4,"00000",STR_PAD_LEFT);
    }
     return $retVal;
    }
	
	public function insertloop()
	{
		$queryy = $this->db->query("
		insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
		select t.kd_kasir,
		t.no_transaksi,
		dt.urut,
		dt.tgl_transaksi,
		tc.kd_component,
		tc.tarif
		from tarif_component tc 
		inner join detail_transaksi dt on 
		tc.kd_tarif = dt.kd_tarif
		inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
		and t.kd_unit = tc.kd_unit
		where tc.kd_produk= '112' and tc.kd_unit='202' and dt.tgl_transaksi in ('2015-02-09') and tc.kd_tarif='TU' 
		and  tc.tgl_berlaku in ('2014-03-01') 
		and t.kd_kasir = '01' and t.no_transaksi = '0000007'");
		return $queryy;
	}
	
	public function masukcobaan()
	{
		//$this->load->model('test','',TRUE);
			$query = $this->db->query("
		insert into detail_component (kd_kasir,no_transaksi,urut,tgl_transaksi,kd_component,tarif)
		select t.kd_kasir,
		t.no_transaksi,
		dt.urut,
		dt.tgl_transaksi,
		tc.kd_component,
		tc.tarif
		from tarif_component tc 
		inner join detail_transaksi dt on 
		tc.kd_tarif = dt.kd_tarif
		inner join transaksi t on t.kd_kasir  = dt.kd_kasir and t.no_transaksi = dt.no_transaksi
		and t.kd_unit = tc.kd_unit
		where tc.kd_produk= '112' and tc.kd_unit='202' and dt.tgl_transaksi in ('2015-02-09') and tc.kd_tarif='TU' 
		and  tc.tgl_berlaku in ('2014-03-01') 
		and t.kd_kasir = '01' and t.no_transaksi = '0000007'");
		if($query)
		{
			echo "sukses";
		}
		else
		{
			echo "gagal";
		}
	}
	
    public function CreateDataObj()
    {
            if ($this->IsAjaxRequest())
            {
                $arrPost=array();

                foreach($_POST as $key=>$value)
                {
                    $arrPost[$key]= $value;
                }

                if (count($arrPost)>0)
                {
                    
                    $target = $arrPost['Table'];

                    if ($target!="")
                    {
                        
                        $this->CRUDLoadController($target,$arrPost,'save');

                    }  else  $this->load->view('main/index');

                }   else $this->load->view('main/index');

            } else $this->load->view('main/index');
//       }
//       else
//       {
//           $this->Logoff();
//
//        }
    }

    public function ReadDataUmur()
    {
            if ($this->IsAjaxRequest())
            {
                $arrPost=array();

                foreach($_POST as $key=>$value)
                {
                    $arrPost[$key]= $value;
                }

                if (count($arrPost)>0)
                {

                    $target = $arrPost['Table'];

                    if ($target!="")
                    {

                        $this->CRUDLoadController($target,$arrPost,'umur');

                    }  else  $this->load->view('main/index');

                }   else $this->load->view('main/index');

            } else $this->load->view('main/index');
//       }
//       else
//       {
//           $this->Logoff();
//
//        }
    }

    public function DeleteDataObj()
    {
//       if  ($this->_is_logged_in()===false)
//       {
           
       
        if ($this->IsAjaxRequest())
        {

            $arrPost=array();

            foreach($_POST as $key=>$value)
            {
                $arrPost[$key]= $value;
            }

            if (count($arrPost)>0)
            {
                $target = $arrPost['Table'];

                if ($target!="")
                {

                    $this->CRUDLoadController($target,$arrPost,'delete');

                } else $this->load->view('main/index');

            } else $this->load->view('main/index');

        } else $this->load->view('main/index');
//        }
//       else
//       {$this->Logoff();}
    }
	    
    public function ReadDataObj()
    {
 
        if ($this->IsAjaxRequest())
        {
            $Skip = isset($_REQUEST['Skip']) ? $_REQUEST['Skip'] : 0;
            $Take = isset($_REQUEST['Take']) ? $_REQUEST['Take'] : 1000;
            $Sort = isset($_REQUEST['Sort']) ? $_REQUEST['Sort'] : "";
            $Sortdir = isset($_REQUEST['Sortdir']) ? $_REQUEST['Sortdir'] : "ASC";
            $target = isset($_REQUEST['target']) ? $_REQUEST['target'] : "";
            $param = isset($_REQUEST['param']) ? $_REQUEST['param'] : "";

            $Params = array((int)$Skip,(int)$Take,$Sort,$Sortdir,$param);

            if ($target=="")
            {
                    $this->load->view('main/index');
            } else $this->CRUDLoadController($target,$Params,'read');

        } else $this->load->view('main/index');

    }

    public function ReadData()
    {
            if ($this->IsAjaxRequest())
            {
                $arrPost=array();

                foreach($_POST as $key=>$value)
                {
                    $arrPost[$key]= $value;
                }

                if (count($arrPost)>0)
                {

                    $target = $arrPost['Table'];

                    if ($target!="")
                    {

                        $this->CRUDLoadController($target,$arrPost,'viewfunction');

                    }  else  $this->load->view('main/index');

                }   else $this->load->view('main/index');

            } else $this->load->view('main/index');
    }

    private function IsAjaxRequest()
    {
        /* AJAX check  */
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
    	} else return false;

    }
    
    public function ExecProc()
    {

            
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/ProcessList.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);

        /* echo '<script type="text/javascript">alert("ada");</script>'; */

         foreach ($tambahdelimiter as $rowProc)
        {

            if (trim($rowProc)!='')
            {

                list($pengenal, $sintak)= explode(';',$rowProc);

                if (trim($ModuleID)==trim($pengenal))
                {
                    //load controller
                    //return echo json lsg dari controller

                    list($mod, $ctrl, $method)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    $this->$ctrl->$method($Params);

                }
            }
        }

    }
    
    public function ExecReport()
    {
    	//format txt proses yang akan digunakan
       
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID =isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";
        $Params = isset($_REQUEST['Params']) ? $_REQUEST['Params'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/ReportList.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);

        /* echo '<script type="text/javascript">alert("ada");</script>'; */

        foreach ($tambahdelimiter as $rowProc)
        {

            if (trim($rowProc)!='')
            {

                list($pengenal, $sintak)= explode(';',$rowProc);

                if (trim($ModuleID)==trim($pengenal))
                {
                    //load controller
                    //return echo json lsg dari controller

                    list($mod, $ctrl)= explode('/',$sintak);
                    $this->load->module($mod.'/'.$ctrl);
                    echo $this->$ctrl->CreateReport('', $UserID,$Params,0,0,'','');

                }
            }
        }
//        }
//               else
//       {$this->Logoff();}
    }



    public function getModule() //$UserID,$ModuleID)
    {
       if  ($this->_is_logged_in())
       {
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/".$ModuleID."jslist.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);

        foreach ($tambahdelimiter as $rowJS)
        {
            $arrListJS[]= str_replace( "./",base_url() , $rowJS);  //'.' ,base_url(), $rowJS,1 );
        }
											
    	$this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetModule($UserID, $ModuleID);

        if ($query->num_rows() > 0)
        {
            //$row = $query->row();
            $row=$query->row_array();
            //$id= $row['Mod_ID'];
            $id= $row['mod_id'];
            //$title = $row['Mod_Name'];
            $title = $row['mod_name'];

            $url= ''; 
        }
        //Dim v = New With {.title = vd.title, .id = vd.id, .url = vd.url, .htm = m}

        echo '{success:true, title:"'.$title.'", id:"'.$id.'", url:"'.$url.'", htm:'.json_encode($arrListJS).'}';
       }
       else
       {
            echo '{success : false, expire:true}';
       }

    }
	
    public function getReport()
    {
       if  ($this->_is_logged_in())
       {

       
        $UserID = isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
        $ModuleID = isset($_REQUEST['ModuleID']) ? $_REQUEST['ModuleID'] : "";

        $loadFile=file_get_contents( base_url()."ui/jslist/".$ModuleID."jslist.php");
        $tambahdelimiter= explode(chr(13).chr(10),$loadFile);
        $arrListJS='';
        foreach ($tambahdelimiter as $rowJS)
        {
            if (strlen($rowJS)>0)
            {
                $arrListJS[]=str_replace( "./",base_url() , $rowJS); //$rowJS;
            }
        }

    	/*  sementara langsung tanpa cek trustee 

         *
         */
        $this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetModule($UserID, $ModuleID);

        if ($query->num_rows() > 0)
        {
            //$row = $query->row();
            $row=$query->row_array();
            $id= $row['mod_id'];
            $title = $row['mod_name'];
            $url= ''; //$row['Mod_URL'];
        }


        $haveDialog="false";

        if (is_array($arrListJS)!==false)
        {$haveDialog="true";}

        echo '{success:true, title:"'.$title.'", id:"'.$id.'", url:"'.$url.'",
            htm:'.json_encode($arrListJS).', havedialog: '.$haveDialog.'}';
        }
        else
       {  echo '{success : false, expire:true}';}
    }

    Public Function Upload()
    {
        //$direktori = isset($_REQUEST['direktori']) ? $_REQUEST['direktori'] : "";
        //$file = isset($_REQUEST['file']) ? $_REQUEST['file'] : "";
        $rEFileTypes = "/^\.(png|gif|tiff|bmp|jpg|jpeg|xls|doc|txt|pdf|ppt|txt|pptx|docx|exe){1}$/i";

        $isFile = is_uploaded_file($_FILES['file']['tmp_name']);

        if ($isFile)
        {

            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($_FILES['file']['name']));

            $lokasi_file = $_FILES['file']['tmp_name'];
            //$nama_file   = $_FILES['file']['name'];
            //$ukuran_file = $_FILES['file']['size'];
            //$type_file = $_FILES['file']['type'];

            $uploads_dir =base_url().'Img Asset/'.$safe_filename;

            //($type_file == $type_allow and strpos($nama_file, " ") == false)

            if (preg_match($rEFileTypes, strrchr($safe_filename, '.')))
            {
                $result = move_uploaded_file($lokasi_file, $uploads_dir);

                if ($result==1)
                {

                    echo '{success:true, result:"Upload Sukses", namafile:"'.$safe_filename.'"}';

                } else echo '{success:false, error:"Upload Gagal"}';

            } else echo '{success:false, error:"Type File Salah"}';
            
        } else echo '{success:false, error:"Upload Gagal"}';

    }
	
    Public Function UploadRef()
    {

        if ($isFile)
        {
            $safe_filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),array("_", ""),trim($_FILES['fileAsetRef']['name']));

            $lokasi_file = $_FILES['fileAsetRef']['tmp_name'];
            //$nama_file   = $_FILES['file']['name'];
            //$ukuran_file = $_FILES['file']['size'];
            //$type_file = $_FILES['fileAsetRef']['type'];

            $uploads_dir =base_url().'Doc Asset/'.$safe_filename;

            $extension= end(explode(".", $file['name']));

            $file_type =  strrchr($safe_filename, '.');

            if (preg_match($rEFileTypes, $file_type))
            {
                $result = move_uploaded_file($lokasi_file, $uploads_dir);

                if ($result==1)
                {

                    echo '{success:true, result: "Upload Sukses", namafile: "'.$safe_filename.'", namaext: "'.$file_type.'"}';

                } else echo '{success:false, error:"Upload Gagal"}';

            } else echo '{success:false, error:"Type File Salah"}';
            
        } else echo '{success:false, error:"Upload Gagal"}';
        
    }
	
    Public function getLanguage()
    {
		
        $Kd_User=isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";

        //$Kd_User='0';
        $strLanguageID="1";
        $arrLanguage=array();
		
    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();

        if ($query->num_rows() > 0)
        {
            //$strLanguageID=$row->LANGUAGE_ID;
            $strLanguageID=$row->language_id;
        } else $strLanguageID="1";

        if ($strLanguageID==null or $strLanguageID=="" )
                $strLanguageID='1';

        $this->load->model('main/vilanguage');
        $query = $this->vilanguage->read($strLanguageID);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $rows)
            {
                //$arrLanguage[]=$rows;
                $arrLanguage[]=$this->FillRow($rows);
            }

//Dim result = New With {.success = True, .total = mListLanguange.Count, .ListLanguange = mListLanguange}
            echo '{success:true, total:'.count($arrLanguage).', ListLanguage:'.json_encode($arrLanguage).'}';

        }
        else
        {
            echo '{success:false}';
        };
				
    }

    private function FillRow($rec)
    {
        $row=new Rowgetlanguage;
        $row->LIST_KEY=$rec["list_key"];
        $row->LABEL=$rec["label"];

        return $row;
    }
	    
    public function getTrustee()
    {
//   	       if  ($this->_is_logged_in()===false)
//       {
        
    	$Kd_User=isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : "";
    	
        //$Kd_User='0';
		
        $this->load->model('main/vi_gettrustee');
        $query = $this->vi_gettrustee->readforGetTrustee1($Kd_User);
        //$idx=0;
        $cGroup="";
        $amList = array();
        $agroup=array();
        $strModName="";

        //foreach($query->result() as $rows){

        foreach($query->result_array() as $rows) {
// yang Asli dari Pa Ali $strModName=$this->getModName($Kd_User, $rows['mod_id']);          
            $strModName=$this->getModName($Kd_User, $rows['mod_name']); //Editan Hidayat Untuk Merubah Tampilan Menu menjadi nama bukan ID
	
            if ($strModName == "")
// yang Asli dari Pa Ali $strModName = $rows['mod_id'];//$rows->Mod_ID;
                $strModName = $rows['mod_name'];//Editan Hidayat Untuk Merubah Tampilan Menu menjadi nama bukan ID
			
                $strGroupName=$this->getModName($Kd_User, $rows['mod_key']);
				
                if ($strGroupName == "")
                    $strGroupName = $rows['mod_group']; //$rows->Mod_Group;
                
                $strTmpGroup = $rows['mod_group']; //$rows->Mod_Group;

                $rows['mod_group']=$strGroupName;
        	$rows['mod_url'] =str_replace("[mod_id]", $rows['mod_id'], $rows['mod_url']);
        	$rows['mod_url'] =str_replace("[image]",base_url()."ui/".$rows['mod_imgurl'],$rows['mod_url']);
        	$rows['mod_url'] =str_replace("[Mod_Name]",$strModName ,$rows['mod_url']);
			
                if (strlen(strstr($cGroup,$strTmpGroup))==0)
                {
                    $htm="";
                    $this->load->model('main/vi_gettrustee');
                    $query1 = $this->vi_gettrustee->readforGetTrustee2($Kd_User,$strTmpGroup);

                    foreach($query1->result_array() as $rows1) {
                        $strModName=$this->getModName($Kd_User,$rows1['mod_id']);

                        if ($strModName=="")
                                $strModName=$rows1['mod_name']; //$rows1->Mod_Name;
						
                	$rows1['mod_url'] = str_replace("[mod_id]", $rows1['mod_id'],$rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[image]", base_url()."ui/".$rows1['mod_imgurl'],$rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[Mod_Name]", $strModName, $rows1['mod_url']);
                        $rows1['mod_url'] = str_replace("[Mod_Desc]", $rows1['mod_desc'],$rows1['mod_url']);
			$htm .= $rows1['mod_url'].chr(13).chr(10);
					
		}
				
					
		//$agroup[]=array("Mod_Group"=>$strGroupName, "Htm"=>$htm);
                $agroup[]=array("mod_group"=>$strGroupName, "htm"=>$htm);
            }
			
            $amList[]=$rows;
	}

        //foreach(amList as ak)
        //{

        //};
			
        //Dim result = New With {.success = True, .total = mListJournal.Count, .ListTrustee = mListJournal, .ListReport = agroup}

        $dd ='{success:true, total:'.count($amList).', ListTrustee:'.json_encode($amList).', ListReport:'.json_encode($agroup).'}';

        /* echo '<script type="text/javascript">alert("'.$dd.'");</script>';*/
        echo $dd;
//       }
//              else
//       {$this->Logoff();}
    }     
            
    private function getModName($Kd_User, $strModID)
    {
   		
   		// ambil strIDLang
    	$this->load->model('userman/zusers');
        $query = $this->zusers->read($Kd_User);
        $row= $query->row();
        //$strIDLang=$row->LANGUAGE_ID;
        $strIDLang=$row->language_id;

        if ($strIDLang==null)
                $strIDLang=1;

        $this->load->model('main/getmodname');
        $query = $this->getmodname->read($strModID,$strIDLang);

        // isi return function

        $returnVal="";

        if ($query->num_rows() > 0)
        {
            $row= $query->row();
            //$returnVal=$row->LABEL;
            $returnVal=$row->label;
        };

        return $returnVal;
																		   	 	
    }
	
	public function GetUmur() {
        $tmptgllahir = $_POST['TanggalLahir'];
        $strQuery = "select age(timestamp '".$tmptgllahir."')";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
             foreach($res as $data)
             {
                $umur = $data->age;
             }
         echo $umur;
}
         return $umur;
    }
	
	public function posting()
{
	$noTrans = $_POST['_notransaksi'];
	$query = $this->db->query("select postingtransaksi('".$noTrans."')");

	 //--- Query SQL Server ----//
	 /*	$DB = $this->load->database('otherdb',true);
		$DB->query("UPDATE transaksi set posting_transaksi = TRUE where no_transaksi = '$noTrans'");
		$DB->close();*/
	  //--- Akhir Query SQL Server---//
	if($query)
	{
	echo "{success:true}";	
	}
	else
	{
	echo "{success:false}";		
	}
	
}

//---get shift untuk IGD----//
function getkdbagianIGD()
	{
	$query_kdbagian = $this->db->query("select bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat'");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianrwj = $kdbag->kd_bagian;
			}
			return $kdbagianrwj;
	}
	
	function getMaxkdbagianIGD()
	{
	if(isset($_POST['command']))
	{
	$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Darurat'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag)
			{
			$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
	}
	}
	
	   //function untuk mengambil nilai shift         
     function getcurrentshiftIGD()
    {   
	if(isset($_POST['command']))
	{
	  $kdbagianrwj = $this->getkdbagianIGD();
	  $strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                $curentshift = $data->getcurrentshift;
				
             }
            }
         echo $curentshift;
	}
	else
	{
	$kdbagianrwj = $this->getkdbagianIGD();
	$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                $curentshift = $data->getcurrentshift;
				
             }
            }
         return $curentshift;
	}
    }
    //----------------END------------------------


//---get shift untuk RWI----//
function getkdbagianRWI()
	{
	$query_kdbagian = $this->db->query("select bagian_shift.kd_bagian from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Inap'");
			$result_kdbagian = $query_kdbagian->result();
			foreach($result_kdbagian as $kdbag)
			{
			$kdbagianrwj = $kdbag->kd_bagian;
			}
			return $kdbagianrwj;
	}

public function GetPasienTranfer() 
{
        $kdtr = $_POST['notr'];
        $strQuery = "SELECT  Unit.Nama_Unit as namaunit, Pasien.Nama as namapasien, Pasien.ALamat, kunjungan.Kd_pasien as kdpas,
					Transaksi.Tgl_Transaksi as tgtr, Dokter.Nama 
					as Nama_Dokter, Kunjungan.*, Transaksi.NO_TRANSAKSI as notransaksi, TRANSAKSI.Kd_Kasir as kdkas, TRANSAKSI.co_status, Unit.kd_kelas, 
					Dokter.Kd_Dokter,kunjungan.kd_unit as kdunit, 
					Transaksi.ORDERLIST, knt.Jenis_Cust From 
					((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit)
					INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien)
					INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter) 
					Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer) 
					INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit 
					and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.Urut_masuk=kunjungan.Urut_masuk where Transaksi.Co_status ='false' and unit.kd_bagian = 1
					and kunjungan.kd_pasien='".$kdtr."' order by Transaksi.Tgl_Transaksi desc limit 1 ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string .= "<>".$data->namaunit ;
				$string .= "<>".$data->namapasien;
				$string .= "<>".$data->notransaksi;
				$string .= "<>".$data->tgtr;
				$string .= "<>".$data->kdpas;
				$string .= "<>".$data->kdunit;
				$string .= "<>".$data->kdkas;
             }
            echo $string;
		 }	
         return $string;
    }

public function GetPasienTranferLab_rad() 
{
			  $kdtr = $_POST['notr'];
		      $kdkasir = $_POST['kdkasir'];
        $strQuery = "SELECT Unit.Nama_Unit as namaunit,
					Pasien.Nama as namapasien,
					Pasien.ALamat,
					kunjungan.Kd_pasien as kdpas,
					Transaksi.Tgl_Transaksi as tgtr,
					Dokter.Nama as Nama_Dokter,
					Kunjungan.*, 
					Transaksi.NO_TRANSAKSI as notransaksi,
					TRANSAKSI.Kd_Kasir as kdkas, 
					TRANSAKSI.co_status, 
					Unit.kd_kelas, 
					Dokter.Kd_Dokter,
					kunjungan.kd_unit as kdunit, 
					Transaksi.ORDERLIST, knt.Jenis_Cust
					From ((((unit INNER JOIN kunjungan ON kunjungan.kd_unit=unit.kd_unit) 
					INNER JOIN Pasien On pasien.kd_pasien=kunjungan.kd_pasien) 
					INNER JOIN dokter on dokter.kd_dokter=kunjungan.kd_Dokter) 
					Left JOIN Kontraktor knt On knt.Kd_Customer=kunjungan.Kd_Customer) 
					INNER JOIN Transaksi On Transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit and transaksi.Tgl_transaksi=kunjungan.tgl_masuk
					and transaksi.Urut_masuk=kunjungan.Urut_masuk 
					where Transaksi.Co_status ='false' and Transaksi.kd_kasir = '$kdkasir' and Transaksi.no_transaksi='$kdtr' order by Transaksi.Tgl_Transaksi desc limit 1 ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string .= "<>".$data->namaunit ;
				$string .= "<>".$data->namapasien;
				$string .= "<>".$data->notransaksi;
				$string .= "<>".$data->tgtr;
				$string .= "<>".$data->kdpas;
				$string .= "<>".$data->kdunit;
				$string .= "<>".$data->kdkas;
             }
            echo $string;
		 }	
         return $string;
    }
	
	
	public function GettotalTranfer() {
        $kdtr = $_POST['notr'];
        $strQuery = "select sum(harga*qty) as total from detail_transaksi where no_transaksi='".$kdtr."' ";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
         {
		 $string = "";
		  foreach($res as $data)
             {
                $string = $data->total ;
				
             }
         echo $string;
		 }	
         return $string;
    }
	
	
	function getMaxkdbagianRWI()
	{
	if(isset($_POST['command']))
	{
	$query_maxkdbagian = $this->db->query("select bagian_shift.numshift from bagian_shift INNER JOIN bagian on bagian_shift.kd_bagian = bagian.kd_bagian where bagian = 'Rawat Inap'");
			$result_maxkdbagian = $query_maxkdbagian->result();
			foreach($result_maxkdbagian as $maxkdbag)
			{
			$maxkdbagianrwj = $maxkdbag->numshift;
			}
			echo $maxkdbagianrwj;
	}
	}
	
	   //function untuk mengambil nilai shift         
     function getcurrentshiftRWI()
    {   
	if(isset($_POST['command']))
	{
	  $kdbagianrwj = $this->getkdbagianRWI();
	  $strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                $curentshift = $data->getcurrentshift;
				
             }
            }
         echo $curentshift;
	}
	else
	{
	$kdbagianrwj = $this->getkdbagianRWI();
	$strQuery = "select getcurrentshift(".$kdbagianrwj.")";
		//echo $kdbagian.":";
        $query = $this->db->query($strQuery);
        $res = $query->result();
         if ($query->num_rows() > 0)
            {
             foreach($res as $data)
             {
                $curentshift = $data->getcurrentshift;
				
             }
            }
         return $curentshift;
	}
    }
    //----------------END------------------------

        
}







class Rowgetlanguage
{
    public $LIST_KEY;
    public $LABEL;

}

?>