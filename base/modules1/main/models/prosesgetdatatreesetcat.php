<?php

class prosesgetdatatreesetcat extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
    }

    public function index()
    {

            $this->load->view('main/index');
  
    }

    private function getDetailChild($id)
    {

        $arr = array();
        $this->load->model('setup/tblam_klas_produk');
        //x.Open("where type ='G' and left(parent," & id.Length & ") ='" & id & "'order by KD_KLAS")
        $this->tblam_klas_produk->db->where(" substr(parent,1,".strlen($id).") = '".$id."'" ,null,false);
        $query = $this->tblam_klas_produk->GetRowList( 0, 1000, 'ASC', 'kd_klas', '');

        if ($query[1] > 0)
        {
            foreach ($query[0] as $row)
            {
                $arr[] = $row;
            }
        }

        return $arr;
    }

    public function read($Params) //index()// read()
    {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);

        try
        {
            $this->load->model('setup/tblam_klas_produk');
            $this->tblam_klas_produk->db->where("parent = '0'",null,false);
            $query = $this->tblam_klas_produk->GetRowList( 0, 1000, 'ASC', 'kd_klas', '');

            $arrChild = array();
            $arr=array();

            if ($query[1] > 0)
            {

                foreach ($query[0] as $row)
                {
                    $Tree = new clsTreeRow;
                    $Tree->id = $row->KD_KLAS;
                    $Tree->text = $row->KLASIFIKASI;
                    $arrChild = $this->getDetailChild($row->KD_KLAS);

                    if (count($arrChild)>0)
                    {
                        $Tree->leaf = false;
                        $Tree->parents = $row->PARENT;
                        $Tree->parents_name="";

                        foreach ($arrChild as $a)
                        {
                            if ($a->PARENT == $Tree->id)
                            {
                                $Tree2 = new clsTreeRow();
                                $Tree2->id = $a->KD_KLAS;
                                $Tree2->text = $a->KLASIFIKASI;
                                $Tree2->leaf = false;
                                $Tree2->parents = $a->PARENT;
                                $Tree2->parents_name = $Tree->text;
                                $Tree->children[]=$Tree2;
                            } else {
                                if (count($Tree->children) > 0)
                                {
                                    foreach ($Tree->children as $b )
                                    {
                                        $Tree3 = new clsTreeRow;
                                        if ($a->PARENT== $b->id)
                                        {
                                            $Tree3->id = $a->KD_KLAS;
                                            $Tree3->text = $a->KLASIFIKASI;
                                            $Tree3->leaf = false;
                                            $Tree3->parents = $a->PARENT;
                                            $Tree3->parents_name = $b->text;
                                            $b->leaf = false;
                                            $b->children[]= $Tree3;
                                        } else {
                                            if (count($b->children)>0)
                                            {
                                                Foreach($b->children as $c)
                                                {
                                                    $Tree4 = new clsTreeRow;
                                                    if ($a->PARENT == $c->id)
                                                    {
                                                        $Tree4->id= $a->KD_KLAS;
                                                        $Tree4->text = $a->KLASIFIKASI;
                                                        $Tree4->leaf = false;
                                                        $Tree4->parents = $a->PARENT;
                                                        $Tree4->parents_name = $c->text;
                                                        $c->leaf = false;
                                                        $c->children[] = $Tree4;
                                                    } else {
                                                        if (count($c->children)>0)
                                                        {
                                                            foreach ($c->children as $d)
                                                            {
                                                                $Tree5 = new clsTreeRow;
                                                                if ($a->PARENT == $d->id)
                                                                {
                                                                    $Tree5->id = $a->KD_KLAS;
                                                                    $Tree5->text = $a->KLASIFIKASI;
                                                                    $Tree5->leaf = false;
                                                                    $Tree5->parents = $a->PARENT;
                                                                    $Tree5->parents_name = $d->text;
                                                                    $d->leaf = false;
                                                                    $d->children[] = $Tree5;
                                                                } else {
                                                                    if (count($d->children) > 0)
                                                                    {
                                                                        foreach ($d->children as $e)
                                                                        {
                                                                            $Tree6 = new clsTreeRow();
                                                                            if ($a->PARENT === $e->id)
                                                                            {
                                                                                $Tree6->id = $a->KD_KLAS;
                                                                                $Tree6->text = $a->KLASIFIKASI;
                                                                                $Tree6->leaf = false;
                                                                                $Tree6->parents = $a->PARENT;
                                                                                $Tree6->parents_name = $e->text;
                                                                                $e->leaf = false;
                                                                                $e->children[]=$Tree6;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        $Tree->leaf = false;
                        $Tree->parents = $row->KD_KLAS;
                        $Tree->parents_name = "";
                    }

                    //$arr[] =$Tree;

                    //$arr[] =$Tree;
                    $arr[] =$Tree;
                }

                //Dim res = New With {.success = True, .arr = arr}
                //output json nya aneh
                $res = "{success: true, arr: ".json_encode($arr)."}";

            } else {
                $res = '{success: false}';
            }

        }
        catch(Exception $o)
        {
            $res = '{success: false}';
        }

        echo $res;
    }

//    public function read($Params) //index()// read()
//    {
//		try
//		{
//
//                        $this->load->model('setup/tblam_klas_produk');
//                        $this->tblam_klas_produk->db->where("KD_KLAS <> '9999'");
//			$res = $this->tblam_klas_produk->GetRowList( 0, 0, '', 'KD_KLAS', '');
//			$prn='';
//			$lst='';
//			foreach ($res[0] as $node)
//			{
//				$x=new clsTreeRow;
//				if ( strlen($node->PARENT)===0 )
//				{
//
//        				$prn=$node->KD_KLAS;
//					$x->id=$node->KD_KLAS;
//					$x->text=$node->KLASIFIKASI;
//                                        $x->leaf=false;
//					$lst[$x->id] = $x ;
//
//				}
//				else
//				{
//					$x->id=$node->KD_KLAS;
//					$x->text=$node->KLASIFIKASI;
//                                        $x->parents=$node->PARENT;
//                                        $x->leaf=false;
//                                        $x->parents_name = $lst[$x->parents]->text;
//					$lst[$x->parents]->children[]=$x;
//				}
//			}
//
//		}
//		catch(Exception $o)
//		{
//			echo 'Debug  fail ';
//
//		}
//
//	 	echo '{success: true,  arr:'.json_encode($lst).'}';
//
//
//    }


//    private function getDetailChild($id)
//    {
//
//        $arr = array();
//        $this->load->model('setup/tblam_klas_produk');
//        //x.Open("where type ='G' and left(parent," & id.Length & ") ='" & id & "'order by KD_KLAS")
//        $this->tblam_klas_produk->db->where("type = 'D' and substr(parent,1,".strlen($id).") = '".$id."' order by KD_KLAS");
//        $query = $this->tblam_klas_produk->GetRowList( 0, 1000, '', '', '');
//
//        if ($query[1] > 0)
//        {
//            foreach ($query[0] as $row)
//            {
//                $arr[] = $row;
//            }
//        }
//
//        return $arr;
//    }
//
//    public function read($Params) //index()// read()
//    {
//        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
//
//        try
//        {
//            $this->load->model('setup/tblam_klas_produk');
//            $this->tblam_klas_produk->db->where("type = 'G' and parent is null order by KD_KLAS");
//            $query = $this->tblam_klas_produk->GetRowList( 0, 1000, '', '', '');
//
//            $arrChild = array();
//            $arr=array();
//
//            if ($query[1] > 0)
//            {
//                $Tree = new clsTreeRow;
//                foreach ($query[0] as $row)
//                {
//                    $Tree->id = $row->KD_KLAS;
//                    $Tree->text = $row->KLASIFIKASI;
//                    $arrChild = $this->getDetailChild($row->KD_KLAS);
//
//                    if (count($arrChild)>0)
//                    {
//                        $Tree->leaf = false;
//                        $Tree->parents = $row->PARENT;
//                        $Tree->parents_name="";
//
//                        foreach ($arrChild as $a)
//                        {
//                            if ($a->PARENT == $Tree->id)
//                            {
//                                $Tree2 = new clsTreeRow();
//                                $Tree2->id = $a->KD_KLAS;
//                                $Tree2->text = $a->KLASIFIKASI;
//                                $Tree2->leaf = false;
//                                $Tree2->parents = $a->PARENT;
//                                $Tree2->parents_name = $Tree->text;
//                                $Tree->children[]=$Tree2;
//                            } else {
//                                if (count($Tree->children) > 0)
//                                {
//                                    foreach ($Tree->children as $b )
//                                    {
//                                        $Tree3 = new clsTreeRow;
//                                        if ($a->PARENT== $b->id)
//                                        {
//                                            $Tree3->id = $a->KD_KLAS;
//                                            $Tree3->text = $a->KLASIFIKASI;
//                                            $Tree3->leaf = false;
//                                            $Tree3->parents = $a->PARENT;
//                                            $Tree3->parents_name = $b->text;
//                                            $b->leaf = false;
//                                            $b->children[]= $Tree3;
//                                        } else {
//                                            if (count($b->children)>0)
//                                            {
//                                                Foreach($b->children as $c)
//                                                {
//                                                    $Tree4 = new clsTreeRow;
//                                                    if ($a->PARENT == $c->id)
//                                                    {
//                                                        $Tree4->id= $a->KD_KLAS;
//                                                        $Tree4->text = $a->KLASIFIKASI;
//                                                        $Tree4->leaf = false;
//                                                        $Tree4->parents = $a->PARENT;
//                                                        $Tree4->parents_name = $c->text;
//                                                        $c->leaf = false;
//                                                        $c->children[] = $Tree4;
//                                                    } else {
//                                                        if (count($c->children)>0)
//                                                        {
//                                                            foreach ($c->children as $d)
//                                                            {
//                                                                $Tree5 = new clsTreeRow;
//                                                                if ($a->PARENT == $d->id)
//                                                                {
//                                                                    $Tree5->id = $a->KD_KLAS;
//                                                                    $Tree5->text = $a->KLASIFIKASI;
//                                                                    $Tree5->leaf = false;
//                                                                    $Tree5->parents = $a->PARENT;
//                                                                    $Tree5->parents_name = $d->text;
//                                                                    $d->leaf = false;
//                                                                    $d->children[] = $Tree5;
//                                                                } else {
//                                                                    if (count($d->children) > 0)
//                                                                    {
//                                                                        foreach ($d->children as $e)
//                                                                        {
//                                                                            $Tree6 = new clsTreeRow();
//                                                                            if ($a->PARENT === $e->id)
//                                                                            {
//                                                                                $Tree6->id = $a->KD_KLAS;
//                                                                                $Tree6->text = $a->KLASIFIKASI;
//                                                                                $Tree6->leaf = false;
//                                                                                $Tree6->parents = $a->PARENT;
//                                                                                $Tree6->parents_name = $e->text;
//                                                                                $e->leaf = false;
//                                                                                $e->children[]=$Tree6;
//                                                                            }
//                                                                        }
//                                                                    }
//                                                                }
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    } else {
//
//                        $Tree->leaf = false;
//                        $Tree->parents = $row->KD_KLAS;
//                        $Tree->parents_name = "";
//                    }
//
//                    //$arr[] =$Tree;
//                    $arr[] =$Tree;
//                }
//
//                //Dim res = New With {.success = True, .arr = arr}
//                //output json nya aneh
//                $res = "{success: true, arr: ".json_encode($arr)."}";
//
//            } else {
//                $res = '{success: false}';
//            }
//
//        }
//        catch(Exception $o)
//        {
//            $res = '{success: false}';
//        }
//
//        echo $res;
//    }
}


?>
