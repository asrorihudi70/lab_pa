<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class vilanguage extends Model
{
    private $strQuery="SELECT gkl.group_key_id, gkl.list_key_id, gkl.language_id, gkl.label, lk.list_key
        FROM am_language l
        INNER JOIN am_group_key_language gkl ON l.language_id = gkl.language_id
        INNER JOIN (am_group_key gk
        INNER JOIN am_group_list_key glk ON gk.group_key_id = glk.group_key_id)
        ON gkl.group_key_id = glk.group_key_id AND gkl.list_key_id = glk.list_key_id
        INNER JOIN am_list_key lk ON glk.list_key_id = lk.list_key_id";

    function vilanguage()
    {
        parent::Model();
        $this->load->database();
    }


    function read($strLanguageID)
    {
        //$this->db->where("LANGUAGE_ID",$strLanguageID);
        //$this->db->where("language_id",$strLanguageID);
        //$query = $this->db->get('dbo.vilanguage');
        //$query = $this->get($this->strQuery);

        $strQ = $this->strQuery." where l.language_id = '".$strLanguageID."' ";
        $query = $this->db->query($strQ);

        return $query;
    }

    function readParam($strParams)
    {
        //$this->db->where($strParams);
        //$query = $this->db->get('dbo.vilanguage');
        //$query = $this->get($this->strQuery);
        $strQ = $this->strQuery." where ". $strParams;
        $query = $this->get($strQ);

        return $query;
    }

}

?>