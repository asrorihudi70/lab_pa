<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

//pasang script load js mainpage disini

/**
 *      <script type="text/javascript">
 *          strUser = '<%=Me.User.Identity.Name%>'
 *          strIDBahasa ='<%=Request.QueryString("Lang")%>'
 *      </script>
 */
?>
<?php if ($this->_ci_cached_vars['controller']->_is_logged_in()) { ?>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>MediSmart</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/aset-def.css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/fileuploadfield.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/RowEditor.css" rel="stylesheet" type="text/css" />
</head>

<body >
	<div id="loading" align="center" style="margin-top:250px; "  > 
	<center>
    <img alt = "" src="<?php echo base_url(); ?>ui/images/default/shared/blue-loading.gif" width="32" height="32"  style="float:center;vertical-align:middle;"/><br />
    <span id="loading-msg"></span>
	</center>
	</div> 
	
    <script type="text/javascript">
        var strUser;
        var baseURL='<?php echo base_url(); ?>';
        var strModul = 'NCI-MediSmart';
        document.getElementById('loading-msg').innerHTML = 'Loading Core Component... <br>' + strModul;
    </script>		
    
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/adapter/ext/ext-base.js"></script>
    
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-base.js"></script> -->
 	
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-all.js"></script> -->
    
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/ext-all.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CommonRenderer.js"></script>
    <!--
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/HeaderCompany.js"></script>
    -->
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/StatusBar.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/menu.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Penamaan.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Language.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/LookupEmployee.js">
	</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/InputLogMetering.js">
	</script>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/WarningLogMetering.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Trustee.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/App.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Module.js"></script>
    <script type="javascript" src="<?php echo base_url(); ?>ui/Scripts/SessionProvider.js"></script>
    
	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = ' Initialization......... <br> ' + strModul;</script>
			
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GroupSummary.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FileUploadField.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/cook.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowExpander.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowEditor.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/swfobject.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/flashplugin.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/UtilityAset.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GantiPass.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/SettingBahasa.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FilterRow.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CheckColumn.js"></script>
	
	<script type="text/javascript">
	var msgd =document.getElementById('loading').style.visibility='hidden';
	</script>
	<div id="bd"> 
	<div>
    	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPanel.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPage.js"></script>
    </div>   
	</div>    
	 		
</body> 
</html>
<script>

Ext.onReady(function () {
    if(Ext.isChrome===true){       
        var chromeDatePickerCSS = ".x-date-picker {border-color: #1b376c;background-color:#fff;position: relative;width: 185px;}";
        Ext.util.CSS.createStyleSheet(chromeDatePickerCSS,'chromeDatePickerStyle');
    }
});
</script>
<?php } else { ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Login Page | NCI Medismart </title>
<link href="<?php echo base_url(); ?>ui/images/css/style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>ui/images/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>ui/images/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>ui/images/css/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>ui/images/bootstrap/js/bootstrap.min.js"></script>
<link rel="shortcut icon" href="<?php echo base_url(); ?>ui/images/aset/favicon.ico"></link>
</head>

<body>
<div class="navbar"></div>
<nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="<?php echo base_url(); ?>ui/images/aset/logo.png"  class="img-responsive" id="img" alt="logo"/>
           
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav" style="margin-left:10px;">
                <li class="active"><a href="#" target="_blank"><span class="glyphicon glyphicon-home"></span>&nbsp;  Home</a></li>
                <li><a href="#" target="_blank"><span class="glyphicon glyphicon-exclamation-sign"></span> &nbsp;About</a></li>
                <li><a href="#" target="_blank"><span class="glyphicon glyphicon-earphone"></span>&nbsp;Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
    <div class="slider col-sm-11 col-md-11 col-lg-9">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?php echo base_url(); ?>ui/images/aset/1.jpg" alt="Chania">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>ui/images/aset/1.jpg" alt="Chania" title="test" alt="test">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>ui/images/aset/1.jpg" alt="Flower">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>ui/images/aset/1.jpg" alt="Flower">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        <!--<h1>Learn to Create Websites</h1>
        <p>In today's world internet is the most popular way of connecting with the people. At <a href="http://www.tutorialrepublic.com" target="_blank">tutorialrepublic.com</a> you will learn the essential of web development technologies along with real life practice example, so that you can create your own website to connect with the people around the world.</p>
        <p><a href="http://www.tutorialrepublic.com" target="_blank" class="btn btn-success btn-lg">Get started today</a></p>-->
    </div>

    <div class=" col-sm-11 col-md-11 col-lg-3" >
<div class="form-login">
                     <form id="formID"  method="post" action="<?php echo base_url() ?>index.php/main/Login">
                       <div class="header-login"><a id="link1" style="color:#000;"><h4><span class="glyphicon glyphicon-lock"></span> Login Untuk Akses Sistem</h4></a></div>
                           <div id="form1">
                           
                             <div class="notif">
                             <?php if(isset($this->_ci_cached_vars['error']) and $this->_ci_cached_vars['error'] != ''){ print_r($this->_ci_cached_vars['error']);} else { echo "&nbsp;"; } ?>
                             </div>
                           
                        <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
  <input type="text" class="form-control"  ID="UserName" name="UserName" placeholder="Masukan Username" aria-describedby="basic-addon1">
</div>
                        <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span>  </span>
  <input type="password" class="form-control" ID="Password" name="Password" placeholder="Masukan Password" aria-describedby="basic-addon1">
</div>
                         <label for="btn"></label>
                        <button type="button" id="login" name="btn" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Login&nbsp;&nbsp;</button>
                        <button type="reset" name="btn" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
                        
                        
                   </div>    
                   
                    <div id="form3">
                    <div class="user-window input-group">
                        <div class="col-lg-12" >
                        <img src="<?php echo base_url(); ?>ui/images/aset/User-info.png" />
    </div>
     <div id="text-user"><b>Selamat Datang, John Doe</b></div>
     <div><b>Sistem akan Memeriksa Account Anda</b></div>
    
                       <div> <img src="<?php echo base_url(); ?>ui/images/aset/loader.gif" style="width:60px; height:60px;" /></div>
  
 </div>
                      
                    </div>     
               
                    
                   
                       
                    </form>

</div>
<div class="header-login-reffid"><h4><b>NCI Medismart v.5</b></h4></div>
 <!--<div class="header-login-reffid"><a id="link2" style="color:#000;"><h4><span class="glyphicon glyphicon-tags">  </span> &nbsp;&nbsp;Login Via Reff ID</h4></a></div>-->

</div>
        </div>
    <div class="row">
    <div class="content">
        <div class="subcontent col-sm-6 col-md-4 col-lg-4">
            <div class="header-content"><h4><b>About PT. NCI</b></h4></div>
           <p>PT Nuansa Cerah Informasi Adalah Perusahan Konsultant IT berpengalaman sejak 1991, Kamo telah berhasil menciptakan pola kemitraan dalam pengembangan sistem informasi untuk menggantikan pola konvesional penjual-pembeli</p>
            <p><a href="#" target="_blank" class="btn btn-primary">Read More &raquo;</a></p>
        </div>
        <div class="subcontent col-sm-6 col-md-4 col-lg-4">
            <div class="header-content"><h4><b>About NCI Medismart</b></h4></div>
             <p>NCI MediSmart, adalah Software Aplikasi Sistem Informasi Manajemen Rumah Sakit yang didesain dan dikembangkan dengan berbagai fasilitas untuk memberikan kemudahan, kecepatan dan keakuratan proses Manajemen Rumah Sakit.</p>
            <p><a href="#" target="_blank" class="btn btn-primary">Read More &raquo;</a></p>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="subcontent col-sm-6 col-md-4 col-lg-4">
            <div class="header-content"><h4><b>Contact Us</b></h4></div>
           
<p><b> Head Office : </b>Jln. Pelajar Pejuang 45 No 43 Bandung 40163
Phone 022.7317077
Fax 022.7300790</p>
<p>website : <a target="_blank" href="http://www.ncimedismart.com"> NCI MediSmart</a></p>
<p>email: <a target="_blank" href="mailto:marketing@ncimedismart.com">marketing@ncimedismart.com</a></p>
            <p><a href="#" target="_blank" class="btn btn-primary">Read More &raquo;</a></p>
        </div>
    </div>
    
    <hr>
    </div>

    <br />
    <hr style="display: block; height: 1px;border: 0; border-top: 1px solid #ccc;margin: 1em 0; padding: 0;"></hr>
    <div class="row">
        <div class="col-sm-12">
            <footer>
                <p><b>© Copyright 2015 NCI Medismart </b></p>
            </footer>
        
    </div>
    </div>
</div>
</body>
</html>

<script type="text/javascript">
$(document).ready(function(e) {
	$("#UserName").focus();
$('#form1').show();
$('#form3').hide();

var w = $('#form1').width();
var h = $('#form1').height();
$('#form3').css({'width':'100%','height':h});

$('#link2').click(function(e) {
    $('#form1').stop().animate().hide(500);
	$('#form3').stop().animate().hide(500);
});
    
$('#link1').click(function(e) {
    $('#form1').stop().animate().show(500);
	$('#form3').stop().animate().hide(500);
});
    

$( "#login" ).click(function( e ) {
var nama = $("#UserName").val();	
var pass = $("#Password").val();	


if(nama == '')
{
	alert('Username belum diisi');
	e.preventDefault();

}
else if(pass == '')
{
	alert('Password belum diisi');
	e.preventDefault();
}
else
{
$('#form1').stop().animate().hide();
$("#text-user").empty().append('<b>Selamat Datang, '+nama+'</b>');
$('#form3').stop().animate().show();	

var timer = setInterval(function(){
complete();
clearInterval(timer);
},2000);
}
});


});

$("#Password").keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
	e.preventDefault();  
   $("#login").click();
  }
});

$("#UserName").keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
	e.preventDefault(); 
   $("#login").click();
  }
})   


function complete()
{
	$('#formID').submit();
}
</script>

<?php } ?>
