<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */

//pasang script load js mainpage disini

/**
 *      <script type="text/javascript">
 *          strUser = '<%=Me.User.Identity.Name%>'
 *          strIDBahasa ='<%=Request.QueryString("Lang")%>'
 *      </script>
 */
?>
<?php if ($this->_ci_cached_vars['controller']->_is_logged_in()) { ?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>MediSmart</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ui/css/aset-def.css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/fileuploadfield.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>ui/css/ext/RowEditor.css" rel="stylesheet" type="text/css" />
</head>

<body >
	<div id="loading" align="center" style="margin-top:250px; "  > 
	<center>
    <img alt = "" src="<?php echo base_url(); ?>ui/images/default/shared/blue-loading.gif" width="32" height="32"  style="float:center;vertical-align:middle;"/><br />
    <span id="loading-msg"></span>
	</center>
	</div> 
	
    <script type="text/javascript">
        var strUser;
        var baseURL='<?php echo base_url(); ?>';
        var strModul = 'PrimAZ';
        document.getElementById('loading-msg').innerHTML = 'Loading Core Component... <br>' + strModul;
    </script>		
    
        <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/adapter/ext/ext-base.js"></script>
    
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-base.js"></script> -->
 	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/CommonRenderer.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-all.js"></script> -->
    
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/ext-3.3.0/ext-all.js"></script>
    
    <!--
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/HeaderCompany.js"></script>
    -->
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/StatusBar.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/menu.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Penamaan.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Language.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/LookupEmployee.js">
	</script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/modules/General/InputLogMetering.js">
	</script>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/WarningLogMetering.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Trustee.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/App.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/Module.js"></script>
    <script type="javascript" src="<?php echo base_url(); ?>ui/Scripts/SessionProvider.js"></script>
    
	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = ' Initialization......... <br> ' + strModul;</script>
			
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/GroupSummary.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FileUploadField.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/cook.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowExpander.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/RowEditor.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/swfobject.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/flashplugin.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/UtilityAset.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/SettingBahasa.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/FilterRow.js"></script>
	
	<script type="text/javascript">
	var msgd =document.getElementById('loading').style.visibility='hidden';
	</script>
	<div id="bd"> 
	<div>
    	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPanel.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>ui/Scripts/MainPage.js"></script>
    </div>   
	</div>    
	 		
</body> 
</html>
<?php } else { ?>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Nci Medismart Login Page</title>

    <style type="text/css">
        a { text-decoration:none; }
        a:link, a:visited { color: #2AD60D; }
        a:hover, a:active { color: #2AD60D; font-weight:bold; }
        a:hover { background-color: #2AD60D; }
        .login-footer {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-style: italic;
            font-weight: normal;
            color: #2AD60D;
            padding-left: 5px;
            padding-top: 5px;
        }
        .style1
        {
            width: 227px;
        }
        
    </style>

</head>
<body bgcolor="#2AD60D">
    <p>
        &nbsp;</p>
    <form id="form1"  method="post" action="<?php echo base_url() ?>index.php/main/Login">
    <div>

                <table border="0" cellpadding="0" cellspacing="0" bgcolor="#2AD60D">
                <tr>
                    <td><img src="<?php echo base_url() ?>ui/images/aset/login/backgrounddepan.png" width="1340" height="100" /></td>
                </tr>
                <tr>
                    <td>
                        <!-- table baris login form -->
                        <table  cellpadding="0" cellspacing="0" border="0" bgcolor="#2AD60D">
                        <tr>
                            <!-- table baris login col 1 -->
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" border="0" bgcolor="#2AD60D" width="500" height="265">
                                
                                <tr><td><img src="<?php echo base_url() ?>ui/images/aset/login/log_img_05.png" width="523" height="200"  /></td></tr>
                                
                                </table>
                            </td>

                            <!-- table baris login col 2 -->
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" border="0" bgcolor="#2AD60D" width="446" height="317" background="<?php echo base_url() ?>ui/images/aset/login/icon.png">
                                <tr><td><img src="<?php echo base_url() ?>ui/images/aset/login/log_img_01.png" width="2" height="200"/></td>
                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color: #2AD60D;">
    	                                <table border="0" cellpadding="0" cellspacing="0" border="0" width="400" height="197" background="<?php echo base_url() ?>ui/images/upi/log_img/log_img_01.png">
                                        <tr><td colspan="5">&nbsp;</td></tr>
    	                                <tr><td></td><td valign="top" align="right">User Name :</td><td>&nbsp;</td>
        	                                <td valign="top" align="left" class="style1"><input type="TextBox" ID="UserName" name="UserName"  ></input>
                                                        </td><td>&nbsp;</td></tr>
    	                                <tr><td></td><td valign="top" align="right">Password  :</td><td>&nbsp;</td>
        	                                <td valign="top" align="left" class="style1"><input type="password" ID="Password" name="Password"  ></input>
                                           	</td><td>&nbsp;</td></tr>
    	                               
                                        <tr><td colspan="5" align=center style="color: Red;">&nbsp;<br /></td></tr>
   		                                <tr><td></td><td valign="top" align="right">&nbsp;</td><td>&nbsp;</td>
                                                    <td valign="top" align="left" class="style1"><input type="submit" ID="Button1" class="Button1"
                                                                                                    value ="Log In" ></input></td><td>&nbsp;</td></tr>
   		                                <tr><td></td><td valign="top" align="right">&nbsp;</td><td>&nbsp;</td>
        	                                <td valign="top" align="left" class="style1"><a href="#">&nbsp;<?php echo $this->_ci_cached_vars['controller']->ErrLoginMsg ?></a></td><td>&nbsp;</td></tr>
                                        <tr><td colspan="5">&nbsp;</td></tr>
                                        </table>
                                    </td>
                                    <td><img src="<?php echo base_url() ?>ui/images/aset/login/log_img_11.png" width="2" height="200"/></td>
                            </tr>
                                </table>
                            </td>
                            
                            <!-- table baris login col 3 -->
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" border="2" bgcolor="#2AD60D" width="388" height="265">
                                    <tr><td><img src="<?php echo base_url() ?>ui/images/aset/login/log_img_11.png" width="5" height="201"/></td>
                                    <td><img src="<?php echo base_url() ?>ui/images/aset/login/log_img_07.png"  width="380" height="201" /></td></tr>
                                </table>
                            </td>
                        </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?php echo base_url() ?>ui/images/aset/login/backgrounddepan.png" width="1200" height="120" /></td>
                </tr>
                <tr>
                    <td align="Center"><font color="#48D1CC">powered by NCI MediSmart</font></td>
                </tr>
                <tr><td colspan="5">&nbsp;</td></tr>
                </table>

    </div>

    </form>
</body>
</html>
<?php } ?>
