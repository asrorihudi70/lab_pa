<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class am_bagian extends Model
{

	function am_unit()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('KD_BAGIAN', $data['KD_BAGIAN']);
		$this->db->set('BAGIAN', $data['BAGIAN']);
		$this->db->insert('bagian');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		$this->db->where('KD_BAGIAN', $id);
		$query = $this->db->get('bagian');

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('bagian');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('KD_BAGIAN', $data['KD_BAGIAN']);
		$this->db->set('BAGIAN', $data['BAGIAN']);
		$this->db->update('bagian');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('KD_BAGIAN', $id);
		$this->db->delete('bagian');

		return $this->db->affected_rows();
	}

}


?>
