<?php

/**
 * @author
 * @copyright
 */
class viewaccountinterface extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $traces=array();
        $result = $this->db->query("SELECT a.account,a.Kd_Produk, p.Deskripsi, a.Kd_Unit, u.Nama_Unit 
                                    FROM (
                                        ACC_PROD_UNIT a 
                                        INNER JOIN UNIT u ON a.Kd_Unit = u.Kd_Unit
                                         ) 
                                    INNER JOIN Produk p ON a.Kd_Produk = p.Kd_Produk 
                                    ".$Params[4]."
                                    order by nama_unit, deskripsi  asc limit 100")->result();
                                
        for($i=0 ; $i < count($result) ; $i++){
            $trace=array();
            $trace['ACCOUNT']=$result[$i]->account;
            $trace['KODEPRODUK']=$result[$i]->kd_produk;
            $trace['NAMAPRODUK']=$result[$i]->deskripsi;
            $trace['KDUNIT']=$result[$i]->kd_unit;
            $trace['UNIT']=$result[$i]->nama_unit;
            $traces[]=$trace;
        }
        echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($traces).'}';
    }

    public function getGridLookUpBarang(){
        $result=$this->db->query("SELECT Account, Name, Parent, Account||' '||Name as Acc_Name FROM ACCOUNTS WHERE Groups = 4 AND Type = 'G' ORDER BY Groups, Account")->result();
        
        echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
    }

    public function accounts(){
        $select = $this->db->query("SELECT * FROM ACCOUNTS WHERE Type = 'D' AND left(account,1) = '4' and (  
        upper(account) like upper('".$_POST['text']."%') )
        or ( upper(name) like upper('".$_POST['text']."%') ) ORDER BY Groups, Account limit 10")->result();
        $jsonResult=array();
        $jsonResult['processResult']='SUCCESS';
        $jsonResult['listData']=$select;
        echo json_encode($jsonResult);
    }

    public function produk(){
        $select = $this->db->query("select kp.kd_klas as kode, klasifikasi as name, 
                                    kd_produk, deskripsi 
                                    from klas_produk kp 
                                    inner join produk p on p.kd_klas = kp.kd_klas 
                                    where (kd_produk = '".(int)$_POST['text']."') or (lower(deskripsi) like lower('".$_POST['text']."%'))and parent <> '0' ORDER By kp.Kd_Klas limit 10")->result();
        $jsonResult=array();
        $jsonResult['processResult']='SUCCESS';
        $jsonResult['listData']=$select;
        echo json_encode($jsonResult);
    }

    public function loaddatadetailunit(){
        $kd_produk = $_POST["kd_produk"];
        $kd_unit = $_POST["kd_unit"];
        $select=$this->db->query("SELECT pu.Kd_Unit, u.Nama_Unit, '' as tag
                                    FROM (PRODUK_UNIT pu INNER JOIN UNIT u ON pu.Kd_Unit = u.Kd_Unit) 
                                    INNER JOIN Produk p ON pu.Kd_Produk = p.Kd_Produk 
                                    WHERE pu.Kd_Produk = ".$kd_produk." And u.kd_unit 
                                    Not In (Select Distinct kd_unit From Acc_prod_unit Where Kd_produk='".$kd_produk."') AND u.Kd_Bagian = ".$kd_unit)->result();
        echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
    }

    public function save(){
        $this->db->trans_begin();
        for ($i=0; $i < $_POST['jumlah']; $i++){
            if ($_POST['tag'.$i] != 'false') {
                $cekdatadetail = $this->db->query("select * from ACC_PROD_UNIT where account = '".$_POST['account']."' and kd_produk = '". $_POST['kdproduk'] ."' and kd_unit = '".$_POST['kd_unit'.$i]."'")->row(); 
                        if (count($cekdatadetail) != 0) {
                            
                            $save=true;
                        }else{
                            $data =array(
                            'account'       =>  $_POST['account'],
                            'kd_produk'     =>  $_POST['kdproduk'],
                            'kd_unit'       =>  $_POST['kd_unit'.$i]
                            );
                            $save=$this->db->insert('acc_prod_unit',$data);       
                        }
                }
        }
        
        if ($save) {
            $this->db->trans_commit();
            echo "{success:true}";  
        }else{
            $this->db->trans_rollback();    
            echo "{success:false}";
        }
    }

    public function loaddatabelumterpakai(){
        $kd_produk = $_POST["kd_produk"];
        $deskripsi = $_POST["deskripsi"];
        $kd_unit = $_POST["kd_unit"];
        $nama_unit = $_POST["nama_unit"];
        $param = '';
        if ($kd_produk != '' || $deskripsi != '' || $kd_unit != '' || $nama_unit != '') {
            if ($kd_produk != '') {
                if ($param === '') {
                   $param = "where kd_produk = '".$kd_produk."'";
                }else{
                    $param .= "and kd_produk = '".$kd_produk."'";
                }
            }

            if ($deskripsi != '') {
                if ($param === '') {
                   $param = "where lower(deskripsi) like lower('%".$deskripsi."%')";
                }else{
                    $param .= "and lower(deskripsi) like lower('%".$deskripsi."%')";
                }
              
            }

            if ($kd_unit != '') {
                if ($param === '') {
                   $param = "where kd_unit = '".$kd_unit."'";
                }else{
                    $param .= "and kd_unit = '".$kd_unit."'";
                }
            }

            if ($nama_unit != '') {
                if ($param === '') {
                   $param = "where lower(nama_unit) like lower('%".$nama_unit."%')";
                }else{
                    $param .= "and lower(nama_unit) like lower('%".$nama_unit."%')";
                }
            }

            $select=$this->db->query("select * from getprodukbelumterdaftar() ". $param." limit 400")->result();
            echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
        }else{
            $select=$this->db->query("select * from getprodukbelumterdaftar() limit 400")->result();
            echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
        }
        
    }

    public function loaddatalebihdari1(){
        $kd_produk = $_POST["kd_produk"];
        $deskripsi = $_POST["deskripsi"];
        $kd_unit = $_POST["kd_unit"];
        $nama_unit = $_POST["nama_unit"];
        $account = $_POST["account"];
        $param = '';
        if ($kd_produk != '' || $deskripsi != '' || $kd_unit != '' || $nama_unit != '' || $account != '') {
            if ($kd_produk != '') {
                if ($param === '') {
                   $param = "where kd_produk = '".$kd_produk."'";
                }else{
                    $param .= "and kd_produk = '".$kd_produk."'";
                }
            }

            if ($deskripsi != '') {
                if ($param === '') {
                   $param = "where lower(deskripsi) like lower('%".$deskripsi."%')";
                }else{
                    $param .= "and lower(deskripsi) like lower('%".$deskripsi."%')";
                }
              
            }

            if ($kd_unit != '') {
                if ($param === '') {
                   $param = "where kd_unit = '".$kd_unit."'";
                }else{
                    $param .= "and kd_unit = '".$kd_unit."'";
                }
            }

            if ($nama_unit != '') {
                if ($param === '') {
                   $param = "where lower(nama_unit) like lower('%".$nama_unit."%')";
                }else{
                    $param .= "and lower(nama_unit) like lower('%".$nama_unit."%')";
                }
            }

            if ($account != '') {
                if ($param === '') {
                   $param = "where account like '%".$account."%'";
                }else{
                    $param .= "and account like '%".$account."%'";
                }
            }

            $select=$this->db->query("select * from getproduklebihdari1account() ". $param." limit 400")->result();
            echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
        }else{
            $select=$this->db->query("select * from getproduklebihdari1account()")->result();
            echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
        }
        
    }

    public function loaddataaccountbelumterdaftar(){
        $kd_produk = $_POST["kd_produk"];
        $deskripsi = $_POST["deskripsi"];
        $kd_unit = $_POST["kd_unit"];
        $nama_unit = $_POST["nama_unit"];
        $account = $_POST["account"];
        $param = '';
        if ($kd_produk != '' || $deskripsi != '' || $kd_unit != '' || $nama_unit != '' || $account != '') {
            if ($kd_produk != '') {
                if ($param === '') {
                   $param = "where kd_produk = '".$kd_produk."'";
                }else{
                    $param .= "and kd_produk = '".$kd_produk."'";
                }
            }

            if ($deskripsi != '') {
                if ($param === '') {
                   $param = "where lower(deskripsi) like lower('%".$deskripsi."%')";
                }else{
                    $param .= "and lower(deskripsi) like lower('%".$deskripsi."%')";
                }
              
            }

            if ($kd_unit != '') {
                if ($param === '') {
                   $param = "where kd_unit = '".$kd_unit."'";
                }else{
                    $param .= "and kd_unit = '".$kd_unit."'";
                }
            }

            if ($nama_unit != '') {
                if ($param === '') {
                   $param = "where lower(nama_unit) like lower('%".$nama_unit."%')";
                }else{
                    $param .= "and lower(nama_unit) like lower('%".$nama_unit."%')";
                }
            }

            if ($account != '') {
                if ($param === '') {
                   $param = "where account like '%".$account."%'";
                }else{
                    $param .= "and account like '%".$account."%'";
                }
            }

            $select=$this->db->query("select * from getaccountbelumterdaftar() ". $param." limit 400")->result();
            echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
        }else{
            $select=$this->db->query("select * from getaccountbelumterdaftar()")->result();
            echo"{success:true ,totalrecords:".count($select).", ListDataObj:".json_encode($select)." }";
        }
        
    }
}

?>