<?php

/**
 * @author
 * @copyright
 */
class viewdetma extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('master_data/tblviewdetma');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewdetma->db->where($criteria, null, false);
        }
        $query = $this->tblviewdetma->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $this->load->model('master_data/tblcrudma');

        $ID = $this->db->query("SELECT ma_id FROM acc_bd_ma order by ma_id desc limit 1")->row();
        $ID = $ID->ma_id;
        if ($ID == '0') {
            $ID = 1;
        } else {
            $ID = $ID + 1;
        }
        echo "{success:true, ID: " . $ID . "}";
    }

}

?>