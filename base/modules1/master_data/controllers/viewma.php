<?php

/**
 * @author
 * @copyright
 */
class viewma extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('master_data/tblviewma');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewma->db->where($criteria, null, false);
        }
        $query = $this->tblviewma->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $dana = $Params["DANAID"];
        $id = $Params["MAID"];
        $name = $Params["NAMEMA"];
        $lv = $Params["LVMA"];
        $parent = $Params["PARENTMA"];
        $type = $Params["TYPEMA"];
        $debit = $Params["DEBITMA"];
        $group = $Params["GROUPID"];
        $tahun = $Params["TGL"];
        $pagu = $Params["PAGU"];
        if ($pagu === '') {
            $pagu = 0;
        }
        $criteria = "where ma_id = '".$parent."' and years = $tahun and isdebit = '".$debit."' and ma_type = 'G' and ma_levels < $lv and dana_id = $dana";
        $cekparent = $this->db->query("SELECT * FROM ACC_BD_MATA ".$criteria)->row();
        
        if (count($cekparent) === 0 && $lv != '1' && $type != 'G') {
            echo "{success:true,pesan:'data tidak ada'}";
        }else{
            
            $pagu = str_replace(".", "", $pagu);
            $data = array(
                "dana_id" => $dana,
                "ma_id" => $id,
                "ma_name" => $name,
                "ma_type" => $type,
                "ma_levels" => $lv,
                "ma_parent" => $parent,
                "group_id" => $group,
                "isdebit" => $debit,
                "years" => $tahun
            );
            $this->load->model('master_data/tblcrudma');


            $criteria = "ma_id = '" . $id . "' and isdebit = '" . $debit . "' and years = '" . $tahun . "'";
            $this->tblcrudma->db->where($criteria, null, false);
            $query = $this->tblcrudma->GetRowList(0, 1, "", "", "");

            if ($query[1] == 1) {
                $res = $this->tblcrudma->db->where($criteria, null, false);
                $res = $this->tblcrudma->update($data);
            } else {
                $res = $this->tblcrudma->save($data);
            }

            if ($res) {
                $this->load->model('master_data/tblcrudab');
                $data1 = array(
                    "dana_id" => $dana,
                    "ma_id" => $id,
                    "years" => $tahun,
                    "plan0" => $pagu
                );
                $criteria = "ma_id = '" . $id . "' and dana_id = '" . $dana . "' and years = '" . $tahun . "'";
                $this->tblcrudab->db->where($criteria, null, false);

                $query = $this->tblcrudab->GetRowList(0, 1, "", "", "");
                if ($query[1] == 1) {
                    $res = $this->db->query("select update_nilai_pagu($dana,'". $id."',$tahun,'".$tahun."','".$debit."','".$parent."',$lv,$pagu)");
                    
                } else {
                    $res = $this->tblcrudab->save($data1);
                    $res = $this->db->query("select update_nilai_pagu($dana,'". $id."',$tahun,'".$tahun."','".$debit."','".$parent."',$lv,$pagu)");
                }

                echo "{success:true, pesan:''}";
            }
        }
    }

    public function delete($Params = null) {

        $dana = $Params["DANAID"];
        $id = $Params["MAID"];
        $tahun = $Params["TGL"];
        $debit = $Params["DEBITMA"];
        $lv = $Params["LVMA"];
        $parent =  $Params["PARENTMA"];
        $pagu = $Params["PAGU"];
        $pagu = str_replace(".", "", $pagu);

        $criteria = "where ma_parent = '".$id."' and years = $tahun and isdebit = '".$debit."' and ma_type = 'G' and ma_levels > $lv and dana_id = $dana";
        $cekparent = $this->db->query("SELECT * FROM ACC_BD_MATA ".$criteria)->row();

        if (count($cekparent) != 0) {
            echo '{success: true, pesan: 1}';
        }else{
          $criteria = "ma_id = '" . $id . "' and dana_id = '" . $dana . "' and years = '" . $tahun . "'";

          $this->load->model('master_data/tblcrudma');

          $this->tblcrudma->db->where($criteria, null, false);

          $query = $this->tblcrudma->GetRowList();
          if ($query[1] != 0) {
              $res = $this->db->query("select delete_nilai_pagu($dana,'". $id."',$tahun,'".$tahun."','".$debit."','".$parent."',$lv,$pagu)");
              if ($res) {
                   $this->tblcrudma->db->where($criteria, null, false);
                   $result = $this->tblcrudma->Delete();

                   if ($result > 0) {
                       echo '{success: true, pesan: 0}';
                   } else {
                       echo '{success: false, pesan: 1}';
                   }
              }else {
                       echo '{success: false, pesan: 1}';
                   }                            
             
          } else {
              echo '{success: false, pesan: 0}';
          }  
      }
        
    }

}

?>