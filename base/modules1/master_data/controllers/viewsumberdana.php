<?php

/**
 * @author
 * @copyright
 */
class viewsumberdana extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function read($Params = null) {
        $this->load->model('master_data/tblviewsumberdana');

        if (strlen($Params[4]) > 0) {
            $criteria = str_replace("~", "'", $Params[4]);
            $this->tblviewsumberdana->db->where($criteria, null, false);
        }
        $query = $this->tblviewsumberdana->GetRowList($Params[0], $Params[1], $Params[3], $Params[2], "");
        echo '{success:true, totalrecords:' . $query[1] . ', ListDataObj:' . json_encode($query[0]) . '}';
    }

    public function save($Params = null) {

        $id = $Params["ID"];
        $sd = $Params["SD"];
        $ket = $Params["KET"];


        $data = array(
            "dana_id" => $id,
            "sumber_dana" => $sd,
            "keterangan" => $ket
        );
        $this->load->model('master_data/tblviewsumberdana');


        $criteria = "dana_id = '" . $id . "'";
        $this->tblviewsumberdana->db->where($criteria, null, false);

        $query = $this->tblviewsumberdana->GetRowList(0, 1, "", "", "");

        if ($query[1] == 1) {
            $res = $this->tblviewsumberdana->db->where($criteria, null, false);
            $res = $this->tblviewsumberdana->update($data);
        } else {
            $res = $this->tblviewsumberdana->save($data);
        }
        //  $this->tblviewrencanaasuhandetail->db->where($criteria, null, false);
        echo "{success:true}";
    }

    public function delete($Params = null) {

        $criteria = $Params['query'];

        $this->load->model('master_data/tblviewsumberdana');

        $this->tblviewsumberdana->db->where($criteria, null, false);

        $query = $this->tblviewsumberdana->GetRowList();
        if ($query[1] != 0) {
            $this->tblviewsumberdana->db->where($criteria, null, false);
            $result = $this->tblviewsumberdana->Delete();
            if ($result > 0) {
                echo '{success: true}';
            } else {
                echo '{success: false, pesan: 1}';
            }
        } else {
            echo '{success: false, pesan: 0}';
        }
    }

}

?>