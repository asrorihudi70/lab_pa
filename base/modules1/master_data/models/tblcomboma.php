﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblcomboma extends TblBase {

    function __construct() {
        $this->StrSql = "dana_id,ma_id,years,ma_name,ma_type,ma_levels,ma_dana_parent,ma_parent,group_id,isdebit";
        $this->SqlQuery = "Select Dana_ID, 
                            MA_ID, Years, 
                            MA_ID || ' - ' || MA_NAME as MA_NAME, 
                            MA_TYPE, MA_LEVELS , 
                            MA_DANA_PARENT, MA_PARENT, 
                            GROUP_ID, ISDEBIT from ACC_BD_MATA  ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->DANAID = $rec->dana_id;
        $row->MAID = $rec->ma_id;
        $row->YEAR = $rec->years;
        $row->MANAME = $rec->ma_name;
        $row->MATYPE = $rec->ma_type;
        $row->MALEVEL = $rec->ma_levels;
        $row->MADANAPARENT = $rec->ma_dana_parent;
        $row->MAPARENT = $rec->ma_parent;
        $row->GROUPID = $rec->group_id;
        $row->DEBIT = $rec->isdebit;
        return $row;
    }

}

class Rowncp {

    public $DANAID;
    public $MAID;
    public $MANAME;
    public $MATYPE;
    public $MALEVEL;
    public $MADANAPARENT;
    public $MAPARENT;
    public $GROUPID;
    public $DEBIT;
    public $YEAR;

}
