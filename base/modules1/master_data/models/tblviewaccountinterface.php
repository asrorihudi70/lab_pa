﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewaccountinterface extends TblBase {

    function __construct() {
        $this->StrSql = "account,kd_produk,deskripsi,kd_unit,nama_unit";
        $this->SqlQuery = "SELECT a.account,a.Kd_Produk, p.Deskripsi, a.Kd_Unit, u.Nama_Unit 
                            FROM (
                                ACC_PROD_UNIT a 
                                INNER JOIN UNIT u ON a.Kd_Unit = u.Kd_Unit
                                 ) 
                            INNER JOIN Produk p ON a.Kd_Produk = p.Kd_Produk ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->ACCOUNT = $rec->account;
        $row->KODEPRODUK = $rec->kd_produk;
        $row->NAMAPRODUK = $rec->deskripsi;
        $row->KDUNIT = $rec->kd_unit;
        $row->UNIT = $rec->nama_unit;
        return $row;
    }

}

class Rowncp {
    
    public $ACCOUNT;
    public $KODEPRODUK;
    public $NAMAPRODUK;
    public $KDUNIT;
    public $UNIT;

}
