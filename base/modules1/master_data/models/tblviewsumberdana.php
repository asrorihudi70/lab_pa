﻿<?php

/**
 * @author HDHT
 * @copyright 2015
 */
class tblviewsumberdana extends TblBase {

    function __construct() {
        $this->TblName = 'acc_bd_dana';
        TblBase::TblBase(true);

        $this->SqlQuery = "";
    }

    function FillRow($rec) {
        $row = new Rowncp;

        $row->ID = $rec->dana_id;
        $row->SUMBER = $rec->sumber_dana;
        $row->KET = $rec->keterangan;
        return $row;
    }

}

class Rowncp {

    public $ID;
    public $SUMBER;
    public $KET;

}
