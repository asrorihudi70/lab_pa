<?php

class viewgridresep extends MX_Controller{

	public function __construct(){
    	parent::__construct();
    }

	public function index()
	{
    	$this->load->view('main/index');
    }

    public function read($Params=null){
		try{
		  $result = $this->db->query("SELECT A.kd_pasien,C.kd_prd,C.nama_obat,B.jumlah,D.satuan,B.cara_pakai,E.nama as kd_dokter,  B.jumlah as jml_stok_apt,b.urut,A.id_mrresep,
		  case when(B.verified = 0) then 'Disetujui' else 'Tdk Disetujui' end as verified,
		  case when a.order_mng = 'f' then 'Belum Dilayani' when a.order_mng = 't' then 'Dilayani' end as order_mng, 
		  case when B.order_mng = 'f' then 'Belum Dilayani' when B.order_mng = 't' then 'Dilayani' end as order_mng_det
		  ,B.racikan FROM MR_RESEP A
				LEFT JOIN MR_RESEPDTL B ON A.ID_MRRESEP = B.ID_MRRESEP
				LEFT JOIN APT_OBAT C ON C.KD_PRD = B.KD_PRD  
				LEFT JOIN DOKTER E ON E.kd_dokter = B.kd_dokter
				LEFT JOIN APT_SATUAN D ON D.kd_satuan = C.kd_satuan WHERE ".$Params[4]."  order by b.urut")->result();
			
		}catch(Exception $o){
			echo 'Debug  fail ';
		}
		echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
	}
	
	/*
	public function save($Params=null)
	{
		$kdpasien = $Params['KdPasien'];
		$urutmasuk = $Params['UrutMasuk'];
		$tglmasuk = $Params['Tgl'];
		$kdunit = $Params['KdUnit'];
		$list = $Params['List'];
		$anamnese = $Params['Anamnese'];
		$catatan= $Params['Catatan'];
		$idkonpas = $this->getIdkonpas($kdpasien,$tglmasuk,$urutmasuk,$kdunit);
		
		$data = array(
		"id_konpas" => $idkonpas,
		"kd_pasien" => $kdpasien,
		"tgl_masuk" => $tglmasuk,
		"kd_unit" => $kdunit,
		"urut_masuk" => $urutmasuk
		);
		
		$this->load->model("poliklinik/tb_konpas");
		$criteria = "kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND kd_unit = '$kdunit' AND urut_masuk = $urutmasuk";
		 $query =  $this->tb_konpas->db->where($criteria, null, false);
		  $query = $this->tb_konpas->GetRowList( 0,1, "", "","");
            if ($query[1]==0)
            {
        	 $save = $this->tb_konpas->save($data);		
            }else{
		     echo "";
			}
	
		$this->load->model("rawat_jalan/tb_kunjungan_pasien");
		$update = $this->tb_kunjungan_pasien->db->where($criteria, null, false);
		$dataupdate = array("anamnese"=>$anamnese,"cat_fisik"=>$catatan);
		$update = $this->tb_kunjungan_pasien->update($dataupdate);
		
		
		$cut = explode('<>',$list);
		for($i=0;$i<count($cut)-1;$i++)
		{
			if($cut[$i] == '')
			{
				echo "";
			}
			else
			{
			$data = explode('::',$cut[$i]);
			
			$kondisi = array(
			"id_kondisi" => $data[0],
			"id_konpas" => $idkonpas,
			"hasil" => $data[1]
			);
			$this->load->model("poliklinik/tb_konpasdtl");
			$filter = "id_konpas = '$idkonpas' AND id_kondisi = '".$data[0]."'";
		
				 $detaildata = $this->tb_konpasdtl->db->where($filter, null, false);
				 $detaildata = $this->tb_konpasdtl->GetRowList( 0,1, "", "","");
				 if ($detaildata[1]==0)
				 {
				 $save = $this->tb_konpasdtl->save($kondisi);		
				 }else{
				 $hasil = array("hasil"=>$data[1]);	 
				 $updatedetail = $this->tb_konpasdtl->db->where($filter, null, false);
				 $updatedetail = $this->tb_konpasdtl->update($hasil);
				 }
			}
		
		}
	
		
		$success = "sukses";
		
		echo '{success: true}';

		
	}
	
	private function getIdkonpas($kdpasien,$tglmasuk,$urutmasuk,$kdunit)
	{
		$date= date('Ymd');
		$query = $this->db->query("select id_konpas from mr_konpas order by id_konpas desc limit 1");
		if($query->num_rows() == 0)
		{
		$newid=$date."0001";
		}
		
		else
		{
		$getcurrent = $this->db->query("select id_konpas from mr_konpas where kd_pasien = '$kdpasien' AND tgl_masuk = '$tglmasuk' AND urut_masuk = $urutmasuk AND kd_unit = '$kdunit'");
		if($getcurrent->num_rows != 0)
		{
			$res = $getcurrent->row();
			$newid = $res->id_konpas;
		}
		else
		{	
		$result = $query->row();	
		$lastid = $result->id_konpas;
		$cutid = substr($lastid, -4);
		$newno = (int) $cutid +1;
		$newid = $date.str_pad($newno,4,"00000",STR_PAD_LEFT);
		}
		}
		return $newid;
	}

*/
 }

?>