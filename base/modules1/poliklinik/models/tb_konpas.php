<?php

class tb_konpas extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_konpas';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkondisi;
        $row->ID_KONPAS=$rec->id_konpas;
        $row->KD_PASIEN=$rec->kd_pasien;
        $row->TGL_MASUK=$rec->tgl_masuk;
        $row->URUT_MASUK=$rec->urut_masuk;
        $row->KD_UNIT=$rec->kd_unit;
        return $row;
    }
}

class Rowviewkondisi
{
    public $ID_KONPAS;
    public $KD_PASIEN;
    public $TGL_MASUK;
    public $URUT_MASUK;
    public $KD_UNIT;
}

?>
