<?php

class tb_konpasdtl extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_konpasdtl';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkondisidtl;
        $row->ID_KONPAS=$rec->id_konpas;
        $row->ID_KONDISI=$rec->id_kondisi;
        $row->HASIL=$rec->hasil;
        return $row;
    }
}

class Rowviewkondisidtl
{
    public $ID_KONPAS;
    public $ID_KONDISI;
    public $HASIL;
}

?>
