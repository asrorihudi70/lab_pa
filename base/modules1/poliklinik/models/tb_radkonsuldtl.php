<?php

class tb_radkonsuldtl extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_radkonsuldtl';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewkondisidtl;
        $row->ID_RADKONSUL=$rec->id_radkonsul;
        $row->KD_DOKTER=$rec->kd_dokter;
        $row->KD_PRODUK=$rec->kd_produk;
        return $row;
    }
}

class Rowviewkondisidtl
{
    public $ID_RADKONSUL;
    public $KD_DOKTER;
    public $KD_PRODUK;
}

?>
