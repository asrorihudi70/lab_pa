<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class file_management extends MX_Controller {

	public function __construct(){
		parent::__construct();
			$this->load->library('session');
			$this->load->library('result');
			$this->load->library('common');
	}	 

	public function index(){
		$this->load->view('main/index');            		
	}

	public function gallery($medrec = null){
		$path = "./Img Asset/RAD";

		$files = $this->db->query("SELECT * from rad_document WHERE file_name like '".$medrec."%'");
		$path_ = $this->db->query("SELECT * FROM sys_setting where key_data = 'rad_path_hasil'");

		if ($path_->num_rows() > 0) {
			$path = $path_->row()->setting;
		}

		$this->load->view('rad/gallery', 
			array(
				'files' => $files,
				'path'  => $path,
			)
		);
	}


	public function upload_hasil(){
		$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|bmp';
		$config['max_size']      = '3000';
		// $config['upload_path']   = 'C:\Users\AlGhazali\Downloads';
		// $path = $this->db->query("SELECT file_location FROM zusers WHERE kd_user = '".$this->session->userdata['user_id']['id']."'")->row()->file_location;
		$path = $this->db->query("SELECT * FROM sys_setting where key_data = 'rad_path_hasil'");
		if ($path->num_rows() > 0) {
			$config['upload_path'] = $path->row()->setting;
		}else{
			$config['upload_path'] = './Img Asset/RAD';
		}
		// var_dump($config);
		$data_pasien = explode("_", $this->input->post('file_name'));

		$transaksi = $this->db->query("SELECT t.no_transaksi, t.tgl_transaksi, t.kd_unit FROM 
				transaksi t 
			INNER JOIN
			unit u ON u.kd_unit = t.kd_unit 
		WHERE t.kd_pasien = '".$data_pasien[0]."' and t.tgl_transaksi = '".$data_pasien[1]."' and u.kd_bagian = '5'");
		
		$response = array();
		if ($transaksi->num_rows() > 0) {

			$path_parts = pathinfo($_FILES["file"]["name"]);
			$extension = $path_parts['extension'];

			// echo $_FILES["file"]["size"];die();
			$name_file = $data_pasien[0]."_".$transaksi->row()->no_transaksi."_".time();
			$config['file_name'] = $name_file.".".$extension;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
	
			if ( ! $this->upload->do_upload('file')){
				$response['status'] = false;
				$response['error']  = $this->upload->display_errors();
			}else{
				$paramsInsert = array(
					'file_name' 	=> $name_file,
					'extension' 	=> $extension,
					'path' 			=> $config['upload_path'],
					'size' 	 		=> round(((int)$_FILES["file"]["size"]/1028), 1),
					'date_upload' 	=> date('Y-m-d'),
				);

				$this->db->insert('rad_document', $paramsInsert);
				$response['status'] = true;
			}
		}else{
			$response['status'] = false;
		}
		echo json_encode($response);

	}
}
?>