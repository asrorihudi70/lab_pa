<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
			 $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
	#LAPORAN PENERIMAAN PER JENIS PENERIMAAN
   	public function cetakLaporan1(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Rekapitulasi Penerimaan Radiologi';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 1){
				$tipe= 'Perseorangan';
			}else if($kel_pas ==2){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19','09','15','20','10','16','33')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('09','15','20')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('10','16','33')";
			$nama_asal_pasien = 'IGD';
		}
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad ==2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select py.Uraian, 
										case when py.Kd_pay in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end as UT, 
										case when py.Kd_pay not in ('DP','IA','TU') And py.Kd_pay not in ('DC') Then Sum(Jumlah) Else 0 end as PT, 
										case when py.Kd_pay in ('DC') Then Sum(Jumlah) Else 0 end as SSD
											From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir  
																INNER JOIN unit on unit.kd_unit=t.kd_unit  
																INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
											INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
											INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
											INNER JOIN Customer c on c.kd_customer=k.kd_customer  
											LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
										Where 
										$q_shift
										$kriteria_bayar
										".$crtiteriaUnitRad."
										$crtiteriaAsalPasien
										$jniscus
										$customerx
										 
										and unit.kd_bagian=5  
										Group By py.kd_pay, py.Uraian  Order By py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Uang Tunai</th>
					<th align="center">Jumlah Piutang</th>
					<th align="center">Jumah Subsidi</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($line->ut,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->pt,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($line->ssd,0, "." , ".").' &nbsp;</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				$total_ut = $total_ut + $line->ut;
				$total_pt = $total_pt + $line->pt;
				$total_ssd = $total_ssd + $line->ssd;
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="2"><b>Total &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		// echo $html;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('any name you want'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_per_jenis_penerimaan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Laporan Rekapitulasi Penerimaan Radiologi',$html);	
		}
		
		//$html.='</table>';
		
   	}
	
	#LAPORAN PENERIMAAN PER PASIEN
	public function cetakLaporan2(){
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);	
		
   		$title='Laporan Rekapitulasi Penerimaan Radiologi';
		$tgl_awal_i = str_replace("/","-", $param->start_date);
		$tgl_akhir_i = str_replace("/","-",$param->last_date) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		
		//$type_file = $param->type_file;
		$html='';	
   		
	
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$kriteria_bayar = " And (db.Kd_Pay in (".$tmpKdPay.")) ";
   		
		//jenis pasien
		 $kel_pas_t = $param->pasien;
		if($kel_pas_t == 'Semua'){
			$kel_pas = 0;
		}else{
			$kel_pas = $kel_pas_t;
		}
		
		if($kel_pas== 0)
		{
			$jniscus=" ";
			$tipe = 'Semua';
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$kel_pas." ";
			if($kel_pas == 1){
				$tipe= 'Perseorangan';
			}else if($kel_pas == 2){
				$tipe= 'Perusahaan';
			}else if($kel_pas == 3){
				$tipe= 'Asuransi';
			}
		}
		
		//kd_customer
		if($kel_pas!= 0){
			$kel_pas_d = $param->kd_customer;
			if($kel_pas_d!='' || $kel_pas_d != 'Semua'){
				$customerx=" And Ktr.kd_Customer='".$kel_pas_d."' ";
				$customer=$this->db->query("Select * From customer Where Kd_customer='$kel_pas_d'")->row()->customer;
		
			}else{
				$customerx=" ";
				$customer ='Semua';
			}
		}else{
			$customerx=" ";
			$customer='Semua ';
		}
		
		
		$kduser = $this->session->userdata['user_id']['id'];
		$carikd_unit=$this->db->query("select kd_unit from zusers where kd_user='".$kduser."'")->row()->kd_unit;
		$key="'5";
		$kd_unit='';
		if (strpos($carikd_unit,","))
		{
			
			$pisah_kata=explode(",",$carikd_unit);
			for($i=0;$i<count($pisah_kata);$i++)
			{
				$cek_kata=stristr($pisah_kata[$i],$key);
				if ($cek_kata != '' || $cek_kata != null)
				{
					$kd_unit=$cek_kata;
				}
			}
			
		}else
		{
			$kd_unit= $carikd_unit;
		}
		//jenis pasien
		$asal_pasien = $param->asal_pasien;
		$KdKasirRwj=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='1'")->row()->kd_kasir;
		$KdKasirRwi=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='2'")->row()->kd_kasir;
		$KdKasirIGD=$this->db->query("Select * From Kasir_Unit Where Kd_unit=$kd_unit and kd_asal='3'")->row()->kd_kasir;
		
		if($asal_pasien == 0 || $asal_pasien == 'Semua'){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19','09','15','20','10','16','33')";
			$nama_asal_pasien = 'Semua Pasien';
		} else if($asal_pasien == 1){
			$crtiteriaAsalPasien="and t.kd_kasir in ('14','04','19')";
			$nama_asal_pasien = 'RWJ';
		} else if($asal_pasien == 2){
			$crtiteriaAsalPasien="and t.kd_kasir in('09','15','20')";
			$nama_asal_pasien = 'RWI';
		}else if($asal_pasien == 3){
			$crtiteriaAsalPasien="and t.kd_kasir in('10','16','33')";
			$nama_asal_pasien = 'IGD';
		}
		$unit_rad = $param->unit_rad;
		if($unit_rad == 'Semua'){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		}else if($unit_rad == 0){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51', '52', '53')";
			$nama_unit_rad = 'Semua Unit Radiologi';
		} else if($unit_rad == 1){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('51')";
			$nama_unit_rad = 'Radiologi IGD';
		} else if($unit_rad == 2){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('53')";
			$nama_unit_rad = 'Radiologi Pav';
		}else if($unit_rad == 3){
			$crtiteriaUnitRad="and T.KD_UNIT IN ('52')";
			$nama_unit_rad = 'Radiologi Umum';
		}
		//shift
		$q_shift='';
   		$t_shift='';
   		if($param->shift0=='true'){
   			$q_shift=" ((db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='Semua Shift';
   		}else{
   			if($param->shift1=='true' || $param->shift2=='true' || $param->shift3=='true'){
   				$s_shift='';
   				if($param->shift1=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($param->shift2=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($param->shift3=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$param->start_date."'  And '".$param->last_date."'  And db.Shift In (".$s_shift.")";
   				if($param->shift3=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($param->start_date . ' +1 day'))."'  And '".date('Y-m-d', strtotime($param->last_date . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='Shift '.$s_shift;
   				//$q_shift="And ".$q_shift;
   			}
   		}
		
		
		$query = $this->db->query("Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama, py.Uraian,  
									case when max(py.Kd_pay) in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end AS UT, 
									case when max(py.Kd_pay) in ('DC') Then Sum(Jumlah) Else 0 end AS SSD,  
									case when max(py.Kd_pay) not in ('DC') And  max(py.Kd_pay) not in ('DP','IA','TU') Then Sum(Jumlah) Else 0 end  AS PT
										From (Transaksi t LEFT JOIN unit_asal ua on t.no_transaksi=ua.no_transaksi  and t.kd_kasir=ua.kd_kasir 
												INNER JOIN unit on unit.kd_unit=t.kd_unit  
												INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi)
										INNER JOIN  Payment py On py.Kd_pay=db.Kd_Pay  
										INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
										INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
										INNER JOIN Customer c on c.kd_customer=k.kd_customer  
										LEFT JOIN Kontraktor ktr On ktr.kd_Customer=c.Kd_Customer  
											Where  
												$q_shift
												$kriteria_bayar
												".$crtiteriaUnitRad."
												$crtiteriaAsalPasien
												$jniscus
												$customerx
												and unit.kd_bagian=5
															
								Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian,  ps.Nama
									Order By ps.Nama, db.No_Transaksi, py.Uraian")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="6">'.$title.'<br>
					</tr>
					<tr>
						<th colspan="6">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th colspan="6">'.$t_shift.'</th>
					</tr>
					<tr>
						<th colspan="6">Kelompok '.$tipe.' - '.$nama_asal_pasien.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table class="t1" border = "1" cellpadding="2">
			<thead>
				 <tr>
					<th align="center">No</th>
					<th align="center">No. Trans</th>
					<th align="center">No. Medrec</th>
					<th align="center">Nama Pasien</th>
					<th align="center">Jenis Penerimaan</th>
					<th align="center">Jumlah Total</th>
				  </tr>
			</thead><tbody>';
		if(count($query) > 0) {
			$no=0;
			$total_ut=0;
			$total_pt=0;
			$total_ssd=0;
			$total_jumlah=0;
			foreach ($query as $line) 
			{
				$jumlah = $line->ut + $line->pt + $line->ssd;
				$no++;
				$html.='<tr>
								<td align="center">'.$no.'</td>
								<td>'.$line->no_transaksi.'</td>
								<td>'.$line->kd_pasien.'</td>
								<td>'.$line->nama.'</td>
								<td>'.$line->uraian.'</td>
								<td width="" align="right">'.number_format($jumlah,0, "." , ".").' &nbsp;</td>
							</tr>';
				
				if($line->ut != 0)
				{
					$total_ut = $total_ut + $line->ut;
				}else if($line->pt != 0)
				{
					$total_pt = $total_pt + $line->pt;
				}else if($line->ssd != 0)
				{
					$total_ssd = $total_ssd + $line->ssd;
				}
				
				$total_jumlah = $total_jumlah + $jumlah;
			}
			
			$html.='<tr>
						<td width="" align="right" colspan="5"><b>Jumlah &nbsp;</b></td>
						<td width="" align="right">'.number_format($total_jumlah,0, "." , ".").' &nbsp;</td>
					</tr>';
			$html.='<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Tunai &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ut,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Piutang &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_pt,0, "." , ".").' &nbsp;</td>
					</tr>
					<tr>
						<td width="" align="right" colspan="5"><b><i>Total Penerimaan Subsidi &nbsp;</i></b></td>
						<td width="" align="right">'.number_format($total_ssd,0, "." , ".").' &nbsp;</td>
					</tr>';
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		
		$html.='</tbody></table>';
		$type_file = $param->type_file;
		$prop=array('foot'=>true);
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:G90');
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment right
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			
			# Fungsi Autosize
            for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
			# END Fungsi Autosize

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('any name you want'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Laporan Rekapitulasi Penerimaan Radiologi',$html);	
		}
		
		// echo $html;
		
		//$html.='</table>';
   	}
}
?>