<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewpenjasrad extends MX_Controller {

    public function __construct()
    {
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }
    /* coding untuk load pasien rad dari sql server
   public function read($Params=null)
    {
        try
        {   
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('lab/tblviewpenjaslab');
            $kriteratambahan = explode("||", $Params[4]);
            $kreteria_1 = $kriteratambahan[0];
            $kreteria_2 = $kriteratambahan[1];

            // $kreteria_1 = '';
            // $kreteria_2 = '';
            // if (count($kriteratambahan) = 1) {
            //   // for ($i=0; $i < $count($kriteratambahan); $i++) { 
            //   //   $kreteria_1 = $kriteratambahan[$i];
            //   //   $kreteria_2 = '';
            //   // }              
            // }else{
            //   // for ($i=0; $i < $count($kriteratambahan); $i++) { 
            //   //  // $kreteria_1 = $kriteratambahan[$i];
            //   //  // $kreteria_2 = $kriteratambahan[$i];
            //   // }
            // }


			     if (strlen($Params[4])!== 0)
            {

            }else
            {
                    
      			  $Params[4] = "u.kd_bagian = 5  
      			               and tr.tgl_transaksi >='".$tgl_tampil."' and tr.tgl_transaksi <= '".$date."'
      										 left(u.kd_unit,1) IN ('2','3')
      										 BY tr.tgl_transaksi desc, tr.no_transaksi";
					 }
            $result =_QMS_Query("SELECT TOP 10 * from(
                                  SELECT pasien.kd_pasien, pasien.telepon, u.kd_bagian, tr.no_transaksi, nama_pasien = pasien.NAMA, pasien.Alamat, kunjungan.urut_masuk
                                  , kunjungan.TGL_MASUK AS MASUK, tgl_lahir = convert(varchar(10), pasien.tgl_lahir,111), pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, kunjungan.no_sjp
                                  , dokter.nama AS DOKTER, dokter.kd_dokter, dokterasal.nama as dokter_asal, dokterasal.kd_dokter as kd_dokter_asal, tr.kd_unit
                                  , uasal.kd_unit as kd_unit_asal, uasal.nama_unit as nama_unit_asal, tr.kd_Kasir, tgl_transaksi = convert(varchar(10), tr.tgl_transaksi,111)
                                  , tr.posting_transaksi, pasien.handphone, tr.co_status, tr.kd_user, u.nama_unit as nama_unit, customer.customer, 
                                  case when kontraktor.jenis_cust=0 then 'Perseorangan' 
                                  when kontraktor.jenis_cust=1 then 'Perusahaan' 
                                  when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,
                                  case when 
                                   (select sum(harga*qty) as tagihan from detail_transaksi where kd_kasir = tr.kd_kasir AND no_transaksi = tr.no_transaksi group by kd_kasir, no_transaksi) 
                                   = 
                                   (select sum(jumlah) as bayar from detail_bayar where kd_kasir = tr.kd_kasir AND no_transaksi = tr.no_transaksi group by kd_kasir, no_transaksi) 
                                  then 't' else 'f' end as lunas,
                                  kunjungan.no_foto_rad 
                                  FROM pasien 
                                  INNER JOIN (
                                  ( kunjungan 
                                  INNER JOIN ( transaksi tr INNER JOIN unit u on u.kd_unit=tr.kd_unit
                                  ) on kunjungan.kd_pasien=tr.kd_pasien AND kunjungan.kd_unit=tr.kd_unit AND kunjungan.tgl_masuk=tr.tgl_transaksi 
                                  and kunjungan.urut_masuk=tr.urut_masuk 
                                  INNER JOIN customer on customer.kd_customer = kunjungan.kd_customer 
                                  ) INNER JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter 
                                  INNER JOIN unit_asal ua ON tr.no_transaksi=ua.no_transaksi AND tr.kd_kasir=ua.kd_kasir 
                                  INNER JOIN kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                  INNER JOIN transaksi trasal ON trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal 
                                  INNER JOIN kunjungan kjasal ON kjasal.kd_pasien = trasal.kd_pasien and kjasal.kd_unit = trasal.kd_unit and kjasal.urut_masuk = trasal.urut_masuk 
                                  and kjasal.tgl_masuk = trasal.tgl_transaksi 
                                  INNER JOIN dokter dokterasal ON kjasal.kd_dokter=dokterasal.kd_dokter 
                                  INNER JOIN UNIT uAsal ON trasal.kd_unit=uAsal.kd_unit 
                                  )ON kunjungan.kd_pasien=pasien.kd_pasien
                                      WHERE  ".$kreteria_1."
                                      ) as data
                                      ".$kreteria_2."
                                      ORDER BY tgl_transaksi desc, no_transaksi
                                      ")->result();

                    $list=array();
                    foreach ($result as $data) {
                       $o=array();

                       $o['KD_PASIEN'] = $data->kd_pasien;
                       $o['NO_TRANSAKSI'] = $data->no_transaksi;
                       $o['NAMA'] = $data->nama_pasien;
                       $o['ALAMAT'] = $data->Alamat;
                       $o['MASUK'] = $data->MASUK;
                       $o['TGL_LAHIR'] = $data->tgl_lahir;
                       $o['JENIS_KELAMIN'] = $data->jenis_kelamin;
                       $o['GOL_DARAH'] = $data->gol_darah;
                       $o['KD_CUSTOMER'] = $data->kd_Customer;
                       $o['CUSTOMER'] = $data->customer;
                       $o['DOKTER'] = $data->DOKTER;
                       $o['KD_DOKTER'] = $data->kd_dokter;
                       $o['DOKTER_ASAL'] = $data->dokter_asal;
                       $o['KD_DOKTER_ASAL'] = $data->kd_dokter_asal;
                       $o['KD_UNIT'] = $data->kd_unit;
                       $o['KD_UNIT_ASAL'] = $data->kd_unit_asal;
                       $o['KD_KASIR'] = $data->kd_Kasir;
                       $o['TGL_TRANSAKSI'] = $data->tgl_transaksi;
                       $o['POSTING_TRANSAKSI'] = $data->posting_transaksi;
                       $o['CO_STATUS'] = $data->co_status;
                       $o['KD_USER'] = $data->kd_user;
                       $o['NAMA_UNIT_ASLI'] = $data->nama_unit;
                       $o['NAMA_UNIT_ASAL'] = $data->nama_unit_asal;
                       $o['KELPASIEN'] = $data->kelpasien;
                       $o['LUNAS'] = $data->lunas;
                       $o['URUT'] = $data->urut_masuk;
                       $o['HP'] = $data->handphone;
                       $o['TELEPON'] = $data->telepon;
                       $o['SJP'] = $data->no_sjp;
                       $o['NO_FOTO'] = $data->no_foto_rad;
                       $list[]=$o;
                    }
        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($list) . '}';
    }
   */ 

     public function read($Params=null)
    {
        try
        {   
            $date = date("Y-m-d");
            $tgl_tampil = date('Y-m-d',strtotime('-3 day',strtotime($date)));
            $this->load->model('rad/tblviewpenjasrad');
      
      if (strlen($Params[4])!== 0)
            {
        
                    $this->db->where(str_replace("~", "","".$Params[4]." limit 20"  ) ,null, false) ;
                    $res = $this->tblviewpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
            }else{
                    //echo $tgl_tampil;and left(u.kd_unit,1) IN ('2','3')
            $h=$this->db->where(str_replace("~", "","u.kd_bagian = 5  
                      and tr.tgl_transaksi >='".$tgl_tampil."' and tr.tgl_transaksi <= '".$date."'
                      and left(u.kd_unit,1) IN ('2','3')
                      ORDER BY tr.tgl_transaksi desc, tr.no_transaksi limit 10"  ) ,null, false) ;
                      
                    $res = $this->tblviewpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
          
                  }
            /*  if (strlen($Params[4])!== 0)
            {
                    $this->db->where($Params[4] ,null , false) ;
                    $res = $this->tblviewpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
            }else{
                    $kriteria = "tr.tgl_transaksi >= '".$date."' and tr.tgl_transaksi <= '".$tgl_tampil."' and left(u.kd_unit,1) IN ('2','3') and tr.co_Status='0' ORDER BY tr.no_transaksi desc limit 50";
                    $this->db->where($kriteria ,null, false) ;
                    $res = $this->tblviewpenjasrad->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
                  } */
        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';


    }
	
  public function getPasien(){
		// $date = date("Y-m-d");
		// $tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
		// $unit=$_POST['unit'];
		// if($unit == ''){
		// 	$kd_unit ="and left(u.kd_unit,1) IN ('2')";
		// } else{
		// 	if($unit == 'RWI'){
		// 		$kd_unit ="and left(trasal.kd_unit, 1) = '1'";
		// 	} else if($unit == 'Langsung'){
		// 		$kd_unit ="and left(tr.kd_pasien, 2) = 'RD'";
		// 	} else if($unit == 'IGD'){
		// 		$kd_unit ="and left(u.kd_unit,1) IN ('3')";
		// 	} else{
		// 		$kd_unit ="and left(u.kd_unit,1) IN ('2')";
		// 	}
		// }
		// $kriteria=$_POST['kriteria'];
		//   $result = $this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, pasien.handphone, pasien.telepon,kunjungan.no_sjp,
  //             								kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah,  kunjungan.kd_Customer, 
  //                                                   dokter.nama AS DOKTER, dokter.kd_dokter, tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif,
  //                                                   to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
  //                                                   tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial,nginap.akhir,
  //                                                   case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,kunjungan.no_foto_rad
  //                                               FROM pasien 
  //                                                   LEFT JOIN (
  //                                                       ( kunjungan  
  //                                                        LEFT join ( transaksi tr 
  //                                                                   INNER join unit u on u.kd_unit=tr.kd_unit)  
  //                                                        on kunjungan.kd_pasien=tr.kd_pasien 
  //                                                        and kunjungan.kd_unit= tr.kd_unit 
  //                                                        and kunjungan.tgl_masuk=tr.tgl_transaksi 
  //                                                        and kunjungan.urut_masuk = tr.urut_masuk

  //                                                        LEFT join customer on customer.kd_customer = kunjungan.kd_customer
  //                                                        left join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
  //                                                        and nginap.kd_unit=kunjungan.kd_unit 
  //                                                        and nginap.tgl_masuk=kunjungan.tgl_masuk 
  //                                                        and nginap.urut_masuk=kunjungan.urut_masuk 
  //                                                        and nginap.akhir='t'
  //                                                        inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
  //                                                        inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
  //                                                       )   
  //                                                       LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
  //                                                       LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
  //                                                       LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
  //                                                       LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
  //                                                   )
  //                                                   ON kunjungan.kd_pasien=pasien.kd_pasien
  //       						WHERE $kriteria")->result();
      
    $date = date("Y-m-d");
    $tgl_tampil = date('Y-m-d',strtotime('-4 day',strtotime($date)));
    $unit=$_POST['unit'];
    if($unit == ''){
      $kd_unit ="and left(u.kd_unit,1) IN ('2')";
    } else{
      if($unit == 'RWI'){
        $kd_unit ="and left(trasal.kd_unit, 1) = '1'";
      } else if($unit == 'Langsung'){
        $kd_unit ="and left(tr.kd_pasien, 2) = 'RD'";
      } else if($unit == 'IGD'){
        $kd_unit ="and left(u.kd_unit,1) IN ('3')";
      } else{
        $kd_unit ="and left(u.kd_unit,1) IN ('2')";
      }
    }

    //mendapatkan nilai kode unit nginap terakhir
      $query_kode_unit_nginap = 'tr.kd_unit';
        if($unit == 'RWI'){
          $query_kode_unit_nginap  = 'nginap.kd_unit_kamar as kd_unit';
        } else{
          $query_kode_unit_nginap = 'tr.kd_unit';
        }

      //mendapatkan nama kamar nginap terakhir
      $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        if($unit == 'RWI'){
          $query_nama_unit_nginap = 'kamar.nama_kamar as nama_unit_asli';
        } else{
          $query_nama_unit_nginap = 'u.nama_unit as nama_unit_asli';
        }

      $kriteria=$_POST['kriteria'];
      $result = $this->db->query("SELECT pasien.kd_pasien, tr.no_transaksi, pasien.nama, pasien.Alamat, pasien.handphone, kunjungan.no_sjp,
                        kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, 
                                              dokter.nama AS DOKTER, dokter.kd_dokter, ".$query_kode_unit_nginap.", ".$query_nama_unit_nginap.", tr.kd_Kasir, tarif_cust.kd_tarif,
                                              to_char(tr.tgl_transaksi,'dd-mm-yyyy') as tgl,tr.tgl_transaksi, tr.posting_transaksi,
                                              tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, nginap.no_kamar, nginap.kd_spesial, nginap.akhir,
                                              case when kontraktor.jenis_cust=0 then 'Perseorangan' when kontraktor.jenis_cust=1 then 'Perusahaan' when kontraktor.jenis_cust=2 then 'Asuransi' end as kelpasien,kunjungan.no_foto_rad
                                          FROM pasien 
                                              LEFT JOIN (
                                                  ( kunjungan  
                                                    LEFT join ( transaksi tr 
                                                              INNER join unit u on u.kd_unit=tr.kd_unit)  
                                                      on kunjungan.kd_pasien=tr.kd_pasien 
                                                        and kunjungan.kd_unit= tr.kd_unit 
                                                        and kunjungan.tgl_masuk=tr.tgl_transaksi 
                                                        and kunjungan.urut_masuk = tr.urut_masuk

                                                    LEFT join customer on customer.kd_customer = kunjungan.kd_customer
                                                    LEFT join nginap on nginap.kd_pasien=kunjungan.kd_pasien 
                                                      and nginap.kd_unit=kunjungan.kd_unit 
                                                      and nginap.tgl_masuk=kunjungan.tgl_masuk 
                                                      and nginap.urut_masuk=kunjungan.urut_masuk 
                                                      and nginap.akhir='t'
                                                    LEFT JOIN kamar ON kamar.no_kamar=nginap.no_kamar AND kamar.kd_unit=nginap.kd_unit_kamar
                                                    inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer
                                                    inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer
                                                  )   
                                                  LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter
                                                  LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir
                                                  LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                                                  LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit
                                              )
                                              ON kunjungan.kd_pasien=pasien.kd_pasien
                                              WHERE $kriteria")->result();
    $arrayres=array();
      for($i=0;$i<count($result);$i++){
        $arrayres[$i]['KD_PASIEN'] = $result[$i]->kd_pasien;
        $arrayres[$i]['NO_TRANSAKSI'] = $result[$i]->no_transaksi;
        $arrayres[$i]['NAMA'] = $result[$i]->nama;
        $arrayres[$i]['ALAMAT'] = $result[$i]->alamat;
        $arrayres[$i]['TGL_LAHIR'] = $result[$i]->tgl_lahir;
        $arrayres[$i]['JENIS_KELAMIN'] = $result[$i]->jenis_kelamin;
        $arrayres[$i]['KD_CUSTOMER'] = $result[$i]->kd_customer;
        $arrayres[$i]['DOKTER'] = $result[$i]->dokter;
        $arrayres[$i]['KD_DOKTER'] = $result[$i]->kd_dokter;
        $arrayres[$i]['KD_UNIT'] = $result[$i]->kd_unit;
        $arrayres[$i]['NAMA_UNIT_ASLI'] = $result[$i]->nama_unit_asli;
        $arrayres[$i]['KD_KASIR'] = $result[$i]->kd_kasir;
        $arrayres[$i]['KD_TARIF'] = $result[$i]->kd_tarif;
        $arrayres[$i]['TGL'] = $result[$i]->tgl;
        $arrayres[$i]['TGL_TRANSAKSI'] = $result[$i]->tgl_transaksi;
        $arrayres[$i]['POSTING_TRANSAKSI'] = $result[$i]->posting_transaksi;
        $arrayres[$i]['CO_STATUS'] = $result[$i]->co_status;
        $arrayres[$i]['KD_USER'] = $result[$i]->kd_user;
        $arrayres[$i]['NAMA_UNIT'] = $result[$i]->nama_unit;
        $arrayres[$i]['CUSTOMER'] = $result[$i]->customer;
        $arrayres[$i]['NO_KAMAR'] = $result[$i]->no_kamar;
        $arrayres[$i]['KD_SPESIAL'] = $result[$i]->kd_spesial;
        $arrayres[$i]['AKHIR'] = $result[$i]->akhir;
        $arrayres[$i]['KELPASIEN'] = $result[$i]->kelpasien;
        $arrayres[$i]['GOL_DARAH'] = $result[$i]->gol_darah;
        $arrayres[$i]['HP'] = $result[$i]->handphone;
        $arrayres[$i]['SJP'] = $result[$i]->no_sjp;
        $arrayres[$i]['NO_FOTO'] = $result[$i]->no_foto_rad;
      }       
      echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($arrayres).'}';

        /*
            ------------------------------------------MERUBAH LOAD DATA PASIEN DARI POSTGRESQL KE SQL SERVER------------------------------------------------------

            Programmer  : HDHT
            TGL         : 16-februari-2017
            Tempat      : Madiun
            Implementor : DONAT
            Status      : Belum Terpakai
            -------------------------------------------------------------------------------------------------------------------------------------------------------
        
        $result =_QMS_Query("SELECT TOP 10 pasien.kd_pasien, tr.no_transaksi, nama_pasien = pasien.nama, pasien.Alamat, pasien.handphone, 
                            pasien.telepon,kunjungan.no_sjp, kunjungan.TGL_MASUK AS MASUK,kunjungan.urut_masuk , tgl_lahir = convert(varchar(10),pasien.tgl_lahir,111), 
                            pasien.jenis_kelamin, pasien.gol_darah, kunjungan.kd_Customer, dokter.nama AS DOKTER, dokter.kd_dokter, 
                            tr.kd_unit, u.nama_unit as nama_unit_asli, tr.kd_Kasir, tarif_cust.kd_tarif, tgl = tr.tgl_transaksi,
                            tgl_transaksi = convert(varchar(10),tr.tgl_transaksi,111), tr.posting_transaksi, tr.co_status, tr.kd_user, uasal.nama_unit as nama_unit, customer.customer, 
                            nginap.no_kamar, nginap.kd_spesial,nginap.akhir, 
                            case 
                            when kontraktor.jenis_cust=0 then 'Perseorangan' 
                            when kontraktor.jenis_cust=1 then 'Perusahaan' 
                            when kontraktor.jenis_cust=2 then 'Asuransi' 
                            end as kelpasien,
                            kunjungan.no_foto_rad 
                            FROM pasien LEFT JOIN (
                                      (kunjungan 
                                        LEFT join (
                                          transaksi tr 
                                          INNER join unit u on u.kd_unit=tr.kd_unit) on kunjungan.kd_pasien=tr.kd_pasien and kunjungan.kd_unit= tr.kd_unit and kunjungan.tgl_masuk=tr.tgl_transaksi and kunjungan.urut_masuk = tr.urut_masuk 
                                          LEFT join customer on customer.kd_customer = kunjungan.kd_customer left join nginap on nginap.kd_pasien=kunjungan.kd_pasien and nginap.kd_unit=kunjungan.kd_unit and nginap.tgl_masuk=kunjungan.tgl_masuk and nginap.urut_masuk=kunjungan.urut_masuk and nginap.akhir=1 inner join tarif_cust on tarif_cust.kd_customer=kunjungan.kd_customer 
                                          inner join kontraktor on kontraktor.kd_customer=kunjungan.kd_customer ) 
                                        LEFT JOIN dokter ON kunjungan.kd_dokter=dokter.kd_dokter 
                                        LEFT JOIN unit_asal ua on tr.no_transaksi = ua.no_transaksi and tr.kd_kasir = ua.kd_kasir 
                                        LEFT JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal 
                                        LEFT join unit uasal on trasal.kd_unit = uasal.kd_unit ) ON kunjungan.kd_pasien=pasien.kd_pasien  WHERE $kriteria")->result();
		
        $list=array();
      	foreach ($result as $data) {
               $o=array();
               $o['KD_PASIEN'] = $data->kd_pasien;
               $o['NO_TRANSAKSI'] = $data->no_transaksi;
               $o['NAMA'] = $data->nama_pasien;
               $o['ALAMAT'] = $data->Alamat;
               $o['TGL_LAHIR'] = $data->tgl_lahir;
               $o['JENIS_KELAMIN'] = $data->jenis_kelamin;
               $o['KD_CUSTOMER'] = $data->kd_Customer;
               $o['DOKTER'] = $data->DOKTER;
               $o['KD_DOKTER'] = $data->kd_dokter;
               $o['KD_UNIT'] = $data->kd_unit;
               $o['NAMA_UNIT_ASLI'] = $data->nama_unit_asli;
               $o['KD_KASIR'] = $data->kd_Kasir;
               $o['KD_TARIF'] = $data->kd_tarif;
               $o['TGL'] = $data->tgl;
               $o['TGL_TRANSAKSI'] = $data->tgl_transaksi;
               $o['POSTING_TRANSAKSI'] = $data->posting_transaksi;
               $o['CO_STATUS'] = $data->co_status;
               $o['KD_USER'] = $data->kd_user;
               $o['NAMA_UNIT'] = $data->nama_unit;
               $o['CUSTOMER'] = $data->customer;
               $o['NO_KAMAR'] = $data->no_kamar;
               $o['KD_SPESIAL'] = $data->kd_spesial;
               $o['AKHIR'] = $data->akhir;
               $o['KELPASIEN'] = $data->kelpasien;
               $o['GOL_DARAH'] = $data->gol_darah;
               $o['HP'] = $data->telepon;
               $o['SJP'] = $data->no_sjp;
               $o['NO_FOTO'] = $data->no_foto_rad;
               $list[]=$o;
            }			
		echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($list) . '}';
    */
	}	

}