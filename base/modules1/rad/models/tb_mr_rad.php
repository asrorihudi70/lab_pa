<?php

class tb_mr_rad extends TblBase
{
    function __construct()
    {
        $this->TblName='mr_rad';
        TblBase::TblBase(true);

        $this->SqlQuery="";
    }

    function FillRow($rec)
    {
        $row=new Rowviewmrrad;

          $row->KD_PASIEN=$rec->kd_pasien;
          $row->KD_UNIT=$rec->kd_unit;
          $row->TGL_MASUK=$rec->tgl_masuk;
          $row->HASIL=$rec->hasil;

          return $row;
    }

}

class Rowviewmrrad
{
    public $KD_PASIEN;
    public $KD_UNIT;
    public $TGL_MASUK;
    public $HASIL;
}

?>
