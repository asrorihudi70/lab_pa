﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewdethasilrad extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="klasifikasi,deskripsi,kd_test,normal,satuan,hasil,urut";
		$this->SqlQuery="SELECT Klas_Produk.Klasifikasi,Produk.Deskripsi,Rad_test.*,Rad_hasil.hasil,Rad_hasil.Urut  
                                From 
                                (Rad_Test 
                                left join Rad_hasil on Rad_test.kd_test=Rad_hasil.kd_Test )  
                                inner join (produk 
                                inner join klas_produk on Produk.kd_klas=klas_produk.kd_Klas)  
                                on Rad_Test.kd_Test = produk.Kd_Produk  
                                ";
		$this->TblName='viewkasirrwj';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->KLASIFIKASI=$rec->klasifikasi;
		$row->DESKRIPSI=$rec->deskripsi;
		$row->KD_TEST=$rec->kd_test;
		$row->NORMAL=$rec->normal;
		$row->SATUAN=$rec->satuan;
		$row->HASIL=$rec->hasil;
		$row->URUT=$rec->urut;
		return $row;
	}
}
class Rowdokter
{
        public $KLASIFIKASI;
        public $DESKRIPSI;
        public $KD_TEST;
        public $NORMAL;
        public $SATUAN;
        public $HASIL;
        public $URUT;
}

?>