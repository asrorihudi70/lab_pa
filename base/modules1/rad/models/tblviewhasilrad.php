﻿<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tblviewhasilrad extends TblBase {

    function __construct() {
        $this->StrSql = "kd_pasien, nama, alamat, tgl_masuk, kd_dokter, nama_unit, kd_kelas, urut_masuk, nama_dokter,
			       tgl_lahir, jenis_kelamin, gol_darah, nama_unit_asal,kd_unit";
        $this->SqlQuery = "SELECT Pasien.Kd_pasien, pasien.Nama, Pasien.Alamat, Kunjungan.Tgl_Masuk, Kunjungan.kd_unit, uNIT.Nama_unit, Unit.kd_kelas, Dokter.Kd_Dokter, kunjungan.urut_masuk,  Dokter.Nama as Nama_Dokter, pasien.tgl_lahir, pasien.jenis_kelamin, pasien.gol_darah, uasal.nama_unit as nama_unit_asal
                        From unit 
                        INNER join (((kunjungan  
                        INNER join  Pasien  on Kunjungan.kd_pasien=pasien.kd_pasien) 
                        INNER join dokter on Dokter.kd_Dokter = kunjungan.Kd_Dokter)) on unit.kd_unit=Kunjungan.kd_unit  
                        INNER JOIN Transaksi   On transaksi.kd_Pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.Kd_Unit 
                        and transaksi.Tgl_transaksi=kunjungan.tgl_masuk and transaksi.urut_masuk=kunjungan.urut_masuk
                        left JOIN unit_asal ua on transaksi.no_transaksi = ua.no_transaksi and transaksi.kd_kasir = ua.kd_kasir
                        INNER JOIN transaksi trasal on trasal.no_transaksi = ua.no_transaksi_asal and trasal.kd_kasir = ua.kd_kasir_asal
                        INNER join unit uasal on trasal.kd_unit = uasal.kd_unit ";
        $this->TblName = '';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowdokter;

        $row->KD_PASIEN = $rec->kd_pasien;
        $row->NAMA = $rec->nama;
        $row->ALAMAT = $rec->alamat;
        $row->TGL_MASUK = $rec->tgl_masuk;
        $row->KD_DOKTER = $rec->kd_dokter;
        if ($rec->nama_unit_asal == "") {
            $row->NAMA_UNIT = $rec->nama_unit;
        } else {
            $row->NAMA_UNIT = $rec->nama_unit_asal;
        }
        $row->KD_KELAS = $rec->kd_kelas;
        $row->URUT_MASUK = $rec->urut_masuk;
        $row->NAMA_DOKTER = $rec->nama_dokter;
        $row->TGL_LAHIR = $rec->tgl_lahir;
        if ($rec->jenis_kelamin == 't') {
            $row->JENIS_KELAMIN = 'Laki-Laki';
        } else {
            $row->JENIS_KELAMIN = 'Perempuan';
        }
        $row->GOL_DARAH = $rec->gol_darah;
        $row->NAMA_UNIT_ASAL = $rec->nama_unit_asal;
        $row->KD_UNIT = $rec->kd_unit;

        return $row;
    }

}

class Rowdokter {

    public $KD_PASIEN;
    public $NAMA;
    public $ALAMAT;
    public $TGL_MASUK;
    public $KD_DOKTER;
    public $NAMA_UNIT;
    public $KD_KELAS;
    public $URUT_MASUK;
    public $NAMA_DOKTER;
    public $TGL_LAHIR;
    public $JENIS_KELAMIN;
    public $GOL_DARAH;
    public $NAMA_UNIT_ASAL;

}
?>