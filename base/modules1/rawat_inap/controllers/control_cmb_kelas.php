<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_cmb_kelas extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	private $kd_kasir      = "";
	private $id_user       = "";

	public function __construct()
	{
		parent::__construct();
		$this->jam      = date("H:i:s");
		$this->tanggal 	= date("Y-m-d");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
		$this->id_user  = $this->session->userdata['user_id']['id'];
	}

	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function get_kelas(){
		$resultPG 	= array();
		$queryPG 	= $this->db->query("SELECT * FROM kelas");

		$x=1;
		$resultPG[0]['kd_kelas'] = 'SEMUA';
		$resultPG[0]['kelas']    = 'SEMUA';
		foreach ($queryPG->result() as $result) {
			$resultPG[$x]['kd_kelas'] 	= $result->kd_kelas;
			$resultPG[$x]['kelas'] 		= $result->kelas;
			$x++;
		}
		echo '{success:true, totalrecords:'.count($resultPG).', listData:'.json_encode($resultPG).'}';
	}
}
?>
