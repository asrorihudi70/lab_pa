<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_diagnosa extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	private $urut_masuk    = "";
	private $tgl_masuk     = "";
	private $urut_nginap   = "";
	private $kd_unit_kamar = "";
	private $no_kamar      = "";
	private $kd_kasir      = "";
	private $setup_db_sql  = false;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('rawat_inap/Model_pindah_kamar');
		$this->load->model('Tbl_data_spesialisasi');
		$this->load->model('Tbl_data_kelas');
		$this->load->model('Tbl_data_kamar');
		$this->load->model('Tbl_data_kunjungan');
		$this->jam      = date("H:i:s");

		/*$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}
		// SETTING KONEKSI SQL
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		}*/
		$this->kd_kasir = '02';
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function list_diagnosa(){
		$this->db->trans_begin();
		// SETTING KONEKSI SQL
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$response 	= array();
		
		$resultSQL	= true;
		$result 	= true;
		$resultQuery= false;


		$recPenyakit=$this->db->query("SELECT p.kd_penyakit,p.penyakit,m.tgl_masuk AS tgl_masuk,
			case when m.stat_diag=0 then 'Diagnosa Awal' else 'Diagnosa Utama' end as stat_diag  
			from mr_penyakit m
			inner join penyakit p ON p.kd_penyakit=m.kd_penyakit
			where kd_pasien='".$_POST['kd_pasien']."'  and m.stat_diag in(0,1) order by tgl_masuk ASC");
		$data = array();

		if ($recPenyakit->num_rows() > 0) {
			$x = 0;
			foreach ($recPenyakit->result() as $result_query) {
				$data[$x]['kd_penyakit'] 	= $result_query->kd_penyakit;
				$data[$x]['penyakit']		= $result_query->penyakit;
				$data[$x]['stat_diag'] 		= $result_query->stat_diag;
				$data[$x]['tgl_masuk'] 		= date_format(date_create($result_query->tgl_masuk), 'd/M/Y');
				$x++;
			}
		}
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$this->db->trans_commit();		
			// SETTING KONEKSI SQL
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {	
				$this->dbSQL->trans_commit();
			}
			$response['success'] 	= true;
			$response['RIWAYAT_PENYAKIT'] 	= $data;
		}else{	
			// SETTING KONEKSI SQL
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['success'] 	= false;
		}
		$this->db->close();	
		// SETTING KONEKSI SQL
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}

}
?>
