<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class control_pindah_kamar extends MX_Controller
{
	private $jam           = "";
	private $tanggal       = "";
	private $dbSQL         = "";
	private $urut_masuk    = "";
	private $tgl_masuk     = "";
	private $urut_nginap   = "";
	private $kd_unit_kamar = "";
	private $no_kamar      = "";
	private $kd_kasir      = "";
	private $setup_db_sql  = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('rawat_inap/Model_pindah_kamar');
		$this->load->model('Tbl_data_spesialisasi');
		$this->load->model('Tbl_data_kelas');
		$this->load->model('Tbl_data_kamar');
		$this->load->model('Tbl_data_kunjungan');
		$this->jam      = date("H:i:s");
		$this->kd_kasir = '02';
		$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		}
	}


	public function index()
	{
		$this->load->view('main/index');
	}	 

	public function pindah_kamar(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$response 	= array();
		
		$resultSQL	= false;
		$result 	= false;
		$resultQuery= false;

		$this->tanggal 		= date('Y-m-d',strtotime($this->input->post('tgltransaksi')));
		$critera_kamar 		= array(
			'no_kamar' 	=> $this->input->post('Kamar'),
			'kd_unit' 	=> $this->input->post('Ruang'),
		);

		$this->kd_unit_kamar= $this->Model_pindah_kamar->getSelectedDataKamar($critera_kamar)->row()->kd_unit;
		$params = array(
			'TmpNoMedrec'     => $this->input->post('NoMedrec'),
			'TmpNoTransaksi'  => $this->input->post('NoTransaksi'),
			'TmpPoli'         => $this->input->post('Poli'),
			'TmpPoliLast'     => $this->input->post('UnitLast'),
			'TmpKamar'        => $this->input->post('Kamar'),
			'TmpKelas'        => $this->input->post('Kelas'),
			'TmpRuang'        => $this->input->post('Ruang'),
			'TmpKdUnitKamar'  => $this->input->post('KDUnitKamar'),
			'TmpAct'          => $this->input->post('Act'),
			'TmpUrutMasuk'    => $this->input->post('Urut_masuk'),
			'TmpNoKamarLast'  => $this->input->post('no_kamar'),
			'TmpTglMasuk'  	  => $this->input->post('tglmasuk'),
			'TmpTglPindah'	  => $this->input->post('tgl_pindah'),
			'TmpKdUnit'	  	  => $this->input->post('kd_unit_kunjungan'),
		);

		$criteriaParams = array(
			'kd_spesial' 	=> $params['TmpPoli'],
		);
		$response['spesialisasi'] 	= $this->Tbl_data_spesialisasi->get($criteriaParams)->row()->spesialisasi;

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kelas' 	=> $params['TmpKelas'],
		);
		$response['kelas'] 			= $this->Tbl_data_kelas->get($criteriaParams)->row()->kelas;
		$response['no_kamar'] 		= $params['TmpKamar'];

		unset($criteriaParams);
		$criteriaParams = array(
			'no_kamar' 	=> $params['TmpKamar'],
		);
		$response['nama_kamar'] 	= $this->Tbl_data_kamar->select($criteriaParams)->row()->nama_kamar;

		/* 
			PROSES UPDATE PINDAH KAMAR 
			PADA TABEL NGINAP
			=====================================================================================================================
		*/
		$criteria_nginap_SQL = array(
			'kd_pasien'  => $params['TmpNoMedrec'],
			'kd_unit'    => $params['TmpKdUnit'],
			'urut_masuk' => $params['TmpUrutMasuk'],
			'tgl_masuk'  => date('Y-m-d', strtotime($params['TmpTglMasuk'])),
			'akhir' 	 => '1',
		);

		$criteria_nginap = array(
			'kd_pasien'  => $params['TmpNoMedrec'],
			'kd_unit'    => $params['TmpKdUnit'],
			'urut_masuk' => $params['TmpUrutMasuk'],
			'tgl_masuk'  => date('Y-m-d', strtotime($params['TmpTglMasuk'])),
			'akhir' 	 => 'TRUE',
		);

		$result 	= $this->Model_pindah_kamar->getSelectedData($criteria_nginap);

		foreach ($result->result() as $result) {
			$this->urut_masuk  = $result->urut_masuk;
			$this->tgl_masuk   = date('Y-m-d', strtotime($result->tgl_masuk));
			$this->urut_nginap = (int)$result->urut_nginap+1;
		}

		$set = array(
			'tgl_keluar' 	=> $params['TmpTglPindah'],
			'jam_keluar' 	=> "1900-01-01 ".$this->jam,
			'akhir' 		=> '0',
		);
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$resultSQL 			= $this->Model_pindah_kamar->updatePindahKamarSQL($criteria_nginap_SQL, $set);
		}else{
			$resultSQL 			= true;
		}
		$set['akhir'] 		= 'FALSE';
		$result 			= $this->Model_pindah_kamar->updatePindahKamar($criteria_nginap, $set);
		$response['tahap'] 	= 'Update Pindah kamar';
		
		/* 
			PROSES UPDATE PINDAH KAMAR =========================================================================================
		*/

		/*
			PROSES INSERT KE TABLE NGINAP 
			=======================================================================================================================
		 */
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$paramsInsert = array(
				'kd_unit_kamar' => $this->kd_unit_kamar,
				'no_kamar'      => $params['TmpKamar'],
				'kd_pasien'     => $params['TmpNoMedrec'],
				'kd_unit'       => $params['TmpKdUnit'],
				'tgl_masuk'     => $this->tgl_masuk,
				'urut_masuk'    => $this->urut_masuk,
				'tgl_inap'      => $params['TmpTglPindah'],
				'jam_inap'      => "1900-01-01 ".$this->jam,
				'akhir'         => '1',
				'kd_spesial'    => $params['TmpPoli'],
				'urut_nginap'   => $this->urut_nginap,
			);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->insertPindahKamarSQL($paramsInsert);
			}else{
				$resultSQL = true;
			}
			$paramsInsert['akhir'] = 'TRUE';

			$result = $this->Model_pindah_kamar->insertPindahKamar($paramsInsert);
			$response['tahap'] 	= 'Insert Pindah Kamar';
		}else{
			$result    = false;
			$resultSQL = false;
		}
		
		/*
			PROSES INSERT KE TABLE NGINAP =========================================================================================
		 */
		

		/*
			PROSES UPDATE KETERANGAN PASIEN INAP 
			TABLE PASIEN INNAP ===========================================================================================================
		 */
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$paramsPasienInap = array(
				'kd_unit'    => $this->kd_unit_kamar,
				'no_kamar'   => $params['TmpKamar'],
				'kd_spesial' => $params['TmpPoli'],
			);
			$criteriaPasienInap = array(
				'kd_kasir'     => $this->kd_kasir,
				'no_transaksi' => $params['TmpNoTransaksi'],
			);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updatePasienInapSQL($criteriaPasienInap, $paramsPasienInap);
			}else{
				$resultSQL = true;
			}
			$result    = $this->Model_pindah_kamar->updatePasienInap($criteriaPasienInap, $paramsPasienInap);
			$response['tahap'] 	= 'Update pasien inap';
		}else{
			$result    = false;
			$resultSQL = false;
		}
		/*
			PROSES UPDATE KETERANGAN PASIEN INAP 
			TABLE PASIEN INNAP ===========================================================================================================
		 */
		

		/*
			PROSES UPDATE KETERANGAN QUANTITY DIGUNAKAN
			TABLE KAMAR ===========================================================================================================

		 */
		
		/*
			========================================= UPDATE STATUS DIGUNAKAN OLEH KAMAR SEBELUMNYA 
		*/
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$criteriaKamarInduk = array(
				'no_kamar' 	=> $params['TmpNoKamarLast'],
				'kd_unit' 	=> $params['TmpPoliLast'],
			);
			$paramsKamarIndukLast['digunakan'] = $this->operateQuantityDigunakan($params['TmpNoKamarLast'], "kurang");

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updateKamarSQL($criteriaKamarInduk, $paramsKamarIndukLast);
			}else{
				$resultSQL = true;
			}
			$result    = $this->Model_pindah_kamar->updateKamar($criteriaKamarInduk, $paramsKamarIndukLast);
			$response['tahap'] 	= 'Update kamar kurang quantity digunakan';
		}else{
			$result    = false;
			$resultSQL = false;
		}
		/*
			===========================================================================================================================
		*/

		/*
			========================================= UPDATE STATUS DIGUNAKAN OLEH KAMAR SETELAHNYA 
		*/
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$criteriaKamarInduk = array(
				'no_kamar' 	=> $params['TmpKamar'],
				'kd_unit' 	=> $params['TmpRuang'],
			);
			$paramsKamarIndukLast['digunakan'] = $this->operateQuantityDigunakan($params['TmpKamar'], "tambah");

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updateKamarSQL($criteriaKamarInduk, $paramsKamarIndukLast);
			}else{
				$resultSQL = true;
			}
			$result    = $this->Model_pindah_kamar->updateKamar($criteriaKamarInduk, $paramsKamarIndukLast);
			$response['tahap'] 	= 'Update kamar tambah quantity digunakan';
		}else{
			$result    = false;
			$resultSQL = false;
		}

			//$result    = false;
			//$resultSQL = false;
		/*
			===========================================================================================================================
		*/
		

		/*
			PROSES UPDATE KETERANGAN QUANTITY DIGUNAKAN
			TABLE KAMAR ===========================================================================================================

		 */
		
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$response['result'] 	= $result;
			$this->db->trans_commit();	

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				// $response['SQL_Connect'] 		= true;
				$this->dbSQL->trans_commit();
			}
			$response['success'] 	= true;
			$response['kd_unit'] 	= $this->kd_unit_kamar;
		}else{
			$this->db->trans_rollback();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['success'] 	= false;
			$response['kd_unit'] 	= $this->kd_unit_kamar;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}

	public function ganti_kamar(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$resultSQL = true;
		$result    = true;
		$response  = array();

		$this->tgl_masuk	= date('Y-m-d',strtotime($this->input->post('tglmasuk')));
		$critera_kamar 		= array(
			'no_kamar' => $this->input->post('Kamar'),
			'kd_unit'  => $this->input->post('Ruang'),
		);

		$this->kd_unit_kamar= $this->Model_pindah_kamar->getSelectedDataKamar($critera_kamar)->row()->kd_unit;
		$params = array(
			'TmpNoMedrec'     => $this->input->post('NoMedrec'),
			'TmpNoTransaksi'  => $this->input->post('NoTransaksi'),
			'TmpPoli'         => $this->input->post('Poli'),
			'TmpPoliLast'     => $this->input->post('UnitLast'),
			'TmpKamar'        => $this->input->post('Kamar'),
			'TmpKelas'        => $this->input->post('Kelas'),
			'TmpRuang'        => $this->input->post('Ruang'),
			'TmpKdUnitKamar'  => $this->input->post('KDUnitKamar'),
			'TmpAct'          => $this->input->post('Act'),
			'TmpUrutMasuk'    => $this->input->post('Urut_masuk'),
			'TmpNoKamarLast'  => $this->input->post('no_kamar'),
			'TmpKdUnit'	  	  => $this->input->post('kd_unit_kunjungan'),
		);

		$criteriaParams = array(
			'kd_spesial' 	=> $params['TmpPoli'],
		);
		$response['spesialisasi'] 	= $this->Tbl_data_spesialisasi->get($criteriaParams)->row()->spesialisasi;

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kelas' 	=> $params['TmpKelas'],
		);
		$response['kelas'] 			= $this->Tbl_data_kelas->get($criteriaParams)->row()->kelas;
		$response['no_kamar'] 		= $params['TmpKamar'];

		unset($criteriaParams);
		$criteriaParams = array(
			'no_kamar' 	=> $params['TmpKamar'],
		);
		$response['nama_kamar'] 	= $this->Tbl_data_kamar->select($criteriaParams)->row()->nama_kamar;



		unset($criteriaKunjungan);
		$criteriaKunjungan = array(
			'kd_pasien'  => $params['TmpNoMedrec'],
			'tgl_masuk'  => $this->tgl_masuk,
			'kd_unit'    => $params['TmpKdUnit'],
			'urut_masuk' => $params['TmpUrutMasuk'],
			// 'akhir'      => 'true',
		);

		$queryNginap= $this->Model_pindah_kamar->getSelectedData($criteriaKunjungan);
		/* KUNJUNGAN { KD_UNIT, } */

		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			unset($criteriaKunjungan);
			$criteriaKunjungan = array(
				'kd_pasien'  => $params['TmpNoMedrec'],
				'tgl_masuk'  => $this->tgl_masuk,
				'kd_unit'    => $params['TmpKdUnit'],
				'urut_masuk' => $params['TmpUrutMasuk'],
			);

			$setKunjungan = array(
				'kd_unit' 	=> $this->kd_unit_kamar,
			);

			if ($queryNginap->num_rows() == 1) {
				$result     = $this->Tbl_data_kunjungan->update($criteriaKunjungan, $setKunjungan);
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$resultSQL  = $this->Tbl_data_kunjungan->updateSQL($criteriaKunjungan, $setKunjungan);
				}else{
					$resultSQL 	= true;
				}
				$params['TmpKdUnit'] = $this->kd_unit_kamar;
			}else{
				$result     = true;
				$resultSQL  = true;
			}
			$response['tahap'] 		= "Update data kunjungan";
		}else{
			$result    = false;
			$resultSQL = false;
		}
		
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			unset($criteriaKunjungan);
			$criteriaKunjungan = array(
				'kd_pasien'  => $params['TmpNoMedrec'],
				'tgl_masuk'  => $this->tgl_masuk,
				'kd_unit'    => $params['TmpKdUnit'],
				'urut_masuk' => $params['TmpUrutMasuk'],
				'akhir'      => 'true',
			);

			$setKunjungan = array(
				'kd_unit_kamar' => $this->kd_unit_kamar,
				'no_kamar' 		=> $params['TmpKamar'],
				'kd_spesial' 	=> $params['TmpPoli'],
			);

			// $queryNginap= $this->Model_pindah_kamar->getSelectedData($criteriaKunjungan);
			// if ($queryNginap->num_rows() == 1) {
				$result     = $this->Model_pindah_kamar->updatePindahKamar($criteriaKunjungan, $setKunjungan);
				$criteriaKunjungan['akhir'] = 1;
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$resultSQL  = $this->Model_pindah_kamar->updatePindahKamarSQL($criteriaKunjungan, $setKunjungan);
				}else{
					$resultSQL 	= true;
				}
				$response['tahap'] 		= "Update data nginap";
			// }else{
			// 	$resultSQL = false;
			// 	$result    = false;
			// }
		}else{
			$result    = false;
			$resultSQL = false;
		}

		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$paramsPasienInap = array(
				'kd_unit'    => $this->kd_unit_kamar,
				'no_kamar'   => $params['TmpKamar'],
				'kd_spesial' => $params['TmpPoli'],
			);
			$criteriaPasienInap = array(
				'kd_kasir'     => $this->kd_kasir,
				'no_transaksi' => $params['TmpNoTransaksi'],
			);
			$response['tahap'] 		= "Update pasien inap";
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updatePasienInapSQL($criteriaPasienInap, $paramsPasienInap);
			}else{
				$resultSQL = true;
			}
			$result    = $this->Model_pindah_kamar->updatePasienInap($criteriaPasienInap, $paramsPasienInap);
		}else{
			$resultSQL = false;
			$result    = false;
		}

		/*
			UPDATE KAMAR SEBELUMNYA 
			==========================================================================================================================
		 */
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$criteriaKamarInduk = array(
				'no_kamar' 	=> $params['TmpNoKamarLast'],
			);
			$paramsKamarIndukLast['digunakan'] = $this->operateQuantityDigunakan($params['TmpNoKamarLast'], "kurang");

			$response['tahap'] 		= "Update kamar kurang";

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updateKamarSQL($criteriaKamarInduk, $paramsKamarIndukLast);
			}else{
				$resultSQL = true;
			}
			$result    = $this->Model_pindah_kamar->updateKamar($criteriaKamarInduk, $paramsKamarIndukLast);
		}else{
			$resultSQL = false;
			$result    = false;
		}
		/*
			END UPDATE KAMAR SEBELUMNYA 
			==========================================================================================================================
		*/

		/*
			UPDATE KAMAR SESUDAHNYA 
			==========================================================================================================================
		*/
		
		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$criteriaKamarInduk = array(
				'no_kamar' 	=> $params['TmpKamar'],
			);
			$paramsKamarIndukLast['digunakan'] = $this->operateQuantityDigunakan($params['TmpKamar'], "tambah");

			$response['tahap'] 		= "Update kamar tambah";
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updateKamarSQL($criteriaKamarInduk, $paramsKamarIndukLast);
			}else{
				$resultSQL = true;
			}
			$result    = $this->Model_pindah_kamar->updateKamar($criteriaKamarInduk, $paramsKamarIndukLast);
		}else{
			$result    = false;
			$resultSQL = false;
		}
		/*
			END UPDATE KAMAR SESUDAHNYA 
			==========================================================================================================================
		*/


		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			$response['kd_unit'] 	= $this->kd_unit_kamar;
			$response['success'] 	= true;
		}else{
			$this->db->trans_rollback();

			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['success'] 	= false;
		}

		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}

	public function savePasienPulang(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}
		$response 	= array();
		$resultSQL 	= false;
		$result 	= false;
		$params 	= array(
			'kd_kasir'      => $this->kd_kasir,
			'kd_pasien'     => $this->input->post('no_medrec'),
			'no_transaksi'  => $this->input->post('no_transaksi'),
			'tgl_boleh_plg' => date('Y-m-d', strtotime($this->input->post('tgl_boleh_plg')))." 00:00:00",
			'jam_boleh_plg' => "1900-01-01 ".$this->input->post('jam_boleh_plg'),
		);	

		$criteriaTransaksi = array(
			'kd_kasir'     => $params['kd_kasir'],
			'kd_pasien'    => $params['kd_pasien'],
			'no_transaksi' => $params['no_transaksi'],
		);

		$dataUpdate 		= array(
			'tgl_boleh_plg' => $params['tgl_boleh_plg'],
			'jam_boleh_plg' => $params['jam_boleh_plg'],
		);

		$result    = $this->Model_pindah_kamar->getSelectedDataTransaksi($criteriaTransaksi);
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$resultSQL = $this->Model_pindah_kamar->getSelectedDataTransaksiSQL($criteriaTransaksi);
		}else{
			$resultSQL = true;
		}

		if ($result->num_rows() > 0) {
			$result    = $this->Model_pindah_kamar->updateDataPulangPasien($criteriaTransaksi, $dataUpdate);
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$resultSQL = $this->Model_pindah_kamar->updateDataPulangPasienSQL($criteriaTransaksi, $dataUpdate);
			}else{
				$resultSQL = true;
			}
		}else{
			$resultSQL = false;
			$result    = false;
		}

		if (($result>0 || $result===true) && ($resultSQL>0 || $resultSQL===true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			$response['success'] 	= true;
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['success'] 	= false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}

	private function operateQuantityDigunakan($criteria, $perintah){
		$resultQuery = "";
		$jumlah = 0;
		$jumlahKasurLast = 0;
		$criteriaKamarInduk = array(
			'no_kamar' 	=> $criteria,
		);
		$resultQuery = $this->Model_pindah_kamar->getSelectedDataKamar($criteriaKamarInduk);
		foreach ($resultQuery->result_array() as $data) {
			$jumlahKasurLast = (int)$data['digunakan'];
		}
		if (($perintah===true) || $perintah=="tambah") {
			$jumlah = (int)$jumlahKasurLast+1;
		}else{
			$jumlah = (int)$jumlahKasurLast-1;
		}
		return $jumlah;
	}
	

	public function cek_perpindahan(){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$resultSQL = true;
		$result    = true;
		$response  = array();

		$this->tgl_masuk	= date('Y-m-d',strtotime($this->input->post('tglmasuk')));
		$critera_kamar 		= array(
			'no_kamar' => $this->input->post('no_kamar'),
		);

		$this->kd_unit_kamar= $this->Model_pindah_kamar->getSelectedDataKamarSQL($critera_kamar)->row()->kd_unit;
		$params = array(
			'TmpNoMedrec'     => $this->input->post('NoMedrec'),
			'TmpNoTransaksi'  => $this->input->post('NoTransaksi'),
			'TmpPoli'         => $this->input->post('Poli'),
			'TmpPoliLast'     => $this->input->post('UnitLast'),
			'TmpKamar'        => $this->input->post('Kamar'),
			'TmpKelas'        => $this->input->post('Kelas'),
			'TmpRuang'        => $this->input->post('Ruang'),
			'TmpKdUnitKamar'  => $this->input->post('KDUnitKamar'),
			'TmpAct'          => $this->input->post('Act'),
			'TmpUrutMasuk'    => $this->input->post('Urut_masuk'),
			'TmpNoKamarLast'  => $this->input->post('no_kamar'),
		);

		$criteriaKunjungan = array(
			'kd_pasien'  => $params['TmpNoMedrec'],
			'tgl_masuk'  => $this->tgl_masuk,
			'kd_unit'    => $params['TmpPoli'],
			'urut_masuk' => $params['TmpUrutMasuk'],
			'akhir'      => 'true',
		);

		$queryNginap= $this->Model_pindah_kamar->getSelectedData($criteriaKunjungan);
		if ($queryNginap->num_rows() > 0) {
			$response['status'] 	= true;
		}else{
			$response['status'] 	= false;
		}
		echo json_encode($response);
	}

	public function history_pindah_kamar(){
		$data = array();
		$response = array();
		$params = array(
			'kd_unit'    => $this->input->post('kd_unit'),
			'tgl_masuk'  => $this->input->post('tgl_masuk'),
			'kd_pasien'  => $this->input->post('kd_pasien'),
			'urut_masuk' => $this->input->post('urut_masuk'),
		);

		$query = $this->db->query("SELECT 
			kam.nama_kamar, 
			ngi.tgl_inap, 
			ngi.tgl_keluar, 
			ngi.jam_inap,
			ngi.jam_keluar, 
			(ngi.tgl_keluar::date - ngi.tgl_inap::date + 1)||' hari' as lama_rawat, 
			(ngi.urut_nginap + 1) as urut_nginap
			FROM NGINAP ngi
			inner join kamar kam on kam.kd_unit=ngi.kd_unit_kamar and kam.no_kamar=ngi.no_kamar
			inner join pasien pas on pas.kd_pasien=ngi.kd_pasien
			WHERE 
			ngi.KD_PASIEN = '".$params['kd_pasien']."' 
			and ngi.tgl_masuk = '".$params['tgl_masuk']."'
			and ngi.kd_unit = '".$params['kd_unit']."'
			and ngi.urut_masuk = '".$params['urut_masuk']."'
			order by ngi.urut_nginap	
		");

		if ($query->num_rows > 0) {
			foreach ($query->result() as $result_data) {
				$tmp_data = array();
				$tmp_data['nama_kamar']  = $result_data->nama_kamar;
				if ($result_data->jam_inap == null) {
					$tmp_data['tgl_inap']    = "";
				}else{
					$tmp_data['tgl_inap']    = date_format(date_create($result_data->tgl_inap), 'd-M-Y');
				}


				if ($result_data->jam_inap == null) {
					$tmp_data['jam_inap']    = "";
				}else{
					$tmp_data['jam_inap']    = date_format(date_create($result_data->jam_inap), 'H:i:s');
				}

				if ($result_data->tgl_keluar == null) {
					$tmp_data['tgl_keluar']  = "";
				}else{
					$tmp_data['tgl_keluar']  = date_format(date_create($result_data->tgl_keluar), 'd-M-Y');
				}
				
				if ($result_data->jam_keluar == null) {
					$tmp_data['jam_keluar']  = "";
				}else{
					$tmp_data['jam_keluar']  = date_format(date_create($result_data->jam_keluar), 'H:i:s');
				}
				$tmp_data['lama_rawat']  = $result_data->lama_rawat;
				$tmp_data['urut_nginap'] = $result_data->urut_nginap;
				array_push($data, $tmp_data);
			}
		}
		$response['data'] = $data;
		echo json_encode($response);
	}
}
?>
