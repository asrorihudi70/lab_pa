<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class directkwitansi extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
    $this->load->view('main/index');
    }
    
    
    public function save($Params=NULL)
    {
        $mError = "";
        $mError = $this->cetak($Params);
        if ($mError=="sukses")
        {
            echo '{success: true}';
        }
        else{
            echo $mError;
        }
    }
    
    public function cetak($Params)
    {
        $strError = "";
        $no_transaksi = '0208218';//$Params["No_TRans"];
        $Total = "";
        $kd_user = $this->session->userdata['user_id']['id'];
        $q = $this->db->query("select * from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '".$no_transaksi."'");//-- and jenis_pay = 1
        
        if($q->num_rows == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
        foreach ($q->result() as $data)
        {
            $Total = $data->jumlah;
        }
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        
        $criteria = "no_transaksi = '".$no_transaksi."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $Medrec = $query[0][0]->KD_PASIEN;
           $Status = $query[0][0]->STATUS;
           $Dokter = $query[0][0]->DOKTER;
           $Nama = $query[0][0]->NAMA;
           $Alamat = $query[0][0]->ALAMAT;
           $Poli = $query[0][0]->UNIT;
           $Notrans = $query[0][0]->NO_TRANSAKSI;
           $Tgl = $query[0][0]->TGL_TRANS;
           $KdUser = $query[0][0]->KD_USER;
           $uraian = $query[0][0]->DESKRIPSI;
           $jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
        }
        $waktu = explode(" ",$Tgl);
        $tanggal = $waktu[0];
        $printer = $this->db->query("select p_kwitansi from zusers where kd_user = '".$kd_user."'")->row()->p_kwitansi;
        $nosurat=$this->db->query("select setting from sys_setting where key_data = 'no_surat_default_kwitansi'")->row()->setting;
        $t1 = 4;
        $t3 = 30;
        $t2 = 36 - ($t3 + $t1);
        // $printer = "192.168.0.39\Epson-LX-310-ip39-17-10-2017";       
        $format1 = date('d F Y', strtotime($Tgl));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= chr(27) . chr(87) . chr(49);
        $Data .= str_pad($bold1."K W I T A N S I".$bold0,62," ",STR_PAD_BOTH)."\n";
        $Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $bold0;
        $Data .= str_pad("No. Bukti Pembayaran         : ".$no_transaksi,40," ").str_pad($nosurat,40," ",STR_PAD_LEFT)."\n";
        $Data .= $bold0;
        $Data .= str_pad("Telah Terima Dari            : ".$Nama, 40," ")."\n";
        $Data .= $bold0;
        $Data .= "Banyaknya uang terbilang     : ".$bold1.terbilang($Total)."Rupiah".$bold0."\n";
        $Data .= "\n";
        $Data .= "Untuk Pembayaran Biaya Rawat Inap RSUD dr. Soedono Madiun Madiun "."\n";
        $Data .= "a/n ".$Nama."   Pada Tanggal ".$tanggal."\n";;
        // $Data .= "Unit yang dituju : ".$Poli."\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($Kota . ' , ' . $today, 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("Petugas Pembayaran", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("(---------------------)", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 40, " ", STR_PAD_BOTH) . "\n";
        fwrite($handle, $Data);
        fclose($handle);
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
		}
        
        //copy($file, $printer);  # Lakukan cetak
        //unlink($file);
        
        //echo $file;
        $strError = "sukses";
        
        
        }
        return $strError;
        
    }
    
   
            
}




