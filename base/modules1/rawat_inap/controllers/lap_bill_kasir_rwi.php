<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_bill_kasir_rwi extends MX_Controller {
	private $dbSQL = false;
    public function __construct(){
        parent::__construct();
		$this->load->library('session');
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_visite_dokter');
		$this->load->model('Tbl_data_pasien');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_pasien_inap');
		$this->load->model('Tbl_data_nginap');
		$this->load->model('Tbl_data_spesialisasi');
    }	 

	public function index(){
        $this->load->view('main/index');       
   	}

    public function Cetak($Params = NULL)
    {        
    	$erorkwitansi = '';
    	$erorbill = '';
    	
        if ($_POST['cetakan'] === 'Billing' || $_POST['cetakan'] == 0  || $_POST['cetakan'] == '0' ) {
        	if ($_POST['cetakkwitansi'] === 'false' || $_POST['cetakkwitansi'] === false) {
        		$erorbill = $this->cetakBiling($Params);
        		if ($erorbill === "sukses") {
        			echo '{success: true}';
        		}else{
        			echo '{success: false}';
        		}//
        	}else{
        		$erorkwitansi =  $this->cetakkwitansi($Params);
        		if ($erorkwitansi === 'sukses') {
        			$erorbill = $this->cetakBiling($Params);

        			if ($erorbill === "sukses") {
        			echo '{success: true}';
		    		}else{
		    			echo '{success: false}';
		    		}
        		}else{
        			echo '{success: false}';
        		}
        		
        	}
        	
        }else{
        	$erorkwitansi =  $this->cetakkwitansi($Params);
        	if ($erorkwitansi === "sukses") {
			echo '{success: true}';
    		}else{
    			echo '{success: false}';
    		}
        }
    }
   	
   	public function cetaklaporanBillRWI($Params)
	{
   		$title='PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP ';
		$param=json_decode($Params);

		$kdspesial = $param->Spesialisasi;
		$namaspesial = $param->dtlspesialisasi;
		$Tglpulang = $param->Tglpulang;
		$Tgljenisprt = $param->jenisprint;
		$folio = $param->folio;
		$notrans = $param->notrans;
		$KdKasir = $param->KdKasir;
		$reff = $param->reff;
		$medrec = $param->kdpasien;
		$nama = $param->nama;
		$tglmasuk = $param->TglMasuk;
		$kdunit =$param->kdunit;
		$costat = $param->costat;
				
		$kdUser=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("select user_names from zusers where kd_user = '$kdUser'")->row()->user_names;
		$html='';
		$html.='
			<table class="t2" cellspacing="0" border="0">
					<tr>
						<th colspan="9" style="font-size:15px">'.$title.'</th>
					</tr>
			</table>
			';

		$cekpasien = $this->db->query("select date_part('year',age(tgl_lahir)) as umur,c.customer,p.alamat,u.nama_unit,k.tgl_keluar,k.tgl_masuk from 
					pasien p
					INNER JOIN kunjungan k on k.kd_pasien = p.kd_pasien
					INNER JOIN customer c on c.kd_customer = k.kd_customer
					INNER JOIN unit u on u.kd_unit = k.kd_unit
					where p.kd_pasien = '".$medrec."' and k.tgl_masuk = '".$tglmasuk."' and k.kd_unit = '".$kdunit."'");
		foreach ($cekpasien->result() as $data) {
			$umur = $data->umur;
			$customer = $data->customer;
			$alamat = $data->alamat;
			$nama_unit = $data->nama_unit;
			$tgl_keluar = $data->tgl_keluar;
		}
		$html.='
			<table class="t1" border = "0">
			 <tr>
			     <td colspan="2">no medrec</td>
			     <td>:</td>
			     <td>'.$medrec.'</td>
			     <td colspan="2">Kelompok Pasien</td>
			     <td>:</td>
			     <td colspan="2">'.$customer.'</td>
			   </tr>
			   <tr>
			     <td colspan="2">nama Pasien</td>
			     <td>:</td>
			     <td colspan="6">'.$nama.'</td>
			   </tr>
			   <tr>
			     <td colspan="2">Umur</td>
			     <td>:</td>
			     <td align="left" colspan="6">'.$umur.'</td>
			   </tr>
			   <tr>
			     <td colspan="2">Alamat</td>
			     <td>:</td>
			     <td colspan="6">'.$alamat.'</td>
			   </tr>
			   <tr>
			     <td colspan="2">Tgl. Masuk</td>
			     <td>:</td>
			     <td align="left" colspan="6">'.$tglmasuk.'</td>
			   </tr>
			   <tr>
			     <td colspan="2">Dirawat Di Unit</td>
			     <td>:</td>
			     <td colspan="6">'.$nama_unit.'</td>
			   </tr>
			   <tr>
			     <td colspan="2">Tgl. Keluar</td>
			     <td>:</td>
			     <td>'.$tgl_keluar.'</td>
			     <td colspan="2">Spesialisasi</td>
			     <td>:</td>
			     <td colspan="2">'.$namaspesial.'</td>
			   </tr>
			   <tr>
			     <td colspan="9">&nbsp;</td>
			   </tr>
			   ';
			   // echo $kdspesial;
			   if ($kdspesial !== 'NULL') {
			   	
			   	$tmpkdspesial1 = " where kd_spesial = "."'".$kdspesial."'";
			   	$tmpkdspesial2 = " And kd_spesial = "."'".$kdspesial."'";
			   }else{
			   	
			   	$tmpkdspesial1 = '';
			   	$tmpkdspesial2 = '';
			   }
		$query = "select Distinct groups from getallbillkasirrwi('".$KdKasir."','".$notrans."','".$costat."')".$tmpkdspesial1;
		$result = pg_query($query) or die('Query failed: ' . pg_last_error());
       	$grand_total = 0;
       	if(pg_num_rows($result) <= 0)
		   {
				$html.='';
		   }else{
		   		$tmpgroup = '';
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					
					$tmpgroup = $line['groups'];
					if ($line['groups'] !== '') {
						if ($line['groups'] === 'NULL') {
							$html.='
							<tr>
			                 <td colspan="9">&nbsp;</td>
			                </tr>
						';
						}else{
							$html.='
							<tr>
			                 <td colspan="9">'.$line['groups'].'</td>
			                </tr>
						';
						}
						$querydetail = "select * from getallbillkasirrwi('".$KdKasir."','".$notrans."','".$costat."') where groups = '".$tmpgroup."'".$tmpkdspesial2;
						$resultdetail=$this->db->query($querydetail)->result();
						foreach ($resultdetail as $linedetail) 
						{
							$jumlah = $linedetail->harga * $linedetail->qty;
							$html.='
									<tr>
									    <td>&nbsp;</td>
									    <td colspan="3">'.$linedetail->deskripsi.'</td>
									    <td align="right">Rp.</td>
									    <td align="right"> '.number_format($linedetail->harga,0,'.',',').'</td>
									    <td align="right">&nbsp;</td>
									    <td align="right">Rp.</td>
									    <td align="right"> '.number_format($jumlah,0,'.',',').'</td>
								    </tr>
									
							';
							if ($linedetail->detail_dr === 'True') {
								$querycekdokter = $this->db->query("
													select d.nama 
													from visite_dokter vd
														inner join dokter d on vd.kd_dokter = d.kd_dokter
													where kd_kasir = '".$KdKasir."' and no_transaksi = '".$notrans."' and 
													tag_int = '".$linedetail->kd_produk."'")->result();
								foreach ($querycekdokter as $linedokter){
									$html .= '
									<tr>
                              			<td colspan="9" align="left">'.$linedokter->nama.'</td>
  									</tr>
									';
								}
						   
							}
							$grand_total += $jumlah;
						}
						
					}else{

					}
					
					
					
				}

				

					$html .='
						<tr>
						    <td colspan="7" align="right">Sub Total Non Paviliun</td>
						    <td align="right">Rp.</td>
						    <td align="right"> '.number_format($grand_total,0,',','.').'</td>
					    </tr>
						<tr>
						    <td colspan="9">&nbsp;</td>
						  </tr>
						  <tr>
						    <td colspan="2" align="left">'.$user.'</td>
						    <td align="right">&nbsp;</td>
						    <td align="right">&nbsp;</td>
						    <td colspan="3" align="right">Total Biaya</td>
						    <td align="right">Rp.</td>
						    <td align="right"> '.number_format($grand_total,0,',','.').'</td>
					      </tr>
						  <tr>
						    <td colspan="2">&nbsp;</td>
						    <td>&nbsp;</td>
						    <td>&nbsp;</td>
						    <td colspan="5">Madiun, '.date('d F Y').'</td>
						  </tr>
						  <tr>
						    <td colspan="3">Keterangan Rangkap</td>
						    <td>&nbsp;</td>
						    <td colspan="2">&nbsp;</td>
						    <td>&nbsp;</td>
						    <td colspan="2">&nbsp;</td>
						  </tr>
						  <tr>
						    <td colspan="2">- Lembar 1</td>
						    <td>:</td>
						    <td>Pasien</td>
						    <td colspan="2">&nbsp;</td>
						    <td>&nbsp;</td>
						    <td colspan="2">&nbsp;</td>
						  </tr>
						  <tr>
						    <td colspan="2">- Lembar 2</td>
						    <td>:</td>
						    <td>Keuangan</td>
						    <td colspan="2">&nbsp;</td>
						    <td>&nbsp;</td>
						    <td colspan="2">&nbsp;</td>
						  </tr>
						  <tr>
						    <td colspan="2">- Lembar 3</td>
						    <td>:</td>
						    <td>Rekam Medis</td>
						    <td colspan="2">&nbsp;</td>
						    <td>&nbsp;</td>
						    <td colspan="2">&nbsp;</td>
						  </tr>
					';
			}
			$html.="</table>";
			// echo $html;
			#Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			#- Margin
			#- Print area
			#- Type font
			#- Paragraph alignment
			#- Password protected
			#Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			#Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			// echo $html;
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			// # Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:I90');
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I9')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('F')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(2);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
            //     $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('lap_Bill_kasir_RWI'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=lap_Bill_kasir_RWI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
            $eror = 'true';
	}

	public function cetakkwitansi($Params){
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$resultPG 	= false;
		$resultSQL 	= false;
		$strError = "";
		$no_transaksi = $_POST["notrans"];
        $Total = "";

        $kd_user = $this->session->userdata['user_id']['id'];
        $q = $this->db->query("select * from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '".$no_transaksi."'");//-- and jenis_pay = 1
        
        $queryNota = $this->dbSQL->query("SELECT MAX(no_nota) as no_nota from nota_bill where kd_kasir = '".$this->input->post('KdKasir')."'");
        if ($queryNota->num_rows() > 0) {
        	$no_nota = (int)$queryNota->row()->no_nota + 1;
        }else{
        	$no_nota = 1;
        }

        $labelNota = "0000000";
        $labelNota = substr($labelNota, strlen($no_nota)).$no_nota;
        //echo $labelNota;kdunit
        $paramsInsert = array(
			'jumlah' 		=> 0,
			'kd_kasir' 		=> $this->input->post('KdKasir'),
			'no_transaksi' 	=> $this->input->post('notrans'),
			'no_nota' 		=> $no_nota,
			'kd_user' 		=> $this->session->userdata['user_id']['id'],
			'kd_unit' 		=> $this->input->post('kdunit'),
			'tgl_cetak' 	=> date('Y-m-d'),
			'jenis' 		=> 'TO',
        );
        $resultPG 	= $this->db->insert('nota_bill', $paramsInsert);
        $resultSQL 	= $this->dbSQL->insert('nota_bill', $paramsInsert);
        if($q->num_rows == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
        foreach ($q->result() as $data)
        {
            $Total = $data->jumlah;
        }
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        
        $criteria = "no_transaksi = '".$no_transaksi."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $Medrec = $query[0][0]->KD_PASIEN;
           $Status = $query[0][0]->STATUS;
           $Dokter = $query[0][0]->DOKTER;
           $Nama = $query[0][0]->NAMA;
           $Alamat = $query[0][0]->ALAMAT;
           $Poli = $query[0][0]->UNIT;
           $Notrans = $query[0][0]->NO_TRANSAKSI;
           $Tgl = $query[0][0]->TGL_TRANS;
           $KdUser = $query[0][0]->KD_USER;
           $uraian = $query[0][0]->DESKRIPSI;
           $jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
        }
        $waktu = explode(" ",$Tgl);
        $tanggal = $waktu[0];
        $printer = $this->db->query("select p_kwitansi from zusers where kd_user = '".$kd_user."'")->row()->p_kwitansi;
        $nosurat=$this->db->query("select setting from sys_setting where key_data = 'no_surat_default_kwitansi'")->row()->setting;

        $t1 = 4;
        $t3 = 30;
        $t2 = 36 - ($t3 + $t1);
        // $printer = "192.168.0.39\Epson-LX-310-ip39-17-10-2017";       
        $format1 = date('d F Y', strtotime($Tgl));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$condensed2  = Chr(27).Chr(33).Chr(32);
        $Data  = $initialized;
        $Data .= $condensed1;
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= chr(27) . chr(87) . chr(49);
        $Data .= str_pad($bold1.$condensed2."K W I T A N S I".$condensed1.$bold0,62," ",STR_PAD_BOTH)."\n\n";
        $Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $bold0;
        //$Data .= str_pad("No. Kwitansi                 : ".$bold1.$labelNota,40," ").str_pad($nosurat,40," ",STR_PAD_LEFT)."\n";
        $Data .= str_pad("No. Kwitansi                 : ".$bold1.$labelNota,40," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("No. Transaksi                : ".$bold1.$no_transaksi,40," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("No. Medrec                   : ".$bold1.$Medrec,40," ")."\n";
        $Data .= $bold0;
        $Data .= str_pad("Telah Terima Dari            : ".$Nama, 40," ")."\n";
        $Data .= $bold0;
        $Data .= "Banyaknya uang               : Rp. ".$bold1.number_format($Total,0,'.',',').$bold0."\n";
        $Data .= "\n";
        $Data .= "Untuk Pembayaran Biaya Rawat Inap RSUD dr. Soedono Madiun Madiun "."\n";
        $Data .= "a/n ".$Nama."   Pada Tanggal ".$tanggal."\n";
        $Data .= "Banyaknya uang terbilang     : ".$bold1.terbilang($Total)." Rupiah".$bold0."\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($Kota . ' , ' . $today, 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("Petugas Pembayaran", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad("(---------------------)", 40, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 40, " ", STR_PAD_BOTH) . "\n";
        fwrite($handle, $Data);
        fclose($handle);
		
		if (($resultPG>0 || $resultPG===true) && ($resultSQL>0 || $resultSQL===true)) {
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$this->db->close();
			$this->dbSQL->close();
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				copy($file, $printer);  # Lakukan cetak
				unlink($file);
				# printer windows -> nama "printer" di komputer yang sharing printer
			} else{
				shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
			}
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$this->db->close();
			$this->dbSQL->close();
		}
        
        //copy($file, $printer);  # Lakukan cetak
        //unlink($file);
        
        //echo $file;
        $strError = "sukses";
        
        
        }
        return $strError;		
	}

	public function cetakBiling($Params){
		
		$strError = "";
		$TmpTotPaviliun 	= 0;
		$TmpTotNonPaviliun 	= 0;
        $logged = $this->session->userdata('user_id');
        $param=json_decode($Params);
		
		$kd_user = $this->session->userdata['user_id']['id'];
		$user=$this->db->query("select user_names from zusers where kd_user = '$kd_user'")->row()->user_names;
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
			$NameRS  = $query[0][0]->NAME;
			$Address = $query[0][0]->ADDRESS;
			$TLP     = $query[0][0]->PHONE1;
			$Kota    = $query[0][0]->CITY;
        } else {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }

		$no_transaksi = $_POST["notrans"];
		$kdspesial    = $_POST["Spesialisasi"];
		$namaspesial  = $_POST["dtlspesialisasi"];
		$Tglpulang    = $_POST["Tglpulang"];
		$Tgljenisprt  = $_POST["jenisprint"];
		$folio        = $_POST["folio"];
		$notrans      = $_POST["notrans"];
		$KdKasir      = $_POST["KdKasir"];
		$reff         = $_POST["reff"];
		$medrec       = $_POST["kdpasien"];
		$nama         = $_POST["nama"];
		$tglmasuk     = $_POST["TglMasuk"];
		$kdunit       = $_POST["kdunit"];
		$costat       = $_POST["costat"];
		$kdunit_kamar = $_POST["kdunit_kamar"];

        $criteria = "no_transaksi = '" . $no_transaksi . "'";
        $paramquery = "where no_transaksi = '" . $no_transaksi . "'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
			$Medrec      = $query[0][0]->KD_PASIEN;
			$Status      = $query[0][0]->STATUS;
			$Dokter      = $query[0][0]->DOKTER;
			$Nama        = $query[0][0]->NAMA;
			$Alamat      = $query[0][0]->ALAMAT;
			$Poli        = $query[0][0]->UNIT;
			$Notrans     = $query[0][0]->NO_TRANSAKSI;
			$Tgl         = $query[0][0]->TGL_TRANS;
			$KdUser      = $query[0][0]->KD_USER;
			$uraian      = $query[0][0]->DESKRIPSI;
			$jumlahbayar = $query[0][0]->JUMLAH;
        } else {
			//$NoNota = "";
			$Medrec   = "";
			$Status   = "";
			$Dokter   = "";
			$Nama     = "";
			$Alamat   = "";
			$Poli     = " ";
			$Notrans  = "";
			$Tgl      = "";
        }

        if ($kdspesial !== 'NULL') {
			$tmpkdspesial1 = " where kd_spesial = "."'".$kdspesial."'";
	   		$tmpkdspesial2 = " And kd_spesial = "."'".$kdspesial."'";
	   }else{	   	
		   	$tmpkdspesial1 = '';
		   	$tmpkdspesial2 = '';
	   }
        $printer = $this->db->query("select p_kwitansi from zusers where kd_user = '".$kd_user."'")->row()->p_kwitansi;
        //138
        
        $criteriaParams = array(
        	'kd_pasien' 	=> $Medrec,
        );

        $queryPasien 	= $this->Tbl_data_pasien->getDataSelectedPasien($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_pasien' => $Medrec,
			'tgl_masuk' => $tglmasuk,
			'kd_unit'   => $kdunit,
		);

		$querykunjungan = $this->Tbl_data_kunjungan->getDataSelectedKunjungan($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kasir' 		=> $KdKasir,
			'no_transaksi' 	=> $no_transaksi,
			'kd_unit'  		=> $kdunit,
		);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_kasir' 		=> $KdKasir,
			'no_transaksi' 	=> $no_transaksi,
		);
		$queryTransaksi = $this->Tbl_data_transaksi->getTransaksi($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_pasien' 		=> $medrec,
			'kd_unit_kamar' 	=> $kdunit_kamar,
			'akhir' 			=> 'true',
		);
		$queryPasienInap = $this->Tbl_data_nginap->selectNginap($criteriaParams);

		unset($criteriaParams);
		$criteriaParams = array(
			'kd_spesial' 	=> $queryPasienInap->row()->kd_spesial,
		);
		$querySpesialisasi = $this->Tbl_data_spesialisasi->get($criteriaParams);
		
		$t1          = 8;
		$t3          = 10;
		$t4          = 10;
		$t5          = 5;
		$t6          = 7;
		$t2          = 130 - ($t4 + $t3 + $t1 + $t5 + $t6);
		$format1     = date('d F Y', strtotime($Tgl));
		$today       = date("d F Y");
		$Jam         = date("G:i:s");
		$tmpdir      = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file        = tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle      = fopen($file, 'w');
		$condensed   = Chr(27) . Chr(33) . Chr(4);
		$bold1       = Chr(27) . Chr(69);
		$bold0       = Chr(27) . Chr(70);
		$initialized = chr(27) . chr(64);
		$condensed1  = chr(15);
		$condensed0  = chr(18);
		$condensed2  = Chr(27).Chr(33).Chr(32);
		$Data        = $initialized;
		$Data        .= $condensed1;
        //$Data .= str_pad("No. Trans  : " . $Notrans,"",STR_PAD_LEFT."\n");
        $labelNo_transaksi = "No. Trans  : " . $Notrans;

		//$Data .= str_pad($labelNo_transaksi, 10, ' ', STR_PAD_RIGHT)."\n";
        $Data .= $bold1;
        $Data .= chr(27) . chr(33) . chr(24);
		$Data .= str_pad("No. Trans  : " . $Notrans, 150," ",STR_PAD_LEFT)."\n";
        $Data .= $bold0;
		//$Data .= str_pad("No. Trans  : " . $Notrans, 133," ",STR_PAD_LEFT)."\n";
        $Data .= chr(27) . chr(33) . chr(8);
		//$Data .= ;
		$Data .= $NameRS . "\n";
		$Data .= $Address . "\n";
		$Data .= "Phone : " . $TLP . "\n";
		$Data .= $condensed1;
		$Data .= "-------------------------------------------------------------------------------------------------------------------------------------\n";

        /*
        $Data .= chr(27) . chr(87) . chr(49);
        $Data .= str_pad($bold1.$condensed2."PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP".$condensed1.$bold0,62," ",STR_PAD_BOTH)."\n\n";
        $Data .= chr(27) . chr(87) . chr(48);
        $Data .= $bold0;
        */
        $Data .= $bold1;
        $Data .= chr(27) . chr(33) . chr(24);
        //$Data .= "PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP\n\n";
		$Data .= str_pad("PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP", 60," ",STR_PAD_LEFT)."\n\n";
        $Data .= chr(27) . chr(33) . chr(8);
        $Data .= $bold0;

        //$Data .= str_pad($bold1.$condensed2."PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP".$condensed1.$bold0,62," ",STR_PAD_BOTH)."\n\n";
		$Data .= $condensed1;
		//$Data .= str_pad(" ", 17, " ")."PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP"."\n"; //CENTER
        $labelKelPasien = "Kelompok Pasien  : " . $Status;
        $labelNoMedrec  = "No. Medrec       : " . $Medrec;
		$Data .= $labelNoMedrec;// . "        " . $labelKelPasien . "\n";
		$Data .= str_pad($labelKelPasien, 50," ",STR_PAD_LEFT)."\n";

        //$Data .= "No. Medrec : " . $Medrec . "        " . "Tgl       : " . $format1 . "\n";
        //$Data .= "Kel Pasien : " . $Status . "\n";
        $Data .= "Nama Pasien      : " . $Nama . "\n";
        $tmpTanggalLahir 	= $this->db->query("SELECT getumur('".date('Y-m-d')."', '".date_format(date_create($queryPasien->row()->tgl_lahir), 'Y-m-d')."')")->row()->getumur;
        // 25 years 11 mons 29 days
        $tmpTanggalLahir 	= str_replace("years","Tahun",$tmpTanggalLahir);
        $tmpTanggalLahir 	= str_replace("mons","Bulan",$tmpTanggalLahir);
        $tmpTanggalLahir 	= str_replace("days","Hari",$tmpTanggalLahir);
        $Data .= "Umur             : " . substr($tmpTanggalLahir , 0, strlen($tmpTanggalLahir)-1). "\n";
        $Data .= "Alamat           : " . $Alamat . "\n";
        $Data .= "Tanggal Masuk    : " . date_format(date_create($tglmasuk), 'd/M/Y') . "\n";
        //$Data .= "Dokter     : " . $Dokter . "\n";
        //if (isset($querykunjungan->row()->tgl_keluar)) {  $querykunjungan->row()->tgl_keluar;  }else{ "---"; }
        $Data .= "Dirawat Unit     : " . $Poli . "\n";
        $Data .= "Tgl. Masuk       : " . date_format(date_create($querykunjungan->row()->tgl_masuk), 'd/m/Y') . " [".date_format(date_create($querykunjungan->row()->jam_masuk), 'H:i:s')."]". "\n";
        $Data .= "Tgl. Boleh Pulang: " . date_format(date_create($queryTransaksi->row()->tgl_boleh_plg), 'd/m/Y') . " [".date_format(date_create($queryTransaksi->row()->jam_boleh_plg), 'H:i:s')."]" . "\n";

        $labelTglKeluar = "Tgl. Keluar      : " . date_format(date_create($querykunjungan->row()->tgl_keluar), 'd/m/Y') . " [".date_format(date_create($querykunjungan->row()->jam_keluar), 'H:i:s')."]";
        $labelSpesialisasi	 = "Spesialisasi : " . $querySpesialisasi->row()->spesialisasi;
		$Data .= $labelTglKeluar;// . "        " . $labelKelPasien . "\n";
		$Data .= str_pad($labelSpesialisasi, 50," ",STR_PAD_LEFT)."\n";
        //$Data .= "Jam Boleh Pulang : " . $querykunjungan->row()->jam_keluar . "        " . "Spesialisasi : " . $querySpesialisasi->row()->spesialisasi . "\n";
        $Data .= "-------------------------------------------------------------------------------------------------------------------------------------\n";
        //$Data .= str_pad("", $t1, " ") . str_pad("Uraian", $t2, " ") . str_pad("", $t5, " ") . str_pad("Harga", $t3, " ",STR_PAD_BOTH) . str_pad("", $t6, " ") . str_pad("Total", $t4, " ",STR_PAD_BOTH) . "\n";
        //$Data .= str_pad("Uraian", $t2, " ") . str_pad("", $t5, " ") . str_pad("Harga", $t3, " ",STR_PAD_BOTH) . str_pad("", $t6, " ") . str_pad("Total", $t4, " ",STR_PAD_BOTH) . "\n";
        $Data .= "-------------------------------------------------------------------------------------------------------------------------------------\n";
        $query = "select Distinct header,urut,kd_unit_tr from getallbillkasirrwi('".$KdKasir."','".$notrans."','".$costat."')  WHERE kd_unit_tr not like '1003%' order by urut ASC";
        $no = 0;
        $queryDet = $this->db->query($query)->result();
        foreach ($queryDet as $line) {
        	if ($line->header === 'NULL') {
				$Data       .= "\n";
				$detail_1   = $t1;
				$detail_2   = $t1;
				$kurangin_1 = 0;
				$kurangin_2 = 0;
        	}else{
				$detail_1   = ($t1*2)-$t1;
				$detail_2   = ($t1*3)-$t1;
				$kurangin_1 = $t1;
				$kurangin_2 = $t1*2;
        		//$Data .= str_pad("", $t1, " ") . str_pad($line->header, $t2, " ") . str_pad("", $t5, " ") . str_pad("", $t3, " ") . str_pad("", $t6, " ") . str_pad("", $t4, " ") . "\n";
        		$Data .= str_pad($line->header, $t2, " ") . str_pad("", $t5, " ") . str_pad("", $t3, " ") . str_pad("", $t6, " ") . str_pad("", $t4, " ") . "\n";
        	}			
			$tmpgroup 	 = $line->header;
			$tmpKdUnitTr = $line->kd_unit_tr;
			$querydetail = "select * from getallbillkasirrwi('".$KdKasir."','".$notrans."','".$costat."') where header = '".$tmpgroup."' AND kd_unit_tr='".$line->kd_unit_tr."' order by urut ASC";
			$resultdetail=$this->db->query($querydetail)->result();
			foreach ($resultdetail as $linedetail) 
			{

				$totaldetail = $linedetail->tarif * $linedetail->qty;
				if (strtolower($linedetail->desk_quantity) == 'true') {
					$desk_quantity = "Hari";
				}else{
					$desk_quantity = "X";
				}

				$Data .= 
					str_pad("", $detail_1, " ").
					str_pad($linedetail->deskripsi." ".$linedetail->qty." ".$desk_quantity, $t2-$kurangin_1, " ").
					str_pad(" Rp. ", $t5, " ", STR_PAD_LEFT).
					str_pad(number_format($linedetail->tarif,0,'.',','), $t3, " ", STR_PAD_LEFT).
					str_pad(" Rp. ", $t6, " ", STR_PAD_LEFT).
					str_pad(number_format($totaldetail,0,'.',','), $t4+8, " ", STR_PAD_LEFT) . "\n";
					//$Data .= "<br>";
					//DISINI
					$TmpTotNonPaviliun += $totaldetail;
					if (strtolower($linedetail->dokter) == 'true') {
						$querycekdokter = $this->db->query("SELECT DISTINCT(d.nama), v.jp as jp from 
							detail_transaksi dt 
							inner join
							visite_dokter v on dt.kd_kasir = v.kd_kasir AND dt.no_transaksi = v.no_transaksi AND dt.tgl_transaksi = v.tgl_transaksi AND dt.urut = v.urut 
							inner join dokter d ON d.kd_dokter = v.kd_dokter 
							where dt.kd_kasir = '".$linedetail->kd_kasir."' and dt.no_transaksi = '".$linedetail->no_transaksi."' and dt.kd_unit='".$linedetail->kd_unit."' and dt.kd_produk = '".$linedetail->kd_produk."' 
							order by d.nama asc");
						foreach ($querycekdokter->result() as $linedokter){
							$Data .= str_pad("", $detail_2, " ") . str_pad($linedokter->nama, $t2-($kurangin_2), " ");
							if (strtolower($linedetail->detail_dr) == 'true') {
								$Data .= str_pad(" Rp. ", $t5, " ", STR_PAD_LEFT) . str_pad(number_format($linedokter->jp,0,'.',','), $t3, " ", STR_PAD_LEFT);
								$Data .= str_pad(" Rp. ", $t6, " ", STR_PAD_LEFT) . str_pad(number_format(($linedokter->jp*$linedetail->qty),0,'.',','), $t4, " ", STR_PAD_LEFT)."\n";
							}else{
								$Data .= "\n";;
							}
							//$Data .= "<br>";
							//$Data .= $linedokter->nama."<br>";
						}
					}
			}
			$no++;
        }
        $Data .= $bold1;
        $Data .= str_pad("", ($t2-strlen("SUB TOTAL NON PAVILIUN")), " ") . str_pad("SUB TOTAL NON PAVILIUN", $t5, " ") . str_pad("", $t3+7, " ") . str_pad(" Rp. ", $t6-2, " ") . str_pad(number_format(($TmpTotNonPaviliun),0,'.',','), 18, " ", STR_PAD_LEFT)."\n";
        
		//$Data .= "<br>";
		//$Data .= "<br>";
		//$Data .= "<br>";
		//$Data .= str_pad("SUB TOTAL NON PAVILIUN", 90, " ", STR_PAD_LEFT).str_pad("Rp. ", 20, " ", STR_PAD_LEFT). str_pad(number_format(($TmpTotNonPaviliun),0,'.',','), 20, " ", STR_PAD_LEFT)."\n";
		$Data .= $bold0;
        $query = "select Distinct header,urut,kd_unit_tr from getallbillkasirrwi('".$KdKasir."','".$notrans."','".$costat."')  WHERE kd_unit_tr like '1003%' order by urut ASC";
        $no = 0;
        $queryDet = $this->db->query($query)->result();
        foreach ($queryDet as $line) {
        	if ($line->header === 'NULL') {
				$Data       .= "\n";
				$detail_1   = $t1;
				$detail_2   = $t1;
				$kurangin_1 = 0;
				$kurangin_2 = 0;
        	}else{
				$detail_1   = ($t1*2)-$t1;
				$detail_2   = ($t1*3)-$t1;
				$kurangin_1 = $t1;
				$kurangin_2 = $t1*2;
        		//$Data .= str_pad("", $t1, " ") . str_pad($line->header, $t2, " ") . str_pad("", $t5, " ") . str_pad("", $t3, " ") . str_pad("", $t6, " ") . str_pad("", $t4, " ") . "\n";
        		$Data .= str_pad($line->header, $t2, " ") . str_pad("", $t5, " ") . str_pad("", $t3, " ") . str_pad("", $t6, " ") . str_pad("", $t4, " ") . "\n";
        	}			
			$tmpgroup 	 = $line->header;
			$tmpKdUnitTr = $line->kd_unit_tr;
			$querydetail = "select * from getallbillkasirrwi('".$KdKasir."','".$notrans."','".$costat."') where header = '".$tmpgroup."' AND kd_unit_tr='".$line->kd_unit_tr."' order by urut ASC";
			$resultdetail=$this->db->query($querydetail)->result();
			foreach ($resultdetail as $linedetail) 
			{

				$totaldetail = $linedetail->tarif * $linedetail->qty;
				if ($linedetail->desk_quantity === true) {
					$desk_quantity = "Hari";
				}else{
					$desk_quantity = "X";
				}/*
				if (strlen($linedetail->deskripsi>)) {
					# code...
				}*/
				$Data .= 
					str_pad("", $detail_1, " ").
					str_pad($linedetail->deskripsi." ".$linedetail->qty." ".$desk_quantity, $t2-$kurangin_1, " ").
					str_pad(" Rp. ", $t5, " ", STR_PAD_LEFT).
					str_pad(number_format($linedetail->tarif,0,'.',','), $t3, " ", STR_PAD_LEFT).
					str_pad(" Rp. ", $t6, " ", STR_PAD_LEFT).
					str_pad(number_format($totaldetail,0,'.',','), $t4+8, " ", STR_PAD_LEFT) . "\n";
					//$Data .= "<br>";
					//DISINI
					$TmpTotPaviliun += $totaldetail;
					if (strtolower($linedetail->dokter) == 'true') {
						$querycekdokter = $this->db->query("SELECT DISTINCT(d.nama), v.jp as jp from 
							detail_transaksi dt 
							inner join
							visite_dokter v on dt.kd_kasir = v.kd_kasir AND dt.no_transaksi = v.no_transaksi AND dt.tgl_transaksi = v.tgl_transaksi AND dt.urut = v.urut 
							inner join dokter d ON d.kd_dokter = v.kd_dokter 
							where dt.kd_kasir = '".$linedetail->kd_kasir."' and dt.no_transaksi = '".$linedetail->no_transaksi."' and dt.kd_unit='".$linedetail->kd_unit."' and dt.kd_produk = '".$linedetail->kd_produk."' 
							order by d.nama asc");
						foreach ($querycekdokter->result() as $linedokter){
							$Data .= str_pad("", $detail_2, " ") . str_pad($linedokter->nama, $t2-($kurangin_2), " ");
							if (strtolower($linedetail->detail_dr) == 'true') {
								$Data .= str_pad(" Rp. ", $t5, " ", STR_PAD_LEFT) . str_pad(number_format($linedokter->jp,0,'.',','), $t3, " ", STR_PAD_LEFT);
								$Data .= str_pad(" Rp. ", $t6, " ", STR_PAD_LEFT) . str_pad(number_format(($linedokter->jp*$linedetail->qty),0,'.',','), $t4, " ", STR_PAD_LEFT)."\n";
							}else{
								$Data .= "\n";;
							}
							//$Data .= "<br>";
							//$Data .= $linedokter->nama."<br>";
						}
					}
			}
			$no++;
        }
        $Data .= $bold1;/*
        $Data .= str_pad("", $t1, " ",STR_PAD_LEFT).str_pad("SUB TOTAL PAVILIUN", 90, " ") . str_pad("", $t5, " ") . str_pad("", $t3, " ") . str_pad(" Rp. ", $t6, " ") . str_pad(number_format(($TmpTotPaviliun),0,'.',','), 20, " ", STR_PAD_LEFT)."\n";*/

        $Data .= str_pad("", ($t2-strlen("SUB TOTAL PAVILIUN")), " ") . str_pad("SUB TOTAL PAVILIUN", $t5, " ") . str_pad("", $t3+5, " ") . str_pad("   Rp. ", $t6, " ") . str_pad(number_format(($TmpTotPaviliun),0,'.',','), 18, " ", STR_PAD_LEFT)."\n";
		//$Data .= str_pad("SUB TOTAL PAVILIUN", 90, " ", STR_PAD_LEFT).str_pad("Rp. ", 20, " ", STR_PAD_LEFT). str_pad(number_format(($TmpTotPaviliun),0,'.',','), 20, " ", STR_PAD_LEFT)."\n";
        $Data .= $bold0;
        $Data .= "-------------------------------------------------------------------------------------------------------------------------------------\n";
        $queryJum = $this->db->query("select sum(harga) as total from (select harga from detail_transaksi where no_transaksi = '" . $Notrans . "') as x")->result();
        foreach ($queryJum as $line) {
        	$Data .= $bold1;
        	$Data .= str_pad($user, ($t2-strlen("Total BIAYA")), " ") . str_pad("TOTAL BIAYA", $t5, " ") . str_pad("", $t3+5, " ") . str_pad("   Rp. ", $t6, " ") . str_pad(number_format(($TmpTotPaviliun+$TmpTotNonPaviliun),0,'.',','), 18, " ", STR_PAD_LEFT)."\n";
        	$Data .= $bold0;
           // $Data .= str_pad("Total", 110, " ", STR_PAD_LEFT) . str_pad(number_format($line->total, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $queryTotJum = $this->db->query("select uraian,jumlah from detail_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $Notrans . "'")->result();
        foreach ($queryTotJum as $line) {
        	$Data .= $bold1;
        	$Data .= str_pad("", ($t2-strlen($line->uraian)), " ") . str_pad($line->uraian, $t5, " ") . str_pad("", $t3+5, " ") . str_pad("   Rp. ", $t6, " ") . str_pad(number_format($line->jumlah,0,'.',','), 18, " ", STR_PAD_LEFT)."\n";
        	$Data .= $bold0;
            //$Data .= str_pad($line->uraian, 110, " ", STR_PAD_LEFT) . str_pad(number_format($line->jumlah, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $Data .= "\n";
        $Data .= str_pad("Keterangan Rangkap : ", 65, " ") . str_pad($Kota . ' , ' . $today, 65, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad("- Lembar 1 : Pasien       ", 65, " ") .str_pad("Petugas Rincian Biaya Pasien", 65, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad("- Lembar 2 : Keuangan     ", 65, " ") ."\n";
        $Data .= str_pad("- Lembar 3 : Rekam Medis  ", 65, " ") ."\n";
        $Data .= str_pad("", 65-8, " ") . str_pad("Kasir Rawat Inap", 65, " ", STR_PAD_LEFT) . "\n";
        //$Data .= str_pad(" ", 110, " ") . str_pad($user, 20, " ", STR_PAD_BOTH) . "\n";
        $Data .= "\n";
        $Data .= str_pad("NB : Mohon maaf apabila ada lembar tagihan yang belum tertagihkan dalam perincian ini, akan ditagihkan kemudian", 65, " ") ."\n";// str_pad("Operator : " . $user, 65, " ", STR_PAD_LEFT) . "\n";
        //echo $Data;
        fwrite($handle, $Data);
        fclose($handle);
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
		}

        $error = "sukses";
        return $error;		
	}

	// public function cetaklaporanKwitansiRWI()
	// {
	// 	$param=json_decode($_POST['data']);

	// 	$kdspesial = $param->Spesialisasi;
	// 	$namaspesial = $param->dtlspesialisasi;
	// 	$Tglpulang = $param->Tglpulang;
	// 	$Tgljenisprt = $param->jenisprint;
	// 	$folio = $param->folio;
	// 	$notrans = $param->notrans;
	// 	$KdKasir = $param->KdKasir;
	// 	$reff = $param->reff;
	// 	$medrec = $param->kdpasien;
	// 	$nama = $param->nama;
	// 	$tglmasuk = $param->TglMasuk;
	// 	$kdunit =$param->kdunit;
	// 	$costat = $param->costat;
				
	// 	$kdUser=$this->session->userdata['user_id']['id'];
	// 	$user=$this->db->query("select user_names from zusers where kd_user = '$kdUser'")->row()->user_names;
	// 	$html='';
	// 	$html.='
	// 		<table class="t2" cellspacing="0" border="0">
	// 				<tr>
	// 					<th colspan="4" style="font-size:20px">KWITANSI</th>
	// 				</tr>
	// 		</table>
	// 		';
	// 	$jumlah = '';
	// 	$QueryJumlah = $this->db->query("select sum(jumlah) as jumlah from detail_bayar db
	// 									inner join payment p on db.kd_pay = p.kd_pay
	// 									where no_transaksi = '0208218' and p.kd_pay = 'TU'");
	// 	foreach ($QueryJumlah->result() as $data) {
	// 		$jumlah = $data->jumlah;
	// 	}

	// 	$querykunjungan = $this->db->query("select * from kunjungan where kd_pasien = '6-52-91-83' and kd_unit = '301' and tgl_masuk = '2013-03-04'");
	// 	foreach ($querykunjungan->result() as $data) {
	// 		$tglkeluar = $data->tgl_keluar;
	// 	}
	// 	$realtglkeluar = date_format( date_create($tglkeluar), 'd/F/Y' );
	// 	$realtglmasuk = date_format( date_create($tglmasuk), 'd/F/Y' );
	// 	$html.='
	// 		<table class="t1" border = "0">
	// 		  <tr>
	// 		    <td colspan="4" align="right">RSSM/FRRS/012/004</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="4" align="right">&nbsp;</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="3" align="right">No. Transaksi</td>
	// 		    <td width="221">'.$notrans.'</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="3" align="right">No. Medrec</td>
	// 		    <td>'.$medrec.'</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="3" align="right">&nbsp;</td>
	// 		    <td>&nbsp;</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td width="175">Telah Di terima dari</td>
	// 		    <td width="7">:</td>
	// 		    <td colspan="2">'.$nama.'</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td>Banyaknya Uang</td>
	// 		    <td>:</td>
	// 		    <td colspan="2">Rp. '.number_format($jumlah,0,'.',',').'</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="4">Untuk Pembayaran biaya pelayanan kesehatan rawat inap a/n '.$nama.'</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="4">Dari tanggal '.$realtglmasuk.' s/d '.$realtglkeluar.' di RSUD DR. SOEDONO MADIUN.</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td height="63" valign="top">Terbilang</td>
	// 		    <td valign="top">:</td>
	// 		    <td colspan="2" valign="top">'.terbilang($jumlah).' Rupiah</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="3" rowspan="2">&nbsp;</td>
	// 		    <td>Madiun, '.date("d F Y").'</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td>Yang Mengetahui</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td height="70" colspan="4">&nbsp;</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td colspan="3" rowspan="3">&nbsp;</td>
	// 		    <td>Bendahara</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td>RSUD RS. SOEDONO MADIUN</td>
	// 		  </tr>
	// 		  <tr>
	// 		    <td>(............................)</td>
	// 		  </tr>
	// 		   ';
	// 		$html.="</table>";
	// 		#Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
	// 		#- Margin
	// 		#- Print area
	// 		#- Type font
	// 		#- Paragraph alignment
	// 		#- Password protected
	// 		#Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
	// 		#Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
	// 		// echo $html;
 //            $table    = $html;

 //            # save $table inside temporary file that will be deleted later
 //            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
 //            file_put_contents($tmpfile, $table);
		
	// 		// # Create object phpexcel
 //            $objPHPExcel     = new PHPExcel();
            
	// 		# Fungsi untuk set PRINT AREA
	// 		$objPHPExcel->getActiveSheet()
 //                        ->getPageSetup()
 //                        ->setPrintArea('A1:E25');
	// 		# END Fungsi untuk set PRINT AREA			
						
	// 		# Fungsi untuk set MARGIN
	// 		$objPHPExcel->getActiveSheet()
	// 					->getPageMargins()->setTop(0.1);
	// 		$objPHPExcel->getActiveSheet()
	// 					->getPageMargins()->setRight(0.1);
	// 		$objPHPExcel->getActiveSheet()
	// 					->getPageMargins()->setLeft(0.1);
	// 		$objPHPExcel->getActiveSheet()
	// 					->getPageMargins()->setBottom(0.1);
	// 		# END Fungsi untuk set MARGIN
			
	// 		# Fungsi untuk set TYPE FONT 
	// 		$styleArrayHead = array(
	// 			'font'  => array(
	// 				'bold'  => true,
	// 				'size'  => 12,
	// 				'name'  => 'Courier New'
	// 			));
	// 		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHead);
	// 		$styleArrayBody = array(
	// 			'font'  => array(
	// 				'size'  => 9,
	// 				'name'  => 'Courier New'
	// 			));
	// 		$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
	// 		# END Fungsi untuk set TYPE FONT 
						
	// 		# Fungsi untuk set ALIGNMENT 
	// 		$objPHPExcel->getActiveSheet()
	// 					->getStyle('A1')
	// 					->getAlignment()
	// 					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// 		$objPHPExcel->getActiveSheet()
	// 					->getStyle('A3:A4')
	// 					->getAlignment()
	// 					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	// 		$objPHPExcel->getActiveSheet()
	// 					->getStyle('A5:A6:D5:D6')
	// 					->getAlignment()
	// 					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	// 		$objPHPExcel->getActiveSheet()
	// 					->getStyle('A12:C12')
	// 					->getAlignment()
	// 					->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	// 		$objPHPExcel->getActiveSheet()->getStyle('A12:C12')
 //    					->getAlignment()->setWrapText(true); 
	// 		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(22);
	// 		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(2);
	// 		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	// 		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
	// 		$objPHPExcel->getActiveSheet()->getRowDimension('12')->setRowHeight(30);
	// 		$objPHPExcel->getActiveSheet()->getRowDimension('15')->setRowHeight(50);
	// 		# END Fungsi untuk set ALIGNMENT 
			
	// 		# Fungsi untuk PROTECTED SHEET
	// 		$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
	// 		$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
	// 		$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
	// 		$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
	// 		# Set Password
	// 		$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
	// 		# END fungsi PROTECTED SHEET
			
	// 		# Fungsi AUTOSIZE

 //            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
 //            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
 //            $objPHPExcel->getActiveSheet()->setTitle('Kwitansi RWI'); # Change sheet's title if you want

 //            unlink($tmpfile); # delete temporary file because it isn't needed anymore

 //            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
 //            header('Content-Disposition: attachment;filename=laporan_penerimaan_per_pasien_rwj.xls'); # specify the download file name
 //            header('Cache-Control: max-age=0');

 //            # Creates a writer to output the $objPHPExcel's content
 //            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
 //            $writer->save('php://output');
 //            exit;
	// }

	
}
?>