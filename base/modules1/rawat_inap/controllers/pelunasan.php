<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class pelunasan extends MX_Controller
{
	private $setup_db_sql = false;
	private $id_user;
	private $dbSQL;
	private $tgl_now;

	public function __construct() {
		parent::__construct();
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_detail_bayar');
		$this->load->model('Tbl_data_detail_tr_bayar');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->tgl_now = date("Y-m-d");

		$this->setup_db_sql = $this->db->query("SELECT * from sys_setting where key_data = 'Setup_db_sql'");
		if ($this->setup_db_sql->num_rows() > 0) {
			$this->setup_db_sql = $this->setup_db_sql->row()->setting;
		}
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		}
	}

	public function cek_data(){
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$response 	= array();
		$resultSQL 	= false;
		$resultPG 	= false;
		
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		// $paramsCriteria = " no_transaksi = '".$params['no_transaksi']."' and kd_kasir = '".$params['kd_kasir']."' and kd_pay not in ('PT')";

		$paramsCriteria = array(
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_kasir' 		=> $params['kd_kasir'],
		);

		$query_detail_tr_bayar = $this->Tbl_data_detail_tr_bayar->getDetailTRBayar($paramsCriteria);
		$query_transaksi       = $this->Tbl_data_transaksi->getTransaksi($paramsCriteria);
		$query_detail_bayar    = $this->Tbl_data_detail_bayar->getDetailBayar($paramsCriteria);
		$this->db->select("*");
		$this->db->where($paramsCriteria);
		$this->db->from('detail_bayar_lunas');
		$query_detail_bayar_lunas = $this->db->get();

		if ($query_detail_bayar_lunas->num_rows() == 0) {
			foreach ($query_detail_bayar->result() as $result) {
				if ($result->kd_pay != 'PT') {
					unset($paramsInsert);
					$paramsInsert = array(
						'kd_kasir' 		=> $result->kd_kasir,
						'no_transaksi' 	=> $result->no_transaksi,
						'urut' 			=> $result->urut,
						'tgl_transaksi' => $result->tgl_transaksi,
						'kd_user' 		=> $result->kd_user,
						'kd_unit' 		=> $result->kd_unit,
						'kd_pay' 		=> $result->kd_pay,
						'jumlah' 		=> $result->jumlah,
						'folio' 		=> $result->folio,
						'shift' 		=> $result->shift,
						'status_bayar'	=> '1',
						// 'deposit'		=> $result->deposit,
						'kd_dokter'		=> $result->kd_dokter,
						// 'tag'			=> $result->tag,
						'sisa'			=> $result->sisa,
					);
					
					/*if ($result->status_bayar === true || $result->status_bayar == 1) {
						$paramsInsert['status_bayar'] = '1';
					}else{
						$paramsInsert['status_bayar'] = '0';
					}*/

					$resultPG 	= $this->db->insert('detail_bayar_lunas', $paramsInsert);
					if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
						$resultSQL 	= $this->dbSQL->insert('detail_bayar_lunas', $paramsInsert);
					}else{
						$resultSQL 	= true;
					}
					$response['tahap'] = "Insert detail bayar lunas";
					if (($resultSQL == 0 || $resultSQL===false) && ($resultPG == 0 || $resultPG===false)) {
						break;
						$resultSQL 	= false;
						$resultPG 	= false;
					}
				}
			}

			if (($resultSQL > 0 || $resultSQL===true) && ($resultPG > 0 || $resultPG===true)) {
				foreach ($query_detail_tr_bayar->result() as $result_tr_bayar) {
					if ($result_tr_bayar->kd_pay != 'PT') {
					unset($paramsInsert);
						$paramsInsert = array(
							'kd_kasir' 		=> $result_tr_bayar->kd_kasir,
							'no_transaksi' 	=> $result_tr_bayar->no_transaksi,
							'urut' 			=> $result_tr_bayar->urut,
							'tgl_transaksi' => $result_tr_bayar->tgl_transaksi,
							// 'kd_user' 		=> $result->kd_user,
							// 'kd_unit' 		=> $result->kd_unit,
							'urut_bayar' 	=> $result_tr_bayar->urut_bayar,
							'kd_pay' 		=> $result_tr_bayar->kd_pay,
							'jumlah' 		=> $result_tr_bayar->jumlah,
							'tgl_bayar'		=> $result_tr_bayar->tgl_bayar,
							// 'folio' 		=> $result->folio,
							// 'shift' 		=> $result->shift,
							// 'status_bayar'	=> $result->status_bayar,
							// 'deposit'		=> $result->deposit,
							// 'kd_dokter'		=> $result->kd_dokter,
							// 'tag'			=> $result->tag,
							// 'sisa'			=> $result->sisa,
						);

						$resultPG 	= $this->db->insert('detail_tr_bayar_lunas', $paramsInsert);
						if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
							$resultSQL 	= $this->dbSQL->insert('detail_tr_bayar_lunas', $paramsInsert);
						}else{
							$resultSQL 	= true;
						}
						if (($resultSQL == 0 || $resultSQL===false) && ($resultPG == 0 || $resultPG===false)) {
							break;
							$resultSQL 	= false;
							$resultPG 	= false;
						}
					}
				}
			}else{
				$resultSQL 	= false;
				$resultPG 	= false;
			}

			if (($resultSQL > 0 || $resultSQL===true) && ($resultPG > 0 || $resultPG===true)) {
				$resultPG = $this->db->query("INSERT INTO 
						detail_tr_bayar_component_lunas(
							kd_kasir,
							no_transaksi,
							urut,
							tgl_transaksi,
							kd_pay,
							urut_bayar,
							tgl_bayar,
							kd_component,
							jumlah)
							SELECT 
								dtb.kd_kasir, 
								dtb.no_transaksi, 
								dtb.urut, 
								dtb.tgl_transaksi, 
								dtb.kd_pay, 
								dtb.urut_bayar, 
								dtb.tgl_bayar, 
								dc.kd_component, 
								(dtb.jumlah/ dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
							FROM detail_tr_bayar_lunas dtb 
							INNER JOIN detail_transaksi dt ON 
								dt.kd_kasir = dtb.kd_kasir and 
								dt.no_transaksi = dtb.no_transaksi and 
								dt.urut = dtb.urut and 
								dt.tgl_transaksi = dtb.tgl_transaksi 
							INNER JOIN detail_component dc ON 
								dt.kd_kasir = dc.kd_kasir and 
								dt.no_transaksi = dc.no_transaksi and 
								dt.urut = dc.urut and 
								dt.tgl_transaksi = dc.tgl_transaksi 
							INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
							WHERE 
							dtb.kd_Kasir = '".$params['kd_kasir']."' AND dtb.no_transaksi ='".$params['no_transaksi']."'");
				if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
					$resultSQL = $this->dbSQL->query("INSERT INTO 
							detail_tr_bayar_component_lunas(
								kd_kasir,
								no_transaksi,
								urut,
								tgl_transaksi,
								kd_pay,
								urut_bayar,
								tgl_bayar,
								kd_component,
								jumlah)
								SELECT 
									dtb.kd_kasir, 
									dtb.no_transaksi, 
									dtb.urut, 
									dtb.tgl_transaksi, 
									dtb.kd_pay, 
									dtb.urut_bayar, 
									dtb.tgl_bayar, 
									dc.kd_component, 
									(dtb.jumlah/ dt.Harga) * (dc.tarif - dc.disc + dc.markup) as bayar 
								FROM detail_tr_bayar_lunas dtb 
								INNER JOIN detail_transaksi dt ON 
									dt.kd_kasir = dtb.kd_kasir and 
									dt.no_transaksi = dtb.no_transaksi and 
									dt.urut = dtb.urut and 
									dt.tgl_transaksi = dtb.tgl_transaksi 
								INNER JOIN detail_component dc ON 
									dt.kd_kasir = dc.kd_kasir and 
									dt.no_transaksi = dc.no_transaksi and 
									dt.urut = dc.urut and 
									dt.tgl_transaksi = dc.tgl_transaksi 
								INNER JOIN produk_component pc ON dc.kd_component = pc.kd_component 
								WHERE dtb.kd_Kasir = '".$params['kd_kasir']."' AND dtb.no_transaksi ='".$params['no_transaksi']."'");
				}else{
					$resultSQL = true;
				}
			}else{
				$resultSQL 	= false;
				$resultPG 	= false;
			}
		}

		if (($resultSQL > 0 || $resultSQL===true) && ($resultPG > 0 || $resultPG===true)) {
			$this->db->trans_commit();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_commit();
			}
			$response['status'] 	= true;
		}else{
			$this->db->trans_rollback();
			if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
				$this->dbSQL->trans_rollback();
			}
			$response['status'] 	= false;
		}
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
		echo json_encode($response);
	}

	public function get_data_transaksi(){
		$response = array();
		$resultPG = false;
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$resultPG = $this->db->query("SELECT * from transaksi WHERE no_transaksi = '".$params['no_transaksi']."' AND kd_kasir = '".$params['kd_kasir']."'");

		if ($resultPG->num_rows() > 0) {
			$data = array();
			$index= 0;

			foreach ($resultPG->result() as $result) {
				$data['kd_kasir']      = $result->kd_kasir;
				$data['no_transaksi']  = $result->no_transaksi;
				$data['kd_pasien']     = $result->kd_pasien;
				$data['kd_unit']       = $result->kd_unit;
				$data['kd_unit']       = $result->kd_unit;
				$data['tgl_transaksi'] = $result->tgl_transaksi;
				$data['urut_masuk']    = $result->urut_masuk;
				$index++;
			}
			$response['data'] = $data;
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		$response['result'] = $resultPG->num_rows();
		echo json_encode($response);
		$this->db->close();
	}

	public function get_data_detail_transaksi(){
		$response = array();
		$resultPG = false;
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$resultPG = $this->db->query("SELECT (qty*harga) as harga from detail_transaksi WHERE no_transaksi = '".$params['no_transaksi']."' AND kd_kasir = '".$params['kd_kasir']."'");

		if ($resultPG->num_rows() > 0) {
			$data = array();
			$index= 0;

			$data['harga'] = 0;
			foreach ($resultPG->result() as $result) {
				$data['harga']      += $result->harga;
				$index++;
			}
			$response['data'] = $data;
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		$response['result'] = $resultPG->num_rows();
		echo json_encode($response);
		$this->db->close();
	}

	public function get_data_detail_tr_bayar_lunas(){
		$response = array();
		$resultPG = false;
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$resultPG = $this->db->query("SELECT * from detail_tr_bayar_lunas WHERE no_transaksi = '".$params['no_transaksi']."' AND kd_kasir = '".$params['kd_kasir']."'");

		if ($resultPG->num_rows() > 0) {
			$data = array();
			$index= 0;

			$data['harga'] = 0;
			foreach ($resultPG->result() as $result) {
				$data['harga']      += $result->jumlah;
				$index++;
			}
			$response['data'] = $data;
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		$response['result'] = $resultPG->num_rows();
		echo json_encode($response);
		$this->db->close();
	}

	public function get_data_sisa_tagihan(){
		$response = array();
		$resultPG = false;
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
		);

		$resultTotal = $this->db->query("SELECT sum(harga*qty) as harga from detail_transaksi WHERE no_transaksi = '".$params['no_transaksi']."' AND kd_kasir = '".$params['kd_kasir']."'");
		$resultDibayar = $this->db->query("SELECT sum(jumlah) as jumlah from detail_tr_bayar_lunas WHERE no_transaksi = '".$params['no_transaksi']."' AND kd_kasir = '".$params['kd_kasir']."'");

		if ($resultTotal->num_rows() > 0 && $resultDibayar->num_rows() > 0) {
			$data = array();
			$index= 0;

			$data['harga'] = 0;
			$data['total_tagihan'] = $resultTotal->row()->harga;
			$data['total_dibayar'] = $resultDibayar->row()->jumlah;
			$data['harga'] = round($resultTotal->row()->harga - $resultDibayar->row()->jumlah);
			
			$response['data'] = $data;
			$response['status'] = true;
		}else{
			$response['status'] = false;
		}
		
		echo json_encode($response);
		$this->db->close();
	}

	public function update_verified(){
		$response 	= array();
		$resultPG 	= false;
		$resultSQL 	= false;
		$this->db->trans_begin();
		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'verified' 		=> $this->input->post('verified'),
		);


		$paramsCriteria = array(
			'no_transaksi' 	=> $params['no_transaksi'],
			'kd_kasir' 		=> $params['kd_kasir'],
		);

		if ($params['verified'] === true || $params['verified'] == 'true') {
			$paramsUpdate = array(
				'kd_user_ver' 	=> $this->id_user,
				'tgl_ver' 		=> $this->tgl_now,
				'verified' 		=> 1,
			);
			// $paramsUpdate['verified'] = 1;
		}else{
			$paramsUpdate = array(
				'kd_user_ver' 	=> 0,
				'tgl_ver' 		=> $this->tgl_now,
				'verified' 		=> 0,
			);
			// $paramsUpdate['verified'] = 0;
		}

		$this->db->where($paramsCriteria);
		$resultPG = $this->db->update("detail_bayar_lunas", $paramsUpdate);
			
		$this->dbSQL->where($paramsCriteria);
		$resultSQL = $this->dbSQL->update("detail_bayar_lunas", $paramsUpdate);
// $resultSQL	= true;
		if (($resultPG > 0 || $resultPG === true ) && ($resultSQL > 0 || $resultSQL === true )) {
			$data = array();
			$this->db->trans_commit();
			$this->dbSQL->trans_commit();
			$response['status'] = true;
		}else{
			$this->db->trans_rollback();
			$this->dbSQL->trans_rollback();
			$response['status'] = false;
		}
		
		echo json_encode($response);
		$this->db->close();
		$this->dbSQL->close();
	}

	public function cek_verified(){
		$response 	= array();
		$resultPG 	= false;
		$resultSQL 	= false;
		$this->db->trans_begin();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->trans_begin();
		}

		$params = array(
			'no_transaksi' 	=> $this->input->post('no_transaksi'),
			'kd_kasir' 		=> $this->input->post('kd_kasir'),
			'verified' 		=> $this->input->post('verified'),
		);

		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->select("*");
			$this->dbSQL->where($params);
			$this->dbSQL->from("detail_bayar_lunas");
			$resultPG = $this->dbSQL->get();
		}else{
			$this->db->select("*");
			$this->db->where($params);
			$this->db->from("detail_bayar_lunas");
			$resultPG = $this->db->get();
		}
		
		if ($resultPG->num_rows() > 0) {
			$data = array();
			$response['status'] = true;
			$response['verified'] = $params['verified'];
		}else{
			$response['status'] = false;
		}
		
		echo json_encode($response);
		$this->db->close();
		if ($this->setup_db_sql === true || $this->setup_db_sql == 'true') {
			$this->dbSQL->close();
		}
	}
}

?>
