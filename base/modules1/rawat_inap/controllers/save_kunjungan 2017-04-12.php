<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class save_kunjungan extends MX_Controller
{
	private $id_user;
	private $dbSQL;
	private $tgl_now                = "";
	private $key_data_kd_kasir      = "default_kd_kasir_rwi";
	private $key_data_jasa_dok      = "pel_jasa_dok";
	private $key_data_jasa_anastasi = "pel_JasaDokterAnestasi";
	private $ErrorPasien            = false;
	private $AppId                  = "";
	private $no_medrec              = "";
	private $tmp_kd_unit            = "";
	private $tmp_kd_kasir           = "";
	private $tmp_kd_kecamatan       = "";
	private $tmp_antrian            = "";
	private $tmp_shift_bagian       = "";
	
	private $tmp_get_appto          = "";
	private $tmp_kd_tarif           = "";
	private $tmp_tgl_berlaku        = "";
	private $tmp_kd_produk        	= "";
	private $notrans                = "";

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
		$this->load->model('Tbl_data_pasien');
		$this->load->model('Tbl_data_kunjungan');
		$this->load->model('Tbl_data_transaksi');
		$this->load->model('Tbl_data_tarif');
		$this->load->model('Tbl_data_detail_transaksi');
		$this->load->model('Tbl_data_detail_component');
		$this->load->model('Tbl_data_pembayaran');
		$this->load->model('rawat_inap/Model_pindah_kamar');
		$this->load->model('rawat_inap/Model_visite_dokter');
		$this->tgl_now = date("Y-m-d");

		//$this->dbSQL->db_debug 	= TRUE;
		//$this->db->db_debug 	= TRUE;
	}

	public function save(){
		$response = array();
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$data = array(
			'NoMedrec'                     => $this->input->post('NoMedrec'),
			'NamaPasien'                   => $this->input->post('NamaPasien'),
			'NamaKeluarga'                 => $this->input->post('NamaKeluarga'),
			'JenisKelamin'                 => $this->input->post('JenisKelamin'),
			'Tempatlahir'                  => $this->input->post('Tempatlahir'),
			'TglLahir'                     => $this->input->post('TglLahir'),
			'Agama'                        => $this->input->post('Agama'),
			'GolDarah'                     => $this->input->post('GolDarah'),
			'StatusMarita'                 => $this->input->post('StatusMarita'),
			'StatusWarga'                  => $this->input->post('StatusWarga'),
			'Alamat'                       => $this->input->post('Alamat'),
			'No_Tlp'                       => $this->input->post('No_Tlp'),
			'Pendidikan'                   => $this->input->post('Pendidikan'),
			'Pekerjaan'                    => $this->input->post('Pekerjaan'),
			'suami_istrinya'               => $this->input->post('suami_istrinya'),
			'Pendidikan_Ayah'              => $this->input->post('Pendidikan_Ayah'),
			'Pekerjaan_Ayah'               => $this->input->post('Pekerjaan_Ayah'),
			'Pendidikan_Ibu'               => $this->input->post('Pendidikan_Ibu'),
			'Pekerjaan_Ibu'                => $this->input->post('Pekerjaan_Ibu'),
			'Pendidikan_SuamiIstri'        => $this->input->post('Pendidikan_SuamiIstri'),
			'Pekerjaan_SuamiIstri'         => $this->input->post('Pekerjaan_SuamiIstri'),
			'NamaPeserta'                  => $this->input->post('NamaPeserta'),
			'KD_Asuransi'                  => $this->input->post('KD_Asuransi'),
			'NoAskes'                      => $this->input->post('NoAskes'),
			'NoSjp'                        => $this->input->post('NoSjp'),
			'Kd_Suku'                      => $this->input->post('Kd_Suku'),
			'Jabatan'                      => $this->input->post('Jabatan'),
			'Perusahaan'                   => $this->input->post('Perusahaan'),
			'Perusahaan1'                  => $this->input->post('Perusahaan1'),
			'Cek'                          => $this->input->post('Cek'),
			'Poli'                         => $this->input->post('Poli'),
			'TanggalMasuk'                 => $this->input->post('TanggalMasuk'),
			'UrutMasuk'                    => $this->input->post('UrutMasuk'),
			'JamKunjungan'                 => $this->input->post('JamKunjungan'),
			'CaraPenerimaan'               => $this->input->post('CaraPenerimaan'),
			'KdRujukan'                    => $this->input->post('KdRujukan'),
			'KdCustomer'                   => $this->input->post('KdCustomer'),
			'KdDokter'                     => $this->input->post('KdDokter'),
			'Baru'                         => $this->input->post('Baru'),
			'Shift'                        => $this->input->post('Shift'),
			'Karyawan'                     => $this->input->post('Karyawan'),
			'Kontrol'                      => $this->input->post('Kontrol'),
			'Antrian'                      => $this->input->post('Antrian'),
			'NoSurat'                      => $this->input->post('NoSurat'),
			'Alergi'                       => $this->input->post('Alergi'),
			'Anamnese'                     => $this->input->post('Anamnese'),
			'TahunLahir'                   => $this->input->post('TahunLahir'),
			'BulanLahir'                   => $this->input->post('BulanLahir'),
			'HariLahir'                    => $this->input->post('HariLahir'),
			'Kota'                         => $this->input->post('Kota'),
			'Kd_Kecamatan'                 => $this->input->post('Kd_Kecamatan'),
			'Kelurahan'                    => $this->input->post('Kelurahan'),
			'AsalPasien'                   => $this->input->post('AsalPasien'),
			'KDPROPINSI'                   => $this->input->post('KDPROPINSI'),
			'KDKABUPATEN'                  => $this->input->post('KDKABUPATEN'),
			'KDKECAMATAN'                  => $this->input->post('KDKECAMATAN'),
			'KDKECAMATANKTP'               => $this->input->post('KDKECAMATANKTP'),
			'KDPENDIDIKAN'                 => $this->input->post('KDPENDIDIKAN'),
			'KDPEKERJAAN'                  => $this->input->post('KDPEKERJAAN'),
			'KDAGAMA'                      => $this->input->post('KDAGAMA'),
			'NAMA_PJ'                      => $this->input->post('NAMA_PJ'),
			'AlamatPJ'                     => $this->input->post('AlamatPJ'),
			'TgllahirPJ'                   => $this->input->post('TgllahirPJ'),
			'WNIPJ'                        => $this->input->post('WNIPJ'),
			'KelurahanPJ'                  => $this->input->post('KelurahanPJ'),
			'PosPJ'                        => $this->input->post('PosPJ'),
			'TeleponPj'                    => $this->input->post('TeleponPj'),
			'HpPj'                         => $this->input->post('HpPj'),
			'KTP'                          => $this->input->post('KTP'),
			'KelurahanKtpPJ'               => $this->input->post('KelurahanKtpPJ'),
			'AlamatKtpPJ'                  => $this->input->post('AlamatKtpPJ'),
			'KdPosKtp'                     => $this->input->post('KdPosKtp'),
			'KdPendidikanPj'               => $this->input->post('KdPendidikanPj'),
			'kdpekerjaanPj'                => $this->input->post('kdpekerjaanPj'),
			'PerusahaanPj'                 => $this->input->post('PerusahaanPj'),
			'HubunganPj'                   => $this->input->post('HubunganPj'),
			'AlamatKtpPasien'              => $this->input->post('AlamatKtpPasien'),
			'KdKelurahanKtpPasien'         => $this->input->post('KdKelurahanKtpPasien'),
			'KdPostKtpPasien'              => $this->input->post('KdPostKtpPasien'),
			'NamaAyahPasien'               => $this->input->post('NamaAyahPasien'),
			'NamaIbuPasien'                => $this->input->post('NamaIbuPasien'),
			'KdposPasien'                  => $this->input->post('KdposPasien'),
			'EmailPenanggungjawab'         => $this->input->post('EmailPenanggungjawab'),
			'tempatlahirPenanggungjawab'   => $this->input->post('tempatlahirPenanggungjawab'),
			'StatusMaritalPenanggungjawab' => $this->input->post('StatusMaritalPenanggungjawab'),
			'TLPNPasien'                   => $this->input->post('TLPNPasien'),
			'HPPasien'                     => $this->input->post('HPPasien'),
			'EmailPasien'                  => $this->input->post('EmailPasien'),
			'JKpenanggungJwab'             => $this->input->post('JKpenanggungJwab'),
			'RadioRujukanPasien'           => $this->input->post('RadioRujukanPasien'),
			'PasienBaruRujukan'            => $this->input->post('PasienBaruRujukan'),
			'NoNIK'                        => $this->input->post('NoNIK'),
			'NonKunjungan'                 => $this->input->post('NonKunjungan'),
			'KdDiagnosa'                   => $this->input->post('KdDiagnosa'),
			'part_number_nik'              => $this->input->post('part_number_nik'),
			'unitMana'                     => $this->input->post('unitMana'),
			'JenisRujukan'                 => $this->input->post('JenisRujukan'),
			'spesialisasi'                 => $this->input->post('spesialisasi'),
			'Kamar'                        => $this->input->post('Kamar'),
			'Kelas'                        => $this->input->post('Kelas'),
			'Ruang'                        => $this->input->post('Ruang'),
			'KDUnitKamar'                  => $this->input->post('KDUnitKamar'),
			'BayiBrLahir'                  => $this->input->post('BayiBrLahir'),
		);
			$response['bayi'] 		= false;
			// if (isset($data['TanggalMasuk'])) {
				// $date = date_create(str_replace('/', '-', $data['TanggalMasuk']));
				// $this->tgl_now = date_format($date,"Y-m-d");
			// }
				if ($data["NoMedrec"] == "" || $data["NoMedrec"] == "Automatic from the system...") {
					$this->no_medrec = $this->Tbl_data_pasien->getNoMedrecSQL();
				} else  {
					$this->no_medrec = $data["NoMedrec"];
				}

				/*
					========================================= SIMPAN DATA PASIEN JIKA BAYI BARU LAHIR
				*/
				if ($data['BayiBrLahir'] === true || $data['BayiBrLahir'] === 'True') {
					$this->ErrorPasien = $this->simpan_pasien($data);
					$response['bayi'] 		= true;
					$data['BayiBrLahir'] 	= false;
				}

				/*
					========================================= SIMPAN DATA KUNJUNGAN
				*/
				if ($data['BayiBrLahir'] !== true || $data['BayiBrLahir'] !== 'True') {
					$this->ErrorPasien = $this->simpan_kunjungan($data);
				}else{
					$this->ErrorPasien = false;
				}

				/*
					========================================= SIMPAN DATA TRANSAKSI
				*/
				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$this->ErrorPasien = $this->simpanTransaksi($data);
				}else{
					$this->ErrorPasien = false;
				}

				/*
					========================================= SIMPAN DATA VISITE DOKTER
				*/
				/*if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$criteria_dokter_int = array(
						'kd_job' 	=> 1,
					);

					$result_dokter_int = $this->Model_visite_dokter->getDataDokterInapSQL($criteria_dokter_int);
					if ($result_dokter_int!=false) {
						$data['kd_component'] = $result_dokter_int->row()->kd_component;
						$data['kd_job']       = $result_dokter_int->row()->kd_job;
						$data['prc']       	= $result_dokter_int->row()->prc;
					}

					$criteria_tarif_component = array(
						'kd_component' 	=> $data['kd_component'],
						'kd_tarif' 		=> $data['kd_tarif'],
						'kd_produk' 	=> $data['kd_produk'],
						'kd_unit' 		=> $data['kd_unit'],
						'tgl_berlaku' 	=> $this->tgl_now,
					);

					$this->ErrorPasien = $this->simpanVisiteDokter($data);
				}else{
					$this->ErrorPasien = false;
				}*/
				/*
					========================================= SIMPAN DATA PASIEN INAP
				*/
				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$this->ErrorPasien = $this->simpanPasienInap($data);
				}else{
					$this->ErrorPasien = false;
				}

				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$this->ErrorPasien = $this->simpanNginap($data);
					//$this->ErrorPasien = false;
				}else{
					$this->ErrorPasien = false;
				}

				/*
					========================================= UPDATE STATUS DIGUNAKAN OLEH KAMAR SETELAHNYA 
				*/
				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$criteriaKamarInduk = array(
						'no_kamar' 	=> $data['Kamar'],
					);
					$paramsKamarIndukLast['digunakan'] = $this->operateQuantityDigunakan($data['Kamar'], "tambah");

					$this->ErrorPasien = $this->Model_pindah_kamar->updateKamarSQL($criteriaKamarInduk, $paramsKamarIndukLast);
					if($this->ErrorPasien === true || $this->ErrorPasien > 0){
						$result    = $this->Model_pindah_kamar->updateKamar($criteriaKamarInduk, $paramsKamarIndukLast);
					}else{
						$this->ErrorPasien = false;
					}
				}else{
					$this->ErrorPasien = false;
				}

/*
				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$this->ErrorPasien = $this->simpanDetailTransaksi($data);
					//$response['ErrorPasien'] = $this->ErrorPasien;
					//$this->ErrorPasien = false;
				}else{
					$this->ErrorPasien = false;
				}

				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$this->simpanDetailTrDokter($data);
				}else{
					$this->ErrorPasien = false;
				}

				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {					
					$criteriaCustom = array(
						'table' 		=> 'zusers',
						'field_return' 	=> 'auto_paid',
						'field_criteria'=> 'kd_user',
						'value_criteria'=> $this->id_user,
					);
					
					$paid = $this->Tbl_data_transaksi->getCustom($criteriaCustom);

					if ($paid == 1 || $paid == '1') {
						$criteriaDetail = array(
							'kd_kasir' 		=> $this->tmp_kd_kasir,
							'no_transaksi' 	=> (string)$this->notrans,
						);
						$queryDetail = $this->Tbl_data_detail_transaksi->getDataDetailTransaksi($criteriaDetail);
						$totalBayar = 0;
						foreach ($queryDetail->result() as $result) {
							$totalBayar+=((int)$result->qty*(int)$result->harga);
						}
						if ($totalBayar>0) {
							$criteriaPay = array(
								'table' 		=> 'payment',
								'field_return' 	=> 'kd_pay',
								'field_criteria'=> 'kd_customer',
								'value_criteria'=> $data["KdCustomer"],
							);
							$resPay = $this->Tbl_data_transaksi->getCustom($criteriaPay);

							$paramsPembayaran = array(
								'kd_kasir' 		=> $this->tmp_kd_kasir,
								'no_transaksi' 	=> $this->notrans,
								'urut' 			=> $queryDetail->row()->urut,
								'tgl_transaksi'	=> $this->tgl_now,
								'id_user'		=> $this->id_user,
								'kd_unit'		=> $this->tmp_kd_unit,
								'kd_pay'		=> $resPay,
								'total_bayar'	=> $totalBayar,
								'folio'			=> 'A',
								'shift'			=> $queryDetail->row()->shift,
								'status_bayar' 	=> 1,
								'tgl_bayar' 	=> $this->tgl_now,
								'urut_bayar' 	=> 1,
							);

							$resultQuerySQL_trpembayaran 	= $this->Tbl_data_pembayaran->insertTRPembayaran_SQL($paramsPembayaran); 
							$resultQuerySQL_pembayaran 		= $this->Tbl_data_pembayaran->insertPembayaran_SQL($paramsPembayaran); 
							$paramsPembayaran['status_bayar'] = 'true';
							$resultQueryPG_trpembayaran 	= $this->Tbl_data_pembayaran->insertTRPembayaran($paramsPembayaran); 
							$resultQueryPG_pembayaran 		= $this->Tbl_data_pembayaran->insertPembayaran($paramsPembayaran);
							
							if (
									//($resultQuerySQL_trpembayaran === true || $resultQuerySQL_trpembayaran > 0) &&
									//($resultQuerySQL_pembayaran === true || $resultQuerySQL_pembayaran > 0) &&
									($resultQueryPG_trpembayaran === true || $resultQueryPG_trpembayaran > 0) &&
									($resultQueryPG_pembayaran === true || $resultQueryPG_pembayaran > 0)
								) 
							{
								$this->ErrorPasien = true;
							}else{
								$this->ErrorPasien = false;
							}
						}
					}
				}else{
					$this->ErrorPasien = false;
				}
*/				//$this->ErrorPasien = false;
				if (($this->ErrorPasien === true || $this->ErrorPasien > 0)) {
					$this->db->trans_commit();
					$this->dbSQL->trans_commit();
					$this->db->close();
					$this->dbSQL->close();
					$response['success']   = true;
					$response['KD_PASIEN'] = $this->no_medrec;
				}else{
					$this->db->trans_rollback();
					$this->dbSQL->trans_rollback();
					$this->db->close();
					$this->dbSQL->close();
					$response['success']   	= false;
					$response['KD_PASIEN'] 	= $this->no_medrec;
					$response['ErrorPasien']= $this->ErrorPasien;
				}
		echo json_encode($response);
	}	

	/*
		FUNCTION PRIVATE
		SIMPAN DATA PASIEN
		================================================================================================================================================
		================================================================================================================================================
	 */
	private function simpan_pasien($data){
		$strError          = "";
		$suku              = 0;
		$insert_sql_pasien = false;
		$insert_pasien     = false;

		/* 	======================= GOL DARAH ============================*/
		if ($data["GolDarah"] === "") {
			$data["GolDarah"] = 0;
		}

		/* ======================= KECAMATAN ============================*/
		if ($data["KDKECAMATAN"] === "") {
			$this->tmp_kd_kecamatan = $data["Kd_Kecamatan"];
			$tmpkabupaten = $data["AsalPasien"];
			if ($data["Pendidikan"]=='' || $data["Pendidikan"]=='undefined'){
				$tmppendidikan =0;
			}else{
				$tmppendidikan = $data["Pendidikan"];
			}
			if ($data["Pekerjaan"]=='' || $data["Pekerjaan"]=='undefined'){
				$tmppekerjaan =0;
			}else{
				$tmppekerjaan = $data["Pekerjaan"];
			}
			$tmpagama = $data["Agama"];
		}else{
			$this->tmp_kd_kecamatan = $data["KDKECAMATAN"];
			$tmpkabupaten = $data["KDKABUPATEN"];
			if ($data["KDPENDIDIKAN"]=='' || $data["KDPENDIDIKAN"]=='undefined'){
				$tmppendidikan =0;
			}else{
				$tmppendidikan = $data["KDPENDIDIKAN"];
			}
			if ($data["KDPEKERJAAN"]=='' || $data["KDPEKERJAAN"]=='undefined'){
				$tmppekerjaan =0;
			}else{
				$tmppekerjaan = $data["KDPEKERJAAN"];
			}
			$tmpagama = $data["KDAGAMA"];
		}
		
		/* ======================= PERUSAHAAN ============================*/
		if ($data['Cek'] == 'Perseorangan' or $data['Cek'] == 'Perusahaan') {
			$kode_asuransi = NULL;
		} else {
			$kode_asuransi = $data["KD_Asuransi"];
		}
		if ($data["NoAskes"] == "") {
			$tmpno_asuransi = $data["NoSjp"];
		} else {
			$tmpno_asuransi = $data["NoAskes"];
		}
		if ($data["Kelurahan"] == "" or $data["Kelurahan"] === NULL or $data["Kelurahan"] === "Pilih Kelurahan...") {
			$intkd_lurah = $this->GetKdLurah($data['Kd_Kecamatan']);
		} else {
			if (is_numeric($data["Kelurahan"])) {
				$intkd_lurah = $data["Kelurahan"];
			} else {
				$intkd_lurah = $this->GetKdLurah($data['Kd_Kecamatan']);
			}
		}

		/* ======================= KELURAHAN PASIEN KTP ============================*/
		if ($data["KdKelurahanKtpPasien"] == "" or $data["KdKelurahanKtpPasien"] === NULL or $data["KdKelurahanKtpPasien"] === "Pilih Kelurahan...") {
			$intkd_lurahktp = $this->GetKdLurah($data['Kd_Kecamatan']);
		} else {
			$intkd_lurahktp = $data["KdKelurahanKtpPasien"];
		}

		/* ======================= PENDIDIKAN AYAH ============================*/
		if ($data["Pendidikan_Ayah"]=='' || $data["Pendidikan_Ayah"]=='undefined'){
			$pendAyah = 0;
		}else{
			$pendAyah = $data["Pendidikan_Ayah"];
		}
		/* ======================= PENDIDIKAN IBU ============================*/
		if ($data["Pendidikan_Ibu"]=='' || $data["Pendidikan_Ibu"]=='undefined'){
			$pendIbu = 0;
		}else{
			$pendIbu = $data["Pendidikan_Ibu"];
		}

		/* ======================= PENDIDIKAN AYAH ============================*/
		if ($data["Pekerjaan_Ayah"]=='' || $data["Pekerjaan_Ayah"]=='undefined'){
			$pekerAyah = 0;
		}else{
			$pekerAyah = $data["Pekerjaan_Ayah"];
		}

		/* ======================= PEKERJAAN AYAH ============================*/
		if ($data["Pekerjaan_Ibu"]=='' || $data["Pekerjaan_Ibu"]=='undefined'){
			$pekerIbu = 0;
		}else{
			$pekerIbu = $data["Pekerjaan_Ibu"];
		}

		/* ======================= PENDIDIKAN SUAMI / ISTRI ============================*/
		if ($data["Pendidikan_SuamiIstri"]=='' || $data["Pendidikan_SuamiIstri"]=='undefined'){
			$pendSumis=0;
		}else{
			$pendSumis=$data["Pendidikan_SuamiIstri"];
		}

		/* ======================= PEKERJAAN SUAMI / ISTRI ============================*/
		if ($data["Pekerjaan_SuamiIstri"]=='' || $data["Pekerjaan_SuamiIstri"]=='undefined'){
			$pekerSumis=0;
		}else{
			$pekerSumis=$data["Pekerjaan_SuamiIstri"];
		}

		/* ======================= NIK PASIEN ============================*/
		$nik_pasien = $data["part_number_nik"];/*
		if ($data["NoMedrec"] == "" || $data["NoMedrec"] == "Automatic from the system...") {
			$this->no_medrec = $this->Tbl_data_pasien->getNoMedrecSQL();
		} else  {
			$this->no_medrec = $data["NoMedrec"];
		}*/
		
		$criteria = "kd_pasien = '" . $this->no_medrec . "'";
		
		/* ======================= CHANGE VARIABLE SQL SERVER ============================*/
		if ($data["StatusWarga"]=='false' || $data["StatusWarga"]===false){
			$wni=0;
		}else{
			$wni=1;
		}
		if ($data["JenisKelamin"]=='false' || $data["JenisKelamin"]===false){
			$jk=0;
		}else{
			$jk=1;
		}
		$tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $data['TglLahir'])));

		$datasqlsrv = array(
			"kd_pasien"         =>  $this->no_medrec, 
			"nama"              => $data["NamaPasien"],
			"nama_keluarga"     => $data["NamaKeluarga"], 
			"jenis_kelamin"     => $jk,
			"tempat_lahir"      => $data["Tempatlahir"], 
			"tgl_lahir"         => $tmptgllahir,
			"kd_agama"          => $tmpagama, 
			"gol_darah"         => $data["GolDarah"],
			"status_marita"     => $data["StatusMarita"], 
			"wni"               => $wni,
			"alamat"            => $data["Alamat"], 
			"telepon"           => $data["TLPNPasien"],
			"kd_kelurahan"      => $intkd_lurah, 
			"kd_pendidikan"     => $tmppendidikan,
			"kd_pekerjaan"      => $tmppekerjaan, 
			"pemegang_asuransi" => $data["NamaPeserta"],
			"no_asuransi"       => $tmpno_asuransi, 
			"kd_asuransi"       => $kode_asuransi,
			"kd_suku"           => $suku, 
			"jabatan"           => $data["Jabatan"],
			"kd_perusahaan"     => 1,  
			"kd_pos"            => $data["KdposPasien"]
		); 

		$dataPG 	= array(
			"kd_pasien"					=> $this->no_medrec, 
			"nama"						=> $data["NamaPasien"],
			"nama_keluarga"				=> $data["NamaKeluarga"], 
			"jenis_kelamin" 			=> $data["JenisKelamin"],
			"tempat_lahir"				=> $data["Tempatlahir"], 
			"tgl_lahir" 				=> $tmptgllahir,
			"kd_agama"					=> (int)$tmpagama, "gol_darah" => $data["GolDarah"],
			"status_marita"            => $data["StatusMarita"], "wni" => $data["StatusWarga"],
			"alamat"                   => $data["Alamat"], "telepon" => $data["TLPNPasien"],
			"kd_kelurahan"             => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
			"kd_pekerjaan"             => $tmppekerjaan, "pemegang_asuransi" => $data["NamaPeserta"],
			"no_asuransi"              => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
			"kd_suku"                  => $suku, "jabatan" => $data["Jabatan"],
			"kd_perusahaan"            => 0, "alamat_ktp" => $data["AlamatKtpPasien"],
			"kd_kelurahan_ktp"         => $intkd_lurahktp,
			"kd_pos_ktp"               => $data["KdPostKtpPasien"], "nama_ayah" => $data["NamaAyahPasien"],
			"nama_ibu"                 => $data["NamaIbuPasien"], "kd_pos" => $data["KdposPasien"],
			"handphone"                => $data["HPPasien"],
			"email"                    => $data["EmailPasien"],
			"suami_istri"              => $data["suami_istrinya"],
			"kd_pendidikan_ayah"       => $pendAyah,
			"kd_pendidikan_ibu"        => $pendIbu,
			"kd_pekerjaan_ayah"        => $pekerAyah,
			"kd_pekerjaan_ibu"         => $pekerIbu,
			"kd_pendidikan_suamiistri" => $pendSumis,
			"kd_pekerjaan_suamiistri"  => $pekerSumis,
			"part_number_nik"          => $nik_pasien 
		);
		
		//---POSTGRE
		

		$result_query = $this->Tbl_data_pasien->getDataSelectedPasien($criteria);
		if ($result_query->num_rows() == 0) {
			$data["kd_pasien"] = $this->no_medrec;
			$insert_pasien     = $this->Tbl_data_pasien->insert($dataPG);            
		} else {
			$insert_pasien     = $this->Tbl_data_pasien->update($criteria, $dataPG);
		} 
		
		$result_querySQL = $this->Tbl_data_pasien->getDataSelectedPasienSQL($criteria);
		if ($result_querySQL->num_rows() == 0) {
			$datasqlsrv["kd_pasien"] = $this->no_medrec;
			$insert_sql_pasien       = $this->Tbl_data_pasien->insertSQL($datasqlsrv);
		} else {
			$insert_sql_pasien 		 = $this->Tbl_data_pasien->updateSQL($criteria, $datasqlsrv);
		}

		if (($insert_pasien > 0 ||$insert_pasien === true) && ($insert_sql_pasien > 0 ||$insert_sql_pasien === true)) {
			$strError = true;
		}else{
			$strError = false;
		}

        return $strError;
	}

	/*
		FUNCTION PRIVATE
		SIMPAN DATA KUNJUNGAN
		================================================================================================================================================
		================================================================================================================================================
	 */
	private function simpan_kunjungan($data){
		$baru;
		$kunjungan;
		$barusqlsrv;
		$resultQueryKunjungan;
		$resultQueryAll;
		$resultQuery;
		$resultQuerySJP = true;
		$resultQueryRujukan;
		$resultQueryPenanggungJawab = true;

		$insert_sjp;
		$insert_sql_sjp;

		$insert_penanggung_jawab;

		$insert_rujukan;
		$insert_sql_rujukan;

		$insert_pasien;
		$insert_sql_pasien;
		$barusqlsrv;
		
		$strError   = "";
		$caraNerima = "";
		date_default_timezone_set("Asia/Jakarta");
		$JamKunjungan = "1900-01-01".gmdate(" H:i:s", time()+60*60*7);

		$criteriaPasienBaru = array(
			'kd_pasien' => $this->no_medrec,
		);
		$kunjungan = $this->Tbl_data_kunjungan->getDataSelectedKunjunganSQL($criteriaPasienBaru);
		if ($kunjungan->num_rows() == 0) {
			$baru       = 'TRUE';
			$barusqlsrv = 1;
		} else {
			$baru       = 'FALSE';
			$barusqlsrv = 0;
		}

		$this->tmp_shift_bagian = $this->GetShiftBagian("bagian_shift", 2);
		$this->tmp_shift_bagian = (int) $this->tmp_shift_bagian;
		$this->tmp_antrian      = $this->GetUrutKunjungan($this->no_medrec, $data["Poli"]);
		$kode_customer          = $data["KdCustomer"];
		$caraNerima             = $data["CaraPenerimaan"];
		$tmpkd_rujuk            = $data["KdRujukan"];
		/*if ($data["RadioRujukanPasien"] == 'false' || $data["RadioRujukanPasien"] == false)
		{
			$caraNerima  = 99;
			$tmpkd_rujuk = 0;
		}
		else
		{
			$caraNerima = $data["CaraPenerimaan"];
			if ($data["KdRujukan"] === '' || $data["KdRujukan"] === 'Pilih Rujukan...') {
				$tmpkd_rujuk = 0;
			} else {
				$tmpkd_rujuk = $data["KdRujukan"];
			}
		}*/


		$cari_kd_unit = $this->db->query("SELECT setting from sys_setting where key_data='".$data['unitMana']."_default_kd_unit'")->row()->setting;
		$asal_pasien  = $this->db->query("SELECT kd_asal from asal_pasien where kd_unit='".$cari_kd_unit."'")->row()->kd_asal;
		
		$this->tmp_kd_unit = $data["Poli"];
		$dataPg = array(
			"kd_pasien"       => $this->no_medrec, 
			"kd_unit"         => $this->tmp_kd_unit, 
			"tgl_masuk"       => $this->tgl_now,
			"urut_masuk"      => $this->tmp_antrian, 
			"jam_masuk"       => $JamKunjungan, 
			"cara_penerimaan" => $caraNerima, 
			"asal_pasien"     => $asal_pasien, 
			"kd_rujukan"      => $tmpkd_rujuk, 
			"kd_dokter"       => $data["KdDokter"],
			"baru"            => $baru, 
			"kd_customer"     => $data["KdCustomer"], 
			"shift"           => $this->tmp_shift_bagian, 
			"karyawan"        => $data["Karyawan"],
			"kontrol"         => $data["Kontrol"], 
			"antrian"         => $data["Antrian"], 
			"no_surat"        => $data["NoSurat"],
			"alergi"          => $data["Alergi"],
			"anamnese"        => $data["Anamnese"], 
			"no_sjp"          => $data["NoSjp"]
		);

        $criteria = "kd_pasien = '" . $this->no_medrec . "' AND kd_unit = '" . $this->tmp_kd_unit . "' AND tgl_masuk = '" . $this->tgl_now . "' ";

		$kunjungan = $this->Tbl_data_kunjungan->getDataSelectedKunjunganSQL($criteria);
		if ($kunjungan->num_rows() >= 0) {
            $datasql = array(
				"kd_pasien"       => $this->no_medrec, 
				"kd_unit"         => $this->tmp_kd_unit,
				"tgl_masuk"       => $this->tgl_now, 
				"urut_masuk"      => $this->tmp_antrian, 
				"jam_masuk"       => $JamKunjungan,
				"cara_penerimaan" => $caraNerima,
				"asal_pasien"     => $asal_pasien, 
				"kd_rujukan"      => $tmpkd_rujuk, 
				"kd_dokter"       => $data["KdDokter"],
				"baru"            => $baru, 
				"kd_customer"     => $kode_customer, 
				"shift"           => $this->tmp_shift_bagian,
				"karyawan"        => $data["Karyawan"], 
				"kontrol"         => $data["Kontrol"],
				"antrian"         => $data["Antrian"], 
				"no_surat"        => $data["NoSurat"]
			);

            $datasql["kd_pasien"] = $this->no_medrec;
			if ($data["Kontrol"]=='false'){
				$kontrolsrv=0;
			}else{
				$kontrolsrv=1;
			}

			$datasqlsrv =array(
				"kd_pasien"       => $this->no_medrec,
				"kd_unit"         => $this->tmp_kd_unit, 
				"tgl_masuk"       => $this->tgl_now,
				"urut_masuk"      => $this->tmp_antrian, 
				//"urut_masuk"      => 1, 
				"jam_masuk"       => $JamKunjungan, 
				"cara_penerimaan" => $caraNerima, 
				"asal_pasien"     => $asal_pasien, 
				"kd_rujukan"      => $tmpkd_rujuk,
				"kd_dokter"       => $data["KdDokter"],
				"baru"            => $barusqlsrv, 
				"kd_customer"     => $data["KdCustomer"], 
				"shift"           => $this->tmp_shift_bagian, 
				"karyawan"        => $data["Karyawan"],
				"kontrol"         => $kontrolsrv, 
				"antrian"         => $data["Antrian"], 
				"no_surat"        => $data["NoSurat"],
				"alergi"          => $data["Alergi"], 
				"no_sjp"          => $data["NoSjp"]
			);
			
				$kunjungan = $this->Tbl_data_kunjungan->getDataSelectedKunjunganSQL($criteria);
				if ($kunjungan->num_rows() >= 0){
					$insert_pasien     = $this->Tbl_data_kunjungan->insertSQL($datasqlsrv);
					$insert_sql_pasien = $this->Tbl_data_kunjungan->insert($dataPg);
					if (($insert_pasien > 0 ||$insert_pasien === true) && ($insert_sql_pasien > 0 ||$insert_sql_pasien === true)) {
						$resultQueryKunjungan 	= true;
					}else{
						$resultQueryKunjungan 	= false;
					}
				}
				else{
					$hapusKunjunganPg=$this->HapusKunjunganDiPg($this->tmp_kd_unit, $this->tgl_now, $this->no_medrec, $this->tmp_antrian);
					if ($hapusKunjunganPg==true){
						$insert_pasien     = $this->Tbl_data_kunjungan->insertSQL($datasqlsrv);
						$insert_sql_pasien = $this->Tbl_data_kunjungan->insert($dataPg);
						if (($insert_pasien > 0 ||$insert_pasien === true) && ($insert_sql_pasien > 0 ||$insert_sql_pasien === true)) {
							$resultQueryKunjungan 	= true;
						}else{
							$resultQueryKunjungan 	= false;
						}
					}else{
						$resultQueryKunjungan 	= false;
					}
				}
			
			
			if ($resultQueryKunjungan == true) {
				$params_reg_unit = array(
					'kd_pasien' 	=> $this->no_medrec,
					'kd_unit' 		=> $this->tmp_kd_unit,
				);

				$countRegUnit = $this->Tbl_data_kunjungan->getCountRegUnitSQL($params_reg_unit);
				if ($countRegUnit->num_rows() == 0) {
					$params_reg_unit['no_register'] = 0;	
					//$params_reg_unit['no_register'] = (int)$countRegUnit->row()->count + 1;
					$this->Tbl_data_kunjungan->insert_reg_unitSQL($params_reg_unit);
					$this->Tbl_data_kunjungan->insert_reg_unit($params_reg_unit);	
				}else{			
				}
				if ($data["NoSjp"] != "") {
					$params_sjp_sql = array(
						'kd_pasien'  => $this->no_medrec,
						'kd_unit'    => $this->tmp_kd_unit,
						'tgl_masuk'  => $this->tgl_now,
						'urut_masuk' => $this->tmp_antrian, 
						'no_sjp'     => $data['NoSjp'],
						'no_reg_sjp' => $data['NoAskes'],
					);
					
					$params_sjp = array(
						'kd_pasien'  => $this->no_medrec,
						'kd_unit'    => $this->tmp_kd_unit,
						'tgl_masuk'  => $this->tgl_now,
						'urut_masuk' => $this->tmp_antrian, 
						'no_sjp'     => $data['NoSjp'],
						'no_sjp'     => $data['NoAskes'],
					);

					$insert_sql_sjp = $this->Tbl_data_kunjungan->insertSJP_SQL($params_sjp_sql);
					$insert_sjp     = $this->Tbl_data_kunjungan->insertSJP($params_sjp);
					if (($insert_sjp > 0 ||$insert_sjp === true) && ($insert_sql_sjp > 0 ||$insert_sql_sjp === true)) {
						$resultQuerySJP = true;
					}else{
						$resultQuerySJP = false;
					}
				}

				$params_rujukan = array(
					'kd_rujukan' => $tmpkd_rujuk,
					'kd_pasien'  => $this->no_medrec,
					'kd_unit'    => $this->tmp_kd_unit,
					'tgl_masuk'  => $this->tgl_now,
					'urut_masuk' => $this->tmp_antrian, 
				);
				$insert_rujukan     = $this->Tbl_data_kunjungan->insertRujukan($params_rujukan);
				$insert_sql_rujukan = $this->Tbl_data_kunjungan->insertRujukan_SQL($params_rujukan);
				if (($insert_rujukan > 0 ||$insert_rujukan === true) && ($insert_sql_rujukan > 0 ||$insert_sql_rujukan === true)) {
					$resultQueryRujukan = true;
				}else{
					$resultQueryRujukan = false;
				}
				
				if ($data["NAMA_PJ"] === "" && $data["KTP"] === "") {
					$resultQuery = true;
				} else {
					$params_PJ = array(
						'kd_pasien'        => $this->no_medrec,
						'kd_unit'          => $this->tmp_kd_unit,
						'tgl_masuk'        => $this->tgl_now,
						'urut_masuk'       => $this->tmp_antrian, 
						'kd_kelurahan'     => $data['KelurahanPJ'],
						'kd_pekerjaan'     => $data['kdpekerjaanPj'],
						'nama_pj'          => $data['NAMA_PJ'],
						'alamat'           => $data['AlamatPJ'],
						'telepon'          => $data['TeleponPj'],
						'kd_pos'           => $data['PosPJ'],
						'hubungan'         => $data['HubunganPj'],
						'alamat_ktp'       => $data['AlamatKtpPJ'],
						'no_hp'            => $data['HpPj'],
						'kd_pos_ktp'       => $data['KdPosKtp'],
						'no_ktp'           => $data['KTP'],
						'kd_pendidikan'    => $data['KdPendidikanPj'],
						'email'            => $data['EmailPenanggungjawab'],
						'tempat_lahir'     => $data['tempatlahirPenanggungjawab'],
						'tgl_lahir'        => $data['TgllahirPJ'],
						'status_marital'   => $data['StatusMaritalPenanggungjawab'],
						'kd_agama'         => $data['1'],
						'kd_kelurahan_ktp' => $data['KelurahanKtpPJ'],
						'wni'              => $data['WNIPJ'],
						'jenis_kelamin'    => $data['JKpenanggungJwab'],
					);
					$insert_penanggung_jawab = $this->Tbl_data_kunjungan->insertPJ($params_PJ);

					if ($insert_penanggung_jawab > 0 ||$insert_penanggung_jawab === true) {
						$resultQueryPenanggungJawab = true;
					}else{
						$resultQueryPenanggungJawab = false;
					}
				}

				if (
						($resultQuerySJP>0 || $resultQuerySJP ===true) &&
						($resultQueryRujukan>0 || $resultQueryRujukan ===true) &&
						($resultQueryPenanggungJawab>0 || $resultQueryPenanggungJawab ===true)
					) {
					$resultQueryAll = true;
				}else{
					$resultQueryAll = false;
				}
			}


			if (($resultQueryKunjungan > 0 ||$resultQueryKunjungan === true) && ($resultQueryAll > 0 ||$resultQueryAll === true)) {
				$resultQuery = true;
			}else{
				$resultQuery = false;
			}
		} else {
			$resultQuery = false;
		}
		return $resultQuery;
	}

	/*
		FUNCTION PRIVATE
		SIMPAN DATA TRANSAKSI
		================================================================================================================================================
		================================================================================================================================================
	 */
	private function simpanTransaksi($data){
		$resultQuery   = false;
		$resultSQL     = false;
		$result        = false;
		
		$this->tmp_kd_kasir = $this->db->query("select setting from sys_setting where key_data = '".$this->key_data_kd_kasir."'")->row()->setting;
		$this->notrans = $this->GetIdTransaksi($this->tmp_kd_kasir);
		$paramsPG = array(
			"kd_kasir"      	=> $this->tmp_kd_kasir,
			"no_transaksi"  	=> $this->notrans,
			"kd_pasien"     	=> $this->no_medrec,
			"kd_unit"       	=> $this->tmp_kd_unit,
			"tgl_transaksi" 	=> $this->tgl_now,
			//"urut_masuk"    	=> $this->tmp_antrian,
			"urut_masuk"    	=> $this->tmp_antrian, 
			"tgl_co"        	=> NULL,
			"co_status"     	=> "False",
			"orderlist"     	=> NULL,
			"ispay"         	=> "False",
			"app"           	=> "False",
			"kd_user"       	=> $this->id_user,
			"tag"           	=> NULL,
			"lunas"         	=> "False",
			"posting_transaksi"	=> "true",
			"tgl_lunas"     	=> NULL
        ); 
		$paramSQL = array(
			"kd_kasir"      => $this->tmp_kd_kasir,
			"no_transaksi"  => $this->notrans,
			"kd_pasien"     => $this->no_medrec,
			"kd_unit"       => $this->tmp_kd_unit,
			"tgl_transaksi" => $this->tgl_now,
			"urut_masuk"    => $this->tmp_antrian, 
			"tgl_co"        => NULL,
			"co_status"     => 0,
			"orderlist"     => NULL,
			"ispay"         => 0,
			"app"           => 0,
			"kd_user"       => $this->id_user,
			"tag"           => NULL,
			"lunas"         => 0,
			"tgl_lunas"     => NULL
		);

		$criteriaCekTransaksi = array(
			"kd_kasir"      => $this->tmp_kd_kasir,
			"no_transaksi"  => $this->notrans,
			"tgl_transaksi" => $this->tgl_now,
		);

		$resultTransaksi = $this->Tbl_data_transaksi->getTransaksi_SQL($criteriaCekTransaksi);
		if ($resultTransaksi->num_rows() == 0) {
			$resultSQL = $this->Tbl_data_transaksi->insertTransaksi_SQL($paramSQL);
			$result    = $this->Tbl_data_transaksi->insertTransaksi($paramsPG);
		}

		if (($resultSQL == true || $resultSQL > 0) && ($result == true || $result > 0)) {
			$resultQuery = true;
		}else{
			$resultQuery = false;
		}
		return $resultQuery;
	}

	/*
		FUNCTION PRIVATE
		SIMPAN DATA DETAIL TRANSAKSI
		================================================================================================================================================
		================================================================================================================================================
	 */
	private function simpanDetailTransaksi($data){
		$resultQuery 	= false;
		$resultSQL   	= false;
		$resultPG      	= false;

		$querySQL 		= false;
		$query 			= false;

		$autocharge_kd_produk		= ""; 
		$autocharge_kd_unit			= ""; 
		$autocharge_tarif			= ""; 
		$autocharge_kd_tarif		= ""; 
		$autocharge_tgl_berlaku		= ""; 

		$autocharge_kd_produk_SQL	= ""; 
		$autocharge_kd_unit_SQL		= ""; 
		$autocharge_tarif_SQL		= ""; 
		$autocharge_kd_tarif_SQL	= ""; 
		$autocharge_tgl_berlaku_SQL	= ""; 

		$this->tmp_get_appto   	= $this->Tbl_data_transaksi->GetfunctionGetAppTo('false', $data['PasienBaruRujukan'],$data['RadioRujukanPasien'],'false','false',$data['JenisRujukan']);
		$this->tmp_kd_tarif    	= $this->Tbl_data_transaksi->GetKdTarif($data['KdCustomer']);
		$this->tmp_tgl_berlaku 	= $this->Tbl_data_transaksi->GetTanggalBerlakuFromKlas($this->tmp_kd_tarif, $this->tgl_now, $this->tgl_now, 71);
		if($this->tmp_tgl_berlaku == "" || empty($tmp_tgl_berlaku)){
			$this->tmp_tgl_berlaku = $this->tgl_now;
		}
		/*
		$criteriaAutoCharge = array(
			'left(autocharge.kd_unit,1)'=> substr($this->tmp_kd_unit, 0, 1),
			'tarif.kd_tarif'     		=> $this->tmp_kd_tarif,
			'tarif.tgl_berlaku <= ' 	=> $this->tmp_tgl_berlaku,
		);
		$querySQL = $this->Tbl_data_transaksi->getDataAutoCharge_SQL($criteriaAutoCharge, $this->tmp_get_appto);

		foreach ($querySQL->result() as $result) {
			$autocharge_kd_produk_SQL 	= $result->kd_produk;
			$autocharge_kd_unit_SQL 	= $result->kd_unit;
			$autocharge_tarif_SQL 		= $result->tarif;
			$autocharge_kd_tarif_SQL	= $result->kd_tarif;
			$autocharge_tgl_berlaku_SQL	= $result->tgl_berlaku;
		}*/

		$criteriaAutoCharge = array(
			'autocharge.kd_unit'		=> $this->tmp_kd_unit,
			'tarif.kd_tarif'     		=> $this->tmp_kd_tarif,
			'tarif.tgl_berlaku <= ' 	=> $this->tmp_tgl_berlaku,
		);
		$query = $this->Tbl_data_transaksi->getDataAutoCharge($criteriaAutoCharge, $this->tmp_get_appto);

		foreach ($query->result() as $result) {
			$this->tmp_kd_produk 	= $result->kd_produk;
			$autocharge_kd_unit 	= $result->kd_unit;
			$autocharge_tarif 		= $result->tarif;
			$autocharge_kd_tarif 	= $result->kd_tarif;
			$this->tmp_tgl_berlaku	= $result->tgl_berlaku;
		}

		/*if ($autocharge_tgl_berlaku == $autocharge_tgl_berlaku_SQL) {
			$this->tmp_tgl_berlaku 	= $autocharge_tgl_berlaku_SQL;
		}

		if ($autocharge_kd_produk == $autocharge_kd_produk_SQL) {
			$this->tmp_kd_produk 	= $autocharge_kd_produk_SQL;
		}*/

		$paramsInsert = array(
			'kd_kasir' 		=> $this->tmp_kd_kasir,
			'no_transaksi' 	=> $this->notrans,
			'urut' 			=> 1,
			'tgl_transaksi' => $this->tgl_now,
			'kd_user' 		=> $this->id_user,
			'kd_tarif' 		=> $autocharge_kd_tarif,
			'kd_produk' 	=> $this->tmp_kd_produk,
			'kd_unit' 		=> $autocharge_kd_unit,
			'tgl_berlaku' 	=> $this->tmp_tgl_berlaku,
			'charge' 		=> 'true',
			'adjust' 		=> 'true',
			'folio' 		=> 'A',
			'qty' 			=> 1,
			'harga' 		=> $autocharge_tarif,
			'shift' 		=> $this->tmp_shift_bagian,
			'tag' 			=> 'false',
			'no_faktur' 	=> '',
		);

		$resultPG 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi($paramsInsert);
		$paramsInsert['charge'] = 1;
		$paramsInsert['adjust'] = 1;
		$paramsInsert['tag'] 	= 0;
		if (substr($autocharge_kd_unit,0,1) == '3') {
			$paramsInsert['kd_unit']= substr($autocharge_kd_unit,0,1);
		}
		$resultSQL 	= $this->Tbl_data_detail_transaksi->insertDetailTransaksi_SQL($paramsInsert);

		if ($query->num_rows() > 0) {
			$criteriaTarifComponent = array(
				'kd_unit' 		=> $this->tmp_kd_unit,
				'tgl_berlaku' 	=> $this->tmp_tgl_berlaku,
				'kd_tarif' 		=> $this->tmp_kd_tarif,
				'kd_produk'		=> $this->tmp_kd_produk,
			);

			$querySQL = $this->Tbl_data_tarif->getTarifComponent($criteriaTarifComponent);
			foreach ($querySQL->result() as $result) {
				$paramsData = array(
					'kd_kasir' 		=> $this->tmp_kd_kasir,
					'no_transaksi' 	=> $this->notrans,
					'urut' 			=> 1,
					'tgl_transaksi'	=> $this->tgl_now,
					'kd_component'	=> $result->kd_component,
					'tarif'			=> $result->tarif,
					'disc'			=> 0,
				);
				$resultSQL 	= $this->Tbl_data_detail_component->insertComponent_SQL($paramsData);
				$resultPG 	= $this->Tbl_data_detail_component->insertComponent($paramsData);
				if (($resultSQL === false || $resultSQL == 0) && ($resultPG === false || $resultPG == 0)) {
					$resultSQL 	= false;
					$result 	= false;
					break;
				}
			}
		}

		if (($resultSQL == true || $resultSQL > 0) && ($resultPG == true || $resultPG > 0)) {
			$resultQuery = true;
		}else{
			$resultQuery = false;
		}
		return $resultQuery;
	}

	/*
		FUNCTION PRIVATE
		SIMPAN DATA DETAIL TRANSAKSI
		================================================================================================================================================
		================================================================================================================================================
	*/
	private function simpanDetailTrDokter($data){
		$resultQuery 	= false;
		$resultSQL   	= false;
		$result      	= false;
		$criteriaCustom = array(
			'table' 		=> 'sys_setting',
			'field_return' 	=> 'setting',
			'field_criteria'=> 'key_data',
			'value_criteria'=> $this->key_data_jasa_dok,
		);

		$jasaDokter 		= $this->Tbl_data_transaksi->getCustom($criteriaCustom);

		$criteriaTarifComponent = array(
			'kd_unit'			=> $this->tmp_kd_unit,
			'tgl_berlaku' 		=> $this->tmp_tgl_berlaku,
			'kd_tarif' 			=> $this->tmp_kd_tarif,
			'kd_produk'			=> $this->tmp_kd_produk,
			'kd_component'		=> $jasaDokter,
		);
		$querySQL = $this->Tbl_data_tarif->getTarifComponent($criteriaTarifComponent);
		foreach ($querySQL->result() as $result) {
			$paramsInsert = array(
				'kd_kasir' 		=> $this->tmp_kd_kasir,
				'no_transaksi' 	=> $this->notrans,
				'urut' 			=> 1,
				'kd_dokter' 	=> $data['KdDokter'],
				'tgl_transaksi' => $this->tgl_now,
				'tim_persen' 	=> 0,
				'kd_job' 		=> 0,
				'jp' 			=> $result->tarif,
				'pajak' 		=> 0,
				'bayar' 		=> 0,
				'pot_ops' 		=> 0,
			);
			$resultSQL = $this->Tbl_data_transaksi->insertDetailTRDokter_SQL($paramsInsert);
			unset($paramsInsert['kd_job']);
			$paramsInsert['kd_component'] = 0;
			$result    = $this->Tbl_data_transaksi->insertDetailTRDokter($paramsInsert);
		}

		if (($resultSQL == true || $resultSQL > 0) && ($result == true || $result > 0)) {
			$resultQuery = true;
		}else{
			$resultQuery = false;
		}
		return $resultQuery;
	}


	/*
		FUNCTION PRIVATE
		SIMPAN DATA VISITE DOKTER 
		================================================================================================================================================
		================================================================================================================================================
	 */
	
	private function simpanVisiteDokter($data){
		$resultQuery 	= false;
		$resultSQL   	= false;
		$resultPG      	= false;
		$result      	= false;

		if (($resultSQL === true || $resultSQL > 0) && ($resultPG === true || $resultPG > 0)) {
			$resultQuery = true;
		}else{
			$resultQuery = false;
		}
		return $resultQuery;
	}

	/*
		FUNCTION PRIVATE
		SIMPAN DATA PASIEN INAP 
		================================================================================================================================================
		================================================================================================================================================
	 */
	
	private function simpanPasienInap($data){
		$resultQuery 	= false;
		$resultSQL   	= false;
		$resultPG      	= false;
		$result      	= false;
		$paramsInsert = array(
			'kd_kasir' 		=> $this->tmp_kd_kasir,
			'no_transaksi' 	=> $this->notrans,
			'kd_unit'		=> $this->tmp_kd_unit,
			'no_kamar'		=> $data['Kamar'],
			'kd_spesial'	=> $data['spesialisasi'],
		);

		$resultSQL 		= $this->Model_pindah_kamar->insertPasienInap_SQL($paramsInsert);
		$resultPG 		= $this->Model_pindah_kamar->insertPasienInap($paramsInsert);
		if (($resultSQL === true || $resultSQL > 0) && ($resultPG === true || $resultPG > 0)) {
			$resultQuery = true;
		}else{
			$resultQuery = false;
		}
		return $resultQuery;
	}

	/*
		FUNCTION PRIVATE
		SIMPAN DATA NGINAP 
		================================================================================================================================================
		================================================================================================================================================
	 */
	
	private function simpanNginap($data){
		$resultQuery 	= false;
		$resultSQL   	= false;
		$resultPG      	= false;
		$result      	= false;

		$criteria = array(
			'kd_pasien' 		=> $this->no_medrec,
		);

		
		$criteriaCustom = array(
			'table' 		=> 'kamar',
			'field_return' 	=> 'kd_unit',
			//'field_criteria'=> 'no_kamar',
			'field_criteria'=> array(
				'no_kamar' 	=> $data['Kamar'],
				'kd_unit' 	=> $this->tmp_kd_unit,
			),
			//'value_criteria'=> $data['Kamar'],
		);

		$kd_unit_kamar 		= $this->Tbl_data_transaksi->getCustom($criteriaCustom);

		$paramsInsert = array(
			//'kd_unit_kamar1'	=> $kd_unit_kamar,
			//'kd_unit_kamar'	=> $this->tmp_kd_unit,
			'no_kamar' 		=> $data['Kamar'],
			'kd_pasien'		=> $this->no_medrec,
			'kd_unit' 		=> $this->tmp_kd_unit,
			//'urut_masuk' 	=> $this->tmp_antrian,
			'tgl_inap' 		=> $this->tgl_now,
			'tgl_masuk'		=> $this->tgl_now,
			'jam_inap'		=> "1900-01-01".gmdate(" H:i:s", time()+60*60*7),/*
			'tgl_keluar'	=> null,	
			'jam_keluar'	=> null,	
			'bed'			=> null,*/
			'kd_spesial'	=> $data['spesialisasi'],
			'akhir'			=> 1,	
			'urut_masuk'	=> $this->tmp_antrian, 
		);

		if (isset($kd_unit_kamar)) {
			$paramsInsert['kd_unit_kamar'] = $kd_unit_kamar;
		}else{
			$paramsInsert['kd_unit_kamar'] = $this->tmp_kd_unit;
		}

		$resultSQL 	= $this->Model_pindah_kamar->insertPindahKamarSQL($paramsInsert);
		$paramsInsert['akhir'] 	= 'true';
		$resultPG 	= $this->Model_pindah_kamar->insertPindahKamar($paramsInsert);
		/*$resultQuery = $this->Tbl_data_kunjungan->getDataKunjungan($criteria, true, 1);
		if ($resultQuery->num_rows() > 0 ) {
			$paramsInsert = array(
				'kd_unit_kamar'	=> $data
				'no_kamar' 		=> $data['Kamar'],
				'kd_pasien'		=> $this->no_medrec,
				'kd_unit' 		=> $this->tmp_kd_unit,
				'urut_masuk' 	=> $this->tmp_antrian,
				'tgl_inap' 		=> $this->
				'JAM_INAP'
				'TGL_KELUAR'
				'JAM_KELUAR'
				'BED'
				'KD_SPESIAL'
				'AKHIR'
				'URUT_NGINAP'
			);
			foreach ($resultQuery->result() as $result) {
				$paramsInsert['tgl_masuk']	= $result->tgl_masuk;
			}
		}else{

		}*/
		if (($resultSQL === true || $resultSQL > 0) && ($resultPG === true || $resultPG > 0)) {
			$resultQuery = true;
		}else{
			$resultQuery = false;
		}
		return $resultQuery;
	}

	/*
		FUNCTION PENUNJANG / PRIVATE
		================================================================================================================================================
		================================================================================================================================================
	*/
	private function HapusKunjunganDiPg($kd_unit, $tgl_kunjungan, $kd_pasien, $urut_masuk){
		//$shift   = $this->Tbl_data_kunjungan->getBagianShiftSQL("bagian_shift", 2)->row()->shift;
		//$kd_user = $this->id_user;
		$this->db->trans_begin();
		date_default_timezone_set("Asia/Jakarta");
		$q_delete_kunjungan=$this->db->query("DELETE from kunjungan where kd_pasien='$kd_pasien' and tgl_masuk='$tgl_kunjungan' and kd_unit='$kd_unit' ");
		if ($this->db->affected_rows()==0){
			$this->db->trans_rollback();
			return false;
		}else {
			$this->db->trans_commit();
			return true;
		}
	}

	private function GetShiftBagian($table, $kd_bagian) {
		$sqlbagianshift = $this->Tbl_data_kunjungan->getBagianShiftSQL($table, $kd_bagian)->row()->shift;
		$lastdate       = date("Y-m-d", strtotime($this->Tbl_data_kunjungan->lastDateShiftSQL($table, $kd_bagian)->row()->lastdate));
		if ($lastdate <> $this->tgl_now && $sqlbagianshift === '3') {
			$sqlshift = '4';
		} else {
			$sqlshift = $sqlbagianshift;
		}
		return $sqlshift;
	}

	private function GetUrutKunjungan($medrec, $unit) {
		$nm     = 0;
		$nomor  = 0;
		$retVal = 1;
		$retVal = 0;

		if ($medrec === "Automatic from the system..." || $medrec === " ") {
			$tmpmedrec = " ";
		} else {
			$tmpmedrec = $medrec;
		}

		$params = array(
			'kd_pasien' => $medrec,
			'kd_unit'   => $unit,
			'tgl_masuk' => $this->tgl_now,
		);

		$res = $this->Tbl_data_kunjungan->getUrutMasukKunjunganSQL($params);
		if ($res->num_rows() > 0) {
			$nm     = $res->row()->URUT_MASUK;
			$nomor  = (int) $nm + 1;
			$retVal = $nomor;
		}else{
			$retVal = 1;
		}

		return $retVal;
	}

    private function GetKdLurah($kd_Kec) {
    	if (is_numeric($kd_Kec)) {
			$intKdKec = $this->dbSQL->query("SELECT * FROM kecamatan WHERE kd_kecamatan='".$kd_Kec."'")->row()->KD_KECAMATAN;
    	}else{
			$intKdKec = $this->dbSQL->query("SELECT * FROM kecamatan WHERE kecamatan='".$kd_Kec."'")->row()->KD_KECAMATAN;
    	}
		$intKdLurah;
		$criteria = "where kec.kd_kecamatan=" . $intKdKec . " And (Kelurahan='DEFAULT' or Kelurahan='.' OR Kelurahan is null)";
		$this->load->model("rawat_jalan/tb_getkelurahan");
		$this->tb_getkelurahan->db->where($criteria, null, false);
		$query    = $this->tb_getkelurahan->GetRowList(0, 1, "", "", "");
		if ($query[1] != 0) {
			if ($query[0][0]->KD_KELURAHAN == '') {
				$tmp_kdlurah = $this->GetLastKd_daerah(1);
				$this->load->model("general/tb_kelurahan");
				$data        = array("kd_kelurahan" => $tmp_kdlurah, "kd_kecamatan" => $intKdKec, "kelurahan" => "DEFAULT");
				$result      = $this->tb_kelurahan->Save($data);
				$strError    = $tmp_kdlurah;
			} else {
				$strError = $query[0][0]->KD_KELURAHAN;
			}
		} else {
			if ($intKdKec == "") {
				$intKdKec = $this->GetLastKd_daerah(2);
				$this->load->model("general/tbl_kecamatan");
				$data     = array("kd_kabupaten" => $tmp_kdlurah, "kd_kecamatan" => $intKdKec, "kecamatan" => "DEFAULT");
				$result   = $this->tbl_kecamatan->Save($data);
				$strError = $intKdKec;
			} else {
				$intKdKec = $kd_Kec;
			}
			$tmp_kdlurah = $this->GetLastKd_daerah(1);
			$this->load->model("general/tb_kelurahan");
			$data        = array("kd_kelurahan" => $tmp_kdlurah, "kd_kecamatan" => $intKdKec, "kelurahan" => "DEFAULT");
			$result      = $this->tb_kelurahan->Save($data);
			$strError    = $tmp_kdlurah;
		}
		return $strError;
    }

	private function GetIdTransaksi($kd_kasir) {
		$resultSQL = false;
		$result    = false;

		$criteria = array(
			'kd_kasir' => $kd_kasir,
		);
		$counter = $this->Tbl_data_transaksi->getKasir_SQL($criteria)->row()->counter;
		$retVal  = (int)$counter+1;

		$params = array(
			'counter' => $retVal,
		);

		$resultSQL = $this->Tbl_data_transaksi->updateKasir_SQL($criteria, $params);
		$result    = $this->Tbl_data_transaksi->updateKasir($criteria, $params);

		if (strlen($retVal) == 1) {
			$retValreal = "000000".$retVal;
		}else if (strlen($retVal) == 2){
			$retValreal = "00000".$retVal;
		}else if (strlen($retVal) == 3){
			$retValreal = "0000".$retVal;
		}else if (strlen($retVal) == 4){
			$retValreal = "000".$retVal;
		}else if (strlen($retVal) == 5){
			$retValreal = "00".$retVal;
		}else if (strlen($retVal) == 6){
			$retValreal = "0".$retVal;
		}else{
			$retValreal = $retVal;
		}
		return $retValreal;
	}

	private function GetAntrian($kd_dokter) {
		$criteria = array(
			'kd_pasien' => $this->no_medrec,
			'kd_unit'   => $this->tmp_kd_unit,
			'tgl_masuk' => $this->tgl_now,
			'kd_pasien' => $kd_dokter,
		);
		$retVal = 1;
		$res    = $this->Tbl_data_kunjungan->getUrutKunjungan_SQL($criteria);
		if ($res->num_rows() > 0) {
			$nm     = $res->row()->urut_masuk;
			$retVal = $nm;
		}
		return $retVal;
	}

	private function operateQuantityDigunakan($criteria, $perintah){
		$resultQuery = "";
		$jumlah = 0;
		$jumlahKasurLast = 0;
		$criteriaKamarInduk = array(
			'no_kamar' 	=> $criteria,
		);
		$resultQuery = $this->Model_pindah_kamar->getSelectedDataKamarSQL($criteriaKamarInduk);
		foreach ($resultQuery->result_array() as $data) {
			$jumlahKasurLast = (int)$data['digunakan'];
		}
		if (($perintah===true) || $perintah=="tambah") {
			$jumlah = (int)$jumlahKasurLast+1;
		}else{
			$jumlah = (int)$jumlahKasurLast-1;
		}
		return $jumlah;
	}

}

?>
