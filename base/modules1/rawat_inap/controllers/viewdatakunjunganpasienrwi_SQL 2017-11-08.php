<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class viewdatakunjunganpasienrwi extends MX_Controller {
    
    public function __construct() {
        //parent::Controller();
        parent::__construct();
		
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
    	// echo 'a';

		$list = array();	
		$sqldatasrv="SELECT top 100 P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR, sp.SPESIALISASI,c.CUSTOMER
						FROM PASIEN P
						INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
						inner join customer c on c.kd_customer = k.kd_customer
						INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
						inner JOIN nginap n ON n.KD_PASIEN=K.KD_PASIEN
						inner join spesialisasi sp on sp.kd_spesial = n.kd_spesial
						INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
						where";
		if (strlen($Params[4]) !== 0) {
			
            $kriteria=str_replace("~", "'", $Params[4] . " and n.akhir=1 group by  P.KD_PASIEN, k.urut_masuk,p.NAMA,k.urut_masuk,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA, d.KD_DOKTER , K.TGL_KELUAR
				, sp.SPESIALISASI,c.CUSTOMER
						order by k.TGL_MASUK desc");
        } else {
			$sqldatasrv="SELECT top 100 P.KD_PASIEN, k.urut_masuk,P.NAMA,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA as NAMA_DOKTER, d.KD_DOKTER , K.TGL_KELUAR, sp.SPESIALISASI,c.CUSTOMER
						FROM PASIEN P
						INNER JOIN KUNJUNGAN K ON K.KD_PASIEN = P.KD_PASIEN
						inner join customer c on c.kd_customer = k.kd_customer
						INNER JOIN UNIT U ON u.KD_UNIT = k.KD_UNIT
						inner JOIN nginap n ON n.KD_PASIEN=K.KD_PASIEN
						inner join spesialisasi sp on sp.kd_spesial = n.kd_spesial
						INNER JOIN DOKTER D ON D.KD_DOKTER = K.KD_DOKTER
						";
            $kriteria=str_replace("~", "'", " and n.akhir=1 group by  P.KD_PASIEN, k.urut_masuk,p.NAMA,k.urut_masuk,u.KD_UNIT,u.NAMA_UNIT,k.TGL_MASUK,D.NAMA, d.KD_DOKTER , K.TGL_KELUA
				, sp.SPESIALISASI,c.CUSTOMER
						order by k.TGL_MASUK desc ");
        }
		
	   $dbsqlsrv = $this->load->database('otherdb2',TRUE);
	   $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
		
		
		foreach ($res->result() as $rec)
		{
		  $o=array();
		  $o['KD_PASIEN']=$rec->KD_PASIEN;
		  $o['KD_UNIT']=$rec->KD_UNIT;
		  $o['POLI']=$rec->NAMA_UNIT;
		  $o['URUT']=$rec->urut_masuk;
		  $o['TGL']=date("d-M-Y", strtotime($rec->TGL_MASUK));
		  $o['DOKTER']=$rec->NAMA_DOKTER;
		  $o['KD_DOKTER']=$rec->KD_DOKTER;
		  $o['SPESIALISASI']=$rec->SPESIALISASI;
		  $o['CUSTOMER']=$rec->CUSTOMER;
		  if ($rec->TGL_KELUAR==null)
		  {
		  	$o['TGL_KELUAR']='';
		  }else

		  {
		  	$o['TGL_KELUAR']=date("d-M-Y", strtotime($rec->TGL_KELUAR));
		  }
		  
          $list[]=$o;	
		}

        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }
	
	function getPasien() {
        try {
			$sqldatasrv="SELECT pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, 
						 pasien.jenis_kelamin, pasien.tempat_lahir, pasien.tgl_lahir, 
						 agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, 
						 pasien.alamat, pasien.kd_kelurahan, pendidikan.pendidikan, pekerjaan.pekerjaan, 
						 kb.kd_kabupaten,kb.kabupaten, kc.kd_kecamatan,kc.kecamatan, pr.propinsi,
						 pasien.email,pasien.handphone,pasien.nama_ayah,pasien.nama_ibu,
						 kl.kelurahan,pr.kd_propinsi, pasien.kd_pekerjaan, 
						 pasien.kd_pendidikan, pasien.telepon ,pasien.kd_agama, pasien.no_asuransi
						 FROM pasien left join agama ON pasien.kd_agama = agama.kd_agama 
						 left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
						 left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
						 left join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
						 left join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
						 left join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
						 left join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI where ";
            if (strlen($_POST['query']) !== 0) {
                $kriteria=str_replace("~", "'", $_POST['query'] . " limit 50");
            } else {
                $kriteria=str_replace("~", "'", " kd_pasien Like '%-%'
						   order by pasien.kd_pasien desc limit 50");
            }
			
			// $dbsqlsrv = $this->load->database('otherdb2',TRUE);
            // $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
            $res = $this->db->query($sqldatasrv.$kriteria);
			foreach ($res->result() as $rec)
			{
				$o=array();
				$o['KD_PASIEN']=$rec->kd_pasien;
				$o['NAMA']=$rec->nama;
				$o['NAMA_KELUARGA']=$rec->nama_keluarga;
				$o['JENIS_KELAMIN']=$rec->jenis_kelamin;
				$o['TEMPAT_LAHIR']=$rec->tempat_lahir;
				$o['TGL_LAHIR']=date("M d Y",strtotime($rec->tgl_lahir));
				if (substr($rec->tgl_lahir, 6, 5) == '1900') {
				$o['TGL_LAHIR_PASIEN'] = '1900-01-01';
				}else{
				$o['TGL_LAHIR_PASIEN']  = date("Y-m-d",strtotime($rec->tgl_lahir));
				}
				//$o['TGL_LAHIR_PASIEN']=date("Y-m-d",strtotime($rec->tgl_lahir));
				$o['AGAMA']=$rec->agama;
				$o['GOL_DARAH']=$rec->gol_darah;
				$o['WNI']=$rec->wni;
				$o['STATUS_MARITA']=$rec->status_marita;
				$o['ALAMAT']=$rec->alamat;
				$o['KD_KELURAHAN']=$rec->kd_kelurahan;
				$o['PENDIDIKAN']=$rec->pendidikan;
				$o['KABUPATEN']=$rec->kabupaten;
				$o['KECAMATAN']=$rec->kecamatan;
				$o['PROPINSI']=$rec->propinsi;
				$o['KD_KABUPATEN']=$rec->kd_kabupaten;
				$o['KD_KECAMATAN']=$rec->kd_kecamatan;
				$o['KD_PROPINSI']=$rec->kd_propinsi;
				$o['KD_PENDIDIKAN']=$rec->kd_pendidikan;
				$o['KD_PEKERJAAN']=$rec->kd_pekerjaan;
				$o['KD_AGAMA']=$rec->kd_agama;
				$o['PEKERJAAN']=$rec->pekerjaan;
				$o['NO_ASURANSI']=$rec->no_asuransi;

				$o['AYAH']=$rec->nama_ayah;
				$o['IBU']=$rec->nama_ibu;
				$o['KELURAHAN']=$rec->kelurahan;

				$o['EMAIL']=$rec->email;
				$o['HP']=$rec->handphone;
				$o['TELEPON']=$rec->telepon;
				$list[]=$o;	
			} 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:' . count($res->result()) . ', ListDataObj:' . json_encode($list) . '}';
    }
    
	private function GetShiftBagian(){
		$sqlbagianshift = $this->db->query("SELECT shift FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->shift;
		$lastdate = $this->db->query("SELECT to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='1'")->row()->lastdate;
		$datnow= date('Y-m-d');
		if($lastdate<>$datnow && $sqlbagianshift==='3')
		{
			$sqlbagianshift2 = '4';
		}else{
			$sqlbagianshift2 = $sqlbagianshift;
		}
			
        return $sqlbagianshift2;
	}

    public function deletekunjungan_Revisi(){
		$response              = array();
		$result_PG             = false;
		$result_SQL            = false;
		$kd_unit               = $_POST['kd_unit'];
		$tgl_kunjungan         = $_POST['Tglkunjungan'];
		$kd_pasien             = $_POST['Kodepasein'];
		$urut_masuk            = $_POST['urut'];
		$keterangan            = $_POST['alasan'];
		$shift                 = $this->GetShiftBagian();
		$kd_user               = $this->session->userdata['user_id']['id'];
		$response['transaksi'] = false;
		$response['bayar']     = false;
		$this->db->trans_begin();
		// $this->dbSQL->trans_begin();
		
		$resultKunjungan = $this->db->query("SELECT * from kunjungan k
			inner join transaksi t on k.kd_pasien=t.kd_pasien and k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
			inner join pasien p on k.kd_pasien=p.kd_pasien
			inner join unit u on k.kd_unit=u.kd_unit
			where k.kd_pasien='".$kd_pasien."' and k.kd_unit='".$kd_unit."' and k.tgl_masuk='".$tgl_kunjungan."' and k.urut_masuk=".$urut_masuk."")->row();

		$resultTransaksi = $this->db->query("SELECT dt.*, produk.deskripsi from detail_transaksi dt 
											inner join produk on produk.kd_produk = dt.kd_produk
											where no_transaksi='".$resultKunjungan->no_transaksi."' and kd_kasir='".$resultKunjungan->kd_kasir."' and tgl_transaksi='".$resultKunjungan->tgl_transaksi."'");
		$resultNginap = $this->db->query("select * from nginap where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk." and akhir='t'");
												
		# Cek detail transaksi, jika ada data maka tidak boleh dihapus
		$resdetailtr = $this->db->query("select * from detail_transaksi where no_transaksi='".$resultKunjungan->no_transaksi."' and kd_kasir='".$resultKunjungan->kd_kasir."'")->result();
		if(count($resdetailtr) > 0){
			$response['pesan'] = "Terdapat entrian data pada detail transaksi. Kunjungan tidak dapat diHapus!";
			$response['success'] = false;
			echo json_encode($response);
			exit;
		}
		
		$jumlah=0;
		for($i=0;$i<$resultTransaksi->num_rows();$i++){
			$total=0;
			$total=$resultTransaksi->row()->qty * $resultTransaksi->row()->harga;
			$jumlah += $total;
		}
		$resultNotaBill = $this->db->query("SELECT * from nota_bill where no_transaksi='".$resultKunjungan->no_transaksi."' and kd_kasir='".$resultKunjungan->kd_kasir."' ");
		$user = $this->db->query("select * from zusers where kd_user='".$kd_user."'")->row();

		if ($resultTransaksi->num_rows() > 0) {
			$response['transaksi'] = true;
		}

		/* 	========================================================= HISTORY KUNJUNGAN ============================================== */
		$response['tahap'] = 'History Kunjungan';
		$paramsInsert = array(
			"tgl_kunjungan" => $tgl_kunjungan,
			"kd_pasien" 	=> $kd_pasien,
			"nama" 			=> $resultKunjungan->nama,
			"kd_unit" 		=> $kd_unit,
			"nama_unit" 	=> $resultKunjungan->nama_unit,
			"kd_user_del" 	=> $kd_user,
			"shift" 		=> $resultKunjungan->shift,
			"shiftdel" 		=> $shift,
			"username" 		=> $user->user_names,
			"ket" 			=> $keterangan,
			"tgl_batal" 	=> date('Y-m-d'),
			"jam_batal" 	=> gmdate("d/M/Y H:i:s", time()+60*61*7) 
		);
		$result_PG = $this->db->insert('history_batal_kunjungan',$paramsInsert);

		/* unset($paramsInsert);
		$paramsInsert = array(
			"kd_pasien" 	=> $kd_pasien,
			"kd_unit" 		=> $kd_unit,
			"tgl_masuk" 	=> $tgl_kunjungan,
			"urut_masuk" 	=> $urut_masuk,
			"urut" 			=> $urut_masuk,
			"kd_user" 		=> $kd_user,
			"tgl_update" 	=> date('Y-m-d'),
			"jam_update" 	=> gmdate("d/M/Y H:i:s", time()+60*61*7),
			"keterangan" 	=> $keterangan);
		$result_SQL = $this->dbSQL->insert('history_kunjungan',$paramsInsert); */

		/* 	========================================================= HISTORY TRANSAKSI ============================================== */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			if ($resultTransaksi->num_rows()>0) {
				$response['tahap'] = 'History Transaksi';
				unset($paramsInsert);
				$paramsInsert = array(
					"kd_kasir" 		=> $resultKunjungan->kd_kasir,
					"no_transaksi" 	=> $resultKunjungan->no_transaksi,
					"ispay" 		=> $resultKunjungan->ispay,
					"tgl_transaksi" => $tgl_kunjungan,
					"kd_pasien" 	=> $kd_pasien,
					"nama" 			=> $resultKunjungan->nama,
					"kd_unit" 		=> $kd_unit,
					"nama_unit" 	=> $resultKunjungan->nama_unit,
					"kd_user_del" 	=> $kd_user,
					"kd_user" 		=> $resultKunjungan->kd_user,
					"user_name" 	=> $user->user_names,
					"jumlah" 		=> $jumlah,
					"tgl_batal" 	=> date('Y-m-d'),
					"jam_batal" 	=> gmdate("d/M/Y H:i:s", time()+60*61*7),
					"ket" 			=> $keterangan
				);
	
				$result_PG = $this->db->insert('history_trans',$paramsInsert);
				if ($resultKunjungan->ispay === true) {
					$paramsInsert['ispay'] = 1;
				}else{
					$paramsInsert['ispay'] = 0;
				}
				$paramsInsert['jumlah'] = $jumlah;
				// $result_SQL = $this->dbSQL->insert('history_trans',$paramsInsert);
			}else{
				$response['tahap'] = 'History Transaksi';
				$result_PG 	= true;
				// $result_SQL = true;
			}
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}

		/* 	========================================================= HISTORY DETAIL TRANSAKSI ============================================== */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			if ($resultTransaksi->num_rows()>0) {
				$response['tahap'] = 'History Detail Transaksi';
				foreach ($resultTransaksi->result() as $data) {
					unset($paramsInsert);
					$paramsInsert = array(
						"kd_kasir" 		=> $resultKunjungan->kd_kasir,
						"no_transaksi" 	=> $resultKunjungan->no_transaksi,
						"tgl_transaksi" => $tgl_kunjungan,
						"kd_pasien" 	=> $kd_pasien,
						"nama" 			=> $resultKunjungan->nama,
						"kd_unit" 		=> $kd_unit,
						"nama_unit" 	=> $resultKunjungan->nama_unit,
						"kd_produk" 	=> $data->kd_produk,
						"uraian" 		=> $data->deskripsi,
						"kd_user_del" 	=> $kd_user,
						"kd_user" 		=> $resultKunjungan->kd_user,
						"shift" 		=> $resultKunjungan->shift,
						"shiftdel" 		=> $shift,
						"user_name" 	=> $user->user_names,
						"jumlah" 		=> $data->qty*$data->harga,
						"tgl_batal" 	=> date('Y-m-d'),
						"ket" 			=> $keterangan
					);
					$result_PG	= $this->db->insert('history_detail_trans',$paramsInsert);
					// $result_SQL = $this->dbSQL->insert('history_detail_trans',$paramsInsert);
				}
			}else{
				$response['tahap'] = 'History Detail Transaksi';
				$result_PG 	= true;
				// $result_SQL = true;
			}
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}
		
		/* 	========================================================= MR PENYAKIT ============================================== */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			$resultMR = $this->db->query("SELECT * from mr_penyakit where kd_pasien='".$kd_pasien."' and kd_unit='".$kd_unit."' and tgl_masuk='".$tgl_kunjungan."' and urut_masuk=".$urut_masuk."");
			if ($resultMR->num_rows() > 0) {
				unset($criteriaParams);
				$criteriaParams = array(
					'kd_pasien' 	=> $kd_pasien,
					'kd_unit' 		=> $kd_unit,
					'tgl_masuk' 	=> $tgl_kunjungan,
					'urut_masuk' 	=> $urut_masuk,
				);

				$result_PG 	= $this->db->delete('mr_penyakit', $criteriaParams);
				// $result_SQL = $this->dbSQL->delete('mr_penyakit', $criteriaParams);
			}else{
				$response['tahap'] = 'Delete MR Penyakit';
				$result_PG 	= true;
				// $result_SQL = true;
			}
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}

		/* 	========================================================= DELETE KUNJUNGAN YANG MENGINAP ============================================== */
		if(substr($kd_unit,0,1)=='1'){
			$response['tahap'] = 'Delete Kunjungan Rawat Inap';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_kasir' 		=> $resultKunjungan->kd_kasir,
				'kd_unit' 		=> $kd_unit,
				'no_transaksi' 	=> $resultKunjungan->no_transaksi,
			);
			// $result_SQL = $this->dbSQL->delete("pasien_inap", $criteriaParams); 
			$result_PG = $this->db->delete("pasien_inap", $criteriaParams); 						
		}
		
		/* 	========================================================= DELETE SJP KUNJUNGAN ============================================== */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			$response['tahap'] = 'Delete SJP Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("sjp_kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("sjp_kunjungan", $criteriaParams);
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}

		/* 	========================================================= DELETE RUJUKAN KUNJUNGAN ============================================== */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			$response['tahap'] = 'Delete SJP Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("rujukan_kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("rujukan_kunjungan", $criteriaParams);
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}

		/* 	========================================================= DELETE KUNJUNGAN PASIEN ============================================== */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			$response['tahap'] = 'Delete Kunjungan';
			unset($criteriaParams);
			$criteriaParams = array(
				'kd_pasien' 	=> $kd_pasien,
				'kd_unit' 		=> $kd_unit,
				'tgl_masuk'		=> $tgl_kunjungan,
				'urut_masuk'	=> $urut_masuk,
			);

			// $result_SQL 	= $this->dbSQL->delete("kunjungan", $criteriaParams);
			$result_PG 		= $this->db->delete("kunjungan", $criteriaParams);
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}
		
		/* 	========================================================= UPDATE KAMAR ======================================================= */
		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			if ($resultNginap->num_rows()>0) {
				$response['tahap'] = 'UPDATE KAMAR';
				unset($criteriaParams);
				$kamar = $this->db->query("select * from kamar where kd_unit='".$kd_unit."' and no_kamar='".$resultNginap->row()->no_kamar."'")->row();
				$criteriaParams = array(
					'no_kamar' 	=> $resultNginap->row()->no_kamar,
					'kd_unit' 	=> $kd_unit
				);
				$value = array(
					'digunakan' => $kamar->digunakan - 1
				);
				// $this->dbSQL->where($criteriaParams);
				$this->db->where($criteriaParams);
				// $result_SQL 	= $this->dbSQL->update("kamar", $value);
				$result_PG 		= $this->db->update("kamar", $value);
			} else{
				$response['tahap'] = 'UPDATE KAMAR';
				$result_PG 	= true;
				// $result_SQL = true;
			}
		}else{
			$result_PG 	= false;
			// $result_SQL = false;
		}

		// if (($result_SQL>0 || $result_SQL=== true) && ($result_PG>0 || $result_PG=== true)) {
		if (($result_PG>0 || $result_PG=== true)) {
			$this->db->trans_commit();
			// $this->dbSQL->trans_commit();
			$this->db->close();
			// $this->dbSQL->close();
			$response['success'] = true;
		}else{
			$this->db->trans_rollback();
			// $this->dbSQL->trans_rollback();
			$this->db->close();
			// $this->dbSQL->close();
			$response['success'] = false;
		}
		echo json_encode($response);
	}
}

?>			