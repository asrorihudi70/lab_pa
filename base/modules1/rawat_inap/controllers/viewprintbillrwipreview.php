<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewprintbillrwipreview extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
			// $this->CI->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

   	
   	public function cetakBill(){
   		//$common=$this->common;
   		$result=$this->result;
   		$title='PERINCIAN BIAYA PELAYANAN KESEHATAN RAWAT INAP';
		$param=json_decode($_POST['data']);
	   // $kd_rs=$this->CI->session->userdeta['user_id']['kd_rs'];
	   	$kd_rs='3577015';
	    $kodeuserrs = $this->session->userdata['user_id']['id'];
		$name=$this->db->query("SELECT * FROM zusers WHERE kd_user='".$kodeuserrs."'")->row()->user_names;
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		echo $rs->phone1;
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='<br>Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='<br>Fax. '.$rs->fax.'.';
				
		}
		$no_transaksi = $param->No_TRans;
        $Total = $param->JmlBayar;
        $Bayar = $param->JmlDibayar;
        $criteria = "no_transaksi = '".$no_transaksi."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
		$this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where( $criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
			//echo $query[0][0]->TGL_KELUAR;
           $Medrec = $query[0][0]->KD_PASIEN;
           $Status = $query[0][0]->STATUS;
           $Dokter = $query[0][0]->DOKTER;
           $Nama = $query[0][0]->NAMA;
           $Alamat = $query[0][0]->ALAMAT;
           $Poli = $query[0][0]->UNIT;
           $Notrans = $query[0][0]->NO_TRANSAKSI;
           $Tgl = $query[0][0]->TGL_TRANS;
		   $Tgl_keluar = $query[0][0]->TGL_KELUAR;
           $KdUser = $query[0][0]->KD_USER;
           $uraian = $query[0][0]->DESKRIPSI;
           $jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           //$NoNota = "";
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
		   $Tgl_keluar = "";
        }
        $t1 = 4;
        $t3 = 20;
        $t2 = 60-($t3+$t1);       
        $format1 = date('d F Y', strtotime($Tgl));
		$format2 = date('d F Y', strtotime($Tgl_keluar));
        $today = date("d F Y");
        $Jam = date("G:i:s");
		$queryHead = $this->db->query( "  SELECT distinct(no_kamar),nama_kamar FROM
										(
										 SELECT 1 AS pk_id,
										 kin.nama_kamar,
										 dtrk.no_kamar,	
											t.no_transaksi,
										ps.kd_pasien,
										ps.nama,
										ps.alamat,
										c.customer,
										p.deskripsi,
										dtr.qty::double precision * dtr.harga AS jumlah,
										dtr.kd_user,
										dtr.urut,
										k.baru,
										k.kd_customer,
										t.tgl_transaksi,
										DTR.HARGA,
										p.kd_klas,
										kp.klasifikasi,
										p.kd_produk,
										dtr.qty,
										dtr.kd_kasir,
										dtr.tag,
										d.nama AS nama_dokter,
										k.antrian,
										unit.nama_unit,
										dtr.qty::double precision * TARIF.tarif AS total
										   FROM pasien ps
													 JOIN (transaksi t
														LEFT JOIN (kunjungan k
														LEFT JOIN customer c ON k.kd_customer = c.kd_customer
														LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
															   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
														JOIN (detail_transaksi dtr
																JOIN tarif
																	  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																JOIN produk p ON dtr.kd_produk = p.kd_produk
														 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
														JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
														JOIN unit ON t.kd_unit = unit.kd_unit
													) ON t.kd_pasien = ps.kd_pasien
												JOIN dokter d ON d.kd_dokter = k.kd_dokter 
													 left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
													 dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													 left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
													 where dtr.no_transaksi='".$no_transaksi."')  as a order by no_kamar asc

										");
		$query = $queryHead->result();
		$d_medrec=$this->uri->segment(4,0);
	 	$this->load->library('m_pdf');
        $this->m_pdf->load();
		$mpdf=new mPDF('utf-8', 'A4');
		$mpdf->AddPage($type, // L - landscape, P - portrait
				'', '', '', '',
				$marginLeft, // margin_left
				15, // margin right
				15, // margin top
				15, // margin bottom
				0, // margin header
				12); // margin footer
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumPrefix = 'Hal : ';
		$mpdf->pagenumSuffix = '';
		$mpdf->nbpgPrefix = ' Dari ';
		$mpdf->nbpgSuffix = '';
		$date = date("d-M-Y / H:i");
		$arr = array (
				'odd' => array (
						'L' => array (
								'content' => 'Operator : '.$name,
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'C' => array (
								'content' => "Tgl/Jam : ".$date."",
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'R' => array (
								'content' => '{PAGENO}{nbpg}',
								'font-size' => 8,
								'font-style' => '',
								'font-family' => 'serif',
								'color'=>'#000000'
						),
						'line' => 0,
				),
				'even' => array ()
		);
		for($x=0; $x<35; $x++)
						 {
						 $SPASI.='&nbsp;';
						 $underline.='_';
						 }
	 
		 
		$html='';
		
		$html.="<table style='font-size: 9;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   			<tr>
   				<td width='50'>
   					<img src='./ui/images/Logo/LOGO RSBA.png' width='50' height='50' />
   				</td>
   				<td>
   					<b>".$rs->name."</b><br>
			   		<font style='font-size: 8px;'>".$rs->address.", ".$rs->city."</font>
			   		<font style='font-size: 8px;'>".$telp."</font>
			   		<font style='font-size: 8px;'>".$fax."</font>
   				</td>
   			</tr>
   		</table>";
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'</th>
					</tr>
				</tbody>
			</table><br>';
		$html.='<table width="100%" border="0
					">
					  <tr>
						<td width="10%"><font size="48">No. Nota</font></td>
						<td width="2%">: 01</td>
						<td width="18%">No. Trans</td>
						<td width="12%">: '.$Notrans.'</td>
					  </tr>
					  <tr>
						<td>No. Medrec</td>
						<td>: '.$Medrec.'</td>
						<td>Tanggal Masuk</td>
						<td>: '.$format1.'</td>
						
					  </tr>
					  <tr>
						<td>Status P.</td>
						<td>: '.$Status.'</td>
						<td>Tanggal Keluar</td>
						<td>: '.$format2.'</td>
					  </tr>
					  <tr>
						<td>Dokter</td>
						<td>: '.$Dokter.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>Nama</td>
						<td>: '.$Nama.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>Alamat</td>
						<td>: '.$Alamat.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>Poliklinik Inst.</td>
						<td>: '.$Poli.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					</table><hr>';
		$html.='
			<table class="t1" border = "0">
			';
			/* <thead>
			  <tr>
					<th width="3">No</th>
					<th width="350" align="center">Tindakan</th>
					<th width="20" align="center">Qty</th>
					<th width="100" align="center">Tarif</th>
					<th width="100" align="center">Jumlah</th>
			  </tr>
			</thead>
			
			<td width=""></td>
			
			
									<td width="">'.$no.'</td> */
		if(count($query) > 0) {	
			
			foreach ($query as $line) 
			{
				//$no++;
				$html.='
				<tbody>
						<tr class="headerrow"> 
								
								<th width="" colspan="4" align="left">'.$line->nama_kamar.'</th>
						</tr>
				';
				$queryHead2 = $this->db->query( "  SELECT distinct(kd_klas),klasifikasi,no_kamar,nama_kamar FROM
										(
										 SELECT 1 AS pk_id,
										 kin.nama_kamar,
										 dtrk.no_kamar,	
											t.no_transaksi,
										ps.kd_pasien,
										ps.nama,
										ps.alamat,
										c.customer,
										p.deskripsi,
										dtr.qty::double precision * dtr.harga AS jumlah,
										dtr.kd_user,
										dtr.urut,
										k.baru,
										k.kd_customer,
										t.tgl_transaksi,
										DTR.HARGA,
										p.kd_klas,
										kp.klasifikasi,
										p.kd_produk,
										dtr.qty,
										dtr.kd_kasir,
										dtr.tag,
										d.nama AS nama_dokter,
										k.antrian,
										unit.nama_unit,
										dtr.qty::double precision * TARIF.tarif AS total
										   FROM pasien ps
													 JOIN (transaksi t
														LEFT JOIN (kunjungan k
														LEFT JOIN customer c ON k.kd_customer = c.kd_customer
														LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
															   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
														JOIN (detail_transaksi dtr
																JOIN tarif
																	  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																JOIN produk p ON dtr.kd_produk = p.kd_produk
														 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
														JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
														JOIN unit ON t.kd_unit = unit.kd_unit
													) ON t.kd_pasien = ps.kd_pasien
												JOIN dokter d ON d.kd_dokter = k.kd_dokter 
													 left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
													 dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													 left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
													 where dtr.no_transaksi='".$no_transaksi."' and dtrk.no_kamar='".$line->no_kamar."')  as a order by no_kamar asc

										");
				$query_v = $queryHead2->result();
				foreach ($query_v as $line_v) 
				{
					//$no++;
					$html.='
					<tbody>
							<tr class="headerrow"> 
									
									<th width="" colspan="4" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$line_v->klasifikasi.'</th>
							</tr>
					';
					$queryHasil = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah,tgl_transaksi FROM
													(
													 SELECT 1 AS pk_id,
													 dtrk.no_kamar,
														kin.nama_kamar,
														t.no_transaksi,
														ps.kd_pasien,
														ps.nama,
														ps.alamat,
														c.customer,
														p.deskripsi,
														dtr.qty::double precision * dtr.harga AS jumlah,
														dtr.kd_user,
														dtr.urut,
														k.baru,
														k.kd_customer,
														dtr.tgl_transaksi,
														DTR.HARGA as tarif,
														p.kd_klas,
														kp.klasifikasi,
														p.kd_produk,
														dtr.qty,
														dtr.kd_kasir,
														dtr.tag,
														d.nama AS nama_dokter,
														k.antrian,
														unit.nama_unit,
														dtr.qty::double precision * TARIF.tarif AS total
													   FROM pasien ps
														 JOIN (transaksi t
															LEFT JOIN (kunjungan k
															LEFT JOIN customer c ON k.kd_customer = c.kd_customer
															LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
																   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
															JOIN (detail_transaksi dtr
																	JOIN tarif
																		  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																	  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																	JOIN produk p ON dtr.kd_produk = p.kd_produk
															 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
															JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
															JOIN unit ON t.kd_unit = unit.kd_unit
														) ON t.kd_pasien = ps.kd_pasien
													JOIN dokter d ON d.kd_dokter = k.kd_dokter 										
													left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
														dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
														 where dtr.no_transaksi='".$no_transaksi."' and p.kd_klas='".$line_v->kd_klas."' and dtrk.no_kamar='".$line_v->no_kamar."')  as a order by urut asc

													");
					$query2 = $queryHasil->result();
					$no=0;
					
					foreach ($query2 as $line2) 
					{
						$q_visite=$this->db->query("select * from visite_dokter where no_transaksi='".$Notrans."' and tgl_transaksi='".$line2->tgl_transaksi."' and tag_int=".$line2->kd_produk." ")->result();
						$q_visite2=$this->db->query("select * from produk where kd_produk=".$line2->kd_produk."")->row()->kd_klas;
						if ($q_visite)
						{
							$kd_klas=substr($q_visite2, 0,2);
							if ($kd_klas=='61')
							{
								$q_getvisite=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
													inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter
													inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
													where vd.no_transaksi='".$Notrans."' and tgl_transaksi='".$line2->tgl_transaksi."' and vd.tag_int='".$line2->kd_produk."' and vd.urut='".$line2->urut."' and dii.groups=1")->result();
							}
							else
							{
								$q_getvisite=$this->db->query("select d.nama as kd_nama,vd.kd_job ||'-'||dii.label as kd_lab, vd.jp as jpp from dokter d 
													inner join visite_dokter vd on d.kd_dokter = vd.kd_dokter 
													inner join dokter_inap_int dii on vd.kd_job = dii.kd_job
													where vd.no_transaksi='".$Notrans."' and tgl_transaksi='".$line2->tgl_transaksi."' and vd.tag_int='".$line2->kd_produk."' and vd.urut='".$line2->urut."' and dii.groups=0")->result();
							}
							$jml=count($q_getvisite);
						}
						
						$no++;
						$html.='
						<tbody>
								<tr class="headerrow"> 
										<td width="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
										if ($q_visite)
										{	
											if ($jml>0)
											{
												$nm_dok='';
												if ($jml==1)
												{
													foreach ($q_getvisite as $b)
													{
														$nm_dok.=$b->kd_nama;
													}
												}
												else
												{
													foreach ($q_getvisite as $b)
													{
														$nm_dok.=$b->kd_nama.';';
													}
												}
												 $html.=''.$line2->deskripsi.'('.$nm_dok.')';
											}
											else
											{
												$html.=''.$line2->deskripsi;
											}
										}
										else
										{
												$html.=''.$line2->deskripsi;
											}											
										$html.='</td>
										<td width="" align="center">';
										if ($line2->kd_klas=='2')
										{
											 $html.=''.$line2->qty.' Hari';
										}
										else
										{
											$html.=''.$line2->qty.' X';
										}
										$html.='
										</td>
										<td width="" align="right">@ Rp. '.number_format($line2->tarif,0,',','.').'</td>
										<td width="" align="right">Rp. '.number_format($line2->jumlah,0,',','.').'</td>
								</tr>
						';
						//$sub_tot+=$line2->jumlah;
						/*$tot_order+=$line2->qty_order;
						$tot_selisih+=$line2->selisih; */
					
					}
					$queryHasilTanpaBiayaAdm = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah FROM
													(
													 SELECT 1 AS pk_id,
													 kin.nama_kamar,
														dtrk.no_kamar,	
														t.no_transaksi,
														ps.kd_pasien,
														ps.nama,
														ps.alamat,
														c.customer,
														p.deskripsi,
														dtr.qty::double precision * dtr.harga AS jumlah,
														dtr.kd_user,
														dtr.urut,
														k.baru,
														k.kd_customer,
														dtr.tgl_transaksi,
														DTR.HARGA as tarif,
														p.kd_klas,
														kp.klasifikasi,
														p.kd_produk,
														dtr.qty,
														dtr.kd_kasir,
														dtr.tag,
														d.nama AS nama_dokter,
														k.antrian,
														unit.nama_unit,
														dtr.qty::double precision * TARIF.tarif AS total
													   FROM pasien ps
														 JOIN (transaksi t
															LEFT JOIN (kunjungan k
															LEFT JOIN customer c ON k.kd_customer = c.kd_customer
															LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
																   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
															JOIN (detail_transaksi dtr
																	JOIN tarif
																		  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																	  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																	JOIN produk p ON dtr.kd_produk = p.kd_produk
															 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
															JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
															JOIN unit ON t.kd_unit = unit.kd_unit
														) ON t.kd_pasien = ps.kd_pasien
													JOIN dokter d ON d.kd_dokter = k.kd_dokter  
													left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
														dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
														 where dtr.no_transaksi='".$no_transaksi."' and p.kd_klas='".$line_v->kd_klas."' and dtrk.no_kamar='".$line_v->no_kamar."' and kp.kd_klas not in (select kd_klas from klas_produk where kd_klas='1') )  as a order by urut asc

													");
					$query5 = $queryHasilTanpaBiayaAdm->result();
					$no=0;
					foreach ($query5 as $line5) 
					{
						$sub_tot+=$line5->jumlah;
						
					}
					$queryHasilDenganBiayaAdm = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah FROM
													(
													 SELECT 1 AS pk_id,
													 kin.nama_kamar,
														dtrk.no_kamar,	
														t.no_transaksi,
														ps.kd_pasien,
														ps.nama,
														ps.alamat,
														c.customer,
														p.deskripsi,
														dtr.qty::double precision * dtr.harga AS jumlah,
														dtr.kd_user,
														dtr.urut,
														k.baru,
														k.kd_customer,
														dtr.tgl_transaksi,
														DTR.HARGA as tarif,
														p.kd_klas,
														kp.klasifikasi,
														p.kd_produk,
														dtr.qty,
														dtr.kd_kasir,
														dtr.tag,
														d.nama AS nama_dokter,
														k.antrian,
														unit.nama_unit,
														dtr.qty::double precision * TARIF.tarif AS total
													   FROM pasien ps
														 JOIN (transaksi t
																LEFT JOIN (kunjungan k
																LEFT JOIN customer c ON k.kd_customer = c.kd_customer
																LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter
																	   ) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
																JOIN (detail_transaksi dtr
																		JOIN tarif
																			  ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit 
																		  AND tarif.tgl_berlaku = dtr.tgl_berlaku
																		JOIN produk p ON dtr.kd_produk = p.kd_produk
																 ) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
																JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
																JOIN unit ON t.kd_unit = unit.kd_unit
															) ON t.kd_pasien = ps.kd_pasien
														JOIN dokter d ON d.kd_dokter = k.kd_dokter 
														left join detail_tr_kamar dtrk on  dtr.no_transaksi =dtrk.no_transaksi AND dtrk.kd_kasir = dtr.kd_kasir and
														dtr.urut =dtrk.urut AND dtrk.tgl_transaksi = dtr.tgl_transaksi
													left join kamar_induk kin on dtrk.no_kamar=kin.no_kamar
														 where dtr.no_transaksi='".$no_transaksi."' and p.kd_klas='".$line_v->kd_klas."' and dtrk.no_kamar='".$line_v->no_kamar."' and kp.kd_klas in (select kd_klas from klas_produk where kd_klas='1') )  as a order by urut asc

													");
					$query6 = $queryHasilDenganBiayaAdm->result();
					$no=0;
					foreach ($query6 as $line6) 
					{
						$biaya_adm+=$line6->jumlah;
					}
				}
				$sub_totals=$sub_tot;
				$biaya_admin=$biaya_adm;
				$total_biaya=$biaya_admin+$sub_totals;
			}
			$queryHasilKodeKasir = $this->db->query("select setting from sys_setting where key_data='rwi_kd_kasir'");
			$query3 = $queryHasilKodeKasir->result();
			$kd_kasir=$queryHasilKodeKasir->row()->setting;
			/* foreach ($query3 as $line3) 
				{
					$kd_kasir=$line3->setting;
				} */
			$queryHasilTotalTrx = $this->db->query("select db.no_transaksi,db.shift, db.tgl_transaksi as tgl_bayar, db.kd_pay, db.kd_user, z.user_names, p.jenis_pay, pt.deskripsi, 
                            db.jumlah as bayar, db.urut as urut_bayar
                            from detail_bayar db
                            left join payment p on db.kd_pay = p.kd_pay 
                            left join payment_type pt on p.jenis_pay = pt.jenis_pay 
                            left join zusers z on db.kd_user = z.kd_user::integer
                            where db.no_transaksi='".$no_transaksi."' and db.kd_kasir='".$kd_kasir."'");
			$query4 = $queryHasilTotalTrx->result();
			 $html.='
			 <tr class="headerrow"> 
					<th width="" colspan="4"><hr></th>
			</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right">Sub Total</th>
					<th width="" align="right">Rp. '.number_format($sub_totals,0,',','.').'</th>
				</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right">Biaya Adm.</th>
					<th width="" align="right">Rp. '.number_format($biaya_admin,0,',','.').'</th>
				</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right">Total Biaya</th>
					<th width="" align="right">Rp. '.number_format($total_biaya,0,',','.').'</th>
				</tr>
				<tr class="headerrow"> 
					<th width="" colspan="3" align="right"></th>
					<th width="" colspan="1" align="right"><hr></th>
				</tr>
				

			'; 	
			foreach ($query4 as $line4) 
				{
					$html.='<tr class="headerrow"> 
						<th width="" colspan="3" align="right">'.$line4->deskripsi.'</th>
						<th width="" align="right">Rp. '.number_format($line4->bayar,0,',','.').'</th>
					</tr>
				';
				}
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="9" align="center">Data tidak ada</th>
				</tr>
			';		
		}
		$html.='</tbody></table>';
		$mpdf->WriteHTML("
           <style>
   				table{
	   				width: 100%;
					font-family: Arial, Helvetica, sans-serif;
   					border-collapse: collapse;
   					font-size: 12;
   				}
           </style>
           ");
		$mpdf->SetTitle('PELAYANAN KESEHATAN RAWAT INAP');
		$mpdf->SetFooter($arr);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "I");
		header ( 'Content-type: application/pdf' );
		header ( 'Content-Disposition: attachment; filename="bill.pdf"' );
		readfile ( 'original.pdf' );
		//$prop=array('foot'=>true);
		//$this->common->setPdf('P','BIAYA PELAYANAN kESEHATAN RAWAT INAP',$html);	
   	}
}
?>