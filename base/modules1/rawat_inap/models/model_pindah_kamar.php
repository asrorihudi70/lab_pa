<?php 	
class Model_pindah_kamar extends Model
{
	private $tbl_nginap      = "nginap";
	private $tbl_kamar       = "kamar";
	private $tbl_kunjungan   = "kunjungan";
	private $tbl_pasien_inap = "pasien_inap";
	private $tbl_transaksi 	 = "transaksi";
	//private $tbl_kamar_induk = "kamar_induk";
	private $id_user         = "";
	private $dbSQL           = "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		======================================================================================================================
		======================================================== SQL QUERY ===================================================
		======================================================================================================================
	 */
	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE NGINAP
	 */
		public function getSelectedDataSQL($criteria){
			$result = false;
			$this->dbSQL->select(" *, URUT_MASUK as urut_masuk, TGL_MASUK as tgl_masuk, URUT_NGINAP as urut_nginap ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_nginap);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function updatePindahKamarSQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_nginap, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			
			return $result;
		}

		public function insertPindahKamarSQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->tbl_nginap, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;			
			}
			return $result;
		}
	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE NGINAP
	 */
	
	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE KUNJUNGAN
	 */
		public function getLastKunjunganSQL($criteria){
			$result = false;
			$this->dbSQL->select(" top 1 *");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_kunjungan);
			$this->dbSQL->order_by('tgl_masuk', 'DESC');
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function updateDataKunjunganSQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_kunjungan, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}		
			return $result;
		}


	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE KUNJUNGAN
	 */
	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE KAMAR
	 */
		public function getSelectedDataKamarSQL($criteria){
			$result = false;
			$this->dbSQL->select(" DIGUNAKAN as digunakan, KD_UNIT as kd_unit ");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_kamar);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}

		public function updateKamarSQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_kamar, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE KAMAR
	 */
	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE TRANSAKSI
	 */
		public function getSelectedDataTransaksiSQL($criteria){
			$result = false;
			$this->dbSQL->select("*");
			$this->dbSQL->where($criteria);
			$this->dbSQL->from($this->tbl_transaksi);
			$result = $this->dbSQL->get();
			//$this->dbSQL->close();
			return $result;
		}


		public function updateDataPulangPasienSQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_transaksi, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}		
			return $result;
		}


	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE TRANSAKSI
	 */
	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE PASIEN INAP
	 */
		public function updatePasienInapSQL($criteria, $data){
			$result = false;
			try {
				$this->dbSQL->where($criteria);
				$this->dbSQL->update($this->tbl_pasien_inap, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;
			}
			return $result;
		}

		public function insertPasienInap_SQL($data){
			$result = false;
			try {
				$this->dbSQL->insert($this->tbl_pasien_inap, $data);
				if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;			
			}
			return $result;
		}

	/*
		=====================================================================================================================
		=====================================================================================================================
		================================================================================================= TABLE PASIEN INAP
	 */

	/*public function insertPulangPasien($data){
		$result = false;
		try {
			$this->dbSQL->insert($this->tbl_transaksi, $data);
			if ($this->dbSQL->affected_rows()>0 || $this->dbSQL->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;			
		}
		return $result;
	}*/
	/*
		======================================================================================================================
		==================================================== END SQL QUERY ===================================================
		======================================================================================================================
	 */

	public function getLastKunjungan($criteria){
		$result = false;
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->tbl_kunjungan);
		$this->db->order_by('tgl_masuk', 'DESC LIMIT 1');
		$result = $this->db->get();
		return $result;
	}

	public function getSelectedData($criteria){
		$result = false;
		$this->db->select(" *, urut_masuk as urut_masuk, tgl_masuk as tgl_masuk, urut_nginap as urut_nginap ");
		//$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->tbl_nginap);
		$result = $this->db->get();
		//$this->db->close();
		return $result;
	}

	public function getSelectedDataTransaksi($criteria){
		$result = false;
		$this->db->select("*");
		$this->db->where($criteria);
		$this->db->from($this->tbl_transaksi);
		$result = $this->db->get();
		//$this->db->close();
		return $result;
	}

	public function getSelectedDataKamar($criteria){
		$result = false;
		//$this->db->select("*");
		$this->db->select(" digunakan as digunakan, kd_unit as kd_unit ");
		$this->db->where($criteria);
		$this->db->from($this->tbl_kamar);
		$result = $this->db->get();
		//$this->db->close();
		return $result;
	}

	public function updateDataPulangPasien($criteria, $data){
		$result = false;
		try {
			$this->db->where($criteria);
			$this->db->update($this->tbl_transaksi, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}		
		return $result;
	}

	public function updateDataKunjungan($criteria, $data){
		$result = false;
		try {
			$this->db->where($criteria);
			$this->db->update($this->tbl_kunjungan, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;
		}
		
		return $result;
	}
	public function updatePindahKamar($criteria, $data){
		$result = false;
		try {
			$this->db->where($criteria);
			$this->db->update($this->tbl_nginap, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;	
		}
		return $result;
	}

	public function updatePasienInap($criteria, $data){
		$result = false;
		try {
			$this->db->where($criteria);
			$this->db->update($this->tbl_pasien_inap, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;	
			
		}
		return $result;
	}

	public function updateKamar($criteria, $data){
		$result = false;
		try {
			$this->db->where($criteria);
			$this->db->update($this->tbl_kamar, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;	
			
		}
		return $result;
	}

	public function insertPindahKamar($data){
		$result = false;
		try {
			$this->db->insert($this->tbl_nginap, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;			
		}
		return $result;
	}

		public function insertPasienInap($data){
			$result = false;
			try {
				$this->db->insert($this->tbl_pasien_inap, $data);
				if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
					$result = true;
				}else{
					$result = false;
				}
			} catch (Exception $e) {
				$result = false;			
			}
			return $result;
		}
	/*public function insertPulangPasien($data){
		$result = false;
		try {
			$this->db->insert($this->tbl_transaksi, $data);
			if ($this->db->affected_rows()>0 || $this->db->affected_rows()===true) {
				$result = true;
			}else{
				$result = false;
			}
		} catch (Exception $e) {
			$result = false;			
		}
		return $result;
	}*/
}
?>