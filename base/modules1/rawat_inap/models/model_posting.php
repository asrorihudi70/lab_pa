<?php 	
class Model_posting extends Model
{
	private $tbl_produk_charge    = 'produk_charge';
	private $tbl_produk           = 'produk';
	private $tbl_produk_component = 'produk_component';
	private $id_user              = "";
	private $dbSQL                = "";

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
		$this->id_user = $this->session->userdata['user_id']['id'];
		$this->dbSQL   = $this->load->database('otherdb2',TRUE);
	}

	/*
		======================================================================================================================
		======================================================== SQL QUERY ===================================================
		======================================================================================================================
	 */
	
	public function getDataComboProduk($criteria){
		$result = false;
		$this->dbSQL->select("*, produk.deskripsi as deskripsi, produk.kd_produk as kd_produk, produk_charge.kd_unit as kd_unit ");
		if (isset($criteria)) {
			$this->dbSQL->where($criteria);
		}
		$this->dbSQL->from($this->tbl_produk);
		$this->dbSQL->join($this->tbl_produk_charge, $this->tbl_produk_charge.".kd_produk = ".$this->tbl_produk.".kd_produk", "INNER");
		$this->dbSQL->order_by($this->tbl_produk.".deskripsi", "ASC");
		$result = $this->dbSQL->get();
		$this->dbSQL->close();
		return $result;
	}
	
	public function getDataProdukComponent(){
		$result = false;
		$this->dbSQL->select("*, KD_COMPONENT as kd_component, KD_JENIS as kd_jenis, COMPONENT as component ");
		$this->dbSQL->where(" kd_jenis NOT IN ('3') ");
		$this->dbSQL->from($this->tbl_produk_component);
		$this->dbSQL->order_by("component", "ASC");
		$result = $this->dbSQL->get();
		$this->dbSQL->close();
		return $result;
	}

	/*
		======================================================================================================================
		==================================================== END SQL QUERY ===================================================
		======================================================================================================================
	 */

}
?>