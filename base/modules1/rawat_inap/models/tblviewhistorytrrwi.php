<?php
class tblviewhistorytrrwi extends TblBase
{
    function __construct()
    {
        $this->TblName='tblviewhistorytrrwi';
        TblBase::TblBase(true);
		$this->StrSql="no_transaksi, kd_kasir,tgl_bayar,deskripsi,bayar,user_names,urut_bayar,kd_user,shift, kd_pay";
		
        $this->SqlQuery= "select  * from (
                            select db.no_transaksi,db.kd_kasir,db.shift, db.tgl_transaksi as tgl_bayar, db.kd_pay, db.kd_user, z.user_names, p.jenis_pay, p.uraian as deskripsi, 
                            db.jumlah as bayar, db.urut as urut_bayar, db.kd_unit
                            from detail_bayar db
                            left join payment p on db.kd_pay = p.kd_pay 
                            left join payment_type pt on p.jenis_pay = pt.jenis_pay 
                            left join zusers z on db.kd_user = z.kd_user::integer
                            order by  db.urut
                            ) as resdata ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirdetailrwi;

        $row->NO_TRANSAKSI = $rec->no_transaksi;
        $row->KD_KASIR = $rec->kd_kasir;
        $row->TGL_BAYAR = $rec->tgl_bayar;
        $row->DESKRIPSI = $rec->deskripsi;
        $row->BAYAR = $rec->bayar;
        $row->KD_UNIT = $rec->kd_unit;
        $row->KD_PAY = $rec->kd_pay;
        $row->USERNAME = $rec->user_names;
		$row->URUT = $rec->urut_bayar;
		$row->KD_USER = $rec->kd_user;
		$row->SHIFT = $rec->shift;
        return $row;
    }

}

class Rowtblviewkasirdetailrwi
{

    public $NO_TRANSAKSI;
    public $KD_KASIR;
    public $TGL_BAYAR;
    public $DESKRIPSI;
    public $BAYAR;
    public $KD_UNIT;
    public $KD_PAY;
    public $USERNAME;
    public $URUT;
	public $KD_USER;
	public $SHIFT;
	
}

?>
