﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewkasirrwi extends TblBase
{
	function __construct()
	{
		$this->StrSql="customer,nama_kamar,no_transaksi,kd_unit,
		
		nama,alamat,

		kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,lunas,co_status,kd_kasir,
		kd_kelas,no_kamar,kd_unit_kamar,
		ket_payment";
	
		/*$this->SqlQuery = "	select * from (  select customer.customer,k.kd_kelas,unit.parent as parentnya, k.kelas as namakelas,unit.nama_unit,datainap.kd_unit,datainap.no_kamar, 
												kamar.nama_kamar,datainap.kd_unit as unitKamar,unit.kd_bagian,nginap.kd_unit_kamar, nginap.tgl_inap,pasien.nama, pasien.alamat,
												kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, dokter.nama as nama_dokter,transaksi.lunas, 
												kunjungan.*, transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,dokter.kd_dokter, transaksi.orderlist,
												transaksi.posting_transaksi, knt.jenis_cust ,payment_type.deskripsi as cara_bayar,payment.uraian as ket_payment,payment_type.jenis_pay,payment.kd_pay,payment_type.type_data
											from (( kunjungan inner join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											left join dokter on dokter.kd_dokter=kunjungan.kd_dokter)
											inner join customer on customer.kd_customer= kunjungan.kd_customer 
											inner join kontraktor knt on knt.kd_customer=kunjungan.kd_customer
											inner join transaksi on transaksi.kd_pasien = kunjungan.kd_pasien
												and transaksi.kd_unit =kunjungan.kd_unit and transaksi.tgl_transaksi=kunjungan.tgl_masuk 
												and transaksi.urut_masuk=kunjungan.urut_masuk
											left join pasien_inap as datainap on transaksi.no_transaksi=datainap.no_transaksi
												AND transaksi.kd_kasir = datainap.kd_kasir 
											inner join unit on kunjungan.kd_unit=unit.kd_unit 
											inner join payment on payment.kd_customer = kunjungan.kd_customer
											inner join payment_type on payment.jenis_pay = payment_type.jenis_pay
											inner join nginap on kunjungan.kd_pasien=nginap.kd_pasien and kunjungan.kd_unit=nginap.kd_unit
											and kunjungan.tgl_masuk=nginap.tgl_masuk and kunjungan.urut_masuk=nginap.urut_masuk and nginap.akhir=true  
											inner join kamar on kamar.no_kamar=nginap.no_kamar and kamar.kd_unit=nginap.kd_unit_kamar
											inner join kelas k ON k.kd_kelas = unit.kd_kelas
							) as resdata ";*/
		$this->SqlQuery = "SELECT 
				p.kd_pasien, 
				p.kd_pasien as kode_pasien, 
				p.nama, t.no_transaksi, 
				t.tgl_transaksi, 
				k.tgl_masuk, 
				k.jam_masuk, 
				t.kd_unit, 
				u.nama_unit, 
				i.kd_unit as inap_unit, 
				l.kd_kelas, 
				l.kelas, 
				i.no_kamar, 
				u.parent as parentnya, 
				q.Nama_Kamar, k.Kd_Customer, c.Customer, 
				tc.Kd_Tarif, 
				t.Kd_Kasir, 
				t.Urut_Masuk, 
				sp.kd_spesial,
				sp.spesialisasi,
				t.co_status, 
				t.posting_transaksi,
				--case when gettagihan(t.kd_kasir, t.no_transaksi) = getpembayaran(t.kd_kasir, t.no_transaksi) then 't' else 'f' end as lunas,
				t.lunas as lunas,
				l.kelas as namakelas, 
				p.Alamat, 
			 p.No_Asuransi, p.Pemegang_Asuransi, i.kd_spesial, dr.kd_dokter, dr.nama as Nama_Dokter,
				paytype.deskripsi as cara_bayar,
				pay.uraian as ket_payment,
				paytype.jenis_pay,
				pay.kd_pay
				FROM ((((((((Pasien_Inap i 
				INNER JOIN Transaksi t ON i.Kd_Kasir = t.Kd_Kasir AND i.No_Transaksi = t.No_Transaksi) 
				INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien)
				INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = K.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk)
				INNER JOIN Customer C ON k.Kd_Customer = c.Kd_Customer)
				INNER JOIN Dokter dr ON k.kd_dokter=dr.kd_dokter) 
				INNER JOIN Unit u ON i.Kd_Unit = u.Kd_Unit)
				INNER JOIN Kelas l ON u.Kd_Kelas = l.Kd_Kelas) 
				INNER JOIN Tarif_Cust tc ON k.Kd_Customer = tc.Kd_Customer) 
				INNER JOIN Kamar q ON i.Kd_Unit = q.Kd_Unit AND i.No_Kamar = q.No_Kamar 
				INNER JOIN spesialisasi sp ON sp.kd_spesial = i.kd_spesial
				inner join payment pay on pay.kd_customer = k.kd_customer
				inner join payment_type paytype on pay.jenis_pay = paytype.jenis_pay
			--WHERE
		";
		$this->TblName='viewtrrwi';
		TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT         = $rec->nama_unit;
		$row->NAMA              = $rec->nama;
		$row->ALAMAT            = $rec->alamat;
		$row->KD_PASIEN         = $rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI = $rec->tgl_transaksi;
		$row->TANGGAL_MASUK     = date_format(date_create($rec->tgl_masuk), 'Y-m-d');
		$row->JAM_MASUK     	= date_format(date_create($rec->jam_masuk), 'H:i:s');
		$row->NAMA_DOKTER       = $rec->nama_dokter;
		$row->NO_TRANSAKSI      = $rec->no_transaksi;
		$row->KD_CUSTOMER       = $rec->kd_customer;
		$row->CUSTOMER          = $rec->customer;
		$row->KD_UNIT           = $rec->kd_unit;
		$row->KD_DOKTER         = $rec->kd_dokter;
		$row->URUT_MASUK        = $rec->urut_masuk;
		$row->NAMA_KAMAR        = $rec->nama_kamar;
		//$row->KET_PAYMENT     = $rec->ket_payment;
		//$row->CARA_BAYAR      = $rec->cara_bayar;
		//$row->KD_PAY          = $rec->kd_pay;
		//$row->JENIS_PAY       = $rec->jenis_pay;
		//$row->TYPE_DATA       = $rec->type_data;
		$row->POSTING_TRANSAKSI = $rec->posting_transaksi;
		$row->CO_STATUS         = $rec->co_status;
		$row->PARENTNYA         = $rec->parentnya;
		$row->LUNAS             = $rec->lunas;
		$row->NO_KAMAR          = $rec->no_kamar;
		// $row->TGL_NGINAP      	= $rec->tgl_inap;
		$row->KELAS             = $rec->namakelas;
		$row->KD_KELAS          = $rec->kd_kelas;
		$row->KD_UNIT_KAMAR     = $rec->inap_unit;
		$row->KD_KASIR          = $rec->kd_kasir;
		$row->KD_SPESIAL        = $rec->kd_spesial;
		$row->SPESIALISASI      = $rec->spesialisasi;
		$row->KETERANGAN        = $rec->spesialisasi."/ ".$rec->namakelas."/ ".$rec->no_kamar." [ ".$rec->nama_kamar." ] ";
		$row->CARA_BAYAR        = $rec->cara_bayar;
		$row->KET_PAYMENT       = $rec->ket_payment;
		$row->JENIS_PAY         = $rec->jenis_pay;
		$row->KD_PAY         	= $rec->kd_pay;
		return $row;
	}
}
class Rowdokter
{
	
	public $KD_UNIT;
    //public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $TANGGAL_MASUK;
	public $JAM_MASUK;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $NAMA_KAMAR;
	//public $KET_PAYMENT;
	//public $CARA_BAYAR;
	//public $KD_PAY;
	//public $JENIS_PAY;
	//public $TYPE_DATA;
	public $POSTING_TRANSAKSI;
	public $CO_STATUS;
	public $LUNAS;
	public $KELAS;
	public $TGL_NGINAP;
	public $NO_KAMAR;
	public $KD_KELAS;
	public $KD_UNIT_KAMAR;
	public $KD_KASIR;
	public $SPESIALISASI;
	public $KD_SPESIAL;
	public $KETERANGAN;
	public $CARA_BAYAR;
	public $KET_PAYMENT;
	public $JENIS_PAY;
	public $KD_PAY;
}

?>