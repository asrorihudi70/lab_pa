<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class directkwitansi extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
    $this->load->view('main/index');
    }
    
    
    public function save($Params=NULL)
    {
        $mError = "";
        $mError = $this->cetak($Params);
        if ($mError=="sukses")
        {
            echo '{success: true}';
        }
        else{
            echo $mError;
        }
    }
    
    public function cetak($Params)
    {
        $strError = "";
        $no_transaksi = $Params["No_TRans"];
        $Total = $Params["JmlBayar"];
        $tmptotalBayar = str_replace(".","",$Total);
		//echo $tmptotalBayar;
		$queryHasil = $this->db->query( "SELECT urut,kd_klas,klasifikasi,kd_produk,deskripsi,qty, tarif, qty* tarif as jumlah FROM
												(
												 SELECT 1 AS pk_id,
													t.no_transaksi,
													ps.kd_pasien,
													ps.nama,
													ps.alamat,
													c.customer,
													p.deskripsi,
													dtr.qty::double precision * dtr.harga AS jumlah,
													dtr.kd_user,
													dtr.urut,
													k.baru,
													k.kd_customer,
													t.tgl_transaksi,
													tc.tarif,
													tc.kd_component,
													p.kd_klas,
													kp.klasifikasi,
													p.kd_produk,
													dtr.qty,
													dtr.kd_kasir,
													dtr.tag,
													pc.parent,
													d.nama AS nama_dokter,
													k.antrian,
													unit.nama_unit,
													dtr.qty::double precision * tc.tarif AS total
												   FROM pasien ps
													 JOIN (transaksi t
													 LEFT JOIN (kunjungan k
													 LEFT JOIN customer c ON k.kd_customer = c.kd_customer
													 LEFT JOIN dokter ON k.kd_dokter = dokter.kd_dokter) ON k.kd_pasien = t.kd_pasien AND t.kd_unit = k.kd_unit AND k.tgl_masuk = t.tgl_transaksi AND k.urut_masuk = t.urut_masuk
													 JOIN (detail_transaksi dtr
													 JOIN (tarif
													 JOIN (tarif_component tc
													 JOIN produk_component pc ON pc.kd_component = tc.kd_component) ON tarif.kd_produk = tc.kd_produk AND tarif.kd_tarif = tc.kd_tarif AND tarif.kd_unit = tc.kd_unit AND tarif.tgl_berlaku = tc.tgl_berlaku) ON tarif.kd_produk = dtr.kd_produk AND tarif.kd_tarif = dtr.kd_tarif AND tarif.kd_unit = dtr.kd_unit AND tarif.tgl_berlaku = dtr.tgl_berlaku
													 JOIN produk p ON dtr.kd_produk = p.kd_produk) ON dtr.no_transaksi = t.no_transaksi AND t.kd_kasir = dtr.kd_kasir
													 JOIN klas_produk kp ON p.kd_klas = kp.kd_klas
													 JOIN unit ON t.kd_unit = unit.kd_unit) ON t.kd_pasien = ps.kd_pasien
													 JOIN dokter d ON d.kd_dokter = k.kd_dokter where dtr.no_transaksi='".$no_transaksi."')  as a order by urut asc

												");
				$query2 = $queryHasil->result();
				$tot_biaya=0;
				foreach ($query2 as $line2)
				{
					$tot_biaya+=$line2->jumlah;
				}
				
        $q = $this->db->query("select * from detail_bayar db
                                inner join payment p on db.kd_pay = p.kd_pay
                                where no_transaksi = '".$no_transaksi."' and jenis_pay = 1");
        
        if($q->num_rows == 0)
        {
            $strError= '{ success : false, pesan : "Data Tidak Di temukan / Jenis Pembayaran Bukan Tunai"}';
        }
        else {
        $logged = $this->session->userdata('user_id');
         
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $NameRS = $query[0][0]->NAME;
           $Address = $query[0][0]->ADDRESS;
           $TLP = $query[0][0]->PHONE1;
           $Kota = $query[0][0]->CITY;
        }
        else
        {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        
        $criteria = "no_transaksi = '".$no_transaksi."'";
        $paramquery = "where no_transaksi = '".$no_transaksi."'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList( 0,1, "", "","");
        if($query[1]!=0)
        {
           $Medrec = $query[0][0]->KD_PASIEN;
           $Status = $query[0][0]->STATUS;
           $Dokter = $query[0][0]->DOKTER;
           $Nama = $query[0][0]->NAMA;
           $Alamat = $query[0][0]->ALAMAT;
           $Poli = $query[0][0]->UNIT;
           $Notrans = $query[0][0]->NO_TRANSAKSI;
           $Tgl = $query[0][0]->TGL_TRANS;
           $KdUser = $query[0][0]->KD_USER;
           $uraian = $query[0][0]->DESKRIPSI;
           $jumlahbayar = $query[0][0]->JUMLAH;
        }
        else
        {
           $Medrec = "";
           $Status = "";
           $Dokter = "";
           $Nama = "";
           $Alamat = "";
           $Poli = " ";
           $Notrans = "";
           $Tgl = "";
        }
        
        //$printer = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_printer_kwitansi'")->row()->setting;
        $printer = $Params["printer"];
               
        $format1 = date('d F Y', strtotime($Tgl));
        $today = Bulaninindonesia(date("d F Y"));
        $Jam = date("G:i:s");
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $condensed1;    
        $Data .= chr(27) . chr(87) . chr(49) ;
        $Data .= $NameRS."\n";
        $Data .= "\n";
        $Data .= $Address."\n";
        $Data .= "\n";
        $Data .= "Phone : ".$TLP."\n";
        $Data .= "\n";
        $Data .= str_pad($bold1."KWITANSI".$bold0,62," ",STR_PAD_BOTH)."\n";
        $Data .= "\n";
        $Data .= str_pad("No. Transaksi : ".$no_transaksi,31," ").str_pad("No. Medrec : ".$Medrec,31," ")."\n";
        $Data .= "\n";
        $Data .= str_pad("Telah Terima Dari : ".$Nama, 30," ")."\n";
        $Data .= "\n";
        $Data .= "Banyaknya uang terbilang : ".terbilang($tot_biaya).' Rupiah'."\n";
        $Data .= "\n";
        $Data .= "Untuk pembayaran Pemeriksaan untuk Pasien : ".$Nama."\n";
        $Data .= "\n";
        $Data .= "Unit yang dituju : ".$Poli."\n";
        $Data .= "\n";
        $Data .= "Jumlah Rp. ".number_format($tot_biaya,0,',','.')."\n";
        $Data .= "\n";
        $Data .= str_pad(" ",31, " ").str_pad($Kota.' , '.$today, 31," ",STR_PAD_LEFT)."\n";
        $Data .= "\n";
        $Data .= "\n\n\n\n\n\n\n\n\n\n\n\n\n";
        fwrite($handle, $Data);
        fclose($handle);
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P ".$printer." ".$file); # Lakukan cetak linux
		}
        
//        copy($file, $printer);  # Lakukan cetak
//        unlink($file);
        
        $strError = "sukses";
        
        
        }
        return $strError;
        
    }
    
   
            
}




