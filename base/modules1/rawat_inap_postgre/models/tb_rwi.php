<?php

class tb_rwi extends TblBase
{
    function __construct()
    {
        $this->TblName='viewkunjungan';
        TblBase::TblBase(true);

        $this->SqlQuery= "SELECT kunjungan.anamnese,pasien.kd_pasien, pasien.nama, pasien.nama_keluarga, pasien.jenis_kelamin, pasien.tempat_lahir,
	 pasien.tgl_lahir, agama.agama, pasien.gol_darah, pasien.wni, pasien.status_marita, pasien.alamat, pasien.kd_kelurahan,
	 pendidikan.pendidikan, pekerjaan.pekerjaan, unit.nama_unit,transaksi.kd_unit, kunjungan.tgl_masuk, kunjungan.urut_masuk,
	 kb.kd_kabupaten, kb.KABUPATEN, kc.kd_kecamatan, kc.KECAMATAN, pr.kd_propinsi, pr.PROPINSI, pasien.kd_pendidikan, pasien.kd_pekerjaan,
		pasien.kd_agama ,pasien.alamat_ktp,pasien.nama_ayah,pasien.nama_ibu,
		pasien.kd_kelurahan_ktp,pasien.kd_pos_ktp,pasien.kd_pos,proktp.kd_propinsi as propinsiktp,kabktp.kd_kabupaten as kabupatenktp,
		kecktp.kd_kecamatan as kecamatanktp,
		kelktp.kd_kelurahan as kelurahanktp ,kl.kelurahan as kelurahan_pasien,
		kelktp.kelurahan as kel_ktp ,
		kecktp.kecamatan as kec_ktp,kabktp.kabupaten as kab_ktp ,proktp.propinsi as pro_ktp,
		pasien.email,pasien.handphone,pasien.telepon,pasien.no_asuransi
		 FROM pasien
     INNER JOIN kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien 
     INNER join agama ON pasien.kd_agama = agama.kd_agama
      INNER JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
      INNER JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan 
      inner join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN 
      inner join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN 
      inner join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN 
      inner join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI 
      inner join transaksi on transaksi.KD_PASIEN = kunjungan.KD_PASIEN and kunjungan.urut_masuk= transaksi.urut_masuk and kunjungan.kd_unit=transaksi.kd_unit and kunjungan.tgl_masuk=transaksi.tgl_transaksi
      INNER JOIN unit ON transaksi.kd_unit = unit.kd_unit

	left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp
	left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
	left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten
	left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi
                           
";
        }

    function FillRow($rec)
    {
        $row=new Rowviewrwi;

          
          $row->KD_PASIEN=$rec->kd_pasien;
          $row->NAMA=$rec->nama;
          $row->NAMA_KELUARGA=$rec->nama_keluarga;
          $row->JENIS_KELAMIN=$rec->jenis_kelamin;
          $row->TEMPAT_LAHIR=$rec->tempat_lahir;
          $row->TGL_LAHIR=$rec->tgl_lahir;
          $row->AGAMA=$rec->agama;
          $row->GOL_DARAH=$rec->gol_darah;
          $row->WNI=$rec->wni;
          $row->STATUS_MARITA=$rec->status_marita;
          $row->ALAMAT=$rec->alamat;
          $row->KD_KELURAHAN=$rec->kd_kelurahan;
          $row->PENDIDIKAN=$rec->pendidikan;
          $row->PEKERJAAN=$rec->pekerjaan;
          $row->KABUPATEN=$rec->kabupaten;
          $row->KECAMATAN=$rec->kecamatan;
          $row->PROPINSI=$rec->propinsi;
          $row->KD_KABUPATEN=$rec->kd_kabupaten;
          $row->KD_KECAMATAN=$rec->kd_kecamatan;
          $row->KD_PROPINSI=$rec->kd_propinsi;
          $row->KD_PENDIDIKAN=$rec->kd_pendidikan;
          $row->KD_PEKERJAAN=$rec->kd_pekerjaan;
          $row->KD_AGAMA=$rec->kd_agama;
	
		  $row->ALAMAT_KTP=$rec->alamat_ktp;
		  $row->NAMA_AYAH=$rec->nama_ayah;
		  $row->NAMA_IBU=$rec->nama_ibu;
		  $row->KD_KELURAHAN_KTP=$rec->kd_kelurahan_ktp;
		  $row->KD_POS_KTP=$rec->kd_pos_ktp;
		  $row->KD_POS=$rec->kd_pos;
		  $row->anamnese=$rec->anamnese;
		
		  $row->PROPINSIKTP=$rec->propinsiktp;
		  $row->KABUPATENKTP=$rec->kabupatenktp;
		  $row->KECAMATANKTP=$rec->kecamatanktp;
		  $row->KELURAHANKTP=$rec->kelurahanktp;
		  $row->KELURAHAN=$rec->kelurahan_pasien;
		  $row->NAMA_UNIT=$rec->nama_unit;
		 
		  $row->KEL_KTP=$rec->kel_ktp;
		  $row->KEC_KTP=$rec->kec_ktp;
		  $row->KAB_KTP=$rec->kab_ktp;
		  $row->PRO_KTP=$rec->pro_ktp;
		  $row->NO_ASURANSI=$rec->no_asuransi;
		  
		  $row->EMAIL_PASIEN=$rec->email;
		  $row->HP_PASIEN=$rec->handphone;
		  $row->TLPN_PASIEN=$rec->telepon;
        return $row;
    }

}

class Rowviewrwi
{
        		  public $KEL_KTP;
		  public $KEC_KTP;
		  public $KAB_KTP;
		  public $PRO_KTP;
          public $KD_PASIEN;
          public $NAMA;
          public $NAMA_KELUARGA;
          public $JENIS_KELAMIN;
          public $TEMPAT_LAHIR;
          public $TGL_LAHIR;
          public $AGAMA;
          public $GOL_DARAH;
          public $WNI;
          public $STATUS_MARITA;
          public $ALAMAT;
          public $KD_KELURAHAN;
          public $PENDIDIKAN;
          public $PEKERJAAN;
          public $NAMA_UNIT;
          public $TGL_MASUK;
          public $URUT_MASUK;
          public $KABUPATEN;
          public $KECAMATAN;
          public $PROPINSI;
          public $KD_KABUPATEN;
          public $KD_KECAMATAN;
          public $KD_PROPINSI;
          public $KD_PENDIDIKAN;
          public $KD_PEKERJAAN;
          public $KD_AGAMA;
		  public $KELURAHAN;
		  public $ALAMAT_KTP;
		  public $NAMA_AYAH;
		  public $NAMA_IBU;
		  public $KD_KELURAHAN_KTP;
		  public $KD_POS_KTP;
		  public $KD_POS;
		  public $KD_KELUHAN;
		  public $PROPINSIKTP;
		  public $KABUPATENKTP;
		  public $KECAMATANKTP;
		  public $KELURAHANKTP;
		  Public $EMAIL_PASIEN;
		  Public $HP_PASIEN;
		  Public $TLPN_PASIEN;
			Public $NO_ASURANSI;
			public $anamnese;
}

?>
