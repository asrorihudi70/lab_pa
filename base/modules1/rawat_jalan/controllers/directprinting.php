<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class directprinting extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    public function save($Params = NULL) {
        $mError = "";
        $mError = $this->cetak($Params);
        if ($mError == "sukses") {
            echo '{success: true}';
        } else {
            echo '{success: false}';
        }
    }

    public function cetak($Params) {
        $strError = "";
        $setpage    = new Pilihkertas;
        $logged = $this->session->userdata('user_id');
//        print_r($logged);
        $this->load->model('general/tb_dbrs');
        $query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            $NameRS = $query[0][0]->NAME;
            $Address = $query[0][0]->ADDRESS;
            $TLP = $query[0][0]->PHONE1;
            $Kota = $query[0][0]->CITY;
        } else {
            $NameRS = "";
            $Address = "";
            $TLP = "";
        }
        $no_transaksi = $Params["No_TRans"];
        $Total = $Params["JmlBayar"];
        $Bayar = $Params["JmlDibayar"];
        $medrec = $Params["Medrec"];
        $kdKasir=$Params["KdKasir"];
        $pno_urut=$Params["no_urut"];
        $pkd_produk=$Params["kd_produk"];
		$Data="";
        if ($no_transaksi == '') {
            $queryJum = $this->db->query("select sum(harga) as total from (select harga from detail_transaksi where no_transaksi = '" . $no_transaksi . "') as x")->result();
            foreach ($queryJum as $line) {
                $Data .= str_pad("Total", 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->total, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
            }
        } else {
            
        }
		
        $criteria = "no_transaksi = '" . $no_transaksi . "'";
        $this->load->model('general/tb_cekdetailtransaksi');
        $this->tb_cekdetailtransaksi->db->where($criteria, null, false);
        $query = $this->tb_cekdetailtransaksi->GetRowList(0, 1, "", "", "");
		
		$urutnya=$this->db->query("select no_urut from antrian_poliklinik where kd_pasien='".$medrec."' and kd_unit='".$Params["kdUnit"]."' order by tgl_transaksi, no_urut desc")->row()->no_urut;
		
        if ($query[1] != 0) {
            $Medrec = $query[0][0]->KD_PASIEN;
            $Status = $query[0][0]->STATUS;
            $Dokter = $query[0][0]->DOKTER;
            $Nama = $query[0][0]->NAMA;
            $Alamat = $query[0][0]->ALAMAT;
            $Poli = $query[0][0]->UNIT;
            $Notrans = $query[0][0]->NO_TRANSAKSI;
            $Tgl = $query[0][0]->TGL_TRANS;
            $KdUser = $query[0][0]->KD_USER;
            $uraian = $query[0][0]->DESKRIPSI;
            $jumlahbayar = $query[0][0]->JUMLAH;
        } else {
            //$NoNota = "";
            $Medrec = "";
            $Status = "";
            $Dokter = "";
            $Nama = "";
            $Alamat = "";
            $Poli = " ";
            $Notrans = "";
            $Tgl = "";
        }
        //$printer = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_printer_bill'")->row()->setting;
		$kd_kasir= '01';
		$cek_notabill=$this->db->query("select * from nota_bill where kd_kasir='".$kd_kasir."' order by tgl_cetak desc limit 1")->result();
		//$cek_notabill=$this->db->query("select * from nota_bill where kd_kasir='$kd_kasir' and kd_unit='".$Params["kdUnit"]."'")->result();
        if (count($cek_notabill)==0)
		{
			$no_nota=1;
			$KdUserNota=$this->session->userdata['user_id']['id'];
			$tambahkenotabill=$this->db->query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,jenis,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_nota','".$Params["kdUnit"]."',$KdUserNota,'$Total','','".date('Y-m-d H:i:s')."')");
			// $tambahkenotabill_sql=$this->db->query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_nota','".$Params["kdUnit"]."',$KdUserNota,'$Total','".date('Y-m-d H:i:s')."')");
		}
		else
		{
			
			$kd_unitnya=$Params["kdUnit"];
			$no_nota=count($cek_notabill);
			//$nonota=$this->db->query("select kd_kasir,no_nota,no_transaksi,kd_unit from nota_bill where kd_kasir='$kd_kasir' and no_transaksi='$no_transaksi' and kd_unit='".$Params["kdUnit"]."' and no_nota='$no_nota'")->row();
			 $ceklagi=$this->db->query("select kd_kasir,no_nota,no_transaksi,kd_unit from nota_bill where kd_kasir='$kd_kasir' and no_transaksi='$no_transaksi' and kd_unit='".$Params["kdUnit"]."'")->row();
			if (count($ceklagi)<>0)
			{
				$no_nota=$ceklagi->no_nota;
			}
			else
			{  
				$cekNoNot=$this->db->query("select max(no_nota) as no_nota from nota_bill where kd_kasir='$kd_kasir' --and no_transaksi='$no_transaksi' and kd_unit='".$Params["kdUnit"]."'")->row();
				$no_nota=$cekNoNot->no_nota+1;  
				$KdUserNota=$this->session->userdata['user_id']['id'];
				$tambahkenotabill=$this->db->query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,jenis,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_nota','".$Params["kdUnit"]."',$KdUserNota,'$Total','','".date('Y-m-d H:i:s')."')");
				// $tambahkenotabill_sql=_QMS_Query("insert into nota_bill (kd_kasir,no_transaksi,no_nota,kd_unit,kd_user,jumlah,tgl_cetak)values('$kd_kasir','$no_transaksi','$no_nota','".$Params["kdUnit"]."',$KdUserNota,'$Total','".date('Y-m-d H:i:s')."')");
			}
			
			
		}
		
		//$printer = $Params["printer"];
		$printer = $this->db->query("SELECT p_bill FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_bill;
        if($printer === ''){
        	$printer = $this->db->query("select setting from sys_setting where key_data = 'rad_lab_default_printer_bill'")->row()->setting;
        }else{
        	
        }
        $KdUser = $this->db->query("select setting from sys_setting where key_data = 'KabagKasirRWJ'")->row()->setting;
		//echo $logged[];
		$updatetrx=$this->db->query("update transaksi set tag='$no_nota' where kd_kasir='$kd_kasir' and no_transaksi='$no_transaksi'");
		
        $t1 = 4;
        $t3 = 20;
        $t2 = 60 - ($t3 + $t1);
        $format1 = date('d F Y', strtotime($Tgl));
        $today = date("d F Y");
        $Jam = date("G:i:s");
        $tglSekarang = date('d-M-Y G:i:s');
        $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
        $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
        $handle = fopen($file, 'w');
        $feed=$setpage->PageLength('laporan/2');
        //$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
        $reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
        $formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
        $condensed = Chr(27) . Chr(33) . Chr(4);
        $bold1 = Chr(27) . Chr(69);
        $bold0 = Chr(27) . Chr(70);
        $italicON = Chr(27) . Chr(52); # Italic On
        $italicOFF = Chr(27) . Chr(53); # Italic OFF
        $initialized = chr(27).chr(64);
        $condensed1 = chr(15);
        $condensed0 = chr(18);
        $Data  = $initialized;
        $Data .= $feed;
        $Data .= $condensed1;
        $Data .= $NameRS . "                   " . "Antrian : " . $urutnya . "\n";
        $Data .= $Address . "\n";
        $Data .= "Phone : " . $TLP . "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= "No. Nota   : " . $no_nota . "                " . "No. Trans : " . $Notrans . "\n";
        $Data .= "No. Medrec : " . $Medrec . "        " . "Tgl       : " . $format1 . "\n";
        $Data .= "Status P.  : " . $Status . "\n";
        $Data .= "Dokter     : " . $Dokter . "\n";
        $Data .= "Nama       : " . $Nama . "\n";
        $Data .= "Alamat     : " . $Alamat . "\n";
        $Data .= "Poliklinik Inst. " . $Poli . "\n";
        $Data .= "------------------------------------------------------------\n";
        $Data .= str_pad("No.", $t1, " ") . str_pad("Uraian", $t2, " ") . str_pad("Sub Total", $t3, " ", STR_PAD_LEFT) . "\n";
        $Data .= "------------------------------------------------------------\n";
		//echo json_encode($params);
		$strUrut='';
		for($i=0,$iLen=count($pno_urut);$i<$iLen;$i++){
			if($strUrut!=''){
				$strUrut.=',';
			}
			$strUrut.=$pno_urut[$i];
		}
        $queryDet = $this->db->query("select p.deskripsi, dt.harga,dt.qty, dt.urut,dt.kd_produk from detail_transaksi dt inner join produk p on dt.kd_produk = p.kd_produk where no_transaksi = '" . $no_transaksi . "' and dt.urut in(".$strUrut.") order by deskripsi")->result();
        $no = 0;
        foreach ($queryDet as $line) {
			 $no++;
			$Data .= str_pad($no, $t1, " ") . str_pad($line->deskripsi, $t2, " ") . str_pad($jadi = number_format($line->harga * $line->qty, 0, ',', '.'), $t3, " ", STR_PAD_LEFT) . "\n";
        }
		$tag=array(
			'tag'=>'t'
		);
		$this->db->where(array('no_transaksi'=>$no_transaksi)); 
		$this->db->update('detail_transaksi',$tag); 
		
        $Data .= "------------------------------------------------------------\n";
        $queryJum = $this->db->query("select sum(harga*qty) as total from (select harga,qty from detail_transaksi where no_transaksi = '" . $Notrans . "'  and urut in(".$strUrut.") ) as x")->result();
        foreach ($queryJum as $line) {
            $Data .= str_pad("Total", 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->total, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $queryTotJum = $this->db->query("select uraian,sum(jumlah) as jumlah from detail_tr_bayar db inner join payment p on db.kd_pay = p.kd_pay where no_transaksi = '" . $Notrans . "' and urut in(".$strUrut.")  group by uraian")->result();
        foreach ($queryTotJum as $line) {
            $Data .= str_pad($line->uraian, 40, " ", STR_PAD_LEFT) . str_pad(number_format($line->jumlah, 0, ',', '.'), 20, " ", STR_PAD_LEFT) . "\n";
        }
        $Data .= "\n";
        $Data .= str_pad(" ", 30, " ") . str_pad($Kota . ' , ' . $today, 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= "\n";
        $Data .= str_pad(" ", 30, " ") . str_pad("(---------------------)", 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= str_pad(" ", 40, " ") . str_pad($KdUser, 20, " ", STR_PAD_BOTH) . "\n";
        $Data .= "\n";
        $Data .= str_pad("Jam : " . $Jam, 30, " ") . str_pad("Operator : " . $logged['username'], 30, " ", STR_PAD_LEFT) . "\n";
        $Data .= $formfeed;
        fwrite($handle, $Data);
        fclose($handle);
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			copy($file, $printer);  # Lakukan cetak
			unlink($file);
			# printer windows -> nama "printer" di komputer yang sharing printer
		} else{
			shell_exec("lpr -P " . $printer . " " . $file); # Lakukan cetak linux
		}
        

        $error = "sukses";
        return $error;
    }

}
