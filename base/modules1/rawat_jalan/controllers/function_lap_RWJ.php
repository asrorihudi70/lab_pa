<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class function_lap_RWJ extends  MX_Controller {		
	private $cw_medrec      = 10;
	private $cw_no_transaksi= 15;
	private $cw_nama_pasien = 25;
	private $cw_tanggal 	= 15;
	private $cw_nominal 	= 20;

	private $jam;
	private $dbSQL;
	private $kd_kasir;
	private $id_user;
    public $ErrLoginMsg='';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
		$this->load->library('result');
        $this->load->library('common');
		$this->load->model('Tbl_data_transaksi');
		$this->jam      = date("H:i:s");
		$this->dbSQL    = $this->load->database('otherdb2',TRUE);
		$this->kd_kasir = '02';
		$this->id_user 	= $this->session->userdata['user_id']['id'];
    }
	 
	public function index()
    {
        
		$this->load->view('main/index');

	}

	function cetak_lap_reg_detail(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        $Split = explode("##@@##", $getcriteria,  5);
		$tglsum = $Split[0];
        $tglsummax = $Split[1];
        $isikriteria=$Split[2];
		if($isikriteria==="ruang")
		{
			$feildkriteria="";
			$criteria="";
			$tmpunit = explode(',', $Split[3]);
			for($i=0;$i<count($tmpunit);$i++)
			{
				$criteria .= "'".$tmpunit[$i]."',";
			}
			$feildkriteria="sp_u.kd_unit";
			$criteria = substr($criteria, 0, -1);

		}

		if($isikriteria==="kelas"){
			$feildkriteria="";
			$criteria="";
			$tmpunit = explode(',', $Split[3]);
			for($i=0;$i<count($tmpunit);$i++)
			{
				$criteria .= "'".$tmpunit[$i]."',";
			}
			$feildkriteria="sp_u.kd_kelas";
			$criteria = substr($criteria, 0, -1);
		}
		
		if($isikriteria==="spesialis")
		{
			$feildkriteria="";
			$criteria="";
			$tmpunit = explode(',', $Split[3]);
			for($i=0;$i<count($tmpunit);$i++)
			{
				$criteria .= "'".$tmpunit[$i]."',";
			}
			$feildkriteria="sp_u.kd_spesial";
			$criteria = substr($criteria, 0, -1);
		}
		
		$pasientype=$Split[4];
		$jenis_pasien='';
		if($pasientype=='kabeh')
		{
			$pasientype="";
			$jenis_pasien ='Semua Pasien';
		}else{
			$pasientype=" and k.Baru='".$Split[4]."'";
			if($Split[4]=='true'){
				$jenis_pasien ='Pasien Baru';
			}else if($Split[4]=='false'){
				$jenis_pasien ='Pasien Lama';
			}
		}

		$no = 0;
		if($isikriteria==="ruang")
		{
			$fieldkriteriadet="sp.kd_unit";
			$groupingby ='u.nama_unit';
		}
		
		if($isikriteria==="spesialis")
		{
			$fieldkriteriadet="ng.kd_spesial";
			$groupingby ='s.spesialisasi';
		}
		
		if($isikriteria==="kelas")
		{
			$fieldkriteriadet="sp.kd_kelas";
			$groupingby ='kl.kelas';
		}
		
		$query = $this->db->query("SELECT distinct spesialis2 from (Select  $groupingby as spesialis2, 
										ps.KD_Pasien as kdpasien,
										ps.Nama as namapasien,
										case when ps.Jenis_Kelamin=true then 'L' else 'P' end as jk,
										ps.Tgl_Lahir,
										ps.Alamat as alamatpas,
										pk.Pekerjaan as pekerjaan,
										k.Tgl_Masuk as tglmas,
										k.Jam_Masuk as jammas,
										c.Customer as customer,
										s.spesialisasi as spesialis,
										z.user_names as username
									From (pasien ps
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)
										inner join  Kunjungan k   on k.Kd_Pasien = ps.Kd_Pasien
										INNER JOIN Customer c on c.Kd_Customer=k.Kd_Customer
										inner join transaksi t on t.kd_pasien = k.kd_pasien and  k.tgl_masuk = t.tgl_transaksi AND k.Kd_Unit=t.Kd_Unit AND k.Urut_Masuk=t.Urut_Masuk
										inner  join nginap ng on ng.kd_pasien = k.kd_pasien and ng.tgl_masuk = k.tgl_masuk AND k.Kd_Unit=ng.Kd_Unit_kamar AND k.Urut_masuk=ng.Urut_masuk
										inner join spesialisasi s on s.kd_spesial = ng.kd_spesial
										inner join spc_unit sp ON sp.kd_unit = k.kd_unit 
										inner join unit u on u.kd_unit=sp.kd_unit
										inner join kelas

										kl ON kl.kd_kelas = sp.kd_kelas inner join Zusers z on t.kd_user = z.kd_user
										inner join kontraktor ktr ON c.kd_customer = ktr.kd_customer
										where $fieldkriteriadet in ($criteria) and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $pasientype
										group by  ps.Kd_Pasien,ps.Nama ,ps.Alamat, ps.jenis_kelamin,
										ps.Tgl_Lahir,k.Tgl_Masuk, pk.pekerjaan,
										k.Jam_masuk , k.Tgl_masuk, k.Urut_masuk, k.Kd_Unit, k.KD_Pasien,$groupingby, c.Customer,s.spesialisasi,z.user_names 
										Order By  $groupingby) x order by spesialis2
										")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='
				<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="13">Laporan Registrasi Detail Rawat Inap</th>
						</tr>
						<tr>
							<th  colspan="13">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
						<tr>
							<th  colspan="13">'.$jenis_pasien.'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1">
					<thead>
					  <tr>
							<th align="center" width="100">Unit</th>
							<th align="center" width="24" >No. </th>	 
							<th align="center" width="80" >No. Medrec</th>
							<th align="center" width="210">Nama Pasien</th>
							<th align="center" width="220" >Alamat</th>
							<th align="center" width="26">JK</th>
							<th align="center" width="150" >Umur</th>
							<th align="center" width="82" >Pekerjaan</th>
							<th align="center" width="68" >Tanggal Masuk</th>
							<th align="center" width="30" >Jam Masuk</th>
							<th align="center" width="100">Spesialisasi</th>
							<th align="center" width="63" >Customer</th>
							<th align="center" width="63" >User</th>
					  </tr>
					</thead>';

		$baris=0;
		if(count($query)>0){
			
			foreach($query as $line){
				$spesialis= $line->spesialis2;
				$html.= '<tr><td>'.$spesialis.'</td><td colspan="12"></td></tr>';
				$query_sub = $this->db->query("SELECT * from (Select  $groupingby as spesialis2, 
												ps.KD_Pasien as kdpasien,
												ps.Nama as namapasien,
												case when ps.Jenis_Kelamin=true then 'L' else 'P' end as jk,
												ps.Tgl_Lahir,
												ps.Alamat as alamatpas,
												pk.Pekerjaan as pekerjaan,
												k.Tgl_Masuk as tglmas,
												k.Jam_Masuk as jammas,
												c.Customer as customer,
												s.spesialisasi as spesialis,
												z.user_names as username
											From (pasien ps
												left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)
												inner join  Kunjungan k   on k.Kd_Pasien = ps.Kd_Pasien
												INNER JOIN Customer c on c.Kd_Customer=k.Kd_Customer
												inner join transaksi t on t.kd_pasien = k.kd_pasien and  k.tgl_masuk = t.tgl_transaksi AND k.Kd_Unit=t.Kd_Unit AND k.Urut_Masuk=t.Urut_Masuk
												inner  join nginap ng on ng.kd_pasien = k.kd_pasien and ng.tgl_masuk = k.tgl_masuk AND k.Kd_Unit=ng.Kd_Unit_kamar AND k.Urut_masuk=ng.Urut_masuk
												inner join spesialisasi s on s.kd_spesial = ng.kd_spesial
												inner join spc_unit sp ON sp.kd_unit = k.kd_unit 
												inner join unit u on u.kd_unit=sp.kd_unit
												inner join kelas

												kl ON kl.kd_kelas = sp.kd_kelas inner join Zusers z on t.kd_user = z.kd_user
												inner join kontraktor ktr ON c.kd_customer = ktr.kd_customer
												where $fieldkriteriadet in ($criteria) and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $pasientype
												group by  ps.Kd_Pasien,ps.Nama ,ps.Alamat, ps.jenis_kelamin,
												ps.Tgl_Lahir,k.Tgl_Masuk, pk.pekerjaan,
												k.Jam_masuk , k.Tgl_masuk, k.Urut_masuk, k.Kd_Unit, k.KD_Pasien,$groupingby, c.Customer,s.spesialisasi,z.user_names 
												Order By  $groupingby) x where spesialis2='$spesialis'
												")->result();
				// echo '{success:true, totalrecords:'.count($query_sub).', listData:'.json_encode($query_sub).'}';
				$no=1;
				$baris++;
				foreach($query_sub as $line2){
				
					$tgl_lahir = new DateTime ($line2->tgl_lahir);
					$today = new DateTime($line2->tglmas);
					$diff = $today->diff($tgl_lahir);
					$umurAr=$diff->y;
					$html.='<tr>
								<td></td>
								<td align="center">'.$no.'</td>
								<td width="50" align="left">'.$line2->kdpasien.'</td>
								<td width="50" align="left">'.$line2->namapasien.'</td>
								<td width="50" align="left">'.$line2->alamatpas.'</td>
								<td width="50" align="center">'.$line2->jk.'</td>
								<td width="50" align="center">'.$umurAr.' th </td>
								<td width="50" align="left">'.$line2->pekerjaan.'</td>
								<td width="50" align="center">'.  date('d-M-Y', strtotime($line2->tglmas)).'</td>
								<td width="50" align="left">'.date('h:m', strtotime($line2->jammas)).'</td>
								<td width="50" align="left">'.$line2->spesialis.'</td>
								<td width="50" align="left">'.$line2->customer.'</td>
								<td width="50" align="left">'.$line2->username.'</td>
							</tr>';
					$no++;
					$baris++;
				}
			}
			
		}else{
			$html.='<tr><td colspan="13" align="center"> Data Tidak Ada</td></tr>';
			$baris++;
		}
		
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:M'.$baris;
		$area_wrap='A6:M'.$baris;
		$area_kanan='C8:M'.$baris;
		if($type_file == 1){
			
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:M7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:M4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:M6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('M')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()
						->getStyle('M7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LapRegistrasiDetailRawatInap.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L',' Laporan Registrasi Detail Rawat Inap',$html);
		
		}
		
	}
	
	function cetak_lap_reg_summary(){
		$html='';
		
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        //
		$Split = explode("##@@##", $getcriteria,  4);
		$tglsum = $Split[0];
		$tglsummax = $Split[1];
       
		$criteria="";
		$tmpunit = explode(',', $Split[3]);
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "'".$tmpunit[$i]."',";
		}
		
		$feildkriteria="sp_u.kd_unit";
		$criteria = substr($criteria, 0, -1);
		$pasientype = $Split[2];
		$jenis_pasien='';
		if($pasientype=='kabeh')
		{
			$pasientype="";
			$jenis_pasien ='Semua Pasien';
		}
		else{
			$pasientype=" and k.Baru='".$Split[2]."'";
			if($Split[2]=='true'){
				$jenis_pasien ='Pasien Baru';
			}else if($Split[2]=='false'){
				$jenis_pasien ='Pasien Lama';
			}
		}
		$q = $this->db->query(" SELECT x.Nama_Unit as namaunit2,  Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
								Sum(PHB)as phb, Sum(nPHB) as nphb,  
								sum(pbi) as pbi, 
								sum(non_pbi) as non_pbi, 
								sum(jamkesda) as jamkesda, 
								sum(lain_lain) as lain_lain,  
								sum(umum) as umum ,x.Kd_Unit as kdunit
								From (
									Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
									Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
									Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
									Case When k.Baru=true Then 1 else 0 end as Br,
									Case When k.Baru=false Then 1 else 0 end as Lm,
									Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
									Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
									case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
									case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
									case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
									case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
									case when ktr.jenis_cust   =0 then 1 else 0 end as umum
									From Unit u
									INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
									Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
									LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='1'   and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in( $criteria) $pasientype
								) x Group By x.kd_unit,
								x.Nama_Unit  Order By x.kd_unit");
		
		if($param->type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="10">Laporan Summary Pasien Rawat Inap</th>
						</tr>
						<tr>
							<th  colspan="10">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
						<tr>
							<th  colspan="10">'.$jenis_pasien.'</th>
						</tr>
				</table><br>';
		$html.='<table class="t1" border = "1" cellpadding="3">
				<thead>
				<tr>
					<th align="center" width="40" rowspan="2">No. </th>
					<th align="center"  width="100" rowspan="2">Nama Unit </th>
					<th align="center"  width="60" rowspan="2">Jumlah Pasien</th>
					<th align="center" colspan="2">Jenis kelamin</th>
					<th align="center" colspan="2">Kunjungan</th>
					<th align ="center" width="68" colspan="4">Asuransi</th>
					<th align ="center" width="68" rowspan="2">Umum</th>
				  </tr>
				 <tr>    
					<th align="center" >L</th>
					<th align="center" >P</th>
					<th align="center" >Baru</th>
					<th align="center" >Lama</th>
					<th align="center" width="50">BPJS NON PBI</th>
					<th align="center" width="50">BPJS PBI</th>
					<th align="center" width="50">TM/ PM JAMKESDA</th>
					<th align="center" width="50">LAIN-LAIN</th>
				 </tr>  
				</thead>';
		$total_pasien     = 0;
		$total_lk         = 0;
		$total_pr         = 0;
		$total_br         = 0;
		$total_lm         = 0;
		$total_perusahaan = 0;
		$total_non_pbi    = 0;
		$total_pbi        = 0;
		$total_jamkesda   = 0;
		$total_lain       = 0;
		$total_umum       = 0;
		$baris=0;
		
		if($q->num_rows == 0)
        {
           $html.='<tr><td colspan="10" align="center"> Data Tidak Ada</td></tr>';
		   $baris++;
        } else {
			$query = $q->result();
		    $baris++;
			$no = 0;
				foreach ($query as $line) 
				{
				   $no++;
				   $html.='<tr class="headerrow"> 
							<td align="right">'.$no.'</td>

							<td align="left">'.$line->namaunit2.'</td>
							<td align="right">'.$line->jumlahpasien.'</td>
							<td align="right">'.$line->lk.'</td>
							<td align="right">'.$line->pr.'</td>
							<td align="right">'.$line->br.'</td>
							<td align="right">'.$line->lm.'</td>
							<td align="right">'.$line->non_pbi.'</td>
							<td align="right">'.$line->pbi.'</td>
							<td align="right">'.$line->jamkesda.'</td>
							<td align="right">'.$line->lain_lain.'</td>
							<td align="right">'.$line->umum.'</td>
							</tr>';
					$total_pasien     += $line->jumlahpasien;
					$total_lk         += $line->lk;
					$total_pr         += $line->pr;
					$total_br         += $line->br;
					$total_lm         += $line->lm;
					$total_non_pbi    += $line->non_pbi;
					$total_pbi        += $line->pbi;
					$total_jamkesda   += $line->jamkesda;
					$total_lain       += $line->lain_lain;
					$total_umum       += $line->umum;
					$baris++;
				}
				$html.='
				<tr>
					<td colspan="2" align="right"><b> Jumlah</b></td>
					<td align="right"><b>'.$total_pasien.'</b></td>
					<td align="right"><b>'.$total_lk.'</b></td>
					<td align="right"><b>'.$total_pr.'</b></td>
					<td align="right"><b>'.$total_br.'</b></td>
					<td align="right"><b>'.$total_lm.'</b></td>
					<td align="right">'.$total_non_pbi.'</td>
					<td align="right">'.$total_pbi.'</td>
					<td align="right">'.$total_jamkesda.'</td>
					<td align="right">'.$total_lain.'</td>
					<td align="right"><b>'.$total_umum.'</b></td>
				</tr>';
				/* $queryjumlah= $this->db->query("Select   Count(X.KD_Pasien) as jumlahpasien , Sum(lk) as lk, Sum(pr) as pr, Sum(br) as br, Sum(Lm)as lm, 
											Sum(PHB)as phb, Sum(nPHB) as nphb,  sum(PERUSAHAAN)as perusahaan,  sum(ASKES) as askes, sum(umum) as umum 
											From (
													Select U.Nama_Unit, u.Kd_Unit, ps.kd_Pasien, 
													Case When ps.Jenis_Kelamin=true Then 1 else 0 end as Lk,
													Case When ps.Jenis_Kelamin=false Then 1 else 0 end as Pr, 
													Case When k.Baru=true Then 1 else 0 end as Br,
													Case When k.Baru=false Then 1 else 0 end as Lm,
													Case When k.kd_Customer='0000000001' Then 1 Else 0 end as PHB,  
													Case When k.kd_Customer<>'0000000001' Then 1 Else 0 end as nPHB, 
													case when ktr.jenis_cust=1 then 1 else 0 end as PERUSAHAAN
													case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000043' then 1 else 0 end as pbi,
													case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000044' then 1 else 0 end as non_pbi, 
													case when ktr.jenis_cust   = 2 and k.kd_customer = '0000000008' then 1 else 0 end as jamkesda, 
													case when ktr.jenis_cust   = 2 and k.kd_customer not in ('0000000043', '0000000044', '0000000008') then 1 else 0 end as lain_lain, 
													From Unit u
													INNER JOIN Kunjungan k on k.kd_unit=U.kd_unit   
													Inner Join Pasien ps on k.Kd_Pasien = ps.Kd_Pasien 
													LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  where u.kd_bagian='1' and k.tgl_masuk between  '".$Split[0]."' and  '".$Split[1]."' and u.kd_unit in($criteria)
											 ) x")->result();
				foreach ($queryjumlah as $line) 
				{
					$baris++;
					$html.='<tr>
										<th align="right"> </th>
										<th align="right"> Jumlah </th>
										<th align="right">'.number_format($line->jumlahpasien,0,',','.').'</th>
										<th align="right">'.number_format($line->lk,0,',','.').'</th>
									   <th align="right">'.number_format($line->pr,0,',','.').'</th>
									   <th align="right">'.number_format($line->br,0,',','.').'</th>
									   <th align="right">'.number_format($line->lm,0,',','.').'</th>
									   <th align="right">'.number_format($line->perusahaan,0,',','.').'</th>
									   <th align="right">'.number_format($line->askes,0,',','.').'</th>
									   <th align="right">'.number_format($line->umum,0,',','.').'</th>
								</tr>';
				} */   
		}
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:J'.$baris;
		$area_wrap='A6:J'.$baris;
		$area_kanan='C8:J'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:M7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:J4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:J7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Registrasi_Summary_Rawat_Inap.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P',' Laporan Registrasi Summary Rawat Inap',$html);
		}
	}
	
	function cetak_lap_pasien_rwi(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        $Split = explode("##@@##", $getcriteria,  11);
		//print_r($Split); 
		
		if(count($Split) > 0){
			$orderby= $Split[1];
			$unitruang= $Split[3];
			$kdUnit= $Split[4];
			$kamar= $Split[6];
			$noKamar= $Split[7];
			$kelompokPasien= $Split[8];
			$customer = $Split[9];
			$kdCustomer = $Split[10];
			
			if($customer=='NULL'){
				$customer='Semua';
			} else{
				$customer=$customer;
			}
			
			if($orderby == 'No Medrec'){
				$orderby='no_medrec';
			} else if($orderby == 'Nama'){
				$orderby='nama';
			}else if($orderby == 'Tanggal Masuk'){
				$orderby='tgl_masuk';
			} else {
				$orderby='nama_kamar';
			}
			
			if($unitruang == 'Semua' and $kamar == 'SEMUA' and $kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kamar == 'SEMUA'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and i.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA' and $kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							ORDER BY ".$orderby."";
				
			} else if($unitruang == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and i.no_kamar='".$noKamar."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA'){
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($kelompokPasien=='Semua'){
				$param="WHERE Parent in ('1001','1002','1003')
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and i.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else {
				$param="WHERE Parent in ('1001','1002','1003'))
							AND t.Tgl_CO IS NULL
							and  i.kd_unit='".$kdUnit."'
							and i.no_kamar='".$noKamar."'
							and K.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			}
		}
		
		$kd_kasir = $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$queryHasil = $this->db->query( " SELECT p.Kd_Pasien as No_medrec, p.Nama, case when p.Alamat='' then '' else p.Alamat end,  
												p.Nama_Keluarga, case when p.Nama_Keluarga='' then '' else p.Nama_Keluarga end,  
												t.Tgl_Transaksi as Tgl_Masuk, i.No_Kamar, kmr.
												Nama_Kamar  
											FROM (((Pasien_Inap i 
												INNER JOIN (Transaksi t 
												INNER JOIN (Kunjungan k 
												INNER JOIN Customer c ON k.kd_customer = c.kd_customer)  ON t.kd_pasien = k.kd_pasien 
													AND t.kd_unit = k.kd_unit AND t.tgl_transaksi = k.tgl_masuk 
													AND t.urut_masuk = k.urut_masuk)  ON i.Kd_Kasir = t.Kd_Kasir
													AND i.No_Transaksi = t.No_Transaksi) 
												INNER JOIN Pasien p ON t.Kd_Pasien = p.Kd_Pasien) 
												INNER JOIN Kamar kmr ON i.Kd_Unit = kmr.Kd_Unit AND i.No_Kamar = kmr.No_Kamar ) 
												inner join kontraktor ktr on k.kd_customer = ktr.kd_customer 
											WHERE 
												t.Kd_Kasir = '".$kd_kasir."'
												AND 
												i.Kd_Unit IN (SELECT Kd_Unit FROM Unit ".$param."
											");
											
		$query = $queryHasil->result();
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="7">PASIEN RAWAT INAP (RWI)</th>
						</tr>
						<tr>
							<th  colspan="7">'.date("d M Y").' </th>
						</tr>
						<tr>
							<th  colspan="7">Kelompok Pasien : '.$kelompokPasien.' ( '.$customer.' )</th>
						</tr>
				</table><br>';
			
			//-------------------------MENGATUR TAMPILAN TABEL------------------------------------------------
			$html.='
					<table class="t1" border = "1" cellpadding="3">
					<thead>
					  <tr>
						<th>No</th>
						<th  align="center">No Medrec</th>
						<th >Nama Pasien</th>
						<th >Alamat</th>
						<th >Nama Keluarga</th>
						<th >Tgl. Masuk</th>
						<th >Kamar</th>
					  </tr>
					</thead>';
		$baris=0;
		if(count($query) == 0)
        {
            $html.='<tr><td colspan="7" align="center" >Data Tidak Ada</td></tr>';
			$baris++;
        }else {									
			
			$no = 0;
			
			foreach ($query as $line) 
			{
				$noMedrec=$line->no_medrec;
				$nama=$line->nama;
				$alamat=$line->alamat;
				$namaKeluarga=$line->nama_keluarga;
				$namaKamar=$line->nama_kamar;
				
				$tmptglmasuk=substr($line->tgl_masuk, 0, 10);
				$tmptglmasuk=date('d-M-Y',strtotime($tmptglmasuk));
				
				$no++;            
				$html.='<tr > 
							<td align="center">'.$no.'</td>
							<td >'.$noMedrec.'</td>
							<td >'.$nama.'</td>
							<td >'.$alamat.'</td>
							<td >'.$namaKeluarga.'</td>
							<td >'.$tmptglmasuk.'</td>
							<td >'.$namaKamar.'</td>
						</tr>';
				$baris++;
			}
			
		}			 
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:G'.$baris;
		$area_wrap='A6:G'.$baris;
		if($type_file == 1){
			
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:G6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Pasien_Rawat_Inap.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P',' Laporan Pasien Rawat Inap',$html);
		
		}
	}
	
	function cetak_lap_pasien_pulang_rwi(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
	
        $Split = explode("##@@##", $getcriteria,  15);
		
		ini_set('memory_limit', '-1');
		if(count($Split) > 0){
			$tglAwal = $Split[1];
			$tglAkhir = $Split[2];
			$orderby= $Split[4];
			$unitruang= $Split[6];
			$kdUnit= $Split[7];
			$kamar= $Split[9];
			$noKamar= $Split[10];
			$kelompokPasien= $Split[11];
			$customer = $Split[12];
			$kdCustomer = $Split[13];
			
			if($customer=='NULL'){
				$customer='Semua';
			} else{
				$customer=$customer;
			}
			
			if($orderby == 'No Medrec'){
				$orderby='no_medrec';
			} else if($orderby == 'Nama'){
				$orderby='nama';
			}else if($orderby == 'Tanggal Masuk'){
				$orderby='tgl_masuk';
			} else {
				$orderby='nama_kamar';
			}
			
			
			if($unitruang == 'Semua' and $kamar == 'SEMUA' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							ORDER BY ".$orderby."";
				
			} else if($unitruang == 'Semua' and $kamar == 'SEMUA'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)	
							and g.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'SEMUA' and $kelompokPasien == 'Semua' ){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else if($unitruang == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'	
							and g.no_kamar='".$noKamar."'
							ORDER BY ".$orderby."";
			} else if($kamar == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else if($kelompokPasien == 'Semua'){
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)	
							and g.no_kamar='".$noKamar."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			} else{
				$param="WHERE Parent in ('1001','1002','1003'))			
							AND g.Tgl_Keluar BETWEEN '".$tglAwal."' AND '".$tglAkhir."'
							AND g.Akhir = 't'
							and p.kd_pasien not in (select kd_pasien from pasien_perinatal)
							and Kj.kd_Customer='".$kdCustomer."'	
							and g.no_kamar='".$noKamar."'
							and g.Kd_Unit_Kamar='".$kdUnit."'
							ORDER BY ".$orderby."";
			}
			
		}
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="10">LAPORAN PASIEN PULANG ('.$unitruang.')</th>
						</tr>
						<tr>
							<th  colspan="10">Tanggal '.$tglAwal.' s/d '.$tglAkhir.'</th>
						</tr>
						<tr>
							<th  colspan="10">Kelompok Pasien '.$kelompokPasien.' ( '.$customer.' )</th>
						</tr>
				</table><br>';
		$html.='<table   border = "1" cellpadding="3">
				<thead>
				  <tr>
					<th>No</th>
					<th align="center">No Medrec</th>
					<th>Nama Pasien</th>
					<th>Alamat</th>
					<th>Telepon</th>
					<th>Nama Keluarga</th>
					<th>Tgl. Masuk</th>
					<th>Tgl. Keluar</th>
					<th>Lama (Hari)</th>
					<th>Kamar</th>
				  </tr>
				</thead>';
				
		$queryHasil = $this->db->query( " SELECT p.Kd_Pasien as No_Medrec , p.Nama, p.Alamat, p.Telepon, 
											Nama_keluarga, case when p.Nama_Keluarga = '' then '' else p.Nama_Keluarga end, 
											g.TGL_MASUK , g.Tgl_Keluar, g.No_Kamar, k.Nama_Kamar , g.Tgl_Keluar-g.TGL_MASUK as lama, 
											g.kd_unit_kamar, c.customer
										  FROM ((Nginap g
											INNER JOIN Transaksi t ON t.Kd_Pasien = g.Kd_Pasien 
												AND t.Kd_Unit = g.Kd_Unit 
												AND t.Urut_Masuk = g.Urut_Masuk 
												AND t.Tgl_Transaksi = g.Tgl_Masuk 
												AND t.Tgl_CO = g.Tgl_Keluar 
											INNER JOIN KUNJUNGAN kj ON t.kd_pasien = kj.kd_pasien 
												AND t.kd_unit = kj.kd_unit 
												AND t.Tgl_Transaksi = kj.TGL_MASUK 
												And t.URUT_MASUK = kj.URUT_MASUK 
											inner join CUSTOMER c on kj.kd_customer=c.kd_customer
											INNER JOIN Pasien p ON g.Kd_Pasien = p.Kd_Pasien) ) 
											INNER JOIN KAMAR k ON g.Kd_Unit_Kamar = k.Kd_Unit 
												AND g.No_Kamar = k.No_Kamar 
											inner join kontraktor ktr on kj.kd_customer = ktr.kd_customer 
												AND g.Akhir = 't' 
												AND g.Kd_Unit_Kamar IN (SELECT Kd_Unit FROM Unit ".$param."
											");
		$baris=0;									
		$query = $queryHasil->result();
		if(count($query) == 0)
        {
           $html.="<tr><td colspan='10' align='center'> Data Tidak Ada</td></tr>";
		   $baris++;
        }
        else {		
			$no = 0;
			
				foreach ($query as $line) 
				{
					$noMedrec=$line->no_medrec;
					$nama=$line->nama;
					$alamat=$line->alamat;
					$telepon=$line->telepon;
					$namaKeluarga=$line->nama_keluarga;
					$lama=$line->lama;
					$namaKamar=$line->nama_kamar;
					
					//"2015-05-13 00:00:00"
					$tmptglmasuk=substr($line->tgl_masuk, 0, 10);
					$tmptglkeluar=substr($line->tgl_keluar, 0, 10);
					
					 //$tmpumur=substr(, 0, 2);date('d-M-Y',strtotime($date1 . "+1 days"));
					$tmptglmasuk=date('d-M-Y',strtotime($tmptglmasuk));
					$tmptglkeluar=date('d-M-Y',strtotime($tmptglkeluar));
					
					//"6 days"			
					if($tmptglmasuk == $tmptglkeluar){
						$lamanginap= '1';
					} else{
						$pisahhari = explode(" ", $lama,  2);
						$lamanginap=$pisahhari[0];
					}
					
					$no++;            
					$html.='
						<tr class="headerrow"> 
							<td align="center">'.$no.'</td>
							<td>'.$noMedrec.'</td>
							<td >'.$nama.'</td>
							<td >'.$alamat.'</td>
							<td >'.$telepon.'</td>
							<td >'.$namaKeluarga.'</td>
							<td >'.$tmptglmasuk.'</td>
							<td >'.$tmptglkeluar.'</td>
							<td align="center">'.$lamanginap.'</td>
							<td >'.$namaKamar.'</td>
						</tr>
					';
					$baris++;
				}	
		}
		$html.="</table>";		
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:J'.$baris;
		$area_wrap='A6:J'.$baris;
		// $area_kanan='C8:J'.$baris;
		if($type_file == 1){
			
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:J6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:J4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:J6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Pasien_Pulang.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P',' Laporan Pasien Pulang',$html);
		
		}
	}
	function cetak_lap_keluar_masuk(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
		$jenis = $param->jenis;
	
	
        $Split = explode("##@@##", $getcriteria,  4);
		if (count($Split) > 0 )
		{
			$tmptmbahParam = $Split[0];
			$tmptgl = $Split[1];
			if($tmptgl === 'Tanggal')
			{
				$tgl1 = $Split[2];
				$tgl2 = $Split[3];
				if ($tgl1 === $tgl2)
				{
					$kriteria = "Periode ".$tgl1."";
				}else{
					$kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
				}
				$tmptmbahParamtanggal = " K.Tgl_Masuk >= ('".$tgl1."') and K.Tgl_Masuk <= ('".$tgl2."') ";
			}else{   
				$tgl1 = $Split[2];
				$tgl2 = $Split[3];   

				$tmptgl1 = explode("-",$tgl1);
				$bln1 = $tmptgl1[0];
				$thn1 = $tmptgl1[1];
				$tmptgl2 = explode("-",$tgl1);
				$bln2 = $tmptgl2[0];
				$thn2 = $tmptgl2[1];

				switch ($bln1) {
					case '01': $textbln = 'Januari';
						break;
					case '02': $textbln = 'Febuari';
						break;
					case '03': $textbln = 'Maret';
						break;
					case '04': $textbln = 'April';
						break;
					case '05': $textbln = 'Mei';
						break;
					case '06': $textbln = 'Juni';
						break;
					case '07': $textbln = 'Juli';
						break;
					case '08': $textbln = 'Agustus';
						break;
					case '09': $textbln = 'September';
						break;
					case '10': $textbln = 'Oktober';
						break;
					case '11': $textbln = 'November';
						break;
					case '12': $textbln = 'Desember';
				}

				switch ($bln2) {
					case '01': $textbln2 = 'Januari';
						break;
					case '02': $textbln2 = 'Febuari';
						break;
					case '03': $textbln2 = 'Maret';
						break;
					case '04': $textbln2 = 'April';
						break;
					case '05': $textbln2 = 'Mei';
						break;
					case '06': $textbln2 = 'Juni';
						break;
					case '07': $textbln2 = 'Juli';
						break;
					case '08': $textbln2 = 'Agustus';
						break;
					case '09': $textbln2 = 'September';
						break;
					case '10': $textbln2 = 'Oktober';
						break;
					case '11': $textbln2 = 'November';
						break;
					case '12': $textbln2 = 'Desember';
				}
				
				if ($tgl1 === $tgl2)
				{
				   $kriteria = "Periode ".$textbln." ".$thn1."";
				}else
				{
					$kriteria = "Periode ".$textbln." ".$thn1." s/d ".$textbln2." ".$thn2."";
				}
					
				$tmptmbahParamtanggal = " date_part('Year',K.Tgl_Masuk) >= ".$thn1."  and date_part('month',K.Tgl_Masuk) >= ".$bln1."  and date_part('Year',K.Tgl_Masuk) <= ".$thn2."   
											  and date_part('month',K.Tgl_Masuk) <= ".$bln2."   ";
			}
										  
			$Param = "Where ".$tmptmbahParamtanggal." and left(K.kd_unit,1)='1' and to_char(nginap.tgl_inap, 'dd-MM-YYYY') = to_char(k.tgl_masuk, 'dd-MM-YYYY')"
					. " ".$tmptmbahParam." ORDER BY p.nama";
        }
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					
						<tr>
							<th style='.$font_style.' colspan="14">Daftar Pasien Masuk/Keluar Rawat Inap</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="14">'.$kriteria.'</th>
						</tr>
						<tr>
							<th  colspan="14">'.$jenis.'</th>
						</tr>
					
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="3">
				<thead>
					<tr>
						<th rowspan="2" align="center"><strong>No</strong></th>
						<th rowspan="2" align="center"><strong>No. medrec</strong></th>
						<th rowspan="2" align="center"><strong>Nama Pasien</strong></th>
						<th rowspan="2" align="center"><strong>Alamat</strong></th>
						<th rowspan="2" align="center"><strong>JK</strong></th>
						<th rowspan="2" align="center"><strong>Umur</strong></th>
						<th rowspan="2" align="center"><strong>Kelas</strong></th>
						<th rowspan="2" align="center"><strong>Ruang</strong></th>
						<th rowspan="2" align="center"><strong>Unit</strong></th>
						<th height="23" colspan="2" align="center"><strong>Masuk</strong></th>
						<th colspan="2" align="center"><strong>Keluar</strong></th>
						<th rowspan="2" align="center"><strong>Lama Rawat</strong></th>
					</tr>
					<tr>
						<th height="23" align="center"><strong>Tanggal</strong></th>
						<th align="center"><strong>Jam</strong></th>
						<th align="center"><strong>Tanggal</strong></th>
						<th align="center"><strong>Jam</strong></th>
					</tr>
				</thead>
				';
				
		$tmpquery = "Select P.Kd_Pasien, P.Nama, P.Alamat, P.tgl_lahir,
					CASE WHEN P.Jenis_Kelamin = 't' then 'L' else 'P' End as JK,
					case when age(K.Tgl_Masuk,P.Tgl_Lahir)='00:00:00' then '1' else age(K.Tgl_Masuk,P.Tgl_Lahir) end as JmlUmur, 
					Spesialisasi.spesialisasi, Kelas.Kelas, kamar.nama_kamar, 
					to_char(k.tgl_masuk, 'dd-MM-YYYY') as tgl_masuk, 
					to_char(k.jam_masuk, 'HH:ss') as Jam_Masuk, 
					to_char(k.tgl_keluar, 'dd-MM-YYYY') as tgl_keluar, 
					to_char(k.jam_keluar, 'HH:ss') as Jam_Keluar
						From pasien p inner join ((kunjungan k inner join 
													(unit u inner join kelas on u.kd_kelas=kelas.kd_kelas) on u.kd_unit=k.kd_unit) 
													INNER JOIN (((Select * from NGINAP Where Akhir='t') nginap inner join spesialisasi on nginap.kd_spesial=spesialisasi.kd_spesial)  
													inner join kamar on nginap.kd_unit_kamar=kamar.kd_unit and nginap.no_kamar=kamar.no_kamar) 
												ON k.KD_Pasien = NGINAP.KD_Pasien AND K.KD_UNIT=NGINAP.KD_UNIT AND K.TGL_MASUK=NGINAP.TGL_MASUK AND K.URUT_MASUK=NGINAP.URUT_MASUK) 
													on p.kd_pasien=k.kd_pasien  ".$Param;
		$q = $this->db->query($tmpquery);
		$baris=0;
		if($q->num_rows == 0)
        {
            $html.="<tr><td colspan='14' align='center'> Data Tidak Ada</td></tr>";
			$baris++;
        }else {
        
			$query = $q->result();
			$no = 0;
			$baris++;
				foreach ($query as $line) 
				{
					$baris++;
					$no++;
					$tgl_lahir = new DateTime ($line->tgl_lahir);
					$today = new DateTime($line->tgl_masuk);
					$diff = $today->diff($tgl_lahir);
					$umurAr=$diff->y;
					if($umurAr == 0){
						$umurAr=1;
					}
					$start_date =new DateTime($line->tgl_masuk);
					$end_date =new DateTime($line->tgl_keluar);
					$lama = $start_date->diff($end_date);
					$html.='
						<tr class="headerrow"> 
							<td align="right">'.$no.'</td>
							<td align="center">'.$line->kd_pasien.'</td>
							<td align="left">'.$line->nama.'</td>
							<td align="left">'.$line->alamat.'</td>
							<td align="center">'.$line->jk.'</td>
							<td align="center">'.$umurAr.' th</td>
							<td align="left">'.$line->kelas.'</td>
							<td align="left">'.$line->nama_kamar.'</td>
							<td align="center">'.$line->spesialisasi.'</td>
							<td align="center">'.$line->tgl_masuk.'</td>
							<td align="center">'.$line->jam_masuk.'</td>
							<td align="center">'.$line->tgl_keluar.'</td>
							<td align="center">'.$line->jam_keluar.'</td>
							<td align="center">'.$lama->days.' hr </td>
						</tr>';
				}
         }
		$html.="</table>";		
		$baris=$baris+15;
		$print_area='A1:N'.$baris;
		$area_wrap='A6:N'.$baris;
		$area_kanan='C8:N'.$baris;
		$prop=array('foot'=>true);
		if($type_file == 1){
			
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:N7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:N4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('N')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:N7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(7);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(7);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Keluar_Masuk.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
			
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L',' Laporan_Keluar_Masuk',$html);
		
		}
	}
	
	function cetak_lap_perkamar(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
	
        $Split = explode("##@@##", $getcriteria,  4);
		if (count($Split) > 0 ){
			$tgl1 = $Split[0];
			$tgl2 = $Split[1];
			$tmpkelas = $Split[2];
			$kelas = $Split[3];
		}
		if ($tgl1 === $tgl2){
			$kriteria = "Periode ".$tgl1." ";
		}else{
			$kriteria = "Periode ".$tgl1." s/d ".$tgl2."";
		}
		if ($kelas != 0){
			$tmpParamkelas = " and i.no_kamar='".$kelas."' ";
		}else{
			$tmpParamkelas = " ";
		}
		
        if ($kelas != 0){
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.no_kamar='".$kelas."' and i.Akhir='t' ";
        }else{
            $tmptambah = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
				and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.Akhir='t' ";
        }
      
        if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="9">Daftar Pasien Masuk Rawat Inap PerKamar</th>
						</tr>
						<tr>
							<th  colspan="9">Tanggal '.$kriteria.'</th>
						</tr>
				</table><br>';
		$html.='
				<table class="t1" border = "1" cellpadding="3">
				<thead>
					<tr>
						<td align="center" width="31">No.</td>
						<td align="center" width="60">No. Medrec</td>
						<td align="center" width="100"> Nama Pasien</td>
						<td align="center" width="184">Alamat</td>
						<td align="center" width="26">JK</td>
						<td align="center" width="79">Umur</td>
						<td align="center" width="83">Agama</td>
						<td align="center" width="112">Nama Dokter</td>
						<td align="center" width="103">Tgl Masuk</td>
					</tr>
				</thead>';
				
		$Params = "Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
					and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') ".$tmpParamkelas." and i.Akhir='t' ORDER BY kamar ";
        
        $tmpquery = "SELECT  p.kd_pasien,p.Nama, 
						CASE WHEN p.jenis_kelamin = 't' then 'L' else 'P' END AS JK, Agama.Agama, k.Tgl_Masuk, tgl_lahir,
						CASE WHEN age(k.Tgl_Masuk, tgl_lahir) ='00:00:00' then '1' else age(k.Tgl_Masuk, tgl_lahir) end as JmlUmur,
						P.Alamat,d.Nama AS Nama_Dokter,u.nama_unit AS Kelas,kmr.no_kamar || '(' || kmr.nama_kamar || ')' AS KAMAR, to_char(i.Tgl_Inap, 'dd-MM-YYYY') AS Tgl_Masuk
						FROM unit u
						INNER JOIN 
						(Pasien p  
						inner JOIN Agama on agama.kd_agama=p.kd_agama  
						inner Join (transaksi t 
						inner JOIN ((nginap i 
						inner JOIN kamar kmr ON (i.no_kamar=kmr.no_kamar) AND (i.kd_unit_kamar=kmr.kd_unit)) 
						inner Join(kunjungan k 
						inner JOIN dokter d  ON d.kd_dokter=K.kd_dokter) ON (k.kd_unit=i.kd_unit)  
						AND (k.kd_pasien=i.kd_pasien)  
						AND (k.tgl_masuk=i.tgl_masuk) 
						AND (k.urut_masuk=i.urut_masuk)) ON (t.kd_pasien=i.kd_pasien) 
						AND (t.kd_unit=i.kd_unit)  and (t.tgl_transaksi=i.tgl_masuk) 
						and (t.urut_masuk=i.urut_masuk)) ON t.kd_pasien=p.kd_pasien) ON u.kd_unit=i.kd_unit_kamar ";
        $q = $this->db->query($tmpquery.$Params);
		$tmphquery = $this->db->query(" select kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar from kunjungan k 
										inner join transaksi t on k.kd_pasien = t.kd_pasien and  k.kd_unit=t.kd_unit and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk
										inner join kamar kmr on kmr.kd_unit = k.kd_unit
										inner join nginap i on kmr.no_kamar = i.no_kamar 
											".$tmptambah."
											group by kmr.no_kamar, kmr.nama_kamar, i.kd_unit_kamar order by kmr.nama_kamar
										")->result();
		$baris=0;
		if($q->num_rows == 0){
			$html.="<tr><td colspan='9' align='center'> Data Tidak Ada</td></tr>";
			$baris++;
		}else{
			//perulangan kamar yg dipilih untuk ambil no_kamar
			foreach ($tmphquery as $line){
				$tmpparam2 = " Where to_char(i.tgl_inap, 'dd-MM-YYYY') = to_char(i.tgl_masuk, 'dd-MM-YYYY')
								and  (t.tgl_transaksi between '".$tgl1."' and '".$tgl2."') and i.no_kamar= '".$line->no_kamar."' and i.kd_unit_kamar='".$line->kd_unit_kamar."' and i.Akhir='t' order by kmr.nama_kamar     
								";
				$dquery =  $this->db->query($tmpquery.$tmpparam2);
				$no = 0;
				if($dquery->num_rows == 0){
					$html.='';
					$baris++;
				}else{
					$baris++;
					$html.='<tr>
							  <td></td><td colspan="8"><b>'.$line->nama_kamar.'</b></td>
							</tr>';
					foreach ($dquery->result() as $d){
						$no++;
						$baris++;
						// $umurAr=$this->date->umur1($d->tgl_lahir,$d->tgl_masuk);
										
						$tgl_lahir = new DateTime ($d->tgl_lahir);
						$today = new DateTime($d->tgl_masuk);
						$diff = $today->diff($tgl_lahir);
						$umurAr=$diff->y;
						$html.='<tr > 
									<td align="center">'.$no.'</td>
									<td width="50">'.$d->kd_pasien.'</td>
									<td width="50">'.$d->nama.'</td>
									<td width="50" >'.$d->alamat.'</td>
									<td width="50" align="center">'.$d->jk.'</td>
									<td width="50" align="center">'.$umurAr.' th </td>
									<td width="50" >'.$d->agama.'</td>
									<td width="50" >'.$d->nama_dokter.'</td>
									<td width="50" >'.$d->tgl_masuk.'</td>
							   </tr> ';
					}
				}
			}
		}  
		$html.="</table>";		
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:I'.$baris;
		$area_wrap='A6:I'.$baris;
		$area_kanan='C8:I'.$baris;
		if($type_file == 1){
			
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Daftar_Pasien_Masuk_RWI_PerKamar.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Daftar_Pasien_Masuk_RWI_PerKamar',$html);
		
		}
	}
	
	function cetak_lap_per_perujuk(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
        $Split = explode("##@@##", $getcriteria,  11);
		if (count($Split) > 0 )
		{
			$tgl1 = $Split[0];
			$tgl2 = $Split[1];
			$tmprujuk = $Split[2];
			$rujuk = $Split[3];
			$tmpjenis = $Split[4];
			$unit = $Split[5];
			$criteria = "";
			$tmpunit = explode(',', $Split[5]);
			for($i=0;$i<count($tmpunit);$i++)
			{
			$criteria .= "'".$tmpunit[$i]."',";
			}
			$criteria = substr($criteria, 0, -1);
			
			$tmptambahParam = " ";
			$tmprujukan = "";   
			if ($tgl1 === $tgl2)
			{
				$kriteria = "Periode ".$tgl1;
			}else
			{
				$kriteria = "Periode ".$tgl1." s/d ".$tgl2." ";
			}
			if ($rujuk != 'Semua')
			{
				$tmprujukan = "and k.kd_Rujukan = ".$rujuk." ";
			}else
			{
				$tmprujukan = "";
			}
			$Param = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
					. $tmptambahParam
					. $tmprujukan
					. "and k.kd_Unit in ($criteria)  "
					. "Order By r.rujukan,u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
        }
      
       $tmphquery = $this->db->query("select kd_rujukan, rujukan from rujukan");
                    
        
        $tmpquery = "Select r.kd_rujukan,r.rujukan,u.Nama_Unit, u.kd_Unit, ps.Kd_Pasien,ps.Nama,
                        ps.Alamat, case when ps.jenis_kelamin = 't' then 'L' else 'P' end as JK,
                        k.Tgl_Masuk, ps.Tgl_Lahir, 
                        case when k.baru = 't' then 'X' else '' end as Baru,
                        case when k.Baru = 'f' then 'X' else '' end as Lama,
                        pk.pekerjaan, prs.perusahaan,  k.kd_Rujukan, r.Rujukan, k.jam_masuk, 
                        c.customer, CASE WHEN k.kd_rujukan != 0 THEN 'x' ELSE ' ' END As textrujukan

                        From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
                        left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
                        inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
                        LEFT JOIN Rujukan r On r.kd_Rujukan=k.kd_Rujukan INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer ";
   
        $q = $this->db->query($tmpquery.$Param);
		
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="11">Daftar Rawat Inap PerRujukan</th>
						</tr>
						<tr>
							<th  colspan="11">Tanggal '.$kriteria.'</th>
						</tr>
					
				</table><br>';
		
		$html.='
           <table class="t1" border = "1">
           <thead>
            <tr>
              <th align="center" width="24" rowspan="2">No. </th>
              <th align="center" width="137" rowspan="2">No. Medrec </th>
              <th align="center" width="137" rowspan="2">Nama Pasien</th>
              <th align="center" width="273" rowspan="2">Alamat</th>
              <th align="center" width="26" rowspan="2">JK</th>
              <th align="center" width="82" rowspan="2">Umur</th>
              <th align="center" colspan="2">Kunjungan</th>
              <th align="center" width="82" rowspan="2">Pekerjaan</th>
              <th align="center" width="68" rowspan="2">Customer</th>
              <th align="center" width="63" rowspan="2">Rujukan</th>
            </tr>
            <tr>
              <th align="center" width="37">Baru</th>
              <th align="center" width="39">Lama</th>
            </tr>
          </thead>
          ';
		$baris=0;
        if($q->num_rows == 0)
        {
			$html.="<tr><td colspan='11' align='center'> Data Tidak Ada</td></tr>";
			$baris++;
		}else {
			$query = $q->result();
		   
			$tmpparam2 = "Where (k.Tgl_Masuk >= '".$tgl1."' and k.Tgl_Masuk <= '".$tgl2."' ) "
						. $tmptambahParam
						. $tmprujukan
						. "and k.kd_Unit in ($criteria) "
						. "Order By u.nama_unit ,k.kd_Rujukan, k.KD_PASIEN";
			$dquery =  $this->db->query($tmpquery.$tmpparam2);
			$no = 0;
			if($dquery->num_rows == 0)
			{
				$html.='';
				$baris++;
			}else {
				$lastkdrujukan='';
				foreach ($q->result() as $d)
				{
					$no++;
					if($d->rujukan != $lastkdrujukan){
						$html.='<tr class="headerrow">
										<td></td>
									   <td colspan="10" align="left">'.$d->rujukan.'</td>
								</tr>';
						$lastkdrujukan=$d->rujukan;
						$no=1;			
						$baris++;
					}
					$tgl_lahir = new DateTime ($d->tgl_lahir);
					$today = new DateTime($d->tgl_masuk);
					$diff = $today->diff($tgl_lahir);
					$umurAr=$diff->y;
					$html.='
					   <tr class="headerrow"> 
						   <td align="right">'.$no.'</td>
						   <td width="50">'.$d->kd_pasien.'</td>
						   <td width="50">'.$d->nama.'</td>
						   <td width="50">'.$d->alamat.'</td>
						   <td width="50" align="center">'.$d->jk.'</td>
						   <td width="50">'.$umurAr.' </td>
						   <td width="50" align="center">'.$d->baru.'</td>
						   <td width="50" align="center">'.$d->lama.'</td>
						   <td width="50">'.$d->pekerjaan.'</td>
						   <td width="50">'.$d->customer.'</td>
						   <td width="50" align="center">'.$d->textrujukan.'</td>
					   </tr>'; 
					$baris++;
				}  
			}  
		}
		$html.="</table>";		
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:K'.$baris;
		$area_wrap='A6:K'.$baris;
		$area_tengah='E8:H'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_tengah)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Daftar_Rawat_Inap_PerRujukan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Daftar Rawat Inap PerRujukan',$html);
		
		}
	}
	
	function cetak_lap_ResponTime(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$tgl_awal     = $param->tgl_awal;
		$tgl_akhir    = $param->tgl_akhir;
		$kel_pas      = $param->kel_pas;
		$type_file    = $param->type_file;
		$kel_pas2     = $param->kel_pas2;
		$kel_pas      = $param->kel_pas;
		$list_kd_unit = $param->list_kd_unit;
		
		if($kel_pas == 0 ){
			$kelompokPasien='Perseorangan';
		}else if($kel_pas == 1 ){
			$kelompokPasien='Perusahaan';
		}else if($kel_pas == 2 ){
			$kelompokPasien='Asuransi';
		}else{
			$kelompokPasien='Semua';
		}
		
		$criteria_customer = "";
		$customer = 'Semua';

		if (strtolower($kel_pas2) == 'semua' && strtolower($kel_pas) != 'semua') {
			$query_kontraktor = $this->db->query("SELECT * FROM kontraktor WHERE jenis_cust = '".$kel_pas."'");
			foreach ($query_kontraktor->result() as $result) {
				$criteria_customer .= "'".$result->kd_customer."',";
			}
			$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			$criteria_customer = " AND k.kd_customer in (".$criteria_customer.") ";
			$customer = 'Semua';
		}else if(strtolower($kel_pas) != 'semua') {
			$criteria_customer=" AND k.kd_customer = '".$kel_pas2."' ";
			$customer = $this->db->query("SELECT * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
		}

		$criteriaUnit = " and u.parent in (".$list_kd_unit.") ";
		$query = $this->db->query("SELECT p.KD_PASIEN, p.NAMA, p.ALAMAT, km.NAMA_KAMAR,
										date_trunc('day',t.tgl_boleh_plg) as TGL_BLHPLG, 
										date_trunc('second',t.jam_boleh_plg) as  DETIK_BLHPLG,
										date_trunc('day',k.TGL_KELUAR) as  TGL_CETAK,
										date_trunc('second',k.JAM_KELUAR) as  DETIK_CETAK,
										
										CASE WHEN k.JAM_KELUAR >= t.jam_boleh_plg 
											THEN date_part('day',k.tgl_keluar -  t.tgl_boleh_plg )
										ELSE 
											date_part ('day',k.tgl_keluar - t.tgl_boleh_plg ) - 1 
										END as selisih_tgl, 
										
										date_part('hour',k.JAM_KELUAR - t.jam_boleh_plg) as SELISIH_JAM,
										date_part('minute',k.JAM_KELUAR - t.jam_boleh_plg) as SELISIH_MENIT,
										date_part('second',k.JAM_KELUAR - t.jam_boleh_plg) as SELISIH_DETIK
										from kunjungan k
										  inner join PASIEN p on p.KD_PASIEN = k.KD_PASIEN
										  inner join UNIT u on u.KD_UNIT = k.KD_UNIT
										  inner join TRANSAKSI t on t.KD_PASIEN = k.KD_PASIEN and t.KD_UNIT = k.KD_UNIT and t.TGL_TRANSAKSI = k.TGL_MASUK and t.URUT_MASUK = k.URUT_MASUK
										  inner join NGINAP ng on ng.KD_PASIEN = k.KD_PASIEN and ng.KD_UNIT = k.KD_UNIT and ng.TGL_MASUK = k.TGL_MASUK and ng.URUT_MASUK = k.URUT_MASUK
										  inner join KAMAR km on km.KD_UNIT = ng.KD_UNIT_KAMAR and km.NO_KAMAR = ng.NO_KAMAR
										where 
											T.TGL_CO between '".$tgl_awal."' and '".$tgl_akhir."'
											".$criteriaUnit."	
											and k.TGL_KELUAR is not null	
											and ng.AKHIR = 't'	
											and t.tgl_boleh_plg is not null			  
										 	".$criteria_customer."	")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th colspan="10" style='.$font_style.'>LAPORAN RESPONSE TIME PASIEN PULANG RAWAT INAP</th>
						</tr>
						<tr>
							<th colspan="10" style='.$font_style2.'>'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th colspan="10"  style='.$font_style2.'> Kelompok Pasien '.$kelompokPasien.' ( '.$customer.' )</th>
						</tr>
				</table><br>';
		
		$html.='
           <table class="t1" border = "1" cellpadding="2">
           <thead>
            <tr>
              <th align="center" >No. </th>
              <th align="center" >No. Medrec </th>
              <th align="center" >Nama Pasien</th>
              <th align="center" >Alamat</th>
              <th align="center" >Ruang Perawatan</th>
              <th align="center"  >Tgl Boleh Pulang Oleh Dokter</th>
              <th align="center"  >Jam Boleh Pulang Oleh Dokter</th>
              <th align="center"  >Tgl Cetak Billing</th>
              <th align="center"  >Jam Cetak Billing</th>
              <th align="center"  >Response Time</th>
            </tr>
          </thead>';
		$baris=0;
		$arr_time=array();
		if(count($query)>0){
			$no=0;
			$i=0;
			foreach ($query as $line){
				$baris++;
				$arr_time[$i]=$line->selisih_jam.':'.$line->selisih_menit.':'.$line->selisih_detik;
				$jam_plg = substr($line->detik_blhplg,-8);//MENGAMBIL JAM
				if($line->selisih_jam < 10){
					$jam='0'.$line->selisih_jam; 
				}else{
					$jam=$line->selisih_jam; 
				}
				
				if($line->selisih_menit < 10){
					$menit='0'.$line->selisih_menit; 
				}else{
					$menit=$line->selisih_menit; 
				}
				
				if($line->selisih_detik < 10){
					$detik='0'.$line->selisih_detik; 
				}else{
					$detik=$line->selisih_detik; 
				}
				$no++;
				$html.=' <tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->kd_pasien.'</td>
							<td>'.$line->nama.'</td>
							<td>'.$line->alamat.'</td>
							<td>'.$line->nama_kamar.'</td>
							<td align="center">'.date('d-M-Y', strtotime($line->tgl_blhplg)).'</td>
							<td align="center">'.$jam_plg.'</td>
							<td align="center">'.date('d-M-Y', strtotime($line->tgl_cetak)).'</td>
							<td align="center">'.date_format(date_create($line->detik_cetak), 'H:i:s').'</td>
							<td align="center">'.$jam.':'.$menit.':'.$detik.'</td>
						</tr>';
				$i++;
			}
			 $sum = strtotime('00:00:00');
			 $sum2=0;  
			 foreach ($arr_time as $v){
					$sum1=strtotime($v)-$sum;
					$sum2 = $sum2+$sum1;
			}
			$sum3=$sum+$sum2;
			$html.='<tr><td colspan="9" align="right"> <b>Total Jam</b></td><td align="center">'.date("H:i:s",$sum3).'</td></tr>';
			$baris++;
		}else{
			$html.='<tr><td colspan="10" align="center"> Data Tidak Ada</td></tr>';
			$baris++;
		}
		$html.='</table>';		
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:J'.$baris;
		$area_wrap='A6:J'.$baris;
		$area_tengah='E8:J'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:J6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:J6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Disposition: attachment;filename=Lap_Respons_T.xls'); # specify the download file name
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','LAPORAN RESPONSE TIME PASIEN PULANG RAWAT INAP',$html);
		
		}
	}
	
	function cetak_lap_batal_transaksi(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
		$params=explode('#aje#',$getcriteria);
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
			
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
						<tr>
							<th style='.$font_style.' colspan="11">'.$params[2].'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="11">'.$params[0].' s/d '.$params[1].'</th>
						</tr>
				</table><br>';
		$html.='<table border="1" cellpadding="3">
				<thead>
	 				<tr>
	 					<td align= "center"><b>No.</b></td>
	 					<td align= "center"><b>No. Medrec</b></td>
						<td align= "center"><b>Nama Pasien</b></td>
						<td align= "center"><b>No transaksi</b></td>
						<td align= "center"><b>Unit</b></td>
						<td align= "center"><b>Tanggal batal</b></td>
						<td align= "center"><b>Shift batal</b></td>
						<td align= "center"><b>Jam batal</b></td>
						<td align= "center"><b>Alasan Batal</b></td>
						<td align= "right"><b>Jumlah</b></td>
	 					<td align= "center"><b>Petugas </b></td>
	 				</tr>
				</thead>
			';
		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$query=$this->db->query("select 
						  kd_kasir,no_transaksi ,tgl_transaksi ,kd_pasien ,nama ,
						  kd_unit,nama_unit,
						  kd_user ,kd_user_del ,
						  shift, shiftdel ,jumlah,
						  user_name ,tgl_batal , ket,jam_batal  from history_batal_trans  
						where 
							kd_kasir='".$kd_kasir."' and 
							tgl_batal between '".$params[0]."' and  '".$params[1]."'
						 ")->result();
		$totall=0;
		$baris=0;
		if(count($query)>0){
			$baris++;
			foreach ($query as $line) 
			{
				$tanggal=date_create($line->tgl_batal);
				$jam=date_create($line->jam_batal);
				$totall+=$line->jumlah;
				$no=1;
				$html.='
						<tr>
							<td>'.$no .'</td>
							<td>'.$line->kd_pasien .'</td>
							<td>'.$line->nama .'</td>
							<td>'.$line->no_transaksi .'</td>
							<td>'.$line->nama_unit .'</td>
							<td align="center">'.date_format($tanggal,"Y/m/d") .'</td>
							<td align="center">'.$line->shiftdel .'</td>
							<td  align="center">'.date_format($jam,"H:i") .'</td>
							<td  >'.$line->ket .'</td>
							<td  align= "right">'.number_format($line->jumlah,0,".",",") .'</td>
							<td  >'.$line->user_name .' </td>
						</tr>
					';
				$no++;
				$baris++;
			}
			 $html.='<tr>
							<td align= "right" colspan="9"><strong>Grand Total</strong></td>
							<td align= "right"><strong>'.number_format($totall,0,".",",").'</strong></td>
							<td >&nbsp;</td>
						</tr>
					';
			$baris++;
		}else{
			$html.='<tr><td colspan="11" align="center">Data Tidak Ada</td></tr>';
			$baris++;
		}
		$html.="</table>";
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:K'.$baris;
		$area_wrap='A5:K'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:K5')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			# END Fungsi untuk set ALIGNMENT 
			
			 # END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Batal_Transaksi_Rawat_Inap.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$common=$this->common;
			$this->common->setPdf('P','Laporan_Batal_Transaksi_Rawat_Inap',$html);
		
		}
	}
	
	function cetak_lap_pelayanan_dokter(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		$type_file = $param->type_file;
		
		$Split = explode("##@@##", $getcriteria, 12);
		if (count($Split) > 0 ){
			$autocas = $Split[1];
			$unit = $Split[2];
			$kdunit = $Split[3];
			$dokter = $Split[4];
			$kddokter = $Split[5];
			$tglAwal = $Split[6];
			$tglAkhir = $Split[7];
			$kelPasien = $Split[9];
			$kdCustomer = $Split[10];
			//echo $kdCustomer;

			
			if($dokter =='Dokter' || $kddokter =='Semua'){
				$paramdokter="";
			} else{
				$paramdokter =" and dtd.kd_Dokter='$kddokter'";
			}
			
			if($kelPasien=='Semua' || $kdCustomer==NULL){
				$paramcustomer="";
			} else{
				$paramcustomer =" and k.Kd_Customer ='$kdCustomer'";
			}
			
			if($unit =='Unit' || $kdunit=='Semua'){
				$paramunit='';
			} else{
				$paramunit=" and t.kd_unit ='$kdunit' ";
			}
			
			if($autocas == 1){
				$params=" and dt.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
			} else if($autocas == 2){
				$params=" and dt.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
			} else if($autocas == 3 || $autocas == 4){
				$params="";
			}
			
		}
		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$queryHasil = $this->db->query( "
                                      Select distinct(d.Nama) as Dokter, dtd.kd_Dokter
										From Detail_TRDokter dtd  
											INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter  
											INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut  
											INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir  
											INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk  
											INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer  
											INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien  
											INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
											INNER JOIN unit u On u.kd_unit=t.kd_unit 
											Where 	dt.Kd_kasir='".$kd_kasir."'						
											and  t.ispay='t' 	
											And u.kd_bagian='1'						
											And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'			
											And dt.Qty * dtd.JP >0	
											".$paramdokter."".$paramcustomer."".$params."".$paramunit."						
										Group By d.Nama, dtd.kd_Dokter 
										Order By Dokter, dtd.kd_Dokter 
                                        
		
                                      ");
		$query = $queryHasil->result();
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="5">Laporan Jasa Pelayanan Dokter Per Pasien</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="5">'.$tglAwal.' s/d '.$tglAkhir.'</th>
						</tr>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap">
					<thead>
					  <tr>
						<th >No</th>
						<th align="center">DOKTER/PASIEN</th>
						<th align="right">JP. DOKTER</th>
						<th align="right">PAJAK</th>
						<th align="right">JUMLAH</th>
					  </tr>
					</thead>';
		$baris=0;
		if(count($query) == 0)
		{
			$html.='<tr><td colspan="5" align="center"> Data Tidak Ada</td></tr>';
			$baris++;
		} else {
            $no = 0;	
			$jd = 0;
			$pph = 0;
			$grand =0;
			foreach ($query as $line)
			{
				$baris++;
				$no++;
				$html.='<tr class="headerrow">
							<td>'.$no.'</td>
							<td width="200" align="left" colspan="4">'.$line->dokter.'</td>
						</tr>';
				$qdok=$this->db->query("select kd_dokter from dokter where nama='$line->dokter'")->row();
				$dok=$qdok->kd_dokter;

				$queryHasil2 = $this->db->query( "Select d.Nama as Dokter, max(p.Nama) as nama, p.kd_pasien, Sum(dt.Qty * dtd.JP) as JD, ((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as pph,
													Sum(dt.Qty * dtd.JP)-((select setting from sys_setting where key_data='rwj_pphdokter')::double precision /100)*Sum(dt.Qty * dtd.JP) as jumlah
													From Detail_TRDokter dtd
														INNER JOIN Dokter d On d.kd_Dokter=dtd.kd_Dokter
														INNER JOIN Detail_Transaksi dt  On dt.Kd_Kasir=dtd.Kd_kasir And dt.No_Transaksi=dtd.no_Transaksi  and dt.Tgl_Transaksi=dtd.tgl_Transaksi And dt.Urut=dtd.Urut
														INNER JOIN Transaksi t On t.no_Transaksi = dt.no_Transaksi And t.KD_kasir=dt.Kd_kasir
														INNER JOIN Kunjungan k On k.kd_pasien=t.kd_pasien And k.Kd_Unit=t.kd_Unit  And k.Tgl_masuk=t.Tgl_Transaksi And t.Urut_masuk=k.Urut_masuk
														INNER JOIN Kontraktor knt On knt.Kd_Customer=k.Kd_Customer
														INNER JOIN Pasien p On p.kd_Pasien=t.KD_pasien
														INNER JOIN Produk pr On pr.KD_Produk=dt.kd_Produk
														INNER JOIN unit u On u.kd_unit=t.kd_unit
													Where dt.Kd_kasir='".$kd_kasir."'
													And t.ispay='t'
													and u.kd_bagian='1'
													And dt.Tgl_Transaksi>= '$tglAwal'  And dt.Tgl_Transaksi<= '$tglAkhir'
													And dt.Qty * dtd.JP >0
													And dtd.kd_Dokter='$dok'
													".$paramcustomer."".$params."".$paramunit."
													Group By d.Nama, p.Kd_pasien
													Order By Dokter, p.Kd_pasien");
				$query2 = $queryHasil2->result();
				$noo=0;
				$sub_jumlah=0;
				$sub_pph=0;
				$sub_jd=0;
				foreach ($query2 as $line2)
				{
					$noo++;
					$baris++;
					$sub_jumlah+=$line2->jumlah;
					$sub_pph +=$line2->pph;
					$sub_jd+=$line2->jd;
					$html.='<tr>
								<td > </td>
								<td >'.$noo.'. '.$line2->kd_pasien.' '.$line2->nama.'</td>
								<td align="right" >'.number_format($line2->jd,0,',',',').'</td>
								<td align="right">'.number_format($line2->pph,0,',',',').'</td>
								<td align="right">'.number_format($line2->jumlah,0,',',',').'</td>
							</tr>';
				}
				$html.='<tr class="headerrow">
						<td align="right" colspan="2">Sub Total</td>
						<td align="right">'.number_format($sub_jd,0,',',',').'</td>
						<td align="right">'.number_format($sub_pph,0,',',',').'</td>
						<td align="right">'.number_format($sub_jumlah,0,',',',').'</td>
					</tr>';
				$jd += $sub_jd;
				$pph += $sub_pph;
				$grand += $sub_jumlah;
				$baris++;
			}
			$html.='<tr class="headerrow">
						<td align="right" colspan="2">GRAND TOTAL</td>
						<td align="right">'.number_format($jd,0,',',',').'</td>
						<td align="right">'.number_format($pph,0,',',',').'</td>
						<td align="right" >'.number_format($grand,0,',',',').'</td>
					</tr>';
			$baris++;			
		}
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:E'.$baris;
		$area_wrap='A6:E'.$baris;
		$area_kanan='C6:E'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:E7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:E4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:E5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LAP_JASA_PEL_DOKTER_PER_PASIEN.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('P','LAPORAN JASA PELAYANAN DOKTER PER PASIEN',$html);
		
		}
	}
	function cetak_lap_transaksi(){
		$html='';
		$param=json_decode($_POST['data']);
		
		$getcriteria = $param->criteria;
		
		$type_file = $param->type_file;
		
		$Split = explode("##@@##", $getcriteria, 10);
		$tglsum = $Split[0];
		$tglsummax = $Split[1];
		$kdtranfer = $this->db->query("select setting from sys_setting where key_data = 'sys_kd_pay_transfer'")->row()->setting;
		$tglsumx=$Split[0]+strtotime("+1 day");
		$tglsum2= date("Y-m-d", $tglsumx);
		$tglsummaxx=$Split[1]+strtotime("+1 day");
		$tglsummax2= date("Y-m-d", $tglsummaxx);
				
		if($Split[2]==="Semua" || $Split[2]==="undefined")
		{
			$kdUNIT2="";
			$kdUNIT="";
		}else
		{
			
			$kdUNIT2="";
			$kdUNIT="";
		}
				
		if($Split[3]==="Semua")
		{
			$jniscus="";
		}else{
			$jniscus=" and  Ktr.Jenis_cust=".$Split[3]."";
		}
				
				
		if ($Split[4]==="NULL")
		{
			$customerx="";
		}else{
			$customerx=" And Ktr.kd_Customer='".$Split[4]."'";
		}
		
		if (count($Split)===9)
		{
			$Shift4x=" Or  (Tgl_Transaksi>= '$tglsum2'  And Tgl_Transaksi<= '$tglsummax2'  And Shift=4)";
			if ($Split[7]=== "ya")
			{
				if($Split[8]=== "ya"){
					$c_tindakan=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')
										or x.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1') )";
				
				}else{
					$c_tindakan=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
				}
			}else{
				$c_tindakan="";
			}
	
		}else {
			if ($Split[7]=== "ya")
			{
				if($Split[8]=== "ya"){
					$c_tindakan=" and (x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')
										or x.KD_PRODUK NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1') )";
				
				}else{
					$c_tindakan=" and x.KD_PRODUK IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where u.kd_bagian='1')";
				}
			}else{
				$c_tindakan="";
			}
			
			$Shift4x="";
		}
		$shiftx = substr($Split[5], 0, -1);
			
					
		$criteria="";
		$tmpunit = explode(',', $Split[3]);
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1);
					
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="5">LAPORAN TRANSAKSI</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="5">Per Produk</th>
						</tr>
						<tr>
							<th  colspan="5">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
				</table><br>';
	
		$html.='<table class="t1" border = "1">
				<thead>
				 <tr >
					<th align="center" >Unit</th>
					<th align="center"  >Tindakan</th> 
					<th align="center"  > Pasien</th>
					<th align="center"  >Produk</th>
					<th align="center" >(Rp.)</th>
					  
				 </tr></thead>';
			
		$fquery =$this->db->query("select kd_unit,nama_unit from unit where kd_bagian ='01' $kdUNIT 
							group by kd_unit,nama_unit  order by nama_unit asc")->result();

		$kd_kasir= $this->db->query("select setting from sys_setting where key_data='default_kd_kasir_rwi'")->row()->setting;
		$baris=0;
		foreach($fquery as $f)
		{
			$query = "Select U.Nama_Unit as namaunit, p.deskripsi as keterangan, Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan 
						From ((
								( Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk) 
									INNER JOIN 
									(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
										From  detail_transaksi Where kd_kasir ='$kd_kasir' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
										$Shift4x)  
										Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
									) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi) 
								INNER JOIN Unit u On u.kd_unit=t.kd_unit) 
						INNER JOIN Produk p on p.kd_produk=x.kd_produk 
						LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
						INNER JOIN 
							(
								select  kd_kasir,no_transaksi from DETAIL_BAYAR db where db.kd_pay<> '".$kdtranfer."' group by  kd_kasir,no_transaksi 
							) db On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
			Where 	left(k.kd_Unit,1) ='1'	
					and t.ispay='true'	
					And k.kd_Unit ='".$f->kd_unit."'								
					$jniscus				
					$customerx
					$c_tindakan						
			Group By u.Nama_Unit, p.Deskripsi		
			Order by U.Nama_Unit, p.Deskripsi ";
			$result =$this->db->query($query)->result();
			$i=1;

			if(count($result) <= 0)
			{
				$html.='';
				$baris++;
			}else{
				$html.='<tr><td ><b>'.$f->nama_unit.'</b></td><td></td><td></td><td></td><td></td></tr>';
				$pasientotal=0;
				$Jumlahtindakan=0;
				$duittotal=0;
				foreach ($result as $line)
				{
					$baris++;
					$pasientotal+=$line->totalpasien;
					$Jumlahtindakan+=$line->jumlahtindakan;
					$duittotal+=$line->duittotal;

				   $html.='<tr > 
								<td></td>
								<td align="left">'.$i.'. '.$line->keterangan.'</td>
								<td align="right">'. $line->totalpasien.'</td>
								<td align="right">'.$line->jumlahtindakan.'</td>
								<td align="right">'.substr(number_format($line->duittotal,2,',',','),0,-3).'</td>
						   </tr> ';
					$i++;
				}
	
				$i--;
				$html.='<tr>
							<td></td>
							<td ><b>Sub Total '.$f->nama_unit.' </b></td>
							<td colspan="1" align="right">'.$pasientotal.'</td>
							<td colspan="1" align="right">'.$Jumlahtindakan.'</td>
							<td colspan="1" align="right">'.substr(number_format($duittotal,2,',',','),0,-3).'</td>
						</tr>';						  
			}
        }
		$query2 = "Select  Count(k.Kd_Pasien) as totalpasien, COALESCE(sum(x.Jumlah),0) as duittotal,Sum(x.Qty) as jumlahtindakan From 
					(((Kunjungan k INNER JOIN Transaksi t On t.Kd_Pasien=k.Kd_pasien And k.Kd_Unit=t.Kd_Unit And k.Tgl_Masuk=t.Tgl_Transaksi And k.Urut_Masuk=t.Urut_Masuk) 
						INNER JOIN 
							(Select kd_kasir, No_transaksi, kd_produk, Sum(Qty) as Qty, Sum(qty*Harga) as Jumlah,urut,tgl_transaksi 
								From  detail_transaksi Where kd_kasir ='$kd_kasir' And ((tgl_transaksi>= '$tglsum'   And tgl_transaksi<= '$tglsummax' And Shift In ($shiftx))  
								$Shift4x)  
								Group by kd_kasir, No_transaksi, kd_produk,urut,tgl_transaksi 
							) x ON x.Kd_Kasir=t.Kd_Kasir And x.No_Transaksi=t.No_Transaksi
						) INNER JOIN Unit u On u.kd_unit=t.kd_unit
					) 	INNER JOIN Produk p on p.kd_produk=x.kd_produk 
						LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
						INNER JOIN 
						(
							select  kd_kasir,no_transaksi 
							from DETAIL_BAYAR db    
							where db.kd_pay<> '".$kdtranfer."'
							group by  kd_kasir,no_transaksi 
						) db On t.kd_kasir=db.kd_kasir and t.No_transaksi=db.No_transaksi 
					Where 	
					left(k.kd_Unit,1) ='1'	
					and t.ispay='true'	
					$kdUNIT2
					$jniscus				
					$customerx
					$c_tindakan ";
		$result2 = $this->db->query($query2)->result();
		$i=1;

		//pengecekan untuk menjadi parameter data kosong
		$total_pasien=0;
		foreach ($result2 as $line2)
		{
			$total_pasien = $total_pasien + $line2->totalpasien;
		}
		
		if($total_pasien > 0){
			$baris++;
			foreach ($result2 as $line2)
			{
				
				 $html.='<tr class="headerrow"> 
							<td ></td>
							<td ><b>Grand Total</b></td>
							<td width="90" align="right">'.$line2->totalpasien.'</td>
							<td width="90" align="right">'.$line2->jumlahtindakan.'</td>
							<td width="90" align="right">'.substr(number_format($line2->duittotal,2,',',','),0,-3).'</td>
						   </tr>';	
			}	
		}else{
			$html.='<tr><td align="center" colspan="5"> Data Tidak Ada</td></tr>';
			$baris++;
		}
		
		
		$html.='</table>';
		// echo $html;
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:E'.$baris;
		$area_wrap='A6:E'.$baris;
		$area_kanan='E7:E'.$baris;
		if($type_file == 1){
			
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:E7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:E4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:E6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LAP_TRANSAKSI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$this->common->setPdf('P','LAPORAN TRANSAKSI',$html);
		
		}
	}
	
	function get_kelas_produk(){
		$result=$this->db->query("select Kd_klas,Klasifikasi from Klas_produk Where Kd_klas in ('31','32','61','68','69','65','66','62','67')
									UNION Select '9999' as Kd_klas, 'Semua' as Klasifikasi Order By Klasifikasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

	}
	function getUser()
    {
        
		$result=$this->db->query("Select DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
									From zUsers 
										inner join  zMember ON zmember.kd_user=zuserS.kd_user 
										inner join  zGroups on zmember.group_id=zgroups.group_id 
										inner join zTrustee on zgroups.group_id =ztrustee.group_id 
										inner join zModule On ztrustee.mod_id=zmodule.mod_id 
									UNION 
									Select '9999' as kd_user, 'Semua' as user_names 
									Order By user_names")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	function getPayment(){
		$result=$this->db->query("SELECT kd_pay, uraian FROM payment UNION SELECT '999', 'SEMUA ' order by uraian")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getUnit(){
		$result=$this->db->query("SELECT kd_unit,nama_unit, 'false' as check FROM UNIT WHERE Kd_Bagian = '1' AND Parent = '1'")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getSpesialisasi(){
		$result=$this->db->query("SELECT kd_spesial, spesialisasi FROM spesialisasi 
				UNION 
				SELECT 999, 'SEMUA SPESIALISASI' ORDER BY spesialisasi")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getKelas(){
		$kd_spesial = $_POST['kd_spesial'];
		$kd_unit = $_POST['kd_unit'];
		$kd_units = substr($kd_unit, 0, -1);
		if($kd_spesial==''){
			$kd_spesial_s = "";
		}else{
			$kd_spesial_s = "su.kd_spesial='$kd_spesial' and ";
		}
		$result=$this->db->query("select * from (SELECT u.Kd_Unit, u.Nama_unit FROM Unit u INNER JOIN spc_Unit su ON su.kd_unit=u.kd_unit
									WHERE $kd_spesial_s  u.parent in($kd_units) UNION SELECT '999', 'SEMUA KELAS' ) x
									ORDER BY Kd_Unit")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	function getDokter(){
		$result=$this->db->query("SELECT kd_dokter,nama FROM Dokter 
									WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003'))
									union select '999' as kd_Dokter, 'Semua' as nama
									ORDER BY Nama ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getKamar(){
		$kd_irna = $_POST['kd_irna2'];
		$kd_irnas = substr($kd_irna, 0, -1);
		$kd_spesial = $_POST['kd_spesial'];
		$kd_kelas = $_POST['kd_kelas'];
		$kd_spesial_s='';
		if( $kd_spesial!='' ){
			$kd_spesial_s = " and kd_spesial='$kd_spesial'  ";
		}else{
			$kd_spesial_s='';
		}
		
		$kd_kelas_s='';
		if( $kd_kelas!='' ){
			$kd_kelas_s = " and kmr.kd_Unit in('$kd_kelas' ) ";
		}else{
			$kd_kelas_s='';
		}
		$result=$this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
									FROM Kamar kmr 
										INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit 
									WHERE 
										left (kmr.kd_Unit,4) in ($kd_irnas)
										$kd_spesial_s
										$kd_kelas_s										
									UNION 
									SELECT '999', 'SEMUA KAMAR' ORDER BY Nama_Kamar ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function getKamarRekap(){
		$kd_irna = $_POST['kd_irna2'];
		$kd_irnas = substr($kd_irna, 0, -1);
		$result=$this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
								FROM Kamar kmr 
									INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit 
									INNER JOIN unit u ON u.kd_unit=kmr.kd_unit 
								WHERE 
								-- 	left (kmr.kd_Unit,4) in ('1001')					
									u.parent in ($kd_irnas)
								UNION 
								SELECT '999', 'SEMUA KAMAR' ORDER BY Nama_Kamar ")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	
	}
	
	function cetak_lap_PelDokterPerTindakan_query(){
		$html='';
		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit = $param->unit; 
		$kd_unit = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		// $kd_unit_s = str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false
		if($pasien_pulang =='true'){
			$k_tgl_pasien_plg="	AND g.CO_Status = 't' AND g.Tgl_CO BETWEEN '$tgl_awal' AND '$tgl_akhir'";
		}else{
			$k_tgl_pasien_plg="	AND g.CO_Status = 'f' AND d.Tgl_Transaksi  BETWEEN '$tgl_awal' AND '$tgl_akhir'";
		}
		
		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		#KRITERIA DOKTER
		$dokter = $param->dokter; //tanpa kutip
		$tmpKdDokter='';
		if($dokter == 'Semua' || $dokter == '999'){
			$get_kd_dokter= $this->db->query("SELECT kd_dokter,nama FROM Dokter WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003')) ")->result();
			$k_kd_dokter ='';
			$i=1;
			foreach($get_kd_dokter as $line){
				if($i==1){
					$k_kd_dokter="'".$line->kd_dokter."'";
				}else{
					$k_kd_dokter=$k_kd_dokter.","."'".$line->kd_dokter."'";
				}
				$i++;
			}
			$tmpKdDokter=$k_kd_dokter;
		}else{
			$tmpKdDokter = "'$dokter'";
		}
		
		#KRITERIA OPERATOR
		$operator = $param->operator; //tanpa kutip
		$tmpOperator ='';
		if($operator == 'Semua' || $operator == '9999'){
			$get_kd_user= $this->db->query( "Select DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
													From zUsers 
														inner join  zMember ON zmember.kd_user=zuserS.kd_user 
														inner join  zGroups on zmember.group_id=zgroups.group_id 
														inner join zTrustee on zgroups.group_id =ztrustee.group_id 
														inner join zModule On ztrustee.mod_id=zmodule.mod_id 
											")->result();
			$k_kd_user ='';
			$i=1;
			foreach($get_kd_user as $line){
				if($i==1){
					$k_kd_user="'".$line->kd_user."'";
				}else{
					$k_kd_user=$k_kd_user.","."'".$line->kd_user."'";
				}
				$i++;
			}
			$tmpOperator=$k_kd_user;
		}else{
			$tmpOperator="'$operator'";
		}
		#PEMANGGILAN FUNGSI LAPORAN
		$query = $this->db->query("select distinct dokter from(
													SELECT x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,  
														CASE WHEN x.Tag_Char = '20' THEN x.JP  ELSE 0 END as A1, CASE WHEN x.Tag_Char = '21' THEN x.JP  ELSE 0 END as A2, CASE WHEN x.Tag_Char = '27' THEN x.JP  ELSE 0 END as A3 , 
														CASE WHEN x.Tag_Char = '29' THEN x.JP  ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp  ELSE 0 END as A5, CASE WHEN x.Tag_Char = '31' THEN x.JP  ELSE 0 END as A6 ,  
														sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien,	'15'  as Pajak 
													From 
														(SELECT d.Flag, k.NAMA_KAMAR,u.NAMA_UNIT as Kelas,v.Kd_Dokter, o.Nama as Dokter, g.kd_pasien,p.Kd_Klas, p.Deskripsi, p.Kd_Produk, d.Qty, v.JP as JP , 
															CASE WHEN v.Tag_Char = '20' THEN d.Qty*v.JP ELSE 0 END as JP20, CASE WHEN v.Tag_Char = '21' THEN d.Qty*v.JP ELSE 0 END as JP21, CASE WHEN v.Tag_Char = '27' THEN d.Qty*v.JP ELSE 0 END as JP27, 
															CASE WHEN v.Tag_Char = '28' THEN d.Qty*v.JP ELSE 0 END as JP28 ,CASE WHEN v.Tag_Char = '29' THEN d.Qty*v.JP ELSE 0 END as  JP29, CASE WHEN v.Tag_Char = '31' THEN d.Qty*v.JP ELSE 0 END as JP31, v.Tag_Char 
														FROM (((((Transaksi g INNER JOIN (Detail_Transaksi d 
															LEFT JOIN (Detail_Tr_Kamar dk 
																	INNER JOIN Kamar k ON dk.No_Kamar=k.No_Kamar AND dk.kd_Unit=k.kd_Unit) 
																ON d.kd_kasir=dk.kd_kasir AND d.No_Transaksi=dk.No_Transaksi AND d.urut=dk.urut AND d.Tgl_Transaksi=dk.Tgl_Transaksi) ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi) 
															INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
															LEFT JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
															INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
															INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk  
															INNER JOIN Klas_Produk KP On p.kd_klas=kp.kd_klas ) 
															INNER JOIN Unit u ON k.Kd_Unit = u.Kd_Unit 
															INNER JOIN  Kunjungan kj On kj.Kd_Pasien=g.Kd_pasien And kj.Kd_Unit=g.Kd_Unit     And kj.Tgl_Masuk=g.Tgl_Transaksi And kj.Urut_Masuk=g.Urut_Masuk        
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=kj.Kd_Customer
															LEFT JOIN Pasien_inap i On i.kd_kasir =g.Kd_kasir and i.no_transaksi = g.no_transaksi
															LEFT JOIN Detail_Bayar db On g.KD_kasir=db.KD_kasir And g.No_Transaksi=db.No_Transaksi
														WHERE d.Flag < 2 
														And v.Tag_Char in  ('20','21','27','28','29','31')  
															AND u.Parent in ($kd_unit) 
															And dk.no_kamar in ($tmpKdKamar)
															and Ktr.Jenis_cust in($kd_kelpas)  
															And Ktr.kd_Customer in ($k_customer)
															and kp.kd_klas in ($k_kelas_produk)
															AND db.KD_PAY in ($tmpKdPay)
															and o.kd_dokter in($tmpKdDokter)
															and db.kd_user in($tmpOperator)
															$k_tgl_pasien_plg
														) x 
													group by  x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,x.Tag_Char,x.jp
													Union All
													select '' as NAMA_KAMAR,'' as kelas ,x.KD_DOKTER ,x.NAMA as dokter ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK,
															CASE WHEN x.Tag_Char = '20' THEN x.JP ELSE 0 END as A1 , CASE WHEN x.Tag_Char = '21' THEN x.JP    ELSE 0 END as A2 , CASE WHEN x.Tag_Char = '27' THEN x.JP    ELSE 0 END as A3, 
															CASE WHEN x.Tag_Char = '29' THEN x.JP    ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp    ELSE 0 END as A5 , CASE WHEN x.Tag_Char = '31' THEN x.JP    ELSE 0 END as A6, 
															sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien, '15'  as Pajak
													from 
														(
														select   x.Kelas,dok.KD_DOKTER ,dok.NAMA as nama ,pro.KD_KLAS , pro.DESKRIPSI,pro.KD_PRODUK , dt.qty , vd.JP as JP ,
															CASE WHEN vd.Tag_Char = '20' THEN dt.Qty*vd.JP ELSE 0 END as JP20, CASE WHEN vd.Tag_Char = '21' THEN dt.Qty*vd.JP ELSE 0 END as JP21, CASE WHEN vd.Tag_Char = '27' THEN dt.Qty*vd.JP ELSE 0 END as JP27, 
															CASE WHEN vd.Tag_Char = '28' THEN dt.Qty*vd.JP ELSE 0 END as JP28, CASE WHEN vd.Tag_Char = '29' THEN dt.Qty*vd.JP ELSE 0 END as JP29, CASE WHEN vd.Tag_Char = '31' THEN dt.Qty*vd.JP ELSE 0 END as JP31, 
															vd.Tag_Char,t.kd_pasien
														from DETAIL_TRANSAKSI dt
															inner join VISITE_DOKTER vd on vd.KD_KASIR = dt.KD_KASIR and vd.NO_TRANSAKSI = dt.NO_TRANSAKSI and vd.URUT = dt.URUT
															inner join TRANSAKSI t on t.KD_KASIR = dt.KD_KASIR and t.NO_TRANSAKSI = dt.NO_TRANSAKSI
															inner join DOKTER dok on dok.KD_DOKTER = vd.KD_DOKTER
															inner join PRODUK pro on pro.KD_PRODUK = dt.KD_PRODUK
															Inner Join
															(
																select  u.NAMA_UNIT as Kelas,dt.kd_unit_tr,dt.KD_UNIT,dt.NO_FAKTUR  
																from TRANSAKSI  t
																	inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR= t.KD_KASIR and dt.NO_TRANSAKSI = t.NO_TRANSAKSI
																	inner join UNIT u on t.KD_UNIT = u.KD_UNIT
																where t.KD_KASIR = '02' 
																	AND t.Tgl_CO BETWEEN '$tgl_awal' AND '$tgl_akhir'
																	AND u.Parent in ($kd_unit)
																	and dt.KD_UNIT in ($tmpKdKamar) 
															) x on x.KD_UNIT = t.KD_UNIT  and x.NO_FAKTUR= t.NO_TRANSAKSI
														WHERE dt.Flag < 2  
															And vd.Tag_Char in  ('20','21','27','28','29','31') and vd.KD_KASIR = '30'
															and dok.kd_Dokter in($tmpKdDokter)
															and dt.kd_user in($tmpOperator)
														) x
													group by x.KD_DOKTER ,x.NAMA  ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK , x.TAG_CHAR,x.JP
												) z
												ORDER BY Dokter

												")->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
	
	if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tbody>
						<tr>
							<th style='.$font_style.' colspan="13">Laporan Jasa Pelayanan Dokter Per Tindakan</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
					</tbody>
				</table><br>';
		$html.='<table class="t1" border = "1" style="overflow: wrap" cellpadding="4">
					<thead>
					  <tr>
						<th align="center" width="40" >No</td>
						<th align="center" >Tindakan</th>
						<th align="center" width="70">Jumlah Pasien</th>
						<th align="center" width="70">Jumlah Produk</th>
						<th align="center" width="70">Dokter</th>
						<th align="center" width="70">Perawat</th>
						<th align="center" width="70">Operator</th>
						<th align="center" width="70">Anasthesi</th>
						<th align="center" width="70"> Ass. Operator</th>
						<th align="center" width="70"> Ass. Anasthesi</th>
						<th align="center" width="70">Jumlah</th>
						<th align="center" width="70">Pajak</th>
						<th align="center" width="70"> Diterima</th>
					  </tr>
					</thead><tbody>';
					

		if(count($query)>0){
			$no=1;
			foreach ($query as $line){
				#AGAR NAMA DOKTER TIDAK TAMPIL BERULANG
				// if($nama_dokter!= $line->dokter){
				// $html.='<tr>
							// <td align="center">'.$no.'</td>
							// <td>'.$line->dokter.'</td>
							// <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						// </tr>';
						// $no++;
				// }
				$nama_dokter=$line->dokter;
				$html.='<tr>
							<td align="center">'.$no.'</td>
							<td>'.$line->dokter.'</td>
							<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						</tr>';
						$no++;
				$query_body = $this->db->query("select * from(
													SELECT x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,  
														CASE WHEN x.Tag_Char = '20' THEN x.JP  ELSE 0 END as A1, CASE WHEN x.Tag_Char = '21' THEN x.JP  ELSE 0 END as A2, CASE WHEN x.Tag_Char = '27' THEN x.JP  ELSE 0 END as A3 , 
														CASE WHEN x.Tag_Char = '29' THEN x.JP  ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp  ELSE 0 END as A5, CASE WHEN x.Tag_Char = '31' THEN x.JP  ELSE 0 END as A6 ,  
														sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien,	'15'  as Pajak 
													From 
														(SELECT d.Flag, k.NAMA_KAMAR,u.NAMA_UNIT as Kelas,v.Kd_Dokter, o.Nama as Dokter, g.kd_pasien,p.Kd_Klas, p.Deskripsi, p.Kd_Produk, d.Qty, v.JP as JP , 
															CASE WHEN v.Tag_Char = '20' THEN d.Qty*v.JP ELSE 0 END as JP20, CASE WHEN v.Tag_Char = '21' THEN d.Qty*v.JP ELSE 0 END as JP21, CASE WHEN v.Tag_Char = '27' THEN d.Qty*v.JP ELSE 0 END as JP27, 
															CASE WHEN v.Tag_Char = '28' THEN d.Qty*v.JP ELSE 0 END as JP28 ,CASE WHEN v.Tag_Char = '29' THEN d.Qty*v.JP ELSE 0 END as  JP29, CASE WHEN v.Tag_Char = '31' THEN d.Qty*v.JP ELSE 0 END as JP31, v.Tag_Char 
														FROM (((((Transaksi g INNER JOIN (Detail_Transaksi d 
															LEFT JOIN (Detail_Tr_Kamar dk 
																	INNER JOIN Kamar k ON dk.No_Kamar=k.No_Kamar AND dk.kd_Unit=k.kd_Unit) 
																ON d.kd_kasir=dk.kd_kasir AND d.No_Transaksi=dk.No_Transaksi AND d.urut=dk.urut AND d.Tgl_Transaksi=dk.Tgl_Transaksi) ON g.Kd_Kasir = d.Kd_Kasir AND g.No_Transaksi = d.No_Transaksi) 
															INNER JOIN Pasien a ON g.Kd_Pasien = a.Kd_Pasien) 
															LEFT JOIN Visite_Dokter v ON d.Kd_Kasir = v.Kd_Kasir AND d.No_Transaksi = v.No_Transaksi AND d.Urut = v.Urut AND d.Tgl_Transaksi = v.Tgl_Transaksi) 
															INNER JOIN Dokter o ON v.Kd_Dokter = o.Kd_Dokter) 
															INNER JOIN Produk p ON d.Kd_Produk = p.Kd_Produk  
															INNER JOIN Klas_Produk KP On p.kd_klas=kp.kd_klas ) 
															INNER JOIN Unit u ON k.Kd_Unit = u.Kd_Unit 
															INNER JOIN  Kunjungan kj On kj.Kd_Pasien=g.Kd_pasien And kj.Kd_Unit=g.Kd_Unit     And kj.Tgl_Masuk=g.Tgl_Transaksi And kj.Urut_Masuk=g.Urut_Masuk        
															LEFT JOIN Kontraktor ktr On ktr.kd_Customer=kj.Kd_Customer
															LEFT JOIN Pasien_inap i On i.kd_kasir =g.Kd_kasir and i.no_transaksi = g.no_transaksi
															LEFT JOIN Detail_Bayar db On g.KD_kasir=db.KD_kasir And g.No_Transaksi=db.No_Transaksi
														WHERE d.Flag < 2 
														And v.Tag_Char in  ('20','21','27','28','29','31')  
															AND u.Parent in ($kd_unit) 
															AND d.Tgl_Transaksi BETWEEN '$tgl_awal' AND '$tgl_akhir'
															And dk.no_kamar in ($tmpKdKamar)
															and Ktr.Jenis_cust in($kd_kelpas)  
															And Ktr.kd_Customer in ($k_customer)
															and kp.kd_klas in ($k_kelas_produk)
															AND db.KD_PAY in ($tmpKdPay)
															and o.kd_dokter in($tmpKdDokter)
															and db.kd_user in($tmpOperator)
															$k_tgl_pasien_plg
														) x 
													group by  x.NAMA_KAMAR,x.Kelas,x.Kd_Dokter, x.Dokter, x.Kd_Klas, x.Deskripsi, x.Kd_Produk,x.Tag_Char,x.jp
													Union All
													select '' as NAMA_KAMAR,'' as kelas ,x.KD_DOKTER ,x.NAMA as dokter ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK,
															CASE WHEN x.Tag_Char = '20' THEN x.JP ELSE 0 END as A1 , CASE WHEN x.Tag_Char = '21' THEN x.JP    ELSE 0 END as A2 , CASE WHEN x.Tag_Char = '27' THEN x.JP    ELSE 0 END as A3, 
															CASE WHEN x.Tag_Char = '29' THEN x.JP    ELSE 0 END as A4, CASE WHEN x.Tag_Char = '28' THEN x.jp    ELSE 0 END as A5 , CASE WHEN x.Tag_Char = '31' THEN x.JP    ELSE 0 END as A6, 
															sum(x.qty) as Qty, count(x.kd_pasien) as kd_pasien, '15'  as Pajak
													from 
														(
														select   x.Kelas,dok.KD_DOKTER ,dok.NAMA as nama ,pro.KD_KLAS , pro.DESKRIPSI,pro.KD_PRODUK , dt.qty , vd.JP as JP ,
															CASE WHEN vd.Tag_Char = '20' THEN dt.Qty*vd.JP ELSE 0 END as JP20, CASE WHEN vd.Tag_Char = '21' THEN dt.Qty*vd.JP ELSE 0 END as JP21, CASE WHEN vd.Tag_Char = '27' THEN dt.Qty*vd.JP ELSE 0 END as JP27, 
															CASE WHEN vd.Tag_Char = '28' THEN dt.Qty*vd.JP ELSE 0 END as JP28, CASE WHEN vd.Tag_Char = '29' THEN dt.Qty*vd.JP ELSE 0 END as JP29, CASE WHEN vd.Tag_Char = '31' THEN dt.Qty*vd.JP ELSE 0 END as JP31, 
															vd.Tag_Char,t.kd_pasien
														from DETAIL_TRANSAKSI dt
															inner join VISITE_DOKTER vd on vd.KD_KASIR = dt.KD_KASIR and vd.NO_TRANSAKSI = dt.NO_TRANSAKSI and vd.URUT = dt.URUT
															inner join TRANSAKSI t on t.KD_KASIR = dt.KD_KASIR and t.NO_TRANSAKSI = dt.NO_TRANSAKSI
															inner join DOKTER dok on dok.KD_DOKTER = vd.KD_DOKTER
															inner join PRODUK pro on pro.KD_PRODUK = dt.KD_PRODUK
															Inner Join
															(
																select  u.NAMA_UNIT as Kelas,dt.kd_unit_tr,dt.KD_UNIT,dt.NO_FAKTUR  
																from TRANSAKSI  t
																	inner join DETAIL_TRANSAKSI dt on dt.KD_KASIR= t.KD_KASIR and dt.NO_TRANSAKSI = t.NO_TRANSAKSI
																	inner join UNIT u on t.KD_UNIT = u.KD_UNIT
																where t.KD_KASIR = '02' 
																	AND t.Tgl_CO BETWEEN '$tgl_awal' AND '$tgl_akhir'
																	AND u.Parent in ($kd_unit)
																	and dt.KD_UNIT in ($tmpKdKamar) 
																	
															) x on x.KD_UNIT = t.KD_UNIT  and x.NO_FAKTUR= t.NO_TRANSAKSI
														WHERE 	dt.Flag < 2  
																And vd.Tag_Char in  ('20','21','27','28','29','31') and vd.KD_KASIR = '30'
																and dok.kd_Dokter in($tmpKdDokter)
																and dt.kd_user in($tmpOperator)
														) x
													group by x.KD_DOKTER ,x.NAMA  ,x.KD_KLAS , x.DESKRIPSI,x.KD_PRODUK , x.TAG_CHAR,x.JP
												) z where dokter='$nama_dokter'
												ORDER BY Dokter, Kd_Dokter, Kd_Klas, Deskripsi, Kd_Produk 

												")->result();
				$tot_pasien=0;
				$tot_qty=0;
				$tot_a1=0;
				$tot_a2=0;
				$tot_a3=0;
				$tot_a4=0;
				$tot_a5=0;
				$tot_a6=0;
				$tot_jumlah=0;
				$tot_pajak=0;
				$tot_diterima=0;
				foreach($query_body as $line2){
					$jumlah=$line2->a1 + $line2->a2 + $line2->a3 + $line2->a4 +$line2->a5 + $line2->a6;
					$diterima = $jumlah - (($jumlah*$line2->pajak)/100);
					$html.='<tr>
							<td></td>
							<td>'.$line2->deskripsi.'</td>
							<td align="right">'.$line2->kd_pasien.'</td>
							<td align="right">'.$line2->qty.'</td>
							<td align="right">'.number_format($line2->a1,0,',',',').'</td>
							<td align="right">'.number_format($line2->a2,0,',',',').'</td>
							<td align="right">'.number_format($line2->a3,0,',',',').'</td>
							<td align="right">'.number_format($line2->a4,0,',',',').'</td>
							<td align="right">'.number_format($line2->a5,0,',',',').'</td>
							<td align="right">'.number_format($line2->a6,0,',',',').'</td>
							<td align="right">'.number_format($jumlah,0,',',',').'</td>
							<td align="right">'.$line2->pajak.'</td>
							<td align="right">'.number_format($diterima,0,',',',').'</td>
						</tr>';
					$tot_pasien=$tot_pasien + $line2->kd_pasien;
					$tot_qty=$tot_qty + $line2->qty;
					$tot_a1=$tot_a1+$line2->a1;
					$tot_a2=$tot_a2+$line2->a2;
					$tot_a3=$tot_a3+$line2->a3;
					$tot_a4=$tot_a4+$line2->a4;
					$tot_a5=$tot_a5+$line2->a5;
					$tot_a6=$tot_a6+$line2->a6;
					$tot_jumlah=$tot_jumlah + $jumlah;
					$tot_pajak=$tot_pajak + $line2->pajak;
					$tot_diterima=$tot_diterima + $diterima;
				}
				
				
				$html.='<tr>
							<td></td>
							<td ><b>Subtotal</b></td>
							<td align="right"><b>'.number_format($tot_pasien,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_qty,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a1,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a2,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a3,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a4,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a5,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a6,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_jumlah,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_pajak,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_diterima,0,',',',').'</b></td>
				</tr>';
				//$nama_dokter = $line->dokter;
			}
		}else{
			$html.='<tr><td align="center" colspan="13">Data Tidak Ada</td></tr>';
		}
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		if($type_file == 1){
			$name='Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);
			echo $html;
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan',$html);
		
		}
	}
	
	function cetak_lap_PelDokterPerTindakan_fungsi(){
		$html='';
		
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit = $param->unit; 
		$kd_unit = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		$kd_unit= str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		$k_customer= str_replace("'","",$k_customer ); //menghilangkan kutip
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		$k_kelas_produk= str_replace("'","",$k_kelas_produk); //menghilangkan kutip
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false

		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		$tmpKdPay= str_replace("'","",$tmpKdPay); //menghilangkan kutip
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		
		$tmpKdKamar= str_replace("'","",$tmpKdKamar); //menghilangkan kutip
		
		#KRITERIA DOKTER
		$dokter = $param->dokter; //tanpa kutip
		$tmpKdDokter='';
		if($dokter == 'Semua' || $dokter == '999'){
			$get_kd_dokter= $this->db->query("SELECT kd_dokter,nama FROM Dokter WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003')) ")->result();
			$k_kd_dokter ='';
			$i=1;
			foreach($get_kd_dokter as $line){
				if($i==1){
					$k_kd_dokter="'".$line->kd_dokter."'";
				}else{
					$k_kd_dokter=$k_kd_dokter.","."'".$line->kd_dokter."'";
				}
				$i++;
			}
			$tmpKdDokter=$k_kd_dokter;
		}else{
			$tmpKdDokter = "'$dokter'";
		}
		$tmpKdDokter= str_replace("'","",$tmpKdDokter); //menghilangkan kutip
		
		#KRITERIA OPERATOR
		$operator = $param->operator; //tanpa kutip
		$tmpOperator ='';
		if($operator == 'Semua' || $operator == '9999'){
			$get_kd_user= $this->db->query( "SELECT DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
													From zUsers 
														inner join  zMember ON zmember.kd_user=zuserS.kd_user 
														inner join  zGroups on zmember.group_id=zgroups.group_id 
														inner join zTrustee on zgroups.group_id =ztrustee.group_id 
														inner join zModule On ztrustee.mod_id=zmodule.mod_id 
											")->result();
			$k_kd_user ='';
			$i=1;
			foreach($get_kd_user as $line){
				if($i==1){
					$k_kd_user="'".$line->kd_user."'";
				}else{
					$k_kd_user=$k_kd_user.","."'".$line->kd_user."'";
				}
				$i++;
			}
			$tmpOperator=$k_kd_user;
		}else{
			$tmpOperator="'$operator'";
		}
		$tmpOperator= str_replace("'","",$tmpOperator); //menghilangkan kutip
		
		
		#PEMANGGILAN FUNGSI LAPORAN
		
		$kriteria="SELECT * from rwi_lap_jasa_pel_dokter_pertindakan_revisi 
									(
										'{".$kd_unit."}',
										'{".$tmpKdKamar."}',
										'{".$kd_kelpas."}',
										'{".$k_customer."}',
										'{".$k_kelas_produk."}',
										'{".$tmpKdPay."}',
										'{".$tmpOperator."}',
										'{".$tmpKdDokter."}',
										'{".$tgl_awal."}',
										'{".$tgl_akhir."}',
										{$pasien_pulang}
									) order by kd_unit asc";
		$query = $this->db->query($kriteria)->result();
		$kriteria2="select distinct(kd_unit) from rwi_lap_jasa_pel_dokter_pertindakan_revisi
									(
										'{".$kd_unit."}',
										'{".$tmpKdKamar."}',
										'{".$kd_kelpas."}',
										'{".$k_customer."}',
										'{".$k_kelas_produk."}',
										'{".$tmpKdPay."}',
										'{".$tmpOperator."}',
										'{".$tmpKdDokter."}',
										'{".$tgl_awal."}',
										'{".$tgl_akhir."}',
										{$pasien_pulang}
									) order by kd_unit asc";
		$queryHeader = $this->db->query($kriteria2)->result();
		//echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		
		$arr_nama_dokter=array();
		$t_nama_dokter='';
		$i_t=0; //indeks array nama dokter
		foreach ($query as $line3){
			if($t_nama_dokter != $line3->dokter){
				$arr_nama_dokter[$i_t]=$line3->dokter;
				$i_t++;
			}
			$t_nama_dokter=$line3->dokter;
		}
		
		// print_r($arr_nama_dokter);
		if($type_file == 1){
			$font_style="font-size:16px";
			$font_style2="font-size:15px";
		}else{
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
					<tr>
							<th style='.$font_style.' colspan="13">Laporan Jasa Pelayanan Dokter Per Tindakan</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">'.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="13">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
					
				</table><br>';
		$html.='<table class="t1" border = "1" cellpadding="4">
					<thead>
					  <tr>
						<th align="center" width="40" >No</th>
						<th align="center" >Tindakan</th>
						<th align="center" width="70">Jumlah Pasien</th>
						<th align="center" width="70">Jumlah Produk</th>
						<th align="center" width="70">Dokter</th>
						<th align="center" width="70">Perawat</th>
						<th align="center" width="70">Operator</th>
						<th align="center" width="70">Anasthesi</th>
						<th align="center" width="70"> Ass. Operator</th>
						<th align="center" width="70"> Ass. Anasthesi</th>
						<th align="center" width="70">Jumlah</th>
						<th align="center" width="70">Pajak</th>
						<th align="center" width="70"> Diterima</th>
					  </tr>
					</thead>'; 
					

		$baris=0;
		$baris_khusus = array();
		$x 	= 0;
		if(count($arr_nama_dokter)>0){
			$no=1;
			foreach($arr_nama_dokter as $nama_dokter){
				$baris++;
				$html.='<tr>
						<td align="center">'.$no.'</td>
						<td>'.$nama_dokter.'</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>';
				$tot_pasien=0;
				$tot_qty=0;
				$tot_a1=0;
				$tot_a2=0;
				$tot_a3=0;
				$tot_a4=0;
				$tot_a5=0;
				$tot_a6=0;
				$tot_jumlah=0;
				$tot_pajak=0;
				$tot_diterima=0;
				foreach ($queryHeader as $data_head) {
					unset($paramsCriteria);
					$paramsCriteria = array(
						'field_criteria' 	=> "kd_unit",
						'value_criteria' 	=> $data_head->kd_unit,
						'table' 			=> 'unit',
						'field_return' 		=> null,
					);
					$queryUnit = $this->Tbl_data_transaksi->getCustom($paramsCriteria);

					foreach($query as $resultQuery){
						if ($resultQuery->kd_unit == $data_head->kd_unit && $resultQuery->dokter == $nama_dokter) {
							$html.='<tr>
								<td></td>
								<td colspan="12">'.$queryUnit->row()->nama_unit.'</td>
							</tr>';
							break;
						}
					}

					foreach($query as $line2){
						if($nama_dokter == $line2->dokter && $line2->kd_unit == $data_head->kd_unit){
							$jumlah=$line2->a1 + $line2->a2 + $line2->a3 + $line2->a4 +$line2->a5 + $line2->a6;
							$diterima = $jumlah - (($jumlah*$line2->pajak)/100);
							
							$html.='<tr>
								<td></td>
								<td> - '.$line2->deskripsi.'</td>
								<td align="right">'.$line2->kd_pasien.'</td>
								<td align="right">'.$line2->qty.'</td>
								<td align="right">'.number_format($line2->a1,0,',',',').'</td>
								<td align="right">'.number_format($line2->a2,0,',',',').'</td>
								<td align="right">'.number_format($line2->a3,0,',',',').'</td>
								<td align="right">'.number_format($line2->a4,0,',',',').'</td>
								<td align="right">'.number_format($line2->a5,0,',',',').'</td>
								<td align="right">'.number_format($line2->a6,0,',',',').'</td>
								<td align="right">'.number_format($jumlah,0,',',',').'</td>
								<td align="right">'.$line2->pajak.'</td>
								<td align="right">'.number_format($diterima,0,',',',').'</td>
							</tr>';
							$tot_pasien=$tot_pasien + $line2->kd_pasien;
							$tot_qty=$tot_qty + $line2->qty;
							$tot_a1=$tot_a1+$line2->a1;
							$tot_a2=$tot_a2+$line2->a2;
							$tot_a3=$tot_a3+$line2->a3;
							$tot_a4=$tot_a4+$line2->a4;
							$tot_a5=$tot_a5+$line2->a5;
							$tot_a6=$tot_a6+$line2->a6;
							$tot_jumlah=$tot_jumlah + $jumlah;
							$tot_pajak=$tot_pajak + $line2->pajak;
							$tot_diterima=$tot_diterima + $diterima;
							$baris++;
						}
					}
				}
				$html.='<tr>
							<td></td>
							<td ><b>Subtotal</b></td>
							<td align="right"><b>'.number_format($tot_pasien,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_qty,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a1,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a2,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a3,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a4,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a5,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_a6,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_jumlah,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_pajak,0,',',',').'</b></td>
							<td align="right"><b>'.number_format($tot_diterima,0,',',',').'</b></td>
				</tr>';
				$no++;
				$baris++;
				$baris_khusus[$x] = (int)$baris+7+$x;
				$x++;
			}
		}else{
			$html.='<tr><td align="center" colspan="13">Data Tidak Ada</td></tr>';
			$baris++;
		}
		$html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:M'.$baris;
		$area_wrap='A6:M'.$baris;
		$area_kanan='C7:M'.$baris;
		if($type_file == 1){

			$name='Laporan_jasa_pelayanan_dokter_pertindakan.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			die();
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:M6')->applyFromArray($styleArrayHead);
			
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:M6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:M5')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:M'.$baris)
						->getNumberFormat()
					    ->setFormatCode($textFormat);
			/*for ($i=0; $i < $x; $i++) { 
				$styleArrayBold = array(
					'font'  => array(
						'bold'  => true,
						'size'  => 9,
						'name'  => 'Courier New'
					));
				$objPHPExcel->getActiveSheet()->getStyle('A'.$baris_khusus[$i].':M'.$baris_khusus[$i])->applyFromArray($styleArrayBold);
			}*/
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan_Jasa_Pelayanan_Dokter_Per_Tindakan',$html);
		
		} }

		
	function cetak_lap_rekap_arus_pend(){
		$html='';
		
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit = $param->unit; 
		$kd_unit = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		$kd_unit= str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		$k_customer= str_replace("'","",$k_customer ); //menghilangkan kutip
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		$k_kelas_produk= str_replace("'","",$k_kelas_produk); //menghilangkan kutip
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false
		if ($pasien_pulang=='true'){
			$status_pulang=1;
		}else{
			$status_pulang=0;
		}
		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		$tmpKdPay= str_replace("'","",$tmpKdPay); //menghilangkan kutip
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		
		$tmpKdKamar= str_replace("'","",$tmpKdKamar); //menghilangkan kutip
		
		
		#KRITERIA SPESIALISASI
		$arrayDataSpesialis = $param->kd_spesialis;
		
		$tmpKdSpesialis='';
		for ($i=0; $i < count($arrayDataSpesialis); $i++) { 
			$tmpKdSpesialis .= "'".$arrayDataSpesialis[$i][0]."',";
		}
		$tmpKdSpesialis = substr($tmpKdSpesialis, 0, -1); //arr kd pay
		
		$semua_spesialis="'"."999"."'";
		if($tmpKdSpesialis==$semua_spesialis ){ //semua kd_pay
			$get_kd_spesialis= $this->db->query("SELECT kd_spesial, spesialisasi FROM spesialisasi")->result();
			$k_kd_spesialis ='';
			$i=1;
			foreach($get_kd_spesialis as $line){
				if($i==1){
					$k_kd_spesialis="'".$line->kd_spesial."'";
				}else{
					$k_kd_spesialis=$k_kd_spesialis.","."'".$line->kd_spesial."'";
				}
				$i++;
			}
			$tmpKdSpesialis=$k_kd_spesialis;
		}
		$tmpKdSpesialis= str_replace("'","",$tmpKdSpesialis);
		
		$arr_explode_spesialis=explode(",",$tmpKdSpesialis);
		// print_r($arr_explode_spesialis);
		$i=1;
		$nama_spesialis ='';
		
		foreach ($arr_explode_spesialis as $line){
			$nm_spesialis= $this->db->query("SELECT spesialisasi FROM spesialisasi where kd_spesial=$line")->result();
			foreach ($nm_spesialis as $line2){
				$NS =$line2->spesialisasi;
			}
			if($i==1){
				$nama_spesialis=$NS;
			}else{
				$nama_spesialis=$nama_spesialis.", ".$NS;
			}
			$i++;
		}
		
		// echo $nama_spesialis;
		
		$kriteria="select * from rwi_lap_rekap_arus_pend_pasien 
									(
										'{".$tgl_awal."}',
										'{".$tgl_akhir."}',
										'{".$kd_unit."}',
										'{".$k_kelas_produk."}',
										'{".$kd_kelpas."}',
										'{".$k_customer."}',
										'{".$tmpKdSpesialis."}',
										'{".$tmpKdKamar."}',
										'{".$tmpKdPay."}',
										{$status_pulang}
									)";
		$query = $this->db->query($kriteria)->result();
		// echo '{success:true, totalrecords:'.count($query).', listData:'.json_encode($query).'}';
		if($type_file == 1){
			$font_style="font-size:13px";
			$font_style2="font-size:12px";
		}else{
			$font_style="font-size:11px";
			$font_style2="font-size:10px";
		}
		$font_style3="font-size:8px";
		$html.='<table  cellspacing="0" cellpadding="4" border="0">
						<tr>
							<th style='.$font_style.' colspan="29">REKAPITULASI ARUS PENDAPATAN RAWAT INAP PER PASIEN</th>
						</tr>
						<tr>
							<th style='.$font_style.' colspan="29">PEMBANTU KASIR PENERIMA UANG</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="29">Periode '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="29">Kelompok Pasien :'.$nama_kel_pas.'('.$customer.')</th>
						</tr>
						<tr>
							<th style='.$font_style2.' colspan="29">Spesialisasi : '.$nama_spesialis.'</th>
						</tr>
				</table><br>';
		$html.='<table class="t1" border = "1" style='.$font_style3.' cellpadding="4" >
					<thead>
					  <tr>
						<th align="center" rowspan="2" width="20" >No</th>
						<th align="center" rowspan="2">No Medrec</th>
						<th align="center" rowspan="2">Nama Pasien</th>
						<th align="center" rowspan="2">Kamar</th>
						<th align="center" rowspan="2">Makan</th>
						<th align="center" rowspan="2">Visite</th>
						<th align="center" rowspan="2">Operatif</th>
						<th align="center" rowspan="2">Non Op</th>
						<th align="center" rowspan="2">Rad</th>
						<th align="center" rowspan="2">Lab PK</th>
						<th align="center" rowspan="2">Lab PA</th>
						<th align="center" rowspan="2">HD</th>
						<th align="center" rowspan="2">IRM</th>
						<th align="center" rowspan="2">IRD</th>
						<th align="center" rowspan="2">RWJ</th>
						<th align="center" rowspan="2">ADM</th>
						<th align="center" rowspan="2">BHP</th>
						<th align="center" rowspan="2">Farmasi</th>
						<th align="center" rowspan="2">O2</th>
						<th align="center" rowspan="2">Gizi</th>
						<th align="center" rowspan="2">ECG</th>
						<th align="center" rowspan="2" >Transfusi</th>
						<th align="center" rowspan="2">EEG</th>
						<th align="center" rowspan="2">Total POP</th>
						<th align="center" colspan="2">Pembayaran</th>
						<th align="center" colspan="2">Piutang</th>
						<th align="center" rowspan="2">Keterangan (+/-)/Penanggung</th>
					  </tr>
					  <tr>
						<th align="center" >Jml Kwitansi</th>
						<th align="center" >Jml Bayar</th>
						<th align="center" >Jml Pasien</th>
						<th align="center" >Tot Tagihan</th>
					  </tr>
					</thead>';
		$no=1;
		$tot_kamar=0;
		$tot_konsumsi=0;
		$tot_visite_konsul=0;
		$tot_operatif=0;
		$tot_nonoperatif=0;
		$tot_radiologi=0;
		$tot_laboratoriumpk=0;
		$tot_laboratoriumpa=0;
		$tot_hd=0;
		$tot_irm=0;
		$tot_ird=0;
		$tot_rwj=0;
		$tot_adm=0;
		$tot_bhp=0;
		$tot_obat=0;
		$tot_o2=0;
		$tot_gizi=0;
		$tot_ecg=0;
		$tot_transfusi=0;
		$tot_eeg=0;
		$tot_total_pop=0;
		$tot_jml_kw=0;
		$tot_jml_byr=0;
		$tot_jml_pasien=0;
		$tot_jml_tagihan=0;
		$baris=0;
		if(count($query)==0){
			$html.='<tr><td colspan="29">Data Tidak Ada</td></tr>';
		}else{
		foreach ($query as $line){
			$jml_kw=1;
			$jml_pasien=1;
			$baris++;
			$total_pop = $line->kamar+$line->konsumsi + $line->visite_konsul + $line->operatif + $line->nonoperatif 
						 + $line->radiologi + $line->laboratoriumpk + $line->laboratoriumpa+  $line->hd 
						+ $line->irm + $line->ird + $line->rwj + $line->adm + $line->bhp + $line->obat+ $line->o2 + $line->gizi+ $line->ecg +$line->transfusi +$line->eeg;
			$tagihan = $total_pop - $line->bayar;	
		
			$html.='<tr>
						<td>'.$no.'</td>
						<td>'.$line->kd_pasien.'</td>
						<td>'.$line->nama_pasien.'</td>
						<td align="right">'.number_format($line->kamar,0,',',',').'</td>
						<td align="right">'.number_format($line->konsumsi,0,',',',').'</td>
						<td align="right">'.number_format($line->visite_konsul,0,',',',').'</td>
						<td align="right">'.number_format($line->operatif,0,',',',').'</td>
						<td align="right">'.number_format($line->nonoperatif,0,',',',').'</td>
						<td align="right">'.number_format($line->radiologi,0,',',',').'</td>
						<td align="right">'.number_format($line->laboratoriumpk,0,',',',').'</td>
						<td align="right">'.number_format($line->laboratoriumpa,0,',',',').'</td>
						<td align="right">'.number_format($line->hd,0,',',',').'</td>
						<td align="right">'.number_format($line->irm,0,',',',').'</td>
						<td align="right">'.number_format($line->ird,0,',',',').'</td>
						<td align="right">'.number_format($line->rwj,0,',',',').'</td>
						<td align="right">'.number_format($line->adm,0,',',',').'</td>
						<td align="right">'.number_format($line->bhp,0,',',',').'</td>
						<td align="right">'.number_format($line->obat,0,',',',').'</td>
						<td align="right">'.number_format($line->o2,0,',',',').'</td>
						<td align="right">'.number_format($line->gizi,0,',',',').'</td>
						<td align="right">'.number_format($line->ecg,0,',',',').'</td>
						<td align="right">'.number_format($line->transfusi,0,',',',').'</td>
						<td align="right">'.number_format($line->eeg,0,',',',').'</td>
						<td align="right">'.number_format($total_pop,0,',',',').'</td>
						<td align="right">'.number_format($jml_kw,0,',',',').'</td>
						<td align="right">'.number_format($line->bayar,0,',',',').'</td>
						<td align="right">'.number_format($jml_pasien,0,',',',').'</td>
						<td align="right">'.number_format($tagihan,0,',',',').'</td>
						<td></td>
					</tr>';
			$no++; 
			$tot_kamar=$tot_kamar + $line->kamar;
			$tot_konsumsi=$tot_konsumsi + $line->konsumsi;
			$tot_visite_konsul=$tot_visite_konsul + $line->visite_konsul;
			$tot_operatif=$tot_operatif + $line->operatif;
			$tot_nonoperatif=$tot_nonoperatif + $line->nonoperatif;
			$tot_radiologi=$tot_radiologi + $line->radiologi;
			$tot_laboratoriumpk=$tot_laboratoriumpk + $line->laboratoriumpk;
			$tot_laboratoriumpa=$tot_laboratoriumpa + $line->laboratoriumpa;
			$tot_hd=$tot_hd + $line->hd;
			$tot_irm=$tot_irm + $line->irm;
			$tot_ird=$tot_ird + $line->ird;
			$tot_rwj=$tot_rwj + $line->rwj;
			$tot_adm=$tot_adm + $line->adm;
			$tot_bhp=$tot_bhp + $line->bhp;
			$tot_obat=$tot_obat + $line->obat;
			$tot_o2=$tot_o2 + $line->o2;
			$tot_gizi=$tot_gizi + $line->gizi;
			$tot_ecg=$tot_ecg + $line->ecg;
			$tot_transfusi=$tot_transfusi + $line->transfusi;
			$tot_eeg=$tot_eeg + $line->eeg;
			$tot_total_pop=$tot_total_pop + $total_pop;
			$tot_jml_kw=$tot_jml_kw + $jml_kw;
			$tot_jml_byr=$tot_jml_byr + $line->bayar;
			$tot_jml_pasien=$tot_jml_pasien + $jml_pasien;
			$tot_jml_tagihan=$tot_jml_tagihan + $tagihan;
		} 
		$html.='<tr>
					<td></td>
					<td></td>
					<td>TOTAL</td>
					<td align="right">'.number_format($tot_kamar,0,',',',').'</td>
					<td align="right">'.number_format($tot_konsumsi,0,',',',').'</td>
					<td align="right">'.number_format($tot_visite_konsul,0,',',',').'</td>
					<td align="right">'.number_format($tot_operatif,0,',',',').'</td>
					<td align="right">'.number_format($tot_nonoperatif,0,',',',').'</td>
					<td align="right">'.number_format($tot_radiologi,0,',',',').'</td>
					<td align="right">'.number_format($tot_laboratoriumpk,0,',',',').'</td>
					<td align="right">'.number_format($tot_laboratoriumpa,0,',',',').'</td>
					<td align="right">'.number_format($tot_hd,0,',',',').'</td>
					<td align="right">'.number_format($tot_irm,0,',',',').'</td>
					<td align="right">'.number_format($tot_ird,0,',',',').'</td>
					<td align="right">'.number_format($tot_rwj,0,',',',').'</td>
					<td align="right">'.number_format($tot_adm,0,',',',').'</td>
					<td align="right">'.number_format($tot_bhp,0,',',',').'</td>
					<td align="right">'.number_format($tot_obat,0,',',',').'</td>
					<td align="right">'.number_format($tot_o2,0,',',',').'</td>
					<td align="right">'.number_format($tot_gizi,0,',',',').'</td>
					<td align="right">'.number_format($tot_ecg,0,',',',').'</td>
					<td align="right">'.number_format($tot_transfusi,0,',',',').'</td>
					<td align="right">'.number_format($tot_eeg,0,',',',').'</td>
					<td align="right">'.number_format($tot_total_pop,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_kw,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_byr,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_pasien,0,',',',').'</td>
					<td align="right">'.number_format($tot_jml_tagihan,0,',',',').'</td>
					<td></td>
				</tr>';
		}
		// $html.='</table>';
		$prop=array('foot'=>true);
		$baris=$baris+15;
		$print_area='A1:AC'.$baris;
		$area_wrap='A4:AC'.$baris;
		$area_kanan='D10:AC'.$baris;
		if($type_file == 1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			// $html_='<table><tr><td>TEST</td></tr></table>';
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:AC9')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:AC8')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A8:AC9')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set ALIGNMENT 
			
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			# Fungsi AUTOSIZE
            // for ($col = 'A'; $col != 'P'; $col++) {
                // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            // }
			# END Fungsi AUTOSIZE
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(11);
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle('A5:AC5')
						->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_Rekapitulasi_Arus_Pendapatan_RWI.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			// echo $html;
			$common=$this->common;
			$this->common->setPdf('L','Laporan_Rekapitulasi_Arus_Pendapatan_RWI',$html);
		
		} 
	
	}

	/*
		PENAMBAHAN FUNGSI PRINT BARU PENERIMAAN PASIEN
		OLEH  	: HADAD AL GOJALI 
		TANGGAL : 2017 / 05 / 10
	*/

	function cetakLaporan_penerimaanPasien(){
		$common 	= $this->common;
   		$result 	= $this->result;
		$this->db->trans_begin();
		//$this->dbSQL->trans_begin();
		// $tmp_kamar = $this->input->post('kd_kamar');

		$param 		= json_decode($_POST['data']);
		$params = array(
			'kd_kamar' 		=> $param->kd_kamar,
			'kd_pay' 		=> $param->kd_pay,
			'pasien_pulang' => $param->pasien_pulang,
			'shift_1' 		=> $param->shift_1,
			'shift_2' 		=> $param->shift_2,
			'shift_3' 		=> $param->shift_3,
			'shift_all' 	=> $param->shift_all,
			'tgl_akhir' 	=> $param->tgl_akhir,
			'tgl_awal' 		=> $param->tgl_awal,
			'type_file' 	=> $param->type_file,
			'unit' 			=> $param->unit,
		);

		// print_r($params);
		$criteriaShift_4 = "";
		$criteriaShift 	 = "";
		if ($params['shift_all'] === true || $params['shift_all'] == "true") {
			$criteriaShift_4 = " and db.shift=4";
			$criteriaShift 	 = " and db.shift In (1,2,3)";
		}else if(($params['shift_1'] === true || $params['shift_1'] == "true") || ($params['shift_2'] === true || $params['shift_2'] == "true") || ($params['shift_3'] === true || $params['shift_3'] == "true")){
			$criteriaShift = " and db.shift In (";
			if ($params['shift_1'] === true || $params['shift_1'] == "true") {
				$criteriaShift .= "1,";
			}else if ($params['shift_2'] === true || $params['shift_2'] == "true") {
				$criteriaShift .= "2,";
			}else if ($params['shift_3'] === true || $params['shift_3'] == "true") {
				$criteriaShift .= "3,";
			}
			$criteriaShift = substr($criteriaShift, 0, strlen($criteriaShift)-1);
			$criteriaShift .= ")";
		}

		if (isset($params['unit']) && $params['unit']!="") {
			$params['unit'] = "AND u.Parent IN (".substr($params['unit'], 0, strlen($params['unit'])-1).")";
		}else{
			$params['unit'] = "";
		}

		if (isset($params['kd_pay']) && $params['kd_pay']!="") {
			$params['kd_pay'] = "AND db.kd_pay IN (".$params['kd_pay'].")";
		}else{
			unset($params['kd_pay']);
			$params['kd_pay'] = "";
		}
		//echo json_encode($params);
		$queryPG_header = $this->db->query("SELECT 
					DISTINCT(c.Customer)
					FROM ( 
						( 
							( 
								( 
								(Transaksi t INNER JOIN Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
								INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = k.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk 
								) 
								INNER JOIN Customer c ON k.Kd_Customer = c.Kd_Customer 
							) 
						) 
						INNER JOIN DETAIL_TR_BAYAR DTB ON DTB.KD_KASIR = D.KD_KASIR AND DTB.NO_TRANSAKSI = D.NO_TRANSAKSI AND DTB.TGL_TRANSAKSI = D.TGL_TRANSAKSI AND DTB.URUT = D.URUT 
					)
					INNER JOIN DETAIL_BAYAR DB ON DB.KD_KASIR = DTB.KD_KASIR AND DB.NO_TRANSAKSI = DTB.NO_TRANSAKSI AND DB.TGL_TRANSAKSI = DTB.TGL_BAYAR AND DB.URUT = DTB.URUT_BAYAR 
					INNER JOIN Unit u ON db.Kd_Unit = u.Kd_Unit WHERE d.Flag < 2 ".$params['kd_pay']." 
					".$params['unit']." 
					AND t.CO_Status = '".$params['pasien_pulang']."' 
					AND (( db.TGL_TRANSAKSI BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' ".$criteriaShift.") 
						            or (db.TGL_TRANSAKSI BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' ".$criteriaShift_4.")) 
					GROUP BY c.Customer, t.Kd_Kasir, t.No_Transaksi, t.Kd_Pasien"); 
		$queryPG_body = $this->db->query("SELECT 
							x.Customer as customer,
						    p.kd_pasien as kode_pasien, 
						    p.nama as nama_pasien, 
						    x.Kd_Pasien, 
						    x.tagihan as q_tagihan, 
						    y.A1 as q_subsidi, 
						    y.A2 as q_tunai, 
						    y.A3 as q_iur_bayar, 
						    y.A4 as q_perusahaan, 
						    y.A5 as q_raharja, 
						    y.A6 as q_bpjs, 
						    y.A7 as q_piutang, 
						    y.A8 as q_asuransi, 
						    y.* 
						FROM 
							(
						        (
						            SELECT 
						            	c.Customer, 
						            	t.Kd_Kasir, 
						            	t.No_Transaksi, 
						            	t.Kd_Pasien, 
						            	SUM(DTB.JUMLAH) as tagihan 
						           	FROM 
						            (
						                (
						                    (
						                        (
						                            (Transaksi t INNER JOIN Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi) 
						                        	INNER JOIN Kunjungan k ON t.Kd_Pasien = k.Kd_Pasien AND t.Tgl_Transaksi = k.Tgl_Masuk AND t.Kd_Unit = k.Kd_Unit AND t.Urut_Masuk = k.Urut_Masuk
						                    ) 
						                    INNER JOIN Customer c ON k.Kd_Customer = c.Kd_Customer
						                ) 
									) 
						            INNER JOIN DETAIL_TR_BAYAR DTB ON DTB.KD_KASIR = D.KD_KASIR 
						                AND DTB.NO_TRANSAKSI = D.NO_TRANSAKSI 
						                AND DTB.TGL_TRANSAKSI = D.TGL_TRANSAKSI  
						                AND DTB.URUT = D.URUT 
								)INNER JOIN DETAIL_BAYAR DB ON DB.KD_KASIR = DTB.KD_KASIR 
						            AND DB.NO_TRANSAKSI = DTB.NO_TRANSAKSI 
						            AND DB.TGL_TRANSAKSI = DTB.TGL_BAYAR 
						            AND DB.URUT = DTB.URUT_BAYAR 
							INNER JOIN Unit u ON db.Kd_Unit = u.Kd_Unit  WHERE d.Flag < 2 ".$params['kd_pay']." 
						".$params['unit']." 
						AND t.CO_Status = '".$params['pasien_pulang']."' 
						            AND (( db.TGL_TRANSAKSI BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' ".$criteriaShift.") 
						            or (db.TGL_TRANSAKSI BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' ".$criteriaShift_4.")
						) 
						            GROUP BY c.Customer, t.Kd_Kasir, t.No_Transaksi, t.Kd_Pasien) x 
						LEFT JOIN (SELECT t.Kd_Kasir, 
						           SUM(CASE WHEN py.Jenis_Pay = 6  ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A1, 
						           SUM(CASE WHEN py.Kd_Pay = 'TU'  ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A2, 
						           SUM(CASE WHEN py.Kd_Pay = 'IA'  ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A3,
						           SUM(CASE WHEN py.Jenis_Pay = 3  ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A4, 
						           SUM(CASE WHEN py.kd_pay='JS'  ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A5, 
						           SUM(CASE WHEN py.kd_pay in ('BJ','BP') ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A6, 
						           SUM(CASE WHEN py.Kd_Pay = 'PT' ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A7, 
						           SUM(CASE WHEN py.jenis_pay=4 and py.kd_pay not in ('JS','BJ','BP') ".$params['unit']." THEN dtb.jumlah ELSE 0 END) as A8, 
						t.No_Transaksi FROM (Payment py INNER JOIN Detail_Bayar db ON py.Kd_Pay = db.Kd_Pay) 
						INNER JOIN Transaksi t ON db.Kd_Kasir = t.Kd_Kasir AND db.No_Transaksi = t.No_Transaksi 
						INNER JOIN Detail_Transaksi d ON t.Kd_Kasir = d.Kd_Kasir AND t.No_Transaksi = d.No_Transaksi 
						INNER JOIN Unit u ON db.Kd_Unit = u.Kd_Unit  INNER JOIN DETAIL_TR_BAYAR DTB ON DTB.KD_KASIR = D.KD_KASIR AND DTB.NO_TRANSAKSI = D.NO_TRANSAKSI AND DTB.TGL_TRANSAKSI = D.TGL_TRANSAKSI  AND DTB.URUT = D.URUT  and DB.KD_KASIR = DTB.KD_KASIR AND DB.NO_TRANSAKSI = DTB.NO_TRANSAKSI AND DB.TGL_TRANSAKSI = DTB.TGL_BAYAR AND DB.URUT = DTB.URUT_BAYAR 
						WHERE d.Flag < 2 ".$params['kd_pay']." 
						".$params['unit']." 
						AND t.CO_Status = '".$params['pasien_pulang']."' 
						AND (( db.TGL_TRANSAKSI BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' ".$criteriaShift.") 
										or (db.TGL_TRANSAKSI BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' ".$criteriaShift_4."))
						GROUP BY t.Kd_Kasir, t.No_Transaksi) y ON x.Kd_Kasir = y.Kd_Kasir AND x.No_Transaksi = y.No_Transaksi) 
						INNER JOIN Pasien p ON x.Kd_Pasien = p.Kd_Pasien ORDER BY x.Customer, p.Nama");
		$html = "";
		$html .= "<table border='1' cellpadding='0' cellspacing='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th colspan='12' align='center'>Penerimaan Rawat Inap</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th colspan='12' align='center'>Periode tanggal ".date_format(date_create($param->tgl_awal), 'd/M/Y')." s/d ".date_format(date_create($param->tgl_akhir), 'd/M/Y')."</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "</table>";
		
		$groupCustomer = array();
		$index 	= 0;		
		$no 	= 1;
		$x 		= 0;		
		$total_q_tagihan 	= 0;
		$total_q_subsidi 	= 0;
		$total_q_tunai 		= 0;
		$total_q_iur_bayar	= 0;
		$total_q_perusahaan	= 0;
		$total_q_raharja	= 0;
		$total_q_bpjs		= 0;
		$total_q_piutang	= 0;
		$total_q_asuransi	= 0;

		$jumlah_q_tagihan 	= 0;
		$jumlah_q_subsidi 	= 0;
		$jumlah_q_tunai 	= 0;
		$jumlah_q_iur_bayar	= 0;
		$jumlah_q_perusahaan= 0;
		$jumlah_q_raharja	= 0;
		$jumlah_q_bpjs		= 0;
		$jumlah_q_piutang	= 0;
		$jumlah_q_asuransi	= 0;
		$html .= "<table border='1' width='100%' cellpadding='0' cellspacing='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th width='3%'>NO</th>";
		$html .= "<th width='10%'>Medrec</th>";
		$html .= "<th width='35%'>Nama Pasien</th>";
		$html .= "<th width='8%'>Tagihan</th>";
		$html .= "<th width='8%'>Disc Subsidi</th>";
		$html .= "<th width='8%'>Tunai</th>";
		$html .= "<th width='8%'>Iur Bayar</th>";
		$html .= "<th width='8%'>Perusahaan</th>";
		$html .= "<th width='8%'>Js Raharja</th>";
		$html .= "<th width='8%'>BPJS</th>";
		$html .= "<th width='8%'>Piutang Pas</th>";
		$html .= "<th width='8%'>Asuransi</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";

		if ($queryPG_header->num_rows() > 0) {
			foreach($queryPG_header->result() as $data_head){
				$html .= "<tr>";
				$html .= "<td colspan='3' style='padding-left:10px;'><b>".$data_head->customer."</b><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "<td style='padding-left:10px;'><td>";
				$html .= "</tr>";
				
				foreach($queryPG_body->result() as $data_body){
					if($data_head->customer == $data_body->customer){
						$html .= "<tr>";
						$html .= "<td align='center'>".$no."</td>";
						$html .= "<td style='padding-left:10px;'>".$data_body->kd_pasien."</td>";
						$html .= "<td style='padding-left:10px;'>".$data_body->nama_pasien."</td>";
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_tagihan."</td>";
						$total_q_tagihan 	+= $data_body->q_tagihan;
						$jumlah_q_tagihan 	+= $data_body->q_tagihan;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_subsidi."</td>";
						$total_q_subsidi 	+= $data_body->q_subsidi;
						$jumlah_q_subsidi 	+= $data_body->q_subsidi;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_tunai."</td>";
						$total_q_tunai 		+= $data_body->q_tunai;
						$jumlah_q_tunai 	+= $data_body->q_tunai;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_iur_bayar."</td>";
						$total_q_iur_bayar 	+= $data_body->q_iur_bayar;
						$jumlah_q_iur_bayar += $data_body->q_iur_bayar;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_perusahaan."</td>";
						$total_q_perusahaan += $data_body->q_perusahaan;
						$jumlah_q_perusahaan+= $data_body->q_perusahaan;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_raharja."</td>";
						$total_q_raharja 	+= $data_body->q_raharja;
						$jumlah_q_raharja 	+= $data_body->q_raharja;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_bpjs."</td>";
						$total_q_bpjs 		+= $data_body->q_bpjs;
						$jumlah_q_bpjs 		+= $data_body->q_bpjs;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_piutang."</td>";
						$total_q_piutang 	+= $data_body->q_piutang;
						$jumlah_q_piutang 	+= $data_body->q_piutang;
						$html .= "<td align='right' style='padding-right:5px;'>".$data_body->q_asuransi."</td>";
						$total_q_asuransi 	+= $data_body->q_asuransi;
						$jumlah_q_asuransi 	+= $data_body->q_asuransi;
						$html .= "</tr>";
						$no++;
					}
				}
				$html .= "<tr>";
				$html .= "<td colspan='3' style='padding-left:10px;'> Total Biaya ".$data_head->customer."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_tagihan."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_subsidi."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_tunai."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_iur_bayar."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_perusahaan."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_raharja."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_bpjs."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_piutang."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_q_asuransi."</td>";
				$html .= "</tr>";

				$jumlah_q_tagihan 	= 0;
				$jumlah_q_subsidi 	= 0;
				$jumlah_q_tunai 	= 0;
				$jumlah_q_iur_bayar	= 0;
				$jumlah_q_perusahaan= 0;
				$jumlah_q_raharja	= 0;
				$jumlah_q_bpjs		= 0;
				$jumlah_q_piutang	= 0;
				$jumlah_q_asuransi	= 0;
				$no = 1;
			}
			$html .= "<tr>";
			$html .= "<td colspan='3' style='padding-left:10px;'> Total Biaya</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_tagihan."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_subsidi."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_tunai."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_iur_bayar."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_perusahaan."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_raharja."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_bpjs."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_piutang."</td>";
			$html .= "<td align='right' style='padding-right:5px;'>".$total_q_asuransi."</td>";
			$html .= "</tr>";
		}else{
			$html .= "<tr valign='top'>";
			$html .= "<td colspan='12' align='center'>Tidak ada data</td>";
			$html .= "</tr>";
		}
		$html .= "</tbody>";
		$html .= "</table>";
		
		if($params['type_file'] == true){
			$no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$print_area	= 'A1:L'.$no;
			$area_wrap	= 'A4:L'.$no;
			/*$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	*/	
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A4:L3')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
				/*'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			 	),
				'borders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					// 'top' => array(
			 	// 	)
			 	),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
						'argb' => 'FFA0A0A0'
					),
					'endcolor' => array(
						'argb' => 'FFFFFFFF'
					)
				)*/
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(8);
			$objPHPExcel->getActiveSheet()->getStyle("A1:L2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 

			$textFormat='@';//'General','0.00','@';
			$numberormat='#,##0';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:C'.$no)
						->getNumberFormat()
					    ->setFormatCode($textFormat);

			$objPHPExcel->getActiveSheet()
						->getStyle('D5:L'.$no)
						->getNumberFormat()
					    ->setFormatCode($numberormat);
			/*$objPHPExcel->getActiveSheet()
						->getStyle('A4:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_TOP);*/
			$objPHPExcel->getActiveSheet()
						->getStyle('C5:C'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D5:D'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E5:E'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F5:F'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G5:G'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H5:H'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I5:I'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J5:J'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K5:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('L5:L'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(9);
			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_penerimaan_pasien_rawat_inap.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan penerimaan pasien RWI',$html);	
		}
		// print_r(array_unique($groupCustomer));
		//echo strlen($groupCustomer);
	}

	function  cetakLaporan_rekapitulasiPendapatan(){
		$common 	= $this->common;
   		$result 	= $this->result;
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();
		$no = 1;
		$total_kamar = 0;
		$total_konsumsi = 0;
		$total_visite_konsul = 0;
		$total_operatif = 0;
		$total_non_operatif = 0;
		$total_radiologi = 0;
		$total_laboratorium_pk = 0;
		$total_laboratorium_pa = 0;
		$total_hd = 0;
		$total_irm = 0;
		$total_ird = 0;
		$total_rwj = 0;
		$total_adm = 0;
		$total_bhp = 0;
		$total_obat = 0;
		$total_o2 = 0;
		$total_gizi = 0;
		$total_ecg = 0;
		$total_transfusi = 0;
		$total_biaya = 0;
		$total_jml_kwitansi = 0;
		$total_jml_pasien = 0;
		$total_tagihan = 0;
		$total_detail_transaksi = 0;
		// $tmp_kamar = $this->input->post('kd_kamar');

		$param 		= json_decode($_POST['data']);
		$params = array(
			'kd_kamar' 			=> $param->kd_kamar,
			'kd_pay' 			=> $param->kd_pay,
			'kd_spesial' 		=> $param->kd_spesial,
			'pasien_pulang' 	=> $param->pasien_pulang,
			// 'shift_1' 		=> $param->shift_1,
			// 'shift_2' 		=> $param->shift_2,
			// 'shift_3' 		=> $param->shift_3,
			// 'shift_all' 		=> $param->shift_all,
			'tgl_akhir' 		=> $param->tgl_akhir,
			'tgl_awal' 			=> $param->tgl_awal,
			'type_file' 		=> $param->type_file,
			'unit' 				=> $param->unit,
			'kel_pas' 			=> $param->kel_pas,
			'kel_pas2' 			=> $param->kel_pas2,
			'kelas_produk' 		=> $param->kelas_produk,
			'length_kd_pay' 	=> $param->length_kd_pay,
			'length_kd_kamar' 	=> $param->length_kd_kamar,
			'length_kd_spesial'	=> $param->length_kd_spesial,
		);
		

		$criteriaCustomer = "";
		$Hcustomer = "";
		if ($params['kel_pas'] == 2) { 
			# PERSEORANGAN
			if (strtolower($params['kel_pas2']) == strtolower("semua") || $params['kel_pas2'] == 1) {
				$query = $this->db->query("SELECT * FROM kontraktor INNER JOIN customer ON kontraktor.kd_customer = customer.kd_customer WHERE  kontraktor.jenis_cust = '0'");
				foreach ($query->result() as $data) {
					$criteriaCustomer .= "'".$data->kd_customer."',";
				}
				$criteriaCustomer = "AND kd_customer in (".substr($criteriaCustomer, 0, strlen($criteriaCustomer)-1).")";
				$Hcustomer = "Semua Perseorangan";
			}else{
				$criteriaCustomer = "AND kd_customer = '". $params['kel_pas2']."'";
				$Hcustomer = $this->db->query("select customer from customer where kd_customer='".$params['kel_pas2']."'")->row()->customer;
			}
		}else if($params['kel_pas'] == 3){
			# PERUSAHAAN
			if (strtolower($params['kel_pas2']) == strtolower("semua") || $params['kel_pas2'] == 1) {
				$query = $this->db->query("SELECT * FROM kontraktor INNER JOIN customer ON kontraktor.kd_customer = customer.kd_customer WHERE  kontraktor.jenis_cust = '1'");
				foreach ($query->result() as $data) {
					$criteriaCustomer .= "'".$data->kd_customer."',";
				}
				$criteriaCustomer = "AND kd_customer in (".substr($criteriaCustomer, 0, strlen($criteriaCustomer)-1).")";
				$Hcustomer = "Semua Perusahaan";
			}else{
				$criteriaCustomer = "AND kd_customer = '". $params['kel_pas2']."'";
				$Hcustomer = $this->db->query("select customer from customer where kd_customer='".$params['kel_pas2']."'")->row()->customer;
			}
		}else if($params['kel_pas'] == 4){
			# ASURANSI
			if (strtolower($params['kel_pas2']) == strtolower("semua") || $params['kel_pas2'] == 1) {
				$query = $this->db->query("SELECT * FROM kontraktor INNER JOIN customer ON kontraktor.kd_customer = customer.kd_customer WHERE  kontraktor.jenis_cust = '2'");
				foreach ($query->result() as $data) {
					$criteriaCustomer .= "'".$data->kd_customer."',";
				}
				$criteriaCustomer = "AND kd_customer in (".substr($criteriaCustomer, 0, strlen($criteriaCustomer)-1).")";
				$Hcustomer = "Semua Asuransi";
			}else{
				$criteriaCustomer = "AND kd_customer = '". $params['kel_pas2']."'";
				$Hcustomer = $this->db->query("select customer from customer where kd_customer='".$params['kel_pas2']."'")->row()->customer;
			}
		}else{
			$criteriaCustomer = "";
			$Hcustomer = "Semua";
		}

		$criteriaUnit = "";
		if (strlen($params['unit']) > 0) {
			$criteriaUnit = "AND u.parent in (".substr($params['unit'], 0, strlen($params['unit'])-1).") ";
		}

		$criteriaPay = "";
		if (strlen($params['kd_pay']) > 0) {
			$criteriaPay = "AND (db.kd_pay in (".$params['kd_pay'].")) ";
		}

		$criteriaSpesial = "";
		$Hspesialisasi = "";
		if (strlen($params['kd_spesial']) > 0) {
			$criteriaSpesial = "AND s.kd_spesial in (".$params['kd_spesial'].") ";
			$Resspesialisasi = $this->db->query("select * from spesialisasi where kd_spesial in(".$params['kd_spesial'].")")->result();
			$pembagi = count($Resspesialisasi) / 2;
			for($i=0;$i<count($Resspesialisasi);$i++){
				$Hspesialisasi .= $Resspesialisasi[$i]->spesialisasi.", ";
				if(count($Resspesialisasi) > 10 )//&& count($Resspesialisasi) < 15){
					if($i == (int)$pembagi){
						$Hspesialisasi .= "<br>";
					}
			}
			$Hspesialisasi = substr($Hspesialisasi, 0, -2);
		}

		$criteriaKamar = "";
		if (strlen($params['kd_kamar']) > 0) {
			$criteriaKamar = "AND n.no_kamar in (".$params['kd_kamar'].") ";
		}

		unset($query);
		// $query = $this->db->query("SELECT * FROM get_all_pendapatan_arus('02', '".$params['tgl_awal']."', '".$params['tgl_akhir']."', ".$params['pasien_pulang'].") ".$criteriaUnit." ".$criteriaCustomer." ".$criteriaPay."  order by nama_pasien");
		$query = $this->db->query(" 
			SELECT
				x.customer,
				x.nama_pasien, 
				x.kd_pasien, 
				x.tgl_masuk,
				x.tgl_keluar,
				sum(x.Kamar) as kamar,
				sum(x.Konsumsi) as konsumsi,
				sum(x.Visite_Konsul) as visite_Konsul,
				sum(x.Operatif) as operatif,
				sum(x.non_operatif) as non_operatif,
				sum(x.Radiologi) as radiologi,
				sum(x.laboratorium_pk) as laboratorium_pk,
				sum(x.laboratorium_pa) as laboratorium_pa,
				sum(x.HD) as HD,
				sum(x.IRM) as IRM,
				sum(x.IRD) as IRD,
				sum(x.RWJ) as RWJ,
				sum(x.ADM) as ADM,
				sum(x.BHP) as BHP,
				sum(x.OBAT) as OBAT,
				sum(x.O2) as o2,
				sum(x.GIZI) as GIZI,
				sum(x.ECG) as ECG,
				sum(x.Transfusi) as transfusi,
				y.bayar as bayar
			FROM( 
				SELECT distinct 
					nama as nama_pasien, 
					p.kd_pasien, 
					t.Tgl_Transaksi as tgl_masuk, 
					K .TGL_KELUAR,
					cust.customer,
					sum(x.kamar) as kamar,
					sum(x.konsumsi) as konsumsi,
					sum(x.visite_konsul) as visite_konsul,
					sum(x.operatif) as operatif,
					sum(x.NonOperatif) as non_operatif,
					sum(x.Radiologi) as radiologi,
					sum(x.LaboratoriumPK) as laboratorium_pk,
					sum(x.LaboratoriumPA) as laboratorium_pa,
					sum(x.HD) as hd,
					sum(x.IRM) as irm,
					sum(x.IRD) as ird,
					sum(x.RWJ) as rwj,
					sum(x.ADM) as adm,
					sum(x.BHP) as bhp,
					sum(x.OBAT) as obat,
					sum(x.O2) as o2,
					sum(x.GIZI) as gizi,
					sum(x.ECG) as ecg,
					sum(x.Transfusi)  as transfusi,
					t.no_transaksi, 
					t.kd_kasir
				FROM Transaksi t 
					INNER JOIN nginap n ON t.kd_pasien=n.kd_pasien AND t.tgl_transaksi=n.tgl_masuk 
						AND t.urut_masuk=n.urut_masuk and akhir ='true'
					INNER JOIN Spesialisasi s ON n.kd_spesial=s.kd_spesial 
					INNER JOIN Pasien p ON t.kd_pasien=p.kd_pasien 
					inner join kunjungan k ON t.kd_pasien = k.kd_pasien and t.tgl_transaksi = k.tgl_masuk and t.kd_unit = k.kd_unit and t.urut_masuk = k.urut_masuk 
					LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
					LEFT JOIN customer cust on cust.kd_customer = k.kd_customer
						
					INNER JOIN (
						SELECT distinct 
							dt.no_transaksi, 
							dt.kd_kasir,
							dt.tgl_transaksi,
							dt.urut, 
							dt.harga,
							dt.qty, 
							/*db.kd_pay,*/ 
							SUM(case when  kp.kd_klas in ('2')  and dt.kd_produk in ('1010','6921','6773','6780','6793') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as kamar,
							SUM(case when  kp.kd_klas in ('2')  and dt.kd_produk in ('1401','5822','6600','6601','6602','6603','6604','6605','6606','6607','6608','6774','6773') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as konsumsi,
							SUM(case when  kp.kd_klas in ('31','32','33','34')  and dt.kd_produk not in ('2449','5585','6405','6406') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as visite_konsul,
							SUM(case when  kp.kd_klas in ('61','6101','6102','6103','6104','6105','6106','610201','610202','610203','610204',
							'610205','610301','610302','610303','610304','610402','610403','610404','610501','610502','610503','610504','610505','610506','610601','610602','610603','610604','9')  
							and dt.kd_produk in ('6285') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as operatif,
							SUM(case when  kp.kd_klas in ('62'
							,'65','66','67','68','69','6201','6202','6203','6204','6205','6206','6207',
							'6208','6209','6210','6301','6302','630101','630102','630103','630104','630106','630107','630108','630201','630202','630203','630204','630205','630206','630207','630209','630210','630211','630213','630214',
							'630215','630218','6303','630301','630302','630303','630304','630305','630307','6601','6602','6603','6604','6605','6608','6609','6701','6702','6703','6704','6705','6706','6707','6708','6710','6801',
							'6802','6803','6804','6805','6901','6902','6903','6904','6905','69101','69102','69103','69104','69106','69107','69108','69109','69110','69114','69115','69116','69117','69118','69119','69120','69121',
							'69123','69201','69202','69203','69204','69205','69207','69209','69210','7501','7502','7503','7504','7505','7507','7508','7509','7510')  
							and dt.kd_produk not in ('635','1036','1086','1096','1571','1934','1949','2224','2547','2561','2568','2603','2604','2605','3798','3802','4215','4255') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as NonOperatif,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('1093') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) radiologi,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('1092') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as laboratoriumpk,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('3848') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as laboratoriumpa,
							SUM(case when  (kp.kd_klas in ('9')  and dt.kd_produk in ('5543') or (kp.kd_klas in ('77'))) and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as hd,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('3799') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as irm,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('1095') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as ird,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('1094') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as rwj,
							SUM(case when  kp.kd_klas in ('1','803')  and dt.kd_produk in ('1','416','1402','3800','5544','5545','5546','6388','6437','6487','6489','6807') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as adm,
							SUM(case when  kp.kd_klas in ('41','43','9')  and dt.kd_produk in ('1516','2604','3802','6449','6481','501','6451') and dt.kd_produk not in ('1092','1093','1094','1095','2491','3799','3848','3892','5543','6285') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as bhp,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('6440','6771') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as obat,
							SUM(case when  kp.kd_klas in ('42')  and dt.kd_produk in ('3830','6481') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as o2,
							SUM(case when  kp.kd_klas in ('32')  and dt.kd_produk in ('2449','5585','6405','6406') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as gizi,
							SUM(case when  kp.kd_klas in ('62','66','67','69','6202','6204','6301','6303')  and dt.kd_produk in ('635','1036','1086','1571','1934','1949','2224','2547','2561','2568','4215','4255') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as ecg,
							SUM(case when  kp.kd_klas in ('9')  and dt.kd_produk in ('3829') and dt.kd_kasir = '02' then dt.harga * dt.qty else 0 end) as transfusi 
						FROM detail_transaksi dt 
							inner join transaksi t ON t.no_transaksi = dt.no_transaksi and t.kd_kasir = dt.kd_kasir 
							inner join detail_bayar db ON t.kd_kasir = db.kd_kasir and t.no_transaksi = db.no_transaksi 
							INNER JOIN Unit u ON dt.Kd_Unit_tr = u.Kd_Unit 
							inner join produk p on p.kd_produk = dt.kd_produk 
							inner join klas_produk kp on kp.kd_klas = p.kd_klas 
						where  dt.Kd_Kasir='02' 
							and db.tgl_transaksi BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."' 
							and t.ispay = 'true' ".$criteriaUnit." ".$criteriaPay."
						group by dt.tgl_transaksi,dt.urut,dt.harga,dt.qty,dt.no_transaksi, dt.kd_kasir, dt.kd_produk, dt.kd_kasir, dt.harga, db.kd_pay)x 
							on  t.no_transaksi = x.no_transaksi and t.kd_kasir = x.kd_kasir 
				WHERE 
					t.Kd_Kasir='02' AND t.co_status = '".$params['pasien_pulang']."' 
						".$criteriaSpesial."
						".$criteriaKamar."
				group by Nama, p.Kd_Pasien, t.Tgl_Transaksi, t.tgl_Co, k.Tgl_masuk, k.TGL_KELUAR,
							cust.customer, t.no_transaksi, t.kd_kasir, n.kd_unit_kamar )x 
				INNER JOIN (select kd_kasir, no_transaksi, sum(jumlah) as bayar 
			FROM detail_bayar 
				where kd_kasir = '02'  
				group by kd_kasir, no_transaksi )y 
				ON x.no_transaksi = y.no_transaksi and x.kd_kasir = y.kd_kasir 
			GROUP BY  x.Nama_Pasien, x.Kd_Pasien, x.tgl_masuk,
						x.tgl_keluar,
						x.customer,y.bayar 
			ORDER BY x.Nama_Pasien
		");

		$html = "";
		$html .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th colspan='28' algin='center'>REKAPITULASI ARUS PENDAPATAN RAWAT INAP PER PASIEN</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th colspan='28' algin='center'>PEMBANTU KASIR PENERIMA UANG</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th colspan='28' algin='center'>Periode : ".date_format(date_create($params['tgl_awal']), 'd/M/Y')." - ".date_format(date_create($params['tgl_akhir']), 'd/M/Y')."</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th colspan='28' algin='center'>Spesialisasi : ".$Hspesialisasi."</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th colspan='28' algin='center'>Kelompok Pasien : ".$Hcustomer."</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "</table>";
		$html .= "<table border='1' width='100%' cellpadding='0' cellspacing='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th rowspan='3' valign='middle' align='center'>NO</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Customer</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Medrec</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Nama Pasien</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Tanggal Masuk</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Tanggal Keluar</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Kamar</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>konsumsi</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Visite Konsul</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Operatif</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Non Operatif</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Radiologi</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Laboratorium PK</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Laboratorium PA</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>HD</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>IRM</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>IRD</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>RWJ</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>ADM</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>BHP</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Farmasi</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>O2</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Gizi</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>ECG</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Transfusi</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Total POP</th>";
		$html .= "<th colspan='2' align='center'>Pembayaran</th>";
		$html .= "<th colspan='2' align='center'>Piutang</th>";
		$html .= "<th rowspan='3' valign='middle' align='center'>Keterangan (+/-)/Penanggung</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th align='center'>JML</th>";
		$html .= "<th align='center'>Bayar</th>";
		$html .= "<th align='center'>Pasien</th>";
		$html .= "<th align='center'>Total</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th align='center'>Kwitansi</th>";
		$html .= "<th align='center'>Bayar</th>";
		$html .= "<th align='center'>Pasien</th>";
		$html .= "<th align='center'>Tagihan</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";
		$tmpTotal = 0;
		foreach ($query->result() as $data) {
			$queryTransaksi 	= $this->db->query("SELECT * from transaksi where kd_pasien='".$data->kd_pasien."' and tgl_transaksi = '".$data->tgl_masuk."' and kd_kasir = '02' and co_status='".$params['pasien_pulang']."'");
			$jumlahDetailTransaksi 	= $this->db->query("SELECT sum(harga*qty) as jumlah from detail_transaksi where no_transaksi = '".$queryTransaksi->row()->no_transaksi."' and kd_kasir = '02'")->row()->jumlah;
			$jumlahKwitansi 		= $this->db->query("SELECT count(*) as counter FROM nota_bill where no_transaksi='".$queryTransaksi->row()->no_transaksi."' and kd_kasir = '02'")->row()->counter;
			$html .= "<tr>";
			$html .= "<td>".$no."</td>";
			$html .= "<td>".$data->customer."</td>";
			$html .= "<td>".$data->kd_pasien."</td>";
			$html .= "<td>".$data->nama_pasien."</td>";
			$html .= "<td>".$data->tgl_masuk."</td>";
			$html .= "<td>".$data->tgl_keluar."</td>";
			$html .= "<td>".number_format($data->kamar,0,'.',',')."</td>";
			$total_kamar 			+= $data->kamar;
			$html .= "<td>".number_format($data->konsumsi,0,'.',',')."</td>";
			$total_konsumsi 		+= $data->konsumsi;
			$html .= "<td>".number_format($data->visite_konsul,0,'.',',')."</td>";
			$total_visite_konsul 	+= $data->visite_konsul;
			$html .= "<td>".number_format($data->operatif,0,'.',',')."</td>";
			$total_operatif 		+= $data->operatif;
			$html .= "<td>".number_format($data->non_operatif,0,'.',',')."</td>";
			$total_non_operatif 	+= $data->non_operatif;
			$html .= "<td>".number_format($data->radiologi,0,'.',',')."</td>";
			$total_radiologi 		+= $data->radiologi;
			$html .= "<td>".number_format($data->laboratorium_pk,0,'.',',')."</td>";
			$total_laboratorium_pk	+= $data->laboratorium_pk;
			$html .= "<td>".number_format($data->laboratorium_pa,0,'.',',')."</td>";
			$total_laboratorium_pa	+= $data->laboratorium_pa;
			$html .= "<td>".number_format($data->hd,0,'.',',')."</td>";
			$total_hd				+= $data->hd;
			$html .= "<td>".number_format($data->irm,0,'.',',')."</td>";
			$total_irm				+= $data->irm;
			$html .= "<td>".number_format($data->ird,0,'.',',')."</td>";
			$total_ird				+= $data->ird;
			$html .= "<td>".number_format($data->rwj,0,'.',',')."</td>";
			$total_rwj				+= $data->rwj;
			$html .= "<td>".number_format($data->adm,0,'.',',')."</td>";
			$total_adm				+= $data->adm;
			$html .= "<td>".number_format($data->bhp,0,'.',',')."</td>";
			$total_bhp				+= $data->bhp;
			$html .= "<td>".number_format($data->obat,0,'.',',')."</td>";
			$total_obat				+= $data->obat;
			$html .= "<td>".number_format($data->o2,0,'.',',')."</td>";
			$total_o2				+= $data->o2;
			$html .= "<td>".number_format($data->gizi,0,'.',',')."</td>";
			$total_gizi				+= $data->gizi;
			$html .= "<td>".number_format($data->ecg,0,'.',',')."</td>";
			$total_ecg				+= $data->ecg;
			$html .= "<td>".number_format($data->transfusi,0,'.',',')."</td>";
			$total_transfusi		+= $data->transfusi;

			$tmpTotal = (int)$data->kamar+(int)$data->konsumsi+(int)$data->visite_konsul+(int)$data->operatif+(int)$data->non_operatif+(int)$data->radiologi+(int)$data->laboratorium_pk+(int)$data->laboratorium_pa+(int)$data->hd+(int)$data->irm+(int)$data->ird+(int)$data->rwj+(int)$data->adm+(int)$data->bhp+(int)$data->obat+(int)$data->o2+(int)$data->gizi+(int)$data->ecg+(int)$data->transfusi;
			$html .= "<td>".number_format($tmpTotal,0,'.',',')."</td>";
			$html .= "<td>".$jumlahKwitansi."</td>";
			$html .= "<td>".number_format($data->bayar,0,'.',',')."</td>";
			$html .= "<td>1</td>";
			// $html .= "<td>".number_format(((int)$data->bayar - (int)$jumlahDetailTransaksi),0,'.',',')."</td>";
			$html .= "<td>".number_format(((int)$tmpTotal - (int)$jumlahDetailTransaksi),0,'.',',')."</td>";
			$html .= "<td></td>";
			$html .= "</tr>";

			$total_detail_transaksi += (int)$jumlahDetailTransaksi;
			$total_jml_pasien++;
			$total_jml_kwitansi += $jumlahKwitansi;
			$total_tagihan += ((int)$data->bayar - (int)$jumlahDetailTransaksi);
			$total_biaya += $data->bayar;
			$no++;
		}
		$html .= "<tr>";
		$html .= "<td></td>";
		$html .= "<td></td>";
		$html .= "<td></td>";
		$html .= "<td><b>JUMLAH<b></td>";
		$html .= "<td>".number_format($total_kamar,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_konsumsi,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_visite_konsul,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_operatif,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_non_operatif,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_radiologi,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_laboratorium_pk,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_laboratorium_pa,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_hd,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_irm,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_ird,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_rwj,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_adm,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_bhp,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_obat,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_o2,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_gizi,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_ecg,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_transfusi,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_detail_transaksi,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_jml_kwitansi,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_biaya,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_jml_pasien,0,'.',',')."</td>";
		$html .= "<td>".number_format($total_tagihan,0,'.',',')."</td>";
		$html .= "<td></td>";
		$html .= "</tr>";
		$html .= "</tbody>";
		$html .= "</table>";
		// echo $html;
		// echo $params['type_file'];
		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
			$print_area	= 'A1:L'.$no;
			$area_wrap	= 'A4:L'.$no;
			$name='Laporan_rekapitulasi_pendapatan_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan rekapitulasi pendapatan',$html);	
		}
	}

	public function laporanCetakPelayananDokter(){
		$common 	= $this->common;
   		$result 	= $this->result;
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();

		$param 		= json_decode($_POST['data']);
		$params = array(
			'kd_kamar' 			=> $param->kd_kamar,
			'kd_pay' 			=> $param->kd_pay,
			'kd_spesial' 		=> $param->kd_spesial,
			'pasien_pulang' 	=> $param->pasien_pulang,
			// 'shift_1' 		=> $param->shift_1,
			// 'shift_2' 		=> $param->shift_2,
			// 'shift_3' 		=> $param->shift_3,
			// 'shift_all' 		=> $param->shift_all,
			'tgl_akhir' 		=> $param->tgl_akhir,
			'tgl_awal' 			=> $param->tgl_awal,
			'type_file' 		=> $param->type_file,
			'unit' 				=> substr($param->unit, 0 , strlen($param->unit)-1),
			'kel_pas' 			=> $param->kel_pas,
			'kel_pas2' 			=> $param->kel_pas2,
			'kelas_produk' 		=> $param->kelas_produk,
			'length_kd_pay' 	=> $param->length_kd_pay,
			'length_kd_kamar' 	=> $param->length_kd_kamar,
			'length_kd_spesial'	=> $param->length_kd_spesial,
		);

		$criteriaKamar = "";
		if (isset($params['kd_kamar'])) {
			// $criteriaKamar = " AND (dk.NO_KAMAR IN (".$params['kd_kamar']."))";
			$criteriaKamar = " array[".$params['kd_kamar']."] ";
		}

		$criteriaPay = "";
		if (isset($params['kd_pay'])) {
			// $criteriaPay = " AND (db.KD_PAY IN (".$params['kd_pay'].")) ";
			$criteriaPay = " array[".$params['kd_pay']."] ";
		}

		$criteriaUnit = "";
		if (isset($params['unit'])) {
			// $criteriaUnit = " AND (u.PARENT IN (".$params['unit']."))  ";
			$criteriaUnit = " array[".$params['unit']."]  ";
		}

		$criteriaCoStatus = "";
		if (isset($params['pasien_pulang'])) {
			$criteriaCoStatus = " AND (g.CO_STATUS = ".$params['pasien_pulang'].")  ";
		}

		$criteriaTglCO = "";
		if (isset($params['tgl_awal']) && isset($params['tgl_akhir'])) {
			$criteriaTglCO = " AND (g.TGL_CO BETWEEN '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."') ";
		}

		$queryBody = $this->db->query("SELECT dokter, nama, a1, a2, a3, a4, a5, a6 from sp_rwi_pelayanan_per_pasien(".$params['pasien_pulang'].", 
			'".$params['tgl_awal']."',
			'".$params['tgl_akhir']."',
			".$criteriaUnit.",
			".$criteriaPay.",
			".$criteriaKamar."
			)");

		foreach ($queryBody->result() as $result_header) {
			$header[] = $result_header->dokter;
		}
		$tmp_header = implode('#', array_unique($header));

		// var_dump($queryHeader);die();
		$html = "";
		$html .= "<table border='0' cellspacing='0' width='100%'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th align='center' colspan='9'>Laporan Pelayanan Dokter per Pasien</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th align='center' colspan='9'>Periode ".$params['tgl_awal']." s/d ".$params['tgl_akhir']."</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "</table>";
		
		$html .= "<table border='1' cellspacing='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th>NO</th>";
		$html .= "<th>Nama Pasien</th>";
		$html .= "<th>Dokter</th>";
		$html .= "<th>Perawat</th>";
		$html .= "<th>Operator</th>";
		$html .= "<th>Anasthesi</th>";
		$html .= "<th>Ass. Operator</th>";
		$html .= "<th>Ass. Anasthesi</th>";
		$html .= "<th>Jumlah</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		$html .= "<tbody>";
		$no = 1;
		$no_header = 1;
		$jumlah_per_dokter = 0;
		$total_per_dokter = 0;

		$_gtotal_per_dokter = 0;
			$total_a1 = 0;
			$total_a2 = 0;
			$total_a3 = 0;
			$total_a4 = 0;
			$total_a5 = 0;
			$total_a6 = 0;
			$array_header = explode("#", $tmp_header);
			for ($i=0; $i < count($array_header); $i++) {
				$total_per_dokter = 0;
				$jumlah_a1 = 0;
				$jumlah_a2 = 0;
				$jumlah_a3 = 0;
				$jumlah_a4 = 0;
				$jumlah_a5 = 0;
				$jumlah_a6 = 0;
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td colspan='8'>".$array_header[$i]."</td>";
				$html .= "</tr>";
				foreach ($queryBody->result() as $result_body) {
					if ($result_body->dokter == $array_header[$i]) {
						$jumlah_a1 += (int)$result_body->a1;
						$jumlah_a2 += (int)$result_body->a2;
						$jumlah_a3 += (int)$result_body->a3;
						$jumlah_a4 += (int)$result_body->a4;
						$jumlah_a5 += (int)$result_body->a5;
						$jumlah_a6 += (int)$result_body->a6;
						$jumlah_per_dokter = (int)$result_body->a1+(int)$result_body->a2+(int)$result_body->a3+(int)$result_body->a4+(int)$result_body->a5+(int)$result_body->a6;
						$html .= "<tr>";
						$html .= "<td width='25' style='padding-left:5px;'>".$no."</td>";
						$html .= "<td style='padding-left:5px;'>".$result_body->nama."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$result_body->a1."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$result_body->a2."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$result_body->a3."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$result_body->a4."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$result_body->a5."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$result_body->a6."</td>";
						$html .= "<td width='100' align='right' style='padding-right:5px;'>".$jumlah_per_dokter."</td>";
						$html .= "</tr>";
						$total_per_dokter += $jumlah_per_dokter;
						$no++;
					}
				}
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td style='padding-left:5px;'>Sub Total</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_a1."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_a2."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_a3."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_a4."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_a5."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$jumlah_a6."</td>";
				$html .= "<td align='right' style='padding-right:5px;'>".$total_per_dokter."</td>";
				$html .= "</tr>";
				$total_a1 += $jumlah_a1;
				$total_a2 += $jumlah_a2;
				$total_a3 += $jumlah_a3;
				$total_a4 += $jumlah_a4;
				$total_a5 += $jumlah_a5;
				$total_a6 += $jumlah_a6;
				$_gtotal_per_dokter += $total_per_dokter;
				$no_header++;
			}
		$html .= "<tr>";
		$html .= "<td></td>";
		$html .= "<td style='padding-left:5px;'>Grand Total Total</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$total_a1."</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$total_a2."</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$total_a3."</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$total_a4."</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$total_a5."</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$total_a6."</td>";
		$html .= "<td align='right' style='padding-left:5px;'>".$_gtotal_per_dokter."</td>";
		$html .= "</tr>";
		$html .= "</tbody>";
		$html .= "</table>";
		// echo $html;

		$total_row = $no+((int)$no_header*2);
		if ($params['type_file'] === true || $params['type_file'] == 'true') {
			$total_row 	= $total_row+2;
			$print_area	= 'A1:I'.$total_row;
			$area_wrap	= 'A4:I'.$total_row;
			/*$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	*/	
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A4:I3')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
				/*'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			 	),
				'borders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					// 'top' => array(
			 	// 	)
			 	),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
						'argb' => 'FFA0A0A0'
					),
					'endcolor' => array(
						'argb' => 'FFFFFFFF'
					)
				)*/
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(8);
			$objPHPExcel->getActiveSheet()->getStyle("A1:I2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 

			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:I'.$total_row)
						->getNumberFormat()
					    ->setFormatCode($textFormat);

			$numberormat='#,##0';//'General','0.00','@';

			$objPHPExcel->getActiveSheet()
						->getStyle('C5:I'.$total_row)
						->getNumberFormat()
					    ->setFormatCode($numberormat);

			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/*$objPHPExcel->getActiveSheet()
						->getStyle('A4:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_TOP);*/
			$objPHPExcel->getActiveSheet()
						->getStyle('C5:C'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D5:D'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E5:E'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F5:F'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G5:G'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H5:H'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I5:I'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);/*
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);*/
			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_pelayanan_dokter_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
			echo $html;
			
		}else{
			// echo $html;
			$this->common->setPdf('L','Laporan pelayanan dokter per pasien',$html);	
		}
	}

	public function laporanCetakPelayananDokterDetail(){
		$common 	= $this->common;
   		$result 	= $this->result;
		$this->db->trans_begin();
		$this->dbSQL->trans_begin();

		$param 		= json_decode($_POST['data']);
		$params = array(
			'kd_pay' 			=> $param->kd_pay,
			// 'shift_1' 		=> $param->shift_1,
			// 'shift_2' 		=> $param->shift_2,
			// 'shift_3' 		=> $param->shift_3,
			// 'shift_all' 		=> $param->shift_all,
			'kd_dokter' 		=> $param->kd_dokter,
			'tgl_akhir' 		=> $param->tgl_akhir,
			'tgl_awal' 			=> $param->tgl_awal,
			'type_file' 		=> $param->type_file,
			'unit' 				=> substr($param->unit, 0 , strlen($param->unit)-1),
			'kel_pas' 			=> $param->kel_pas,
			'kel_pas2' 			=> $param->kel_pas2,
			'length_kd_pay' 	=> $param->length_kd_pay,
		);

		$criteriaUnit = "";
		if (isset($params['unit'])) {
			// $criteriaUnit = "(U.PARENT IN (".$params['unit'].")) AND";
			$criteriaUnit = "array[".$params['unit']."]";
		}
		$criteriaPay = "";
		if (isset($params['kd_pay'])) {
			// $criteriaPay = "(PY.KD_PAY IN (".$params['kd_pay']."))";
			$criteriaPay = "array[".$params['kd_pay']."]";
		}

		$criteriaDokter = "";
		if ($params['kd_dokter'] == '999' || strtolower($params['kd_dokter']) == 'semua') {
			// $criteriaDokter = " AND D.KD_DOKTER = '".$params['kd_dokter']."'";
		}else{
			$criteriaDokter = " AND D.KD_DOKTER = '".$params['kd_dokter']."'";
		}

		$criteriaCustomer = "";
		if (isset($params['kel_pas']) && $params['kel_pas'] != 1) {
			if (isset($params['kel_pas2']) && $params['kel_pas2'] != 1) {
				$criteriaCustomer = "(C.KD_CUSTOMER = ".$params['kel_pas2'].") AND";
			}
		}
		/*SELECT 
			KMR.NAMA_KAMAR, 
			VX.NAMA AS dokter, 
			PX.KD_PASIEN, 
			PX.NAMA, 
			VX.QTY, 
			VX.DESKRIPSI, 
			SUM(VX.JP) AS JP, SUM(VX.JP) * 0.15 AS OPS, 
			SUM(VX.JP) - SUM(VX.JP) * 0.15 AS NET 
			FROM 
			TRANSAKSI AS T 
			INNER JOIN NGINAP AS NG ON NG.KD_PASIEN = T.KD_PASIEN AND NG.KD_UNIT = T.KD_UNIT AND NG.URUT_MASUK = T.URUT_MASUK AND NG.TGL_KELUAR = T.TGL_CO 
			INNER JOIN KAMAR AS KMR ON KMR.NO_KAMAR = NG.NO_KAMAR AND KMR.KD_UNIT = NG.KD_UNIT 
			INNER JOIN PASIEN AS PX ON PX.KD_PASIEN = T.KD_PASIEN 
			INNER JOIN KUNJUNGAN AS K ON K.KD_PASIEN = T.KD_PASIEN AND K.KD_UNIT = T.KD_UNIT AND K.URUT_MASUK = T.URUT_MASUK AND K.TGL_MASUK = T.TGL_TRANSAKSI 
			INNER JOIN KONTRAKTOR AS KTR ON K.KD_CUSTOMER = KTR.KD_CUSTOMER 
			INNER JOIN CUSTOMER AS C ON C.KD_CUSTOMER = KTR.KD_CUSTOMER 
			INNER JOIN DETAIL_BAYAR AS DB ON DB.KD_KASIR = T.KD_KASIR AND DB.NO_TRANSAKSI = T.NO_TRANSAKSI AND DB.TGL_TRANSAKSI = T.TGL_CO 
			INNER JOIN PAYMENT AS PY ON PY.KD_PAY = DB.KD_PAY 
			INNER JOIN UNIT AS U ON U.KD_UNIT = T.KD_UNIT 
			INNER JOIN ( 
				SELECT VD.KD_KASIR, VD.NO_TRANSAKSI, D.NAMA, COUNT(VD.TAG_INT) AS QTY, P.DESKRIPSI, SUM(VD.JP) AS JP 
				FROM VISITE_DOKTER AS VD 
				INNER JOIN DOKTER AS D ON D.KD_DOKTER = VD.KD_DOKTER 
				INNER JOIN PRODUK AS P ON P.KD_PRODUK = VD.TAG_INT 
				where (d.jenis_dokter = 1)
				GROUP BY VD.KD_KASIR, VD.NO_TRANSAKSI, D.NAMA, P.DESKRIPSI 
			) AS VX ON T.KD_KASIR = VX.KD_KASIR AND T.NO_TRANSAKSI = VX.NO_TRANSAKSI 
			WHERE (T.CO_STATUS = 'true') AND (T.TGL_CO BETWEEN '01-Oct-2017' AND '31-Oct-2017') AND (NG.AKHIR = '1') AND (U.PARENT IN ('1003')) AND 
			(PY.KD_PAY IN ('999','AN','AR','AS','AX','BA','BI','BJ','BN','BP','DC','DP','EM','G1','G2','G3','G4','G5','G6','G7','GS','HT','IA','IJ','IM','IN','IP','IS','IT','IU','J1','J2','J3','JD','JK','JL','JP','JS','JT','KA','KR',
			'PL','PM','PR','PT','PU','R1','R2','RP','RS','S1','S2','SD','T1','TL','TP','TR','TS','TU')) 
			GROUP BY T.KD_UNIT, KMR.NAMA_KAMAR, VX.NAMA, PX.KD_PASIEN, PX.NAMA, VX.QTY, VX.DESKRIPSI ORDER BY T.KD_UNIT, KMR.NAMA_KAMAR, VX.NAMA
		*/
		$resultQuery_body = $this->db->query("SELECT *  
			FROM sp_rwi_pelayanan_per_pasien_detail(true, '".$params['tgl_awal']."', '".$params['tgl_akhir']."', ".$criteriaUnit.", ".$criteriaPay.")");
		$header = array();
		foreach ($resultQuery_body->result() as $result_header) {
			$header[] = $result_header->nama_kamar;
		}
		$tmp_header = implode('#', array_unique($header));

		$html = "";
		$html .= "<table border='0' cellspacing='0' width='100%'>";
		$html .= "<tr>";
		$html .= "<th colspan='9'>Laporan Pelayanan Dokter per Detail</th>";
		$html .= "</tr>";
		$html .= "<tr>";
		$html .= "<th colspan='9'>Periode ".$params['tgl_awal']." / ".$params['tgl_akhir']."</th>";
		$html .= "</tr>";
		$html .= "</table>";

		$html .= "<table border='1' cellspacing='0'>";
		$html .= "<thead>";
		$html .= "<tr>";
		$html .= "<th>No</th>";
		$html .= "<th>Nama Dokter</th>";
		$html .= "<th>Medrec</th>";
		$html .= "<th>Nama Pasien</th>";
		$html .= "<th>QTY</th>";
		$html .= "<th>Tindakan</th>";
		$html .= "<th>JP</th>";
		$html .= "<th>OPS 15%</th>";
		$html .= "<th>NET</th>";
		$html .= "</tr>";
		$html .= "</thead>";
		
		$html .= "<tbody>";
		$no_head = 1;
		$no_body = 1;
		$sub_jp 	= 0;
		$sub_ops 	= 0;
		$sub_net 	= 0;

		$grand_jp 	= 0;
		$grand_ops 	= 0;
		$grand_net 	= 0;
		$array_header = explode("#", $tmp_header);
		for ($i=0; $i < count($array_header); $i++) { 
			$html .= "<tr>";
			$html .= "<td align='center'>".$no_head."</td>";
			$html .= "<td colspan='8' style='padding-left:15px;'>".$array_header[$i]."</td>";
			$html .= "</tr>";
			foreach ($resultQuery_body->result() as $result_body) {
				if ($result_body->nama_kamar == $array_header[$i]) {
					$html .= "<tr>";
					$html .= "<td align='center'></td>";
					$html .= "<td style='padding-left:10px;'> - ".$result_body->dokter."</td>";
					$html .= "<td style='padding-left:10px;'>".$result_body->kd_pasien."</td>";
					$html .= "<td style='padding-left:10px;'>".$result_body->nama."</td>";
					$html .= "<td style='padding-left:10px;'>".$result_body->qty."</td>";
					$html .= "<td style='padding-left:10px;'>".$result_body->deskripsi."</td>";
					$html .= "<td style='padding-left:10px;'>".number_format($result_body->jp, 0 ,',',',')."</td>";
					$html .= "<td style='padding-left:10px;'>".number_format($result_body->ops, 0 ,',',',')."</td>";
					$html .= "<td style='padding-left:10px;'>".number_format($result_body->net, 0 ,',',',')."</td>";
					$html .= "</tr>";
					$sub_jp 	+= $result_body->jp;
					$sub_ops 	+= $result_body->ops;
					$sub_net 	+= $result_body->net;
					$no_body++;
				}
			}
			$html .= "<tr>";
			$html .= "<td align='center'></td>";
			$html .= "<td align = 'left' style='padding-right:10px;' colspan='5'>Sub Total</td>";
			$html .= "<td style='padding-left:10px;'>".number_format($sub_jp, 0 ,',',',')."</td>";
			$html .= "<td style='padding-left:10px;'>".number_format($sub_ops, 0 ,',',',')."</td>";
			$html .= "<td style='padding-left:10px;'>".number_format($sub_net, 0 ,',',',')."</td>";
			$grand_jp 	+= $sub_jp;
			$grand_ops 	+= $sub_ops;
			$grand_net 	+= $sub_net;
			$sub_jp 	= 0;
			$sub_ops 	= 0;
			$sub_net 	= 0;
			$html .= "</tr>";
			$no_head++;
		}
		$html .= "<tr>";
		$html .= "<td align='center'></td>";
		$html .= "<td align = 'left' style='padding-right:10px;' colspan='5'>Grand Total</td>";
		$html .= "<td style='padding-left:10px;'>".number_format($grand_jp, 0 ,',',',')."</td>";
		$html .= "<td style='padding-left:10px;'>".number_format($grand_ops, 0 ,',',',')."</td>";
		$html .= "<td style='padding-left:10px;'>".number_format($grand_net, 0 ,',',',')."</td>";
		$html .= "</tbody>";
		$html .= "</table>";

		if ($params['type_file'] == 1 || $params['type_file'] == '1') {
			$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;		
			die();
			$total_row = $no_body+((int)$no_head*2);

			$total_row 	= $total_row+2;
			$print_area	= 'A1:I'.$total_row;
			$area_wrap	= 'A4:I'.$total_row;
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A4:I3')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
				/*'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			 	),
				'borders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
					// 'top' => array(
			 	// 	)
			 	),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
						'argb' => 'FFA0A0A0'
					),
					'endcolor' => array(
						'argb' => 'FFFFFFFF'
					)
				)*/
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(8);
			$objPHPExcel->getActiveSheet()->getStyle("A1:I2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 

			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:I'.$total_row)
						->getNumberFormat()
					    ->setFormatCode($textFormat);

			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/*$objPHPExcel->getActiveSheet()
						->getStyle('A4:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_TOP);*/
			$objPHPExcel->getActiveSheet()
						->getStyle('C5:C'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('D5:D'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('E5:E'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('F5:F'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('G5:G'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('H5:H'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('I5:I'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);/*
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);*/
			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_pelayanan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan pelayanan per pasien',$html);	
		}
		// echo json_encode($params);
	}
	
	function cetak_lap_pelunasan(){
		$common 	= $this->common;
		$result 	= $this->result;
		
		$param 		= json_decode($_POST['data']);
		$params = array(
			'tgl_akhir' 		=> $param->tgl_akhir,
			'tgl_awal' 			=> $param->tgl_awal,
			'type_file' 		=> $param->type_file,
			'no_kamar' 			=> $param->no_kamar,
			'kel_pas' 			=> $param->kel_pas,
			'kel_pas2' 			=> $param->kel_pas2,
		);
		$criteria_customer = "";
		if ($params['kel_pas'] == 2) {
			// PERSEORANGAN
			if (strtolower($params['kel_pas2']) == 'semua') {
				$query_kontraktor = $this->db->query("SELECT * from kontraktor where jenis_cust='0'");
				foreach ($query_kontraktor->result() as $result) {
					$criteria_customer .= "'".$result->kd_customer."',";
				}
				$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			}else{
				$criteria_customer = "'".$params['kel_pas2']."'";
			}
		}else if ($params['kel_pas'] == 3) {
			// PERUSAHAAN
			if (strtolower($params['kel_pas2']) == 'semua') {
				$query_kontraktor = $this->db->query("SELECT * from kontraktor where jenis_cust='1'");
				foreach ($query_kontraktor->result() as $result) {
					$criteria_customer .= "'".$result->kd_customer."',";
				}
				$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			}else{
				$criteria_customer = "'".$params['kel_pas2']."'";
			}
		}else if ($params['kel_pas'] == 4){
			// ASURANSI
			if (strtolower($params['kel_pas2']) == 'semua') {
				$query_kontraktor = $this->db->query("SELECT * from kontraktor where jenis_cust='2'");
				foreach ($query_kontraktor->result() as $result) {
					$criteria_customer .= "'".$result->kd_customer."',";
				}
				$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			}else{
				$criteria_customer = "'".$params['kel_pas2']."'";
			}
		}else{
			$query_kontraktor = $this->db->query("SELECT * from customer");
			foreach ($query_kontraktor->result() as $result) {
				$criteria_customer .= "'".$result->kd_customer."',";
			}
			$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
		}

		$queryHead = $this->db->query("SELECT distinct(no_transaksi), kd_pasien, jml_pt from rwi_laporan_pelunasan(
			'".$params['tgl_awal']."',
			'".$params['tgl_akhir']."',
			array[".$criteria_customer."],
			array[".substr($params['no_kamar'], 0, strlen($params['no_kamar'])-1)."]
		)");
		$html = "";
		$html .="<table border='1' width='100%'>";
		$html .="<thead>";
		$html .="<tr>";
		$html .="<th colspan='12'>LAPORAN PELUNASAN PIUTANG</th>";
		$html .="</tr>";
		$html .="<tr>";
		$html .="<th colspan='12'>Periode ".$params['tgl_awal']." s/d ".$params['tgl_akhir']."</th>";
		$html .="</tr>";

		$html .="<tr>";
		$html .="<th rowspan='2'>No</th>";
		$html .="<th rowspan='2'>Medrec</th>";
		$html .="<th rowspan='2'>Nama</th>";
		$html .="<th rowspan='2'>No Transaksi</th>";
		$html .="<th colspan='3'>Tanggal</th>";
		$html .="<th rowspan='2'>Uraian</th>";
		$html .="<th rowspan='2' width='120'>Piutang</th>";
		$html .="<th rowspan='2' width='120'>Bayar</th>";
		$html .="<th rowspan='2' width='120'>Sisa</th>";
		$html .="<th rowspan='2' width='8%'>Tanggal Cetak</th>";
		$html .="</tr>";

		$html .="<tr>";
		$html .="<th width='8%'>Masuk</th>";
		$html .="<th width='8%'>Keluar</th>";
		$html .="<th width='8%'>Verifikasi</th>";
		$html .="</tr>";

		$html .="</thead>";
		$html .="<tbody>";
		$no = 1;
		$row = 0;
		if ($queryHead->num_rows() >0) {
			foreach ($queryHead->result() as $resultHead) {
				/*$html .= "<tr>";
				$html .= "<td>".$resultHead->no_transaksi."</td>";
				$html .= "<td>".$resultHead->jml_pt."</td>";
				$html .= "</tr>";*/
				$total_pt = $resultHead->jml_pt;
				$queryBody = $this->db->query("SELECT * from rwi_laporan_pelunasan(
					'".$params['tgl_awal']."',
					'".$params['tgl_akhir']."',
					array[".$criteria_customer."],
					array[".substr($params['no_kamar'], 0, strlen($params['no_kamar'])-1)."]
				) where no_transaksi = '".$resultHead->no_transaksi."' and kd_pasien = '".$resultHead->kd_pasien."'");
				$row = $queryBody->num_rows();
				foreach ($queryBody->result() as $resultBody) {
					$tgl_cetak = $this->db->query("SELECT tgl_cetak FROM nota_bill WHERE kd_kasir = '".$resultBody->kd_kasir."' AND no_transaksi = '".$resultBody->no_transaksi."'");
					$tmp_tgl_cetak = "";
					if ($tgl_cetak->num_rows() > 0) {
						$tmp_tgl_cetak = date_format(date_create($tgl_cetak->row()->tgl_cetak), 'd/M/Y');
					}
					if (strtolower($resultBody->uraian) != "piutang") {
						$html .= "<tr>";
						$html .= "<td>".$no."</td>";
						$html .= "<td>".$resultBody->kd_pasien."</td>";
						$html .= "<td>".$resultBody->nama."</td>";
						$html .= "<td>".(string)$resultBody->no_transaksi."</td>";
						$html .= "<td>".date_format(date_create($resultBody->tgl_masuk), 'd/M/Y')."</td>";
						$html .= "<td>".date_format(date_create($resultBody->tgl_keluar), 'd/M/Y')."</td>";
						$html .= "<td>".date_format(date_create($resultBody->tgl_bayar), 'd/M/Y')."</td>";
						$html .= "<td>".$resultBody->uraian."</td>";
						$html .= "<td align='right'> ".number_format($total_pt,0,',',',')."</td>";
						$html .= "<td align='right'> ".number_format($resultBody->jml_bayar,0,',',',')."</td>";
						$html .= "<td align='right'> ".number_format($resultBody->sisa,0,',',',')."</td>";
						$html .= "<td align='right'> ".$tmp_tgl_cetak."</td>";
						$html .= "</tr>";
						$no++;
						$total_pt = $total_pt - $resultBody->jml_bayar;
					}
				}
			}
		}else{

		}
		
		$html .="</tbody>";
		$html .="</table>";
		// echo $html;
		if ($params['type_file'] == 1 || $params['type_file'] == '1') {
			$total_row = $no;

			$total_row 	= $total_row+3;
			$print_area	= 'A1:L'.$total_row;
			$area_wrap	= 'A5:L'.$total_row;
			/*$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	*/	
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, 'A3:L'.$total_row);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A1:L4')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle("A1:L2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 


			$objPHPExcel->getActiveSheet()
						->getStyle('A1:L4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:L'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:L'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_TOP);

						
			$objPHPExcel->getActiveSheet()
						->getStyle('I4:I'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('J4:J'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()
						->getStyle('K4:K'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth($this->cw_medrec);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth($this->cw_nama_pasien);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth($this->cw_no_transaksi);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth($this->cw_tanggal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth($this->cw_tanggal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth($this->cw_tanggal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth($this->cw_nominal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth($this->cw_nominal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth($this->cw_nominal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth($this->cw_tanggal);

			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:L'.$total_row)
						->getNumberFormat()
					    ->setFormatCode($textFormat);

			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_pelunasan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan pelunasan per pasien',$html);	
		}
	}
	function cetak_lap_jumlahharirawat(){
		$common 	= $this->common;
		$result 	= $this->result;
		
		$param 		= json_decode($_POST['data']);
		$params = array(
			'tgl_akhir' 		=> $param->tgl_akhir,
			'tgl_awal' 			=> $param->tgl_awal,
			'type_file' 		=> $param->type_file,
		);

		$query = $this->db->query("
									--TAGIHAN RWI
									SELECT x.UNIT, 
										sum(x.JML_PX_UMUM) as JML_PX_UMUM, 
										sum(x.TTL_HARIRWT_UMUM) as TTL_HARIRWT_UMUM,
										sum(x.JML_PX_BPJSPBI) as JML_PX_BPJSPBI, 
										sum(x.TTL_HARIRWT_BPJSPBI) as TTL_HARIRWT_BPJSPBI,
										sum(x.JML_PX_BPJSNONPBI) as JML_PX_BPJSNONPBI, 
										sum(x.TTL_HARIRWT_BPJSNONPBI) as TTL_HARIRWT_BPJSNONPBI,
										sum(x.JML_PX_LAIN) as JML_PX_LAIN, 
										sum(x.TTL_HARIRWT_LAIN) as TTL_HARIRWT_LAIN
									FROM
									(
										SELECT 'RWI'::text as UNIT, u.TGL_KELUAR, u.Tgl_masuk,
											case when u.kd_customer = '0000000001' then count(p.Kd_Pasien) end as JML_PX_UMUM,
											case when u.kd_customer = '0000000001' then SUM(u.TGL_KELUAR::date-u.TGL_MASUK::date) end as TTL_HARIRWT_UMUM,			
											case when u.kd_customer in ('0000000003','0000000043') then count(p.Kd_Pasien) end as JML_PX_BPJSPBI,
											case when u.kd_customer in ('0000000003','0000000043') then SUM(u.TGL_KELUAR::date-u.TGL_MASUK::date) end as TTL_HARIRWT_BPJSPBI,
											case when u.kd_customer in ('0000000002','0000000037','0000000044','0000000008','0000000033','0000000039') then count(p.Kd_Pasien) end as JML_PX_BPJSNONPBI,
											case when u.kd_customer in ('0000000002','0000000037','0000000044','0000000008','0000000033','0000000039') then SUM(u.TGL_KELUAR::date-u.TGL_MASUK::date) end as TTL_HARIRWT_BPJSNONPBI,
											case when u.kd_customer not in ('0000000001','0000000002','0000000003','0000000008','0000000033','0000000037','0000000039','0000000043','0000000044') then count(p.Kd_Pasien) end as JML_PX_LAIN,
											case when u.kd_customer not in ('0000000001','0000000002','0000000003','0000000008','0000000033','0000000037','0000000039','0000000043','0000000044') then SUM(u.TGL_KELUAR::date-u.TGL_MASUK::date) end as TTL_HARIRWT_LAIN
											
									--		case when u.kd_customer = '0000000001' then count(p.Kd_Pasien) end as JML_PX_UMUM,
									-- 		case when u.kd_customer = '0000000001' then sum(DATE_PART('day', u.Tgl_masuk)- DATE_PART('day', u.TGL_KELUAR)) + 1 end as TTL_HARIRWT_UMUM,
									-- 		case when u.kd_customer in ('0000000003','0000000043') then count(p.Kd_Pasien) end as JML_PX_BPJSPBI,
									-- 		case when u.kd_customer in ('0000000003','0000000043') then sum(DATE_PART('day', u.TGL_KELUAR)- DATE_PART('day', u.Tgl_masuk)) + 1 end as TTL_HARIRWT_BPJSPBI,
									-- 		case when u.kd_customer in ('0000000002','0000000037','0000000044','0000000008','0000000033','0000000039') then count(p.Kd_Pasien) end as JML_PX_BPJSNONPBI,
									-- 		case when u.kd_customer in ('0000000002','0000000037','0000000044','0000000008','0000000033','0000000039') then sum(DATE_PART('day', u.TGL_KELUAR)- DATE_PART('day', u.Tgl_masuk)) + 1 end as TTL_HARIRWT_BPJSNONPBI,
									-- 		case when u.kd_customer not in ('0000000001','0000000002','0000000003','0000000008','0000000033','0000000037','0000000039','0000000043','0000000044') then count(p.Kd_Pasien) end as JML_PX_LAIN,
									-- 		case when u.kd_customer not in ('0000000001','0000000002','0000000003','0000000008','0000000033','0000000037','0000000039','0000000043','0000000044') then sum(DATE_PART('day', u.TGL_KELUAR)- DATE_PART('day', u.Tgl_masuk)) + 1 end as TTL_HARIRWT_LAIN
										FROM (((Nginap g 
											INNER JOIN Transaksi t ON t.Kd_Pasien = g.Kd_Pasien AND t.Kd_Unit = g.Kd_Unit 
												AND t.Urut_Masuk = g.Urut_Masuk AND t.Tgl_Transaksi = g.Tgl_Masuk AND t.Tgl_CO = g.Tgl_Keluar) 
											INNER JOIN Kunjungan u ON t.Kd_Pasien = u.Kd_Pasien AND t.Kd_Unit = u.Kd_Unit 
												AND t.Urut_Masuk = u.Urut_Masuk AND t.Tgl_Transaksi = u.Tgl_Masuk AND t.Tgl_CO = u.Tgl_Keluar) 
											INNER JOIN Pasien p ON g.Kd_Pasien = p.Kd_Pasien) 
											INNER JOIN KAMAR k ON g.Kd_Unit_Kamar = k.Kd_Unit AND g.No_Kamar = k.No_Kamar 
										WHERE t.CO_Status = 't' AND g.Akhir = 't'  
											AND t.Kd_Unit IN (SELECT Kd_Unit FROM Unit WHERE Parent in ('1002','1001','1003')) 
											--AND extract('year' from g.Tgl_Keluar) = '2017' AND extract(month from g.Tgl_Keluar) = '8'
											AND g.Tgl_Keluar between '".$params['tgl_awal']."' AND '".$params['tgl_akhir']."'
											AND g.no_kamar in 
											('651','101','104','105','106','107','108','109','110','112','113','115','362','635','650','655','109','201','202','203','204',
										'205','206','207','208','209','210','363','650','301','302','303','304','305','306','307','308','309','311','313','364','650',
										'103','102','202','203','101','201','343','344','345','346','347','348','349','350','352','355','356','357','358','639','658',
										'337','338','339','342','365','636','640','652','330','331','332','333','334','335','336','351','353','648','649','361','637',
										'638','237','238','239','240','241','242','243','244','245','318','319','320','321','322','323','324','325','326','327','328',
										'329','656','657','314','315','316','317','360','359','641','642','643','644','645','646','647','693','694','695','696','697',
										'698','699','700','701','702','703','704','705','706','707','708','709','710','711','712','713',
										'714','715','716','717','718','660','661','662','663','664','665','666','667','668','669','670','671','672','673','674','675',
										'676','677','678','679','680','681','682','683','684','685','686','687','688','689','690','691','692','723','719','720','721',
										'722','724')
										Group by u.kd_customer, u.TGL_KELUAR, u.Tgl_masuk
									) x
									group by x.unit
");
		$html = "";
		$html .="<table border='1' width='100%'>";
		$html .="<thead>";
		$html .="<tr>";
		$html .="<th colspan='10'>LAPORAN HARI RAWAT PASIEN RAWAT INAP</th>";
		$html .="</tr>";
		$html .="<tr>";
		$html .="<th colspan='10'>Periode ".$params['tgl_awal']." s/d ".$params['tgl_akhir']."</th>";
		$html .="</tr>";

		$html .="<tr>";
		$html .="<th rowspan='2' width='8%'>No</th>";
		$html .="<th rowspan='2' width='8%'>UNIT</th>";
		$html .="<th colspan='2'>UMUM</th>";
		$html .="<th colspan='2'>BPJS PBI</th>";
		$html .="<th colspan='2'>BPJS NON PBI</th>";
		$html .="<th colspan='2'>LAINNYA</th>";
		$html .="</tr>";

		$html .="<tr>";
		$html .="<th width='8%'>JUMLAH PASIEN</th>";
		$html .="<th width='8%'>TOTAL HARI RAWAT</th>";
		
		$html .="<th width='8%'>JUMLAH PASIEN</th>";
		$html .="<th width='8%'>TOTAL HARI RAWAT</th>";
		
		$html .="<th width='8%'>JUMLAH PASIEN</th>";
		$html .="<th width='8%'>TOTAL HARI RAWAT</th>";
		
		$html .="<th width='8%'>JUMLAH PASIEN</th>";
		$html .="<th width='8%'>TOTAL HARI RAWAT</th>";
		
		$html .="</tr>";

		$html .="</thead>";
		$html .="<tbody>";
		$no = 1;
		$row = 0;
		if ($query->num_rows() >0) {
			foreach ($query->result() as $resultBody) {
				
						$html .= "<tr>";
						$html .= "<td>".$no."</td>";
						$html .= "<td>".$resultBody->unit."</td>";
						$html .= "<td align='right'>".$resultBody->jml_px_umum."</td>";
						$html .= "<td align='right'>".$resultBody->ttl_harirwt_umum."</td>";
						$html .= "<td align='right'>".$resultBody->jml_px_bpjspbi."</td>";
						$html .= "<td align='right'>".$resultBody->ttl_harirwt_bpjspbi."</td>";
						$html .= "<td align='right'>".$resultBody->jml_px_bpjsnonpbi."</td>";
						$html .= "<td align='right'>".$resultBody->ttl_harirwt_bpjsnonpbi."</td>";
						$html .= "<td align='right'> ".$resultBody->jml_px_lain."</td>";
						$html .= "<td align='right'> ".$resultBody->ttl_harirwt_lain."</td>";
						$html .= "</tr>";
						$no++;
						//$total_pt = $total_pt - $resultBody->jml_bayar;
					
			}
		}else{

		}
		
		$html .="</tbody>";
		$html .="</table>";
		// echo $html;
		if ($params['type_file'] == 1 || $params['type_file'] == '1') {
			$total_row = $no;

			$total_row 	= $total_row+3;
			$print_area	= 'A1:K'.$total_row;
			$area_wrap	= 'A5:K'.$total_row;
			/*$name='Laporan_pulang_pasien.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attschment; filename=".$name);	
			echo $html;	*/	
			$table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			// Set autofilter
			 // Always include the complete filter range!
			 // Excel does support setting only the caption
			 // row, but that's not a best practise...
			$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
			 
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			 
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	),/*
					'font'  => array(
						'size'  => 9,
						'name'  => 'Courier New'
					)*/
			 	)
			);
			 
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, 'A3:K'.$total_row);
			 
			// Set style for header row using alternative method
			$objPHPExcel->getActiveSheet()->getStyle('A1:K4')->applyFromArray(
			 array(
			 'font' => array(
			 	'bold' => true
			 	),
			 )
			);
			 
			// Add a drawing to the worksheet
			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$objDrawing->setName('Logo');
			$objDrawing->setDescription('Logo');
			$objDrawing->setPath('../images/logo2.png');
			$objDrawing->setCoordinates('B2');
			$objDrawing->setHeight(120);
			$objDrawing->setWidth(120);
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
			 
			/*$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle($print_area)->getFont()->setSize(7);*/
			# Fungsi untuk set TYPE FONT 
			/*$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 9,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);*/
			# END Fungsi untuk set TYPE FONT 
						
			$objPHPExcel->getActiveSheet()->getStyle("A1:K2")->getFont()->setSize(12);
			# Fungsi untuk set ALIGNMENT 


			$objPHPExcel->getActiveSheet()
						->getStyle('A1:K4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:K'.$total_row)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()
						->getStyle('A5:K'.$no)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_TOP);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(false);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
			# Set Password
			// $objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth($this->cw_medrec);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth($this->cw_nama_pasien);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth($this->cw_no_transaksi);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth($this->cw_tanggal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth($this->cw_tanggal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth($this->cw_tanggal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth($this->cw_nominal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth($this->cw_nominal);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth($this->cw_nominal);

			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle('A4:K'.$total_row)
						->getNumberFormat()
					    ->setFormatCode($textFormat);

			/*$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);*/
			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
			// $excel->getActiveSheet();
			// $sheet->getSheetView()->setZoomScale(300);
			/*$objPHPExcel->getActiveSheet()
					    ->getSheetView()
					    ->setZoomScale(85);*/
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
            header('Content-Disposition: attachment;filename=Laporan_pelunasan_per_pasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
			echo $html;
		}else{
			$this->common->setPdf('L','Laporan pelunasan per pasien',$html);	
		}
	}
	/*
		LAPORAN RESUME PASIEN
	 */
	public function laporan_resume($kd_pasien=null, $kd_unit=null, $tgl_masuk=null, $print=null, $type_file = null, $title = null){
		// http://192.168.1.25/medismart_/index.php/rawat_inap/function_lap_RWI/laporan_resume/6-71-24-97/10021/2017-07-21/true/false
		$common 	= $this->common;
		$result 	= $this->result;
		$params = array(
			'kd_pasien' => $kd_pasien,
			'kd_unit'   => $kd_unit,
			'tgl_masuk' => $tgl_masuk,
			'print'     => $print,
			'type_file' => $type_file,
		);

		$query = $this->db->query("SELECT 
				k.kd_pasien, 
				ps.nama,
				u.nama_unit || '/' || kmr.nama_kamar as unit_ruang_rawat, 
				dkt.nama as dokter, 
				k.tgl_masuk, 
				getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk,k.urut_masuk) as deskripsi,
				gettagihan(t.kd_kasir, t.no_transaksi) as tagihan,
				getallpemeriksaanlabpk(k.tgl_masuk :: date, k.kd_pasien) as pemeriksaan_lab_pk,
				getallpemeriksaanlabpa(k.tgl_masuk :: date, k.kd_pasien) as pemeriksaan_lab_pa,
				getallpemeriksaanrad(k.tgl_masuk :: date, k.kd_pasien) as pemeriksaan_rad,
				getallresepobat(k.tgl_masuk::date, k.kd_pasien, k.kd_unit) as resep_obat
			from kunjungan k
				inner join unit u on u.kd_unit = k.kd_unit 
				inner join nginap ng on k.kd_pasien = ng.kd_pasien and k.kd_unit = ng.kd_unit and k.tgl_masuk = ng.tgl_masuk and k.urut_masuk = ng.urut_masuk and akhir = '1'
				inner join kamar kmr on kmr.kd_unit = ng.kd_unit_kamar and kmr.no_kamar = ng.no_kamar
				inner join transaksi t on k.kd_pasien = t.kd_pasien and k.kd_unit = t.kd_unit and k.tgl_masuk = t.tgl_transaksi and k.urut_masuk = t.urut_masuk
				inner join pasien ps on ps.kd_pasien = k.kd_pasien 
				inner join dokter dkt on dkt.kd_dokter = k.kd_dokter
			where k.tgl_masuk = '".$params['tgl_masuk']."' and k.kd_unit = '".$params['kd_unit']."' and k.kd_pasien = '".$params['kd_pasien']."'");
		$html = "";
		$html .= "
			<table width='100%'>
				<tr>
					<th width='25%' align='left'>Kode Pasien</th>
					<th colspan='5' align='left'>: ".$params['kd_pasien']."</th>
				</tr>
				<tr>
					<th width='25%' align='left'>Nama Pasien</th>
					<th colspan='5' align='left'>: ".$query->row()->nama."</th>
				</tr>
				<tr>
					<th width='25%' align='left'>Ruang Perawatan</th>
					<th colspan='5' align='left'>: ".$query->row()->unit_ruang_rawat."</th>
				</tr>
				<tr>
					<th width='25%' align='left'>Tanggal Masuk</th>
					<th colspan='5' align='left'>: ".date_format(date_create($params['tgl_masuk']), 'd/M/Y')."</th>
				</tr>
				<tr>
					<th width='25%' align='left'>Dokter Penanggung Jawab</th>
					<th colspan='5' align='left'>: ".$query->row()->dokter."</th>
				</tr>
			</table>
		";
		$html .= "<br>";
		$html .= "
			<table width='100%' border='1' cellspacing='0'>
				<thead>
					<tr>
						<th width='25%' rowspan='2'>Diagnosa</th>
						<th colspan='3'>Tindakan Penunjang</th>
						<th rowspan='2' width='15%'>Obat</th>
						<th rowspan='2' width='15%'>Biaya Penagihan RS</th>
					</tr>
					<tr>
						<th>Lab. Patalogi Klinik</th>
						<th>Lab. Patalogi Anatomi</th>
						<th>Radiologi</th>
					</tr>
				</thead>";

		$html .="<tbody>";
		// $diagnosa = explode(";", $query->row()->deskripsi);
		// $diagnosa   = $query->row()->deskripsi;
		// $resep_obat =  explode(";", $query->row()->resep_obat);
		$diagnosa 	=  explode(";", $query->row()->deskripsi);
		$lab_pa 	=  explode(";", $query->row()->pemeriksaan_lab_pa);
		$lab_pk 	=  explode(";", $query->row()->pemeriksaan_lab_pk);
		$rad 		=  explode(";", $query->row()->pemeriksaan_rad);
		$resep_obat =  explode(";", $query->row()->resep_obat);
		$data_diagnosa = "";
		for ($i=0; $i < count($diagnosa); $i++) { 
			$data_diagnosa .= " - ".$diagnosa[$i]."<br>";
		}

		$data_lab_pa = "";
		for ($i=0; $i < count($lab_pa); $i++) { 
			$data_lab_pa .= " - ".$lab_pa[$i]."<br>";
		}

		$data_lab_pk = "";
		for ($i=0; $i < count($lab_pk); $i++) { 
			$data_lab_pk .= " - ".$lab_pk[$i]."<br>";
		}

		$data_rad = "";
		for ($i=0; $i < count($rad); $i++) { 
			$data_rad .= " - ".$rad[$i]."<br>";
		}

		$data_resep_obat = "";
		for ($i=0; $i < count($resep_obat); $i++) { 
			$data_resep_obat .= " - ".$resep_obat[$i]."<br>";
		}

		$html .="<tr>";
			$html .="<td valign='top' style='padding:5px;'>".$data_diagnosa."</td>";
			$html .="<td valign='top' style='padding:5px;'>".$data_lab_pa."</td>";
			$html .="<td valign='top' style='padding:5px;'>".$data_lab_pk."</td>";
			$html .="<td valign='top' style='padding:5px;'>".$data_rad."</td>";
			$html .="<td valign='top' style='padding:5px;'>".$data_resep_obat."</td>";
			$html .="<td valign='top' style='padding:5px;' align='right'> Rp.".number_format($query->row()->tagihan,0,'.',',')."</td>";
		$html .="</tr>";
		
		// $induk = explode(";", $this->input->post('List'));
		$html .="</tbody>";
		$html .="</table>";
		// echo $html;
		// die();
		if ($params['print'] == false || $params['print'] == 'false') {
			/*$content ="
			<code>
				<pre>".htmlspecialchars(file_get_contents($this->common->setPdf('L','Resume Pasien',$html)))."</pre>
			</code>";
	    	echo $content;*/
	    	echo $html;
		}else{
			if ($params['type_file'] == 'true') {
				// $no 		= ((int)$queryPG_body->num_rows()+((int)$queryPG_header->num_rows()*2)+5);
				/*$print_area	= 'A1:L'.$no;
				$area_wrap	= 'A4:L'.$no;*/
				$name='Laporan_resume_pasien.xls';
				header("Content-Type: application/vnd.ms-excel");
				header("Expires: 0");
				header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
				header("Content-disposition: attschment; filename=".$name);	
				echo $html;	
			}else{
				$this->common->setPdf('L','Resume Pasien',$html);	
			}
		}
	}
	
	public function laporan_cetak_sep(){	
		$title = "Laporan Cetak SEP";	
		$common=$this->common;
   		$result=$this->result;
		$url           	= "Doc Asset/format_laporan/".$title.".xls";

		$param = json_decode($_POST['data']);
		
		$tgl_awal		= $param->start_date;
		$tgl_akhir		= $param->last_date;
		$kd_customer 	= $param->tmp_kd_customer;
		$kd_unit 		= $param->tmp_kd_unit;
		$sorting_by		= $param->sorting_by;
		$type_file 		= $param->type_file;

		// var_dump($param);die();
		$query = "SELECT 
				K.kd_pasien,
				ps.nama,
				u.nama_unit,
				k.tgl_masuk,
				c.customer as kelompok_pasien,
				k.jam_masuk,
				k.no_sjp as no_sep,
				ps.no_asuransi as no_asuransi 
			FROM
				kunjungan
				K INNER JOIN unit u ON K.kd_unit = u.kd_unit
				INNER JOIN customer C ON K.kd_customer = C.kd_customer
				INNER JOIN pasien ps ON K.kd_pasien = ps.kd_pasien 
			WHERE
				K.tgl_masuk BETWEEN '$tgl_awal' AND '$tgl_akhir' 
				AND K.kd_customer IN ( $kd_customer ) 
				AND u.kd_unit IN ( $kd_unit ) 
			ORDER BY
				K.tgl_masuk,
				u.nama_unit";
		$label_laporan = "";
		$query_result  = $this->db->query($query);

		if ($query_result->num_rows() > 0) {
			if ($type_file === false) {
				$table = "";
				$table .= "<table width='100%' cellspacing='0' border='0'>";
				$table .= "<tr>";
				$table .= "<th align='center'>Laporan Cetak SEP</th>";
				$table .= "</tr>";
				$table .= "<tr>";
				$table .= "<th align='center'>Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_akhir), 'Y-m-d')."</th>";
				$table .= "</tr>";
				$table .= "</table>";
				$table .= "</br>";
				$table .= "</hr>";
				$table .= "<table width='100%' cellspacing='0' border='1'>";
				$table .= "<thead>";
				$table .= "<tr>";
				$table .= "<th>No</th>";
				$table .= "<th>Medrec</th>";
				$table .= "<th>Nama</th>";
				$table .= "<th>Unit</th>";
				$table .= "<th>Tgl Masuk</th>";
				$table .= "<th>Jam Masuk</th>";
				$table .= "<th>Penjamin</th>";
				$table .= "<th>No SEP</th>";
				$table .= "<th>No Asuransi</th>";
				$table .= "</tr>";
				$table .= "</thead>";
				$no = 1;
				foreach ($query_result->result() as $result) {
					if(strlen($result->no_sep) > 0){
						$table .= "<tbody>";
						$table .= "<tr>";
						$table .= "<td>".$no."</td>";
						$table .= "<td>".$result->kd_pasien."</td>";
						$table .= "<td>".$result->nama."</td>";
						$table .= "<td>".$result->nama_unit."</td>";
						$table .= "<td>".date_format(date_create($result->tgl_masuk), 'Y-m-d')."</td>";
						$table .= "<td>".date_format(date_create($result->jam_masuk), 'H:i:s')."</td>";
						$table .= "<td>".$result->kelompok_pasien."</td>";
						$table .= "<td>".$result->no_sep."</td>";
						$table .= "<td>".$result->no_asuransi."</td>";
						$table .= "</tr>";
						$table .= "</tbody>";
						$no++;
					}
				}
				$table .= "</table>";

				$this->common->setPdf('P',$title,$table);
				// echo $table;
			}else{
				$phpExcel = new PHPExcel();
				$phpExcel = PHPExcel_IOFactory::load($url);
				$sheet    = $phpExcel ->getActiveSheet();
				$no       = 1;
				$baris    = 7;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_akhir), 'Y-m-d'));
				foreach ($query_result->result() as $result) {
					if(strlen($result->no_sep) > 0){
						$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
						$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->kd_pasien);
						$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->nama);
						$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->nama_unit);
						$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->kelompok_pasien);
						$phpExcel->getActiveSheet()->setCellValue('F'.$baris, date_format(date_create($result->tgl_masuk), 'Y-m-d'));
						$phpExcel->getActiveSheet()->setCellValue('G'.$baris, date_format(date_create($result->jam_masuk), 'H:i:s'));
						$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->no_sep);
						$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $result->no_asuransi);
						
						$no++;
						$baris++;
					}
				}
				header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
				header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $title).'.xls'); # specify the download file name
				header('Cache-Control: max-age=0');
				
				# Creates a writer to output the $objPHPExcel's content
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}
	
	public function cetakLapRWICutOffPerBulan(){		
		$common=$this->common;
   		$result=$this->result;

		$param=json_decode($_POST['data']);
		
		$tgl_awal		= $param->tgl_awal;
		$tgl_akhir		= $param->tgl_akhir;
		$kd_customer 	= $param->kd_customer;
		$kel_pas 		= $param->kel_pas;
		$ckelpas		= $param->kel_pas;
		$customer 		= $param->kd_customer;
		$parent 		= $param->parent;
		$no_kamar 		= $param->no_kamar;
		$type_file 		= $param->type_file;
		$params['kel_pas']  = $kel_pas;
		$params['kel_pas2'] = $kd_customer;
		$criteria 		= "array[".$parent."]";
		$criteria_kamar	= "array[".$no_kamar."]";
		$label_laporan = "";

		$ckelpas="";
		$criteria_customer = "";
		/*if ($params['kel_pas'] == 2) {
			// PERSEORANGAN
			$ckelpas = "Perseorangan";
		}else if ($params['kel_pas'] == 3) {
			// PERUSAHAAN
			$ckelpas = "Perusahaan";
		}else if ($params['kel_pas'] == 4){
			// ASURANSI
			$ckelpas = "Asuransi";
		}else{
			$ckelpas = "Semua";
		}*/

		if ($params['kel_pas'] == 2) {
			// PERSEORANGAN
			if (strtolower($params['kel_pas2']) == 'semua' || $params['kel_pas2'] == 1) {
				$query_kontraktor = $this->db->query("SELECT * from kontraktor where jenis_cust='0'");
				foreach ($query_kontraktor->result() as $result) {
					$criteria_customer .= "'".$result->kd_customer."',";
				}
				$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			}else{
				$criteria_customer = "'".$params['kel_pas2']."'";
			}
		}else if ($params['kel_pas'] == 3) {
			// PERUSAHAAN
			if (strtolower($params['kel_pas2']) == 'semua') {
				$query_kontraktor = $this->db->query("SELECT * from kontraktor where jenis_cust='1'");
				foreach ($query_kontraktor->result() as $result) {
					$criteria_customer .= "'".$result->kd_customer."',";
				}
				$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			}else{
				$criteria_customer = "'".$params['kel_pas2']."'";
			}
		}else if ($params['kel_pas'] == 4){
			// ASURANSI
			if (strtolower($params['kel_pas2']) == 'semua') {
				$query_kontraktor = $this->db->query("SELECT * from kontraktor where jenis_cust='2'");
				foreach ($query_kontraktor->result() as $result) {
					$criteria_customer .= "'".$result->kd_customer."',";
				}
				$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
			}else{
				$criteria_customer = "'".$params['kel_pas2']."'";
			}
		}else{
			$query_kontraktor = $this->db->query("SELECT * from customer");
			foreach ($query_kontraktor->result() as $result) {
				$criteria_customer .= "'".$result->kd_customer."',";
			}
			$criteria_customer = substr($criteria_customer, 0, strlen($criteria_customer)-1);
		}

   		$title = "LAPORAN KOMPONEN HPP PASIEN RAWAT INAP ".$label_laporan." PER PERIODE (CUT OFF)";
		
		if($kd_customer != 'Semua'){
			$customer = $this->db->query("select customer from customer where kd_customer ='".$kd_customer."'")->row()->customer;
		}
		
		$queryHead = $this->db->query( "SELECT  * from rwi_laporan_cut_off('".$tgl_awal."','".$tgl_akhir."', array[".$criteria_customer."],".$criteria.", ".$criteria_kamar.")");
		$query = $queryHead->result();
		$html='';
		//-------------------------MENGATUR TAMPILAN TABEL HASIL LABORATORIUM------------------------------------------------
		$html.='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th colspan="13" align="center">'.$title.'</th>
					</tr>
					<tr>
						<th  colspan="13" align="center"> Periode : '.$tgl_awal.' s/d '.$tgl_akhir.'</th>
					</tr>
					<tr>
						<th  colspan="13" align="center"> Kelompok Pasien : '.$ckelpas.' ('.$customer.')</th>
					</tr>
				</tbody>
			</table><br>';
			
		$html.='
			<table class="t1" border = "1">
			<thead>
			  <tr>
					<th rowspan="2" bgcolor="#8db7fc">No.</th>
					<th rowspan="2" bgcolor="#8db7fc">Customer</th>
					<th rowspan="2" bgcolor="#8db7fc">No. MR</th>
					<th rowspan="2" bgcolor="#8db7fc">Nama Pasien</th>
					<th rowspan="2" bgcolor="#8db7fc">Total Biaya <br> (POP)<br> (6+7+10+11+13)</th>
					<th rowspan="2" bgcolor="#8db7fc">Jasa Sarana</th>
					<th rowspan="2" bgcolor="#8db7fc">Jasa Pelayanan</th>
					<th colspan="3" bgcolor="#8db7fc">Farmasi</th>
					<th rowspan="2" bgcolor="#8db7fc">Biaya Makan</th>
					<th rowspan="2" bgcolor="#8db7fc">HPP Makan</th>
					<th rowspan="2" bgcolor="#8db7fc">Transfusi</th>
			  </tr>
			  <tr>
					<th bgcolor="#8db7fc">HPP Obat</th>
					<th bgcolor="#8db7fc">HPP Alkes</th>
					<th bgcolor="#8db7fc">Resep  <br> (8+9)*Markup</th>
			  </tr>
			  <tr>
					<th bgcolor="#8db7fc">1</th>
					<th bgcolor="#8db7fc">2</th>
					<th bgcolor="#8db7fc">3</th>
					<th bgcolor="#8db7fc">4</th>
					<th bgcolor="#8db7fc">5</th>
					<th bgcolor="#8db7fc">6</th>
					<th bgcolor="#8db7fc">7</th>
					<th bgcolor="#8db7fc">8</th>
					<th bgcolor="#8db7fc">9</th>
					<th bgcolor="#8db7fc">10</th>
					<th bgcolor="#8db7fc">11</th>
					<th bgcolor="#8db7fc">12</th>
					<th bgcolor="#8db7fc">13</th>
			  </tr>
			</thead>';
		if(count($query) > 0) {
			
			$no=0;
			foreach ($query as $line) 
			{
				$no++;
				$html.='

				<tbody>
						<tr class="headerrow"> 
							<td>'.$no.'</td>
							<td>'.$line->customer.'</td>
							<td>'.$line->kd_pasien.'</td>
							<td>'.$line->nama.'</td>
							<td align="right">'.$line->total_biaya.'</td>
							<td align="right">'.$line->jassar.'</td>
							<td align="right">'.$line->jasyan.'</td>
							<td align="right">'.$line->hppobat.'</td>
							<td align="right">'.$line->hppalkes.'</td>
							<td align="right">'.$line->resep.'</td>
							<td align="right">'.$line->makan.'</td>
							<td align="right">'.$line->transfusi.'</td>
							<td align="right">'.$line->hpp_makan.'</td>
						</tr>

				';
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="13" align="center">Data tidak ada</th>
				</tr>

			';		
		}
		$html.='</tbody></table>';
		$title='Lap_HPPRWI'.$label_laporan.'_'.date('d/M/Y');
		$prop=array('foot'=>true);
		if ($type_file == 0) {
			$this->common->setPdf('P',$title,$html);	

		}else{
			$name=$title.'.xls';
			header("Content-Type: application/vnd.ms-excel");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attachment; filename=".$name);
			echo $html;
		}
	}
	
	public function cetak_laporan_lain_lain(){
		$params = json_decode($_POST['data']);

		$total 	= array();
		$order_by = "";

		if (isset($params->order_by)) {
			$order_by = "Order By ".str_replace(" ", "_", $params->order_by);
		}
		$query_laporan 	= $this->db->query("SELECT * from laporan_lain_lain_rwi where kd_sp = '".$params->laporan."'");
		$nama_file     	= $query_laporan->row()->nama_excel;
		$url           	= "Doc Asset/format_laporan/".$nama_file.".xls";
		$query 			= $this->db->query("SELECT ".$query_laporan->row()->columns." from ".$query_laporan->row()->kd_sp."('".$params->start_date."', '".$params->end_date."') ".$order_by);
		
		$phpExcel = new PHPExcel();
		$phpExcel = PHPExcel_IOFactory::load($url);
		$sheet    = $phpExcel ->getActiveSheet();
			
			$baris 	= $query_laporan->row()->baris_awal_isi_excel;
			$no 	= 1;
			if ($query->num_rows() > 0) {

				foreach ($query->result_array() as $value) {
					$col 	= 'B';
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					for ($i=1; $i <= $query_laporan->row()->jml_col; $i++) { 
						$phpExcel->getActiveSheet()->setCellValue($col.$baris, $value['col'.$i]);
						$col++;
					}
					$baris++;
					$no++;
				}
				for ($i=1; $i <= $query_laporan->row()->jml_col; $i++) {
					$total[$i] = null;
				} 

				for ($i=1; $i <= $query_laporan->row()->jml_col; $i++) { 
					foreach ($query->result_array() as $value) {
						if (is_numeric($value['col'.$i]) === true) {
							$total[$i] += (int)$value['col'.$i]; 
						}
					}
				}

				$col 	= 'B';
				for ($i=1; $i <= $query_laporan->row()->jml_col; $i++) { 
					$phpExcel->getActiveSheet()->setCellValue($col.$baris, $total[$i]);
					$col++;
				}
				// die();
	            header('Content-Type: text/html; charset=ISO-8859-1'); # header for .xls file
	            header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $query_laporan->row()->deskripsi_sp).'.xls'); # specify the download file name
	            header('Cache-Control: max-age=0');

	            # Creates a writer to output the $objPHPExcel's content
	            $writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
	            $writer->save('php://output');
				echo $html;
			}
	}

	function cetak_lap_PelDokterPerTindakan_fungsi_revisi(){
		$html='';
		$nama_file='Pelayanan Dokter Per Tindakan (RWI)';
		
		$param=json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA
		$unit      = $param->unit; 
		$kd_unit   = substr($unit, 0, -1); //irna rs,ugd,pav ('1001','1002','1003')
		$kd_unit   = str_replace("'","",$kd_unit); //menghilangkan kutip
		
		$tgl_awal  = $param->tgl_awal;
		$tgl_akhir = $param->tgl_akhir;
		
		#KRITERIA KELOMPOK PASIEN
		$kel_pas = $param->kel_pas; // 0 semua
		if($kel_pas == '1'){
			$nama_kel_pas = 'Semua';
			$kd_kelpas='0,1,2';
		}else if($kel_pas == '2'){
			$nama_kel_pas = 'Perseorangan';
			$kd_kelpas=0;
		}else if($kel_pas == '3'){
			$nama_kel_pas = 'Perusahaan';
			$kd_kelpas=1;
		}else if($kel_pas == '4'){
			$nama_kel_pas = 'Asuransi';
			$kd_kelpas=2;
		}
		
		#KRTIERIA JENIS CUSTOMER
		$kel_pas2 = $param->kel_pas2;//0 semua
		$k_customer ='';
		if($kel_pas == '1'){
			$get_customer=$this->db->query("select kd_customer from kontraktor ")->result();
			$k_customer ='';
			$i=1;
			foreach($get_customer as $line){
				if($i==1){
					$k_customer="'".$line->kd_customer."'";
				}else{
					$k_customer=$k_customer.","."'".$line->kd_customer."'";
				}
				$i++;
			}
			$customer='Semua';
		}
		else if($kel_pas == '2'){
			$customer='Umum';
			$k_customer="'0000000001'";
		}else{
			if($kel_pas2 != '0'){
			
				if($kel_pas2 == 'Semua'){
					$get_customer=$this->db->query("select kd_customer from kontraktor where jenis_cust='".$kd_kelpas."'")->result();
					$k_customer ='';
					$i=1;
					foreach($get_customer as $line){
						if($i==1){
							$k_customer="'".$line->kd_customer."'";
						}else{
							$k_customer=$k_customer.","."'".$line->kd_customer."'";
						}
						$i++;
					}
					$customer='Semua';
				}else{
					$customer=$this->db->query("select * from customer where kd_customer='".$kel_pas2."'")->row()->customer;
					$k_customer = "'$kel_pas2'";
				}
				
			}else{
				$customer='Semua';
			}
		}
		$k_customer= str_replace("'","",$k_customer ); //menghilangkan kutip
		
		#KRITERIA TYPE FILE
		$type_file = $param->type_file;
		
		#KRITERIA KLAS PRODUK
		$kelas_produk = $param->kelas_produk; //if kelas_produk=9999 
		if($kelas_produk == 9999 || $kelas_produk == 'Semua'){
			$get_kelas_produk = $this->db->query("select Kd_klas,Klasifikasi from Klas_produk")->result();
		
			$k_kelas_produk ='';
			$i=1;
			foreach($get_kelas_produk as $line){
				if($i==1){
					$k_kelas_produk="'".$line->kd_klas."'";
				}else{
					$k_kelas_produk=$k_kelas_produk.","."'".$line->kd_klas."'";
				}
				$i++;
			}
		}else{
			$k_kelas_produk = "'".$kelas_produk."'";
		}
		$k_kelas_produk= str_replace("'","",$k_kelas_produk); //menghilangkan kutip
		
		
		#KRITERIA PASIEN PULANG
		$pasien_pulang = $param->pasien_pulang; // checkbox pasien pulang , true/false

		#KRITERIA PEMBAYARAN (PAYMENT)
		$tmpKdPay='';
		$arrayDataPay = $param->kd_pay;
		
		#KD PAY tidak semua
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1); //arr kd pay
		
		#KD PAY semua
		$semua_pay="'"."999"."'";
		if($tmpKdPay==$semua_pay ){ //semua kd_pay
			$get_kd_pay= $this->db->query("SELECT kd_pay, uraian FROM payment")->result();
			$k_kd_pay ='';
			$i=1;
			foreach($get_kd_pay as $line){
				if($i==1){
					$k_kd_pay="'".$line->kd_pay."'";
				}else{
					$k_kd_pay=$k_kd_pay.","."'".$line->kd_pay."'";
				}
				$i++;
			}
			$tmpKdPay=$k_kd_pay;
		}
		$tmpKdPay= str_replace("'","",$tmpKdPay); //menghilangkan kutip
		
		//echo $tmpKdPay;
		#KRITERIA KAMAR
		$arrayDataKamar = $param->kd_kamar;
		
		$tmpKdKamar='';
		for ($i=0; $i < count($arrayDataKamar); $i++) { 
			$tmpKdKamar .= "'".$arrayDataKamar[$i][0]."',";
		}
		$tmpKdKamar = substr($tmpKdKamar, 0, -1); //arr kd pay
		
		$semua_kamar="'"."999"."'";
		if($tmpKdKamar==$semua_kamar ){ //semua kd_pay
			$get_kd_kamar= $this->db->query("SELECT kmr.no_kamar, Nama_Kamar 
												FROM Kamar kmr 
													INNER JOIN spc_Kamar sk ON sk.no_kamar=kmr.no_kamar AND sk.kd_Unit=kmr.kd_Unit")->result();
			$k_kd_kamar ='';
			$i=1;
			foreach($get_kd_kamar as $line){
				if($i==1){
					$k_kd_kamar="'".$line->no_kamar."'";
				}else{
					$k_kd_kamar=$k_kd_kamar.","."'".$line->no_kamar."'";
				}
				$i++;
			}
			$tmpKdKamar=$k_kd_kamar;
		}
		
		$tmpKdKamar= str_replace("'","",$tmpKdKamar); //menghilangkan kutip
		
		#KRITERIA DOKTER
		$dokter = $param->dokter; //tanpa kutip
		$tmpKdDokter='';
		if($dokter == 'Semua' || $dokter == '999'){
			$get_kd_dokter= $this->db->query("SELECT kd_dokter,nama FROM Dokter WHERE Kd_Dokter IN (SELECT Kd_Dokter FROM Dokter_Inap WHERE Kd_Unit in ('1001','1002','1003')) ")->result();
			$k_kd_dokter ='';
			$i=1;
			foreach($get_kd_dokter as $line){
				if($i==1){
					$k_kd_dokter="'".$line->kd_dokter."'";
				}else{
					$k_kd_dokter=$k_kd_dokter.","."'".$line->kd_dokter."'";
				}
				$i++;
			}
			$tmpKdDokter=$k_kd_dokter;
		}else{
			$tmpKdDokter = "'$dokter'";
		}
		$tmpKdDokter= str_replace("'","",$tmpKdDokter); //menghilangkan kutip
		
		#KRITERIA OPERATOR
		$operator = $param->operator; //tanpa kutip
		$tmpOperator ='';
		if($operator == 'Semua' || $operator == '9999'){
			$get_kd_user= $this->db->query( "SELECT DISTINCT zUSERS.KD_USER,zUSERS.USER_NAMES 
													From zUsers 
														inner join  zMember ON zmember.kd_user=zuserS.kd_user 
														inner join  zGroups on zmember.group_id=zgroups.group_id 
														inner join zTrustee on zgroups.group_id =ztrustee.group_id 
														inner join zModule On ztrustee.mod_id=zmodule.mod_id 
											")->result();
			$k_kd_user ='';
			$i=1;
			foreach($get_kd_user as $line){
				if($i==1){
					$k_kd_user="'".$line->kd_user."'";
				}else{
					$k_kd_user=$k_kd_user.","."'".$line->kd_user."'";
				}
				$i++;
			}
			$tmpOperator=$k_kd_user;
		}else{
			$tmpOperator="'$operator'";
		}
		$tmpOperator= str_replace("'","",$tmpOperator); //menghilangkan kutip
		
		
		#PEMANGGILAN FUNGSI LAPORAN
		
		$kriteria = "SELECT * from rwi_lap_jasa_pel_dokter_pertindakan_revisi 
									(
										'{".$kd_unit."}',
										'{".$tmpKdKamar."}',
										'{".$kd_kelpas."}',
										'{".$k_customer."}',
										'{".$k_kelas_produk."}',
										'{".$tmpKdPay."}',
										'{".$tmpOperator."}',
										'{".$tmpKdDokter."}',
										'{".$tgl_awal."}',
										'{".$tgl_akhir."}',
										{$pasien_pulang}
									) order by kd_unit asc";
		$query = $this->db->query($kriteria);

		if ($query->num_rows() > 0) {

			if($type_file == 1){
				$url      = "Doc Asset/format_laporan/".$nama_file.".xls";
				$baris    = 6;
				
				$phpExcel    = new PHPExcel();
				$sharedStyle = new PHPExcel_Style();
				$phpExcel    = PHPExcel_IOFactory::load($url);
				$sheet       = $phpExcel->getActiveSheet();
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d'));
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				
				foreach ($query->result() as $result_header) {
					$header[] = $result_header->dokter;
				}

				foreach ($query->result() as $result_header) {
					$list_unit[] = $result_header->kd_unit;
				}

				$tmp_header   = implode('#', array_unique($header));
				$tmp_unit     = implode('#', array_unique($list_unit));
				$array_header = explode("#", $tmp_header);
				$array_unit   = explode("#", $tmp_unit);
				for ($i=0; $i < count($array_header); $i++) {
					// echo $array_header[$i]."<br>";
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, ($i+1));
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $array_header[$i]);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':M'.$baris);
					$phpExcel->getActiveSheet()->mergeCells('B'.$baris.':M'.$baris);
					for ($j=0; $j < count($array_unit); $j++) {
						foreach ($query->result() as $result_body) {
							if ($result_body->dokter == $array_header[$i] && $result_body->kd_unit == $array_unit[$j]) {
								$baris++;
								$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result_body->kelas);
								$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':M'.$baris);
								$phpExcel->getActiveSheet()->mergeCells('B'.$baris.':M'.$baris);
								// echo $result_body->kelas."<br>";
								break;
							}
						}
						
						$no = 1;
						foreach ($query->result() as $result_body) {
							if ($result_body->dokter == $array_header[$i] && $result_body->kd_unit == $array_unit[$j]) {
								$baris++;
								$jumlah=$result_body->a1 + $result_body->a2 + $result_body->a3 + $result_body->a4 +$result_body->a5 + $result_body->a6;
								$diterima = $jumlah - (($jumlah*$result_body->pajak)/100);
								$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $no." - ".$result_body->deskripsi);
								$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result_body->kd_pasien);
								$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result_body->qty);
								$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result_body->a1);
								$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result_body->a2);
								$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result_body->a3);
								$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result_body->a4);
								$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $result_body->a5);
								$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $result_body->a6);
								$phpExcel->getActiveSheet()->setCellValue('K'.$baris, $jumlah);
								$phpExcel->getActiveSheet()->setCellValue('L'.$baris, $result_body->pajak);
								$phpExcel->getActiveSheet()->setCellValue('M'.$baris, $diterima);

								$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':M'.$baris);
								$no++;
							}
						}
					}
					// echo "<br>";
					$baris++;
				}
				// die();

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.$nama_file.'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}else{
				$baris = 1;
				$html = "";
				$html .= "<table width='100%' border='0' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
				$html .= "<td colspan='13' align='center'>Laporan Jasa Pelayanan Dokter Per Tindakan</td>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<td colspan='13' align='center'>Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d')."</td>";
				$html .= "</tr>";
				$html .= "</thead>";
				$html .= "</table>";
				$html .= "</br>";
				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
				$html .= "<th>No</th>";
				$html .= "<th>Tindakan</th>";
				$html .= "<th>Jumlah Pasien</th>";
				$html .= "<th>Jumlah Produk</th>";
				$html .= "<th>Dokter</th>";
				$html .= "<th>Perawat</th>";
				$html .= "<th>Operator</th>";
				$html .= "<th>Anasthesi</th>";
				$html .= "<th>Ass. Operator</th>";
				$html .= "<th>Ass. Anasthesi</th>";
				$html .= "<th>Jumlah</th>";
				$html .= "<th>Pajak</th>";
				$html .= "<th>Diterima</th>";
				$html .= "</tr>";
				$html .= "</thead>";
				$html .= "<tbody>";

				foreach ($query->result() as $result_header) {
					$header[] = $result_header->dokter;
				}

				foreach ($query->result() as $result_header) {
					$list_unit[] = $result_header->kd_unit;
				}

				$tmp_header   = implode('#', array_unique($header));
				$tmp_unit     = implode('#', array_unique($list_unit));
				$array_header = explode("#", $tmp_header);
				$array_unit   = explode("#", $tmp_unit);

				for ($i=0; $i < count($array_header); $i++) {
					// echo $array_header[$i]."<br>";
					$html .= "<tr>";
					$html .= "<td>".($i+1)."</td>";
					$html .= "<td colspan='12'>".$array_header[$i]."</td>";
					$html .= "</tr>";
					for ($j=0; $j < count($array_unit); $j++) {
						foreach ($query->result() as $result_body) {
							if ($result_body->dokter == $array_header[$i] && $result_body->kd_unit == $array_unit[$j]) {
								$baris++;
								$html .= "<tr>";
								$html .= "<td></td>";
								$html .= "<td colspan='12'>".$result_body->kelas."</td>";
								$html .= "</tr>";
								// echo $result_body->kelas."<br>";
								break;
							}
						}
						$no = 1;
						foreach ($query->result() as $result_body) {
							if ($result_body->dokter == $array_header[$i] && $result_body->kd_unit == $array_unit[$j]) {
								$baris++;
								$jumlah=$result_body->a1 + $result_body->a2 + $result_body->a3 + $result_body->a4 +$result_body->a5 + $result_body->a6;
								$diterima = $jumlah - (($jumlah*$result_body->pajak)/100);
								
								$html.="<tr>
									<td></td>
									<td>".$no." - ".$result_body->deskripsi."</td>
									<td align='right'>".$result_body->kd_pasien."</td>
									<td align='right'>".$result_body->qty."</td>
									<td align='right'>".number_format($result_body->a1,0,',',',')."</td>
									<td align='right'>".number_format($result_body->a2,0,',',',')."</td>
									<td align='right'>".number_format($result_body->a3,0,',',',')."</td>
									<td align='right'>".number_format($result_body->a4,0,',',',')."</td>
									<td align='right'>".number_format($result_body->a5,0,',',',')."</td>
									<td align='right'>".number_format($result_body->a6,0,',',',')."</td>
									<td align='right'>".number_format($jumlah,0,',',',')."</td>
									<td align='right'>".$result_body->pajak."</td>
									<td align='right'>".number_format($diterima,0,',',',')."</td>
								</tr>";
								$no++;
							}
						}
					}
					// echo "<br>";
					$baris++;
				}
				$html .= "</tbody>";
				$html .= "</table>";
				echo $html;
				// $this->common->setPdf('L',$nama_file,$html);
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}

	public function lap_index_pekerjaan(){
		$html         = "";
		$nama_file    = "Lap. Index Pekerjaan";
		
		$param        = json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA		
		$tgl_awal     = $param->tgl_awal;
		$tgl_akhir    = $param->tgl_akhir;
		$kd_bagian    = $param->bagian;
		$type_file    = $param->type_file;
		$kd_pekerjaan = $param->pekerjaan;
		$criteria 		= "";

		if (strtolower($kd_pekerjaan) != 'semua' && strlen($kd_pekerjaan) > 0) {
			$criteria = " where kd_pekerjaan = '".(int)$kd_pekerjaan."' ";
		}
		// echo $kd_pekerjaan;die();
		$query = $this->db->query("SELECT * FROM query_index_pekerjaan('".date_format(date_create($tgl_awal),'Y-m-d')."', '".date_format(date_create($tgl_akhir),'Y-m-d')."', '".$kd_bagian."') ".$criteria);

		if ($query->num_rows() > 0) {
			if ($type_file == '0') {
				$html .= "<table width='100%' cellspacing='0' border='0'>";
				$html .= "<tr>";
				$html .= "<th align='center'>Laporan Index Pekerjaan</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th align='center'>Periode : ".date_format(date_create($tgl_awal), 'd-M-Y')." s/d ".date_format(date_create($tgl_akhir), 'd-M-Y')."</th>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "<br>";
				
				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
					$html .= "<th rowspan='2'>No</th>";
					$html .= "<th rowspan='2'>Pekerjaan</th>";
					$html .= "<th colspan='2'>Pasien Baru</th>";
					$html .= "<th colspan='2'>Pasien Lama</th>";
					$html .= "<th colspan='2'>Jumlah</th>";
					$html .= "<th rowspan='2'>Total</th>";
				$html .= "</tr>";
				
				$html .= "<tr>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
				$html .= "</tr>";
				$html .= "</thead>";

				$html .= "<tbody>";
				$no = 1;
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				foreach ($query->result() as $result) {
					$html .= "<tr>";
					$html .= "<td>".$no."</td>";
					$html .= "<td>".$result->pekerjaan."</td>";
					$html .= "<td>".$result->lbaru."</td>";
					$html .= "<td>".$result->pbaru."</td>";
					$html .= "<td>".$result->llama."</td>";
					$html .= "<td>".$result->plama."</td>";
					$html .= "<td>".$result->jumlahl."</td>";
					$html .= "<td>".$result->jumlahp."</td>";
					$html .= "<td>".((int)$result->jumlahp + (int)$result->jumlahl)."</td>";
					$html .= "</tr>";

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;

					$no++;
				}
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td>Total</td>";
				$html .= "<td>".$total_lbaru."</td>";
				$html .= "<td>".$total_pbaru."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_jumlahl."</td>";
				$html .= "<td>".$total_jumlahp."</td>";
				$html .= "<td>".$total_seluruh."</td>";
				$html .= "</tr>";
				$html .= "</tbody>";
				$html .= "</table>";
				$this->common->setPdf('L',$nama_file,$html);
			}else{
				$url         = "Doc Asset/format_laporan/".$nama_file.".xls";
				$baris       = 7;
				$no          = 1;
				
				$phpExcel    = new PHPExcel();
				$sharedStyle = new PHPExcel_Style();
				$phpExcel    = PHPExcel_IOFactory::load($url);
				$sheet       = $phpExcel->getActiveSheet();
				// $phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d'));
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'd-M-Y')." s/d ".date_format(date_create($tgl_akhir), 'd-M-Y'));
				foreach ($query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->pekerjaan);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->lbaru);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->pbaru);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->llama);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result->plama);
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->jumlahl);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->jumlahp);
					$phpExcel->getActiveSheet()->setCellValue('I'.$baris, ((int)$result->jumlahp + (int)$result->jumlahl));
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;
					$total_seluruh += ((int)$result->jumlahp + (int)$result->jumlahl);

					$no++;
					$baris++;
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "");
				$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_lbaru);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_pbaru);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_llama);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_plama);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jumlahl);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jumlahp);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_seluruh);
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $nama_file).'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}	
	}

	public function lap_index_pendidikan(){
		$html         = "";
		$nama_file    = "Lap. Index Pendidikan";
		
		$param        = json_decode($_POST['data']);
		
		#KRITERIA UNIT IRNA		
		$tgl_awal     = $param->tgl_awal;
		$tgl_akhir    = $param->tgl_akhir;
		$kd_bagian    = $param->bagian;
		$type_file    = $param->type_file;
		$kd_pendidikan= $param->pendidikan;
		$criteria 		= "";

		if (strtolower($kd_pendidikan) != 'semua' && strlen($kd_pendidikan) > 0) {
			$criteria = " where kd_pendidikan = '".(int)$kd_pendidikan."' ";
		}
		// echo $kd_pekerjaan;die();
		$query = $this->db->query("SELECT * FROM query_index_pendidikan('".date_format(date_create($tgl_awal),'Y-m-d')."', '".date_format(date_create($tgl_akhir),'Y-m-d')."', '".$kd_bagian."') ".$criteria);

		if ($query->num_rows() > 0) {
			if ($type_file == '0') {
				$html .= "<table width='100%' cellspacing='0' border='0'>";
				$html .= "<tr>";
				$html .= "<th align='center'>Laporan Index Pekerjaan</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th align='center'>Periode : ".date_format(date_create($tgl_awal), 'd-M-Y')." s/d ".date_format(date_create($tgl_akhir), 'd-M-Y')."</th>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "<br>";
				
				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
				$html .= "<tr>";
					$html .= "<th rowspan='2'>No</th>";
					$html .= "<th rowspan='2'>Pendidikan</th>";
					$html .= "<th colspan='2'>Pasien Baru</th>";
					$html .= "<th colspan='2'>Pasien Lama</th>";
					$html .= "<th colspan='2'>Jumlah</th>";
					$html .= "<th rowspan='2'>Total</th>";
				$html .= "</tr>";
				
				$html .= "<tr>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
					$html .= "<th>L</th>";
					$html .= "<th>P</th>";
				$html .= "</tr>";
				$html .= "</thead>";

				$html .= "<tbody>";
				$no = 1;
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				foreach ($query->result() as $result) {
					$html .= "<tr>";
					$html .= "<td>".$no."</td>";
					$html .= "<td>".$result->pendidikan."</td>";
					$html .= "<td>".$result->lbaru."</td>";
					$html .= "<td>".$result->pbaru."</td>";
					$html .= "<td>".$result->llama."</td>";
					$html .= "<td>".$result->plama."</td>";
					$html .= "<td>".$result->jumlahl."</td>";
					$html .= "<td>".$result->jumlahp."</td>";
					$html .= "<td>".((int)$result->jumlahp + (int)$result->jumlahl)."</td>";
					$html .= "</tr>";

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;
					$total_seluruh += ((int)$result->jumlahp + (int)$result->jumlahl);

					$no++;
				}
				$html .= "<tr>";
				$html .= "<td></td>";
				$html .= "<td>Total</td>";
				$html .= "<td>".$total_lbaru."</td>";
				$html .= "<td>".$total_pbaru."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_llama."</td>";
				$html .= "<td>".$total_jumlahl."</td>";
				$html .= "<td>".$total_jumlahp."</td>";
				$html .= "<td>".$total_seluruh."</td>";
				$html .= "</tr>";
				$html .= "</tbody>";
				$html .= "</table>";
				$this->common->setPdf('L',$nama_file,$html);
			}else{
				$url         = "Doc Asset/format_laporan/".$nama_file.".xls";
				$baris       = 7;
				$no          = 1;
				
				$phpExcel    = new PHPExcel();
				$sharedStyle = new PHPExcel_Style();
				$phpExcel    = PHPExcel_IOFactory::load($url);
				$sheet       = $phpExcel->getActiveSheet();
				// $phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d'));
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);
				$total_lbaru   = 0;
				$total_pbaru   = 0;
				$total_llama   = 0;
				$total_plama   = 0;
				$total_jumlahl = 0;
				$total_jumlahp = 0;
				$total_seluruh = 0;
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'd-M-Y')." s/d ".date_format(date_create($tgl_akhir), 'd-M-Y'));
				foreach ($query->result() as $result) {
					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no);
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result->pendidikan);
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->lbaru);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->pbaru);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->llama);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result->plama);
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->jumlahl);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->jumlahp);
					$phpExcel->getActiveSheet()->setCellValue('I'.$baris, ((int)$result->jumlahp + (int)$result->jumlahl));
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

					$total_lbaru   += $result->lbaru;
					$total_pbaru   += $result->pbaru;
					$total_llama   += $result->llama;
					$total_plama   += $result->plama;
					$total_jumlahl += $result->jumlahl;
					$total_jumlahp += $result->jumlahp;
					$total_seluruh += ((int)$result->jumlahp + (int)$result->jumlahl);

					$no++;
					$baris++;
				}
				$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "");
				$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $total_lbaru);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $total_pbaru);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $total_llama);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $total_plama);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $total_jumlahl);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $total_jumlahp);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $total_seluruh);
				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':I'.$baris);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $nama_file).'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}	
	}


	public function laporan_transaksi_per_jenis_produk(){
		$param 	= json_decode($_POST['data']);
		$title 	= 'Laporan Transaksi per Jenis Produk (RWJ)';
		
		$params = array();
		$params['tgl_awal']  = $param->start_date;
		$params['tgl_akhir'] = $param->last_date;
		

		$html          ='';	
		
		/*Parameter Unit*/
		$tmpKdUnit     ="";
		$arrayDataUnit = $param->tmp_kd_unit;
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$tmpKdUnit .= $arrayDataUnit[$i][0].",";
		}
		$tmpKdUnit 	= substr($tmpKdUnit, 0, -1);

		/* Parameter Pembayaran*/
		$tmpKdPay="";
		$arrayDataPay = $param->tmp_payment;
		for ($i=0; $i < count($arrayDataPay); $i++) { 
			$tmpKdPay .= "'".$arrayDataPay[$i][0]."',";
		}
		$tmpKdPay = substr($tmpKdPay, 0, -1);
		
		$semua ="'00'"; //jika semua kd pay
		if($tmpKdPay == $semua){
			$kd_pay=$this->db->query("select kd_pay from payment ")->result();
			$tmpKdPay = '';
			foreach($kd_pay as $pay){
				$tmpKdPay.= "'".$pay->kd_pay."',";
			}
			$tmpKdPay = substr($tmpKdPay, 0, -1);
		}
		$kel_pas = $param->pasien;

			if($kel_pas == '-1' || $kel_pas < 0){
				$kriteria_kelpas='';
				$customerx='';
				$t_kelpas='Semua';
				$t_customer='Semua';
				$customer=$this->db->query("SELECT * From customer")->result();
				$tmpKdCustomer="";
				foreach ($customer as $line){
					$tmpKdCustomer .= "'".$line->kd_customer."',";
				}
				$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);
				// echo "masuks";
			}else{
				// echo "masuk";
				$kriteria_kelpas=" and Ktr.jenis_cust in ('".$kel_pas."') ";
				if($kel_pas == 0){
					$t_kelpas='Perseorangan';
				}else if($kel_pas == 1){
					$t_kelpas='Perusahaan';
				}else{
					$t_kelpas='Asuransi';
				}
				
				/*Parameter Customer*/
				$arrayDataCustomer = $param->tmp_kd_customer;
			
				$tmpKdCustomer="";
				for ($i=0; $i < count($arrayDataCustomer); $i++) { 
					$tmpKdCustomer .= "'".$arrayDataCustomer[$i][0]."',";
				}
				
				$tmpKdCustomer = substr($tmpKdCustomer, 0, -1);

				$semua="'0'";
				if($tmpKdCustomer == $semua){
					$t_customer='Semua';
					$customer=$this->db->query("SELECT * From kontraktor Where jenis_cust = '$kel_pas'")->result();
					$tmpKdCustomer='';
					foreach ($customer as $line){
						$tmpKdCustomer = $line->kd_customer.", ".$tmpKdCustomer;
					}
					$tmpKdCustomer = substr($tmpKdCustomer, 0, -2);
				}
			}

		$query = $this->db->query("
									SELECT * 
									FROM rwj_laporan_detail_per_produk('".$params['tgl_awal']."', '".$params['tgl_akhir']."', array[".$tmpKdCustomer."]
										, array[".$tmpKdUnit."], array[".$tmpKdPay."])
								");

		if ($query->num_rows() > 0) {
			if($param->type_file == false){ 
				foreach ($query->result() as $result_header) {
					$header[] = $result_header->nama_unit;
				}

				$tmp_header      = implode('#', array_unique($header));
				
				$html = "";
				$html .= "<table width='100%' border='' cellspacing='0'>";
				$html .= "<tr>";
				$html .= "<th align='center'>".$title."</th>";
				$html .= "</tr>";
				$html .= "<tr>";
				$html .= "<th align='center'>Periode : ".date_format(date_create($params['tgl_awal']),'Y-m-d')." s/d ".date_format(date_create($params['tgl_akhir']),'Y-m-d')."</th>";
				$html .= "</tr>";
				$html .= "</table>";
				$html .= "</hr>";
				$html .= "<table width='100%' border='1' cellspacing='0'>";
				$html .= "<thead>";
					$html .= "<tr>";
						$html .= "<th rowspan='2'>No</th>";
						$html .= "<th rowspan='2' width='200'>Data Pasien</th>";
						$html .= "<th rowspan='2'>Jumlah Bayar</th>";
						$html .= "<th colspan='7'>Detail Produk</th>";
					$html .= "</tr>";
					$html .= "<tr>";
						$html .= "<th>Pendaftaran</th>";
						$html .= "<th>Tindakan</th>";
						$html .= "<th>Lab PK</th>";
						$html .= "<th>Lab PA</th>";
						$html .= "<th>Radiologi</th>";
						$html .= "<th>Apotek</th>";
						$html .= "<th>Lain-lain</th>";
					$html .= "</tr>";
				$html .= "</thead>";

				$html .= "<tbody>";
				// echo count($tmp_header);die();
				$array_header      = explode("#", $tmp_header);

				$grand_jumlah_bayar = 0;
				$grand_jumlah_pendaftaran = 0;
				$grand_jumlah_tindakan = 0;
				$grand_jumlah_lab_pk = 0;
				$grand_jumlah_lab_pa = 0;
				$grand_jumlah_rad = 0;
				$grand_jumlah_apotek = 0;
				$grand_jumlah_lain_lain = 0;
				$no_header_poli = "A";
				for ($i=0; $i < count($array_header); $i++) { 

					$jumlah_poli_bayar = 0;
					$jumlah_poli_pendaftaran = 0;
					$jumlah_poli_tindakan = 0;
					$jumlah_poli_lab_pk = 0;
					$jumlah_poli_lab_pa = 0;
					$jumlah_poli_rad = 0;
					$jumlah_poli_apotek = 0;
					$jumlah_poli_lain_lain = 0;
					$html .= "<tr>";
						$html .= "<td  align='center'><b>".$no_header_poli."</b></td>";
						$html .= "<td colspan='9' style'padding-left:5px;'><b>".strtoupper($array_header[$i])."</b></td>";
					$html .= "</tr>";

					$query_header = $this->db->query("
														SELECT DISTINCT (kd_pasien, nama_unit), kd_pasien, nama_pasien, nama_unit, no_nota 
														FROM rwj_laporan_detail_per_produk('".$params['tgl_awal']."', '".$params['tgl_akhir']."', array[".$tmpKdCustomer."]
															, array[".$tmpKdUnit."], array[".$tmpKdPay."]) 
														WHERE nama_unit = '".$array_header[$i]."' 
														ORDER BY nama_pasien ASC
													");
					$no_header = 1;
					foreach ($query_header->result() as $result_head) {
						if ($array_header[$i] == $result_head->nama_unit) {


							$jumlah_bayar = 0;
							$jumlah_pendaftaran = 0;
							$jumlah_tindakan = 0;
							$jumlah_lab_pk = 0;
							$jumlah_lab_pa = 0;
							$jumlah_rad = 0;
							$jumlah_apotek = 0;
							$jumlah_lain_lain = 0;

							$html .= "<tr>";
								$html .= "<td align='right'><b>".$no_header.". </b></td>";
								$html .= "<td colspan='9' style'padding-left:5px;'><b>".$result_head->kd_pasien." || ".$result_head->nama_pasien." || No Nota: ".$result_head->no_nota."</b></td>";
							$html .= "</tr>";
							$no      = 1;
							$rowspan = 1;
							foreach ($query->result() as $result) {
								if ( ($result_head->nama_unit == $result->nama_unit) && ($result_head->kd_pasien == $result->kd_pasien) ) {
									$rowspan++;
								}
							}
							foreach ($query->result() as $result) {
								if ( ($result_head->nama_unit == $result->nama_unit) && ($result_head->kd_pasien == $result->kd_pasien) ) {
									$html .= "<tr>";
										//$html .= "<td style='padding-rigt:5px;' align='right'>".$no."</td>";
										$html .= "<td></td>";
										$html .= "<td style='padding-left:5px;'>".$no.". ".$result->deskripsi."</td>";
										// $html .= "<td style='padding-left:5px;'>".$result->total_bayar."</td>";

										if ($no == 1) {
											$html .= "<td style='padding-right:5px;'  align='right' rowspan='".$rowspan."' valign='middle'>".number_format($result->total_bayar,0,',',',')."</td>";
											$tmp_total = $result->total_bayar;
										}else{
											$tmp_total = 0;
										}

										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->pendaftaran,0,',',',')."</td>";
										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->tindakan,0,',',',')."</td>";
										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->lab_pk,0,',',',')."</td>";
										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->lab_pa,0,',',',')."</td>";
										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->rad,0,',',',')."</td>";
										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->apotek,0,',',',')."</td>";
										$html .= "<td style='padding-right:5px;' align='right'>".number_format($result->lain_lain,0,',',',')."</td>";
										$no++;
									$html .= "</tr>";

									$jumlah_bayar += $tmp_total;
									$jumlah_pendaftaran += $result->pendaftaran;
									$jumlah_tindakan += $result->tindakan;
									$jumlah_lab_pk += $result->lab_pk;
									$jumlah_lab_pa += $result->lab_pa;
									$jumlah_rad += $result->rad;
									$jumlah_apotek += $result->apotek;
									$jumlah_lain_lain += $result->lain_lain;
								}
							}
							$html .= "<tr>";
								$html .= "<td></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>Jumlah</b></td>";
								// $html .= "<td style='padding-right:5px;' align='right'><b>".$jumlah_bayar."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_pendaftaran,0,',',',')."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_tindakan,0,',',',')."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_lab_pk,0,',',',')."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_lab_pa,0,',',',')."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_rad,0,',',',')."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_apotek,0,',',',')."</b></td>";
								$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_lain_lain,0,',',',')."</b></td>";
								$no++;
							$html .= "</tr>";

							$jumlah_poli_bayar 	+= $jumlah_bayar;
							$jumlah_poli_pendaftaran 	+= $jumlah_pendaftaran;
							$jumlah_poli_tindakan 	+= $jumlah_tindakan;
							$jumlah_poli_lab_pk 	+= $jumlah_lab_pk;
							$jumlah_poli_lab_pa 	+= $jumlah_lab_pa;
							$jumlah_poli_rad 	+= $jumlah_rad;
							$jumlah_poli_apotek 	+= $jumlah_apotek;
							$jumlah_poli_lain_lain 	+= $jumlah_lain_lain;
						}
						$no_header++;
					}

					$html .= "<tr>";
						$html .= "<td></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>Sub Total Poli ".strtoupper($array_header[$i])."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_bayar,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_pendaftaran,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_tindakan,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_lab_pk,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_lab_pa,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_rad,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_apotek,0,',',',')."</b></td>";
						$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($jumlah_poli_lain_lain,0,',',',')."</b></td>";
						$no++;
					$html .= "</tr>";


					$grand_jumlah_bayar += $jumlah_poli_bayar;
					$grand_jumlah_pendaftaran += $jumlah_poli_pendaftaran;
					$grand_jumlah_tindakan += $jumlah_poli_tindakan;
					$grand_jumlah_lab_pk += $jumlah_poli_lab_pk;
					$grand_jumlah_lab_pa += $jumlah_poli_lab_pa;
					$grand_jumlah_rad += $jumlah_poli_rad;
					$grand_jumlah_apotek += $jumlah_poli_apotek;
					$grand_jumlah_lain_lain += $jumlah_poli_lain_lain;
					$html .= "<tr>";
						$html .= "<td colspan='10'></td>";
					$html .= "</tr>";
					$no_header_poli++;
				}

				$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>Grand Total Poli</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_bayar,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_pendaftaran,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_tindakan,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_lab_pk,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_lab_pa,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_rad,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_apotek,0,',',',')."</b></td>";
					$html .= "<td style='padding-right:5px;' align='right'><b>".number_format($grand_jumlah_lain_lain,0,',',',')."</b></td>";
					$no++;
				$html .= "</tr>";

				$html .= "</tbody>";
				$html .= "</table>";
				// echo $html;
				$this->common->setPdf('P',$title,$html);
			}else{
				foreach ($query->result() as $result_header) {
					$header[] = $result_header->nama_unit;
				}

				$tmp_header      = implode('#', array_unique($header));
				$url         = "Doc Asset/format_laporan/".$title.".xls";
				$baris       = 8;
				$no          = 1;
				
				$phpExcel    = new PHPExcel();
				$sharedStyle = new PHPExcel_Style();
				$phpExcel    = PHPExcel_IOFactory::load($url);
				$sheet       = $phpExcel->getActiveSheet();
				// $phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($tgl_awal), 'Y-m-d')." s/d ".date_format(date_create($tgl_awal), 'Y-m-d'));
				$sharedStyle->applyFromArray(
					 array(
					 	'borders' => array(
							 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
					 	),
				 	)
				);

				$styleTextBold = array(
					'font' => array(
						'bold' => true
					)
				);
				$phpExcel->getActiveSheet()->setCellValue('A2', "Periode : ".date_format(date_create($params['tgl_awal']),'Y-m-d')." s/d ".date_format(date_create($params['tgl_akhir']),'Y-m-d'));

				$array_header      = explode("#", $tmp_header);

				$numberormat='#,##0';//'General','0.00','@';
				$grand_jumlah_bayar = 0;
				$grand_jumlah_pendaftaran = 0;
				$grand_jumlah_tindakan = 0;
				$grand_jumlah_lab_pk = 0;
				$grand_jumlah_lab_pa = 0;
				$grand_jumlah_rad = 0;
				$grand_jumlah_apotek = 0;
				$grand_jumlah_lain_lain = 0;
				$no_header_poli = "A";
				for ($i=0; $i < count($array_header); $i++) { 
					$jumlah_poli_bayar = 0;
					$jumlah_poli_pendaftaran = 0;
					$jumlah_poli_tindakan = 0;
					$jumlah_poli_lab_pk = 0;
					$jumlah_poli_lab_pa = 0;
					$jumlah_poli_rad = 0;
					$jumlah_poli_apotek = 0;
					$jumlah_poli_lain_lain = 0;

					$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no_header_poli );
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, strtoupper($array_header[$i]));

					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':J'.$baris);
					$phpExcel->getActiveSheet()->getStyle("A".$baris.":J".$baris)->getFont()->setBold(true);
					$baris++;

					$query_header = $this->db->query("
														SELECT DISTINCT (kd_pasien, nama_unit), kd_pasien, nama_pasien, nama_unit, no_nota 
														FROM rwj_laporan_detail_per_produk('".$params['tgl_awal']."', '".$params['tgl_akhir']."', array[".$tmpKdCustomer."]
															, array[".$tmpKdUnit."], array[".$tmpKdPay."]) 
														WHERE nama_unit = '".$array_header[$i]."' 
														ORDER BY nama_pasien ASC
													");
					$no_header = 1;
					foreach ($query_header->result() as $result_head) {

						if ($array_header[$i] == $result_head->nama_unit) {

							$jumlah_bayar = 0;
							$jumlah_pendaftaran = 0;
							$jumlah_tindakan = 0;
							$jumlah_lab_pk = 0;
							$jumlah_lab_pa = 0;
							$jumlah_rad = 0;
							$jumlah_apotek = 0;
							$jumlah_lain_lain = 0;

							$phpExcel->getActiveSheet()->setCellValue('A'.$baris, $no_header);
							$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $result_head->kd_pasien." || ".$result_head->nama_pasien." || No Nota: ".$result_head->no_nota);
							$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':J'.$baris);
							$phpExcel->getActiveSheet()->getStyle("A".$baris.":J".$baris)->getFont()->setBold(true);
							$baris++;
							$rowspan = $baris;
							$no = 1;
							foreach ($query->result() as $result) {
								if ( ($result_head->nama_unit == $result->nama_unit) && ($result_head->kd_pasien == $result->kd_pasien) ) {
									$phpExcel->getActiveSheet()->setCellValue('A'.$baris, "");
									$phpExcel->getActiveSheet()->setCellValue('B'.$baris, $no.". ".$result->deskripsi);
									if ($no == 1) {
										$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $result->total_bayar);
										$phpExcel->getActiveSheet()
													->getStyle('C'.$baris)
													->getAlignment()
													->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
										$tmp_total = $result->total_bayar;
									}else{
										$tmp_total = 0;
									}
									$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $result->pendaftaran);
									$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $result->tindakan);
									$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $result->lab_pk);
									$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $result->lab_pa);
									$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $result->rad);
									$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $result->apotek);
									$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $result->lain_lain);
									
									$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':J'.$baris);
									$phpExcel->getActiveSheet()
												->getStyle('C'.$baris.':J'.$baris)
												->getNumberFormat()
												->setFormatCode($numberormat);
									// $phpExcel->getStyle('C'.$baris)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
									// $rowspan++;	

									$jumlah_bayar += $tmp_total;
									$jumlah_pendaftaran += $result->pendaftaran;
									$jumlah_tindakan += $result->tindakan;
									$jumlah_lab_pk += $result->lab_pk;
									$jumlah_lab_pa += $result->lab_pa;
									$jumlah_rad += $result->rad;
									$jumlah_apotek += $result->apotek;
									$jumlah_lain_lain += $result->lain_lain;
									$no++;	
									$baris++;	
								}
							}

							$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Jumlah Biaya");
							$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $jumlah_bayar);
							$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $jumlah_pendaftaran);
							$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $jumlah_tindakan);
							$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $jumlah_lab_pk);
							$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $jumlah_lab_pa);
							$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $jumlah_rad);
							$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $jumlah_apotek);
							$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $jumlah_lain_lain);
							$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':J'.$baris);
							$phpExcel->getActiveSheet()
										->getStyle('C'.$baris.':J'.$baris)
										->getNumberFormat()
									    ->setFormatCode($numberormat);
							$phpExcel->getActiveSheet()
													->getStyle('C'.$baris)
													->getAlignment()
													->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);

							$phpExcel->getActiveSheet()->mergeCells('C'.$rowspan.':C'.((int)$baris));
							// $phpExcel->getStyle('C'.$baris)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
							$phpExcel->getActiveSheet()->getStyle("A".$baris.":J".$baris)->getFont()->setBold(true);
							$jumlah_poli_bayar 	+= $jumlah_bayar;
							$jumlah_poli_pendaftaran 	+= $jumlah_pendaftaran;
							$jumlah_poli_tindakan 	+= $jumlah_tindakan;
							$jumlah_poli_lab_pk 	+= $jumlah_lab_pk;
							$jumlah_poli_lab_pa 	+= $jumlah_lab_pa;
							$jumlah_poli_rad 	+= $jumlah_rad;
							$jumlah_poli_apotek 	+= $jumlah_apotek;
							$jumlah_poli_lain_lain 	+= $jumlah_lain_lain;
							$baris++;	
						}
						$no_header++;
					}
					$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Sub Total Poli ".strtoupper($array_header[$i]));
					$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $jumlah_poli_bayar);
					$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $jumlah_poli_pendaftaran);
					$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $jumlah_poli_tindakan);
					$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $jumlah_poli_lab_pk);
					$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $jumlah_poli_lab_pa);
					$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $jumlah_poli_rad);
					$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $jumlah_poli_apotek);
					$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $jumlah_poli_lain_lain);
					$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':J'.$baris);
					$phpExcel->getActiveSheet()
								->getStyle('C'.$baris.':J'.$baris)
								->getNumberFormat()
							    ->setFormatCode($numberormat);
					$phpExcel->getActiveSheet()->getStyle("A".$baris.":J".$baris)->getFont()->setBold(true);
					$phpExcel->getActiveSheet()
								->getStyle('C'.$baris)
								->getAlignment()
								->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					$baris+=2;	

					$grand_jumlah_bayar += $jumlah_poli_bayar;
					$grand_jumlah_pendaftaran += $jumlah_poli_pendaftaran;
					$grand_jumlah_tindakan += $jumlah_poli_tindakan;
					$grand_jumlah_lab_pk += $jumlah_poli_lab_pk;
					$grand_jumlah_lab_pa += $jumlah_poli_lab_pa;
					$grand_jumlah_rad += $jumlah_poli_rad;
					$grand_jumlah_apotek += $jumlah_poli_apotek;
					$grand_jumlah_lain_lain += $jumlah_poli_lain_lain;
					$no_header_poli++;
				}
				$baris--;	
				$phpExcel->getActiveSheet()->setCellValue('B'.$baris, "Grand Total");
				$phpExcel->getActiveSheet()->setCellValue('C'.$baris, $grand_jumlah_bayar);
				$phpExcel->getActiveSheet()->setCellValue('D'.$baris, $grand_jumlah_pendaftaran);
				$phpExcel->getActiveSheet()->setCellValue('E'.$baris, $grand_jumlah_tindakan);
				$phpExcel->getActiveSheet()->setCellValue('F'.$baris, $grand_jumlah_lab_pk);
				$phpExcel->getActiveSheet()->setCellValue('G'.$baris, $grand_jumlah_lab_pa);
				$phpExcel->getActiveSheet()->setCellValue('H'.$baris, $grand_jumlah_rad);
				$phpExcel->getActiveSheet()->setCellValue('I'.$baris, $grand_jumlah_apotek);
				$phpExcel->getActiveSheet()->setCellValue('J'.$baris, $grand_jumlah_lain_lain);

				$phpExcel->getActiveSheet()->setSharedStyle($sharedStyle, 'A'.$baris.':J'.$baris);
				$phpExcel->getActiveSheet()
							->getStyle('C'.$baris.':J'.$baris)
							->getNumberFormat()
						    ->setFormatCode($numberormat);
				$phpExcel->getActiveSheet()->getStyle("A".$baris.":J".$baris)->getFont()->setBold(true);


				$phpExcel->getActiveSheet()
							->getStyle('A8:J'.$baris)
							->getAlignment()
							->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

				header('Content-Type: text/html; charset=ISO-8859-1');
				header('Content-Disposition: attachment;filename='.str_replace(" ", "_", $title).'.xls'); 
				header('Cache-Control: max-age=0');
				
				$writer = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5');
				$writer->save('php://output');
			}
		}else{
			$this->keterangan_tidak_ada_data();
		}
	}

	private function keterangan_tidak_ada_data(){
		echo "<h3>Tidak Ada Data</h3>";
	} 
}
?>