<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWJPasienPerStatus extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak(){
		$common=$this->common;
		$result=$this->result;
		$title='LAPORAN DAFTAR PASIEN PER STATUS PULANG';
		$param=json_decode($_POST['data']);
		
		$kd_poli   = $param->kd_poli;
		$tglAwal   = $param->tglAwal;
		$tglAkhir  = $param->tglAkhir;
		$id_status = $param->kd_status;
		$order_by  = $param->order_by;
		
        if(strtolower($order_by) == strtolower("Medrec")  || $order_by == 0){
            $criteriaOrder = "ORDER BY p.kd_pasien ASC";
        }else if(strtolower($order_by) == strtolower("Nama Pasien")  || $order_by == 1){
            $criteriaOrder = "ORDER BY p.nama ASC";
        }else if(strtolower($order_by) == strtolower("Penjamin")  || $order_by == 3){
            $criteriaOrder = "ORDER BY c.customer ASC";
        }else{
            $criteriaOrder = "ORDER BY k.tgl_masuk ASC";
        }

		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if(strtoupper($id_status) == 'SEMUA' || $id_status == ''){
			$ckd_status_far="";
			$statusfar='SEMUA STATUS PASIEN';
		} else{
			$ckd_status_far=" AND mrs.id_status='".$id_status."'";
			$statusfar=""; 
			$statusfar.="STATUS " ; 
			$statusfar.=strtoupper($this->db->query("SELECT id_status,status from mr_status_rwirujukan where id_status='".$id_status."'")->row()->status);
		}
		
		if(strtoupper($kd_poli) == 'SEMUA' || $kd_poli == ''){
			$ckd_unit_far=" AND LEFT (u.kd_unit, 1)='2'";
			$unitfar='SEMUA POLIKLINIK';
		} else{
			$unitfar = "";
			$ckd_unit_far=" AND u.kd_unit = '".$kd_poli."'";
			$unitfar.="POLIKLINIK "; 
			$unitfar.=strtoupper($this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit='".$kd_poli."'")->row()->nama_unit); 
		}
		
		$queryHead = $this->db->query(
			"SELECT 
			DISTINCT(mrs.id_status),
			mrs.status  
			from kunjungan k  
				inner join unit u on u.kd_unit=k.kd_unit 
				inner join mr_status_rwirujukan mrs on mrs.id_status=k.status_periksa 
			WHERE (k.tgl_masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."') ".$ckd_status_far." ".$ckd_unit_far.""
		);
		

		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th> Laporan Pasien dari '.$statusfar.' dan '.$unitfar.'</th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="35%" align="center">Nama Pasien</th>
					<th width="20%" align="center">Tanggal Masuk</th>
					<th width="20%" align="center">Kelompok</th>
					<th width="20%" align="center">Unit</th>
					<th width="20%" align="center">Diagnosa</th>
				  </tr>
			</thead>';
			//echo count($query);
		if(count($query) > 0) {

			$no=0;
			foreach($query as $line){
				$no++;
				$html.='<tbody>
								<tr>
									<th align="left" colspan="6" style="background-color: #fff700; padding-left:5px;">'.$line->status.'</th>
								  </tr>';
				$queryBody = $this->db->query( 
					"SELECT 
						DISTINCT(p.kd_pasien),
						p.nama as nama_pasien, mrs.id_status, mrs.status, c.customer   
						from kunjungan k 
							inner join pasien p on p.kd_pasien=k.kd_pasien 
							inner join customer c on c.kd_customer=k.kd_customer 
							inner join dokter d on d.kd_dokter=k.kd_dokter
							inner join unit u on u.kd_unit=k.kd_unit 
							inner join mr_status_rwirujukan mrs on mrs.id_status=k.status_periksa 
						where 
						mrs.id_status='".$line->id_status."' 
						AND (k.tgl_masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."')
						".$ckd_unit_far." 
						".$criteriaOrder.""
				);
										
				$query2 = $queryBody->result();
				
				$noo=0;
				foreach ($query2 as $line2) 
				{
					$noo++;
					
					$html.="<tr>";
					$html.="<td valign='top' align='center'>".$noo."</td>";
					$html.="<td width='' style='padding-left:10px;' valign='top'>(".$line2->kd_pasien.") ".$line2->nama_pasien."</td>";
					$html.="<td colspan='4' style='padding:-1px;'>";
						$queryBodyDetail = $this->db->query( 
							"SELECT 
								k.kd_pasien,
								k.kd_unit,
								k.kd_customer,
								k.tgl_masuk,
								k.urut_masuk,
								k.kd_customer,

								p.nama as nama_pasien,
								p.kd_pasien,

								pn.penyakit, 
								pn.kd_penyakit, 

								c.kd_customer,
								c.customer,

								d.kd_dokter,
								d.nama as nama_dokter,

								u.kd_unit,
								u.nama_unit,

								mrp.kd_pasien,
								mrp.kd_unit,
								mrp.urut_masuk,
								mrp.kd_penyakit, 
								mrs.id_status,
								mrs.status  
								from kunjungan k 
									left join mr_penyakit mrp on (mrp.kd_pasien=k.kd_pasien and mrp.kd_unit=k.kd_unit and mrp.tgl_masuk=k.tgl_masuk and mrp.urut_masuk=k.urut_masuk) 
									inner join pasien p on p.kd_pasien=k.kd_pasien 
									left join penyakit pn on pn.kd_penyakit=mrp.kd_penyakit 
									inner join customer c on c.kd_customer=k.kd_customer  
									inner join dokter d on d.kd_dokter=k.kd_dokter 
									inner join unit u on u.kd_unit=k.kd_unit  
									inner join mr_status_rwirujukan mrs on mrs.id_status=k.status_periksa 
								where p.kd_pasien='".$line2->kd_pasien."' AND (k.tgl_masuk BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								AND mrs.id_status='".$line->id_status."'
								".$ckd_unit_far." "
						);

						$query3 = $queryBodyDetail->result();
						if(count($query3) > 0) {	
							$html.='<table width="100%" border="1" cellspacing="0">';
							foreach ($query3 as $line3) {
							$html.="<tr>";
							$html.="<tr><td width='100%' style='padding-left:10px;' valign='top'>".tanggalstring(date('Y-m-d',strtotime($line3->tgl_masuk)))."</td>";
							$html.="<td width='100%' style='padding-left:10px;' valign='top'>".$line3->customer."</td>";
							$html.="<td width='100%' style='padding-left:10px;' valign='top'>".$line3->nama_unit."</td>";
							$html.='<td width="100%" align="left" style="padding-left:10px;" valign="top">'.$line3->penyakit.'</td>';
							$html.="</tr>";
							}
							$html.='</table>';
						}
					$html.="</td></tr>";
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="6" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('L','Lap. Pasien Per Status',$html);	
   	}
	
}
?>