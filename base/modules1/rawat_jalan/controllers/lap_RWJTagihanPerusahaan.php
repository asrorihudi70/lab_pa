<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWJTagihanPerusahaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, ',', '.');
	}
	
		public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
   		$title='LAPORAN DAFTAR TAGIHAN PERUSAHAAN';
		$param=json_decode($_POST['data']);
		
		$status    				=$param->status;
		$sort   				=$param->sort;
		$tglAwal   				=$param->tglAwal;
		$tglAkhir  				=$param->tglAkhir;
		$kd_customer  			=$param->kd_kelompok;
		$jenis_cust  			=$param->jenis_cust;
		
		$awal 	= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 	= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer_far="";
			$customerfar='SEMUA KELOMPOK CUSTOMER';
		} else{
			$ckd_customer_far=" AND c.kd_customer='".$kd_customer."'";
			$customerfar=""; 
			$customerfar.="KELOMPOK CUSTOMER " ; 
			$customerfar.=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}

		$ckd_sort_pasien 	= "";
		$ckd_sort_tanggal 	= "";
		$ckd_sort_status 	= "";
		if ($sort == 1) {
			$ckd_sort_pasien = " Order by ps.Kd_pasien ASC";
		}else if($sort == 2){
			$ckd_sort_pasien = " Order by ps.Nama ASC";
		}else{
			$ckd_sort_tanggal= "Order by dt.Tgl_Transaksi ASC";	
		}

		if ($status == 1) {
			$ckd_sort_status = " AND (db.jumlah IS NULL OR NULLIF(db.jumlah,0)<>0 OR NULLIF(db.jumlah,0)=0)";
			$status_pembayaran = "Semua Status Pembayaran";
		}else if($status == 2){
			$ckd_sort_status = " AND (NULLIF(db.jumlah,0)<>0)";
			$status_pembayaran = "Status Pembayaran Lunas";
		}else if($status == 3){
			$ckd_sort_status = " AND (NULLIF(db.jumlah,0)=0 OR db.jumlah IS NULL)";	
			$status_pembayaran = "Status Pembayaran Belum Lunas";
		}else{
			$ckd_sort_status = " AND (db.jumlah IS NULL OR NULLIF(db.jumlah,0)<>0 OR NULLIF(db.jumlah,0)=0)";
			$status_pembayaran = "Semua Status Pembayaran";
		}

		if ($jenis_cust == 1) {
			$ckd_jenis_cust = "(knt.jenis_Cust=1 OR knt.jenis_Cust=2)";
		}else if($jenis_cust == 2){
			$ckd_jenis_cust = "knt.jenis_Cust=1";
		}else if($jenis_cust == 3){
			$ckd_jenis_cust = "knt.jenis_Cust=2";
		}else{
			$ckd_jenis_cust = "(knt.jenis_Cust=1 OR knt.jenis_Cust=2)";	
		}

		$queryHead = $this->db->query("			
			Select 
				DISTINCT(c.kd_customer), 
				c.Customer 
			From Detail_Transaksi dt 
				INNER JOIN Transaksi t 
					On t.kd_Kasir=dt.kd_kasir 
					And t.No_transaksi=dt.No_transaksi  
				INNER JOIN Kunjungan k 
					ON t.Kd_Pasien=k.Kd_Pasien 
					And t.Kd_Unit=k.Kd_Unit   
					And t.Tgl_Transaksi=k.Tgl_masuk 
					And t.Urut_Masuk=k.Urut_Masuk  
				INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
				INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer  
				INNER JOIN Detail_Bayar db  
					On t.kd_Kasir=db.kd_kasir 
					And t.No_transaksi=db.No_transaksi 
			Where 
			".$ckd_jenis_cust." 
			".$ckd_customer_far."
			And (
					(dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (1,2,3)) 
					Or  
					(dt.Tgl_Transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And dt.Shift=4)
				) And Left(t.kd_Unit,1) = '2'
		");
		

		$query = $queryHead->result();			
		//-------------JUDUL-----------------------------------------------
		$html='
			<table  cellspacing="0" border="0">
				<tbody>
					<tr>
						<th>'.$title.'<br>
					</tr>
					<tr>
						<th> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th> Laporan Tagihan Perusahaan '.$status_pembayaran.' </th>
					</tr>
				</tbody>
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100%" height="20" border = "1">
			<thead>
				 <tr>
					<th width="5%" align="center">No</th>
					<th width="25%" align="center">Perusahaan/ Pasien</th>
					<th width="15%" align="center">Tanggal</th>
					<th width="15%" align="center">Pemeriksaan</th>
					<th width="5%" align="center">Qty</th>
					<th width="15%" align="center">Harga</th>
					<th width="15%" align="center">Jumlah</th>
				  </tr>
			</thead>';
			//echo count($query);
			$html.="<tbody>";
		if(count($query) > 0) {
			$all_total_qty_semua = 0;
			$all_total_harga_semua = 0;
			$all_total_jumlah_semua = 0;
			$no = 0;
			foreach($query as $line){
				$no++;
				$html.='<tr>
							<td style="padding-left:10px;">'.$no.'</td>
							<td align="left" colspan="6" style="padding-left:10px;"><b>'.$line->customer.'</b></td>
						</tr>';
				$all_total_qty = 0;
				$all_total_harga = 0;
				$all_total_jumlah = 0;
				$queryBody = $this->db->query( 
					"Select 
						DISTINCT(ps.Kd_pasien), 
						ps.Kd_pasien as kd_pasien, 
						ps.Nama as nama_pasien 
					From Detail_Transaksi dt 
						INNER JOIN Transaksi t 
							On t.kd_Kasir=dt.kd_kasir 
							And t.No_transaksi=dt.No_transaksi  
						INNER JOIN Kunjungan k 
							ON t.Kd_Pasien=k.Kd_Pasien 
							And t.Kd_Unit=k.Kd_Unit   
							And t.Tgl_Transaksi=k.Tgl_masuk 
							And t.Urut_Masuk=k.Urut_Masuk  
						INNER JOIN Pasien Ps 
							On ps.Kd_pasien=k.Kd_pasien  
						INNER JOIN Produk p 
							On P.Kd_produk=dt.Kd_produk  
						INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
						INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer  
						LEFT JOIN Detail_Bayar db  
							On t.kd_Kasir=db.kd_kasir 
							And t.No_transaksi=db.No_transaksi 
					Where 
					".$ckd_jenis_cust." 
					And c.kd_Customer='".$line->kd_customer."' 
					".$ckd_sort_status." 
					And (
						(dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (1,2,3)) 
						Or  
						(dt.Tgl_Transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And dt.Shift=4)
					) And Left(t.kd_Unit,1) = '2' 
					".$ckd_sort_pasien.""
				);
										
				$query2 = $queryBody->result();
				
				//$noo = 0;
				foreach ($query2 as $line2) 
				{
				$html.="<tr>";
				$html.="<td></td>";
					//$noo++;
					$html.="<td colspan='6' style='padding-left:10px;'>(".$line2->kd_pasien.") ".$line2->nama_pasien."</td>";
				$html.="</tr>";
					$queryBodyDetail = $this->db->query( 
						"Select 
						COUNT(ps.Kd_pasien) as count_data,
						ps.Kd_pasien as kd_pasien, 
						ps.Nama as nama_pasien, 
						dt.Tgl_Transaksi as tgl_transaksi, 
						p.Deskripsi as deskripsi,
						dt.qty,
						COUNT(dt.qty) as quantity, 
						dt.harga as harga  
						From Detail_Transaksi dt 
							INNER JOIN Transaksi t 
								On t.kd_Kasir=dt.kd_kasir 
								And t.No_transaksi=dt.No_transaksi  
							INNER JOIN Kunjungan k 
								ON t.Kd_Pasien=k.Kd_Pasien 
								And t.Kd_Unit=k.Kd_Unit   
								And t.Tgl_Transaksi=k.Tgl_masuk 
								And t.Urut_Masuk=k.Urut_Masuk  
							INNER JOIN Pasien Ps 
								On ps.Kd_pasien=k.Kd_pasien  
							INNER JOIN Produk p 
								On P.Kd_produk=dt.Kd_produk  
							INNER JOIN Customer c On c.Kd_Customer=k.Kd_Customer  
							INNER JOIN Kontraktor knt On c.Kd_Customer=knt.Kd_Customer  
							LEFT JOIN Detail_Bayar db  
								On t.kd_Kasir=db.kd_kasir 
								And t.No_transaksi=db.No_transaksi 
						Where 
						".$ckd_jenis_cust." 
						And ps.Kd_pasien='".$line2->kd_pasien."'   
						".$ckd_sort_status." 
						And (
							(dt.Tgl_Transaksi between '".$tglAwal."' And  '".$tglAkhir."'  And dt.Shift In (1,2,3)) 
							Or  
							(dt.Tgl_Transaksi between '".$tglAwal."'  And '".$tglAkhir."'  And dt.Shift=4)
						) And Left(t.kd_Unit,1) = '2'
						
						GROUP BY c.Customer, ps.Kd_pasien, ps.Nama, dt.Tgl_Transaksi, p.Deskripsi, dt.qty, dt.harga, dt.urut
						".$ckd_sort_tanggal.""
					);
					$query3 = $queryBodyDetail->result();
									
					$total_qty 		= 0;
					$total_harga 	= 0;
					$total_jumlah 	= 0;
					foreach ($query3 as $line3) 
					{
						$total_qty  	+= $line3->quantity;
						$total_harga 	+= $line3->harga;
						$total_jumlah 	+= ($line3->harga*$line3->quantity);
						$html.="<tr>";
						$html.="<td></td>";
						$html.="<td></td>";
						$html.="<td style='padding-left:10px;'>".tanggalstring(date('Y-m-d',strtotime($line3->tgl_transaksi)))."</td>";
						$html.="<td style='padding-left:10px;'>".$line3->deskripsi."</td>";
						$html.="<td style='padding-left:10px;'>".$line3->quantity."</td>";
						$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line3->harga)."</td>";
						$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line3->harga*$line3->quantity)."</td>";
						$html.="</tr>";
					}

					$all_total_qty += $total_qty;
					$all_total_harga += $total_harga;
					$all_total_jumlah += $total_jumlah;
					$html.="<tr>";
					$html.="<td colspan='4' align='right' style='padding-right:10px;'><b>Sub Total</b></td>";
					$html.="<td style='padding-left:10px;'><b>".$total_qty."</b></td>";
					$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah(ceil($total_harga))."</b></td>";
					$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah(ceil($total_jumlah))."</b></td>";
					$html.="</tr>";
				}
				$html.="<tr>";
				$html.="<td colspan='4' align='right' style='padding-right:10px;'><b>Total Tagihan</b></td>";
				$html.="<td style='padding-left:10px;'><b>".$all_total_qty."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah(ceil($all_total_harga))."</b></td>";
				$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah(ceil($all_total_jumlah))."</b></td>";
				$html.="</tr>";

				$all_total_qty_semua += $all_total_qty;
				$all_total_harga_semua += $all_total_harga;
				$all_total_jumlah_semua += $all_total_jumlah;
			}
			$html.="<tr>";
			$html.="<td colspan='4' align='right' style='padding-right:10px;'><b>Grand Total</b></td>";
			$html.="<td style='padding-left:10px;'><b>".$all_total_qty_semua."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah(ceil($all_total_harga_semua))."</b></td>";
			$html.="<td style='padding-right:10px;' align='right'><b>".$this->rupiah(ceil($all_total_jumlah_semua))."</b></td>";
			$html.="</tr>";
			
		}else {		
			$html.='
				<tr class="headerrow"> 
					<th width="" colspan="7" align="center">Data tidak ada</th>
				</tr>

			';		
		} 
		$html.='</tbody></table>';
		$prop=array('foot'=>true);
		$this->common->setPdf('P','Lap. Tagihan Perusahaan',$html);	
   	}
	
}
?>