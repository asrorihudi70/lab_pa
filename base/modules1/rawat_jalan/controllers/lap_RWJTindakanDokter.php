<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class lap_RWJTindakanDokter extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
             ini_set('memory_limit', "256M");
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}

	public function rupiah($nilai, $pecahan = 0) {
	    return number_format($nilai, $pecahan, '.', ',');
	}
	
	function getUser()
    {
        
		$result=$this->db->query("SELECT '111' as id,kd_user, full_name FROM zusers
									UNION
									SELECT '000' as id,'000' as kd_user, 'Semua' as full_name
									ORDER BY id,full_name")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';

    }
	
	public function cetak(){
   		$common=$this->common;
   		$result=$this->result;
		$param=json_decode($_POST['data']);
		
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and dt.kd_unit in(".substr($criteria1, 0, -1).")";
		
		if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		}

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='1'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='0'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter="";
			$dokter='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter=" AND d.kd_dokter='".$kd_dokter."'";
			$dokter=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		
		if($kd_user == '000' || $kd_user == 'Semua' || $kd_user == ''){
			$ckd_user="";
		} else{
			$ckd_user=" AND t.kd_user='".$kd_user."'";
		}
		
		if($detail == true  || $detail == 'true'){
			$criteriaHead = " t.ispay='t' And dt.Kd_kasir='".$KdKasir."' ";
		}else{
			$criteriaHead = " dt.Kd_kasir='".$KdKasir."' ";
		}

		$queryHead = $this->db->query(
			"SELECT 
						DISTINCT dtd.kd_dokter, d.nama as nama_dokter
					From Detail_Transaksi dt 
						INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
						INNER JOIN dokter d on d.kd_dokter = dtd.kd_dokter
					Where 
						dt.Kd_kasir='01' And 
						Folio in ('A', 'E') AND 
						(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
						".$criteria."
						".$criteriaunit."
						".$ckd_dokter."
					-- 	and dt.kd_produk in ('3975')
					-- 	and dtd.kd_dokter = '010'
						--and dt.no_transaksi = '2005649'
					Group By dtd.kd_dokter, d.nama
					ORDER BY d.nama ASC
                                        ")->result();		
		//-------------JUDUL-----------------------------------------------
		if($detail == true  || $detail == 'true'){
			$html='
				<table  cellspacing="0" border="0">
						<tr>
							<th colspan="8"> LAPORAN JASA PELAYANAN '.$dokter.'</th>
						</tr>
						<tr>
							<th colspan="8"> PERIODE '.$awal.' s/d '.$akhir.'</th>
						</tr>
						<tr>
							<th colspan="8"> KELOMPOK PASIEN '.$customerfar.'</th>
						</tr>
						<tr>
							<th colspan="8"> OPERATOR '.$full_name.'</th>
						</tr>
				</table><br>';
		}else{
			$html='
				<table  cellspacing="0" border="0">
						<tr>
							<th colspan="4"> LAPORAN JASA TINDAKAN '.$dokter.'</th>
						</tr>
						<tr>
							<th colspan="4"> PERIODE '.$awal.' s/d '.$akhir.'</th>
						</tr>
						<tr>
							<th colspan="4"> KELOMPOK PASIEN '.$customerfar.'</th>
						</tr>
						<tr>
							<th colspan="4"> OPERATOR '.$full_name.'</th>
						</tr>
				</table><br>';
		}
		//---------------ISI-----------------------------------------------------------------------------------
		$pajaknya=$this->db->query("select setting from sys_setting where key_data='pajak_dokter'")->row()->setting;
		if($detail == true  || $detail == 'true'){
			$html.='
				<table border = "1">
				<thead>
					 <tr>
						<th width="5%" align="center">No</th>
						<th width="13%" align="center">Keterangan</th>
						<th width="13%" align="center">Jml Pasien</th>
						<th width="13%" align="center">Jml Produk</th>
						<th width="13%" align="center">Jasa Dokter</th>
						<th width="13%" align="center">Total</th>
						<th width="13%" align="center">Pajak ('.$pajaknya.'%)</th>
						<th width="13%" align="center">Total Setelah Potongan</th>
					  </tr>
				</thead>';
		}else{
			$html.='
				<table border = "1">
				<thead>
					 <tr>
						<th width="5%" align="center">No</th>
						<th width="13%" align="center">Keterangan</th>
						<th width="13%" align="center">Jml Pasien</th>
						<th width="13%" align="center">Jml Produk</th>
					  </tr>
				</thead>';
		}
		$no_set = 1;	
		$baris=6;
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			$jmllinearea=count($queryHead)+6; # Untuk menghitung jumlah baris print area. 6 adalah jumlah baris judul dan baris jarak antara tabel
			foreach($queryHead as $line){
				$no++;
				if($detail == true  || $detail == 'true'){
					$html.='<tr>
								<td style="padding-left:10px;">'.$no.'</td>
								<td align="left" colspan="7" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>
							</tr>';
				}else{
					$html.='<tr>
								<td style="padding-left:10px;">'.$no.'</td>
								<td align="left" colspan="3" style="padding-left:10px;"><b>'.$line->nama_dokter.'</b></td>
							</tr>';
				}
				$baris++;
				$criteriaunitDetail = " and t.kd_unit in(".substr($criteria1, 0, -1).")";
				$jmllinearea += 1;
				// $queryBody = $this->db->query("");
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component = 0 THEN max(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component = 1 THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where dt.Kd_kasir='".$KdKasir."' And Folio in ('A','E') 
								AND (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." 
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						Where 
						xdt.kd_dokter='".$line->kd_dokter."' ".$criteria_profesi."
						".$ckd_customer." ".$criteriaunitDetail." ".$ckd_user."						
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);					
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;
				
				foreach ($query2 as $line2) {
					if($detail == true  || $detail == 'true'){
						$html.="<tr>";
						$html.="<td></td>";
						$tmpDesk = str_replace('&','dan',$line2->deskripsi);
						//$tmpDesk = str_replace('>','lebih dari',$line2->deskripsi);
						$tmpDesk = str_replace('<','kurang dari',$line2->deskripsi);
							$noo++;
							$total_jml_pasien += $line2->jml_pasien;
							$total_qty += $line2->qty_as;
							$total_jpdok += $line2->jpdok;
							$total_jumlah += $line2->jumlah_as;
							//$total_pajak += (($line2->jumlah_as/100)*$pajaknya);
							$total_pajak += $line2->pajak_pph_as;
							//$total_all += $line2->total_as-(($line2->jumlah_as/100)*$pajaknya);
							$total_all += $line2->total_as-$line2->pajak_pph_as;
							$html.="<td style='padding-left:10px;'>".$noo." - ".$tmpDesk."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jpdok)."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->jumlah_as)."</td>";
							//$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah((($line2->jumlah_as/100)*$pajaknya))."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->pajak_pph_as)."</td>";
							//$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->total_as-(($line2->jumlah_as/100)*$pajaknya))."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$this->rupiah($line2->total_as-$line2->pajak_pph_as)."</td>";
						$html.="</tr>";
						$jmllinearea += 1;
						$no_set++;
						// $baris++;
					}else{
						$html.="<tr>";
						$html.="<td></td>";
						$tmpDesk = str_replace('&','dan',$line2->deskripsi);
						//$tmpDesk = str_replace('>','lebih dari',$line2->deskripsi);
						$tmpDesk = str_replace('<','kurang dari',$line2->deskripsi);
							$noo++;
							$total_jml_pasien += $line2->jml_pasien;
							$total_qty += $line2->qty_as;
							$total_jpdok += $line2->jpdok;
							$total_jumlah += $line2->jumlah_as;
							//$total_pajak += (($line2->jumlah_as/100)*$pajaknya);
							$total_pajak += $line2->pajak_pph_as;
							//$total_all += $line2->total_as-(($line2->jumlah_as/100)*$pajaknya);
							$total_all += $line2->total_as-$line2->pajak_pph_as;
							$html.="<td style='padding-left:10px;'>".$noo." - ".$tmpDesk."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->jml_pasien."</td>";
							$html.="<td style='padding-right:10px;' align='right'>".$line2->qty_as."</td>";
						$html.="</tr>";
						$jmllinearea += 1;
						$no_set++;
						// $baris++;
					}
				
					$baris++;
				}
				
				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty        += $total_qty;
				$all_total_jpdok      += $total_jpdok;
				$all_total_jumlah     += $total_jumlah;
				$all_total_pajak      += $total_pajak;
				$all_total_all        += $total_all;

				if($detail == true  || $detail == 'true'){
					$html.="<tr>";
					$html.="<td></td>";
					$html.="<td align='right'>Sub Total</td>";
					$html.="<td align='right'>".$total_jml_pasien."</td>";
					$html.="<td align='right'>".$total_qty."</td>";
					$html.="<td align='right'>".number_format($total_jpdok,0,',',',')."</td>";
					$html.="<td align='right'>".$this->rupiah($total_jumlah)."</td>";
					$html.="<td align='right'>".$this->rupiah($total_pajak)."</td>";
					$html.="<td align='right'>".$this->rupiah($total_all)."</td>";
					$html.="</tr>";
				
					$html.="<tr>";
					$html.="<td colspan='8'>&nbsp;</td>";
					$html.="</tr>";
				}else{
					$html.="<tr>";
					$html.="<td></td>";
					$html.="<td align='right'><b>Sub Total</b></td>";
					$html.="<td align='right'><b>".$total_jml_pasien."</b></td>";
					$html.="<td align='right'><b>".$total_qty."</b></td>";
					$html.="</tr>";

					$html.="<tr>";
					$html.="<td colspan='4'>&nbsp;</td>";
					$html.="</tr>";
				}
				$jmllinearea += 1;
				$no_set++;
				
			$baris+=2;
			}

			if($detail == true  || $detail == 'true'){
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td align='right'><b>Grand Total</b></td>";
				$html.="<td  align='right'><b>".$all_total_jml_pasien."</b></td>";
				$html.="<td  align='right'><b>".$all_total_qty."</b></td>";
				$html.="<td  align='right'><b>".number_format($all_total_jpdok,0,',',',')."</b></td>";
				$html.="<td align='right'><b>".number_format($all_total_jumlah,0,',',',')."</b></td>";
				$html.="<td align='right'><b>".number_format($all_total_pajak,0,',',',')."</b></td>";
				$html.="<td  align='right'><b>".number_format($all_total_all,0,',',',')."</b></td>";
				$html.="</tr>";
			}else{
				$html.="<tr>";
				$html.="<td></td>";
				$html.="<td align='right'><b>Grand Total</b></td>";
				$html.="<td  align='right'><b>".$all_total_jml_pasien."</b></td>";
				$html.="<td  align='right'><b>".$all_total_qty."</b></td>";
				$html.="</tr>";
			}
			$jmllinearea = $jmllinearea + 4;
			$no_set++;
			$baris++;
		}else {		
			$jmllinearea = 8;

			if($detail == true  || $detail == 'true'){
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="8" align="center">Data tidak ada</th>
					</tr>
				';
			}else{
				$html.='
					<tr class="headerrow"> 
						<th width="" colspan="4" align="center">Data tidak ada</th>
					</tr>
				';
			}
			$no_set++;		
		} 

		$baris++;
		$html.='</table>';
		$prop=array('foot'=>true);
		$area_kanan='C9:D'.$baris;
		if($type_file == 1){
			$area_wrap	= 'A7:D'.((int)$baris);
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>, hilangkan jika ada.
		
			
            $table    = $html;
            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			if($jmllinearea < 90){
				if($jmllinearea < 45){
					$linearea=45;
				}else{
					$linearea=90;
				}
			}else{
				$linearea=$jmllinearea;
			}
				
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea('A1:D'.$linearea);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	)/* ,
					'font'  => array(
						'size'  => 12,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 // 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	)/* ,
					'font'  => array(
						'size'  => 11,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, 'A1:D7');
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:D7')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:D4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C:H')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			/* $objPHPExcel->getActiveSheet()
						->getStyle('C7:H7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			$objPHPExcel->getActiveSheet()
						->getStyle($area_kanan)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			# END Fungsi untuk set alignment 
			
			# END Fungsi untuk set Orientation Paper 
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi protected
			
			# Fungsi Autosize
            /* for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            } */
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);/*
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(23);*/
			# END Fungsi Autosize
			
			# Fungsi Wraptext
			// $objPHPExcel->getActiveSheet()->getStyle('A1:I999')
						// ->getAlignment()->setWrapText(true); 
			# Fungsi Wraptext
			
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('pelayanan_dokter_rwj'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_pelayanan_dokter_rwj.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('P','Lap. Jasa Pelayanan Dokter',$html);	
			echo $html;		
		}
   	}
	
	function cetakDirect(){
		
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,4,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		$pelayananPendaftaran   = $param->pelayananPendaftaran;
		$pelayananTindak    	= $param->pelayananTindak;
		$kd_dokter    			= $param->kd_dokter;
		$kd_user    			= $param->kd_user;
		$tglAwal   				= $param->tglAwal;
		$tglAkhir  				= $param->tglAkhir;
		$kd_customer  			= $param->kd_kelompok;
		$kd_profesi  			= $param->kd_profesi;
		$full_name  			= $param->full_name;
		$JmlList  				= $param->JmlList;
		$type_file 				= $param->type_file;
		$detail 				= $param->detail;
		$KdKasir				= $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		$awal 					= tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir 					= tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
		$u="";
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		$tmpunit = explode(',', $u);
		$criteria1 = "";
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria1 .= "".$tmpunit[$i].",";
		}
		$criteriaunit = " and dt.kd_unit in(".substr($criteria1, 0, -1).")";
		
		if($pelayananPendaftaran == 1 && ($pelayananTindak == 0 || $pelayananTindak == "")){
			$criteria=" and dt.kd_Produk IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananTindak == 1 && ($pelayananPendaftaran == 0 || $pelayananPendaftaran == "")){
			$criteria=" and dt.kd_Produk NOT IN (Select distinct Kd_Produk From AutoCharge a INNER JOIN Unit u On u.kd_Unit=a.kd_unit Where LEFT(u.kd_unit,1)='2')";
		} else if($pelayananPendaftaran == 1 && $pelayananTindak == 1){
			$criteria="";
		}else{
			$criteria="";			
		}

		if($kd_profesi == 1 ){
			$criteria_profesi=" and d.jenis_dokter='1'";
			$profesi="Dokter";			
		} else {
			$criteria_profesi=" and d.jenis_dokter='0'";			
			$profesi="Perawat";			
		}

		if(strtoupper($kd_customer) == 'SEMUA' || $kd_customer == ''){
			$ckd_customer="";
			$customerfar='SEMUA';
		} else{
			$ckd_customer=" AND k.kd_customer='".$kd_customer."'";
			$customerfar=strtoupper($this->db->query("SELECT kd_customer,customer from customer where kd_customer='".$kd_customer."'")->row()->customer);
		}
		
		if(strtoupper($kd_dokter) == 'SEMUA' || $kd_dokter == ''){
			$ckd_dokter="";
			$dokter='SEMUA '.strtoupper($profesi);
		} else{
			$ckd_dokter=" AND d.kd_dokter='".$kd_dokter."'";
			$dokter=$profesi." ".$this->db->query("SELECT kd_dokter,nama from dokter where kd_dokter='".$kd_dokter."'")->row()->nama;
		}
		
		if($kd_user == '000' || $kd_user == 'Semua' || $kd_user == ''){
			$ckd_user="";
		} else{
			$ckd_user=" AND t.kd_user='".$kd_user."'";
		}
		
		if($detail == true  || $detail == 'true'){
			$criteriaHead = " t.ispay='t' And dt.Kd_kasir='".$KdKasir."' ";
		}else{
			$criteriaHead = " dt.Kd_kasir='".$KdKasir."' ";
		}

		$queryHead = $this->db->query(
			"SELECT 
						DISTINCT dtd.kd_dokter, d.nama as nama_dokter
					From Detail_Transaksi dt 
						INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
						INNER JOIN dokter d on d.kd_dokter = dtd.kd_dokter
					Where 
						dt.Kd_kasir='01' And 
						Folio in ('A', 'E') AND 
						(dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
						".$criteria."
						".$criteriaunit."
						".$ckd_dokter."
					-- 	and dt.kd_produk in ('3975')
					-- 	and dtd.kd_dokter = '010'
						--and dt.no_transaksi = '2005649'
					Group By dtd.kd_dokter, d.nama
                                        ")->result();		
		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 5)
			->setColumnLength(1, 60)
			->setColumnLength(2, 30)
			->setColumnLength(3, 30)
			->setUseBodySpace(true);
		
	
		$tp	->addSpace("header")
			->addColumn($rs->name, 4,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city,4,"left")
			->commit("header")
			->addColumn($telp, 4,"left")
			->commit("header")
			->addColumn($fax, 4,"left")
			->commit("header")
			->addColumn("LAPORAN JASA TINDAKAN ".$dokter, 4,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 4,"center")
			->commit("header")
			->addColumn("KELOMPOK PASIEN ".$customerfar, 4,"center")
			->commit("header")
			->addColumn("OPERATOR ".$full_name, 4,"center")
			->commit("header")
			->addSpace("header");
		
		
		# SET HEADER REPORT
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KETERANGAN", 1,"left")
			->addColumn("JUMLAH PASIEN", 1,"right")
			->addColumn("JUMLAH PRODUK", 1,"right")
			->commit("header");
		$no_set = 1;	
		$baris=0;
		if(count($queryHead) > 0) {
			$no=0;
			$all_total_jml_pasien = 0;
			$all_total_qty = 0;
			$all_total_jpdok = 0;
			$all_total_jumlah = 0;
			$all_total_pajak = 0;
			$all_total_all = 0;
			foreach($queryHead as $line){
				$no++;
				$tp	->addColumn($no.".", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
					->addColumn($line->nama_dokter, 3,"left")
					->commit("header");
				$criteriaunitDetail = " and t.kd_unit in(".substr($criteria1, 0, -1).")";
				$queryBody = $this->db->query( 
					"SELECT
					 x.deskripsi, 
					 max(x.jml_pasien) as jml_pasien, 
					 max(x.qty) as qty_as,
					 sum(x.JPDOK) as jpdok, 
					 sum(x.JPA) as jpa_as, 
					 sum(x.jumlah) as jumlah_as,
					 sum(x.Pajak_ops) as pajak_ops_as, 
					 sum(x.Pajak_pph) as Pajak_pph_as,
					 sum(x.Total) As total_as, 
					 ((select setting from sys_setting where key_data='pajak_dokter')::double precision /100)*Sum(x.qty * x.JPA) as pph 
					 From
					 (
						Select  
						d.Nama, 
						p.Deskripsi, 
						Count(k.Kd_pasien) as Jml_Pasien, 
						Sum(xdt.Qty) as Qty, 
						(CASE WHEN xdt.kd_component = 0 THEN max(xdt.Tarif ) ELSE 0 END) as JPDok, 
						(CASE WHEN xdt.kd_component = 1 THEN max(xdt.Tarif ) ELSE 0 END) as JPA, 
						SUM(xdt.Qty * xdt.Tarif) as Jumlah,
						(Sum(xdt.Qty) * max(xdt.pajak_op)) as Pajak_ops,
						(Sum(xdt.Qty) * max(xdt.pajak_pph)) as Pajak_pph,
						Sum(xdt.Qty * xdt.Tarif) - (Sum(xdt.pajak_pph)) as Total
						From 
						(
							(
							(
								(
								kunjungan k Left Join Kontraktor knt On k.kd_Customer=knt.kd_Customer
								) 
								INNER JOIN Transaksi t ON t.Kd_Pasien=k.Kd_Pasien And t.Kd_Unit=k.Kd_Unit  
								And t.Tgl_Transaksi=k.Tgl_masuk And t.Urut_Masuk=k.Urut_Masuk
							)  
							INNER JOIN 
							(
								Select 
									dt.kd_Kasir, 
									dt.no_transaksi, 
									dt.kd_Produk, 
									dtd.Kd_Dokter, 
									dtd.kd_component, 
									Sum(Qty) as Qty, 
									max(dtd.JP) as tarif, 
									max(dtd.POT_OPS) as pajak_op, 
									max(dtd.pajak) as pajak_pph, 
									max(dt.harga) as harga 
								From Detail_Transaksi dt 
								INNER JOIN Detail_trDokter Dtd On dtd.No_transaksi=dt.No_transaksi And dtd.Kd_Kasir=dt.Kd_Kasir 
								And dtd.Urut=dt.Urut And dtd.Tgl_Transaksi=dt.Tgl_Transaksi 
								Where dt.Kd_kasir='".$KdKasir."' And Folio in ('A','E') 
								AND (dt.Tgl_Transaksi BETWEEN '".$tglAwal."' AND '".$tglAkhir."') 
								 ".$criteria." 
								Group By dt.kd_kasir, dt.no_transaksi, dt.Kd_Produk, dtd.Kd_Dokter, Dtd.kd_component
							) xdt 
						On xdt.No_Transaksi=t.No_Transaksi And xdt.Kd_Kasir=t.Kd_Kasir)  
						INNER JOIN Produk p ON p.Kd_Produk=xdt.KD_Produk)  
						INNER JOIN Dokter d ON d.Kd_Dokter=xdt.KD_Dokter 
						INNER JOIN Unit U ON t.kd_Unit = U.Kd_Unit 
						Where 
						xdt.kd_dokter='".$line->kd_dokter."' ".$criteria_profesi."
						".$ckd_customer." ".$criteriaunitDetail." ".$ckd_user."						
						Group By Nama, Deskripsi, xdt.kd_component 
						Having Max(xdt.Tarif) > 0 
						) x
				 group by x.Nama, x.Deskripsi
				 Order By x.Nama, x.Deskripsi"
				);					
				$query2 = $queryBody->result();
				
				$noo=0;
				$total_jml_pasien = 0;
				$total_qty = 0;
				$total_jpdok = 0;
				$total_jumlah = 0;
				$total_pajak = 0;
				$total_all = 0;
				
				foreach ($query2 as $line2) 
				{
					$noo++;
					$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
						->addColumn($noo." - ".$line2->deskripsi, 1,"left")
						->addColumn($line2->jml_pasien, 1,"right")
						->addColumn($line2->qty_as, 1,"right")
						->commit("header");
					$total_jml_pasien += $line2->jml_pasien;
					$total_qty += $line2->qty_as;
					$total_jpdok += $line2->jpdok;
					$total_jumlah += $line2->jumlah_as;
					$total_pajak += $line2->pajak_pph_as;
					$total_all += $line2->total_as-$line2->pajak_pph_as;
					$no_set++;
					$baris++;
					
				}
				
				
				$all_total_jml_pasien += $total_jml_pasien;
				$all_total_qty        += $total_qty;
				$all_total_jpdok      += $total_jpdok;
				$all_total_jumlah     += $total_jumlah;
				$all_total_pajak      += $total_pajak;
				$all_total_all        += $total_all;

				$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
					->addColumn("Sub Total", 1,"left")
					->addColumn($total_jml_pasien, 1,"right")
					->addColumn($total_qty, 1,"right")
					->commit("header");
				
				$tp	->addColumn("", 4,"left")
					->commit("header");
				
			}

			$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
				->addColumn("Grand Total", 1,"left")
				->addColumn($all_total_jml_pasien, 1,"right")
				->addColumn($all_total_qty, 1,"right")
				->commit("header");
			
		}else{		
			$tp	->addColumn("Data tidak ada", 4,"center")
				->commit("header");
			
		} 
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 2,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 2,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_tindakan_dokter_rwj.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
?>