<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class lap_daftar_pasien_rwj extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	public function index()
	{
		$this->load->view('main/index');
	} 
   
	public function cetak()
    {
		
		$param       = json_decode($_POST['data']);
		$Params      = $param->criteria;
		$type_file   = $param->type_file;
		$order_by    = $param->order_by;
		$html        ='';
		$totalpasien = 0;
		// $UserID   = 0;
		$Split       = explode("##@@##", $Params,  9);
		//var_dump($Split);
		$tglsum      = $Split[0];
		$tglsummax   = $Split[1];
		$criteria    = "";
		$tmpunit     = explode(',', $Split[3]);
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1);
		$Paramplus = " ";

		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "Order By ps.kd_pasien ASC";
		}else if (strtolower($order_by) == strtolower("Nama Pasien")) {
			$criteriaOrder = "Order By ps.nama ASC";
		}else if (strtolower($order_by) == strtolower("Penjamin")) {
			$criteriaOrder = "Order By c.customer ASC";
		}else{
			$criteriaOrder = "Order By k.tgl_masuk ASC";
		}

		$tmpkelpas = $Split[4];
		$kelpas = $Split[5];
            // print_r($Split);
		if ($tmpkelpas !== 'Semua')
			{
				if ($tmpkelpas === 'Umum')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						// $tmpTambah = 'Umum';
						$tmpTambah = 'Kelompok Pasien : Umum';
					}else{
						$tmpTambah = "Kelompok Pasien : SEMUA Umum";
					}
				}elseif ($tmpkelpas === 'Perusahaan')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						// $tmpTambah = $Split[6];
						$tmpTambah = "Kelompok Pasien : ".$Split[6];
					} else{
						$tmpTambah = "Kelompok Pasien : SEMUA Perusahaan";
					}
				}elseif ($tmpkelpas === 'Asuransi')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						// $tmpTambah = $Split[6];
						$tmpTambah = "Kelompok Pasien : ".$Split[6];
					} else{
						$tmpTambah = "Kelompok Pasien : SEMUA Asuransi";
					}
				}
			}else {
				$Param = $Paramplus." ";
				// $tmpTambah = 'Umum';
				$tmpTambah = "Semua Kelompok Pasien ";
			}

				
			$baris=0;	   
		// while ($line = pg_fetch_array($resultRS, null, PGSQL_ASSOC)) {
			$html.='
					<table  cellspacing="0" border="0">
						<tr>
							<th colspan="8">Laporan Daftar Pasien Rawat Jalan </th>
						</tr>
						<tr>
							<th colspan="8">'.$tmpTambah.'</th>
						</tr>
						<tr>
							<th colspan="8">Periode '.$tglsum.' s/d '.$tglsummax.'</th>
						</tr>
					</table><br>';
				$html.='
						   <table  border = "1">
							 <tr>
								   <th align="center" rowspan="2">No. </th>
								   <th align="center" rowspan="2">Kode Pasien</th>
								   <th align="center" rowspan="2">Nama Pasien</th>
								   <th align="center" width="70" colspan="2">Kelamin</th>
								   <th align="center" width="70" colspan="2">Status Kunj</th>
								   <th align="center" width="120" rowspan="2">Tgl Masuk</th>
								   <th align="center" width="120" rowspan="2">Penjamin</th>
							 </tr>
							 <tr>
								   <th align="center" width="37">L</th>
								   <th align="center" width="37">P</th>
								   <th align="center" width="37">Baru</th>
								   <th align="center" width="37">Lama</th>
							 </tr>
						   ';

			$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
			on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($criteria)
			group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
			");


			while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
			{
			   if ( $Split[2]=='kosong')
			   {
				   $query = "SELECT k.kd_unit as kdunit,u.nama_unit as namaunit, ps.kd_pasien as kdpasien,ps.nama  as namapasien,
						ps.alamat as alamatpas, case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, k.baru,
						case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, case when date_part('year',age(ps.Tgl_Lahir))<=5 then
						to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
						to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari' else to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn' end as umur,
						case when k.Baru=true then 'x'  else ''  end as pasienbar,
						case when k.Baru=false then 'x'  else ''  end as pasienlama,pk.pekerjaan as pekerjaan, 
						prs.perusahaan as perusahaan,  case when k.kd_Rujukan=0 then '' else 'x' end as rujukan ,
						k.Jam_masuk as jammas, k.Tgl_masuk as tglmas,dr.nama as dokter, c.customer as customer, zu.user_names as username,
						getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
						getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
					From ((	pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
							left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
					INNER JOIN  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
					LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
					INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
					INNER JOIN transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
						and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
					WHERE u.kd_unit='".$f['kd_unit']."' 
						and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
					 group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
					 zu.user_names,k.urut_masuk
					 $criteriaOrder";
					// Order By k.kd_unit, k.KD_PASIEN";
			   }else {
				   $query = "SELECT u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien, ps.nama as namapasien, case when ps.jenis_kelamin= true then 'L' else 'P' end as jk ,
								k.Tgl_masuk as tglmas, k.Tgl_keluar as tglkeluar,c.customer as customer, k.baru
						From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan) 
								left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan) 
							INNER JOIN (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit) on k.Kd_Pasien = ps.Kd_Pasien 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer 
						WHERE u.kd_unit in ('".$f['kd_unit']."') and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' 
							and k.Baru ='".$Split[2]."' $Paramplus
						group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,c.customer, k.urut_masuk 
						$criteriaOrder";
						// Order By k.kd_unit, k.KD_PASIEN";
				}
				
				$result = pg_query($query) or die('Query failed: ' . pg_last_error());
				$i=1;

				if(pg_num_rows($result) <= 0){
					$html.="<tr><td>Data Tidak Ada</td></tr>";
					$baris++;
				}else{
					$tmpNamaUnit=$f['nama_unit'];
					$tmpNamaUnit = str_replace("&","dan",$tmpNamaUnit);
					$html.='<tr><td></td><td colspan="8">'.$tmpNamaUnit.'</td></tr>';
					$baris++;			
					$tot_baru 		= 0;
					$tot_lama 		= 0;
					$tot_laki 		= 0;
					$tot_perempuan	= 0;
					while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
						$tmptgl_masuk 	= '';

						if ($line['baru'] == 't') {
							$tot_baru++;
						}else{
							$tot_lama++;
						}

						if($line['tglmas'] != ''){
							$tmptgl_masuk = date('d-m-Y', strtotime($line['tglmas']));
						}else
						{
							$tmptgl_masuk = '';
						}
						
					   $html.='
							   	<tr> 
									   <td align="center">'.$i.'</td>
									   <td  align="left">'.$line['kdpasien'].'</td>
									   <td  align="left">'.$line['namapasien'].'</td>';
									   if (strtolower($line['jk']) == "l") {
									   	$html.='
										   	<td  align="center">X</td>
											<td  align="center"></td>
									   	';
									   	$tot_laki++;
									   }else{
									   	$html.='
										   	<td  align="center"></td>
											<td  align="center">X</td>
									   	';
									   	$tot_perempuan++;
									   }
						$pasienbar='';
						if(!empty($line['pasienbar'])){
							$pasienbar = $line['pasienbar'];
						}
						$pasienlama='';
						if(!empty($line['pasienlama'])){
							$pasienlama = $line['pasienlama'];
						}
						$html.= '<td  align="center">'.$pasienbar.'</td>
									   <td  align="center">'.$pasienlama.'</td>
									   <td  align="center">'.$tmptgl_masuk.'</td>
									   <td  align="center">'.$line['customer'].'</td>
							   </tr>';
						$i++;
						$baris++;			
					}
						$i--;
						$totalpasien += $i;
						$html.='<tr><td colspan="3">Total Pasien di Poli '.$tmpNamaUnit.' : '.$i.'</td>
						<td align="center">'.$tot_laki.'</td>
						<td align="center">'.$tot_perempuan.'</td>
						<td align="center">'.$tot_baru.'</td>
						<td align="center">'.$tot_lama.'</td>
						<td align="center"></td>
						<td ></td></tr>';
						$baris++;	
				}
					  
			}
         $html.='<tr><td colspan="3"><b>Total Keseluruhan Pasien : '.$totalpasien.'</b></td>
         		<td></td>
				<td align="center"></td>
				<td align="center"></td>
				<td></td><td></td><td></td></tr>';
		 $html.='</table>';
		 
		$prop=array('foot'=>true);
		$baris=$baris+8;
		$print_area='A1:I'.$baris;
		$area_wrap='A1:I'.$baris;
		
		if($type_file==1){
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Print area
			# - Type font
			# - Paragraph alignment
			# - Password protected
			# Lap. bentuk excel tidak dapat membaca tag html -> <tbody>. Hilangkan jika ada.
			# Lap. bentuk excel tidak dapat membaca simbol -> &. Ganti dengan karakter biasa jika ada.
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set PRINT AREA
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set PRINT AREA			
						
			# Fungsi untuk set MARGIN
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0.4);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set MARGIN
			
			# Fungsi untuk set TYPE FONT 
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 11,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:I6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			
			$styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A6:I'.$baris)->applyFromArray($styleBorder);
			# END Fungsi untuk set TYPE FONT 
						
			# Fungsi untuk set ALIGNMENT 
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:I6')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('A6:A'.$baris)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()
						->getStyle('D'.$baris.':'.'I'.$baris)
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// # END Fungsi untuk set ALIGNMENT 
			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
			# Fungsi untuk PROTECTED SHEET
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('ncimedis');
			# END fungsi PROTECTED SHEET
			
			#Fungsi mengatur lebar kolom
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
			
			$objPHPExcel->getActiveSheet()
						->getPageSetup()
						->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

			#Fungsi wraptext
			$objPHPExcel->getActiveSheet()->getStyle($area_wrap)
						->getAlignment()->setWrapText(true); 
            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('Lap'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xls file
            header('Content-Disposition: attachment;filename=LaporanDaftarRWJ.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
		}else{
			$common=$this->common;
			 $this->common->setPdf('P',' LAPORAN DAFTAR PASIEN RAWAT JALAN',$html);
		}
	
	}
	
	
	function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user_s=$this->session->userdata['user_id']['id'];
		$users=$this->db->query("SELECT user_names FROM zusers WHERE kd_user='".$kd_user_s."'")->row()->user_names;
   		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$Params      = $param->criteria;
		$type_file   = $param->type_file;
		$order_by    = $param->order_by;
		$html        ='';
		$totalpasien = 0;
		// $UserID   = 0;
		$Split       = explode("##@@##", $Params,  9);
		//var_dump($Split);
		$tglsum      = $Split[0];
		$tglsummax   = $Split[1];
		$criteria    = "";
		$tmpunit     = explode(',', $Split[3]);
		for($i=0;$i<count($tmpunit);$i++)
		{
			$criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1);
		$Paramplus = " ";

		if (strtolower($order_by) == strtolower("Medrec")) {
			$criteriaOrder = "Order By ps.kd_pasien ASC";
		}else if (strtolower($order_by) == strtolower("Nama Pasien")) {
			$criteriaOrder = "Order By ps.nama ASC";
		}else if (strtolower($order_by) == strtolower("Penjamin")) {
			$criteriaOrder = "Order By c.customer ASC";
		}else{
			$criteriaOrder = "Order By k.tgl_masuk ASC";
		}

		$tmpkelpas = $Split[4];
		$kelpas = $Split[5];
            // print_r($Split);
		if ($tmpkelpas !== 'Semua')
			{
				if ($tmpkelpas === 'Umum')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						// $tmpTambah = 'Umum';
						$tmpTambah = 'Kelompok Pasien : Umum';
					}else{
						$tmpTambah = "Kelompok Pasien : SEMUA Umum";
					}
				}elseif ($tmpkelpas === 'Perusahaan')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						// $tmpTambah = $Split[6];
						$tmpTambah = "Kelompok Pasien : ".$Split[6];
					}else{
						$tmpTambah = "Kelompok Pasien : SEMUA Perusahaan";
					}
				}elseif ($tmpkelpas === 'Asuransi')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						// $tmpTambah = $Split[6];
						$tmpTambah = "Kelompok Pasien : ".$Split[6];
					} else{
						$tmpTambah = "Kelompok Pasien : SEMUA Asuransi";
					}
				}
			}else {
				$Param = $Paramplus." ";
				// $tmpTambah = 'Umum';
				$tmpTambah = "Semua Kelompok Pasien ";
			}

		
		# SET JUMLAH KOLOM BODY
		$tp ->setColumnLength(0, 3)
			->setColumnLength(1, 15)
			->setColumnLength(2, 30)
			->setColumnLength(3, 10)
			->setColumnLength(4, 10)
			->setColumnLength(5, 10)
			->setColumnLength(6, 10)
			->setColumnLength(7, 15)
			->setColumnLength(8, 20)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN DAFTAR PASIEN RAWAT JALAN", 9,"center")
			->commit("header")
			->addColumn($tmpTambah, 9,"center")
			->commit("header")
			->addColumn("Periode ".$tglsum." s/d ".$tglsummax, 9,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("KODE PASIEN", 1,"left")
			->addColumn("NAMA PASIEN", 1,"left")
			->addColumn("JENIS KELAMIN", 2,"center")
			->addColumn("STATUS KUNJUNGAN", 2,"center")
			->addColumn("TGL MASUK", 1,"left")
			->addColumn("PENJAMIN", 1,"left")
			->commit("header");
			
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("L", 1,"center")
			->addColumn("P", 1,"center")
			->addColumn("BARU", 1,"center")
			->addColumn("LAMA", 1,"center")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");
			
		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
			on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($criteria)
			group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
			");


			while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
			{
			   if ( $Split[2]=='kosong')
			   {
				   $query = "SELECT k.kd_unit as kdunit,u.nama_unit as namaunit, ps.kd_pasien as kdpasien,ps.nama  as namapasien,
						ps.alamat as alamatpas, case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, k.baru,
						case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, case when date_part('year',age(ps.Tgl_Lahir))<=5 then
						to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '||to_char(date_part('month',age(ps.Tgl_Lahir)), '999')||' bln ' ||
						to_char(date_part('days',age(ps.Tgl_Lahir)), '999')||' hari' else to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn' end as umur,
						case when k.Baru=true then 'X'  else ''  end as pasienbar,
						case when k.Baru=false then 'X'  else ''  end as pasienlama,pk.pekerjaan as pekerjaan, 
						prs.perusahaan as perusahaan,  case when k.kd_Rujukan=0 then '' else 'X' end as rujukan ,
						k.Jam_masuk as jammas, k.Tgl_masuk as tglmas,dr.nama as dokter, c.customer as customer, zu.user_names as username,
						getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
						getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
					From ((	pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
							left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan)  
					INNER JOIN  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   on k.Kd_Pasien = ps.Kd_Pasien  
					LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
					INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
					INNER JOIN transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
						and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
					WHERE u.kd_unit='".$f['kd_unit']."' 
						and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
					 group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
					 zu.user_names,k.urut_masuk
					 $criteriaOrder";
					// Order By k.kd_unit, k.KD_PASIEN";
			   }else {
				   $query = "SELECT u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien, ps.nama as namapasien, case when ps.jenis_kelamin= true then 'L' else 'P' end as jk ,
								k.Tgl_masuk as tglmas, k.Tgl_keluar as tglkeluar,c.customer as customer, k.baru
						From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan) 
								left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan) 
							INNER JOIN (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit) on k.Kd_Pasien = ps.Kd_Pasien 
							LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
							INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer 
						WHERE u.kd_unit in ('".$f['kd_unit']."') and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' 
							and k.Baru ='".$Split[2]."' $Paramplus
						group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,c.customer, k.urut_masuk 
						$criteriaOrder";
						// Order By k.kd_unit, k.KD_PASIEN";
				}
				
				$result = pg_query($query) or die('Query failed: ' . pg_last_error());
				$i=1;

				if(pg_num_rows($result) <= 0){
					$tp	->addColumn("Data Tidak Ada", 9,"left")
						->commit("header");
				}else{
					$tp	->addColumn("", 1,"left")
						->addColumn($f['nama_unit'], 8,"left")
						->commit("header");	
					$tot_baru 		= 0;
					$tot_lama 		= 0;
					$tot_laki 		= 0;
					$tot_perempuan	= 0;
					while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
						$tmptgl_masuk 	= '';

						if ($line['baru'] == 't') {
							$tot_baru++;
						}else{
							$tot_lama++;
						}

						if($line['tglmas'] != ''){
							$tmptgl_masuk = date('d-m-Y', strtotime($line['tglmas']));
						}else
						{
							$tmptgl_masuk = '';
						}
						
						$tp	->addColumn($i.".", 1,"left")
							->addColumn($line['kdpasien'], 1,"left")
							->addColumn($line['namapasien'], 1,"left");
							
						if (strtolower($line['jk']) == "l") {
							$tp	->addColumn("X", 1,"center")
								->addColumn("", 1,"center");
							$tot_laki++;
						}else{
							$tp	->addColumn("", 1,"center")
								->addColumn("X", 1,"center");
							$tot_perempuan++;
						}	
						$pasienbar='';
						if(!empty($line['pasienbar'])){
							$pasienbar = $line['pasienbar'];
						}
						$pasienlama='';
						if(!empty($line['pasienlama'])){
							$pasienlama = $line['pasienlama'];
						}
						$tp	->addColumn($pasienbar, 1,"center")
							->addColumn($pasienlama, 1,"center")
							->addColumn($tmptgl_masuk, 1,"left")
							->addColumn($line['customer'], 1,"left")
							->commit("header");
						$i++;		
					}
						$i--;
						$totalpasien += $i;
						$tp	->addColumn("Total Pasien di Poli ".$f['nama_unit']." : ".$i, 3,"left")
							->addColumn($tot_laki, 1,"center")
							->addColumn($tot_perempuan, 1,"center")
							->addColumn($tot_baru, 1,"center")
							->addColumn($tot_lama, 1,"center")
							->addColumn("", 1,"right")
							->addColumn("", 1,"right")
							->commit("header");
					$tp	->addColumn("", 9,"left")
						->commit("header");
				}
					  
			}
		$tp	->addColumn("Total Pasien  Keseluruhan Pasien : ".$totalpasien, 3,"left")
			->addColumn("", 6,"right")
			->commit("header");
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$users, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_daftar_pasien_rwj.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user_s."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
}
?>