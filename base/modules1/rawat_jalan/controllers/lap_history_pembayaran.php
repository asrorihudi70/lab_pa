<?php

/**
 * Maya Silviana
 * 09-10-2017
 */


class lap_history_pembayaran extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,9,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$param=json_decode($_POST['data']);
		$html='';
   		
		$KdKasir=$this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		
   		$type_file = $param->type_file;
		$tgl_awal_i = str_replace("/","-", $param->tglAwal);
		$tgl_akhir_i = str_replace("/","-",$param->tglAkhir) ;
		$tgl_awal = date('Y-m-d', strtotime($tgl_awal_i));
		$tgl_akhir = date('Y-m-d', strtotime($tgl_akhir_i));
		$awal = tanggalstring($tgl_awal);
		$akhir = tanggalstring($tgl_akhir);
   		
		$result   = $this->db->query("Select * 
										FROM HISTORY_DETAIL_bayar  
										where kd_kasir = '".$KdKasir."' 
											and Tgl_Batal Between '".$tgl_awal."' and '".$tgl_akhir."' 
									")->result();
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 10)
			->setColumnLength(2, 10)
			->setColumnLength(3, 20)
			->setColumnLength(4, 15)
			->setColumnLength(5, 15)
			->setColumnLength(6, 10)
			->setColumnLength(7, 15)
			->setColumnLength(8, 20)
			->setUseBodySpace(true);
		
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 9,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city,9,"left")
			->commit("header")
			->addColumn($telp, 9,"left")
			->commit("header")
			->addColumn($fax, 9,"left")
			->commit("header")
			->addColumn("LAPORAN HISTORY PEMBAYARAN", 9,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 9,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NO. TRANS", 1,"left")
			->addColumn("NO. MEDREC", 1,"left")
			->addColumn("NAMA PASIEN", 1,"left")
			->addColumn("UNIT", 1,"left")
			->addColumn("URAIAN", 1,"left")
			->addColumn("OPERATOR", 1,"left")
			->addColumn("JUMLAH", 1,"right")
			->addColumn("KETERANGAN", 1,"left")
			->commit("header");
		if(count($result)==0){
			$tp	->addColumn("Data tidak ada", 9,"left")
				->commit("header");	
		} else{
			$no=0;
			$jumlah=0;
			foreach($result as $line){
				$noo=0;
				$no++;
				$tp	->addColumn($no.".", 1,"left")
					->addColumn($line->no_transaksi, 1,"left")
					->addColumn($line->kd_pasien, 1,"left")
					->addColumn($line->nama, 1,"left")
					->addColumn($line->nama_unit, 1,"left")
					->addColumn($line->uraian, 1,"left")
					->addColumn($line->user_name, 1,"left")
					->addColumn(number_format($line->jumlah,0,'.',','), 1,"right")
					->addColumn($line->ket, 1,"left")
					->commit("header");	
					$jumlah += $line->jumlah;
				
			}
			$tp	->addColumn("", 7,"left")
				->commit("header");	
			$tp	->addColumn("TOTAL", 7,"right")
				->addColumn(number_format($jumlah,0,'.',','), 1,"right")
				->addColumn("", 1,"left")
				->commit("header");	
		}	
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 7,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_history_pembayaran_rwj.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>