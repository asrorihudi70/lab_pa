<?php

/**
 * Maya Silviana
 * 09-10-2017
 */


class lap_register_detail extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
             $this->load->library('result');
             $this->load->library('common');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
	
	public function cetak_direct(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,16,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		$JenisPasien = $param->JenisPasien;
		$TglAwal = $param->TglAwal;
		$TglAkhir = $param->TglAkhir;
		$KelPasien = $param->KelPasien;
		$KelPasien_d = $param->KelPasien_d;
		$type_file = $param->Type_File;
		$JmlList = $param->JmlList;
		$nama_kel_pas =$param->nama_kel_pas;
		//ambil list kd_unit
		$u="";
		
		for($i=0;$i<$JmlList;$i++){
			$kd_unit ="kd_unit".$i;
			if($u !=''){
				$u.=', ';
			}
			$u.="'".$param->$kd_unit."'";
		}
		
		$totalpasien = 0;
        $UserID = 0;
		$tglsum = $TglAwal; //tgl awal
        $tglsummax = $TglAkhir; //tgl akhir
		$awal=tanggalstring(date('Y-m-d',strtotime($tglsum)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglsummax)));
		$criteria="";
		$tmpunit = explode(',', $u); //arr kd_unit
		for($i=0;$i<count($tmpunit);$i++)
		{
		   $criteria .= "'".$tmpunit[$i]."',";
		}
		$criteria = substr($criteria, 0, -1); //menghilangkan koma
		$Paramplus = " "; //potongan query
		$tmpkelpas = $KelPasien; //kel pasien
		$kelpas = $KelPasien_d; // kel pasien detail
		$tmpTambah='';
		if ($tmpkelpas !== 'Semua')
			{
				if ($tmpkelpas === 'Umum')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=0 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : Umum';
					}
				}elseif ($tmpkelpas === 'Perusahaan')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=1 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}elseif ($tmpkelpas === 'Asuransi')
				{
					$Paramplus = $Paramplus." and ktr.Jenis_cust=2 ";
					if ($kelpas !== 'NULL')
					{
						$Paramplus = $Paramplus." and ktr.kd_Customer= "."'".$kelpas."'"." ";
						$tmpTambah = 'Kelompok Pasien : '.$nama_kel_pas;
					}
				}
			}else {
				$Param = $Paramplus." ";
				$tmpTambah = $nama_kel_pas;
			}
		
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 3)
			->setColumnLength(1, 10)
			->setColumnLength(2, 11)
			->setColumnLength(3, 10)
			->setColumnLength(4, 2)
			->setColumnLength(5, 6)
			->setColumnLength(6, 4)
			->setColumnLength(7, 4)
			->setColumnLength(8, 10)
			->setColumnLength(9, 10)
			->setColumnLength(10, 4)
			->setColumnLength(11, 9)
			->setColumnLength(12, 12)
			->setColumnLength(13, 10)
			->setColumnLength(14, 6)
			->setColumnLength(15, 5)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 16,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 16,"left")
			->commit("header")
			->addColumn($telp, 16,"left")
			->commit("header")
			->addColumn($fax, 16,"left")
			->commit("header")
			->addColumn("LAPORAN BUKU REGISTRASI DETAIL RAWAT JALAN", 16,"center")
			->commit("header")
			->addColumn($tmpTambah, 16,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 16,"center")
			->commit("header");
		$tp	->addColumn("", 16,"center")
			->commit("header");
	
		#QUERY HEAD
		
		$fquery = pg_query("select unit.kd_unit,unit.nama_unit from unit left join kunjungan 
						   on kunjungan.kd_unit=unit.kd_unit where kd_bagian ='02' and kunjungan.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and unit.kd_unit in($u)
						   group by unit.kd_unit,unit.nama_unit  order by nama_unit asc
							");
							
	
		$tp	->addColumn("NO.", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NO. MEDREC", 1,"left")
			->addColumn("NAMA PASIEN", 1,"left")
			->addColumn("ALAMAT", 1,"left")
			->addColumn("JK", 1,"left")
			->addColumn("UMUR", 1,"left")
			->addColumn("KUNJUNGAN", 2,"center")
			->addColumn("PEKERJAAN", 1,"left")
			->addColumn("TGL MASUK", 1,"left")
			->addColumn("RUJ.", 1,"left")
			->addColumn("JAM MASUK", 1,"left")
			->addColumn("KD. DIAGNOSA", 1,"left")
			->addColumn("DIAGNOSA", 1,"left")
			->addColumn("DOKTER", 1,"left")
			->addColumn("USER", 1,"left")
			->commit("header");
			
			
		$tp	->addColumn("", 1,"left")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("BARU", 1,"left")
			->addColumn("LAMA", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->addColumn("", 1,"left")
			->commit("header");
		$totBaru=0;
		$totLama=0;
		$totRujukan=0;
		while($f = pg_fetch_array($fquery, null, PGSQL_ASSOC))
		{
			if ( $JenisPasien=='kosong')
			{
			   $query = "
							Select k.kd_unit as kdunit,
							u.nama_unit as namaunit, 
							ps.kd_Pasien as kdpasien,
							ps.nama  as namapasien,
							ps.alamat as alamatpas, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
							case when date_part('year',age(ps.Tgl_Lahir))<=5 then
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '
							else
								to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
							end
							as umur,
							
							case when k.Baru=true then 'X'  else ''  end as pasienbar,
							case when k.Baru=false then 'X'  else ''  end as pasienlama,
							pk.pekerjaan as pekerjaan, 
							prs.perusahaan as perusahaan,  
							case when k.kd_Rujukan=0 then '' else 'X' end as rujukan ,
							k.Jam_masuk as jammas,
							k.Tgl_masuk as tglmas,
							dr.nama as dokter, 
							c.customer as customer, 
							zu.user_names as username,
							getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
							getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From (
										(pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									)  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit='".$f['kd_unit']."' and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' $Paramplus
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
									zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN 
			   ";
			} else {
					$query = "
								Select k.kd_unit as kdunit,
								u.nama_unit as namaunit, 
								ps.kd_Pasien as kdpasien,
								ps.nama  as namapasien,
								ps.alamat as alamatpas,
								k.Baru	,			
								case when ps.jenis_kelamin=  true then 'L' else 'P' end as jk, 
								case when date_part('year',age(ps.Tgl_Lahir))<=5 then
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn '
								else
									to_char(date_part('year',age(ps.Tgl_Lahir)), '999')||' thn'
								end
								as umur,
								case when k.Baru=true then 'X'  else ''  end as pasienbar,
								case when k.Baru=false then 'X'  else ''  end as pasienlama,
								pk.pekerjaan as pekerjaan, 
								prs.perusahaan as perusahaan,  
								case when k.kd_Rujukan=0 then '' else 'X' end as rujukan ,
								k.Jam_masuk as jammas,
								k.Tgl_masuk as tglmas,
								dr.nama as dokter, 
								c.customer as customer, 
								zu.user_names as username,
								getallkdpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as kd_diagnosa, 
								getallpenyakit(k.kd_pasien,k.kd_unit,k.tgl_masuk::date,k.urut_masuk) as penyakit
								From ((pasien ps left join perusahaan prs on prs.kd_perusahaan=ps.kd_perusahaan)  
										left join pekerjaan pk on ps.kd_pekerjaan=pk.kd_pekerjaan
									  )  
								inner join  (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit)   
									on k.Kd_Pasien = ps.Kd_Pasien  
								LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer 
								INNER JOIN DOKTER dr on k.kd_dokter=dr.kd_dokter INNER JOIN CUSTOMER c ON k.kd_customer = c.kd_customer  
								inner join transaksi t on t.kd_pasien = k.kd_pasien and t.kd_unit = k.kd_unit   
									and t.tgl_transaksi = k.tgl_masuk and k.urut_masuk = t.urut_masuk  
								left join zusers zu ON zu.kd_user = t.kd_user 
								where u.kd_unit ::integer in (".$f['kd_unit'].") and k.tgl_masuk between '".$tglsum."' and '".$tglsummax."' and k.Baru ='".$JenisPasien."' $Paramplus
									group by k.kd_unit,k.KD_PASIEN, u.Nama_Unit, ps.Kd_Pasien, ps.Nama , ps.Alamat, ps.jenis_kelamin, ps.Tgl_Lahir, ktr.jenis_cust,c.kd_customer, k.Baru, pk.pekerjaan, prs.perusahaan, k.kd_Rujukan ,k.Jam_masuk,k.Tgl_masuk,dr.nama,c.customer, 
								zu.user_names,k.urut_masuk
								Order By k.kd_unit, k.KD_PASIEN ";
				};
			$result = pg_query($query) or die('Query failed: ' . pg_last_error());
			$i=1;

			if(pg_num_rows($result) <= 0)
			{
				$tp	->addColumn("Data tidak ada", 15,"center")
					->commit("header");
			}else{
				 $tp ->addColumn($f['nama_unit'], 15,"left")
					 ->commit("header");
				while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
				{
					if($line['pasienbar']=='X'){
						$totBaru+=1;
					}
					if($line['pasienlama']=='X'){
						$totLama+=1;
					}
					
					if($line['rujukan'] == 'X'){	
						$totRujukan = $totRujukan+1 ;
					}
					
					$tmp_umur = explode ("thn",$line['umur']);
					$umur = (int)$tmp_umur[0];
					
					$tgl_masuk_tmp = $line['jammas'];
					$dt = new DateTime($tgl_masuk_tmp);
					// $time = date_format(date_create($dt), 'H:i');
					$time =date_format(date_create($line['jammas']), 'H:i:s');
					$tp	->addColumn(($i).".", 1)
						->addColumn($line['kdpasien'], 1,"left")
						->addColumn($line['namapasien'], 1,"left")
						->addColumn($line['alamatpas'], 1,"left")
						->addColumn($line['jk'], 1,"center")
						->addColumn($umur." thn", 1,"left")
						->addColumn($line['pasienbar'], 1,"center")
						->addColumn($line['pasienlama'], 1,"center")
						->addColumn($line['pekerjaan'], 1,"left")
						->addColumn(date('d-m-Y', strtotime($line['tglmas'])), 1,"left")
						->addColumn($line['rujukan'], 1,"center")
						->addColumn($time, 1,"left")
						->addColumn($line['kd_diagnosa'], 1,"left")
						->addColumn($line['penyakit'], 1,"left")
						->addColumn($line['dokter'], 1,"left")
						->addColumn($line['username'], 1,"left")
						->commit("header");
							
					$i++;	
				}
				$i--;
				$totalpasien += $i;
				$tp	->addColumn("Total Pasien Daftar di ".$f['nama_unit']." : ", 4)
					->addColumn($i, 1,"center")
					->addColumn("", 1,"center")
					->addColumn($totBaru, 1,"center")
					->addColumn($totLama, 1,"center")
					->addColumn("", 2,"center")
					->addColumn($totRujukan, 1,"center")
					->addColumn("", 5,"center")
					->commit("header"); 
				$tp	->addColumn("", 16,"center")
					->commit("header"); 
				$totBaru=0;
				$totLama=0;
				$totRujukan=0;
			}
        }
		 $tp ->addColumn("Total Keseluruhan Pasien", 3,"left")
			 ->addColumn(":", 1,"right")
			 ->addColumn($totalpasien, 12,"left")
			 ->commit("header");
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 7,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_register_detail_rwj.txt'; # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$fast = chr(27).chr(115).chr(1);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $fast;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
	}
	
}
?>