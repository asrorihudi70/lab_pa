<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


//class main extends Controller {
class lap_tindakan_perpasien extends  MX_Controller {		

    public $ErrLoginMsg='';
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('result');
		$this->load->library('common');
	}
	 
	public function index()
	{
		$this->load->view('main/index');
	} 
   
   public function preview(){
		$common      = $this->common;
		$result      = $this->result;
		$title       = 'LAPORAN TINDAKAN PERPASIEN';
		$param       = json_decode($_POST['data']);
		$html        = '';
		// $kd_unit     = $param->kd_unit;
		$kd_customer = $param->kd_customer;
		$tglAwal     = $param->tglAwal;
		$tglAkhir    = $param->tglAkhir;
		$type_file   = $param->type_file;
		$order_by    = $param->order_by;
		$arrayDataUnit = $param->poliklinik;
		$criteriaUnit="";
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$criteriaUnit 	= substr($criteriaUnit, 0, -1);
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
        if(strtolower($order_by) == strtolower("Medrec")  || $order_by == 0){
            $criteriaOrder = "ORDER BY t.kd_pasien ASC";
        }else if(strtolower($order_by) == strtolower("Nama Pasien")  || $order_by == 1){
            $criteriaOrder = "ORDER BY ps.nama ASC";
        }else if(strtolower($order_by) == strtolower("Penjamin")  || $order_by == 3){
            $criteriaOrder = "ORDER BY c.customer ASC";
        }else{
            $criteriaOrder = "ORDER BY t.tgl_transaksi ASC";
        }
		$ctgl = "where k.tgl_masuk >= '".date('Y-m-d',strtotime($tglAwal))."' and k.tgl_masuk <= '".date('Y-m-d',strtotime($tglAkhir))."'";
		/* if($kd_unit == 'Semua' || $kd_unit == ''){
			$cKdunit=" and left(u.parent,3) not in('100') and left(t.kd_unit,1) !='3'";
			// $cKdunit=" and left(u.parent,3) not in('100') and left(t.kd_unit,1) !='3'";
			// $cKdunit='';
			$unit = 'SEMUA POLIKLINIK';
			// echo "SEMUA";
		} else{
			$cKdunit="and t.kd_unit='".$kd_unit."'";
			$unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
		} */
		$cKdunit=" AND t.kd_unit in (".$criteriaUnit.")";
		$unitres = $this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit in (".$criteriaUnit.")")->result();
		$unit = '';
		for($i=0;$i<count($unitres);$i++){
			$unit.=$unitres[$i]->nama_unit.", ";
		}
		$unit = substr($unit, 0, -2);
		
		if($kd_customer == 'SEMUA' || $kd_customer==''){
			$cKdcustomer = '';
			$customer='SEMUA';
		} else{
			$cKdcustomer = "and k.kd_customer='".$kd_customer."'";
			$customer = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
		}
		
		$queryHead = $this->db->query( "SELECT distinct(t.kd_unit),u.nama_unit
										from detail_transaksi dt 
											inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
											inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
											inner join produk p on p.kd_produk=dt.kd_produk
											inner join pasien ps on ps.kd_pasien=k.kd_pasien
											inner join dokter d on d.kd_dokter=k.kd_dokter
											inner join unit u on u.kd_unit=t.kd_unit
											inner join customer c on c.kd_customer=k.kd_customer
										WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
										".$cKdunit.$cKdcustomer." 
										order by u.nama_unit");
		$queryHead = $queryHead->result();	
		//-------------JUDUL-----------------------------------------------
		if($type_file == 1){
			$font_style="font-size:16px";
		}else{
			$font_style="font-size:13px";
		}
		$html.='
			<table  class="t2" cellspacing="0" border="0">
				
					<tr>
						<th colspan="7" style='.$font_style.'>'.$title.'</th>
					</tr>
					<tr>
						<th colspan="7" style='.$font_style.'> Periode '.$awal.' s/d '.$akhir.'</th>
					</tr>
					<tr>
						<th colspan="7" style='.$font_style.'> Poli : '.$unit.'</th>
					</tr>
					<tr>
						<th colspan="7" style='.$font_style.'> Jenis Pasien : '.$customer.'</th>
					</tr>
				
			</table><br>';
			
		//---------------ISI-----------------------------------------------------------------------------------
		$html.='
			<table width="100" height="20" border = "1">
			
				 <tr>
					<th width="5" align="center" style='.$font_style.'>No</th>
					<th width="60" align="center" style='.$font_style.'>No. Medrec</th>
					<th width="80" align="center" style='.$font_style.'>Nama</th>
					<th width="40" align="center" style='.$font_style.'>Tgl. Transaksi</th>
					<th width="40" align="center" style='.$font_style.'>Customer</th>
					<th width="10" align="center" style='.$font_style.'>Deskripsi tindakan</th>
				  </tr>
			';
					// <td width="60" align="center" style='.$font_style.'>Unit</td>
		$no_set = 1;
		if(count($queryHead) > 0) {
			foreach($queryHead as $linehead){
				$html.='
						<tr>
							<th align="left" colspan="6">POLIKLINIK : '.str_replace("&","dan",$linehead->nama_unit).'</th>
						</tr>';
								
				$no=0;
				$query = $this->db->query( "SELECT distinct(t.kd_pasien),ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer, t.no_transaksi
										from detail_transaksi dt 
											inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
											inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
											inner join produk p on p.kd_produk=dt.kd_produk
											inner join pasien ps on ps.kd_pasien=k.kd_pasien
											inner join dokter d on d.kd_dokter=k.kd_dokter
											inner join unit u on u.kd_unit=t.kd_unit
											inner join customer c on c.kd_customer=k.kd_customer
										WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
										and t.kd_unit='".$linehead->kd_unit."'
										".$cKdcustomer." 
										".$criteriaOrder."")->result();
										// <td align="left">'.str_replace("&","dan",$line->nama_unit).'</td>
				foreach($query as $line){
					$no++;
					$html.='
								<tr>
									<td align="left">'.$no.'</td>
									<td align="left">'.$line->kd_pasien.'</td>
									<td align="left">'.$line->nama.'</td>
									<td align="left">'.tanggalstring($line->tgl_transaksi).'</td>
									<td align="left">'.$line->customer.'</td>
									<td></td>
								</tr>';
						/*$queryBody = $this->db->query( "SELECT p.deskripsi as desk 
								from detail_transaksi dt 
									inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
									inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
									inner join produk p on p.kd_produk=dt.kd_produk
									inner join pasien ps on ps.kd_pasien=k.kd_pasien
									inner join dokter d on d.kd_dokter=k.kd_dokter
									inner join unit u on u.kd_unit=t.kd_unit
									inner join customer c on c.kd_customer=k.kd_customer
								WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
								and t.kd_unit='".$line->kd_unit."' and k.kd_customer='".$line->kd_customer."' and t.kd_pasien='".$line->kd_pasien."'
								order by c.customer asc ");*/
						$queryBody = $this->db->query( "SELECT p.deskripsi as desk 
									from detail_transaksi dt 
									inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir 
									inner join produk p on p.kd_produk=dt.kd_produk 
								WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
								and t.kd_unit='".$line->kd_unit."' and t.no_transaksi='".$line->no_transaksi."' and t.kd_pasien='".$line->kd_pasien."'");
						$query2 = $queryBody;
					$no_set++;
					foreach($query2->result() as $line2){
						$tmpDesk = $line2->desk;
						$tmpDesk = str_replace("&","dan",$tmpDesk);
						$tmpDesk = str_replace("/"," atau ",$tmpDesk);
						$tmpDesk = str_replace(":"," sama ",$tmpDesk);
						$tmpDesk = str_replace(".",",",$tmpDesk);
						$tmpDesk = str_replace('&','dan',$tmpDesk);
						//$tmpDesk = str_replace('>','lebih dari',$tmpDesk);
						$tmpDesk = str_replace('<','kurang dari',$tmpDesk);
						$html.='<tr>
									<td align="left" colspan="5"></td>
									<td align="left">'.$tmpDesk.'</td>
								</tr>';
						$no_set++;
					}
				}
			}
		}else {		
			$html.='
				<tr class="headerrow"> 
					<td width="" colspan="7" align="center">Data tidak ada</td>
				</tr>

			';		
		} 
		
		$html.='</table>';
		// echo $html;
		$prop=array('foot'=>true);
		
		if($type_file == 1){
			$print_area	= 'A1:G'.((int)$no_set+6);
			$area_wrap	= 'A7:G'.((int)$no_set+6);
			# Lap. bentuk excel, format yang harus disesuaikan dengan jenis laporan : 
			# - Margin
			# - Type font bold
			# - Paragraph alignment
			# - Password protected
		
			
            $table    = $html;

            # save $table inside temporary file that will be deleted later
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $table);
		
			# Create object phpexcel
            $objPHPExcel     = new PHPExcel();
            
			# Fungsi untuk set print area
			$objPHPExcel->getActiveSheet()
                        ->getPageSetup()
                        ->setPrintArea($print_area);
			# END Fungsi untuk set print area			
						
			# Fungsi untuk set margin
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setTop(0);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setRight(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setLeft(0.1);
			$objPHPExcel->getActiveSheet()
						->getPageMargins()->setBottom(0.1);
			# END Fungsi untuk set margin
			
			# Fungsi untuk set font bold
			$styleArrayHead = array(
				'font'  => array(
					'bold'  => true,
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getStyle('A1:G6')->applyFromArray($styleArrayHead);
			$styleArrayBody = array(
				'font'  => array(
					'size'  => 10,
					'name'  => 'Courier New'
				));
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArrayBody);
			# END Fungsi untuk set bold
						
			# Fungsi untuk set alignment center
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle2 = new PHPExcel_Style();
			 
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	)/* ,
					'font'  => array(
						'size'  => 12,
						'name'  => 'Courier New'
					) */
			 	)
			);
			$sharedStyle2->applyFromArray(
				 array(
				 	'borders' => array(
						 // 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 // 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
						 // 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
				 	)/* ,
					'font'  => array(
						'size'  => 11,
						'name'  => 'Courier New'
					) */
			 	)
			);
			 
			
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle2, 'A1:G6');
			$objPHPExcel->getActiveSheet()
						->getStyle('A1:G4')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* $objPHPExcel->getActiveSheet()
						->getStyle('G7')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			# END Fungsi untuk set alignment center
			
			# Fungsi untuk set alignment right
			/* $objPHPExcel->getActiveSheet()
						->getStyle('G')
						->getAlignment()
						->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); */
			# END Fungsi untuk set alignment right
			$objPHPExcel->getActiveSheet()
							->getPageSetup()
							->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			# END Fungsi untuk set Orientation Paper 
			# Fungsi untuk protected sheet
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
			$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
			# Set Password
			$objPHPExcel->getActiveSheet()->getProtection()->setPassword('123456');
			# END fungsi protected
			/* $styleBorder = array(
				'borders' => array(
					'allborders' => array (
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('A7:G'.((int)$no_set+6))->applyFromArray($styleBorder); */
			$sharedStyle1 = new PHPExcel_Style();
			$sharedStyle1->applyFromArray(
				 array(
				 	'borders' => array(
						 'bottom' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'top' 		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'right' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
						 'left' 	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
				 	)/* ,
					'font'  => array(
						'size'  => 10,
						'name'  => 'Courier New'
					) */
			 	)
			);
			
			$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, $area_wrap);
			# Fungsi Autosize
           /*  for ($col = 'A'; $col != 'P'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            } */
			# END Fungsi Autosize
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(39);
			
			$textFormat='@';//'General','0.00','@';
			$objPHPExcel->getActiveSheet()
						->getStyle($print_area)
						->getNumberFormat()
					    ->setFormatCode($textFormat);

			$objPHPExcel->getActiveSheet()
					    ->getPageSetup()
					    ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

            $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
            $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
            $objPHPExcel->getActiveSheet()->setTitle('tindakan per pasien'); # Change sheet's title if you want

            unlink($tmpfile); # delete temporary file because it isn't needed anymore

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); # header for .xlxs file
            header('Content-Disposition: attachment;filename=laporan_tindakan_perpasien.xls'); # specify the download file name
            header('Cache-Control: max-age=0');

            # Creates a writer to output the $objPHPExcel's content
            $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $writer->save('php://output');
            exit;
		}else{
			$this->common->setPdf('L','Lap. Tindakan PerPasien',$html);	
		}
		echo $html;
   	}
	
	public function doprint(){
		ini_set('display_errors', '1');
		# GET DATA FROM JS
		$param=json_decode($_POST['data']);
		$kdUnit=$this->session->userdata['user_id']['aptkdunitfar'];
		$kd_user=$this->session->userdata['user_id']['id'];
		$user=$this->db->query("SELECT full_name FROM zusers WHERE kd_user='".$kd_user."'")->row()->full_name;
   		# Create Data
		$tp = new TableText(145,7,'',0,false);
		# SET HEADER 
		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
		$telp='';
		$fax='';
		if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
			$telp='Telp. ';
			$telp1=false;
			if($rs->phone1 != null && $rs->phone1 != ''){
				$telp1=true;
				$telp.=$rs->phone1;
			}
			if($rs->phone2 != null && $rs->phone2 != ''){
				if($telp1==true){
					$telp.='/'.$rs->phone2.'.';
				}else{
					$telp.=$rs->phone2.'.';
				}
			}else{
				$telp.='.';
			}
		}
		if($rs->fax != null && $rs->fax != ''){
			$fax='Fax. '.$rs->fax.'.';
				
		}
		
		// $kd_unit     = $param->kd_unit;
		$kd_customer = $param->kd_customer;
		$tglAwal     = $param->tglAwal;
		$tglAkhir    = $param->tglAkhir;
		$type_file   = $param->type_file;
		$order_by    = $param->order_by;
		$arrayDataUnit = $param->poliklinik;
		$criteriaUnit="";
		for ($i=0; $i < count($arrayDataUnit); $i++) { 
			$criteriaUnit .= "'".$arrayDataUnit[$i][0]."',";
		}
		$criteriaUnit 	= substr($criteriaUnit, 0, -1);
		
		$awal=tanggalstring(date('Y-m-d',strtotime($tglAwal)));
		$akhir=tanggalstring(date('Y-m-d',strtotime($tglAkhir)));
		
        if(strtolower($order_by) == strtolower("Medrec")  || $order_by == 0){
            $criteriaOrder = "ORDER BY t.kd_pasien ASC";
        }else if(strtolower($order_by) == strtolower("Nama Pasien")  || $order_by == 1){
            $criteriaOrder = "ORDER BY ps.nama ASC";
        }else if(strtolower($order_by) == strtolower("Penjamin")  || $order_by == 3){
            $criteriaOrder = "ORDER BY c.customer ASC";
        }else{
            $criteriaOrder = "ORDER BY t.tgl_transaksi ASC";
        }
		$ctgl = "where k.tgl_masuk >= '".date('Y-m-d',strtotime($tglAwal))."' and k.tgl_masuk <= '".date('Y-m-d',strtotime($tglAkhir))."'";
		// if($kd_unit == 'Semua' || $kd_unit == ''){
			// $cKdunit=" and left(u.parent,3) not in('100') and left(t.kd_unit,1) !='3'";
			// $unit = 'SEMUA POLIKLINIK';
		// } else{
			// $cKdunit="and t.kd_unit='".$kd_unit."'";
			// $unit = $this->db->query("select nama_unit from unit where kd_unit='".$kd_unit."'")->row()->nama_unit;
		// }
		$cKdunit=" AND t.kd_unit in (".$criteriaUnit.")";
		$unitres = $this->db->query("SELECT kd_unit,nama_unit from unit where kd_unit in (".$criteriaUnit.")")->result();
		$unit = '';
		for($i=0;$i<count($unitres);$i++){
			$unit.=$unitres[$i]->nama_unit.", ";
		}
		$unit = substr($unit, 0, -2);
		
		if($kd_customer == 'SEMUA' || $kd_customer==''){
			$cKdcustomer = '';
			$customer='SEMUA';
		} else{
			$cKdcustomer = "and k.kd_customer='".$kd_customer."'";
			$customer = $this->db->query("select customer from customer where kd_customer='".$kd_customer."'")->row()->customer;
		}
		
		$qHead = $this->db->query( "SELECT distinct(t.kd_unit),u.nama_unit
										from detail_transaksi dt 
											inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
											inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
											inner join produk p on p.kd_produk=dt.kd_produk
											inner join pasien ps on ps.kd_pasien=k.kd_pasien
											inner join dokter d on d.kd_dokter=k.kd_dokter
											inner join unit u on u.kd_unit=t.kd_unit
											inner join customer c on c.kd_customer=k.kd_customer
										WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
										".$cKdunit.$cKdcustomer." 
										order by u.nama_unit");
		$queryHead = $qHead->result();	
		
		
		# SET JUMLAH KOLOM BODY
		$tp->setColumnLength(0, 5)
			->setColumnLength(1, 10)
			->setColumnLength(2, 30)
			->setColumnLength(3, 17)
			->setColumnLength(4, 20)
			->setColumnLength(5, 20)
			->setColumnLength(6, 25)
			->setUseBodySpace(true);
			
		# SET HEADER REPORT
		$tp	->addSpace("header")
			->addColumn($rs->name, 7,"left")
			->commit("header")
			->addColumn($rs->address.", ".$rs->city, 7,"left")
			->commit("header")
			->addColumn($telp, 7,"left")
			->commit("header")
			->addColumn($fax, 7,"left")
			->commit("header")
			->addColumn("LAPORAN TINDAKAN PERPASIEN", 7,"center")
			->commit("header")
			->addColumn("PERIODE ".$awal." s/d ".$akhir, 7,"center")
			->commit("header")
			->addColumn ( "POLI : ".$unit, 7,"center")
			->commit("header")
			->addColumn ( "JENIS PASIEN : ".$customer, 7,"center")
			->commit("header")
			->addSpace("header");
		
		$tp	->addColumn("NO.", 1,"center")# (NAMA_HEADER,JML_COLSPAN,ALIGN_HEADER)
			->addColumn("NO. MEDREC", 1,"left")
			->addColumn("NAMA", 1,"left")
			->addColumn("TGL. TRANSAKSI", 1,"left")
			// ->addColumn("UNIT", 1,"left")
			->addColumn("CUSTOMER", 1,"left")
			->addColumn("DESKRIPSI TINDAKAN", 1,"left")
			->commit("header");
		$no_set = 1;
		if(count($queryHead) > 0) {
			foreach($queryHead as $linehead){
				$tp	->addColumn("========= POLIKLINIK ".$linehead->nama_unit." =========", 6,"left")
					->commit("header");
				$query = $this->db->query( "SELECT distinct(t.kd_pasien),ps.nama,t.tgl_transaksi,t.kd_unit,u.nama_unit,k.kd_dokter,d.nama as nama_dokter,k.kd_customer,c.customer, t.no_transaksi
											from detail_transaksi dt 
												inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir
												inner join kunjungan k on k.kd_pasien=t.kd_pasien and k.tgl_masuk=t.tgl_transaksi and k.urut_masuk=t.urut_masuk and k.kd_unit=t.kd_unit
												inner join produk p on p.kd_produk=dt.kd_produk
												inner join pasien ps on ps.kd_pasien=k.kd_pasien
												inner join dokter d on d.kd_dokter=k.kd_dokter
												inner join unit u on u.kd_unit=t.kd_unit
												inner join customer c on c.kd_customer=k.kd_customer
											WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
											and t.kd_unit='".$linehead->kd_unit."'
											".$cKdcustomer." 
											".$criteriaOrder."")->result();
				$no=0;
				foreach($query as $line){
					$no++;
					$tp	->addColumn($no.".", 1,"left")
						->addColumn($line->kd_pasien, 1,"left")
						->addColumn($line->nama, 1,"left")
						->addColumn(tanggalstring($line->tgl_transaksi), 1,"left")
						// ->addColumn($line->nama_unit, 1,"left")
						->addColumn($line->customer, 1,"left")
						->commit("header");
						$queryBody = $this->db->query( "SELECT p.deskripsi as desk 
									from detail_transaksi dt 
									inner join transaksi t on t.no_transaksi=dt.no_transaksi and dt.kd_kasir=t.kd_kasir 
									inner join produk p on p.kd_produk=dt.kd_produk 
								WHERE t.tgl_transaksi >= '".$tglAwal."' and t.tgl_transaksi <= '".$tglAkhir."'
								and t.kd_unit='".$line->kd_unit."' and t.no_transaksi='".$line->no_transaksi."'  and t.kd_pasien='".$line->kd_pasien."'");
						$query2 = $queryBody;
					$no_set++;
					foreach($query2->result() as $line2){
						$tp	->addColumn("", 5,"left")
							->addColumn($line2->desk, 1,"left")
							->commit("header");
							$no_set++;
					}
				}
			}
		}else {		
			$tp	->addColumn("Data tidak ada", 6,"left")
				->commit("header");	
		} 
		
		# SET FOOTER
		date_default_timezone_set("Asia/Jakarta");		
		$tp	->addLine("footer")
			->addColumn("Operator : ".$user, 3,"left")
			->addColumn(tanggalstring(date('Y-m-d'))." ".gmdate(" H:i:s", time()+60*60*7), 3,"center")
			->addLine("footer")
			->commit("footer");

		$data = $tp->getText();
		/* echo $data; */
		# End Data
		
		$file =  '/home/tmp/lap_tindakan_perpasien_rwj.txt';  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27) . Chr(33) . Chr(4);
		$feed = chr(27) . chr(67) . chr(20); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx)
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris
		$formfeed = chr(12); # mengeksekusi $feed
		$bold1 = Chr(27) . Chr(69); # Teks bold
		$bold0 = Chr(27) . Chr(70); # mengapus Teks bold
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$margin = chr(27) . chr(78). chr(90);
		$margin_off = chr(27) . chr(79);

		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $feed;
		$Data .= $margin;
		$Data .= $data;
		$Data .= $margin_off;

		fwrite($handle, $Data);
		fclose($handle);
		$printer=$this->db->query("select p_bill from zusers where kd_user='".$kd_user."'")->row()->p_bill;//'EPSON-LX-310-ME';
		shell_exec("lpr -P " . $printer . " " . $file);
   	}
}
?>