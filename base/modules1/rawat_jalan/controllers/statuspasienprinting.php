<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class statuspasienprinting extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
    $this->load->view('main/index');
    }
    
    public function save($Params=NULL)
    {
		$bataskertas= new Pilihkertas;
		$kdpasien = $Params['NoMedrec'];
		$namapasien = $Params['NamaPasien'];
		$alamat = $Params['Alamat'];
        $kdunit = $Params['Poli'];
		$tglmasuk = $Params['TanggalMasuk'];
		$jammasuk = date('Y-m-d h:i:s');
		$kddokter = $Params['KdDokter'];

		//$unit = $this->db->query("SELECT NAMA_UNIT FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
		//$dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;
					
       

	/*  $data = array(
	 "kd_pasien"=>$kdpasien,
	 "nama_pasien"=>$namapasien,
	 "nama_unit"=>$unit,
	 "nama_dokter"=>$dokter,
	 "tgl_masuk"=>$tglmasuk,
	 "jam_masuk"=>$jammasuk,
	 "alamat"=>$alamat
	 ); */
	 
/* $this->load->model('rawat_jalan/tbltracer');	   
  $save = $this->tbltracer->save($data); */
	 
//AWAL SCRIPT PRINT

			 
		$sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = 'f' THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' ");
		/* $sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' "); */
		if($sql->num_rows == 0)
		{
			$nama = '1';
			$kd_pasien = '2';
			$tgl_masuk = '3';
			$kota = '5';
			$nama_ortu = '6';
			$penjamin = '7';
			$jenis_kelamin = '8';
			$status_marita='9';
			$tgl_lahir = '10';
			$dokter = '11';
			$alamat = '12';
			$customer = '13';
			$jam = '14';
			$baru = '15';
			$umur = '16';
			$tlp = '17';
			$pekerjaan='18';
		}
		 else {
			

			foreach($sql->result() as $row)
			{
				$nama = $row->nama;
				$kd_pasien = $row->kd_pasien;
				$tgl_masuk = $row->tgl_masuk;
				$kota = $row->kota;
				$nama_ortu = $row->nama_keluarga;
				$penjamin = $row->customer;
				$jenis_kelamin = '';
				$status_marita='';
				if ($row->jenis_kelamin=='t')
				{
					$jenis_kelamin='Laki-laki';
				}else
				{
					$jenis_kelamin='Perempuan';
				}
				
				
				if ($row->status_marita==0)
				{
					$status_marita='Blm Menikah';
				}else if ($row->status_marita==1)
				{
					$status_marita='Menikah';
				}else if ($row->status_marita==2)
				{
					$status_marita='Janda';
				}else if ($row->status_marita==3)
				{
					$status_marita='Duda';
				}
				
				$tgl_lahir =date("d-m-Y",strtotime($row->tgl_lahir));
				//$dokter = $row->nama_dokter;
				$alamat = $row->alamat;
				$customer = $row->customer;
				$baru = $row->baru;
				$umur = '0 years';
				
				$strQuery = "select age(timestamp '".date("Y-m-d",strtotime($row->tgl_lahir))."')";
				$query = $this->db->query($strQuery);
				$res = $query->result();
				 if ($query->num_rows() > 0)
				 {
					 foreach($res as $data)
					 {
						$pisah = explode(" ",$data->age);
						$umur = $pisah[0]." TAHUN";
					 }
					// echo $umur;
				 }
				 $tlp='--';
				 if ($row->TELEPON=='')
				 {
					 $tlp='--';
				 }
				 else
				 {
					 $tlp=$row->TELEPON;
				 }
				 $pekerjaan=$row->PEKERJAAN;
			 }
			 
		//$printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_status_pasien'")->row()->setting;
		$printer = $this->db->query("SELECT p_statuspasien FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_statuspasien;
			
		$waktu = explode(" ",$tgl_masuk);
		$tanggal = $waktu[0];
		//$jam = $waktu[1];


		$x = 0;
		$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$feed=$bataskertas->PageLength('laporan');
		//$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
		$condensed = Chr(27).Chr(33).Chr(4);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		//$size1 = Chr(27) . Chr(80);
		$bold1 = Chr(27) . Chr(69);
		$bold0 = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1 = chr(20);
		$condensed0 = chr(15);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		$size0 = Chr(27).Chr(109);
		$Data  = $initialized;
		$Data .= $feed;
		$Data .= $condensed1;
		/* 
		$Data .= chr(27) . chr(67) . chr(70);
		$Data .= chr(27) . chr(87) . chr(49);
		$Data .= chr(27).chr(77); */
		//$Data .= "".$bold1."INSTALASI REKAM MEDIK".$bold0.""."\n"; $x++;

		$Data .= "\n";$x++;
		$Data .= "			        ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		// $Data .= "No. Medrec	: ".$bold1.$kd_pasien.$bold0." \n";$x++;
		$Data .= "No. Medrec	: ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		$Data .= "Nama Pasien	: ".$nama." \n";$x++;
		$Data .= "Tgl. Lahir/Umur	: ".$tgl_lahir."/".$umur." \n";$x++;
		$Data .= "Jenis Kelamin	: ".$jenis_kelamin." Status : ".$status_marita." \n";$x++;
		$tmp = $tmp = str_split($alamat,25);
		$Data .= "Alamat Lengkap	: ";$x++;
		for ($str = 0; $str < count($tmp); $str++)
		{
			if ($str==0)
			{
				$Data .= $tmp[$str]."\n";$x++;
			}
			else
			{
				$Data .= "	    	  ".$tmp[$str]."\n";$x++;
			}
		}
		//$Data .= "Alamat Lengkap  	: ".$alamat." \n";$x++;
		$Data .= "No. Telp	: ".$tlp." \n";$x++;
		$Data .= "Pekerjaan	: ".$pekerjaan." \n";$x++;
		$Data .= "Nama Orang Tua	: ".$nama_ortu." \n";$x++;
		$Data .= "Penjamin	: ".$customer." \n";$x++;
		/* $Data .= "\n";$x++;
		$Data .= $unit." / ".$customer."\n";$x++;
		$Data .= $dokter."\n";$x++;
		$Data .= "\n";$x++;
		$Data .= $tanggal." ".$jam."\n";$x++; */


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;

		$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
		if($counter == '7')
		{
		   $Data .=''; 
		   $newVal = 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}
		else
		{
		$Data .= "\n";
		$Data .= "\n";
		$Data .= "\n";
			$newVal = (int)$counter + 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}

		if ($x < 17)
		{
			$y = $x-17;
			for ($z = 1; $z<=$y;$z++)
			{
				$Data .= "\n";
			}
		}
		$Data .= $formfeed;
		$Data .= $size0;
		fwrite($handle, $Data);
		fclose($handle);
		//echo $printer;
		$print = shell_exec("lpr -P ".$printer." -r ".$file);  # Lakukan cetak
//copy($file, $printer);  # Lakukan cetak

//AKHIR	 	 
	   
	 /*  if($save)
	   { */
		  
		   echo "{success:true}";
	   /* }
	   else
	   {
		   echo "{success:false}";
	   } */
	 
	 

    }
    
   
    
   
            
}
     public function save_LAMA($Params=NULL)
    {
		$bataskertas= new Pilihkertas;
		$kdpasien = $Params['NoMedrec'];
		$namapasien = $Params['NamaPasien'];
		$alamat = $Params['Alamat'];
        $kdunit = $Params['Poli'];
		$tglmasuk = $Params['TanggalMasuk'];
		$jammasuk = date('Y-m-d h:i:s');
		$kddokter = $Params['KdDokter'];

		//$unit = $this->db->query("SELECT NAMA_UNIT FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
		//$dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;
					
       

	/*  $data = array(
	 "kd_pasien"=>$kdpasien,
	 "nama_pasien"=>$namapasien,
	 "nama_unit"=>$unit,
	 "nama_dokter"=>$dokter,
	 "tgl_masuk"=>$tglmasuk,
	 "jam_masuk"=>$jammasuk,
	 "alamat"=>$alamat
	 ); */
	 
/* $this->load->model('rawat_jalan/tbltracer');	   
  $save = $this->tbltracer->save($data); */
	 
//AWAL SCRIPT PRINT

			 
		$sql = _QMS_query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, 
			CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' ");
		/* $sql = $this->db->query("select t.*, k.KD_RUJUKAN,pek.PEKERJAAN,c.KD_CUSTOMER, c.CUSTOMER, k.TGL_MASUK, k.JAM_MASUK, CASE WHEN k.BARU = 0 THEN '(B)' ELSE '' END as BARU
								from pasien t
								INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien
								INNER JOIN pekerjaan pek on pek.kd_pekerjaan = t.kd_pekerjaan
								INNER JOIN customer c on k.kd_customer = c.kd_customer 
								WHERE t.KD_PASIEN = '".$kdpasien."' And k.tgl_masuk = '".$tglmasuk."' "); */
		if($sql->num_rows == 0)
		{
			$nama = '1';
			$kd_pasien = '2';
			$tgl_masuk = '3';
			$kota = '5';
			$nama_ortu = '6';
			$penjamin = '7';
			$jenis_kelamin = '8';
			$status_marita='9';
			$tgl_lahir = '10';
			$dokter = '11';
			$alamat = '12';
			$customer = '13';
			$jam = '14';
			$baru = '15';
			$umur = '16';
			$tlp = '17';
			$pekerjaan='18';
		}
		 else {
			

			foreach($sql->result() as $row)
			{
				$nama = $row->NAMA;
				$kd_pasien = $row->KD_PASIEN;
				$tgl_masuk = $row->TGL_MASUK;
				$kota = $row->KOTA;
				$nama_ortu = $row->NAMA_KELUARGA;
				$penjamin = $row->CUSTOMER;
				$jenis_kelamin = '';
				$status_marita='';
				if ($row->JENIS_KELAMIN==1)
				{
					$jenis_kelamin='Laki-laki';
				}else
				{
					$jenis_kelamin='Perempuan';
				}
				
				
				if ($row->STATUS_MARITA==0)
				{
					$status_marita='Blm Menikah';
				}else if ($row->STATUS_MARITA==1)
				{
					$status_marita='Menikah';
				}else if ($row->STATUS_MARITA==2)
				{
					$status_marita='Janda';
				}else if ($row->STATUS_MARITA==3)
				{
					$status_marita='Duda';
				}
				
				$tgl_lahir =date("d-m-Y",strtotime($row->TGL_LAHIR));
				//$dokter = $row->nama_dokter;
				$alamat = $row->ALAMAT;
				$customer = $row->CUSTOMER;
				$baru = $row->BARU;
				$umur = '0 years';
				
				$strQuery = "select age(timestamp '".date("Y-m-d",strtotime($row->TGL_LAHIR))."')";
				$query = $this->db->query($strQuery);
				$res = $query->result();
				 if ($query->num_rows() > 0)
				 {
					 foreach($res as $data)
					 {
						$pisah = explode(" ",$data->age);
						$umur = $pisah[0]." TAHUN";
					 }
					// echo $umur;
				 }
				 $tlp='--';
				 if ($row->TELEPON=='')
				 {
					 $tlp='--';
				 }
				 else
				 {
					 $tlp=$row->TELEPON;
				 }
				 $pekerjaan=$row->PEKERJAAN;
			 }
			 
		//$printer = $this->db->query("select setting from sys_setting where key_data = 'igd_default_printer_status_pasien'")->row()->setting;
		$printer = $this->db->query("SELECT p_statuspasien FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row()->p_statuspasien;
			
		$waktu = explode(" ",$tgl_masuk);
		$tanggal = $waktu[0];
		//$jam = $waktu[1];


		$x = 0;
		$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$feed=$bataskertas->PageLength('laporan');
		//$feed = chr(27) . chr(67) . chr(10); # menentuka panjang halaman setelah cetak selesai chr(27) . chr(67) . chr(xx) //TAMBAHAN
		$reversefeed = chr(27).chr(106).chr(4320); # load halaman keatas sebelum printing dimulai chr(27).chr(106).chr(xx) / xx/216 => 216/216= 1 baris //TAMBAHAN
		$formfeed = chr(12); # mengeksekusi $feed //TAMBAHAN
		$condensed = Chr(27).Chr(33).Chr(4);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		//$size1 = Chr(27) . Chr(80);
		$bold1 = Chr(27) . Chr(69);
		$bold0 = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1 = chr(20);
		$condensed0 = chr(15);
		$condensed2 = Chr(27).Chr(33).Chr(32);
		$size0 = Chr(27).Chr(109);
		$Data  = $initialized;
		$Data .= $feed;
		$Data .= $condensed1;
		/* 
		$Data .= chr(27) . chr(67) . chr(70);
		$Data .= chr(27) . chr(87) . chr(49);
		$Data .= chr(27).chr(77); */
		//$Data .= "".$bold1."INSTALASI REKAM MEDIK".$bold0.""."\n"; $x++;

		$Data .= "\n";$x++;
		$Data .= "			        ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		// $Data .= "No. Medrec	: ".$bold1.$kd_pasien.$bold0." \n";$x++;
		$Data .= "No. Medrec	: ".$condensed2.$bold1.$kd_pasien.$bold0.$condensed." \n";$x++;
		$Data .= chr(27) . chr(87) . chr(48);
        $Data .= chr(27) . chr(33) . chr(8);
		$Data .= "Nama Pasien	: ".$nama." \n";$x++;
		$Data .= "Tgl. Lahir/Umur	: ".$tgl_lahir."/".$umur." \n";$x++;
		$Data .= "Jenis Kelamin	: ".$jenis_kelamin." Status : ".$status_marita." \n";$x++;
		$tmp = $tmp = str_split($alamat,25);
		$Data .= "Alamat Lengkap	: ";$x++;
		for ($str = 0; $str < count($tmp); $str++)
		{
			if ($str==0)
			{
				$Data .= $tmp[$str]."\n";$x++;
			}
			else
			{
				$Data .= "	    	  ".$tmp[$str]."\n";$x++;
			}
		}
		//$Data .= "Alamat Lengkap  	: ".$alamat." \n";$x++;
		$Data .= "No. Telp	: ".$tlp." \n";$x++;
		$Data .= "Pekerjaan	: ".$pekerjaan." \n";$x++;
		$Data .= "Nama Orang Tua	: ".$nama_ortu." \n";$x++;
		$Data .= "Penjamin	: ".$customer." \n";$x++;
		/* $Data .= "\n";$x++;
		$Data .= $unit." / ".$customer."\n";$x++;
		$Data .= $dokter."\n";$x++;
		$Data .= "\n";$x++;
		$Data .= $tanggal." ".$jam."\n";$x++; */


		$Data .= "\n";$x++;
		$Data .= "\n";$x++;

		$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
		if($counter == '7')
		{
		   $Data .=''; 
		   $newVal = 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}
		else
		{
		$Data .= "\n";
		$Data .= "\n";
		$Data .= "\n";
			$newVal = (int)$counter + 1; 
		   $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}

		if ($x < 17)
		{
			$y = $x-17;
			for ($z = 1; $z<=$y;$z++)
			{
				$Data .= "\n";
			}
		}
		$Data .= $formfeed;
		$Data .= $size0;
		fwrite($handle, $Data);
		fclose($handle);
		//echo $printer;
		$print = shell_exec("lpr -P ".$printer." -r ".$file);  # Lakukan cetak
//copy($file, $printer);  # Lakukan cetak

//AKHIR	 	 
	   
	 /*  if($save)
	   { */
		  
		   echo "{success:true}";
	   /* }
	   else
	   {
		   echo "{success:false}";
	   } */
	 
	 

    }
    
   
    
   
            
}
    
   
    
   
            
}




