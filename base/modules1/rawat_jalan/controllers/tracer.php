<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class tracer extends MX_Controller
{
    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }
    public function index(){
		$this->load->view('main/index');
    }
    public function save($Params=NULL){
		$this->load->model('general/tb_dbrs');
		$query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
		if($query[1] != 0) {
			$NameRS = $query[0][0]->NAME;
			$Address = $query[0][0]->ADDRESS;
			$TLP = $query[0][0]->PHONE1;
			$Kota = $query[0][0]->CITY;
		}else{
			$NameRS = "";
			$Address = "";
			$TLP = "";
		}
        $kdpasien = $_POST['NoMedrec'];
        $namapasien = $_POST['NamaPasien'];
        $alamat = $_POST['Alamat'];
        $tmpkdunit = $_POST['Poli'];
        $tglmasuk = date('Y-m-d');
        $jammasuk = date('Y-m-d h:i:s');
        $kddokter = $_POST['KdDokter'];
        $kdunit = str_replace(" ","",$tmpkdunit);
        $unit = $this->db->query("SELECT NAMA_UNIT as nama_unit FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
        // echo "SELECT NAMA_UNIT as nama_unit FROM UNIT WHERE KD_UNIT = '".$kdunit."'";
        $dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;

        $sqldatasrv="SELECT count(KD_PASIEN) as jumlah FROM KUNJUNGAN WHERE KD_PASIEN = '".$kdpasien."' and  KD_UNIT =  '".$kdunit."' ";
        $dbsqlsrv = $this->load->database('otherdb2',TRUE);
        $res = $this->db->query($sqldatasrv);
        
        foreach ($res->result() as $rec)
        {
          $jumlah=$rec->jumlah;// + 1;
        }
                        
           

      $data = array(
      "kd_pasien"=>$kdpasien,
      "nama_pasien"=>$namapasien,
      "nama_unit"=>$unit,
      "nama_dokter"=>$dokter,
      "tgl_masuk"=>$tglmasuk,
      "jam_masuk"=>$jammasuk,
      "alamat"=>$alamat
      );
      // Print_r($data);
        $this->load->model('rawat_jalan/tbltracer');    
      $save = $this->tbltracer->save($data);
         
    // //AWAL SCRIPT PRINT

      $sql = $this->db->query("select t.*, u.kd_unit, k.kd_rujukan, c.customer, k.jam_masuk, CASE WHEN k.baru = 't' THEN '(B)' ELSE '' END as baru,
                            no_antrian,age(p.tgl_lahir) as tgl_lahir
                            from tracer t
                            INNER JOIN unit u on t.nama_unit = u.nama_unit
                            INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien and t.tgl_masuk = k.tgl_masuk and u.kd_unit = k.kd_unit
                            inner join pasien p on p.kd_pasien = t.kd_pasien
                            INNER JOIN customer c on k.kd_customer = c.kd_customer
                            INNER JOIN antrian_poliklinik ap on ap.kd_pasien = t.kd_pasien and ap.tgl_transaksi = t.tgl_masuk and u.kd_unit = ap.kd_unit
                            WHERE t.KD_PASIEN = '".$kdpasien."' And k.kd_unit = '".$kdunit."' And t.tgl_masuk = '".$tglmasuk."' ");
    $sqlUrut=$this->db->query("SELECT no_urut_cetak FROM tracer WHERE kd_pasien='".$kdpasien."' and nama_unit='".$unit."' and tgl_masuk='".$tglmasuk."'")->row()->no_urut_cetak;
	if($sql->num_rows == 0)
    {
     $nama = '1';
     $kd_pasien = '2';
     $tgl_masuk = '3';
     $unit = '4';
     $dokter = '5';
     $alamat = '6';
    $customer = '7';
    $jam = '8';
    $baru = '9';
        
    }
     else {
        
    $tmpumur = '';
    foreach($sql->result() as $row)
    {
     $nama = $row->nama_pasien;
     $kd_pasien = $row->kd_pasien;
     $tgl_masuk = $row->tgl_masuk;
     $unit = $row->nama_unit;
     $dokter = $row->nama_dokter;
     $alamat = $row->alamat;
      $customer = $row->customer;
      $jam = substr($row->jam_masuk,11);
      $baru = $row->baru;
      $antrian = $row->no_antrian;
      $Split1 = explode(" ", $row->tgl_lahir, 6);
      if (count($Split1) == 6)
      {
          $tmp1 = $Split1[0];
          $tmp2 = $Split1[1];
          $tmp3 = $Split1[2];
          $tmp4 = $Split1[3];
          $tmp5 = $Split1[4];
          $tmp6 = $Split1[5];
          $tmpumur = $tmp1.'th';
      }else if (count($Split1) == 4)
      {
          $tmp1 = $Split1[0];
          $tmp2 = $Split1[1];
          $tmp3 = $Split1[2];
          $tmp4 = $Split1[3];
          if ($tmp2 == 'years')
          {
             $tmpumur = $tmp1.'th';
          }else if ($tmp2 == 'mon')
          {
             $tmpumur = $tmp1.'bl';
          }
          else if ($tmp2 == 'days')
          {
             $tmpumur = $tmp1.'hr';
          }

      }  else {
          $tmp1 = $Split1[0];
          $tmp2 = $Split1[1];
          if ($tmp2 == 'years')
          {
             $tmpumur = $tmp1.'th';
          }else if ($tmp2 == 'mons')
          {
             $tmpumur = $tmp1.'bl';
          }
          else if ($tmp2 == 'days')
          {
             $tmpumur = $tmp1.'hr';
          }
      }
    }
     }
    $printer = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_printer_tracer'")->row()->setting;

        
    $waktu = explode(" ",$tgl_masuk);
    $tgl = $waktu[0];
    $date=date_create($tgl);
    $tanggal = date_format($date,"d/m/Y");

    $t1 = 4;
    $t3 = 20;
    $t2 = 63 - ($t3 + $t1);
    $x = 0;
    $x = 0;
    $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
    $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
    $handle = fopen($file, 'w');
    $condensed = Chr(27).Chr(33).Chr(4);
    $condensed2 = Chr(27).Chr(33).Chr(8);
    $bold1 = Chr(27) . Chr(69);
    $bold0 = Chr(27) . Chr(70);
    $initialized = chr(27).chr(64);
    $condensed1 = chr(15);
    $condensed0 = chr(18);
    $size0 = Chr(27).Chr(109);
    $Data  = $initialized;
    $Data .= $condensed1;
    $Data .= $condensed;
    $Data .= $condensed.str_pad($bold1.$NameRS.$bold0,64," ",STR_PAD_BOTH)."\n";$x++;
    $Data .= $condensed.str_pad($bold1.$Address.$bold0,64," ",STR_PAD_BOTH)."\n";$x++;
    $Data .= $condensed.str_pad($bold1.$TLP.$bold0,64," ",STR_PAD_BOTH)."\n";$x++;
    $Data .= $condensed."----------------------------------------------------------------\n"; $x++;
    $Data .= $condensed.str_pad($bold1."TRACER".$bold0,64," ",STR_PAD_BOTH)."\n";
    $Data .= $condensed."----------------------------------------------------------------\n"; $x++; //64
	$Data .= $condensed."No. Antrian Cetak: " .$condensed2.$sqlUrut."\n";$x++;
	$Data .= $condensed."No. MR           : " .$condensed2.$kd_pasien."\n";$x++;
    $Data .= $condensed."Nama             : " .$condensed2.$namapasien." ".$baru."\n";$x++;
    $Data .= $condensed."Usia             : " .$condensed2.$tmpumur."\n";$x++;
    $Data .= $condensed."Poliklinik       : " .$condensed2.$unit."\n";$x++;
   // $Data .= $condensed."No. Antrian      : " .$condensed2.$antrian."\n";$x++;
    $Data .= $condensed."Kelompok Pasien  : " .$condensed2.$customer."\n";$x++;
    $Data .= $condensed."Tgl Periksa      : " .$condensed2.$tanggal."\n";$x++;
    $Data .= $condensed."Jumlah Kunjungan : " .$condensed2.$jumlah."\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= $condensed."      Pengirim                           " & $condensed."     Penerima       ";
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= $condensed."(___________________)                    " & $condensed."(___________________)";
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;
    $Data .= "\n";$x++;

    $counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
    if($counter == '7')
    {
       $Data .=''; 
       $newVal = 1; 
       $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
    }
    else
    {
    $Data .= "\n";
    $Data .= "\n";
    $Data .= "\n";
        $newVal = (int)$counter + 1; 
       $update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
    }

    if ($x < 17)
    {
        $y = $x-17;
        for ($z = 1; $z<=$y;$z++)
        {
            $Data .= "\n";
        }
    }


    fwrite($handle, $Data);
    fclose($handle);
    $print = shell_exec("lpr -P ".$printer." -r ".$file);
           // echo $printer;
       if($save)
        {
              
            echo "{success:true}";
        }
        else
        {
            echo "{success:false}";
        }
    }
	
	public function gettracerpencarianmedrec(){
		$result = $this->db->query("select no_urut,'X-XX-XX-'|| awal || ' - ' || 'X-XX-XX-'|| akhir as value from tracer_pencarian order by no_urut")->result();
		
		echo '{success:true, totalrecords:'.count($result).', listData:'.json_encode($result).'}';
	}
	
	public function getmedrecgrid(){
		$db = $this->load->database('otherdb2',TRUE);
		if($_POST['status'] == ''){
			$status = "";
		} else{
			$status = $_POST['status'];
		}
		
		if($_POST['kd_unit'] == ''){
			$kd_unit = "";
		} else{
			$kd_unit = " and k.kd_unit = '".$_POST['kd_unit']."'";
		}
		
		$rangeawalmedrec = $this->db->query("Select * from tracer_pencarian where no_urut=".$_POST['no_urut']."")->row();
		
		$result = $db->query("select distinct  ps.ALAMAT,ps.Kd_Pasien, ps.nama, u.NAMA_UNIT, u.KD_UNIT, k.TGL_MASUK, k.JAM_MASUK, Umur=Datediff(YEAR,ps.Tgl_Lahir,k.Tgl_Masuk), c.customer ,0 as  STS_PRINT,AP.id_status_antrian,AP.STS_DITEMUKAN,d.KD_DOKTER,AP.NO_ANTRIAN
            From pasien ps 
            inner join (Kunjungan k inner join unit u on k.kd_unit=u.kd_unit) on k.Kd_Pasien = ps.Kd_Pasien 
            inner join DOKTER d on d.KD_DOKTER = k.KD_DOKTER 
            inner join customer c on c.kd_customer = k.kd_customer 
            INNER JOIN ANTRIAN_POLIKLINIK AP ON AP.kd_unit = k.kd_unit and cast(cast(ap.TGL_TRANSAKSI as varchar(12))as datetime) = k.tgl_masuk and ap.kd_pasien = k.kd_pasien Where
            k.Tgl_Masuk = '".date('Y-m-d')."' and k.baru = 0  ".$kd_unit.$status." and right(k.kd_pasien,2) between '".$rangeawalmedrec->awal."' and '".$rangeawalmedrec->akhir."'
			Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc")->result();
		
		$urutan = 1;
		$o=array();
        for($i=0;$i<count($result);$i++)
        {
          
          $o[$i]['MEDREC']=$result[$i]->Kd_Pasien;
          $o[$i]['PASIEN']=$result[$i]->nama;
          $o[$i]['UNIT']=$result[$i]->NAMA_UNIT;
          $o[$i]['KD_UNIT']=$result[$i]->KD_UNIT;
          $o[$i]['TANGGAL']=date("d-M-Y", strtotime($result[$i]->TGL_MASUK));
          $o[$i]['POLI']=$result[$i]->JAM_MASUK;
          $o[$i]['UMUR']=$result[$i]->Umur;
          $o[$i]['CUSTOMER']=$result[$i]->customer;
          $o[$i]['PRINT']=$result[$i]->STS_PRINT;
          if($result[$i]->NO_ANTRIAN != NULL)
          {
            $o[$i]['NO_ANTRI']=$result[$i]->NO_ANTRIAN;
          }else{
            $o[$i]['NO_ANTRI']=0;
          }
          
          if($result[$i]->STS_DITEMUKAN === 0){
            $o[$i]['DITEMUKAN']= false;
          }else{
            $o[$i]['DITEMUKAN']= true;
          }
          $o[$i]['DOKTER'] = $result[$i]->KD_DOKTER;
          $o[$i]['ALAMAT'] = $result[$i]->ALAMAT;
          $o[$i]['URUTAN'] = str_pad($urutan, 5, "0", STR_PAD_LEFT);
        }

        echo '{success:true, totalrecords:' . count($result) . ', ListDataObj:' . json_encode($o) . '}';	
	}
	
}
    
//    
   
   

//     }