<?php
class tracerprinting extends MX_Controller{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
		$this->load->view('main/index');
    }
    public function save($Params=NULL){
		$db = $this->load->database('otherdb2',TRUE);
    	$this->load->model('general/tb_dbrs');
		$query = $this->tb_dbrs->GetRowList(0, 1, "", "", "");
		if ($query[1] != 0) {
			$NameRS = $query[0][0]->NAME;
			$Address = $query[0][0]->ADDRESS;
			$TLP = $query[0][0]->PHONE1;
			$Kota = $query[0][0]->CITY;
		} else {
			$NameRS = "";
			$Address = "";
			$TLP = "";
		}
		$kduser=$this->session->userdata['user_id']['id'];
		$kdpasien = $Params['NoMedrec'];
		$namapasien = $Params['NamaPasien'];
		$alamat = $Params['Alamat'];
        $kdunit = $Params['Poli'];
		$tglmasuk = date('Y-m-d');
		$jammasuk = date('Y-m-d h:i:s');
		$kddokter = $Params['KdDokter'];
		$unit = $this->db->query("SELECT NAMA_UNIT FROM UNIT WHERE KD_UNIT = '".$kdunit."'")->row()->nama_unit;
		$dokter = $this->db->query("SELECT nama as dokter FROM dokter WHERE KD_dokter = '".$kddokter."'")->row()->dokter;
	    $sqldatasrv="SELECT count(KD_PASIEN) as jumlah FROM KUNJUNGAN WHERE KD_PASIEN = '".$kdpasien."' and  KD_UNIT =  '".$kdunit."' ";
        $dbsqlsrv = $this->load->database('otherdb2',TRUE);
        $res = $this->db->query($sqldatasrv);
		$dbsqlsrv->trans_begin();
		# *** Load data dari POSTGRES ***
		/* # No. Antrian Cetak berdasarkan tgl_masuk, kd_unit.
		$sqlTracer="SELECT COUNT(no_urut_cetak) AS jum FROM tracer WHERE tgl_masuk='".$tglmasuk."'";
		$resTracer=$this->db->query($sqlTracer)->row();
        foreach ($res->result() as $rec){
          $jumlah=$rec->JUMLAH;
        }
		$urutCetak=$resTracer->jum+1; */
		
		# *** Load data dari SQL ***
		# No Antrian Prioritas
		
		/* $updateantrianpoliklinikpostgres = $this->db->query("update antrian_poliklinik set no_urut=".$urutCetak.", no_prioritas=".$no_prioritas." 
														where kd_unit='".$kdunit."' and tgl_transaksi='".$tglmasuk."' 
														and kd_pasien='".$kdpasien."' and kd_user='".$kduser."'");
		$updateantrianpolikliniksql = $db->query("update antrian_poliklinik set no_urut=".$urutCetak.", no_prioritas=".$no_prioritas."
												where kd_unit='".$kdunit."' and  tgl_transaksi='".$tglmasuk."' 
														and kd_pasien='".$kdpasien."' and kd_user='".$kduser."'"); */
												// cast(cast(TGL_TRANSAKSI as varchar(12))as datetime)='".$tglmasuk."' 
		$no_prioritas=$this->db->query("select no_prioritas from antrian_poliklinik WHERE KD_PASIEN = '".$kdpasien."' And kd_unit = '".$kdunit."' And tgl_transaksi = '".$tglmasuk."' order by no_urut desc")->row()->no_prioritas;
		$urutCetak=$this->db->query("select no_urut from antrian_poliklinik WHERE KD_PASIEN = '".$kdpasien."' And kd_unit = '".$kdunit."' And tgl_transaksi = '".$tglmasuk."' order by no_urut desc")->row()->no_urut;
		$jumlah = $this->db->query("select count(*) as jumkunjungan from kunjungan where kd_pasien='".$kdpasien."'")->row()->jumkunjungan;
		if ($jumlah==0 || $jumlah==1)
		{
			$anyar="(B)";
			$jumlah=1;
		}
		else
		{
			$anyar="";
		}
		$data = array(
			"kd_pasien"=>$kdpasien,
			"nama_pasien"=>$namapasien,
			"nama_unit"=>$unit,
			"nama_dokter"=>$dokter,
			"tgl_masuk"=>$tglmasuk,
			"jam_masuk"=>$jammasuk,
			"alamat"=>$alamat,
			"no_urut_cetak"=>$urutCetak
		);
		$this->load->model('rawat_jalan/tbltracer');	   
		$save = $this->tbltracer->save($data);
		$sql = $this->db->query("select t.*, u.kd_unit, k.kd_rujukan, c.customer, k.jam_masuk, CASE WHEN k.baru = 't' THEN '(B)' ELSE '' END as baru,
			no_antrian,age(p.tgl_lahir) as tgl_lahir
			from tracer t
			INNER JOIN unit u on t.nama_unit = u.nama_unit
			INNER JOIN kunjungan k on k.kd_pasien = t.kd_pasien and t.tgl_masuk = k.tgl_masuk and u.kd_unit = k.kd_unit
			inner join pasien p on p.kd_pasien = t.kd_pasien
			INNER JOIN customer c on k.kd_customer = c.kd_customer
			INNER JOIN antrian_poliklinik ap on ap.kd_pasien = t.kd_pasien and ap.TGL_TRANSAKSI = t.tgl_masuk and u.kd_unit = ap.kd_unit
			WHERE t.KD_PASIEN = '".$kdpasien."' And k.kd_unit = '".$kdunit."' And t.tgl_masuk = '".$tglmasuk."' ");
		if($sql->num_rows == 0){
			$nama = '1';
			$kd_pasien = '2';
			$tgl_masuk = '3';
			$unit = '4';
			$dokter = '5';
			$alamat = '6';
			$customer = '7';
			$jam = '8';
			$baru = '9';
		}else {
			$tmpumur = '';
			foreach($sql->result() as $row){
				$nama = $row->nama_pasien;
				$kd_pasien = $row->kd_pasien;
				$tgl_masuk = $row->tgl_masuk;
				$unit = $row->nama_unit;
				$dokter = $row->nama_dokter;
				$alamat = $row->alamat;
				$customer = $row->customer;
				$jam = substr($row->jam_masuk,11);
				$baru = $row->baru;
				$antrian = $row->no_antrian;
				$Split1 = explode(" ", $row->tgl_lahir, 6);
				if (count($Split1) == 6){
					$tmp1 = $Split1[0];
					$tmp2 = $Split1[1];
					$tmp3 = $Split1[2];
					$tmp4 = $Split1[3];
					$tmp5 = $Split1[4];
					$tmp6 = $Split1[5];
					$tmpumur = $tmp1.'th';
				}else if (count($Split1) == 4){
					$tmp1 = $Split1[0];
					$tmp2 = $Split1[1];
					$tmp3 = $Split1[2];
					$tmp4 = $Split1[3];
					if ($tmp2 == 'years'){
						$tmpumur = $tmp1.'th';
					}else if ($tmp2 == 'mon'){
						$tmpumur = $tmp1.'bl';
					}else if ($tmp2 == 'days'){
						$tmpumur = $tmp1.'hr';
					}
				} else{
					$tmp1 = $Split1[0];
					$tmp2 = $Split1[1];
					if ($tmp2 == 'years') {
						$tmpumur = $tmp1.'th';
					}else if ($tmp2 == 'mons'){
						$tmpumur = $tmp1.'bl';
					} else if ($tmp2 == 'days'){
						$tmpumur = $tmp1.'hr';
					}
				}
			}
		}
		$printer = $this->db->query("select setting from sys_setting where key_data = 'rwj_default_printer_tracer'")->row()->setting;
		$waktu = explode(" ",$tgl_masuk);
		$tanggal = $waktu[0];
		$t1 = 4;
		$t3 = 20;
		$t2 = 60 - ($t3 + $t1);
		$x = 0;
		$tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
		$file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
		$handle = fopen($file, 'w');
		$condensed = Chr(27).Chr(33).Chr(4);
		$condensed2 = Chr(27).Chr(33).Chr(8);
		$bold1 = Chr(27) . Chr(69);
		$bold0 = Chr(27) . Chr(70);
		$initialized = chr(27).chr(64);
		$condensed1 = chr(15);
		$condensed0 = chr(18);
		$size0 = Chr(27).Chr(109);
		$Data  = $initialized;
		$Data .= $condensed1;
		$Data .= $condensed;
		$Data .= $condensed.str_pad($bold1.$NameRS.$bold0,64," ",STR_PAD_BOTH)."\n";$x++;
		$Data .= $condensed.str_pad($bold1.$Address.$bold0,64," ",STR_PAD_BOTH)."\n";$x++;
		$Data .= $condensed.str_pad($bold1.$TLP.$bold0,64," ",STR_PAD_BOTH)."\n";$x++;
		$Data .= $condensed."----------------------------------------------------------------\n"; $x++;
		$Data .= $condensed.str_pad($bold1."TRACER".$bold0,64," ",STR_PAD_BOTH)."\n";
		$Data .= $condensed."----------------------------------------------------------------\n"; $x++; //64
		$Data .= $condensed."No. Antrian Prioritas   : " .$condensed2.$no_prioritas."\n";$x++;
		$Data .= $condensed."No. Antrian Poli	: " .$condensed2.$urutCetak."\n";$x++;
		$Data .= $condensed."No. MR           	: " .$condensed2.$kd_pasien."\n";$x++;
		$Data .= $condensed."Nama             	: " .$condensed2.$namapasien." ".$anyar."\n";$x++;
		$Data .= $condensed."Usia             	: " .$condensed2.$tmpumur."\n";$x++;
		$Data .= $condensed."Poliklinik       	: " .$condensed2.$unit."\n";$x++;
		//$Data .= $condensed."No. Antrian      : " .$condensed2.$antrian."\n";$x++;
		$Data .= $condensed."Kelompok Pasien  	: " .$condensed2.$customer."\n";$x++;
		$Data .= $condensed."Tgl Periksa      	: " .$condensed2.$tanggal."\n";$x++;
		$Data .= $condensed."Jumlah Kunjungan 	: " .$condensed2.$jumlah."\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "   Pengirim            Penerima   ";
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "_______________     _______________";
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$Data .= "\n";$x++;
		$counter = $this->db->query("select setting from sys_setting where key_data = 'setmarginprint'")->row()->setting;
		if($counter == '7'){
			$Data .=''; 
			$newVal = 1; 
			$update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}else{
			$Data .= "\n";
			$Data .= "\n";
			$Data .= "\n";
			$newVal = (int)$counter + 1; 
			$update = $this->db->query("update sys_setting set setting = '$newVal' where key_data = 'setmarginprint'") ;
		}
		if ($x < 17){
			$y = $x-17;
			for ($z = 1; $z<=$y;$z++){
				$Data .= "\n";
			}
		}
		fwrite($handle, $Data);
		fclose($handle);
		//echo $Data;
		$success=false;
		$message='';
		//echo '1.'.$this->session->userdata['user_id']['id'].'<br>';
		if($this->session->userdata['user_id']['id']){
			$res=$this->db->query("SELECT auto_print_tracer FROM zusers WHERE kd_user='".$this->session->userdata['user_id']['id']."'")->row();
			if($res){
				//echo $res->auto_print_tracer;
				//echo '2.'.$res->auto_print_tracer.'<br>';
				if($res->auto_print_tracer==1){
					$success=true;
					$message='Tracer Berhasil diCetak.';
					$print = shell_exec("lpr -P ".$printer." -r ".$file);
				}
			}
		}
		//copy($file, $printer);  # Lakukan cetak 	 
		if($success==true){
		   echo "{result:'SUCCESS',message:'".$message."'}";
	   } else if(!$save){
		    echo "{result:'ERROR'}";
	   }else{
		   echo "{result:'SUCCESS'}";
	   }
    }          
}