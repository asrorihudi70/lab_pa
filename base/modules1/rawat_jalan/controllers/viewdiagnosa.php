<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Viewdiagnosa extends MX_Controller{
    public function __construct(){
        parent::__construct();

    }

	public function index(){
        $this->load->view('main/index');
    }

	function read($Params=null){
		try{
			$result=$this->db->query('SELECT A.*,B.penyakit,C.morfologi,D.sebab FROM mr_penyakit A
			LEFT JOIN penyakit B ON A.kd_penyakit=B.kd_penyakit 
			LEFT JOIN neoplasma C ON 
			C.kd_penyakit=A.kd_penyakit AND C.kd_pasien=A.kd_pasien AND C.kd_unit=A.kd_unit AND C.tgl_masuk=A.tgl_masuk AND C.tgl_masuk=A.tgl_masuk
			LEFT JOIN kecelakaan D ON
			D.kd_penyakit=A.kd_penyakit AND D.kd_pasien=A.kd_pasien AND D.kd_unit=A.kd_unit AND D.tgl_masuk=A.tgl_masuk AND D.tgl_masuk=A.tgl_masuk
			 WHERE '.str_replace("~", "'", $Params[4]))->result();
			$a=array();
			foreach($result as $row){
				$b['KD_PENYAKIT']=$row->kd_penyakit;
				$b['KD_PASIEN']=$row->kd_pasien;
				$b['KD_UNIT']=$row->kd_unit;
				$b['TGL_MASUK']=$row->tgl_masuk;
				$b['URUT_MASUK']=$row->urut_masuk;
				$b['URUT']=$row->urut;
				if($row->stat_diag == 0){
					$b['STAT_DIAG'] = 'Diagnosa Awal';
				}else if($row->stat_diag  == 1){
					$b['STAT_DIAG'] = 'Diagnosa Utama';
				}else if($row->stat_diag  == 2){
					$b['STAT_DIAG'] = 'Komplikasi';
				}else if($row->stat_diag == 3){
					$b['STAT_DIAG'] = 'Diagnosa Sekunder';
				}
				$b['KASUS']='Lama';
				if($row->kasus=='f'){
					$b['KASUS']='Baru';
				}
				$b['TINDAKAN']=$row->tindakan;
				$b['PERWATAN']=$row->perawatan;
				$b['PENYAKIT']=$row->penyakit;
				$b['NOTE']=0;
				$b['DETAIL']='';
				if($row->morfologi!= null && $row->morfologi!= ''){
					$b['NOTE']=1;
					$b['DETAIL']=$row->morfologi;
				}else if($row->sebab!= null && $row->sebab!= ''){
					$b['NOTE']=2;
					$b['DETAIL']=$row->sebab;
				}
				//$b['KD_RUJUKAN']=$row->kd_rujukan;
				//$b['KD_DOKTER']=$row->kd_dokter;
				//$b['STATUS_BAHAYA']=$row->status_bahaya;
				$a[]=$b;
			}

		}catch(Exception $o){
			echo 'Debug  fail ';
		}
	 	echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($a).'}';
	}
}
?>