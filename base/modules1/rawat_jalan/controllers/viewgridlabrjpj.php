<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewgridlabrjpj extends MX_Controller {
    public function __construct(){
        parent::__construct();
    }	 

    public function index(){
   	 $this->load->view('main/index');
    }
   
   public function read($Params=null) {
        $result=$this->db->query('select E.klasifikasi,C.kd_produk,C.kd_klas,C.deskripsi,
			CASE WHEN (SELECT NAMA FROM DOKTER WHERE KD_DOKTER = B.KD_DOKTER) IS NULL THEN
			'."'Admin'".'
			ELSE
			(SELECT NAMA FROM DOKTER WHERE KD_DOKTER = B.KD_DOKTER)
			END
			AS username,B.kd_lab from mr_labkonsuldtl B
			LEFT JOIN mr_labkonsul A ON B.id_labkonsul=A.id_labkonsul 
			LEFT JOIN produk C ON C.kd_produk=B.kd_produk
			LEFT JOIN dokter D ON D.kd_dokter=B.kd_dokter
			LEFT JOIN klas_produk E using (kd_klas)
			WHERE '.str_replace("~", "'", $Params[4]))->result();
        echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($result).'}';
   }
   
}

?>