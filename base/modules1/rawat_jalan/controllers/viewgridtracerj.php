<?php

/**
 * @author
 * @copyright
 */


class Viewgridtracerj extends MX_Controller {

    public function __construct(){
        parent::__construct();

    }

    public function index(){
        $this->load->view('main/index');
    }
    
    function read($Params=null) {
    	$traces=array();
    	$result= $this->db->query('select * from tracer WHERE '.str_replace("~", "'", $Params[4]))->result();
    	for($i=0; $i<count($result) ;$i++){
    		$trace=array();
    		$date = new DateTime($result[$i]->tgl_masuk);
    		$trace['TANGGAL']=strval($date->format('d/m/y'));
			$datejammasuk = new DateTime($result[$i]->jam_masuk);
    		$trace['JAM']=strval($datejammasuk->format('h:i:s'));
    		$trace['KD_PASIEN']=$result[$i]->kd_pasien;
    		$trace['NAMA_PASIEN']=$result[$i]->nama_pasien;
    		$trace['UNIT']=$result[$i]->nama_unit;
    		$trace['DOKTER']=$result[$i]->nama_dokter;
    		$trace['ALAMAT']=$result[$i]->alamat;
    		$traces[]=$trace;
    	}
    	echo '{success:true, totalrecords:'.count($result).', ListDataObj:'.json_encode($traces).'}';
    }
}

?>