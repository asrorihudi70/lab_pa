<?php

class viewjadwaldokter extends MX_Controller
 {
 	

        public function __construct()
        {
            //parent::Controller();
            parent::__construct();

        }


	public function index()
	{
            $this->load->view('main/index');
        }


	 public function read($Params=null)
	{
		
		try
		{
			
			$this->load->model('rawat_jalan/tblviewjadwaldokter');
           if (strlen($Params[4])!==0)
           {
           $this->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
           }
			$res = $this->tblviewjadwaldokter->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
			//return $res;
		}
		catch(Exception $o)
		{
			echo 'Debug  fail ';

		}
		echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
		
	}


	 function save($param)
	 {
		 $this->db->trans_begin();
		$this->load->model('setup/tblam_jadwaldokter');
	
		switch($param['Hari'])
		{
			case 'Minggu' 	: 	$Arr['hari'] = 0;
								break;
			case 'Senin' 	: 	$Arr['hari'] = 1;
								break;
			case 'Selasa' 	: 	$Arr['hari'] = 2;
								break;
			case 'Rabu' 	: 	$Arr['hari'] = 3;
								break;
			case 'Kamis' 	: 	$Arr['hari'] = 4;
								break;
			case 'Jumat' 	: 	$Arr['hari'] = 5;
								break;
			case 'Sabtu' 	: 	$Arr['hari'] = 6;
								break;
		}
		$jam=$param['jam'].':'.$param['menit'].':00';
		$Arr['jam']='1900-01-01 '.$jam;
			
		$Arr['kd_dokter']=$param['Kd_Dokter'];
		$Arr['kd_unit']=$param['Kd_Unit'];
		$Arr['max_pelayanan']=$param['maxantri'];
		$Arr['durasi_periksa']=$param['durasi'];
		$qPostgres=count($this->db->query("select * from jadwal_poli where kd_dokter='".$Arr['kd_dokter']."' and kd_unit='".$Arr['kd_unit']."' and hari='".$Arr['hari']."' ")->result());
		
		if ($qPostgres<>0)
		{
			$res=$this->db->query("update jadwal_poli set jam='".$Arr['jam']."' , max_pelayanan=".$Arr['max_pelayanan'].", durasi_periksa=".$Arr['durasi_periksa']." where kd_dokter='".$Arr['kd_dokter']."' and kd_unit='".$Arr['kd_unit']."' and hari='".$Arr['hari']."'");
		}
		else
		{
			$res=$this->tblam_jadwaldokter->Save($Arr);
		}
		
		
		
		$ArrMySql['kd_dokter']=$param['Kd_Dokter'];
		$ArrMySql['nama_dok']=$param['Dokter'];
		$ArrMySql['kd_unit']=$param['Kd_Unit'];
		$ArrMySql['nama_unit']=$param['Unit'];
		$ArrMySql['hari']='DAY_'.$Arr['hari'];
		$ArrMySql['jam']=$jam;
		$ArrMySql['max_pelayanan']=$param['maxantri'];
		$ArrMySql['durasi_periksa']=$param['durasi'];
		$qMySql=count($this->common->db2()->query("select * from rs_jadwal_dokter where kd_dokter='".$ArrMySql['kd_dokter']."' and kd_unit='".$ArrMySql['kd_unit']."' and hari='".$ArrMySql['hari']."' ")->result());
		
		if ($qMySql<>0)
		{
			$simpanMySql=$this->common->db2()->query("update rs_jadwal_dokter set nama_dok='".$ArrMySql['nama_dok']."', nama_unit='".$ArrMySql['nama_unit']."' , jam='".$jam."' , max_pelayanan=".$ArrMySql['max_pelayanan'].", durasi_periksa=".$ArrMySql['durasi_periksa']." where kd_dokter='".$ArrMySql['kd_dokter']."' and kd_unit='".$ArrMySql['kd_unit']."' and hari='".$ArrMySql['hari']."'");
		}
		else
		{
			$simpanMySql=$this->common->db2()->insert('rs_jadwal_dokter',$ArrMySql);
		}
		
		
		if ($res && $simpanMySql)
		{
			$this->db->trans_commit();
			echo '{success: true, kd_unit: "'.$Arr['kd_unit'].'" }';
		}else
		{
			$this->db->trans_rollback();
			echo '{success: false, kd_unit: "'.$Arr['kd_unit'].'" }';
		}
		

		
	}
	
	
	 function GetNewID()
         {
             $res = $this->tblam_employees->GetRowList( 0 , 1, 'DESC', 'emp_id', '');
             if ($res[1]===0)
             {
                 return 1;
             }
             else
             {
                 return $res[0][0]->EMP_ID+1;
             }
         }
//	 function UpdateData($VConn, $param)
//	 {
//		$this->load->model('setup/tblam_employees');
//				$Arr['emp_id']=$_REQUEST['EMP_ID'];
//		$Arr['dept_id']=$_REQUEST['DEPT_ID'];
//		$Arr['emp_name']=$_REQUEST['EMP_NAME'];
//		$Arr['emp_address']=$_REQUEST['EMP_ADDRESS'];
//		$Arr['emp_city']=$_REQUEST['EMP_CITY'];
//		$Arr['emp_state']=$_REQUEST['EMP_STATE'];
//		$Arr['emp_pos_code']=$_REQUEST['EMP_POS_CODE'];
//		$Arr['emp_phone1']=$_REQUEST['EMP_PHONE1'];
//		$Arr['emp_phone2']=$_REQUEST['EMP_PHONE2'];
//		$Arr['emp_email']=$_REQUEST['EMP_EMAIL'];
//		$Arr['is_aktif']=$_REQUEST['IS_AKTIF'];
//
//		$Arr=array_filter( $Arr , "ArrayUtils::NullElement"  );
//	 	$res=$this->tblam_employees->UpdateData($VConn,$Arr, $param); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
//	 	return $res;
//	 }

public function getsession()
{
	$session = $this->session->userdata['user_id'];
	var_dump($session);
}
	 
	 function delete($param)
	 {
		 switch($param['Hari'])
		{
			case 'Minggu' 	: 	$hari = 0;
								break;
			case 'Senin' 	: 	$hari = 1;
								break;
			case 'Selasa' 	: 	$hari = 2;
								break;
			case 'Rabu' 	: 	$hari = 3;
								break;
			case 'Kamis' 	: 	$hari = 4;
								break;
			case 'Jumat' 	: 	$hari = 5;
								break;
			case 'Sabtu' 	: 	$hari = 6;
								break;
		}
		//get id Poliklinik
	
		/*$unit = $this->db->query("select kd_unit from unit where nama_unit = '".$param['Unit']."'");
		$res = $unit->result();
		foreach($res as $u)
		{
			$kUnit = $u->kd_unit;
		}
		
		$dokter = $this->db->query("select kd_dokter from dokter where nama_unit = '".$param['Dokter']."'");
		$res = $unit->result();
		foreach($res as $u)
		{
			$kUnit = $u->kd_unit;
		}*/
				
		
		   
		$this->load->model('setup/tblam_jadwaldokter');
                $Sql=$this->db;
                $Sql->where("hari = '".$hari."' and kd_dokter = '".$param['Kd_Dokter']."' AND kd_unit = '".$param['Kd_Unit']."'", null, false);
	 	$res=$this->tblam_jadwaldokter->Delete(); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
		

	 	//return $res;
                if ($res>0)
                {
                    echo '{success: true}';
                } else echo var_dump($param);//$param['Hari'].":".$kUnit;//echo '{success: false}';
	 }	

 }
 //VIEWSETUPEMPLOYEE

?>