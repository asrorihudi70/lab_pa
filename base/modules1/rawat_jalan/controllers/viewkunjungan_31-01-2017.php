<?php
class viewkunjungan extends MX_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $this->load->view('main/index');
    }
    function read($Params = null) {
		$list=array();
        try {
			$sqldatasrv="SELECT distinct top 500 KD_PASIEN=pasien.kd_pasien, NAMA=pasien.nama, NAMA_KELUARGA=pasien.nama_keluarga,
			JENIS_KELAMIN=pasien.jenis_kelamin, TEMPAT_LAHIR=pasien.tempat_lahir, TGL_LAHIR=pasien.tgl_lahir,AGAMA=agama.agama, GOL_DARAH=pasien.gol_darah,NO_ASURANSI=pasien.no_asuransi, 
			WNI=pasien.wni, STATUS_MARITA=pasien.status_marita, ALAMAT=pasien.alamat, KD_KELURAHAN=pasien.kd_kelurahan,
			PENDIDIKAN=pendidikan.pendidikan, PEKERJAAN=pekerjaan.pekerjaan,KD_KABUPATEN=kb.kd_kabupaten, KABUPATEN=kb.KABUPATEN, KD_KECAMATAN=kc.kd_kecamatan, KECAMATAN=kc.KECAMATAN, KD_PROPINSI=pr.kd_propinsi, PROPINSI=pr.PROPINSI,
			KD_PENDIDIKAN=pasien.kd_pendidikan, KD_PEKERJAAN=pasien.kd_pekerjaan, KD_AGAMA=pasien.kd_agama, ALAMAT_KTP=pasien.alamat_ktp,NAMA_AYAH=pasien.nama_ayah,KD_PENDIDIKAN_AYAH=pasien.kd_pendidikan_ayah, PENDIDIKAN_AYAH=p1.pendidikan,KD_PEKERJAAN_AYAH=pasien.kd_pekerjaan_ayah, PEKERJAAN_AYAH=pek1.pekerjaan,
			NAMA_IBU=pasien.nama_ibu,KD_PENDIDIKAN_IBU=pasien.kd_pendidikan_ibu, PENDIDIKAN_IBU=p2.pendidikan ,KD_PEKERJAAN_IBU=pasien.kd_pekerjaan_ibu, PEKERJAAN_IBU=pek2.pekerjaan,NAMA_SUAMIISTRI=pasien.suami_istri,KD_PENDIDIKAN_SUAMIISTRI=pasien.kd_pendidikan_suamiistri, PENDIDIKAN_SUAMIISTRI=p3.pendidikan,KD_PEKERJAAN_SUAMIISTRI=pasien.kd_pekerjaan_suamiistri, PEKERJAAN_SUAMIISTRI=pek3.pekerjaan,
			KD_KELURAHAN_KTP=pasien.kd_kelurahan_ktp,KD_POS_KTP=pasien.kd_pos_ktp, KD_POS=pasien.kd_pos, PROPINSIKTP=proktp.kd_propinsi,KABUPATENKTP=kabktp.kd_kabupaten,
			KECAMATANKTP=kecktp.kd_kecamatan,
			KELURAHANKTP=kelktp.kd_kelurahan ,KELURAHAN=kl.kelurahan,
			KEL_KTP=kelktp.kelurahan ,
			KEC_KTP=kecktp.kecamatan,KAB_KTP=kabktp.kabupaten ,PRO_KTP=proktp.propinsi,
			EMAIL=pasien.email,HANDPHONE=pasien.handphone,TELEPON=pasien.telepon,hck.kd_pasien as cekkartu
			FROM pasien 
			left JOIN agama ON pasien.kd_agama = agama.kd_agama 
			left JOIN pendidikan ON pasien.kd_pendidikan = pendidikan.kd_pendidikan 
			left JOIN pendidikan p1 ON pasien.kd_pendidikan_ayah = p1.kd_pendidikan 
			left JOIN pendidikan p2 ON pasien.kd_pendidikan_ibu = p2.kd_pendidikan 
			left JOIN pendidikan p3 ON pasien.kd_pendidikan_suamiistri = p3.kd_pendidikan 
			left JOIN pekerjaan ON pasien.kd_pekerjaan = pekerjaan.kd_pekerjaan
			left JOIN pekerjaan pek1 ON pasien.kd_pekerjaan_ayah = pek1.kd_pekerjaan
			left JOIN pekerjaan pek2 ON pasien.kd_pekerjaan_ibu = pek2.kd_pekerjaan
			left JOIN pekerjaan pek3 ON pasien.kd_pekerjaan_suamiistri = pek3.kd_pekerjaan
			LEFT join kelurahan kl on kl.kd_kelurahan = pasien.KD_KELURAHAN
			LEFT join KECAMATAN kc on kc.KD_KECAMATAN = kl.KD_KECAMATAN
			LEFT join KABUPATEN kb on kb.KD_KABUPATEN = kc.KD_KABUPATEN
			LEFT join PROPINSI pr on pr.KD_PROPINSI = kb.KD_PROPINSI
			left join kelurahan kelktp on kelktp.kd_kelurahan=pasien.kd_kelurahan_ktp
			left join KECAMATAN kecktp on kecktp.kd_kecamatan=kelktp.kd_kecamatan 
			left join KABUPATEN kabktp on kabktp.kd_kabupaten=kecktp.kd_kabupaten
			left join kunjungan ON pasien.kd_pasien = kunjungan.kd_pasien
			left join Propinsi proktp on proktp.kd_propinsi=kabktp.kd_propinsi 
			left join history_cetak_kartu hck on hck.kd_pasien = pasien.kd_pasien
			where"; 
            if (strlen($Params[4]) !== 0) {
                $kriteria=str_replace("~", "'", $Params[4] . " and pasien.kd_pasien Like '%-%'  order by pasien.kd_pasien desc --offset " . $Params[0] . "  limit 500 ");
            } else {
                $kriteria=str_replace("~", "'", " pasien.kd_pasien Like '%-%' order by pasien.kd_pasien desc   --offset " . $Params[0] . " limit 500 ");
            }
			//echo $sqldatasrv.$kriteria;
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
			foreach ($res->result() as $rec){
				$o=array();
				$selectKartuPasien = ("SELECT count(kd_pasien) as counter FROM history_cetak_kartu WHERE tgl_cetak = '".date('Y-m-d')."' AND kd_pasien='".$rec->KD_PASIEN."'");
				$counter = $dbsqlsrv->query($selectKartuPasien);
				$counter = $counter->row()->counter;
				$o['KD_PASIEN']=$rec->KD_PASIEN;
				$o['NAMA']=$rec->NAMA;
				$o['NAMA_KELUARGA']=$rec->NAMA_KELUARGA;
				$o['JENIS_KELAMIN']=$rec->JENIS_KELAMIN;
				$o['TEMPAT_LAHIR']=$rec->TEMPAT_LAHIR;
				$o['TGL_LAHIR']=date("d/m/Y",strtotime($rec->TGL_LAHIR));
				$o['AGAMA']=$rec->AGAMA;
				$o['GOL_DARAH']=$rec->GOL_DARAH;
				$o['WNI']=$rec->WNI;
				$o['STATUS_MARITA']=$rec->STATUS_MARITA;
				$o['ALAMAT']=$rec->ALAMAT;
				$o['KD_KELURAHAN']=$rec->KD_KELURAHAN;
				$o['PENDIDIKAN']=$rec->PENDIDIKAN;
				$o['PENDIDIKAN_AYAH']=$rec->PENDIDIKAN_AYAH;
				$o['PENDIDIKAN_IBU']=$rec->PENDIDIKAN_IBU;
				$o['PENDIDIKAN_SUAMIISTRI']=$rec->PENDIDIKAN_SUAMIISTRI;
				$o['PEKERJAAN']=$rec->PEKERJAAN;
				$o['PEKERJAAN_AYAH']=$rec->PEKERJAAN_AYAH;
				$o['PEKERJAAN_IBU']=$rec->PEKERJAAN_IBU;
				$o['PEKERJAAN_SUAMIISTRI']=$rec->PEKERJAAN_SUAMIISTRI;
				$o['KABUPATEN']=$rec->KABUPATEN;
				$o['KECAMATAN']=$rec->KECAMATAN;
				$o['PROPINSI']=$rec->PROPINSI;
				$o['KD_KABUPATEN']=$rec->KD_KABUPATEN;
				$o['KD_KECAMATAN']=$rec->KD_KECAMATAN;
				$o['KD_PROPINSI']=$rec->KD_PROPINSI;
				$o['KD_PENDIDIKAN']=$rec->KD_PENDIDIKAN;
				$o['KD_PENDIDIKAN_AYAH']=$rec->KD_PENDIDIKAN_AYAH;
				$o['KD_PENDIDIKAN_IBU']=$rec->KD_PENDIDIKAN_IBU;
				$o['KD_PENDIDIKAN_SUAMIISTRI']=$rec->KD_PENDIDIKAN_SUAMIISTRI;
				$o['KD_PEKERJAAN']=$rec->KD_PEKERJAAN;
				$o['KD_PEKERJAAN_AYAH']=$rec->KD_PEKERJAAN_AYAH;
				$o['KD_PEKERJAAN_IBU']=$rec->KD_PEKERJAAN_IBU;
				$o['KD_PEKERJAAN_SUAMIISTRI']=$rec->KD_PEKERJAAN_SUAMIISTRI;
				$o['KD_AGAMA']=$rec->KD_AGAMA;
				$o['ALAMAT_KTP']=$rec->ALAMAT_KTP;
				$o['NAMA_AYAH']=$rec->NAMA_AYAH;
				$o['NAMA_IBU']=$rec->NAMA_IBU;
				$o['NAMA_SUAMIISTRI']=$rec->NAMA_SUAMIISTRI;
				$o['KD_KELURAHAN_KTP']=$rec->KD_KELURAHAN_KTP;
				$o['KD_POS_KTP']=$rec->KD_POS_KTP;
				$o['KD_POS']=$rec->KD_POS;
				$o['PROPINSIKTP']=$rec->PROPINSIKTP;
				$o['KABUPATENKTP']=$rec->KABUPATENKTP;
				$o['KECAMATANKTP']=$rec->KECAMATANKTP;
				$o['KELURAHANKTP']=$rec->KELURAHANKTP;
				$o['KELURAHAN']=$rec->KELURAHAN;
				$o['KEL_KTP']=$rec->KEL_KTP;
				$o['KEC_KTP']=$rec->KEC_KTP;
				$o['KAB_KTP']=$rec->KAB_KTP;
				$o['PRO_KTP']=$rec->PRO_KTP;
				$o['NO_ASURANSI']=$rec->NO_ASURANSI;
				$o['EMAIL_PASIEN']=$rec->EMAIL;
				$o['HP_PASIEN']=$rec->HANDPHONE;
				$o['TLPN_PASIEN']=$rec->TELEPON;
				if ($counter > 0) {
					$o['KARTU'] = 't';
				}else{
					$o['KARTU'] = 'f';
				}
				// $o['KARTU']=$rec->cekkartu;
				$list[]=$o;	
			} 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }
    public function umur($Params = null) {
        $hitungumur = $this->datediff($asli, $sekarang);
        if ($hitungumur == "ketemu") {
            echo '{success: true, NoMedrec: "' . $SchId . '"}';
        } else{
			echo '{success: false}';
		} 
    }
	public function saveDiagnosa($Params, $SchId){
		$db = $this->load->database('otherdb2',TRUE);
		$this->db->trans_begin();
		$db->trans_begin();
		$kdPasien = $Params["NoMedrec"];
		$kdUnit = $Params["Poli"];
		$Tgl =date('Y-m-d');
		$urut_masuk = 0;
		$kdPenyakit=$Params['KdDiagnosa'];
		$perawatan = 99;
		$tindakan = 99;
		$inser_batch=array();
		$this->db->query("DELETE FROM mr_penyakit WHERE kd_pasien='".$kdPasien."' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk='".$urut_masuk."'");
		$diagnosa = 0;
		$kasus = 'FALSE';
		$zkasus = 0;
		$urut = $this->db->query("select getUrutMrPenyakit('".$kdPasien."','".$kdUnit."','".$Tgl."',".$urut_masuk.") ");
		$result = $urut->result();
		foreach ($result as $data){
			$Urutan = $data->geturutmrpenyakit;
		}
		$query = $this->db->query("select insertdatapenyakit('".$kdPenyakit."','".$kdPasien."','".$kdUnit."','".$Tgl."',".$diagnosa.",".$kasus.",
		".$tindakan.",".$perawatan.",".$Urutan.",".$urut_masuk.")");
		//INSERT MR PENYAKIT KE SQL SERVER
		$mr_penyakit=array(
			'KD_PENYAKIT'=>$kdPenyakit,
			'KD_PASIEN'=>$kdPasien,
			'KD_UNIT'=>$kdUnit,
			'TGL_MASUK'=>$Tgl,
			'URUT_MASUK'=>$urut_masuk,
			'URUT'=>($Urutan-1),
			'STAT_DIAG'=>$diagnosa,
			'TINDAKAN'=>$tindakan,
			'PERAWATAN'=>$perawatan
		);
		if($kasus=='TRUE'){
			$mr_penyakit['KASUS']=1;
		}else{
			$mr_penyakit['KASUS']=0;
		}
		$db->query("DELETE FROM mr_penyakit WHERE kd_pasien='".$kdPasien."' and kd_unit='".$kdUnit."' and tgl_masuk='".$Tgl."' and urut_masuk='".$urut_masuk."'");
		$db->insert('mr_penyakit',$mr_penyakit);
		if($this->db->trans_status()==true && $db->trans_status()){
			$this->db->trans_commit();
			$db->trans_commit();
		}else{
			$this->db->trans_rollback();
			$db->trans_rollback();
		}	
	}
    public function save($Params = null) {
		$db = $this->load->database('otherdb2',TRUE);
        $mError = "";
        $AppId = "";
        $SchId = "";
        $Schunit = "";
        $Schtgl = "";
        $Schurut = "";
        $notrans = "";
        
		//echo json_encode($Params);
		$this->db->trans_begin();
        $db->trans_begin();
		$now=new DateTime();
        $mError = $this->SimpanPasien($Params, $SchId);
		
        if ($mError == "ada"){
            if ($Params['NonKunjungan'] === 'true') {
                $this->db->trans_commit();
                $db->trans_commit();
                echo '{success: true, KD_PASIEN: "' . $SchId . '"}';
            } else {
                $mError = $this->SimpanKunjungan($Params, $SchId, $Schunit, $Schtgl, $Schurut);
                if ($mError == "aya") {
					$mError = explode(" ",$this->SimpanTransaksi($Params, $SchId, $Schurut, $notrans));
					if ($mError[0] == "sae") {
						
						/* //$ceknourutantrian=$this->db->query('select max(no_urut) as no_urut from antrian_poliklinik')->row()->no_urut;
						$ceknourutantrian=$db->query("select max(no_urut) as no_urut from antrian_poliklinik WHERE 
									cast(cast(TGL_TRANSAKSI as varchar(12)) as datetime)='".$now->format('Y-m-d')."' 
									AND kd_unit='".$Params['Poli']."'")->row()->no_urut;
						//echo $ceknourutantrian;	
						$getnourutantrian=1;
						if($ceknourutantrian){
							$getnourutantrian=$ceknourutantrian+1;
						} */
						
						$_kduser = $this->session->userdata['user_id']['id'];
						# pertama kali daftar no_antrian belum digenerate, tapi dari tracer setelah berkas ditemukan.
						// $no_antrian = $this->getNoantrian($Params['Poli']);
						$no_antrian = 0;
						
						# *** Status pasien lama/baru untuk identifikasi tracer ***
						# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
						$kunj = _QMS_QUERY("select * from kunjungan where kd_pasien='".$Params["NoMedrec"]."' ");
						if ($kunj->num_rows() == 0) {
							$baru = 1;
						} else {
							$baru = 0;
						}
						date_default_timezone_set("Asia/Jakarta");
						$TglAntrian = gmdate("Y-m-d H:i:s", time()+60*60*7);
						
						# POSTGRES
						$q_simpan_antrian_poli=$this->db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$Params['Poli']."','".date('Y-m-d')."',0,'".$SchId."',$_kduser,$baru,FALSE,".$no_antrian.",0)");
						# SQLSERVER
						$q_simpan_antrian_poli_ss=$db->query("insert into antrian_poliklinik 
							 (kd_unit,tgl_transaksi,no_urut,kd_pasien,kd_user,id_status_antrian,sts_ditemukan,no_antrian,no_prioritas) values 
							 ('".$Params['Poli']."','".$TglAntrian."',0,'".$SchId."',$_kduser,$baru,0,".$no_antrian.",0)");
						if ($q_simpan_antrian_poli && $q_simpan_antrian_poli_ss){
							$this->db->trans_commit();
							$db->trans_commit();
							echo '{success: true, KD_PASIEN: "' . $SchId . '",NoTrans: "' . $mError[1] . '"}';
						}else{
							$this->db->trans_rollback();
							$db->trans_rollback();
							echo '{success: false}';
						}	
					} else {
						$this->db->trans_rollback();
						$db->trans_rollback();
						echo '{success: false}';
					}
                } else if ($mError == "eror") {             
                    $this->db->trans_rollback();
					$db->trans_rollback();
                    echo '{success: false, cari: true}';
                } else {
					$this->db->trans_rollback();
					$db->trans_rollback();
                    echo '{success: false}';
                }
            }
        } else {
            $this->db->trans_rollback();
			$db->trans_rollback();
            echo '{success: false}';
        }
    }
    public function SimpanPasien($Params, & $AppId = "") {
        $strError = "";
        $suku = 0;
        if ($Params["GolDarah"] === "") {
            $Params["GolDarah"] = 0;
        }
        if ($Params["KDKECAMATAN"] === "") {
            // $tmppropinsi = $Params["Kelurahan"];
            $tmpkecamatan = $Params["Kd_Kecamatan"];
            $tmpkabupaten = $Params["AsalPasien"];
			if ($Params["Pendidikan"]=='' || $Params["Pendidikan"]=='undefined'){
				$tmppendidikan =0;
			}else{
				$tmppendidikan = $Params["Pendidikan"];
			}
			if ($Params["Pekerjaan"]=='' || $Params["Pekerjaan"]=='undefined'){
				$tmppekerjaan =0;
			}else{
				$tmppekerjaan = $Params["Pekerjaan"];
			}
            $tmpagama = $Params["Agama"];
        } else {
            // $tmppropinsi = $Params["KDPROPINSI"];
            $tmpkecamatan = $Params["KDKECAMATAN"];
            $tmpkabupaten = $Params["KDKABUPATEN"];
			if ($Params["KDPENDIDIKAN"]=='' || $Params["KDPENDIDIKAN"]=='undefined'){
				$tmppendidikan =0;
			}else{
				$tmppendidikan = $Params["KDPENDIDIKAN"];
			}
			if ($Params["KDPEKERJAAN"]=='' || $Params["KDPEKERJAAN"]=='undefined'){
				$tmppekerjaan =0;
			}else{
				$tmppekerjaan = $Params["KDPEKERJAAN"];
			}
            $tmpagama = $Params["KDAGAMA"];
        }
        if ($Params['Cek'] == 'Perseorangan' or $Params['Cek'] == 'Perusahaan') {
            $kode_asuransi = NULL;
        } else {
            $kode_asuransi = $Params["KD_Asuransi"];
        }
        if ($Params["NoAskes"] == "") {
            $tmpno_asuransi = $Params["NoSjp"];
        } else {
            $tmpno_asuransi = $Params["NoAskes"];
        }
        if ($Params["Kelurahan"] == "" or $Params["Kelurahan"] === NULL or $Params["Kelurahan"] === "Pilih Kelurahan...") {
            $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
        } else {
            if (is_numeric($Params["Kelurahan"])) {
                $intkd_lurah = $Params["Kelurahan"];
            } else {
                $intkd_lurah = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
            }
        }
        if ($Params["KdKelurahanKtpPasien"] == "" or $Params["KdKelurahanKtpPasien"] === NULL or $Params["KdKelurahanKtpPasien"] === "Pilih Kelurahan...") {
            $intkd_lurahktp = $this->GetKdLurah($kd_Kec = $Params["KDKECAMATANKTP"]);
        } else {
            $intkd_lurahktp = $Params["KdKelurahanKtpPasien"];
        }
		if ($Params["Pendidikan_Ayah"]=='' || $Params["Pendidikan_Ayah"]=='undefined'){
			$pendAyah=0;
		}else{
			$pendAyah=$Params["Pendidikan_Ayah"];
		}
		if ($Params["Pendidikan_Ibu"]=='' || $Params["Pendidikan_Ibu"]=='undefined'){
			$pendIbu=0;
		}else{
			$pendIbu=$Params["Pendidikan_Ibu"];
		}
		if ($Params["Pekerjaan_Ayah"]=='' || $Params["Pekerjaan_Ayah"]=='undefined'){
			$pekerAyah=0;
		}else{
			$pekerAyah=$Params["Pekerjaan_Ayah"];
		}
		if ($Params["Pekerjaan_Ibu"]=='' || $Params["Pekerjaan_Ibu"]=='undefined'){
			$pekerIbu=0;
		}else{
			$pekerIbu=$Params["Pekerjaan_Ibu"];
		}
		if ($Params["Pendidikan_SuamiIstri"]=='' || $Params["Pendidikan_SuamiIstri"]=='undefined'){
			$pendSumis=0;
		}else{
			$pendSumis=$Params["Pendidikan_SuamiIstri"];
		}
		if ($Params["Pekerjaan_SuamiIstri"]=='' || $Params["Pekerjaan_SuamiIstri"]=='undefined'){
			$pekerSumis=0;
		}else{
			$pekerSumis=$Params["Pekerjaan_SuamiIstri"];
		}
		$nik_pasien=$Params["part_number_nik"];
		if ($Params["NoMedrec"] == " " || $Params["NoMedrec"] == "Automatic from the system...") {
            $AppId = $this->GetIdRWJ();
        } else {
            $AppId = $Params["NoMedrec"];
        }
       
		
        $criteria = "kd_pasien = '" . $AppId . "'";
        $this->load->model("rawat_jalan/tb_pasien");
        $this->tb_pasien->db->where($criteria, null, false);
		
		//---SQL SERVER
		if ($Params["StatusWarga"]=='false'){
			$wni=0;
		}else{
			$wni=1;
		}
		if ($Params["JenisKelamin"]=='false'){
			$jk=0;
		}else{
			$jk=1;
		}
		$tmptgllahir = date('Y-m-d',strtotime(str_replace('/','-', $_POST['TglLahir'])));
		//echo $tmptgllahir;
		$datasqlsrv = array("kd_pasien" => $Params["NoMedrec"], "nama" => $Params["NamaPasien"],
            "nama_keluarga" => $Params["NamaKeluarga"], "jenis_kelamin" => $jk,
            "tempat_lahir" => $Params["Tempatlahir"], "tgl_lahir" => $tmptgllahir,
            "kd_agama" => $tmpagama, "gol_darah" => $Params["GolDarah"],
            "status_marita" => $Params["StatusMarita"], "wni" => $wni,
            "alamat" => $Params["Alamat"], "telepon" => $Params["TLPNPasien"],
            "kd_kelurahan" => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
            "kd_pekerjaan" => $tmppekerjaan, "pemegang_asuransi" => $Params["NamaPeserta"],
            "no_asuransi" => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
            "kd_suku" => $suku, "jabatan" => $Params["Jabatan"],
            "kd_perusahaan" => 1,  "kd_pos" => $Params["KdposPasien"]
        );  	
		$query = _QMS_QUERY("select * from pasien where $criteria ");
        if (count($query->result()) == 0) {
            $datasqlsrv["kd_pasien"] = $AppId;
			_QMS_insert('pasien', $datasqlsrv);
            $strError = "ada";
        } else {
			_QMS_update('pasien', $datasqlsrv,$criteria);
            $strError = "ada";
        } 
		
		//---POSTGRE
		
		 $data = array("kd_pasien" => $AppId, "nama" => $Params["NamaPasien"],
            "nama_keluarga" => $Params["NamaKeluarga"], "jenis_kelamin" => $Params["JenisKelamin"],
            "tempat_lahir" => $Params["Tempatlahir"], "tgl_lahir" => $tmptgllahir,
            "kd_agama" => (int)$tmpagama, "gol_darah" => $Params["GolDarah"],
            "status_marita" => $Params["StatusMarita"], "wni" => $Params["StatusWarga"],
            "alamat" => $Params["Alamat"], "telepon" => $Params["TLPNPasien"],
            "kd_kelurahan" => $intkd_lurah, "kd_pendidikan" => $tmppendidikan,
            "kd_pekerjaan" => $tmppekerjaan, "pemegang_asuransi" => $Params["NamaPeserta"],
            "no_asuransi" => $tmpno_asuransi, "kd_asuransi" => $kode_asuransi,
            "kd_suku" => $suku, "jabatan" => $Params["Jabatan"],
            "kd_perusahaan" => 0, "alamat_ktp" => $Params["AlamatKtpPasien"],
            "kd_kelurahan_ktp" => $intkd_lurahktp,
            "kd_pos_ktp" => $Params["KdPostKtpPasien"], "nama_ayah" => $Params["NamaAyahPasien"],
            "nama_ibu" => $Params["NamaIbuPasien"], "kd_pos" => $Params["KdposPasien"],
            "handphone" => $Params["HPPasien"],
            "email" => $Params["EmailPasien"],
			"suami_istri" => $Params["suami_istrinya"],
			"kd_pendidikan_ayah" => $pendAyah,
			"kd_pendidikan_ibu" => $pendIbu,
			"kd_pekerjaan_ayah" => $pekerAyah,
			"kd_pekerjaan_ibu" => $pekerIbu,
			"kd_pendidikan_suamiistri" => $pendSumis,
			"kd_pekerjaan_suamiistri" => $pekerSumis,
			"part_number_nik" => $nik_pasien 
        );
		$query =$this->db->query("select * from pasien where $criteria ");
		if (count($query->result()) == 0) {
            $data["kd_pasien"] = $AppId;
            $result = $this->db->insert('pasien',$data);
            $strError = "ada";
        } else {
            $this->tb_pasien->db->where($criteria, null, false);
            $result = $this->tb_pasien->Update($data);
            $strError = "ada";
        } 
		
        return $strError;
    }
	public function ceknikpasien(){
		$db = $this->load->database('otherdb2',TRUE);
		$q_cek=$db->query("select * from pasien where kd_pasien='".$_POST['kdPasien']."'")->row();
		if ($q_cek->PART_NUMBER_NIK == 0 || $q_cek->PART_NUMBER_NIK == ''){
			echo '{success: false}';
		}else{
			echo '{success: true, niknya: '.json_encode($q_cek->PART_NUMBER_NIK).', nomedrec: '.json_encode($q_cek->KD_PASIEN).'}';
		}
	}
	
	public function ceknikpasien2(){
		$q_cek=$this->db->query("select * from pasien where part_number_nik='".$_POST['part_number_nik']."' or kd_pasien='".$_POST['part_number_nik']."' ")->row();
		if (count($q_cek)==0){
			echo '{success: false}';
		}else{
			echo '{success: true, niknya: '.json_encode($q_cek->part_number_nik).', nomedrec: '.json_encode($q_cek->kd_pasien).'}';
		}
	}
    public function GetKdLurah($kd_Kec) {
        $intKdKec = $kd_Kec;
        $intKdLurah;
        $criteria = "where kec.kd_kecamatan=" . $intKdKec . " And (Kelurahan='DEFAULT' or Kelurahan='.' OR Kelurahan is null)";
        $this->load->model("rawat_jalan/tb_getkelurahan");
        $this->tb_getkelurahan->db->where($criteria, null, false);
        $query = $this->tb_getkelurahan->GetRowList(0, 1, "", "", "");
        if ($query[1] != 0) {
            if ($query[0][0]->KD_KELURAHAN == '') {
                $tmp_kdlurah = $this->GetLastKd_daerah(1);
                $this->load->model("general/tb_kelurahan");
                $data = array("kd_kelurahan" => $tmp_kdlurah, "kd_kecamatan" => $intKdKec, "kelurahan" => "DEFAULT");
                $result = $this->tb_kelurahan->Save($data);
                $strError = $tmp_kdlurah;
            } else {
                $strError = $query[0][0]->KD_KELURAHAN;
            }
        } else {
            if ($intKdKec == "") {
                $intKdKec = $this->GetLastKd_daerah(2);
                $this->load->model("general/tbl_kecamatan");
                $data = array("kd_kabupaten" => $tmp_kdlurah, "kd_kecamatan" => $intKdKec, "kecamatan" => "DEFAULT");
                $result = $this->tbl_kecamatan->Save($data);
                $strError = $intKdKec;
            } else {
                $intKdKec = $kd_Kec;
            }
            $tmp_kdlurah = $this->GetLastKd_daerah(1);
            $this->load->model("general/tb_kelurahan");
            $data = array("kd_kelurahan" => $tmp_kdlurah, "kd_kecamatan" => $intKdKec, "kelurahan" => "DEFAULT");
            $result = $this->tb_kelurahan->Save($data);
            $strError = $tmp_kdlurah;
        }
        return $strError;
    }
    public function GetLastKd_daerah($LevelLokasi) {
        if ($LevelLokasi == "1") {
            $this->load->model('general/tb_case1');
            $res = $this->tb_case1->GetRowList(0, 1, "", "", "");
            if ($res[1] > 0) {
                $nm = $res[0][0]->KODE;
                $nomor = (int) $nm + 1;
            }
        } else if ($LevelLokasi == "2") {
            $this->load->model('general/tb_case2');
            $res = $this->tb_case2->GetRowList(0, 1, "", "", "");
            if ($res[1] > 0) {
                $nm = $res[0][0]->KODE;
                $nomor = (int) $nm + 1;
            }
        } else if ($LevelLokasi == "3") {
            $this->load->model('general/tb_case3');
            $res = $this->tb_case3->GetRowList(0, 1, "", "", "");
            if ($res[1] > 0) {
                $nm = $res[0][0]->KODE;
                $nomor = (int) $nm + 1;
            }
        } else if ($LevelLokasi == "4") {
            $this->load->model('general/tb_case4');
            $res = $this->tb_case4->GetRowList(0, 1, "", "", "");
            if ($res[1] > 0) {
                $nm = $res[0][0]->KODE;
                $nomor = (int) $nm + 1;
            }
        }
        return $nomor;
    }

    public function SimpanKunjungan($Params, $SchId) {
        $db = $this->load->database('otherdb2',TRUE);
        //$kunj = _QMS_QUERY("select * from kunjungan where kd_pasien='".$Params["NoMedrec"]."' and kd_unit='".$Params["Poli"]."'");
        
		# Pasien lama adalah pasien yg sudah mempunyai no medrec, tidak terkait dengan unit.
		$kunj = _QMS_QUERY("select * from kunjungan where kd_pasien='".$Params["NoMedrec"]."' ");
        /* $kunj = $db->where('kd_unit', $Params["Poli"]);
        
		$kunj = $db->get("kunjungan"); */
        if ($kunj->num_rows() == 0) {
            $baru = 'true';
			$barusqlsrv=1;
        } else {
            $baru = 'false';
			$barusqlsrv=0;
        }
        //akhir cek kunjungan poliklinik
        $Shiftbagian = $this->GetShiftBagian();
        $Shiftbagian = (int) $Shiftbagian;
        $antrian = $this->GetUrutKunjungan($Params["NoMedrec"], $Params["Poli"], date('Y-m-d'));
        $kode_customer = $Params["KdCustomer"];
        $Params["NoMedrec"] = $SchId;
        
        $strError = "";
		$caraNerima='';
		if ($Params["RadioRujukanPasien"] == 'false' || $Params["RadioRujukanPasien"] == false)
		{
			$caraNerima=99;
			$tmpkd_rujuk = 0;
		}
		else
		{
			$caraNerima=$Params["CaraPenerimaan"];
			if ($Params["KdRujukan"] === '' || $Params["KdRujukan"] === 'Pilih Rujukan...') {
            $tmpkd_rujuk = 0;
			} else {
				$tmpkd_rujuk = $Params["KdRujukan"];
			}
		}
        date_default_timezone_set("Asia/Jakarta");
		$JamKunjungan = gmdate("Y-m-d H:i:s", time()+60*60*7);
        //$JamKunjungan = $Params["JamKunjungan"];
		$cari_kd_unit=$this->db->query("select setting from sys_setting where key_data='".$_POST['unitMana']."_default_kd_unit'")->row()->setting;
		$asal_pasien=$this->db->query("select kd_asal from asal_pasien where kd_unit='$cari_kd_unit'")->row()->kd_asal;
        $data = array("kd_pasien" => $Params["NoMedrec"], "kd_unit" => $Params["Poli"], "tgl_masuk" => date('Y-m-d'),
            "urut_masuk" => $antrian, "jam_masuk" => $JamKunjungan, "cara_penerimaan" => $caraNerima, "asal_pasien" =>
            $asal_pasien, "kd_rujukan" =>$tmpkd_rujuk, "kd_dokter" => $Params["KdDokter"],
            "baru" => $baru, "kd_customer" => $Params["KdCustomer"], "shift" => $Shiftbagian, "karyawan" => $Params["Karyawan"],
            "kontrol" => $Params["Kontrol"], "antrian" => $Params["Antrian"], "no_surat" => $Params["NoSurat"],
            "alergi" => $Params["Alergi"],
            "anamnese" => $Params["Anamnese"], "no_sjp" => $Params["NoSjp"]);
        $AppId = $Params["NoMedrec"];
        $Schunit = $Params["Poli"];
        $Schtgl = date('Y-m-d');
        $Schurut = $antrian;
        $criteria = "kd_pasien = '" . $AppId . "' AND kd_unit = '" . $Schunit . "' AND tgl_masuk = '" . $Schtgl . "' ";
        $this->load->model("rawat_jalan/tb_kunjungan_pasien");
        $this->tb_kunjungan_pasien->db->where($criteria, null, false);
        //$query = $this->tb_kunjungan_pasien->GetRowList(0, 1, "", "", "");
		$query = _QMS_QUERY('select * from kunjungan where '.$criteria);
		if (count($query->result()) == 0) {
            //-----------insert to sq1 server Database---------------//
            $datasql = array("kd_pasien" => $Params["NoMedrec"], "kd_unit" => $Params["Poli"],
                "tgl_masuk" => date('Y-m-d'), "urut_masuk" => $antrian, "jam_masuk" => $JamKunjungan,
                "cara_penerimaan" => $caraNerima,
                "asal_pasien" =>$asal_pasien, "kd_rujukan" => $tmpkd_rujuk, "kd_dokter" => $Params["KdDokter"],
                "baru" => $baru, "kd_customer" => $kode_customer, "shift" => 1,
                "karyawan" => $Params["Karyawan"], "kontrol" => $Params["Kontrol"],
                "antrian" => $Params["Antrian"], "no_surat" => $Params["NoSurat"]
			);
            //-----------akhir insert ke database sql server----------------//				
            $datasql["kd_pasien"] = $AppId;
            $data["kd_pasien"] = $AppId;
            $result = $this->tb_kunjungan_pasien->Save($data);
			if ($Params["Kontrol"]=='false'){
				$kontrolsrv=0;
			}else{
				$kontrolsrv=1;
			}
			$datasqlsrv =array("kd_pasien" => $Params["NoMedrec"], "kd_unit" => $Params["Poli"], "tgl_masuk" => date('Y-m-d'),
				"urut_masuk" => $antrian, "jam_masuk" => $JamKunjungan, "cara_penerimaan" => $caraNerima, "asal_pasien" =>$asal_pasien, 
				"kd_rujukan" =>$tmpkd_rujuk,
				"kd_dokter" => $Params["KdDokter"],
				"baru" => $barusqlsrv, "kd_customer" => $Params["KdCustomer"], "shift" => $Shiftbagian, "karyawan" => $Params["Karyawan"],
				"kontrol" => $kontrolsrv, "antrian" => $Params["Antrian"], "no_surat" => $Params["NoSurat"],
				"alergi" => $Params["Alergi"], "no_sjp" => $Params["NoSjp"]);
            _QMS_insert('kunjungan', $datasqlsrv);
            if ($result) {
                if ($Params["KelurahanPJ"] === "" or $Params["KelurahanPJ"] === NULL or
                        $Params["KelurahanPJ"] === "Pilih Kelurahan...") {
                    $intkd_lurahktp = $this->GetKdLurah($kd_Kec = $tmpkecamatan);
                }
                if ($Params["NoSjp"] != "") {
                    $result = $this->db->query("
						INSERT INTO sjp_kunjungan
						(kd_pasien,kd_unit,tgl_masuk,urut_masuk,no_sjp
						) values
						('" . $Params["NoMedrec"] . "','" . $Params["Poli"] . "','" . date('Y-m-d') . "'," . $antrian . ",
						'" . $Params["NoSjp"] . "')	
					");
                }
                if ($Params["NAMA_PJ"] === "" && $Params["KTP"] === "") {
                    $strError = "aya";
                } else {
                    $result = $this->db->query("
						INSERT INTO penanggung_jawab
						(kd_pasien,kd_unit,tgl_masuk,urut_masuk,kd_kelurahan,kd_pekerjaan,
						nama_pj ,alamat ,telepon ,kd_pos,hubungan,alamat_ktp,no_hp,
						kd_pos_ktp ,no_ktp,kd_pendidikan,email,tempat_lahir,tgl_lahir,status_marital,
						kd_agama,kd_kelurahan_ktp,wni,jenis_kelamin
						) values
						('" . $Params["NoMedrec"] . "','" . $Params["Poli"] . "','" . date('Y-m-d') . "'," . $antrian . ",
						" . $Params["KelurahanPJ"] . "," . $Params["kdpekerjaanPj"] . "
						,'" . $Params["NAMA_PJ"] . "','" . $Params["AlamatPJ"] . "','" . $Params["TeleponPj"] . "',
						'" . $Params["PosPJ"] . "'," . $Params["HubunganPj"] . ",'" . $Params["AlamatKtpPJ"] . "',
						'" . $Params["HpPj"] . "','" . $Params["KdPosKtp"] . "','" . $Params["KTP"] . "',
						" . $Params["KdPendidikanPj"] . ",'" . $Params["EmailPenanggungjawab"] . "'
						,'" . $Params["tempatlahirPenanggungjawab"] . "','" . $Params["TgllahirPJ"] . "',
						" . $Params["StatusMaritalPenanggungjawab"] . ",1," . $Params["KelurahanKtpPJ"] . ",
						" . $Params["WNIPJ"] . "," . $Params["JKpenanggungJwab"] . "
					 )");
                    $strError = "aya";
                }
            }
        } else {
            $strError = "eror";
        }
        return $strError;
    }
    public function SimpanTransaksi($Params, $SchId, $Schurut, $notrans) {
        $_kduser = $this->session->userdata['user_id']['id'];
        $strError = "";
        $strcek = "";
        $strcekdata = "";
        $kd_unit = "";
        $appto = "";
        $kd_kasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
        $Params["NoMedrec"] = $SchId;
		$loop=true;
		$now=new DateTime();
		$Schkasir = $this->db->query("select setting from sys_setting where key_data = 'default_kd_kasir_rwj'")->row()->setting;
		while($loop==true){
			$notrans = $this->GetIdTransaksi($kd_kasir);
			//$this->load->model("general/tb_transaksi");
			$criteria = "no_transaksi = '".$notrans."' and kd_kasir = '".$Schkasir."'";
			//$this->tb_transaksi->db->where($criteria, null, false);
			//$query = $this->tb_transaksi->GetRowList(0, 1, "", "", "");
			$query = _QMS_QUERY('select * from transaksi where '.$criteria)->result();
			$queryPG = $this->db->query('select * from transaksi where '.$criteria)->result();
			if(count($query)==0 && count($queryPG)==0){
				$loop=false;
			}
		}
        // if ($notrans == '') {
            // $notrans = $this->GetIdTransaksi($kd_kasir);
        // } else {
            // $notrans = $notrans;
        // }
		//echo $notrans;
		$now=new DateTime();
        $Schurut = $this->GetAntrian($Params["NoMedrec"], $Params["Poli"], $now->format('Y-m-d'), $Params["KdDokter"]);
        $data = array("kd_kasir" => $kd_kasir,
            "no_transaksi" => $notrans,
            "kd_pasien" => $Params["NoMedrec"],
            "kd_unit" => $Params["Poli"],
            "tgl_transaksi" => $now->format('Y-m-d'),
            "urut_masuk" => $Schurut,
            "tgl_co" => NULL,
            "co_status" => "False",
            "orderlist" => NULL,
            "ispay" => "False",
            "app" => "False",
            "kd_user" => $_kduser,
            "tag" => NULL,
            "lunas" => "False",
            "tgl_lunas" => NULL); 
		$datasqlsrv = array("kd_kasir" => $kd_kasir,
            "no_transaksi" => $notrans,
            "kd_pasien" => $Params["NoMedrec"],
            "kd_unit" => $Params["Poli"],
            "tgl_transaksi" => $now->format('Y-m-d'),
            "urut_masuk" => $Schurut,
            "tgl_co" => NULL,
            "co_status" => 0,
            "orderlist" => NULL,
            "ispay" => 0,
            "app" => 0,
            "kd_user" => $_kduser,
            "tag" => NULL,
            "lunas" => 0,
            "tgl_lunas" => NULL);
        
        $criteria = "no_transaksi = '" . $notrans . "' and kd_kasir = '" . $Schkasir . "'";
        $this->load->model("general/tb_transaksi");
        $this->tb_transaksi->db->where($criteria, null, false);
        //$query = $this->tb_transaksi->GetRowList(0, 1, "", "", "");
		$query = _QMS_QUERY('select * from transaksi where '.$criteria)->result();
        if (count($query) == 0) {
            //-----------insert to sq1 server Database---------------//
            //-----------akhir insert ke database sql server----------------
            //RadioRujukanPasien,PasienBaruRujukan
            $data["no_transaksi"] = $notrans;
            _QMS_insert('transaksi', $datasqlsrv);
            $result = $this->tb_transaksi->Save($data);
            $strError = "sae ".$notrans;
            if ($result) {
				//echo 'fyufuy';
                $querygetappto = $this->db->query("Select getappto(False," . $Params['PasienBaruRujukan'] . "," . $Params['RadioRujukanPasien'] . ",False,False,'".$Params['JenisRujukan']."')")->result();
                $sqlPasienBaruRujukan=1;
				if($Params['PasienBaruRujukan']=='false'){
					$sqlPasienBaruRujukan=0;
				}
				$sqlRadioRujukanPasien=1;
				if($Params['RadioRujukanPasien']=='false'){
					$sqlRadioRujukanPasien=0;
				}
				$sqlJenisRujukan=1;
				if($Params['JenisRujukan']=='false'){
					$sqlJenisRujukan=0;
				}
				//echo 'fyufuy';
				$querygetapptoSQL = _QMS_Query("Select dbo.GetAppTo(0," . $sqlPasienBaruRujukan . "," . $sqlRadioRujukanPasien . ",0,0,'".$Params['JenisRujukan']."')  AS appto")->row();
                foreach ($querygetappto as $getappto) 
                {
                    $hasilgetappto = $getappto->getappto;
                }
				$hasilgetapptoSql=$querygetapptoSQL->appto;
                if ($hasilgetappto != "") {
                    $querygetappto = $this->db->query("Select getappto(False," . $Params['PasienBaruRujukan'] . "," . $Params['RadioRujukanPasien'] . ",False,False,'".$Params['JenisRujukan']."')")->result();
                    foreach ($querygetappto as $getappto) {
                        $hasilgetappto = $getappto->getappto;
                    }
                    $Shiftbagian = $this->GetShiftBagian();
                    $Shiftbagian = (int) $Shiftbagian;
                    $kdtarifcus = $this->db->query("Select getkdtarifcus('" . $Params['KdCustomer'] . "')")->result();
                    foreach ($kdtarifcus as $getkdtarifcus) {
                        $KdTarifpasien = $getkdtarifcus->getkdtarifcus;
                    }
					$gettglberlaku = $this->db->query("Select gettanggalberlakufromklas('$KdTarifpasien','" . date('Y-m-d') . "','" . date('Y-m-d') . "','71')")->result();
                    foreach ($gettglberlaku as $gettanggalberlakufromklas) {
                        $Tglberlaku_pasien = $gettanggalberlakufromklas->gettanggalberlakufromklas;
                    }
					if($Tglberlaku_pasien==""||$Tglberlaku_pasien==""){
						$Tglberlaku_pasien=date('Y-m-d');
					}
					$sql="INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
						select  
							'$Schkasir',
							'$notrans',
							row_number() OVER () as rnum,
							'" . date('Y-m-d') . "',
							" . $_kduser . ",
							rn.kd_tarif,
							rn.kd_produk,
							rn.kd_unit,
							rn.tglberlaku,
							'true',
							'true',
							'A'
							,1,
							rn.tarifx,
							" . $Shiftbagian . ",
							'false',
							''  
							from(
							Select AutoCharge.appto,
							tarif.kd_tarif,
							AutoCharge.kd_produk,
							AutoCharge.kd_unit,
							max (tarif.tgl_berlaku) as tglberlaku,
							tarif.tarif as tarifx,
							row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
							From AutoCharge inner join tarif on 
							tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
							inner join produk on produk.kd_produk = tarif.kd_produk  
							Where 
							AutoCharge.kd_unit='" . $Params["Poli"] . "'
							and AutoCharge.appto in $hasilgetappto
							and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
							and tgl_berlaku <= '$Tglberlaku_pasien'
							group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku 
							order by tglberlaku desc
							) as rn
							where rn = 1";
//echo $sql;
/* 						echo "select  
							'$Schkasir',
							'$notrans',
							row_number() OVER () as rnum,
							'" . date('Y-m-d') . "',
							" . $_kduser . ",
							rn.kd_tarif,
							rn.kd_produk,
							rn.kd_unit,
							rn.tglberlaku,
							'true',
							'true',
							'A'
							,1,
							rn.tarifx,
							" . $Shiftbagian . ",
							'false',
							''  
							from(
							Select AutoCharge.appto,
							tarif.kd_tarif,
							AutoCharge.kd_produk,
							AutoCharge.kd_unit,
							max (tarif.tgl_berlaku) as tglberlaku,
							tarif.tarif as tarifx,
							row_number() over(partition by AutoCharge.kd_produk order by tarif.tgl_berlaku desc) as rn
							From AutoCharge inner join tarif on 
							tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
							inner join produk on produk.kd_produk = tarif.kd_produk  
							Where 
							left(AutoCharge.kd_unit,1)=left('" . $Params["Poli"] . "',1)
							and AutoCharge.appto in $hasilgetappto
							and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
							and tgl_berlaku <= '$Tglberlaku_pasien'
							group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,tarif.tgl_berlaku order by tglberlaku desc asc
							) as rn
							where rn = 1"; */
							
                    $queryinsertdetailtransaksi = $this->db->query($sql);
                    $sql_SS="INSERT INTO detail_transaksi
						(kd_kasir, no_transaksi, urut,tgl_transaksi, kd_user, kd_tarif,kd_produk,kd_unit,
						tgl_berlaku,charge,adjust,folio,qty,harga,shift,tag,no_faktur)
						select top 1 '$Schkasir',
						'$notrans',1, '" . date('Y-m-d') . "',
						" . $_kduser . ", rn.kd_tarif, 
						rn.kd_produk, rn.kd_unit, rn.tglberlaku, 1, 1, 'A' ,1, rn.tarifx, " . $Shiftbagian . ", 0, '' 
						from( 
						Select AutoCharge.appto, tarif.kd_tarif, AutoCharge.kd_produk, 
						AutoCharge.kd_unit, max (tarif.tgl_berlaku) as tglberlaku, tarif.tarif as tarifx
						From AutoCharge inner join tarif on tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit 
						inner join produk on produk.kd_produk = tarif.kd_produk Where 
						AutoCharge.kd_unit='" . $Params["Poli"] . "'
						and AutoCharge.appto in $hasilgetapptoSql
						and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
						and tgl_berlaku <= '$Tglberlaku_pasien'
						group by AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,tarif.kd_tarif,tarif.tarif,AutoCharge.appto,
						tarif.tgl_berlaku
						)
						as rn order by tglberlaku desc";
 //echo $sql_SS;
                    _QMS_Query($sql_SS);
                    $query = $this->db->query("
						select * from(
						Select row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
						tarif.kd_tarif,
						AutoCharge.kd_produk,
						AutoCharge.kd_unit,
						max (tarif.tgl_berlaku) as tglberlaku,
						max(tarif.tarif) as tarifx
						From AutoCharge inner join tarif on 
						tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
						inner join produk on produk.kd_produk = tarif.kd_produk  
						Where AutoCharge.kd_unit='" . $Params["Poli"] . "'
						and AutoCharge.appto in $hasilgetappto
						and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
						and tgl_berlaku <= '$Tglberlaku_pasien'
						group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
						tarif.kd_tarif order by AutoCharge.kd_produk asc) as  rn where rn.rnum=1");
						// echo "
						// select * from(
						// Select row_number() OVER (ORDER BY AutoCharge.kd_produk )AS rnum,
						// tarif.kd_tarif,
						// AutoCharge.kd_produk,
						// AutoCharge.kd_unit,
						// max (tarif.tgl_berlaku) as tglberlaku,
						// max(tarif.tarif) as tarifx
						// From AutoCharge inner join tarif on 
						// tarif.kd_produk=autoCharge.kd_Produk and tarif.kd_unit=autoCharge.kd_unit
						// inner join produk on produk.kd_produk = tarif.kd_produk  
						// Where AutoCharge.kd_unit='" . $Params["Poli"] . "'
						// and AutoCharge.appto in $hasilgetappto
						// and LOWER(kd_tarif)=LOWER('$KdTarifpasien')
						// and tgl_berlaku <= '$Tglberlaku_pasien'
						// group by  AutoCharge.kd_unit,AutoCharge.kd_produk,AutoCharge.shift,
						// tarif.kd_tarif order by AutoCharge.kd_produk asc) as  rn where rn.rnum=1";
                    if ($query->num_rows() > 0){
						$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
						$kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
                        foreach ($query->result() as $row){
                            $kdprodukpasien = $row->kd_produk;
                            $kdTarifpasien = $row->kd_tarif;
                            $tglberlakupasien = $row->tglberlaku;
                            $kd_unitpasein = $row->kd_unit;
                            $urutpasein = $row->rnum;
							$query = $this->db->query("INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								Select '$Schkasir','$notrans',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
								From Tarif_Component 
								Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien");	
							$sqlComponent="INSERT INTO Detail_Component 
								(Kd_Kasir, No_Transaksi, Urut,Tgl_Transaksi,Kd_Component,Tarif, Disc)
								Select '$Schkasir','$notrans',$urutpasein,'" . date('Y-m-d') . "',kd_component, tarif as FieldResult,0
								From Tarif_Component 
								Where KD_Unit='$kd_unitpasein' And Tgl_Berlaku='$tglberlakupasien' And KD_Tarif='$kdTarifpasien' And Kd_Produk=$kdprodukpasien";
								//echo $sqlComponent;
							_QMS_Query($sqlComponent);
							if ($query){
								
								
								
								$trDokter = $this->db->query("insert into detail_trdokter 
									Select '$Schkasir','$notrans',$urutpasein,'".$Params["KdDokter"]."','".date('Y-m-d')."',0,0, 
									tarif as FieldResult,0,0,0 From Tarif_Component
									Where 
									(KD_Unit='$kd_unitpasein' 
									And Tgl_Berlaku='$tglberlakupasien'
									And KD_Tarif='$kdTarifpasien'
									And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasadok')
									 OR   (KD_Unit='$kd_unitpasein' 
									 And Tgl_Berlaku='$tglberlakupasien'
									 And KD_Tarif='$kdTarifpasien'
									 And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasaanas')");
								_QMS_Query("insert into detail_trdokter 
									Select '$Schkasir','$notrans',$urutpasein,'".$Params["KdDokter"]."','".date('Y-m-d')."',0,0, 
									tarif as FieldResult,0,0,0 From Tarif_Component
									Where 
									(KD_Unit='$kd_unitpasein' 
									And Tgl_Berlaku='$tglberlakupasien'
									And KD_Tarif='$kdTarifpasien'
									And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasadok')
									 OR   (KD_Unit='$kd_unitpasein' 
									 And Tgl_Berlaku='$tglberlakupasien'
									 And KD_Tarif='$kdTarifpasien'
									 And Kd_Produk=$kdprodukpasien 
									and kd_component = '$kdjasaanas')");
							}
                        }
                    }
					$paid=$this->db->query("select auto_paid from zusers where kd_user = '".$_kduser."'")->row()->auto_paid;
					if($paid==1 && $Params["KdCustomer"]!= "0000000001"){
						# postgre
						$sqlDetailTransaksi="SELECT * FROM detail_transaksi WHERE kd_kasir='$Schkasir' AND no_transaksi='$notrans'";
						$resDetailTransaksi=$this->db->query($sqlDetailTransaksi)->result();
						$totBayar=0;
						for($i=0,$iLen=count($resDetailTransaksi); $i<$iLen;$i++){
							$totBayar+=((int)$resDetailTransaksi[$i]->qty*(int)$resDetailTransaksi[$i]->harga);
						}
						if($totBayar>0){
							$sqlPay="SELECT kd_pay FROM payment WHERE kd_customer='".$Params["KdCustomer"]."'";
							$resPay=$this->db->query($sqlPay)->row();
							$sql="SELECT inserttrpembayaran('".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
								'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",true,'".$now->format('Y-m-d')."',1)";
							$this->db->query($sql);
							$sql="SELECT insertpembayaran('".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
								'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",true,'".$now->format('Y-m-d')."',1)";
							$this->db->query($sql);
							
							// # sql
							$sql="EXEC dbo.v5_inserttrpembayaran '".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
								'".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",1";
							 _QMS_Query($sql);
							$sql="EXEC dbo.v5_insertpembayaran '".$Schkasir."','".$notrans."',1,'".$now->format('Y-m-d')."','".$_kduser."','".$Params["Poli"]."',
								 '".$resPay->kd_pay."',".$totBayar.",'A',".$Shiftbagian.",1,'".$now->format('Y-m-d')."',1";
								// echo $sql;
							 _QMS_Query($sql);
						}
						
						# ***Update Auto paid***
						# POSTGRE
						$updatedetailtransaksipg = $this->db->query("update detail_transaksi set tag='t' WHERE kd_kasir='$Schkasir' AND no_transaksi='$notrans'");
						$updatedetailbayarpg = $this->db->query("update detail_bayar set tag='t' WHERE kd_kasir='$Schkasir' AND no_transaksi='$notrans'");
						# SQLSERVER
						$updatedetailtransaksisql = _QMS_Query("update detail_transaksi set tag='1' WHERE kd_kasir='$Schkasir' AND no_transaksi='$notrans'");
						$updatedetailbayarsql = _QMS_Query("update detail_bayar set tag='1' WHERE kd_kasir='$Schkasir' AND no_transaksi='$notrans'");
						
						
					}
                }
            }
        }
		$sqlCekReg="select count(kd_pasien) as jum from reg_unit where kd_unit='".$Params["Poli"]."' AND kd_pasien='".$Params["NoMedrec"]."'";
		$arrCekReg=_QMS_QUERY($sqlCekReg)->row()->jum;
		if($arrCekReg==0){
			$sqlReg="select max(no_register) as no from reg_unit where kd_unit='".$Params["Poli"]."'";
			$resReg = _QMS_QUERY($sqlReg)->row()->no;
			$resReg++;
			$arrReg=array(
				'kd_pasien'=>$Params["NoMedrec"],
				'kd_unit'=>$Params["Poli"],
				'no_register'=>$resReg,
				'urut_masuk'=>0
			);
			_QMS_insert('reg_unit', $arrReg);
		}
		if ($Params['KdDiagnosa']<>''){
			$this->saveDiagnosa($Params, $SchId);
		}
        return $strError;
    }
    private function GetAntrian($medrec, $Poli, $Tgl, $Dokter) {
        $retVal = 1;
        //$this->load->model('general/tb_getantrian');
		//$db = $this->load->database('otherdb2',TRUE);
		$res= _QMS_QUERY("select urut_masuk from kunjungan where kd_pasien = '" . $medrec . "' and kd_unit = '" . $Poli . "' and tgl_masuk = '" . $Tgl . "'and kd_dokter = '" . $Dokter . "'", null, false);
        //$res = $this->tb_getantrian->GetRowList(0, 1, "DESC", "no_transaksi", "");
        if (count($res->result()) > 0) {
            $nm = $res->row()->urut_masuk;
            $retVal = $nm;
        }
        return $retVal;
    }
    private function GetIdRWJ() {
        /* $ci =& get_instance();
          $db = $ci->load->database('otherdb',TRUE);
          $sqlcounter = $db->query("select top 1 kd_pasien as kdpas from pasien Where kd_pasien Like '%-%' And isnumeric(replace(kd_pasien,'-',''))=1 Order By kd_pasien desc
          ")->row();
          $sqlvalsql = $sqlcounter->kdpas;
          $sqlvalsql3 = str_replace("-", "",$sqlvalsql);
          $sqlvalsql4 = $sqlvalsql3+1;
          if ($sqlvalsql4<999999)
          {
          $sqlvalsql2=str_pad($sqlvalsql4,8,"0",STR_PAD_LEFT);
          }
          else
          {
          $sqlvalsql2=str_pad($sqlvalsql4,7,"0",STR_PAD_LEFT);
          }
          $getnewmedrecsql = substr($sqlvalsql2, 0,1).'-'.substr($sqlvalsql2,2,2).'-'.substr($sqlvalsql2,4,2).'-'.substr($sqlvalsql2,-2); 
		*/
		$db = $this->load->database('otherdb2',TRUE);
       // $this->load->model('rawat_jalan/getmedrec');
        //$res = $this->getmedrec->GetRowList(0, 1, "DESC", "kd_pasien", "");
        $res = $db->query("Select top 1 replace(kd_pasien,'-','') as KD_PASIEN from pasien Where kd_pasien Like '%-%' Order By kd_pasien desc");
        if (count($res->result()) > 0) {
            $nm = $res->row()->KD_PASIEN; 
            $nomor = (int) $nm + 1;
            if ($nomor < 999999) {
                $retVal = str_pad($nomor, 8, "0", STR_PAD_LEFT);
            } else {
                $retVal = str_pad($nomor, 7, "0", STR_PAD_LEFT);
            }
            $getnewmedrec = substr($retVal, 0, 1) . '-' . substr($retVal, 1, 2) . '-' . substr($retVal, 3, 2) . '-' . substr($retVal, -2);
		} else {
            $strNomor = "0-00-" . str_pad("00-", 2, '0', STR_PAD_LEFT);
            $getnewmedrec = $strNomor . "01";
        }
        return $getnewmedrec;
    }
    private function GetIdTransaksi($kd_kasir) {
        /* $ci =& get_instance();
          $db = $ci->load->database('otherdb',TRUE);
          $sqlcounterkasir = $db->query("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
          $sqlsqlcounterkasir2 = $sqlcounterkasir->counter;
          $sqlnotr = $sqlsqlcounterkasir2+1; */
		//$db = $this->load->database('otherdb2',TRUE);
        $counter =_QMS_QUERY("select counter from kasir where kd_kasir = '$kd_kasir'")->row();
        $no = $counter->counter;
        $retVal = $no + 1;
        /* if($sqlnotr>$retVal)
          {
          $retVal=$sqlnotr;
          } */
        $query = "update kasir set counter=$retVal where kd_kasir='$kd_kasir'";
        $update = $this->db->query($query);
        //-----------insert to sq1 server Database---------------//
        _QMS_query($query);
        //-----------akhir insert ke database sql server----------------//
        return $retVal;
    }
    private function GetShiftBagian() {
        /* 	$ci =& get_instance();
          $db = $ci->load->database('otherdb',TRUE);
          $sqlbagianshift = $db->query("SELECT  CONVERT(VARCHAR(3), shift)AS SHIFTBAG FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row();
          $sqlbagianshift2 = $sqlbagianshift->SHIFTBAG; */
        //$sqlbagianshift2=1;
        $sqlbagianshift = $this->db->query("SELECT   shift FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->shift;
        $lastdate = $this->db->query("SELECT   to_char(lastdate,'YYYY-mm-dd') as lastdate FROM BAGIAN_SHIFT  where KD_BAGIAN='2'")->row()->lastdate;
        $datnow = date('Y-m-d');
        if ($lastdate <> $datnow && $sqlbagianshift === '3') {
            $sqlbagianshift2 = '4';
        } else {
            $sqlbagianshift2 = $sqlbagianshift;
        }
        return $sqlbagianshift2;
    }
    private function GetUrutKunjungan($medrec, $unit, $tanggal) {
        $retVal = 0;
        if ($medrec === "Automatic from the system..." || $medrec === " ") {
            $tmpmedrec = " ";
        } else {
            $tmpmedrec = $medrec;
        }
        /* $this->load->model('general/tb_getantrian');
        $this->tb_getantrian->db->where(" kd_pasien = '" . $tmpmedrec . "' and kd_unit = '" . $unit . "'and tgl_masuk = '" . $tanggal . "'order by urut_masuk desc Limit 1", null, false);
        $res = $this->tb_getantrian->GetRowList(0, 1, "DESC", "no_transaksi", ""); */
		//$db = $this->load->database('otherdb2',TRUE);
        $res =_QMS_QUERY("select top 1 URUT_MASUK from kunjungan where kd_pasien = '" . $tmpmedrec . "' and kd_unit = '" . $unit . "' and tgl_masuk = '" . $tanggal . "' order by urut_masuk desc", null, false);
        //$res = $this->tb_getantrian->GetRowList(0, 1, "DESC", "no_transaksi", "");
        if (count($res->result()) > 0) {
		//if ($res[1] > 0) {
            $nm = $res->row()->URUT_MASUK;
            $nomor = (int) $nm + 1;
            $retVal = $nomor;
        }
        return $retVal;
    }
    public function CekdataAutoCharge($kd_unit, $appto) {
        $Params = "";
        $app = "";
        $pasien = "";
        $strcek = $this->readcekautocharge($Params);
        if ($strcek !== "") {
            $appto = $strcek;
        }
        $strError = "";
        $kd_unit = "'" . "202" . "'";

        try {
            $this->load->model('general/autocharge');
            $this->autocharge->db->where('left(kd_unit,3)=' . $kd_unit . 'and AutoCharge.appto in' . $appto . '');
            $res = $this->autocharge->GetRowList($app, $pasien);
            if ($res[1] > 0) {
                $nm = $res;
            } else {
                $nm = "";
            }
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        return $nm;
    }

    public function CekdataTarif() {
        $Params = "";
        $app = "";
        $pasien = "";
        $strcek = $this->readcekautocharge($Params);
        if ($strcek !== "") {
            $appto = $strcek;
        }
        $strError = "";
        $kd_unit = "'" . "202" . "'";

        try {
            
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        return $nm;
    }

    function readcekautocharge($Params = null) {
        $strError = "";
        $app = "false";
        $pasien = "false";
        $rujukan = "false";
        $kontrol = "false";
        $kartu = "false";
        $jenisrujukan = "1";
        try {
            $strQuery = "Select GetAppTo" . "(" . $app . "," . $pasien . "," . $rujukan . "," . $kontrol . "," . $kartu . "," . $jenisrujukan . "::text" . ")";

            $query = $this->db->query($strQuery);
            $res = $query->result();
            if ($query->num_rows() > 0) {
                foreach ($res as $data) {
                    $curentshift = $data->getappto;
                }
            }
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        return $curentshift;
    }

  //   public function updatestatusantrian($kd_pasien,$kd_unit){		
		// $no_antrian = $this->getNoantrian($kd_unit);
		// $dataupdate = array('no_antrian'=>$no_antrian);
		// $criteria = array('tgl_transaksi'=>date('Y-m-d'),'kd_pasien'=>$kd_pasien,'kd_unit'=>$kd_unit);
		// $this->db->where($criteria);
		// $update = $this->db->update('antrian_poliklinik',$dataupdate);

  //   	$sqldatasrv="UPDATE ANTRIAN_POLIKLINIK  SET NO_ANTRIAN = ".$no_antrian."  WHERE kd_pasien='".$kd_pasien."' and tgl_transaksi='".date('Y-m-d')."' and kd_unit='".$kd_unit."'";
  //       $dbsqlsrv = $this->load->database('otherdb2',TRUE);
  //       $res = $dbsqlsrv->query($sqldatasrv);

  //       _QMS_update('pasien', $datasqlsrv,$criteria);
  //   }
    
  //   public function updatestatusantrian_sedangdilayani(){
  //   	$dataupdate = array('id_status_antrian'=>3);
  //   	$criteria = array('tgl_transaksi'=>$_POST['tgl_masuk'],'kd_pasien'=>$_POST['kd_pasien'],
  //   						'sts_ditemukan'=>'true','kd_unit'=>$_POST['kd_unit']);
  //   	$this->db->where($criteria);
  //   	$update = $this->db->update('antrian_poliklinik',$dataupdate);
    	
  //   	# Update STATUS ANTRIAN PASIEN kunjungan -> SEDANG DILAYANI
  //   	date_default_timezone_set("Asia/Jakarta");
  //   	$JamDilayani = gmdate("Y-m-d H:i:s", time()+60*60*7);
  //   	$data = array('id_status_antrian'=>3,'jam_dilayani'=>$JamDilayani);
  //   	$criteria = array('tgl_masuk'=>$_POST['tgl_masuk'],'kd_pasien'=>$_POST['kd_pasien'],
  //   						'kd_unit'=>$_POST['kd_unit'],'urut_masuk'=>$_POST['urut_masuk']);
  //   	$this->db->where($criteria);
  //   	$updatekunjungan = $this->db->update('kunjungan',$data);
  //   	if($updatekunjungan){
  //   		$this->db->trans_commit();
  //   		echo "{success:true}";
  //   	} else{
  //   		$this->db->trans_rollback();
  //   		echo "{success:false}";
  //   	}
    	
  //   }
    
    function getNoantrian($kd_unit){
    	$no = $this->db->query("select max(no_antrian) as no_antrian from antrian_poliklinik 
			where tgl_transaksi='".date('Y-m-d')."' and kd_unit='".$kd_unit."'
			order by no_antrian desc limit 1");
    	if(count($no->result()) > 0){
    		$no_antrian = $no->row()->no_antrian + 1;
    	} else{
    		$no_antrian = 1;
    	}
    	return $no_antrian;
    }
}
?>			