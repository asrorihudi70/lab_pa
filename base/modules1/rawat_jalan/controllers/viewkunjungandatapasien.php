<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewkunjungandatapasien extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();        
    }	 

    public function index()
    {
        $this->load->view('main/index');
    }

    public function read($Params=null)
    {
        $list=array();
        try
        {
			
            /*$tgl=date('M-d-Y');
			$this->load->model('rawat_jalan/tblviewkunjunganedit');
                         if (strlen($Params[4])!==0)
                        {
							$this->db->where(str_replace("~", "'",$Params[4]. " order by k.tgl_masuk desc limit 50 "  ) ,null, false) ;
							$res = $this->tblviewkunjunganedit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
						}	else{
								 $this->db->where(str_replace("~", "'", "  order by k.tgl_masuk desc") ,null, false) ;
								 $res = $this->tblviewkunjunganedit->GetRowList($Params[0], $Params[1], $Params[3],strtolower($Params[2]), $Params[4]);
								}*/

            $sqldatasrv="
            SELECT
            --TOP 5 k.kd_pasien,
            DISTINCT t.no_transaksi,
            k.kd_pasien,
            p.nama as nama_pasien,p.alamat,u.nama_unit,
            k.kd_unit,k.urut_masuk,k.tgl_masuk, d.nama as dokter ,sjp.no_sjp,
            cus.customer,k.jam_masuk,k.tgl_keluar, t.no_transaksi, t.co_status, kon.customer, d.kd_dokter, 
            t.kd_kasir, 
            kon.customer , ruj.rujukan
            from kunjungan k 
            inner join pasien p on p.kd_pasien=k.kd_pasien 
            inner join transaksi t on t.kd_pasien=k.kd_pasien and t.tgl_transaksi = k.tgl_masuk and t.urut_masuk = k.urut_masuk and t.kd_unit = k.kd_unit 
            left join unit u on k.kd_unit=u.kd_unit 
            left join dokter d on d.kd_dokter=k.kd_dokter left join customer cus on cus.kd_customer=k.kd_customer 
            left join sjp_kunjungan sjp on k.kd_unit=sjp.kd_unit and sjp.kd_pasien=k.kd_pasien AND sjp.tgl_masuk=k.tgl_masuk and sjp.urut_masuk=k.urut_masuk 
            inner join customer kon on kon.kd_customer=k.kd_customer
			inner join rujukan ruj on ruj.kd_rujukan=k.kd_rujukan
			WHERE "; 
            if (strlen($Params[4]) !== 0) {
                $kriteria=str_replace("~", "'", $Params[4] . " order by k.tgl_masuk desc");
            } else {
                $kriteria=str_replace("~", "'", " order by k.tgl_masuk desc");
            }
            //$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            //$res = $dbsqlsrv->query($sqldatasrv.$kriteria);
			$res = $this->db->query($sqldatasrv.$kriteria);
            foreach ($res->result() as $rec){
                $o=array();
                $o['KD_PASIEN']    = $rec->kd_pasien;
                $o['NAMA_PASIEN']  = $rec->nama_pasien;
                $o['ALAMAT']       = $rec->alamat;
                $o['TGL_MASUK']    = date("Y/M/d", strtotime($rec->tgl_masuk));
                //$o['TGL_MASUK']    = $rec->tgl_masuk;
                $o['NAMA_UNIT']    = $rec->nama_unit;
                $o['KD_UNIT']      = $rec->kd_unit;
                $o['DOKTER']       = $rec->dokter;
                $o['URUT']         = $rec->urut_masuk;
                // $o['JAM']          = date("H:i:s", strtotime($rec->jam_masuk));
                $o['JAM']          = date_format(date_create($rec->jam_masuk), 'H:i:s');
                if ($rec->tgl_keluar!= null || strlen($rec->tgl_keluar)>0) {
                    $o['TGL_KELUAR']   = date("Y/M/d", strtotime($rec->tgl_keluar));
                }else{
                    $o['TGL_KELUAR']   = "-";
                }
                $o['NO_SEP']       = $rec->no_sjp;
                $o['CUST']         = $rec->customer;
                $o['NO_TRANSAKSI'] = $rec->no_transaksi;
                $o['KD_DOKTER']    = $rec->kd_dokter;
                $o['KD_KASIR']     = $rec->kd_kasir;
                $o['KD_CUSTOMER']  = $rec->customer;
				$o['RUJUKAN']  	   = $rec->rujukan;
                if($rec->co_status == 'f' || $rec->co_status == false || $rec->co_status == '1' || $rec->co_status == 1){
                    $o['CO_STATUS'] = "Belum";
                }else{
                    $o['CO_STATUS'] = "Sudah";          
                }
                $list[]=$o;
            }

        }
        catch(Exception $o)
        {
           
            echo '{success: false}';
        }


        echo '{success:true, totalrecords:'.$res->num_rows().', ListDataObj:'.json_encode($list).'}';


    }
    
   

}

?>