<?php
class viewkunjunganlist extends MX_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $this->load->view('main/index');
    }
    function read($Params = null) {
		$list=array();
        try {
			$sqldatasrv="SELECT pasien.kd_pasien as kd_pasien, pasien.nama, pasien.nama_keluarga,pasien.alamat FROM pasien 
			where"; 
            if (strlen($Params[4]) !== 0) {
                $kriteria=str_replace("~", "'", $Params[4] . " and pasien.kd_pasien Like '%-%' order by pasien.kd_pasien desc limit ".$Params[0]."");
            } else {
                $kriteria=str_replace("~", "'", " pasien.kd_pasien Like '%-%' order by pasien.kd_pasien desc  limit ".$Params[0]."");
            }
            $res = $this->db->query($sqldatasrv.$kriteria);
			foreach ($res->result() as $rec){
				$o=array();
				$selectKartuPasien = ("SELECT count(kd_pasien) as counter FROM history_cetak_kartu WHERE tgl_cetak = '".date('Y-m-d')."' AND kd_pasien='".$rec->kd_pasien."'");
				$counter = $this->db->query($selectKartuPasien);
				$counter = $counter->row()->counter;
				$o['KD_PASIEN']=$rec->kd_pasien;
				$o['NAMA']=$rec->nama;
				$o['NAMA_KELUARGA']=$rec->nama_keluarga;
				$o['ALAMAT']=$rec->alamat;
				if ($counter > 0) {
					$o['KARTU'] = 't';
				}else{
					$o['KARTU'] = 'f';
				}
				$list[]=$o;	
			} 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }
}
/* class viewkunjunganlist extends MX_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        $this->load->view('main/index');
    }
    function read($Params = null) {
		$list=array();
        try {
			$sqldatasrv="SELECT distinct top ".$Params[0]." KD_PASIEN=pasien.kd_pasien, NAMA=pasien.nama, NAMA_KELUARGA=pasien.nama_keluarga,
			ALAMAT=pasien.alamat
			FROM pasien 
			where"; 
            if (strlen($Params[4]) !== 0) {
                $kriteria=str_replace("~", "'", $Params[4] . " and pasien.kd_pasien Like '%-%'  order by pasien.kd_pasien desc --offset " . $Params[0] . "  limit 500 ");
            } else {
                $kriteria=str_replace("~", "'", " pasien.kd_pasien Like '%-%' order by pasien.kd_pasien desc   --offset " . $Params[0] . " limit 500 ");
            }
			//echo $sqldatasrv.$kriteria;
			$dbsqlsrv = $this->load->database('otherdb2',TRUE);
            $res = $dbsqlsrv->query($sqldatasrv.$kriteria);
			foreach ($res->result() as $rec){
				$o=array();
				$selectKartuPasien = ("SELECT count(kd_pasien) as counter FROM history_cetak_kartu WHERE tgl_cetak = '".date('Y-m-d')."' AND kd_pasien='".$rec->KD_PASIEN."'");
				$counter = $dbsqlsrv->query($selectKartuPasien);
				$counter = $counter->row()->counter;
				$o['KD_PASIEN']=$rec->KD_PASIEN;
				$o['NAMA']=$rec->NAMA;
				$o['NAMA_KELUARGA']=$rec->NAMA_KELUARGA;
				$o['ALAMAT']=$rec->ALAMAT;
				if ($counter > 0) {
					$o['KARTU'] = 't';
				}else{
					$o['KARTU'] = 'f';
				}
				// $o['KARTU']=$rec->cekkartu;
				$list[]=$o;	
			} 
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }
        echo '{success:true, totalrecords:' . count($res) . ', ListDataObj:' . json_encode($list) . '}';
    }
} */
?>			