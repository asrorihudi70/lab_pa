﻿<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class tbl_cmb_dokter extends Model
{
    private $criteria = "";

    function viewunitcmb()
    {
        parent::Model();
        $this->load->database();
    }

    function setCriteria($data){
        $this->criteria = $data;
    }   

    function read($id)
    {
        $this->db->where('kd_dokter', $id);
        $query = $this->db->get('dokter');

        return $query;
    }

    function readAll()
    {    
        /*$this->db->select('DISTINCT(d.nama), d.kd_dokter, d.nama, p.kd_unit, d.jenis_dokter');
        $this->db->from('dokter d');
        $this->db->join('dokter_klinik p', 'd.kd_dokter = p.kd_dokter', 'INNER');
        if (!empty($this->criteria)) {
            $this->db->where($this->criteria);
        }
        $query = $this->db->get();*/
        if (strlen($this->criteria)>0) {
            $query = $this->db->query("
                select DISTINCT(dokter_klinik.kd_dokter), dokter.kd_dokter, dokter.nama, dokter.jenis_dokter, dokter_klinik.kd_unit   
                from dokter INNER JOIN 
                dokter_klinik on dokter_klinik.kd_dokter = dokter.kd_dokter 
            ".$this->criteria."");
        }else{
             $query = $this->db->query("
                select *   
                from dokter");
        }
        return $query;
    }



}


class Rowam_dokter
{
    public $KD_DOKTER;
    public $NAMA;
}

?>