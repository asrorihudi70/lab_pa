﻿<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class tbl_cmb_unit extends Model
{
    private $criteria = "";

    function viewunitcmb()
    {
        parent::Model();
        $this->load->database();
    }

    function setCriteria($data){
        $this->criteria = $data;
    }   

    function read($id)
    {
        $this->db->where('kd_unit', $id);
        $query = $this->db->get('unit');

        return $query;
    }

    function readAll()
    {
    
        $this->db->select('*');
        $this->db->from('unit');
        if (!empty($this->criteria)) {
            $this->db->where($this->criteria);
        }
        $query = $this->db->get();

        return $query;
    }
}

class Row_unit
{

    public $KD_UNIT;
    public $NAMA_UNIT;
}

?>