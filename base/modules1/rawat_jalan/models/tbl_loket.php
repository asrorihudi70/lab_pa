<?php

class tbl_loket extends TblBase
{
    function __construct()
    {
        $this->TblName='loket';
        TblBase::TblBase(true);

        $this->SqlQuery='SELECT * from antrian_list_loket order by kode_loket::integer asc';
    }

    function FillRow($rec)
    {
        $row=new RowviewLoket;
        $row->GROUPPING=$rec->id_group." - ".$rec->nama_loket;
        $row->KODE_LOKET=$rec->kode_loket;
		$row->NAMA_LOKET=$rec->nama_loket;
		$row->ID_GROUP=$rec->id_group;
        
        return $row;
    }
    
    function readAll()
    {
    
        //$this->db->where('parent', '2');
        //$this->db->order_by('status', 'asc');
        $this->db->select('*');
        $this->db->from('loket');
        $query = $this->db->get();

        return $query;
    }

}

class RowviewLoket
{
   public $GROUPPING;
   public $KODE_LOKET;
   public $NAMA_LOKET;  
   public $ID_GROUP;  

}

?>
