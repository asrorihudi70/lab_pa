<?php
class tblviewhistorytrrwj extends TblBase
{
    function __construct()
    {
        $this->TblName='tblviewhistorytrrwj';
        TblBase::TblBase(true);
		$this->StrSql="no_transaksi,tgl_bayar,deskripsi,bayar,user_names,urut_bayar,kd_user,shift";
		
        $this->SqlQuery= "select  * from (
                            select db.kd_kasir,db.no_transaksi,db.shift, db.tgl_transaksi as tgl_bayar, db.kd_pay, db.kd_user, z.user_names, p.jenis_pay, p.uraian as deskripsi, 
                            db.jumlah as bayar, db.urut as urut_bayar
                            from detail_bayar db
                            left join payment p on db.kd_pay = p.kd_pay 
                            left join payment_type pt on p.jenis_pay = pt.jenis_pay 
                            left join zusers z on db.kd_user = z.kd_user::integer
                            order by  db.urut
                            ) as resdata  ";

    }

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirdetailrwj;

        $row->NO_TRANSAKSI = $rec->no_transaksi;
        $row->TGL_BAYAR = $rec-> tgl_bayar;
        $row->DESKRIPSI = $rec->deskripsi;
        $row->BAYAR = $rec->bayar;
        $row->USERNAME = $rec->user_names;
		$row->URUT = $rec->urut_bayar;
		$row->KD_USER = $rec->kd_user;
		$row->SHIFT = $rec->shift;
        return $row;
    }

}

class Rowtblviewkasirdetailrwj
{

    public $NO_TRANSAKSI;
    public $TGL_BAYAR;
    public $DESKRIPSI;
    public $BAYAR;
    public $USERNAME;
    public $URUT;
	public $KD_USER;
	public $SHIFT;
	
}

?>
