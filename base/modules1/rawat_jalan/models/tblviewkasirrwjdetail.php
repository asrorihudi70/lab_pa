<?php
class tblviewkasirrwjdetail extends TblBase
{
	/* 
		EDIT 	: MSD
		TGL 	: 10-03-2017
		KET		: TAMBAH SELECT FIELD TMP_TGL_TRANSAKSI UNTUK KEPERLUAN UGD
	*/
    function __construct()
    {
        $this->TblName='viewgridkasirrwjdetail';
        TblBase::TblBase(true);
		$this->StrSql="kd_produk,deskripsi,deskripsi,harga,flag,qty,tgl_berlaku,no_transaksi,urut,adjust,kd_dokter,kd_unit,cito,kd_customer,tgl_transaksi,tmp_tgl_transaksi,total,tunai,dicount,Piutang,jumlah_dokter, folio, kd_klas, manual";
		
		$kdjasadok  = $this->db->query("select setting from sys_setting where key_data = 'pel_jasa_dok'")->row()->setting;
		 $kdjasaanas = $this->db->query("select setting from sys_setting where key_data = 'pel_JasaDokterAnestasi'")->row()->setting;
		
		
        $this->SqlQuery= "SELECT * from ( SELECT 
			produk.kp_produk,
			produk.kd_klas,
			produk.manual,
			detail_transaksi.kd_kasir, 
			detail_transaksi.urut, 
			detail_transaksi.no_transaksi, 
			detail_transaksi.tgl_transaksi, 
			detail_transaksi.tgl_transaksi as tmp_tgl_transaksi, 
			detail_transaksi.kd_user, 
			detail_transaksi.kd_tarif, 
			detail_transaksi.kd_produk, 
			detail_transaksi.tgl_berlaku, detail_transaksi.kd_unit, detail_transaksi.charge, detail_transaksi.adjust, detail_transaksi.folio, detail_transaksi.harga, detail_transaksi.qty, detail_transaksi.shift, detail_transaksi.kd_dokter, 
			detail_transaksi.kd_unit_tr, detail_transaksi.cito, detail_transaksi.js, detail_transaksi.jp, detail_transaksi.no_faktur, detail_transaksi.flag, detail_transaksi.tag, detail_transaksi.hrg_asli ,
			detail_transaksi.harga * detail_transaksi.qty as total,d.jumlah_dokter, produk.deskripsi, detail_transaksi.folio
			from detail_transaksi inner join produk on detail_transaksi.kd_produk = produk.kd_produk 
				left join (
					select count(kd_dokter) as jumlah_dokter,
					no_transaksi,urut,kd_kasir,tgl_transaksi 
					from detail_trdokter 
					group by no_transaksi,urut,kd_kasir,tgl_transaksi
					) as d ON d.urut = detail_transaksi.urut And detail_transaksi.tgl_transaksi = d.tgl_transaksi AND detail_transaksi.kd_kasir = d.kd_kasir and d.no_transaksi= detail_transaksi.no_transaksi 
		) as resdata ";
	}

    function FillRow($rec)
    {
        $row=new Rowtblviewkasirrwjdetail;

        $row->KP_PRODUK = $rec->kp_produk;
        $row->KD_PRODUK = $rec->kd_produk;
        $row->KD_KLAS 	= $rec->kd_klas;
        $row->MANUAL 	= $rec->manual;
        $row->DESKRIPSI = $rec->deskripsi;
        $row->KD_TARIF = $rec->kd_tarif;
        $row->DESKRIPSI2 = $rec->deskripsi;
		$text = strtolower($rec->deskripsi);
		if (stripos($text, "konsul") !== false) {
				$row->STATUS_KONSULTASI	= true;
			}else{
				$row->STATUS_KONSULTASI = false;
			}
        $row->HARGA = $rec->harga;
        $row->FLAG = $rec->flag;
        $row->QTY = $rec->qty;
        $row->TGL_BERLAKU = $rec->tgl_berlaku;
        $row->NO_TRANSAKSI= $rec->no_transaksi;
        $row->URUT = $rec->urut;
        $row->ADJUST = $rec->adjust;
        $row->KD_DOKTER = $rec->kd_dokter;
        $row->KD_UNIT= $rec->kd_unit;
        $row->CITO = $rec->cito;
        //$row->KD_CUSTOMER = $rec->kd_customer;
		$row->TGL_TRANSAKSI = $rec->tgl_transaksi;
		$row->TMP_TGL_TRANSAKSI = $rec->tmp_tgl_transaksi;
		$row->TOTAL = $rec->total;
		$row->FOLIO = $rec->folio;
		//$row->BAYARTR = $rec->tunai;
		//$row->PIUTANG = $rec->piutang;
		//$row->DISCOUNT = $rec->dicount;
		$row->JUMLAH_DOKTER = $rec->jumlah_dokter;
		if ($rec->jumlah_dokter > 0) {
			$row->STATUS_PENINDAK = true;
		}else{
			$row->STATUS_PENINDAK = false;
		}
		//$row->JUMLAH = $rec->jumlah;
		
        return $row;
    }

}

class Rowtblviewkasirrwjdetail
{

    public $KP_PRODUK;
    public $KD_PRODUK;
    public $MANUAL;
    public $KD_KLAS;
    public $DESKRIPSI;
    public $KD_TARIF;
    public $DESKRIPSI2;
	public $STATUS_KONSULTASI;
    public $HARGA;
    public $FLAG;
    public $QTY;
    public $TGL_BERLAKU;
    public $NO_TRANSAKSI;
    public $URUT;
    public $ADJUST;
    public $KD_DOKTER;
    public $KD_UNIT;
    public $CITO;
    public $KD_CUSTOMER;
	public $TGL_TRANSAKSI;
	public $TMP_TGL_TRANSAKSI;
	public $TOTAL;
	public $BAYARTR;
	public $DISCOUNT;
	public $PIUTANG;
	public $FOLIO;
	public $JUMLAH_DOKTER;
	public $JUMLAH;
}

?>
