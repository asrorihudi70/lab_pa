﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewtrrwj extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="customer,no_transaksi,kd_unit,nama_unit,nama,alamat,kode_pasien,kd_bagian,tgl_transaksi,kd_dokter,nama_dokter,kd_customer,urut_masuk,
					kd_kasir,asal_pasien,cara_penerimaan,no_urut'";
		$this->SqlQuery="SELECT  * from (
											
											select customer.customer, unit.kd_unit, unit.kd_unit as tmp_kd_unit, unit.nama_unit,unit.kd_bagian, pasien.nama, pasien.alamat, kunjungan.kd_pasien as kode_pasien, transaksi.tgl_transaksi, 
											case when dokter.nama = '0' then 'BELUM DIPILIH' else dokter.nama end as nama_dokter, 
											kunjungan.*,  transaksi.no_transaksi, transaksi.kd_kasir, transaksi.co_status,  unit.kd_kelas, dokter.kd_dokter, transaksi.orderlist,transaksi.posting_transaksi, 
											knt.jenis_cust,
											case 
												when ap.id_status_antrian=0 then 'Tunggu Berkas' 
												when ap.id_status_antrian=1 then 'Tunggu Panggilan' 
												when ap.id_status_antrian=2 then 'Sedang Dilayani' 
												else 'Selesai' end as status_antrian, 
											ap.no_antrian,ap.no_urut,ap.id_status_antrian as ap_status_antrian
											from (((((kunjungan  
											inner join unit on kunjungan.kd_unit=unit.kd_unit)   
											left join pasien on pasien.kd_pasien=kunjungan.kd_pasien) 
											inner join dokter on dokter.kd_dokter=kunjungan.kd_dokter)   
											inner join customer on customer.kd_customer= kunjungan.kd_customer)
											left join kontraktor knt on knt.kd_customer=kunjungan.kd_customer)
											  
											inner join transaksi  on transaksi.kd_pasien = kunjungan.kd_pasien and transaksi.kd_unit =kunjungan.kd_unit 
											and transaksi.tgl_transaksi=kunjungan.tgl_masuk  and transaksi.urut_masuk=kunjungan.urut_masuk
											left join antrian_poliklinik ap on kunjungan.kd_pasien=ap.kd_pasien and kunjungan.tgl_masuk=ap.tgl_transaksi and kunjungan.kd_unit=ap.kd_unit
										)as resdata ";
		$this->TblName='viewkasirrwj';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->NAMA=$rec->nama;
		$row->ALAMAT=$rec->alamat;
		$row->KD_PASIEN=$rec->kode_pasien;
		$row->TANGGAL_TRANSAKSI=$rec->tgl_transaksi;
		$row->NAMA_DOKTER=$rec->nama_dokter;
		$row->NO_TRANSAKSI=$rec->no_transaksi;
		$row->KD_CUSTOMER=$rec->kd_customer;
		$row->CUSTOMER=$rec->customer;
		$row->KD_UNIT=$rec->kd_unit;
		$row->KD_DOKTER=$rec->kd_dokter;
		$row->URUT_MASUK=$rec->urut_masuk;
		$row->POSTING_TRANSAKSI=$rec->posting_transaksi;
		$row->ANAMNESE = $rec->anamnese;
		$row->CAT_FISIK = $rec->cat_fisik;
		$row->KD_KASIR = $rec->kd_kasir;
		$row->ASAL_PASIEN = $rec->asal_pasien;
		$row->CARA_PENERIMAAN = $rec->cara_penerimaan;
		$row->STATUS_ANTRIAN = $rec->status_antrian;
		$row->NO_ANTRIAN = $rec->no_urut;
		return $row;
	}
}
class Rowdokter
{
	public $KD_UNIT;
    public $NAMA_UNIT;
    public $NAMA;
	public $ALAMAT;
    public $KD_PASIEN;
	public $TANGGAL_TRANSAKSI;
	public $NAMA_DOKTER;
	public $NO_TRANSAKSI;
	public $KD_CUSTOMER;
	public $CUSTOMER;
	public $KD_DOKTER;
	public $URUT_MASUK;
	public $POSTING_TRANSAKSI;
	public $ANAMNESE;
	public $CAT_FISIK;
	public $KD_KASIR;
	public $ASAL_PASIEN;
	public $CARA_PENERIMAAN;
	public $STATUS_ANTRIAN;
	public $NO_ANTRIAN;
}

?>