<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class Lap_penerimaan extends MX_Controller {

    public function __construct(){
        parent::__construct();
             $this->load->library('session');
    }	 

	public function index(){
        $this->load->view('main/index');            		
   	}
   	
   	public function getDokter(){
   		$result=$this->db->query("select distinct(dk.kd_dokter),d.nama
		from dokter_klinik dk
		inner join dokter d on dk.kd_dokter=d.kd_dokter
		where d.nama not in('-')
		order by d.nama")->result();
   		$jsonResult['processResult']='SUCCESS';
   		$jsonResult['data']=$result;
   		echo json_encode($jsonResult);
   	}
   
   	public function printData(){
   		$kd_rs=$this->session->userdata['user_id']['kd_rs'];
   		$rs=$this->db->query("SELECT * FROM db_rs WHERE code='".$kd_rs."'")->row();
   		$td_t_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left'")->row()->setting;
   		$td_t_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right'")->row()->setting;
   		$td_t_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center'")->row()->setting;
   		
   		$td_n_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_name'")->row()->setting;
   		$td_n_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_name'")->row()->setting;
   		$td_n_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_name'")->row()->setting;
   		
   		$td_i_left=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_left_nip'")->row()->setting;
   		$td_i_right=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_right_nip'")->row()->setting;
   		$td_i_center=$this->db->query("SELECT * FROM sys_setting WHERE key_data='rwj_report_ttd_center_nip'")->row()->setting;
   		
   		$this->load->library('m_pdf');
   		$this->m_pdf->load();
   		$mpdf= new mPDF('utf-8', 'A4');
   		$mpdf->SetDisplayMode('fullpage');
   		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
   		$mpdf->pagenumPrefix = 'Hal : ';
   		$mpdf->pagenumSuffix = '';
   		$mpdf->nbpgPrefix = ' Dari ';
   		$mpdf->nbpgSuffix = '';
   		$date = date("d-M-Y / H:i");
   		$arr = array (
   				'odd' => array (
   						'L' => array (
   								'content' => 'Operator : (0) Admin',
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'C' => array (
   								'content' => "Tgl/Jam : ".$date."",
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'R' => array (
   								'content' => '{PAGENO}{nbpg}',
   								'font-size' => 8,
   								'font-style' => '',
   								'font-family' => 'serif',
   								'color'=>'#000000'
   						),
   						'line' => 0,
   						),
   						'even' => array ()
   				);
   		$mpdf->SetFooter($arr);
   		$mpdf->SetTitle('LAP PENERIMAAN (Per Pasien Per Jenis Penerimaan)');
   		$q_dokter='';
   		if(isset($_POST['kd_dokter'])){
   			$q_dokter="AND k.kd_dokter='".$_POST['kd_dokter']."' ";
   		}
   		$q_jenis='';
   		if($_POST['pasien']>=0 && $_POST['pasien']!='Semua'){
   			$q_jenis="AND Ktr.Jenis_cust=".$_POST['pasien']." ";
   		}
   		$q_customer='';
   		if(isset($_POST['kd_customer'])){
   			$q_customer="AND Ktr.kd_customer='".$_POST['kd_customer']."' ";
   		}
   		$q_poliklinik='';
   		if(isset($_POST['kd_klinik'])){
   			$q_poliklinik="AND t.kd_unit='".$_POST['kd_klinik']."' ";
   		}
   		$q_shift='';
   		$t_shift='';
   		if($_POST['shift0']=='true'){
   			$q_shift="And ((db.tgl_transaksi between '".$_POST['start_date']."'  And '".$_POST['last_date']."'  And db.Shift In (1,2,3))		
			Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."'  And '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."'  And db.Shift=4) )	";
   			$t_shift='SEMUA KELOMPOK PASIEN SHIFT 1,2,3';
   		}else{
   			if($_POST['shift1']=='true' || $_POST['shift2']=='true' || $_POST['shift3']=='true'){
   				$s_shift='';
   				if($_POST['shift1']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='1';
   				}
   				if($_POST['shift2']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='2';
   				}
   				if($_POST['shift3']=='true'){
   					if($s_shift!='')$s_shift.=',';
   					$s_shift.='3';
   				}
   				$q_shift.="db.tgl_transaksi between '".$_POST['start_date']."'  And '".$_POST['last_date']."'  And db.Shift In (".$s_shift.")";
   				if($_POST['shift3']=='true'){
   					$q_shift="(".$q_shift." Or  (db.Tgl_Transaksi between '".date('Y-m-d', strtotime($_POST['start_date'] . ' +1 day'))."'  And '".date('Y-m-d', strtotime($_POST['last_date'] . ' +1 day'))."'  And db.Shift=4) )	";
   				}
   				$t_shift='SEMUA KELOMPOK PASIEN SHIFT '.$s_shift;
   				$q_shift="And ".$q_shift;
   			}
   		}
   		$queri="Select db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, Max(ps.Nama) AS Nama,  
	case when py.jenis_pay =2 then textcat(py.Uraian,COALESCE(db.no_kartu,''))  else py.Uraian END AS uraian,  
	case when max(py.Kd_pay) in ('IA','TU') Then Sum(Jumlah) Else 0 end AS UT,  
	case when max(py.Kd_pay) in ('00','D1') Then Sum(Jumlah) Else 0 end AS SSD,  
	case when max(py.Kd_pay) not in ('IA','TU','00','D1','KR') Then Sum(Jumlah) Else 0 end AS PT,  
	case when max(py.jenis_pay) in ('2') Then Sum(Jumlah) Else 0 end AS KR
From (Transaksi t 
	INNER JOIN Detail_Bayar db On t.KD_kasir=db.KD_kasir And t.No_Transaksi=db.No_Transaksi) 
	INNER JOIN Payment py On py.Kd_pay=db.Kd_Pay  
	INNER JOIN  Kunjungan k on k.kd_pasien=t.kd_Pasien And k.Kd_Unit=t.Kd_Unit And t.Urut_masuk=k.Urut_masuk And k.tgl_masuk=t.tgl_Transaksi  
	INNER JOIN  Pasien ps on k.kd_pasien=ps.kd_Pasien  
	LEFT JOIN Kontraktor ktr On ktr.kd_Customer=k.Kd_Customer  
   	INNER JOIN unit u On u.kd_unit=t.kd_unit  
Where 
	 t.ispay='1' AND u.kd_bagian='2'	
   	".$q_poliklinik."	
	".$q_shift."			
	".$q_jenis."	
	".$q_customer."	
	".$q_dokter."
Group By db.No_Transaksi, t.Tgl_Transaksi, t.kd_Pasien, py.Uraian ,db.no_kartu ,py.jenis_pay  
Order By py.Uraian, t.Tgl_Transaksi, t.kd_Pasien, max(db.Urut) ";
   		//echo $queri;
   		
   		$result=$this->db->query($queri)->result();
   		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
   		$mpdf->pagenumPrefix = 'Hal : ';
   		$mpdf->pagenumSuffix = '';
   		$mpdf->nbpgPrefix = ' Dari ';
   		$mpdf->nbpgSuffix = '';
   		$mpdf->WriteHTML("
           <style>
   				
           </style>
           ");
   		 $telp='';
   		 $fax='';
   		 if(($rs->phone1 != null && $rs->phone1 != '')|| ($rs->phone2 != null && $rs->phone2 != '')){
   		 	$telp='<br>Telp. ';
   		 	$telp1=false;
   		 	if($rs->phone1 != null && $rs->phone1 != ''){
   		 		$telp1=true;
   		 		$telp.=$rs->phone1;
   		 	}
   		 	if($rs->phone2 != null && $rs->phone2 != ''){
   		 		if($telp1==true){
   		 			$telp.='/'.$rs->phone2.'.';
   		 		}else{
   		 			$telp.=$rs->phone2.'.';
   		 		}
   		 	}else{
   		 		$telp.='.';
   		 	}
   		 }
   		 if($rs->fax != null && $rs->fax != ''){
   		 	$fax='<br>Fax. '.$rs->fax.'.';
   		 	
   		 }
   		$mpdf->WriteHTML("
   				<table style='width: 1000px;font-size: 15;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tbody>
   					<tr>
   						<td align='left' colspan='2'>
   							<table  cellspacing='0' border='0'>
   								<tr>
   									<td>
   										<img src='./ui/images/Logo/LOGO RSBA.png' width='76' height='74' />
   									</td>
   									<td>
   										<b>".$rs->name."</b><br>
			   							<font style='font-size: 11px;'>".$rs->address."</font>
			   							<font style='font-size: 11px;'>".$telp."</font>
			   							<font style='font-size: 11px;'>".$fax."</font>
   									</td>
   								</tr>
   							</table>
   						</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'></td>
   					</tr>
   					<tr>
   						<th colspan='2' align='center' style='font-weight: bold;'>LAPORAN PENERIMAAN (Per Pasien Per Jenis Penerimaan)</th>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;font-size: 12px;'>".date('d M Y', strtotime($_POST['start_date']))." s/d ".date('d M Y', strtotime($_POST['last_date']))."</td>
   					</tr>
   					<tr>
   						<td colspan='2' align='center' style='font-weight: bold;font-size: 12px;'>".$t_shift."</td>
   					</tr>
   				</tbody>
   			</table><br>
   							<table border='1' style='width: 1000px;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;font-size: 15;'>
   								<thead>
   									<tr>
   										<th rowspan='2' width='40' align='center'>NO.</th>
				   						<th rowspan='2' width='80'>NO. TRANSAKSI</th>
				   						<th rowspan='2' width='80'>NO. MEDREC</th>
				   						<th rowspan='2' width='250'>NAMA PASIEN</th>
				   						<th colspan='4'>JENIS PENERIMAAN</th>
				   						<th rowspan='2' width='100' align='right'>JUMLAH</th>
   									</tr>
									<tr>
   										<th width='100'>TUNAI</th>
				   						<th width='100'>PIUTANG</th>
				   						<th width='100' >SUBSIDI RS</th>
				   						<th width='100'>CREDIT CARD</th>
   									</tr>
   								</thead>
   								
   				");
   			$jum=0;
   			$ut=0;
   			$ssd=0;
   			$pt=0;
   			$kr=0;
   			for($i=0; $i<count($result); $i++){
   				$j=$result[$i]->ut+$result[$i]->ssd+$result[$i]->pt+$result[$i]->kr;
   				$ut+=$result[$i]->ut;
   				$ssd+=$result[$i]->ssd;
   				$pt+=$result[$i]->pt;
   				$kr+=$result[$i]->kr;
   				$jum+=$j;
   				$mpdf->WriteHTML("<tr>
   						<td align='center'>".($i+1)."</td>
   						<td align='center'>".$result[$i]->no_transaksi."</td>
   						<td align='center'>".$result[$i]->kd_pasien."</td>
   						<td>".$result[$i]->nama."</td>
   						<td align='right'>".number_format($result[$i]->ut,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->pt,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->ssd,0,',','.')."</td>
						<td align='right'>".number_format($result[$i]->kr,0,',','.')."</td>
   						<td align='right'>".number_format($j,0,',','.')."</td>
   					</tr>");
   			}
			//<td>".$result[$i]->uraian."</td>
   			if(count($result)==0){
   				$mpdf->WriteHTML("<tr>
   						<td align='center' colspan='6'>Tidak Ada Data</td>
   					</tr>");
   			}
   			$mpdf->WriteHTML("
				   					<tr>
				   						<td align='right' colspan='4' style='font-weight: bold;'>JUMLAH &nbsp;</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($ut,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($pt,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($ssd,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($kr,0,',','.')."</td>
				   						<td align='right' style='font-weight: bold;'>".number_format($jum,0,',','.')."</td>
				   					</tr>
				   					<tbody>
					   			</tbody>
					   		</table>
					 <table style='width: 1000px;font-size: 14;font-family: Arial, Helvetica, sans-serif;' cellspacing='0' border='0'>
   					<tr>
   						<td colspan='2'>&nbsp;</td>
   					</tr>
   					<tr>
   						<td>&nbsp;</td>
   						<td align='center'>".$rs->city.", ".date("d M Y")."</td>
   					</tr>
   					<tr>
   						<td align='center'>".$td_t_left."</td>
   						<td align='center'>".$td_t_right."</td>
   					</tr>
   					<tr>
   						<td colspan='2'>&nbsp;</td>
   					</tr>
   					<tr>
   						<td colspan='2'>&nbsp;</td>
   					</tr>
   					<tr>
   						<td colspan='2'>&nbsp;</td>
   					</tr>
   					<tr>
   						<td align='center'>".$td_n_left."</td>
   						<td align='center'>".$td_n_right."</td>
   					</tr>
   					<tr>
   						<td align='center'><span  style='border-top: 1px solid black;'>".$td_i_left."</span></td>
   						<td align='center'><span  style='border-top: 1px solid black;'>".$td_i_right."</span></td>
   					</tr>
   					<tr>
   						<td>&nbsp;</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'>".$td_t_center."</td>
   					</tr>
   					<tr>
   						<td>&nbsp;</td>
   					</tr>
   					<tr>
   						<td>&nbsp;</td>
   					</tr>
   					<tr>
   						<td>&nbsp;</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'>".$td_n_center."</td>
   					</tr>
   					<tr>
   						<td align='center' colspan='2'><span  style='border-top: 1px solid black;'>".$td_i_center."</span></td>
   					</tr>");
   			$mpdf->WriteHTML("</tbody></table>");
   		$tmpbase = 'base/tmp/';
   		$datenow = date("dmY");
   		$tmpname = time().'LapPenerimaanRWJ';
   		$mpdf->Output($tmpbase.$datenow.$tmpname.'.pdf', 'F');
   		$jsonResult['processResult']='SUCCESS';
   		//$jsonResult['list']=$result;
   		$jsonResult['data']=base_url().$tmpbase.$datenow.$tmpname.'.pdf';
   		echo json_encode($jsonResult);
   	}
}
?>