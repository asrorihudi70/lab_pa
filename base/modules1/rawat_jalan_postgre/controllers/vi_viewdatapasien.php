<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class vi_viewdatapasien extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/index');
    }

    function read($Params = null) {
        try {
            $this->load->model('rawat_jalan/tbl_datapasien');
            if (strlen($Params[4]) !== 0) {
                $this->db->where(str_replace("~", "'", $Params[4] . "limit 50"), null, false);
                $res = $this->tbl_datapasien->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
            } else {
                $this->db->where(str_replace("~", "'", "pr.KD_PROPINSI=27 and kd_pasien Like '%-%' 
						   order by pasien.kd_pasien desc limit 50"), null, false);
                $res = $this->tbl_datapasien->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
            }
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:' . $res[1] . ', ListDataObj:' . json_encode($res[0]) . '}';
    }

    public function save($Params = null) {
        $mError = "";
        $parametersave = $Params['TMPPARAM'];
        if ($parametersave === '0') {
            $mError = $this->SimpanPasien($Params);

            if ($mError == "update") {
                echo '{success: true}';
            } else {
                echo '{success: false}';
            }
        } else {
            $mError = $this->SimpanHistoriDelDataPasien($Params);

            if ($mError == "sukses") {
                echo '{success: true}';
            } else {
                echo '{success: false}';
            }
        }
    }

    public function SimpanPasien($Params) {
        $strError = "";
        if ($Params["ID_JENIS_KELAMIN"] === '1') {
            $tmpjk = 'TRUE';
        } else {
            $tmpjk = 'FALSE';
        }
        $data = array("nama" => $Params["NAMA"],
            "tempat_lahir" => $Params["TEMPAT_LAHIR"],
            "tgl_lahir" => $Params["TGL_LAHIR"],
            "jenis_kelamin" => $tmpjk,
            "gol_darah" => $Params["ID_GOL_DARAH"],
            "alamat" => $Params["ALAMAT"],
            "telepon" => $Params["NO_TELP"],
            "kd_agama" => $Params["KD_AGAMA"],
            "status_marita" => $Params["KD_STS_MARITAL"],
            "kd_pendidikan" => $Params["KD_PENDIDIKAN"],
            "kd_pekerjaan" => $Params["KD_PEKERJAAN"],
            "nama_ayah" => $Params["AYAHPASIEN"],
            "nama_ibu" => $Params["IBUPASIEN"],
            "email" => $Params["Emailpasien"],
            "handphone" => $Params["HPpasien"],
        );

        $datasql = array("nama" => $Params["NAMA"],
            "tempat_lahir" => $Params["TEMPAT_LAHIR"],
            "tgl_lahir" => $Params["TGL_LAHIR"],
            "jenis_kelamin" => $tmpjk,
            "gol_darah" => $Params["ID_GOL_DARAH"],
            "alamat" => $Params["ALAMAT"],
            "telepon" => $Params["NO_TELP"],
            "agama" => $Params["KD_AGAMA"],
            "status_marita" => $Params["KD_STS_MARITAL"],
            "kd_pendidikan" => $Params["KD_PENDIDIKAN"],
            "kd_pekerjaan" => $Params["KD_PEKERJAAN"]
        );


        $criteria = "kd_pasien = '" . $Params["NO_MEDREC"] . "'";

        $this->load->model("rawat_jalan/tb_pasien");
        $this->tb_pasien->db->where($criteria, null, false);
        $query = $this->tb_pasien->GetRowList(0, 1, "", "", "");
        if ($query[1] == 0) {
            $strError = "simpan";
        } else {
            $this->tb_pasien->db->where($criteria, null, false);
//            $dataUpdate = array("nama"=>$Params["NamaPasien"]);
            $result = $this->tb_pasien->Update($data);
            _QMS_update('pasien', $datasql, $criteria);



            $strError = "update";
        }
        return $strError;
    }

    public function SimpanHistoriDelDataPasien($Params) {
        $strError = "";

        $waktu = date("m/d/y"); // 10/11/14
        $jam = date("m/d/y H:i:s");
        $Medrec = $Params['NOMEDREC'];
        $TglMasuk = $Params['TGLMASUK'];
        $UNIT = $Params['KDUNIT'];
        $URUT = $Params['URUTMASUK'];
        $tmptglmasuk = substr_replace($TglMasuk, '', 10);
        $criteria = "t.kd_pasien = '" . $Medrec . "' AND t.tgl_transaksi = '" . $tmptglmasuk . "' AND t.kd_unit = '" . $UNIT . "' AND t.urut_masuk = '" . $URUT . "'";
        $this->load->model("rawat_jalan/tblviewhistorypasien");
        $this->tblviewhistorypasien->db->where($criteria, null, false);
        $query = $this->tblviewhistorypasien->GetRowList(0, 1, "", "", "");

        if ($query[1] != 0) {
            $data = array("kd_kasir" => $query[0][0]->KD_KASIR,
                "no_transaksi" => $query[0][0]->NO_TRANSAKSI,
                "tgl_transaksi" => $query[0][0]->TGL_TRANSAKSI,
                "kd_pasien" => $Params['NOMEDREC'],
                "nama" => $query[0][0]->NAMA,
                "kd_unit" => $query[0][0]->KD_UNIT,
                "nama_unit" => $query[0][0]->NAMA_UNIT,
                "ispay" => 'FALSE',
                "kd_user" => 0,
                "kd_user_del" => 0,
                "user_name" => 'Admin',
                "jumlah" => $query[0][0]->JML,
                "tgl_batal" => $waktu,
                "ket" => $Params["KET"],
                "jam_batal" => $jam
            );
            $this->load->model("rawat_jalan/tblviewhistorydatapasien");
            $result = $this->tblviewhistorydatapasien->Save($data);

            if ($result > 0) {
//                                echo('a');
                $criteria = "kd_pasien = '" . $Medrec . "' AND tgl_masuk = '" . $tmptglmasuk . "' AND kd_unit = '" . $UNIT . "' AND urut_masuk = '" . $URUT . "'";
                //$this->load->model('cm/am_request_cm_detail');
                $this->load->model('rawat_jalan/tb_kunjungan');
                $this->tb_kunjungan->db->where($criteria, null, false);
                $result = $this->tb_kunjungan->Delete();




                $strError = "sukses";
            } else {
                echo '{success: false, pesan: 0}';
            }
        } else {
            
        }
        return $strError;
    }

}

?>
        