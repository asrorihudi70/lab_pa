<?php

/**
 * @author Ali
 * @copyright NCI 2010
 */


class viewcombolookupunit extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();
        
    }    


    public function index()
    {
        $this->load->view('main/index');                    
    }
   
   public function read($Params=null)
   {
        //$Params = array($Skip,$Take,$Sort,$Sortdir,$param);
                        
        $this->load->model('rawat_jalan/tbl_cmb_unit');
         
        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tbl_cmb_unit->setCriteria($criteria);

        }

        $query = $this->tbl_cmb_unit->readAll();
                    
        $arrResult=array();

        $row=new Row_unit;
        $row->KD_UNIT='Semua';
        $row->NAMA_UNIT='Semua';
        $arrResult[]=$row;
        foreach ($query->result_object() as $rows)
        {
            $arrResult[] = $this->FillRow($rows);
        }

    //Dim res = New With {.success = True, .totalrecords = m.Count, .ListDataObj = m.Skip(Skip).Take(Take)}
    
        echo '{success:true, totalrecords:'.count($arrResult).', ListDataObj:'.json_encode($arrResult).'}';
            
                    
   }
    function FillRow($rec)
    {
        $row=new Row_unit;
        $row->KD_UNIT   = $rec->kd_unit;
        $row->NAMA_UNIT = $rec->nama_unit;

        return $row;
    }
}



?>