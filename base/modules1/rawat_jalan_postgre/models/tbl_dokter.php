﻿<?php

class tbl_dokter extends TblBase
{
    function __construct()
    {
        $this->TblName='dokter';
        TblBase::TblBase(true);

        $this->SqlQuery='SELECT DISTINCT(d.kd_dokter), d.kd_dokter, d.nama FROM dokter d INNER JOIN dokter_klinik p ON p.kd_dokter=d.kd_dokter';
    }

    function FillRow($rec)
    {
        $row=new RowviewDokter;
        $row->KD_DOKTER=$rec->kd_dokter;
		$row->NAMA=$rec->nama;
        
        return $row;
    }
    
    function readAll()
    {
    
        //$this->db->where('parent', '2');
        //$this->db->order_by('status', 'asc');
        $this->db->select('*');
        $this->db->from('dokter');
        $query = $this->db->get();

        return $query;
    }

}

class RowviewDokter
{
   public $KD_DOKTER;
   public $NAMA;  

}

?>
