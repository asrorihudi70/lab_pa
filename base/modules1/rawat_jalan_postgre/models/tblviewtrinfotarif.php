﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewtrinfotarif extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_tarif,kd_produk,deskripsi,nama_unit,kd_unit,tgl_berlaku,tarif'";
		$this->SqlQuery="select  * from (
										
										select     tarif.kd_tarif, tarif.kd_produk, produk.deskripsi, tarif.kd_unit, unit.nama_unit, tarif.tgl_berlaku, tarif.tarif, ti.deskripsi as jenis_tarif
										from         tarif inner join
													produk on tarif.kd_produk = produk.kd_produk inner join
													unit on tarif.kd_unit = unit.kd_unit 
													inner join tarif_induk ti on tarif.kd_tarif = ti.kd_tarif
										)as resdata  ";
		$this->TblName='trtarifkasir';
                TblBase::TblBase(true);
	}


	function FillRow($rec)
	{
		$row=new Rowdokter;
	
		$row->KD_TARIF=$rec->kd_tarif;
		$row->KD_PRODUK=$rec->kd_produk;
		$row->DESKRIPSI=$rec->deskripsi;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->KD_UNIT=$rec->kd_unit;
		$row->TGL_BERLAKU=$rec->tgl_berlaku;
		$row->TARIF=$rec->tarif;
		$row->JENIS_TARIF=$rec->jenis_tarif;
	
		
		return $row;
	}
}
class Rowdokter
{
	public $KD_TARIF;
	public $KD_PRODUK;
	public $DESKRIPSI;
    public $NAMA_UNIT;
   	public $KD_UNIT;
	public $TGL_BERLAKU;
	public $TARIF;
	public $JENIS_TARIF;

}

?>