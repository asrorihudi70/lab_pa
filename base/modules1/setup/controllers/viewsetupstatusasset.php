<?php
class viewsetupstatusasset extends MX_Controller
{

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }

    function read($Params=null)
    {

        try
        {

            $this->load->model('setup/tblam_status_asset');
            if (strlen($Params[4])!==0)
            {
               $this->tblam_status_asset->db->where(str_replace("~", "'", $Params[4]) ,null, false) ;
            }
            $res = $this->tblam_status_asset->GetRowList($Params[0], $Params[1], $Params[3],  strtolower( $Params[2]), $Params[4]);

        }
        catch(Exception $o)
        {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';

    }


     function save($param)
     {
        $this->load->model('setup/tblam_status_asset');

        $Arr['status_asset_id']=$param['StatusAssetId'];
        $Arr['status_asset']=$param['StatusAsset'];

        if (strlen($Arr['status_asset_id'])===0)
        {
            $Arr['status_asset_id']=$this->GetNewID();
            $res=$this->tblam_status_asset->Save($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
                if ($res>0)
                {
                    echo '{success: true, StatusAssetID: "'.$Arr['status_asset_id'].'" }';
                } else echo '{success: false}';
        }
        else
        {
            $this->db->where("status_asset_id = '".$Arr['status_asset_id']."'", null, false);
            $res=$this->tblam_status_asset->Update($Arr); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);

            if ($res>0)
            {
                echo '{success: true}';
            } else echo '{success: false}';
        }

//                if ($res !==0)
//                {
//                    echo '{success:true, totalrecords:'.$res[1].', ListDataObj:'.json_encode($res[0]).'}';
//                }

     }

     function GetNewID()
     {
         $res = $this->tblam_status_asset->GetRowList( 0 , 1, 'DESC', 'status_asset_id', '');
         if ($res[1]===0)
         {
             return 1;
         }
         else
         {
             return $res[0][0]->STATUS_ASSET_ID+1;
         }
     }
         
     function delete( $param)
     {
        $this->load->model('setup/tblam_status_asset');
        $Sql=$this->db;
        $Sql->where("status_asset_id = '".$param['StatusAssetId']."'", null, false);
        $res=$this->tblam_status_asset->Delete(); //' $param,$Skip  ,$Take   ,$SortDir, $Sort);
        if ($res>0)
        {
            echo '{success: true}';
        } else echo '{success: false}';

     }
}
?>
