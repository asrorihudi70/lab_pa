<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class am_company extends  Model
{

    function am_company()
    {
        parent::Model();
        $this->load->database();
    }

    function readAll()
    {
        //$query = $this->db->get('dbo.AM_COMPANY');
        $query = $this->db->get('am_company');

        return $query;
    }

    function readparam($ar_filters = null, $take = 10, $skip = 0, $ar_sort = null, $sortdir='ASC')
    {

        if ($ar_filters != null)
        {
            if (is_array($ar_filters))
            {
                foreach ($ar_filters as $field => $value)
                {
                    $this->db->where(strtolower($field), $value);
                }
            }
        }

        if ($take > 0)
        {
            $this->db->limit($take, $skip);
        }

        if ($ar_sort != null)
        {
            if (is_array($ar_sort))
            {
                foreach ($ar_sort as $field) {
                    $this->db->orderby(strtolower($field), $sortdir);
                }
            } else $this->db->orderby(strtolower($ar_sort), $sortdir);
        }

        $query = $this->db->get('am_company');

        return $query;
        }

        
	function create($data)
	{

                $this->db->set('comp_id', $data['COMP_ID']);
                $this->db->set('comp_name', $data['COMP_NAME']);
                $this->db->set('comp_address', $data['COMP_ADDRESS']);
                $this->db->set('comp_telp', $data['COMP_TELP']);
                $this->db->set('comp_fax', $data['COMP_FAX']);
                $this->db->set('comp_city', $data['COMP_CITY']);
                $this->db->set('comp_pos_code', $data['COMP_POS_CODE']);
                $this->db->set('comp_email', $data['COMP_EMAIL']);
		//$this->db->set('LOGO', $data['LOGO']);
                //$this->db->set('logo', $data['LOGO']);
		//$this->db->insert('dbo.am_company');
                $this->db->insert('am_company');

		return $this->db->affected_rows();
	}

	function read($id)
	{
		//$this->db->where('COMP_ID', $id);
                $this->db->where('comp_id', $id);
		//$query = $this->db->get('dbo.am_company');
                $query = $this->db->get('am_company');

		return $query;
	}

	function update($data)
	{

                $this->db->where('comp_id', $data['COMP_ID']);
                $this->db->set('comp_name', $data['COMP_NAME']);
                $this->db->set('comp_address', $data['COMP_ADDRESS']);
                $this->db->set('comp_telp', $data['COMP_TELP']);
                $this->db->set('comp_fax', $data['COMP_FAX']);
                $this->db->set('comp_city', $data['COMP_CITY']);
                $this->db->set('comp_pos_code', $data['COMP_POS_CODE']);
                $this->db->set('comp_email', $data['COMP_EMAIL']);
		//$this->db->set('LOGO', $data['LOGO']);
		//$this->db->update('dbo.am_company');
                $this->db->update('am_company');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('COMP_ID', $id);
		$this->db->delete('dbo.AM_COMPANY');

		return $this->db->affected_rows();
	}

}



?>