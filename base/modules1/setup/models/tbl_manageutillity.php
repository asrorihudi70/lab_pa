﻿<?php
class tbl_manageutillity extends Model  
{
		private $table = "zusers";
		private $key   = "kd_user";
		
		function tbl_manageutillity()
		{
			// Call the CI_Model constructor
			parent::Model();
			$this->load->database();
		}

		function set_config($data, $key)
		{
			$this->db->where($this->key, $key);
			$this->db->update($this->table, $data);
			//$this->db->from($this->table);
			if ($this->db->affected_rows() > 0) {
				return true;
			}else{
				return false;
			}
		}

		function getDataSetting($key){
			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where($this->key, $key);
			return $this->db->get();
		}

		function getData($table, $field, $value){
			$this->db->select("*");
			$this->db->from($table);
			$this->db->where($field, $value);
			return $this->db->get();
		}

		function getDataCostumer($kd_customer){
			$this->db->select("*");
			$this->db->from("kontraktor");
			$this->db->join("customer", "kontraktor.kd_customer = customer.kd_customer");
			$this->db->where("kontraktor.kd_customer", $kd_customer);
			return $this->db->get();
		}
}
?>