<?php
class tblam_status_asset extends TblBase
{

	function __construct()
	{
		$this->StrSql="";
		$this->TblName='am_status_asset';
		TblBase::TblBase();
	}

	function FillRow($rec)
	{
		$row=new Rowtblam_status_asset;
                $row->STATUS_ASSET_ID=$rec->status_asset_id;
		$row->STATUS_ASSET=$rec->status_asset;

		return $row;
	}
}
class Rowtblam_status_asset
{
    public $STATUS_ASSET_ID;
    public $STATUS_ASSET;

}
?>
