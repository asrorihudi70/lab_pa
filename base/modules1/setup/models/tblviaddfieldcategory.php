<?php

class tblviaddfieldcategory extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (
            
        SELECT  category_id, row_add, addfield, type_field, lenght,
        CASE WHEN a.type_field = '1' THEN 'Text' WHEN a.type_field = '2' THEN 'Numeric' ELSE 'Date / Time' END AS typefield
        FROM am_add_field AS a

            ) as resdata";

        $this->TblName='viaddfieldcategory';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblviaddfieldcategory;

        $row->CATEGORY_ID=$rec->category_id;
        $row->ROW_ADD=$rec->row_add;
        $row->ADDFIELD=$rec->addfield;
        $row->TYPE_FIELD=$rec->type_field;
        $row->LENGHT=$rec->lenght;
        $row->TYPEFIELD=$rec->typefield;

        return $row;
    }
}

class Rowtblviaddfieldcategory
{
    public $CATEGORY_ID;
    public $ROW_ADD;
    public $ADDFIELD;
    public $TYPE_FIELD;
    public $LENGHT;
    public $TYPEFIELD;

}

?>
