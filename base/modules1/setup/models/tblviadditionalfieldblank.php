<?php
class tblviadditionalfieldblank extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT category_id, row_add, addfield, type_field, '' AS value, '' AS asset_maint_id
            FROM  am_add_field

            ) as resdata";

        $this->TblName='viadditionalfieldblank';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblviadditionalfieldblank;

        $row->CATEGORY_ID=$rec->category_id;
        $row->ROW_ADD=$rec->row_add;
        $row->ADDFIELD=$rec->addfield;
        $row->TYPE_FIELD=$rec->type_field;
        $row->VALUE=$rec->value;
        $row->ASSET_MAINT_ID=$rec->asset_maint_id;

        return $row;
    }
}

class Rowtblviadditionalfieldblank
{
    public $CATEGORY_ID;
    public $ROW_ADD;
    public $ADDFIELD;
    public $TYPE_FIELD;
    public $VALUE;
    public $ASSET_MAINT_ID;
    
}

?>
