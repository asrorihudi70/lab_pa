<?php

class tblvicategory extends TblBase
{

    function __construct()
    {
        $this->StrSql="kd_produk,deskripsi,kd_unit,nama_unit,
		manual,kp_produk,kd_kat,kd_klas,klasifikasi,parent,kd_tarif,
		tgl_berlaku,tarif,tgl_berakhir ";
        $this->SqlQuery="select * from (
                      
					  select produk_unit.kd_produk,produk.deskripsi, tarif.kd_unit, unit.nama_unit, 
 					  produk.manual, produk.kp_produk, produk.kd_kat, produk.kd_klas, 
                       klas_produk.klasifikasi, klas_produk.parent, tarif.kd_tarif, tarif.tgl_berlaku,tarif.tarif, tarif.tgl_berakhir
					
						from         
							produk 	
						inner join klas_produk on produk.kd_klas = klas_produk.kd_klas 
						inner join tarif on produk.kd_produk = tarif.kd_produk 
						inner join produk_unit on produk.kd_produk = produk_unit.kd_produk and produk_unit.kd_unit = tarif.kd_unit
									inner join unit on tarif.kd_unit = unit.kd_unit 
                      where LOWER(kd_tarif)=LOWER('TU') and left(tarif.kd_unit,3)='202' and tgl_berlaku= ('2014-03-01')


            ) as resdata";

        $this->TblName='vicategory';
        TblBase::TblBase(true);
    }

    function FillRow($rec)
    {
        $row=new Rowtblvicategory;
		$row->KD_PRODUK=$rec->kd_produk;
        $row->DESKRIPSI=$rec->deskripsi;
		$row->KD_UNIT=$rec->kd_unit;
		$row->NAMA_UNIT=$rec->nama_unit;
		$row->MANUAL=$rec->manual;
        $row->KP_PRODUK=$rec->kp_produk;
		$row->KD_KAT=$rec->kd_kat;
		$row->KD_KLAS=$rec->kd_klas;
		$row->KLASIFIKASI=$rec->klasifikasi;
		$row->PARENT=$rec->parent;
		$row->KD_TARIF=$rec->kd_tarif;
		$row->TGL_BERLAKU=$rec->tgl_berlaku;
		$row->TARIF=$rec->tarif;
        $row->TGL_BERAKHIR=$rec->tgl_berakhir;
       
       
        

        return $row;
    }
}

class Rowtblvicategory
{
    public $KD_PRODUK;
    public $DESKRIPSI;
    public $KD_UNIT;
    public $NAMA_UNIT;
    public $MANUAL;
    public $KP_PRODUK;
	public $KD_KAT;
    public $KD_KLAS;
    public $KLASIFIKASI;
    public $PARENT;
    public $KD_TARIF;
	public $TGL_BERLAKU;
    public $TARIF;
    public $TGL_BERAKHIR;

}

?>
