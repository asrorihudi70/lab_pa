<?php
class tblviewsetupassetref extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT am_reference_asset.asset_maint_id, am_reference_asset.title, am_reference_asset.description,
                am_reference.path_image, am_reference.description AS ext, am_reference_asset.row_reff,
                am_reference_asset.reff_id, am_reference_asset.path
            FROM am_reference INNER JOIN
                am_reference_asset ON am_reference.reff_id = am_reference_asset.reff_id

            ) as resdata";

        $this->TblName='viewsetupassetref';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblviewsetupassetref;

        $row->ASSET_MAINT_ID = $rec->asset_maint_id;
        $row->TITLE = $rec->title;
        $row->DESCRIPTION = $rec->description;
        $row->PATH_IMAGE = $rec->path_image;
        $row->EXT = $rec->ext;
        $row->ROW_REFF = $rec->row_reff;
        $row->REFF_ID = $rec->reff_id;
        $row->PATH = $rec->path;
        
        return $row;
    }
}
class Rowtblviewsetupassetref
{
    public $ASSET_MAINT_ID;
    public $TITLE;
    public $DESCRIPTION;
    public $PATH_IMAGE;
    public $EXT;
    public $ROW_REFF;
    public $REFF_ID;
    public $PATH;
    
}
?>
