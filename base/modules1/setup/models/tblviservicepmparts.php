﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviservicepmparts extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="service_id,category_id,part_id,qty,unit_cost,tot_cost,instruction,part_name,service_name,category_name"  
;
		$this->SqlQuery="select am_service_parts.service_id,  am_service_parts.category_id,  am_service_parts.part_id,  am_service_parts.qty, 
				am_service_parts.unit_cost,  am_service_parts.tot_cost,  am_service_parts.instruction, 
				am_spareparts.part_name,  am_service.service_name,  am_category.category_name
				from am_spareparts inner join
				am_service_parts on  am_spareparts.part_id =  am_service_parts.part_id inner join
				am_service on  am_service_parts.service_id =  am_service.service_id inner join
				am_category on  am_service_parts.category_id =  am_category.category_id";
		$this->TblName='viservicepmparts';
		TblBase::TblBase(true);
	}

	
	function FillRow($rec)
	{
		$row=new Rowviservicepmparts;
				$row->SERVICE_ID=$rec->service_id;
		$row->CATEGORY_ID=$rec->category_id;
		$row->PART_ID=$rec->part_id;
		$row->QTY=$rec->qty;
		$row->UNIT_COST=$rec->unit_cost;
		$row->TOT_COST=$rec->tot_cost;
		$row->INSTRUCTION=$rec->instruction;
		$row->PART_NAME=$rec->part_name;
		$row->SERVICE_NAME=$rec->service_name;
		$row->CATEGORY_NAME=$rec->category_name;

		return $row;
	}
}
class Rowviservicepmparts
{
	public $SERVICE_ID;
public $CATEGORY_ID;
public $PART_ID;
public $QTY;
public $UNIT_COST;
public $TOT_COST;
public $INSTRUCTION;
public $PART_NAME;
public $SERVICE_NAME;
public $CATEGORY_NAME;

}

?>