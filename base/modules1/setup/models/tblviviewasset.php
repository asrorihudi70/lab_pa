<?php
class tblviviewasset extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT am_asset_maint.asset_maint_id, am_asset_maint.category_id, am_asset_maint.comp_id,
                am_asset_maint.location_id, am_asset_maint.dept_id, am_asset_maint.asset_maint_name,
                am_asset_maint.years, am_asset_maint.description_asset, am_asset_maint.asset_id,
                am_category.category_name, am_department.dept_name, am_location.location,
                am_company.comp_name, coalesce(am_asset_maint.path_image, ' ') as path_image,
                am_asset_maint.status_asset_id, am_status_asset.status_asset

            FROM am_asset_maint INNER JOIN
                am_category ON am_asset_maint.category_id = am_category.category_id INNER JOIN
                am_company ON am_asset_maint.comp_id = am_company.comp_id INNER JOIN
                am_department ON am_asset_maint.dept_id = am_department.dept_id INNER JOIN
                am_location ON am_asset_maint.location_id = am_location.location_id INNER JOIN
                am_status_asset ON am_asset_maint.status_asset_id = am_status_asset.status_asset_id

            ) as resdata";

        $this->TblName='viviewasset';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblviviewasset;

        $row->ASSET_MAINT_ID = $rec->asset_maint_id;
        $row->CATEGORY_ID = $rec->category_id;
        $row->COMP_ID = $rec->comp_id;
        $row->LOCATION_ID = $rec->location_id;
        $row->DEPT_ID = $rec->dept_id;
        $row->ASSET_MAINT_NAME = $rec->asset_maint_name;
        $row->YEARS = $rec->years;
        $row->DESCRIPTION_ASSET = $rec->description_asset;
        $row->ASSET_ID = $rec->asset_id;
        $row->CATEGORY_NAME = $rec->category_name;
        $row->DEPT_NAME = $rec->dept_name;
        $row->LOCATION = $rec->location_id;
        $row->COMP_NAME = $rec->comp_name;
        $row->PATH_IMAGE = $rec->path_image;
        $row->STATUS_ASSET_ID = $rec->status_asset_id;
        $row->STATUS_ASSET = $rec->status_asset;
        
        return $row;
    }
}

class Rowtblviviewasset
{
    public $ASSET_MAINT_ID;
    public $CATEGORY_ID;
    public $COMP_ID;
    public $LOCATION_ID;
    public $DEPT_ID;
    public $ASSET_MAINT_NAME;
    public $YEARS;
    public $DESCRIPTION_ASSET;
    public $ASSET_ID;
    public $CATEGORY_NAME;
    public $DEPT_NAME;
    public $LOCATION;
    public $COMP_NAME;
    public $PATH_IMAGE;
    public $STATUS_ASSET_ID;
    public $STATUS_ASSET;

}

?>
