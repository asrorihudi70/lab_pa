<?php
class viewinputlogmetering extends MX_Controller {

    public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }

    public function index()
    {
       $this->load->view('main/index');
    }

    public function read($Params=null)
    {
        $this->load->model('setup/tblam_history_log_metering');

        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblam_history_log_metering->db->where($criteria, null, false);
        }

        $query = $this->tblam_history_log_metering->GetRowList( $Params[0], $Params[1], $Params[3], $Params[2], "");

        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';

    }

    public function save($Params=null)
    {
        
    }
    
    public function delete($Params=null)
    {
        
    }


    private function GetIdHistoryLog()
    {
        $today = getdate();
        $strNomor=$today['year'].str_pad($today['mon'],2,'0',STR_PAD_LEFT);
        $retVal=$strNomor."0001";

        $this->load->model('setup/tblam_history_log_metering');
        $this->tblam_history_log_metering->db->where("substring(req_id,1,6) = '".$strNomor."'",null,false);
        $res = $this->tblam_history_log_metering->GetRowList( 0, 1, "DESC", "hist_log_id",  "");

        if ($res[1]>0)
        {
            $nomor = (int)substr($res[0][0]->HIST_LOG_ID, -4) + 1;
            $retVal=$strNomor.str_pad($nomor,4,"0000",STR_PAD_LEFT);
        }

        return $retVal;
    }

    private function getMetering($strIDAsset)
    {
        $result = 0;
        $this->load->model('setup/tblam_history_log_metering');
        $this->tblam_history_log_metering->db->where("asset_maint_id = '".$strIDAsset."'", null, false);
        $query = $this->tblam_history_log_metering->GetRowList( 0, 1, "", "",  "");
        if ($query[1]>0)
            $result = $query[0][0]->METER;

        return $result;
    }

}

?>
