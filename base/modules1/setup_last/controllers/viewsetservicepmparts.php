<?php
class viewsetservicepmparts extends MX_Controller
{
	 public function __construct()
    {
        //parent::Controller();
        parent::__construct();

    }


    public function index()
    {
        $this->load->view('main/index');
    }


    public function read($Params=null)
    {
        $this->load->model('setup/tblviservicepmparts');

        if (strlen($Params[4])>0)
        {
            $criteria = str_replace("~" ,"'",$Params[4]);
            $this->tblviservicepmparts->db->where($criteria);
        }

        $query = $this->tblviservicepmparts->GetRowList( $Params[0], $Params[1], $Params[3], $Params[2], "");

        echo '{success:true, totalrecords:'.$query[1].', ListDataObj:'.json_encode($query[0]).'}';

    }

    public function save($Params=null)
    {
        $xRow["category_id"] = $Params["CATEGORY_ID"];
        $xRow["part_id"] = $Params["PART_ID"];
        $xRow["service_id"] = $Params["SERVICE_PM_ID"];
        $xRow["instruction"] = $Params["INSTRUCTION"];
        $xRow["qty"] = $Params["QTY"];
        $xRow["unit_cost"] = $Params["UNIT_COST"];
        $xRow["tot_cost"] = $Params["TOT_COST"];

        $this->load->model('setup/tblviservicepmparts');
        $criteria = "service_id = '".$xRow["service_id"]."' and category_id = '".$xRow["category_id"]."'";
        $this->tblam_service_parts->db->where($criteria);
        $query = $this->tblam_service_parts->GetRowList( 0, 1, "", "",  "");

        if ($query[1]==0)
        {
            $mError = $this->SimpanServicePMParts($Params);

            if ($mError=="")
            {
                echo "{success : true}";
            } else echo "{success : false}";
        } else {
            $result = $this->tblam_service_parts->Update($xRow);
            if ($result>0)
            {
                echo "{success : true}";
            } else echo "{success : false}";
       }
    }

    public function delete($Params=null)
    {
        $flag =0;
        $xRow["categori_id"] = $Params["CatdId"];
        $xRow["service_id"] = $Params["ServId"];

        $this->load->model("setup/tblam_service_parts");

        if ((int)$Params["Hapus"]==1)
        {
            $criteria = "service_id = '".$xRow["service_id"]."' and category_id = '".$xRow["category_id"]."'";
            $this->tblam_service_parts->db->where($criteria, null, false);
            $query = $this->tblam_service_parts->GetRowList( 0, 1000, "", "",  "");

            if ($query[1]>0)
            {
                foreach ($query[0] as $dtlRow)
                {
                    $this->tblam_service_parts->db->where($criteria." and part_id = '".$dtlRow->PART_ID."'");
                    $result = $this->tblam_service_parts>Delete();
                    if ($result>0)
                        $flag +=1;
                }
            }

            if ($query[1]==0 or $query[1]==$flag)
            {
                $result = $this->tblam_service_parts>Delete();
                if ($result>0)
                {
                    echo "{success : true}";
                } else echo "{success : false}";
            }
            

        } else {

            $xRow["part_id"] = $Params["PART_ID"];
            $criteria = "part_id = '".$xRow["part_id"]."' and service_id = '".$xRow["service_id"]."'
                and category_id = '".$xRow["category_id"]."'";
            $this->tblam_service_parts->db->where($criteria, null, false);
            $result = $this->tblam_service_parts>Delete();
            if ($result>0)
            {
                echo "{success : true}";
            } else echo "{success : false}";                        
            
        }


    }

    private function SimpanServicePMParts($Params=null)
    {
        $arr = $this->GetListDetail($Params);
        $strError = "";
        $arrSimpan = array();

        if (count($arr)>0)
        {
            foreach($arr as $dtlRow)
            {
                $xRow=array();

                $criteria = "part_id = '".$dtlRow["part_id"]."' and
                    service_id = '".$dtlRow["service_id"]."' and
                    category_id = '".$dtlRow["category_id"]."'";

                $this->load->model("setup/tblam_service_parts");
                $this->tblam_service_parts->db->where($criteria, null, false);
                $query = $this->tblam_service_parts->GetRowList( 0, 1, "", "",  "");

                if ($query[1]>0)
                {
                    $result = $this->tblam_service_parts->Update($dtlRow);
                    if ($result==0)
                        $strError = "ada";
                } else {
                    $result = $this->tblam_service_parts->Save($dtlRow);
                    if ($result==0)
                        $strError = "ada";
                }
            }
        }

        return $strError;
    }

    private function GetListDetail($Params=null)
    {

        $jml = (int)$Params["JmlField"];
        $arrList = $this->splitListDetail($Params["List"],(int)$Params["JmlList"],$jml);

        $arrListRow=array();

        if (count($arrList)>0)
        {
            foreach ($arrList as $str)
            {
                $arrListField= array();

                for ($i=0;$i<$jml;$i+=1)
                {
                    $splitField=explode("=",$str[$i],2);
                    $arrListField[]=$splitField[1];
                }

                if (count($arrListField)>0)
                {
                    $arrListRow['PART_ID']= $arrListField[0];
                    $arrListRow['SERVICE_ID']= $arrListField[1];
                    $arrListRow['CATEGORY_ID']= $arrListField[2];
                    $arrListRow['QTY']= $arrListField[3];
                    $arrListRow['UNIT_COST']= $arrListField[4];
                    $arrListRow['TOT_COST']= $arrListField[3]*$arrListField[4];
                    $arrListRow['INSTRUCTION']= $arrListField[6];

                }
            }
        }

        return $arrListRow;

   }

    private function splitListDetail($str, $jmlList, $jmlField)
    {
        $splitList = explode("##[[]]##",$str,$jmlList);

        $arrList=array();

        for ($i=0;$i<$jmlList;$i+=1)
        {
            $splitRecord= explode("@@##$$@@", $splitList[$i] , $jmlField);
            $arrList=array($splitRecord);
        }

        return $arrList;
    }


}

?>
