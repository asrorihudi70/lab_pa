<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class tblam_company extends TblBase
{ 
    function __construct()
    {
        //$this->StrSql="comp_id,comp_name,comp_address,comp_telp,comp_fax,comp_city,comp_pos_code,comp_email,logo";
        $this->TblName='am_company';
              TblBase::TblBase();
              $this->SqlQuery= "";
    }

    function FillRow($rec)
    {
        $row=new Rowtblam_company();
        $row->COMP_ID=$rec->comp_id;
        $row->COMP_NAME=$rec->comp_name;
        $row->COMP_ADDRESS=$rec->comp_address;
        $row->COMP_TELP=$rec->comp_telp;
        $row->COMP_FAX=$rec->comp_fax;
        $row->COMP_CITY=$rec->comp_city;
        $row->COMP_POS_CODE=$rec->comp_pos_code;
        $row->COMP_EMAIL=$rec->comp_email;
        $row->LOGO=$rec->logo;

        return $row;
    }



}
class Rowtblam_company
{
    public $COMP_ID;
    public $COMP_NAME;
    public $COMP_ADDRESS;
    public $COMP_TELP;
    public $COMP_FAX;
    public $COMP_CITY;
    public $COMP_POS_CODE;
    public $COMP_EMAIL;
    public $LOGO;
}
?>
