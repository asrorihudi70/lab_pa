<?php
class tblam_department extends TblBase
{
    function __construct()
    {
        $this->TblName='am_department';
        TblBase::TblBase();

        $this->SqlQuery= "";;
    }

    function FillRow($rec)
    {
        $row=new Rowtblamdepartment;

        $row->DEPT_ID=$rec->dept_id;
        $row->DEPT_NAME=$rec->dept_name;

        return $row;
    }

}

class Rowtblamdepartment
{
    public $DEPT_ID;
    public $DEPT_NAME;
}
?>
