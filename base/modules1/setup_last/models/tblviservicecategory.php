<?php
class tblviservicecategory extends TblBase
{

    function __construct()
    {
        $this->StrSql="";
        $this->SqlQuery="select * from (

            SELECT am_service_category.service_id, am_service_category.category_id, am_service_category.type_service_id,
                am_service_category.is_aktif, am_service_category.interval, am_service_category.metering_assumptions,
                am_service_category.days_assumptions, am_service_category.flag, am_type_service.service_type,
                am_category.category_name, am_service.service_name, am_service_category.unit_id, am_unit.unit,
                am_service_category.act_day_before, am_service_category.act_day_target, am_service_category.act_day_after,
                am_service_category.act_before, am_service_category.act_target, am_service_category.act_after,
                am_service_category.legend_before, am_service_category.legend_target, am_service_category.legend_after,
            CASE WHEN am_service_category.act_before = '1' THEN 'Send Email'
                WHEN am_service_category.act_before = '2' THEN 'Send SMS' END AS ACTION_BEFORE,
            CASE WHEN am_service_category.act_target = '1' THEN 'Send Email' 
                WHEN am_service_category.act_target = '2' THEN 'Send SMS' END AS ACTION_TARGET,
            CASE WHEN am_service_category.act_after = '1' THEN 'Send Email'
                WHEN am_service_category.act_after = '2' THEN 'Send SMS' END AS ACTION_AFTER,
                am_service_category.tag_act_before, am_service_category.tag_act_target, am_service_category.tag_act_after
            FROM  am_service_category INNER JOIN
                am_category ON am_service_category.category_id = am_category.category_id INNER JOIN
                am_type_service ON am_service_category.type_service_id = am_type_service.type_service_id INNER JOIN
                am_service ON am_service_category.service_id = am_service.service_id INNER JOIN
                am_unit ON am_service_category.unit_id = am_unit.unit_id

            ) as resdata";

        $this->TblName='viservicecategory';
        TblBase::TblBase(true);
    }


    function FillRow($rec)
    {
        $row=new Rowtblviservicecategory;

        $row->SERVICE_ID = $rec->service_id;
        $row->CATEGORY_ID = $rec->category_id;
        $row->TYPE_SERVICE_ID = $rec->type_service_id;
        $row->IS_AKTIF = $rec->is_aktif;
        $row->INTERVAL = $rec->interval;
        $row->METERING_ASSUMPTIONS = $rec->metering_assumptions;
        $row->DAYS_ASSUMPTIONS = $rec->days_assumptions;
        $row->FLAG = $rec->flag;
        $row->SERVICE_TYPE = $rec->service_type;
        $row->CATEGORY_NAME = $rec->category_name;
        $row->SERVICE_NAME = $rec->service_name;
        $row->UNIT_ID = $rec->unit_id;
        $row->UNIT = $rec->unit;
        $row->ACT_DAY_BEFORE = $rec->act_day_before;
        $row->ACT_DAY_TARGET = $rec->act_day_target;
        $row->ACT_DAY_AFTER = $rec->act_day_after;
        $row->ACT_BEFORE = $rec->act_before;
        $row->ACT_TARGET = $rec->act_target;
        $row->ACT_AFTER = $rec->act_after;
        $row->LEGEND_BEFORE = $rec->legend_before;
        $row->LEGEND_TARGET = $rec->legend_target;
        $row->LEGEND_AFTER = $rec->legend_after;
        $row->ACTION_BEFORE = $rec->action_before;
        $row->ACTION_TARGET = $rec->action_target;
        $row->ACTION_AFTER = $rec->action_after;
        $row->TAG_ACT_BEFORE = $rec->tag_act_before;
        $row->TAG_ACT_TARGET = $rec->tag_act_target;
        $row->TAG_ACT_AFTER = $rec->tag_act_after;

        return $row;
    }
}
class Rowtblviservicecategory
{
    public $SERVICE_ID;
    public $CATEGORY_ID;
    public $TYPE_SERVICE_ID;
    public $IS_AKTIF;
    public $INTERVAL;
    public $METERING_ASSUMPTIONS;
    public $DAYS_ASSUMPTIONS;
    public $FLAG;
    public $SERVICE_TYPE;
    public $CATEGORY_NAME;
    public $SERVICE_NAME;
    public $UNIT_ID;
    public $UNIT;
    public $ACT_DAY_BEFORE;
    public $ACT_DAY_TARGET;
    public $ACT_DAY_AFTER;
    public $ACT_BEFORE;
    public $ACT_TARGET;
    public $ACT_AFTER;
    public $LEGEND_BEFORE;
    public $LEGEND_TARGET;
    public $LEGEND_AFTER;
    public $ACTION_BEFORE;
    public $ACTION_TARGET;
    public $ACTION_AFTER;
    public $TAG_ACT_BEFORE;
    public $TAG_ACT_TARGET;
    public $TAG_ACT_AFTER;    

}
?>
