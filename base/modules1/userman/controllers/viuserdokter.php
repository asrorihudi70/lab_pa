<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class viuserdokter extends MX_Controller {

    public function __construct() {
        //parent::Controller();
        parent::__construct();
    }

    public function index() {
        $this->load->view('main/user_management');
    }

    function read($Params = null) {

        try {

            $tgl = date('M-d-Y');
            $this->load->model('userman/tbl_user_dokter');
            if (strlen($Params[4]) !== 0) {
                $criteria = str_replace("~", "'", $Params[4]);
                $this->tbl_user_dokter->db->where($criteria, null, false);
            }

            $res = $this->tbl_user_dokter->GetRowList($Params[0], $Params[1], $Params[3], strtolower($Params[2]), $Params[4]);
        } catch (Exception $o) {
            echo 'Debug  fail ';
        }

        echo '{success:true, totalrecords:' . $res[1] . ', ListDataObj:' . json_encode($res[0]) . '}';
    }

}

//VIEWSETUPASSET
?>
