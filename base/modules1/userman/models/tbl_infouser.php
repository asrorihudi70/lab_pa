<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tbl_infouser extends TblBase {

    function __construct() {
        $this->StrSql = "kd_user,user_names,full_name,description,password,tag1,tag2";
        $this->SqlQuery = "select  * from (
                            select u.kd_user as 
                            kd_user,u.user_names as 
                            user_names,u.full_name as full_name,u.description2 as 
                            description,u.password,u.tag1,u.tag2,
                            u.kd_unit,u.kd_milik,u.kd_unit_far,u.kd_dokter,
                            un.nama_unit, au.nm_unit_far, am.milik, d.nama
                            from zusers u
                            left join unit un on un.kd_unit = u.kd_unit  
                            left join apt_unit au on au.kd_unit_far = u.kd_unit_far
                            left join apt_milik am on am.kd_milik = u.kd_milik
                            left join dokter d on d.kd_dokter = u.kd_dokter 
                            )as resdata ";
        $this->TblName = 'tbl_infouser';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowuser;

        $row->KD_USER = $rec->kd_user;
        $row->USER_NAMES = $rec->user_names;
        $row->FULL_NAME = $rec->full_name;
        $row->DESCRIPTION = $rec->description;
        $row->PASSWORD = $rec->password;
        $row->TAG1 = $rec->tag1;
        $row->TAG2 = $rec->tag2;
        $row->KDPOLI = $rec->kd_unit;
        $row->KDMILIK = $rec->kd_milik;
        $row->KDUNIT = $rec->kd_unit_far;
        $row->KDDOKTER = $rec->kd_dokter;
        $row->NAMADOKTER = $rec->nama;
        $row->PEMILIK = $rec->milik;
        $row->UNIT = $rec->nm_unit_far;
        $row->POLI = $rec->nama_unit;

        return $row;
    }

}

class Rowuser {

    public $KD_USER;
    public $USER_NAMES;
    public $FULL_NAME;
    public $DESCRIPTION;
    public $PASSWORD;
    public $TAG1;
    public $TAG2;
    public $KDPOLI;
    public $KDMILIK;
    public $KDUNIT;
    public $KDDOKTER;
    public $NAMADOKTER;
    public $PEMILIK;
    public $UNIT;
    public $POLI;

}

?>