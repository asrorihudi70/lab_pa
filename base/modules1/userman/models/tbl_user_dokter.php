<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tbl_user_dokter extends TblBase {

    function __construct() {
        $this->StrSql = "kd_dokter,nama";
        $this->SqlQuery = "select kd_dokter, nama from dokter order by nama";
        $this->TblName = 'tbl_user_dokter';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowuser;

        $row->KODE = $rec->kd_dokter;
        $row->NAMA = $rec->nama;

        return $row;
    }

}

class Rowuser {

    public $KODE;
    public $NAMA;

}

?>