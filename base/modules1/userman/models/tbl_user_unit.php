<?php

/**
 * @author OLIB
 * @copyright 2008
 */
class tbl_user_unit extends TblBase {

    function __construct() {
        $this->StrSql = "kd_unit_far,nm_unit_far";
        $this->SqlQuery = "select kd_unit_far,nm_unit_far from apt_unit order by nm_unit_far";
        $this->TblName = 'tbl_user_unit';
        TblBase::TblBase(true);
    }

    function FillRow($rec) {
        $row = new Rowuser;

        $row->KODE = $rec->kd_unit_far;
        $row->NAMA = $rec->nm_unit_far;

        return $row;
    }

}

class Rowuser {

    public $KODE;
    public $NAMA;

}

?>