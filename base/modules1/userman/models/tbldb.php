﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tbldb extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="
		code,
		kd_propinsi,
		main_dept,
		name,
		address,
		phone1,
		phone2,
		fax,
		city,
		state,
		country,
		kelas_rs,
		logo,
		zip";
		$this->TblName='db_rs';
			TblBase::TblBase();
		$this->SQL=$this->db;
	}

	
	function FillRow($rec)
	{
		$row=new Rowzusers;
		$row->CODE 			= $rec->code;
		$row->KD_PROPINSI 	= $rec->kd_propinsi;
		$row->MAIN_DEPT 	= $rec->main_dept;
		$row->NAME 			= $rec->name;
		$row->ADDRESS 		= $rec->address;
		$row->PHONE1 		= $rec->phone1;
		$row->PHONE2 		= $rec->phone2;
		$row->FAX 			= $rec->fax;
		$row->CITY 			= $rec->city;
		$row->STATE 		= $rec->state;
		$row->COUNTRY 		= $rec->country;
		$row->KELAS_RS 		= $rec->kelas_rs;
		$row->LOGO 	 		= $rec->logo;
		$row->ZIP 			= $rec->zip;
		return $row;
	}
}
class Rowzusers
{
	public $CODE;
	public $KD_PROPINSI;
	public $MAIN_DEPT;
	public $NAME;
	public $ADDRESS;
	public $PHONE1;
	public $PHONE2;
	public $FAX;
	public $CITY;
	public $STATE;
	public $COUNTRY;
	public $KELAS_RS;
	public $LOGO;
	public $ZIP;
}

?>