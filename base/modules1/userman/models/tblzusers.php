﻿<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblzusers extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="kd_user,kd_dokter,user_names,full_name,description2,password,tag1,tag2,userid,language_id,kd_unit"  
;
		$this->TblName='zusers';
			TblBase::TblBase();
		$this->SQL=$this->db;
	}

	
	function FillRow($rec)
	{
		$row=new Rowzusers;
		$row->KD_USER      =$rec->kd_user;
		$row->USER_NAMES   =$rec->user_names;
		$row->FULL_NAME    =$rec->full_name;
		$row->DESCRIPTION2 =$rec->description2;
		$row->PASSWORD     =$rec->password;
		$row->TAG1         =$rec->tag1;
		$row->TAG2         =$rec->tag2;
		$row->USERID       =$rec->userid;
		$row->LANGUAGE_ID  =$rec->language_id;
		$row->KD_UNIT      =$rec->kd_unit;
		$row->KD_MILIK     =$rec->kd_milik;
		$row->KD_UNIT_FAR  =$rec->kd_unit_far;
		$row->KD_DOKTER    =$rec->kd_dokter;
		return $row;
	}
}
class Rowzusers
{
    public $KD_USER;
    public $USER_NAMES;
    public $FULL_NAME;
    public $DESCRIPTION2;
    public $PASSWORD;
    public $TAG1;
    public $TAG2;
    public $USERID;
    public $LANGUAGE_ID;
    public $KD_UNIT;
	public $KD_MILIK;
	public $KD_UNIT_FAR;
	public $KD_DOKTER;
}

?>