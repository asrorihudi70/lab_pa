<?php
/**
* @author Ali
* @copyright NCI 2010
*/

class zmodgroup extends Model
{

	function zmodgroup()
	{
		parent::Model();
		$this->load->database();
	}

	function create($data)
	{
		$this->db->set('Mod_Group', $data['Mod_Group']);
		$this->db->set('Mod_Img', $data['Mod_Img']);
		$this->db->set('Mod_Row', $data['Mod_Row']);
		$this->db->insert('zmodgroup');

		return $this->db->affected_rows();
	}

	function read($id = null)
	{
		if (isset($id)) {
			$this->db->where('mod_group', $id);
		}
		$query = $this->db->get('zmodgroup');

		return $query;
	}

	function read_join_zmodule($id = null)
	{
		if (isset($id)) {
			$this->db->where($id);
		}
		$query = $this->db->query("SELECT * FROM zmodgroup WHERE mod_group in (SELECT mod_group FROM zmodule) ORDER BY mod_group asc");

		return $query;
	}

	function readAll()
	{
		$query = $this->db->get('zmodgroup');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('Mod_Group', $data['Mod_Group']);
		$this->db->set('Mod_Img', $data['Mod_Img']);
		$this->db->set('Mod_Row', $data['Mod_Row']);
		$this->db->update('zmodgroup');

		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('Mod_Group', $id);
		$this->db->delete('zmodgroup');

		return $this->db->affected_rows();
	}

}



?>