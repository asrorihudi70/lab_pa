<?php
/**
 * @author OLIB
 * @copyright 2008
 */


class tblviewinfousergroup extends TblBase
{
	
	function __construct()
	{
		$this->StrSql="group_id, group_name, group_desc";
	$this->SqlQuery="
					select group_id, group_name, group_desc from zgroups
									 ";

		$this->TblName='viewinfousergroup';
                TblBase::TblBase(true);
	}



	function FillRow($rec)
	{
		$row=new RowGroupUser;
		$row->GROUP_ID=$rec->group_id;
		$row->GROUP_DESC=$rec->group_desc;
		$row->GROUP_NAME=$rec->group_name;
	
		return $row;
		
	}
}
class RowGroupUser
{
    public $GROUP_ID;
	public $GROUP_DESC;
	public $GROUP_NAME;

	//public $RP='Rp';
	 
}

?>