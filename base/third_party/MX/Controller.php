<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
require dirname(__FILE__).'/Base.php';
class MX_Controller {
	public $autoload = array();
	public $setting=array();
	public $mod_id='';
	public function __construct() {
		parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		// log_message('debug', $class." MX_Controller Initialized");	
		$this->load = clone load_class('Loader');
		$this->load->_init();	
		$this->load->_autoloader($this->autoload);
	}
	public function __get($class) {
		return CI::$APP->$class;
	}
	public function setSetting($mod_id,$keys=array()){
		$this->mod_id=$mod_id;
		$jum=count($keys);
		$subQuery='';
		if($jum>0){
			$q='';
			for($i=0,$iLen=$jum;$i<$iLen;$i++){
				if($q!=''){
					$q.=",";
				}
				$q.="'".$keys[$i]."'";
			}
			$subQuery="AND keys in(".$q.")";
		}
		$res=$this->db->query("SELECT keys,valuess FROM zmodule_setting where mod_id='".$mod_id."' ".$subQuery)->result();
		for($i=0,$iLen=count($res);$i<$iLen;$i++){
			$this->setting[$res[$i]->keys]=$res[$i]->valuess;
		}
		return $this->setting;
	}
	public function getSetting($key){
		if(isset($this->setting[$key])){
			return $this->setting[$key];
		}else{
			echo "Setting Mod ID '".$this->mod_id."' Kode Setting '".$key."' Tidak Ada.";
			exit;
		}		
	}
}