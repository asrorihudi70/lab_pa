<!DOCTYPE html>
<html>

<head>
    <title>Tanda Tangan Touch Screen HTML</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript" src="<?= base_url() . 'assets/js/jquery.signature.js'; ?>"></script>
    <script type="text/javascript" src="<?= base_url() . 'assets/js/jquery.ui.touch-punch.min.js'; ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url() . 'assets/css/jquery.signature.css'; ?>">
    <style>
        #fileInputContainer {
            position: relative;
            overflow: hidden;
            display: inline-block;
        }

        #fileInput {
            font-size: 100px;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
        }

        #customButton {
            cursor: pointer;
        }

        .kbw-signature {
            width: 300px;
            height: 300px;
        }

        #sig canvas {
            width: 100% !important;
            height: auto;
        }

        .container {
            text-align: center;
        }

        button {
            width: 400px;
        }
    </style>
</head>

<body>

    <div class="container">
        <form method="POST" action="<?= base_url() . 'index.php/general/SignaturController/uploadDOPE'; ?>" enctype="multipart/form-data" onsubmit="return validateForm()">
            <h1>Tanda Tangan</h1>
            <div class="col-md-12">
                <label for="">Silahkan tanda tangan di bawah ini:</label>
                <br />
                <div>
                    <label for="namaTtd">Nama : </label>
                    <input type="text" id="namaTtd" name="namaTtd" style="width:350px;">
                </div>
                <div id="sig"></div>
                <br />
                <br />
                <button class="btn btn-danger" id="clear">Hapus Tanda Tangan</button>
                <textarea id="signature64" name="signed" style="display: none"></textarea>
                <input id="kdDokter" name="kdDokter" style="display: none">
            </div>
            <br />
            <button class="btn btn-success">Simpan Tanda Tangan</button>
            <br />
            <br />
            <button id="fileInputContainer" class="btn btn-success">
                <label for="fileInput" id="customButton">Upload Tanda Tangan</label>
                <input type="file" name="signatureFile" accept=".png, .jpg, .jpeg" id="fileInput" onchange="loadSignature(this)" />
            </button>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript">
        function validateForm() {
        var namaTtd = document.getElementById('namaTtd').value;
        var kdDokter = document.getElementById('kdDokter').value;
        if (namaTtd == '' || namaTtd == null) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Field "Nama" harus diisi!'
            });
            return false;
        }
        if (kdDokter == '' || kdDokter == null) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Harap Masukkan Dokter yg Sudah Terdaftar!'
            });
            return false;
        }
        return true;
        }

        var sig = $('#sig').signature({
            syncField: '#signature64',
            syncFormat: 'PNG'
        });
        $('#clear').on('click', function(e) {
            e.preventDefault();
            sig.signature('clear');
            $("#signature64").val('');
        });

        $(document).ready(function() {
            var popupMessage = '<?php echo $popupMessage; ?>';
            if (popupMessage !== '') {
                // alert(popupMessage);
            }
        });

        var namaData = <?php echo json_encode($dokter_data); ?>;

        $("#namaTtd").autocomplete({
            source: function(request, response) {
                var term = request.term.toLowerCase();
                var filteresData = namaData.filter(function(item) {
                    console.log(item);
                    return item.nama_dokter.toLowerCase().indexOf(term) !== -1;
                });
                response($.map(filteresData, function(item) {
                    return {
                        label: item.nama_dokter,
                        value: item.nama_dokter,
                        kd_dokter: item.kd_dokter,
                        paraf: item.paraf
                    };
                }));
            },
            select: function(event, ui) {
                console.log(ui.item);
                console.log(ui.item.kd_dokter);
                $("#kdDokter").val(ui.item.kd_dokter);
                if (ui.item.paraf != undefined) {
                    console.log('ada tanda tangan');
                    var initialValue = ui.item.paraf;
                    sig.signature('draw', initialValue);
                    $("#signature64").val(initialValue);
                } else {
                    sig.signature('clear');
                    $("#signature64").val('');
                }
            },
            autoFocus: true
        });

        function loadSignature(input) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var img = new Image();
                img.src = e.target.result;
                img.onload = function() {
                    var canvas = document.createElement('canvas');
                    var context = canvas.getContext('2d');
                    canvas.width = $('#sig').width(); // Sesuaikan lebar dengan lebar div "sig"
                    canvas.height = $('#sig').height(); // Sesuaikan tinggi dengan tinggi div "sig"
                    context.drawImage(img, 0, 0, canvas.width, canvas.height);
                    sig.signature('draw', canvas.toDataURL('image/png'));
                    $("#signature64").val(canvas.toDataURL('image/png'));
                };
            };
            reader.readAsDataURL(input.files[0]);
        }
    </script>
</body>

</html>