<HTML>
<style type="text/css"> 
    .welcometext { font-size: 8pt; color: #333333 }
    .welcomeheader { font-weight: bold; font-size: 8pt; color: #333333 }
    body{
        /*background-color:#000;*/
        background-image: url('images/aset/main/MainForm_Medismart_Slice_06.png');
        /*width: 90%; */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        margin:0px;
        font-family: calibri;
    }
    
    header{
        /*width: 100%; */
        min-height: 65px;
        background: -webkit-linear-gradient(-135deg, #174025, #45BF6E);
        background: -o-linear-gradient(-135deg, #174025, #45BF6E);
        background: -moz-linear-gradient(-135deg, #174025, #45BF6E);
        background: linear-gradient(-135deg, #174025, #45BF6E);
    }

    .content_{
        /*width: 100%;*/
        font-size: 12px;
        padding: 5px 10px 5px 10px;
    }
</style>
<?php $base = "http://localhost:8080/rs_suaka_insan/RSSI_TRAINING/";?>
<link rel ="stylesheet" type="text/css" href="<?php echo $base; ?>/Doc Asset/css login/version 1/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<title>Selamat Datang di Web Site NCI MediSmart</title>
<body>
    <!-- <h2>Selamat Datang, <?php //echo $this->session->userdata['user_id']['username']; ?></h2> -->
    <!-- <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
            <td width="25%"><img src="<?php //echo base_url(); ?>Img Asset/rsunandlogo-small.png" /></td>
            <td width="25%"></td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
    </table> -->
    <header>
        <div style="float:left; color:#fff; padding: 25px 10px 25px 10px;">SISTEM INFORMASI RUMAH SAKIT</div>
        <div style="float:right; color:#fff; font-size:10px; padding: 25px 10px 25px 10px;">powered by NCI</div> 
    </header>
    <div class="content_">
        <h2>Selamat Datang di SISTEM INFORMASI RUMAH SAKIT</h2>
        <div style="margin-top: -15px;">Untuk memahami dan menggunakannya, bacalah petunjuk penggunaan terlebih dahulu.</div><br>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="15%" align="center" style="background: #2E8049;" height="45"><img src="<?php echo $base; ?>Img Asset/Modern XP/Png x256/ModernXP (1).png" width="50" height="50"></td>
                <td align="left" valign="top" class="content_"><h4>SISTEM INFORMASI RUMAH SAKIT NCI-MediSmart</h4>
                    <div style="margin-top: -10px;">Merupakan sistem informasi manajemen rumah sakit berupa software terintegrasi berbasis web yang mengelola proses pencatatan medis dan data keuangan dari mulai pelayanan (front office), penunjang medis, hingga top manajemen (back office).</div>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="12"></td>
            </tr>
            <tr>
                <td width="15%" align="center" style="background: #2E8049;" height="45"><img src="<?php echo $base; ?>Img Asset/Modern XP/Png x256/ModernXP (21).png" width="50" height="50"></td>
                <td align="left" valign="top" class="content_"><h4>PENGUMUMAN DAN PEMBERITAHUAN</h4>
                    <div style="margin-top: -10px;">-</div>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="12"></td>
            </tr>
            <tr>
                <td width="15%" align="center" style="background-color: #2E8049;" height="45"><img src="<?php echo $base; ?>Img Asset/Modern XP/Png x256/ModernXP (30).png" width="50" height="50"></td>
                <td align="left" valign="top" class="content_"><h4>PETUNJUK PENGGUNAAN</h4>
                    Petunjuk penggunaan Application Software.
                </td>
            </tr>
            <tr>
                <td colspan="2" height="12"></td>
            </tr>
            <tr>
                <td width="15%" align="center" style="background-color: #2E8049;"><img src="<?php echo $base; ?>Img Asset/logors.png" width="50" height="50"></td>
                <td align="left" valign="top" class="content_"><h4><a href="https://www.rssuakainsan.or.id/" target="_blank">WEB SITE</a></h4>
                    RSU SUAKA INSAN - Web Site.
                </td>
            </tr>
            <tr>
                <td colspan="2" height="12"></td>
            </tr>
            <!-- <tr>
                <td width="15%" align="center" style="background-color: #2E8049;" height="45"><img src="<?php// echo $base; ?>Img Asset/Phone-icon.png" width="35" height="50"></td>
                <td align="left" valign="top" class="content_"><h4>NO. TELEPON (HOTLINE)</h4>

                </td>
            </tr> -->
        </table>
    </div>
    <!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th width="50%" style="height: 64px;">Informasi Rumah Sakit</th>
            <th width="50%" style="height: 64px;">Informasi Sistem Rumah Sakit</th>
        </tr>
        <tr>
            <td width="50%">Informasi Rumah Sakit</td>
            <td width="50%">Informasi Sistem Rumah Sakit</td>
        </tr>
    </table> -->
</body>
</HTML>
