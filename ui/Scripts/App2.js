/*!
* Ext JS Library 3.1.0
* Copyright(c) 2006-2009 Ext JS, LLC
* licensing@extjs.com
* http://www.extjs.com/license
*/

Ext.useShims = true;
Ext.namespace('WebApp');

Ext.app.App = function(cfg) {
    Ext.apply(this, cfg);
    this.addEvents({
        'ready': true,
        'beforeunload': true
    });

    Ext.onReady(this.initApp, this);
};

Ext.extend(Ext.app.App, Ext.util.Observable, {
    isReady: false,
    startMenu: null,
    modules: null,

    getStartConfig: function() {

    },

    initApp: function() {
        this.startConfig = this.startConfig || this.getStartConfig();

        this.desktop = new Ext.Desktop(this);

        this.launcher = this.desktop.taskbar.startMenu;

        this.modules = this.getModules();
        if (this.modules) {
            this.initModules(this.modules);
        }
        // add sstyles
        this.styles = this.styles || this.getStyles();
        this.initStyles();
        this.init();

        Ext.EventManager.on(window, 'beforeunload', this.onUnload, this);
        this.fireEvent('ready', this);
        this.isReady = true;
    },

    getModules: Ext.emptyFn,
    init: Ext.emptyFn,

    initModules: function(ms) {
        for (var i = 0, len = ms.length; i < len; i++) {
            var m = ms[i];
            this.launcher.add(m.launcher);
            m.app = this;
        }
    },

    initStyles: function() {
        var s = this.styles;
        if (!s) {
            return false;
        }

        this.desktop.setBackgroundColor(s.backgroundcolor);
        this.desktop.setFontColor(s.fontcolor);
        this.desktop.setTheme(s.theme);
        this.desktop.setTransparency(s.transparency);
        this.desktop.setWallpaper(s.wallpaper);
        this.desktop.setWallpaperPosition(s.wallpaperposition);

        return true;
    },

    getModule: function(name) {
        var ms = this.modules;
        for (var i = 0, len = ms.length; i < len; i++) {
            if (ms[i].id == name || ms[i].appType == name) {
                return ms[i];
            }
        }
        return '';
    },

    onReady: function(fn, scope) {
        if (!this.isReady) {
            this.on('ready', fn, scope);
        } else {
            fn.call(scope, this);
        }
    },

    getDesktop: function() {
        return this.desktop;
    },

    onUnload: function(e) {
        if (this.fireEvent('beforeunload', this) === false) {
            e.stopEvent();
        }
    }
});

// proxy for data all

WebApp.DataProxy = function(config) {
    WebApp.DataProxy.superclass.constructor.call(this, Ext.apply({
        api: {
            //read: {url:'./Datapool.mvc/ReadDataObj',method: 'GET'},                        
            read: './Datapool.mvc/ReadDataObj',
            //create: {url:'./Datapool.mvc/CreateData',method: 'POST'},
            //create: './Datapool.mvc/UpdateData',
            //create: './Datapool.mvc/CreateData',
            create: 'CreateCustomers',
            //update: {url:'./Datapool.mvc/UpdateData',method: 'PUT'},
            //update: './Datapool.mvc/UpdateData',
            update: 'UpdateCustomers',
            destroy: 'DeleteCustomers'
            //destroy: {url:'./Datapool.mvc/DeleteData',method: 'DELETE'}
        }
    }, config));
};
Ext.extend(WebApp.DataProxy, Ext.data.HttpProxy, {});


WebApp.DataProxyGrid = function(config) {
    WebApp.DataProxyGrid.superclass.constructor.call(this, Ext.apply({
        api: {
            //read: {url:'./Datapool.mvc/ReadDataObj',method: 'GET'},                        
            read: './Datapool.mvc/ReadDataObj',
            //create: {url:'./Datapool.mvc/CreateData',method: 'POST'},
            create: './Datapool.mvc/UpdateData',
            //create: './Datapool.mvc/CreateData',
            //create: 'CreateCustomers',
            //update: {url:'./Datapool.mvc/UpdateData',method: 'PUT'},
            //update: './Datapool.mvc/UpdateData',
            update: 'UpdateCustomers',
            destroy: 'DeleteCustomers'
            //destroy: {url:'./Datapool.mvc/DeleteData',method: 'DELETE'}
        }
    }, config));
};
Ext.extend(WebApp.DataProxyGrid, Ext.data.HttpProxy, {});

//test nulis
var writer = new Ext.data.JsonWriter({
    encode: false   // <-- don't return encoded JSON -- causes Ext.Ajax#request to send data using jsonData config rather than HTTP params
});


WebApp.DataStore = function(config) {
    WebApp.DataStore.superclass.constructor.call(this, Ext.apply({
        paramNames: {
            "start": "Skip",
            "limit": "Take",
            "sort": "Sort",
            "dir": "SortDir",
            "target": "target",
            "param": "params"
        },
        proxy: new WebApp.DataProxy(),
        writer: writer,
        //        reader: Customersreader,
        //        writer: Customerswriter,  // <-- plug a DataWriter into the store just as you would a Reader
        //autoSave: true,
        listeners: {
            write: function(store, action, result, res, rs) {
                App.setAlert(res.success, res.message); // <-- show user-feedback for all write actions
            },
            exception: function(proxy, type, action, options, res, arg) {
                if (type === 'remote') {
                    Ext.Msg.show({
                        title: 'REMOTE EXCEPTION',
                        msg: res.message,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        },
        totalProperty: 'totalrecords',
        root: 'ListDataObj',
        remoteSort: true,
        autoLoad: false,
        restful: true,
        //id: "KD_CUSTOMER",
        method: "POST"
    }, config));
};

Ext.extend(WebApp.DataStore, Ext.data.JsonStore, {});

WebApp.PaggingBar = function(config) {
    WebApp.PaggingBar.superclass.constructor.call(this, Ext.apply({
        paramNames: {
            "start": "Skip",
            "limit": "Take",
            "sort": "Sort",
            "dir": "SortDir",
            "target": "target",
            "param": "param"
        },
        // private
        doLoad: function(start) {
            var o = {}, pn = this.getParams();
            //alert("pagging " + this.store.lastOptions.params.target + " from " + start + " to " +this.pageSize);
            o[pn.start] = start;
            o[pn.limit] = this.pageSize;
            o[pn.sort] = this.store.lastOptions.params.Sort;
            o[pn.dir] = this.store.lastOptions.params.SortDir;
            o[pn.target] = this.store.lastOptions.params.target;
            o[pn.param] = this.store.lastOptions.params.param;
            o["SID"] = Math.random();
            if (this.fireEvent('beforechange', this, o) !== false) {

                this.store.load({ params: o });

            }
        }

    }, config));
};
Ext.extend(WebApp.PaggingBar, Ext.PagingToolbar, {});