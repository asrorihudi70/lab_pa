﻿/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var vWinFormEntryChgPwd;

function GetChangePassword() 
{
    vWinFormEntryChgPwd = new Ext.Window
	(
		{
			id: 'vWinFormEntryChgPwd',
			title: 'Change Password',
			closeAction: 'hide',
			closable:true,
			width: 400,
			height: 220,//240,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'ChgPwd',
			modal: true,                                   
			items: 
			[
				
				GetPanelChangePwd() 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
    vWinFormEntryChgPwd.show();
};

function GetPanelChangePwd() 
{
	var vTabPanelChangePwd = new Ext.Panel
	(
		{
		    id: 'vTabPanelChangePwd',
		    margins: '5 5 5 5',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    frame: false,
		    border: true,
		    height: 173,
			width: 360,
		    anchor: '100%',
		    items:
			[
				
				{
					columnWidth:1,
					layout: 'form',
					labelWidth:120,
					width: 340,
					labelAlign:'right',
					border: false,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'User Name ',
							name: 'txtUsernameChgPwd',
							id: 'txtUsernameChgPwd',
							readOnly:true,
							value:strUser,
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							inputType:'password',
							fieldLabel: 'Old Password ',
							name: 'txtOldPwd',
							id: 'txtOldPwd',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							inputType:'password',
							fieldLabel: 'New Password ',
							name: 'txtNewPwd',
							id: 'txtNewPwd',
							anchor: '100%'
						},
						{
							xtype: 'textfield',
							inputType:'password',
							fieldLabel: 'Confirm Password ',
							name: 'txtConfirmPwd',
							id: 'txtConfirmPwd',
							anchor: '100%'
						},
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 5 0 0' },
							style:{'margin-left':'119px','margin-top':'15px'},
							anchor: '94%',
							layoutConfig: 
							{
								padding: '3',
								pack: 'start',
								align: 'middle'
							},
							items:
							[
								{
									xtype:'button',
									text:'Ok',
									width:70,
									style:{'margin-left':'3px','margin-top':'2px'},
									hideLabel:true,
									id: 'btnOkChgPwd',
									handler:function()
									{
										SaveChangePassword();
									}
								},
								{
									xtype:'button',
									text:'Cancel',
									width:70,
									style:{'margin-left':'5px','margin-top':'2px'},
									hideLabel:true,
									id: 'btnChgPwd',
									handler:function() 
									{
										vWinFormEntryChgPwd.close();
									}
								}
							]
						}
					]
							
				}
			]
		}
	)
	
	var FormChangePwd = new Ext.Panel  
	(
		{
			id: 'FormChangePwd',
			region: 'center',
			layout: 'form',
			title: '',
			anchor:'100%',
			bodyStyle: 'padding:7px 7px 7px 7px',
			border: true,
			width: 380,
			height:190,// 210,
			shadhow: true,
			items: 
			[
				vTabPanelChangePwd
			]
        }
	);
	
	return FormChangePwd;
};



function SaveChangePassword()
{
	if (ValidasiChgPwd('Change Password') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/CreateDataObj",
				params:getParamChgPwd(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success == true) 
					{
						ShowPesanInfoChgPwd('Change Passoword Succeesed','Change Password');
						vWinFormEntryChgPwd.close();
						
					}
					else if (cst.success == false) 
					{
						ShowPesanWarningChgPwd('Change Passoword Not Succeesed','Change Password');
					}
					else
					{
						ShowPesanErrorChgPwd(cst.pesan,'Change Password')
					}
				}
			}
		)
	};
};

function ValidasiChgPwd(modul)
{
	var x = 1;
	if (Ext.get('txtOldPwd').getValue() == '' || Ext.get('txtNewPwd').getValue() == '' || Ext.get('txtConfirmPwd').getValue() == '' || (Ext.get('txtConfirmPwd').getValue() != Ext.get('txtNewPwd').getValue()))
	{
		if (Ext.get('txtOldPwd').getValue() == '')
		{
			ShowPesanWarningChgPwd(nmGetValidasiKosong('Old Password'),modul);
			x=0;
		}
		else if (Ext.get('txtNewPwd').getValue() == '')
		{
			ShowPesanWarningChgPwd(nmGetValidasiKosong('New Password'),modul);
			x=0;
		}
		else if (Ext.get('txtConfirmPwd').getValue() == '')
		{
			ShowPesanWarningChgPwd(nmGetValidasiKosong('Confirm Password'),modul);
			x=0;
		}
		else if (Ext.get('txtConfirmPwd').getValue() != Ext.get('txtNewPwd').getValue())
		{
			ShowPesanWarningChgPwd('Confirm Password different with New Password',modul);
			x=0;
		}
		
	}
	return x;
};


function ShowPesanInfoChgPwd(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width:250
		}
	);
};

function ShowPesanErrorChgPwd(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width:350
		}
	);
};

function ShowPesanWarningChgPwd(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:250
		}
	);
};


function getParamChgPwd()
 {
    var params =
	{	
		Table: 'ViewChangePwd',   
	    nUser: strUser,
		OldPwd: Ext.get('txtOldPwd').getValue(),
	    NewPwd: Ext.get('txtNewPwd').getValue()
	};
    return params
};



///---------------------------------------------------------------------------------------///