// --------------------------------------- # Start Function # ---------------------------------------
function timestimetodate(date){
 var a= new Date(date);
 return a.getFullYear()+'-'+(a.getMonth()+1)+'-'+a.getDate();
}
function getSetting(menu_id,setting_key,callback){
	Ext.Ajax.request({
		method:'POST',
		url: baseURL + "index.php/main/main/getSetting",
		params: {
			mod_id: menu_id,
			'key[]': setting_key
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if(cst.result=='SUCCESS'){
				callback(cst.data);
			}else{
				if(cst.message != null && cst.message !=''){
					Ext.Msg.alert('Information',cst.message);
				}
			}
		},error: function (jqXHR, exception) {
			Nci.ajax.ErrorMessage(jqXHR, exception);
		}
	});
}
var shortcut={
	id:'',
	storage:null,
	execute:function(a,e){
		this.storage=JSON.parse(localStorage.getItem('shortcut'));
		this.id=this.getModule();
		if(this.storage != undefined && this.storage != null){
			if(this.storage[this.id] != undefined){
				if(this.storage[this.id].length>0){
					if(this.storage[this.id][0] != undefined){
						for(var i=0,iLen=this.storage[this.id][0].list.length; i<iLen;i++){
							if(a==this.storage[this.id][0].list[i].key){
								e.preventDefault();
								eval(this.storage[this.id][0].list[i].fn);
								f();
							}
						}
						
					}
				}
			}
		}
	},
	set:function(fn){
		this.storage=JSON.parse(localStorage.getItem('shortcut'));
		this.storage=JSON.parse(localStorage.getItem('shortcut'));
		this.id=this.getModule();
		if(this.storage==undefined || this.storage==null){
			this.storage={};
			this.storage[this.id]=[];
		}else{
			if(this.storage[this.id] == undefined ||this.storage[this.id]==null){
				this.storage[this.id]=[];
			}
		}
		for(var i=0,iLen=this.storage[this.id].length;i<iLen;i++){
			if(this.storage[this.id][i].code==fn.code){
				this.storage[this.id].splice(i,1);
			}
		}
		for(var i=0,iLen=fn.list.length;i<iLen;i++){
			fn.list[i].fn="var f="+String(fn.list[i].fn);
		}
		this.storage[this.id].unshift(fn);
		localStorage.setItem('shortcut',JSON.stringify(this.storage));
	},
	remove:function(code){
		this.id=this.getModule();
		this.storage=JSON.parse(localStorage.getItem('shortcut'));
		if(this.storage != undefined && this.storage != null){
			if(this.storage[this.id] != undefined){
				if(this.storage[this.id].length>0){
					for(var i=0,iLen=this.storage[this.id].length;i<iLen;i++){
						if(this.storage[this.id][i].code==code){
							this.storage[this.id].splice(i,1);
							break;
						}
					}
				}
			}
		}
		localStorage.setItem('shortcut',JSON.stringify(this.storage));
	},
	getModule:function(){
		var crItem=Ext.getCmp('MainContentPage').items.items[0].items.items;
		var id='';
		for(var i=0,iLen=crItem.length; i<iLen; i++){
			if(crItem[i].hidden==false){
				id=crItem[i].id;
			}
		}
		return id;
	}
};
$(document).unbind().bind('keydown',function(e){
	var nav=navigator.platform.match("Mac");
	if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){//ctrl+s
		shortcut.execute('ctrl+s',e);
	}else if (e.keyCode == 70 && ( nav? e.metaKey : e.ctrlKey)){//ctrl+f
		shortcut.execute('ctrl+f',e);
	}else if(e.keyCode == 68 && ( nav? e.metaKey : e.ctrlKey)){//ctrl+d
		shortcut.execute('ctrl+d',e);
	}else if(e.keyCode == 78 && ( nav? e.metaKey : e.ctrlKey)){//ctrl+n
		shortcut.execute('ctrl+n',e);
	}else if(e.keyCode == 79 && ( nav? e.metaKey : e.ctrlKey)){ //ctrl+o
		 shortcut.execute('ctrl+o',e);
	}else if(e.keyCode == 27){//esc
		shortcut.execute('esc',e);
	}else if(e.keyCode == 112){//f1
		shortcut.execute('f1',e);
	}else if(e.keyCode == 113){//f2
		shortcut.execute('f2',e);
	}else if(e.keyCode == 114){//f3
		shortcut.execute('f3',e);
	}else if(e.keyCode == 115){//f4
		shortcut.execute('f4',e);
	}else if(e.keyCode == 116){//f5
		shortcut.execute('f5',e);
	}else if(e.keyCode == 117){//f6
		shortcut.execute('f6',e);
	}else if(e.keyCode == 118){//f7
		shortcut.execute('f7',e);
	}else if(e.keyCode == 119){//f8
		shortcut.execute('f8',e);
	}else if(e.keyCode == 120){//f9
		shortcut.execute('f9',e);
	}else if(e.keyCode == 121){//f10
		shortcut.execute('f10',e);
	}else if(e.keyCode == 122){//f11
		shortcut.execute('f11',e);
	}else if(e.keyCode == 123){//f12
		shortcut.execute('f12',e);
	}
});
/**
*   Function : dataGrid_mod_name
*   
*   Sebuah fungsi untuk membuat custom paging pada grid
*/

var $progress = $('#progress');
$(document).ajaxStart(function() {
	//if ($progress.length === 0) {
  		//$progress = $('<div><dt/><dd/></div>').attr('id', 'progress');
  		//$("body").append($progress);
	//}
	//$progress.width((50 + Math.random() * 30) + "%");
});

$(document).ajaxComplete(function() {
   // $progress.width("100%").delay(200).fadeOut(400, function() {
  	//	$progress.width("0%").delay(200).show();
    //});
 });

var _bpjs={
	cetakSep:function(noSep,callback,catatan){
		if (noSep === "" ){
			Ext.MessageBox.alert('cetak Sep', 'Nomor SEP tidak ada.');
		}else{
			var note="";
			note=catatan.replace("/","~~_~~");
			var url = baseURL + "index.php/rawat_jalan/functionRWJ/cetaksep/" +noSep+"/"+note;
			new Ext.Window({
				title: 'SEP',
				width: 900,
				height: 500,
				constrain: true,
				modal: true,
				listeners:{
					deactivate: function (){
						if(callback != undefined){
							callback();
						}
					}
				},
                tbar:[
                {
                    xtype:'button',
                    text:'Cetak',
                    iconCls     : 'print',
					hidden:false,
                    handler: function (){
           //                          Ext.Ajax.request({
           //                              method:'POST',
           //                              url: baseURL + "index.php/rawat_jalan/functionRWJ/cetaksepdirect",
           //                              params: {
           //                                  noSep: noSep,
											// catatan: note
           //                              },
           //                              success: function (o) {
                                            
                                            
           //                              },error: function (jqXHR, exception) {
                                            
           //                              }
           //                          });
                        window.open(baseURL + "index.php/laporan/lap_bpjs/cetak_sep/"+noSep, '_blank');
                    }
                }
                ],
				html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
			}).show();
		}
	},
	generateObjectToString:function(obj){
		$this=this;
		var res='';
		if(typeof obj=='object'){
			for(key in obj){
				if(typeof obj[key]=='string' || typeof obj[key]=='number'){
					res+=key+' : '+obj[key]+' \n';
				}else if(typeof obj[key]=='object'){
					res+=this.generateObjectToString(obj[key]);
				}
			}
		}else if(typeof obj=='string'){
			res=obj+' \n';
		}else if(typeof obj=='number'){
			res=obj+' \n';
		}
		return res;
	}
};
function bbar_paging(mod_name, count, source)
{
    // Deklarasi Variabel pada bbar_paging # --------------
    var combo_mod_name = "combo_"+mod_name;
    var count_mod_name = "count_"+mod_name;
    var source_mod_name = "source_"+mod_name;
    var bbar_mod_name = "bbar_"+mod_name;
    var count_mod_name = count;
    var source_mod_name = source;
    // End Deklarasi Variabel pada bbar_paging # --------------

    // Pengaturan Bar Paging Bawah # --------------

    // Combo Per Halaman # --------------
    var combo_mod_name = new Ext.form.ComboBox
    (
        {
            name : 'perpage_bbar_paging',
            width: 40,
            store: new Ext.data.ArrayStore
            (
                {
                    fields: ['id'],
                    data  : [
                            ['1'],
                            ['10'], 
                            ['25'],
                            ['50']
                    ]
                }
            ),
            mode : 'local',
            value: count_mod_name,
         
            listWidth     : 40,
            triggerAction : 'all',
            displayField  : 'id',
            valueField    : 'id',
            editable      : false,
            forceSelection: true
        }
    );
    // End Combo Per Halaman # --------------

    // ExtJS Paging # --------------
    var bbar_mod_name = new WebApp.PaggingBar
    (
        {
            store: source,
            displayInfo: true,
            pageSize: count_mod_name,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;",
            items : [
                        '-',
                        'Per Halaman: ',
                        combo_mod_name
                    ]
        }
    );
    // End ExtJS Paging # --------------
    
    // Event refresh tampilan grid sesuai jumlah saat dipilih pada combobox  # --------------    
    combo_mod_name.on('select', function(combo_mod_name, record_mod_name) 
        {
            bbar_mod_name.pageSize = parseInt(record_mod_name.get('id'), 10);
            bbar_mod_name.doLoad(bbar_mod_name.cursor);
        }, 
    this
    );
    // End Event refresh tampilan grid sesuai jumlah saat dipilih pada combobox  # --------------    

    // End Pengaturan Bar Paging Bawah # --------------
    return bbar_mod_name;
}

    
// --------------------------------------- # End Function # ---------------------------------------

//----------------------------------------------------
// kontributor : Iman G.  nama fungsi asli (tgltampil)
//----------------------------------------------------



function ShowDate(cTglAsli) {
    if (cTglAsli === undefined) {
            cTglAsli = new Date();
            cTglAsli = cTglAsli.format('Ymd');
        }
//        if (cTglAsli.length !== 8) {
//            if (cTglAsli === "") {
//                cTglAsli = new Date()
//            };
//            var dsl = new Date(cTglAsli);
//            cTglAsli = dsl.format('Ymd')
//        };
//        var cHari = cTglAsli.substr(6, 2);
//        var cBulan = cTglAsli.substr(6, 4);
//        var cThn = cTglAsli.substr(0, 4);
        var cHari = cTglAsli.substr(8, 2);
        var cBulan = cTglAsli.substr(5, 2);
        var cThn = cTglAsli.substr(0, 4);
        var cBln = '';
        if (cBulan == '01')
            {
                cBln = 'Jan';
            }
            else if (cBulan == '02')
            {
                cBln = 'Feb';
            }
             else if (cBulan == '03')
            {
                cBln = 'Mar';
            }
             else if (cBulan == '04')
            {
                cBln = 'Apr';
            }
             else if (cBulan == '05')
            {
                cBln = 'May';
            }
             else if (cBulan == '06')
            {
                cBln = 'Jun';
            }
             else if (cBulan == '07')
            {
               cBln = 'Jul';
            }
             else if (cBulan == '08')
            {
                cBln = 'Aug';
            }
             else if (cBulan == '09')
            {
                cBln = 'Sep';

            }
             else if (cBulan == '10')
            {
                cBln = 'Oct';
            }
             else if (cBulan == '11')
            {
                cBln = 'Nov';
            }
             else if (cBulan == '12')
            {
                cBln = 'Dec';
            }
             else
            {
                cBln = '...';
            }
    return cHari + '/' + cBln + '/' + cThn
}
function ShowDateReal2(cTglAsli) {
    if (cTglAsli === undefined) {
            cTglAsli = new Date();
            cTglAsli = cTglAsli.format('Ymd');
        }

        var cHari = cTglAsli.substr(0, 2);
        var cBulan = cTglAsli.substr(3, 3);
        var cThn = cTglAsli.substr(7, 4);
        var cBln = '';
        if (cBulan == 'Jan')
            {
                cBln = '01';
            }
            else if (cBulan == 'Feb')
            {
                cBln = '02';
            }
             else if (cBulan == 'Mar')
            {
                cBln = '03';
            }
             else if (cBulan == 'Apr')
            {
                cBln = '04';
            }
             else if (cBulan == 'May')
            {
                cBln = '05';
            }
             else if (cBulan == 'Jun')
            {
                cBln = '06';
            }
             else if (cBulan == 'Jul')
            {
               cBln = '07';
            }
             else if (cBulan == 'Aug')
            {
                cBln = '08';
            }
             else if (cBulan == 'Sep')
            {
                cBln = '09';

            }
             else if (cBulan == 'Oct')
            {
                cBln = '10';
            }
             else if (cBulan == 'Nov')
            {
                cBln = '11';
            }
             else if (cBulan == 'Dec')
            {
                cBln = '12';
            }
             else
            {
                cBln = '...';
            }
    return   cHari+ '/' + cBln + '/' + cThn
}
function ShowDateReal(cTglAsli) {
    if (cTglAsli === undefined) {
            cTglAsli = new Date();
            cTglAsli = cTglAsli.format('Ymd');
        }

        var cHari = cTglAsli.substr(0, 2);
        var cBulan = cTglAsli.substr(3, 3);
        var cThn = cTglAsli.substr(7, 4);
        var cBln = '';
        if (cBulan == 'Jan')
            {
                cBln = '01';
            }
            else if (cBulan == 'Feb')
            {
                cBln = '02';
            }
             else if (cBulan == 'Mar')
            {
                cBln = '03';
            }
             else if (cBulan == 'Apr')
            {
                cBln = '04';
            }
             else if (cBulan == 'May')
            {
                cBln = '05';
            }
             else if (cBulan == 'Jun')
            {
                cBln = '06';
            }
             else if (cBulan == 'Jul')
            {
               cBln = '07';
            }
             else if (cBulan == 'Aug')
            {
                cBln = '08';
            }
             else if (cBulan == 'Sep')
            {
                cBln = '09';

            }
             else if (cBulan == 'Oct')
            {
                cBln = '10';
            }
             else if (cBulan == 'Nov')
            {
                cBln = '11';
            }
             else if (cBulan == 'Dec')
            {
                cBln = '12';
            }
             else
            {
                cBln = '...';
            }
    return  cBln + '/' + cHari + '/' + cThn
}

function ShowDateUbah(cTglAsli) {
    var cHari = cTglAsli.substr(0, 2);
    var cBln = cTglAsli.substr(2, 2);
    var cThn = cTglAsli.substr(4, 4);
     
    return cHari + '/' + cBln + '/' + cThn;  
}



function ShowDateUbahdatetimeextjs(cTglAsli){
    var cHari = cTglAsli.substr(0, 2);
    var cBln = cTglAsli.substr(2, 2);
    var cThn = cTglAsli.substr(4, 4);
               
    var tgl = cHari + '/' + cBln + '/' + cThn;
    var tglasliextjs = ShowDate(tgl);
    return tglasliextjs;
}

function formatnomedrec(noMedrec){
        var retVal=str_pad(noMedrec,7,'0','STR_PAD_LEFT');
        var getnewmedrec = retVal.substr(0,1) + '-' + retVal.substr(1,2) + '-' + retVal.substr(3,2) + '-' + retVal.substr(-2);
        //untuk cek jika ada kesalahan
//        alert(retVal.substr(0,1));
//        alert(retVal.substr(1,2));
//        alert(retVal.substr(3,2));
//        alert(retVal.substr(-2));
        return getnewmedrec;
}

function str_pad(input, pad_length, pad_string, pad_type) {
  //  discuss at: http://phpjs.org/functions/str_pad/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Michael White (http://getsprink.com)
  //    input by: Marco van Oort
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: str_pad('Kevin van Zonneveld', 30, '-=', 'STR_PAD_LEFT');
  //   returns 1: '-=-=-=-=-=-Kevin van Zonneveld'
  //   example 2: str_pad('Kevin van Zonneveld', 30, '-', 'STR_PAD_BOTH');
  //   returns 2: '------Kevin van Zonneveld-----'

  var half = '',
    pad_to_go;

  var str_pad_repeater = function(s, len) {
    var collect = '',
      i;

    while (collect.length < len) {
      collect += s;
    }
    collect = collect.substr(0, len);

    return collect;
  };

  input += '';
  pad_string = pad_string !== undefined ? pad_string : ' ';

  if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
    pad_type = 'STR_PAD_RIGHT';
  }
  if ((pad_to_go = pad_length - input.length) > 0) {
    if (pad_type === 'STR_PAD_LEFT') {
      input = str_pad_repeater(pad_string, pad_to_go) + input;
    } else if (pad_type === 'STR_PAD_RIGHT') {
      input = input + str_pad_repeater(pad_string, pad_to_go);
    } else if (pad_type === 'STR_PAD_BOTH') {
      half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
      input = half + input + half;
      input = input.substr(0, pad_length);
    }
  }

  return input;
}

function ShowNewDate(cTglAsli) {
     cTgl = cTglAsli //.substr(4, cTglAsli.length-4 );
	cTgl = Ext.isDate(cTgl) ? cTgl.format("M") : cTgl;

	
            var cBulan = cTgl.substring(3,6);

switch (cBulan) {
        case 'Jan': cBln = '01';
            break
        case 'Feb': cBln = '02';
            break
        case 'Mar': cBln = '03';
            break
        case 'Apr': cBln = '04';
            break
        case 'May': cBln = '05';
            break
        case 'Jun': cBln = '06';
            break
        case 'Jul': cBln = '07';
            break
        case 'Aug': cBln = '08';
            break
        case 'Sep': cBln = '09';
            break
        case 'Oct': cBln = '10';
            break
        case 'Nov': cBln = '11';
            break
        case 'Dec': cBln = '12';
    }
    return cBln
}

//function ShowDate(cTglAsli) 
//{
//    if (cTglAsli != 0) {
//        cTgl = cTglAsli; //.substr(4, cTglAsli.length-4 );
//        //    alert(cTgl + ' ' + cTglAsli );
//        //    if (cTgl == '') { cTgl = '2009:01:01'; }
//        if (cTglAsli === undefined) {
//            cTgl = new Date()
//            cTgl = cTgl.format('Ymd')
//        }
//        if (cTgl.length !== 8) {
//            if (cTgl === "") {
//                cTgl = new Date()
//            };
//            var dsl = new Date(cTgl);
//            cTgl = dsl.format('Ymd')
//        };
//        var cHari = cTgl.substr(6, 2);        
//        var cBulan = cTgl.substr(4, 2);
//        var cThn = cTgl.substr(0, 4);
//        var cBln = '';
//        switch (cBulan) {
//            case '01': cBln = 'Jan'
//                break
//            case '02': cBln = 'Feb'
//                break
//            case '03': cBln = 'Mar'
//                break
//            case '04': cBln = 'Apr'
//                break
//            case '05': cBln = 'May'
//                break
//            case '06': cBln = 'Jun'
//                break
//            case '07': cBln = 'Jul'
//                break
//            case '08': cBln = 'Aug'
//                break
//            case '09': cBln = 'Sep'
//                break
//            case '10': cBln = 'Oct'
//                break
//            case '11': cBln = 'Nov'
//                break
//            case '12': cBln = 'Dec'
//        }
//
//        if (cThn === '0001') {
//            return '';
//        }
//        else {
//            return cHari + '/' + cBln + '/' + cThn;
//        };
//    } else {
//        return '';
//    }
//};
//
function getMonthYear(cTglAsli, btmp) 
{
    if (cTglAsli === undefined) {
        cTgl = new Date()
    }
    cTgl = cTglAsli;

    if (cTgl.length !== 8) 
    {
        if (cTgl === "") 
        {
            cTgl = new Date()
        };
       var dsl = new Date(cTgl);
        cTgl = dsl.format('Ymd')
   };
    var cHari = cTgl.substr(6, 2);
    var cBulan = cTgl.substr(4, 2);
    var cThn = cTgl.substr(0, 4);

    if (btmp === 1) 
    {
        switch (cBulan) 
        {
            case '01': cBulan = '02';
                break
            case '02': cBulan = '03';
                break
            case '03': cBulan = '04';
                break
            case '04': cBulan = '05';
                break
            case '05': cBulan = '06';
                break
            case '06': cBln = '07';
                break
            case '07': cBulan = '08';
                break
            case '08': cBulan = '09';
                break
            case '09': cBulan = '10';
                break
            case '10': cBulan = '11';
                break
            case '11': cBulan = '12';
                break
            case '12': cBulan = '12';
        }
    }
    return cThn+cBulan;
};

function formatCurrency(num) 
{
    if (num != undefined) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' +
num.substring(num.length - (4 * i + 3));
        //return (((sign) ? '' : '-') + 'Rp.' + num);
        return (((sign) ? '' : '-') + num);
    }
    return 0;
};

function NonFormatCurrency(dblNilai) 
{
    dblNilai = dblNilai.replace('Rp.', '');
    for (var i = 0; i < dblNilai.length; i++) {
        var x = dblNilai.substr(i, 1);
        if (x === '.') {
            dblNilai = dblNilai.replace('.', '');
        }
    }
    return Ext.num(dblNilai);
};

function ShowMonth(cTglAsli) {

    cTgl = cTglAsli;

    if (cTgl.length !== 8) {
        if (cTgl === "") {
            cTgl = new Date()
        };
        var dsl = new Date(cTgl);
        cTgl = dsl.format('Ymd')
    };
    var cHari = cTgl.substr(6, 2);
    var cBulan = cTgl.substr(4, 2);
    var cThn = cTgl.substr(0, 4);
    var cBln = '';
    switch (cBulan) {
        case '01': cBln = 'Januari';
            break
        case '02': cBln = 'Febuari';
            break
        case '03': cBln = 'Maret';
            break
        case '04': cBln = 'April';
            break
        case '05': cBln = 'Mei';
            break
        case '06': cBln = 'Juni';
            break
        case '07': cBln = 'Juli';
            break
        case '08': cBln = 'Agustus';
            break
        case '09': cBln = 'September';
            break
        case '10': cBln = 'Oktober';
            break
        case '11': cBln = 'November';
            break
        case '12': cBln = 'Desember';
    }
    return cBulan + '~' + cBln;
};


function getnewformatdate(cTglAsli) {
    cTglreal = cTglAsli;

    var cBulan = cTglreal.substring(0,3);
    var cTahun = cTglreal.substring(3);
    
    switch (cBulan) {
        case 'Jan': cTglreal = '01';
            break
        case 'Feb': cTglreal = '02';
            break
        case 'Mar': cTglreal = '03';
            break
        case 'Apr': cTglreal = '04';
            break
        case 'May': cTglreal = '05';
            break
        case 'Jun': cTglreal = '06';
            break
        case 'Jul': cTglreal = '07';
            break
        case 'Aug': cTglreal = '08';
            break
        case 'Sep': cTglreal = '09';
            break
        case 'Oct': cTglreal = '10';
            break
        case 'Nov': cTglreal = '11';
            break
        case 'Dec': cTglreal = '12';
    }
    return cTglreal + cTahun;
}

function showLoading(condition)
{
	if(condition == 1)
	{
		document.getElementById('loading').style.visibility='visible';
		document.getElementById('overlay').style.visibility='visible';
	}
	else
	{
		document.getElementById('loading').style.visibility='hidden';
		document.getElementById('overlay').style.visibility='hidden';
	}
}

var Nci={};
Nci.ajax={};
Nci.form={};
Nci.form.Combobox={};

Nci.getId=function(){
	if(this.getId.number == undefined)this.getId.number=0;
	this.getId.number+=1;
	return 'nci-gen'+this.getId.number;
};

Nci.required=function(data){
	var val={},e=true;
	for(var i in data){
		if(data[i].required != undefined){
			if(data[i].required()==false){
				e=false;
			}
		}
		if(data[i].value != undefined){
			val[data[i].name]=data[i].value();
		}
	}
	return {required:e, params:val};
};
Nci.ajax.ErrorMessage=function(jqXHR, exception){
	if (jqXHR.status === 0) {
    	Ext.Msg.alert('Information','Not connected.\nPlease verify your network connection.');
    } else if (jqXHR.status == 404) {
    	Ext.Msg.alert('Information','The requested page not found. [404]');
    } else if (jqXHR.status == 500) {
    	Ext.Msg.alert('Information','Internal Server Error [500].');
    } else if (exception === 'parsererror') {
    	Ext.Msg.alert('Information','Requested JSON parse failed.');
    } else if (exception === 'timeout') {
    	Ext.Msg.alert('Information','Time out error.');
    } else if (exception === 'abort') {
    	Ext.Msg.alert('Information','Ajax request aborted.');
    } else {
    	Ext.Msg.alert('Information','Uncaught Error.\n' + jqXHR.responseText);
    }
};
Nci.form.Combobox.autoComplete=function(data){
	var $this=Nci,
		value='',
		listWidth=0,
		displayField='',
		valueField='',
		width= 150,
		emptyText='',
		fieldLabel='',
		y=0,
		x=0,
		showVar='listData';
	if(data.listVar != undefined)showVar=data.listVar;
	if(data.x != undefined)x=data.x;
	if(data.y != undefined)y=data.y;
	if(data.fieldLabel != undefined){
		fieldLabel=data.fieldLabel;
	}
	if(data.emptyText != undefined){
		emptyText=data.emptyText;
	}
	if(data.width != undefined){
		width=data.width;
	}
	if(data.value != undefined){
		value=data.value;
	}
	if(data.listWidth != undefined){
		listWidth=data.listWidth;
	}
	if(data.valueField == undefined){
		if(data.displayField!=undefined){
			valueField=data.displayField;
		}
	}else{
		valueField=data.valueField;
	}
	if(data.displayField== undefined){
		if(data.valueField!=undefined){
			displayField=data.valueField;
		}
	}else{
		displayField=data.displayField;
	}
	return new Ext.form.ComboBox({
		id				: $this.getId()+data.id,
		typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
		fieldLabel		: fieldLabel,
	    mode			: 'local',
	    emptyText		: '',
	    x				: x,
	    y  				: y,
		store			: data.store,
		width			: width,
		valueField		: valueField,
		listWidth		: listWidth,
		hideTrigger		: true,
		enableKeyEvents:true,
		emptyText		: emptyText,
		displayField	: displayField,
		value			: value,
		nci				: {
			select	: data.select
		},
		listeners		: {
			select	: function(a, b, c){
				a.setValue(b.data[a.valueField]);
				a.setRawValue(b.data[a.valueField]);
				if(a.nci.select != undefined)a.nci.select(a,b,c);
		    },
			keyDown: function(a,b,c){
				//alert();
				$this1=this;
				if(b.getKey()==13){
					if($this1.isExpanded()==false){
						clearTimeout(this.time);
						data.store.loadData([],false);
						//this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								if(data.param != undefined){
									param=data.param(a,b,c);
								}
								param['text']=$this1.lastQuery;
								if (a.rendered && a.innerList != null) {
									a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
									a.restrictHeight();
									a.selectedIndex = -1;
								}
								a.expand();
								Ext.Ajax.request({
									url			: data.url,
									params		: param,
									failure		: function(o){
										if (a.rendered && a.innerList != null) {
											a.innerList.update(a.loadingText ? '&nbsp; Kesalahan, Hubungi Admin' : '');
											a.restrictHeight();
											a.selectedIndex = -1;
										}
									},
									success		: function(o){
										var cst = Ext.decode(o.responseText);
										a.resp=cst;
										for(var i=0,iLen=cst[showVar].length; i<iLen; i++){
											var recs    = [],
											recType = data.store.recordType;
											var o=cst[showVar][i];
											recs.push(new recType(data.insert(o)));
											data.store.add(recs);
										}
										if(data.onShowList != undefined)data.onShowList(cst[showVar]);
										if(cst.listData.length>0){
											a.doQuery(a.allQuery, true);
											a.expand();
											a.selectText(value.length,value.length);
										}else{
											if (a.rendered && a.innerList != null) {
												a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
												a.restrictHeight();
												a.selectedIndex = -1;
											}
										}
									}
								});
							}
						//},1000);
						if(data.onEnter != undefined){
							data.onEnter(a,b,c);
						}
					}
				}
				if(data.keydown != undefined){
					data.keydown(a,b,c);
				}
			},
		    keyUp: function(a,b,c){
		    	$this1=this;
		    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
		    		clearTimeout(this.time);
		    		data.store.loadData([],false);
		    		this.time=setTimeout(function(){
	    				if($this1.lastQuery != '' ){
		    				var value=$this1.lastQuery;
		    				var param={};
		    				if(data.param != undefined){
		    					param=data.param(a,b,c);
		    				}
        		    		param['text']=$this1.lastQuery;
        		    		if (a.rendered && a.innerList != null) {
                	            a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
                	            a.restrictHeight();
                	            a.selectedIndex = -1;
                	        }
        		    		a.expand();
        		    		Ext.Ajax.request({
        		    			url			: data.url,
        		    			params		: param,
        		    			failure		: function(o){
        		    				if (a.rendered && a.innerList != null) {
    	    	        	            a.innerList.update(a.loadingText ? '&nbsp; Kesalahan, Hubungi Admin' : '');
    	    	        	            a.restrictHeight();
    	    	        	            a.selectedIndex = -1;
    	    	        	        }
        		    			},
        		    			success		: function(o){
        		    				var cst = Ext.decode(o.responseText);
									a.resp=cst;
        	    					for(var i=0,iLen=cst[showVar].length; i<iLen; i++){
        	    						var recs    = [],
        	    						recType = data.store.recordType;
        	    						var o=cst[showVar][i];
        	    						recs.push(new recType(data.insert(o)));
        	    						data.store.add(recs);
        	    					}
        	    					if(data.onShowList != undefined)data.onShowList(cst[showVar]);
        	    					if(cst.listData.length>0){
        	    						a.doQuery(a.allQuery, true);
            	    					a.expand();
            	    					a.selectText(value.length,value.length);
        	    					}else{
        	    						if (a.rendered && a.innerList != null) {
        	    	        	            a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
        	    	        	            a.restrictHeight();
        	    	        	            a.selectedIndex = -1;
        	    	        	        }
        	    					}
        		    			}
        		    		});
	    				}
    		    	},1000);
		    	}
		    },
		    blur: function(a,b,c){
		    	clearTimeout(this.time);
		    	data.store.loadData([],false);
		    	a.setValue(a.value);
				a.setRawValue(a.value);
		    	if(data.blur != undefined)data.blur(a,b,c);
		    }
		}
	})
};
Nci.merge=function(first,second){
	if(second != undefined){
		for(var key in second){
			first[key]=second[key];
		}
	}
};
function toFormat(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
function toInteger(num){
	var a=num.toString().split(',');
	var ori='';
	for(var i=0; i<a.length ; i++){
		if(a[i]!= undefined)
		ori+=a[i];	
	}
	if(isNaN(parseFloat(ori))){
		return 0;
	}else{
		return parseFloat(ori);
	}
	
}
var Q=function(im){
	return {
		autocomplete:function(data){
			var $this=this,
				value='',
				listWidth=0,
				displayField='',
				valueField='',
				width= 150,
				hidden=false,
				emptyText='',
				y=0,
				x=0,
			    fieldLabel='',
				showVar='listData';
			if(data.store==undefined)data.store=$this.arraystore();
			if(data.listVar != undefined)showVar=data.listVar;
			if(data.x != undefined)x=data.x;
			if(data.y != undefined)y=data.y;
			if(data.emptyText != undefined){
				emptyText=data.emptyText;
			}
			if(data.width != undefined){
				width=data.width;
			}if(data.hidden != undefined){
				hidden=data.hidden;
			}
			if(data.fieldLabel != undefined){
				fieldLabel=data.fieldLabel;
			}
			if(data.value != undefined){
				value=data.value;
			}
			if(data.listWidth != undefined){
				listWidth=data.listWidth;
			}
			if(data.valueField == undefined){
				if(data.displayField!=undefined){
					valueField=data.displayField;
				}
			}else{
				valueField=data.valueField;
			}
			if(data.displayField== undefined){
				if(data.valueField!=undefined){
					displayField=data.valueField;
				}
			}else{
				displayField=data.displayField;
			}
			return new Ext.form.ComboBox({
				typeAhead		: true,
			    triggerAction	: 'all',
			    lazyRender		: true,
				fieldLabel		: fieldLabel,
			    mode			: 'local',
			    emptyText		: '',
			    x				: x,
			    y  				: y,
				store			: data.store,
				width			: width,
				valueField		: data.keyField,
				listWidth		: listWidth,
				hideTrigger		: true,
				emptyText		: emptyText,
				displayField	: displayField,
				value			: value,
				hidden			: hidden,
				nci				: {
					select	: data.select
				},
				listeners		: {
					select	: function(a, b, c){
						a.setValue(b.data[data.keyField]);
						a.key=b.data[data.keyField];
						a.val=b.data[data.valueField];
						a.setRawValue(b.data[data.valueField]);
						if(a.nci.select != undefined)a.nci.select(a,b,c);
				    },
				    keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    		data.store.loadData([],false);
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
				    				if(data.param != undefined){
				    					param=data.param(a,b,c);
				    				}
		        		    		param['text']=$this1.lastQuery;
		        		    		if (a.rendered && a.innerList != null) {
		                	            a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
		                	            a.restrictHeight();
		                	            a.selectedIndex = -1;
		                	        }
		        		    		a.expand();
		        		    		Ext.Ajax.request({
		        		    			url			: data.url,
		        		    			params		: param,
		        		    			failure		: function(o){
		        		    				if (a.rendered && a.innerList != null) {
		    	    	        	            a.innerList.update(a.loadingText ? '&nbsp; Kesalahan, Hubungi Admin' : '');
		    	    	        	            a.restrictHeight();
		    	    	        	            a.selectedIndex = -1;
		    	    	        	        }
		        		    			},
		        		    			success		: function(o){
		        		    				var cst = Ext.decode(o.responseText);
		        		    				if(data.success != undefined){
		        		    					cst=data.success(cst);
		        		    				}
		        		    				
		        	    					for(var i=0,iLen=cst.length; i<iLen; i++){
		        	    						var recs    = [],
		        	    						recType = data.store.recordType;
		        	    						var o=cst[i];
		        	    						recs.push(new recType(data.insert(o)));
		        	    						data.store.add(recs);
		        	    					}
		        	    					if(data.onShowList != undefined)data.onShowList(cst);
		        	    					if(cst.length>0){
		        	    						a.doQuery(a.allQuery, true);
		            	    					a.expand();
		            	    					a.selectText(value.length,value.length);
		        	    					}else{
		        	    						if (a.rendered && a.innerList != null) {
		        	    	        	            a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
		        	    	        	            a.restrictHeight();
		        	    	        	            a.selectedIndex = -1;
		        	    	        	        }
		        	    					}
		        		    			}
		        		    		});
			    				}
		    		    	},1000);
				    	}
				    },
				    blur: function(a,b,c){
				    	clearTimeout(this.time);
				    	data.store.loadData([],false);
				    	a.setValue(a.key);
						a.setRawValue(a.val);
				    	if(data.blur != undefined)data.blur(a,b,c);
				    }
				}
			})
		},
		panel:function(panel){
			var $this=this,
				vars={
					autoWidth:true,
					autoHeight:true,
					bodyStyle:'padding: 10px',
					border:false
				};
				Ext.SplitButton
			Nci.merge(vars,panel);
			return new Ext.Panel(vars);
		},
		window:function(window){
			var $this=this,
				vars={
					constrain:true,
					resizable: false,
					modal:true,
					layout:'fit'
				};
			Nci.merge(vars,window);
			return new Ext.Window(vars);
		},
		display:function(display){
			var $this=this;
			var vars={
				xtype:'displayfield',
				style:'margin-top: 4px;margin-left: 3px;margin-right:3px;'
			};
			Nci.merge(vars,display);
			return vars;
		},
		datefield:function(datefield){
			var $this=this;
			var vars={
				format:'d/M/Y',
				q:{
					type:'datefield',
					vars:vars
				},
				value:new Date()
			};
			Nci.merge(vars,datefield);
			return new Ext.form.DateField(vars)
		},
		checkbox:function(checkbox){
			var $this=this;
			var vars={
				q:{
					type:'checkbox',
					vars:vars
				},
				style: {'margin-top':'5px'}
			};
			Nci.merge(vars,checkbox);
			return new Ext.form.Checkbox(vars);
		},
		fieldset:function(fieldset){
			var $this=this;
			var vars={
				style:{'padding-bottom':'4px','margin':'4px 0','text-align':'left'}
			};
			Nci.merge(vars,fieldset);
			return new Ext.form.FieldSet(vars);
		},
		input:function(input){
			var $this=this;
			var vars={
				xWidth:100,
				yWidth:'auto',
				width:'auto',
				label:'',
				autoWidth:true,
				labelAlign:'left',
				bodyStyle:'margin: 3px 0;background:transparent;',
				style:'',
				separator:':',
				separatorWidth:10,
				items: []
			};
			Nci.merge(vars,input);
			return new Ext.Panel({
				layout:'column',
				width:vars.width,
				style:vars.style,
				bodyStyle:vars.bodyStyle,
				border:false,
				items:[
					{
						xtype:'displayfield',
						width: vars.xWidth,
						style:{'margin-top':'3px','text-align':vars.labelAlign},
						value:vars.label
					},{
						xtype:'displayfield',
						width: vars.separatorWidth,
						style:{'margin-top':'3px'},
						value:vars.separator
					},{
						layout:'column',
						width: vars.yWidth,
						border:false,
						items:vars.items
					}
				]
			})
		},
		arraystore:function(arraystore){
			var $this=this;
			var vars={
				fields:[],
				q:{
	            	type:'arraystore',
	            	vars:vars
	            },
				data: []
			};
			Nci.merge(vars,arraystore);
			return new Ext.data.ArrayStore(vars);
		},
		dropdown:function(dropdown){
			var $this=this;
			var vars={
				store:new Ext.data.ArrayStore({fields:['id','text']}),
				width: 120,
				value:'',
				id:'',
				emptyText:'',
				displayField  : 'text',
	            valueField    : 'id',
				x:0,
				y:0
			};
			if(dropdown != undefined){
				if(dropdown.width != undefined)vars.width=dropdown.width;
				if(dropdown.emptyText != undefined)vars.emptyText=dropdown.emptyText;
				if(dropdown.value != undefined)vars.value=dropdown.value;
				if(dropdown.displayField != undefined)vars.displayField=dropdown.displayField;
				if(dropdown.valueField != undefined)vars.valueField=dropdown.valueField;
				if(dropdown.id != undefined)vars.id=dropdown.id;
				if(dropdown.x != undefined)vars.x=dropdown.x;
				if(dropdown.y != undefined)vars.y=dropdown.y;
				if(dropdown.data != undefined){
					vars.store.loadData([],false);
					for(var i=0; i<dropdown.data.length; i++){
						vars.store.add(new vars.store.recordType(dropdown.data[i]))
					}
				}
			}
			return new Ext.form.ComboBox({
	            width: vars.width,
	            store: vars.store,
	            q:{
	            	type:'dropdown',
	            	vars:vars
	            },
	            mode : 'local',
	            value: vars.value,
	            id: vars.id,
	            x: vars.x,
	            y: vars.y,
	            emptyText	: vars.emptyText,
	            triggerAction : 'all',
	            displayField  : vars.displayField,
	            valueField    : vars.valueField,
	            editable      : true,
	            forceSelection: true,
	            listeners:{
	            	select:function(a){
	            		if(dropdown.select != undefined)dropdown.select(a);
	            	}
	            }
	        })
		},
		table:function(table){
			var $this=this;
			var vars={
				store:new Ext.data.ArrayStore({fields:[]}),
				fields:[],
				title:'',
				autoScroll: true,
				columnLines: true,
				border:true,
				val:1,
				flex: 1,
				anchor: '100% 100%',
				count:0,
				column:	new Ext.grid.ColumnModel([]),
				tbar:[],
				config:{
					forceFit: true
				},
				pageSize: 50
			};
			if(table != undefined){
				if(table.title != undefined)vars.title=table.title;
				if(table.store != undefined)vars.store=table.store;
				if(table.autoScroll != undefined)vars.autoScroll=table.autoScroll;
				if(table.columnLines != undefined)vars.columnLines=table.columnLines;
				if(table.border != undefined)vars.border=table.border;
				if(table.anchor != undefined)vars.anchor=table.anchor;
				if(table.flex != undefined)vars.flex=table.flex;
				if(table.column != undefined){
					vars.column=table.column;
					for(var i=0; i<table.column.length ; i++){
						if(table.column[i].dataIndex != undefined){
							vars.fields.push(table.column[i].dataIndex);
						}
					}
					vars.store=new Ext.data.ArrayStore({fields:vars.fields});
				}
				if(table.tbar != undefined)vars.tbar=table.tbar;
				if(table.config != undefined)vars.config=table.config;
				if(table.pageSize != undefined)vars.pageSize=table.pageSize;
				if(table.ajax != undefined)vars.ajax=table.ajax;
			}
			vars.bbar=new Ext.PagingToolbar({
		            store: vars.store,
		            displayInfo: true,
		            pageSize: vars.pageSize,
		            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
		            emptyMsg: "no data record&nbsp;&nbsp;",
		            items : [
                        '-',
                        'Per Halaman: ',
                        Q().dropdown({
                        	data:[{id:'1'},{id:'10'},{id:'25'},{id:'50'}],
                        	width: 40,
                        	displayField  : 'id',
				            valueField    : 'id',
				            value: vars.pageSize,
				            select:function(a){
				            	vars.pageSize=a.getValue();
			            		vars.bbar.inputItem.setValue(1);
			            		vars.bbar.doLoad();
				            }
                        })
                    ],
		            doRefresh : function() {
					    this.doLoad(this.cursor);
					},
					moveFirst:function(){
						vars.val=1;
						this.inputItem.setValue(vars.val);
						Q(vars.grid).refresh();
					},
					movePrevious:function(){
						vars.val=parseInt(vars.val)-1;
						this.inputItem.setValue(vars.val);
						Q(vars.grid).refresh();
					},
					moveNext:function(){
						vars.val=parseInt(vars.val)+1;
						this.inputItem.setValue(vars.val);
						Q(vars.grid).refresh();
					},
					moveLast:function(){
						vars.val=Math.ceil(vars.count/vars.pageSize);
						this.inputItem.setValue(vars.val);
						Q(vars.grid).refresh();
					},
					doLoad:function(a){
						var $this=this;
						vars.val=this.inputItem.getValue();
						this.inputItem.on('blur',function(){
							if(parseInt(vars.val)>parseInt(vars.of)){
								this.setValue(vars.of);
							}else{
								this.setValue(vars.val);
							}
						});
						if(parseInt(vars.val)>parseInt(vars.of)){
							vars.val=vars.of;
						}
						this.inputItem.setValue(vars.val);
						Q(vars.grid).refresh();
					}
		        });
			return vars.grid= new Ext.grid.EditorGridPanel({
				title: vars.title,
				q:{
					type:'table',
					vars:vars
				},
				store: vars.store,
				flex:vars.flex,
				autoScroll: vars.autoScroll,
				columnLines: vars.columnLines,
				border:vars.border,
				anchor: vars.anchor,
				selModel: new Ext.grid.RowSelectionModel({
						singleSelect: true,
						listeners:{
							rowselect: function(sm, row, rec){
								if(table.rowselect != undefined)table.rowselect(sm, row, rec,vars.store);
							}
						}
					}
				),
				listeners:{
					rowdblclick: function (sm, ridx, cidx){
						if(table.rowdblclick != undefined)table.rowdblclick(sm, ridx, cidx,vars.store);
					}
				},
				colModel: vars.column,
				tbar: vars.tbar,
				bbar : vars.bbar,
				viewConfig:vars.config
			});
		},
		val:function(data){
			if(im != undefined && im.q != undefined){
				var vars=im.q.vars;
				if(im.q.type=='checkbox' && data==undefined){
					return im.getValue();
				}else if(im.q.type=='datefield' && data==undefined){
					return timestimetodate(im.getValue());
				}else if(im.q.type=='dropdown' && data !=undefined){
					im.setValue(data);
				}
			}
		},
		add:function(dat){
			if(im != undefined && im.q != undefined){
				var vars=im.q.vars;
				if(im.q.type=='dropdown'){
					if(typeof dat=='object'){
						if(dat.length == undefined){
							vars.store.add(new vars.store.recordType(dat));
						}else{
							for(var i=0; i<dat.length; i++){
								vars.store.add(new vars.store.recordType(dat[i]));
							}
						}
						
					}
				}else if(im.q.type=='arraystore'){
					if(typeof dat=='object'){
						if(dat.length == undefined){
							im.add(new im.recordType(dat));
						}else{
							for(var i=0; i<dat.length; i++){
								im.add(new im.recordType(dat[i]));
							}
						}
					}
				}
			}
		},
		size:function(){
			if(im != undefined && im.q != undefined){
				var vars=im.q.vars;
				if(im.q.type=='dropdown'){
					return vars.store.getCount();
				}else if(im.q.type=='arraystore'){
					return im.getCount();
				}
			}
		},
		reset:function(){
			if(im != undefined && im.q != undefined){
				var vars=im.q.vars;
				if(im.q.type=='dropdown'){
					vars.store.loadData([],false);
					im.setValue('');
				}else if(im.q.type=='arraystore'){
					im.loadData([],false);
				}
			}
		},
		refresh:function(){
			if(im != undefined && im.q != undefined){
				var vars=im.q.vars;
				if(im.q.type=='table'){
					vars.bbar.show();
					var val=vars.val;
					if(vars.ajax != undefined)vars.ajax({start:(parseInt(vars.val)-1),size:vars.pageSize},function(list,total){
						vars.store.loadData([],false);
						for(var i=0; i<list.length; i++){
							vars.store.add(new vars.store.recordType(list[i]));
						}
						vars.count=total;
						vars.countData=list.length;
						vars.grid.getView().refresh();
						var page=Math.ceil(vars.count/vars.pageSize);
						if(page==0)page=1;
						vars.of=page;
						vars.bbar.afterTextItem.update('of '+page);
						vars.bbar.inputItem.setValue(vars.val);
						if(parseInt(vars.val)>1)vars.bbar.first.enable();
						if(parseInt(vars.val)>1)vars.bbar.prev.enable();
						if(parseInt(vars.val)<page)vars.bbar.next.enable();
						if(parseInt(vars.val)<page)vars.bbar.last.enable();
						var display="";
						if(total==0){
							display='no data record';
						}else if(vars.pageSize==1 && total==1){
							display='displaying 1 record';
						}else if(vars.pageSize==1 && total>1){
							display='displaying '+(((parseInt(vars.val)-1)*vars.pageSize)+1)+' of '+vars.count+' records';
						}else if(vars.pageSize>1 && total >1){
							display='displaying '+(((parseInt(vars.val)-1)*vars.pageSize)+1)+' - '+(((parseInt(vars.val)-1)*vars.pageSize)+vars.countData)+' of '+vars.count+' records';
						}
						vars.bbar.items.items[15].update(display);
					});
				}
			}
		}
	}
};
var loadMask = {
	show:function(){
		new Ext.LoadMask(Ext.getBody(), {msg:'Progress Sedang Berjalan.....'}).show();
	},
	hide :function(){
		new Ext.LoadMask(Ext.getBody(), {msg:'Progress Sedang Berjalan.....'}).hide();
	}
};

Ext.Ajax.on("beforerequest", function(){
	
	//loadMask.show();
   /* if ($progress.length === 0) {
  		$progress = $('<div><dt/><dd/></div>').attr('id', 'progress');
  		$("body").append($progress);
	}
	$progress.width((50 + Math.random() * 30) + "%");*/
});
Ext.Ajax.on("requestcomplete", function(){
	//loadMask.hide();
    /*$progress.width("100%").delay(200).fadeOut(400, function() {
  		$progress.width("0%").delay(200).show();
    });*/
});



function aptpembulatan(total){
	var hasil;
	var angkasplit;
	var angkasubstr;
	var pembulatan=100;
	var hasilfinal=0;
	
	
	if(total == 0 || total == '' || total == undefined){
		return hasilfinal;
	} else{
		angkasplit = total.toString().split(".");//buang angka dibelakang koma 
		angkasubstr= angkasplit[0].toString().substr(-2); //get 2 angka dari belakang setelah di buang koma
		
		if(angkasubstr == '00'){
			hasil=0;
		} else{
			hasil = pembulatan-parseInt(angkasubstr);
		}
		
		hasilfinal = hasil + parseInt(angkasplit[0]);
		return toFormat(hasilfinal);
	}
}

function aptpembulatankebawah(total){
    var hasil;
    var angka=0;
    var angka1=0;
    var angka2=0;
    var angka3=0;
    var angka4=0;
    var angka5=0;
    var angkasubstr;
    var pembulatan='00';
    var hasilfinal=0;
    
    
    angka = total;
	angka1 = parseFloat(total) * 0.01; // 165,44
	angka2 = Math.floor(angka1);// 165
	angka3 = parseFloat(angka1) - parseFloat(angka2);//0.44
	angka4 = parseFloat(angka3) * 100;//44
	angka5 = parseInt(total) - parseInt(angka4);//16500
	
    return angka5;
}

function formatNumberDecimal(num){
	return parseFloat(Math.round(toInteger(num) * 100) / 100).toFixed(2);
}

function formatNumberDecimal5(num){
	return parseFloat(Math.round(toInteger(num) * 100) / 100).toFixed(5);
}

function formatNumberDecimalParam(num,dec){
    return parseFloat(Math.round(toInteger(num) * 100) / 100).toFixed(dec);
}


Nci.form.Combobox.autoCompleteId=function(data){
	var $this=Nci,
		value='',
		listWidth=0,
		displayField='',
		valueField='',
		width= 150,
		emptyText='',
		y=0,
		x=0,
		showVar='listData';
	if(data.listVar != undefined)showVar=data.listVar;
	if(data.x != undefined)x=data.x;
	if(data.y != undefined)y=data.y;
	if(data.emptyText != undefined){
		emptyText=data.emptyText;
	}
	if(data.width != undefined){
		width=data.width;
	}
	if(data.value != undefined){
		value=data.value;
	}
	if(data.listWidth != undefined){
		listWidth=data.listWidth;
	}
	if(data.valueField == undefined){
		if(data.displayField!=undefined){
			valueField=data.displayField;
		}
	}else{
		valueField=data.valueField;
	}
	if(data.displayField== undefined){
		if(data.valueField!=undefined){
			displayField=data.valueField;
		}
	}else{
		displayField=data.displayField;
	}
	return new Ext.form.ComboBox({
		id				: data.id,
		typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
	    x				: x,
	    y  				: y,
		store			: data.store,
		width			: width,
		valueField		: valueField,
		listWidth		: listWidth,
		hideTrigger		: true,
		emptyText		: emptyText,
		displayField	: displayField,
		value			: value,
		nci				: {
			select	: data.select
		},
		listeners		: {
			select	: function(a, b, c){
				a.setValue(b.data[a.valueField]);
				a.setRawValue(b.data[a.valueField]);
				if(a.nci.select != undefined)a.nci.select(a,b,c);
		    },
		    keyUp: function(a,b,c){
		    	$this1=this;
		    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
		    		clearTimeout(this.time);
		    		data.store.loadData([],false);
		    		this.time=setTimeout(function(){
	    				if($this1.lastQuery != '' ){
		    				var value=$this1.lastQuery;
		    				var param={};
		    				if(data.param != undefined){
		    					param=data.param(a,b,c);
		    				}
        		    		param['text']=$this1.lastQuery;
        		    		if (a.rendered && a.innerList != null) {
                	            a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
                	            a.restrictHeight();
                	            a.selectedIndex = -1;
                	        }
        		    		a.expand();
        		    		Ext.Ajax.request({
        		    			url			: data.url,
        		    			params		: param,
        		    			failure		: function(o){
        		    				if (a.rendered && a.innerList != null) {
    	    	        	            a.innerList.update(a.loadingText ? '&nbsp; Kesalahan, Hubungi Admin' : '');
    	    	        	            a.restrictHeight();
    	    	        	            a.selectedIndex = -1;
    	    	        	        }
        		    			},
        		    			success		: function(o){
        		    				var cst = Ext.decode(o.responseText);
									a.resp=cst;
        	    					for(var i=0,iLen=cst[showVar].length; i<iLen; i++){
        	    						var recs    = [],
        	    						recType = data.store.recordType;
        	    						var o=cst[showVar][i];
        	    						recs.push(new recType(data.insert(o)));
        	    						data.store.add(recs);
        	    					}
        	    					if(data.onShowList != undefined)data.onShowList(cst[showVar]);
        	    					if(cst.listData.length>0){
        	    						a.doQuery(a.allQuery, true);
            	    					a.expand();
            	    					a.selectText(value.length,value.length);
        	    					}else{
        	    						if (a.rendered && a.innerList != null) {
        	    	        	            a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
        	    	        	            a.restrictHeight();
        	    	        	            a.selectedIndex = -1;
        	    	        	        }
        	    					}
        		    			}
        		    		});
	    				}
    		    	},1000);
		    	}
		    },
		    blur: function(a,b,c){
		    	clearTimeout(this.time);
		    	data.store.loadData([],false);
		    	a.setValue(a.value);
				a.setRawValue(a.value);
		    	if(data.blur != undefined)data.blur(a,b,c);
		    }
		}
	})
};


function ShowDateAkuntansi(cTglAsli) 
{
	//var cTgl;
    cTgl = cTglAsli ;
		 if (cTgl.length !== 8) 
			{
				if (cTgl === "") 
				{
					cTgl = new Date();
				};
			   var dsl = new Date(cTgl);
				cTgl = dsl.format('Ymd');
		   };
    var cHari = cTgl.substr(6, 2);
    var cBulan = cTgl.substr(4, 2);
    var cThn = cTgl.substr(0, 4);
    var cBln = '';
    switch (cBulan) {
        case '01': cBln = 'Jan';
            break
        case '02': cBln = 'Feb';
            break
        case '03': cBln = 'Mar';
            break
        case '04': cBln = 'Apr';
            break
        case '05': cBln = 'May';
            break
        case '06': cBln = 'Jun';
            break
        case '07': cBln = 'Jul';
            break
        case '08': cBln = 'Aug';
            break
        case '09': cBln = 'Sep';
            break
        case '10': cBln = 'Oct';
            break
        case '11': cBln = 'Nov';
            break
        case '12': cBln = 'Dec';
    }
    return cHari + '/' + cBln + '/' + cThn;
}
function FormatDateReport(cTglAsli) 
{
	//var cTgl;
    cTgl = cTglAsli 
		 if (cTgl.length !== 8) 
			{
				if (cTgl === "") 
				{
					cTgl = new Date()
				};
			   var dsl = new Date(cTgl);
				cTgl = dsl.format('Ymd')
		   };
    var cHari = cTgl.substr(6, 2);
    var cBulan = cTgl.substr(4, 2);
    var cThn = cTgl.substr(0, 4);
    
    return cThn + '-' + cBulan + '-' + cHari
}

function formatCurrencyDec(num) 
{
	// alert(num +'=' + Math.floor(num * 100 + 0.50000000001));
    if (num != undefined) 
	{
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            // cents = cents;
        	cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' +
			num.substring(num.length - (4 * i + 3));
        //return (((sign) ? '' : '-') + 'Rp.' + num);
        return (((sign) ? '' : '-') + num +','+cents);
    }
};

function getNumber(dblNilai)
{

    var dblAmount;

    dblAmount = dblNilai ;//.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) 
	{
        var x = dblAmount.substr(i, 1);
        if (x === '.') 
		{
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount);
};