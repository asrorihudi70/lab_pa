///////////////////////////////////////////////////////////
// clok
///////////////////////////////////////////////////////////

	var myfont_face = "Arial";
	var myfont_size = "9";
	var myfont_color = "#ffffcc";
	var myback_color = "#000066";
	var mywidth = 80;
	var my12_hour = 1;

	var dn = ""; var old = "";



function show_clock() {
		var Digital = new Date();
		var hours = Digital.getHours();
		var minutes = Digital.getMinutes();
		var seconds = Digital.getSeconds();

		if (my12_hour) {
			dn = "AM";
			if (hours > 12) { dn = "PM"; hours = hours - 12; }
			if (hours == 0) { hours = 12; }
		} else {
			dn = "";
		}
		if (minutes <= 9) { minutes = "0"+minutes; }
		if (seconds <= 9) { seconds = "0"+seconds; }

		myclock = '';
		myclock += hours+':'+minutes+':'+seconds+' '+dn;
		if (old == "true") {
			Ext.get("Clock-Label").dom.setValue();
			old = "die"; return;
		}
		setTimeout("show_clock()",1000);
}
