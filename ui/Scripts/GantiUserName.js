/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />


var dsCompanyListGantiPassword;
var dsLanguageGantiPassword;
var selectLanguageGantiPassword;
var mTabGantiPassword=false;
var mVarKdUserGantiPassword;


function FormGantiUserName() 
{
    vWinFormEntryGantiPassword = new Ext.Window
    (
        {
            id: 'vWinFormEntryGantiPassword',
            title: 'Ganti Username',
            closeAction: 'hide',
            closable:false,
            width: 500,
            height: 170,//420,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Utility',
            modal: true,
            items:
            [
                GetPanelGantiPassword()
            ],
            listeners:
            {
                activate: function()
                { }
            }
        }
    );
	
    vWinFormEntryGantiPassword.show();
};

function GetPanelGantiPassword() 
{
    var vTabPanelGantiPassword = new Ext.Panel
    (
        {
            id: 'vTabPanelGantiPassword',
            region: 'center',
            margins: '5 5 5 5',
            bodyStyle: 'padding:10px 10px 10px 10px',
            activeTab: 0,
            plain: true,
            defferedRender: false,
            frame: false,
            border: true,
            height: 90,
                //width:742,
            anchor: '100%',
            items:
            [
                    {
                         columnWidth:1,
                                layout: 'form',
                                    labelWidth:165,
                                    labelAlign:'right',
                                border: false,
                                items:
                                    [
                                        {
                                                xtype: 'textfield',
												inputType:'password',
                                                fieldLabel: 'Password Lama  ',
                                                name: 'txtPass_Lama',
                                                id: 'txtPass_Lama',
                                                anchor: '100%',
                                        },
                                        {
                                                xtype: 'textfield',
												inputType:'password',
                                                fieldLabel: 'Password Baru  ',
                                                name: 'txtPass_Baru',
                                                id: 'txtPass_Baru',
                                                anchor: '100%'
                                        },
                                        {
                                                xtype: 'textfield',
												inputType:'password',
                                                fieldLabel: 'Konfirmasi Password Baru  ',
                                                name: 'txtKonfirPass_Baru',
                                                id: 'txtKonfirPass_Baru',
                                                anchor: '100%'
                                        },
                                        ],
                         listeners:
                        {
                                activate: function()
                                {
                                        mTabGantiPassword=true;
                                        initCompanyGantiPassword();
                                }
                        }
                        }
                    ]
            }
	)
	
	var FormGantiPassword = new Ext.Panel  
	(
		{
			id: 'FormGantiPassword',
			region: 'center',
			layout: 'form',
			title: '',
			anchor:'100%',
			bodyStyle: 'padding:7px 7px 7px 7px',
			border: true,
			height: 434,//392,
			shadhow: true,
			items: 
			[
				vTabPanelGantiPassword,
				{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:0px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Ok',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiPassword',
						handler:function()
						{
							simpanPass();
							
						}
					},
					{
						xtype:'button',
						text:'Cancel' ,
						width:70,
						hideLabel:true,
						id: 'btnCancelGantiPassword',
						handler:function() 
						{
							vWinFormEntryGantiPassword.close();
						}
					},
					{
						xtype:'button',
						text:nmBtnApply ,
						width:70,
						hideLabel:true,
						id: 'btnApplyGantiPassword',
						hidden:true,
						handler:function() 
						{
							if(mTabGantiPassword === true)
							{
								CompanyGantiPasswordSave(false);
							};
							BahasaGantiPasswordSave(false);
						}
					}
				]
			}
			]
        }
	);
	
	return FormGantiPassword;
};
function simpanPass()
{
	if (ValidasiEntryGantiPass('Medismart', false) == 1)
    { 
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/main/gantiPass",
                            params: {
								passlama:Ext.get('txtPass_Lama').getValue(),
								passbaru:Ext.get('txtPass_Baru').getValue(),
								konfpass:Ext.get('txtKonfirPass_Baru').getValue()
							},
                            failure: function (o)
                            {
                                ShowPesanWarningGantiPassword('Data tidak bisa diSimpan segera Hubungi Admin', 'Gagal');
                            },
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoGantiPassword('Password sudah diganti', 'Medismart');
                                    vWinFormEntryGantiPassword.close();
                                    
                                } else if (cst.success === 'teuaya')
                                {
                                    ShowPesanWarningGantiPassword('Password lama tidak valid', 'Medismart');
                                }
								else
								{
									ShowPesanWarningGantiPassword('Data tidak bisa disimpan / Password tidak valid', 'Gagal');
								}
                                ;
                            }
                        }
                )

    } /* else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ; */
}
;

function ValidasiEntryGantiPass(modul, mBolHapus)
{
    var x = 1;

    if (Ext.get('txtPass_Lama').getValue() == '')
        {
            ShowPesanWarningGantiPassword(('Password lama tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtPass_Baru').getValue() == '')
        {
            ShowPesanWarningGantiPassword(('Password baru tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtKonfirPass_Baru').getValue() == '')
        {
            ShowPesanWarningGantiPassword('Konfirmasi password barunya !', modul);
            x = 0;
        }
	if (Ext.get('txtPass_Baru').getValue() !== Ext.get('txtKonfirPass_Baru').getValue())
		{
            ShowPesanWarningGantiPassword('Konfirmasi password tidak sama dengan password baru !', modul);
            x = 0;
        }
        ;
    return x;
}
;
function initCompanyGantiPassword()
{
	if (dsCompanyListGantiPassword != undefined)
	{
		if(dsCompanyListGantiPassword.getCount() > 0 )
		{
			Ext.get('txtKode_SetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_ID;
			Ext.get('txtNameSetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_NAME;
			Ext.get('txtAddressSetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_ADDRESS;
			Ext.get('txtTlpSetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_TELP;
			Ext.get('txtFaxSetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_FAX;
			Ext.get('txtCitySetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_CITY;
			Ext.get('txtPosSetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_POS_CODE;
			Ext.get('txtEmailSetCompany').dom.value=dsCompanyListGantiPassword.data.items[0].data.COMP_EMAIL;
		}
		else
		{
			ClearTextCompanyGantiPassword();
		};
	}
	else
	{
		ClearTextCompanyGantiPassword();
	};
};

function ClearTextCompanyGantiPassword()
{
	Ext.get('txtKode_SetCompany').dom.value='';
	Ext.get('txtNameSetCompany').dom.value='';
	Ext.get('txtAddressSetCompany').dom.value='';
	Ext.get('txtTlpSetCompany').dom.value='';
	Ext.get('txtFaxSetCompany').dom.value='';
	Ext.get('txtCitySetCompany').dom.value='';
	Ext.get('txtPosSetCompany').dom.value='';
	Ext.get('txtEmailSetCompany').dom.value='';
};


function getItemPanelGantiPassword() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:33,
		anchor:'100%',
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:0px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiPassword',
						handler:function()
						{
							if(mTabGantiPassword === true)
							{
								CompanyGantiPasswordSave(true);
							};
							
							var x = BahasaGantiPasswordSave(true);
							if (x===undefined)
							{
								vWinFormEntryGantiPassword.close();
							};
							
						}
					},
					{
						xtype:'button',
						text:nmBtnCancel ,
						width:70,
						hideLabel:true,
						id: 'btnCancelGantiPassword',
						handler:function() 
						{
							vWinFormEntryGantiPassword.close();
						}
					},
					{
						xtype:'button',
						text:nmBtnApply ,
						width:70,
						hideLabel:true,
						id: 'btnApplyGantiPassword',
						handler:function() 
						{
							if(mTabGantiPassword === true)
							{
								CompanyGantiPasswordSave(false);
							};
							BahasaGantiPasswordSave(false);
						}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningGantiPassword(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};



function mComboBahasaGantiPassword() 
{
	var Field = ['LANGUAGE_ID','LANGUAGE'];

    dsLanguageGantiPassword = new WebApp.DataStore({ fields: Field });
	
    dsLanguageGantiPassword.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'LANGUAGE',
			    Sortdir: 'ASC',
			    target: 'ViewComboBahasaUtility',
			    param: ''
			}
		}
	);
	
    var cboBahasaGantiPassword = new Ext.form.ComboBox
	(
		{
		    id: 'cboBahasaGantiPassword',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmBhsUtility + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsLanguageGantiPassword,
		    valueField: 'LANGUAGE_ID',
		    displayField: 'LANGUAGE',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectLanguageGantiPassword = b.data.LANGUAGE_ID;
			    }
			}
		}
	);
	
    return cboBahasaGantiPassword;
};

function RefreshDataGantiPassword()
{	
	var Field = ['COMP_ID','COMP_NAME','COMP_ADDRESS','COMP_TELP','COMP_FAX','COMP_CITY','COMP_POS_CODE','COMP_EMAIL','LOGO'];

    dsCompanyListGantiPassword = new WebApp.DataStore({ fields: Field });
	dsCompanyListGantiPassword.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					//Sort: 'Company_ID',
                                        Sort: 'Comp_ID',
					Sortdir: 'ASC', 
					target:'ViewSetupCompany',
					param : ''
				}			
			}
		);       
		
	return dsCompanyListGantiPassword;
};

function CompanyGantiPasswordSave(mBol) 
{
    if (Ext.get('txtKode_SetCompany').dom.value === '')
    {
        Ext.Ajax.request
        (
            {
                //url: "./Datapool.mvc/CreateDataObj",
                url: baseURL + "index.php/main/CreateDataObj",
                params: getParamCompanyGantiPassword(),
                success: function(o)
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        if(mBol === false)
                        {
                            Ext.get('txtKode_SetCompany').dom.value=cst.CompanyId;
                            ShowPesanInfoGantiPassword(nmPesanSimpanSukses,nmTitleFormUtility);
                        };
                    }
                    else if  (cst.success === false && cst.pesan===0)
                    {
                        if(mBol === false)
                        {
                            ShowPesanWarningGantiPassword(nmPesanSimpanGagal,nmTitleFormUtility);
                        };
                    }
                    else
                    {
                        if(mBol === false)
                        {
                            ShowPesanErrorGantiPassword(nmPesanSimpanError,nmTitleFormUtility);
                        };
                    };
                }
            }
        )
    }
    else
    {
        Ext.Ajax.request
         (
            {
                //url: "./Datapool.mvc/UpdateDataObj",
                url: baseURL + "index.php/main/UpdateDataObj",
                params: getParamCompanyGantiPassword(),
                success: function(o)
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        if(mBol === false)
                        {
                            ShowPesanInfoGantiPassword(nmPesanEditSukses,nmTitleFormUtility);
                        };
                    }
                    else if  (cst.success === false && cst.pesan===0)
                    {
                        if(mBol === false)
                        {
                            ShowPesanWarningGantiPassword(nmPesanEditGagal,nmTitleFormUtility);
                        };
                    }
                    else
                    {
                        if(mBol === false)
                        {
                            ShowPesanErrorGantiPassword(nmPesanEditError,nmTitleFormUtility);
                        };
                    };
                }
            }
        )
    };
};

function BahasaGantiPasswordSave(mBol) 
{
	if (ValidasiBahasaGantiPassword(nmTitleFormUtility) === 1 ) 
	{
		Ext.Ajax.request
		(
			{
				//url: "./Datapool.mvc/UpdateDataObj",
                                url: baseURL + "index.php/main/UpdateDataObj",
				params: getParamBahasaGantiPassword(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						mLanguage(strUser);
						if(mBol === false)
						{
							ShowPesanInfoGantiPassword(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility,nmTitleFormUtility);
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						if(mBol === false)
						{
							ShowPesanWarningGantiPassword(nmPesanSimpanGagal,nmTitleFormUtility);
						};
					}
					else 
					{
						if(mBol === false)
						{
							ShowPesanErrorGantiPassword(nmPesanSimpanError,nmTitleFormUtility);
						};
					};
				}
			}
		)
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};

function ValidasiBahasaGantiPassword(modul)
{
	var x=1;
	if(Ext.get('cboBahasaGantiPassword').dom.value === '' && ( selectLanguageGantiPassword === '' || selectLanguageGantiPassword === undefined))
	{
		x=0;
		ShowPesanWarningGantiPassword(nmGetValidasiKosong(nmBhsUtility),modul);
	};
	
	return x;
};

function getParamCompanyGantiPassword() 
{
    var params =
	{	
            Table: 'ViewSetupCompany',
	    CompId: Ext.get('txtKode_SetCompany').getValue(),
	    CompName: Ext.get('txtNameSetCompany').getValue(),
            CompAddress:Ext.get('txtAddressSetCompany').getValue(),
            CompTelp:Ext.get('txtTlpSetCompany').getValue(),
            CompFax:Ext.get('txtFaxSetCompany').getValue(),
            CompCity:Ext.get('txtCitySetCompany').getValue(),
            CompPos:Ext.get('txtPosSetCompany').getValue(),
            CompEmail:Ext.get('txtEmailSetCompany').getValue()
	};
    return params
};

function getParamBahasaGantiPassword() 
{
    var params =
	{	
		Table: 'ViewComboBahasaUtility',   
		Id:selectLanguageGantiPassword,
		KdUser:mVarKdUserGantiPassword
	};
	
    return params
};

function ShowPesanWarningGantiPassword(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :300
		}
	);
};

function ShowPesanErrorGantiPassword(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :300
		}
	);
};

function ShowPesanInfoGantiPassword(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :300
		}
	);
};

function GetBahasaGantiPassword()
{
     Ext.Ajax.request
     (
        {
            //url: "./Module.mvc/ExecProc",
            url: baseURL + "index.php/main/ExecProc",
            params:
            {
                UserID: 'Admin',
                ModuleID: 'ProsesGetBahasaGantiPassword',
                Params:	strUser
            },
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    selectLanguageGantiPassword=cst.Id;
                    Ext.get('cboBahasaGantiPassword').dom.value=cst.Bahasa;
                    mVarKdUserGantiPassword=cst.KdUser;
                }
                else
                {
                    selectLanguageGantiPassword = '';
                    Ext.get('cboBahasaGantiPassword').dom.value='';
                    mVarKdUserGantiPassword=''
                };
            }

        }
    );
};

///---------------------------------------------------------------------------------------///