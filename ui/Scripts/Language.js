Ext.ns("TRust");
var cLanguage;

function mLanguage(cLanguage) {

    cTarget = 'Aset';
    Ext.Ajax.request({
    //url: "./Language.mvc/getLanguage",
    url: baseURL + "index.php/main/getLanguage",
        params: {
        UserID: cLanguage,            
            Pwd: 'Aset'                      // parameter untuk url yang dituju (fungsi didalam controller)
        },
		failure: function(o)
		{
                    alert ('gagal');
		},        
        success: function(o) {
            var cst = Ext.decode(o.responseText);
			if(cst.ListLanguage != null){
				LoadLanguage(cst.ListLanguage);   
			}
        }
    });

};


function LoadLanguage(records) {   
    var i;   

    for (var i = 0; i < records.length; i++) 
	{
        var x = records[i];
        if (x.LIST_KEY==='nmSimpan')
		{
			nmSimpan=x.LABEL;
		}else
		if (x.LIST_KEY==='nmSimpanKeluar')
		{
			nmSimpanKeluar=x.LABEL;
		}else 
		if (x.LIST_KEY==='nmHapus')
		{
			nmHapus=x.LABEL;
		}else   
		if (x.LIST_KEY==='nmLookup')
		{
			nmLookup=x.LABEL;
		}else   
		if (x.LIST_KEY==='nmCetak')
		{
			nmCetak=x.LABEL;
		}else
		if (x.LIST_KEY === 'nmTambah') {
		    nmTambah = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmEditData') {
		    nmEditData = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmRefresh') {
		    nmRefresh = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmMaksData') {
		    nmMaksData = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmTambahBaris') {
		    nmTambahBaris = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmHapusBaris') {
		    nmHapusBaris = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdLokasi') {
		    nmKdLokasi = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdLokasi2') {
		    nmKdLokasi2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmLokasi') {
		    nmLokasi = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmFormLokasi') {
		    nmFormLokasi = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdDepartement') {
		    nmKdDepartement = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdDepartement2') {
		    nmKdDepartement2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmDepartement') {
		    nmDepartement = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmFormDepartement') {
		    nmFormDepartement = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdPart') {
		    nmKdPart = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdPart2') {
		    nmKdPart2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmPart') {
		    nmPart = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmFormPart') {
		    nmFormPart = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdVendor') {
		    nmKdVendor = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmKdVendor2') {
		    nmKdVendor2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendor') {
		    nmVendor = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendor2') {
		    nmVendor2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorContact1') {
		    nmVendorContact1 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorContact2') {
		    nmVendorContact2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorAddress') {
		    nmVendorAddress = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorCity') {
		    nmVendorCity = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorPosCode') {
		    nmVendorPosCode = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorCountry') {
		    nmVendorCountry = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorPhone1') {
		    nmVendorPhone1 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmVendorPhone2') {
		    nmVendorPhone2 = x.LABEL;
		}else
		if (x.LIST_KEY === 'nmFormVendor') {
		    nmFormVendor = x.LABEL;
		}
		else
		    if (x.LIST_KEY === 'nmHeaderSimpanData') {
		        nmHeaderSimpanData = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanSimpanSukses') {
		        nmPesanSimpanSukses = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanSimpanGagal') {
		        nmPesanSimpanGagal = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanSimpanError') {
		        nmPesanSimpanError = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmHeaderEditData') {
		        nmHeaderEditData = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanEditSukses') {
		        nmPesanEditSukses = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanEditGagal') {
		        nmPesanEditGagal = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanEditError') {
		        nmPesanEditError = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmHeaderHapusData') {
		        nmHeaderHapusData = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanHapusSukses') {
		        nmPesanHapusSukses = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanHapusGagal') {
		        nmPesanHapusGagal = x.LABEL;
		} else
		    if (x.LIST_KEY === 'nmPesanHapusError') {
		        nmPesanHapusError = x.LABEL;
		    } else
		        if (x.LIST_KEY === 'nmKdSatuan') {
		            nmKdSatuan = x.LABEL;
		    } else
		        if (x.LIST_KEY === 'nmKdSatuan2') {
		            nmKdSatuan2 = x.LABEL;
		    } else
		        if (x.LIST_KEY === 'nmSatuan') {
		            nmSatuan = x.LABEL;
		    } else
		        if (x.LIST_KEY === 'nmFormSatuan') {
		            nmFormSatuan = x.LABEL;
		        } else
		            if (x.LIST_KEY === 'nmKdService') {
		                nmKdService = x.LABEL;
		        } else
		            if (x.LIST_KEY === 'nmKdService2') {
		                nmKdService2 = x.LABEL;
		        } else
		            if (x.LIST_KEY === 'nmService') {
		                nmService = x.LABEL;
		        } else
		            if (x.LIST_KEY === 'nmFormService') {
		                nmFormService = x.LABEL;
		            } else
		                if (x.LIST_KEY === 'nmKdReference') {
		                    nmKdReference = x.LABEL;
		            } else
		                if (x.LIST_KEY === 'nmKdReference2') {
		                    nmKdReference2 = x.LABEL;
		            } else
		                if (x.LIST_KEY === 'nmReference') {
		                    nmReference = x.LABEL;
		            } else
		                if (x.LIST_KEY === 'nmFormReference') {
		                    nmFormReference = x.LABEL;
		            } 
					else
		                if (x.LIST_KEY === 'nmStatusRequest') {
		                    nmStatusRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRequestId') {
		                    nmRequestId = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRequestDate') {
		                    nmRequestDate = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAssetRequest') {
		                    nmAssetRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAsetIDRequest') {
		                    nmAsetIDRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAsetNameRequest') {
		                    nmAsetNameRequest = x.LABEL;
		            }
					else
		                if (x.LIST_KEY === 'nmLocationRequest') {
		                    nmLocationRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmProblemRequest') {
		                    nmProblemRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRequesterRequest') {
		                    nmRequesterRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDeptRequest') {
		                    nmDeptRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmImpactRequest') {
		                    nmImpactRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTargetDateRequest') {
		                    nmTargetDateRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDeskRequest') {
		                    nmDeskRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormRequest') {
		                    nmTitleFormRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleDetailFormRequest') {
		                    nmTitleDetailFormRequest = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStatusScheduleCM') {
		                    nmStatusScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatScheduleCM') {
		                    nmCatScheduleCM = x.LABEL;
					
		            }
					else
		                if (x.LIST_KEY === 'nmAssetScheduleCM') {
		                    nmAssetScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmProblemScheduleCM') {
		                    nmProblemScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateScheduleCM') {
		                    nmStartDateScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateScheduleCM') {
		                    nmFinishDateScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleScheduleCM') {
		                    nmTitleScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleInfoAssetScheduleCM') {
		                    nmTitleInfoAssetScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleScheduleScheduleCM') {
		                    nmTitleScheduleScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFilterScheduleCM') {
		                    nmFilterScheduleCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStatusWOCM') {
		                    nmStatusWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWOIDCM') {
		                    nmWOIDCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartWOCM') {
		                    nmTotalPersonPartWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalWOCM') {
		                    nmTotalWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmSupervisorWOCM') {
		                    nmSupervisorWOCM = x.LABEL;
		            }
					else
		                if (x.LIST_KEY === 'nmWODateCM') {
		                    nmWODateCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAsetWOCM') {
		                    nmAsetWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateWOCM') {
		                    nmStartDateWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateWOCM') {
		                    nmFinishDateWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescWOCM') {
		                    nmDescWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleWOCM') {
		                    nmTitleWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCategoryWOCM') {
		                    nmCategoryWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRepairWOCM') {
		                    nmRepairWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleSchInfoWOCM') {
		                    nmTitleSchInfoWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleWOWOCM') {
		                    nmTitleWOWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPICWOCM') {
		                    nmPICWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmExternalWOCM') {
		                    nmExternalWOCM = x.LABEL;
		            }
					else
		                if (x.LIST_KEY === 'nmAssetWOCM') {
		                    nmAssetWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmProblemWOCM') {
		                    nmProblemWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateWOCM') {
		                    nmStartDateWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDate') {
		                    nmFinishDate = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleServiceWOCM') {
		                    nmTitleServiceWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitlePersonWOCM') {
		                    nmTitlePersonWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitlePartWOCM') {
		                    nmTitlePartWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServiceIDWOCM') {
		                    nmServiceIDWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServiceNameWOCM') {
		                    nmServiceNameWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDWOCM') {
		                    nmEmpIDWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpNameWOCM') {
		                    nmEmpNameWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescWOCM') {
		                    nmDescWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostPersonWOCM') {
		                    nmCostPersonWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNumberWOCM') {
		                    nmPartNumberWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartName') {
		                    nmPartName = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmQtyWOCM') {
		                    nmQtyWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmUnitCostWOCM') {
		                    nmUnitCostWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotCostWOCM') {
		                    nmTotCostWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescPartWOCM') {
		                    nmDescPartWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendorIdWOCM') {
		                    nmVendorIdWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendorNamaWOCM') {
		                    nmVendorNamaWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmContactPersonWOCM') {
		                    nmContactPersonWOCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNotes') {
		                    nmNotes = x.LABEL;
		            }
					//App Cm
					else
		                if (x.LIST_KEY === 'nmStatusApproveCM') {
		                    nmStatusApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRequestDateApproveCM') {
		                    nmRequestDateApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRequesterApproveCM') {
		                    nmRequesterApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAssetIdNameApproveCM') {
		                    nmAssetIdNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmProblemApproveCM') {
		                    nmProblemApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmApproverApproveCM') {
		                    nmApproverApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStatusDescApproveCM') {
		                    nmStatusDescApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormApproveCM') {
		                    nmTitleFormApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoReqApproveCM') {
		                    nmTitleTabInfoReqApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoAsetApproveCM') {
		                    nmTitleTabInfoAsetApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabApprovalApproveCM') {
		                    nmTitleTabApprovalApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNoteApproveCM') {
		                    nmNoteApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartApproveCM') {
		                    nmTotalPersonPartApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalApproveCM') {
		                    nmTotalApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabServiceApproveCM') {
		                    nmTitleTabServiceApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPersonApproveCM') {
		                    nmTitleTabPersonApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPartApproveCM') {
		                    nmTitleTabPartApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDApproveCM') {
		                    nmEmpIDApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpNameApproveCM') {
		                    nmEmpNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPersonNameApproveCM') {
		                    nmPersonNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescApproveCM') {
		                    nmDescApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostApproveCM') {
		                    nmCostApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDApproveCM') {
		                    nmVendIDApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendNameApproveCM') {
		                    nmVendNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServIDApproveCM') {
		                    nmServIDApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServNameApproveCM') {
		                    nmServNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNumApproveCM') {
		                    nmPartNumApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNameApproveCM') {
		                    nmPartNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmQtyApproveCM') {
		                    nmQtyApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmUnitCostApproveCM') {
		                    nmUnitCostApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotCostApproveCM') {
		                    nmTotCostApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTargetDateApproveCM') {
		                    nmTargetDateApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDNameApproveCM') {
		                    nmVendIDNameApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRepairApproveCM') {
		                    nmRepairApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRdoIntApproveCM') {
		                    nmRdoIntApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRdoExtApproveCM') {
		                    nmRdoExtApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNoReqApproveCM') {
		                    nmNoReqApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDeptApproveCM') {
		                    nmDeptApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLocApproveCM') {
		                    nmLocApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateApproveCM') {
		                    nmStartDateApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateApproveCM') {
		                    nmFinishDateApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEstCostApproveCM') {
		                    nmEstCostApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAssetIdNameEntryApproveCM') {
		                    nmAssetIdNameEntryApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAppIDApproveCM') {
		                    nmAppIDApproveCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAppNameApproveCM') {
		                    nmAppNameApproveCM = x.LABEL;
		            }
					//Result Cm
					else
		                if (x.LIST_KEY === 'nmColResultCM') {
		                    nmColResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWOIDResultCM') {
		                    nmColWOIDResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWODateResultCM') {
		                    nmColWODateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetIdNameResultCM') {
		                    nmColAssetIdNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColSchStartDateResultCM') {
		                    nmColSchStartDateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColSchFinishDateResultCM') {
		                    nmColSchFinishDateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColRsltFinishDateResultCM') {
		                    nmColRsltFinishDateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColRsltDescResultCM') {
		                    nmColRsltDescResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWODateResultCM') {
		                    nmWODateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWOFinishResultCM') {
		                    nmWOFinishResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWONotesResultCM') {
		                    nmWONotesResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormResultCM') {
		                    nmTitleFormResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatResultCM') {
		                    nmCatResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoSchResultCM') {
		                    nmTitleTabInfoSchResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoWOResultCM') {
		                    nmTitleTabInfoWOResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoRsltResultCM') {
		                    nmTitleTabInfoRsltResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNoteResultCM') {
		                    nmNoteResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPICResultCM') {
		                    nmPICResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateResultCM') {
		                    nmStartDateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateResultCM') {
		                    nmFinishDateResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinalCostResultCM') {
		                    nmFinalCostResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRefResultCM') {
		                    nmRefResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartEstResultCM') {
		                    nmTotalPersonPartEstResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartRealResultCM') {
		                    nmTotalPersonPartRealResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalResultCM') {
		                    nmTotalResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabServiceResultCM') {
		                    nmTitleTabServiceResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPersonResultCM') {
		                    nmTitleTabPersonResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPartResultCM') {
		                    nmTitleTabPartResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServIDResultCM') {
		                    nmServIDResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServNameResultCM') {
		                    nmServNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDResultCM') {
		                    nmEmpIDResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpNameResultCM') {
		                    nmEmpNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPersonNameResultCM') {
		                    nmPersonNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNumResultCM') {
		                    nmPartNumResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNameResultCM') {
		                    nmPartNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmQtyResultCM') {
		                    nmQtyResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmUnitCostResultCM') {
		                    nmUnitCostResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotCostResultCM') {
		                    nmTotCostResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDNameResultCM') {
		                    nmVendIDNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescResultCM') {
		                    nmDescResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostResultCM') {
		                    nmCostResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRealCostResultCM') {
		                    nmRealCostResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDResultCM') {
		                    nmVendIDResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendNameResultCM') {
		                    nmVendNameResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAssetIdNameEntryResultCM') {
		                    nmAssetIdNameEntryResultCM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRepairExtResultCM') {
		                    nmRepairExtResultCM = x.LABEL;
		            }
					// Result PM
					else
		                if (x.LIST_KEY === 'nmColResultPM') {
		                    nmColResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColResultIDPM') {
		                    nmColResultIDPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWOIDResultPM') {
		                    nmColWOIDResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWODateResultPM') {
		                    nmColWODateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetIdNameResultPM') {
		                    nmColAssetIdNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColSchStartDateResultPM') {
		                    nmColSchStartDateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColSchFinishDateResultPM') {
		                    nmColSchFinishDateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColRsltFinishDateResultPM') {
		                    nmColRsltFinishDateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColRsltDescResultPM') {
		                    nmColRsltDescResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWODateResultPM') {
		                    nmWODateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWOFinishResultPM') {
		                    nmWOFinishResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWONotesResultPM') {
		                    nmWONotesResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormResultPM') {
		                    nmTitleFormResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatResultPM') {
		                    nmCatResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoSchResultPM') {
		                    nmTitleTabInfoSchResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoWOResultPM') {
		                    nmTitleTabInfoWOResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoRsltResultPM') {
		                    nmTitleTabInfoRsltResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNoteResultPM') {
		                    nmNoteResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPICResultPM') {
		                    nmPICResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateResultPM') {
		                    nmStartDateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateResultPM') {
		                    nmFinishDateResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinalCostResultPM') {
		                    nmFinalCostResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRefResultPM') {
		                    nmRefResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartEstResultPM') {
		                    nmTotalPersonPartEstResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartRealResultPM') {
		                    nmTotalPersonPartRealResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalResultPM') {
		                    nmTotalResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabServiceResultPM') {
		                    nmTitleTabServiceResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPersonResultPM') {
		                    nmTitleTabPersonResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPartResultPM') {
		                    nmTitleTabPartResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServIDResultPM') {
		                    nmServIDResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServNameResultPM') {
		                    nmServNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDResultPM') {
		                    nmEmpIDResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpNameResultPM') {
		                    nmEmpNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPersonNameResultPM') {
		                    nmPersonNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNumResultPM') {
		                    nmPartNumResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNameResultPM') {
		                    nmPartNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmQtyResultPM') {
		                    nmQtyResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmUnitCostResultPM') {
		                    nmUnitCostResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotCostResultPM') {
		                    nmTotCostResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDNameResultPM') {
		                    nmVendIDNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescResultPM') {
		                    nmDescResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostResultPM') {
		                    nmCostResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRealCostResultPM') {
		                    nmRealCostResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDResultPM') {
		                    nmVendIDResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendNameResultPM') {
		                    nmVendNameResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAssetIdNameEntryResultPM') {
		                    nmAssetIdNameEntryResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRepairExtResultPM') {
		                    nmRepairExtResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStatusResultPM') {
		                    nmStatusResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLocResultPM') {
		                    nmLocResultPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDueDateResultPM') {
		                    nmDueDateResultPM = x.LABEL;
		            }
					//Employee
					else
		                if (x.LIST_KEY === 'nmKdEmp') {
		                    nmKdEmp = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKdEmp2') {
		                    nmKdEmp2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmp') {
		                    nmEmp = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmp2') {
		                    nmEmp2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpAddress') {
		                    nmEmpAddress = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpCity') {
		                    nmEmpCity = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpPosCode') {
		                    nmEmpPosCode = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpState') {
		                    nmEmpState = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpPhone1') {
		                    nmEmpPhone1 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpPhone2') {
		                    nmEmpPhone2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpEmail') {
		                    nmEmpEmail = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpAktif') {
		                    nmEmpAktif = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFormEmp') {
		                    nmFormEmp = x.LABEL;
		            }
					// Category Aset
					else
		                if (x.LIST_KEY === 'nmKdCategoryAset') {
		                    nmKdCategoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKdCategoryAset2') {
		                    nmKdCategoryAset2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCategoryAset') {
		                    nmCategoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCategoryAset2') {
		                    nmCategoryAset2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleGridAddField') {
		                    nmTitleGridAddField = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmHeaderColAddFieldLabel') {
		                    nmHeaderColAddFieldLabel = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmHeaderColAddFieldType') {
		                    nmHeaderColAddFieldType = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFormCategoryAset') {
		                    nmFormCategoryAset = x.LABEL;
		            }
					// TypeServicePM
					else
		                if (x.LIST_KEY === 'nmKdTypeServicePM') {
		                    nmKdTypeServicePM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKdTypeServicePM2') {
		                    nmKdTypeServicePM2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTypeServicePM') {
		                    nmTypeServicePM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTypeServicePM2') {
		                    nmTypeServicePM2 = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFormTypeServicePM') {
		                    nmFormTypeServicePM = x.LABEL;
		            }
					// Schedule PM
					else
		                if (x.LIST_KEY === 'nmStatusSchPM') {
		                    nmStatusSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatSchPM') {
		                    nmCatSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAsetIDSchPM') {
		                    nmAsetIDSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAsetNameSchPM') {
		                    nmAsetNameSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServiceSchPM') {
		                    nmServiceSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDueDateSchPM') {
		                    nmDueDateSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormSchPM') {
		                    nmTitleFormSchPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmYearsSchPM') {
		                    nmYearsSchPM = x.LABEL;
		            }
					//General
					else
		                if (x.LIST_KEY === 'nmAlertTabService') {
		                    nmAlertTabService = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKonfirmasiHapusBaris') {
		                    nmKonfirmasiHapusBaris = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmOperatorDengan') {
		                    nmOperatorDengan = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmOperatorAnd') {
		                    nmOperatorAnd = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKonfirmasiWO') {
		                    nmKonfirmasiWO = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWarningSelectEditData') {
		                    nmWarningSelectEditData = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAlertReqRejected') {
		                    nmAlertReqRejected = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPilihStatus') {
		                    nmPilihStatus = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTreeComboParent') {
		                    nmTreeComboParent = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKonfirmasiRejected') {
		                    nmKonfirmasiRejected = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPilihDept') {
		                    nmPilihDept = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKonfirmasiResult') {
		                    nmKonfirmasiResult = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPilihCategory') {
		                    nmPilihCategory = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNomorOtomatis') {
		                    nmNomorOtomatis = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmSd') {
		                    nmSd = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBaris') {
		                    nmBaris = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWaitMsgUploadFile') {
		                    nmWaitMsgUploadFile = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKonfirmasiUploadFile') {
		                    nmKonfirmasiUploadFile = x.LABEL;
		            }
					// WO PM
					else
		                if (x.LIST_KEY === 'nmColWOPM') {
		                    nmColWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColIDWOPM') {
		                    nmColIDWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDateWOPM') {
		                    nmColDateWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetIdNameWOPM') {
		                    nmColAssetIdNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColLocWOPM') {
		                    nmColLocWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServWOPM') {
		                    nmServWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateWOPM') {
		                    nmStartDateWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateWOPM') {
		                    nmFinishDateWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescWOPM') {
		                    nmDescWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWODateWOPM') {
		                    nmWODateWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormWOPM') {
		                    nmTitleFormWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatWOPM') {
		                    nmCatWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStatusWOPM') {
		                    nmStatusWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoAsetWOPM') {
		                    nmTitleTabInfoAsetWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabInfoWOPM') {
		                    nmTitleTabInfoWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNotesWOPM') {
		                    nmNotesWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalPersonPartWOPM') {
		                    nmTotalPersonPartWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalWOPM') {
		                    nmTotalWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabServiceWOPM') {
		                    nmTitleTabServiceWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPersonWOPM') {
		                    nmTitleTabPersonWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabPartWOPM') {
		                    nmTitleTabPartWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDNameWOPM') {
		                    nmVendIDNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDWOPM') {
		                    nmEmpIDWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpNameWOPM') {
		                    nmEmpNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDResultWOPM') {
		                    nmVendIDResultWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendNameWOPM') {
		                    nmVendNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPersonNameWOPM') {
		                    nmPersonNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostWOPM') {
		                    nmCostWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServIDWOPM') {
		                    nmServIDWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServNameWOPM') {
		                    nmServNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDueDateWOPM') {
		                    nmDueDateWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNumWOPM') {
		                    nmPartNumWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNameWOPM') {
		                    nmPartNameWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmQtyWOPM') {
		                    nmQtyWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmUnitCostWOPM') {
		                    nmUnitCostWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotCostWOPM') {
		                    nmTotCostWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRdoIntWOPM') {
		                    nmRdoIntWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRdoExtWOPM') {
		                    nmRdoExtWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRepairWOPM') {
		                    nmRepairWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLocWOPM') {
		                    nmLocWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPICWOPM') {
		                    nmPICWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAssetIdNameEntryWOPM') {
		                    nmAssetIdNameEntryWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmApproverIDWOPM') {
		                    nmApproverIDWOPM = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmApproverNameWOPM') {
		                    nmApproverNameWOPM = x.LABEL;
		            }
					// History Aset
					else
		                if (x.LIST_KEY === 'nmTitleFormHistoryAset') {
		                    nmTitleFormHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDateHistoryAset') {
		                    nmDateHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServiceHistoryAset') {
		                    nmServiceHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmMaintHistoryAset') {
		                    nmMaintHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmRepairHistoryAset') {
		                    nmRepairHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTotalHistoryAset') {
		                    nmTotalHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServIDHistoryAset') {
		                    nmServIDHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServNameHistoryAset') {
		                    nmServNameHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostHistoryAset') {
		                    nmCostHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescHistoryAset') {
		                    nmDescHistoryAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormInfoVendor') {
		                    nmTitleFormInfoVendor = x.LABEL;
		            }
					// Info Log Metering	
					else
		                if (x.LIST_KEY === 'nmTitleFormInfoLogMet') {
		                    nmTitleFormInfoLogMet = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLastInfoLogMet') {
		                    nmLastInfoLogMet = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCurrInfoLogMet') {
		                    nmCurrInfoLogMet = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTargetInfoLogMet') {
		                    nmTargetInfoLogMet = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDNameInfoLogMet') {
		                    nmEmpIDNameInfoLogMet = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmUpdateLastServInfoLogMet') {
		                    nmUpdateLastServInfoLogMet = x.LABEL;
		            }
					// Lookup Aset	
					else
		                if (x.LIST_KEY === 'nmTitleFormLookupAset') {
		                    nmTitleFormLookupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmIDAsetLookupAset') {
		                    nmIDAsetLookupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNameLookupAset') {
		                    nmNameLookupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLocLookupAset') {
		                    nmLocLookupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatLookupAset') {
		                    nmCatLookupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDeptLookupAset') {
		                    nmDeptLookupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAsetLookupAset') {
		                    nmAsetLookupAset = x.LABEL;
		            }
					// Lookup Employee	
					else
		                if (x.LIST_KEY === 'nmTitleFormLookupEmp') {
		                    nmTitleFormLookupEmp = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpLookupEmp') {
		                    nmEmpLookupEmp = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpIDLookupEmp') {
		                    nmEmpIDLookupEmp = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpNameLookupEmp') {
		                    nmEmpNameLookupEmp = x.LABEL;
		            }
					// LookupPart	
					else
		                if (x.LIST_KEY === 'nmTitleFormLookupPart') {
		                    nmTitleFormLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartLookupPart') {
		                    nmPartLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNumberLookupPart') {
		                    nmNumberLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNameLookupPart') {
		                    nmNameLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPriceLookupPart') {
		                    nmPriceLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescLookupPart') {
		                    nmDescLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPartNameLookupPart') {
		                    nmPartNameLookupPart = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmSelectLookupPart') {
		                    nmSelectLookupPart = x.LABEL;
		            }
					//Button
					else
		                if (x.LIST_KEY === 'nmBtnHistory') {
		                    nmBtnHistory = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnVendorInfo') {
		                    nmBtnVendorInfo = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnAdd') {
		                    nmBtnAdd = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnOK') {
		                    nmBtnOK = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnCancel') {
		                    nmBtnCancel = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnUpload') {
		                    nmBtnUpload = x.LABEL;
		            }
					// Lookup Service	
					else
		                if (x.LIST_KEY === 'nmTitleFormLookupServ') {
		                    nmTitleFormLookupServ = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServLookupServ') {
		                    nmServLookupServ = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServIDLookupServ') {
		                    nmServIDLookupServ = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmServNameLookupServ') {
		                    nmServNameLookupServ = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmSelectLookupServ') {
		                    nmSelectLookupServ = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDueDateLookupServ') {
		                    nmDueDateLookupServ = x.LABEL;
		            }
					// Upload File	
					else
		                if (x.LIST_KEY === 'nmTitleFormUpload') {
		                    nmTitleFormUpload = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmptyPathUpload') {
		                    nmEmptyPathUpload = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLabelPathUpload') {
		                    nmLabelPathUpload = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleUpload') {
		                    nmTitleUpload = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescUpload') {
		                    nmDescUpload = x.LABEL;
		            }
					// Lookup Vendor	
					else
		                if (x.LIST_KEY === 'nmTitleFormLookupVend') {
		                    nmTitleFormLookupVend = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendLookupVend') {
		                    nmVendLookupVend = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendIDLookupVend') {
		                    nmVendIDLookupVend = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmVendNameLookupVend') {
		                    nmVendNameLookupVend = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAddressLookupVend') {
		                    nmAddressLookupVend = x.LABEL;
		            }
					// Dashboard
					else
		                if (x.LIST_KEY === 'nmTitleFormDashboard') {
		                    nmTitleFormDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPeriodDashboard') {
		                    nmPeriodDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitlePnlCostMaintDashboard') {
		                    nmTitlePnlCostMaintDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostDashboard') {
		                    nmCostDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTooltipCostDashboard') {
		                    nmTooltipCostDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCostRateDashboard') {
		                    nmCostRateDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitlePnlPerformDashboard') {
		                    nmTitlePnlPerformDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitlePnlMaintDashboard') {
		                    nmTitlePnlMaintDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitlePnlCatDashboard') {
		                    nmTitlePnlCatDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColCatDashboard') {
		                    nmColCatDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColSchDashboard') {
		                    nmColSchDashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWODashboard') {
		                    nmColWODashboard = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColCloseDashboard') {
		                    nmColCloseDashboard = x.LABEL;
		            }
					// Early Warning	
					else
		                if (x.LIST_KEY === 'nmTitleFormEarly') {
		                    nmTitleFormEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColCatEarly') {
		                    nmColCatEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColBeforeEarly') {
		                    nmColBeforeEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColTargetEarly') {
		                    nmColTargetEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAfterEarly') {
		                    nmColAfterEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetEarly') {
		                    nmColAssetEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceEarly') {
		                    nmColServiceEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColLocEarly') {
		                    nmColLocEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDeptEarly') {
		                    nmColDeptEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDueDateEarly') {
		                    nmColDueDateEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormAfterEarly') {
		                    nmTitleFormAfterEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormBeforeEarly') {
		                    nmTitleFormBeforeEarly = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormTargetEarly') {
		                    nmTitleFormTargetEarly = x.LABEL;
		            }
					// Maintenance Monitoring	
					else
		                if (x.LIST_KEY === 'nmTitleFormMaintMon') {
		                    nmTitleFormMaintMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColCatMaintMon') {
		                    nmColCatMaintMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColReqMaintMon') {
		                    nmColReqMaintMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColSchMaintMon') {
		                    nmColSchMaintMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWOMaintMon') {
		                    nmColWOMaintMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColResultMaintMon') {
		                    nmColResultMaintMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFilterMaintMon') {
		                    nmFilterMaintMon = x.LABEL;
		            }
					// Maintenance Request Monitoring	
					else
		                if (x.LIST_KEY === 'nmTitleFormMaintReqMon') {
		                    nmTitleFormMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColStatusMaintReqMon') {
		                    nmColStatusMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColReqIDMaintReqMon') {
		                    nmColReqIDMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDateMaintReqMon') {
		                    nmColDateMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetMaintReqMon') {
		                    nmColAssetMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColProblemMaintReqMon') {
		                    nmColProblemMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColRequesterMaintReqMon') {
		                    nmColRequesterMaintReqMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColTargetDateMaintReqMon') {
		                    nmColTargetDateMaintReqMon = x.LABEL;
		            }
					// Maintenance Result Monitoring	
					else
		                if (x.LIST_KEY === 'nmTitleFormMaintRsltMon') {
		                    nmTitleFormMaintRsltMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetMaintRsltMon') {
		                    nmColAssetMaintRsltMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColFinishDateMaintRsltMon') {
		                    nmColFinishDateMaintRsltMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceMaintRsltMon') {
		                    nmColServiceMaintRsltMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColCostMaintRsltMon') {
		                    nmColCostMaintRsltMon = x.LABEL;
		            }
					// Maintenance Schedule Monitoring	
					else
		                if (x.LIST_KEY === 'nmTitleFormMaintSchMon') {
		                    nmTitleFormMaintSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetMaintSchMon') {
		                    nmColAssetMaintSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColLocMaintSchMon') {
		                    nmColLocMaintSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDeptMaintSchMon') {
		                    nmColDeptMaintSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceMaintSchMon') {
		                    nmColServiceMaintSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDueDateMaintSchMon') {
		                    nmColDueDateMaintSchMon = x.LABEL;
		            }
					// Maintenance WO Monitoring	
					else
		                if (x.LIST_KEY === 'nmTitleFormMaintWOMon') {
		                    nmTitleFormMaintWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetMaintWOMon') {
		                    nmColAssetMaintWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColWOIDMaintWOMon') {
		                    nmColWOIDMaintWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColStartDateMaintWOMon') {
		                    nmColStartDateMaintWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColFinishDateMaintWOMon') {
		                    nmColFinishDateMaintWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColJenisMaintWOMon') {
		                    nmColJenisMaintWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceMaintWOMon') {
		                    nmColServiceMaintWOMon = x.LABEL;
		            }
					// On Close Monitoring		
					else
		                if (x.LIST_KEY === 'nmTitleFormOnCloseMon') {
		                    nmTitleFormOnCloseMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetOnCloseMon') {
		                    nmColAssetOnCloseMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColLocOnCloseMon') {
		                    nmColLocOnCloseMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDepOnCloseMon') {
		                    nmColDepOnCloseMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceOnCloseMon') {
		                    nmColServiceOnCloseMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDueDateOnCloseMon') {
		                    nmColDueDateOnCloseMon = x.LABEL;
		            }
					// On Schedule Monitoring
					else
		                if (x.LIST_KEY === 'nmTitleFormOnSchMon') {
		                    nmTitleFormOnSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetOnSchMon') {
		                    nmColAssetOnSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColLocOnSchMon') {
		                    nmColLocOnSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDepOnSchMon') {
		                    nmColDepOnSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceOnSchMon') {
		                    nmColServiceOnSchMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDueDatOnSchMon') {
		                    nmColDueDatOnSchMon = x.LABEL;
		            }
					// On WO Monitoring	
					else
		                if (x.LIST_KEY === 'nmTitleFormOnWOMon') {
		                    nmTitleFormOnWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAssetOnWOMon') {
		                    nmColAssetOnWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColLocOnWOMon') {
		                    nmColLocOnWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDepOnWOMon') {
		                    nmColDepOnWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColServiceOnWOMon') {
		                    nmColServiceOnWOMon = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColDueDatOnWOMon') {
		                    nmColDueDatOnWOMon = x.LABEL;
		            }
					// GEneral
					else
		                if (x.LIST_KEY === 'nmTitleFormDlgApprovalRpt') {
		                    nmTitleFormDlgApprovalRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgHistoryCMRpt') {
		                    nmTitleFormDlgHistoryCMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgHistoryCostRpt') {
		                    nmTitleFormDlgHistoryCostRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgHistoryCostSumRpt') {
		                    nmTitleFormDlgHistoryCostSumRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgHistoryPartRpt') {
		                    nmTitleFormDlgHistoryPartRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgHistoryPMRpt') {
		                    nmTitleFormDlgHistoryPMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgResultCMRpt') {
		                    nmTitleFormDlgResultCMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgResultPMRpt') {
		                    nmTitleFormDlgResultPMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgReqCMRpt') {
		                    nmTitleFormDlgReqCMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgSchCMRpt') {
		                    nmTitleFormDlgSchCMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgSchPMRpt') {
		                    nmTitleFormDlgSchPMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgWOCMRpt') {
		                    nmTitleFormDlgWOCMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormDlgWOPMRpt') {
		                    nmTitleFormDlgWOPMRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPeriodeDlgRpt') {
		                    nmPeriodeDlgRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTahunDlgRpt') {
		                    nmTahunDlgRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDeptDlgRpt') {
		                    nmDeptDlgRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmStartDateDlgRpt') {
		                    nmStartDateDlgRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmFinishDateDlgRpt') {
		                    nmFinishDateDlgRpt = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWarningDateDlgRpt') {
		                    nmWarningDateDlgRpt = x.LABEL;
		            }
					// History Input Log
					else
		                if (x.LIST_KEY === 'nmTitleFormHistoryInputLog') {
		                    nmTitleFormHistoryInputLog = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDateHistoryInputLog') {
		                    nmDateHistoryInputLog = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmMeterHistoryInputLog') {
		                    nmMeterHistoryInputLog = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmEmpHistoryInputLog') {
		                    nmEmpHistoryInputLog = x.LABEL;
		            }
					
					else
		                if (x.LIST_KEY === 'nmKonfirmasiPathUploadFile') {
		                    nmKonfirmasiPathUploadFile = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmWarningDtlCat') {
		                    nmWarningDtlCat = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPilihCompany') {
		                    nmPilihCompany = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmPilihLocation') {
		                    nmPilihLocation = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnMetering') {
		                    nmBtnMetering = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnLog') {
		                    nmBtnLog = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnHistoryLog') {
		                    nmBtnHistoryLog = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnBrowse') {
		                    nmBtnBrowse = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmBtnRemoveImage') {
		                    nmBtnRemoveImage = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleFormSetupAset') {
		                    nmTitleFormSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmKdAsetSetupAset') {
		                    nmKdAsetSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmColAsetSetupAset') {
		                    nmColAsetSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCatSetupAset') {
		                    nmCatSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDeptSetupAset') {
		                    nmDeptSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmYearSetupAset') {
		                    nmYearSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmDescSetupAset') {
		                    nmDescSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleGenInfoSetupAset') {
		                    nmTitleGenInfoSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmNamaAsetSetupAset') {
		                    nmNamaAsetSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabSpecSetupAset') {
		                    nmTitleTabSpecSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleTabRefSetupAset') {
		                    nmTitleTabRefSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmTitleDocSetupAset') {
		                    nmTitleDocSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmAttributSetupAset') {
		                    nmAttributSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmValueSetupAset') {
		                    nmValueSetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmCompanySetupAset') {
		                    nmCompanySetupAset = x.LABEL;
		            }else
		                if (x.LIST_KEY === 'nmLocationSetupAset') {
		                    nmLocationSetupAset = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmAsetCodeSetupAset')
					{
		                    nmAsetCodeSetupAset = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmDescSetPart')
					{
		                    nmDescSetPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPriceSetPart')
					{
		                    nmPriceSetPart = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleFormUtility')
					{
		                    nmTitleFormUtility  = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabGenUtility')
					{
		                    nmTitleTabGenUtility  = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleFieldBahasaUtility')
					{
		                    nmTitleFieldBahasaUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabCompUtility')
					{
		                    nmTitleTabCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmIdCompUtility')
					{
		                    nmIdCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmNamaCompUtility')
					{
		                    nmNamaCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmAdresCompUtility')
					{
		                    nmAdresCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTelpCompUtility')
					{
		                    nmTelpCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmFaxCompUtility')
					{
		                    nmFaxCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmCityCompUtility')
					{
		                    nmCityCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPosCdCompUtility')
					{
		                    nmPosCdCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmEmailCompUtility')
					{
		                    nmEmailCompUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmBhsUtility')
					{
		                    nmBhsUtility = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleFormSetServPart')
					{
		                    nmTitleFormSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmIdServSetServPart')
					{
		                    nmIdServSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmNamaServSetServPart')
					{
		                    nmNamaServSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleFieldServSetServPart')
					{
		                    nmTitleFieldServSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPartNumSetServPart')
					{
		                    nmPartNumSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPartNamaSetServPart')
					{
		                    nmPartNamaSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmInsSetServPart')
					{
		                    nmInsSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmQtySetServPart')
					{
		                    nmQtySetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmCostSetServPart')
					{
		                    nmCostSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTotCostSetServPart')
					{
		                    nmTotCostSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmCatSetServPart')
					{
		                    nmCatSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPartDtlSetServPart')
					{
		                    nmPartDtlSetServPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmKonfirmBhsUtility')
					{
		                    nmKonfirmBhsUtility = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPilihService')
					{
		                    nmPilihService = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmBtnApply')
					{
		                    nmBtnApply = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPathImageSetRef')
					{
		                    nmPathImageSetRef = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmWarningDeleteSaveDB')
					{
		                    nmWarningDeleteSaveDB = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmWarningSelectRow')
					{
		                    nmWarningSelectRow = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmWarningSelectRowDelete')
					{
		                    nmWarningSelectRowDelete = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmKonfirmDelete')
					{
		                    nmKonfirmDelete = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmWarningIsiText')
					{
		                    nmWarningIsiText = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmWarningTextKosong')
					{
		                    nmWarningTextKosong = x.LABEL;
		            }
					// else if (x.LIST_KEY === 'nmMenuAcorMaintTask')
					// {
		                    // nmMenuAcorMaintTask = x.LABEL;
		            // }
					// else if (x.LIST_KEY === 'nmMenuAcorReport')
					// {
		                    // nmMenuAcorReport = x.LABEL;
		            // }
					
					else if (x.LIST_KEY === 'nmTitleFormSettingBhs')
					{
		                    nmTitleFormSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmBhsSettingBhs')
					{
		                    nmBhsSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmImportBhsSettingBhs')
					{
		                    nmImportBhsSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmGroupSettingBhs')
					{
		                    nmGroupSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmDtBhsSettingBhs')
					{
		                    nmDtBhsSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmDescSettingBhs')
					{
		                    nmDescSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLabelSettingBhs')
					{
		                    nmLabelSettingBhs = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmKonfirmasiSettingBhs')
					{
		                    nmKonfirmasiSettingBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleFormSetupBhs')
					{
		                    nmTitleFormSetupBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmIDSetupBhs')
					{
		                    nmIDSetupBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmNamaSetupBhs')
					{
		                    nmNamaSetupBhs = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabFormInformasi')
					{
		                    nmTitleTabFormInformasi = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegFutureSchPM')
					{
		                    nmLegFutureSchPM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegLostSchPM')
					{
		                    nmLegLostSchPM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegOnSchSchPM')
					{
		                    nmLegOnSchSchPM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegFutureScheduleCM')
					{
		                    nmLegFutureScheduleCM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegLostScheduleCM')
					{
		                    nmLegLostScheduleCM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegOnSchScheduleCM')
					{
		                    nmLegOnSchScheduleCM = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleGridAddPart')
					{
		                    nmTitleGridAddPart = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColLengthAddFieldSetEqpCat')
					{
		                    nmColLengthAddFieldSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColNumPartSetEqpCat')
					{
		                    nmColNumPartSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColNamePartSetEqpCat')
					{
		                    nmColNamePartSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColServMetSetEqpCat')
					{
		                    nmColServMetSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColLogMetSetEqpCat')
					{
		                    nmColLogMetSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColIntMetSetEqpCat')
					{
		                    nmColIntMetSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColUnitMetSetEqpCat')
					{
		                    nmColUnitMetSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmParentSetEqpCat')
					{
		                    nmParentSetEqpCat = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTypeSetEqpCat')
					{
		                    nmTypeSetEqpCat = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleFormReportGenView')
					{
		                    nmTitleFormReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeCMReportGenView')
					{
		                    nmTreeCMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreePMReportGenView')
					{
		                    nmTreePMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeReqCMReportGenView')
					{
		                    nmTreeReqCMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeAppCMReportGenView')
					{
		                    nmTreeAppCMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeSchCMReportGenView')
					{
		                    nmTreeSchCMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeWOCMReportGenView')
					{
		                    nmTreeWOCMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeResultCMReportGenView')
					{
		                    nmTreeResultCMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeSchPMReportGenView')
					{
		                    nmTreeSchPMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeWOPMReportGenView')
					{
		                    nmTreeWOPMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTreeResultPMReportGenView')
					{
		                    nmTreeResultPMReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleFormReportGenQuery')
					{
		                    nmTitleFormReportGenQuery = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTreeHeaderReportGenView')
					{
		                    nmTitleTreeHeaderReportGenView = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTreeHeaderReportGenQuery')
					{
		                    nmTitleTreeHeaderReportGenQuery = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleNamaKolomReportGen')
					{
		                    nmTitleNamaKolomReportGen = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleReportGen')
					{
		                    nmTitleReportGen = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLabelDefaultSettingBhs')
					{
		                    nmLabelDefaultSettingBhs = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmAsetNameFilterSchPM')
					{
		                    nmAsetNameFilterSchPM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLegCloseSchPM')
					{
		                    nmLegCloseSchPM = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColReqDashboard')
					{
		                    nmColReqDashboard = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColOffSchDashboard')
					{
		                    nmColOffSchDashboard = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleFormWarningLogMet')
					{
		                    nmTitleFormWarningLogMet = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColKdAsetWarningLogMet')
					{
		                    nmColKdAsetWarningLogMet = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColNamaAsetWarningLogMet')
					{
		                    nmColNamaAsetWarningLogMet = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColCurrentWarningLogMet')
					{
		                    nmColCurrentWarningLogMet = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColLastDateWarningLogMet')
					{
		                    nmColLastDateWarningLogMet = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmColEmpWarningLogMet')
					{
		                    nmColEmpWarningLogMet = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleFormTracking')
					{
		                    nmTitleFormTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmReqIDTracking')
					{
		                    nmReqIDTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmReqDateTracking')
					{
		                    nmReqDateTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmAssetTracking')
					{
		                    nmAssetTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmLocationTracking')
					{
		                    nmLocationTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmProblemTracking')
					{
		                    nmProblemTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmRequesterTracking')
					{
		                    nmRequesterTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmDeptTracking')
					{
		                    nmDeptTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmImpactTracking')
					{
		                    nmImpactTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTargetDateTracking')
					{
		                    nmTargetDateTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmCategoryTracking')
					{
		                    nmCategoryTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabReqInfoTracking')
					{
		                    nmTitleTabReqInfoTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabAsetInfoTracking')
					{
		                    nmTitleTabAsetInfoTracking = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmTitleTabAppInfoTracking')
					{
		                    nmTitleTabAppInfoTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabWOInfoTracking')
					{
		                    nmTitleTabWOInfoTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmTitleTabCloseInfoTracking')
					{
		                    nmTitleTabCloseInfoTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmStatusAppTracking')
					{
		                    nmStatusAppTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmApproverTracking')
					{
		                    nmApproverTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmStatusDescAppTracking')
					{
		                    nmStatusDescAppTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmDescAppTracking')
					{
		                    nmDescAppTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmStartDateAppTracking')
					{
		                    nmStartDateAppTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmFinishDateAppTracking')
					{
		                    nmFinishDateAppTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmRepairAppTracking')
					{
		                    nmRepairAppTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmDateWOTracking')
					{
		                    nmDateWOTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmFinishDateWOTracking')
					{
		                    nmFinishDateWOTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPICWOTracking')
					{
		                    nmPICWOTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmNotesWOTracking')
					{
		                    nmNotesWOTracking = x.LABEL;
		            }
					
					else if (x.LIST_KEY === 'nmFinishDateCloseTracking')
					{
		                    nmFinishDateCloseTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmNoteCloseTracking')
					{
		                    nmNoteCloseTracking = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmStatusSetupAsset')
					{
		                    nmStatusSetupAsset = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmPilihStatusSetupAset')
					{
		                    nmPilihStatusSetupAset = x.LABEL;
		            }
					else if (x.LIST_KEY === 'nmStatusAssetApprove')
					{
		                    nmStatusAssetApprove = x.LABEL;
		            }
		;			
		
		
    };
   
};

    

