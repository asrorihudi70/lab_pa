﻿// <reference path="../../ext-base.js" />
// <reference path="../../ext-all.js" />

var rowSelectedLook_vupload;
var strpathfile;
var FormWindow;
var nLink;
var strDirektori = 'none';
var strFileName = 'none';
var uFormPenempatan=1; 
var uFormKTPPeg=2; 
var uFormNPWPPeg=3; 
var uFormPendidikanPeg=4; 
var uFormFoto=5; 
var uFormSertifikat=6; 
var uFormkehadiran=7; 
var uFormKKPeg=8; 
var uFormTunjPerPeriode=9; 
var vWinFormEntryUploadfile;

function FormLookupUploadfile(Link, Direktori = null, File_name = null) 
{	
    vWinFormEntryUploadfile = new Ext.Window
	(			
		{
			id: 'formLookupUploadfile',
			title: 'Upload File',
			closeAction: 'destroy',
			width: 450,
			height: 120,
			border: false,			
			frame: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',			
			modal: true,                                   
			items: 
			[
				GetpanelUploadFile(this.Direktori, this.File_name)
			],		
			listeners:
			{ 
				activate: function()
				{ 
					nLink        = Link;
					strDirektori = Direktori;
					strFileName  = File_name;
				} 
			}
		}
	);	
    vWinFormEntryUploadfile.show();
};

function GetpanelUploadFile(Direktori, File_name)
{
    var vWinFormUploadfile = new Ext.FormPanel
	({
		//id: 'FormUpldSeminar',
		layout: 'form',
		width: 400,
		height: 100,
		frame: true,
		border: false,
		fileUpload: true,		
		anchor: '100% 100%',
		items:
		[
			{
				xtype: 'fileuploadfield',
				emptyText: 'Path file upload',
				fieldLabel: 'Path file',
				frame: false,
				border: false,
				name: 'file',
				id: 'file',
				anchor: '100%',
				buttonText: 'browse'
			},
			{
				xtype: 'hidden',
				name: 'direktori',
				id: 'direktori',
			},
			{
				xtype: 'hidden',
				name: 'file_name',
				id: 'file_name',
			}
		],
		buttons:
		[
			{
				text: 'Upload',
				handler: function() 
				{
					// var strUrl="./"+nLink;
					var strUrl= nLink;
					Ext.getCmp('direktori').setValue(strDirektori);
					Ext.getCmp('file_name').setValue(strFileName);
					/*if (nLink == uFormFoto || nLink == uFormkehadiran || nLink == uFormTunjPerPeriode) 
					{
						Ext.get('file').dom.value = strFileName;
						strUrl="./"+nLink
					}else
					{
						// Ext.get('direktori').dom.value = strPathUploadAll + strDirektori;
						Ext.get('file').dom.value = strFileName;
						strUrl="./"+nLink
					} */
					
					if (vWinFormUploadfile.getForm().isValid()) 
					{
						vWinFormUploadfile.getForm().submit({

							url: strUrl,//"./home.mvc/Upload",
							// waitMsg: 'Uploading your photo...',
							// waitMsg: 'Uploading your File...',
							success: function(form, o) //(3)
							{	
								console.log(form);
								console.log(o);
								// Ext.Msg.alert('Success', 'aaaa');
								// strpathfile = o.result.namafile;
								// Ext.Msg.show
								// ({
								// 	title: 'Result',
								// 	msg: o.result.result,
								// 	buttons: Ext.Msg.OK,
								// 	icon: Ext.Msg.INFO
								// });
							},								
							failure: function(form, o) //(4)
							{
								// Ext.Msg.show
								// ({
								// 	title: 'Result',
								// 	msg: o.result.error,
								// 	buttons: Ext.Msg.OK,
								// 	icon: Ext.Msg.ERROR
								// });
							}
						});
					}

				}
			},
			{
				text: 'Cancel',
				handler: function() 
				{
					/*var pathupload;
					pathupload = strDirektori + '/' + strpathfile;
					// pathupload = strPathUploadAll + strDirektori + '/' +strpathfile;
					if (nLink == uFormPenempatan) 
					{
						Ext.get('txtpathsk_viPenempatan').dom.value = pathupload;
					}
					if (nLink == uFormKTPPeg) 
					{
						Ext.get('txtPathKtp_viDtPgw').dom.value = pathupload;
					}
					if (nLink == uFormKKPeg) 
					{
						Ext.get('txtPathKK_viDtPgw').dom.value = pathupload;
					}
					if (nLink == uFormNPWPPeg) 
					{
						Ext.get('txtPathNPWP_viDtPgw').dom.value = pathupload;
					}
					if (nLink == uFormPendidikanPeg) 
					{
						Ext.get('txtpathijazah_vipendPGW').dom.value = pathupload;
					}
					if (nLink == uFormFoto) 
					{
						Ext.get('txtPathFoto_viDtPgw').dom.value = strDirektori + '/' + strpathfile; //pathupload;
					}
					if (nLink == uFormSertifikat) 
					{
						Ext.get('txtPathSertifikat_viSerdik').dom.value = pathupload;
					}
					if (nLink == uFormkehadiran) 
					{
						// Ext.get('txtPathviKehadiran').dom.value = pathupload;
						Import_viKehadiran('-'+strpathfile);
					}if (nLink == uFormTunjPerPeriode) 
					{ 
						Import_viDafTperiodPay('-'+strpathfile);
					}*/
					//vWinFormEntryUploadfile = Ext.getCmp('FormLookupUploadfile')
					vWinFormEntryUploadfile.close();
				}
			}
		]
	});     
	return vWinFormUploadfile;
};


///---------------------------------------------------------------------------------------///