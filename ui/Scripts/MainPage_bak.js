﻿
var gViewPort

Ext.grid.CheckColumn = function(config) {
    Ext.apply(this, config);
    if (!this.id) {
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype = {
    init: function(grid) {
        this.grid = grid;
        this.grid.on('render', function() {
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown: function(e, t) {
        if (t.className && t.className.indexOf('x-grid3-cc-' + this.id) != -1) {
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);            
            var record = this.grid.store.getAt(index);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
        }
    },
    renderer: function(v, p, record) {
        p.css += ' x-grid3-check-col-td';
        return '<div class="x-grid3-check-col' + (v ? '-on' : '') + ' x-grid3-cc-' + this.id + '">&#160;</div>';
    }
};



Ext.onReady(function() {
    var c_user = 'CurrentUser'; // getCookie('username');
    var dsl = new Date();
    cselesai = dsl.format('d-M-Y');

    var cHead = '<div class="header"><div id="diklat" style="position:absolute;  top:10px; left:5px; width:500px; color:#fff;" float="left"> ';
    cHead += '<img src="images/help.png"></div> ';
    cHead += '<div id="login" style="position:absolute;  top:65px; left:20px; width:500px; color:#fff;" float="left">Anda Login Sebagai : ' + c_user + '&nbsp; <a href="/home/"> Logoff </a>&nbsp; | &nbsp;' + cselesai;
    cHead += ' </div></div>';

    var cHtmAnggaran = '<div align="center"><br/>';
    cHtmAnggaran += '<a href="#"><br><img id="daftar2" src="../images/ico/schedule.png" border="0" onclick="LoadModule(212800)"/></a><div><span class="style3">Rencana</span></div>';
    cHtmAnggaran += '<a href="#"><br><img id="daftar2" src="../images/ico/2.png" border="0" onclick="LoadModule(212801)"/></a><div><span class="style3">Realisasi </span></div>';

    var cHtmKeuangan = '<div align="center"><br/>';
    cHtmKeuangan += '<a href="#"><br><img id="daftar2" src="../images/ico/schedule.png" border="0" onclick="LoadModule(212802)"/></a><div><span class="style3">Penerimaan</span></div>';
    cHtmKeuangan += '<a href="#"><br><img id="daftar2" src="../images/ico/2.png" border="0" onclick="LoadModule(212803)"/></a><div><span class="style3">Pengeluaran </span></div>';
    cHtmKeuangan += '<a href="#"><br><img id="daftar2" src="../images/ico/2.png" border="0" onclick="LoadModule(212804)"/></a><div><span class="style3">Penggunaan </span></div>';

    var cHtmAkuntansi = '<div align="center"><br/>';
    cHtmAkuntansi += '<a href="#"><br><img id="daftar2" src="../images/ico/schedule.png" border="0" onclick="LoadModule(211005)"/></a><div><span class="style3">General <br>Ledger</span></div>';
    cHtmAkuntansi += '<a href="#"><br><img id="daftar2" src="../images/ico/date.png" border="0" onclick="LoadModule(211006)"/></a><div><span class="style3">Posting & <br>End Periode </span></div>';


    var cHtmUnitKerja = '<div align="center"><br/>';
    cHtmUnitKerja += '<a href="#"><br><img id="daftar2" src="../images/ico/schedule.png" border="0" onclick="LoadModule(222002)"/></a><div><span class="style3">RKAT</span></div>';
    cHtmUnitKerja += '<a href="#"><br><img id="daftar2" src="../images/ico/2.png" border="0" onclick="LoadModule(222003)"/></a><div><span class="style3">SPTJB</span></div>';

    var cHtmSetup = '<div align="center"><br/>';
    cHtmSetup += '<a href="#"><br><img id="daftar2" src="../images/ico/kwin.png" border="0" onclick="LoadModule(211002)"/></a><div><span class="style3">Daftar <br>Account</span></div>';
    cHtmSetup += '<a href="#"><br><img id="daftar2" src="../images/ico/kwin.png" border="0" onclick="LoadModule(211003)"/></a><div><span class="style3">Jenis <br>Jurnal</span></div>';
    cHtmSetup += '<a href="#"><br><img id="daftar2" src="../images/ico/kwin.png" border="0" onclick="LoadModule(211028)"/></a><div><span class="style3">Mata <br>Anggaran</span></div>';
    cHtmSetup += '<a href="#"><br><img id="daftar2" src="../images/ico/kwin.png" border="0" onclick="LoadModule(221004)"/></a><div><span class="style3">User </span></div>';
    cHtmSetup += '</div>';


   var cHtmLaporan = '<div align="center"><br/>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231007)">Jurnal</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231008)">General Ledger</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231011)">Balance Sheet</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231015)">Arus Kas</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231016)">Income Statement</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231017)">RKAT</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231018)">SPTJB</a><div></div>';
   cHtmLaporan += '<a href="#" onclick="LoadModule(231019)">Perincian Belanja</a><div></div>';
   cHtmLaporan += '</div>';



    var item0 = new Ext.Panel({
        title: 'Anggaran',
        align: 'center',
        html: cHtmAnggaran,
        autoScroll: true,
        cls: 'empty',
        bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'        
    });

    var item1 = new Ext.Panel({
        title: 'Keuangan/Bendahara',
        html: cHtmKeuangan,
        autoScroll: true,
        cls: 'empty',
        bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
    });

    var item2 = new Ext.Panel({
        title: 'Akuntansi',
        html: cHtmAkuntansi,
        autoScroll: true,
        cls: 'empty',
        bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
    });

    var item3 = new Ext.Panel({
        title: 'Unit Kerja',
        html: cHtmUnitKerja,
        autoScroll: true,
        cls: 'empty',
        bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
    });

    var item4 = new Ext.Panel({
        title: 'Laporan',
        html: cHtmLaporan,
        autoScroll: true,
        cls: 'empty',
        bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
    });

    var item5 = new Ext.Panel({
        title: 'Setup',
        html: cHtmSetup,
        autoScroll: true,
        cls: 'empty',
        bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
    });

  var accordion = new Ext.Panel({
        region: 'west',
                id: 'siku-panel', // see Ext.getCmp() below
                title: 'SIKU',
                split: true,
                width: 150,
                minSize: 150,
                maxSize: 150,
                collapsible: true,
                margins: '0 0 0 5',
                layout: {
                    type: 'accordion',
                    animate: true
                },
                items: [
				{
					title: 'Anggaran',
					align: 'center',
					html: cHtmAnggaran,
					autoScroll: true,
					cls: 'empty',
					border: false,
					bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'  					
				},
				{
					title: 'Keuangan/Bendahara',
					html: cHtmKeuangan,
					autoScroll: true,
					cls: 'empty',
					bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
				},
				{
                    title: 'Akuntansi',
					html: cHtmAkuntansi,
					autoScroll: true,
					cls: 'empty',
					bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
                }, 
				{
                   title: 'Unit Kerja',
					html: cHtmUnitKerja,
					autoScroll: true,
					cls: 'empty',
					bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
                },
				{
					title: 'Laporan',
					html: cHtmLaporan,
					autoScroll: true,
					cls: 'empty',
					bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
				},
				{
					title: 'Setup',
					html: cHtmSetup,
					autoScroll: true,
					cls: 'empty',
					bodyStyle: 'background:url(../images/sidemenubg.gif) repeat-y;#79AFCB;scroll:vertical;'
				}]
			
    });


    var headtable = {
        id: 'PanelHead',
        region: 'north',
        layout: 'table',
        cls: 'empty',       
        bodyStyle: 'background:#255AA5;padding:0px;',
        height: 90,
        border: false,
        margins: '5 5 5 5',
        layoutConfig: {
            columns: 2
        },
        items: [{
            height: 90,
            bodyStyle: 'background:transparent',
            border: false,
            html: '<p>' + cHead + '</p>',
            colspan: 2
        }, {
            html: 'Logged in as :# ' + c_user + '&nbsp; Logoff &nbsp; | &nbsp; ',
            bodyStyle: 'background:transparent;color:#BDBEBF;padding:10px 10px 10px 10px;',
            border: false,
            width: 500,
            height: 35
        }, {
            html: '',
            bodyStyle: 'background:transparent;color:#ffffff;padding:10px 10px 10px 10px;',
            border: false,
            width: 440,
            height: 35
}]
        };
        var viewport = new Ext.Viewport({
            layout: 'border',
            bodyStyle: 'background:#F1F7F9;',           
            items: [headtable, accordion, getPanelData()]
        });     
        viewport.render("MainPage");
    });