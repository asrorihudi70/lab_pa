// get main panel
///
// 
//////////////////////
function getMainPanelData() {
    // main form Panel
    var cHome = '<div id="home" align="center">';
    cHome += '<iframe src="'+ baseURL + 'ui/Informasi.php" frameborder=0 width="100%" height="100%">';
    cHome += '</iframe>';
    cHome += '</div>';
		    
    homePanel = new Ext.Panel({
        title: 'Information',//nmTitleTabFormInformasi,
        align: 'center',
        layout: 'column',

        cls: 'icon_keu',
        iconCls: 'icon_keu',
        html:cHome ,
//        items: [{ columnWidth: .75,
//            baseCls: 'x-plain',
//            bodyStyle: 'padding:5px 0 5px 5px', items: [item1]
//        }, { columnWidth: .25,
//            baseCls: 'x-plain',
//            collapsible: true,
//            bodyStyle: 'padding:5px 0 5px 5px', items: [item2]
//        }
//			],
        viewConfig: {
            forceFit: true
        }

    });

    homePanel.title = 'Information';

    //// its working
    var TabAng = new Ext.TabPanel({
        //renderTo:'MainPage',
        activeItem: 0,
        //autoload:true,
        shim: false,
        deferredRender: false,
        //floating:true,
        //region: 'center',
        resizeTabs: true,
        // turn on tab resizing
        minTabWidth: 100,
        //tabWidth:135,
        enableTabScroll: true,
        //closable:true,
        defaults: { autoScroll: true },
        items: [homePanel],
        plugins: new Ext.ux.TabCloseMenu()
    });
    mainPage = TabAng;

    var TabContainer = {
        id: 'MainContentPage',
        region: 'center', // this is what makes this panel into a region within the containing layout
        layout: 'fit',
        margins: '0 0 0 0',
        border: false,
        deferredRender: true,
        items: [TabAng],
        viewConfig: {
            forceFit: true
        }
    };

    return TabContainer;

};

Ext.ux.TabCloseMenu = function() {
    var tabs, menu, ctxItem;
    this.init = function(tp) {
        tabs = tp;
        tabs.on('contextmenu', onContextMenu);
    };

    function onContextMenu(ts, item, e) {
        if (!menu) { // create context menu on first right click
            menu = new Ext.menu.Menu({
                items: [{
                    id: tabs.id + '-close',
                    text: 'Close Tab',
                    handler: function() {
                        tabs.remove(ctxItem);
                    }
                }, {
                    id: tabs.id + '-close-others',
                    text: 'Close Other Tabs',
                    handler: function() {
                        tabs.items.each(function(item) {
                            if (item.closable && item != ctxItem) {
                                tabs.remove(item);
                            }
                        });
                    }
            }]
                });
            }
            ctxItem = item;
            var items = menu.items;
            items.get(tabs.id + '-close').setDisabled(!item.closable);
            var disableOthers = true;

            tabs.items.each(function() {
                if (this != item && this.closable) {
                    disableOthers = false;
                    return false;
                }
            });

            items.get(tabs.id + '-close-others').setDisabled(disableOthers);
            e.stopEvent();
            menu.showAt(e.getPoint());
        }
    };