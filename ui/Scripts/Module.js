/*!
* Ext JS Library 3.0.0
* Copyright(c) 2006-2009 Ext JS, LLC
* licensing@extjs.com
* http://www.extjs.com/license
*/
Ext.app.Module = function(config) {
    Ext.apply(this, config);
    Ext.app.Module.superclass.constructor.call(this);
    this.init();
};

Ext.extend(Ext.app.Module, Ext.util.Observable, {
    init: Ext.emptyFn
});


Ext.grid.CheckColumn = function(config) {
    Ext.apply(this, config);
    if (!this.id) {
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype = {
    init: function(grid) {
        this.grid = grid;
        this.grid.on('render', function() {
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown: function(e, t) {
        if (t.className && t.className.indexOf('x-grid3-cc-' + this.id) != -1) {
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            //var record = this.grid.store.getAt(index);
            var record = this.grid.store.getAt(index);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
        }
    },
    renderer: function(v, p, record) {
        p.css += ' x-grid3-check-col-td';
        return '<div class="x-grid3-check-col' + (v ? '-on' : '') + ' x-grid3-cc-' + this.id + '">&#160;</div>';
    }
};


Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
    onRender: function(ct, position) {
        this.el = ct.createChild({ tag: 'iframe', id: 'iframe-' + this.id, frameBorder: 0, src: this.url });
    }
});

	var my12_hour = 1;

	var dn = ""; var old = "";



function show_clock() {
    var Digital = new Date();
    var hours = Digital.getHours();
    var minutes = Digital.getMinutes();
    var seconds = Digital.getSeconds();

    if (my12_hour) {
            dn = "AM";
            if (hours > 12) { dn = "PM"; hours = hours - 12; }
            if (hours == 0) { hours = 12; }
    } else {
            dn = "";
    }
    if (minutes <= 9) { minutes = "0"+minutes; }
    if (seconds <= 9) { seconds = "0"+seconds; }

    myclock = '';
    myclock += hours+':'+minutes+':'+seconds+' '+dn;
    if (old == "true") {
            Ext.get("Clock-Label").dom.setValue(myclock);
            old = "die"; return;
    }
    setTimeout("show_clock()",1000);
}
