var nmEditData;
var nmRefresh;
var nmMaksData;
var nmTambah;
var nmSimpan;
var nmSimpanKeluar;
var nmHapus;
var nmLookup;
var nmCetak;

var nmTambahBaris;
var nmHapusBaris;



var nmHeaderSimpanData= 'Simpan Data'; //Simpan Data
var nmPesanSimpanSukses='Proses Simpan Berhasil' ; //Data berhasil di simpan';
var nmPesanSimpanGagal='Proses Simpan Gagal' ; //'Data tidak berhasil di simpan, data tersebut sudah ada';
var nmPesanSimpanError='Proses Simpan Gagal' ; //'Data tidak berhasil di simpan, data tersebut sudah ada';

var nmHeaderEditData = 'Edit Data'; //'Data berhasil di edit'
var nmPesanEditSukses='Proses Edit Berhasil' ; //Data berhasil di simpan';
var nmPesanEditGagal='Proses Edit Gagal' ; //'Data tidak berhasil di simpan, data tersebut sudah ada';
var nmPesanEditError='Proses Edit Gagal' ; //'Data tidak berhasil di simpan, data tersebut sudah ada';

var nmHeaderHapusData = 'Hapus Data'; //'Data berhasil di edit'
var nmPesanHapusSukses='Proses Hapus Berhasil' ; //Data berhasil di simpan';
var nmPesanHapusGagal='Proses Hapus Gagal' ; //'Data tidak berhasil di simpan, data tersebut sudah ada';
var nmPesanHapusError='Proses Hapus Gagal' ; //'Data tidak berhasil di simpan, data tersebut sudah ada';

// Lokasi
var nmKdLokasi;
var nmKdLokasi2;
var nmLokasi;
var nmFormLokasi;

// Status Asset
var nmKdStatusAsset='ID';
var nmKdStatusAsset2='ID';
var nmStatusAsset='Status';
var nmFormStatusAsset='Status';


// Department
var nmKdDepartement;
var nmKdDepartement2;
var nmDepartement;
var nmFormDepartement;

// Part
var nmKdPart; // ='Part Number';
var nmKdPart2; // ='Number';
var nmPart; // ='Part';
var nmDescSetPart; //='Description';
var nmPriceSetPart; //='Price';
var nmFormPart; // ='Part';

// Vendor
var nmKdVendor;
var nmKdVendor2;
var nmVendor;
var nmVendor2;
var nmVendorContact1;
var nmVendorContact2;
var nmVendorAddress;
var nmVendorCity;
var nmVendorPosCode;
var nmVendorCountry;
var nmVendorPhone1;
var nmVendorPhone2;
var nmFormVendor;

// Employee
var nmKdEmp; // ='Employee Id';
var nmKdEmp2; // ='Id';
var nmEmp; // ='Name';
var nmEmp2; // ='Employee';
var nmEmpAddress; //='Address';
var nmEmpCity; //='City';
var nmEmpPosCode; //='Pos Code';
var nmEmpState; //='State';
var nmEmpPhone1; //='Phone 1';
var nmEmpPhone2; //='Phone 2';
var nmEmpEmail; //='Email';
var nmEmpAktif; //='Active';
var nmFormEmp; // ='Employee';

// TypeServicePM
var nmKdTypeServicePM; // ='Service PM Type Id';
var nmKdTypeServicePM2; // ='Id';
var nmTypeServicePM; // ='Service PM Type';
var nmTypeServicePM2; // ='Service PM Type';
var nmFormTypeServicePM; // ='Service PM Type';

// Category Aset
var nmKdCategoryAset; // ='Category Id';
var nmKdCategoryAset2; // ='Id';
var nmCategoryAset; // ='Category';
var nmCategoryAset2; // ='Category';
var nmTitleGridAddField; //='Additional Fields';
var nmHeaderColAddFieldLabel; //='Label Field';
var nmHeaderColAddFieldType; //='Type Field';
var nmFormCategoryAset; // ='Asset Maintenance Category';

var	nmTitleGridAddPart; //	=	'Add Part';
var	nmColLengthAddFieldSetEqpCat; //	= 'Length';
var	nmColNumPartSetEqpCat; //	=	'Part Number';
var	nmColNamePartSetEqpCat; //	=	'Part Name';
var	nmColServMetSetEqpCat; //	=	'Service';
var	nmColLogMetSetEqpCat; //	=	'Log Type';
var	nmColIntMetSetEqpCat; //	=	'Interval';
var	nmColUnitMetSetEqpCat; //	=	'Unit';
var	nmParentSetEqpCat; //	=	'Parent';
var	nmTypeSetEqpCat; //	=	'Type';


// Satuan
var nmKdSatuan;
var nmKdSatuan2;
var nmSatuan;
var nmFormSatuan;

// Satuan
var nmKdService;
var nmKdService2;
var nmService;
var nmFormService;

// Reference Asset
var nmKdReference;
var nmKdReference2;
var nmReference;
var nmFormReference;

//Request CM
var nmStatusRequest;
var nmRequestId;
var nmRequestDate;
var nmAssetRequest;
var nmLocationRequest;
var nmProblemRequest;
var nmRequesterRequest;
var nmDeptRequest;
var nmImpactRequest;
var nmTargetDateRequest;
var nmDeskRequest;
var nmTitleFormRequest;
var nmTitleDetailFormRequest;
var	nmAsetIDRequest;//	=	'Asset Maintenance ID';
var	nmAsetNameRequest;//	=	'Asset Maintenance Name';

//Schedule CM
var nmStatusScheduleCM;
var nmAssetScheduleCM;
var nmProblemScheduleCM;
var nmStartDateScheduleCM;
var nmFinishDateScheduleCM;
var nmTitleScheduleCM;
var nmTitleInfoAssetScheduleCM;
var nmTitleScheduleScheduleCM;
var nmFilterScheduleCM;
var	nmCatScheduleCM;//	=	'Category' ; //tambahan
var nmLegFutureScheduleCM;
var nmLegLostScheduleCM;
var nmLegOnSchScheduleCM;

//Work Order CM
var nmStatusWOCM;
var nmWOIDCM;
var nmWODateCM;
var nmAsetWOCM;
var nmStartDateWOCM;
var nmFinishDateWOCM;
var nmDescWOCM;
var nmTitleWOCM;
var nmCategoryWOCM;
var nmRepairWOCM;
var nmTitleSchInfoWOCM;
var nmTitleWOWOCM;
var nmPICWOCM;
var nmExternalWOCM;
var nmAssetWOCM;
var nmProblemWOCM;
//var nmStartDateWOCM;
var nmFinishDate;
var nmTitleServiceWOCM;
var nmTitlePersonWOCM;
var nmTitlePartWOCM;
var nmServiceIDWOCM;
var nmServiceNameWOCM;
var nmEmpIDWOCM;
var nmEmpNameWOCM;
//var nmDescWOCM;
var nmCostPersonWOCM;
var nmPartNumberWOCM;
var nmPartName;
var nmQtyWOCM;
var nmUnitCostWOCM;
var nmTotCostWOCM;
var nmDescPartWOCM;
var nmVendorIdWOCM;
var nmVendorNamaWOCM;
var nmContactPersonWOCM;
var nmNotes;
var	nmTotalPersonPartWOCM; //	=	'Total Person Cost + Part Cost';
var	nmTotalWOCM; //	=	'Total';
var	nmSupervisorWOCM; //	=	'Supervisor';


// Approval
var nmStatusApproveCM;  //	= 'Status';
var nmRequestDateApproveCM;  //	= 'Request Date';
var nmRequesterApproveCM;  //	= 'Requester';
var nmAssetIdNameApproveCM;  //	= 'Asset Maintenance';
var nmProblemApproveCM;  //	= 'Problems';
var nmApproverApproveCM;  //	= 'Approver';
var nmStatusDescApproveCM;  //	= 'Status Desc.';
var nmTitleFormApproveCM;  //	= 'Request Approval';
var nmTitleTabInfoReqApproveCM;  //	= 'Request Information';
var nmTitleTabInfoAsetApproveCM;  //	= 'Asset Maintenance Information';
var nmTitleTabApprovalApproveCM;  //	= 'Approval';
var nmNoteApproveCM;  //	= 'Notes';
var nmTotalPersonPartApproveCM;  //	= 'Total Person Cost + Part Cost';
var nmTotalApproveCM;  //	= 'Total';
var nmTitleTabServiceApproveCM;  //	= 'Service';
var nmTitleTabPersonApproveCM;  //	= 'Person';
var nmTitleTabPartApproveCM;  //	= 'Part';
var nmEmpIDApproveCM;  //	= 'Employee ID';
var nmEmpNameApproveCM;  //	= 'Employee Name';
var nmPersonNameApproveCM;  //	= 'Person Name';
var nmDescApproveCM;  //	= 'Description';
var nmCostApproveCM;  //	= 'Cost';
var nmVendIDApproveCM;  //	= 'Vendor ID';
var nmVendNameApproveCM;  //	= 'Vendor Name';
var nmServIDApproveCM;  //	= 'Service ID';
var nmServNameApproveCM;  //	= 'Service Name';
var nmPartNumApproveCM;  //	= 'Part Number';
var nmPartNameApproveCM;  //	= 'Part Name';
var nmQtyApproveCM;  //	= 'Qty';
var nmUnitCostApproveCM;  //	= 'Unit Cost';
var nmTotCostApproveCM;  //	= 'Total Cost';
var nmTargetDateApproveCM;  //	= 'Target Date';
var nmVendIDNameApproveCM;  //	= 'Vendor';
var nmRepairApproveCM;  //	= 'Repair by';
var nmRdoIntApproveCM;  //	= 'Internal';
var nmRdoExtApproveCM;  //	= 'External';
var nmNoReqApproveCM;  //	= 'No. Request';
var nmDeptApproveCM;  //	= 'Department';
var nmLocApproveCM;  //	= 'Location';
var nmStartDateApproveCM;  //	= 'Start Date';
var nmFinishDateApproveCM;  //	 = 'Finish Date';
var nmEstCostApproveCM;  //	= 'Estimation Cost';
var nmAssetIdNameEntryApproveCM;  //	= 'Asset Maint.';
var nmAppIDApproveCM;  //	= 'Approver ID';
var nmAppNameApproveCM;  //	= 'Approver Name';
var nmStatusAssetApprove;


// Result CM
var	nmColResultCM;  //	=	'Result';
var	nmColWOIDResultCM;  //	=	'Work Order ID';
var	nmColWODateResultCM;  //	=	'Work Order Date';
var	nmColAssetIdNameResultCM;  //	=	'Asset Maintenance';
var	nmColSchStartDateResultCM;  //	=	'Schedule Start Date';
var	nmColSchFinishDateResultCM;  //	=	'Schedule Finish Date';
var	nmColRsltFinishDateResultCM;  //	=	'Result Finish Date';
var	nmColRsltDescResultCM;  //	=	'Result Description';
var	nmWODateResultCM;  //	=	'WO Date';
var	nmWOFinishResultCM;  //	=	'WO Finish Date';
var	nmWONotesResultCM;  //	=	'WO Notes';
var	nmTitleFormResultCM;  //	=	'Job Close Status';
var	nmCatResultCM;  //	=	'Category';
var	nmTitleTabInfoSchResultCM;  //	=	'Schedule Information';
var	nmTitleTabInfoWOResultCM;  //	=	'Work Order';
var	nmTitleTabInfoRsltResultCM;  //	=	'Job Close Status';
var	nmNoteResultCM;  //	=	'Job Close Status Notes';
var	nmPICResultCM;  //	=	'PIC';
var	nmStartDateResultCM;  //	=	'Start Date';
var	nmFinishDateResultCM;  //	=	'Finish Date';
var	nmFinalCostResultCM;  //	=	'Final Cost';
var	nmRefResultCM;  //	=	'Ref.';
var	nmTotalPersonPartEstResultCM;  //	=	'Total (Person Cost + Part Cost) Est.';
var	nmTotalPersonPartRealResultCM;  //	=	'Total (Person Cost + Part Cost) Real.';
var	nmTotalResultCM;  //	=	'Total';
var	nmTitleTabServiceResultCM;  //	=	'Service';
var	nmTitleTabPersonResultCM;  //	=	'Person';
var	nmTitleTabPartResultCM;  //	=	'Part';
var	nmServIDResultCM;  //	=	'Service ID';
var	nmServNameResultCM;  //	=	'Service Name';
var	nmEmpIDResultCM;  //	=	'Employee ID';
var	nmEmpNameResultCM;  //	=	'Employee Name';
var	nmPersonNameResultCM;  //	=	'Person Name';
var	nmPartNumResultCM;  //	=	'Part Number';
var	nmPartNameResultCM;  //	=	'Part Name';
var	nmQtyResultCM;  //	=	'Qty';
var	nmUnitCostResultCM;  //	=	'Unit Cost';
var	nmTotCostResultCM;  //	=	'Total Cost';
var	nmVendIDNameResultCM;  //	=	'Vendor';
var	nmDescResultCM;  //	=	'Description';
var	nmCostResultCM;  //	=	'Cost';
var	nmRealCostResultCM;  //	=	'Real Cost';
var	nmVendIDResultCM;  //	=	'Vendor ID';
var	nmVendNameResultCM;  //	=	'Vendor Name';
var	nmAssetIdNameEntryResultCM;  //	=	'Asset Maint.';
var	nmRepairExtResultCM;  //=	'External';


// General		
var	nmAlertTabService;  //	= 'Please select row service';
var	nmKonfirmasiHapusBaris;  //	= 'Are you sure delete this row ?';
var	nmOperatorDengan;  //	='with';
var	nmOperatorAnd;  //	='and';
var	nmKonfirmasiWO;  //	='This request had been work order';
var	nmWarningSelectEditData;  //	='Please select row to edit';
var	nmAlertReqRejected;  //	='This request succeesed rejected';
var	nmPilihStatus;  //	='Choose Status...';
var nmTreeComboParent;  //	= 'Asset Maintenance Category';
var nmKonfirmasiRejected ;  //=	'This request had been approved / rejected';
var nmPilihDept;  //	= 'Choose Department...';
var nmKonfirmasiResult;  //	= 'This work order had been close';
var nmPilihCategory;  //	= 'Choose Category...';
var nmNomorOtomatis;  // ='Automatically from the system ...';
var nmSd;  // ='to';
var nmBaris;  //='Row';
var	nmWaitMsgUploadFile; //	= 'Uploading your file...';
var	nmKonfirmasiUploadFile;   //	='This document not yet upload, Are you sure not upload this document ?';		


// Result PM
var	nmColResultPM;  //	=	'Result';
var	nmColResultIDPM;  //	=	'Result ID';
var	nmColWOIDResultPM;  //	=	'Work Order ID';
var	nmColWODateResultPM;  //	=	'Work Order Date';
var	nmColAssetIdNameResultPM;  //	=	'Asset Maintenance';
var	nmColSchStartDateResultPM;  //	=	'Schedule Start Date';
var	nmColSchFinishDateResultPM;  //	=	'Schedule Finish Date';
var	nmColRsltFinishDateResultPM;  //	=	'Result Finish Date';
var	nmColRsltDescResultPM;  //	=	'Result Description';
var	nmWODateResultPM;  //	=	'WO Date';
var	nmWOFinishResultPM;  //	=	'WO Finish Date';
var	nmWONotesResultPM;  //	=	'WO Notes';
var	nmTitleFormResultPM;  //	=	'Job Close Status';
var	nmCatResultPM;  //	=	'Category';
var	nmTitleTabInfoSchResultPM;  //	=	'Schedule Information';
var	nmTitleTabInfoWOResultPM;  //	=	'Work Order';
var	nmTitleTabInfoRsltResultPM;  //	=	'Job Close Status';
var	nmNoteResultPM;  //	=	'Job Close Status Notes';
var	nmPICResultPM;  //	=	'PIC';
var	nmStartDateResultPM;  //	=	'Start Date';
var	nmFinishDateResultPM;  //	=	'Finish Date';
var	nmFinalCostResultPM;  //	=	'Final Cost';
var	nmRefResultPM;  //	=	'Ref.';
var	nmTotalPersonPartEstResultPM;  //	=	'Total (Person Cost + Part Cost) Est.';
var	nmTotalPersonPartRealResultPM;  //	=	'Total (Person Cost + Part Cost) Real.';
var	nmTotalResultPM;  //	=	'Total';
var	nmTitleTabServiceResultPM;  //	=	'Service';
var	nmTitleTabPersonResultPM;  //	=	'Person';
var	nmTitleTabPartResultPM;  //	=	'Part';
var	nmServIDResultPM;  //	=	'Service ID';
var	nmServNameResultPM;  //	=	'Service Name';
var	nmEmpIDResultPM;  //	=	'Employee ID';
var	nmEmpNameResultPM;  //	=	'Employee Name';
var	nmPersonNameResultPM;  //	=	'Person Name';
var	nmPartNumResultPM;  //	=	'Part Number';
var	nmPartNameResultPM;  //	=	'Part Name';
var	nmQtyResultPM;  //	=	'Qty';
var	nmUnitCostResultPM;  //	=	'Unit Cost';
var	nmTotCostResultPM;  //	=	'Total Cost';
var	nmVendIDNameResultPM;  //	=	'Vendor';
var	nmDescResultPM;  //	=	'Description';
var	nmCostResultPM;  //	=	'Cost';
var	nmRealCostResultPM;  //	=	'Real Cost';
var	nmVendIDResultPM;  //	=	'Vendor ID';
var	nmVendNameResultPM;  //	=	'Vendor Name';
var	nmAssetIdNameEntryResultPM;  //	=	'Asset Maint.';
var	nmRepairExtResultPM;  //	=	'External';
var	nmStatusResultPM;  //	=	'Status';
var nmLocResultPM;  //	=	'Location';
var	nmDueDateResultPM;  //	=	'Due Date';


// Schedule PM
var nmStatusSchPM;  //	=	'Status';
var nmCatSchPM;  //	=	'Category';
var nmAsetIDSchPM;  //	=	'Asset Maintenance ID';
var nmAsetNameSchPM;  //	=	'Asset Maintenance Name';
var nmServiceSchPM;  //	=	'Service';
var nmDueDateSchPM;  //	=	'Due Date';
var nmTitleFormSchPM;  //	=	'Schedule PM';
var nmYearsSchPM;  //	=	'Years';
var nmLegFutureSchPM;
var nmLegLostSchPM
var nmLegOnSchSchPM;
var nmAsetNameFilterSchPM;
var nmLegCloseSchPM;

// WO PM
var	nmColWOPM;  //	=	'WO';
var	nmColIDWOPM;  //	=	'Work Order ID';
var	nmColDateWOPM;  //	=	'Work Order Date';
var	nmColAssetIdNameWOPM;  //	=	'Asset Maintenance';
var nmColLocWOPM;  //	=	'Location';
var nmServWOPM;  //	=	'Service';
var	nmStartDateWOPM	;  //=	'Start Date';
var	nmFinishDateWOPM;  //	=	'Finish Date';
var	nmDescWOPM;  //	=	'Description';
var	nmWODateWOPM;  //	=	'WO Date';
var	nmTitleFormWOPM;  //	=	'Work Order Maintenance';
var	nmCatWOPM;  //	=	'Category';
var	nmStatusWOPM;  //	=	'Status';
var	nmTitleTabInfoAsetWOPM;  //	=	'Asset Maintenance Information';
var	nmTitleTabInfoWOPM;  //	=	'Work Order Information';
var	nmNotesWOPM;  //	=	'Notes';
var	nmTotalPersonPartWOPM;  //	=	'Total Person Cost + Part Cost';
var	nmTotalWOPM;  //	=	'Total'; 
var	nmTitleTabServiceWOPM;  //	=	'Service';
var	nmTitleTabPersonWOPM;  //	=	'Person';
var	nmTitleTabPartWOPM;  //	=	'Part';
var	nmVendIDNameWOPM;  //	=	'Vendor';
var	nmEmpIDWOPM;  //	=	'Employee ID';
var	nmEmpNameWOPM;  //	=	'Employee Name';
var	nmVendIDResultWOPM;  //	=	'Vendor ID';
var	nmVendNameWOPM;  //	=	'Vendor Name';
var	nmPersonNameWOPM;  //	=	'Person Name';
var	nmCostWOPM;  //	=	'Cost';
var	nmServIDWOPM;  //	=	'Service ID';
var	nmServNameWOPM;  //	=	'Service Name';
var	nmDueDateWOPM;  //	=	'Due Date';
var	nmPartNumWOPM;  //	=	'Part Number';
var	nmPartNameWOPM;  //	=	'Part Name';
var	nmQtyWOPM;  //	=	'Qty';
var	nmUnitCostWOPM;  //	=	'Unit Cost';
var	nmTotCostWOPM;  //	=	'Total Cost';
var	nmRdoIntWOPM;  //	=	'Internal';
var	nmRdoExtWOPM;  //	=	'External';
var	nmRepairWOPM;  //	=	'Repair by';
var nmLocWOPM;  //	=	'Location';
var	nmPICWOPM;  //	=	'PIC';
var	nmAssetIdNameEntryWOPM;  //	=	'Asset Maint.';
var	nmApproverIDWOPM;  //	=	'Approver ID';
var	nmApproverNameWOPM;  //	=	'Approver Name';


// History Aset
var	nmTitleFormHistoryAset;  //	=	'Asset History';
var	nmDateHistoryAset;  //	=	'Date';
var	nmServiceHistoryAset;  //	=	'Service';
var	nmMaintHistoryAset;  //	=	'Maintenance';
var	nmRepairHistoryAset;  //	=	'Repair';
var	nmTotalHistoryAset;  //	=	'Total';
var	nmServIDHistoryAset;  //	=	'Service ID';
var	nmServNameHistoryAset;  //	=	'Service Name';
var	nmCostHistoryAset;  //	=	'Cost';
var	nmDescHistoryAset;  //	=	'Description';


// Info Vendor
var	nmTitleFormInfoVendor;  //	=	'Vendor Information';

// Info Log Metering				
var	nmTitleFormInfoLogMet;  //	=	'Input Log Metering';
var	nmLastInfoLogMet;  //	=	'Last';
var	nmCurrInfoLogMet;  //	=	'Current';
var	nmTargetInfoLogMet;  //	=	'Target';
var	nmEmpIDNameInfoLogMet;  //	=	'Employee';
var nmUpdateLastServInfoLogMet;

// Lookup Aset				
var	nmTitleFormLookupAset;  //	=	'Lookup Asset';
var	nmIDAsetLookupAset;  //	=	'ID';
var	nmNameLookupAset;  //	=	'Name';
var	nmLocLookupAset;  //	=	'Location';
var	nmCatLookupAset;  //	=	'Category';
var	nmDeptLookupAset;  //	=	'Dept.';
var nmAsetLookupAset;  //	=	'Asset Maintenance';


// Lookup Employee				
var	nmTitleFormLookupEmp;  //	='Lookup Employee';
var nmEmpLookupEmp;  //	=	'Employee';
var	nmEmpIDLookupEmp;  //	=	'Employee ID';
var	nmEmpNameLookupEmp;  //	=	'Employee Name';

// LookupPart				
var	nmTitleFormLookupPart;  //		=	'Lookup Parts';
var nmPartLookupPart;  //		=	'Part';
var	nmNumberLookupPart;  //		=	'Number';
var	nmNameLookupPart;  //		=	'Name';
var	nmPriceLookupPart;  //		=	'Price';
var	nmDescLookupPart;  //		=	'Description';
var nmPartNameLookupPart;  //		=	'Part Name';
var	nmSelectLookupPart;  //		=	'Select';

// Lookup Service				
var	nmTitleFormLookupServ;  //	=	'Lookup Service';
var nmServLookupServ;  //	=	'Service';
var	nmServIDLookupServ;  //	=	'Service ID';
var	nmServNameLookupServ;  //	=	'Service Name';
var	nmSelectLookupServ;  //	=	'Select';
var	nmDueDateLookupServ;  //	=	'Due Date';

//Button	
var nmBtnHistory;  //	= 'History'
var nmBtnVendorInfo;  //	= 'Vendor Information'
var nmBtnAdd;  //	='Add';
var nmBtnOK;  //	= 'Ok';
var nmBtnCancel;  // = 'Cancel';
var nmBtnUpload;  //	= 'Upload';

// Upload File				
var	nmTitleFormUpload;  //	=	'Upload File';
var nmEmptyPathUpload;  //	=	'Path File..........';
var	nmLabelPathUpload;  //	=	'Path File';
var	nmTitleUpload;  //	=	'Title';
var	nmDescUpload;  //	=	'Description';

// Lookup Vendor				
var	nmTitleFormLookupVend;  //	=	'Lookup Vendor';
var nmVendLookupVend;  //	=	'Vendor';
var	nmVendIDLookupVend;  //	=	'ID';
var	nmVendNameLookupVend;  //	=	'Name';
var	nmAddressLookupVend;  //	=	'Address';


// Dashboard
var	nmTitleFormDashboard;  //	=	'Dashboard';
var nmPeriodDashboard;  //	=	'Period';
var	nmTitlePnlCostMaintDashboard;  //	=	'Cost Maintenance';
var	nmCostDashboard;  //	=	'Cost';
var	nmTooltipCostDashboard;  //	=	'Cost maintenance in';
var	nmCostRateDashboard;  //	=	'Cost Rate';
var	nmTitlePnlPerformDashboard;  //	=	'Performance';
var	nmTitlePnlMaintDashboard;  //	=	'Maintenance By Category';
var	nmTitlePnlCatDashboard;  //	=	'Category';
var	nmColCatDashboard;  //	=	'Category';
var	nmColSchDashboard;  //	=	'On Schedule';
var	nmColWODashboard;  //	=	'Work Order';
var	nmColCloseDashboard;  //	=	'Close';
var nmColReqDashboard;
var nmColOffSchDashboard;


// Early Warning				
var	nmTitleFormEarly;  //	=	'Early Warning';
var	nmColCatEarly;  //	=	'Category';
var	nmColBeforeEarly;  //	=	'Before';
var	nmColTargetEarly;  //	=	'Target';
var	nmColAfterEarly;  //	=	'After';
var	nmColAssetEarly;  //	=	'Asset Name';
var	nmColServiceEarly;  //	=	'Service Name';
var	nmColLocEarly;  //	=	'Location';
var	nmColDeptEarly;  //	=	'Department';
var	nmColDueDateEarly;  //	=	'Due Date';
var	nmTitleFormAfterEarly;  //	=	'After Early Warning';
var	nmTitleFormBeforeEarly;  //	=	'Before Early Warning';
var	nmTitleFormTargetEarly;  //	=	'Target Early Warning';


// Maintenance Monitoring				
var	nmTitleFormMaintMon;  //	=	'Maintenance';
var	nmColCatMaintMon;  //	=	'Category';
var	nmColReqMaintMon;  //	=	'Request';
var	nmColSchMaintMon;  //	=	'Schedule';
var	nmColWOMaintMon;  //	=	'Work Order';
var	nmColResultMaintMon;  //	=	'Job Close';
var	nmFilterMaintMon;  //	=	'Filter';
				
// Maintenance Request Monitoring				
var	nmTitleFormMaintReqMon;  //	=	'Maintenance Request';
var	nmColStatusMaintReqMon;  //	=	'Status';
var	nmColReqIDMaintReqMon;  //	=	'Request ID';
var	nmColDateMaintReqMon;  //	=	'Request Date';
var	nmColAssetMaintReqMon;  //	=	'Asset Maintenance';
var	nmColProblemMaintReqMon;  //	=	'Problem (s)';
var	nmColRequesterMaintReqMon;  //	=	'Requester';
var	nmColTargetDateMaintReqMon;  //	=	'Target Date';


// Maintenance Result Monitoring				
var	nmTitleFormMaintRsltMon;  //	=	'Maintenance Job Close';
var	nmColAssetMaintRsltMon;  //	=	'Asset Maintenance';
var	nmColFinishDateMaintRsltMon;  //	=	'Finish Date';
var	nmColServiceMaintRsltMon;  //	=	'Service Name';
var	nmColCostMaintRsltMon;  //	=	'Cost';
				
// Maintenance Schedule Monitoring				
var	nmTitleFormMaintSchMon;  //	=	'Maintenance Schedule';
var	nmColAssetMaintSchMon;  //	=	'Asset Maintenance';
var	nmColLocMaintSchMon;  //	=	'Location';
var	nmColDeptMaintSchMon;  //	=	'Department';
var	nmColServiceMaintSchMon;  //	=	'Service Name';
var	nmColDueDateMaintSchMon;  //	=	'Due Date';

// Maintenance WO Monitoring				
var	nmTitleFormMaintWOMon;  //	=	'Maintenance WO';
var	nmColAssetMaintWOMon;  //	=	'Asset Maintenance';
var	nmColWOIDMaintWOMon;  //	=	'WO ID';
var	nmColStartDateMaintWOMon;  //	=	'Start Date';
var	nmColFinishDateMaintWOMon;  //	=	'Finish Date';
var	nmColJenisMaintWOMon;  //	=	'Jenis';
var	nmColServiceMaintWOMon;  //	=	'Service Name';

// On Close Monitoring				
var	nmTitleFormOnCloseMon;  //	=	'On Close';
var	nmColAssetOnCloseMon;  //	=	'Asset Maintenance';
var	nmColLocOnCloseMon;  //	=	'Location';
var	nmColDepOnCloseMon;  //	=	'Department';
var	nmColServiceOnCloseMon;  //	=	'Service Name';
var	nmColDueDateOnCloseMon;  //	=	'Due Date';

// On Schedule Monitoring				
var	nmTitleFormOnSchMon;  //	=	'On Schedule';
var	nmColAssetOnSchMon;  //	=	'Asset Maintenance';
var	nmColLocOnSchMon;  //	=	'Location';
var	nmColDepOnSchMon;  //	=	'Department';
var	nmColServiceOnSchMon;  //	=	'Service Name';
var	nmColDueDatOnSchMon;  //	=	'Due Date';

// On WO Monitoring				
var	nmTitleFormOnWOMon;  //	=	'On Work Order';
var	nmColAssetOnWOMon;  //	=	'Asset Maintenance';
var	nmColLocOnWOMon;  //	=	'Location';
var	nmColDepOnWOMon;  //	=	'Department';
var	nmColServiceOnWOMon;  //	=	'Service Name';
var	nmColDueDatOnWOMon;  //	=	'Due Date';


// GEneral 
var	nmTitleFormDlgApprovalRpt;  //	=	'Approval Report';
var	nmTitleFormDlgHistoryCMRpt;  //	=	'CM History Report';
var	nmTitleFormDlgHistoryCostRpt;  //	=	'Cost Analysis History Report';
var	nmTitleFormDlgHistoryCostSumRpt;  //	=	'Cost Summary History Report';
var	nmTitleFormDlgHistoryPartRpt;  //	=	'Parts History Report';
var	nmTitleFormDlgHistoryPMRpt;  //	=	'PM History Report';
var	nmTitleFormDlgResultCMRpt;  //	=	'Job Close CM Report';
var	nmTitleFormDlgResultPMRpt;  //	=	'Job Close PM Report';
var	nmTitleFormDlgReqCMRpt;  //	=	'CM Request Report';
var	nmTitleFormDlgSchCMRpt;  //	=	'CM Schedule Report';
var	nmTitleFormDlgSchPMRpt;  //	=	'PM Schedule Report';
var	nmTitleFormDlgWOCMRpt;  //	=	'CM Work Order Report';
var	nmTitleFormDlgWOPMRpt;  //	=	'PM Work Order Report';
var	nmPeriodeDlgRpt;  //	=	'Period';	
var	nmTahunDlgRpt;  //	=	'Tahun';	
var	nmDeptDlgRpt;  //	=	'Department';	
var	nmStartDateDlgRpt;  //	=	'Start Date';	
var	nmFinishDateDlgRpt;  //	=	'Finish Date';	
var	nmWarningDateDlgRpt;  //	=	'The start date criteria should be less more than finish date'; //Kriteria tanggal awal harus lebih kecil dari tanggal akhir

// History Input Log
var nmTitleFormHistoryInputLog;
var nmDateHistoryInputLog;
var nmMeterHistoryInputLog;
var nmEmpHistoryInputLog;

// Setup Aset
var nmKonfirmasiPathUploadFile;
var nmWarningDtlCat;
var nmPilihCompany;
var nmPilihLocation;
var nmBtnMetering;
var nmBtnLog;
var nmBtnHistoryLog;
var nmBtnBrowse;
var nmBtnRemoveImage;
var nmTitleFormSetupAset;
var nmKdAsetSetupAset;
var nmColAsetSetupAset;
var nmCatSetupAset;
var nmDeptSetupAset;
var nmYearSetupAset;
var nmDescSetupAset;
var nmTitleGenInfoSetupAset;
var nmNamaAsetSetupAset;
var nmTitleTabSpecSetupAset;
var nmTitleTabRefSetupAset;
var nmTitleDocSetupAset;
var nmAttributSetupAset;
var nmValueSetupAset;
var nmCompanySetupAset;
var nmLocationSetupAset;
var nmAsetCodeSetupAset;
var nmStatusSetupAsset;

// Utility
var	nmTitleFormUtility; //=	'Utility';
var	nmTitleTabGenUtility; //=	'General';
var	nmTitleFieldBahasaUtility; //=	'Language';
var	nmTitleTabCompUtility; //	=	'Company';
var	nmIdCompUtility; //	=	'ID';
var	nmNamaCompUtility; //	=	'Name';
var	nmAdresCompUtility; //	=	'Address';
var	nmTelpCompUtility; //	=	'Phone';
var	nmFaxCompUtility; //	=	'Fax';
var	nmCityCompUtility; //	=	'City';
var	nmPosCdCompUtility; //	=	'Pos Code';
var	nmEmailCompUtility; //	=	'Email';
var	nmBhsUtility; //	=	'Language';


// General
var nmKonfirmBhsUtility;// = 	'The language setting will be change after close this form'//	Setelah form ini ditutup setting bahasa baru akan berubah
var nmPilihService;//	= 'Choose Service...';
var nmPilihStatusSetupAset;


// Button
var nmBtnApply;//	= 'Apply';


// Reference Asset
var	nmPathImageSetRef;//	='Path Image';

// Setup Service Parts	
var	nmTitleFormSetServPart; //	=	'Service Parts';
var	nmIdServSetServPart; //	=	'ID';
var	nmNamaServSetServPart; //	=	'Service';
var	nmTitleFieldServSetServPart; //	=	'Service Information';
var	nmPartNumSetServPart; //	=	'Part Number';
var	nmPartNamaSetServPart; //	=	'Part Name';
var	nmInsSetServPart; //	=	'Instruction';
var	nmQtySetServPart; //	=	'Qty';
var	nmCostSetServPart; //	=	'Cost';
var	nmTotCostSetServPart; //	=	'Tot. Cost';
var	nmCatSetServPart; //	=	'Category';
var	nmPartDtlSetServPart; //	=	'Service Parts Detail';

// General

var nmWarningDeleteSaveDB;
var nmWarningSelectRow;
var nmWarningSelectRowDelete;
var nmKonfirmDelete;
var nmWarningIsiText;
var nmWarningTextKosong;
var nmKonfirmasiSettingBhs;//	= 'Are you sure will be change the language ?'//	Apakah Anda yakin akan mengganti data bahasa ?
var nmTitleTabFormInformasi;

// Setting Bahasa
var	nmTitleFormSettingBhs;//	=	'Setup Language';
var	nmBhsSettingBhs;//	=	'Language';
var	nmImportBhsSettingBhs;//	=	'Import from Language';
var	nmGroupSettingBhs;//	=	'Group';
var	nmDtBhsSettingBhs;//	=	'data bahasa';
var	nmDescSettingBhs;//	=	'Description';
var	nmLabelSettingBhs;//	=	'Label';
var nmLabelDefaultSettingBhs;

//Setting Bahasa
var	nmTitleFormSetupBhs;//	=	'Setup Language';
var	nmIDSetupBhs;//	=	'ID';
var	nmNamaSetupBhs;//	=	'Language';

// Report Generator
var 	nmTitleFormReportGenView;//	=	'Report Generator';
var 	nmTreeCMReportGenView;//	=	'Corrective Maintenance';
var 	nmTreePMReportGenView;//	=	'Preventive Maintenance';
var 	nmTreeReqCMReportGenView;//	=	'Maintenance Request';
var 	nmTreeAppCMReportGenView;//	=	'Approve Request';
var 	nmTreeSchCMReportGenView;//	=	'Schedule Repair';
var 	nmTreeWOCMReportGenView;//	=	'Work Order Repair';
var 	nmTreeResultCMReportGenView;//	=	'Job Close Status';
var 	nmTreeSchPMReportGenView;//	=	'Schedule Maintenance';
var 	nmTreeWOPMReportGenView;//	=	'Work Order Maintenance';
var 	nmTreeResultPMReportGenView;//	=	'Job Close Status';
var 	nmTitleFormReportGenQuery;//	=	'Report Generator';
var 	nmTitleTreeHeaderReportGenView;//	=	'Report Generator';
var 	nmTitleTreeHeaderReportGenQuery;//	=	'Report Generator';
var 	nmTitleNamaKolomReportGen;//	=	'Column Name';
var 	nmTitleReportGen;//	=	'Report Title';


// alert input log metering
var 	nmTitleFormWarningLogMet;
var 	nmColKdAsetWarningLogMet;
var 	nmColNamaAsetWarningLogMet;
var 	nmColCurrentWarningLogMet;
var 	nmColLastDateWarningLogMet;
var 	nmColEmpWarningLogMet;

// Tracking

var 	nmTitleFormTracking;//	=	Tracking
var 	nmReqIDTracking;//	=	Request ID
var 	nmReqDateTracking;//	=	Request Date
var 	nmAssetTracking;//	=	Asset Maint.
var 	nmLocationTracking;//	=	Location
var 	nmProblemTracking;//	=	Problems
var 	nmRequesterTracking;//	=	Requester
var 	nmDeptTracking;//	=	Department
var 	nmImpactTracking;//	=	Impact
var 	nmTargetDateTracking;//	=	Target Date
var 	nmCategoryTracking;//	=	Category
var 	nmTitleTabReqInfoTracking;//	=	Request Information
var 	nmTitleTabAsetInfoTracking;//	=	Asset Maint. Information
var 	nmTitleTabAppInfoTracking;
var 	nmTitleTabWOInfoTracking;
var 	nmTitleTabCloseInfoTracking;
var 	nmStatusAppTracking;
var 	nmApproverTracking;
var 	nmStatusDescAppTracking;
var 	nmDescAppTracking;
var 	nmStartDateAppTracking;
var 	nmFinishDateAppTracking;
var 	nmRepairAppTracking;
var 	nmDateWOTracking;
var 	nmFinishDateWOTracking;
var 	nmPICWOTracking;
var 	nmNotesWOTracking;

var 	nmFinishDateCloseTracking;
var 	nmNoteCloseTracking;






function nmGetValidasiKosong(x)
{
	//return ('Please enter ' + x + '. ' + x + ' cannot be empty.');
	return (nmWarningIsiText + ' ' + x + '. ' + x + ' ' + nmWarningTextKosong);
};

function nmGetValidasiSelect(x)
{
	//return ('Please select row ' + x );
	return (nmWarningSelectRow + ' ' + x );
};

function nmGetValidasiHapus(x)
{
	//return ('Are you sure to delete the' + ' ' + x + ' ?');
	return (nmKonfirmDelete + ' ' + x + ' ?');
};

function nmGetValidasiHapusBarisDatabase()
{
	//return ('Are you sure to delete this row ? . This row had been save in the database ');
	return (nmWarningDeleteSaveDB);
};

function nmGetKonfirmasiHapusBaris()
{
	//return ('Please select row to delete');
	return (nmWarningSelectRowDelete);
};