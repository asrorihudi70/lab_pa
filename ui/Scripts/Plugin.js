var ws;
function initSocket(){
	ws = new WebSocket("ws://127.0.0.1:1010/");
	ws.onopen = function() {
		console.log('Socket Opened.');
	};
	ws.onmessage = function (evt) {
		socket.onmessage(evt.data);
	};
	ws.onclose = function() {
		setTimeout(function(){initSocket();},5000);
	};
	ws.onerror = function(err) {
	};
}
initSocket();
function sendPrint(data){
	var obj={ID:'',CODE:'SEND_PRINT',DATA:data,DATE:new Date().toString(),SESSION:''};
		// firebase.database().ref('socket').set({data: JSON.stringify(obj)});
		if(ws.readyState!= undefined && ws.readyState==ws.OPEN){
			console.log(JSON.stringify(obj));
		ws.send(JSON.stringify(obj));
	}else{
		Ext.MessageBox.alert('Error', "Socket Not Connect.");
	}
}