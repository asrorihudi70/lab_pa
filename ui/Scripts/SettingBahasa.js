/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />


var dsListKeySettingBahasa;
var dsLanguageSettingBahasa;
var selectLanguageSettingBahasa;
var dsImportLanguageSettingBahasa;
var selectImportLanguageSettingBahasa;
var dsGroupLanguageSettingBahasa;
var selectGroupLanguageSettingBahasa='008';
var mVarDefaultValueGroup='General';


function FormSettingBahasa() 
{
    vWinFormEntrySettingBahasa = new Ext.Window
	(
		{
			id: 'vWinFormEntrySettingBahasa',
			title: nmTitleFormSettingBhs,
			closeAction: 'hide',
			closable:false,
			width: 650,
			height: 462,//420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'Bahasa',
			modal: true,                                   
			items: 
			[
				GetPanelSettingBahasa() 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
	GetBahasaSettingBahasa();
    vWinFormEntrySettingBahasa.show();
};

function GetPanelSettingBahasa() 
{
    var vTabPanelSettingBahasa = new Ext.Panel
    (
        {
            id: 'vTabPanelSettingBahasa',
            region: 'center',
            layout:'form',
            border: false,
            labelAlign : 'right',
            height: 388,
            labelWidth:140,
            anchor: '100%',
            items:
            [
                getItemPanelBahasaSettingBahasa(),
                mComboImportBahasaSettingBahasa(),
                grdLabelBahasaSettingBahasa()
            ]
        }
    )
	
    var FormSettingBahasa = new Ext.Panel
    (
            {
                    id: 'FormSettingBahasa',
                    region: 'center',
                    layout: 'form',
                    title: '',
                    anchor:'100%',
                    bodyStyle: 'padding:7px 7px 7px 7px',
                    border: true,
                    height: 434,//392,
                    shadhow: true,
                    items:
                    [
                            vTabPanelSettingBahasa,getItemPanelSettingBahasa()
                    ]
    }
    );

    return FormSettingBahasa;
};

function getItemPanelBahasaSettingBahasa() 
{
    var items =
    {
        layout: 'column',
        border: false,
        labelAlign : 'right',
        items:
        [
            {
                columnWidth: .6,
                layout: 'form',
                labelAlign : 'right',
                border: false,
                labelWidth:50,
                items:[mComboGroupBahasaSettingBahasa()]
            },
            {
                columnWidth: .35,
                layout: 'form',
                labelAlign : 'right',
                border: false,
                labelWidth:65,
                items:[mComboBahasaSettingBahasa()]
            }
        ]
    }

    return items;
}


function getItemPanelSettingBahasa() 
{
    var items =
    {
	layout: 'column',
	border: false,
        height:33,
	anchor:'100%',
        items:
	[
            {
                layout: 'hBox',
                width:622,
                border: false,
                bodyStyle: 'padding:5px 0px 5px 5px',
                defaults: { margins: '3 3 3 3' },
                anchor: '90%',
                layoutConfig:
                {
                        align: 'middle',
                        pack:'end'
                },
                items:
                [
                    {
                        xtype:'button',
                        text:nmBtnOK,
                        width:70,
                        style:{'margin-left':'0px','margin-top':'0px'},
                        hideLabel:true,
                        id: 'btnOkSettingBahasa',
                        handler:function()
                        {
                            var x = BahasaSettingBahasaSave(true);
                            if (x===undefined)
                            {
                                    vWinFormEntrySettingBahasa.close();
                            };
                        }
                    },
                    {
                        xtype:'button',
                        text:nmBtnCancel ,
                        width:70,
                        hideLabel:true,
                        id: 'btnCancelSettingBahasa',
                        handler:function()
                        {
                                vWinFormEntrySettingBahasa.close();
                        }
                    },
                    {
                        xtype:'button',
                        text:nmBtnApply ,
                        width:70,
                        hideLabel:true,
                        id: 'btnApplySettingBahasa',
                        handler:function()
                        {
                                BahasaSettingBahasaSave(false);
                        }
                   }
                ]
            }
        ]
    }
    return items;
};

function ShowPesanWarningSettingBahasa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};


function mComboBahasaSettingBahasa() 
{
    var Field = ['LANGUAGE_ID','LANGUAGE'];

    dsLanguageSettingBahasa = new WebApp.DataStore({ fields: Field });
	
    dsLanguageSettingBahasa.load
	(
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'LANGUAGE',
                    //Sort: 'language',
                    Sortdir: 'ASC',
                    target: 'ViewComboBahasaUtility',
                    param: ''
                }
            }
	);
	
    var cboBahasaSettingBahasa = new Ext.form.ComboBox
	(
		{
		    id: 'cboBahasaSettingBahasa',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmBhsSettingBhs + ' ',
		    align: 'Right',
		    anchor:'99%',
		    store: dsLanguageSettingBahasa,
		    valueField: 'LANGUAGE_ID',
		    displayField: 'LANGUAGE',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectLanguageSettingBahasa = b.data.LANGUAGE_ID;
			    }
			}
		}
	);
	
    return cboBahasaSettingBahasa;
};





function mComboImportBahasaSettingBahasa() 
{
	var Field = ['LANGUAGE_ID','LANGUAGE'];

    dsImportLanguageSettingBahasa = new WebApp.DataStore({ fields: Field });
	
    dsImportLanguageSettingBahasa.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'LANGUAGE',
			    Sortdir: 'ASC',
			    target: 'ViewComboBahasaUtility',
			    param: ''
			}
		}
	);
	
    var cboImportBahasaSettingBahasa = new Ext.form.ComboBox
	(
		{
		    id: 'cboImportBahasaSettingBahasa',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmImportBhsSettingBhs + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsImportLanguageSettingBahasa,
		    valueField: 'LANGUAGE_ID',
		    displayField: 'LANGUAGE',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectImportLanguageSettingBahasa = b.data.LANGUAGE_ID;
					RefreshDataSettingBahasa();
			    }
			}
		}
	);
	
    return cboImportBahasaSettingBahasa;
};



function mComboGroupBahasaSettingBahasa() 
{
	var Field = ['GROUP_KEY_ID','GROUP_KEY','GROUP_KEY_DESC'];

    dsGroupLanguageSettingBahasa = new WebApp.DataStore({ fields: Field });
	
    dsGroupLanguageSettingBahasa.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'GROUP_KEY',
			    Sortdir: 'ASC',
			    target: 'ViewComboGroupLanguage',
			    param: ''
			}
		}
	);
	
    var cboGroupBahasaSettingBahasa = new Ext.form.ComboBox
	(
		{
		    id: 'cboGroupBahasaSettingBahasa',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmGroupSettingBhs + ' ',
		    align: 'Right',
		    anchor:'100%',
		    store: dsGroupLanguageSettingBahasa,
		    valueField: 'GROUP_KEY_ID',
		    displayField: 'GROUP_KEY',
			value:mVarDefaultValueGroup,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectGroupLanguageSettingBahasa = b.data.GROUP_KEY_ID;
					RefreshDataSettingBahasa();
			    }
			}
		}
	);
	
    return cboGroupBahasaSettingBahasa;
};

function RefreshDataSettingBahasa()
{	
	dsListKeySettingBahasa.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'LIST_KEY_DESC', 
				Sortdir: 'ASC',
				target: 'ViewSettingBahasa',				
				//param: 'where group_key_id =~' + selectGroupLanguageSettingBahasa + '~ and language_id=~' + selectImportLanguageSettingBahasa + '~'
				//param: '"AM_GROUP_KEY_LANGUAGE"."GROUP_KEY_ID" = ~' + selectGroupLanguageSettingBahasa + '~ AND "AM_GROUP_KEY_LANGUAGE"."LANGUAGE_ID" = ~' + selectImportLanguageSettingBahasa + '~'
                                param: 'where am_group_key_language.group_key_id = ~' + selectGroupLanguageSettingBahasa + '~ and am_group_key_language.language_id=~' + selectImportLanguageSettingBahasa + '~'
			}
		}
	);
		
	return dsListKeySettingBahasa;
};


function BahasaSettingBahasaSave(mBol) 
{
	if((Ext.get('cboBahasaSettingBahasa').dom.value === '' && ( selectLanguageSettingBahasa === '' || selectLanguageSettingBahasa === undefined)) || (selectLanguageSettingBahasa != selectImportLanguageSettingBahasa))
	{
		if(Ext.get('cboBahasaSettingBahasa').dom.value === '' && ( selectLanguageSettingBahasa === '' || selectLanguageSettingBahasa === undefined))
		{
			ShowPesanWarningSettingBahasa(nmGetValidasiKosong(nmBhsSettingBhs),nmTitleFormSettingBhs);
		}
		else if (selectLanguageSettingBahasa != selectImportLanguageSettingBahasa)
		{
			Ext.Msg.show
			(
				{
				   title:nmTitleFormSettingBhs,
				   msg: nmKonfirmasiSettingBhs + ' : ' + Ext.get('cboImportBahasaSettingBahasa').dom.value + ' ' + nmOperatorDengan + ' ' + nmDtBhsSettingBhs + ' : ' + Ext.get('cboBahasaSettingBahasa').dom.value ,
				   buttons: Ext.MessageBox.YESNO,
				   width:275,
				   fn: function (btn) 
				   {			
						if (btn =='yes') 
						{
							Ext.Ajax.request
							(
								{
									//url: "./Datapool.mvc/CreateDataObj",
									url: baseURL + "index.php/main/CreateDataObj",
									params: getParamBahasaSettingBahasa(),
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);																			
										if (cst.success === true) 
										{
											//alert('ada');
											
											mLanguage(strUser);
											if(mBol === false)
											{
												ShowPesanInfoSettingBahasa(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility,nmTitleFormSettingBhs);
											};
										}
										else if  (cst.success === false && cst.pesan===0)
										{
											if(mBol === false)
											{
												ShowPesanWarningSettingBahasa(nmPesanSimpanGagal,nmTitleFormSettingBhs);
											};
										}
										else 
										{
											if(mBol === false)
											{
												ShowPesanErrorSettingBahasa(nmPesanSimpanError,nmTitleFormSettingBhs);
											};
										};
									}
								}
							)
						}
					}
				}
			)
		};
		
		if(mBol === true)
		{
			return false;
		};
	}
	else
	{
		Ext.Ajax.request
		(
			{
				//url: "./Datapool.mvc/CreateDataObj",
				url: baseURL + "index.php/main/CreateDataObj",
				params: getParamBahasaSettingBahasa(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);										
					
					if (cst.success === true) 
					{
						//alert('ada');
						mLanguage(strUser);
						if(mBol === false)
						{
							ShowPesanInfoSettingBahasa(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility,nmTitleFormSettingBhs);
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						if(mBol === false)
						{
							ShowPesanWarningSettingBahasa(nmPesanSimpanGagal,nmTitleFormSettingBhs);
						};
					}
					else 
					{
						if(mBol === false)
						{
							ShowPesanErrorSettingBahasa(nmPesanSimpanError,nmTitleFormSettingBhs);
						};
					};
				}
			}
		)
	};
};



function getParamBahasaSettingBahasa() 
{
    var params =
	{	
		Table: 'ViewSettingBahasa', 
		BhsAsal:selectImportLanguageSettingBahasa,
		BhsDest:selectLanguageSettingBahasa,
		List:getArrListSettingBahasa(),
		JmlField:4,
		JmlList:dsListKeySettingBahasa.getCount()
	};
	
    return params
};

function getArrListSettingBahasa()
{
	var x='';
	for(var i = 0 ; i < dsListKeySettingBahasa.getCount();i++)
	{
		var z='@@##$$@@';
				
		y = 'group_key_id = '+dsListKeySettingBahasa.data.items[i].data.GROUP_KEY_ID
		y += z + 'list_key_id = '+dsListKeySettingBahasa.data.items[i].data.LIST_KEY_ID
		y += z + 'language_id = '+ selectLanguageSettingBahasa
		y += z + 'label = '+ dsListKeySettingBahasa.data.items[i].data.LABEL
		
		if (i === (dsListKeySettingBahasa.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		};
	}	
	
	return x;
};

function ShowPesanWarningSettingBahasa(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :300
		}
	);
};

function ShowPesanErrorSettingBahasa(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :300
		}
	);
};

function ShowPesanInfoSettingBahasa(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :300
		}
	);
};

function grdLabelBahasaSettingBahasa()
{

	var fldDetail = ['GROUP_KEY_ID','LIST_KEY_ID','LANGUAGE_ID','LABEL','LIST_KEY','LIST_KEY_DESC','LANGUAGE'];
	dsListKeySettingBahasa = new WebApp.DataStore({ fields: fldDetail });

	var vgrdLabelBahasaSettingBahasa = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vgrdLabelBahasaSettingBahasa',
			title: '',
			stripeRows: true,
			store: dsListKeySettingBahasa,
			height:336,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,
			autoScroll:true,			
			frame:false,
			sm: new Ext.grid.CellSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						cellselect: function(sm, row, rec) 
						{
						}
					}
				}
			),
			cm: fnGridLabelBahasaSettingBahasaColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	dsListKeySettingBahasa.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'LIST_KEY_DESC', 
				Sortdir: 'ASC',
				target: 'ViewSettingBahasa',
				param: selectGroupLanguageSettingBahasa + '%^%' + strUser
			}
		}
	);

	var PnlGridLabelSettingBahasa = new Ext.Panel
	(
		{
		    id: 'PnlGridLabelSettingBahasa',
		    region: 'center',
			layout:'form',
		    border: false,
			autoScroll:false,
			labelAlign : 'right',
		    height: 336,
		    anchor: '100%',
		    items:
			[
				vgrdLabelBahasaSettingBahasa
			]
		}
	);
	
	
	
	return PnlGridLabelSettingBahasa;
};

function fnGridLabelBahasaSettingBahasaColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colListKeyDescSettingBahasa',
				header: nmDescSettingBhs,
				dataIndex: 'LIST_KEY_DESC',				
				width: 200,
				renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
                }
			},
			{ 
				id: 'colLabelSettingBahasaDefault',
				header: nmLabelDefaultSettingBhs,
				dataIndex: 'LABEL',
				width: 250,
				renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
                }
            },
			{ 
				id: 'colLabelSettingBahasa',
				header: nmLabelSettingBhs,
				dataIndex: 'LABEL',
				width: 250,
				editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolLabelSettingBahasa',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
							}
						}
					}
				),
				renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
                }
            }
		]
	)
};

function GetBahasaSettingBahasa()
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
			url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: strUser, //'Admin',
				ModuleID: 'ProsesGetBahasaUtilityAset',
				Params:	strUser 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					selectImportLanguageSettingBahasa=cst.Id;
					Ext.get('cboImportBahasaSettingBahasa').dom.value=cst.Bahasa;
				}
				else
				{
					selectImportLanguageSettingBahasa = '';
					Ext.get('cboImportBahasaSettingBahasa').dom.value='';
				};
			}

		}
	);
};




///---------------------------------------------------------------------------------------///