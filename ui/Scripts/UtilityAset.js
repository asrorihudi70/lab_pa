var newURL = location.href.substr(0, location.href.lastIndexOf("/") + 1);
var dsCompanyListUtilityAset;
var dsPrinterUtilityAsetBill;
var dsPrinterUtilityAsetKwitansi;
var dsPrinterUtilityAsetKasir;
var dsPrinterUtilityAsetLabel;

var dsPrinterUtilityAsetGelang;
var dsPrinterUtilityAsetGelangDewasaP;
var dsPrinterUtilityAsetGelangBayiL;
var dsPrinterUtilityAsetGelangBayiP;


var dsPrinterUtilityAsetStatusPasien;
var dsPrinterUtilityAsetKartuPasien;
var dsPrinterUtilityAsetSEP;
var dsPrinterUtilityAsetTracer;
var dsPrinterUtilityAsetTracerBaru;
var selectLanguageUtilityAset;
var mVarKdUserUtilityAset;
var dsKabupatenUtillity;
var dsKelurahanUtillityAsset;
var dsKecamatanUtillity;
var dsDetailUnitUtillity;
var IdUser;

var CompIDKelurahan;
var CompIDCustomer;
var CompIDApotek;
var CompIDObat;

var CompIDUnit;
var CompKDUnit;
var CompAutoPaid;

var CompUnitKelasLab;
var CompUnitKelasRad;
var CompUnitKelasOK;

var CompP_Bill;
var CompP_Kwitansi;
var CompP_Kasir;
var CompP_Label;

var CompP_Gelang;
var CompP_GelangDewasaP;
var CompP_GelangBayiL;
var CompP_GelangBayiP;

var CompP_StatusPasien;
var CompP_KartuPasien;
var CompP_SEP;
var CompCheck_Tracer;
var CompCheck_TracerBaru;
var CompP_Tracer;
var CompP_TracerBaru;

var dataSourceKepemilikan_UtilityAset;
var secondGridStore;

var KetLaboratorium = "";
var KetRadiologi 	= "";
var KetOK 			= "";

var first_dateSourceSelectedKamar_utillityAsset;
var second_dateSourceSelectedKamar_utillityAsset;
var Field_setup_kamar = [
		{name: 'NO_KAMAR', mapping : 'NO_KAMAR'},
		{name: 'NAMA_KAMAR', mapping : 'NAMA_KAMAR'}
	];

first_dateSourceSelectedKamar_utillityAsset = new WebApp.DataStore({fields: Field_setup_kamar});
second_dateSourceSelectedKamar_utillityAsset = new WebApp.DataStore({fields: Field_setup_kamar});

var FieldKelurahan       = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
dsKelurahanUtillityAsset = new WebApp.DataStore({fields: FieldKelurahan});

var FieldKelompok        = ['KD_CUSTOMER', 'CUSTOMER'];
ds_Customer_viDaftarUtillity = new WebApp.DataStore({fields: FieldKelompok});
/*

	PERBARUAN SETTING KONFIGURASI PER USER
	OLEH 	: HADAD
	TANGGAL : 2016 - 12 - 30
 */
			
getIdGetDataSetting();
function FormUtilityAset() 
{
	vWinFormEntryUtilityAset = new Ext.Window
    (
        {
			id: 'vWinFormEntryUtilityAset',
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 640,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			title: 'Pengaturan ',
			iconCls: 'Utility',
			modal: true,
			items:
            [
                GetPanelUtilityAset(),
			],
			listeners:
			{
				activate: function()
				{ 
				}
            }
        }
    );
    getIdGetDataSetting();
    vWinFormEntryUtilityAset.show();
};

function GetPanelUtilityAset() {
	getIdGetDataSetting();
	
	var Field_poli_viDaftar = ['KD_MILIK','MILIK'];
	dataSourceKepemilikan_UtilityAset = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	var Field_kepemilikan_utilityaset = ['KD_MILIK','MILIK'];
	secondGridStore = new WebApp.DataStore({fieldss: Field_kepemilikan_utilityaset});
	
	datarefreshKepemilikan_UtilityAset();
	datarefreshKepemilikanCurrent_UtilityAset();

	// Generic fields array to use in both store defs.
	var fields = [
		{name: 'KD_MILIK', mapping : 'KD_MILIK'},
		{name: 'MILIK', mapping : 'MILIK'}
	];
	
	var fieldss = [
		{name: 'KD_MILIK', mapping : 'KD_MILIK'},
		{name: 'MILIK', mapping : 'MILIK'}
	];


	// Column Model shortcut array
	var cols = [
		{ id : 'KD_MILIK', header: "Kode Unit", width: 160, sortable: true, dataIndex: 'KD_MILIK',hidden : true},
		{header: "Nama", width: 50, sortable: true, dataIndex: 'MILIK'}
	];

	// Column Model shortcut array
	var cols_kamar = [
		{ id : 'NO_KAMAR', header: "No Kamar", width: 160, sortable: true, dataIndex: 'NO_KAMAR',hidden : true},
		{header: "Nama Kamar", width: 50, sortable: true, dataIndex: 'NAMA_KAMAR'}
	];


	// declare the source Grid LIST KAMAR
	firstGrid_list_kamar = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup_list_kamar',
		store            : first_dateSourceSelectedKamar_utillityAsset,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 400,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Kamar',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
		(
			[
				new Ext.grid.RowNumberer(),
				{
					id: 'colNo_kamar',
					header: 'No',
					dataIndex: 'NO_KAMAR',
					sortable: true,
					hidden : true
				},{
					id: 'colNama_kamar',
					header: 'Kamar',
					dataIndex: 'NAMA_KAMAR',
					sortable: true,
					width: 50
				}
			]
		),
		listeners : {
			afterrender : function(comp) {
			var firstGridDropTargetEl = firstGrid_list_kamar.getView().scroller.dom;
			var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
				ddGroup    : 'firstGridDDGroup_list_kamar',
				notifyDrop : function(ddSource, e, data){
					var records =  ddSource.dragData.selections;
					// console.log(records);
					Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					firstGrid_list_kamar.store.add(records);
					firstGrid_list_kamar.store.sort('KD_UNIT', 'ASC');
					return true
				}
			});
			}
		},
		viewConfig: 
		{
			forceFit: true
		}
	});

	// create the destination Grid
	secondGrid_list_kamar = new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup_list_kamar',
		store            : second_dateSourceSelectedKamar_utillityAsset,
		columns          : cols_kamar,
		enableDragDrop   : true,
		height           : 400,
		stripeRows       : true,
		autoExpandColumn : 'NO_KAMAR',
		title            : 'Kamar dipilih',
		listeners : {
			afterrender : function(comp) {
				var secondGridDropTargetEl = secondGrid_list_kamar.getView().scroller.dom;
				var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup_list_kamar',
					notifyDrop : function(ddSource, e, data){
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							secondGrid_list_kamar.store.add(records);
							secondGrid_list_kamar.store.sort('KD_MILIK', 'ASC');
							return true
					}
				});
			}
		},
		viewConfig: 
		{
			forceFit: true
		}
	});

	// declare the source Grid
	firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : dataSourceKepemilikan_UtilityAset,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		trackMouseOver   : true,
		title            : 'Kepemilikan',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		colModel         : new Ext.grid.ColumnModel
		(
			[
				new Ext.grid.RowNumberer(),
				{
					id: 'colNRM_viDaftar',
					header: 'No.Medrec',
					dataIndex: 'KD_MILIK',
					sortable: true,
					hidden : true
				},
				{
					id: 'colNMPASIEN_viDaftar',
					header: 'Nama',
					dataIndex: 'MILIK',
					sortable: true,
					width: 50
				}
			]
		),
		listeners : {
			afterrender : function(comp) {
			var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
			var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
				ddGroup    : 'firstGridDDGroup',
				notifyDrop : function(ddSource, e, data){
					var records =  ddSource.dragData.selections;
					Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
					firstGrid.store.add(records);
					firstGrid.store.sort('KD_UNIT', 'ASC');
					return true
				}
			});
			}
		},
		viewConfig: 
		{
			forceFit: true
		}
	});

	secondGridStore = new Ext.data.JsonStore({
		fields : fieldss,
		root   : 'records'
	});

	// create the destination Grid
	secondGrid = new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		store            : secondGridStore,
		columns          : cols,
		enableDragDrop   : true,
		height           : 200,
		stripeRows       : true,
		autoExpandColumn : 'KD_MILIK',
		title            : 'Kepemilikan aktif',
		listeners : {
			afterrender : function(comp) {
				var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
				var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
							var records =  ddSource.dragData.selections;
							Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
							secondGrid.store.add(records);
							secondGrid.store.sort('KD_MILIK', 'ASC');
							return true
					}
				});
			}
		},
		viewConfig: 
		{
			forceFit: true
		}
	});
    var vTabPanelUtilityAset = new Ext.TabPanel({
		id: 'vTabPanelUtilityAset',
		region: 'center',
		margins: '5 5 5 5',
		bodyStyle: 'padding:10px 10px 10px 10px',
		activeTab: 0,
		plain: true,
		defferedRender: true,
		frame: false,
		border: true,
		height: 540,
		anchor: '100%',
		items:[
			{
				title: 'General',
				id: 'vTabPanelCompanyUtilityAset',
				items:[
					{
						columnWidth:1,
						layout: 'form',
						labelWidth:100,
						labelAlign:'right',
						border: false,
						items:[
							
							{
								labelAlign:'left',
								xtype: 'checkbox',
								boxLabel: 'Auto Paid ',
								name: 'cbAutoPaid',
								listeners:{
									change: {
										fn:function(a, b, c) {
											//alert();
											console.log(a.getValue());
											CompAutoPaid=a.getValue();
										}
									}
								},
								id: 'cbAutoPaid',
							},{
								xtype: 'label',
								text: 'Setting Daerah',
							},{
								xtype: 'box',
								autoEl : {
									tag : 'hr'
								}
							},
							mComboPropinsiUtillity(),
							mComboKabupatenUtillity(),
							mComboKecamatanUtillity(),
							mCombokelurahanUtilltity(),
							{
								xtype: 'label',
								text: 'Setting Costumer '
							},{
								xtype: 'box',
								autoEl : {
									tag : 'hr'
								}
							},
							mComboDefaultKelompokUtillity(),
							mComboDefaultCustomerUtillity(),
							// mComboDefaultApotekUtillity(),
							// mComboDefaultKepemilikanObat(),
							{
								xtype: 'label',
								text: 'Unit dan Kelas '
							},{
								xtype: 'box',
								autoEl : {
								tag : 'hr'
								}
							},
							new Ext.Panel({
								layout: {
									type: 'accordion',
									titleCollapse: true,
									multi: true,
									fill: false,
									animate: false, 
									flex: 1
								},
								height:200,
								autoScroll:true,
								id:'IDaccordionPanel',
								items: [
									mComboDefaultUnitKelasLab(),
									mComboDefaultUnitKelasRad(),
									mComboDefaultUnitKelasOK(),
								]
							}),
						]
					}
				],
				listeners:{
					activate: function()
					{
						// console.log("Setup kamar");
						Ext.getCmp('btnResetUnit_UtilityAsset').hide();
					}
				}
			},
			{
				title: 'Modul',
				id: 'vTabPanelCompanyUtilityAsetModul',
				items:[
					{
						columnWidth:1,
						layout: 'form',
						labelWidth:100,
						labelAlign:'right',
						border: false,
						items:[
							mComboUnitKerja(),
							mComboDetailUnit()
						]
					}
				],
				listeners:{
					activate: function()
					{
						// console.log("Setup kamar");
						Ext.getCmp('btnResetUnit_UtilityAsset').show();
					}
				}
			},{
				title: 'Printer',
				id: 'vTabPanelGeneralUtilityAset',
						autoScroll : true,
				items:[
					{
						xtype:'fieldset',
						labelAlign:'right',
						border:false,
						// anchor:  '99.99%',
						// height:'40px',
						items :[
							mComboPrinterBill(CompKDUnit),
							mComboPrinterKwitansi(CompKDUnit),
							mComboPrinterKasir(CompKDUnit),
							mComboPrinterLabel(CompKDUnit),
							mComboPrinterGelang(CompKDUnit),
							mComboPrinterGelangDewasaP(CompKDUnit),
							mComboPrinterGelangBayiL(CompKDUnit),
							mComboPrinterGelangBayiP(CompKDUnit),
							mComboPrinterStatusPasien(CompKDUnit),
							mComboPrinterKartuPasien(CompKDUnit),
							mComboPrinterSEP(CompKDUnit),
							mComboPrinterTracer(CompKDUnit),
							mComboPrinterTracerBaru(CompKDUnit),
							mCheckboxPrinterTracer(CompKDUnit),
							{
								xtype: 'textfield',
								id:'txtPrinterEtiket',
								fieldLabel: 'Printer Etiket',
								labelSeparator: ':' ,
								width:375
							},
							{
								xtype: 'textfield',
								id:'txtIPPrinterEtiket',
								fieldLabel: 'IP Printer Etiket',
								labelSeparator: ':' ,
								width:375
							}
						]
					}
				],
				listeners:{
					activate: function()
					{
						// console.log("Setup kamar");
						Ext.getCmp('btnResetUnit_UtilityAsset').hide();
					}
				}
			},
			{
				title: 'Apotek Farmasi',
				id: 'vTabPanelCompanyApotekFarmasi',
				items:
				[
					{
						columnWidth:1,
						layout: 'form',
						labelWidth:100,
						labelAlign:'right',
						border: false,
						items:
							[
								mComboDefaultApotekUtillity(),
								mComboDefaultKepemilikanObat(),
								{
									xtype: 'label',
									text: 'Lookup Kepemilikan Obat '
								},
								{
									xtype: 'box',
									autoEl : {
									tag : 'hr'
									}
								},	
								new Ext.Panel({
									id: 'form_summary_rwj',
									closable: true,
									region: 'center',
									layout: 'column',
									height       : 250,
									itemCls: 'blacklabel',
									bodyStyle: 'padding: 0px 0px 0px 0px',
									border: true,
									shadhow: true,
									margins: '0 5 5 0',
									anchor: '99%',
									items:
									[
										{
											columnWidth: .50,
											layout: 'form',
											border: false,
											autoScroll: true,
											bodyStyle: 'padding: 10px 10px 10px 10px',
											items:
											[
											firstGrid
											]
										},
										{
											columnWidth: .50,
											layout: 'form',
											bodyStyle: 'padding: 10px 10px 10px 10px',
											border: false,
											anchor: '97% 97%',
											items:
											[
											secondGrid
											]
										},
									]
									

								})
								
								
							]
						}
				],
				listeners:{
					activate: function()
					{
						// console.log("Setup kamar");
						Ext.getCmp('btnResetUnit_UtilityAsset').hide();
					}
				}
			},
			{
				title: 'Setup Kamar',
				id: 'vTabPanelSetupKamar',
				items:
				[
					{
						columnWidth:1,
						layout: 'form',
						labelWidth:100,
						labelAlign:'right',
						border: false,
						items:
							[
								{
									xtype: 'label',
									text: 'Pilih kamar '
								},{
									xtype: 'box',
									autoEl : {
										tag : 'hr'
									}
								},	
								new Ext.Panel({
									id: 'form_list_kamar',
									closable: true,
									region: 'center',
									layout: 'column',
									// height       : '100%',
									height       : '100%',
									itemCls: 'blacklabel',
									bodyStyle: 'padding: 0px 0px 0px 0px',
									border: true,
									shadhow: true,
									margins: '0 0 0 0',
									anchor: '100%',
									items:
									[
										{
											columnWidth: .50,
											layout: 'form',
											border: false,
											autoScroll: true,
											bodyStyle: 'padding: 10px 10px 10px 10px',
											items:
											[
												firstGrid_list_kamar,
											]
										},{
											columnWidth: .50,
											layout: 'form',
											bodyStyle: 'padding: 10px 10px 10px 10px',
											border: false,
											anchor: '97% 97%',
											items:
											[
												secondGrid_list_kamar
											]
										},
									]
								}),
								new Ext.Panel({
									id: 'form_operate_list_kamar',
									closable: true,
									region: 'center',
									layout: 'column',
									// height       : '100%',
									height       : '100%',
									itemCls: 'blacklabel',
									bodyStyle: 'padding: 0px 0px 0px 0px',
									border: true,
									shadhow: true,
									margins: '0 0 0 0',
									anchor: '100%',
									items:
									[
										{
											columnWidth: .50,
											layout: 'form',
											border: false,
											autoScroll: true,
											bodyStyle: 'padding: 10px 10px 10px 10px',
											items:
											[
												{
													xtype: 'checkbox',
													id: 'check_all_list_kamar',
													hideLabel:false,
													boxLabel: 'Pilih Semua Unit',
													checked: false,
													listeners: 
													{
														check: function()
														{
															if(Ext.getCmp('check_all_list_kamar').getValue()===true)
															{
																firstGrid_list_kamar.getSelectionModel().selectAll();
															}
															else
															{
																firstGrid_list_kamar.getSelectionModel().clearSelections();
															}
														}
													}
												}
											]
										},{
											columnWidth: .50,
											layout: 'form',
											bodyStyle: 'padding: 10px 10px 10px 10px',
											border: false,
											anchor: '97% 97%',
											items:
											[

												{
													id: 'ResetKamar',
													xtype: 'checkbox',
													hideLabel:false,
													boxLabel: 'Reset Unit',
													checked: false,
													listeners: 
													{
														check: function(){

															Ext.Ajax.request({
																url: baseURL + "index.php/setup/manageutillity/reset_kamar",
																params: { params : null },
																success: function(result){
																	var cst = Ext.decode(result.responseText);
																	if (cst.status == true){
																		secondGrid_list_kamar.getStore().removeAll();
																		loaddatastore_list_kamar_UtillityAsset();
																	}
																}
															});
															// secondGridStore.removeAll();
															// tmp_unit = "";
															// datarefresh_viInformasiUnitLapRWJPelayananDokter();
														}
													}
												}
											]
										},
									]
								}),
							]
						}
				],
				listeners:{
					activate: function(){
						secondGrid_list_kamar.getStore().removeAll();
						loaddatastore_list_kamar_UtillityAsset();
						datarefreshListKamarSelected_UtilityAset();
						Ext.getCmp('btnResetUnit_UtilityAsset').hide();
					}
				}
			},
		]
	});
	var FormUtilityAsset = new Ext.Panel({
		id: 'FormUtilityAsset',
		region: 'center',
		layout: 'form',
		title: '',
		anchor:'100%',
		bodyStyle: 'padding:7px 7px 7px 7px',
		border: true,
		height: 640,//392,
		shadhow: true,
		items:[
			vTabPanelUtilityAset,
			getItemPanelUtilityAset(),
		]
	});
	return FormUtilityAsset;
}

function getItemPanelUtilityAset() {
    var items ={
	    layout: 'column',
	    border: false,
		height:33,
		anchor:'100%',
	    items:[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig:{
					align: 'middle',
					pack:'end'
				},
				items:[
					{
						xtype:'button',
						text:'Reset Unit',
						width:70,
						hidden : true,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnResetUnit_UtilityAsset',
						handler:function(){
							Ext.MessageBox.confirm('Setting default', 'Anda yakin mensetting secara default ?', function(btn){
								if(btn === 'yes'){
									Ext.Ajax.request({
										url: baseURL + "index.php/setup/manageutillity/reset_unit",
										params: { params : null },
										success: function(result){
											var cst = Ext.decode(result.responseText);
											if (cst.status == true){
												vWinFormEntryUtilityAset.close();
												ShowPesanInfoUtilityAset("Data berhasil disimpan", "Success");
											}else{				
												ShowPesanErrorUtilityAset('Error simpan SQL', "Failed");
											}
										}
									});
								}
							});
						}
					},{
						xtype:'button',
						text:'Ok',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkUtilityAset',
						handler:function(){
							if(ValidasiForm() == 1){
								DataUpdate("update");
							}
						}
					},{
						xtype:'button',
						text:'Close' ,
						width:70,
						hideLabel:true,
						id: 'btnCancelUtilityAset',
						handler:function(){
							vWinFormEntryUtilityAset.close();
						}
					},/*{
						xtype:'button',
						text:'Default' ,
						width:70,
						hideLabel:true,
						id: 'btnDefaultUtilityAset',
						handler:function(){
							Ext.MessageBox.confirm('Setting default', 'Anda yakin mensetting secara default ?', function(btn){
								if(btn === 'yes'){
									DataUpdate("default");
								}
							});
						}
					}*/
				]
			}
		]
	};
    return items;
}
function ShowPesanWarningUtilityAset(str, modul) {
    Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.WARNING,
		width:250
	});
}
function DataUpdate(target){	
	Ext.Ajax.request({
		url: baseURL + "index.php/setup/manageutillity/konfigurasiuser",
		params: getParamCompanyUtilityAset(target),
		success: function(result){
			var cst = Ext.decode(result.responseText);
			if (cst.result == true){
				vWinFormEntryUtilityAset.close();
				ShowPesanInfoUtilityAset("Data berhasil disimpan", "Success");
			}else{				
				ShowPesanErrorUtilityAset('Error simpan SQL', "Failed");
			}
		}
	});
}

function ValidasiForm(){
	
	var x=1;
	/*
	if(Ext.getCmp('cbokelurahanUtillity').getValue() === undefined ||
	Ext.getCmp('cbokelurahanUtillity').getValue() === '' || 
	Ext.getCmp('cbokelurahanUtillity').getValue() === 'Pilih Kelurahan...' 
	){
		x=0;
		ShowPesanInfoUtilityAset("Form kelurahan tidak boleh kosong", "Information");
	}
	if(Ext.getCmp('IDComboKepemilikanObat').getValue() === undefined || Ext.getCmp('IDComboKepemilikanObat').getValue() === '' || 
	Ext.getCmp('IDComboKepemilikanObat').getValue() === 'Silahkan Pilih...'
	){
		Ext.getCmp('IDComboKepemilikanObat').setValue(0);
	}
	if (Ext.getCmp('cbAutoPaid').getValue() === true){
		CompAutoPaid = 1;
	} else if (Ext.getCmp('cbAutoPaid').getValue() === false){
		CompAutoPaid = 0;
	}
	*/
	return x;
};

function getParamCompanyUtilityAset(target) {
    var params ="";
	// console.log(CompIDUnit);
	// var sendDataArray = [];
	// secondGridStore.each(function(record){
		// var recordArray = [record.get("KD_MILIK")];
		// sendDataArray.push(recordArray);
	// });
    if (target == "update" || target!="default") {
	    params ={	
			VarIDKelurahan 	: CompIDKelurahan, //ID Kelurahan
			VarIDCustomer 	: CompIDCustomer, // ID Customer
			VarIDApotek 	: CompIDApotek, // ID Apotek
			VarIDObat 		: CompIDObat, // ID Kepemilikan Obat
			varIDUser 		: IdUser, //ID User 
			VarKDUnit 		: CompIDUnit, //KD Unit 
			VarAutoPaid		: CompAutoPaid, //Auto Paid 
			VarUnitKlasLab	: CompUnitKelasLab, //Unit Klas produk Lab
			VarUnitKlasRad	: CompUnitKelasRad, //Unit Klas produk Rad
			VarUnitKlasOK	: CompUnitKelasOK, //Unit Klas produk OK
			VarP_Bill		: CompP_Bill, //Print Bill
			VarP_Kwitansi	: CompP_Kwitansi, //Print Kwiransi
			VarP_Kasir		: CompP_Kasir, //Print Kasir
			VarP_Label		: CompP_Label, //Print Label
			
			VarP_Gelang		: CompP_Gelang, //Print Gelang
			VarP_GelangDewasaP		: CompP_GelangDewasaP, //Print Gelang
			VarP_GelangBayiL		: CompP_GelangBayiL, //Print Gelang
			VarP_GelangBayiP		: CompP_GelangBayiP, //Print Gelang
			
			VarP_StatusPasien		: CompP_StatusPasien, //Print Status Pasien
			VarP_KartuPasien		: CompP_KartuPasien, //Print Kartu Pasien
			VarP_SEP				: CompP_SEP, //Print Kartu Pasien
			VarP_Tracer				: CompP_Tracer, //Print Kartu Pasien
			VarPrinter_Tracer		: CompCheck_Tracer, //Print Kartu Pasien
			VarPrinter_Tracer_Baru		: CompCheck_TracerBaru, //Print Kartu Pasien
			varUnitKerja 			: Ext.getCmp('IDcboPilihanUnitKerjaUtillityAset').getValue(),
			var_printer_etiket: Ext.getCmp('txtPrinterEtiket').getValue(),
			var_ip_printer_etiket: Ext.getCmp('txtIPPrinterEtiket').getValue()
		};
		var tmpArr=[];
		var tmpArrKonsul=[];
		var gridStoreUtilityAset=Ext.getCmp('UtilityAssetgridunit').getStore().data.items;
		for(var i=0,iLen=gridStoreUtilityAset.length; i<iLen;i++){
			// console.log(gridStoreUtilityAset[i].data.KONSUL);
			if (gridStoreUtilityAset[i].data.KONSUL == true) {
				tmpArrKonsul.push(gridStoreUtilityAset[i].data.KD_UNIT);
			}
			if(gridStoreUtilityAset[i].data.SELECT==true){
				tmpArr.push(gridStoreUtilityAset[i].data.KD_UNIT);
			}
		}
		params['list[]']=tmpArr;
		params['listKonsul[]']=tmpArrKonsul;
		params['jumlah_milik']=secondGridStore.getCount();
		var tmpKepemilikan=[];
		for(var i=0,iLen=secondGridStore.getCount(); i<iLen;i++){
			tmpKepemilikan.push(secondGridStore.data.items[i].data.MILIK);
		}

		var tmpKamar=[];
		for(var i=0,iLen=second_dateSourceSelectedKamar_utillityAsset.getCount(); i<iLen;i++){
			tmpKamar.push(second_dateSourceSelectedKamar_utillityAsset.data.items[i].data.NO_KAMAR);
		}

		// console.log(tmpKamar);
		params['list_kamar[]']=tmpKamar;
		params['list_milik[]']=tmpKepemilikan;
	}else{
	    params ={	
			VarIDKelurahan 	: 0, //ID Kelurahan
			VarIDCustomer 	: "", // ID Customer
			VarIDApotek 	: "", // ID Apotek
			VarIDObat 		: 0, // ID Kepemilikan Obat
			varIDUser 		: IdUser, //ID User 
			VarKDUnit 		: "", //KD Unit 
			VarAutoPaid		: 0, //Auto Paid 
			VarUnitKlasLab	: "", //Unit Klas produk Lab
			VarUnitKlasRad	: "", //Unit Klas produk Rad
			VarUnitKlasOK	: "", //Unit Klas produk OK
			VarP_Bill		: "", //Print Bill
			VarP_Kwitansi	: "", //Print Kwitansi
			VarP_Kasir		: "", //Print Kasir
			VarP_Label		: "", //Print Label
			
			VarP_Gelang		: "", //Print Gelang
			VarP_GelangDewasaP		: "", //Print Gelang
			VarP_GelangBayiL		: "", //Print Gelang
			VarP_GelangBayiP		: "", //Print Gelang
			
			VarP_StatusPasien		: "", //Print Status Pasien
			VarP_KartuPasien		: "", //Print Kartu Pasien
			VarP_SEP		: "", //Print Kartu Pasien
			VarP_Tracer		: false, //Print Kartu Pasien
			varKepemilikan	: ""
		};
		params['list']=[];
	}
    return params
}

function clearObjectInputUtillity(){
	Ext.getCmp('IDDetailUnitUtillity').setValue("");
	Ext.getCmp('cbAutoPaid').setValue(false);
	Ext.getCmp('cboPropinsiUtillity').setValue("");
	Ext.getCmp('cboKabupatenUtillity').setValue("");
	Ext.getCmp('cboKecamatanUtillity').setValue("");
	Ext.getCmp('cbokelurahanUtillity').setValue("");
	Ext.getCmp('mComboDefaultCustomerUtillity').setValue("");
	Ext.getCmp('mComboDefaultApotekUtillity').setValue("");
	Ext.getCmp('IDComboKepemilikanObat').setValue("");
	Ext.getCmp('pickerDefaultUnitKelasLab').setTitle('Laboratorium ()');
	Ext.getCmp('pickerDefaultUnitKelasRad').setTitle('Radiologi ()');
	Ext.getCmp('pickerDefaultUnitKelasOK').setTitle('Kamar Operasi ()');
}

function ShowPesanWarningUtilityAset(str,modul){
	Ext.MessageBox.show({
	   title: modul,
	   msg:str,
	   buttons: Ext.MessageBox.OK,
	   icon: Ext.MessageBox.WARNING,
	   width :300
	});
}
function ShowPesanErrorUtilityAset(str,modul){
	Ext.MessageBox.show({
	   title: modul,
	   msg:str,
	   buttons: Ext.MessageBox.OK,
	   icon: Ext.MessageBox.ERROR,
	   width :300
	});
}

function ShowPesanInfoUtilityAset(str,modul){
	Ext.MessageBox.show({
	   title: modul,
	   msg:str,
	   buttons: Ext.MessageBox.OK,
	   icon: Ext.MessageBox.INFO,
	   width :300
	});
}

// =====================================================================================================================================================================
// =================================================================== SETTING PRINTERAN  =================================================
// =====================================================================================================================================================================
function loadPrinterBill(kd_unit,bagian)
{
	dsPrinterUtilityAsetBill.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			// param: "group_id in ('"+kd_unit+"', '4' , '5') AND bagian='" + bagian +"'"
			param: "group_id in ('"+kd_unit+"', '4' , '5') "
		}
	});
	return dsPrinterUtilityAsetBill;
}
function mComboPrinterBill(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetBill = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterBill',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Bill',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetBill,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c) {
				CompP_Bill = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function loadPrinterKwitansi(kd_unit,bagian)
{
	dsPrinterUtilityAsetKwitansi.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id in ('"+kd_unit+"', '4' , '5')"
		}
	});
	return dsPrinterUtilityAsetKwitansi;
}
function mComboPrinterKwitansi(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetKwitansi = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterKwitansi',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Kwitansi',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetKwitansi,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c) {
				CompP_Kwitansi = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function loadPrinterKasir(kd_unit)
{
	dsPrinterUtilityAsetKasir.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			// param: "group_id='"+kd_unit+"' AND bagian='2'"
			param: "group_id='"+kd_unit+"' "
		}
	});
	return dsPrinterUtilityAsetKasir;
}
function mComboPrinterKasir(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetKasir = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterKasir',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Kasir',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetKasir,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_Kasir = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
};
function loadPrinterLabel(kd_unit)
{
	dsPrinterUtilityAsetLabel.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			// param: "group_id='"+kd_unit+"' AND bagian='1'"
			param: "group_id='"+kd_unit+"'"
		}
	});
	
	return dsPrinterUtilityAsetLabel;
}
function mComboPrinterLabel(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetLabel = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterLabel',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Label',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetLabel,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_Label = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
};
function loadPrinterGelang(kd_unit)
{
	dsPrinterUtilityAsetGelang.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='1'"
		}
	});
	return dsPrinterUtilityAsetGelang;
}
function loadPrinterGelangDewasaP(kd_unit)
{
	dsPrinterUtilityAsetGelangDewasaP.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='1'"
		}
	});
	return dsPrinterUtilityAsetGelangDewasaP;
}
function loadPrinterGelangBayiL(kd_unit){
	dsPrinterUtilityAsetGelangBayiL.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='1'"
		}
	});
	return dsPrinterUtilityAsetGelangBayiL;
}

function loadPrinterGelangBayiP(kd_unit){
	dsPrinterUtilityAsetGelangBayiP.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='1'"
		}
	});
	return dsPrinterUtilityAsetGelangBayiP;
}
function mComboPrinterGelang(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetGelang = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterGelang',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Gelang Pria Dewasa',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetGelang,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_Gelang = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function mComboPrinterGelangBayiL(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetGelangBayiL = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterGelangBayiL',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Gelang Bayi Laki-laki',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetGelangBayiL,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_GelangBayiL = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function mComboPrinterGelangBayiP(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetGelangBayiP = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterGelangBayiP',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Gelang Bayi Perempuan',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetGelangBayiP,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_GelangBayiP = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function mComboPrinterGelangDewasaP(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetGelangDewasaP = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterGelangDewasaP',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Gelang Wanita Dewasa',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetGelangDewasaP,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_GelangDewasaP = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function loadPrinterStatusPasien(kd_unit)
{
	dsPrinterUtilityAsetStatusPasien.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='0'"
		}
	});
	return dsPrinterUtilityAsetStatusPasien;
}
function mComboPrinterStatusPasien(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetStatusPasien = new WebApp.DataStore({ fields: Field });	
    
	
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterStatusPasien',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Status Pasien',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetStatusPasien,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c) {
				CompP_StatusPasien = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
};
//mCheckboxPrinterTracer
function mCheckboxPrinterTracer(kd_unit) {
    var cbkPrinterUtilityAset = new Ext.form.Checkbox({
		id: 'cbkPrinterTracer',
		fieldLabel: 'Tracer',
		anchor:'100%',
		listeners:{
			change: {
				fn:function(a, b, c) {
					CompP_Tracer=a.getValue();
				}
			}
		}
	});
    return cbkPrinterUtilityAset;
};
function loadPrinterKartuPasien(kd_unit)
{
	dsPrinterUtilityAsetKartuPasien.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='0'"
		}
	});
	return dsPrinterUtilityAsetKartuPasien;
}
function mComboPrinterKartuPasien(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetKartuPasien = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterKartuPasien',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Kartu Pasien',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetKartuPasien,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_KartuPasien = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function loadPrinterSEP(kd_unit)
{
	dsPrinterUtilityAsetSEP.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='0'"
		}
	});
	return dsPrinterUtilityAsetSEP
}
function mComboPrinterSEP(kd_unit) {
	var Field = ['NAMA','LOKASI'];
    dsPrinterUtilityAsetSEP = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterSEP',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer SEP',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetSEP,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				CompP_SEP = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function loadPrinterTracer(kd_unit)
{
	dsPrinterUtilityAsetTracer.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='0'"
		}
	});
	return dsPrinterUtilityAsetTracer;
}
function loadPrinterTracerBaru(kd_unit)
{
	dsPrinterUtilityAsetTracerBaru.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA',
			Sortdir: 'ASC',
			target: 'viewComboPrinter',
			param: "group_id='"+kd_unit+"' AND bagian='0'"
		}
	});
	return dsPrinterUtilityAsetTracerBaru;
}
function mComboPrinterTracerBaru(kd_unit) {

	var Field = ['NAMA','LOKASI'];
   	dsPrinterUtilityAsetTracerBaru = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterTracerBaru',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Tracer Baru',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetTracerBaru,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				// console.log(b);
				CompCheck_TracerBaru = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
function mComboPrinterTracer(kd_unit) {

	var Field = ['NAMA','LOKASI'];
   	dsPrinterUtilityAsetTracer = new WebApp.DataStore({ fields: Field });	
    
    var cboPrinterUtilityAset = new Ext.form.ComboBox({
		id: 'cboPrinterTracer',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel: 'Printer Tracer Lama',
		align: 'Right',
		anchor:'100%',
		store: dsPrinterUtilityAsetTracer,
		valueField: 'LOKASI',
		displayField: 'NAMA',
		listeners:{
			'select': function(a, b, c){
				// console.log(b);
				CompCheck_Tracer = b.data.NAMA;
			}
		}
	});
    return cboPrinterUtilityAset;
}
// =====================================================================================================================================================================
// =================================================================== SETTING LAIN - LAIN  =================================================
// =====================================================================================================================================================================

function mComboDefaultCustomerUtillity(){
    ds_Customer_viDaftarUtillity.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewComboCostumer',
			param: " ORDER BY customer.customer ASC"
		}
	});
    var mComboDefaultCustomerUtillity = new Ext.form.ComboBox({
		id:'IDComboDefaultCustomer',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		forceSelection: true,
		emptyText:'Silahkan Pilih...',
		fieldLabel: 'Customer',
		anchor: '99%',
		store: ds_Customer_viDaftarUtillity,
		valueField: 'KD_CUSTOMER',
		displayField: 'CUSTOMER',
		listeners:{
			'select': function(a,b,c){
				CompIDCustomer = b.data.KD_CUSTOMER;
			}
		}
	});
	return mComboDefaultCustomerUtillity;
};

function mComboDefaultKelompokUtillity(){
    var mComboDefaultKelompokUtillity = new Ext.form.ComboBox({
		id:'IDComboDefaultKelompok',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		forceSelection: true,
		emptyText:'Silahkan Pilih...',
		fieldLabel: 'Kelompok',
		anchor: '99%',
		store: new Ext.data.ArrayStore
		(
			{
				id: 0,
				fields:
					[
					'Id',
					'displayText'
					],
				data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
			}
		),
		valueField: 'Id',
		displayField: 'displayText',
		listeners:{
			'select': function(a,b,c){
				Ext.getCmp('IDComboDefaultCustomer').setValue("");
				var jeniscusUtilltiy;
				if(b.data.displayText == 'Perseorangan')
				{
					jeniscusIgd='0';
				}
				else if(b.data.displayText == 'Perusahaan')
				{
					jeniscusIgd='1';
				}
				else if(b.data.displayText == 'Asuransi')
				{
					jeniscusIgd='2';
				}
				utillity_RefreshDatacombo(jeniscusIgd);    
			}
		}
	});
	return mComboDefaultKelompokUtillity;
};

function utillity_RefreshDatacombo(jeniscus)  // show combo
{

    ds_Customer_viDaftarUtillity.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    );
	
   // rowSelectedKasirIGD = undefined;
    return ds_Customer_viDaftarUtillity;
};

function mComboDefaultApotekUtillity(){
	var Field        = ['KD_UNIT_FAR', 'NM_UNIT_FAR'];
	ds_Apotek_viDaftar = new WebApp.DataStore({fields: Field});
    ds_Apotek_viDaftar.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nm_unit_far',
			Sortdir: 'ASC',
			target: 'ComboUnitApotekUtillity',
			param: ''
		}
	});
    var mComboDefaultApotekUtillity = new Ext.form.ComboBox({
		id:'IDComboDefaultApotek',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		forceSelection: true,
		emptyText:'Silahkan Pilih...',
		fieldLabel: 'Apotek',
		anchor: '99%',
		store: ds_Apotek_viDaftar,
		valueField: 'KD_UNIT_FAR',
		displayField: 'NM_UNIT_FAR',
		listeners:{
			'select': function(a,b,c){
				CompIDApotek = b.data.KD_UNIT_FAR;
			}
		}
	});
	return mComboDefaultApotekUtillity;
};

function mComboDefaultKepemilikanObat(){
	var Field        			= ['KD_MILIK', 'MILIK'];
	ds_KepemilikanObat_viDaftar = new WebApp.DataStore({fields: Field});
    ds_KepemilikanObat_viDaftar.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nm_unit_far',
			Sortdir: 'ASC',
			target: 'ComboUnitApotekUtillityPemilikObat',
			param: ''
		}
	});
    var mComboDefaultKepemilikanObat = new Ext.form.ComboBox({
		id:'IDComboKepemilikanObat',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		forceSelection: true,
		emptyText:'Silahkan Pilih...',
		fieldLabel: 'Kepemilikan Obat',
		anchor: '99%',
		store: ds_KepemilikanObat_viDaftar,
		valueField: 'KD_MILIK',
		displayField: 'MILIK',
		listeners:{
			'select': function(a,b,c){
				CompIDObat = b.data.KD_MILIK;
			}
		}
	});
	return mComboDefaultKepemilikanObat;
};

// =====================================================================================================================================================================
// =================================================================== UNIT DAN KELAS LABORATORIUM, RADIOLOGI DAN KAMAR OPERASI  =================================================
// =====================================================================================================================================================================

function mComboDefaultUnitKelasLab(){
	var Tree = Ext.tree;
	var tree = new Tree.TreePanel({
		id:'pickerDefaultUnitKelasLab',
		xtype : 'mytreegrid',
		cls : 'x-treegrid',
		title:'Laboratorium ('+KetLaboratorium+')', 
		animate:true, 
		useArrows:true,
		autoScroll:true,
		//loader: NavTreeLoader, 
		loader: new Ext.tree.TreeLoader({
			url: baseURL + 'index.php/general/daftarbarang/initApp/71', 
			requestMethod: 'GET',
			preloadChildren: true,
		}),
		enableDD:true,
		containerScroll: true,
		border: false,
		anchor:'100%',
        rootVisible: false,
		height: 150,
		dropConfig: {appendOnly:true},
		listeners: {
            'click': function(n)
            {
				//console.log(n.leaf);
				if (n.leaf == true) {
					var grid = Ext.getCmp('pickerDefaultUnitKelasLab');
					grid.setTitle('Laboratorium ('+n.text+')');
					CompUnitKelasLab = n.id;
                }
            }
        },
	});
	
	new Tree.TreeSorter(tree, {folderSort:true});
	
	// set the root node
	var root = new Tree.AsyncTreeNode({
		text: 'Ext JS', 
		odeType: 'async',
		draggable:false, 
		expanded: true,
	});
	tree.setRootNode(root);
	
	root.expand(false, false);
	return tree;
};

function mComboDefaultUnitKelasRad()
{
	var Tree = Ext.tree;
	var tree = new Tree.TreePanel({
		xtype : 'mytreegrid',
		cls : 'x-treegrid',
		id:'pickerDefaultUnitKelasRad',
		title:'Radiologi ('+KetLaboratorium+')',
		animate:true, 
		useArrows:true,
		autoScroll:true,
		loader: new Ext.tree.TreeLoader({
			url: baseURL + 'index.php/general/daftarbarang/initApp/72', 
			requestMethod: 'GET',
			preloadChildren: true,
		}),
		enableDD:true,
		containerScroll: true,
		border: false,
		anchor:'99%',
		rootVisible: false,
		height: 150,
		dropConfig: {appendOnly:true},
		listeners: {
			'click': function(n)
			{
				//console.log(n.id);
				if (n.leaf == true) {
					var grid = Ext.getCmp('pickerDefaultUnitKelasRad');
					grid.setTitle('Radiologi ('+n.text+')');
					CompUnitKelasRad = n.id;
				}
			}
		},
	});
	
	new Tree.TreeSorter(tree, {folderSort:true});
	
	var root = new Tree.AsyncTreeNode({
		text: 'Ext JS', 
		odeType: 'async',
		draggable:false, 
		expanded: true,
	});
	tree.setRootNode(root);
	
	root.expand(false, false);
	return tree;
};

function mComboDefaultUnitKelasOK()
{
	var Tree = Ext.tree;
	var tree = new Tree.TreePanel({
		xtype: 'treepicker',
		id:'pickerDefaultUnitKelasOK',
		title:'Kamar Operasi ('+KetOK+')', 
		animate:true, 
		useArrows:true,
		autoScroll:true,
		loader: new Ext.tree.TreeLoader({
			url: baseURL + 'index.php/general/daftarbarang/initApp/73', 
			requestMethod: 'GET',
			preloadChildren: true,
		}),
		enableDD:true,
		containerScroll: true,
		border: false,
		anchor:'99%',
		rootVisible: false,
		height: 150,
		listeners: {
			'click': function(n)
			{
				//console.log(n.id);
				if (n.leaf == true) {
					var grid = Ext.getCmp('pickerDefaultUnitKelasOK');
					grid.setTitle('Kamar Operasi ('+n.text+')');
					CompUnitKelasOK= n.id;
				}
			}
		},
	});
	
	new Tree.TreeSorter(tree, {folderSort:true});
	
	var root = new Tree.AsyncTreeNode({
		text: 'Ext JS', 
		odeType: 'async',
		draggable:false, 
		expanded: true,
	});
	tree.setRootNode(root);
	
	root.expand(false, false);
	return tree;
};

function mComboUnitKerja()
{
    var cboPilihanUnitKerja = new Ext.form.ComboBox
	(
            {
				id:'IDcboPilihanUnitKerjaUtillityAset',
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				selectOnFocus:true,
				forceSelection: true,
				emptyText:'Silahkan Pilih...',
				fieldLabel: 'Unit Kerja',
				anchor: '99%',
				value:1,
                store: new Ext.data.ArrayStore
                (
                    {
                            id: 0,
                            fields:
                            [
                                    'Id',
                                    'displayText'
                            ],
                    data: [[1, 'RWI'], [2, 'RWJ'], [3, 'IGD'], [4, 'LABORATORIUM'], [5, 'RADIOLOGI'], [71, 'KAMAR OPERASI'], [6, 'FARMASI'], [7, 'PENUNJANG']]
                    }
                ),
                valueField: 'Id',
                displayField: 'displayText',
                listeners:
                {
					'select': function(a,b,c)
					{
						// Ext.getCmp('UtilityAssetgridunit');
						loaddatastoreDetailUnit(b.data.Id);

						//Ext.getCmp('IDDetailUnitUtillity').setValue("");
					}
				}
            }
	);
	return cboPilihanUnitKerja;
};

function mComboDetailUnit(){
	var Field           = ['KD_UNIT', 'NAMA_UNIT', 'SELECT'];
	dsDetailUnitUtillity= new WebApp.DataStore({fields: Field});
	/* var chkPilihUnitKerja = new Ext.grid.CheckColumn
		(
			{
				
				id: 'chkPilihUnitKerja',
				header: 'Pilih',
				align: 'center',
				//disabled:false,
				sortable: true,
				dataIndex: 'SELECT',
				anchor: '10% 100%',
				width: 30,
				listeners: 
				{
					checkchange: function()
					{
						alert('hai');
					}
				}/* ,
				renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
							alert('hai');
                            return true;
							
                            break;
                        case 'f':
							alert('fei');
                            return false;
                            break;
                    }
                    return false;
                }  *
		
			}
		);  */
	var gridData = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		title: '',
		id:'UtilityAssetgridunit',
		store: dsDetailUnitUtillity,
		autoScroll: true,
		height:465,
		colModel: new Ext.grid.ColumnModel([
			{
				dataIndex: 'KD_UNIT',
				width: 80
			},{
				header: 'Unit',
				dataIndex: 'NAMA_UNIT',
				width: 200,
			},
			//chkPilihUnitKerja
			{
				header: 'Pilih',
				dataIndex: 'SELECT',
				xtype:'checkcolumn',
				width: 50,
				editor:{
					xtype:'checkbox'
				}
			},
			{
				header: 'Konsultasi',
				dataIndex: 'KONSUL',
				xtype:'checkcolumn',
				width: 50,
				editor:{
					xtype:'checkbox',
				}
			}
		])
	});
	return gridData;
};


// =====================================================================================================================================================================
// =================================================================== COMBO BOX DAERAH  =================================================
// =====================================================================================================================================================================

function mComboPropinsiUtillity()
{
    var Field = ['KD_PROPINSI', 'PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				//Sort: 'DEPT_ID',
				Sort: 'propinsi',
				Sortdir: 'ASC',
				target: 'ViewComboPropinsi',
				param: ''
			}
		}
	);

    var cboPropinsiUtillity = new Ext.form.ComboBox
	(
		{
			id: 'cboPropinsiUtillity',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			forceSelection: true,
			emptyText: 'Pilih  Propinsi...',
			selectOnFocus: true,
			fieldLabel: 'Propinsi',
			align: 'Right',
			store: dsPropinsiRequestEntry,
			valueField: 'KD_PROPINSI',
			displayField: 'PROPINSI',
			anchor: '99%',
			tabIndex: 24,
			listeners:
				{
					'select': function (a, b, c)
					{
						selectPropinsiKtp = b.data.KD_PROPINSI;
						Ext.getCmp('cboKabupatenUtillity').setValue("");
						Ext.getCmp('cboKecamatanUtillity').setValue("");
						Ext.getCmp('cbokelurahanUtillity').setValue("");
						loaddatastoreKabupatenUtillityAsset(b.data.KD_PROPINSI);
					},
					'render': function (c) {
				}									
			}
		}
	);
    return cboPropinsiUtillity;
};

function mComboKabupatenUtillity()
{
	var Field           = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
	dsKabupatenUtillity = new WebApp.DataStore({fields: Field});

	var cboKabupatenUtillity = new Ext.form.ComboBox
	(
		{
			id: 'cboKabupatenUtillity',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih Kabupaten...',
			fieldLabel: 'Kab/Kod',
			align: 'Right',
			store: dsKabupatenUtillity,
			valueField: 'KD_KABUPATEN',
			displayField: 'KABUPATEN',
			tabIndex: 19,
			anchor: '99%',
			listeners:
			{
				'select': function (a, b, c)
				{
					selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
					Ext.getCmp('cboKecamatanUtillity').setValue("");
					Ext.getCmp('cbokelurahanUtillity').setValue("");
					loaddatastorekecamatanUtilltiyAsset(b.data.KD_KABUPATEN);
				},
				'render': function (c)
				{
				
				}				
			}
		}
	);
    return cboKabupatenUtillity;
};

function mComboKecamatanUtillity()
{
	var Field      = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
	dsKecamatanUtillity = new WebApp.DataStore({fields: Field});

	var cboKecamatanUtillity = new Ext.form.ComboBox
	(
		{
			id: 'cboKecamatanUtillity',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih Kecamatan...',
			fieldLabel: 'Kecamatan',
			align: 'Right',
			tabIndex: 26,
			store: dsKecamatanUtillity,
			valueField: 'KD_KECAMATAN',
			displayField: 'KECAMATAN',
			anchor: '99%',
			listeners:
			{
				'select': function (a, b, c)
				{
					selectKecamatanktp = b.data.KD_KECAMATAN;
					loaddatastorekelurahanUtillityAsset(b.data.KD_KECAMATAN);
					Ext.getCmp('cbokelurahanUtillity').setValue("");
				},
			}
		}
	);
	return cboKecamatanUtillity;
};

function mCombokelurahanUtilltity()
{

	var cbokelurahan = new Ext.form.ComboBox
	(
		{
			id: 'cbokelurahanUtillity',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih Kelurahan...',
			fieldLabel: 'Kelurahan',
			align: 'Right',
			tabIndex: 27,
			anchor: '99%',
			store: dsKelurahanUtillityAsset,
			valueField: 'KD_KELURAHAN',
			displayField: 'KELURAHAN',
			anchor: '99%',
			listeners:
			{
				'select': function (a, b, c)
				{
					CompIDKelurahan = b.data.KD_KELURAHAN;
				},
			}
		}
	);
	return cbokelurahan;
};

// =====================================================================================================================================================================
// =================================================================== LOAD DATA STORE  =================================================
// =====================================================================================================================================================================

function loaddatastoreKabupatenUtillityAsset(kd_propinsi)
{
	dsKabupatenUtillity.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				//Sort: 'DEPT_ID',
				Sort: 'kabupaten',
				Sortdir: 'ASC',
				target: 'ViewComboKabupaten',
				param: 'kd_propinsi=' + kd_propinsi
			}
		}
	)
};

function loaddatastorekecamatanUtilltiyAsset(kd_kabupaten)
{
	dsKecamatanUtillity.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'kecamatan',
				Sortdir: 'ASC',
				target: 'ViewComboKecamatan',
				param: 'kd_kabupaten=' + kd_kabupaten
			}
		}
	)
};

function loaddatastorekelurahanUtillityAsset(kd_kecamatan)
{
	dsKelurahanUtillityAsset.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: 'kelurahan',
				Sortdir: 'ASC',
				target: 'ViewComboKelurahan',
				param: 'kd_kecamatan=' + kd_kecamatan
			}
		}
	)
};

function loaddatastore_list_kamar_UtillityAsset()
{
	first_dateSourceSelectedKamar_utillityAsset.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: '',
				Sortdir: 'ASC',
				target: 'viewlistunitkamar',
				param: null
			}
		}
	)
};

function loaddatastoreDetailUnit(kd_unit){
	var params = "(parent='"+kd_unit+"') and type='0' ";
	if(kd_unit == '5'){
		params = "(kd_unit='"+kd_unit+"') and type='0' ";
	}
	dsDetailUnitUtillity.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: params
		},
		callback:function(){
			var gridStoreUtilityAset=Ext.getCmp('UtilityAssetgridunit').getStore().data.items;
				for(var i=0,iLen=gridStoreUtilityAset.length; i<iLen;i++){
					for(var j=0,jLen=CompIDUnit.length; j<jLen;j++){
						//console.log(CompIDUnit[j].replace("'","").replace("'","")+' = '+gridStoreUtilityAset[i].data.KD_UNIT);
						if(gridStoreUtilityAset[i].data.KD_UNIT==CompIDUnit[j].replace("'","").replace("'","")){
							gridStoreUtilityAset[i].data.SELECT = true;
							// console.log(CompIDUnit[j].replace("'","").replace("'",""));
						}
					}
				}
				console.log(Ext.getCmp('UtilityAssetgridunit'));
				for(var i=0,iLen=gridStoreUtilityAset.length; i<iLen;i++){
					if (CompIDKonsul != 'undefined' || CompIDKonsul != '') {
						for(var j=0,jLen=CompIDKonsul.length; j<jLen;j++){
							//console.log(CompIDUnit[j].replace("'","").replace("'","")+' = '+gridStoreUtilityAset[i].data.KD_UNIT);
							if(gridStoreUtilityAset[i].data.KD_UNIT==CompIDKonsul[j].replace("'","").replace("'","")){
								gridStoreUtilityAset[i].data.KONSUL = true;
							}
						}
					}
				}
			Ext.getCmp('UtilityAssetgridunit').getView().refresh();
		}
	});
};

function getIdGetDataSetting(){
	Ext.Ajax.request({
		url: baseURL + "index.php/setup/manageutillity/getDataSetting", 
		params: {
			UserId:0
		},
		success: function(response, opts) {
			var cst = Ext.decode(response.responseText);
			CompIDKelurahan = cst.kd_kelurahan;
			CompIDCustomer 	= cst.kd_customer;
			CompIDApotek 	= cst.kd_unit_far;
			CompIDObat 		= cst.kd_milik;
			CompIDUnit  	= cst.kd_child_unit;
			CompAutoPaid  	= cst.auto_paid;
			CompP_Bill  	= cst.p_bill;
			CompP_Kwitansi	= cst.p_kwitansi;
			CompP_Kasir		= cst.p_kasir;
			CompP_Label		= cst.p_label;
			
			CompP_Gelang	= cst.p_gelang;
			CompP_GelangDewasaP	= cst.p_gelang_dewasa_p;
			CompP_GelangBayiL	= cst.p_gelang_bayi_l;
			CompP_GelangBayiP	= cst.p_gelang_bayi_p;
			
			CompP_StatusPasien	= cst.p_statuspasien;
			CompP_KartuPasien	= cst.p_kartupasien;
			CompP_SEP		= cst.p_sep;
			if(cst.auto_paid=='1'){
				CompAutoPaid=true;
				Ext.getCmp("cbAutoPaid").setValue(true); 
			}else{
				CompAutoPaid=false;
				Ext.getCmp("cbAutoPaid").setValue(false); 
			}
			if(cst.p_tracer=='1'){
				CompP_Tracer= true;
				Ext.getCmp('cbkPrinterTracer').setValue(true);
			}else{
				CompP_Tracer=false;
				Ext.getCmp('cbkPrinterTracer').setValue(false);
			}
			var res      = cst.kd_child_unit.split(',');
			var konsul   = "";
			if (cst.konsultasi != null) {
				konsul = cst.konsultasi.split(',');

				if(konsul.length>0){
					//console.log(res[0]);
					tmpKonsultasi=konsul[0].substring(1,2);
				//	alert(res[0].substring(2,3));
				}
			}
			CompIDUnit   = res;
			CompIDKonsul = konsul;
			var a=1;
			var tmpKonsultasi=1;
			// console.log(res[0].substring(1,2));
			if(res.length>0){
				//console.log(res[0]);
				//alert(res[0].replace("'",'').replace("'",'').substring(0,1));
				a=res[0].replace("'",'').replace("'",'').substring(0,1);
			//	alert(res[0].substring(2,3));
			}
			console.log(a);
			//CompKDUnit='0'+a;
			loaddatastoreDetailUnit(a);
			// loaddatastoreDetailUnit(tmpKonsultasi);
			//console.log(a);
			/*
			var tmpArr=[];
			var gridStoreUtilityAset=Ext.getCmp('UtilityAssetgridunit').getStore().data.items;
			for(var i=0,iLen=gridStoreUtilityAset.length; i<iLen;i++){
				if(gridStoreUtilityAset[i].data.SELECT==true){
					tmpArr.push(gridStoreUtilityAset[i].data.KD_UNIT);
				}
			}
			params['list[]']=tmpArr;
			
			*/
			var bagian=0;
			if (a == '1' || a == 1) {
				CompKDUnit = "02";
				bagian='2';
			}
			else if (a == '2' || a == 2) {
				CompKDUnit = "01";
				bagian='2';
			}else if (a == '3' || a == 3) {
				CompKDUnit = "06";
				bagian='2';
			}else if (a == '4' || a == 4) {
				CompKDUnit = a;
				bagian='3';
			}else if (a == '6' || a == 6) {
				CompKDUnit = '05';
				bagian='3';
			}
			
			//************************** cek apakah ada unit rwj
			var ada_rwj=0;
			for(z= 0 ; z<res.length ;z++){
				var cek_z = res[z].replace("'",'').replace("'",'').substring(0,1);
				if(cek_z == '2' || cek_z == 2){
					ada_rwj = ada_rwj+1;
				}
			}
			console.log(ada_rwj);
			if(ada_rwj > 0){
				CompKDUnit = "01";
				bagian='2';
			}
			/*
			
				PERBARUAN MENAMPILKAN SETTING KONFIGURASI PER USER
				OLEH 	: HADAD
				TANGGAL : 2016 - 12 - 30

			 */
			loadPrinterBill(CompKDUnit,bagian);
			loadPrinterKwitansi(CompKDUnit,bagian);
			loadPrinterKasir(CompKDUnit);
			loadPrinterLabel(CompKDUnit);
			
			loadPrinterGelang(CompKDUnit);
			loadPrinterGelangDewasaP(CompKDUnit);
			loadPrinterGelangBayiL(CompKDUnit);
			loadPrinterGelangBayiP(CompKDUnit);
			
			loadPrinterStatusPasien(CompKDUnit);
			loadPrinterKartuPasien(CompKDUnit);
			loadPrinterSEP(CompKDUnit);
			loadPrinterTracer(CompKDUnit);
			loadPrinterTracerBaru(CompKDUnit);
			Ext.getCmp('cboPrinterStatusPasien').setValue(cst.p_statuspasien);
			Ext.getCmp('cboPrinterKartuPasien').setValue(cst.p_kartupasien);
			Ext.getCmp('cboPrinterSEP').setValue(cst.p_sep);
			Ext.getCmp('cboPrinterTracer').setValue(cst.p_cbo_tracer);
			Ext.getCmp('cboPrinterTracerBaru').setValue(cst.p_cbo_tracer_baru);
			Ext.getCmp('cboPrinterBill').setValue(cst.p_bill);
			Ext.getCmp('cboPrinterKwitansi').setValue(cst.p_kwitansi);
			
			Ext.getCmp('cboPrinterGelang').setValue(cst.p_gelang);
			Ext.getCmp('cboPrinterGelangDewasaP').setValue(cst.p_gelang_dewasa_p);
			Ext.getCmp('cboPrinterGelangBayiL').setValue(cst.p_gelang_bayi_l);
			Ext.getCmp('cboPrinterGelangBayiP').setValue(cst.p_gelang_bayi_p);
			
			Ext.getCmp('cboPrinterLabel').setValue(cst.p_label);
			Ext.getCmp('cboPrinterKasir').setValue(cst.p_kasir);
			Ext.getCmp('IDComboDefaultCustomer').setValue(cst.kd_customer);
			//Ext.getCmp('IDDetailUnitUtillity').setValue(cst.kd_child_unit);
			Ext.getCmp('IDComboDefaultApotek').setValue(cst.kd_unit_far);
			Ext.getCmp('IDComboKepemilikanObat').setValue(cst.kd_milik);

			//Ext.getCmp('IDcboPilihanUnitKerjaUtillityAset').setValue(cst.kd_parent_unit);
			Ext.getCmp('IDcboPilihanUnitKerjaUtillityAset').setValue(a);
			//loaddatastoreDetailUnit(cst.kd_parent_unit);

			Ext.getCmp('cboPropinsiUtillity').setValue(cst.kd_propinsi);
			loaddatastoreKabupatenUtillityAsset(cst.kd_propinsi);
			
			//Ext.getCmp('IDDetailUnitUtillity').setValue(cst.nama_unit);
			
			Ext.getCmp('cboKabupatenUtillity').setValue(cst.kabupaten);
			loaddatastorekecamatanUtilltiyAsset(cst.kd_kabupaten);

			Ext.getCmp('cboKecamatanUtillity').setValue(cst.kecamatan);
			loaddatastorekelurahanUtillityAsset(cst.kd_kecamatan);
			
			Ext.getCmp('cbokelurahanUtillity').setValue(cst.kelurahan);
			
			if (cst.jenis_cust == 0) {
				Ext.getCmp('IDComboDefaultKelompok').setValue("1");
			}else if(cst.jenis_cust == 1){
				Ext.getCmp('IDComboDefaultKelompok').setValue("2");
			}else{
				Ext.getCmp('IDComboDefaultKelompok').setValue("3");
			}


			Ext.getCmp('txtPrinterEtiket').setValue(cst.p_etiket);
			Ext.getCmp('txtIPPrinterEtiket').setValue(cst.ip_printer_etiket);
			if (cst.kd_klas_lab > 0 || cst.kd_klas_lab!=null) {
				CompUnitKelasLab= cst.kd_klas_lab;
			}
			if (cst.klasifikasi_lab > 0 || cst.klasifikasi_lab!=null) {
				KetLaboratorium	= cst.klasifikasi_lab;
				Ext.getCmp('pickerDefaultUnitKelasLab').setTitle('Laboratorium ('+KetLaboratorium+')');
			}

			if (cst.kd_klas_rad > 0 || cst.kd_klas_rad!=null) {
				CompUnitKelasRad= cst.kd_klas_rad;
			}
			if (cst.klasifikasi_rad > 0 || cst.klasifikasi_rad!=null) {
				KetRadiologi	= cst.klasifikasi_rad;
				Ext.getCmp('pickerDefaultUnitKelasRad').setTitle('Radiologi ('+KetRadiologi+')');
			}

			if (cst.kd_klas_ok > 0 || cst.kd_klas_ok!=null) {
				CompUnitKelasOK= cst.kd_klas_ok;
			}
			if (cst.klasifikasi_ok > 0 || cst.klasifikasi_ok!=null) {
				KetOK = cst.klasifikasi_ok;
				Ext.getCmp('pickerDefaultUnitKelasOK').setTitle('Kamar Operasi ('+KetOK+')');
			}
		},
		failure: function(response, opts) {
		}
	});
};


function datarefreshKepemilikan_UtilityAset()
{
    dataSourceKepemilikan_UtilityAset.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'MILIK',
                Sortdir: 'ASC',
                target:'Viewkepemilikanobat',
                param: ""
            }
        }
    )
}
function datarefreshKepemilikanCurrent_UtilityAset(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/general/viewkepemilikanobatcurrent/getMilik",
			params: {text:''},
			failure: function(o)
			{
				Ext.Msg.alert('Gagal','Hubungi admin!');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=secondGridStore.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					secondGridStore.add(recs);
					
					// secondGrid.getView().refresh();
				}
				else 
				{
					Ext.Msg.alert('Gagal','Gagal mebaca kepemilikan!');
				};
			}
		}
		
	)
	
}

function datarefreshListKamarSelected_UtilityAset(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/general/viewlistunitkamar/getkamar",
			params: {text:''},
			failure: function(o)
			{
				Ext.Msg.alert('Gagal','Hubungi admin!');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=second_dateSourceSelectedKamar_utillityAsset.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					second_dateSourceSelectedKamar_utillityAsset.add(recs);
					
					// secondGrid.getView().refresh();
				}
				else 
				{
					Ext.Msg.alert('Gagal','Gagal mebaca kepemilikan!');
				};
			}
		}
		
	)
	
}


///---------------------------------------------------------------------------------------///