﻿/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var dsCompanyListUtilityTenant;
var dsLanguageUtilityTenant;
var dsRowperPageUtilityTenant;
var dsBirthdayAlertUtilityTenant;
var selectLanguageUtilityTenant;
var mTabUtilityTenant=false;
var mVarKdUserUtilityTenant;

/** SETUP BAHASA */
var dsListKeySettingBahasaUtilityTenant;
var dsLanguageSettingBahasaUtilityTenant;
var selectLanguageSettingBahasaUtilityTenant;
var dsImportLanguageSettingBahasaUtilityTenant;
var selectImportLanguageSettingBahasaUtilityTenant;
var dsGroupLanguageSettingBahasaUtilityTenant;
var selectGroupLanguageSettingBahasaUtilityTenant='008';
var mVarDefaultValueGroupUtilityTenant='General';

/** AUTO NUMBERING */
var intAutoNumberingUtilityTenant;


function FormUtilityTenant() 
{
	RefreshDataUtilityTenant();
    vWinFormEntryUtilityTenant = new Ext.Window
	(
		{
			id: 'vWinFormEntryUtilityTenant',
			title: 'Utility Tenant',//nmTitleFormUtility,
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 462,//420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'Utility',
			modal: true,                                   
			items: 
			[				
				GetPanelUtilityTenant() 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
	GetBahasaUtilityTenant();
	GetBahasaSettingBahasaUtilityTenant();
    vWinFormEntryUtilityTenant.show();
};

function GetPanelUtilityTenant() 
{
    var vTabPanelUtilityTenant = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelUtilityTenant',
		    region: 'center',
		    margins: '5 5 5 5',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 388,
		    //width:742,
		    anchor: '100%',
		    items:
			[
				{
				    title: 'General', //nmTitleTabGenUtility,
				    id: 'vTabPanelGeneralUtilityTenant',
				    items:
					 [
						{
						    xtype: 'fieldset',
						    title: 'Language', //nmTitleFieldBahasaUtility,	
						    labelAlign: 'right',
						    anchor: '99.99%',
						    height: '40px',
						    items:
							[
								mComboBahasaUtilityTenant()
							]
						},
						{
						    xtype: 'fieldset',
						    title: 'Row per Page', //nmTitleFieldBahasaUtility,	
						    labelAlign: 'right',
						    anchor: '99.99%',
						    height: '40px',
						    items:
							[
								 {
								     xtype: 'textfield',
								     fieldLabel: 'Row per Page ', //nmIdCompUtility + ' ',
								     name: 'txtRowPerPage',
								     id: 'txtRowPerPage',
								     anchor: '40%'
								 }
							]
						},
						{
						    xtype: 'fieldset',
						    title: 'Birtday Alert', //nmTitleFieldBahasaUtility,	
						    labelAlign: 'right',
						    anchor: '99.99%',
						    height: '40px',
						    items:
							[
								 {
								     xtype: 'textfield',
								     fieldLabel: 'Birthday Alert ', //nmIdCompUtility + ' ',
								     name: 'txtBirthdayAlert',
								     id: 'txtBirthdayAlert',
								     anchor: '40%'
								 }
							]
						},
						{
						    xtype: 'fieldset',
						    title: 'ID Autonumbering', //nmTitleAutoNumbering,	
						    labelAlign: 'right',
						    anchor: '99.99%',
						    height: '40px',
						    items:
							[
								{
									xtype: 'checkbox',
									id: 'chkIDAutoNumbering',
									boxLabel: 'ID AutoNumbering',
									width: 200,
									handler: function(a,b) 
									{
										if(b===true)
										{
											intAutoNumberingUtilityTenant = 1;
										}
										else
										{
											intAutoNumberingUtilityTenant = 0;
										};		
										//ShowPesanInfoUtilityTenant(nmPesanAutoNumbering,nmTitleFormUtility);
									}
								}
							]
						}
					 ],
				    listeners:
					{
					    activate: function() {
					        mTabUtilityTenant = false;
					        initRowPerPageUtilityTenant();
					        initBirthdayAlertUtilityTenant();
							initAutoNumberingUtilityTenant();
					    }
					}
				},
				{
				    title: 'Company', //nmTitleTabCompUtility,
				    id: 'vTabPanelCompanyUtilityTenant',
				    items:
					 [
						{
						    columnWidth: 1,
						    layout: 'form',
						    labelWidth: 65,
						    labelAlign: 'right',
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'ID ', //nmIdCompUtility + ' ',
								    name: 'txtKode_SetCompany',
								    id: 'txtKode_SetCompany',
								    anchor: '40%',
								    readOnly: true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Company ', //nmNamaCompUtility + ' ',
								    name: 'txtNameSetCompany',
								    id: 'txtNameSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Address ', //nmAdresCompUtility + ' ',
								    name: 'txtAddressSetCompany',
								    id: 'txtAddressSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Phone ', //nmTelpCompUtility + ' ',
								    name: 'txtTlpSetCompany',
								    id: 'txtTlpSetCompany',
								    anchor: '60%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Fax ', //nmFaxCompUtility + ' ',
								    name: 'txtFaxSetCompany',
								    id: 'txtFaxSetCompany',
								    anchor: '60%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'City ',
								    name: 'txtCitySetCompany',
								    id: 'txtCitySetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'ZIP Code ', //nmPosCdCompUtility + ' ',
								    name: 'txtPosSetCompany',
								    id: 'txtPosSetCompany',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Email ', //nmEmailCompUtility + ' ',
								    name: 'txtEmailSetCompany',
								    id: 'txtEmailSetCompany',
								    anchor: '100%'
								}
							]
						}						
					 ],
				    listeners:
					{
					    activate: function() {
					        mTabUtilityTenant = true;
					        initCompanyUtilityTenant();
					    }
					}
				},
				GetPanelSettingBahasaUtilityTenant()
			]
		}
	)
	
	var FormUtilityTenant = new Ext.Panel  
	(
		{
			id: 'FormUtilityTenant',
			region: 'center',
			layout: 'form',
			title: '',
			anchor:'100%',
			bodyStyle: 'padding:7px 7px 7px 7px',
			border: true,
			height: 434,//392,
			shadhow: true,
			items: 
			[
				vTabPanelUtilityTenant,getItemPanelUtilityTenant() 
			]
        }
	);
	
	return FormUtilityTenant;
};

/** SETUP BAHASA */
function GetPanelSettingBahasaUtilityTenant() 
{
	var vTabPanelSettingBahasaUtilityTenant = new Ext.Panel
	(
		{
		    id: 'vTabPanelSettingBahasaUtilityTenant',
		    region: 'center',
			layout:'form',
		    border: false,
			labelAlign : 'right',
		    height: 388,
			labelWidth:140,
		    anchor: '100%',
		    items:
			[
				getItemPanelBahasaSettingBahasaUtilityTenant(),
				mComboImportBahasaSettingBahasaUtilityTenant(),
				grdLabelBahasaSettingBahasaUtilityTenant()				
			]
		}
	)
	
	var FormSettingBahasaUtilityTenant = new Ext.Panel  
	(
		{
			id: 'FormSettingBahasaUtilityTenant',
			region: 'center',
			layout: 'form',
			title: 'Language Setup',
			anchor:'100%',
			bodyStyle: 'padding:7px 7px 7px 7px',
			border: false,
			height: 434,//392,
			shadhow: true,
			items: 
			[
				vTabPanelSettingBahasaUtilityTenant
			]
        }
	);
	
	return FormSettingBahasaUtilityTenant;
};

function getItemPanelBahasaSettingBahasaUtilityTenant() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		labelAlign : 'right',
	    items:
		[
			{
			    columnWidth: .5,
			    layout: 'form',
				labelAlign : 'right',
			    border: false,
				labelWidth:50,
			    items:
				[
					mComboGroupBahasaSettingBahasaUtilityTenant() 
				]
			},
			{
			    columnWidth: .5,
			    layout: 'form',
				labelAlign : 'right',
			    border: false,
				labelWidth:65,
			    items:
				[
					mComboBahasaSettingBahasaUtilityTenant() 
				]
			}
		]
	}
	
	return items;
};

function mComboGroupBahasaSettingBahasaUtilityTenant() 
{
	var Field = ['GROUP_KEY_ID','GROUP_KEY','GROUP_KEY_DESC'];

    dsGroupLanguageSettingBahasaUtilityTenant = new WebApp.DataStore({ fields: Field });
	
    /*dsGroupLanguageSettingBahasaUtilityTenant.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'GROUP_KEY',
			    Sortdir: 'ASC',
			    target: 'ViewComboGroupLanguage',
			    param: ''
			}
		}
	);*/
	
    var cboGroupBahasaSettingBahasaUtilityTenant = new Ext.form.ComboBox
	(
		{
		    id: 'cboGroupBahasaSettingBahasaUtilityTenant',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmGroupSettingBhs + ' ',
		    align: 'Right',
		    anchor:'100%',
		    store: dsGroupLanguageSettingBahasaUtilityTenant,
		    valueField: 'GROUP_KEY_ID',
		    displayField: 'GROUP_KEY',
			value:mVarDefaultValueGroup,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectGroupLanguageSettingBahasaUtilityTenant = b.data.GROUP_KEY_ID;
					RefreshDataSettingBahasaUtilityTenant();
			    }
			}
		}
	);
	
    return cboGroupBahasaSettingBahasaUtilityTenant;
};

function RefreshDataSettingBahasaUtilityTenant()
{	
	dsGroupLanguageSettingBahasaUtilityTenant.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'LIST_KEY_DESC', 
				Sortdir: 'ASC',
				target: 'ViewSettingBahasa',
				param: 'where group_key_id =~' + selectGroupLanguageSettingBahasaUtilityTenant + '~ and language_id=~' + selectImportLanguageSettingBahasaUtilityTenant + '~'
			}
		}
	);
		
	return dsGroupLanguageSettingBahasaUtilityTenant;
};

function mComboBahasaSettingBahasaUtilityTenant() 
{
	var Field = ['LANGUAGE_ID','LANGUAGE'];

    dsLanguageSettingBahasaUtilityTenant = new WebApp.DataStore({ fields: Field });
	
    /*dsLanguageSettingBahasaUtilityTenant.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'LANGUAGE',
			    Sortdir: 'ASC',
			    target: 'ViewComboBahasaUtility',
			    param: ''
			}
		}
	);*/
	
    var cboBahasaSettingBahasaUtilityTenant = new Ext.form.ComboBox
	(
		{
		    id: 'cboBahasaSettingBahasaUtilityTenant',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmBhsSettingBhs + ' ',
		    align: 'Right',
		    anchor:'99%',
		    store: dsLanguageSettingBahasaUtilityTenant,
		    valueField: 'LANGUAGE_ID',
		    displayField: 'LANGUAGE',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectLanguageSettingBahasaUtilityTenant = b.data.LANGUAGE_ID;
			    }
			}
		}
	);
	
    return cboBahasaSettingBahasaUtilityTenant;
};

function mComboImportBahasaSettingBahasaUtilityTenant() 
{
	var Field = ['LANGUAGE_ID','LANGUAGE'];

    dsImportLanguageSettingBahasaUtilityTenant = new WebApp.DataStore({ fields: Field });
	
    /*dsImportLanguageSettingBahasaUtilityTenant.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'LANGUAGE',
			    Sortdir: 'ASC',
			    target: 'ViewComboBahasaUtility',
			    param: ''
			}
		}
	);*/
	
    var cboImportBahasaSettingBahasaUtilityTenant = new Ext.form.ComboBox
	(
		{
		    id: 'cboImportBahasaSettingBahasaUtilityTenant',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmImportBhsSettingBhs + ' ',
		    align: 'Right',
		    anchor:'99%',
		    store: dsImportLanguageSettingBahasaUtilityTenant,
		    valueField: 'LANGUAGE_ID',
		    displayField: 'LANGUAGE',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectImportLanguageSettingBahasaUtilityTenant = b.data.LANGUAGE_ID;
					RefreshDataSettingBahasaUtilityTenant();
			    }
			}
		}
	);
	
    return cboImportBahasaSettingBahasaUtilityTenant;
};

function grdLabelBahasaSettingBahasaUtilityTenant()
{

	var fldDetail = ['GROUP_KEY_ID','LIST_KEY_ID','LANGUAGE_ID','LABEL','LIST_KEY','LIST_KEY_DESC','LANGUAGE'];
	
	dsListKeySettingBahasaUtilityTenant = new WebApp.DataStore({ fields: fldDetail });
	/*dsListKeySettingBahasaUtilityTenant.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'LIST_KEY_DESC', 
				Sortdir: 'ASC',
				target: 'ViewSettingBahasa',
				param: selectGroupLanguageSettingBahasaUtilityTenant + '%^%' + strUser
			}
		}
	);*/

	var vgrdLabelBahasaSettingBahasaUtilityTenant = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vgrdLabelBahasaSettingBahasaUtilityTenant',
			title: '',
			stripeRows: true,
			store: dsListKeySettingBahasaUtilityTenant,
			height:280,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,
			autoScroll:true,			
			frame:false,
			sm: new Ext.grid.CellSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						cellselect: function(sm, row, rec) 
						{
						}
					}
				}
			),
			cm: fnGridLabelBahasaSettingBahasaUtilityTenantColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	
	var PnlGridLabelSettingBahasaUtilityTenant = new Ext.Panel
	(
		{
		    id: 'PnlGridLabelSettingBahasaUtilityTenant',
		    region: 'center',
			layout:'form',
		    border: false,
			autoScroll:false,
			labelAlign : 'right',
		    height: 280,
		    anchor: '100%',
		    items:
			[
				vgrdLabelBahasaSettingBahasaUtilityTenant
			]
		}
	);
	
	return PnlGridLabelSettingBahasaUtilityTenant;
};

function fnGridLabelBahasaSettingBahasaUtilityTenantColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colListKeyDescSettingBahasaUtilityTenant',
				header: nmDescSettingBhs,
				dataIndex: 'LIST_KEY_DESC',
				width: 200,
				renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
                }
			},
			{ 
				id: 'colLabelSettingBahasaUtilityTenantDefault',
				header: nmLabelDefaultSettingBhs,
				dataIndex: 'LABEL',
				width: 250,
				renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
                }
            },
			{ 
				id: 'colLabelSettingBahasaUtilityTenant',
				header: nmLabelSettingBhs,
				dataIndex: 'LABEL',
				width: 250,
				editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolLabelSettingBahasaUtilityTenant',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
							}
						}
					}
				),
				renderer: function(value, cell) 
				{
					var str='';
					if (value != undefined)
					{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";

					};
			        return str;
                }
            }
		]
	)
};

function GetBahasaSettingBahasaUtilityTenant()
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetBahasaUtilityAset',
				Params:	strUser 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					selectImportLanguageSettingBahasa=cst.Id;
					Ext.get('cboImportBahasaSettingBahasaUtilityTenant').dom.value=cst.Bahasa;
				}
				else
				{
					selectImportLanguageSettingBahasa = '';
					Ext.get('cboImportBahasaSettingBahasaUtilityTenant').dom.value='';
				};
			}

		}
	);
};

/** --- SETUP BAHASA */

function initCompanyUtilityTenant()
{
	if (dsCompanyListUtilityTenant != undefined)
	{
		if(dsCompanyListUtilityTenant.getCount() > 0 )
		{
			Ext.get('txtKode_SetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_ID;
			Ext.get('txtNameSetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_NAME;
			Ext.get('txtAddressSetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_ADDRESS;
			Ext.get('txtTlpSetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_TELP;
			Ext.get('txtFaxSetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_FAX;
			Ext.get('txtCitySetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_CITY;
			Ext.get('txtPosSetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_POS_CODE;
			Ext.get('txtEmailSetCompany').dom.value=dsCompanyListUtilityTenant.data.items[0].data.COMP_EMAIL;
		}
		else
		{
			ClearTextCompanyUtilityTenant();
		};
	}
	else
	{
		ClearTextCompanyUtilityTenant();
	};
};

function initRowPerPageUtilityTenant()
{
	Ext.get('txtRowPerPage').dom.value= intRowPerPage;
};

function ClearTextRowPerPageUtilityTenant()
{
	Ext.get('txtRowPerPage').dom.value='';
}

function initBirthdayAlertUtilityTenant() {
    Ext.get('txtBirthdayAlert').dom.value = intBirthdayAlert;
};

function ClearTextBirthdayAlertUtilityTenant() {
    Ext.get('txtBirthdayAlert').dom.value = '';
}

function initAutoNumberingUtilityTenant() {
	var isAutoNum = intIDAutoNumbering == 1 ? true : false;
	Ext.get('chkIDAutoNumbering').dom.checked = isAutoNum;
};

function ClearCheckAutoNumberingUtilityTenant() {
    Ext.get('chkIDAutoNumbering').dom.checked = false;
}

function ClearTextCompanyUtilityTenant()
{
	Ext.get('txtKode_SetCompany').dom.value='';
	Ext.get('txtNameSetCompany').dom.value='';
	Ext.get('txtAddressSetCompany').dom.value='';
	Ext.get('txtTlpSetCompany').dom.value='';
	Ext.get('txtFaxSetCompany').dom.value='';
	Ext.get('txtCitySetCompany').dom.value='';
	Ext.get('txtPosSetCompany').dom.value='';
	Ext.get('txtEmailSetCompany').dom.value='';
};


function getItemPanelUtilityTenant() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    height: 33,
	    anchor: '100%',
	    items:
		[
			{
			    layout: 'hBox',
			    width: 522,
			    border: false,
			    bodyStyle: 'padding:5px 0px 5px 5px',
			    defaults: { margins: '3 3 3 3' },
			    anchor: '90%',
			    layoutConfig:
				{
				    align: 'middle',
				    pack: 'end'
				},
			    items:
				[
					{
					    xtype: 'button',
					    text: 'Ok ', //nmBtnOK,
					    width: 70,
					    style: { 'margin-left': '0px', 'margin-top': '0px' },
					    hideLabel: true,
					    id: 'btnOkUtilityTenant',
					    handler: function() {
					        if (mTabUtilityTenant === true) {
					            CompanyUtilityTenantSave(true);
					        };

					        var x = BahasaUtilityTenantSave(true);
					        var y = RowPerPageUtilityTenantSave(true);
					        var z = BirthdayAlertUtilityTenantSave(true);
							var z1 = AutoNumberingUtilityTenantSave(true);
					        intRowPerPage = Ext.get('txtRowPerPage').getValue();
					        intBirthdayAlert = Ext.get('txtBirthdayAlert').getValue();
							intIDAutoNumbering = intAutoNumberingUtilityTenant;
					        GetRowPerPagePage();
					        GetBirthdayAlert();
							GetAutoNumbering();
					        if (x === undefined || y === undefined || z===undefined || z1 === undefined) {
					            vWinFormEntryUtilityTenant.close();
					        };

					    }
					},
					{
					    xtype: 'button',
					    text: 'Cancel ', //nmBtnCancel ,
					    width: 70,
					    hideLabel: true,
					    id: 'btnCancelUtilityTenant',
					    handler: function() {
					        vWinFormEntryUtilityTenant.close();
					    }
					},
					{
					    xtype: 'button',
					    text: 'Apply ', //nmBtnApply ,
					    width: 70,
					    hideLabel: true,
					    id: 'btnApplyUtilityTenant',
					    handler: function() {
					        if (mTabUtilityTenant === true) {
					            CompanyUtilityTenantSave(false);
					        };
					        BahasaUtilityTenantSave(false);
					        RowPerPageUtilityTenantSave(false);
					        BirthdayAlertUtilityTenantSave(false);
							AutoNumberingUtilityTenantSave(false);
					        intRowPerPage = Ext.get('txtRowPerPage').getValue();
					        intBirthdayAlert = Ext.get('txtBirthdayAlert').getValue();
							intIDAutoNumbering = intAutoNumberingUtilityTenant;
					        GetRowPerPagePage();
					        GetBirthdayAlert();
							GetAutoNumbering();
					    }
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningUtilityTenant(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};



function mComboBahasaUtilityTenant() 
{
	var Field = ['LANGUAGE_ID','LANGUAGES'];

    dsLanguageUtilityTenant = new WebApp.DataStore({ fields: Field });
	
    dsLanguageUtilityTenant.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'LANGUAGES',
			    Sortdir: 'ASC',
			    target: 'ViewComboBahasaUtility',
			    param: ''
			}
		}
	);
	
    var cboBahasaUtilityTenant = new Ext.form.ComboBox
	(
		{
		    id: 'cboBahasaUtilityTenant',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: 'Utility ',//nmBhsUtility + ' ',
		    align: 'Right',
		    anchor:'60%',
		    store: dsLanguageUtilityTenant,
		    valueField: 'LANGUAGE_ID',
		    displayField: 'LANGUAGES',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectLanguageUtilityTenant = b.data.LANGUAGE_ID;
			    }
			}
		}
	);
	
    return cboBahasaUtilityTenant;
};

function RefreshDataUtilityTenant()
{	
	var Field = ['COMP_ID','COMP_NAME','COMP_ADDRESS','COMP_TELP','COMP_FAX','COMP_CITY','COMP_POS_CODE','COMP_EMAIL','LOGO'];

    dsCompanyListUtilityTenant = new WebApp.DataStore({ fields: Field });
	dsCompanyListUtilityTenant.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: 'Company_ID', 
					Sortdir: 'ASC', 
					target:'ViewSetupCompany',
					param : ''
				}			
			}
		);       
		
	return dsCompanyListUtilityTenant;
};

function RefreshDataRowPerPageUtilityTenant()
{	
	var Field = ['KEY_DAYA','SETTINGS'];

    dsRowperPageUtilityTenant = new WebApp.DataStore({ fields: Field });
	dsRowperPageUtilityTenant.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: 'Company_ID', 
					Sortdir: 'ASC', 
					target:'ViewRowPerPageUtility',
					param : ''
				}			
			}
		);       
		
	return dsRowperPageUtilityTenant;
};

function RefreshDataBirthdayAlertUtilityTenant()
{	
	var Field = ['KEY_DAYA','SETTINGS'];

    dsBirthdayAlertUtilityTenant = new WebApp.DataStore({ fields: Field });
	dsBirthdayAlertUtilityTenant.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: 1000, 
					Sort: '', 
					Sortdir: 'ASC', 
					target:'ViewBirthdayAlertUtility',
					param : ''
				}			
			}
		);       
		
	return dsBirthdayAlertUtilityTenant;
};

function CompanyUtilityTenantSave(mBol) 
{
	if (Ext.get('txtKode_SetCompany').dom.value === '') 
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/CreateDataObj",
				params: getParamCompanyUtilityTenant(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						if(mBol === false)
						{
							Ext.get('txtKode_SetCompany').dom.value=cst.CompanyId;
							ShowPesanInfoUtilityTenant(nmPesanSimpanSukses,nmTitleFormUtility);
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						if(mBol === false)
						{
							ShowPesanWarningUtilityTenant(nmPesanSimpanGagal,nmTitleFormUtility);
						};
					}
					else 
					{
						if(mBol === false)
						{
							ShowPesanErrorUtilityTenant(nmPesanSimpanError,nmTitleFormUtility);
						};
					};
				}
			}
		)
	}
	else 
	{
		Ext.Ajax.request
		 (
			{
				url: "./Datapool.mvc/UpdateDataObj",
				params: getParamCompanyUtilityTenant(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						if(mBol === false)
						{
							ShowPesanInfoUtilityTenant(nmPesanEditSukses,nmTitleFormUtility);
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						if(mBol === false)
						{
							ShowPesanWarningUtilityTenant(nmPesanEditGagal,nmTitleFormUtility);
						};
					}
					else 
					{
						if(mBol === false)
						{
							ShowPesanErrorUtilityTenant(nmPesanEditError,nmTitleFormUtility);
						};
					};
				}
			}
		)
	};
};

function BahasaUtilityTenantSave(mBol) 
{
	if (ValidasiBahasaUtilityTenant(nmTitleFormUtility) === 1 ) 
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/UpdateDataObj",
				params: getParamBahasaUtilityTenant(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						mLanguage(strUser);
						if(mBol === false)
						{
							ShowPesanInfoUtilityTenant(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility,nmTitleFormUtility);
						};
					}
					else if  (cst.success === false && cst.pesan===0)
					{
						if(mBol === false)
						{
							ShowPesanWarningUtilityTenant(nmPesanSimpanGagal,nmTitleFormUtility);
						};
					}
					else 
					{
						if(mBol === false)
						{
							ShowPesanErrorUtilityTenant(nmPesanSimpanError,nmTitleFormUtility);
						};
					};
				}
			}
		)
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};

function RowPerPageUtilityTenantSave(mBol) 
{
	if (ValidasiRowPerPageUtilityTenant(nmTitleFormUtility) === 1 ) 
	{
	    Ext.Ajax.request
		(
			{
			    url: "./Datapool.mvc/UpdateDataObj",
			    params: getParamRowPerPageUtilityTenant(),
			    success: function(o) {
			        var cst = Ext.decode(o.responseText);
			        if (cst.success === true) {
			            if (mBol === false) {	
			                ShowPesanInfoUtilityTenant(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility, nmTitleFormUtility);
			            };
			        }
			        else if (cst.success === false && cst.pesan === 0) {
			        if (mBol === false) {			            
			                ShowPesanWarningUtilityTenant(nmPesanSimpanGagal, nmTitleFormUtility);
			            };
			        }
			        else {
			            if (mBol === false) {			                
			                ShowPesanErrorUtilityTenant(nmPesanSimpanError, nmTitleFormUtility);
			            };
			        };
			    }
			}
		)
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};

function BirthdayAlertUtilityTenantSave(mBol) {
    if (ValidasiBirthdayAlertUtilityTenant(nmTitleFormUtility) === 1) {
        Ext.Ajax.request
		(
			{
			    url: "./Datapool.mvc/UpdateDataObj",
			    params: getParamBirtdayAlertUtilityTenant(),
			    success: function(o) {
			        var cst = Ext.decode(o.responseText);
			        if (cst.success === true) {
			            if (mBol === false) {
			                ShowPesanInfoUtilityTenant(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility, nmTitleFormUtility);
			            };
			        }
			        else if (cst.success === false && cst.pesan === 0) {
			            if (mBol === false) {
			                ShowPesanWarningUtilityTenant(nmPesanSimpanGagal, nmTitleFormUtility);
			            };
			        }
			        else {
			            if (mBol === false) {
			                ShowPesanErrorUtilityTenant(nmPesanSimpanError, nmTitleFormUtility);
			            };
			        };
			    }
			}
		)
    }
    else {
        if (mBol === true) {
            return false;
        };
    };
};

function AutoNumberingUtilityTenantSave(mBol) {
    //if (ValidasiBirthdayAlertUtilityTenant(nmTitleFormUtility) === 1) {
        Ext.Ajax.request
		(
			{
			    url: "./Datapool.mvc/UpdateDataObj",
			    params: getParamAutoNumberingUtilityTenant(),
			    success: function(o) {
			        var cst = Ext.decode(o.responseText);
			        if (cst.success === true) {
			            if (mBol === false) {
			                ShowPesanInfoUtilityTenant(nmPesanSimpanSukses + ' , ' + nmKonfirmBhsUtility, nmTitleFormUtility);
			            };
			        }
			        else if (cst.success === false && cst.pesan === 0) {
			            if (mBol === false) {
			                ShowPesanWarningUtilityTenant(nmPesanSimpanGagal, nmTitleFormUtility);
			            };
			        }
			        else {
			            if (mBol === false) {
			                ShowPesanErrorUtilityTenant(nmPesanSimpanError, nmTitleFormUtility);
			            };
			        };
			    }
			}
		);
    /*}
    else {
        if (mBol === true) {
            return false;
        };
    };*/
};

function ValidasiBahasaUtilityTenant(modul)
{
	var x=1;
	if(Ext.get('cboBahasaUtilityTenant').dom.value === '' && ( selectLanguageUtilityTenant === '' || selectLanguageUtilityTenant === undefined))
	{
		x=0;
		ShowPesanWarningUtilityTenant(nmGetValidasiKosong(nmBhsUtility),modul);
	};
	
	return x;
};

function ValidasiRowPerPageUtilityTenant(modul)
{
	var x=1;
	if(Ext.get('txtRowPerPage').dom.value === '' || Ext.get('txtRowPerPage').dom.value === 0)
	{
		x=0;
		ShowPesanWarningUtilityTenant(nmGetValidasiKosong(nmBhsUtility),modul);
	};
	
	return x;
};

function ValidasiBirthdayAlertUtilityTenant(modul) {
    var x = 1;
    if (Ext.get('txtBirthdayAlert').dom.value === '' || Ext.get('txtBirthdayAlert').dom.value === 0) {
        x = 0;
        ShowPesanWarningUtilityTenant(nmGetValidasiKosong(nmBhsUtility), modul);
    };

    return x;
};

function getParamCompanyUtilityTenant() 
{
    var params =
	{	
		Table: 'ViewSetupCompany',   
	    CompId: Ext.get('txtKode_SetCompany').getValue(),
	    CompName: Ext.get('txtNameSetCompany').getValue(),
		CompAddress:Ext.get('txtAddressSetCompany').getValue(),
		CompTelp:Ext.get('txtTlpSetCompany').getValue(),
		CompFax:Ext.get('txtFaxSetCompany').getValue(),
		CompCity:Ext.get('txtCitySetCompany').getValue(),
		CompPos:Ext.get('txtPosSetCompany').getValue(),
		CompEmail:Ext.get('txtEmailSetCompany').getValue()
	};
    return params
};

function getParamBahasaUtilityTenant() 
{
    var params =
	{	
		Table: 'ViewComboBahasaUtility',   
		Id:selectLanguageUtilityTenant,
		KdUser:mVarKdUserUtilityTenant
	};
	
    return params
};

function getParamRowPerPageUtilityTenant() 
{
    var params =
	{	
		Table: 'ViewRowPerPageUtility',   
		Jumlah: Ext.get('txtRowPerPage').getValue()
	};
    return params
};

function getParamBirtdayAlertUtilityTenant() {
    var params =
	{
	    Table: 'ViewBirthdayAlertUtility',
	    day: Ext.get('txtBirthdayAlert').getValue()
	};
    return params
};

function getParamAutoNumberingUtilityTenant() {
    var params =
	{
	    Table: 'ViewAutoNumberingUtility',
	    Auto: intAutoNumberingUtilityTenant
	};
    return params
};

function ShowPesanWarningUtilityTenant(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :300
		}
	);
};

function ShowPesanErrorUtilityTenant(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :300
		}
	);
};

function ShowPesanInfoUtilityTenant(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :300
		}
	);
};

function GetBahasaUtilityTenant()
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetBahasaUtilityTenant',
				Params:	strUser 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					selectLanguageUtilityTenant=cst.Id;
					Ext.get('cboBahasaUtilityTenant').dom.value=cst.Bahasa;
					mVarKdUserUtilityTenant=cst.KdUser;
				}
				else
				{
					selectLanguageUtilityTenant = '';
					Ext.get('cboBahasaUtilityTenant').dom.value='';
					mVarKdUserUtilityTenant=''
				};
			}

		}
	);
};

///---------------------------------------------------------------------------------------///