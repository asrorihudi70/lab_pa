var dsSetWarningLogMetList;
var SetWarningLogMetLookUps;
var cellSelectMeteringSetWarningLogMet;


function SetWarningLogMetLookUp() 
{

	var lebar=600;
    SetWarningLogMetLookUps = new Ext.Window   	
    (
		{
		    id: 'SetWarningLogMetLookUps',
		    title: nmTitleFormWarningLogMet,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 300,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupWarningLogMet',
		    modal: true,
		    items: 
			[	
				getFormEntrySetWarningLogMet(lebar)
			],
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					RefreshDataSetWarningLogMet();
				}
            }
		}
	);


    SetWarningLogMetLookUps.show();
	RefreshDataSetWarningLogMet();
	RefreshDataSetWarningLogMet();
};


function getFormEntrySetWarningLogMet(lebar) 
{
    var pnlSetWarningLogMet = new Ext.FormPanel
    (
		{
		    id: 'PanelSetWarningLogMet',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 235,//365,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    iconCls: 'SetupWarningLogMet',
		    border: false,
		    items:
			[GetDTLMeteringSetMateringAsset()]
		}
	); 
	
	var FormTRSetWarningLogMet = new Ext.Panel
	(
		{
		    id: 'FormTRSetWarningLogMet',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlSetWarningLogMet,
				{
					layout: 'hBox',
					width:470,
					border: false,
					bodyStyle: 'padding:0px 0px 0px 0px',
					defaults: { margins: '3 3 3 3' },
					anchor: '98.9%',
					layoutConfig: 
					{
						align: 'middle',
						pack:'end'
					},
					items:
					[
						{
							xtype:'button',
							text:nmBtnOK,
							width:70,
							style:{'margin-left':'0px','margin-top':'0px'},
							hideLabel:true,
							id: 'btnOkLookupWarningLogMet',
							handler:function()
							{
								SetWarningLogMetLookUps.close();
							}
						}
					]
				}
			]
		}
	);

    return FormTRSetWarningLogMet;
};

function GetDTLMeteringSetMateringAsset() 
{
    var fldDetail = ['CURRENT_METER','ASSET_MAINT_ID','ASSET_MAINT_NAME','EMP_ID','EMP_NAME','TGL_TERAKHIR'];
	
    dsSetWarningLogMetList = new WebApp.DataStore({ fields: fldDetail })

    var gridMeteringSetMateringAsset = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsSetWarningLogMetList,
		    border: true,
		    columnLines: true,
		    frame: false,
			width:566,
			height:220,
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectMeteringSetWarningLogMet = dsSetWarningLogMetList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					cellSelectMeteringSetWarningLogMet = dsSetWarningLogMetList.getAt(ridx);
					InputLogMeteringLookUp(cellSelectMeteringSetWarningLogMet.data.ASSET_MAINT_ID,cellSelectMeteringSetWarningLogMet.data.ASSET_MAINT_NAME); 
				}
			},
		    cm: MeteringSetWarningLogMetColumModel(),
			 viewConfig: { forceFit: true }
		}
	);

    return gridMeteringSetMateringAsset;
};

function MeteringSetWarningLogMetColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colAsetIDSetWarningLogMet',
				header: nmColKdAsetWarningLogMet,
				dataIndex: 'ASSET_MAINT_ID',
				width:150
			},
			{
				id: 'colAsetNameSetWarningLogMet',
				header: nmColNamaAsetWarningLogMet,
				dataIndex: 'ASSET_MAINT_NAME',
				width:200
			},
			{
				id: 'colCurrentSetWarningLogMet',
				header: nmColCurrentWarningLogMet,
				dataIndex: 'CURRENT_METER',
				width:100
			},
			{
			   id:'colLastWarningLogMetering',
			   header: nmColLastDateWarningLogMet,
			   dataIndex: 'TGL_TERAKHIR',
			   width: 120,
			   renderer: function(v, params, record) 
				{
					return ShowDate(record.data.TGL_TERAKHIR);
				}
			},
			{
				id: 'colEmpWarningLogMetering',
				header: nmColEmpWarningLogMet,
				dataIndex: 'EMP_NAME',
				width:120
			}
		]
	)
};


function RefreshDataSetWarningLogMet()
{	
	dsSetWarningLogMetList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'TGL_TERAKHIR',
                                Sort: 'tgl_terakhir',
				Sortdir: 'DESC', 
				target:'ViewWarningLogMetering',
				param: ''
			} 
		}
	);

	return dsSetWarningLogMetList;
};


 


