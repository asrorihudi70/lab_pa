var App={};
function imageZoom(imgID, resultID) {
  var img, lens, result, cx, cy;
  img = document.getElementById(imgID);
  result = document.getElementById(resultID);
  /*create lens:*/
  lens = document.createElement("DIV");
  lens.setAttribute("class", "img-zoom-lens");
  lens.setAttribute("style", "width: 40px;height: 40px;position: absolute;border: 1px solid white;");
  /*insert lens:*/
  img.parentElement.insertBefore(lens, img);
  /*calculate the ratio between result DIV and lens:*/
  cx = result.offsetWidth / lens.offsetWidth;
  cy = result.offsetHeight / lens.offsetHeight;
  /*set background properties for the result DIV:*/
  result.style.backgroundImage = "url('" + img.src + "')";
  result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
  /*execute a function when someone moves the cursor over the image, or the lens:*/
  lens.addEventListener("mousemove", moveLens);
  img.addEventListener("mousemove", moveLens);
  /*and also for touch screens:*/
  lens.addEventListener("touchmove", moveLens);
  img.addEventListener("touchmove", moveLens);
  
  function moveLens(e) {
	var pos, x, y;
	/*prevent any other actions that may occur when moving over the image:*/
	e.preventDefault();
	/*get the cursor's x and y positions:*/
	pos = getCursorPos(e);
	/*calculate the position of the lens:*/
	x = pos.x - (lens.offsetWidth / 2);
	y = pos.y - (lens.offsetHeight / 2);
	/*prevent the lens from being positioned outside the image:*/
	if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
	if (x < 0) {x = 0;}
	if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
	if (y < 0) {y = 0;}
	/*set the position of the lens:*/
	lens.style.left = x + "px";
	lens.style.top = y + "px";
	/*display what the lens "sees":*/
	result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
  }
  function getCursorPos(e) {
	var a, x = 0, y = 0;
	e = e || window.event;
	/*get the x and y positions of the image:*/
	a = img.getBoundingClientRect();
	/*calculate the cursor's x and y coordinates, relative to the image:*/
	x = e.pageX - a.left;
	y = e.pageY - a.top;
	/*consider any page scrolling:*/
	x = x - window.pageXOffset;
	y = y - window.pageYOffset;
	return {x : x, y : y};
  }
}
App.FotoUpload = Ext.extend(Ext.Panel, {
    constructor: function(config){
		conf={
			result:null,
			tempResult:null,
			border:false,
			face:false,
			style:'background: black;',
			folder:'Img Asset/',
			paddingBottom:false,
			closeAction:'destroy',
			bodyStyle:'display: table-cell;vertical-align: middle;text-align: center;',
			property:{},
			tooltip:'Pencarian <b>[Ctrl+Shift+f]</b>',
			cls:'i-transparent',
			width: 100,
			initial: '',
			height: 100,
			input:true,
			html:'<img style="width: 100%;cursor:pointer;" src="'+baseURL+'Img Asset/blank.png"></img>',
		};
		if(config != undefined){
			for (var key in config) {
				conf[key]=config[key];
			}
		}
   		Ext.apply( this, conf );
        App.FotoUpload.superclass.constructor.apply(this, arguments);
   	},
	initComponent: function(config){
		var property=this.property,btnDeleteImage=null,btnReplaceImage=null;
		property.type='fotoupload';
		disabled=false;
		function isBase64(str) {
			try {
				return btoa(atob(str)) == str;
			} catch (err) {
				return false;
			}
		}
		function getMeta(url,callback){
			$("<img/>",{
				load : function(){
					callback(this);
				},
				src  : url
			});
		}
		if(this.input==false){disabled=true;}
		var size = {
				width: window.innerWidth || document.body.clientWidth,
				height: window.innerHeight || document.body.clientHeight
			},
			$this=this,
			panelWindow=new Ext.Panel({html:'<img src="'+baseURL+'Img Asset/blank.png"></img>',paddingBottom:false,border:false,}),
			buttonDownload=new Ext.Button({
				tooltip:'Unduh Gambar',
				iconCls:'save',
				handler:function(){
					// var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$"),src='';
					// if (base64Matcher.test($this.tempResult)) {src='data:image/png;base64,'+$this.tempResult;} else {src=baseURL+'upload/'+$this.tempResult;}
					var src=isBase64($this.tempResult);
					if(src!==false){
						src='data:image/png;base64,'+$this.tempResult;
					}else{
						src=baseURL+$this.folder+$this.tempResult;
					}
					window.open(src);
				}
			});
			if(Ext.getCmp($this.id+'window') != undefined){
				Ext.getCmp($this.id+'window').destroy();
			}
			// console.log(Ext.getCmp($this.id+'window'));
			// console.log('B');
			var windowFoto=Ext.getCmp($this.id+'window');
			if(Ext.getCmp($this.id+'window') == undefined){
				windowFoto=new Ext.Window({
					title:'Foto',
					closeAction:'hide',
					modal:true,
					id:$this.id+'window',
					width: 630,
					height: 360,
					bodyStyle:'background:black;',
					constraint:true,
					tbar:[
						btnDeleteImage=new Ext.Button({
							tooltip:'Ganti Gambar',
							iconCls:'add',
							disabled:disabled,
							handler:function(){
								Ext.getCmp($this.id+'file').fileInput.set({accept:'image/*'});
								Ext.getCmp($this.id+'file').fileInput.dom.click();
							}
						}),'-',
						btnReplaceImage=new Ext.Button({
							tooltip:'Hapus Gambar',
							iconCls:'remove',
							disabled:disabled,
							handler:function(){
								if($this.initial != undefined && $this.initial != null && $this.initial != ''){
									panelWindow.update('<img style="width: 150px;height: 170px;" id="initialWindow-'+$this.id.replace('.','_')+'"></img>');
									var split=$this.initial.split(' '),car='',jum=0,
										tmpInitial=$('#tmp-initial'),
										comInitial=$('#initialWindow-'+$this.id.replace('.','_'));
									for(var i=0,iLen=split.length; i<iLen;i++){
										if(split[i] !='' && split[i] !=null && split[i] !='null'){
											car+=split[i].charAt(0);
											jum++;
										}
									}
									if(comInitial.length==0){
										tmpInitial.initial({name:car,charCount:jum,width:$this.width,height:$this.height});
										panelWindow.update('<img id="initialWindow-'+$this.id.replace('.','_')+'" style="width: 150px;height: 170px;" src="'+$('#tmp-initial').attr('src')+'"></img>');
									}else{
										comInitial.initial({name:car,charCount:jum,width:150,height:170});
									}
								}else{
									panelWindow.update('<img src="'+baseURL+'Img Asset/blank.png"></img>');
								}
								$this.setNull();
								windowFoto.center();
								buttonDownload.disable();
							}
						}),'-',
						buttonDownload
					],
					items:[panelWindow],
					listeners:{
						show:function(a){
							if($this.input==false){btnDeleteImage.disable();btnReplaceImage.disable();
							}else{btnDeleteImage.enable();btnReplaceImage.enable();}
							if($this.result != null){
								buttonDownload.enable();
								var src=isBase64($this.tempResult);
								if(src!==false){
									src='data:image/png;base64,'+$this.tempResult;
								}else{
									src=baseURL+$this.folder+$this.tempResult;
								}
								
								// var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$"),src='';
								// if (base64Matcher.test($this.tempResult)) {src='data:image/png;base64,'+$this.tempResult;
								// } else {src=baseURL+'upload/'+$this.tempResult;}
								panelWindow.update('<div style="position: relative;width: 100%;height: 100%;"><img  id="'+$this.id+'image" 		style="float:left;max-width:'+(300)+'px;max-height:'+(300)+'px;"  src="'+src+'"></img>'+
								'<div id="'+$this.id+'result" style="width: 300px;height: 300px;float:right;"></div></div>');
							}else{
								buttonDownload.disable();
								if($this.initial != undefined && $this.initial != null && $this.initial != ''){
									panelWindow.update('<img style="width: 150px;height: 170px;" id="initialWindow-'+$this.id.replace('.','_')+'"></img>');
									var split=$this.initial.split(' '),car='',jum=0,
										tmpInitial=$('#tmp-initial'),
										comInitial=$('#initialWindow-'+$this.id.replace('.','_'));
									for(var i=0,iLen=split.length; i<iLen;i++){
										if(split[i] !='' && split[i] !=null && split[i] !='null'){
											car+=split[i].charAt(0);
											jum++;
										}
									}
									if(comInitial.length==0){
										tmpInitial.initial({name:car,charCount:jum,width:$this.width,height:$this.height});
										panelWindow.update('<img id="initialWindow-'+$this.id.replace('.','_')+'" style="width: 150px;height: 170px;" src="'+$('#tmp-initial').attr('src')+'"></img>');
									}else{
										comInitial.initial({name:car,charCount:jum,width:150,height:170});
									}
								}else{
									panelWindow.update('<img id="initialWindow-'+$this.id.replace('.','_')+'" style="width: 150px;height: 170px;" src="'+baseURL+'Img Asset/blank.png"></img>');
								}
							}
							a.center();
							imageZoom($this.id+'image',$this.id+'result');
							
						},hide:function(){
							// $('#'+$this.id+'image').undbind('mousemove');
							// $('#'+$this.id+'image').undbind('touchmove');
						}
					}
				});
			}
			var file=new Ext.ux.form.FileUploadField({
					type : 'filefield',
					bodyStyle:'display:none;',
					id:$this.id+'file',
					hidden:true,
					buttonOnly: false,
					result:null,
					listeners:{
						fileselected:function(a){
							var file = a.fileInput.dom.files[0],
							reader = new FileReader();
							if($this.face==false){
								windowFoto.hide();
							}
							reader.onload = (function(theFile) {
								return function(e) {
									$this.result=btoa(e.target.result);
									$this.tempResult=btoa(e.target.result);
									getMeta('data:image/png;base64,'+btoa(e.target.result),function(a){
										if(a.height>a.width){
											$this.update('<img style="height: 100%;cursor:pointer;" src="data:image/png;base64,'+btoa(e.target.result)+'"></img>');
										}else{
											$this.update('<img style="width: 100%;cursor:pointer;" src="data:image/png;base64,'+btoa(e.target.result)+'"></img>');
										}
									});
									panelWindow.update('<div style="position: relative;width: 100%;height: 100%;"><img  id="'+$this.id+'image" style="float:left;max-width:'+(300)+'px;max-height:'+(300)+'px;"  src="data:image/png;base64,'+btoa(e.target.result)+'"></img>'+
								'<div id="'+$this.id+'result" style="border: 1px solid #d4d4d4;width: 300px;height: 300px;float:right;"></div></div>');
									windowFoto.center();
									buttonDownload.enable();
									imageZoom($this.id+'image',$this.id+'result');
								};
							})(file);
							reader.readAsBinaryString(file);
							
						}
					}
				});
		this.items=[file];
		this.listeners={'render': function(panel) {panel.body.on('click', function(){windowFoto.show();
		// windowFoto.maximize();
		});}};
		this.setFoto=function(foto){
			if((foto != null && foto != '' && foto != 'blank.png') || (foto == 'blank.png' && ($this.initial == undefined || $this.initial == null || $this.initial == ''))){
				$this.result=true;
				$this.tempResult=foto;
				var src=isBase64(foto);
				if(src!==false){
					src='data:image/png;base64,'+foto;
				}else{
					src=baseURL+$this.folder+foto;
				}
				// var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$"),src='';
				// if (base64Matcher.test(foto)) {src='data:image/png;base64,'+foto;} else {src=baseURL+'Img Asset/'+foto;}
				getMeta(src,function(a){
					if(a.height>a.width){
						$this.update('<img style="height: 100%;cursor:pointer;" src="'+src+'"></img>');
					}else{
						$this.update('<img style="width: 100%;cursor:pointer;" src="'+src+'"></img>');
					}
				});
				
			}else{
				$this.setNull();
			}
		},
		this.setNull=function(){
			$this.result=null;
			$this.tempResult=null;
			if($this.initial != undefined && $this.initial != null && $this.initial != ''){
				$this.update('<img id="initial-'+$this.id.replace('.','_')+'" style="width: 150px;height: 170px;border: 1px solid #99bce8;cursor:pointer;"></img>');
				var split=$this.initial.split(' '),car='',jum=0
					tmpInitial=$('#tmp-initial'),
					comInitial=$('#initial-'+$this.id.replace('.','_'));
				for(var i=0,iLen=split.length; i<iLen;i++){
					if(split[i] !='' && split[i] !=null && split[i] !='null'){
						car+=split[i].charAt(0);
						jum++;
					}
				}
				if(comInitial.length==0){
					tmpInitial.initial({name:car,charCount:jum,width:$this.width,height:$this.height});
					$this.update('<img id="initial-'+$this.id.replace('.','_')+'" style="width: 150px;height: 170px;border: 1px solid #99bce8;cursor:pointer;" src="'+$('#tmp-initial').attr('src')+'"></img>');
				}else{
					comInitial.initial({name:car,charCount:jum,width:$this.width,height:$this.height});
				}
			}else{
				getMeta(baseURL+'Img Asset/blank.png',function(a){
					if(a.height>a.width){
						$this.update('<img style="height: 100%; border: 1px solid #99bce8;cursor:pointer;" src="'+baseURL+'Img Asset/blank.png"></img>');
					}else{
						$this.update('<img style="width: 100%; cursor:pointer;" src="'+baseURL+'Img Asset/blank.png"></img>');
					}
				});
			}
		};
		App.FotoUpload.superclass.initComponent.call(this);
    },
	// afterLayout: function(component, eOpts) {
        // //this.callParent(arguments);
		// alert();
		// var $this=this;
		// $('#'+$this.items.items[0].container.id).find('input[type=file],select').each(function(a,b,c) {
			// $('#'+b.id).parent().hide();
		// });
        // // component.focus(true); doesn't work
    // },
	onRender : function(ct, position){
        App.FotoUpload.superclass.onRender.call(this, ct, position);
		var $this=this;
		setTimeout(function(){
			$('#'+$this.items.items[0].container.id).find('input[type=file],select').each(function(a,b,c) {
				$('#'+b.id).parent().hide();
			});
		},1);
		console.log(this);
		if(this.value != undefined){this.setFoto(this.value);}else{
			//this.setFoto(null);
		}
    }
});
Ext.reg('ifoto',App.FotoUpload);
