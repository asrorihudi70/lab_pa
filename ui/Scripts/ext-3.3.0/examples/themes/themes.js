/*!
 * Ext JS Library 3.3.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

Ext.onReady(function(){
    var items = [];
    
    Ext.QuickTips.init();
        
    //=============================================================
    // Stylesheet Switcher
    //=============================================================
    Ext.get(document.getElementById('styleswitcher_select').id).on('change',function(e,select){
        var name = select[select.selectedIndex].value;
        setActiveStyleSheet(name);
    });
    
    var cookie = readCookie("style");
    var title = cookie ? cookie : getPreferredStyleSheet();
    Ext.get(document.getElementById('styleswitcher_select').id).dom.value=title;
});