var js_loader={};
js_loader[baseURL+'ui/Scripts/CommonRenderer.js']=true;
js_loader[baseURL+'ui/Scripts/Penamaan.js']=true;
function LoadModule(cTarget) {
    var mdl = { title: '', id: '', url: '', htm: '' };
    try {
        DoGetModule(mdl, cTarget);
    }
    catch (e) {
        alert('errorr' + e);
    }
};

function LoadModuleDashboard(cTarget,cCriteria) 
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    try 
	{
        DoGetModuleDashboard(mdl, cTarget,cCriteria);
    }
    catch (e) 
	{
        alert('errorr' + e);
    }
};

function DoGetModule(mdl, cTarget) {
    var module = Ext.get(cTarget);
    
    
    
    if (module === null) {

        // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
        Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/getModule",
	    params: {
	        //UserID: 'Admin',
	        UserID: '0',
	        ModuleID: cTarget                      // parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
				
	        var cst = Ext.decode(o.responseText);
			
			
			
			if (cst.success === true )
            {
				 /*Ext.Ajax.request(
				{
					//url: "./home.mvc/getModule",
					//url: baseURL + "index.php/main/getTrustee",
					url: baseURL + "index.php/main/getcurrentshiftIGD", 
					 params: {
						 command:0,
						 mod_id:cst.id,
						 },
					success: function(response, opts) {
						var ket=response.responseText;	
						if (ket=='')
						{
							document.getElementById('ext-comp-1007').innerHTML=' ';
						}
						else
						{
							 document.getElementById('ext-comp-1007').innerHTML='Shift : ' +ket;
						}
						//document.getElementById('ext-comp-1007').innerHTML='Shift : 1 ';
						 
							  
							  
					},
					 failure: function(response, opts) {
						 
							NamaUser=response.responseText;
						

					}

				}); */
				mdl.title = cst.title;
				mdl.id = cst.id;
				mdl.url = cst.url;
				mdl.htm = cst.htm;
				// cek apakah modul sudah di load
				// cek rutin disini
				CurrentPage.id = mdl.id;
				//alert(f);
				//jika belum diload maka load script

				LoadScripts(mdl.htm);
				mainPage.doLayout();
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
				 if (cst.expire=== true)
				 {
					window.location.href= WebAppUrl.UrlLogOff ;
				 }
            }

	    }

	});
    }
    else {
        mainPage.setActiveTab(cTarget);
    }
}; //end do getmoduel

function DoGetModuleDashboard(mdl,cTarget,cCriteria) 
{
    var module = Ext.get(cTarget);
    
    if (module === null) {

        // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
        Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    url: baseURL + "index.php/main/getModule",
	    params: {
	        //UserID: 'Admin',
	        UserID: '0',
	        ModuleID: cTarget                      // parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			alert ('load gagal');
		},	    
	    success: function(o) {
	        var cst = Ext.decode(o.responseText);
			//alert(o.responseText);
	        mdl.title = cst.title;
	        mdl.id = cst.id;
	        mdl.url = cst.url;
	        mdl.htm = cst.htm;
	        // cek apakah modul sudah di load
	        // cek rutin disini
	        CurrentPage.id = mdl.id;
			CurrentPage.criteria=cCriteria;
	        //alert(f);
	        //jika belum diload maka load script
		
	        LoadScripts(mdl.htm);
	        mainPage.doLayout();

	    }

	});
    }
    else {
        mainPage.setActiveTab(cTarget);
    }
}; 

function LoadScripts(records) 
{
    var i;

    for (var i = 0; i < records.length; i++) {
        var x = records[i];
        if (x!='')
        	dhtmlLoadScript(x);
			        	
        //alert(x);
        //dhtmlLoadScript(baseURL + x.Script);
        
    };

};

function DoProcess(mdl, cTarget) {
  
        // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
        Ext.Ajax.request({
            //url: "./Module.mvc/ExecProc",
            url: baseURL + "index.php/main/ExecProc",
            params: {
                UserID: 'Admin',
                ModuleID: cTarget                      // parameter untuk url yang dituju (fungsi didalam controller)
            },
            success: function(o) {
                var cst = Ext.decode(o.responseText);


                mdl.title = cst.title;
                mdl.id = cst.id;
                mdl.url = cst.url;
                mdl.htm = cst.htm;


                mainPage.add(mddata);
                mainPage.setActiveTab(mdl.id);



            }

        });
   
    //alert(ss);
    return mdl;
}

function dhtmlLoadScript(path,callback) {
	var done = false;
	// if(js_loader[path.split('?')[0]] == undefined){
		// js_loader[path.split('?')[0]]=true;
		var scr = document.createElement("script");
		scr.src = path;
		// scr.type = "text/javascript";  
		scr.onload = handleLoad;
		scr.onreadystatechange = handleReadyStateChange;
		scr.onerror = handleError;	
		document.body.appendChild(scr);
	// }
	function handleLoad() {
        if (!done) {
            done = true;
            // callback(path, "ok");
        }
    }

    function handleReadyStateChange() {
        var state;

        if (!done) {
            state = scr.readyState;
            if (state === "complete") {
                handleLoad();
            }
        }
    }
    function handleError() {
        if (!done) {
            done = true;
			console.log(path);
            // callback(path, "error");
        }
    }
}

// onload = function()
// {
// dhtmlLoadScript("dhtml_way.js");
// }

// </script> 


function GetPanel(mdl) {
    var cHtm = '<div id="home" align="center">';
    cHtm += mdl.htm;
    cHtm += '</div>';
    var TabAng = new Ext.Panel({
        id: mdl.id,
        closable: true,
        title: mdl.title,
        region: 'center', // this is what makes this panel into a region within the containing layout
        layout: 'fit',
        margins: '0 5 5 0',
        border: false,
        deferredRender: false,
        //html: cHtm,
        viewConfig: {
            forceFit: true
        }
    });
    // alert(TabAng.html);
    // TabAng.load();
    // TabAng.render();
    return TabAng;
};

function LoadReport(cTarget) {
    var mdl = { title: '', id: '', url: '', htm: '', havedialog: false };
    
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
    Ext.Ajax.request(
    {
        //url: "./home.mvc/getReport",
        url: baseURL + "index.php/main/getReport",
        params: {
            UserID: strUser,
            ModuleID: cTarget                      // parameter untuk url yang dituju (fungsi didalam controller)
        },
        success: function(o) {
        var cst = Ext.decode(o.responseText);
			if (cst.success === true )
            {
				mdl=cst;
				if (mdl.havedialog === true) {


					CurrentPage.id = mdl.id;
		 
					LoadScripts(mdl.htm);
					mainPage.doLayout();
				}
				else {

					ShowReport('', mdl.id, '');
				}
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
				 if (cst.expire=== true)
				 {
					window.location.href= WebAppUrl.UrlLogOff ;
				 }
            }
    

        }

    });
};
function ShowReport(cUser, cTarget, cParams) {
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/ExecReport",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },

        success: function(o) {
            var tmpurl = o.responseText;
            var res = tmpurl.slice(-128); //memotong eror code bawaan primaz
        
            var cst = Ext.decode(res);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url != '')
            {
                //ganti view php
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}

function GetPanelFrame(mdl) {
    var TabAng = new Ext.ux.IFrameComponent({
	//var TabAng = new Ext.Panel({
        id: mdl.id,
        shim: false,
        floating: true,
        autoLoad: false,
        closable: true,
        title: mdl.title,
        region: 'center', // this is what makes this panel into a region within the containing layout
        layout: 'fit',
        margins: '0 5 5 0',
        border: false,
        deferredRender: false,
        url: mdl.url,
        viewConfig: {
            forceFit: true
        }
    });

    return TabAng
};

function loadlaporanRWJ(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanRWJ/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        
        success: function(o) {
        	if(callback != undefined){
        		callback();
        	}
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
            	//ganti view php
            	
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}

function loadlaporanIGD(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanIGD/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        
        success: function(o) {
        	if(callback != undefined){
        		callback();
        	}
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
            	
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}

function loadlaporanRWI(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanRWI/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure:function(){
        	Ext.Msg.alert('Gagal','Akses Dibatalkan.');
    	 	if(callback != undefined)callback();
        },
        success: function(o) {
            if(callback != undefined)callback();
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}

function loadlaporanRadLab(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanRadLab/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        
        success: function(o) {
            if(callback != undefined){
        		callback();
        	}
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}

function loadlaporanAskep(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanAskep/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        
        success: function(o) {
        	if(callback != undefined){
        		callback();
        	}
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
            	//ganti view php
            	
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}


function loadlaporanApotek(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanApotek/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        
        success: function(o) {
            if(callback != undefined){
        		callback();
        	}
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}

function loadlaporanAnggaran(cUser,cTarget,cParams,callback)
{
    var mdl = { title: '', id: '', url: '', htm: '' };
    // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request

    Ext.Ajax.request({
        //url: "./Module.mvc/ExecReport",
        url: baseURL + "index.php/main/cetaklaporanAnggaran/cetaklaporan",
        params: {
            //UserID: 'Admin',
            UserID: cUser,
            ModuleID: cTarget,    // parameter untuk url yang dituju (fungsi didalam controller)
            Params: cParams    // parameter untuk url yang dituju (fungsi didalam controller)
        },
        
        success: function(o) {
            if(callback != undefined){
                callback();
            }
            var cst = Ext.decode(o.responseText);
            var mdl = { title: '', id: '', url: '', htm: '' };
            mdl.title = cst.title;
            mdl.id = cst.id;
            mdl.url = cst.url;

            if (cst.success === true && cst.url !== '')
            {
                //ganti view php
               window.open(mdl.url, '_blank', 'location=0,resizable=1', false);
            }
            else if (cst.success === false)
            {
               Ext.MessageBox.show
                (
                    {
                       title: cst.title,
                       msg:cst.msg,
                       width:250,
                       buttons: Ext.MessageBox.OK,
                       icon: Ext.MessageBox.INFO
                    }
                );
            }

      }
      
      
    });
}



