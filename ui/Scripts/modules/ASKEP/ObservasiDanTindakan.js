
var now = new Date();
var dsSpesialisasiObservasiDanTindakan;
var ListHasilObservasiDanTindakan;
var rowSelectedHasilObservasiDanTindakan;
var dsKelasObservasiDanTindakan;
var dsKamarObservasiDanTindakan;
var dataSource_ObservasiDanTindakan;
var dataSource_detObservasiDanTindakan;
var dsPerawatObservasiDanTindakan;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpkd_perawat;
var rowSelected_viODT;

CurrentPage.page = getPanelObservasiDanTindakan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelObservasiDanTindakan(mod_id) {
    var Field = ['TGL_MASUK', 'KD_PASIEN', 'NAMA', 'ALAMAT', 'SPESIALISASI', 'KELAS', 'NAMA_KAMAR', 'URUT_MASUK', 'KD_UNIT'];

    dataSource_ObservasiDanTindakan = new WebApp.DataStore({
        fields: Field
    });

    load_ObservasiDanTindakan("ng.AKHIR = 't' limit 50");
    var gridListHasilObservasiDanTindakan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_ObservasiDanTindakan,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viODT = dataSource_ObservasiDanTindakan.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viODT = dataSource_ObservasiDanTindakan.getAt(ridx);
                if (rowSelected_viODT !== undefined)
                {
                    HasilObservasiDanTindakanLookUp(rowSelected_viODT.data);
                }
                else
                {
                    HasilObservasiDanTindakanLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilObservasiDanTindakan',
                        header: 'Tgl. Masuk',
                        dataIndex: 'TGL_MASUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colMedrecViewHasilObservasiDanTindakan',
                        header: 'No. Medrec',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colNamaViewHasilObservasiDanTindakan',
                        header: 'Nama',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colAlamatViewHasilObservasiDanTindakan',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colSpesialisasiViewHasilObservasiDanTindakan',
                        header: 'Spesialisasi',
                        dataIndex: 'SPESIALISASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKelasViewHasilObservasiDanTindakan',
                        header: 'Kelas',
                        dataIndex: 'KELAS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilObservasiDanTindakan',
                        header: 'Kamar',
                        dataIndex: 'NAMA_KAMAR',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilObservasiDanTindakan',
                text: 'Open List',
                tooltip: 'Open List',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {

                    if (rowSelected_viODT !== undefined) {
                        HasilObservasiDanTindakanLookUp(rowSelected_viODT.data);
                    } else {
                        ShowPesanWarningObservasi('Pilih Data Terlebih Dahulu  ', 'Rencana Asuhan');
                    }
                }
            }]
    });
    var FormDepanObservasiDanTindakan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'ObservasiDanTindakan',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilObservasiDanTindakan
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('dtpTanggalakhirObservasiDanTindakan').disable();
                Ext.getCmp('dtpTanggalawalObservasiDanTindakan').disable();
            }
        }
    });
    return FormDepanObservasiDanTindakan;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCariMedrecObservasiDanTindakan',
                        id: 'TxtCariMedrecObservasiDanTindakan',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtCariMedrecObservasiDanTindakan').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(tmpNoMedrec);
                                                Ext.getCmp('TxtCariMedrecObservasiDanTindakan').setValue(tmpgetNoMedrec);
                                                var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                                load_ObservasiDanTindakan(tmpkriteriaObservasi);
                                            }
                                            else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                                    load_ObservasiDanTindakan(tmpkriteriaObservasi);
                                                }
                                                else
                                                    Ext.getCmp('TxtCariMedrecObservasiDanTindakan').setValue('');
                                            }
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPasienObservasiDanTindakan',
                        id: 'TxtCariNamaPasienObservasiDanTindakan',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                            load_ObservasiDanTindakan(tmpkriteriaObservasi);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiObservasiDanTindakan(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasObservasiDanTindakan(),
                    {
                        x: 260,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 310,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarObservasiDanTindakan(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalObservasiDanTindakan',
                        id: 'dtpTanggalawalObservasiDanTindakan', format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 260,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 320,
                        y: 130,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirObservasiDanTindakan',
                        id: 'dtpTanggalakhirObservasiDanTindakan',
                        format: 'd/M/Y',
                        value: now,
                        width: 120
                    },
                    {
                        x: 450,
                        y: 130,
                        xtype: 'checkbox',
                        id: 'chkTglObservasiDanTindakan',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalakhirObservasiDanTindakan').enable();
                                Ext.getCmp('dtpTanggalawalObservasiDanTindakan').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalakhirObservasiDanTindakan').disable();
                                Ext.getCmp('dtpTanggalawalObservasiDanTindakan').disable();
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataObservasiDanTindakan',
                        handler: function () {
                            var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                            load_ObservasiDanTindakan(tmpkriteriaObservasi);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilObservasiDanTindakanLookUp(rowdata) {
    FormLookUpdetailObservasiDanTindakan = new Ext.Window({
        id: 'gridHasilObservasiDanTindakan',
        title: 'Observasi & Tindakan',
        closeAction: 'destroy',
        width: 1000,
        height: 650,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupObservasiDanTindakan()],
        listeners: {
        }
    });
    FormLookUpdetailObservasiDanTindakan.show();
    if (rowdata === undefined) {

    } else {
        tmpkdpasien = rowdata.KD_PASIEN;
        tmpkdunit = rowdata.KD_UNIT;
        tmpurutmasuk = rowdata.URUT_MASUK;
        tmptglmasuk = rowdata.TGL_MASUK;
        datainit_ObservasiDanTindakan(rowdata);
        loadfilter_DetObservasiDanTindakan(rowdata);
    }

}
;
function formpopupObservasiDanTindakan() {
    var FrmTabs_popupObservasiDanTindakan = new Ext.Panel
            (
                    {
                        id: 'formpopupObservasiDanTindakan',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelObservasiDanTindakan(),
                                    TabPanelObservasiTindakan()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {
                                                        SavingData()
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftar',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaODT();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapObservasiTindakan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupObservasiDanTindakan;
}
;
function PanelObservasiDanTindakan() {
    var items = new Ext.Panel
            (
                    {
                        title: 'DataPasien',
                        id: 'PanelDataPasienObservasiDanTindakan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 956,
                                height: 130,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No. Medrec '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupMedrec',
                                        id: 'TxtPopupMedrec',
                                        width: 80,
                                        readOnly: true
                                    },
                                    {
                                        x: 210,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Nama '
                                    },
                                    {
                                        x: 270,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaPasien',
                                        id: 'TxtPopupNamaPasien',
                                        width: 200,
                                        readOnly: true
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Alamat '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupAlamatPasien',
                                        id: 'TxtPopupAlamatPasien',
                                        width: 500,
                                        readOnly: true
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Spesialisasi '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupSpesialisasi',
                                        id: 'TxtPopupSpesialisasi',
                                        width: 80,
                                        readOnly: true
                                    },
                                    {
                                        x: 210,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Kelas '
                                    },
                                    {x: 270,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupKelas',
                                        id: 'TxtPopupKelas',
                                        width: 200,
                                        readOnly: true
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Kamar '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100, xtype: 'textfield',
                                        name: 'TxtPopupKamarPasien',
                                        id: 'TxtPopupkamarPasien',
                                        width: 500,
                                        readOnly: true
                                    }
                                ]
                            }
                        ]
                    });
    return items;
}
;
function TabPanelObservasiTindakan() {
    var items = {
        xtype: 'tabpanel',
        plain: true,
        activeTab: 0,
        height: 430,
        width: 978,
        defaults:
                {
                    bodyStyle: 'padding:2px',
                    autoScroll: true
                },
        items: [
            {
                title: 'Input/Edit Data',
                layout: 'form',
                items:
                        [
                            paneleditObservasiDanTindakan()
                        ]
            },
            {
                title: "Daftar Data Observasi",
                layout: 'form',
                items:
                        [
                            PanelDataObservasiDanTindakan()
                        ]
            }
        ]
    };
    return items;
}
;

function paneleditObservasiDanTindakan()
{
    var paneleditdataObservasiDanTindakan = new Ext.Panel
            (
                    {
                        title: 'Input/Edit Data',
                        id: 'paneleditdataObservasiDanTindakan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tgl Pelaksanaan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'datefield',
                                                name: 'Tglpelaksanaan',
                                                id: 'Tglpelaksanaan',
                                                format: 'd/M/Y',
                                                value: now
                                            },
                                            {
                                                x: 250,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jam '
                                            },
                                            {
                                                x: 280,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 300,
                                                y: 5,
                                                xtype: 'timefield',
                                                name: 'Jampelaksanaan',
                                                id: 'Jampelaksanaan',
                                                format: 'H:i',
                                                increment: 1,
                                                width: 75
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Perawat '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            mComboPerawat()
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tekanan Darah '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txttekanandarah',
                                                width: 100
                                            },
                                            {
                                                x: 250,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Puke '
                                            },
                                            {
                                                x: 280,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 300,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtPuke',
                                                width: 80
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'CVP '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtCVP',
                                                width: 100
                                            },
                                            {
                                                x: 600,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nadi '
                                            },
                                            {
                                                x: 670,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 700,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtNadi',
                                                width: 100
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kesadaran '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtKesadaran',
                                                width: 245
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Suhu '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtSuhu',
                                                width: 100
                                            },
                                            {
                                                x: 600,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'WSD/DRAIN '
                                            },
                                            {
                                                x: 670,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 700,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtWSD/DRAIN',
                                                width: 100
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Perifer '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtPerifer',
                                                width: 456
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: true,
                                height: 40,
                                items:
                                        [
                                            {
                                                x: 5,
                                                y: 1,
                                                xtype: 'label',
                                                text: 'Intake '
                                            },
                                            {
                                                x: 10,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'Oral '
                                            },
                                            {
                                                x: 125,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 15,
                                                xtype: 'textfield',
                                                id: 'txtOral',
                                                width: 245
                                            },
                                            {
                                                x: 400,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'Parental '
                                            },
                                            {
                                                x: 470,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 15,
                                                xtype: 'textfield',
                                                id: 'txtParental',
                                                width: 100
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 5,
                                items:
                                        [
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: true,
                                height: 70,
                                items:
                                        [
                                            {
                                                x: 5,
                                                y: 1,
                                                xtype: 'label',
                                                text: 'Output '
                                            },
                                            {
                                                x: 10,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'Muntah '
                                            },
                                            {
                                                x: 125,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 15,
                                                xtype: 'numberfield',
                                                id: 'txtMuntah',
                                                width: 100
                                            },
                                            {
                                                x: 250,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'BAK '
                                            },
                                            {
                                                x: 280,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 300,
                                                y: 15,
                                                xtype: 'numberfield',
                                                id: 'txtBAK',
                                                width: 80
                                            },
                                            {
                                                x: 400,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'NGT '
                                            },
                                            {
                                                x: 470,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 15,
                                                xtype: 'numberfield',
                                                id: 'txtNGT',
                                                width: 100
                                            },
                                            {
                                                x: 600,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'BAB '
                                            },
                                            {
                                                x: 670,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 700,
                                                y: 15,
                                                xtype: 'numberfield',
                                                id: 'txtBAB',
                                                width: 100
                                            },
                                            {
                                                x: 10,
                                                y: 50,
                                                xtype: 'label',
                                                text: 'Pendarahan '
                                            },
                                            {
                                                x: 125,
                                                y: 50,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 45,
                                                xtype: 'numberfield',
                                                id: 'txtPendarahan',
                                                width: 100
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 126,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 20,
                                                xtype: 'label',
                                                text: 'Tindakan Keperawatan '
                                            },
                                            {
                                                x: 125,
                                                y: 20,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 15,
                                                xtype: 'textarea',
                                                id: 'txttindakankeperawatan',
                                                width: 824,
                                                height: 111
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return paneleditdataObservasiDanTindakan;
}
;
function PanelDataObservasiDanTindakan()
{
    var Field = ['KD_PASIEN_KUNJ', 'KD_UNIT_KUNJ', 'URUT_MASUK_KUNJ', 'TGL_MASUK_KUNJ', 'TGL_OBSERVASI', 'JAM_OBSERVASI',
        'KD_PERAWAT', 'NAMA_PERAWAT', 'TEKANAN_DARAH', 'NADI', 'DETAK_JANTUNG', 'SUHU', 'CVP', 'WSD', 'KESADARAN', 'PERIFER',
        'ORAL', 'PARENTERAL', 'MUNTAH', 'NGT', 'BAB', 'BAK', 'PENDARAHAN', 'TINDAKAN_PERAWAT'];

    dataSource_detObservasiDanTindakan = new WebApp.DataStore({
        fields: Field
    });
    var gridListDataObservasiDanTindakan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_detObservasiDanTindakan,
        anchor: '100% 100%',
        columnLines: false,
        autoScroll: true,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'form',
        region: 'center',
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglHasilRencana_Asuhan',
                        header: 'Tgl. Pelaksanaan',
                        dataIndex: 'TGL_OBSERVASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'coljamHasilRencana_Asuhan',
                        header: 'Jam',
                        dataIndex: 'JAM_OBSERVASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colPerawatHasilRencana_Asuhan',
                        header: 'Perawat',
                        dataIndex: 'NAMA_PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colTDHasilRencana_Asuhan',
                        header: 'TD',
                        dataIndex: 'TEKANAN_DARAH',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colNHasilRencana_Asuhan',
                        header: 'N',
                        dataIndex: 'NADI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colPHasilRencana_Asuhan',
                        header: 'P',
                        dataIndex: 'DETAK_JANTUNG',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colSHasilRencana_Asuhan',
                        header: 'S',
                        dataIndex: 'SUHU',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colCVPHasilRencana_Asuhan',
                        header: 'CVP',
                        dataIndex: 'CVP',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colWSDHasilRencana_Asuhan',
                        header: 'WSD/DRAIN',
                        dataIndex: 'WSD',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKESADARANHasilRencana_Asuhan',
                        header: 'Kesadaran',
                        dataIndex: 'KESADARAN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colPERIFERHasilRencana_Asuhan',
                        header: 'Perifer',
                        dataIndex: 'PERIFER',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colORALHasilRencana_Asuhan',
                        header: 'Oral',
                        dataIndex: 'ORAL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colPARENTERALHasilRencana_Asuhan',
                        header: 'Parental',
                        dataIndex: 'PARENTERAL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colMUNTAHHasilRencana_Asuhan',
                        header: 'Muntah',
                        dataIndex: 'MUNTAH',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colNGTHasilRencana_Asuhan',
                        header: 'NGT',
                        dataIndex: 'NGT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colBABHasilRencana_Asuhan',
                        header: 'BAB',
                        dataIndex: 'BAB',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colPENDARAHANHasilRencana_Asuhan',
                        header: 'Pendarahan',
                        dataIndex: 'PENDARAHAN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colTINDAKAN_PERAWATHasilRencana_Asuhan',
                        header: 'Tindakan Keperawatan',
                        dataIndex: 'TINDAKAN_PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 300
                    }
                ]
                ),
        viewConfig: {
            forceFit: false
        }
    });
    var PanelDataObservasiDanTindakan = new Ext.Panel
            (
                    {
                        title: "Data Rencana Asuhan",
                        id: 'PanelDataObservasiDanTindakan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        height: 400,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            gridListDataObservasiDanTindakan

                        ]
                    }
            );
    return PanelDataObservasiDanTindakan;
}
;

//combo dan load database

function datainit_ObservasiDanTindakan(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupSpesialisasi').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('TxtPopupKelas').setValue(rowdata.KELAS);
    Ext.getCmp('TxtPopupkamarPasien').setValue(rowdata.NAMA_KAMAR);
    caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetODT',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    tmpkd_perawat = '';
                                    Ext.getCmp('cboPerawatObservasiDanTindakan').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtPuke').setValue('');
                                    Ext.getCmp('txtCVP').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtKesadaran').setValue('');
                                    Ext.getCmp('txtSuhu').setValue('');
                                    Ext.getCmp('txtWSD/DRAIN').setValue('');
                                    Ext.getCmp('txtPerifer').setValue('');
                                    Ext.getCmp('txtOral').setValue('');
                                    Ext.getCmp('txtParental').setValue('');
                                    Ext.getCmp('txtMuntah').setValue('');
                                    Ext.getCmp('txtBAK').setValue('');
                                    Ext.getCmp('txtNGT').setValue('');
                                    Ext.getCmp('txtBAB').setValue('');
                                    Ext.getCmp('txtPendarahan').setValue('');
                                    Ext.getCmp('txttindakankeperawatan').setValue('');
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_OBSERVASI.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_OBSERVASI);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    tmpkd_perawat = tmphasil.KD_PERAWAT;
                                    Ext.getCmp('cboPerawatObservasiDanTindakan').setValue(tmphasil.NAMA_PERAWAT);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.TEKANAN_DARAH);
                                    Ext.getCmp('txtPuke').setValue(tmphasil.DETAK_JANTUNG);
                                    Ext.getCmp('txtCVP').setValue(tmphasil.CVP);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.NADI);
                                    Ext.getCmp('txtKesadaran').setValue(tmphasil.KESADARAN);
                                    Ext.getCmp('txtSuhu').setValue(tmphasil.SUHU);
                                    Ext.getCmp('txtWSD/DRAIN').setValue(tmphasil.WSD);
                                    Ext.getCmp('txtPerifer').setValue(tmphasil.PERIFER);
                                    Ext.getCmp('txtOral').setValue(tmphasil.ORAL);
                                    Ext.getCmp('txtParental').setValue(tmphasil.PARENTERAL);
                                    Ext.getCmp('txtMuntah').setValue(tmphasil.MUNTAH);
                                    Ext.getCmp('txtBAK').setValue(tmphasil.BAK);
                                    Ext.getCmp('txtNGT').setValue(tmphasil.NGT);
                                    Ext.getCmp('txtBAB').setValue(tmphasil.BAB);
                                    Ext.getCmp('txtPendarahan').setValue(tmphasil.PENDARAHAN);
                                    Ext.getCmp('txttindakankeperawatan').setValue(tmphasil.TINDAKAN_PERAWAT);
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            caridatapasien();
                            loadfilter_DetObservasiDanTindakan();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoObservasi('Proses Saving Berhasil', 'Save');
                                caridatapasien();
                                loadfilter_DetObservasiDanTindakan();
                            }
                            else
                            {
                                ShowPesanWarningObservasi('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                caridatapasien();
                                loadfilter_DetObservasiDanTindakan();
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{
    var params =
            {
                Table: 'ViewAskepODT',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien,
                KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                TGL_OBSERVASI: Ext.get('Tglpelaksanaan').getValue(),
                JAM_OBSERVASI: Ext.get('Jampelaksanaan').getValue(),
                KD_PERAWAT: tmpkd_perawat,
                TEKANAN_DARAH: Ext.get('txttekanandarah').getValue(),
                NADI: Ext.get('txtNadi').getValue(),
                DETAK_JANTUNG: Ext.get('txtPuke').getValue(),
                SUHU: Ext.get('txtSuhu').getValue(),
                CVP: Ext.get('txtCVP').getValue(),
                WSD: Ext.get('txtWSD/DRAIN').getValue(),
                KESADARAN: Ext.get('txtKesadaran').getValue(),
                PERIFER: Ext.get('txtPerifer').getValue(),
                ORAL: Ext.get('txtOral').getValue(),
                PARENTERAL: Ext.get('txtParental').getValue(),
                MUNTAH: Ext.get('txtMuntah').getValue(),
                NGT: Ext.get('txtNGT').getValue(),
                BAB: Ext.get('txtBAB').getValue(),
                BAK: Ext.get('txtBAK').getValue(),
                PENDARAHAN: Ext.get('txtPendarahan').getValue(),
                TINDAKAN_PERAWAT: Ext.get('txttindakankeperawatan').getValue()
            };
    return params;
}

function mComboSpesialisasiObservasiDanTindakan()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiObservasiDanTindakan = new WebApp.DataStore({fields: Field});
    dsSpesialisasiObservasiDanTindakan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiObservasiDanTindakan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiObservasiDanTindakan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiObservasiDanTindakan,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasObservasiDanTindakan').setValue('');
                                        Ext.getCmp('cboKamarObservasiDanTindakan').setValue('');
                                        loaddatastoreKelasObservasiDanTindakan(b.data.KD_SPESIAL);
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_ObservasiDanTindakan(tmpkriteriaObservasi);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasObservasiDanTindakan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiObservasiDanTindakan;
}
;
function loaddatastoreKelasObservasiDanTindakan(kd_spesial)
{
    dsKelasObservasiDanTindakan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasObservasiDanTindakan()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasObservasiDanTindakan = new WebApp.DataStore({fields: Field});
    var cboKelasObservasiDanTindakan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasObservasiDanTindakan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasObservasiDanTindakan,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarObservasiDanTindakan').setValue('');
                                        loaddatastoreKamarObservasiDanTindakan(b.data.kd_unit);
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_ObservasiDanTindakan(tmpkriteriaObservasi);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarObservasiDanTindakan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasObservasiDanTindakan;
}
;
function loaddatastoreKamarObservasiDanTindakan(kd_unit)
{
    dsKamarObservasiDanTindakan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiObservasiDanTindakan').getValue()
                                }
                    });
}

function mComboKamarObservasiDanTindakan()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarObservasiDanTindakan = new WebApp.DataStore({fields: Field});
    var cboKamarObservasiDanTindakan = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarObservasiDanTindakan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarObservasiDanTindakan,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaObservasi = getCriteriaFilter_viDaftar();
                                        load_ObservasiDanTindakan(tmpkriteriaObservasi);
                                    }
                                }
                    }
            );
    return cboKamarObservasiDanTindakan;
}
;
function load_ObservasiDanTindakan(criteria)
{
    dataSource_ObservasiDanTindakan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepPengkajian',
                                    param: criteria
                                }
                    }
            );
    return dataSource_ObservasiDanTindakan;
}

function loadfilter_DetObservasiDanTindakan(rowdata)
{
    var criteria = '';
    if (rowdata !== undefined) {
        criteria = "kd_pasien_kunj = '" + rowdata.KD_PASIEN + "' and kd_unit_kunj = '" + rowdata.KD_UNIT +
                "' and urut_masuk_kunj = " + rowdata.URUT_MASUK + " and tgl_masuk_kunj = '" + rowdata.TGL_MASUK + "'";
    } else {
        criteria = "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
    }
    dataSource_detObservasiDanTindakan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetODT',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detObservasiDanTindakan;
}


function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecObservasiDanTindakan').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienObservasiDanTindakan').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiObservasiDanTindakan').getValue();
    var tmpkelas = Ext.getCmp('cboKelasObservasiDanTindakan').getValue();
    var tmpkamar = Ext.getCmp('cboKamarObservasiDanTindakan').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalObservasiDanTindakan').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirObservasiDanTindakan').getValue();
    var tmptambahan = Ext.getCmp('chkTglObservasiDanTindakan').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatObservasiDanTindakan = new WebApp.DataStore({fields: Field});
    dsPerawatObservasiDanTindakan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatObservasiDanTindakan = new Ext.form.ComboBox
            (
                    {
                        x: 490,
                        y: 5,
                        id: 'cboPerawatObservasiDanTindakan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatObservasiDanTindakan,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkd_perawat = Ext.getCmp('cboPerawatObservasiDanTindakan').getValue();
                                    }
                                }
                    }
            );
    return cboPerawatObservasiDanTindakan;
}
;

function ShowPesanWarningObservasi(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoObservasi(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningObservasi('Hubungi Admin', 'Error');
                            loadfilter_DetObservasiDanTindakan();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoObservasi('Data berhasil di hapus', 'Information');
                                caridatapasien();
                                loadfilter_DetObservasiDanTindakan();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningObservasi('Gagal menghapus data', 'Error');
                                caridatapasien();
                                loadfilter_DetObservasiDanTindakan();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepODT',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function GetCriteriaODT()
{
    var strKriteria = '';
    strKriteria = tmpkdpasien;
    strKriteria += '##@@##' + tmpkdunit;
    strKriteria += '##@@##' + tmpurutmasuk;
    strKriteria += '##@@##' + tmptglmasuk;
    strKriteria += '##@@##' + Ext.get('TxtPopupMedrec').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupNamaPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupAlamatPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupSpesialisasi').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupKelas').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupkamarPasien').getValue();
    return strKriteria;
}