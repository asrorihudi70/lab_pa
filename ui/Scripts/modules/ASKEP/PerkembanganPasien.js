
var now = new Date();
var dsSpesialisasiPerkembanganPasien;
var ListHasilPerkembanganPasien;
var rowSelectedHasilPerkembanganPasien;
var dsKelasPerkembanganPasien;
var dsKamarPerkembanganPasien;
var dataSource_PerkembanganPasien;
var dataSource_DetPerkembanganPasien;
var dsPerawatPerkembanganPasien;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var kd_perawat;
var rowSelected_viSOAP;
var kdPasien;
var kdUnit;
var urutMasuk;
var tglMasuk;
var tampungjam;

CurrentPage.page = getPanelPerkembanganPasien(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelPerkembanganPasien(mod_id) {
    var Field = ['TGL_MASUK', 'KD_PASIEN', 'NAMA', 'ALAMAT', 'SPESIALISASI', 'KELAS', 'NAMA_KAMAR', 'URUT_MASUK', 'KD_UNIT'];
    dataSource_PerkembanganPasien = new WebApp.DataStore({
        fields: Field
    });
    load_PerkembanganPasien("ng.AKHIR = 't' limit 50");
    var gridListHasilPerkembanganPasien = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_PerkembanganPasien,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viSOAP = dataSource_PerkembanganPasien.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viSOAP = dataSource_PerkembanganPasien.getAt(ridx);
                if (rowSelected_viSOAP !== undefined)
                {
                    HasilPerkembanganPasienLookUp(rowSelected_viSOAP.data);
                }
                else
                {
                    HasilPerkembanganPasienLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilPerkembanganPasien',
                        header: 'Tgl. Masuk',
                        dataIndex: 'TGL_MASUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colMedrecViewHasilPerkembanganPasien',
                        header: 'No. Medrec',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colNamaViewHasilPerkembanganPasien',
                        header: 'Nama',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colAlamatViewHasilPerkembanganPasien',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colSpesialisasiViewHasilPerkembanganPasien',
                        header: 'Spesialisasi',
                        dataIndex: 'SPESIALISASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKelasViewHasilPerkembanganPasien',
                        header: 'Kelas',
                        dataIndex: 'KELAS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilPerkembanganPasien',
                        header: 'Kamar',
                        dataIndex: 'NAMA_KAMAR',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilPerkembanganPasien',
                text: 'Open List',
                tooltip: 'Open List',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    if (dataSource_PerkembanganPasien !== undefined) {
                        HasilPerkembanganPasienLookUp(dataSource_PerkembanganPasien.data);
                    } else {
                        ShowPesanWarningObservasi('Pilih Data Terlebih Dahulu  ', 'Rencana Asuhan');
                    }
                }
            }]
    });
    var FormDepanPerkembanganPasien = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'PerkembanganPasien',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilPerkembanganPasien
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('dtpTanggalakhirPerkembanganPasien').disable();
                Ext.getCmp('dtpTanggalawalPerkembanganPasien').disable();
            }
        }
    });
    return FormDepanPerkembanganPasien;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCariMedrecPerkembanganPasien',
                        id: 'TxtCariMedrecPerkembanganPasien',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtCariMedrecPerkembanganPasien').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(tmpNoMedrec);
                                                Ext.getCmp('TxtCariMedrecPerkembanganPasien').setValue(tmpgetNoMedrec);
                                                var tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                                                load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                                            }
                                            else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                                                    load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                                                }
                                                else
                                                    Ext.getCmp('TxtCariMedrecPerkembanganPasien').setValue('');
                                            }
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPasienPerkembanganPasien',
                        id: 'TxtCariNamaPasienPerkembanganPasien',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                                            load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiPerkembanganPasien(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasPerkembanganPasien(),
                    {
                        x: 260,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 310,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarPerkembanganPasien(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalPerkembanganPasien',
                        id: 'dtpTanggalawalPerkembanganPasien', format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 260,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 320,
                        y: 130,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirPerkembanganPasien',
                        id: 'dtpTanggalakhirPerkembanganPasien',
                        format: 'd/M/Y',
                        value: now,
                        width: 120
                    },
                    {
                        x: 450,
                        y: 130,
                        xtype: 'checkbox',
                        id: 'chkTglPerkembanganPasien',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalakhirPerkembanganPasien').enable();
                                Ext.getCmp('dtpTanggalawalPerkembanganPasien').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalakhirPerkembanganPasien').disable();
                                Ext.getCmp('dtpTanggalawalPerkembanganPasien').disable();
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataPerkembanganPasien',
                        handler: function () {
                            var tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                            load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilPerkembanganPasienLookUp(rowdata) {
    FormLookUpdetailPerkembanganPasien = new Ext.Window({
        id: 'gridHasilPerkembanganPasien',
        title: 'Perkembangan Pasien (SOAP)',
        closeAction: 'destroy',
        width: 1000,
        height: 650,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupPerkembanganPasien()],
        listeners: {
        }
    });
    FormLookUpdetailPerkembanganPasien.show();
    if (rowdata === undefined) {

    } else {
        tmpkdpasien = rowdata.KD_PASIEN;
        tmpkdunit = rowdata.KD_UNIT;
        tmpurutmasuk = rowdata.URUT_MASUK;
        tmptglmasuk = rowdata.TGL_MASUK;
        datainit_PerkembanganPasien(rowdata);
        load_DetPerkembanganPasien(rowdata);
    }

}
;
function formpopupPerkembanganPasien() {
    var FrmTabs_popupPerkembanganPasien = new Ext.Panel
            (
                    {
                        id: 'formpopupPerkembanganPasien',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelPerkembanganPasien(),
                                    paneleditPerkembanganPasien(),
                                    PanelDataPerkembanganPasien()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {
                                                        SavingData();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftar',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaSoap();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapSOAP', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupPerkembanganPasien;
}
;
function PanelPerkembanganPasien() {
    var items = new Ext.Panel
            (
                    {
                        title: 'DataPasien',
                        id: 'PanelDataPasienPerkembanganPasien',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 956,
                                height: 130,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No. Medrec '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupMedrec',
                                        id: 'TxtPopupMedrec',
                                        width: 80
                                    },
                                    {
                                        x: 210,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Nama '
                                    },
                                    {
                                        x: 270,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaPasien',
                                        id: 'TxtPopupNamaPasien',
                                        width: 200
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Alamat '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupAlamatPasien',
                                        id: 'TxtPopupAlamatPasien',
                                        width: 500
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Spesialisasi '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupSpesialisasi',
                                        id: 'TxtPopupSpesialisasi',
                                        width: 80
                                    },
                                    {
                                        x: 210,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Kelas '
                                    },
                                    {x: 270,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupKelas',
                                        id: 'TxtPopupKelas',
                                        width: 200
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Kamar '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100, xtype: 'textfield',
                                        name: 'TxtPopupKamarPasien',
                                        id: 'TxtPopupkamarPasien',
                                        width: 500
                                    }
                                ]
                            }
                        ]
                    });
    return items;
}
;
function paneleditPerkembanganPasien()
{
    var paneleditdataPerkembanganPasien = new Ext.Panel
            (
                    {
                        title: 'Input/Edit Data',
                        id: 'paneleditdataPerkembanganPasien',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tgl Pelaksanaan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'datefield',
                                                name: 'Tglpelaksanaan',
                                                id: 'Tglpelaksanaan',
                                                format: 'd/M/Y',
                                                value: now
                                            },
                                            {
                                                x: 250,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jam '
                                            },
                                            {
                                                x: 280,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 300,
                                                y: 5,
                                                xtype: 'timefield',
                                                name: 'Jampelaksanaan',
                                                id: 'Jampelaksanaan',
//                                                minValue: '8:00 AM',
//                                                maxValue: '6:00 PM',
                                                increment: 5,
                                                width: 80
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tgl Selesai '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
//                                            
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tgl Selesai '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'datefield',
                                                name: 'TglSelesai',
                                                id: 'TglSelesai',
                                                format: 'd/M/Y',
                                                value: now
                                            },
                                            {
                                                x: 600,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Perawat '
                                            },
                                            {
                                                x: 650,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            mComboPerawat()
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Subject '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'txtSPerkembanganPasien',
                                                width: 245,
                                                height: 50
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Object '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'txtOPerkembanganPasien',
                                                width: 245,
                                                height: 50
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Assusment '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'txtAPerkembanganPasien',
                                                width: 245,
                                                height: 50
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Planing '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'txtPPerkembanganPasien',
                                                width: 245,
                                                height: 50
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return paneleditdataPerkembanganPasien;
}
;
function PanelDataPerkembanganPasien()
{
    var Field = ['KD_PASIEN_KUNJ', 'KD_UNIT_KUNJ', 'URUT_MASUK_KUNJ', 'TGL_MASUK_KUNJ', 'TGL_SOAP',
        'JAM_SOAP', 'KD_PERAWAT', 'NAMA_PERAWAT', 'SUBJECT', 'OBJECT', 'ASSUSMENT', 'PLANING', ];
    dataSource_DetPerkembanganPasien = new WebApp.DataStore({
        fields: Field
    });
    var gridListDataDetPerkembanganPasien = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_DetPerkembanganPasien,
        anchor: '100% 100%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglHasilPerkembanganPasien',
                        header: 'Tgl. Pelaksanaan',
                        dataIndex: 'TGL_SOAP',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colJamHasilPerkembanganPasien',
                        header: 'Jam',
                        dataIndex: 'JAM_SOAP',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colPerawatHasilPerkembanganPasien',
                        header: 'Perawat',
                        dataIndex: 'NAMA_PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 150
                    },
                    {
                        id: 'colSUBJECTHasilPerkembanganPasien',
                        header: 'Subject',
                        dataIndex: 'SUBJECT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colOBJECTHasilPerkembanganPasien',
                        header: 'Object',
                        dataIndex: 'OBJECT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colASSUSMENTHasilPerkembanganPasien',
                        header: 'Assusment',
                        dataIndex: 'ASSUSMENT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colPLANINGHasilPerkembanganPasien',
                        header: 'Planing',
                        dataIndex: 'PLANING',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    }
                ]
                ),
        viewConfig: {
            forceFit: false
        }
    });
    var PanelDataPerkembanganPasien = new Ext.Panel
            (
                    {
                        title: "Data Rencana Asuhan",
                        id: 'PanelDataPerkembanganPasien',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        height: 250,
                        items: [
                            gridListDataDetPerkembanganPasien

                        ]
                    }
            );
    return PanelDataPerkembanganPasien;
}
;
//combo dan load database

function datainit_PerkembanganPasien(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupSpesialisasi').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('TxtPopupKelas').setValue(rowdata.KELAS);
    Ext.getCmp('TxtPopupkamarPasien').setValue(rowdata.NAMA_KAMAR);
    caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewDetSoap',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('Tglpelaksanaan').setValue(now);
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    Ext.getCmp('TglSelesai').setValue(now);
                                    Ext.getCmp('cboPerawatPerkembanganPasien').setValue('');
                                    Ext.getCmp('txtSPerkembanganPasien').setValue('');
                                    Ext.getCmp('txtOPerkembanganPasien').setValue('');
                                    Ext.getCmp('txtAPerkembanganPasien').setValue('');
                                    Ext.getCmp('txtPPerkembanganPasien').setValue('');
                                    kd_perawat = '';
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM_SOAP.substring(0, 5);
                                    Ext.getCmp('Tglpelaksanaan').setValue(tmphasil.TGL_SOAP);
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    Ext.getCmp('TglSelesai').setValue(tmphasil.TGL_SOAP);
                                    Ext.getCmp('cboPerawatPerkembanganPasien').setValue(tmphasil.NAMA_PERAWAT);
                                    Ext.getCmp('txtSPerkembanganPasien').setValue(tmphasil.SUBJECT);
                                    Ext.getCmp('txtOPerkembanganPasien').setValue(tmphasil.OBJECT);
                                    Ext.getCmp('txtAPerkembanganPasien').setValue(tmphasil.ASSUSMENT);
                                    Ext.getCmp('txtPPerkembanganPasien').setValue(tmphasil.PLANING);
                                    kd_perawat = tmphasil.KD_PERAWAT;
                                }

                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningSoap('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            caridatapasien();
                            load_DetPerkembanganPasien();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoSoap('Proses Saving Berhasil', 'Save');
                                caridatapasien();
                                load_DetPerkembanganPasien();
                            }
                            else
                            {
                                ShowPesanWarningSoap('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                caridatapasien();
                                load_DetPerkembanganPasien();
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{
    var params =
            {
                Table: 'ViewSoap',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien,
                KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                TGL_SOAP: Ext.get('Tglpelaksanaan').getValue(),
                JAM_SOAP: Ext.get('Jampelaksanaan').getValue(),
                KD_PERAWAT: kd_perawat,
                SUBJECT: Ext.get('txtSPerkembanganPasien').getValue(),
                OBJECT: Ext.get('txtOPerkembanganPasien').getValue(),
                ASSUSMENT: Ext.get('txtAPerkembanganPasien').getValue(),
                PLANING: Ext.get('txtPPerkembanganPasien').getValue()
            };
    return params;
}

function mComboSpesialisasiPerkembanganPasien()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiPerkembanganPasien = new WebApp.DataStore({fields: Field});
    dsSpesialisasiPerkembanganPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiPerkembanganPasien = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiPerkembanganPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiPerkembanganPasien,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasPerkembanganPasien').setValue('');
                                        Ext.getCmp('cboKamarPerkembanganPasien').setValue('');
                                        loaddatastoreKelasPerkembanganPasien(b.data.KD_SPESIAL);
                                        var tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                                        load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasPerkembanganPasien').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiPerkembanganPasien;
}
;
function loaddatastoreKelasPerkembanganPasien(kd_spesial)
{
    dsKelasPerkembanganPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasPerkembanganPasien()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasPerkembanganPasien = new WebApp.DataStore({fields: Field});
    var cboKelasPerkembanganPasien = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasPerkembanganPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasPerkembanganPasien,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarPerkembanganPasien').setValue('');
                                        loaddatastoreKamarPerkembanganPasien(b.data.kd_unit);
                                        var tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                                        load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarPerkembanganPasien').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasPerkembanganPasien;
}
;
function loaddatastoreKamarPerkembanganPasien(kd_unit)
{
    dsKamarPerkembanganPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiPerkembanganPasien').getValue()
                                }
                    });
}

function mComboKamarPerkembanganPasien()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarPerkembanganPasien = new WebApp.DataStore({fields: Field});
    var cboKamarPerkembanganPasien = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarPerkembanganPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarPerkembanganPasien,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaPerkembanganPasien = getCriteriaFilter_viDaftar();
                                        load_PerkembanganPasien(tmpkriteriaPerkembanganPasien);
                                    }
                                }
                    }
            );
    return cboKamarPerkembanganPasien;
}
;
function load_PerkembanganPasien(criteria)
{
    dataSource_PerkembanganPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepPengkajian',
                                    param: criteria
                                }
                    }
            );
    return dataSource_PerkembanganPasien;
}

function load_DetPerkembanganPasien(rowdata)
{
    var criteria = '';
    if (rowdata !== undefined) {
        criteria = "kd_pasien_kunj = '" + rowdata.KD_PASIEN + "' and kd_unit_kunj = '" + rowdata.KD_UNIT +
                "' and urut_masuk_kunj = " + rowdata.URUT_MASUK + " and tgl_masuk_kunj = '" + rowdata.TGL_MASUK + "'";
    } else {
        criteria = "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
    }
    dataSource_DetPerkembanganPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetSoap',
                                    param: criteria
                                }
                    }
            );
    return dataSource_DetPerkembanganPasien;
}

function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecPerkembanganPasien').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienPerkembanganPasien').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiPerkembanganPasien').getValue();
    var tmpkelas = Ext.getCmp('cboKelasPerkembanganPasien').getValue();
    var tmpkamar = Ext.getCmp('cboKamarPerkembanganPasien').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalPerkembanganPasien').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirPerkembanganPasien').getValue();
    var tmptambahan = Ext.getCmp('chkTglPerkembanganPasien').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatPerkembanganPasien = new WebApp.DataStore({fields: Field});
    dsPerawatPerkembanganPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatPerkembanganPasien = new Ext.form.ComboBox
            (
                    {
                        x: 670,
                        y: 5,
                        id: 'cboPerawatPerkembanganPasien',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatPerkembanganPasien,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        kd_perawat = Ext.getCmp('cboPerawatPerkembanganPasien').getValue();
                                    }

                                }
                    }
            );
    return cboPerawatPerkembanganPasien;
}
;

function ShowPesanWarningSoap(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoSoap(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningSoap('Hubungi Admin', 'Error');
                            load_DetPerkembanganPasien();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoSoap('Data berhasil di hapus', 'Information');
                                caridatapasien();
                                load_DetPerkembanganPasien();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningSoap('Gagal menghapus data', 'Error');
                                caridatapasien();
                                load_DetPerkembanganPasien();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewSoap',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function GetCriteriaSoap()
{
    var strKriteria = '';
    strKriteria = tmpkdpasien;
    strKriteria += '##@@##' + tmpkdunit;
    strKriteria += '##@@##' + tmpurutmasuk;
    strKriteria += '##@@##' + tmptglmasuk;
    strKriteria += '##@@##' + Ext.get('TxtPopupMedrec').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupNamaPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupAlamatPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupSpesialisasi').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupKelas').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupkamarPasien').getValue();
    return strKriteria;
}
