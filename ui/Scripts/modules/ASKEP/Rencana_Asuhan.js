
var now = new Date();
var dsSpesialisasiRencana_Asuhan;
var rowSelected_viRencana;
var ListHasilRencana_Asuhan;
var rowSelectedHasilRencana_Asuhan;
var dsKelasRencana_Asuhan;
var dsKamarRencana_Asuhan;
var dataSource_Rencana_Asuhan;
var dataSource_RencanaAsuhan;
var dsPerawatRencana_Asuhan;
var dataSource_Rencana_Asuhan_detail;
var idperawat;
var kdPasien;
var kdUnit;
var urutMasuk;
var tglMasuk;
var tampungjam;



CurrentPage.page = getPanelRencana_Asuhan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelRencana_Asuhan(mod_id) {
    var Field = ['TGL_MASUK', 'KD_PASIEN', 'NAMA', 'ALAMAT', 'SPESIALISASI', 'KELAS', 'NAMA_KAMAR', 'URUT_MASUK', 'KD_UNIT', 'JAM'];

    dataSource_Rencana_Asuhan = new WebApp.DataStore({
        fields: Field
    });

    load_Rencana_Asuhan("ng.AKHIR = 't' limit 50");
    var gridListHasilRencana_Asuhan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Rencana_Asuhan,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viRencana = dataSource_Rencana_Asuhan.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viRencana = dataSource_Rencana_Asuhan.getAt(ridx);
                if (rowSelected_viRencana !== undefined)
                {
                    HasilRencana_AsuhanLookUp(rowSelected_viRencana.data);
                }
                else
                {
                    HasilRencana_AsuhanLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilRencana_Asuhan',
                        header: 'Tgl. Masuk',
                        dataIndex: 'TGL_MASUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colMedrecViewHasilRencana_Asuhan',
                        header: 'No. Medrec',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colNamaViewHasilRencana_Asuhan',
                        header: 'Nama',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colAlamatViewHasilRencana_Asuhan',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colSpesialisasiViewHasilRencana_Asuhan',
                        header: 'Spesialisasi',
                        dataIndex: 'SPESIALISASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKelasViewHasilRencana_Asuhan',
                        header: 'Kelas',
                        dataIndex: 'KELAS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilRencana_Asuhan',
                        header: 'Kamar',
                        dataIndex: 'NAMA_KAMAR',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilRencana_Asuhan',
                text: 'Open List',
                tooltip: 'Open List',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    if (rowSelected_viRencana !== undefined) {
                        HasilRencana_AsuhanLookUp(rowSelected_viRencana.data);
                    } else {
                        ShowPesanWarningRA('Pilih Data Terlebih Dahulu  ', 'Rencana Asuhan');
                    }
                }
            }
        ]
    });
    var FormDepanRencana_Asuhan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Rencana_Asuhan',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilRencana_Asuhan
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('dtpTanggalakhirRencanaAsuhan').disable();
                Ext.getCmp('dtpTanggalawalRencanaAsuhan').disable();
            }
        }
    });
    return FormDepanRencana_Asuhan;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCariMedrecRencanaAsuhan',
                        id: 'TxtCariMedrecRencanaAsuhan',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtCariMedrecRencanaAsuhan').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(tmpNoMedrec);
                                                Ext.getCmp('TxtCariMedrecRencanaAsuhan').setValue(tmpgetNoMedrec);
                                                var tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                                                load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                                            }
                                            else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                                                    load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                                                }
                                                else
                                                    Ext.getCmp('TxtCariMedrecRencanaAsuhan').setValue('');
                                            }
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPasienRencanaAsuhan',
                        id: 'TxtCariNamaPasienRencanaAsuhan',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                                            load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiRencana_Asuhan(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasRencana_Asuhan(),
                    {
                        x: 260,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 310,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarRencana_Asuhan(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalRencanaAsuhan',
                        id: 'dtpTanggalawalRencanaAsuhan', format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 260,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 320,
                        y: 130,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirRencanaAsuhan',
                        id: 'dtpTanggalakhirRencanaAsuhan',
                        format: 'd/M/Y',
                        value: now,
                        width: 120
                    },
                    {
                        x: 450,
                        y: 130,
                        xtype: 'checkbox',
                        id: 'chkTglRencanaAsuhan',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalakhirRencanaAsuhan').enable();
                                Ext.getCmp('dtpTanggalawalRencanaAsuhan').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalakhirRencanaAsuhan').disable();
                                Ext.getCmp('dtpTanggalawalRencanaAsuhan').disable();
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataRencana_Asuhan',
                        handler: function () {
                            var tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                            load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilRencana_AsuhanLookUp(rowdata) {

    FormLookUpdetailRencana_Asuhan = new Ext.Window({
        id: 'gridHasilRencana_Asuhan',
        title: 'Rencana Asuhan',
        closeAction: 'destroy',
        width: 1000,
        height: 650,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupRencana_Asuhan(rowdata)],
        listeners: {
        }
    });
    FormLookUpdetailRencana_Asuhan.show();
    if (rowdata === undefined) {

    } else {
        kdPasien = rowdata.KD_PASIEN;
        kdUnit = rowdata.KD_UNIT;
        urutMasuk = rowdata.URUT_MASUK;
        tglMasuk = rowdata.TGL_MASUK;
        load_Rencana_Asuhan_detail(rowdata);
        loadData_RencanaAsuhan(rowdata);
    }

}
;
function formpopupRencana_Asuhan(rowdata) {
    var FrmTabs_popupRencana_Asuhan = new Ext.Panel
            (
                    {
                        id: 'formpopupRencana_Asuhan',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: false,
                        shadhow: true,
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelRencana_Asuhan(),
                                    paneleditrencanaAsuhan(rowdata),
                                    PanelDataRencanaAsuhan()
                                ],
                        listeners: {
                            'afterrender': function () {
                                Ext.getCmp('dtpTanggalakhirRencanaAsuhan').disable();
                                Ext.getCmp('dtpTanggalawalRencanaAsuhan').disable();
                            }
                        },
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftarASKEP',
                                                    handler: function ()
                                                    {
                                                        saveAskepPerencanaan();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftarASKEP',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftarASKEP',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaRencanaAsuhan();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapRencanaAsuhan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupRencana_Asuhan;
}
;
function PanelRencana_Asuhan() {
    var items = new Ext.Panel
            (
                    {
                        title: 'DataPasien',
                        id: 'PanelDataPasienRencana_Asuhan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 956,
                                height: 130,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No. Medrec '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupMedrec',
                                        id: 'TxtPopupMedrec',
                                        width: 80,
                                        readOnly: true
                                    },
                                    {
                                        x: 210,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Nama '
                                    },
                                    {
                                        x: 270,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaPasien',
                                        id: 'TxtPopupNamaPasien',
                                        width: 200,
                                        readOnly: true
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Alamat '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupAlamatPasien',
                                        id: 'TxtPopupAlamatPasien',
                                        width: 500,
                                        readOnly: true
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Spesialisasi '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupSpesialisasi',
                                        id: 'TxtPopupSpesialisasi',
                                        width: 80,
                                        readOnly: true
                                    },
                                    {
                                        x: 210,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Kelas '
                                    },
                                    {x: 270,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 280,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupKelas',
                                        id: 'TxtPopupKelas',
                                        width: 200,
                                        readOnly: true
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Kamar '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100, xtype: 'textfield',
                                        name: 'TxtPopupKamarPasien',
                                        id: 'TxtPopupkamarPasien',
                                        width: 500,
                                        readOnly: true
                                    }
                                ]
                            }
                        ]
                    });
    return items;
}
;
function paneleditrencanaAsuhan(rowdata)
{
    var paneleditdatarencanaAsuhan = new Ext.Panel
            (
                    {
                        title: 'Input/Edit Data',
                        id: 'paneleditdatarencanaAsuhan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tgl Pelaksanaan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'datefield',
                                                name: 'Tglpelaksanaan',
                                                id: 'Tglpelaksanaan',
                                                format: 'd/M/Y',
                                                value: now
                                            },
                                            {
                                                x: 250,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jam '
                                            },
                                            {
                                                x: 280,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 300,
                                                y: 5,
                                                xtype: 'timefield',
                                                name: 'Jampelaksanaan',
                                                id: 'Jampelaksanaan',
                                                enableKeyEvents: true,
//                                                minValue: '08:00',
//                                                maxValue: '18:00',
                                                format: 'H:i',
                                                increment: 1,
                                                width: 75
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tgl Selesai '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'datefield',
                                                name: 'TglSelesaiPelaksanaan',
                                                id: 'TglSelesaiPelaksanaan',
                                                format: 'd/M/Y',
                                                value: now
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Diagnosa Keperawatan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                fieldLabel: 'Diagnosa Keperawatan',
                                                name: 'txtDiagnosaKeperawatan',
                                                id: 'txtkeluhatxtDiagnosaKeperawatannutama',
                                                height: 50,
                                                width: 841
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tujuan/Kriteria Hasil '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                fieldLabel: 'Tujuan/Kriteria Hasil',
                                                name: 'txtTujuanKriteriahasil',
                                                id: 'txtTujuanKriteriahasil',
                                                height: 50,
                                                width: 841
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Rencana Tindakan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                fieldLabel: 'Rencana Tindakan',
                                                name: 'txtRencanaTindakan',
                                                id: 'txtRencanaTindakan',
                                                height: 50,
                                                width: 841
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Perawat '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            mComboPerawat()
                                        ]
                            }
                        ]
                    }
            );
    return paneleditdatarencanaAsuhan;
}
;

function PanelDataRencanaAsuhan()
{
    var Field = ['KD_PASIEN_KUNJ', 'KD_UNIT_KUNJ', 'URUT_MASUK_KUNJ',
        'TGL_NCP', 'JAM', 'DIAGNOSA', 'TUJUAN', 'RENCANA',
        'KD_PERAWAT', 'TGL_SELESAI', 'NAMA_PERAWAT'];

    dataSource_RencanaAsuhan = new WebApp.DataStore({
        fields: Field
    });
    var gridListDataRencana_Asuhan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_RencanaAsuhan,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 500,
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilRencana_Asuhan',
                        header: 'Tgl. Pelaksanaan',
                        dataIndex: 'TGL_NCP',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colJamViewHasilRencana_Asuhan',
                        header: 'Jam',
                        dataIndex: 'JAM',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25
                    },
                    {
                        id: 'colTglSelesaiViewHasilRencana_Asuhan',
                        header: 'Tgl. Selesai',
                        dataIndex: 'TGL_SELESAI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colDiagnosaViewHasilRencana_Asuhan',
                        header: 'Diagnosa',
                        dataIndex: 'DIAGNOSA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colTujuan/KriteriaViewHasilRencana_Asuhan',
                        header: 'Tujuan/Kriteria Hasil',
                        dataIndex: 'TUJUAN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colRencanaViewHasilRencana_Asuhan',
                        header: 'Rencana',
                        dataIndex: 'RENCANA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colPerawatViewHasilRencana_Asuhan',
                        header: 'Perawat',
                        dataIndex: 'NAMA_PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        }
    });
    var PanelDataRencanaAsuhan = new Ext.Panel
            (
                    {
                        title: "Data Rencana Asuhan",
                        id: 'PanelDataRencanaAsuhan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            gridListDataRencana_Asuhan

                        ]
                    }
            );
    return PanelDataRencanaAsuhan;
}
;

//combo dan load database
function mComboSpesialisasiRencana_Asuhan()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiRencana_Asuhan = new WebApp.DataStore({fields: Field});
    dsSpesialisasiRencana_Asuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiRencana_Asuhan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiRencana_Asuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiRencana_Asuhan,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasRencana_Asuhan').setValue('');
                                        Ext.getCmp('cboKamarRencana_Asuhan').setValue('');
                                        loaddatastoreKelasRencana_Asuhan(b.data.KD_SPESIAL);
                                        var tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                                        load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasRencana_Asuhan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiRencana_Asuhan;
}
;
function loaddatastoreKelasRencana_Asuhan(kd_spesial)
{
    dsKelasRencana_Asuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasRencana_Asuhan()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasRencana_Asuhan = new WebApp.DataStore({fields: Field});
    var cboKelasRencana_Asuhan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasRencana_Asuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasRencana_Asuhan,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarRencana_Asuhan').setValue('');
                                        loaddatastoreKamarRencana_Asuhan(b.data.kd_unit);
                                        var tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                                        load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarRencana_Asuhan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasRencana_Asuhan;
}
;
function loaddatastoreKamarRencana_Asuhan(kd_unit)
{
    dsKamarRencana_Asuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiRencana_Asuhan').getValue()
                                }
                    });
}

function mComboKamarRencana_Asuhan()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarRencana_Asuhan = new WebApp.DataStore({fields: Field});
    var cboKamarRencana_Asuhan = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarRencana_Asuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarRencana_Asuhan,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaRencanaAsuhan = getCriteriaFilter_viDaftar();
                                        load_Rencana_Asuhan(tmpkriteriaRencanaAsuhan);
                                    }
                                }
                    }
            );
    return cboKamarRencana_Asuhan;
}
;
function load_Rencana_Asuhan(criteria)
{
    dataSource_Rencana_Asuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepPengkajian',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Rencana_Asuhan;
}

function loadfilter_Rencana_Asuhan()
{
    var criteria = getCriteriaFilter_viDaftar();
    dataSource_Rencana_Asuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepRencana_Asuhan',
                                    param: criteria
                                }
                    }
            );
    return dataSource_Rencana_Asuhan;
}

function loadData_RencanaAsuhan(rowdata)
{
    var criteria = "";
    if (rowdata !== undefined) {
        criteria = "kd_pasien_kunj = '" + rowdata.KD_PASIEN + "' and kd_unit_kunj = '" + rowdata.KD_UNIT + "' and urut_masuk_kunj = " + rowdata.URUT_MASUK + " and tgl_masuk_kunj = '" + rowdata.TGL_MASUK + "'";
    } else {
        criteria = "kd_pasien_kunj = '" + kdPasien + "' and kd_unit_kunj = '" + kdUnit + "' and urut_masuk_kunj = " + urutMasuk + " and tgl_masuk_kunj = '" + tglMasuk + "'";
    }
    dataSource_RencanaAsuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'viewRencanaAsuhanDetail',
                                    param: criteria
                                }
                    }
            );
    return dataSource_RencanaAsuhan;
}

function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecRencanaAsuhan').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienRencanaAsuhan').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiRencana_Asuhan').getValue();
    var tmpkelas = Ext.getCmp('cboKelasRencana_Asuhan').getValue();
    var tmpkamar = Ext.getCmp('cboKamarRencana_Asuhan').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalRencanaAsuhan').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirRencanaAsuhan').getValue();
    var tmptambahan = Ext.getCmp('chkTglRencanaAsuhan').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatRencana_Asuhan = new WebApp.DataStore({fields: Field});
    dsPerawatRencana_Asuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatRencana_Asuhan = new Ext.form.ComboBox
            (
                    {
                        x: 135,
                        y: 5,
                        id: 'cboPerawatRencana_Asuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatRencana_Asuhan,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        idperawat = b.data.KODE;
                                    },
                                }
                    }
            );
    return cboPerawatRencana_Asuhan;
}
;
function caripasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'viewRencanaAsuhanDetail',
                            query: "kd_pasien_kunj = '" + kdPasien + "' and kd_unit_kunj = '" + kdUnit + "' and urut_masuk_kunj = " + urutMasuk + " and tgl_masuk_kunj = '" + tglMasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('txtkeluhatxtDiagnosaKeperawatannutama').setValue('');
                                    Ext.getCmp('txtTujuanKriteriahasil').setValue('');
                                    Ext.getCmp('txtRencanaTindakan').setValue('');
                                    Ext.getCmp('Jampelaksanaan').setValue('');
                                    Ext.getCmp('cboPerawatRencana_Asuhan').setValue('');
                                    idperawat = '';
                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    var tmpjam = tmphasil.JAM.substr(0, 5);
                                    Ext.getCmp('txtkeluhatxtDiagnosaKeperawatannutama').setValue(tmphasil.DIAGNOSA);
                                    Ext.getCmp('txtTujuanKriteriahasil').setValue(tmphasil.TUJUAN);
                                    Ext.getCmp('txtRencanaTindakan').setValue(tmphasil.RENCANA);
                                    Ext.getCmp('Tglpelaksanaan').setValue(ShowDate(tmphasil.TGL_NCP));
                                    Ext.getCmp('Jampelaksanaan').setValue(tmpjam);
                                    Ext.getCmp('TglSelesaiPelaksanaan').setValue(ShowDate(tmphasil.TGL_SELESAI));
                                    Ext.getCmp('cboPerawatRencana_Asuhan').setValue(tmphasil.NAMA_PERAWAT);
                                    idperawat = tmphasil.KD_PERAWAT;

                                }

                            }
                        }
                    }
            );
}
function load_Rencana_Asuhan_detail(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupSpesialisasi').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('TxtPopupKelas').setValue(rowdata.KELAS);
    Ext.getCmp('TxtPopupkamarPasien').setValue(rowdata.NAMA_KAMAR);
    kdPasien = rowdata.KD_PASIEN;
    kdUnit = rowdata.KD_UNIT;
    urutMasuk = rowdata.URUT_MASUK;
    tglMasuk = rowdata.TGL_MASUK;
    caripasien()
}

function saveAskepPerencanaan(kd_pasien, kd_unit, tgl_masuk, urut_masuk)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparam_viAskep(kd_pasien, kd_unit, tgl_masuk, urut_masuk),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                loadData_RencanaAsuhan();
                                ShowPesanInfo_viDaftar('Data berhasil disimpan', 'Simpan Data');
                            }
                        }
                    }
            );
}

function dataparam_viAskep()
{

    var params =
            {
                Table: 'crudviewRencanaAsuhanDetail',
                _kdpasien: kdPasien,
                _tglmasuk: tglMasuk,
                _urutmasuk: urutMasuk,
                _kdunit: kdUnit,
                diagnosa: Ext.getCmp('txtkeluhatxtDiagnosaKeperawatannutama').getValue(),
                tujuan: Ext.getCmp('txtTujuanKriteriahasil').getValue(),
                rencana: Ext.getCmp('txtRencanaTindakan').getValue(),
                tgl_ncp: Ext.getCmp('Tglpelaksanaan').getValue(),
                jam: Ext.getCmp('Jampelaksanaan').getValue(),
                tgl_selesai: Ext.getCmp('TglSelesaiPelaksanaan').getValue(),
                kode_per: idperawat,
            }
    return params;
}

function ShowPesanInfo_viDaftar(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}

function ShowPesanWarningRA(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningRA('Hubungi Admin', 'Error');
                            loadData_RencanaAsuhan();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfo_viDaftar('Data berhasil di hapus', 'Information');
                                caripasien();
                                loadData_RencanaAsuhan();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningRA('Gagal menghapus data', 'Error');
                                caripasien();
                                loadData_RencanaAsuhan();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'crudviewRencanaAsuhanDetail',
                query: "kd_pasien_kunj = '" + kdPasien + "' and kd_unit_kunj = '" + kdUnit + "' and urut_masuk_kunj = " + urutMasuk + " and tgl_masuk_kunj = '" + tglMasuk + "'"
            };
    return params;
}

function GetCriteriaRencanaAsuhan()
{
    var strKriteria = '';
    strKriteria = kdPasien;
    strKriteria += '##@@##' + kdUnit;
    strKriteria += '##@@##' + urutMasuk;
    strKriteria += '##@@##' + tglMasuk;
    strKriteria += '##@@##' + Ext.get('TxtPopupMedrec').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupNamaPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupAlamatPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupSpesialisasi').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupKelas').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupkamarPasien').getValue();
    return strKriteria;
}
