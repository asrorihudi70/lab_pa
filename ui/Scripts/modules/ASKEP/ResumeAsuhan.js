
var now = new Date();
var dsSpesialisasiResumeAsuhan;
var ListHasilResumeAsuhan;
var rowSelectedHasilResumeAsuhan;
var dsKelasResumeAsuhan;
var dsKamarResumeAsuhan;
var dataSource_ResumeAsuhan;
var dsStatusPulangResumeAsuhan;
var dsDirujukResumeAsuhan;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var selectSetStatusPulang;
var kd_rujukan;
var kd_statpulang;

CurrentPage.page = getPanelResumeAsuhan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelResumeAsuhan(mod_id) {
    var Field = ['TGL_MASUK', 'KD_PASIEN', 'NAMA', 'ALAMAT', 'SPESIALISASI', 'KELAS', 'NAMA_KAMAR', 'URUT_MASUK', 'KD_UNIT'];

    dataSource_ResumeAsuhan = new WebApp.DataStore({
        fields: Field
    });

    load_ResumeAsuhan("ng.AKHIR = 't' limit 50");
    var gridListHasilResumeAsuhan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_ResumeAsuhan,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftar = dataSource_ResumeAsuhan.getAt(ridx);
                if (rowSelected_viDaftar !== undefined)
                {
                    HasilResumeAsuhanLookUp(rowSelected_viDaftar.data);
                }
                else
                {
                    HasilResumeAsuhanLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilResumeAsuhan',
                        header: 'Tgl. Masuk',
                        dataIndex: 'TGL_MASUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colMedrecViewHasilResumeAsuhan',
                        header: 'No. Medrec',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
                    {
                        id: 'colNamaViewHasilResumeAsuhan',
                        header: 'Nama',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colAlamatViewHasilResumeAsuhan',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 100
                    },
                    {
                        id: 'colSpesialisasiViewHasilResumeAsuhan',
                        header: 'Spesialisasi',
                        dataIndex: 'SPESIALISASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKelasViewHasilResumeAsuhan',
                        header: 'Kelas',
                        dataIndex: 'KELAS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilResumeAsuhan',
                        header: 'Kamar',
                        dataIndex: 'NAMA_KAMAR',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilResumeAsuhan',
                text: 'Open List',
                tooltip: 'Open List',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    HasilResumeAsuhanLookUp();
                }
            }]
    });
    var FormDepanResumeAsuhan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Resume Asuhan',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilResumeAsuhan
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('dtpTanggalakhirResumeAsuhan').disable();
                Ext.getCmp('dtpTanggalawalResumeAsuhan').disable();
            }
        }
    });
    return FormDepanResumeAsuhan;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCariMedrecResumeAsuhan',
                        id: 'TxtCariMedrecResumeAsuhan',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtCariMedrecResumeAsuhan').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(tmpNoMedrec);
                                                Ext.getCmp('TxtCariMedrecResumeAsuhan').setValue(tmpgetNoMedrec);
                                                var tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                                                load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                                            }
                                            else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                                                    load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                                                }
                                                else
                                                    Ext.getCmp('TxtCariMedrecResumeAsuhan').setValue('');
                                            }
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPasienResumeAsuhan',
                        id: 'TxtCariNamaPasienResumeAsuhan',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                                            load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiResumeAsuhan(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasResumeAsuhan(),
                    {
                        x: 260,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 310,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarResumeAsuhan(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalResumeAsuhan',
                        id: 'dtpTanggalawalResumeAsuhan', format: 'd/M/Y',
                        value: now,
                        width: 122
                    },
                    {
                        x: 260,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 320,
                        y: 130,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirResumeAsuhan',
                        id: 'dtpTanggalakhirResumeAsuhan',
                        format: 'd/M/Y',
                        value: now,
                        width: 120
                    },
                    {
                        x: 450,
                        y: 130,
                        xtype: 'checkbox',
                        id: 'chkTgl',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalakhirResumeAsuhan').enable();
                                Ext.getCmp('dtpTanggalawalResumeAsuhan').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalakhirResumeAsuhan').disable();
                                Ext.getCmp('dtpTanggalawalResumeAsuhan').disable();
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataResumeAsuhan',
                        handler: function () {
                            var tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                            load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilResumeAsuhanLookUp(rowdata) {
    FormLookUpdetailResumeAsuhan = new Ext.Window({
        id: 'gridHasilResumeAsuhan',
        title: 'Resume Asuhan',
        closeAction: 'destroy',
        width: 1000,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupResumeAsuhan()],
        listeners: {
        }
    });
    FormLookUpdetailResumeAsuhan.show();
    if (rowdata === undefined) {

    } else {
        tmpkdpasien = rowdata.KD_PASIEN;
        tmpkdunit = rowdata.KD_UNIT;
        tmpurutmasuk = rowdata.URUT_MASUK;
        tmptglmasuk = rowdata.TGL_MASUK;
        datainit_ResumeAsuhan(rowdata);
    }

}
;
function formpopupResumeAsuhan() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopupResumeAsuhan',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '99%',
                        iconCls: '',
                        items:
                                [
                                    PanelResumeAsuhan(),
                                    {xtype: 'tbspacer', height: 10},
                                    PanelEditResumeAsuhan()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {
                                                        SavingData();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Hapus',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });

                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftarRA',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaResume();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapAsuhanKeperawatan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupdatahasilrad;
}
;
function PanelResumeAsuhan()
{
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 975,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        readOnly: true,
                        width: 80
                    },
                    {
                        x: 210,
                        y: 10,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 270,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 280,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        readOnly: true,
                        width: 200
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Alamat '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        readOnly: true,
                        width: 500
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupSpesialisasi',
                        id: 'TxtPopupSpesialisasi',
                        readOnly: true,
                        width: 80
                    },
                    {
                        x: 210,
                        y: 70,
                        xtype: 'label',
                        text: 'Kelas '
                    },
                    {x: 270,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 280,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupKelas',
                        id: 'TxtPopupKelas',
                        readOnly: true,
                        width: 200
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100, xtype: 'textfield',
                        name: 'TxtPopupKamarPasien',
                        id: 'TxtPopupkamarPasien',
                        readOnly: true,
                        width: 500
                    }
                ]
            }
        ]
    };
    return items;
}
;
function PanelEditResumeAsuhan()
{
    var PanelEditResumeAsuhan = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelEditResumeAsuhan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        width: 975,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'pasien Dirujuk ke '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            mComboDirujukResumeAsuhan(),
                                            {
                                                x: 270,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Status Pulang '
                                            },
                                            {
                                                x: 340,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            mComboStatusPulangResumeAsuhan()
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Diagnosa Saat Pulang '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'txtdiagnosasaatpulang',
                                                height: 50,
                                                width: 225
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah Keperawatan Saat Dirawat '
                                            },
                                            {
                                                x: 560,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 580,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'txtMasalahKeperawatan',
                                                height: 50,
                                                width: 383
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: true,
                                height: 48,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 5,
                                                xtype: 'label',
                                                text: 'Kondisi Pasien Saat Pulang '
                                            },
                                            {
                                                x: 148,
                                                y: 5,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 15,
                                                y: 25,
                                                xtype: 'radiogroup',
                                                id: 'rgKondisi',
                                                boxMaxWidth: 300,
                                                items: [
                                                    {
                                                        boxLabel: 'Jalan',
                                                        name: 'rgKondisi',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKondisiJalan'
                                                    },
                                                    {
                                                        boxLabel: 'Tongkat',
                                                        name: 'rgKondisi',
                                                        inputValue: 1,
                                                        id: 'rgKondisiTongkat'
                                                    },
                                                    {
                                                        boxLabel: 'Kursi Roda',
                                                        name: 'rgKondisi',
                                                        inputValue: 2,
                                                        id: 'rgKondisiKursi'
                                                    },
                                                    {
                                                        boxLabel: 'Brankar',
                                                        name: 'rgKondisi',
                                                        inputValue: 3,
                                                        id: 'rgKondisiBrankar'
                                                    }
                                                ]
                                            }
                                        ]
                            },
                            {xtype: 'tbspacer', height: 5},
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: true,
                                height: 48,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 5,
                                                xtype: 'label',
                                                text: 'Alat Bantu Yang Terpasang '
                                            },
                                            {
                                                x: 148,
                                                y: 5,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 15,
                                                y: 25,
                                                xtype: 'checkboxgroup',
                                                id: 'cgAlatBantuTerpasang',
                                                columns: 6,
                                                boxMaxWidth: 500,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Tidak Ada', name: 'cgAlatBantuTerpasangtidak', id: 'cgAlatBantuTerpasangtidak'},
                                                    {xtype: 'checkbox', boxLabel: 'Kateter', name: 'cgAlatBantuTerpasangkateter', id: 'cgAlatBantuTerpasangkateter'},
                                                    {xtype: 'checkbox', boxLabel: 'Oksigen', name: 'cgAlatBantuTerpasangoksigen', id: 'cgAlatBantuTerpasangoksigen'},
                                                    {xtype: 'checkbox', boxLabel: 'Infus', name: 'cgAlatBantuTerpasanginfus', id: 'cgAlatBantuTerpasanginfus'},
                                                    {xtype: 'checkbox', boxLabel: 'NGT', name: 'cgAlatBantuTerpasangntg', id: 'cgAlatBantuTerpasangntg'},
                                                    {xtype: 'checkbox', boxLabel: 'Lain - Lain', name: 'cgAlatBantuTerpasangdll', id: 'cgAlatBantuTerpasangdll'}
                                                ]
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 80,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 5,
                                                xtype: 'label',
                                                text: 'Saran/tindakan yang perlu dilanjutkan '
                                            },
                                            {
                                                x: 195,
                                                y: 5,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 15,
                                                y: 25,
                                                xtype: 'textarea',
                                                id: 'txtSaran/tindakan',
                                                height: 50,
                                                width: 947
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 200,
                                border: false,
                                height: 100,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 5,
                                                xtype: 'label',
                                                text: 'Penyuluhan Kesehatan Yang Telah Diberikan Perihals '
                                            },
                                            {
                                                x: 270,
                                                y: 5,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 15,
                                                y: 25,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPenyulahanKesehatan',
                                                columns: 6,
                                                boxMaxWidth: 1000,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Pemberian makan/minum', name: 'cgPenyulahanKesehatanmakanminum', id: 'cgPenyulahanKesehatanmakanminum'},
                                                    {xtype: 'checkbox', boxLabel: 'Pemberian Obat', name: 'cgPenyulahanKesehatanPemberianObat', id: 'cgPenyulahanKesehatanPemberianObat'},
                                                    {xtype: 'checkbox', boxLabel: 'Perawatan Obat', name: 'cgPenyulahanKesehatanPerawatanObat', id: 'cgPenyulahanKesehatanPerawatanObat'},
                                                    {xtype: 'checkbox', boxLabel: 'teknik relaksasi', name: 'cgPenyulahanKesehatanteknik', id: 'cgPenyulahanKesehatanteknik'},
                                                    {xtype: 'checkbox', boxLabel: 'Batuk efektif', name: 'cgPenyulahanKesehatanefektif', id: 'cgPenyulahanKesehatanefektif'},
                                                    {xtype: 'checkbox', boxLabel: 'Batuk fisioterafi', name: 'cgPenyulahanKesehatanfisioterafi', id: 'cgPenyulahanKesehatanfisioterafi'},
                                                    {xtype: 'checkbox', boxLabel: 'Perawatan Bayi Baru lahir', name: 'cgPenyulahanKesehatanBayi', id: 'cgPenyulahanKesehatanBayi'},
                                                    {xtype: 'checkbox', boxLabel: 'Menggunakan Alat bantu', name: 'cgPenyulahanKesehatanAlat', id: 'cgPenyulahanKesehatanAlat'},
                                                    {xtype: 'checkbox', boxLabel: 'Hearing Aid', name: 'cgPenyulahanKesehatanAid', id: 'cgPenyulahanKesehatanAid'},
                                                    {xtype: 'checkbox', boxLabel: 'Kateter', name: 'cgPenyulahanKesehatanKateter', id: 'cgPenyulahanKesehatanKateter'},
                                                    {xtype: 'checkbox', boxLabel: 'Pantangan/Larangan', name: 'cgPenyulahanKesehatanPantangan', id: 'cgPenyulahanKesehatanPantangan'},
                                                    {xtype: 'checkbox', boxLabel: 'Perawatan Payudara', name: 'cgPenyulahanKesehatanPayudara', id: 'cgPenyulahanKesehatanPayudara'},
                                                    {xtype: 'checkbox', boxLabel: 'Larutan PK/Bethadine', name: 'cgPenyulahanKesehatanBethadine', id: 'cgPenyulahanKesehatanBethadine'},
                                                    {xtype: 'checkbox', boxLabel: 'Pengaturan Diet', name: 'cgPenyulahanKesehatanDiet', id: 'cgPenyulahanKesehatanDiet'},
                                                    {xtype: 'checkbox', boxLabel: 'NTG', name: 'cgPenyulahanKesehatanNTG', id: 'cgPenyulahanKesehatanNTG'},
                                                    {xtype: 'checkbox', boxLabel: 'Infus', name: 'cgPenyulahanKesehatanInfus', id: 'cgPenyulahanKesehatanInfus'},
                                                    {xtype: 'checkbox', boxLabel: 'Oksigen', name: 'cgPenyulahanKesehatanOksigen', id: 'cgPenyulahanKesehatanOksigen'},
                                                    {xtype: 'checkbox', boxLabel: 'Lain - Lain', name: 'cgPenyulahanKesehatanLain', id: 'cgPenyulahanKesehatanLain'}
                                                ]
                                            }
                                        ]
                            },
                        ]
                    }
            );
    return PanelEditResumeAsuhan;
}
;







//combo dan load database

function datainit_ResumeAsuhan(rowdata)
{

    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupSpesialisasi').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('TxtPopupKelas').setValue(rowdata.KELAS);
    Ext.getCmp('TxtPopupkamarPasien').setValue(rowdata.NAMA_KAMAR);
    caripasien();
}

function caripasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetResumeAsuhan',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);

                            if (cst.success === true)
                            {
                                if (cst.totalrecords === 0)
                                {
                                    Ext.getCmp('txtdiagnosasaatpulang').setValue('');
                                    Ext.getCmp('txtMasalahKeperawatan').setValue('');
                                    Ext.getCmp('txtSaran/tindakan').setValue('');
                                    Ext.getCmp('cboStatusPulangResumeAsuhan').setValue('');
                                    Ext.getCmp('cboDirujukResumeAsuhan').setValue('');
                                    kd_rujukan = '';
                                    kd_statpulang = '';
                                    Ext.getCmp('rgKondisi').setValue("rgKondisiJalan", false);
                                    Ext.getCmp('rgKondisi').setValue("rgKondisiTongkat", false);
                                    Ext.getCmp('rgKondisi').setValue("rgKondisiKursi", false);
                                    Ext.getCmp('rgKondisi').setValue("rgKondisiBrankar", false);
                                    Ext.getCmp('cgAlatBantuTerpasangtidak').setValue(false);
                                    Ext.getCmp('cgAlatBantuTerpasangkateter').setValue(false);
                                    Ext.getCmp('cgAlatBantuTerpasangoksigen').setValue(false);
                                    Ext.getCmp('cgAlatBantuTerpasanginfus').setValue(false);
                                    Ext.getCmp('cgAlatBantuTerpasangntg').setValue(false);
                                    Ext.getCmp('cgAlatBantuTerpasangdll').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanmakanminum').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanPemberianObat').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanPerawatanObat').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanteknik').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanefektif').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanfisioterafi').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanBayi').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanAlat').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanAid').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanKateter').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanPantangan').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanPayudara').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanBethadine').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanDiet').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanNTG').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanInfus').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanOksigen').setValue(false);
                                    Ext.getCmp('cgPenyulahanKesehatanLain').setValue(false);

                                } else
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    //textbox
                                    Ext.getCmp('txtdiagnosasaatpulang').setValue(tmphasil.DIAGNOSA_AKHIR);
                                    Ext.getCmp('txtMasalahKeperawatan').setValue(tmphasil.MASALAH_KEPERAWATAN);
                                    Ext.getCmp('txtSaran/tindakan').setValue(tmphasil.SARAN_TINDAKAN);

                                    //combobox

                                    Ext.getCmp('cboStatusPulangResumeAsuhan').setValue(tmphasil.STATUS_PULANG);
                                    Ext.getCmp('cboDirujukResumeAsuhan').setValue(tmphasil.RUJUKAN);

                                    kd_rujukan = tmphasil.KD_RUJUKAN;
                                    kd_statpulang = tmphasil.KD_STATUS_PULANG;

                                    //radiogroup
                                    if (tmphasil.MOBILISASI_JALAN === "t")
                                    {
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiJalan", true);
                                    }
                                    else if (tmphasil.MOBILISASI_TONGKAT === "t")
                                    {
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiTongkat", true);
                                    }
                                    else if (tmphasil.MOBILISASI_KURSI_RODA === "t")
                                    {
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiKursi", true);
                                    }
                                    else if (tmphasil.MOBILISASI_BRANKAR === "t")
                                    {
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiBrankar", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiJalan", false);
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiTongkat", false);
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiKursi", false);
                                        Ext.getCmp('rgKondisi').setValue("rgKondisiBrankar", false);
                                    }

                                    if (tmphasil.ALAT_TIDAKADA === "t")
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangtidak').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangtidak').setValue(false);
                                    }
                                    if (tmphasil.ALAT_BANTU_KATETER === "t")
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangkateter').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangkateter').setValue(false);
                                    }
                                    if (tmphasil.ALAT_BANTU_OKSIGEN === "t")
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangoksigen').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangoksigen').setValue(false);
                                    }
                                    if (tmphasil.ALAT_BANTU_INFUS === "t")
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasanginfus').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasanginfus').setValue(false);
                                    }
                                    if (tmphasil.ALAT_BANTU_NGT === "t")
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangntg').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangntg').setValue(false);
                                    }
                                    if (tmphasil.ALAT_BANTU_LAIN === "t")
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangdll').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgAlatBantuTerpasangdll').setValue(false);
                                    }

                                    if (tmphasil.PENYULUHAN_MAKAN === 't')
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanmakanminum').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanmakanminum').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_OBAT === 't')
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanPemberianObat').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanPemberianObat').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_RAWATLUKA === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanPerawatanObat').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanPerawatanObat').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_RELAKSASI === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanteknik').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanteknik').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_BATUKEFEKTIF === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanefektif').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanefektif').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_BATUKFISIO === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanfisioterafi').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanfisioterafi').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_RAWATBBL === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanBayi').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanBayi').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_ALAT_BANTU === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanAlat').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanAlat').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_HEARING_AID === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanAid').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanAid').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_KATETER === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanKateter').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanKateter').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_LARANGAN === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanPantangan').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanPantangan').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_RAWATPAYUDARA === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanPayudara').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanPayudara').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_LARUTANPK === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanBethadine').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanBethadine').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_DIET === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanDiet').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanDiet').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_NGT === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanNTG').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanNTG').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_INFUS === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanInfus').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanInfus').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_OKSIGEN === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanOksigen').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanOksigen').setValue(false);
                                    }
                                    if (tmphasil.PENYULUHAN_LAIN === 't') {
                                        Ext.getCmp('cgPenyulahanKesehatanLain').setValue(true);
                                    } else
                                    {
                                        Ext.getCmp('cgPenyulahanKesehatanLain').setValue(false);
                                    }
                                }

                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningResume('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            caripasien();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoResume('Proses Saving Berhasil', 'Save');
                                caripasien();
                            }
                            else
                            {
                                ShowPesanWarningResume('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                caripasien();
                            }
                            ;
                        }
                    }
            );

}
;

function paramsaving()
{
    var params =
            {
                Table: 'ViewAskepResumeAsuhan',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien,
                KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                KD_STATUS_PULANG: kd_statpulang,
                KD_RUJUKAN: kd_rujukan,
                DIAGNOSA_AKHIR: Ext.get('txtdiagnosasaatpulang').getValue(),
                MASALAH_KEPERAWATAN: Ext.get('txtMasalahKeperawatan').getValue(),
                MOBILISASI_JALAN: Ext.getCmp('rgKondisiJalan').checked,
                MOBILISASI_TONGKAT: Ext.getCmp('rgKondisiTongkat').checked,
                MOBILISASI_KURSI_RODA: Ext.getCmp('rgKondisiKursi').checked,
                MOBILISASI_BRANKAR: Ext.getCmp('rgKondisiBrankar').checked,
                ALAT_TIDAKADA: Ext.getCmp('cgAlatBantuTerpasangtidak').checked,
                ALAT_BANTU_KATETER: Ext.getCmp('cgAlatBantuTerpasangkateter').checked,
                ALAT_BANTU_OKSIGEN: Ext.getCmp('cgAlatBantuTerpasangoksigen').checked,
                ALAT_BANTU_INFUS: Ext.getCmp('cgAlatBantuTerpasanginfus').checked,
                ALAT_BANTU_NGT: Ext.getCmp('cgAlatBantuTerpasangntg').checked,
                ALAT_BANTU_LAIN: Ext.getCmp('cgAlatBantuTerpasangdll').checked,
                ALAT_BANTU_KET: '',
                SARAN_TINDAKAN: Ext.getCmp('txtSaran/tindakan').getValue(),
                PENYULUHAN_MAKAN: Ext.getCmp('cgPenyulahanKesehatanmakanminum').checked,
                PENYULUHAN_OBAT: Ext.getCmp('cgPenyulahanKesehatanPemberianObat').checked,
                PENYULUHAN_RAWATLUKA: Ext.getCmp('cgPenyulahanKesehatanPerawatanObat').checked,
                PENYULUHAN_RELAKSASI: Ext.getCmp('cgPenyulahanKesehatanteknik').checked,
                PENYULUHAN_BATUKEFEKTIF: Ext.getCmp('cgPenyulahanKesehatanefektif').checked,
                PENYULUHAN_BATUKFISIO: Ext.getCmp('cgPenyulahanKesehatanfisioterafi').checked,
                PENYULUHAN_RAWATBBL: Ext.getCmp('cgPenyulahanKesehatanBayi').checked,
                PENYULUHAN_ALAT_BANTU: Ext.getCmp('cgPenyulahanKesehatanAlat').checked,
                PENYULUHAN_HEARING_AID: Ext.getCmp('cgPenyulahanKesehatanAid').checked,
                PENYULUHAN_KATETER: Ext.getCmp('cgPenyulahanKesehatanKateter').checked,
                PENYULUHAN_LARANGAN: Ext.getCmp('cgPenyulahanKesehatanPantangan').checked,
                PENYULUHAN_RAWATPAYUDARA: Ext.getCmp('cgPenyulahanKesehatanPayudara').checked,
                PENYULUHAN_LARUTANPK: Ext.getCmp('cgPenyulahanKesehatanBethadine').checked,
                PENYULUHAN_DIET: Ext.getCmp('cgPenyulahanKesehatanDiet').checked,
                PENYULUHAN_NGT: Ext.getCmp('cgPenyulahanKesehatanNTG').checked,
                PENYULUHAN_INFUS: Ext.getCmp('cgPenyulahanKesehatanInfus').checked,
                PENYULUHAN_OKSIGEN: Ext.getCmp('cgPenyulahanKesehatanOksigen').checked,
                PENYULUHAN_LAIN: Ext.getCmp('cgPenyulahanKesehatanLain').checked,
                PENYULUHAN_KET: ''
            };
    return params;
}

function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningResume('Hubungi Admin', 'Error');
                            caripasien();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoResume('Berhasil menghapus data ini', 'Information');
                                caripasien();

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningResume('Gagal menghapus data ini', 'Error');
                                caripasien();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepResumeAsuhan',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien,
                KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                KD_STATUS_PULANG: kd_statpulang,
                KD_RUJUKAN: kd_rujukan,
            };
    return params;
}

function mComboSpesialisasiResumeAsuhan()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiResumeAsuhan = new WebApp.DataStore({fields: Field});
    dsSpesialisasiResumeAsuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiResumeAsuhan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboSpesialisasiResumeAsuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiResumeAsuhan,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        anchor: '20%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKelasResumeAsuhan').setValue('');
                                        Ext.getCmp('cboKamarResumeAsuhan').setValue('');
                                        loaddatastoreKelasResumeAsuhan(b.data.KD_SPESIAL);
                                        var tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                                        load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasResumeAsuhan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiResumeAsuhan;
}
;
function loaddatastoreKelasResumeAsuhan(kd_spesial)
{
    dsKelasResumeAsuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasResumeAsuhan()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasResumeAsuhan = new WebApp.DataStore({fields: Field});
    var cboKelasResumeAsuhan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasResumeAsuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasResumeAsuhan,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarResumeAsuhan').setValue('');
                                        loaddatastoreKamarResumeAsuhan(b.data.kd_unit);
                                        var tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                                        load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarResumeAsuhan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasResumeAsuhan;
}
;
function loaddatastoreKamarResumeAsuhan(kd_unit)
{
    dsKamarResumeAsuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiResumeAsuhan').getValue()
                                }
                    });
}

function mComboKamarResumeAsuhan()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarResumeAsuhan = new WebApp.DataStore({fields: Field});
    var cboKamarResumeAsuhan = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarResumeAsuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarResumeAsuhan,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaResumeAsuhan = getCriteriaFilter_viDaftar();
                                        load_ResumeAsuhan(tmpkriteriaResumeAsuhan);
                                    }
                                }
                    }
            );
    return cboKamarResumeAsuhan;
}
;
function load_ResumeAsuhan(criteria)
{
    dataSource_ResumeAsuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepPengkajian',
                                    param: criteria
                                }
                    }
            );
    return dataSource_ResumeAsuhan;
}

function loadfilter_ResumeAsuhan()
{
    var criteria = getCriteriaFilter_viDaftar();
    dataSource_ResumeAsuhan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetResumeAsuhan',
                                    param: criteria
                                }
                    }
            );
    return dataSource_ResumeAsuhan;
}

function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecResumeAsuhan').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienResumeAsuhan').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiResumeAsuhan').getValue();
    var tmpkelas = Ext.getCmp('cboKelasResumeAsuhan').getValue();
    var tmpkamar = Ext.getCmp('cboKamarResumeAsuhan').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalResumeAsuhan').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirResumeAsuhan').getValue();
    var tmptambahan = Ext.getCmp('chkTgl').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "")
    {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "")
    {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function radiojelas(tmp)
{
    if (tmp === 'rbjelasya')
    {
        Ext.getDom('rbjelastidak').checked = false;
    }
    if (Ext.getCmp('rbjelastidak').getValue() === true)
    {
        Ext.getDom('rbjelasya').checked = false;
    }
}

function mComboDirujukResumeAsuhan()
{
    var Field = ['KD_RUJUKAN', 'RUJUKAN'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'rujukan',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboRujukan',
                                    param: ""
                                }
                    }
            );
    var cboDirujukResumeAsuhan = new Ext.form.ComboBox
            (
                    {
                        x: 135,
                        y: 5,
                        id: 'cboDirujukResumeAsuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Pilih Rujukan...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: ds_Poli_viDaftar,
                        valueField: 'KD_RUJUKAN',
                        displayField: 'RUJUKAN',
                        width: 125,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        kd_rujukan = Ext.getCmp('cboDirujukResumeAsuhan').getValue();
                                    }
                                }
                    }
            );
    return cboDirujukResumeAsuhan;
}
;

function mComboStatusPulangResumeAsuhan()
{
    var Field = ['KD_STATUS_PULANG', 'STATUS_PULANG'];
    ds_statuspulang_viDaftar = new WebApp.DataStore({fields: Field});

    ds_statuspulang_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'status_pulang',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboStatusPulang',
                                    param: ""
                                }
                    }
            );
    var cboStatusPulangResumeAsuhan = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 5,
                        id: 'cboStatusPulangResumeAsuhan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Pilih Status...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: ds_statuspulang_viDaftar,
                        valueField: 'KD_STATUS_PULANG',
                        displayField: 'STATUS_PULANG',
                        width: 130,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        kd_statpulang = Ext.getCmp('cboStatusPulangResumeAsuhan').getValue();
                                    }
                                }
                    }
            );
    return cboStatusPulangResumeAsuhan;
}
;

function ShowPesanWarningResume(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoResume(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}

function GetCriteriaResume()
{
    var strKriteria = '';
    strKriteria = tmpkdpasien;
    strKriteria += '##@@##' + tmpkdunit;
    strKriteria += '##@@##' + tmpurutmasuk;
    strKriteria += '##@@##' + tmptglmasuk;
    strKriteria += '##@@##' + Ext.get('TxtPopupMedrec').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupNamaPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupAlamatPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupSpesialisasi').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupKelas').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupkamarPasien').getValue();
    return strKriteria;
}