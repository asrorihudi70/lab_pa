
var now = new Date();
var dsSpesialisasiSetupPerawat;
var ListHasilSetupPerawat;
var rowSelectedHasilSetupPerawat;
var dsKelasSetupPerawat;
var dsKamarSetupPerawat;
var dsSetupPerawat;
var dataSource_SetupPerawat;
var TMPTANGGAL = now;
var rowSelected_viDaftar;
var selectrow = {
    data: Object,
    details: Array,
    row: 0
};


CurrentPage.page = getPanelSetupPerawat(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelSetupPerawat(mod_id) {

    var Field = ['KODE', 'NAMA', 'STATUS', 'NIP'];

    dataSource_SetupPerawat = new WebApp.DataStore({
        fields: Field
    });
    load_SetupPerawat("");
    var gridListHasilSetupPerawat = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_SetupPerawat,
        id: 'gridperawat',
        anchor: '100% 75%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                    rowSelected_viDaftar = dataSource_SetupPerawat.getAt(row);
                    selectrow.row = row;
                    selectrow.data = rowSelected_viDaftar;

                    Ext.getCmp('btnHapusSetupPerawat').enable();
                    Ext.getCmp('btnBatalSetupPerawat').enable();
                    Ext.getCmp('btnSimpanSetupPerawat').enable();

//                    Ext.getCmp('txtkodeperawat').setValue(rowSelected_viDaftar.data.KODE);
//                    Ext.getCmp('txtnipperawat').setValue(rowSelected_viDaftar.data.NIP);
//                    Ext.getCmp('txtNamaperawat').setValue(rowSelected_viDaftar.data.NAMA);
//                    if (rowSelected_viDaftar.data.STATUS === 'Aktif')
//                    {
//                        Ext.getCmp('cbstatus').setValue(true);
//                    } else
//                    {
//                        Ext.getCmp('cbstatus').setValue(false);
//                    }
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftar = dataSource_SetupPerawat.getAt(ridx);
                if (rowSelected_viDaftar !== undefined)
                {
                    Ext.getCmp('btnSimpanSetupPerawat').enable();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colkdViewHasilSetupPerawat',
                        header: 'Kode Perawat',
                        dataIndex: 'KODE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colperawatViewHasilSetupPerawat',
                        header: 'Perawat',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 300,
                        editor: {
                            xtype: 'textfield'
                        }
                    },
                    {
                        id: 'colStatusViewHasilSetupPerawat',
                        header: 'Status',
                        dataIndex: 'STATUS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
                        editor: {
                            xtype: 'checkbox',
                            id: 'cbstatusgrid',
                            listener:
                                    {
//                                        console.log(value);
                                    }
                        },
                        renderer: function (value) {
                            Ext.getCmp('cbstatusgrid').setValue(value);
                            if (value === true || value === 'Aktif')
                            {
                                return 'Aktif';
                            } else
                            {
                                return 'Tidak Aktif';
                            }
                        }
                    },
                    {
                        id: 'colnipViewHasilSetupPerawat',
                        header: 'NIP',
                        dataIndex: 'NIP',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
                        editor: {
                            xtype: 'textfield'
                        }
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnTambahHasilSetupPerawat',
                text: 'Tambah Data',
                tooltip: 'Tambah Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    Ext.Ajax.request
                            (
                                    {
                                        url: baseURL + "index.php/main/CreateDataObj",
                                        params: {Table: 'ViewAskepSetupLastPerawat'},
                                        success: function (o)
                                        {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                var tmpkode = cst.kode;
                                                if (tmpkode.toString().length === 2)
                                                {
                                                    tmpkode = '0' + tmpkode;
                                                }
                                                defaultData = {
                                                    KODE: tmpkode,
                                                    NAMA: '',
                                                    STATUS: 'Tidak Aktif',
                                                    NIP: ''
                                                };
                                                var recId = 100; // provide unique id for the record
                                                var r = new dataSource_SetupPerawat.recordType(defaultData, ++recId); // create new record
                                                dataSource_SetupPerawat.insert(0, r);
                                            }
                                            ;
                                        }
                                    }
                            );


                }
            },
            {
                id: 'btnSimpanSetupPerawat',
                text: 'Simpan Data',
                tooltip: 'Simpan Data',
                iconCls: 'save',
                handler: function (sm, row, rec) {
                    SavingData();
                }
            },
            {
                id: 'btnHapusSetupPerawat',
                text: 'Hapus Data',
                tooltip: 'Hapus Data',
                iconCls: 'remove',
                handler: function (sm, row, rec) {
                    Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                        if (button === 'yes') {
                            loadMask.show();
                            DeleteData();
                        }
                    });
                }
            },
            {
                id: 'btnBatalSetupPerawat',
                text: 'Batal Edit',
                tooltip: 'Batal Data',
                iconCls: 'remove',
                handler: function (sm, row, rec) {
//                    console.log(dataSource_SetupPerawat.data.items[selectrow.row].data.NAMA);
                    if (dataSource_SetupPerawat.data.items[selectrow.row].data.NAMA === "")
                    {
                        dataSource_SetupPerawat.removeAt(selectrow.row);
                    } else
                    {

                    }
                    Ext.getCmp('btnHapusSetupPerawat').disable();
                    Ext.getCmp('btnBatalSetupPerawat').disable();
                    Ext.getCmp('btnSimpanSetupPerawat').disable();
                }
            }
        ]
    });
    var FormDepanSetupPerawat = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'SetupPerawat',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianSetupPerawat(),
            gridListHasilSetupPerawat
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cbstatus').setValue('true');
                Ext.getCmp('btnHapusSetupPerawat').disable();
                Ext.getCmp('btnBatalSetupPerawat').disable();
                Ext.getCmp('btnSimpanSetupPerawat').disable();
            }
        }
    });
    return FormDepanSetupPerawat;
}
;
function getPanelPencarianSetupPerawat() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Perawat '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        id: 'txtkodeperawat',
                        width: 50,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_SetupPerawat(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 190,
                        y: 10,
                        xtype: 'label',
                        text: 'NIP '
                    },
                    {
                        x: 230,
                        y: 10,
                        xtype: 'label',
                        text: ': '
                    },
                    {
                        x: 240,
                        y: 10,
                        xtype: 'textfield',
                        id: 'txtnipperawat',
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_SetupPerawat(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Perawat '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        id: 'txtNamaperawat',
                        width: 500,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_SetupPerawat(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Aktif '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 73,
                        xtype: 'checkbox',
                        id: 'cbstatus',
                        listeners:
                                {
                                    check: function () {
                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                        load_SetupPerawat(tmpkriteria);
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataSetupPerawat',
                        handler: function () {
                            var tmpkriteria = getCriteriaFilter_viDaftar();
                            load_SetupPerawat(tmpkriteria);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;



//combo dan load database

function datainit_SetupPerawat(rowdata)
{
    load_detsmp(rowdata.TANGGAL, rowdata.KD_UNIT, rowdata.NO_KAMAR, rowdata.KD_PERAWAT, rowdata.KD_WAKTU);

    TMPTANGGAL = rowdata.TANGGAL;
    TMPKD_UNIT = rowdata.KD_UNIT;
    TMPNO_KAMAR = rowdata.NO_KAMAR;
    TMPKD_PERAWAT = rowdata.KD_PERAWAT;
    TMPKD_WAKTU = rowdata.KD_WAKTU;

    Ext.getCmp('dtpTanggalPelaksanaan').setValue(rowdata.TANGGAL);
    Ext.getCmp('cboSpesialisasiSetupPerawatpopup').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('cbokelasSetupPerawatpopup').setValue(rowdata.NAMA_UNIT);
    Ext.getCmp('cboKamarSetupPerawatpopup').setValue(rowdata.NAMA_KAMAR);
    Ext.getCmp('cboPerawatSetupPerawatPopUp').setValue(rowdata.PERAWAT);
    Ext.getCmp('cboWaktuSetupPerawatPopUp').setValue(rowdata.WAKTU);


}

function load_SetupPerawat(criteria)
{
    dataSource_SetupPerawat.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_perawat',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepSetupPerawat',
                                    param: criteria
                                }
                    }
            );
    return dataSource_SetupPerawat;
}

function getCriteriaFilter_viDaftar()
{
    var tmpkode = Ext.getCmp('txtkodeperawat').getValue();
    var tmpnip = Ext.getCmp('txtnipperawat').getValue();
    var tmpnama = Ext.getCmp('txtNamaperawat').getValue();
    var tmpstatus;

    if (Ext.getCmp('cbstatus').getValue() === true)
    {
        tmpstatus = 1;
    } else
    {
        tmpstatus = 0;
    }
    var strKriteria = '';

//kd_perawat =  'undefined' AND nip = 'undefined' AND nama_perawat ilike '%undefined%'
    if (tmpkode !== undefined || tmpnip !== undefined || tmpnama !== undefined)
    {
        if (tmpkode !== "")
        {
            strKriteria += "  kd_perawat =  '" + tmpkode + "'";
        }
        if (tmpnip !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND nip = '" + tmpnip + "'";
            } else
            {
                strKriteria += " nip = '" + tmpnip + "'";
            }

        }
        if (tmpnama !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND nama_perawat " + "ilike '%" + tmpnama + "%' ";
            } else
            {
                strKriteria += " nama_perawat " + "ilike '%" + tmpnama + "%' ";
            }

        }
        if (tmpstatus !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND aktif ='" + tmpstatus + "' ";
            } else
            {
                strKriteria += " aktif ='" + tmpstatus + "' ";
            }

        }
    }
    else
    {
        strKriteria = '';
    }

    //strKriteria += ' group by ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, kmr.NAMA_KAMAR, sp.SPESIALISASI, ask.KD_PERAWAT, p.NAMA_PERAWAT, NIP,  SP.KD_SPESIAL , AW.WAKTU, ASK.KD_WAKTU  order by ask.TANGGAL ';


    return strKriteria;
}
;


function load_detsmp(TANGGAL, KD_UNIT, NO_KAMAR, KD_PERAWAT, KD_WAKTU)
{
    if (TANGGAL === '') {
        TANGGAL = '';
    }
    if (KD_UNIT === '') {
        KD_UNIT = '';
    }
    if (NO_KAMAR === '') {
        NO_KAMAR = '';
    }
    if (KD_PERAWAT === '') {
        KD_PERAWAT = '';
    }
    if (KD_WAKTU === '') {
        KD_WAKTU = 0;
    }

    var criteria = "b.TANGGAL='" + TANGGAL + "' and b.KD_UNIT='" + KD_UNIT + "' and b.NO_KAMAR='" + NO_KAMAR + "' and kd_perawat = '" + KD_PERAWAT + "'  and b.KD_WAKTU =   " + KD_WAKTU + "";

    dataSource_detSetupPerawat.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetSetupPerawat',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detSetupPerawat;
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningSetupPerawat('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_SetupPerawat("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoSetupPerawat('Proses Saving Berhasil', 'Save');
                                load_SetupPerawat("");
                                Ext.getCmp('btnHapusSetupPerawat').disable();
                                Ext.getCmp('btnBatalSetupPerawat').disable();
                                Ext.getCmp('btnSimpanSetupPerawat').disable();
                            }
                            else
                            {
                                ShowPesanWarningSetupPerawat('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_SetupPerawat("");
                                Ext.getCmp('btnHapusSetupPerawat').disable();
                                Ext.getCmp('btnBatalSetupPerawat').disable();
                                Ext.getCmp('btnSimpanSetupPerawat').disable();

                            }
                            ;
                        }
                    }
            );
}
;

function paramsaving()
{
    var i;
    var x = '';
    var y = '<<>>';
    for (i = 0; i < dataSource_SetupPerawat.getCount(); i++)
    {
        x += y + dataSource_SetupPerawat.data.items[i].data.KODE;
        x += y + dataSource_SetupPerawat.data.items[i].data.NAMA;
        x += y + dataSource_SetupPerawat.data.items[i].data.STATUS;
        x += y + dataSource_SetupPerawat.data.items[i].data.NIP;
        if (i === (dataSource_SetupPerawat.getCount() - 1))
        {
            x += y;
        }
        else
        {
            x += y + '@@@@';
        }
        ;
    }

    var params =
            {
                Table: 'ViewAskepSetupPerawat',
                LIST: x
//                TGL: TMPTANGGAL,
//                KDUNIT: TMPKD_UNIT,
//                NOKAMAR: TMPNO_KAMAR,
//                KDPERAWAT: TMPKD_PERAWAT,
//                KDWAKTU: TMPKD_WAKTU
            };
    return params;
}

function ShowPesanWarningSetupPerawat(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoSetupPerawat(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningSetupPerawat('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                loadMask.hide();
                                ShowPesanInfoSetupPerawat('Data berhasil di hapus', 'Information');
                                dataSource_SetupPerawat.removeAt(selectrow.row);
                                Ext.getCmp('btnHapusSetupPerawat').disable();
                                Ext.getCmp('btnBatalSetupPerawat').disable();
                                Ext.getCmp('btnSimpanSetupPerawat').disable();
                            }
                            else
                            {
                                if (cst.pesan === 0)
                                {
                                    loadMask.hide();
                                    dataSource_SetupPerawat.removeAt(selectrow.row);
                                    Ext.getCmp('btnHapusSetupPerawat').disable();
                                    Ext.getCmp('btnBatalSetupPerawat').disable();
                                    Ext.getCmp('btnSimpanSetupPerawat').disable();
                                } else
                                {
                                    loadMask.hide();
                                    ShowPesanWarningSetupPerawat('Gagal menghapus data', 'Error');
                                }

//                                caridatapasien();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepSetupPerawat',
                query: "kd_perawat = '" + dataSource_SetupPerawat.data.items[selectrow.row].data.KODE + "'"
            };
    return params;
}