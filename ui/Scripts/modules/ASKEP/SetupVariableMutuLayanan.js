
var now = new Date();
var dsSpesialisasiSetupVariabelMutuLayanan;
var ListHasilSetupVariabelMutuLayanan;
var rowSelectedHasilSetupVariabelMutuLayanan;
var dsKelasSetupVariabelMutuLayanan;
var dsKamarSetupVariabelMutuLayanan;
var dsSetupVariabelMutuLayanan;
var dataSource_SetupVariabelMutuLayanan;
var TMPTANGGAL = now;
var rowSelected_viDaftar;
var selectrow = {
    data: Object,
    details: Array,
    row: 0
};


CurrentPage.page = getPanelSetupVariabelMutuLayanan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelSetupVariabelMutuLayanan(mod_id) {

    var Field = ['KODE', 'NAMA'];

    dataSource_SetupVariabelMutuLayanan = new WebApp.DataStore({
        fields: Field
    });
    load_SetupVariabelMutuLayanan("");
    var gridListHasilSetupVariabelMutuLayanan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_SetupVariabelMutuLayanan,
        id: 'gridperawat',
        anchor: '100% 75%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                    rowSelected_viDaftar = dataSource_SetupVariabelMutuLayanan.getAt(row);
                    selectrow.row = row;
                    selectrow.data = rowSelected_viDaftar;

                    Ext.getCmp('btnHapusSetupVariabelMutuLayanan').enable();
                    Ext.getCmp('btnBatalSetupVariabelMutuLayanan').enable();
                    Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').enable();

//                    Ext.getCmp('txtkodevariabelmutu').setValue(rowSelected_viDaftar.data.KODE);
//                    Ext.getCmp('txtvariabel').setValue(rowSelected_viDaftar.data.NIP);
//                    Ext.getCmp('txtNamaperawat').setValue(rowSelected_viDaftar.data.NAMA);
//                    if (rowSelected_viDaftar.data.STATUS === 'Aktif')
//                    {
//                        Ext.getCmp('cbstatus').setValue(true);
//                    } else
//                    {
//                        Ext.getCmp('cbstatus').setValue(false);
//                    }
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftar = dataSource_SetupVariabelMutuLayanan.getAt(ridx);
                if (rowSelected_viDaftar !== undefined)
                {
                    Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').enable();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colkdViewHasilSetupVariabelMutuLayanan',
                        header: 'Kode Status Pulang',
                        dataIndex: 'KODE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colperawatViewHasilSetupVariabelMutuLayanan',
                        header: 'Status Pulang',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 300,
                        editor: {
                            xtype: 'textfield'
                        }
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnTambahHasilSetupVariabelMutuLayanan',
                text: 'Tambah Data',
                tooltip: 'Tambah Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    Ext.Ajax.request
                            (
                                    {
                                        url: baseURL + "index.php/main/CreateDataObj",
                                        params: {Table: 'ViewAskepSetupLastSetupVariabelMutuLayanan'},
                                        success: function (o)
                                        {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                var tmpkode = cst.kode;
                                                if (tmpkode.toString().length === 1)
                                                {
                                                    tmpkode = '0' + tmpkode;
                                                }
                                                defaultData = {
                                                    KODE: tmpkode,
                                                    NAMA: '',
                                                    STATUS: 'Tidak Aktif',
                                                    NIP: ''
                                                };
                                                var recId = 100; // provide unique id for the record
                                                var r = new dataSource_SetupVariabelMutuLayanan.recordType(defaultData, ++recId); // create new record
                                                dataSource_SetupVariabelMutuLayanan.insert(0, r);
                                            }
                                            ;
                                        }
                                    }
                            );


                }
            },
            {
                id: 'btnSimpanSetupVariabelMutuLayanan',
                text: 'Simpan Data',
                tooltip: 'Simpan Data',
                iconCls: 'save',
                handler: function (sm, row, rec) {
                    SavingData();
                }
            },
            {
                id: 'btnHapusSetupVariabelMutuLayanan',
                text: 'Hapus Data',
                tooltip: 'Hapus Data',
                iconCls: 'remove',
                handler: function (sm, row, rec) {
                    Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                        if (button === 'yes') {
                            loadMask.show();
                            DeleteData();
                        }
                    });
                }
            },
            {
                id: 'btnBatalSetupVariabelMutuLayanan',
                text: 'Batal Edit',
                tooltip: 'Batal Data',
                iconCls: 'remove',
                handler: function (sm, row, rec) {
//                    console.log(dataSource_SetupVariabelMutuLayanan.data.items[selectrow.row].data.NAMA);
                    if (dataSource_SetupVariabelMutuLayanan.data.items[selectrow.row].data.NAMA === "")
                    {
                        dataSource_SetupVariabelMutuLayanan.removeAt(selectrow.row);
                    } else
                    {

                    }
                    Ext.getCmp('btnHapusSetupVariabelMutuLayanan').disable();
                    Ext.getCmp('btnBatalSetupVariabelMutuLayanan').disable();
                    Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').disable();
                }
            }
        ]
    });
    var FormDepanSetupVariabelMutuLayanan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Variabel Mutu Layanan',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianSetupVariabelMutuLayanan(),
            gridListHasilSetupVariabelMutuLayanan
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('btnHapusSetupVariabelMutuLayanan').disable();
                Ext.getCmp('btnBatalSetupVariabelMutuLayanan').disable();
                Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').disable();
            }
        }
    });
    return FormDepanSetupVariabelMutuLayanan;
}
;
function getPanelPencarianSetupVariabelMutuLayanan() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Status Pulang '
                    },
                    {
                        x: 140,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 150,
                        y: 10,
                        xtype: 'textfield',
                        id: 'txtkodevariabelmutu',
                        width: 50,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_SetupVariabelMutuLayanan(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Status Pulang '
                    },
                    {
                        x: 140,
                        y: 40,
                        xtype: 'label',
                        text: ': '
                    },
                    {
                        x: 150,
                        y: 40,
                        xtype: 'textfield',
                        id: 'txtvariabel',
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_SetupVariabelMutuLayanan(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataSetupVariabelMutuLayanan',
                        handler: function () {
                            var tmpkriteria = getCriteriaFilter_viDaftar();
                            load_SetupVariabelMutuLayanan(tmpkriteria);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;



//combo dan load database

function load_SetupVariabelMutuLayanan(criteria)
{
    dataSource_SetupVariabelMutuLayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_variabel',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepSetupVariabelMutuLayanan',
                                    param: criteria
                                }
                    }
            );
    return dataSource_SetupVariabelMutuLayanan;
}

function getCriteriaFilter_viDaftar()
{
    var tmpkode = Ext.getCmp('txtkodevariabelmutu').getValue();
    var tmpnama = Ext.getCmp('txtvariabel').getValue();
    var strKriteria = '';

    if (tmpkode !== undefined || tmpnama !== undefined)
    {
        if (tmpkode !== "")
        {
            strKriteria += "  kd_variabel =  '" + tmpkode + "'";
        }
        if (tmpnama !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND variabel " + "ilike '%" + tmpnama + "%' ";
            } else
            {
                strKriteria += " variabel " + "ilike '%" + tmpnama + "%' ";
            }

        }
    }
    else
    {
        strKriteria = '';
    }

    //strKriteria += ' group by ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, kmr.NAMA_KAMAR, sp.SPESIALISASI, ask.KD_PERAWAT, p.NAMA_PERAWAT, NIP,  SP.KD_SPESIAL , AW.WAKTU, ASK.KD_WAKTU  order by ask.TANGGAL ';


    return strKriteria;
}
;

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningSetupVariabelMutuLayanan('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_SetupVariabelMutuLayanan("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoSetupVariabelMutuLayanan('Proses Saving Berhasil', 'Save');
                                load_SetupVariabelMutuLayanan("");
                                Ext.getCmp('btnHapusSetupVariabelMutuLayanan').disable();
                                Ext.getCmp('btnBatalSetupVariabelMutuLayanan').disable();
                                Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').disable();
                            }
                            else
                            {
                                ShowPesanWarningSetupVariabelMutuLayanan('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_SetupVariabelMutuLayanan("");
                                Ext.getCmp('btnHapusSetupVariabelMutuLayanan').disable();
                                Ext.getCmp('btnBatalSetupVariabelMutuLayanan').disable();
                                Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').disable();

                            }
                            ;
                        }
                    }
            );
}
;

function paramsaving()
{
    var i;
    var x = '';
    var y = '<<>>';
    for (i = 0; i < dataSource_SetupVariabelMutuLayanan.getCount(); i++)
    {
        x += y + dataSource_SetupVariabelMutuLayanan.data.items[i].data.KODE;
        x += y + dataSource_SetupVariabelMutuLayanan.data.items[i].data.NAMA;
        if (i === (dataSource_SetupVariabelMutuLayanan.getCount() - 1))
        {
            x += y;
        }
        else
        {
            x += y + '@@@@';
        }
        ;
    }

    var params =
            {
                Table: 'ViewAskepSetupVariabelMutuLayanan',
                LIST: x
            };
    return params;
}

function ShowPesanWarningSetupVariabelMutuLayanan(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoSetupVariabelMutuLayanan(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningSetupVariabelMutuLayanan('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                loadMask.hide();
                                ShowPesanInfoSetupVariabelMutuLayanan('Data berhasil di hapus', 'Information');
                                dataSource_SetupVariabelMutuLayanan.removeAt(selectrow.row);
                                Ext.getCmp('btnHapusSetupVariabelMutuLayanan').disable();
                                Ext.getCmp('btnBatalSetupVariabelMutuLayanan').disable();
                                Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').disable();
                            }
                            else
                            {
                                if (cst.pesan === 0)
                                {
                                    loadMask.hide();
                                    dataSource_SetupVariabelMutuLayanan.removeAt(selectrow.row);
                                    Ext.getCmp('btnHapusSetupVariabelMutuLayanan').disable();
                                    Ext.getCmp('btnBatalSetupVariabelMutuLayanan').disable();
                                    Ext.getCmp('btnSimpanSetupVariabelMutuLayanan').disable();
                                } else
                                {
                                    loadMask.hide();
                                    ShowPesanWarningSetupVariabelMutuLayanan('Gagal menghapus data', 'Error');
                                }

//                                caridatapasien();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepSetupVariabelMutuLayanan',
                query: "kd_variabel = '" + dataSource_SetupVariabelMutuLayanan.data.items[selectrow.row].data.KODE + "'"
            };
    return params;
}