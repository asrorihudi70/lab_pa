
var now = new Date();
var dsSpesialisasiStatistikMenuPelayanan;
var dsSpesialisasiStatistikMenuPelayananpopup;
var dsKamarStatistikMenuPelayananpopup;
var dsKelasStatistikMenuPelayananpopup;
var ListHasilStatistikMenuPelayanan;
var rowSelectedHasilStatistikMenuPelayanan;
var dsKelasStatistikMenuPelayanan;
var dsKamarStatistikMenuPelayanan;
var dsPerawatSMP;
var dsPerawatSMPPopUp;
var dsWaktuSMP;
var dsWaktuSMPPopUp;
var dataSource_SMP;
var dataSource_detSMP;
var TMPTANGGAL = now;
var TMPKD_UNIT;
var TMPNO_KAMAR;
var TMPKD_PERAWAT;
var TMPKD_WAKTU;
var rowSelected_viDaftar;

CurrentPage.page = getPanelStatistikMenuPelayanan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelStatistikMenuPelayanan(mod_id) {
    var Field = ['TANGGAL', 'KD_UNIT', 'NAMA_UNIT', 'NO_KAMAR', 'NAMA_KAMAR', 'SPESIALISASI',
        'KD_PERAWAT', 'PERAWAT', 'KD_SPESIAL', 'WAKTU', 'KD_WAKTU'];

    dataSource_SMP = new WebApp.DataStore({
        fields: Field
    });

    load_SMP("");
    var gridListHasilStatistikMenuPelayanan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_SMP,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viDaftar = dataSource_SMP.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftar = dataSource_SMP.getAt(ridx);
                if (rowSelected_viDaftar !== undefined)
                {
                    HasilStatistikMenuPelayananLookUp(rowSelected_viDaftar.data);
                }
                else
                {
                    HasilStatistikMenuPelayananLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'coltglViewHasilStatistikMenuPelayanan',
                        header: 'Tanggal',
                        dataIndex: 'TANGGAL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colMedrecViewHasilStatistikMenuPelayanan',
                        header: 'Waktu',
                        dataIndex: 'WAKTU',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colNamaViewHasilStatistikMenuPelayanan',
                        header: 'Perawat',
                        dataIndex: 'PERAWAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colAlamatViewHasilStatistikMenuPelayanan',
                        header: 'Spesialisasi',
                        dataIndex: 'SPESIALISASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKelasViewHasilStatistikMenuPelayanan',
                        header: 'Kelas',
                        dataIndex: 'NAMA_UNIT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colKamarViewHasilStatistikMenuPelayanan',
                        header: 'Kamar',
                        dataIndex: 'NAMA_KAMAR',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnEditHasilStatistikMenuPelayanan',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    if (rowSelected_viDaftar !== undefined)
                    {
                        HasilStatistikMenuPelayananLookUp(rowSelected_viDaftar.data);
                    }
                    else
                    {
                        ShowPesanWarningSMP('Silahkan Pilih Data Terlebih Dahulu  ', 'Statistik Menu Pelayanan');
                    }
                }
            },
            {
                id: 'btnNewHasilStatistikMenuPelayanan',
                text: 'Data Baru',
                tooltip: 'Data Baru',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    HasilStatistikMenuPelayananLookUp();
                    load_detsmp('1991/01/01', '', '', '', '');
                }
            }
        ]
    });
    var FormDepanStatistikMenuPelayanan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'StatistikMenuPelayanan',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilStatistikMenuPelayanan
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('chkTglSMP').setValue(true);
//                Ext.getCmp('dtpTanggalakhirStatistikPelayanan').disable();
//                Ext.getCmp('dtpTanggalawalStatistikPelayanan').disable();
            }
        }
    });
    return FormDepanStatistikMenuPelayanan;
}
;
function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiStatistikMenuPelayanan(),
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasStatistikMenuPelayanan(),
                    {
                        x: 320,
                        y: 40,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 370,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarStatistikMenuPelayanan(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Perawat '
                    }, {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPerawat(),
                    {
                        x: 320,
                        y: 70,
                        xtype: 'label',
                        text: 'Waktu '
                    },
                    {
                        x: 370,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboWaktu(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalStatistikPelayanan',
                        id: 'dtpTanggalawalStatistikPelayanan', format: 'd/M/Y',
                        value: now,
                        width: 185
                    },
                    {
                        x: 320,
                        y: 100,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 400,
                        y: 100,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirStatistikPelayanan',
                        id: 'dtpTanggalakhirStatistikPelayanan',
                        format: 'd/M/Y',
                        value: now,
                        width: 170
                    },
                    {
                        x: 575,
                        y: 105,
                        xtype: 'checkbox',
                        id: 'chkTglSMP',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalakhirStatistikPelayanan').enable();
                                Ext.getCmp('dtpTanggalawalStatistikPelayanan').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalakhirStatistikPelayanan').disable();
                                Ext.getCmp('dtpTanggalawalStatistikPelayanan').disable();
                            }
                        }
                    },
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataSMP',
                        handler: function () {
                            var tmpkriteriaSMP = getCriteriaFilter_viDaftar();
                            loadfilter_SMP(tmpkriteriaSMP)
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilStatistikMenuPelayananLookUp(rowdata) {
    FormLookUpdetailStatistikMenuPelayanan = new Ext.Window({
        id: 'gridHasilStatistikMenuPelayanan',
        title: 'StatistikMenuPelayanan',
        closeAction: 'destroy',
        width: 1000,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupSMP()],
        listeners: {
            destroy: function (win)
            {
                load_SMP("");
//                alert('a');
            }
        }
    });
    FormLookUpdetailStatistikMenuPelayanan.show();
    if (rowdata === undefined) {

    } else {
        datainit_SMP(rowdata);
    }

}
;
function formpopupSMP() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopupSMP',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '99%',
                        iconCls: '',
                        items:
                                [
                                    PanelStatistikMenuPelayanan(),
                                    DataVariableMenuLayanan()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {
                                                        SavingData();

                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupdatahasilrad;
}
;
function PanelStatistikMenuPelayanan() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 975,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Tgl Pelaksanaan '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'datefield',
                        name: 'dtpTanggalPelaksanaan',
                        id: 'dtpTanggalPelaksanaan',
                        format: 'd/M/Y',
                        value: now,
                        listeners: {
                            select: function (t, n, o) {
                                TMPTANGGAL = Ext.get('dtpTanggalPelaksanaan').getValue();
//                                alert(TMPTANGGAL)
                            }

                        }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiStatistikMenuPelayananpopup(),
                    {
                        x: 320,
                        y: 40,
                        xtype: 'label',
                        text: 'Kelas '
                    },
                    {
                        x: 370,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    KelasStatistikMenuPelayananpopup(),
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    comboKamarStatistikMenuPelayananpopup(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Perawat '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboPerawatPopUp(),
                    {
                        x: 320,
                        y: 100,
                        xtype: 'label',
                        text: 'Waktu '
                    },
                    {
                        x: 370,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboWaktuPopUp()
                ]
            }
        ]
    };
    return items;
}
;
function DataVariableMenuLayanan()
{
    var Field = ['KD_VARIABEL', 'VARIABEL', 'PSBARU', 'PSLAMA'];

    dataSource_detSMP = new WebApp.DataStore({
        fields: Field
    });
    var gridListHasilSMP = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_detSMP,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
            }
        }),
        listeners: {
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colMedrecViewHasilSMP',
                        header: 'Variabel',
                        dataIndex: 'KD_VARIABEL',
                        sortable: false,
                        hidden: true,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colMedrecViewHasilSMP',
                        header: 'Variabel',
                        dataIndex: 'VARIABEL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 200
                    },
                    {
                        id: 'colBaruViewHasilSMP',
                        header: 'ps. Baru',
                        dataIndex: 'PSBARU',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25,
                        editor: {
                            xtype: 'textfield',
                        }
                    },
                    {
                        id: 'colLamaViewHasilSMP',
                        header: 'Ps. Lama',
                        dataIndex: 'PSLAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 25,
                        editor: {
                            xtype: 'textfield'
                        }
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
        ]
    });
    var PanelDataVariableMenuLayanan = new Ext.Panel
            (
                    {
                        title: 'Data Variable Menu Layanan',
                        id: 'PanelDataVariableMenuLayanan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'fit',
                        height: 400,
                        width: 975,
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            gridListHasilSMP
                        ]
                    }
            );
    return PanelDataVariableMenuLayanan;
}
;



//combo dan load database

function datainit_SMP(rowdata)
{
    load_detsmp(rowdata.TANGGAL, rowdata.KD_UNIT, rowdata.NO_KAMAR, rowdata.KD_PERAWAT, rowdata.KD_WAKTU);

    TMPTANGGAL = rowdata.TANGGAL;
    TMPKD_UNIT = rowdata.KD_UNIT;
    TMPNO_KAMAR = rowdata.NO_KAMAR;
    TMPKD_PERAWAT = rowdata.KD_PERAWAT;
    TMPKD_WAKTU = rowdata.KD_WAKTU;

    Ext.getCmp('dtpTanggalPelaksanaan').setValue(rowdata.TANGGAL);
    Ext.getCmp('cboSpesialisasiStatistikMenuPelayananpopup').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('cbokelasStatistikMenuPelayananpopup').setValue(rowdata.NAMA_UNIT);
    Ext.getCmp('cboKamarStatistikMenuPelayananpopup').setValue(rowdata.NAMA_KAMAR);
    Ext.getCmp('cboPerawatSMPPopUp').setValue(rowdata.PERAWAT);
    Ext.getCmp('cboWaktuSMPPopUp').setValue(rowdata.WAKTU);


}
function mComboSpesialisasiStatistikMenuPelayanan()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiStatistikMenuPelayanan = new WebApp.DataStore({fields: Field});
    dsSpesialisasiStatistikMenuPelayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiStatistikMenuPelayanan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 10,
                        id: 'cboSpesialisasiStatistikMenuPelayanan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiStatistikMenuPelayanan,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        if (Ext.getCmp('cboSpesialisasiStatistikMenuPelayanan').getValue() === '0')
                                        {
                                            Ext.getCmp('cboKelasStatistikMenuPelayanan').disable();
                                            Ext.getCmp('cboKamarStatistikMenuPelayanan').disable();
                                        } else
                                        {
                                            Ext.getCmp('cboKelasStatistikMenuPelayanan').enable();
                                            Ext.getCmp('cboKamarStatistikMenuPelayanan').enable();
                                        }
                                        Ext.getCmp('cboKelasStatistikMenuPelayanan').setValue('');
                                        Ext.getCmp('cboKamarStatistikMenuPelayanan').setValue('');
                                        loaddatastoreKelasStatistikMenuPelayanan(b.data.KD_SPESIAL);
                                        var tmpkriteriaSMP = getCriteriaFilter_viDaftar();
                                        load_SMP(tmpkriteriaSMP);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasStatistikMenuPelayanan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiStatistikMenuPelayanan;
}
;
function loaddatastoreKelasStatistikMenuPelayanan(kd_spesial)
{
    dsKelasStatistikMenuPelayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasStatistikMenuPelayanan()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasStatistikMenuPelayanan = new WebApp.DataStore({fields: Field});
    var cboKelasStatistikMenuPelayanan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboKelasStatistikMenuPelayanan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasStatistikMenuPelayanan,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        widht: 166,
                        listeners:
                                {'select': function (a, b, c)
                                    {

                                        Ext.getCmp('cboKamarStatistikMenuPelayanan').setValue('');
                                        loaddatastoreKamarStatistikMenuPelayanan(b.data.kd_unit);
                                        var tmpkriteriaSMP = getCriteriaFilter_viDaftar();
                                        load_SMP(tmpkriteriaSMP);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarStatistikMenuPelayanan').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasStatistikMenuPelayanan;
}
;
function loaddatastoreKamarStatistikMenuPelayanan(kd_unit)
{
    dsKamarStatistikMenuPelayanan.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiStatistikMenuPelayanan').getValue()
                                }
                    });
}

function mComboKamarStatistikMenuPelayanan()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarStatistikMenuPelayanan = new WebApp.DataStore({fields: Field});
    var cboKamarStatistikMenuPelayanan = new Ext.form.ComboBox
            (
                    {
                        x: 400,
                        y: 40,
                        id: 'cboKamarStatistikMenuPelayanan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarStatistikMenuPelayanan,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaSMP = getCriteriaFilter_viDaftar();
                                        load_SMP(tmpkriteriaSMP);
                                    }
                                }
                    }
            );
    return cboKamarStatistikMenuPelayanan;
}
;

function mComboSpesialisasiStatistikMenuPelayananpopup()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiStatistikMenuPelayananpopup = new WebApp.DataStore({fields: Field});
    dsSpesialisasiStatistikMenuPelayananpopup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiStatistikMenuPelayananpopup = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboSpesialisasiStatistikMenuPelayananpopup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local', forceSelection: true,
                        emptyText: 'Select a Spesialisasi...',
                        selectOnFocus: true,
                        fieldLabel: 'Propinsi',
                        align: 'Right',
                        store: dsSpesialisasiStatistikMenuPelayananpopup,
                        valueField: 'KD_SPESIAL',
                        displayField: 'SPESIALISASI',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cbokelasStatistikMenuPelayananpopup').setValue('');
                                        Ext.getCmp('cboKamarStatistikMenuPelayananpopup').setValue('');
//                                        KDUNIT = Ext.getCmp('cboSpesialisasiStatistikMenuPelayananpopup').getValue();
                                        loadcboKamarStatistikMenuPelayananpopup(b.data.KD_SPESIAL);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKelasStatistikMenuPelayananpopup').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboSpesialisasiStatistikMenuPelayananpopup;
}
;
function loadcboKamarStatistikMenuPelayananpopup(kd_spesial)
{
    dsKelasStatistikMenuPelayananpopup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function KelasStatistikMenuPelayananpopup()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasStatistikMenuPelayananpopup = new WebApp.DataStore({fields: Field});
    var comboKelasStatistikMenuPelayananpopup = new Ext.form.ComboBox
            (
                    {
                        x: 400,
                        y: 40,
                        id: 'cbokelasStatistikMenuPelayananpopup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasStatistikMenuPelayananpopup,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        widht: 166,
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarStatistikMenuPelayananpopup').setValue('');
                                        KamarStatistikMenuPelayananpopup(b.data.kd_unit);
                                        //alert(Ext.getCmp('cbokelasStatistikMenuPelayananpopup').getValue())
                                        TMPKD_UNIT = Ext.getCmp('cbokelasStatistikMenuPelayananpopup').getValue();
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarStatistikMenuPelayananpopup').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return comboKelasStatistikMenuPelayananpopup;
}
;
function KamarStatistikMenuPelayananpopup(kd_unit)
{
    dsKamarStatistikMenuPelayananpopup.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiStatistikMenuPelayananpopup').getValue()
                                }
                    });
}

function comboKamarStatistikMenuPelayananpopup()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarStatistikMenuPelayananpopup = new WebApp.DataStore({fields: Field});
    var combKamarStatistikMenuPelayananpopup = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboKamarStatistikMenuPelayananpopup',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local',
                        forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarStatistikMenuPelayananpopup,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                        alert(Ext.getCmp('cboKamarStatistikMenuPelayananpopup').getValue())
                                        TMPNO_KAMAR = Ext.getCmp('cboKamarStatistikMenuPelayananpopup').getValue();
                                    }
                                }
                    }
            );
    return combKamarStatistikMenuPelayananpopup;
}
;

function load_SMP(criteria)
{
    dataSource_SMP.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepStatistikMenuPelayanan',
                                    param: criteria
                                }
                    }
            );
    return dataSource_SMP;
}

function getCriteriaFilter_viDaftar()
{

    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiStatistikMenuPelayanan').getValue();
    var tmpkelas = Ext.getCmp('cboKelasStatistikMenuPelayanan').getValue();
    var tmpkamar = Ext.getCmp('cboKamarStatistikMenuPelayanan').getValue();
    var tmpperawat = Ext.getCmp('cboPerawatSMP').getValue();
    var tmpwaktu = Ext.getCmp('cboWaktuSMP').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalStatistikPelayanan').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirStatistikPelayanan').getValue();
    var tmptambahan = Ext.getCmp('chkTglSMP').getValue();
    var strKriteria = '';
    if (tmpspesialisasi !== '0')
    {
        if (tmptambahan === true)
        {
            strKriteria += "  ask.TANGGAL BETWEEN '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
        }

        if (tmpkelas !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND ask.KD_UNIT = '" + tmpkelas + "'";
            } else
            {
                strKriteria += " ask.KD_UNIT = '" + tmpkelas + "'";
            }

        }
        if (tmpkamar !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND ask.NO_KAMAR = '" + tmpkamar + "' ";
            } else
            {
                strKriteria += " ask.NO_KAMAR = '" + tmpkamar + "' ";
            }

        }
        if (tmpperawat !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND ask.KD_PERAWAT ='" + tmpperawat + "' ";
            } else
            {
                strKriteria += " ask.KD_PERAWAT ='" + tmpperawat + "' ";
            }

        }
        if (tmpwaktu !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND ASK.KD_WAKTU = " + tmpwaktu + " ";
            } else
            {
                strKriteria += " ASK.KD_WAKTU = " + tmpwaktu + " ";
            }

        }
        strKriteria += ' group by ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, kmr.NAMA_KAMAR, sp.SPESIALISASI, ask.KD_PERAWAT, p.NAMA_PERAWAT, NIP,  SP.KD_SPESIAL , AW.WAKTU, ASK.KD_WAKTU  order by ask.TANGGAL ';
    } else
    {
        if (tmptambahan === true)
        {
            strKriteria += "  ask.TANGGAL BETWEEN '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
        }
        if (tmpperawat !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND ask.KD_PERAWAT ='" + tmpperawat + "' ";
            } else
            {
                strKriteria += " ask.KD_PERAWAT ='" + tmpperawat + "' ";
            }

        }
        if (tmpwaktu !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND ASK.KD_WAKTU = " + tmpwaktu + " ";
            } else
            {
                strKriteria += " ASK.KD_WAKTU = " + tmpwaktu + " ";
            }

        }
//        strKriteria += ' group by ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, kmr.NAMA_KAMAR, sp.SPESIALISASI, ask.KD_PERAWAT, p.NAMA_PERAWAT, NIP,  SP.KD_SPESIAL , AW.WAKTU, ASK.KD_WAKTU  order by ask.TANGGAL ';
    }

    return strKriteria;
}

function radiojelas(tmp)
{
    if (tmp === 'rbjelasya')
    {
        Ext.getDom('rbjelastidak').checked = false;
    }
    if (Ext.getCmp('rbjelastidak').getValue() === true)
    {
        Ext.getDom('rbjelasya').checked = false;
    }
}

function mComboPerawat()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatSMP = new WebApp.DataStore({fields: Field});
    dsPerawatSMP.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatSMP = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboPerawatSMP',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatSMP,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaSMP = getCriteriaFilter_viDaftar();
                                        load_SMP(tmpkriteriaSMP);
                                        kd_perawat = Ext.getCmp('cboPerawatSMP').getValue();
                                    }

                                }
                    }
            );
    return cboPerawatSMP;
}
;
function mComboWaktu()
{
    var Field = ['KD_WAKTU', 'WAKTU'];
    dsWaktuSMP = new WebApp.DataStore({fields: Field});
    dsWaktuSMP.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewWaktu',
                                    param: ''
                                }
                    }
            );
    var cboWaktuSMP = new Ext.form.ComboBox
            (
                    {
                        x: 400,
                        y: 70,
                        id: 'cboWaktuSMP',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Waktu...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsWaktuSMP,
                        valueField: 'KD_WAKTU',
                        displayField: 'WAKTU',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaSMP = getCriteriaFilter_viDaftar();
                                        load_SMP(tmpkriteriaSMP);
                                        kd_waktu = Ext.getCmp('cboWaktuSMP').getValue();
                                    }

                                }
                    }
            );
    return cboWaktuSMP;
}
;

function mComboPerawatPopUp()
{
    var Field = ['KODE', 'NAMA'];
    dsPerawatSMPPopUp = new WebApp.DataStore({fields: Field});
    dsPerawatSMPPopUp.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewPerawat',
                                    param: 'Aktif = 1 Order By nama_perawat'
                                }
                    }
            );
    var cboPerawatSMPPopUp = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboPerawatSMPPopUp',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Perawat...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsPerawatSMPPopUp,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        //alert(Ext.getCmp('cboPerawatSMPPopUp').getValue())
                                        TMPKD_PERAWAT = Ext.getCmp('cboPerawatSMPPopUp').getValue();
                                    }

                                }
                    }
            );
    return cboPerawatSMPPopUp;
}
;
function mComboWaktuPopUp()
{
    var Field = ['KD_WAKTU', 'WAKTU'];
    dsWaktuSMPPopUp = new WebApp.DataStore({fields: Field});
    dsWaktuSMPPopUp.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'ViewWaktu',
                                    param: ''
                                }
                    }
            );
    var cboWaktuSMPPopUp = new Ext.form.ComboBox
            (
                    {
                        x: 400,
                        y: 100,
                        id: 'cboWaktuSMPPopUp',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Waktu...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsWaktuSMPPopUp,
                        valueField: 'KD_WAKTU',
                        displayField: 'WAKTU',
                        widht: 166,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        //alert(Ext.getCmp('cboWaktuSMPPopUp').getValue())
                                        TMPKD_WAKTU = Ext.getCmp('cboWaktuSMPPopUp').getValue();
                                    }

                                }
                    }
            );
    return cboWaktuSMPPopUp;
}
;


function load_detsmp(TANGGAL, KD_UNIT, NO_KAMAR, KD_PERAWAT, KD_WAKTU)
{
    if (TANGGAL === '') {
        TANGGAL = '';
    }
    if (KD_UNIT === '') {
        KD_UNIT = '';
    }
    if (NO_KAMAR === '') {
        NO_KAMAR = '';
    }
    if (KD_PERAWAT === '') {
        KD_PERAWAT = '';
    }
    if (KD_WAKTU === '') {
        KD_WAKTU = 0;
    }

    var criteria = "b.TANGGAL='" + TANGGAL + "' and b.KD_UNIT='" + KD_UNIT + "' and b.NO_KAMAR='" + NO_KAMAR + "' and kd_perawat = '" + KD_PERAWAT + "'  and b.KD_WAKTU =   " + KD_WAKTU + "";

    dataSource_detSMP.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetSMP',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detSMP;
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningSMP('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                load_detsmp(cst.tanggal, cst.kd_unit, cst.no_kamar, cst.kd_perawat, cst.kd_waktu);
                                ShowPesanInfoSMP('Proses Saving Berhasil', 'Save');
                            }
                            else
                            {
                                load_detsmp(cst.tanggal, cst.kd_unit, cst.no_kamar, cst.kd_perawat, cst.kd_waktu);
                                ShowPesanWarningSMP('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');

                            }
                            ;
                        }
                    }
            );
}
;

function paramsaving()
{
    var i;
    var x = '';
    var y = '<<>>';
    for (i = 0; i < dataSource_detSMP.getCount(); i++)
    {
        x += y + dataSource_detSMP.data.items[i].data.KD_VARIABEL;
        x += y + dataSource_detSMP.data.items[i].data.PSBARU;
        x += y + dataSource_detSMP.data.items[i].data.PSLAMA;
        if (i === (dataSource_detSMP.getCount() - 1))
        {
            x += y;
        }
        else
        {
            x += y + '@@@@';
        }
        ;
    }

    var params =
            {
                Table: 'ViewAskepStatistikMenuPelayanan',
                LIST: x,
                TGL: TMPTANGGAL,
                KDUNIT: TMPKD_UNIT,
                NOKAMAR: TMPNO_KAMAR,
                KDPERAWAT: TMPKD_PERAWAT,
                KDWAKTU: TMPKD_WAKTU
            };
    return params;
}

function ShowPesanWarningSMP(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoSMP(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningSMP('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoSMP('Data berhasil di hapus', 'Information');
                                load_detsmp('1991/01/01', '', '', '', '');

                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningSMP('Gagal menghapus data', 'Error');
//                                caridatapasien();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepStatistikMenuPelayanan',
                query: "tanggal = '" + TMPTANGGAL + "' and kd_unit = '" + TMPKD_UNIT + "' and no_kamar = '" + TMPNO_KAMAR + "' and kd_perawat = '" + TMPKD_PERAWAT + "' and kd_waktu = " + TMPKD_WAKTU + ""
            };
    return params;
}