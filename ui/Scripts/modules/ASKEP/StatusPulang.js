
var now = new Date();
var dsSpesialisasiStatusPulang;
var ListHasilStatusPulang;
var rowSelectedHasilStatusPulang;
var dsKelasStatusPulang;
var dsKamarStatusPulang;
var dsStatusPulang;
var dataSource_StatusPulang;
var TMPTANGGAL = now;
var rowSelected_viDaftar;
var selectrow = {
    data: Object,
    details: Array,
    row: 0
};


CurrentPage.page = getPanelStatusPulang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelStatusPulang(mod_id) {

    var Field = ['KODE', 'NAMA'];

    dataSource_StatusPulang = new WebApp.DataStore({
        fields: Field
    });
    load_StatusPulang("");
    var gridListHasilStatusPulang = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_StatusPulang,
        id: 'gridperawat',
        anchor: '100% 75%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {

                    rowSelected_viDaftar = dataSource_StatusPulang.getAt(row);
                    selectrow.row = row;
                    selectrow.data = rowSelected_viDaftar;

                    Ext.getCmp('btnHapusStatusPulang').enable();
                    Ext.getCmp('btnBatalStatusPulang').enable();
                    Ext.getCmp('btnSimpanStatusPulang').enable();

//                    Ext.getCmp('txtkodestatuspulang').setValue(rowSelected_viDaftar.data.KODE);
//                    Ext.getCmp('txttatuspulang').setValue(rowSelected_viDaftar.data.NIP);
//                    Ext.getCmp('txtNamaperawat').setValue(rowSelected_viDaftar.data.NAMA);
//                    if (rowSelected_viDaftar.data.STATUS === 'Aktif')
//                    {
//                        Ext.getCmp('cbstatus').setValue(true);
//                    } else
//                    {
//                        Ext.getCmp('cbstatus').setValue(false);
//                    }
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftar = dataSource_StatusPulang.getAt(ridx);
                if (rowSelected_viDaftar !== undefined)
                {
                    Ext.getCmp('btnSimpanStatusPulang').enable();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colkdViewHasilStatusPulang',
                        header: 'Kode Status Pulang',
                        dataIndex: 'KODE',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colperawatViewHasilStatusPulang',
                        header: 'Status Pulang',
                        dataIndex: 'NAMA',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 300,
                        editor: {
                            xtype: 'textfield'
                        }
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnTambahHasilStatusPulang',
                text: 'Tambah Data',
                tooltip: 'Tambah Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    Ext.Ajax.request
                            (
                                    {
                                        url: baseURL + "index.php/main/CreateDataObj",
                                        params: {Table: 'ViewAskepSetupLastStatusPulang'},
                                        success: function (o)
                                        {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                var tmpkode = cst.kode;
                                                if (tmpkode.toString().length === 1)
                                                {
                                                    tmpkode = '0' + tmpkode;
                                                }
                                                defaultData = {
                                                    KODE: tmpkode,
                                                    NAMA: '',
                                                    STATUS: 'Tidak Aktif',
                                                    NIP: ''
                                                };
                                                var recId = 100; // provide unique id for the record
                                                var r = new dataSource_StatusPulang.recordType(defaultData, ++recId); // create new record
                                                dataSource_StatusPulang.insert(0, r);
                                            }
                                            ;
                                        }
                                    }
                            );


                }
            },
            {
                id: 'btnSimpanStatusPulang',
                text: 'Simpan Data',
                tooltip: 'Simpan Data',
                iconCls: 'save',
                handler: function (sm, row, rec) {
                    SavingData();
                }
            },
            {
                id: 'btnHapusStatusPulang',
                text: 'Hapus Data',
                tooltip: 'Hapus Data',
                iconCls: 'remove',
                handler: function (sm, row, rec) {
                    Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                        if (button === 'yes') {
                            loadMask.show();
                            DeleteData();
                        }
                    });
                }
            },
            {
                id: 'btnBatalStatusPulang',
                text: 'Batal Edit',
                tooltip: 'Batal Data',
                iconCls: 'remove',
                handler: function (sm, row, rec) {
//                    console.log(dataSource_StatusPulang.data.items[selectrow.row].data.NAMA);
                    if (dataSource_StatusPulang.data.items[selectrow.row].data.NAMA === "")
                    {
                        dataSource_StatusPulang.removeAt(selectrow.row);
                    } else
                    {

                    }
                    Ext.getCmp('btnHapusStatusPulang').disable();
                    Ext.getCmp('btnBatalStatusPulang').disable();
                    Ext.getCmp('btnSimpanStatusPulang').disable();
                }
            }
        ]
    });
    var FormDepanStatusPulang = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'StatusPulang',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianStatusPulang(),
            gridListHasilStatusPulang
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('btnHapusStatusPulang').disable();
                Ext.getCmp('btnBatalStatusPulang').disable();
                Ext.getCmp('btnSimpanStatusPulang').disable();
            }
        }
    });
    return FormDepanStatusPulang;
}
;
function getPanelPencarianStatusPulang() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Status Pulang '
                    },
                    {
                        x: 140,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 150,
                        y: 10,
                        xtype: 'textfield',
                        id: 'txtkodestatuspulang',
                        width: 50,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_StatusPulang(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Status Pulang '
                    },
                    {
                        x: 140,
                        y: 40,
                        xtype: 'label',
                        text: ': '
                    },
                    {
                        x: 150,
                        y: 40,
                        xtype: 'textfield',
                        id: 'txttatuspulang',
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
                                            load_StatusPulang(tmpkriteria);
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataStatusPulang',
                        handler: function () {
                            var tmpkriteria = getCriteriaFilter_viDaftar();
                            load_StatusPulang(tmpkriteria);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;



//combo dan load database

function load_StatusPulang(criteria)
{
    dataSource_StatusPulang.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_status_pulang',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepStatusPulang',
                                    param: criteria
                                }
                    }
            );
    return dataSource_StatusPulang;
}

function getCriteriaFilter_viDaftar()
{
    var tmpkode = Ext.getCmp('txtkodestatuspulang').getValue();
    var tmpnama = Ext.getCmp('txttatuspulang').getValue();
    var strKriteria = '';

    if (tmpkode !== undefined || tmpnama !== undefined)
    {
        if (tmpkode !== "")
        {
            strKriteria += "  kd_status_pulang =  '" + tmpkode + "'";
        }
        if (tmpnama !== "")
        {
            if (strKriteria !== '')
            {
                strKriteria += " AND status_pulang " + "ilike '%" + tmpnama + "%' ";
            } else
            {
                strKriteria += " status_pulang " + "ilike '%" + tmpnama + "%' ";
            }

        }
    }
    else
    {
        strKriteria = '';
    }

    //strKriteria += ' group by ask.TANGGAL, ask.KD_UNIT, u.NAMA_UNIT,  ask.NO_KAMAR, kmr.NAMA_KAMAR, sp.SPESIALISASI, ask.KD_PERAWAT, p.NAMA_PERAWAT, NIP,  SP.KD_SPESIAL , AW.WAKTU, ASK.KD_WAKTU  order by ask.TANGGAL ';


    return strKriteria;
}
;


function load_detsmp(TANGGAL, KD_UNIT, NO_KAMAR, KD_PERAWAT, KD_WAKTU)
{
    if (TANGGAL === '') {
        TANGGAL = '';
    }
    if (KD_UNIT === '') {
        KD_UNIT = '';
    }
    if (NO_KAMAR === '') {
        NO_KAMAR = '';
    }
    if (KD_PERAWAT === '') {
        KD_PERAWAT = '';
    }
    if (KD_WAKTU === '') {
        KD_WAKTU = 0;
    }

    var criteria = "b.TANGGAL='" + TANGGAL + "' and b.KD_UNIT='" + KD_UNIT + "' and b.NO_KAMAR='" + NO_KAMAR + "' and kd_perawat = '" + KD_PERAWAT + "'  and b.KD_WAKTU =   " + KD_WAKTU + "";

    dataSource_detStatusPulang.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 50,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewAskepDetStatusPulang',
                                    param: criteria
                                }
                    }
            );
    return dataSource_detStatusPulang;
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningStatusPulang('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_StatusPulang("");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoStatusPulang('Proses Saving Berhasil', 'Save');
                                load_StatusPulang("");
                                Ext.getCmp('btnHapusStatusPulang').disable();
                                Ext.getCmp('btnBatalStatusPulang').disable();
                                Ext.getCmp('btnSimpanStatusPulang').disable();
                            }
                            else
                            {
                                ShowPesanWarningStatusPulang('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_StatusPulang("");
                                Ext.getCmp('btnHapusStatusPulang').disable();
                                Ext.getCmp('btnBatalStatusPulang').disable();
                                Ext.getCmp('btnSimpanStatusPulang').disable();

                            }
                            ;
                        }
                    }
            );
}
;

function paramsaving()
{
    var i;
    var x = '';
    var y = '<<>>';
    for (i = 0; i < dataSource_StatusPulang.getCount(); i++)
    {
        x += y + dataSource_StatusPulang.data.items[i].data.KODE;
        x += y + dataSource_StatusPulang.data.items[i].data.NAMA;
        if (i === (dataSource_StatusPulang.getCount() - 1))
        {
            x += y;
        }
        else
        {
            x += y + '@@@@';
        }
        ;
    }

    var params =
            {
                Table: 'ViewAskepStatusPulang',
                LIST: x
            };
    return params;
}

function ShowPesanWarningStatusPulang(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoStatusPulang(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningStatusPulang('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                loadMask.hide();
                                ShowPesanInfoStatusPulang('Data berhasil di hapus', 'Information');
                                dataSource_StatusPulang.removeAt(selectrow.row);
                                Ext.getCmp('btnHapusStatusPulang').disable();
                                Ext.getCmp('btnBatalStatusPulang').disable();
                                Ext.getCmp('btnSimpanStatusPulang').disable();
                            }
                            else
                            {
                                if (cst.pesan === 0)
                                {
                                    loadMask.hide();
                                    dataSource_StatusPulang.removeAt(selectrow.row);
                                    Ext.getCmp('btnHapusStatusPulang').disable();
                                    Ext.getCmp('btnBatalStatusPulang').disable();
                                    Ext.getCmp('btnSimpanStatusPulang').disable();
                                } else
                                {
                                    loadMask.hide();
                                    ShowPesanWarningStatusPulang('Gagal menghapus data', 'Error');
                                }

//                                caridatapasien();
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepStatusPulang',
                query: "kd_status_pulang = '" + dataSource_StatusPulang.data.items[selectrow.row].data.KODE + "'"
            };
    return params;
}