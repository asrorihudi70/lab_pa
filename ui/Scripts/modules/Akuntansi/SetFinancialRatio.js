﻿
var dsJurnalList;
var AddNewJurnal=false;
var selectCountJurnal=50;
var JurnalLookUps;
var rowSelectedSetupJurnal;

CurrentPage.page=getPanelJurnal(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelJurnal(mod_id) 
{
	
    var Field = ['Journal_Code','Description'];
    dsJurnalList = new WebApp.DataStore({ fields: Field });
	RefreshDataJurnal();

    var grListJurnal = new Ext.grid.EditorGridPanel
    (
		{ 
			id:'grListJurnal',
			stripeRows: true,
			store: dsJurnalList,
			autoScroll: true,
		    columnLines: true,
			border:false,
			anchor: '100% 100%',
			sm: new Ext.grid.RowSelectionModel
            (
				{ 
					singleSelect: true,
                    listeners: 
					{ 
						rowselect: function(sm, row, rec)
						{
							rowSelectedSetupJurnal = dsJurnalList.getAt(row);
						}
                    }
                }
			),
            cm: new Ext.grid.ColumnModel
            (
				[	
					new Ext.grid.RowNumberer(),
                    { 	
						id: 'ROWID_FinancialR',
						header: "Row ID",
						dataIndex: 'Journal_Code',
						sortable: true,
						width: 30
                    },
					{ 	
						id: 'Desc_FinancialR',
						header: "Description",
						dataIndex: 'Description',
						width: 100,
						sortable: true                           
					},
					{ 	
						id: 'Acc_FinancialR',
						header: "Account",
						dataIndex: 'Description',
						width: 75,
						sortable: true                           
					},
					{ 	
						id: 'Div_FinancialR',
						header: "Divider Account",
						dataIndex: 'Description',
						width: 75,
						sortable: true                           
					}
                ]
			),
            tbar: 
			[
				{
				    id: 'btnEditJurnal',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetupJurnal != undefined)
						{
							JurnalLookUp(rowSelectedSetupJurnal.data);
						}
						else
						{
							JurnalLookUp();
						}
				    }
				},' ','-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsJurnalList,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: { forceFit: true }
        }
	); 

	
    var FormJurnal = new Ext.Panel
    (
		{ 
			id: mod_id,
			closable:true,
            region: 'center',
            layout: 'form',
            title: 'Financial Ratio Information',
            itemCls: 'blacklabel',
            bodyStyle: 'padding:15px',
            border: false,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
			margins:'0 5 5 0',
			anchor:'100%',
            iconCls: 'SetupJenisJurnal',
            items: [grListJurnal],
			tbar:
			[
				'Row ID : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Row ID : ',
					id: 'txtKDJurnalFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Description : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Description : ',
					id: 'txtJurnalFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Maks.Data : ', ' ',mComboMaksDataJurnal(),
				' ','-',
				{
				    id: 'btnRefreshJurnal',
				    text: 'Refresh',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataJurnalFilter();
					}
				}
			],
            listeners:
			{ 'afterrender': function()
                {   
					//Ext.getCmp('cboDESKRIPSI').store = getJurnal();
                }
            }
        }
	); 
	
	RefreshDataJurnal();
	return FormJurnal
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///

function RefreshDataJurnal()
{	
	dsJurnalList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountJurnal, 
				Sort: 'Journal_Code', 
				Sortdir: 'DESC', 
				target:'JenisJurnal',
				param: ''
			} 
		}
	);
	rowSelectedSetupJurnal = undefined;
	return dsJurnalList;
};

function JurnalLookUp(rowdata)
{   
	var lebar=600;
    JurnalLookUps = new Ext.Window   	
    (
		{ 	
			id: 'JurnalLookUps',
			title: 'Financial Ratio Information',
			closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 152,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'SetupJenisJurnal',
			modal: true,
			items: getFormEntryJurnal(lebar),
			listeners:
            { 
				activate: function() 
				{ 
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetupJurnal=undefined;
					RefreshDataJurnal();
				}
            }
        }
	);
		
    JurnalLookUps.show();
	if (rowdata == undefined)
	{
		ACC_JOURNALAddNew();
	}
	else
	{
		JurnalInit(rowdata)
	}
};

///-------------------------------------------------------------------------------------///


function getFormEntryJurnal(lebar)   
{	
	
	var pnlJurnal = new Ext.FormPanel  
    (
		{ 	
			id: 'PanelJurnal',
			fileUpload: true,
			region: 'north',
			layout: 'column',
			height: 120,
			anchor: '100%' ,
			bodyStyle: 'padding:10px 0px 10px 10px',	
			iconCls: 'SetupJenisJurnal',
			border: true,
			items: 
			[
				{
					layout: 'column',
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
					width:lebar-36.5,
					labelAlign: 'right',
					anchor: '100%',
					items: 
					[
						{ 	
							columnWidth:.989,
							layout: 'form',
							id: 'PnlKiriJurnal',
							labelWidth:70,//65,
							border: false,
							items: 
							[	
								{
									xtype: 'compositefield',
									fieldLabel: 'Row ID',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtRowIDFinancialRatio',
											flex: 1,
											width: 50
										},
										{
											xtype: 'label',
											forId: 'lblDescFinancialRatio',
											text: 'Description: ',
											style: 'margin: 3px 30px 0 10px',
											width: 80//40
										},
										{
											xtype: 'textfield',
											id: 'txtDescFinancialRatio',
											flex: 1
										}
									]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Account',
									items:
									[
										{
											xtype: 'textfield',
											id: 'txtAccountFinancialRatio',
											flex: 1,
											width: 130
										},
										{
											xtype: 'label',
											forId: 'lblDividerFinancialRatio',
											text: 'Divider Account: ',
											style: 'margin: 3px 30px 0 10px',
											width: 100//40
										},
										{
											xtype: 'textfield',
											id: 'txtDividerFinancialRatio',
											flex: 1,
											width: 80
										}
									]
								},
							]
						}
					]
				}
			],
			tbar: 
			[	
				{ 	
					id:'btnAddJurnal',
					text: 'Tambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() { ACC_JOURNALAddNew() }
				}, '-',
				{ 	
					id:'btnSimpanJurnal',
					text: 'Simpan',
					tooltip: 'Simpan Data ',
					iconCls: 'save',
					handler: function()
					{ 
						ACC_JOURNALSave(false);
						RefreshDataJurnal();		
					}
				}, '-',
				{
					id:'btnSimpanCloseJurnal',
					text: 'Simpan & Keluar',
					tooltip: 'Simpan dan Keluar',
					iconCls: 'saveexit',
					handler: function() 
					{  
						var x = ACC_JOURNALSave(true);
						RefreshDataJurnal();
						if (x===undefined)
						{
							JurnalLookUps.close();
						};
					}
				},
				'-',
				{ 	
					id:'btnHapusJurnal',
					text: 'Hapus',
					tooltip: 'Hapus Data',
					iconCls: 'remove',
					handler: function() 
					{
						ACC_JOURNALDelete();
						RefreshDataJurnal();
					}
				},'-','->','-',
				{
					id:'btnPrintJurnal',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() 
					{
						var criteria = "";
						if(Ext.getCmp('txtJournal_Code').getValue() != "")
						{
							criteria += " WHERE Journal_Code='" + Ext.getCmp('txtJournal_Code').getValue() + "'";
						}
						ShowReport("",'011106',criteria);
					}
				}
			]
		}
	);
    // END VAR pnlJurnal --------------------------------------------------------------------- 
    

	return pnlJurnal;
}; 
//END FUNCTION getFormEntryJurnal
///------------------------------------------------------------------------------------------------------------///





function ACC_JOURNALSave(mBol) 
{
	if (ValidasiEntryJurnal('Simpan Data') == 1 )
	{
		if (AddNewJurnal == true)
		 {
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params:getACC_JOURNALParam(),
					success: function(o) 
					{
						var cst = o.responseText;
						if (cst == '{"success":true}') 
						{
							ShowPesanInfoJurnal('Data berhasil di simpan','Simpan Data');
							RefreshDataJurnal();
							if(mBol === false)
							{
								Ext.get('txtJournal_Code').dom.readOnly=true;
							};
							AddNewJurnal = false;
						}
						else if (cst == '{"pesan":0,"success":false}' )
						{
							ShowPesanWarningJurnal('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorJurnal('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		 }
		else 
		{
			 Ext.Ajax.request
			 (
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params:  getACC_JOURNALParam(), 
					success: function(o) 
					{
						var cst = o.responseText;
						if (cst == '{"success":true}') 
						{
							ShowPesanInfoJurnal('Data berhasil di edit','Edit Data');
							RefreshDataJurnal();
							if(mBol === false)
							{
								Ext.get('txtJournal_Code').dom.readOnly=true;
							};
						}
						else if (cst == '{"pesan":0,"success":false}' )
						{
							ShowPesanWarningJurnal('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else 
						{
							ShowPesanErrorJurnal('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function ACC_JOURNALDelete() 
{
	if (ValidasiEntryJurnal('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params:  getACC_JOURNALParam(), 
				success: function(o) 
				{
					var cst = o.responseText;
					if (cst == '{"success":true}') 
					{
						ShowPesanInfoJurnal('Data berhasil di hapus','Hapus Data');
						RefreshDataJurnal();
						ACC_JOURNALAddNew();
					}
					else if (cst == '{"pesan":0,"success":false}' )
					{
						ShowPesanWarningJurnal('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanErrorJurnal('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		) 
	}
};

function ValidasiEntryJurnal(modul)
{
	var x = 1;
	if (Ext.get('txtJournal_Code').getValue() == '' || Ext.get('txtDescription').getValue() == '')
	{
		if (Ext.get('txtJournal_Code').getValue() == '')
		{
			ShowPesanWarningJurnal('Code jurnal belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningJurnal('Description belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningJurnal(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorJurnal(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoJurnal(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function JurnalInit(rowdata)
{
	AddNewJurnal = false;
	Ext.get('txtJournal_Code').dom.value=rowdata.Journal_Code;
	Ext.get('txtJournal_Code').dom.readOnly=true;
	Ext.get('txtDescription').dom.value=rowdata.Description;
};

function ACC_JOURNALAddNew() 
{
    AddNewJurnal = true;
	Ext.get('txtJournal_Code').dom.value='';
	Ext.get('txtJournal_Code').dom.readOnly=false;
	Ext.get('txtDescription').dom.value='';
};

function  getACC_JOURNALParam()
{
	var params = 
	{
		Table: 'JenisJurnal',
		Journal_Code: Ext.get('txtJournal_Code').getValue(),
		Description: Ext.get('txtDescription').getValue()
    };
    return params 
};


function RefreshDataJurnalFilter() 
{   
	var strJurnal;
    if (Ext.get('txtKDJurnalFilter').getValue() != '')
    { 
		strJurnal = 'kode@ Journal_Code =' + Ext.get('txtKDJurnalFilter').getValue(); 
	}
    if (Ext.get('txtJurnalFilter').getValue() != '')
    { 
		if (strJurnal == undefined)
		{
			strJurnal = 'nama@ Description =' + Ext.get('txtJurnalFilter').getValue();
		}
		else
		{
			strJurnal += '##@@##nama@ Description =' + Ext.get('txtJurnalFilter').getValue();
		}  
	}
        
    if (strJurnal != undefined) 
    {  
		dsJurnalList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountJurnal, 
					Sort: 'Journal_Code', 
					Sortdir: 'DESC', 
					target:'JenisJurnal',
					param: strJurnal
				}			
			}
		);        
    }
	else
	{
		RefreshDataJurnal();
	}
};



function mComboMaksDataJurnal()
{
  var cboMaksDataJurnal = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataJurnal',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'10',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountJurnal,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountJurnal=b.data.displayText ;
					RefreshDataJurnal();
				} 
			}
		}
	);
	return cboMaksDataJurnal;
};
 





