// Data Source ExtJS # --------------

/**
*	Nama File 		: TREnd_Year.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var NamaForm_End_Year="End Year";
var now_End_Year= new Date();
CurrentPage.page = window_End_Year(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);



function window_End_Year(mod_id_End_Year)
{	
    var form_End_Year = new Ext.Panel
    (
		{
			title: NamaForm_End_Year,
			iconCls: 'Studi_Lanjut',
			id: mod_id_End_Year,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [detail_End_Year()],
			tbar:
			[]
			//-------------- # End tbar # --------------
       }
    )
    return form_End_Year;
    //-------------- # End form filter # --------------
}
// End Function window_End_Year # --------------

/**
*	Function : setLookUp_End_Year
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function detail_End_Year(lebar) 
{var pbar1 = new Ext.ProgressBar({
    id:'pbar1',
    width:300
});
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:150,
	    items:
		[   
					{	title:'End Year' ,
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 150,
						anchor: '100% 80%',
						items:
						[                          
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Tanggal posting'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'date_End_Year',
								name: 'date_End_Year',
								format: 'd/M/Y',
								//readOnly : true,
								value: now_End_Year,
								width: 150
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Tahun'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'numberfield',
								fieldLabel: 'Tahun ',
								id: 'Periode_End_Year',
								name: 'Periode_End_Year',
								width: 100
							},
							{
								x: 410,
								y: 10,
								xtype: 'label',
								id  : 'End_Year_label1',
								hidden:true,
								text: 'Hati-hati :'
							},
							{
								x: 520,
								y: 10,
								id  : 'End_Year_label2',
								xtype: 'label',
								hidden: true,
								text: 'sedang dilakukan proses penarikan data'
							},
							{
								x: 520,
								y: 40,
								id  : 'End_Year_label3',
								xtype: 'label',
								hidden: true,
								text: 'Harap tunggu sampai selesai'
							},
							{	x: 290,
								y: 10,
								xtype:'button',
								text:'Proses' ,
								width:70,
								hideLabel:true,
								id: 'btn_find_End_YearLedger',
								handler:function() 
								{
									/* Ext.MessageBox.show({
									  msg: 'Tutup Tahun sedang di proses, harap tunggu sampai proses selesai...',
									  progressText: 'proses...',
									  width:500,
									  wait:true,
									  waitConfig: {interval:200}
									}); 
									 */
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/akuntansi/functionPostingEndYears/end_years",
											params: {
												tahun		:Ext.getCmp('Periode_End_Year').getValue(),
												tgl_posting	:Ext.getCmp('date_End_Year').getValue()
											},
											failure: function(o)
											{
												ShowPesanErrorPostingEndPeriode('Error posting, Hubungi Admin!', 'Error');
												Ext.MessageBox.hide();  
												// loadMask.hide();
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.status === true) 
												{
													Ext.MessageBox.hide();  
													ShowPesanInfoPostingEndPeriode('Data berhasil diposting','Information');
												}
												else 
												{
													Ext.MessageBox.hide(); 
													ShowPesanWarningPostingEndPeriode(cst.pesan, 'Error');
													
												}; 
												// Ext.MessageBox.hide();  
												// loadMask.hide();
											}
										}
										
									)
								}
							}
							
							
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};


function ShowPesanWarningPostingEndPeriode(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:280
		}
	);
};

function ShowPesanErrorPostingEndPeriode(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPostingEndPeriode(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

