
var arraystoreAkun=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});



var Current_GeneralLedger =
{
    data: Object,
    details: Array,
    row: 0
};

var dataBaru_GeneralLedger = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);

var gridAkun_GeneralLedger;
var now = new Date();
var tigaharilalu = new Date().add(Date.DAY, -3);
var nowTglTransaksi_GeneralLegder = new Date();
var tglFormat_GeneralLegder = nowTglTransaksi_GeneralLegder.format("d/M/Y");
var dsTRDetail_GeneralLedger;
var currentcellSelected_GeneralLedger;

var GridDataViewAllTransaksi_GeneralLedger ;
var dataSourceAllTransaksi_GeneralLedger;
var rowSelectedAllTransaksi_GeneralLedger;
var CurrentDataAllTransaksi_GeneralLedger;
var setLookUpData_GeneralLedger;
var dsJournalCode_GeneralLedger;
var cboJournalCode_GeneralLedger;
var cellCurrent_GeneralLedger='';

CurrentPage.page = getPanelGeneralLedger(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function getPanelGeneralLedger(mod_id) 
{
	var FieldMaster_GeneralLedger = 
	[
		'journal_code','gl_number','gl_date,reference','notes','tag','account','description','value','isdebit','posted'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceAllTransaksi_GeneralLedger = new WebApp.DataStore
	({
        fields: FieldMaster_GeneralLedger
    });
	// gridAllTransaksi_GeneralLedger();
	GridDataViewAllTransaksi_GeneralLedger = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Transaksi Akun Telah Dibuat',
			store: dataSourceAllTransaksi_GeneralLedger,
			autoScroll: true,
			columnLines: true,
			border:true,
			anchor: '100% 75%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedAllTransaksi_GeneralLedger = undefined;
							rowSelectedAllTransaksi_GeneralLedger = dataSourceAllTransaksi_GeneralLedger.getAt(row);
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedAllTransaksi_GeneralLedger = dataSourceAllTransaksi_GeneralLedger.getAt(ridx);
					if (rowSelectedAllTransaksi_GeneralLedger != undefined)
					{
						setLookUpNewData_GeneralLedger(rowSelectedAllTransaksi_GeneralLedger.data);
					}
					else
					{
						setLookUpNewData_GeneralLedger();
					}
				}
				// End Function # --------------
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						align		: 'center',
						menuDisabled: true,
						dataIndex	: 'posted',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'Kode Jurnal',
						dataIndex: 'journal_code',
						sortable: false,
						menuDisabled:true,
						width: 35
						
					},
					{
						header: 'No. Jurnal',
						dataIndex: 'gl_number',
						sortable: false,
						menuDisabled:true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header:'Tgl Jurnal',
						dataIndex: 'gl_date',						
						width: 30,
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.gl_date);
						}
					},
					//-------------- ## --------------
					{
						header: 'Referensi',
						dataIndex: 'reference',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'Catatan',
						dataIndex: 'notes',
						sortable: false,
						menuDisabled:true,
						width: 50
					},
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekResepRWI',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Baru',
						iconCls: 'add',
						tooltip: 'Add Data',
						id: 'btnTambahTransaksi_GeneralLedger',
						handler: function(sm, row, rec)
						{
							setLookUpNewData_GeneralLedger()
						}
					},
					'-',
					{	
						/* x: 500,
						y: 40, */
						xtype:'button',
						text:' Refresh' ,
						width:70,
						iconCls:'refresh',
						hideLabel:true,
						id: 'btn_find_generalLedger',
						handler:function() 
						{
							gridAllTransaksi_GeneralLedger();
						}
					},
					
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianTransaksi_GeneralLedger = new Ext.FormPanel({
        labelAlign: 'top',
        frame:false,
        title: 'Pencarian',
        bodyStyle:'padding:0px 0px 0px 0px',
        items: [
			panelPencarianTransaksi_GeneralLedger()
			
		]
	})
	
    var FormDepanGeneralLedger = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Buku Besar',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						pencarianTransaksi_GeneralLedger,
						GridDataViewAllTransaksi_GeneralLedger
                   ],
			tbar:
            [
			
				
			],
            listeners:
            {
                'afterrender': function()
                {
					gridAllTransaksi_GeneralLedger();
				}, 'beforeclose': function(){
					
				}
            }
        }
    );
	
   return FormDepanGeneralLedger;

};

function panelPencarianTransaksi_GeneralLedger() 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 800,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:110,
	    items:
		[   
					{
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 110,
						anchor: '100% 80%',
						items:
						[                          
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Reference'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x : 130,
								y : 10,
								xtype: 'textfield',
								id: 'searchreference_generalLedger',
								name: 'searchreference_generalLedger',
								width: 150,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											gridAllTransaksi_GeneralLedger(Ext.getCmp('searchreference_generalLedger').getValue(),
											Ext.getCmp('dateSearchBegining_generalLedger').getValue(),
											Ext.getCmp('dateSearchEnd_generalLedger').getValue());
										} 						
									}
								}
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Tanggal jurnal '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'datefield',
								id: 'dateSearchBegining_generalLedger',
								name: 'dateSearchBegining_generalLedger',
								format: 'd/M/Y',
								value: now,
								width: 150,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											gridAllTransaksi_GeneralLedger(Ext.getCmp('searchreference_generalLedger').getValue(),Ext.getCmp('dateSearchBegining_generalLedger').getValue(),Ext.getCmp('dateSearchEnd_generalLedger').getValue());
										} 						
									}
								}
							},
							{
								x: 295,
								y: 40,
								xtype: 'label',
								text: 's/d'
							},
							{
								x: 330,
								y: 40,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dateSearchEnd_generalLedger',
								name: 'dateSearchEnd_generalLedger',
								format: 'd/M/Y',
								value: now,
								width: 150,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											gridAllTransaksi_GeneralLedger(Ext.getCmp('searchreference_generalLedger').getValue(),Ext.getCmp('dateSearchBegining_generalLedger').getValue(),Ext.getCmp('dateSearchEnd_generalLedger').getValue());
										} 						
									}
								}
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: '*) Tekan enter untuk mencari',
								style:{'font-size':'10'}
							},
							
								
							//-------------------------------------------------------------
							
						]
					},	
			
		]
	};
    return items;
};

function setLookUpNewData_GeneralLedger(rowdata){
    var lebar = 985;
    setLookUpData_GeneralLedger = new Ext.Window({
        title: 'Entry Buku Besar', 
        closeAction: 'destroy',        
        width: 900,
        height: 520,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: panelLookupEntry_GeneralLegder(lebar,rowdata),
	})
	setLookUpData_GeneralLedger.show();
	loadDataComboJournalCode_GeneralLedger();
	
	if (rowdata == undefined){
    }
    else
    {
        dataInitialTransaksi_GeneralLedger(rowdata);
    }
}

function panelLookupEntry_GeneralLegder(lebar,rowdata){
	var pnlFormDataBasic_viApotekResepRWI = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: 600,
		border: false,
		items:[
			panelHeader_GeneralLedger(),
			Getdet_GeneralLedger(),
			panelTotalDebitCredit_GeneralLedger()
		],
		tbar:
			[
				{
					text: 'Baru',
					id: 'btnLookupBaru_GeneralLedger',
					tooltip: nmLookup,
					iconCls: 'add',
					disabled:false,
					handler: function()
					{
						databaru_GeneralLedger();
					}
				}, 
				'-',
				{
					text: 'Simpan',
					id: 'btnLookupSave_GeneralLedger',
					tooltip: nmLookup,
					iconCls: 'save',
					disabled:false,
					handler: function()
					{
						datasave_GeneralLedger();
					}
				}, 
				'-',
				{
					text: 'Hapus',
					id: 'btnLookupHapus_GeneralLedger',
					tooltip: nmLookup,
					iconCls: 'remove',
					disabled:false,
					handler: function()
					{
						datahapus_GeneralLedger(rowdata);
					}
				}, 
			]
	})
	return pnlFormDataBasic_viApotekResepRWI;
}

function Getdet_GeneralLedger() 
{
    var fldDetail = ['account','name','description','valuedebit','valuecredit'];
	
    dsTRDetail_GeneralLedger = new WebApp.DataStore({ fields: fldDetail })
    gridAkun_GeneralLedger = new Ext.grid.EditorGridPanel
    (
        {
			title: '',
            stripeRows: true,
            store: dsTRDetail_GeneralLedger,
            border: true,
			height:300,
			anchor: '100%,70%',
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            currentcellSelected_GeneralLedger = dsTRDetail_GeneralLedger.getAt(row);
                            Current_GeneralLedger.row = row;
                            Current_GeneralLedger.data = currentcellSelected_GeneralLedger;
                        }
                    }
                }
            ),
            cm: TrGeneralLedgerColumModel(),
			listeners: {
				rowclick: function( $this, rowIndex, e )
				{
					cellCurrent_GeneralLedger = rowIndex;
				},
				celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
			
				}
			},
			tbar:[
				{
					xtype: 'button',
					text: 'Tambah Akun',
					iconCls: 'edit_tr',
					tooltip: 'Add Data',
					id: 'btnTambahAkun_GeneralLedger',
					handler: function(sm, row, rec)
					{
						var records = new Array();
						records.push(new dsTRDetail_GeneralLedger.recordType());
						dsTRDetail_GeneralLedger.add(records);
					}
				},
				'-',
				{
					id:'btnHpsBrsItemRad',
					text: 'Hapus Baris',
					tooltip: 'Hapus Baris',
					disabled:false,
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsTRDetail_GeneralLedger.getCount() > 0 ) {
							if (currentcellSelected_GeneralLedger != undefined)
							{
								Ext.Msg.confirm('Warning', 'Apakah akun ini akan dihapus?', function(button){
									if (button == 'yes'){
										if(currentcellSelected_GeneralLedger != undefined) {
											var line = gridAkun_GeneralLedger.getSelectionModel().selection.cell[0];
											dsTRDetail_GeneralLedger.removeAt(line);
											gridAkun_GeneralLedger.getView().refresh();
										}
									}
								})
							} else {
								ShowPesanWarningGeneralLedger('Pilih akun yang akan dihapus! ','Hapus data');
							}
						}
					}
				}
			],
			viewConfig: 
			{
				forceFit: true
			}
			
        }
	);
	
	

    return gridAkun_GeneralLedger;
};

function TrGeneralLedgerColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [	
			new Ext.grid.RowNumberer(),
			{			
				header: 'Akun',
                dataIndex: 'account',
				 menuDisabled:true,
                width:130,
				editor:new Nci.form.Combobox.autoComplete({
					store	: arraystoreAkun,
					select	: function(a,b,c){
						var line	= gridAkun_GeneralLedger.getSelectionModel().selection.cell[0];
						dsTRDetail_GeneralLedger.getRange()[line].data.account=b.data.account;
						dsTRDetail_GeneralLedger.getRange()[line].data.name=b.data.name;
						dsTRDetail_GeneralLedger.getRange()[line].data.description=b.data.description;
						dsTRDetail_GeneralLedger.getRange()[line].data.valuedebit=0;
						dsTRDetail_GeneralLedger.getRange()[line].data.valuecredit=0;
						
						gridAkun_GeneralLedger.getView().refresh();
					},
					insert	: function(o){
						return {
							account        	: o.account,
							name 			: o.name,
							description		: o.description,
							valuedebit		: o.valuedebit,
							valuecredit		: o.valuecredit,
							text			:  '<table style="font-size: 11px;"><tr><td width="140">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								text:''
							}
					},
					url		: baseURL + "index.php/akuntansi/functionGeneralLedger/getAkun",
					valueField: 'account',
					displayField: 'text',
					listWidth: 340
				})
			},
            {
                header: 'Nama',
                dataIndex: 'name',
                width:230,
				menuDisabled:true,
				hidden :false
                
            },
			
            {
                header: 'Deskripsi',
                dataIndex: 'description',
                width:240,
                menuDisabled:true,
                hidden:false,
				editor: new Ext.form.TextField({})
            },
			{
				xtype:'numbercolumn',
				align:'right',
                header: 'Debit',
                dataIndex: 'valuedebit',
                width:100,
                menuDisabled:true,
                hidden:false,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsTRDetail_GeneralLedger.getRange()[line].data.valuedebit=a.getValue();
							JumlahGrid()
						},
						focus: function(a){
							this.index=gridAkun_GeneralLedger.getSelectionModel().selection.cell[0]
						}
						
					}
				})
            },{
				xtype:'numbercolumn',
				align:'right',
                header: 'Kredit',
                dataIndex: 'valuecredit',
                width:100,
                menuDisabled:true,
                hidden:false,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsTRDetail_GeneralLedger.getRange()[line].data.valuecredit=a.getValue();
							JumlahGrid();
						},
						focus: function(a){
							this.index=gridAkun_GeneralLedger.getSelectionModel().selection.cell[0];
						}
						
					}
					
				})
            }
     
        ]
    )
};

function panelHeader_GeneralLedger(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    //width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:true,
	    height:110,
	    items:
		[   
					{
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 0px 0px 0px 0px',
						border: false,
						width: 100,
						height: 110,
						anchor: '100% 80%',
						items:
						[                          
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode jurnal'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboJounalCode_GeneralCode(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal jurnal '
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dfGlDate_GeneralLedger',
								name: 'dfGlDate_GeneralLedger',
								format: 'd/M/Y',
								//readOnly : true,
								value: now,
								width: 150
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Catatan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								fieldLabel: 'Catatan ',
								id: 'txtNotes_GeneralLedger',
								name: 'txtNotes_GeneralLedger',
								width: 260
							},
							{
								x: 420,
								y: 0,
								xtype: 'label',
								text: 'No. Jurnal'
							},
							{
								x: 530,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 540,
								y: 0,
								xtype: 'textfield',
								readOnly:true,
								id: 'txtGlNumber_GeneralLedger',
								name: 'txtGlNumber_GeneralLedger',
								width: 120
							},
							{
								x: 420,
								y: 30,
								xtype: 'label',
								text: 'Reference'
							},
							{
								x: 530,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 540,
								y: 30,
								xtype: 'textfield',
								id: 'txtReferences_GeneralLedger',
								name: 'txtReferences_GeneralLedger',
								width: 120
							},
							{
								x: 670,
								y: 0,
								xtype: 'label',
								text: 'Posted'
							},
							{
								x: 720,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{	x: 730,
								y: 0,
								xtype: 'checkbox',
								id: 'chk_post_generalLedger',
								text: 'Posted',
								hideLabel:false,
								checked: false,
								disabled: true,
								handler: function()
								{

								}
							},
							/* {
								x: 10,
								y: 92,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'date_find_1_generalLedger',
								name: 'date_find_1_generalLedger',
								format: 'd/M/Y',
								value: now,
								width: 150
							},	
							{
								x: 170,
								y: 92,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'date_find_2_generalLedger',
								name: 'date_find_2_generalLedger',
								format: 'd/M/Y',
								value: now,
								width: 150
							},
							*/
							
							
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};

function panelTotalDebitCredit_GeneralLedger() 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    //width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:false,
	    height:40,
	    items:
		[   
			{
				columnWidth:.99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				height: 50,
				anchor: '100% 80%',
				items:
				[                          
					{
						x: 750,
						y: 10,
						readOnly:true,
						xtype: 'textfield',
						fieldLabel: 'Kredit ',
						id: 'txtTotCredit_GeneralLedger',
						name: 'txtTotCredit_GeneralLedger',
						style:{'text-align':'right'},
						width: 110
					},
					{
						x: 635,
						y: 10,
						readOnly:true,
						xtype: 'textfield',
						fieldLabel: 'Debit ',
						id: 'txtTotDebit_GeneralLedger',
						name: 'txtTotDebit_GeneralLedger',
						style:{'text-align':'right'},
						width: 110
					},
				]
			}
		]
	}
	return items;
}



function comboJounalCode_GeneralCode()
{
	var Field = ['journal_code','description'];
    dsJournalCode_GeneralLedger = new WebApp.DataStore({fields: Field});
    cboJournalCode_GeneralLedger = new Ext.form.ComboBox
	(
            {
                x: 130,
				y: 0,
                id:'cboJournalCode_GeneralLedger',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                width:150,
                store: dsJournalCode_GeneralLedger,
                valueField: 'journal_code',
                displayField: 'journal_code',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboJournalCode_GeneralLedger;
};

function databaru_GeneralLedger(){
	Ext.getCmp('cboJournalCode_GeneralLedger').setValue('');
	Ext.getCmp('dfGlDate_GeneralLedger').setValue(now);
	Ext.getCmp('txtGlNumber_GeneralLedger').setValue('');
	Ext.getCmp('txtReferences_GeneralLedger').setValue('');
	Ext.getCmp('txtNotes_GeneralLedger').setValue('');
	Ext.getCmp('txtTotCredit_GeneralLedger').setValue('');
	Ext.getCmp('txtTotDebit_GeneralLedger').setValue('');
	Ext.getCmp('chk_post_generalLedger').setValue(false);
	
	Ext.getCmp('cboJournalCode_GeneralLedger').setReadOnly(false);
	Ext.getCmp('dfGlDate_GeneralLedger').setReadOnly(false);
	Ext.getCmp('txtReferences_GeneralLedger').setReadOnly(false);
	Ext.getCmp('txtNotes_GeneralLedger').setReadOnly(false);
	
	Ext.getCmp('btnTambahAkun_GeneralLedger').enable();
	Ext.getCmp('btnHpsBrsItemRad').enable();
	Ext.getCmp('btnLookupSave_GeneralLedger').enable();
	
	dsTRDetail_GeneralLedger.removeAll();
	
}

function dataInitialTransaksi_GeneralLedger(rowdata){
	Ext.getCmp('cboJournalCode_GeneralLedger').setValue(rowdata.journal_code);
	Ext.getCmp('dfGlDate_GeneralLedger').setValue(ShowDate(rowdata.gl_date));
	Ext.getCmp('txtGlNumber_GeneralLedger').setValue(rowdata.gl_number);
	Ext.getCmp('txtReferences_GeneralLedger').setValue(rowdata.reference);
	Ext.getCmp('txtNotes_GeneralLedger').setValue(rowdata.notes);
	
	if(rowdata.posted == 't'){
		Ext.getCmp('chk_post_generalLedger').setValue(true);
		Ext.getCmp('txtNotes_GeneralLedger').setReadOnly(true);
		Ext.getCmp('btnTambahAkun_GeneralLedger').disable();
		Ext.getCmp('btnHpsBrsItemRad').disable();
		Ext.getCmp('btnLookupSave_GeneralLedger').disable();
	} else{
		Ext.getCmp('chk_post_generalLedger').setValue(false);
	}
	
	Ext.getCmp('cboJournalCode_GeneralLedger').setReadOnly(true);
	Ext.getCmp('dfGlDate_GeneralLedger').setReadOnly(true);
	Ext.getCmp('txtGlNumber_GeneralLedger').setReadOnly(true);
	Ext.getCmp('txtReferences_GeneralLedger').setReadOnly(true);
	
	ViewDetailAkun_GeneralLedger(rowdata.journal_code,rowdata.gl_date,rowdata.gl_number);
}

function JumlahGrid(){
	var jumlahdebit=0;
	var jumlahcredit=0;
	for(var i=0; i < dsTRDetail_GeneralLedger.getCount() ; i++){
		var o=dsTRDetail_GeneralLedger.getRange()[i].data;
		if(o.valuedebit != undefined || o.valuecredit != undefined ){
			jumlahdebit += parseFloat(o.valuedebit);
			jumlahcredit += parseFloat(o.valuecredit);
		}
	}
	gridAkun_GeneralLedger.getView().refresh();
	Ext.getCmp('txtTotDebit_GeneralLedger').setValue(toFormat(jumlahdebit));
	Ext.getCmp('txtTotCredit_GeneralLedger').setValue(toFormat(jumlahcredit));
}

function getParamSave(){
	var params={
		journal_code:Ext.getCmp('cboJournalCode_GeneralLedger').getValue(),
		gl_date:Ext.getCmp('dfGlDate_GeneralLedger').getValue(),
		gl_number:Ext.getCmp('txtGlNumber_GeneralLedger').getValue(),
		reference:Ext.getCmp('txtReferences_GeneralLedger').getValue(),
		note:Ext.getCmp('txtNotes_GeneralLedger').getValue(),
		tag:Ext.getCmp('cboJournalCode_GeneralLedger').getValue(),
	}
	
	params['jumlah']=dsTRDetail_GeneralLedger.getCount();
	for(var i = 0 ; i < dsTRDetail_GeneralLedger.getCount();i++)
	{
		if(dsTRDetail_GeneralLedger.data.items[i].data.valuecredit == 0 ){
			params['value-'+i]=dsTRDetail_GeneralLedger.data.items[i].data.valuedebit;
			params['isdebit-'+i] = true;
		} else{
			params['value-'+i]=dsTRDetail_GeneralLedger.data.items[i].data.valuecredit;
			params['isdebit-'+i] = false;
		}
		
		params['account-'+i] = dsTRDetail_GeneralLedger.data.items[i].data.account;
		params['line-'+i] = i+1;
		params['description-'+i]=dsTRDetail_GeneralLedger.data.items[i].data.description;
	}
	
	return params;
}


function loadDataComboJournalCode_GeneralLedger(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/akuntansi/functionGeneralLedger/getcombojurnalcode",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboJournalCode_GeneralLedger.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsJournalCode_GeneralLedger.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsJournalCode_GeneralLedger.add(recs);
			}
		}
	});
}

function gridAllTransaksi_GeneralLedger(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionGeneralLedger/getGridAllTransaksi",
			params: {
				reference:Ext.getCmp('searchreference_generalLedger').getValue(),
				tglAwal:Ext.getCmp('dateSearchBegining_generalLedger').getValue(),
				tglAkhir:Ext.getCmp('dateSearchEnd_generalLedger').getValue()
			},
			failure: function(o)
			{
				ShowPesanErrorGeneralLedger('Error grid transaksi. Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{   
				dataSourceAllTransaksi_GeneralLedger.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSourceAllTransaksi_GeneralLedger.recordType;

					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSourceAllTransaksi_GeneralLedger.add(recs);
					
					
					GridDataViewAllTransaksi_GeneralLedger.getView().refresh();
					
				} 
				else 
				{
					ShowPesanErrorGeneralLedger('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	);
}

function ViewDetailAkun_GeneralLedger(journal_code,gl_date,gl_number){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionGeneralLedger/getGridDetailAkun",
			params: {
				journal_code:journal_code,
				gl_date:gl_date,
				gl_number:gl_number
			},
			failure: function(o)
			{
				ShowPesanErrorGeneralLedger('Error detail akun. Hubungi Admin!', 'Error');
			},	
			success: function(o) 
			{   
				dsTRDetail_GeneralLedger.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsTRDetail_GeneralLedger.recordType;

					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetail_GeneralLedger.add(recs);
					
					gridAkun_GeneralLedger.getView().refresh();
					JumlahGrid()
				} 
				else 
				{
					ShowPesanErrorGeneralLedger('Gagal membaca Data ini', 'Error');
				};
				
			}
		}
		
	);
}

function datasave_GeneralLedger(){
	if (ValidasiEntryGeneralLedger(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/functionGeneralLedger/save",
				params: getParamSave(),
				failure: function(o)
				{
					ShowPesanErrorGeneralLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGeneralLedger('Data berhasil disimpan','Information')
						Ext.getCmp('txtGlNumber_GeneralLedger').setValue(cst.gl_number);
						gridAllTransaksi_GeneralLedger();
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function datahapus_GeneralLedger(rowdata){
	// console.log(rowdata.posted);
	 if(rowdata.posted == 't' ){
		ShowPesanInfoGeneralLedger('Transaksi telah diposting, data gagal dihapus!','Information')
	}else{
		
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/functionGeneralLedger/hapus_gl",
				params: {
					journal_code	: Ext.getCmp('cboJournalCode_GeneralLedger').getValue(), 
					gl_number		: Ext.getCmp('txtGlNumber_GeneralLedger').getValue(),
					gl_date			: Ext.getCmp('dfGlDate_GeneralLedger').getValue(),
					referensi		: Ext.getCmp('txtReferences_GeneralLedger').getValue(),
					nilai			: Ext.getCmp('txtTotDebit_GeneralLedger').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorGeneralLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGeneralLedger('Data berhasil dihapus!','Information')
						databaru_GeneralLedger();
						gridAllTransaksi_GeneralLedger();
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal menghapus data ini', 'Error');
						
					};
				}
			}
			
		)
		
	} 
}

function getAmount(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount;
};
function ValidasiEntryGeneralLedger(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJournalCode_GeneralLedger').getValue() === '' || dsTRDetail_GeneralLedger.getCount() === 0 || dsTRDetail_GeneralLedger.getCount() === 1){
		if(Ext.getCmp('cboJournalCode_GeneralLedger').getValue() === ''){
			ShowPesanWarningGeneralLedger('Kode jurnal masih kosong!', 'Warning');
			x = 0;
		} else if(dsTRDetail_GeneralLedger.getCount() === 1 ){
			ShowPesanWarningGeneralLedger('Daftar akun minimal 2 baris!', 'Warning');
			x = 0;
		} 
	}
	
	
	for(var i=0; i<dsTRDetail_GeneralLedger.getCount() ; i++){
		var o=dsTRDetail_GeneralLedger.getRange()[i].data;

		if(o.account == undefined || o.account == ""){
			ShowPesanWarningGeneralLedger('Akun masih kosong, harap isi baris kosong atau hapus untuk melanjutkan!', 'Warning');
			x = 0;
		} else{
			if(o.valuecredit != undefined || o.valuedebit != undefined){
				if(o.valuecredit == undefined){
					ShowPesanWarningGeneralLedger('Kredit baris ke '+ (i+1) +' masih kosong, kredit tidak boleh kosong. Mohon periksa kembali!', 'Warning');
					x = 0;
				}else if(o.valuedebit == undefined){
					ShowPesanWarningGeneralLedger('Debit baris ke '+ (i+1) +' masih kosong, debit tidak boleh kosong. Mohon periksa kembali!', 'Warning');
					x = 0;
				} else if(o.valuedebit != 0 && o.valuecredit != 0){
					ShowPesanWarningGeneralLedger('Mohon periksa kembali baris ke '+ (i+1) +'. Isi salah satu kolom debit atau kredit!', 'Warning');
					x = 0;
				} else if(o.valuedebit == 0 && o.valuecredit == 0){
					ShowPesanWarningGeneralLedger('Mohon periksa kembali baris ke '+ (i+1) +'. Nilai akun masih kosong!', 'Warning');
					x = 0;
				}
			}
		}
	}
	
	return x;
};

function ShowPesanWarningGeneralLedger(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:280
		}
	);
};

function ShowPesanErrorGeneralLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGeneralLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};



