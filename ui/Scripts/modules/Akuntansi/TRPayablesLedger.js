var TrPayablesLedger={};
TrPayablesLedger.form={};
TrPayablesLedger.func={};
TrPayablesLedger.vars={};
TrPayablesLedger.func.parent=TrPayablesLedger;
TrPayablesLedger.form.ArrayStore={};
TrPayablesLedger.form.ComboBox={};
TrPayablesLedger.form.DataStore={};
TrPayablesLedger.form.Record={};
TrPayablesLedger.form.Form={};
TrPayablesLedger.form.Grid={};
TrPayablesLedger.form.Panel={};
TrPayablesLedger.form.TextField={};
TrPayablesLedger.form.Button={};
var autocom_gen;
var cellCurrentrec='';
TrPayablesLedger.form.ArrayStore.a=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_vendor','ap_number','ap_date','vend_code','reference','type','info','due_date','notes','vendor','vc','term','line'
				],
		data: []
});

TrPayablesLedger.form.ArrayStore.b=new Ext.data.ArrayStore({
		id: 0,
		fields: [	'account','name','groups'],
		data: []
});


var CurrentData_PayablesLedger =

{
    data: Object,
    details: Array,
    row: 0
};

var Current_PayablesLedgerList =

{
    data: Object,
    details: Array,
    row: 0
};

var gridDTPayablesLedger;
var dsTRDetail_PayablesLedger;
var rowSelected_PayablesLedger;

var grid_PayablesLedger;
var ds_TrPayablesLedgerList;
var rowSelected_TrPayablesLedgerList;


var ds_vendor;
var ds_vendor_pencarian;
var tmpposting;
var tmpJatuhTempo;
var type_trans=false;
var n_type=0;
var now = new Date();
var tigaharilalu = new Date().add(Date.DAY, -3);

CurrentPage.page = getPanelPayablesLedger(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var tmpKdVendor='';

function getPanelPayablesLedger(mod_id) 
{
	
	var Field = ['status_posting','ap_number','ap_date','vend_code','reference','type','info','due_date','notes','vendor','vc','posted']
	ds_TrPayablesLedgerList = new WebApp.DataStore({ fields: Field });

    grid_PayablesLedger = new Ext.grid.EditorGridPanel
    (
        {
			title: 'Daftar Transaksi Hutang',
			xtype: 'editorgrid',
            stripeRows: true,
            store: ds_TrPayablesLedgerList,
            border: false,
			height:400,
			anchor: '100% 75%',
            columnLines: true,
            frame: false,
            autoScroll:true,
			plugins: [new Ext.ux.grid.FilterRow()],
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {	
							
                            rowSelected_TrPayablesLedgerList = ds_TrPayablesLedgerList.getAt(row);
							rowSelected_TrPayablesLedgerList = undefined;
							rowSelected_TrPayablesLedgerList = ds_TrPayablesLedgerList.getAt(row);
							Current_PayablesLedgerList
							Current_PayablesLedgerList.row = row;
							Current_PayablesLedgerList.data = rowSelected_TrPayablesLedgerList.data;
                        }
                    }
                }
            ),
			listeners:
            {
				//action jika isi grid di double klik
                rowdblclick: function (sm, ridx, cidx)
                {

                    rowSelected_TrPayablesLedgerList = ds_TrPayablesLedgerList.getAt(ridx);
                    if (rowSelected_TrPayablesLedgerList !== undefined)
                    {
						PenHasilLookUp(rowSelected_TrPayablesLedgerList.data); //memanggil fungsi untuk menampilkan window
						
					}
                    else
                    {
                        PenHasilLookUp();
                    }
                }
            },
            cm: new Ext.grid.ColumnModel
            (
                [
                    new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 80,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						align		: 'center',
						menuDisabled: true,
						dataIndex	: 'posted',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
                        id: 'colViewNoJurnal',
                        header: 'No. Jurnal',
                        dataIndex: 'ap_number',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
					{
                        id: 'colViewvendor',
                        header: 'Vendor',
                        dataIndex: 'vendor',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 120
                    },
                    {
                        id: 'colViewTglJurnal',
                        header: 'Tgl Jurnal',
                        dataIndex: 'ap_date',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 70,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.ap_date);

						}
                    },
                    {
                        id: 'colViewReferensi',
                        header: 'Referensi',
                        dataIndex: 'reference',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
					{
                        id: 'colViewCatatan',
						header: 'Catatan',
                        width: 200,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'notes'                        
                    }
                ]
            ),
			
			viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnTambahPayablesLedger',
                        text: 'Tambah Baru',
                        tooltip: nmEditData,
                        iconCls: 'add',
                        handler: function(sm, row, rec)
                        {
							PenHasilLookUp();	
                        }
                    },
					{
						xtype: 'tbseparator'
					},
					{
                        id: 'btnRefreshPayablesLedger',
                        text: 'Refresh',
                        tooltip: nmEditData,
                        iconCls: 'refresh',
                        handler: function(sm, row, rec)
                        {
							ds_TrPayablesLedgerList.removeAll();
							dataGrid_PayablesLedger();
							Ext.getCmp('NoJurnal_PayablesLedger').setValue('');							
							Ext.getCmp('cbovendor_pencarian').setValue('');							
                        }
                    },
                ]
			 
        }
	);
	
	
	var pencarianTransaksi_PayablesLedger = new Ext.FormPanel({
        labelAlign: 'top',
        frame:false,
        title: 'Pencarian',
        bodyStyle:'padding:0px 0px 0px 0px',
        items: [
			 getPanelInput_PayablesLedger()
			
		]
	})
	
	
	
	
    var FormDepanPayablesLedger = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Buku Besar Hutang',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                       pencarianTransaksi_PayablesLedger,
					   grid_PayablesLedger
                   ],
			tbar:
            [
			
				
			],
            listeners:
            {
                'afterrender': function()
                {
					dataGrid_PayablesLedger();
				}, 
				'beforeclose': function(){
					
				}
            }
        }
    );
	
   return FormDepanPayablesLedger;
	
};

//panel dalam lookup detail pasien
function getPanelInput_PayablesLedger(lebar) 
{
    var items =
	{
		title:'',
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:150,
	    items:
		[   
			{
				columnWidth:.99,
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width: 100,
				height: 150,
				anchor: '100% 80%',
				items:
				[                          
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Nomor Jurnal'
					},
					{
						x: 145,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					{
						x: 155,
						y: 10,
						xtype: 'textfield',
						id: 'NoJurnal_PayablesLedger',
						name: 'NoJurnal_PayablesLedger',
						width: 200,
						enableKeyEvents: true,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika nama pasien di enter, maka akan ditampilkan data sesuai pencarian
								{
									dataGrid_PayablesLedger();
								} 						
							}
                        }
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Vendor'
					},
					{
						x: 145,
						y: 40,
						xtype: 'label',
						text: ':'
					},
					combovendor_pencarian(),
					
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Tanggal Jurnal '
					},
					{
						x: 145,
						y: 70,
						xtype: 'label',
						text: ':'
					},
					{
						x: 155,
						y: 70,
						xtype: 'datefield',
						id: 'dtpTglAwalFilterPayablesLedger',
						format: 'd/M/Y',
						value: tigaharilalu,
						width: 100,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
								{
									dataGrid_PayablesLedger();
								} 						
							}
						}
					},
					{
						x: 270,
						y: 70,
						xtype: 'label',
						text: 's/d '
					},
					{
						x: 305,
						y: 70,
						xtype: 'datefield',
						id: 'dtpTglAkhirFilterPayablesLedger',
						format: 'd/M/Y',
						value: now,
						width: 100,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
								{
									dataGrid_PayablesLedger();
								} 						
							}
						}
					},
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: '*) Tekan enter untuk mencari',
						style:{'font-size':'10'}
					}
							
							//-------------------------------------------------------------
							
				]
			},	//akhir panel biodata pasien
		]
	};
    return items;
};



function ShowPesanWarningPayablesLedger(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPayablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPayablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};




function grid_panel_PayablesLedger_satu(){
	var panel_PayablesLedger_satu = new Ext.Panel({
		title: 'Daftar Transaksi Hutang',
		id:'grid_panel_PayablesLedger_satu',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        anchor: '100%,70%',
        border: false,
        items:	[
					GetGrid_PayablesLedger()
				]
    });
	return panel_PayablesLedger_satu;
}

function PenHasilLookUp(rowdata) 
{
    var lebar = 800;
    FormLookUpsdetailPayablesLedger = new Ext.Window //window yang berisi data pasien dan grid editor hasil hemodialisa
    (
        {
            id: 'gridPayablesLedger',
            title: 'Entry Buku Besar Hutang',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: true,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryPayablesLedger(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailPayablesLedger.show();
    if (rowdata === undefined) {
		
	}
	else{
		DataInitPayablesLedger(rowdata);
	}

};
function getFormEntryPayablesLedger(lebar,data) {
    var pnlPayablesLedger = new Ext.FormPanel //panel untuk menampilkan data pasien
    (
        {
            id: 'PanelPayablesLedger',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:180,
            anchor: '100%',
            width: lebar,
            border: false,
            items: 	[
						viewlookupedit(lebar)
						
					], // item yang berisi data pasien,
			tbar:
			[
		
				{
					id:'btnTambahBaru',
					text: 'Tambah Baru',
					tooltip: 'Tambah Baru',
					iconCls:'add',
					handler: function()
					{
					  addNewPayablesLedger();
					}
				},
				{
					xtype:'tbseparator'
				},
				{
					id:'btnSimpan',
					text: 'Simpan',
					tooltip: 'Simpan',
					iconCls:'save',
					handler: function()
					{
					   dataSave_PayablesLedger();
					}
				},
				{
					xtype:'tbseparator'
				},
				{
					id:'btnHapus',
					text: 'Hapus',
					tooltip: 'Hapus',
					iconCls:'remove',
					handler: function()
					{
					   dataHapus_PayablesLedger(data); //hapus data 
					}
				}
				
			],
			
        }
    );
	// toolbar grid lookup
	var x;

	var pnlPayablesLedger2 = new Ext.FormPanel //panel untuk menampilkan tab grid hasil hemodialisa
    (
        {
            id: 'PanelPayablesLedger2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:210,
            anchor: '100%',
            width: lebar,
            border: false,
            items: 	[	
						GetDTGridPayablesLedger(lebar)
					]
        }
    );
	
	var pnlPayablesLedger3 = new Ext.FormPanel //panel untuk menampilkan tab grid hasil hemodialisa
    (
        {
            id: 'PanelPayablesLedger3',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:50,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	
						{
							columnWidth:.99,
							layout: 'absolute',
							bodyStyle: 'padding: 10px 10px 10px 10px',
							border: false,
							// width: 100,
							height: 50,
							anchor: '100% 80%',
							items:
							[                          
								{
									x: 520,
									y: 10,
									readOnly:true,
									xtype: 'textfield',
									fieldLabel: 'Debit ',
									id: 'txtTotDebit_PayablesLedger',
									name: 'txtTotDebit_PayablesLedger',
									style:{'text-align':'right'},
									width: 110
								},
								{
									x: 640,
									y: 10,
									readOnly:true,
									xtype: 'textfield',
									fieldLabel: 'Kredit ',
									id: 'txtTotCredit_PayablesLedger',
									name: 'txtTotCredit_PayablesLedger',
									style:{'text-align':'right'},
									width: 110
								}
							]
						}						
					]
        }
    );

    var FormDetailPayablesLedger = new Ext.Panel //panel yang menampung panel untuk menampilkan data pasien dan panel tab hd
	(
		{
		    id: 'FormDepanPayablesLedger',
		    region: 'center',
		    width: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:10px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[ 
				pnlPayablesLedger,
				pnlPayablesLedger2,
				pnlPayablesLedger3
			]

		}
	);
	
    return FormDetailPayablesLedger
}

function viewlookupedit(lebar){ // fungsi untuk menampilkan data pasien
	var radiosAp = new Ext.form.RadioGroup({
        id: 'radiosa',
		x:130,
		y:70,
        columns: 6,
        items: [
            {
                id: 'rb_debit',
                name: 'rb',
                boxLabel: 'Debit',
                checked: true,
                inputValue: "false"
            }
            ,
            {
                boxLabel: 'Kredit',
                id: 'rb_kredit',
                name: 'rb',
                inputValue: "true"
            }
        ],
        listeners: {
            change: function ()
            {
                type_trans = radiosAp.getValue().inputValue;
				if(type_trans == 'false'){
					n_type = 0;
				}else{
					n_type = 1;
				}
            }
        }

    });
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-40,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:true,
	    height:140,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				// width: 100,
				height: 150,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Nomor Jurnal '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textfield',
						name: 'TxtPopupKdJurnal',
						id: 'TxtPopupKdJurnal',
						readOnly: true,
						width: 80
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Vendor '
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					combovendor(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Tipe '
					},
					{
						x: 110,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
					radiosAp,
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Tanggal '
					},
					{
						x: 110,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 100,
						xtype: 'datefield',
						name: 'dPopupTglJurnal',
						id: 'dPopupTglJurnal',
						width: 110,
						format: 'd/M/Y',
						value: now,
						readOnly:true
					},
					{
						x: 390,
						y: 10,
						xtype: 'checkbox',
						id: 'checkPosting',
						text: 'Posted',
						handler: function ()
						{
							if (this.getValue() === true)
							{
								tmpposting = 'True';
							} else
							{
								tmpposting = 'False';
							}

						},
						disabled:true
					},
					{
						x: 410,
						y: 10,
						xtype: 'label',
						text: ' Posting '
					},
					{
						x: 390,
						y: 40,
						xtype: 'label',
						text: 'Referensi'
					},
					{
						x: 490,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 500,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupReferensi',
						id: 'TxtPopupReferensi',
						width: 200 
					},
					{
						x: 390,
						y: 70,
						xtype: 'label',
						text: 'Jatuh Tempo'
					},
					{
						x: 490,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 500,
						y : 70,
						xtype: 'datefield',
						name: 'dPopupTglJatuhTempo',
						id: 'dPopupTglJatuhTempo',
						width: 110,
						format: 'd/M/Y',
						readOnly:true
					},
					{
						x: 390,
						y: 100,
						xtype: 'label',
						text: 'Catatan '
					},
					{
						x: 490,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 500,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopupCatatan',
						id: 'TxtPopupCatatan',
						width: 250
					}
				]
			},
		]
	};
	return items;
}

function GetDTGridPayablesLedger()
{
	var fm = Ext.form;
    var fldDetailPayablesLedger = ['ar_number','ar_date','cust_code','reference','type','info','duedate','notes','debit','kredit','posted','line'];
	dsTRDetailPayablesLedger = new WebApp.DataStore({ fields: fldDetailPayablesLedger })
    
	gridDTPayablesLedger = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Tambah Akun',
            stripeRows: true,
            store: dsTRDetailPayablesLedger,// DATASTORE
            border: true,
			anchor: '100%,70%',
			height:200,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
							rowSelected_PayablesLedger= undefined;
							rowSelected_PayablesLedger = dsTRDetailPayablesLedger.getAt(row);
							CurrentData_PayablesLedger
							CurrentData_PayablesLedger.row = row;
							CurrentData_PayablesLedger.data = rowSelected_PayablesLedger.data;
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				 new Ext.grid.RowNumberer(),
				{
					id:'colAkun_PayablesLedger',
					header:'Akun',
					dataIndex: 'account',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					editor:new Nci.form.Combobox.autoComplete({		
						store	: TrPayablesLedger.form.ArrayStore.b,
						select	: function(a,b,c){
						var line = gridDTPayablesLedger.getSelectionModel().selection.cell[0];
							dsTRDetailPayablesLedger.data.items[line].data.account=b.data.account;
							dsTRDetailPayablesLedger.data.items[line].data.name=b.data.name;
							dsTRDetailPayablesLedger.data.items[line].data.groups=b.data.groups;
							dsTRDetailPayablesLedger.data.items[line].data.description=b.data.description;
							dsTRDetailPayablesLedger.data.items[line].data.line=b.data.line;
							dsTRDetailPayablesLedger.data.items[line].data.debit=0;
							dsTRDetailPayablesLedger.data.items[line].data.kredit=0;
							gridDTPayablesLedger.getView().refresh();
						},
						insert	: function(o){
							return {
								account        	: o.account,
								name        	: o.name,
								groups        	: o.groups,
								debit        	: o.debit,
								kredit        	: o.kredit,
								text			:  '<table style="font-size: 11px;"><tr><td width="140">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
							}
						},
						param:function(){
							return {
								text:''
							}
						},
						url		: baseURL + "index.php/akuntansi/functionTrPayablesLedger/getAccount",
						valueField: 'account',
			 			displayField: 'text',
						listWidth: 340
					})
					
				},
				{
					id:'colNama_PayablesLedger',
					header:'Nama',
					dataIndex: 'name',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{
					id:'colDeskripsi_PayablesLedger',
					header:'Deskripsi',
					dataIndex: 'description',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200,
					editor: new fm.TextField({
					allowBlank: false
					})
					
				},
				{
					id:'colDebit_PayablesLedger',
					header:'Debit',
					dataIndex: 'debit',
					xtype:'numbercolumn',
					sortable: false,
					hidden:false,
					align:'right',
					menuDisabled:true,
					width:100,
					editor: new fm.NumberField({
						allowBlank: false,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsTRDetailPayablesLedger.getRange()[line].data.debit=a.getValue();
								JumlahGrid();
							},
							focus: function(a){
								this.index=gridDTPayablesLedger.getSelectionModel().selection.cell[0]
							}
							
						}
					})
					
				},
				{
					header:'Kredit',
					dataIndex: 'kredit',
					align:'right',
					xtype:'numbercolumn',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					editor: new fm.NumberField({
						allowBlank: false,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsTRDetailPayablesLedger.getRange()[line].data.kredit=a.getValue();
								JumlahGrid();
							},
							focus: function(a){
								this.index=gridDTPayablesLedger.getSelectionModel().selection.cell[0]
							}
							
						}
					})
					
				},
				{
					id:'colLine_PayablesLedger',
					header:'Line',
					dataIndex: 'line',
					sortable: false,
					hidden:true,
					menuDisabled:true,
					editor: new fm.NumberField({
						allowBlank: false,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsTRDetailPayablesLedger.getRange()[line].data.line=a.getValue();
								JumlahGrid();
							},
							focus: function(a){
								this.index=gridDTPayablesLedger.getSelectionModel().selection.cell[0]
							}
							
						}
					})
				}
			]
			),
			viewConfig:{forceFit: true},
			tbar:
			{
				xtype: 'toolbar',
				id: 'toolbar_PayablesLedger',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Baris',
						iconCls: 'add',
						id: 'btnAddRowPayablesLedger',
						handler: function(){
							var records = new Array();
							records.push(new dsTRDetailPayablesLedger.recordType());
							dsTRDetailPayablesLedger.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus Baris',
						iconCls: 'remove',
						id: 'btnDeletePayablesLedger',
						handler: function()
						{
							var line = gridDTPayablesLedger.getSelectionModel().selection.cell[0];
							dsTRDetailPayablesLedger.removeAt(line);
							gridDTPayablesLedger.getView().refresh();
						}
					}

					//-------------- ## --------------
				]
			},
        }
		
    );
    return gridDTPayablesLedger;
}

function dataGrid_PayablesLedger(){
	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionTrPayablesLedger/getGridPayablesLedger",
			params: { 
				NoJurnal:Ext.getCmp('NoJurnal_PayablesLedger').getValue(),
				KdVendor:Ext.getCmp('cbovendor_pencarian').getValue(),
				TglAwal:Ext.getCmp('dtpTglAwalFilterPayablesLedger').getValue(),
				TglAkhir:Ext.getCmp('dtpTglAkhirFilterPayablesLedger').getValue() 
			},
			failure: function(o)
			{
				ShowPesanErrorPayablesLedger('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{

				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ds_TrPayablesLedgerList.removeAll();
					var recs=[],
						recType=ds_TrPayablesLedgerList.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					ds_TrPayablesLedgerList.add(recs);
					grid_PayablesLedger.getView().refresh();
				}
				else 
				{
					ShowPesanErrorPayablesLedger('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}

function combovendor()
{
	var Field = ['kd_vendor','vend_code','vendor','vc','term'];
    ds_vendor = new WebApp.DataStore({fields: Field});
	loadDataCombovendor();
    cbovendor = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 40,
			id:'cbovendor',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:250,
			store: ds_vendor,
			valueField: 'vend_code',
			displayField: 'vc',
			value:'',
			listeners:
			{
					'select': function(a,b,c)
					{
						selectSetPilihan=b.data.displayText ;
						tmpJatuhTempo = new Date().add(Date.DAY,+b.data.term);
						Ext.getCmp('dPopupTglJatuhTempo').setValue(tmpJatuhTempo);
						tmpKdVendor=b.data.vend_code;
					}
			}
		}
	);
	return cbovendor;
};

function combovendor_pencarian()
{
	var Field = ['kd_vendor','vend_code','vendor','vc','term'];
    ds_vendor_pencarian = new WebApp.DataStore({fields: Field});
	loadDataCombovendor_pencarian();
    cbovendor_pencarian = new Ext.form.ComboBox
	(
		{
			x: 155,
			y: 40,
			id:'cbovendor_pencarian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:250,
			store: ds_vendor_pencarian,
			valueField: 'vend_code', //nilai combo box
			displayField: 'vc', //tampilan combo 
			value:'',
			listeners:
			{
					'select': function(a,b,c)
					{
						selectSetPilihan=b.data.displayText ;
					},
					'specialkey' : function()
					{
							if (Ext.EventObject.getKey() === 13) //ketika nama pasien di enter, maka akan ditampilkan data sesuai pencarian
							{
								dataGrid_PayablesLedger(
									Ext.get('NoJurnal_PayablesLedger').getValue(),
									Ext.getCmp('cbovendor_pencarian').getValue(),
									Ext.get('dtpTglAwalFilterPayablesLedger').getValue(),
									Ext.get('dtpTglAwalFilterPayablesLedger').getValue()
								);
							} 						
					}
			}
		}
	);
	return cbovendor_pencarian;
};
function loadDataCombovendor(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/akuntansi/functionTrPayablesLedger/getvendor",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbovendor.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_vendor.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_vendor.add(recs);
				console.log(o);
			}
		}
	});
}
function loadDataCombovendor_pencarian(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/akuntansi/functionTrPayablesLedger/getvendor",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbovendor_pencarian.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_vendor_pencarian.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_vendor_pencarian.add(recs);
				// console.log(o);
			}
		}
	});
}

function dataSave_PayablesLedger(){
	if (ValidasiEntryPayablesLedger(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/functionTrPayablesLedger/savePayablesLedger",
				params: getParamSavePayablesLedger(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorPayablesLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoPayablesLedger('Berhasil menyimpan data ini','Information');
						Ext.getCmp('TxtPopupKdJurnal').setValue(cst.no_jurnal);
						dataGrid_PayablesLedger();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorPayablesLedger('Gagal menyimpan data ini', 'Error');
						dataGrid_PayablesLedger();
					}
				}
			}
			
		)
	}
}

function dataHapus_PayablesLedger(data){
	if(data.posted == 't' ){
		ShowPesanInfoPayablesLedger('Transaksi telah diposting, data gagal dihapus!','Information')
	}else{
		
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/functionTrPayablesLedger/hapus_ap",
				params: {
					ap_number		: Ext.getCmp('TxtPopupKdJurnal').getValue(), 
					ap_date			: Ext.getCmp('dPopupTglJurnal').getValue(),
					referensi		: Ext.getCmp('TxtPopupReferensi').getValue()
					
				},
				failure: function(o)
				{
					ShowPesanErrorPayablesLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoPayablesLedger('Data berhasil dihapus!','Information')
						addNewPayablesLedger();
						dataGrid_PayablesLedger();
					}
					else 
					{
						ShowPesanErrorPayablesLedger('Gagal menghapus data ini', 'Error');
						
					};
				}
			}
			
		)
		
	}
}
function getParamSavePayablesLedger()
{
	
	var	params =
	{
		NoJurnal:Ext.getCmp('TxtPopupKdJurnal').getValue(),
		Vendor:tmpKdVendor,
		Type:n_type,
		Tgl:Ext.getCmp('dPopupTglJurnal').getValue(),
		Posting:tmpposting,
		Referensi:Ext.getCmp('TxtPopupReferensi').getValue(),
		Tgl_JatuhTempo:Ext.getCmp('dPopupTglJatuhTempo').getValue(),
		Catatan:Ext.getCmp('TxtPopupCatatan').getValue()
	}
	
	params['jml']=dsTRDetailPayablesLedger.getCount();
	for(var i = 0 ; i <dsTRDetailPayablesLedger.getCount();i++)
	{
		params['akun-'+i]=dsTRDetailPayablesLedger.data.items[i].data.account;	
		params['deskripsi-'+i]=dsTRDetailPayablesLedger.data.items[i].data.description;	
		params['line-'+i]=i+1;	
		if(dsTRDetailPayablesLedger.data.items[i].data.debit==0){
			params['value-'+i]=dsTRDetailPayablesLedger.data.items[i].data.kredit;
			params['isDebit-'+i]='false';
		}else{
			params['value-'+i]=dsTRDetailPayablesLedger.data.items[i].data.debit;
			params['isDebit-'+i]='true';
		}
	}
    return params

}

function DataInitPayablesLedger(rowdata)
{
	Ext.getCmp('TxtPopupKdJurnal').setValue(rowdata.ap_number);
	Ext.getCmp('cbovendor').setValue(rowdata.vc);
	tmpKdVendor=rowdata.vend_code;
	if(rowdata.type==0){
		n_type=0;
		Ext.getCmp('rb_debit').setValue(true);
	}else{
		Ext.getCmp('rb_kredit').setValue(true);
		n_type=1;
	}
	
	if(rowdata.posted=='t')
	{
		Ext.getCmp('checkPosting').setValue(true);
	}else{
		Ext.getCmp('checkPosting').setValue(false);
	}
	Ext.get('dPopupTglJurnal').dom.value = ShowDate(rowdata.ap_date);
	Ext.getCmp('TxtPopupReferensi').setValue(rowdata.reference);
	dataGridPayablesLedgerDetail(rowdata.ap_number,rowdata.ap_date,rowdata.line);
	Ext.get('dPopupTglJatuhTempo').dom.value = ShowDate(rowdata.due_date);
	Ext.get('TxtPopupCatatan').dom.value = rowdata.notes;
};

function dataGridPayablesLedgerDetail(kd_jurnal, tgl_jurnal){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionTrPayablesLedger/getPayablesLedger", // memanggil query 
			params: { //parameter 
						KdJurnal:kd_jurnal,
						TglJurnal:tgl_jurnal
					},
			failure: function(o)
			{
				ShowPesanErrorPayablesLedger('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsTRDetailPayablesLedger.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetailPayablesLedger.add(recs); //data ditampung ke datastore
					gridDTPayablesLedger.getView().refresh(); //untuk menampilkan data ke grid
					JumlahGrid();
				}
				else 
				{
					ShowPesanErrorPayablesLedger('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}
function JumlahGrid(){
	var jumlahdebit=0;
	var jumlahcredit=0;
	for(var i=0; i < dsTRDetailPayablesLedger.getCount() ; i++){
		var o=dsTRDetailPayablesLedger.getRange()[i].data;
		if(o.debit != undefined || o.kredit != undefined ){
			jumlahdebit += parseFloat(o.debit);
			jumlahcredit += parseFloat(o.kredit);
		}
	}
	gridDTPayablesLedger.getView().refresh();
	Ext.getCmp('txtTotDebit_PayablesLedger').setValue(toFormat(jumlahdebit));
	Ext.getCmp('txtTotCredit_PayablesLedger').setValue(toFormat(jumlahcredit));
}

function ValidasiEntryPayablesLedger(modul,mBolHapus)
{
	var x = 1;
	if( tmpKdVendor == '' || dsTRDetailPayablesLedger.getCount() === 0 || dsTRDetailPayablesLedger.getCount()  === 1 ){
		if(tmpKdVendor === ''){
			ShowPesanWarningPayablesLedger('Vendor belum dipilih!', 'Warning');
			x = 0;
		} else if(dsTRDetailPayablesLedger.getCount() === 1 ){
			ShowPesanWarningPayablesLedger('Daftar akun minimal 2 baris!', 'Warning');
			x = 0;
		} 
	}
	
	
	for(var i=0; i<dsTRDetailPayablesLedger.getCount() ; i++){
		var o=dsTRDetailPayablesLedger.getRange()[i].data;

		if(o.account == undefined || o.account == ""){
			ShowPesanWarningPayablesLedger('Akun masih kosong, harap isi baris kosong atau hapus untuk melanjutkan!', 'Warning');
			x = 0;
		} else{
			if(o.kredit != undefined || o.debit != undefined){
				if(o.kredit == undefined){
					ShowPesanWarningPayablesLedger('Kredit baris ke '+ (i+1) +' masih kosong, kredit tidak boleh kosong. Mohon periksa kembali!', 'Warning');
					x = 0;
				}else if(o.debit == undefined){
					ShowPesanWarningPayablesLedger('Debit baris ke '+ (i+1) +' masih kosong, debit tidak boleh kosong. Mohon periksa kembali!', 'Warning');
					x = 0;
				} else if(o.debit != 0 && o.kredit != 0){
					ShowPesanWarningPayablesLedger('Mohon periksa kembali baris ke '+ (i+1) +'. Isi salah satu kolom debit atau kredit!', 'Warning');
					x = 0;
				} else if(o.debit == 0 && o.kredit == 0){
					ShowPesanWarningPayablesLedger('Mohon periksa kembali baris ke '+ (i+1) +'. Nilai akun masih kosong!', 'Warning');
					x = 0;
				}
			}
		}
	}
	
	return x;
};
function addNewPayablesLedger()
{
	Ext.getCmp('TxtPopupKdJurnal').setValue('');
	Ext.getCmp('cbovendor').setValue('');
	Ext.getCmp('TxtPopupReferensi').setValue('');
	Ext.getCmp('dPopupTglJatuhTempo').setValue('');
	Ext.getCmp('TxtPopupCatatan').setValue('');
	Ext.getCmp('checkPosting').setValue(false);
	dsTRDetailPayablesLedger.removeAll();
	n_type=0;
	tmpposting='False';
	tmpKdVendor='';
	Ext.getCmp('rb_debit').setValue(true);
	JumlahGrid();
}