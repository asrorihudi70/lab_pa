// Data Source ExtJS # --------------

/**
*	Nama File 		: TRPosting_GeneralLedger.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: POsting Buku Besar (General Ledger Posting), Piutang, Hutang
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Di EDIT tanggal : Juli 2016
*	Di EDIT tanggal : 14 Desember 2016
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*	Editing			: Agung
*	Editing			: Melinda
*/

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var NamaForm_Posting_GeneralLedger="Posting Transaksi Buku Besar";
var now_Posting_GeneralLedger= new Date();
CurrentPage.page = window_Posting_GeneralLedger(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var dsTRDetail_PostingGeneralLedger;
var gridAkun_PostingGeneralLedger;



function window_Posting_GeneralLedger(mod_id_Posting_GeneralLedger)
{	

	var grid_Posting_GeneralLedger = new Ext.Panel
    (
		{
			title: '',
			iconCls: 'Studi_Lanjut',
			id: 'grid_PostingGeneralLedger',
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			bodyStyle: 'padding:10px 10px 10px 10px',
			items: [Getdet_PostingGeneralLedger()],
			//-------------- # End tbar # --------------
       }
    )
    var form_Posting_GeneralLedger = new Ext.Panel
    (
		{
			title: NamaForm_Posting_GeneralLedger,
			iconCls: 'Studi_Lanjut',
			id: mod_id_Posting_GeneralLedger,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: true,  
			margins: '0 5 5 0',
			items: [detail_Posting_GeneralLedger(),
				grid_Posting_GeneralLedger
			],
			tbar:
			[]
			//-------------- # End tbar # --------------
       }
    )
    return form_Posting_GeneralLedger;
    //-------------- # End form filter # --------------
}
// End Function window_Posting_GeneralLedger # --------------

/**
*	Function : setLookUp_Posting_GeneralLedger
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function detail_Posting_GeneralLedger(lebar) 
{
	var pbar1 = new Ext.ProgressBar({
		id:'pbar1',
		width:300
	});
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:150,
	    items:
		[   
					{	title:'Posting Transaksi Buku Besar, Buku Besar Piutang, Buku Besar Hutang' ,
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 150,
						anchor: '100% 80%',
						items:
						[                          
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Tanggal post'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'date_Posting_GeneralLedger',
								name: 'date_Posting_GeneralLedger',
								format: 'd/M/Y',
								//readOnly : true,
								value: now_Posting_GeneralLedger,
								width: 150
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Periode'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'numberfield',
								fieldLabel: 'Periode ',
								id: 'Periode_Posting_GeneralLedger',
								name: 'Periode_Posting_GeneralLedger',
								width: 100
							},
							{
								x: 410,
								y: 10,
								xtype: 'label',
								id  : 'Posting_GeneralLedger_label1',
								hidden:true,
								text: 'Hati-hati :'
							},
							{
								x: 520,
								y: 10,
								id  : 'Posting_GeneralLedger_label2',
								xtype: 'label',
								hidden: true,
								text: 'sedang dilakukan proses penarikan data'
							},
							{
								x: 520,
								y: 40,
								id  : 'Posting_GeneralLedger_label3',
								xtype: 'label',
								hidden: true,
								text: 'Harap tunggu sampai selesai'
							},
							{	x: 290,
								y: 10,
								xtype:'button',
								text:'Proses' ,
								width:70,
								hideLabel:true,
								id: 'btn_find_Posting_GeneralLedger',
								handler:function() 
								{
									Ext.MessageBox.show({
									  msg: 'Posting sedang di proses, harap tunggu sampai proses selesai...',
									  progressText: 'proses...',
									  width:500,
									  wait:true,
									  waitConfig: {interval:200}
									}); 
									
									if (ValidasiEntryPostingGeneralLedger(nmHeaderSimpanData,false) == 1 )
									{
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/akuntansi/functionPostingGeneralLedger/posting",
												params: {
													periode:Ext.getCmp('Periode_Posting_GeneralLedger').getValue(),
													tgl_posting:Ext.getCmp('date_Posting_GeneralLedger').getValue()
												},
												failure: function(o)
												{
													ShowPesanErrorPostingGeneralLedger('Error posting, Hubungi Admin!', 'Error');
													Ext.MessageBox.hide();  
													loadMask.hide();
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.status === true) 
													{
														Ext.MessageBox.hide();  
														ShowPesanInfoPostingGeneralLedger('Data berhasil diposting','Information');
														viewGridPosting_PostingGeneralLedger();
													}
													else 
													{
														ShowPesanErrorPostingGeneralLedger('Gagal melakukan posting', 'Error');
														
													}; 
													// Ext.MessageBox.hide();  
													// loadMask.hide();
												}
											}
											
										)
									}
								}
							}
							
							
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};

function Getdet_PostingGeneralLedger() 
{
    var fldDetail = ['account','name','valuedebit','valuecredit'];
	
    dsTRDetail_PostingGeneralLedger = new WebApp.DataStore({ fields: fldDetail })
    gridAkun_PostingGeneralLedger = new Ext.grid.EditorGridPanel
    (
        {
			title: '',
            stripeRows: true,
            store: dsTRDetail_PostingGeneralLedger,
            border: true,
			height:300,
			anchor: '100%,70%',
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            currentcellSelected_PostingGeneralLedger = dsTRDetail_PostingGeneralLedger.getAt(row);
                            Current_PostingGeneralLedger.row = row;
                            Current_PostingGeneralLedger.data = currentcellSelected_PostingGeneralLedger;
                        }
                    }
                }
            ),
            cm: TrPostingGeneralLedgerColumModel(),
			listeners: {
				rowclick: function( $this, rowIndex, e )
				{
					cellCurrent_PostingGeneralLedger = rowIndex;
				},
				celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
			
				}
			},
			viewConfig: 
			{
				forceFit: true
			}
			
        }
	);
	
	

    return gridAkun_PostingGeneralLedger;
};

function TrPostingGeneralLedgerColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [	
			new Ext.grid.RowNumberer(),
			{
                header: 'Akun',
                dataIndex: 'account',
                width:200,
				menuDisabled:true,
				hidden :false
                
            },
            {
                header: 'Nama',
                dataIndex: 'name',
                width:230,
				menuDisabled:true,
				hidden :false
                
            },
			{
				xtype:'numbercolumn',
				align:'right',
                header: 'Debit',
                dataIndex: 'valuedebit',
                width:100,
                menuDisabled:true,
                hidden:false
            },{
				xtype:'numbercolumn',
				align:'right',
                header: 'Kredit',
                dataIndex: 'valuecredit',
                width:100,
                menuDisabled:true,
                hidden:false
            }
     
        ]
    )
};

function viewGridPosting_PostingGeneralLedger(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionPostingGeneralLedger/getGridDetailAkun",
			params: {
				periode:Ext.getCmp('Periode_Posting_GeneralLedger').getValue(),
				tgl_posting:Ext.getCmp('date_Posting_GeneralLedger').getValue()
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorPostingGeneralLedger('Error detail akun. Hubungi Admin!', 'Error');
			},	
			success: function(o) 
			{   
				dsTRDetail_PostingGeneralLedger.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsTRDetail_PostingGeneralLedger.recordType;

					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetail_PostingGeneralLedger.add(recs);
					
					gridAkun_PostingGeneralLedger.getView().refresh();
				} 
				else 
				{
					loadMask.hide();
					ShowPesanErrorPostingGeneralLedger('Gagal membaca data ini', 'Error');
				};
				
			}
		}
		
	);
}


function ValidasiEntryPostingGeneralLedger(modul,mBolHapus){
	var x = 1;
	var valperiode = parseInt(Ext.getCmp('Periode_Posting_GeneralLedger').getValue());
	
	if (valperiode > 12)
	{
		loadMask.hide();
		ShowPesanWarningPostingGeneralLedger('Periode tidak boleh lebih dari 12!', 'Warning');
		x = 0;
	}
	else if (valperiode < 1 || Ext.getCmp('Periode_Posting_GeneralLedger').getValue()=='')
	{
		loadMask.hide();
		ShowPesanWarningPostingGeneralLedger('Periode tidak boleh kosong atau tidak boleh kurang dari 1!', 'Warning');
		x = 0;
	}
	return x;
};


function ShowPesanWarningPostingGeneralLedger(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:280
		}
	);
};

function ShowPesanErrorPostingGeneralLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPostingGeneralLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};
