// Data Source ExtJS # --------------

/**
*	Nama File 		: TRPosting_PayablesLedger.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var NamaForm_Posting_PayablesLedger="Posting Buku Besar Hutang ";
var now_Posting_PayablesLedger= new Date();
CurrentPage.page = window_Posting_PayablesLedger(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);



function window_Posting_PayablesLedger(mod_id_Posting_PayablesLedger)
{	
    var form_Posting_PayablesLedger = new Ext.Panel
    (
		{
			title: NamaForm_Posting_PayablesLedger,
			iconCls: 'Studi_Lanjut',
			id: mod_id_Posting_PayablesLedger,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [detail_Posting_PayablesLedger()],
			tbar:
			[]
			//-------------- # End tbar # --------------
       }
    )
    return form_Posting_PayablesLedger;
    //-------------- # End form filter # --------------
}
// End Function window_Posting_PayablesLedger # --------------

/**
*	Function : setLookUp_Posting_PayablesLedger
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function detail_Posting_PayablesLedger(lebar) 
{var pbar1 = new Ext.ProgressBar({
    id:'pbar1',
    width:300
});
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:150,
	    items:
		[   
					{	title:'Posting Buku Besar Hutang' ,
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 150,
						anchor: '100% 80%',
						items:
						[                          
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Tanggal pos'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'date_Posting_PayablesLedger',
								name: 'date_Posting_PayablesLedger',
								format: 'd/M/Y',
								//readOnly : true,
								value: now_Posting_PayablesLedger,
								width: 150
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Periode'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'numberfield',
								fieldLabel: 'Periode ',
								id: 'Periode_Posting_PayablesLedger',
								name: 'Periode_Posting_PayablesLedger',
								width: 100
							},
							{
								x: 410,
								y: 10,
								xtype: 'label',
								id  : 'Posting_PayablesLedger_label1',
								hidden:true,
								text: 'Hati-hati :'
							},
							{
								x: 520,
								y: 10,
								id  : 'Posting_PayablesLedger_label2',
								xtype: 'label',
								hidden: true,
								text: 'sedang dilakukan proses penarikan data'
							},
							{
								x: 520,
								y: 40,
								id  : 'Posting_PayablesLedger_label3',
								xtype: 'label',
								hidden: true,
								text: 'Harap tunggu sampai selesai'
							},
							{	x: 290,
								y: 10,
								xtype:'button',
								text:'Proses' ,
								width:70,
								hideLabel:true,
								id: 'btn_find_Posting__PayablesLedger',
								handler:function() 
								{//progress bar
									Ext.getCmp('Posting_PayablesLedger_label1').show();
									Ext.getCmp('Posting_PayablesLedger_label2').show();
									Ext.getCmp('Posting_PayablesLedger_label3').show();
								/* 	 Ext.MessageBox.show({
									  msg: 'proses pengambilan data, harap tunggu sampai proses selesai...',
									  progressText: 'proses...',
									  width:500,
									  wait:true,
									  waitConfig: {interval:200}
									});  */
									//Ext.MessageBox.hide();  
									Posting_PayablesLedger(
										Ext.get('date_Posting_PayablesLedger').getValue(),
										Ext.get('Periode_Posting_PayablesLedger').getValue()
									);
								}
							}
							
							
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};

function Posting_PayablesLedger(tgl,periode){
	// if (ValidasiEntryPayablesLedger(nmHeaderSimpanData,false) == 1 )
	// {
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/function_Posting_TrPayablesLedger/postingPayablesLedger",
				params: {
							Tgl:tgl,
							Periode:periode
						},
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorPostingPayablesLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.status === true) 
					{
						loadMask.hide();
						ShowPesanInfoPostingPayablesLedger('Berhasil posting ','Information');
						// Ext.getCmp('TxtPopupKdJurnal').setValue(cst.no_jurnal);
						// dataGrid_PayablesLedger();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorPostingPayablesLedger('Gagal posting', 'Error');
						// dataGrid_PayablesLedger();
					}
				}
			}
			
		)
	// }
}

function ShowPesanWarningPostingPayablesLedger(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPostingPayablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPostingPayablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};
