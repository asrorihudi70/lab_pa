// Data Source ExtJS # --------------

/**
*	Nama File 		: TRPosting_ReceivablesLedger.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var NamaForm_Posting_ReceivablesLedger="Posting Buku Besar Piutang ";
var now_Posting_ReceivablesLedger= new Date();
CurrentPage.page = window_Posting_ReceivablesLedger(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);



function window_Posting_ReceivablesLedger(mod_id_Posting_ReceivablesLedger)
{	
    var form_Posting_ReceivablesLedger = new Ext.Panel
    (
		{
			title: NamaForm_Posting_ReceivablesLedger,
			iconCls: 'Studi_Lanjut',
			id: mod_id_Posting_ReceivablesLedger,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [detail_Posting_ReceivablesLedger()],
			tbar:
			[]
			//-------------- # End tbar # --------------
       }
    )
    return form_Posting_ReceivablesLedger;
    //-------------- # End form filter # --------------
}
// End Function window_Posting_ReceivablesLedger # --------------

/**
*	Function : setLookUp_Posting_ReceivablesLedger
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function detail_Posting_ReceivablesLedger(lebar) 
{var pbar1 = new Ext.ProgressBar({
    id:'pbar1',
    width:300
});
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:150,
	    items:
		[   
					{	title:'Posting Buku Besar Piutang' ,
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 150,
						anchor: '100% 80%',
						items:
						[                          
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Tanggal pos'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'date_Posting_ReceivablesLedger',
								name: 'date_Posting_ReceivablesLedger',
								format: 'd/M/Y',
								//readOnly : true,
								value: now_Posting_ReceivablesLedger,
								width: 150
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Periode'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'numberfield',
								fieldLabel: 'Periode ',
								id: 'Periode_Posting_ReceivablesLedger',
								name: 'Periode_Posting_ReceivablesLedger',
								width: 100
							},
							{
								x: 410,
								y: 10,
								xtype: 'label',
								id  : 'Posting_ReceivablesLedger_label1',
								hidden:true,
								text: 'Hati-hati :'
							},
							{
								x: 520,
								y: 10,
								id  : 'Posting_ReceivablesLedger_label2',
								xtype: 'label',
								hidden: true,
								text: 'sedang dilakukan proses penarikan data'
							},
							{
								x: 520,
								y: 40,
								id  : 'Posting_ReceivablesLedger_label3',
								xtype: 'label',
								hidden: true,
								text: 'Harap tunggu sampai selesai'
							},
							{	x: 290,
								y: 10,
								xtype:'button',
								text:'Proses' ,
								width:70,
								hideLabel:true,
								id: 'btn_find_Posting__ReceivablesLedger',
								handler:function() 
								{//progress bar
									Ext.getCmp('Posting_ReceivablesLedger_label1').show();
									Ext.getCmp('Posting_ReceivablesLedger_label2').show();
									Ext.getCmp('Posting_ReceivablesLedger_label3').show();
								/* 	 Ext.MessageBox.show({
									  msg: 'proses pengambilan data, harap tunggu sampai proses selesai...',
									  progressText: 'proses...',
									  width:500,
									  wait:true,
									  waitConfig: {interval:200}
									});  */
									//Ext.MessageBox.hide();  
									Posting_ReceivablesLedger(
										Ext.get('date_Posting_ReceivablesLedger').getValue(),
										Ext.get('Periode_Posting_ReceivablesLedger').getValue()
									);
								}
							}
							
							
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};

function Posting_ReceivablesLedger(tgl,periode){
	// if (ValidasiEntryReceivablesLedger(nmHeaderSimpanData,false) == 1 )
	// {
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/function_Posting_TrReceivablesLedger/postingReceivablesLedger",
				params: {
							Tgl:tgl,
							Periode:periode
						},
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorPostingReceivablesLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoPostingReceivablesLedger('Berhasil posting ','Information');
						// Ext.getCmp('TxtPopupKdJurnal').setValue(cst.no_jurnal);
						// dataGrid_ReceivablesLedger();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorPostingReceivablesLedger('Gagal posting', 'Error');
						// dataGrid_ReceivablesLedger();
					}
				}
			}
			
		)
	// }
}

function ShowPesanWarningPostingReceivablesLedger(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPostingReceivablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPostingReceivablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};
