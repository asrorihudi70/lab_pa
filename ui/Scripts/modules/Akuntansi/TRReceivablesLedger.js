var TrReceivablesLedger={};
TrReceivablesLedger.form={};
TrReceivablesLedger.func={};
TrReceivablesLedger.vars={};
TrReceivablesLedger.func.parent=TrReceivablesLedger;
TrReceivablesLedger.form.ArrayStore={};
TrReceivablesLedger.form.ComboBox={};
TrReceivablesLedger.form.DataStore={};
TrReceivablesLedger.form.Record={};
TrReceivablesLedger.form.Form={};
TrReceivablesLedger.form.Grid={};
TrReceivablesLedger.form.Panel={};
TrReceivablesLedger.form.TextField={};
TrReceivablesLedger.form.Button={};
var autocom_gen;
var cellCurrentrec='';
TrReceivablesLedger.form.ArrayStore.a=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_customer','ar_number','ar_date','cust_code','reference','type','info','due_date','notes','cust_code','customer','cc','term','line'
				],
		data: []
});

TrReceivablesLedger.form.ArrayStore.b=new Ext.data.ArrayStore({
		id: 0,
		fields: [	'account','name','groups'],
		data: []
});


var CurrentData_ReceivablesLedger =

{
    data: Object,
    details: Array,
    row: 0
};

var Current_ReceivablesLedgerList =

{
    data: Object,
    details: Array,
    row: 0
};

/* Grid Data Detail Piutang*/
var gridDTReceivablesLedger;
var dsTRDetail_ReceivablesLedger;
var rowSelected_ReceivablesLedger;

/* Grid List Data Piutang*/
var grid_ReceivablesLedger;
var ds_TrReceivablesLedgerList;
var rowSelected_TrReceivablesLedgerList;


var ds_Customer; // datastore untuk combo customer di form input data piutang
var ds_Customer_pencarian; //datastore untuk combo customer di form pencarian data piutang
var tmpposting; //var untuk menampung nilai posting
var tmpJatuhTempo; //var untuk menampung tgl jatuh tempo
var type_trans=false; // var untuk menampung tipe transaksi (debit/kredit)
var now = new Date();
var tigaharilalu = new Date().add(Date.DAY, -3);
var tmpKdCustomer=''; // var untuk menampung kode customer
var n_type=0; // var untuk set nilai tipe (debit/kredit)

CurrentPage.page = getPanelReceivablesLedger(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelReceivablesLedger(mod_id) //panel utama
{
	// dataGrid_ReceivablesLedger(); // load data piutang
	
	var Field = ['status_posting','ar_number','ar_date','cust_code','reference','type','info','due_date','notes','customer','cc','posted']
	ds_TrReceivablesLedgerList = new WebApp.DataStore({ fields: Field }); // data store list data piutang

    grid_ReceivablesLedger = new Ext.grid.EditorGridPanel
    (
        {
			title: 'Daftar Transaksi Piutang',
            stripeRows: true,
            store: ds_TrReceivablesLedgerList,
            border: false,
			height:400,
			anchor: '100%,70%',
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {	
							
                            rowSelected_TrReceivablesLedgerList = ds_TrReceivablesLedgerList.getAt(row);
							rowSelected_TrReceivablesLedgerList = undefined;
							rowSelected_TrReceivablesLedgerList = ds_TrReceivablesLedgerList.getAt(row);
							Current_ReceivablesLedgerList
							Current_ReceivablesLedgerList.row = row;
							Current_ReceivablesLedgerList.data = rowSelected_TrReceivablesLedgerList.data;
                        }
                    }
                }
            ),
			listeners:
            {
				//action jika isi grid di double klik
                rowdblclick: function (sm, ridx, cidx)
                {

                    rowSelected_TrReceivablesLedgerList = ds_TrReceivablesLedgerList.getAt(ridx);
                    if (rowSelected_TrReceivablesLedgerList !== undefined)
                    {
						PenHasilLookUpReceivablesLedgerList(rowSelected_TrReceivablesLedgerList.data); //memanggil fungsi untuk menampilkan window
						
					}
                    else
                    {
                        PenHasilLookUpReceivablesLedgerList();
                    }
                }
            },
            cm: new Ext.grid.ColumnModel
            (
                [
                    new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 80,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						align		: 'center',
						menuDisabled: true,
						dataIndex	: 'posted',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
                        id: 'colViewNoJurnal',
                        header: 'No. Jurnal',
                        dataIndex: 'ar_number',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
					{
                        id: 'colViewCustomer',
                        header: 'Customer',
                        dataIndex: 'customer',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 120
                    },
                    {
                        id: 'colViewTglJurnal',
                        header: 'Tgl Jurnal',
                        dataIndex: 'ar_date',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 70,
						renderer: function(v, params, record)
						{
								return ShowDate(record.data.ar_date);

						}
                    },
                    {
                        id: 'colViewReferensi',
                        header: 'Referensi',
                        dataIndex: 'reference',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
					{
                        id: 'colViewCatatan',
						header: 'Catatan',
                        width: 200,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'notes'                        
                    }
                ]
            ),
			
			viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnTambahReceivablesLedger',
                        text: 'Tambah Baru',
                        tooltip: nmEditData,
                        iconCls: 'add',
                        handler: function(sm, row, rec)
                        {
							PenHasilLookUpReceivablesLedgerList();	// window menampilkan form untuk input data
                        }
                    },
					{
						xtype: 'tbseparator'
					},
					{
                        id: 'btnRefreshReceivablesLedger',
                        text: 'Refresh',
                        tooltip: nmEditData,
                        iconCls: 'refresh',
                        handler: function(sm, row, rec)
                        {
							ds_TrReceivablesLedgerList.removeAll();
							dataGrid_ReceivablesLedger(); //load data piutang
							Ext.getCmp('NoJurnal_ReceivablesLedger').setValue('');							
							Ext.getCmp('cboCustomer_pencarian').setValue('');	
                        }
                    },
                ]
			 
        }
	);
	
	var pencarianTransaksi_ReceivablesLedger = new Ext.FormPanel({
        labelAlign: 'top',
        frame:false,
        title: 'Pencarian',
        bodyStyle:'padding:0px 0px 0px 0px',
        items: [
			 getPanelInput_ReceivablesLedger()
			
		]
	})
	
    var FormDepanReceivablesLedger = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Buku Besar Piutang',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						pencarianTransaksi_ReceivablesLedger,
						grid_ReceivablesLedger
                       // getFormloadl_ReceivablesLedger() //panel grid data piutang
                   ],
           tbar:
            [
			
				
			],
			tbar:
            [
			
				
			],
            listeners:
            {
                'afterrender': function()
                {
					dataGrid_ReceivablesLedger();
				}, 'beforeclose': function(){
					
				}
            }
        }
    );
	
   return FormDepanReceivablesLedger;

};



//panel untuk pencarian data piutang
function getPanelInput_ReceivablesLedger(lebar) 
{
    var items =
	{
		title:'',
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 10px',
	    border:false,
	    height:150,
	    items:
		[   
			{
				columnWidth:.99,
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: true,
				width: 100,
				height: 150,
				anchor: '100% 80%',
				items:
				[                          
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Nomor Jurnal'
					},
					{
						x: 145,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					{
						x: 155,
						y: 10,
						xtype: 'textfield',
						id: 'NoJurnal_ReceivablesLedger',
						name: 'NoJurnal_ReceivablesLedger',
						width: 200,
						enableKeyEvents: true,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika no jurnal di enter, maka akan ditampilkan data sesuai pencarian
								{
								    // refresh data grid sesuai inputan
									dataGrid_ReceivablesLedger();
								} 						
							}
                        }
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Customer'
					},
					{
						x: 145,
						y: 40,
						xtype: 'label',
						text: ':'
					},
					comboCustomer_pencarian(), //pencarian berdasarkan customer
					
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Tanggal Jurnal '
					},
					{
						x: 145,
						y: 70,
						xtype: 'label',
						text: ':'
					},
					{
						x: 155,
						y: 70,
						xtype: 'datefield',
						id: 'dtpTglAwalFilterReceivablesLedger',
						format: 'd/M/Y',
						value: tigaharilalu,
						width: 100,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
								{
									// refresh data grid sesuai inputan
									dataGrid_ReceivablesLedger();
								} 						
							}
						}
					},
					{
						x: 270,
						y: 70,
						xtype: 'label',
						text: 's/d '
					},
					{
						x: 305,
						y: 70,
						xtype: 'datefield',
						id: 'dtpTglAkhirFilterReceivablesLedger',
						format: 'd/M/Y',
						value: now,
						width: 100,
						listeners:
						{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
									{
										// refresh data grid sesuai inputan
										dataGrid_ReceivablesLedger();
									} 						
								}
						}
					},
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: '*) Tekan enter untuk mencari',
						style:{'font-size':'10'}
					}
							
							//-------------------------------------------------------------
							
				]
			},	//akhir panel biodata pasien
		]
	};
    return items;
};



function ShowPesanWarningReceivablesLedger(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorReceivablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoReceivablesLedger(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function PenHasilLookUpReceivablesLedgerList(rowdata) 
{
    var lebar = 800;
    FormLookUpsdetailReceivablesLedger = new Ext.Window //window yang berisi data pasien dan grid editor hasil hemodialisa
    (
        {
            id: 'gridReceivablesLedger',
            title: 'Entry Buku Besar Piutang',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: true,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryReceivablesLedger(lebar,rowdata), //item form
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailReceivablesLedger.show(); //menampilkan form
    if (rowdata === undefined) {
		
	}
	else{
		DataInitReceivablesLedger(rowdata); //load data detail piutang 
	}

};
//item form input data piutang
function getFormEntryReceivablesLedger(lebar,data) {
    var pnlReceivablesLedger = new Ext.FormPanel //panel untuk form input data
    (
        {
            id: 'PanelReceivablesLedger',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:180,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
						viewlookupeditReceivablesLedger(lebar) //item panel form input data
					],
			tbar:
			[
		
				{
					id:'btnTambahBaru',
					text: 'Tambah Baru',
					tooltip: 'Tambah Baru',
					iconCls:'add',
					handler: function()
					{
					   addNewReceivablesLedger(); //clear 
					}
				},
				{
					xtype:'tbseparator'
				},
				{
					id:'btnSimpan',
					text: 'Simpan',
					tooltip: 'Simpan',
					iconCls:'save',
					handler: function()
					{
					   dataSave_ReceivablesLedger(); //simpan data 
					}
				},
				{
					xtype:'tbseparator'
				},
				{
					id:'btnHapus',
					text: 'Hapus',
					tooltip: 'Hapus',
					iconCls:'remove',
					handler: function()
					{
					   dataHapus_ReceivablesLedger(data); //hapus data 
					}
				}
				
			],
			
        }
    );
	// toolbar grid lookup
	var x;

	var pnlReceivablesLedger2 = new Ext.FormPanel //panel untuk grid data detail piutang
    (
        {
            id: 'PanelReceivablesLedger2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:210,
            anchor: '100%',
            width: lebar,
            border: false,
            items:	[	
						GetDTGridReceivablesLedger(lebar) //item panel
					]
        }
    );
	
	var pnlReceivablesLedger3 = new Ext.FormPanel //panel untuk menampilkan jumlah kredit dan debit
    (
        {
            id: 'PanelReceivablesLedger3',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:50,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	
						{
							columnWidth:.99,
							layout: 'absolute',
							bodyStyle: 'padding: 10px 10px 10px 10px',
							border: false,
							// width: 100,
							height: 50,
							anchor: '100% 80%',
							items:
							[                          
								{
									x: 520,
									y: 10,
									readOnly:true,
									xtype: 'textfield',
									fieldLabel: 'Debit ',
									id: 'txtTotDebit_ReceivablesLedger',
									name: 'txtTotDebit_ReceivablesLedger',
									style:{'text-align':'right'},
									width: 110
								},
								{
									x: 640,
									y: 10,
									readOnly:true,
									xtype: 'textfield',
									fieldLabel: 'Kredit ',
									id: 'txtTotCredit_ReceivablesLedger',
									name: 'txtTotCredit_ReceivablesLedger',
									style:{'text-align':'right'},
									width: 110
								}
							]
						}						
					]
        }
    );

    var FormDetailReceivablesLedger = new Ext.Panel //panel yang menampung panel form, grid dan jumlah 
	(
		{
		    id: 'FormDepanReceivablesLedger',
		    region: 'center',
		    width: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:10px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[ 
				pnlReceivablesLedger, //panel input data
				pnlReceivablesLedger2, //panel grid detail data
				pnlReceivablesLedger3 //panel jumlah debit kredit
			]

		}
	);
	
    return FormDetailReceivablesLedger
}

function viewlookupeditReceivablesLedger(lebar){ // item panel form detail 
	var radiosAr = new Ext.form.RadioGroup({
        id: 'radiosaReceivablesLedger',
		x:130,
		y:70,
        columns: 5,
        items: [
            {
                id: 'rb_debitReceivablesLedger',
                name: 'rb',
                boxLabel: 'Debit',
                checked: true,
                inputValue: "false"
            }
            ,
            {
                boxLabel: 'Kredit',
                id: 'rb_kreditReceivablesLedger',
                name: 'rb',
                inputValue: "true"
            }
        ],
        listeners: {
            change: function ()
            {
                type_trans = radiosAr.getValue().inputValue;
				if(type_trans == 'false'){
					n_type = 0;
				}else{
					n_type = 1;
				}
            }
        }

    });
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-40,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:true,
	    height:140,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				// width: 100,
				height: 150,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Nomor Jurnal '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textfield',
						name: 'TxtPopupKdJurnalReceivablesLedger',
						id: 'TxtPopupKdJurnalReceivablesLedger',
						readOnly: true,
						width: 80
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Customer '
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					comboCustomer(),
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Tipe '
					},
					{
						x: 110,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
					radiosAr,
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Tanggal '
					},
					{
						x: 110,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 100,
						xtype: 'datefield',
						name: 'dPopupTglJurnalReceivablesLedger',
						id: 'dPopupTglJurnalReceivablesLedger',
						width: 110,
						format: 'd/M/Y',
						value: now,
						readOnly:true
					},
					{
						x: 390,
						y: 10,
						xtype: 'checkbox',
						id: 'checkPostingReceivablesLedger',
						text: 'Posted',
						handler: function ()
						{
							if (this.getValue() === true)
							{
								tmpposting = 'True';
							} else
							{
								tmpposting = 'False';
							}

						},
						disabled:true
					},
					{
						x: 410,
						y: 10,
						xtype: 'label',
						text: ' Posting '
					},
					{
						x: 390,
						y: 40,
						xtype: 'label',
						text: 'Referensi'
					},
					{
						x: 490,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 500,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupReferensiReceivablesLedger',
						id: 'TxtPopupReferensiReceivablesLedger',
						width: 200 
					},
					{
						x: 390,
						y: 70,
						xtype: 'label',
						text: 'Jatuh Tempo'
					},
					{
						x: 490,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 500,
						y : 70,
						xtype: 'datefield',
						name: 'dPopupTglJatuhTempoReceivablesLedger',
						id: 'dPopupTglJatuhTempoReceivablesLedger',
						width: 110,
						format: 'd/M/Y',
						readOnly:true
					},
					{
						x: 390,
						y: 100,
						xtype: 'label',
						text: 'Catatan '
					},
					{
						x: 490,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 500,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopupCatatanReceivablesLedger',
						id: 'TxtPopupCatatanReceivablesLedger',
						width: 250
					}
				]
			},
		]
	};
	return items;
}

// view grid data detail piutang
function GetDTGridReceivablesLedger()
{
	var fm = Ext.form;
    var fldDetailReceivablesLedger = ['ar_number','ar_date','cust_code','reference','type','info','duedate','notes','debit','kredit','posted','line'];
	dsTRDetailReceivablesLedger = new WebApp.DataStore({ fields: fldDetailReceivablesLedger }) //datastore data detail piutang
    
	gridDTReceivablesLedger = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Tambah Akun',
            stripeRows: true,
            store: dsTRDetailReceivablesLedger,// DATASTORE
            border: true,
			anchor: '100%,70%',
			height:200,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
							rowSelected_ReceivablesLedger= undefined;
							rowSelected_ReceivablesLedger = dsTRDetailReceivablesLedger.getAt(row);
							CurrentData_ReceivablesLedger
							CurrentData_ReceivablesLedger.row = row;
							CurrentData_ReceivablesLedger.data = rowSelected_ReceivablesLedger.data;
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				new Ext.grid.RowNumberer(),
				{
					id:'colAkun_ReceivablesLedger',
					header:'Akun',
					dataIndex: 'account',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					editor:new Nci.form.Combobox.autoComplete({		
						store	: TrReceivablesLedger.form.ArrayStore.b,
						select	: function(a,b,c){
						var line = gridDTReceivablesLedger.getSelectionModel().selection.cell[0];
							dsTRDetailReceivablesLedger.data.items[line].data.account=b.data.account;
							dsTRDetailReceivablesLedger.data.items[line].data.name=b.data.name;
							dsTRDetailReceivablesLedger.data.items[line].data.groups=b.data.groups;
							dsTRDetailReceivablesLedger.data.items[line].data.description=b.data.description;
							dsTRDetailReceivablesLedger.data.items[line].data.line=b.data.line;
							dsTRDetailReceivablesLedger.data.items[line].data.debit=0;
							dsTRDetailReceivablesLedger.data.items[line].data.kredit=0;
							gridDTReceivablesLedger.getView().refresh();
						},
						insert	: function(o){
							return {
								account        	: o.account,
								name        	: o.name,
								groups        	: o.groups,
								debit        	: o.debit,
								kredit        	: o.kredit,
								text			:  '<table style="font-size: 11px;"><tr><td width="140">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
							}
						},
						param:function(){
							return {
								text:''
							}
						},
						url		: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/getAccount",
						valueField: 'account',
			 			displayField: 'text',
						listWidth: 340
					})
					
				},
				{
					id:'colNama_ReceivablesLedger',
					header:'Nama',
					dataIndex: 'name',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{
					id:'colDeskripsi_ReceivablesLedger',
					header:'Deskripsi',
					dataIndex: 'description',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200,
					editor: new fm.TextField({
					allowBlank: false
					})
					
				},
				{
					id:'colDebit_ReceivablesLedger',
					header:'Debit',
					dataIndex: 'debit',
					xtype:'numbercolumn',
					sortable: false,
					hidden:false,
					align:'right',
					menuDisabled:true,
					width:100,
					editor: new fm.NumberField({
						allowBlank: false,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsTRDetailReceivablesLedger.getRange()[line].data.debit=a.getValue();
								JumlahGridReceivablesLedger();
							},
							focus: function(a){
								this.index=gridDTReceivablesLedger.getSelectionModel().selection.cell[0]
							}
							
						}
					})
					
				},
				{
					header:'Kredit',
					dataIndex: 'kredit',
					align:'right',
					xtype:'numbercolumn',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					editor: new fm.NumberField({
						allowBlank: false,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsTRDetailReceivablesLedger.getRange()[line].data.kredit=a.getValue();
								JumlahGridReceivablesLedger();
							},
							focus: function(a){
								this.index=gridDTReceivablesLedger.getSelectionModel().selection.cell[0]
							}
							
						}
					})
					
				},
				{
					id:'colLine_ReceivablesLedger',
					header:'Line',
					dataIndex: 'line',
					sortable: false,
					hidden:true,
					menuDisabled:true,
					editor: new fm.NumberField({
						allowBlank: false,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsTRDetailReceivablesLedger.getRange()[line].data.line=a.getValue();
								JumlahGridReceivablesLedger();
							},
							focus: function(a){
								this.index=gridDTReceivablesLedger.getSelectionModel().selection.cell[0]
							}
							
						}
					})
				}
			]
			),
			viewConfig:{forceFit: true},
			tbar:
			{
				xtype: 'toolbar',
				id: 'toolbar_ReceivablesLedger',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Baris',
						iconCls: 'add',
						id: 'btnAddRowReceivablesLedger',
						handler: function(){
							var records = new Array();
							records.push(new dsTRDetailReceivablesLedger.recordType());
							dsTRDetailReceivablesLedger.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus Baris',
						iconCls: 'remove',
						id: 'btnDeleteReceivablesLedger',
						handler: function()
						{
							var line = gridDTReceivablesLedger.getSelectionModel().selection.cell[0];
							dsTRDetailReceivablesLedger.removeAt(line);
							gridDTReceivablesLedger.getView().refresh();
						}
					}

					//-------------- ## --------------
				]
			},
        }
		
    );
    return gridDTReceivablesLedger;
}

//load data piutang
function dataGrid_ReceivablesLedger(){
	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/getGridReceivablesLedger",
			params: { 
					NoJurnal	:Ext.getCmp('NoJurnal_ReceivablesLedger').getValue(),
					CustCode	:Ext.getCmp('cboCustomer_pencarian').getValue(),
					TglAwal		:Ext.getCmp('dtpTglAwalFilterReceivablesLedger').getValue(),
					TglAkhir	:Ext.getCmp('dtpTglAkhirFilterReceivablesLedger').getValue()
			},
			failure: function(o)
			{
				ShowPesanErrorReceivablesLedger('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{

				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ds_TrReceivablesLedgerList.removeAll();
					var recs=[],
						recType=ds_TrReceivablesLedgerList.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					ds_TrReceivablesLedgerList.add(recs);
					grid_ReceivablesLedger.getView().refresh();
				}
				else 
				{
					ShowPesanErrorReceivablesLedger('Gagal membaca data buku besar piutang', 'Error');
				};
			}
		}
	)
}

//combo customer di form input data
function comboCustomer()
{
	var Field = ['kd_customer','cust_code','customer','cc','term'];
    ds_Customer = new WebApp.DataStore({fields: Field});
	loadDataComboCustomer();
    cboCustomer = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 40,
			id:'cboCustomer',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:250,
			store: ds_Customer,
			valueField: 'cust_code',
			displayField: 'cc',
			value:'',
			listeners:
			{
					'select': function(a,b,c)
					{
						selectSetPilihan=b.data.displayText ;
						tmpJatuhTempo = new Date().add(Date.DAY,+b.data.term);
						Ext.getCmp('dPopupTglJatuhTempoReceivablesLedger').setValue(tmpJatuhTempo);
						tmpKdCustomer=b.data.cust_code;
					}
			}
		}
	);
	return cboCustomer;
};

//combo customer di form pencarian
function comboCustomer_pencarian()
{
	var Field = ['kd_customer','cust_code','customer','cc','term'];
    ds_Customer_pencarian = new WebApp.DataStore({fields: Field});
	loadDataComboCustomer_pencarian();
    cboCustomer_pencarian = new Ext.form.ComboBox
	(
		{
			x: 155,
			y: 40,
			id:'cboCustomer_pencarian',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:250,
			store: ds_Customer_pencarian,
			valueField: 'cust_code',
			displayField: 'cc',
			value:'',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihan=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) //ketika combo  di enter, maka akan ditampilkan data sesuai pencarian
					{
						dataGrid_ReceivablesLedger(
							Ext.get('NoJurnal_ReceivablesLedger').getValue(),
							Ext.getCmp('cboCustomer_pencarian').getValue(),
							Ext.get('dtpTglAwalFilterReceivablesLedger').getValue(),
							Ext.get('dtpTglAkhirFilterReceivablesLedger').getValue()
						);
					} 						
				}
			}
		}
	);
	return cboCustomer_pencarian;
};

//load data customer untuk combo customer di form input
function loadDataComboCustomer(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/getCustomer",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboCustomer.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_Customer.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_Customer.add(recs);
				console.log(o);
			}
		}
	});
}

//load data customer untuk combo customer di form pencarian
function loadDataComboCustomer_pencarian(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/getCustomer",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboCustomer_pencarian.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_Customer_pencarian.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_Customer_pencarian.add(recs);
				// console.log(o);
			}
		}
	});
}
// save data piutang
function dataSave_ReceivablesLedger(){
	if (ValidasiEntryReceivablesLedger(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/saveReceivablesLedger",
				params: getParamSaveReceivablesLedger(),//parameter yang akan disave
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorReceivablesLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoReceivablesLedger('Berhasil menyimpan data ini','Information');
						Ext.getCmp('TxtPopupKdJurnalReceivablesLedger').setValue(cst.no_jurnal);
						dataGrid_ReceivablesLedger();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorReceivablesLedger('Gagal menyimpan data ini', 'Error');
						dataGrid_ReceivablesLedger();
					}
				}
			}
			
		)
	}
}


function dataHapus_ReceivablesLedger(data){
	
	console.log(data);
	if(data.posted == 't' ){
		ShowPesanInfoReceivablesLedger('Transaksi telah diposting, data gagal dihapus!','Information')
	}else{
		
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/hapus_ar",
				params: {
					ar_number		: Ext.getCmp('TxtPopupKdJurnalReceivablesLedger').getValue(), 
					ar_date			: Ext.getCmp('dPopupTglJurnalReceivablesLedger').getValue(),
					referensi		: Ext.getCmp('TxtPopupReferensiReceivablesLedger').getValue()
					
				},
				failure: function(o)
				{
					ShowPesanErrorReceivablesLedger('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoReceivablesLedger('Data berhasil dihapus!','Information')
						addNewReceivablesLedger();
						dataGrid_ReceivablesLedger(
							Ext.get('NoJurnal_ReceivablesLedger').getValue(),
							Ext.getCmp('cboCustomer_pencarian').getValue(),
							Ext.get('dtpTglAwalFilterReceivablesLedger').getValue(),
							Ext.get('dtpTglAkhirFilterReceivablesLedger').getValue()
						);
					}
					else 
					{
						ShowPesanErrorReceivablesLedger('Gagal menghapus data ini', 'Error');
						
					};
				}
			}
			
		)
		
	} 
}
//menampung parameter inputan
function getParamSaveReceivablesLedger()
{
	var	params =
	{
		NoJurnal:Ext.getCmp('TxtPopupKdJurnalReceivablesLedger').getValue(),
		Customer:tmpKdCustomer,
		Type:n_type,
		Tgl:Ext.getCmp('dPopupTglJurnalReceivablesLedger').getValue(),
		Posting:tmpposting,
		Referensi:Ext.getCmp('TxtPopupReferensiReceivablesLedger').getValue(),
		Tgl_JatuhTempo:Ext.getCmp('dPopupTglJatuhTempoReceivablesLedger').getValue(),
		Catatan:Ext.getCmp('TxtPopupCatatanReceivablesLedger').getValue()
	}
	params['jml']=dsTRDetailReceivablesLedger.getCount();
	for(var i = 0 ; i <dsTRDetailReceivablesLedger.getCount();i++)
	{
		params['akun-'+i]=dsTRDetailReceivablesLedger.data.items[i].data.account;	
		params['deskripsi-'+i]=dsTRDetailReceivablesLedger.data.items[i].data.description;	
		params['line-'+i]=i+1;	
		if(dsTRDetailReceivablesLedger.data.items[i].data.debit==0){
			params['value-'+i]=dsTRDetailReceivablesLedger.data.items[i].data.kredit;
			params['isDebit-'+i]='false';
		}else{
			params['value-'+i]=dsTRDetailReceivablesLedger.data.items[i].data.debit;
			params['isDebit-'+i]='true';
		}
	}
   
    return params

}

//set data 
function DataInitReceivablesLedger(rowdata)
{
	Ext.getCmp('TxtPopupKdJurnalReceivablesLedger').setValue(rowdata.ar_number);
	Ext.getCmp('cboCustomer').setValue(rowdata.cc);
	tmpKdCustomer=rowdata.cust_code;
	
	if(rowdata.type==0){
		n_type=0;
		Ext.getCmp('rb_debitReceivablesLedger').setValue(true);
	}else{
		Ext.getCmp('rb_kreditReceivablesLedger').setValue(true);
		n_type=1;
	}
	// alert(n_type);
	
	if(rowdata.posted=='t')
	{
		Ext.getCmp('checkPostingReceivablesLedger').setValue(true);
	}else{
		Ext.getCmp('checkPostingReceivablesLedger').setValue(false);
	}
	// alert(rowdata.posted);
	dataGridReceivablesLedgerDetail(rowdata.ar_number,rowdata.ar_date,rowdata.line);
	Ext.get('dPopupTglJurnalReceivablesLedger').dom.value = ShowDate(rowdata.ar_date);
	Ext.getCmp('TxtPopupReferensiReceivablesLedger').setValue(rowdata.reference);
	Ext.get('dPopupTglJatuhTempoReceivablesLedger').dom.value = ShowDate(rowdata.due_date);
	Ext.get('TxtPopupCatatanReceivablesLedger').dom.value = rowdata.notes;
	
};
//load data detail piutang
function dataGridReceivablesLedgerDetail(kd_jurnal, tgl_jurnal,line){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/akuntansi/functionTrReceivablesLedger/getReceivablesLedger", // memanggil query 
			params: { //parameter 
						KdJurnal:kd_jurnal,
						TglJurnal:tgl_jurnal,
						Line:line
					},
			failure: function(o)
			{
				ShowPesanErrorReceivablesLedger('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsTRDetailReceivablesLedger.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetailReceivablesLedger.add(recs); //data ditampung ke datastore
					gridDTReceivablesLedger.getView().refresh(); //untuk menampilkan data ke grid
					JumlahGridReceivablesLedger();
				}
				else 
				{
					ShowPesanErrorReceivablesLedger('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}
//proses hitung jumlah debit kredit
function JumlahGridReceivablesLedger(){
	var jumlahdebit=0;
	var jumlahcredit=0;
	for(var i=0; i < dsTRDetailReceivablesLedger.getCount() ; i++){
		var o=dsTRDetailReceivablesLedger.getRange()[i].data;
		if(o.debit != undefined || o.kredit != undefined ){
			jumlahdebit += parseFloat(o.debit);
			jumlahcredit += parseFloat(o.kredit);
		}
	}
	gridDTReceivablesLedger.getView().refresh();
	Ext.getCmp('txtTotDebit_ReceivablesLedger').setValue(toFormat(jumlahdebit));
	Ext.getCmp('txtTotCredit_ReceivablesLedger').setValue(toFormat(jumlahcredit));
}

//validasi form input data piutang
function ValidasiEntryReceivablesLedger(modul,mBolHapus)
{
	var x = 1;
	if( tmpKdCustomer == '' || dsTRDetailReceivablesLedger.getCount() === 0 || dsTRDetailReceivablesLedger.getCount()  === 1 ){
		if(tmpKdCustomer === ''){
			ShowPesanWarningReceivablesLedger('Customer belum dipilih!', 'Warning');
			x = 0;
		} else if(dsTRDetailReceivablesLedger.getCount() === 1 ){
			ShowPesanWarningReceivablesLedger('Daftar akun minimal 2 baris!', 'Warning');
			x = 0;
		} 
	}
	
	
	for(var i=0; i<dsTRDetailReceivablesLedger.getCount() ; i++){
		var o=dsTRDetailReceivablesLedger.getRange()[i].data;

		if(o.account == undefined || o.account == ""){
			ShowPesanWarningReceivablesLedger('Akun masih kosong, harap isi baris kosong atau hapus untuk melanjutkan!', 'Warning');
			x = 0;
		} else{
			if(o.kredit != undefined || o.debit != undefined){
				if(o.kredit == undefined){
					ShowPesanWarningReceivablesLedger('Kredit baris ke '+ (i+1) +' masih kosong, kredit tidak boleh kosong. Mohon periksa kembali!', 'Warning');
					x = 0;
				}else if(o.debit == undefined){
					ShowPesanWarningReceivablesLedger('Debit baris ke '+ (i+1) +' masih kosong, debit tidak boleh kosong. Mohon periksa kembali!', 'Warning');
					x = 0;
				} else if(o.debit != 0 && o.kredit != 0){
					ShowPesanWarningReceivablesLedger('Mohon periksa kembali baris ke '+ (i+1) +'. Isi salah satu kolom debit atau kredit!', 'Warning');
					x = 0;
				} else if(o.debit == 0 && o.kredit == 0){
					ShowPesanWarningReceivablesLedger('Mohon periksa kembali baris ke '+ (i+1) +'. Nilai akun masih kosong!', 'Warning');
					x = 0;
				}
			}
		}
	}
	
	return x;
};

//clear
function addNewReceivablesLedger()
{
	Ext.getCmp('TxtPopupKdJurnalReceivablesLedger').setValue('');
	Ext.getCmp('cboCustomer').setValue('');
	Ext.getCmp('TxtPopupReferensiReceivablesLedger').setValue('');
	Ext.getCmp('dPopupTglJatuhTempoReceivablesLedger').setValue('');
	Ext.getCmp('TxtPopupCatatanReceivablesLedger').setValue('');
	Ext.getCmp('checkPostingReceivablesLedger').setValue(false);
	n_type=0;
	tmpposting='False';
	tmpKdCustomer='';
	Ext.getCmp('rb_debitReceivablesLedger').setValue(false);
	dsTRDetailReceivablesLedger.removeAll();
	JumlahGridReceivablesLedger();
}