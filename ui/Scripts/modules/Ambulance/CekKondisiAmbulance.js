var NamaForm_viSetupHasilTest="Cek Kondisi Ambulance";
var mod_name_viSetupHasilTest="Cek Kondisi Ambulance";
var slctCount_viDataKondisiAmbulance=10;
var now_viSetupHasilTest= new Date();
var tanggal = now_viSetupHasilTest.format("d/M/Y");
var jam = now_viSetupHasilTest.format("H/i/s");
var now = new Date();
var tmp_kd_mobil;
var tmp_kd_mobil_key;
var tmp_kd_mobil_cari;
var tmp_kd_fasilitas;
var tmp_kd_fasilitas_key;
var tmp_kd_fasilitas_cari;
var tmp_kd_kondisi;
var tmp_kd_kondisi_key;
var tmp_kd_kondisi_cari;
var tmp_tgl_cek_kondisi;
var tmp_tgl_cek_kondisi_key;
var dataSource_viCekKondisiAmbulance;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viCekKondisiAmbulance;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viCekKondisiAmbulance;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;
var dsMobilAmbulance;
var dsMobilAmbulance_cari;
var dsFasilitasAmbulance_cari;
var dsFasilitasAmbulance;
var dsKondisiAmbulance;
var dsKondisiAmbulance_cari;
var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;
var DataRefresh_viCekKondisi;
var pluginExpanded = true;
var CurrentData_viSetupHasilTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupHasilTest={};
SetupHasilTest.form={};
SetupHasilTest.func={};
SetupHasilTest.vars={};
SetupHasilTest.func.parent=SetupHasilTest;
SetupHasilTest.form.ArrayStore={};
SetupHasilTest.form.ComboBox={};
SetupHasilTest.form.DataStore={};
SetupHasilTest.form.Record={};
SetupHasilTest.form.Form={};
SetupHasilTest.form.Grid={};
SetupHasilTest.form.Panel={};
SetupHasilTest.form.TextField={};
SetupHasilTest.form.Button={};

SetupHasilTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupHasilTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_mobil','kd_fasilitas','kd_kondisi','tgl_cek_kondisi','nama_fasilitas','kondisi','keterangan','mobil'],
	data: []
});

CurrentPage.page = dataGrid_viCekKondisiAmbulance(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viCekKondisiAmbulance(mod_id_viSetupHasilTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupHasilTest = 
	[
		'kd_mobil','kd_fasilitas','kd_kondisi','tgl_cek_kondisi','nama_fasilitas','kondisi','keterangan','mobil'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viCekKondisiAmbulance = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupHasilTest
    });
	dataGridItemHasil_CekKondisiAmbulance();
	//DataRefresh_viCekKondisi();
	loadDataComboMobilAmbulance();
	loadDataComboMobilAmbulance_cari();
	loadDataComboFasilitasAmbulance();
	loadDataComboFasilitasAmbulance_cari();
	loadDataComboKondisiAmbulance();
	loadDataComboKondisiAmbulance_cari();

	var FieldItem=['kd_mobil', 'item_hd'];
	dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viCekKondisiAmbulance = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viCekKondisiAmbulance,
			autoScroll: true,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			/*height:570,*/
			height: 480,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viCekKondisiAmbulance = undefined;
							rowSelected_viCekKondisiAmbulance = dataSource_viCekKondisiAmbulance.getAt(row);
							CurrentData_viSetupHasilTest
							CurrentData_viSetupHasilTest.row = row;
							CurrentData_viSetupHasilTest.data = rowSelected_viCekKondisiAmbulance.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viCekKondisiAmbulance = dataSource_viCekKondisiAmbulance.getAt(ridx);
					if (rowSelected_viCekKondisiAmbulance != undefined)
					{
						DataInitCekKondisiAmbulance(rowSelected_viCekKondisiAmbulance.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viKodeMerkAmbulance',
						header: 'Kode',
						dataIndex: 'kd_mobil', //harus sama dengan nama kolom didatabase
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 10
						
					},
					{
						id: 'colItem_viMerk',
						header: 'Mobil',
						dataIndex: 'mobil',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					
					
					{
						id: 'colItem_viMerk',
						header: 'Fasilitas',
						dataIndex: 'nama_fasilitas',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'colItem_viMerk',
						header: 'Kondisi',
						dataIndex: 'kondisi',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'colItem_viMerk',
						header: 'Tgl Kondisi',
						dataIndex: 'tgl_cek_kondisi',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'colItem_viMerk',
						header: 'Keterangan',
						dataIndex: 'keterangan',
						hideable:false,
						menuDisabled: true,
						width: 20
					},	
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMerkAmbulance',
				items: 
				[
					/*{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupDokterAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viCekKondisiAmbulance != undefined)
							{
								DataInitCekKondisiAmbulance(rowSelected_viCekKondisiAmbulance.data);
							}
							
						}
					}*/
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
		//	bbar : bbar_paging(mod_name_viSetupHasilTest, slctCount_viDataKondisiAmbulance, dataSource_viCekKondisiAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}

			// bbar: Ext.create('Ext.PagingToolbar', {
			// 					store: dataSource_viCekKondisiAmbulance,
			// 					displayInfo: true,
			// 					// tampilan halaman paging
			// 					displayMsg: 'Menampilkan topik {0} - {1} dari {2}',
			// 					// tampilan bila tidak ada data yang bisa ditampilkan
			// 					emptyMsg: "Tidak ada topik yang ditampilkan",
			// 					// menambahkan 1 tombol untuk
			// 					items:[
			// 					'-', {
			// 					text: 'Tampilkan Preview',
			// 					//pluginExpanded adalah variabel yang diset sebelumnya di atas dan nilainya true
			// 					pressed: pluginExpanded,
			// 					enableToggle: true,
			// 					toggleHandler: function(btn, pressed) {
			// 					var preview = Ext.getCmp('gv').getPlugin('preview');
			// 					preview.toggleExpanded(pressed);
			// 					}
			// 					}]
			// 					})
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GridDataView_viSetupReferensiHasil = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupReferensiHasil,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:250,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupReferensiHasil = undefined;
							rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(row);
							CurrentData_viSetupReferensiHasil
							CurrentData_viSetupReferensiHasil.row = row;
							CurrentData_viSetupReferensiHasil.data = rowSelected_viSetupReferensiHasil.data;
							//DataInitCekKondisiAmbulance(rowSelected_viCekKondisiAmbulance.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(ridx);
					if (rowSelected_viSetupReferensiHasil != undefined)
					{
						DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
						//setLookUp_viSetupHasilTest(rowSelected_viCekKondisiAmbulance.data);
					}
					else
					{
						//setLookUp_viSetupHasilTest();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNo_ref_viSetupReferensiHasil',
						header: 'No. Ref',
						dataIndex: 'no_ref',
						hideable:false,
						menuDisabled: true,
						width: 10 
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupReferensiHasil',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colDeskripsi_viSetupReferensiHasil',
						header: 'Deskripsi',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupReferensiHasil',
				items: 
				[
					/*{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupReferensiHasil',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupReferensiHasil != undefined)
							{
								DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
							}
							
						}
					}*/
					//-------------- ## --------------
				]
			},		
		}
	)	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
						
					
		}
	)
	
	var PanelInputSetupSupirAmbulance = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: true,  
        //title: 'tes',
        bodyStyle: 'padding:10px 10px 10px 10px',
        items: [GetFormSetupItemHasil()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupHasil',
						handler: function(){
							AddNewSetupMerkAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupHasil',
						handler: function()
						{
							loadMask.show();
							dataSave_viCekKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupHasil',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viCekKondisiAmbulance();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupHasil',
						handler: function()
						{
							dataSource_viCekKondisiAmbulance.removeAll();
							dataGridItemHasil_CekKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Cek',
						iconCls: 'refresh',
						id: 'btnCek_KondisiAmbulance',
						handler: function()
						{
							loadMask.show();
							cek_kodisi_ambulance();
						}
					}
				]
			}
	})
	var FormCekKondisi = new Ext.Panel
	(
		{
			id: 'mod_id_panel_grid_cari_Cek_Kondisi',
			closable:true,
			region: 'center',
			layout: 'form',
			//title: 'Data Supir Ambulance',
			//margins:'0 5 5 0',
			//bodyStyle: 'padding:15px',
			border: true,
			style: "top: auto; bottom: 0",
			//autoHeight: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'Pagu',
			items: [GridDataView_viCekKondisiAmbulance],
			tbar: 
			[
				'Mobil','',
				mComboMobilAmbulance_cari(),'',
				'Fasilitas',
				mComboFasilitasAmbulance_cari(),'',
				'Kondisi','',
				mComboKondisiAmbulance_cari(),'','','',
				{
					xtype: 'button',
					id:'btnRefreshFasilitasFilter',
					tooltip: 'Tampilkan',
					iconCls: 'find',
					align :'right',
					text: 'Tampilkan',
					handler: function(sm, row, rec) {
						RefreshDataCekKondisiFilter();
					}

				}
			]
		}
	);
	
	//LAYAR FORM UTAMA 
	var FrmData_viSetupHasilTest = new Ext.Panel
    (
		{
			title: 'Cek Kondisi Ambulance',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupHasilTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
			title: '',
			height: 750,
			items:[
				PanelInputSetupSupirAmbulance,
				//GridDataView_viCekKondisiAmbulance
				FormCekKondisi
			]
		}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupHasilTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupItemHasil(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Mobil'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							mComboMobilAmbulance(),
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Fasilitas'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							mComboFasilitasAmbulance(),

							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},

							{	
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtKeterangan_cekKondisiAmbulance',
								name: 'txtKeterangan_cekKondisiAmbulance',
								width: 600,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},

							{
								x:350,
								y: 0,
								xtype: 'label',
								text: 'Tgl Kondisi'
							},
							{
								x: 430,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
                                xtype: 'datefield',
                                x: 450,
								y: 0,
                                id: 'tgl_kondisi_ambulance',
                                name: 'tgl_kondisi_ambulance',
                                format: 'd/M/Y',
                                readOnly: false,
                                value: now,
                                width:200
                               
                            },
							{
								x:350,
								y:30,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 430,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							mComboKondisiAmbulance(),
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 130,
        y: 60,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};


function dataGridItemHasil_CekKondisiAmbulance(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getGridCekKondisiAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viCekKondisiAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viCekKondisiAmbulance.add(recs);
				//	GridDataView_viCekKondisiAmbulance.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data kondisi ambulance', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupMerkAmbulance(){
	Ext.getCmp('cboMobilkAmbulance').setValue('');
	Ext.getCmp('cboFasilitasAmbulance').setValue('');
	Ext.getCmp('cboKondisiAmbulance').setValue('');
	Ext.getCmp('txtKeterangan_cekKondisiAmbulance').setValue('');
};

function dataSave_viCekKondisiAmbulance(){
	if (ValidasiSaveCekKondisiAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/save",
				params: getParamSaveCekKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information')
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
						//loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					}
				}
			}
			
		)
	}
}


function cek_kodisi_ambulance(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/cek_kondisi",
				params: getParamSaveCekKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil Melakukan Cek Kondisi Ambulance','Information')
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					//	loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal Melakukan Cek Kondisi Ambulance', 'Error');
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					}
				}
			}
			
		)
	
}
function getParamSaveCekKondisiAmbulance(){
	var	params =
	{
		kd_mobil_tmp 		:tmp_kd_mobil_key,
		kd_fasilitas_tmp 	:tmp_kd_fasilitas_key,
		tgl_cek_kondisi_tmp :tmp_tgl_cek_kondisi_key,
		kd_kondisi_tmp 		:tmp_kd_kondisi_key,
		keterangan 			:Ext.getCmp('txtKeterangan_cekKondisiAmbulance').getValue(),

		kd_mobil 			:Ext.getCmp('cboMobilkAmbulance').getValue(),
		kd_fasilitas 		:Ext.getCmp('cboFasilitasAmbulance').getValue(),
		kd_kondisi 			:Ext.getCmp('cboKondisiAmbulance').getValue(),
		tgl_cek_kondisi 	:Ext.getCmp('tgl_kondisi_ambulance').getValue(),	
	}
	console.log(params);
    return params
};

function ValidasiSaveCekKondisiAmbulance(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('cboMobilkAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Mobil belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboFasilitasAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Fasilitas belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('tgl_kondisi_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Tanggal belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboKondisiAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Kondisi belum dipilih', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningCekKondisiAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupHasil(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viCekKondisiAmbulance(){
	if (ValidasiSaveCekKondisiAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/delete",
				params: getParamDeleteCekKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupMerkAmbulance()
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamDeleteCekKondisiAmbulance(){
	var	params ={
		kd_mobil 		:tmp_kd_mobil_key,
		kd_fasilitas 	:tmp_kd_fasilitas_key,
		kd_kondisi 		:tmp_kd_kondisi_key,
		tgl_cek_kondisi	:tmp_tgl_cek_kondisi_key
	}
   
    return params
};

function DataInitCekKondisiAmbulance(rowdata){
	console.log(rowdata);
	Ext.getCmp('cboMobilkAmbulance').setValue(rowdata.kd_mobil);
	Ext.getCmp('cboFasilitasAmbulance').setValue(rowdata.kd_fasilitas);
	Ext.getCmp('tgl_kondisi_ambulance').setValue(rowdata.tgl_cek_kondisi);
	Ext.getCmp('cboKondisiAmbulance').setValue(rowdata.kd_kondisi);
	Ext.getCmp('txtKeterangan_cekKondisiAmbulance').setValue(rowdata.keterangan);
	tmp_kd_mobil_key    =rowdata.kd_mobil;
	tmp_kd_fasilitas_key=rowdata.kd_fasilitas;
	tmp_kd_kondisi_key  =rowdata.kd_kondisi;
	tmp_tgl_cek_kondisi_key  =rowdata.tgl_cek_kondisi;
	console.log(tmp_kd_mobil);
	console.log(tmp_kd_fasilitas);
	console.log(tmp_kd_kondisi);
	console.log(tmp_tgl_cek_kondisi);
};

function loadDataComboItemHasil(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasil",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboItemHasil.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_ItemHasil.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_ItemHasil.add(recs);
				console.log(o);
			}
		}
	});
}



function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningCekKondisiAmbulance('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};


function mComboMobilAmbulance(){
	
      var Field = ['kd_mobil','mobil'];
      dsMobilAmbulance = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 0,
		    id: 'cboMobilkAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsMobilAmbulance,
		    valueField: 'kd_mobil',
		    displayField: 'mobil',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_mobil = b.data.kd_mobil;
			},				                       
			}
                }
	);
    return cboMerkAmbulance;
};

function loadDataComboMobilAmbulance(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboMobil",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsMobilAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsMobilAmbulance.add(recs);
			}
				console.log(dsMobilAmbulance);
		}
	});
}


function mComboFasilitasAmbulance(){
	
      var Field = ['kd_fasilitas','nama_fasilitas'];
      dsFasilitasAmbulance = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 30,
		    id: 'cboFasilitasAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsFasilitasAmbulance,
		    valueField: 'kd_fasilitas',
		    displayField: 'nama_fasilitas',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_fasilitas = b.data.kd_fasilitas;
			},
                }
	});
    return cboMerkAmbulance;
};

function loadDataComboFasilitasAmbulance(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboFasilitas",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsFasilitasAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsFasilitasAmbulance.add(recs);
			}
				console.log(dsFasilitasAmbulance);
		}
	});
}


function mComboKondisiAmbulance(){
	
      var Field = ['kd_kondisi','kondisi'];
      dsKondisiAmbulance = new WebApp.DataStore({fields: Field});
      
       cboKondisikAmbulance = new Ext.form.ComboBox
	(
		{
			x: 450,
			y: 30,
		    id: 'cboKondisiAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsKondisiAmbulance,
		    valueField: 'kd_kondisi',
		    displayField: 'kondisi',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_kondisi = b.data.kd_kondisi;				                       
			},
                }
	});
    return cboKondisikAmbulance;
};

function loadDataComboKondisiAmbulance(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboKondisi",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsKondisiAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKondisiAmbulance.add(recs);
			}
				console.log(dsKondisiAmbulance);
		}
	});
}

function DataRefresh_viCekKondisi(criteria){
    dataSource_viCekKondisiAmbulance.load({
		params:{
			Skip: 0,
			Take: slctCount_viDataKondisiAmbulance,
			Sort: 'a.kd_mobil',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataCekKondisiAmbulance',
			param: criteria

		}
	});
    return dataSource_viCekKondisiAmbulance;
}


function mComboMobilAmbulance_cari(){
      var Field = ['kd_mobil','mobil'];
      dsMobilAmbulance_cari = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance_cari = new Ext.form.ComboBox
	(
		{
			
		    id: 'cboMobilkAmbulance_cari',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsMobilAmbulance_cari,
		    valueField: 'kd_mobil',
		    displayField: 'mobil',
		    width:170,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_mobil_cari = b.data.kd_mobil;
					RefreshDataCekKondisiFilter();	
			},  'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13){
	    					RefreshDataCekKondisiFilter();	
							} 						
						},
				blur: function(a){
			    }				                       
			}
                }
	);
    return cboMerkAmbulance_cari;
};

function loadDataComboMobilAmbulance_cari(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboMobil",
		 params: {flag:1},
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				var cst = Ext.decode(o.responseText);

				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType =  dsMobilAmbulance_cari.recordType;
					var o=cst['listData'][i];
			
					recs.push(new recType(o));
					dsMobilAmbulance_cari.add(recs);
				}
					console.log(dsMobilAmbulance_cari);
			}
	});
}


function mComboFasilitasAmbulance_cari(){
	var Field = ['kd_fasilitas','nama_fasilitas'];
      dsFasilitasAmbulance_cari = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance_cari = new Ext.form.ComboBox
	(
		{
		    id: 'cboFasilitasAmbulance_cari',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsFasilitasAmbulance_cari,
		    valueField: 'kd_fasilitas',
		    displayField: 'nama_fasilitas',
		    width:170,
			tabIndex:3,
			 listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_fasilitas_cari = b.data.kd_fasilitas;
					RefreshDataCekKondisiFilter();	
			},  'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13){
	    					RefreshDataCekKondisiFilter();	
							} 						
						},
				blur: function(a){
			    }				                       
			}
	});
    return cboMerkAmbulance_cari;
};

function loadDataComboFasilitasAmbulance_cari(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboFasilitas",
		 params: {flag:1},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsFasilitasAmbulance_cari.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsFasilitasAmbulance_cari.add(recs);
			}
				console.log(dsFasilitasAmbulance_cari);
		}
	});
}

function mComboKondisiAmbulance_cari(){
	
      var Field = ['kd_kondisi','kondisi'];
      dsKondisiAmbulance_cari = new WebApp.DataStore({fields: Field});
      
       cboKondisikAmbulance_cari = new Ext.form.ComboBox
	(
		{
		    id: 'cboKondisiAmbulance_cari',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsKondisiAmbulance_cari,
		    valueField: 'kd_kondisi',
		    displayField: 'kondisi',
		    width:170,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_kondisi_cari = b.data.kd_kondisi;
					RefreshDataCekKondisiFilter();	
			},  'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13){
	    					RefreshDataCekKondisiFilter();	
							} 						
						},
				blur: function(a){
			    }				                       
			}
	});
    return cboKondisikAmbulance_cari;
};

function loadDataComboKondisiAmbulance_cari(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboKondisi",
		 params: {flag:1},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsKondisiAmbulance_cari.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKondisiAmbulance_cari.add(recs);
			}
				console.log(dsKondisiAmbulance_cari);
		}
	});
}
function RefreshDataCekKondisiFilter(){
	Ext.Ajax.request({
			url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getGridCekKondisiAmbulance_cari",
			params: {mobil:tmp_kd_mobil_cari,
			         fasilitas:tmp_kd_fasilitas_cari,
			         kondisi:tmp_kd_kondisi_cari},
			failure: function(o){
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dataSource_viCekKondisiAmbulance.removeAll();
					var recs=[],
						recType=dataSource_viCekKondisiAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viCekKondisiAmbulance.add(recs);
					GridDataView_viCekKondisiAmbulance.getView().refresh();
				}
				else{
					ShowPesanErrorSetupHasil('Gagal membaca data kondisi ambulance', 'Error');
				}
			}
		}
		
	)
}