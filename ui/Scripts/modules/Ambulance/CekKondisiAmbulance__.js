var NamaForm_viSetupHasilTest="Cek Kondisi Ambulance";
var mod_name_viSetupHasilTest="Cek Kondisi Ambulance";
var slctCount_viDataKondisiAmbulance=10;
var now_viSetupHasilTest= new Date();
var tanggal = now_viSetupHasilTest.format("d/M/Y");
var jam = now_viSetupHasilTest.format("H/i/s");
var now = new Date();
var tmp_kd_mobil;
var tmp_kd_fasilitas;
var tmp_kd_kondisi;
var dataSource_viCekKondisiAmbulance;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupHasilTest;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viCekKondisiAmbulance;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;
var dsMobilAmbulance;
var dsFasilitasAmbulance;
var dsKondisiAmbulance;
var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;
var DataRefresh_viCekKondisi;

var CurrentData_viSetupHasilTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupHasilTest={};
SetupHasilTest.form={};
SetupHasilTest.func={};
SetupHasilTest.vars={};
SetupHasilTest.func.parent=SetupHasilTest;
SetupHasilTest.form.ArrayStore={};
SetupHasilTest.form.ComboBox={};
SetupHasilTest.form.DataStore={};
SetupHasilTest.form.Record={};
SetupHasilTest.form.Form={};
SetupHasilTest.form.Grid={};
SetupHasilTest.form.Panel={};
SetupHasilTest.form.TextField={};
SetupHasilTest.form.Button={};

SetupHasilTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupHasilTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_mobil','kd_fasilitas','kd_kondisi','tgl_cek_kondisi','nama_fasilitas','kondisi','keterangan','mobil'],
	data: []
});

CurrentPage.page = dataGrid_viSetupHasilTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupHasilTest(mod_id_viSetupHasilTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupHasilTest = 
	[
		'kd_mobil','kd_fasilitas','kd_kondisi','tgl_cek_kondisi','nama_fasilitas','kondisi','keterangan','mobil'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viCekKondisiAmbulance = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupHasilTest
    });
	//dataGridItemHasil_CekKondisiAmbulance();
	DataRefresh_viCekKondisi();
	loadDataComboMobilAmbulance();
	loadDataComboFasilitasAmbulance();
	loadDataComboKondisiAmbulance();

	var FieldItem=['kd_mobil', 'item_hd'];
	dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viCekKondisiAmbulance = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viCekKondisiAmbulance,
			autoScroll: false,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			height:570,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupHasilTest = undefined;
							rowSelected_viSetupHasilTest = dataSource_viCekKondisiAmbulance.getAt(row);
							CurrentData_viSetupHasilTest
							CurrentData_viSetupHasilTest.row = row;
							CurrentData_viSetupHasilTest.data = rowSelected_viSetupHasilTest.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupHasilTest = dataSource_viCekKondisiAmbulance.getAt(ridx);
					if (rowSelected_viSetupHasilTest != undefined)
					{
						DataInitCekKondisiAmbulance(rowSelected_viSetupHasilTest.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viKodeMerkAmbulance',
						header: 'Kode',
						dataIndex: 'kd_mobil', //harus sama dengan nama kolom didatabase
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 10
						
					},
					{
						id: 'colItem_viMerk',
						header: 'No Polisi',
						dataIndex: 'mobil',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					
					
					{
						id: 'colItem_viMerk',
						header: 'Fasilitas',
						dataIndex: 'nama_fasilitas',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'colItem_viMerk',
						header: 'kondisi',
						dataIndex: 'kondisi',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'colItem_viMerk',
						header: 'Tgl Kondisi',
						dataIndex: 'tgl_cek_kondisi',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'colItem_viMerk',
						header: 'Keterangan',
						dataIndex: 'keterangan',
						hideable:false,
						menuDisabled: true,
						width: 20
					},	
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMerkAmbulance',
				items: 
				[
					/*{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupDokterAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupHasilTest != undefined)
							{
								DataInitCekKondisiAmbulance(rowSelected_viSetupHasilTest.data);
							}
							
						}
					}*/
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viSetupHasilTest, slctCount_viDataKondisiAmbulance, dataSource_viCekKondisiAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GFPenerimaan.form.Grid.b = Q().table({
		flex: 1,
		rowdblclick: function (sm, ridx, cidx,store){
			rowSelected_viApotekPenerimaan = store.getAt(ridx);
			if (rowSelected_viApotekPenerimaan != undefined){
				setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data);
			}else{
				setLookUp_viApotekPenerimaan();
			}
		},
		rowselect: function(sm, row, rec,store){
			rowSelected_viApotekPenerimaan = undefined;
			rowSelected_viApotekPenerimaan = store.getAt(row);
			CurrentData_viApotekPenerimaan
			CurrentData_viApotekPenerimaan.row = row;
			CurrentData_viApotekPenerimaan.data = rowSelected_viApotekPenerimaan.data;
			if (rowSelected_viApotekPenerimaan.data.posting==='0')
			{
				Ext.getCmp('btnHapusTrx_viApotekPenerimaanGF').enable();
			}
			else
			{
				Ext.getCmp('btnHapusTrx_viApotekPenerimaanGF').disable();
			}
			GFStatusPosting=rowSelected_viApotekPenerimaan.data.posting;
		},
		tbar:{
			xtype: 'toolbar',
			items: [
				{
					xtype: 'button',
					text: 'Tambah [F1]',
					iconCls: 'add',
					tooltip: 'Edit Data',
					id:'btnTambahPenerimaanGFPenerimaan',
					handler: function(sm, row, rec){
						GFStatusPosting='0';
						setLookUp_viApotekPenerimaan();
					}
				},
				{
					xtype: 'button',
					text: 'Ubah',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						if(rowSelected_viApotekPenerimaan != undefined){
							setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data)
						}else{
							Ext.Msg.alert('Information','Data Belum Dipilih.');
						}
					}
				},
				{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekPenerimaanGF',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viApotekPenerimaan.data;
							if (datanya===undefined){
								ShowPesanWarningPenerimaanGF('Belum ada data yang dipilih','Gudang Farmasi');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_penerimaan = combo;
															if (variablebatalhistori_penerimaan != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxPenerimaanGF",
																		 params: {
																			
																			no_obat_in: datanya.no_obat_in,
																			tgl_obat_in: datanya.tgl_obat_in,
																			alasan: variablebatalhistori_penerimaan
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanWarningPenerimaanGF('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				Q(GFPenerimaan.form.Grid.b).refresh();
																				ShowPesanInfoPenerimaanGF('Data transaksi Berhasil dihapus','Gudang Farmasi');
																				Ext.getCmp('btnHapusTrx_viApotekPenerimaanGF').disable();
																			}
																			else
																			{
																				ShowPesanErrorPenerimaanGF('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningPenerimaanGF('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},
			]
		},
		ajax:function(data,callback){
			loadMask.show();
			var a=$('#'+GFPenerimaan.form.FormPanel.b.id).find('form').eq(0).serialize();
			a+='&start='+data.start+'&size='+data.size;
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initList",
				data:a,
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						callback(r.listData,r.total);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
//			loadMask.show();
//			var a=[];
//			a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
//			a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
//			a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
//			a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBFFilter').getValue()});
//			a.push({name: 'start',value:data.start});
//			a.push({name: 'size',value:data.size});
//			$.ajax({
//				type: 'POST',
//				dataType:'JSON',
//				url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initList",
//				data:a,
//				success: function(r){
//					loadMask.hide();
//					if(r.processResult=='SUCCESS'){
//						callback(r.listData,r.total);
//					}else{
//						Ext.Msg.alert('Gagal',r.processMessage);
//					}
//				},
//				error: function(jqXHR, exception) {
//					loadMask.hide();
//					Nci.ajax.ErrorMessage(jqXHR, exception);
//				}
//			});
		},
		column:new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 15,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'posting',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header: 'No. Penerimaan',
				dataIndex: 'no_obat_in',
				sortable: true,
				menuDisabled: true,
				width: 35
					
			},{
				//xtype:'datecolumn',
				header:'Tgl Penerimaan',
				dataIndex: 'tgl_obat_in',	
				width: 20,
				sortable: true,
				hideable:false,
                menuDisabled:true,
                renderer: function(v, params, record){
					return ShowDate(record.data.tgl_obat_in);
				}
			},{
				header: 'PBF',
				dataIndex: 'vendor',
				sortable: true,
				hideable:false,
                menuDisabled:true,
				width: 50
			},{
				header: 'No Faktur',
				dataIndex: 'remark',
				sortable: true,
				menuDisabled:true,
				width: 40
			},{
				header: 'Kepemilikan',
				dataIndex: 'milik',
				sortable: true,
				menuDisabled:true,
				width: 40
			}
		])
	});	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
								
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKd_Item_viSetupItemDialisa',
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 5
						
					},
					//-------------- ## --------------
					{
						id		: 'col_Item_viSetupItemDialisa',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 30,
						editor:new Nci.form.Combobox.autoComplete({
							
							store	: SetupHasilTest.form.ArrayStore.b,
							select	: function(a,b,c){
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemDialisa.data.items[line].data.kd_item=b.data.kd_item;
								dataSource_viSetupItemDialisa.data.items[line].data.item_hd=b.data.item_hd;
								GridDataView_viSetupItemDialisa.getView().refresh();
							},
							insert	: function(o){
								return {
									kd_item        	: o.kd_item,
									item_hd 		: o.item_hd,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_item+'</td><td width="150">'+o.item_hd+'</td></tr></table>'
								}
							},
							url		: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasilTest",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemDialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowItemDialisa',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupItemDialisa.recordType());
							dataSource_viSetupItemDialisa.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanItemDialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_ItemDialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Row',
						iconCls: 'remove',
						id: 'btnDeleteItemDialisa',
						handler: function()
						{
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
							dataSource_viSetupItemDialisa.removeAt(line);
							GridDataView_viSetupItemDialisa.getView().refresh();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshItemDialisa',
						handler: function()
						{
							dataSource_viSetupItemDialisa.removeAll();
							dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
							Ext.getCmp('btnAddRowItemDialisa').disable();
							Ext.getCmp('btnSimpanItemDialisa').disable();
							Ext.getCmp('btnDeleteItemDialisa').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viSetupHasilTest, slctCount_viDataKondisiAmbulance, dataSource_viCekKondisiAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelInputSetupHasil = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupItemHasil()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupHasil',
						handler: function(){
							AddNewSetupMerkAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupHasil',
						handler: function()
						{
							loadMask.show();
							dataSave_viCekKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupHasil',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupHasil();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupHasil',
						handler: function()
						{
							dataSource_viCekKondisiAmbulance.removeAll();
							dataGridItemHasil_CekKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Cek',
						iconCls: 'refresh',
						id: 'btnCek_KondisiAmbulance',
						handler: function()
						{
							loadMask.show();
							cek_kodisi_ambulance();
						}
					}
				]
			}
	})
	
	
	
	
	
	/*var FrmHasilTest= new Ext.Panel(
	{
        layout: {
        type: 'accordion',
        titleCollapse: true,
        multi: true,
        fill: false,
        animate: false, 
		id: 'accordionNavigationContainerHemodialisa',
        flex: 1
        },

        
		width: 1000,
		height: 900,
		defaults: {
		},
		layoutConfig: {
			titleCollapse: false,
			animate: true,
			activeOnTop: false
		},
		items: [{
			title: 'Dokter Ambulance',
			height: 750,
			items:[
				PanelInputSetupHasil,
				GridDataView_viCekKondisiAmbulance
			]
		}]
	}
	);*/
	
	//LAYAR FORM UTAMA 
	var FrmData_viSetupHasilTest = new Ext.Panel
    (
		{
			title: 'Cek Kondisi Ambulance',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupHasilTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
			title: '',
			height: 750,
			items:[
				PanelInputSetupHasil,
				//GridDataView_viCekKondisiAmbulance
				GFPenerimaan.form.Grid.b
			]
		}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupHasilTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupItemHasil(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Mobil'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							mComboMobilAmbulance(),
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Fasilitas'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							mComboFasilitasAmbulance(),

							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},

							{	
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtKeterangan_cekKondisiAmbulance',
								name: 'txtKeterangan_cekKondisiAmbulance',
								width: 600,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},

							{
								x:350,
								y: 0,
								xtype: 'label',
								text: 'Tgl Kondisi'
							},
							{
								x: 430,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
                                xtype: 'datefield',
                                x: 450,
								y: 0,
                                id: 'tgl_kondisi_ambulance',
                                name: 'tgl_kondisi_ambulance',
                                format: 'd/M/Y',
                                readOnly: false,
                                value: now,
                                width:200
                               
                            },
							{
								x:350,
								y:30,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 430,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							mComboKondisiAmbulance(),
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 130,
        y: 60,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};


function dataGridItemHasil_CekKondisiAmbulance(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getGridCekKondisiAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viCekKondisiAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viCekKondisiAmbulance.add(recs);
				//	GridDataView_viCekKondisiAmbulance.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data kondisi ambulance', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupMerkAmbulance(){
	Ext.getCmp('txtKdMerkAmbulance').setValue('');
	Ext.getCmp('txtnama_Merk_ambulance').setValue('');
};

function dataSave_viCekKondisiAmbulance(){
	if (ValidasiSaveCekKondisiAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/save",
				params: getParamSaveCekKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information')
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
						loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					}
				}
			}
			
		)
	}
}


function cek_kodisi_ambulance(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/cek_kondisi",
				params: getParamSaveCekKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil Melakukan Cek Kondisi Ambulance','Information')
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
						loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal Melakukan Cek Kondisi Ambulance', 'Error');
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					}
				}
			}
			
		)
	
}
function getParamSaveCekKondisiAmbulance(){
	var	params =
	{
		kd_mobil 		:tmp_kd_mobil,
		kd_fasilitas 	:tmp_kd_fasilitas,
		tgl_cek_kondisi :Ext.getCmp('tgl_kondisi_ambulance').getValue(),
		kd_kondisi 		:tmp_kd_kondisi,
		keterangan 		:Ext.getCmp('txtKeterangan_cekKondisiAmbulance').getValue(),	
	}
    return params
};

function ValidasiSaveCekKondisiAmbulance(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('cboMobilkAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Mobil belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboFasilitasAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Fasilitas belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('tgl_kondisi_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Tanggal belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboKondisiAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Kondisi belum dipilih', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningCekKondisiAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupHasil(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viSetupHasil(){
	if (ValidasiSaveCekKondisiAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/delete",
				params: getParamDeleteSetupMerkAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupMerkAmbulance()
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viCekKondisiAmbulance.removeAll();
						dataGridItemHasil_CekKondisiAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupMerkAmbulance(){
	var	params =
	{
		kd_milik:Ext.getCmp('txtKdMerkAmbulance').getValue()
	}
   
    return params
};

function DataInitCekKondisiAmbulance(rowdata){
	Ext.getCmp('cboMobilkAmbulance').setValue(rowdata.kd_mobil);
	Ext.getCmp('cboFasilitasAmbulance').setValue(rowdata.nama_fasilitas);
	Ext.getCmp('tgl_kondisi_ambulance').setValue(rowdata.tgl_cek_kondisi);
	Ext.getCmp('cboKondisiAmbulance').setValue(rowdata.kondisi);
	Ext.getCmp('txtKeterangan_cekKondisiAmbulance').setValue(rowdata.keterangan);
	tmp_kd_mobil    =rowdata.kd_mobil;
	tmp_kd_fasilitas=rowdata.kd_fasilitas;
	tmp_kd_kondisi  =rowdata.kd_kondisi;
	console.log(tmp_kd_mobil);
	console.log(tmp_kd_fasilitas);
	console.log(tmp_kd_kondisi);
};

function loadDataComboItemHasil(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasil",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboItemHasil.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_ItemHasil.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_ItemHasil.add(recs);
				console.log(o);
			}
		}
	});
}



function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningCekKondisiAmbulance('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningCekKondisiAmbulance('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};


function mComboMobilAmbulance(){
	
      var Field = ['kd_mobil','mobil'];
      dsMobilAmbulance = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 0,
		    id: 'cboMobilkAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsMobilAmbulance,
		    valueField: 'kd_mobil',
		    displayField: 'mobil',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_mobil = b.data.kd_mobil;
			},				                       
			}
                }
	);
    return cboMerkAmbulance;
};

function loadDataComboMobilAmbulance(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboMobil",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsMobilAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsMobilAmbulance.add(recs);
			}
				console.log(dsMobilAmbulance);
		}
	});
}


function mComboFasilitasAmbulance(){
	
      var Field = ['kd_fasilitas','nama_fasilitas'];
      dsFasilitasAmbulance = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 30,
		    id: 'cboFasilitasAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsFasilitasAmbulance,
		    valueField: 'kd_fasilitas',
		    displayField: 'nama_fasilitas',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_fasilitas = b.data.kd_fasilitas;
			},
                }
	});
    return cboMerkAmbulance;
};

function loadDataComboFasilitasAmbulance(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboFasilitas",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsFasilitasAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsFasilitasAmbulance.add(recs);
			}
				console.log(dsFasilitasAmbulance);
		}
	});
}


function mComboKondisiAmbulance(){
	
      var Field = ['kd_kondisi','kondisi'];
      dsKondisiAmbulance = new WebApp.DataStore({fields: Field});
      
       cboKondisikAmbulance = new Ext.form.ComboBox
	(
		{
			x: 450,
			y: 30,
		    id: 'cboKondisiAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsKondisiAmbulance,
		    valueField: 'kd_kondisi',
		    displayField: 'kondisi',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function (a, b, c){
					tmp_kd_kondisi = b.data.kd_kondisi;				                       
			},
                }
	});
    return cboKondisikAmbulance;
};

function loadDataComboKondisiAmbulance(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionCekKondisiAmbulance/getComboKondisi",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsKondisiAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKondisiAmbulance.add(recs);
			}
				console.log(dsKondisiAmbulance);
		}
	});
}

function DataRefresh_viCekKondisi(criteria){
    dataSource_viCekKondisiAmbulance.load({
		params:{
			Skip: 0,
			Take: slctCount_viDataKondisiAmbulance,
			Sort: 'a.kd_mobil',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataKondisiAmbulance',
			param: criteria

		}
	});
    return dataSource_viCekKondisiAmbulance;
}

