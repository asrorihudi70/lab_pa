var NamaForm_viSetupKondisiAmbulanceTest="Setup Kondisi Ambulance";
var mod_name_viSetupKondisiAmbulanceTest="Setup Kondisi Ambulance";
var selectCount_viSetupKondisiAmbulance=0;
var now_viSetupKondisiAmbulanceTest= new Date();
var tanggal = now_viSetupKondisiAmbulanceTest.format("d/M/Y");
var jam = now_viSetupKondisiAmbulanceTest.format("H/i/s");

var dataSource_viSetupKondisiAmbulance;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupKondisiAmbulanceTest;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viSetupKondisiAmbulanceTest;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;

var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;

var CurrentData_viSetupKondisiAmbulanceTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};

var dataGridKondisiAmbulanceRefresh;
var SetupKondisiAmbulanceTest={};
SetupKondisiAmbulanceTest.form={};
SetupKondisiAmbulanceTest.func={};
SetupKondisiAmbulanceTest.vars={};
SetupKondisiAmbulanceTest.func.parent=SetupKondisiAmbulanceTest;
SetupKondisiAmbulanceTest.form.ArrayStore={};
SetupKondisiAmbulanceTest.form.ComboBox={};
SetupKondisiAmbulanceTest.form.DataStore={};
SetupKondisiAmbulanceTest.form.Record={};
SetupKondisiAmbulanceTest.form.Form={};
SetupKondisiAmbulanceTest.form.Grid={};
SetupKondisiAmbulanceTest.form.Panel={};
SetupKondisiAmbulanceTest.form.TextField={};
SetupKondisiAmbulanceTest.form.Button={};

SetupKondisiAmbulanceTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupKondisiAmbulanceTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd'],
	data: []
});

CurrentPage.page = dataGrid_viSetupKondisiAmbulanceTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupKondisiAmbulanceTest(mod_id_viSetupKondisiAmbulanceTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupKondisiAmbulance = 
	[
		'kd_kondisi','kondisi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupKondisiAmbulance = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupKondisiAmbulance
    });
	dataGridSetupKondisiAmbulance();
//	dataGridRefHasil();
	//dataGridHasilDialisa(0);
	//loadDataComboItemHasil();
	//dataGridKondisiAmbulanceRefresh();
	var FieldItem=['kd_item', 'item_hd'];
	//dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viSetupKondisiAmbulanceTest = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupKondisiAmbulance,
			autoScroll: true,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			height:540,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupKondisiAmbulanceTest = undefined;
							rowSelected_viSetupKondisiAmbulanceTest = dataSource_viSetupKondisiAmbulance.getAt(row);
							CurrentData_viSetupKondisiAmbulanceTest
							CurrentData_viSetupKondisiAmbulanceTest.row = row;
							CurrentData_viSetupKondisiAmbulanceTest.data = rowSelected_viSetupKondisiAmbulanceTest.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupKondisiAmbulanceTest = dataSource_viSetupKondisiAmbulance.getAt(ridx);
					if (rowSelected_viSetupKondisiAmbulanceTest != undefined)
					{
						DataInitSetupKondisiAmbulanceTest(rowSelected_viSetupKondisiAmbulanceTest.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viKodeKondisiAmbulance',
						header: 'Kode',
						dataIndex: 'kd_kondisi', //harus sama dengan nama kolom didatabase
						hideable:false,
						menuDisabled: true,
						width: 10
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viNamaKondisi',
						header: 'Kondisi',
						dataIndex: 'kondisi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupTipeAmbulance',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupDokterAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupKondisiAmbulanceTest != undefined)
							{
								DataInitSetupKondisiAmbulanceTest(rowSelected_viSetupKondisiAmbulanceTest.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupKondisiAmbulanceTest, selectCount_viSetupKondisiAmbulance, dataSource_viSetupKondisiAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GridDataView_viSetupReferensiHasil = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupReferensiHasil,
			autoScroll: true,
			columnLines: true,
			flex:1,
			height:250,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupReferensiHasil = undefined;
							rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(row);
							CurrentData_viSetupReferensiHasil
							CurrentData_viSetupReferensiHasil.row = row;
							CurrentData_viSetupReferensiHasil.data = rowSelected_viSetupReferensiHasil.data;
							//DataInitSetupKondisiAmbulanceTest(rowSelected_viSetupKondisiAmbulanceTest.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(ridx);
					if (rowSelected_viSetupReferensiHasil != undefined)
					{
						DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
						//setLookUp_viSetupKondisiAmbulanceTest(rowSelected_viSetupKondisiAmbulanceTest.data);
					}
					else
					{
						//setLookUp_viSetupKondisiAmbulanceTest();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNo_ref_viSetupReferensiHasil',
						header: 'No. Ref',
						dataIndex: 'no_ref',
						hideable:false,
						menuDisabled: true,
						width: 10 
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupReferensiHasil',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colDeskripsi_viSetupReferensiHasil',
						header: 'Deskripsi',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupReferensiHasil',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupKondisiAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupReferensiHasil != undefined)
							{
								DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupKondisiAmbulanceTest, selectCount_viSetupKondisiAmbulance, dataSource_viSetupKondisiAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
								
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKd_Item_viSetupItemDialisa',
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 5
						
					},
					//-------------- ## --------------
					{
						id		: 'col_Item_viSetupItemDialisa',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 30,
						editor:new Nci.form.Combobox.autoComplete({
							
							store	: SetupKondisiAmbulanceTest.form.ArrayStore.b,
							select	: function(a,b,c){
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemDialisa.data.items[line].data.kd_item=b.data.kd_item;
								dataSource_viSetupItemDialisa.data.items[line].data.item_hd=b.data.item_hd;
								GridDataView_viSetupItemDialisa.getView().refresh();
							},
							insert	: function(o){
								return {
									kd_item        	: o.kd_item,
									item_hd 		: o.item_hd,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_item+'</td><td width="150">'+o.item_hd+'</td></tr></table>'
								}
							},
							url		: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/getItemHasilTest",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemDialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowItemDialisa',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupItemDialisa.recordType());
							dataSource_viSetupItemDialisa.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanItemDialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_ItemDialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Row',
						iconCls: 'remove',
						id: 'btnDeleteItemDialisa',
						handler: function()
						{
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
							dataSource_viSetupItemDialisa.removeAt(line);
							GridDataView_viSetupItemDialisa.getView().refresh();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshItemDialisa',
						handler: function()
						{
							dataSource_viSetupItemDialisa.removeAll();
							dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
							Ext.getCmp('btnAddRowItemDialisa').disable();
							Ext.getCmp('btnSimpanItemDialisa').disable();
							Ext.getCmp('btnDeleteItemDialisa').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupKondisiAmbulanceTest, selectCount_viSetupKondisiAmbulance, dataSource_viSetupKondisiAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelInputSetupKondisiAmbulance = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:false,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupItemHasil()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupKondisiAmbulance',
						handler: function(){
							AddNewSetupKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupKondisiAmbulance',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupKondisiAmbulance',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupKondisiAmbulance();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupKondisiAmbulance',
						handler: function()
						{
							dataSource_viSetupKondisiAmbulance.removeAll();
							dataGridSetupKondisiAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					}
				]
			}
	})
	
	var FormSetupTKondisiAmbulanceCari = new Ext.Panel
	(
		{
			id: 'mod_id_panel_grid_cari_kondisi_ambulance',
			closable:true,
			region: 'center',
			layout: 'form',
			//title: 'Data Supir Ambulance',
			//margins:'0 5 5 0',
			//bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'Pagu',
			items: [GridDataView_viSetupKondisiAmbulanceTest],
			tbar: 
			[
				'Kondisi','',
					{
								xtype: 'textfield',
								id: 'txtKondisi_Ambulance_cari',
								name: 'txtKondisi_Ambulance_cari',
								width: 150,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											RefreshDataKondisiAmbulanceFilter();	
										} 						
									},
									blur: function(a){
										
									}
								}
							},
				'','','',
				{
					xtype: 'button',
					id:'btnRefreshKondisiFilter',
					tooltip: 'Tampilkan',
					iconCls: 'find',
					align :'right',
					text: 'Tampilkan',
					handler: function(sm, row, rec) {
						RefreshDataKondisiAmbulanceFilter();
					}

				}
			]
		}
	);
	
	//LAYAR FORM UTAMA 
	var FrmData_viSetupKondisiAmbulanceTest = new Ext.Panel
    (
		{
			title: 'Setup Kondisi Ambulance',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupKondisiAmbulanceTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
			title: '',
			height: 750,
			items:[
				PanelInputSetupKondisiAmbulance,
				//GridDataView_viSetupKondisiAmbulanceTest
				FormSetupTKondisiAmbulanceCari
			]
		}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupKondisiAmbulanceTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupItemHasil(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 70,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdKondisiAmbulance',
								name: 'txtKdKondisiAmbulance',
								width: 100,
								allowBlank: false,
								readOnly: true,
								disabled:true,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtkondisi_ambulance',
								name: 'txtkondisi_ambulance',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							}
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 130,
        y: 60,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};

function dataGridSetupKondisiAmbulance(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionSetupKondisiAmbulance/getGridKondisiAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupKondisiAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupKondisiAmbulance.add(recs);
					GridDataView_viSetupKondisiAmbulanceTest.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupKondisiAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupKondisiAmbulance(){
	Ext.getCmp('txtKdKondisiAmbulance').setValue('');
	Ext.getCmp('txtkondisi_ambulance').setValue('');
};

function dataSave_viSetupKondisiAmbulance(){
	if (ValidasiSaveSetupKondisiAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupKondisiAmbulance/save",
				params: getParamSaveSetupKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						Ext.get('txtKdKondisiAmbulance').dom.value=cst.kode;
						loadMask.hide();
						ShowPesanInfoSetupKondisiAmbulance('Berhasil menyimpan data ini','Information');
					/*	Ext.getCmp('txtKdItem_SetupItemHasil').setValue(cst.kode);*/
						dataSource_viSetupKondisiAmbulance.removeAll();
						dataGridSetupKondisiAmbulance();

					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupKondisiAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupKondisiAmbulance.removeAll();
						dataGridSetupKondisiAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamSaveSetupKondisiAmbulance(){	
	var	params =
	{
		kd_kondisi:Ext.getCmp('txtKdKondisiAmbulance').getValue(),
		kondisi:Ext.getCmp('txtkondisi_ambulance').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupKondisiAmbulance(modul,mBolHapus){
	var x = 1;	
	if(Ext.getCmp('txtkondisi_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupKondisiAmbulance('Kondisi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function ValidasiSaveSetupKondisiAmbulance_delete(modul,mBolHapus){
	var x = 1;	
	if(Ext.getCmp('txtkondisi_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupKondisiAmbulance('Gagal dihapus tidak ada data / data kosong', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningSetupKondisiAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupKondisiAmbulance(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupKondisiAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viSetupKondisiAmbulance(){
	if (ValidasiSaveSetupKondisiAmbulance_delete(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupKondisiAmbulance/delete",
				params: getParamDeleteSetupKondisiAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupKondisiAmbulance('Berhasil menghapus data ini','Information');
						AddNewSetupKondisiAmbulance()
						dataSource_viSetupKondisiAmbulance.removeAll();
						dataGridSetupKondisiAmbulance();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupKondisiAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupKondisiAmbulance.removeAll();
						dataGridSetupKondisiAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupKondisiAmbulance(){
	var	params =
	{
		kd_kondisi:Ext.getCmp('txtKdKondisiAmbulance').getValue()
	}
   
    return params
};

function DataInitSetupKondisiAmbulanceTest(rowdata){
	Ext.getCmp('txtKdKondisiAmbulance').setValue(rowdata.kd_kondisi);
	Ext.getCmp('txtkondisi_ambulance').setValue(rowdata.kondisi);
};

function loadDataComboItemHasil(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/getItemHasil",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboItemHasil.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_ItemHasil.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_ItemHasil.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboItemHasil()
{
	var Field = ['kd_item','item_hd'];
    ds_ItemHasil = new WebApp.DataStore({fields: Field});
	loadDataComboItemHasil();
    cboItemHasil = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 30,
			id:'cboItemHasil',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:200,
			store: ds_ItemHasil,
			valueField: 'kd_item',
			displayField: 'item_hd',
			value:'Pilih Item',
			listeners:
			{
					'select': function(a,b,c)
					{
						selectSetPilihan=b.data.displayText ;
					}
			}
		}
	);
	return cboItemHasil;
};

function dataGridRefHasil(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/getGridRefHasil",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupReferensiHasil.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupReferensiHasil.add(recs);
					GridDataView_viSetupReferensiHasil.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupKondisiAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupKondisiAmbulance('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupKondisiAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupKondisiAmbulance('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupKondisiAmbulance('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupKondisiAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupKondisiAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupKondisiAmbulance('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupKondisiAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupKondisiAmbulance('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningSetupKondisiAmbulance('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupKondisiAmbulanceTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupKondisiAmbulance('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupKondisiAmbulance('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};


function dataGridKondisiAmbulanceRefresh(criteria){
	dataSource_viSetupKondisiAmbulance.load({
		params:{
			Skip: 0,
			Take: selectCount_viSetupKondisiAmbulance,
			Sort: 'kd_tipe',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataKondisiAmbulance',
			param: criteria

		}
	});
    return dataSource_viSetupKondisiAmbulance;
	
}	
function RefreshDataKondisiAmbulanceFilter(){
	Ext.Ajax.request({
			url: baseURL + "index.php/ambulance/functionSetupKondisiAmbulance/getGridKondisiAmbulance_cari",
			params: {kondisi:Ext.getCmp('txtKondisi_Ambulance_cari').getValue()},
			failure: function(o){
				ShowPesanErrorSetupKondisiAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) { 
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dataSource_viSetupKondisiAmbulance.removeAll();
					var recs=[],
						recType=dataSource_viSetupKondisiAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupKondisiAmbulance.add(recs);
					GridDataView_viSetupKondisiAmbulanceTest.getView().refresh();

				}
				else {
					ShowPesanErrorSetupKondisiAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}	

