var NamaForm_viSetupMobilAmbulance="Setup Mobil Ambulance";
var mod_name_viSetupMobilAmbulance="Setup Mobil Ambulance";
var selectCount_viSetupMobilAmbulance=0;
var now_viSetupMobilAmbulance= new Date();
var tanggal = now_viSetupMobilAmbulance.format("d/M/Y");
var jam = now_viSetupMobilAmbulance.format("H/i/s");

var dataSource_viSetupMobilAmbulance;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupMobilAmbulanceTest;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viSetupMobilAmbulanceTest;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;
var dataGridFasilitasAmbulanceRefresh;
var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;
var tmp_kd_milik_cari;
var tmp_kd_tipe_cari;

var CurrentData_viSetupMobilAmbulanceTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupFasilitasAmbulanceTest={};
SetupFasilitasAmbulanceTest.form={};
SetupFasilitasAmbulanceTest.func={};
SetupFasilitasAmbulanceTest.vars={};
SetupFasilitasAmbulanceTest.func.parent=SetupFasilitasAmbulanceTest;	
SetupFasilitasAmbulanceTest.form.ArrayStore={};
SetupFasilitasAmbulanceTest.form.ComboBox={};
SetupFasilitasAmbulanceTest.form.DataStore={};
SetupFasilitasAmbulanceTest.form.Record={};
SetupFasilitasAmbulanceTest.form.Form={};
SetupFasilitasAmbulanceTest.form.Grid={};
SetupFasilitasAmbulanceTest.form.Panel={};
SetupFasilitasAmbulanceTest.form.TextField={};
SetupFasilitasAmbulanceTest.form.Button={};

SetupFasilitasAmbulanceTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupFasilitasAmbulanceTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd'],
	data: []
});

CurrentPage.page = dataGrid_viSetupFasilitasAmbulanceTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupFasilitasAmbulanceTest(mod_id_viSetupMobilAmbulanceTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupFasilitasAmbulance = 
	[
		'kd_milik','nopol','kd_milik','kd_tipe','tipe','merk'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupMobilAmbulance = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupFasilitasAmbulance
    });
	dataGridItem_SetupMobilAmbulance();
	loadDataComboMerk();
	loadDataComboMerk_cari();
	loadDataComboTipe();
	loadDataComboTipe_cari();
	var FieldItem=['kd_item', 'item_hd'];
	dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viSetupMobilAmbulanceTest = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			id:'grid_setup_mobil_ambulance',
			store: dataSource_viSetupMobilAmbulance,
			autoScroll: true,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			height:540,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupMobilAmbulanceTest = undefined;
							rowSelected_viSetupMobilAmbulanceTest = dataSource_viSetupMobilAmbulance.getAt(row);
							CurrentData_viSetupMobilAmbulanceTest
							CurrentData_viSetupMobilAmbulanceTest.row = row;
							CurrentData_viSetupMobilAmbulanceTest.data = rowSelected_viSetupMobilAmbulanceTest.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupMobilAmbulanceTest = dataSource_viSetupMobilAmbulance.getAt(ridx);
					if (rowSelected_viSetupMobilAmbulanceTest != undefined)
					{
						DataInitSetupMobilAmbulance(rowSelected_viSetupMobilAmbulanceTest.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viFasilitasAmbulance',
						header: 'Kode Mobil',
						dataIndex: 'kd_mobil', //harus sama dengan nama kolom didatabase
						hideable:false,
						menuDisabled: true,
						width: 10
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viNamaFasilitas',
						header: 'Nomor Polisi',
						dataIndex: 'nopol',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					
					//-------------- ## --------------
					{
						id: 'colKeteranganFasilitasAmbulance',
						header: 'Merk',
						dataIndex: 'merk',
						hidden:false,
						menuDisabled: true,
						width: 40
					},
					{
						id: 'colKeteranganFasilitasAmbulance',
						header: 'Tipe',
						dataIndex: 'tipe',
						hidden:false,
						menuDisabled: true,
						width: 40
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMobilAmbulance',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupMobilAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupMobilAmbulanceTest != undefined)
							{
								DataInitSetupMobilAmbulance(rowSelected_viSetupMobilAmbulanceTest.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
		//	bbar : bbar_paging(mod_name_viSetupMobilAmbulance, selectCount_viSetupMobilAmbulance, dataSource_viSetupMobilAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GridDataView_viSetupReferensiHasil = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupReferensiHasil,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:270,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupReferensiHasil = undefined;
							rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(row);
							CurrentData_viSetupReferensiHasil
							CurrentData_viSetupReferensiHasil.row = row;
							CurrentData_viSetupReferensiHasil.data = rowSelected_viSetupReferensiHasil.data;
							//DataInitSetupMobilAmbulance(rowSelected_viSetupMobilAmbulanceTest.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(ridx);
					if (rowSelected_viSetupReferensiHasil != undefined)
					{
						DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
						//setLookUp_viSetupFasilitasAmbulanceTest(rowSelected_viSetupMobilAmbulanceTest.data);
					}
					else
					{
						//setLookUp_viSetupFasilitasAmbulanceTest();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNo_ref_viSetupReferensiHasil',
						header: 'No. Ref',
						dataIndex: 'no_ref',
						hideable:false,
						menuDisabled: true,
						width: 10 
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupReferensiHasil',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colDeskripsi_viSetupReferensiHasil',
						header: 'Deskripsi',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupReferensiHasil',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupReferensiHasil',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupReferensiHasil != undefined)
							{
								DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMobilAmbulance, selectCount_viSetupMobilAmbulance, dataSource_viSetupMobilAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
								
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKd_Item_viSetupItemDialisa',
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 5
						
					},
					//-------------- ## --------------
					{
						id		: 'col_Item_viSetupItemDialisa',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 30,
						editor:new Nci.form.Combobox.autoComplete({
							
							store	: SetupFasilitasAmbulanceTest.form.ArrayStore.b,
							select	: function(a,b,c){
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemDialisa.data.items[line].data.kd_item=b.data.kd_item;
								dataSource_viSetupItemDialisa.data.items[line].data.item_hd=b.data.item_hd;
								GridDataView_viSetupItemDialisa.getView().refresh();
							},
							insert	: function(o){
								return {
									kd_item        	: o.kd_item,
									item_hd 		: o.item_hd,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_item+'</td><td width="150">'+o.item_hd+'</td></tr></table>'
								}
							},
							url		: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/getItemHasilTest",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemDialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowItemDialisa',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupItemDialisa.recordType());
							dataSource_viSetupItemDialisa.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanItemDialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_ItemDialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Row',
						iconCls: 'remove',
						id: 'btnDeleteItemDialisa',
						handler: function()
						{
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
							dataSource_viSetupItemDialisa.removeAt(line);
							GridDataView_viSetupItemDialisa.getView().refresh();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshItemDialisa',
						handler: function()
						{
							dataSource_viSetupItemDialisa.removeAll();
							dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
							Ext.getCmp('btnAddRowItemDialisa').disable();
							Ext.getCmp('btnSimpanItemDialisa').disable();
							Ext.getCmp('btnDeleteItemDialisa').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMobilAmbulance, selectCount_viSetupMobilAmbulance, dataSource_viSetupMobilAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelInputSetupMobilAmbulance = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:false,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupMobilAmbulance()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupMobilAmbulance',
						handler: function(){
							AddNewSetupMobilAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupMobilAmbulance',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupMobilAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupMobilAmbulance',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupMobilAmbulance();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupMobilAmbulance',
						handler: function()
						{
							dataSource_viSetupMobilAmbulance.removeAll();
							dataGridItem_SetupMobilAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					}
				]
			}
	})
	
	var FormMobilStp = new Ext.Panel
	(
		{
			id: 'mod_id_panel_grid_cari_Setup_Mobil',
			closable:true,
			region: 'center',
			layout: 'form',
			//title: 'Data Supir Ambulance',
			//margins:'0 5 5 0',
			//bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'Pagu',
			items: [GridDataView_viSetupMobilAmbulanceTest],
			tbar: 
			[
				'Merk','',
				mComboMerk_cari(),'',
				'Tipe',
				comboTypeMobil_cari(),'','','',
				{
					xtype: 'button',
					id:'btnRefreshMobilAmbulanceFilter',
					tooltip: 'Tampilkan',
					iconCls: 'find',
					align :'right',
					text: 'Tampilkan',
					handler: function(sm, row, rec) {
						RefreshDataMobilAmbulanceFilter();
					}

				}
			]
		}
	);
	//LAYAR FORM UTAMA 
	var FrmData_viSetupMobilAmbulanceTest = new Ext.Panel
    (
		{
			title: 'Setup Mobil ambulance',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupMobilAmbulanceTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
			title: '',
			height: 750,
			items:[
				PanelInputSetupMobilAmbulance,
				//GridDataView_viSetupMobilAmbulanceTest
				FormMobilStp
			]
		}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupMobilAmbulanceTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupMobilAmbulance(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Mobil'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdItem_SetupKd_mobilAmbulance',
								name: 'txtKdItem_SetupKd_mobilAmbulance',
								width: 200,
								allowBlank: false,
								readOnly: true,
								disabled:true,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nomor Polisi'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtItem_SetupNopolAmbulance',
								name: 'txtItem_SetupNopolAmbulance',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Merk'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							mComboMerk(),
							{
								x: 350,
								y: 60,
								xtype: 'label',
								text: 'Tipe'
							},
							{
								x: 390,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboTypeMobil()

						]
					}
						]
					}
				]
			}
		]		
	};
        return items;
}


function dataGridItem_SetupMobilAmbulance(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionSetupMobilAmbulance/getGridMobilAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupMobilAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupMobilAmbulance.add(recs);
					GridDataView_viSetupMobilAmbulanceTest.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupFasilitasAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupMobilAmbulance(){
	Ext.getCmp('txtKdItem_SetupKd_mobilAmbulance').setValue('');
	Ext.getCmp('txtItem_SetupNopolAmbulance').setValue('');
	Ext.getCmp('cboMerkAmbulance').setValue('');
	Ext.getCmp('cboTipeAmbulance').setValue('');
	
};

function dataSave_viSetupMobilAmbulance(){
	if (ValidasiSaveSetupMobilAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupMobilAmbulance/save",
				params: getParamSaveSetupMobilAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						Ext.get('txtKdItem_SetupKd_mobilAmbulance').dom.value=cst.kode;
						loadMask.hide();

						ShowPesanInfoSetupMobilAmbulance('Berhasil menyimpan data ini','Information');
						//Ext.getCmp('txtKdItem_SetupItemHasil').setValue(cst.kode);
						dataSource_viSetupMobilAmbulance.removeAll();
						dataGridItem_SetupMobilAmbulance();
					//	loadDataComboItemHasil();
					}
					else {
						loadMask.hide();
						ShowPesanErrorSetupMobilAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupMobilAmbulance.removeAll();
						dataGridItem_SetupMobilAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamSaveSetupMobilAmbulance(){
	var	params =
	{
		kd_mobil:Ext.getCmp('txtKdItem_SetupKd_mobilAmbulance').getValue(),
		nopol:Ext.getCmp('txtItem_SetupNopolAmbulance').getValue(),
		kd_milik: Ext.getCmp('cboMerkAmbulance').getValue(),
		kd_tipe: Ext.getCmp('cboTipeAmbulance').getValue()
		
	}
    return params
};
function ValidasiSaveSetupMobilAmbulance(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtItem_SetupNopolAmbulance').getValue() ==='' && Ext.getCmp('cboMerkAmbulance').getValue() ==='' && Ext.getCmp('cboTipeAmbulance').getValue() ===''){
		loadMask.hide();
		
		ShowPesanErrorSetupMobilAmbulance('Data Mobil Masih Kosong', 'Warning');
		x = 0;
	}
	else if(Ext.getCmp('txtItem_SetupNopolAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanErrorSetupMobilAmbulance('Nomor Polisi Masih Kosong', 'Warning');
		x = 0;
	}
	else if(Ext.getCmp('cboMerkAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanErrorSetupMobilAmbulance('Merk Belum Dipilih', 'Warning');
		x = 0;
	}
	else if(Ext.getCmp('cboTipeAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanErrorSetupMobilAmbulance('Tipe Belum Dipilih', 'Warning');
		x = 0;
	}
	return x;
};
function ValidasiSaveSetupMobilAmbulance_delete(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtItem_SetupNopolAmbulance').getValue() ==='' && Ext.getCmp('cboMerkAmbulance').getValue() ==='' && Ext.getCmp('cboTipeAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Gagal dihapus tidak ada data / data kosong', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningSetupFasilitasAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanErrorSetupMobilAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};
function ShowPesanInfoSetupMobilAmbulance(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};



function dataDelete_viSetupMobilAmbulance(){
	if (ValidasiSaveSetupMobilAmbulance_delete(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupMobilAmbulance/delete",
				params: getParamDeleteSetupMobilAmbulance(),
				failure: function(o){
					loadMask.hide();
					ShowPesanErrorSetupMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						loadMask.hide();
						ShowPesanInfoSetupMobilAmbulance('Berhasil menghapus data ini','Information');
						dataSource_viSetupMobilAmbulance.removeAll();
						AddNewSetupMobilAmbulance();
						dataGridItem_SetupMobilAmbulance();

						
					}
					else {
						loadMask.hide();
						ShowPesanErrorSetupMobilAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupMobilAmbulance.removeAll();
						dataGridItem_SetupMobilAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupMobilAmbulance(){
	var	params =
	{
		kd_mobil:Ext.getCmp('txtKdItem_SetupKd_mobilAmbulance').getValue()
	}
   
    return params
};

function DataInitSetupMobilAmbulance(rowdata){
	Ext.getCmp('txtKdItem_SetupKd_mobilAmbulance').setValue(rowdata.kd_mobil);
	Ext.getCmp('txtItem_SetupNopolAmbulance').setValue(rowdata.nopol);
	Ext.getCmp('cboMerkAmbulance').setValue(rowdata.kd_milik);
	Ext.getCmp('cboTipeAmbulance').setValue(rowdata.kd_tipe);
};

function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMobilAmbulance('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupFasilitasAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupFasilitasAmbulance('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMobilAmbulance('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupFasilitasAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupFasilitasAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupFasilitasAmbulance('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMobilAmbulance('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupFasilitasAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupFasilitasAmbulance('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningSetupFasilitasAmbulance('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupFasilitasAmbulanceTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupMobilAmbulance('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupFasilitasAmbulance('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};

function RefreshDataMobilFilter(){
	Ext.Ajax.request({
			url: baseURL + "index.php/ambulance/functionSetupFasilitasAmbulance/getGridFasilitasAmbulance_cari",
			params: {nama_fasilitas:Ext.getCmp('txtNamaFasilitas_cari').getValue()},
			failure: function(o){
				ShowPesanErrorSetupFasilitasAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o){
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dataSource_viSetupMobilAmbulance.removeAll();
					var recs=[],
						recType=dataSource_viSetupMobilAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupMobilAmbulance.add(recs);
					GridDataView_viSetupMobilAmbulanceTest.getView().refresh();
				}
				else {
					ShowPesanErrorSetupFasilitasAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
)}
function dataGridFasilitasAmbulanceRefresh(criteria){
	dataSource_viSetupMobilAmbulance.load({
		params:{
			Skip: 0,
			Take: selectCount_viSetupMobilAmbulance,
			Sort: 'kd_milik',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataFasilitasAmbulance',
			param: criteria

		}
	});
    return dataSource_viSetupMobilAmbulance;
	
}	

function mComboMerk(){
	
      var Field = ['kd_milik','merk'];
      dsMerkAmbulance = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance = new Ext.form.ComboBox
	(
		{
			x:130,
			y:60,
		    id: 'cboMerkAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsMerkAmbulance,
		    valueField: 'kd_milik',
		    displayField: 'merk',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function(a, b, c){
                          },				                       
			}
                }
	);
    return cboMerkAmbulance;
};

function loadDataComboMerk(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/getGridMerkAmbulance",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsMerkAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsMerkAmbulance.add(recs);
			}
				console.log(dsMerkAmbulance);
		}
	});
}

function comboTypeMobil(){
      var Field = ['kd_tipe','tipe'];
      dsTipeAmbulance = new WebApp.DataStore({fields: Field});
      
       cboTipeAmbulance = new Ext.form.ComboBox
	(
		{
			x: 400,
        	y: 60,
		    id: 'cboTipeAmbulance',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsTipeAmbulance,
		    valueField: 'kd_tipe',
		    displayField: 'tipe',
		    width:200,
			tabIndex:3,
		    listeners:
			{
			    'select': function(a, b, c){
                          },				                       
			}
                }
	);
    return cboTipeAmbulance;
};

function loadDataComboTipe(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionSetupTipeAmbulance/getGridTipeAmbulance",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsTipeAmbulance.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsTipeAmbulance.add(recs);
			}
				console.log(dsTipeAmbulance);
		}
	});
}
function mComboMerk_cari(){
	
      var Field = ['kd_milik','merk'];
      dsMerkAmbulance_cari = new WebApp.DataStore({fields: Field});
      
       cboMerkAmbulance_cari = new Ext.form.ComboBox
	(
		{
		    id: 'cboMerkAmbulance_cari',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsMerkAmbulance_cari,
		    valueField: 'kd_milik',
		    displayField: 'merk',
		    width:150,
			tabIndex:3,
		    listeners:
			{
			    'select': function(a, b, c){
			    	 tmp_kd_milik_cari = b.data.kd_milik;
			    		RefreshDataMobilAmbulanceFilter();
                          },				                       
			}
                }
	);
    return cboMerkAmbulance_cari;
};

function loadDataComboMerk_cari(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/getGridMerkAmbulance",
		params: {flag:1},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsMerkAmbulance_cari.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsMerkAmbulance_cari.add(recs);
			}
				console.log(dsMerkAmbulance_cari);
		}
	});
}

function comboTypeMobil_cari(){
	
      var Field = ['kd_tipe','tipe'];
      dsTipeAmbulance_cari = new WebApp.DataStore({fields: Field});
      
       cboTipeAmbulance_cari = new Ext.form.ComboBox
	(
		{
		    id: 'cboTipeAmbulance_cari',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    store: dsTipeAmbulance_cari,
		    valueField: 'kd_tipe',
		    displayField: 'tipe',
		    width:150,
			tabIndex:3,
		    listeners:
			{
			    'select': function(a, b, c){
			    	 tmp_kd_tipe_cari = b.data.kd_tipe;
			    	RefreshDataMobilAmbulanceFilter();
                          },				                       
			}
                }
	);
    return cboTipeAmbulance_cari;
};

function loadDataComboTipe_cari(){
	Ext.Ajax.request({
	url: baseURL + "index.php/ambulance/functionSetupTipeAmbulance/getGridTipeAmbulance",
		params: {flag:1},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dsTipeAmbulance_cari.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsTipeAmbulance_cari.add(recs);
			}
				console.log(dsTipeAmbulance_cari);
		}
	});
}
function RefreshDataMobilAmbulanceFilter(){
	Ext.Ajax.request({
			url: baseURL + "index.php/ambulance/functionSetupMobilAmbulance/getGridMobilAmbulance_cari",
			params: {kd_milik:tmp_kd_milik_cari,
					 kd_tipe:tmp_kd_tipe_cari},
			failure: function(o){
				ShowPesanErrorSetupMobilAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dataSource_viSetupMobilAmbulance.removeAll();
					var recs=[],
						recType=dataSource_viSetupMobilAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupMobilAmbulance.add(recs);
					GridDataView_viSetupMobilAmbulanceTest.getView().refresh();
				}
				else {
					ShowPesanErrorSetupMobilAmbulance('Gagal Membaca Mobil Ambulance', 'Error');
				}
			}
		}
		
)}

