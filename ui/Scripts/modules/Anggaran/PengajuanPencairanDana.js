var dsGListSP3DForm;
var rowselectGListSP3DForm;
var countGListSP3DForm = 50;
var selectCboFilterUnitKerjaSP3DForm;
var rowselectGridDetailEntrySP3DForm;
var now = new Date();
var winFormEntrySP3DForm;
var dsGridDetailEntrySP3DForm;
var userSp3D;
var dsCboUnitKerjaEntrySP3DForm;
var gridListLookupRKATUnit;
var grdListSP3DForm;
//userSp3D= strKdUser;

var CurrentRowGridDetailEntrySP3DForm = {
	data: Object,
	details: Array,
	row: 0
};
var selectCboUnitKerjaEntrySP3DForm;
var selectCboUnitKerjaEntrySP3DFormTempEdit="";
var vAddNewDataEntrySP3DForm;
var akun;
var jumlah;
var line;
var grdDetailEntrySP3DForm;
var mRecordDetailEntrySP3DForm = Ext.data.Record.create(
	[
		{ NAME: 'tahun_anggaran_ta', mapping: 'tahun_anggaran_ta' },
		{ NAME: 'kd_unit_kerja', mapping: 'kd_unit_kerja' },
		{ NAME: 'kd_jns_rkat_jrka', mapping: 'kd_jns_rkat_jrka' },
		{ NAME: 'no_program_prog', mapping: 'no_program_prog' },
		{ NAME: 'prioritas', mapping: 'prioritas' },
		{ NAME: 'no_sp3d', mapping: 'no_sp3d' },
		{ NAME: 'tgl_sp3d', mapping: 'tgl_sp3d' },
		{ NAME: 'urut', mapping: 'urut' },
		{ NAME: 'account', mapping: 'account' },
		{ NAME: 'name', mapping: 'name' },
		{ NAME: 'deskripsi', mapping: 'deskripsi' },
		{ NAME: 'value', mapping: 'value' },
		{ NAME: 'posted', mapping: 'posted' },
		{ NAME: 'deskripsi', mapping: 'deskripsi' }
	]
);
var dtTglSP3DAsal;
// var thn = gstrTahunAngg; //variable tahun anggaran dari SYS_SETTINGS
var thn = ''; //variable tahun anggaran dari SYS_SETTINGS
var TahunAngg = parseInt(now.format('Y')); // Tahun anggaran dari lookup
var jnsrkatk='0';
var jnsrkatr='0';
var jnsrkato='0';
var StrJnsRKATEntrySP3DForm='';
var ishapusdetail='0';
var REFFID_SP3D_SP3DForm='01'; 
var stris_app=0;
// SDY_RI
// variabel untuk cek selisih
/*var jumlah_RKAT_get;
var jumlah_RKAT_history_get;*/
// End SDY_RI

CurrentPage.page = getPnlEntrySP3DForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPnlEntrySP3DForm(mod_id)
{

	var Fields = 
	[
		'no_sp3d',
		'tgl_sp3d',
		'tahun_anggaran_ta',
		'kd_unit_kerja',
		'kd_jns_rkat_jrka',
		'no_program_prog',
		'prioritas',
		'nama_program_prog',
		'kegiatan',
		'rkat',
		'unitkerja',
		'keterangan',
		'jumlah',
		'app_sp3d',
		'prioritas',
		'app_sp3d_temp',
		'app_level_1',
		'app_level_2',
		'app_level_3',
		'app_level_4',
		'is_jalur',
		'app_level_5',		
		'jml_anggaran',
		'filepath',
		'urut_app'
	];
	dsGListSP3DForm = new WebApp.DataStore({ fields: Fields });
	var chkApproved1SP3DForm = new Ext.grid.CheckColumn(
		{
			id: 'chkApproved1SP3DForm',
			header: 'Level 1',
			align: 'center',
			dataIndex: 'app_level_1',
			disabled: true,
			readOnly: true,
			hidden:true,
			width: 80
		}
	);
	var chkApproved2SP3DForm = new Ext.grid.CheckColumn(
		{
			id: 'chkApproved2SP3DForm',
			header: 'Level 2',
			align: 'center',
			dataIndex: 'app_level_2',
			disabled: true,
			readOnly: true,
			hidden:true,
			width: 80
		}
	);
	var chkApproved3SP3DForm = new Ext.grid.CheckColumn(
		{
			id: 'chkApproved3SP3DForm',
			header: 'Level 3',
			align: 'center',
			dataIndex: 'app_level_3',
			disabled: true,
			readOnly: true,
			hidden:true,
			width: 80
		}
	);
	var chkApproved4SP3DForm = new Ext.grid.CheckColumn(
		{
			id: 'chkApproved4SP3DForm',
			header: 'Level 4',
			align: 'center',
			dataIndex: 'app_level_4',
			disabled: true,
			readOnly: true,
			hidden:true,
			width: 80
		}
	);
	var chkApproved5SP3DForm = new Ext.grid.CheckColumn(
		{
			id: 'chkApproved5SP3DForm',
			header: 'Level 5',
			align: 'center',
			dataIndex: 'app_level_5',
			disabled: true,
			readOnly: true,
			hidden:true,
			width: 80
		}
	);
	var chkApprovedSP3DForm = new Ext.grid.CheckColumn(
		{
			id: 'chkApprovedSP3DForm',
			header: 'Approved',
			align: 'center',			
			dataIndex: 'app_sp3d_temp',
			disabled: true,
			readOnly: true,
			width: 60,
			onMouseDown: function(e, t)
			{
			//var index = this.grid.getView().findRowIndex(t);			  
				if (intLvlApproval == '4' ||intLvlApproval == '3')
				{
				   if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1)
				    {
						e.stopEvent();
						var index = this.grid.getView().findRowIndex(t);
						var record = this.grid.store.getAt(index);
						record.set(this.dataIndex, !record.data[this.dataIndex]);
						rowData = record.data;//save row records. will be used in the ajax request						
						if (record.data.APP_SP3D)
						{	
							if (intLvlApproval == '4' && rowData.APP_LEVEL_3==0)
							{
								Ext.Msg.show
								(
									{
										title: 'Approve',
										msg: 'Approve Belum Dilakukan oleh BKU, Apakah Transaksi Ini Akan DiApprove ?',
										buttons: Ext.MessageBox.YESNO,
										fn: function(btn) 
										{
											if (btn == 'yes') 
											{							
												Ext.Ajax.request
												({ url: "./Datapool.mvc/UpdateDataObj",
													params: getGridParamAppEntrySP3DForm(rowData),
													success: function(o) 
													{
														var cst = Ext.decode(o.responseText);
														if (cst.success === true) 
														{
															ShowPesanInfo_SP3DFormAll('Data berhasil diapprove','Approve Data');								
															userSp3D= cst.pesan;								
														}
														else
														{
															// if (intLvlApproval == '4')
															// {
																// ShowPesanInfo_SP3DFormAll('Data gagal diapprove / Belum di Approve BKU','Approve Data'); 
															// }
															// else
															// {
																// ShowPesanInfo_SP3DFormAll('Data gagal diapprove ','Approve Data'); 
															// }
															ShowPesanInfo_SP3DFormAll(cst.pesan,'Approve Data'); 
														}
														RefreshDataGListSP3DForm(getCriteriaGListSP3DForm()); 
													}
												}) 
											}
											else
											{
												
											}
										},
										icon: Ext.MessageBox.QUESTION
									}
								);
							}
							else
							{
								ApproveGridEntrySP3DForm(rowData);
							}
						}					
					}						
				}
					
				  
			}
		}
	);
	
	grdListSP3DForm = new Ext.grid.EditorGridPanel(
		{
			id: 'grdListSP3DForm',
			anchor: '100% 100%',
			width: 720,
			border: false,
			columnLines: true,
			store: dsGListSP3DForm,
			selModel: new Ext.grid.RowSelectionModel(
			//sm: new Ext.grid.RowSelectionModel(
				{
					singleSelect: true,
					listeners:
					{
						'rowselect': function(sm, row, rec)
						{
							rowselectGListSP3DForm = dsGListSP3DForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				'afterrender': function()
				{
					RefreshDataGListSP3DForm(getCriteriaGListSP3DForm()); 
				},
				'rowdblclick': function(sm, row, rec) {
			        rowselectGListSP3DForm = dsGListSP3DForm.getAt(row);
			        if (rowselectGListSP3DForm != undefined) {
			            showWinFormEntrySP3DForm(rowselectGListSP3DForm.data);
			        }
			        else {
			            showWinFormEntrySP3DForm();
			        }

			    }
			},
			plugins: [new Ext.ux.grid.FilterRow()],chkApprovedSP3DForm,
			colModel: new Ext.grid.ColumnModel(
				[
					new Ext.grid.RowNumberer(),
					{
						xtype: 'gridcolumn',
						header: 'Nomor',
						dataIndex: 'no_sp3d',
						width: 100,
						// hidden:true,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.no_sp3d + "</div>";
							return str;
						},
						filter: {}
					},
					{
						xtype: 'gridcolumn',
						header: 'Tanggal',
						dataIndex: 'tgl_sp3d',
						width: 80,
						renderer: function(v, params, record)
						{
							/* var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.tgl_sp3d + "</div>";
							return str; */
							return ShowDate(record.data.tgl_sp3d);
						},
						filter: {}
					},
					{
						xtype: 'gridcolumn',
						header: 'Unit Kerja',
						dataIndex: 'unitkerja',
						width: 120,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.unitkerja + "</div>";
							return str;
						},
						filter: {}
					},
					{
						xtype: 'gridcolumn',
						header: 'Keperluan',
						dataIndex: 'keterangan',
						width: 180,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.keterangan + "</div>";
							return str;
						},
						filter: {}
					},
					{
						xtype: 'gridcolumn',
						header: 'Jumlah',
						dataIndex: 'jumlah',
						width: 90,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;text-align: right;'>" + formatCurrencyDec(record.data.jumlah) + "</div>";
							return str;
						},
						filter: {}
					},
					chkApproved1SP3DForm,
					chkApproved2SP3DForm,
					chkApproved3SP3DForm,
					chkApproved4SP3DForm,
					chkApproved5SP3DForm,
					{
						xtype: 'gridcolumn',
						id:'colurut',
						header: 'Tahap Approved',
						dataIndex: 'urut_app',
						width: 70,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;text-align: right;'>" + record.data.urut_app + "</div>";
							return str;
						},
						filter: {}
					},
					chkApprovedSP3DForm
				]
			),
			tbar:
			[
				{
					xtype: 'button',
					id: 'btnEditSP3DForm',
					iconCls: 'Edit_Tr',
					text: 'Edit Data',
					handler: function()
					{
						if(rowselectGListSP3DForm !== undefined)
						{
							showWinFormEntrySP3DForm(rowselectGListSP3DForm.data);
						}
						else
						{
							showWinFormEntrySP3DForm();
						}
					}
				},
				{ xtype: 'tbseparator' },
				{ xtype: 'tbtext', text: 'Tanggal dari : ', cls: 'left-label', width: 75 },
				{
					xtype: 'datefield',
					id: 'dtpFilterTgl1SP3DForm',
					format: 'd/M/Y',
					value: now.format('d/M/Y'),
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataGListSP3DForm();
							} 						
						}
					}
				},
				{ xtype: 'tbtext', text: 's/d', style: 'text-align: center;', width: 35 },
				{
					xtype: 'datefield',
					id: 'dtpFilterTgl2SP3DForm',
					format: 'd/M/Y',
					value: now.format('d/M/Y'),
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataGListSP3DForm();
							} 						
						}
					}
				}
				,{ xtype: 'tbseparator',
					hidden:true },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved1SP3DForm',
					boxLabel: 'Level 1',
					hidden:true
				},
				// { xtype: 'tbfill' },
				{ xtype: 'tbseparator',
					hidden:true },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved2SP3DForm',
					boxLabel: 'Level 2',
					hidden:true
				},
				// { xtype: 'tbfill' },
				{ xtype: 'tbseparator',
					hidden:true },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved3SP3DForm',
					boxLabel: 'Level 3',
					hidden:true
				},
				// { xtype: 'tbfill' },
				{ xtype: 'tbseparator',
					hidden:true },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved4SP3DForm',
					boxLabel: 'Level 4',
					hidden:true
				},
				{ xtype: 'tbseparator',
					hidden:true },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved5SP3DForm',
					boxLabel: 'Level 5',
					hidden:true
				},
				{ xtype: 'tbseparator' },
				{ xtype: 'tbtext', text: 'Tahap Approved : ', cls: 'left-label', width: 90 },
				{
					xtype: 'textfield',
					id: 'txtFilterThpappForm',
					width: 60,
					listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									RefreshDataGListSP3DForm(getCriteriaGListSP3DForm());				
								} 						
							}
						}
				}

			],
			bbar: new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsGListSP3DForm,
					pageSize: countGListSP3DForm,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: {forceFit: true} 
		}
	);
	
	var pnlSP3dForm = new Ext.Panel(
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: 'PPD',          
			border: false,  
			closable: true,
			iconCls: 'Pencairan',
			margins: '0 5 5 0',
			items: [grdListSP3DForm],
			tbar:
			[
				{ xtype: 'tbtext', text: 'Nomor : ', cls: 'left-label', width: 45 },
				{
					xtype: 'textfield',
					id: 'txtFilterNoSP3DForm',
					width: 120,
					listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									RefreshDataGListSP3DForm(getCriteriaGListSP3DForm());				
								} 						
							}
						}
				},
				{ xtype: 'tbseparator' },
				{ xtype: 'tbtext', text: 'Unit Kerja'+' : ', cls: 'left-label', width: 75 },
				mCboFilterUnitKerjaSP3DForm(),
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApprovedSP3DForm',
					boxLabel: 'Approved',
					listeners: 
					{
						check: function()
						{
						   RefreshDataGListSP3DForm();
						}
					}
				},
				{ xtype: 'tbfill' },
				{
					id: 'btnRefreshSP3DForm',
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function()
					{
						RefreshDataGListSP3DForm(getCriteriaGListSP3DForm());
					}
				}
			]
		}
	);
	
	return pnlSP3dForm;
}

function mCboFilterUnitKerjaSP3DForm()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	var dsCboFilterUnitKerjaSP3DForm = new WebApp.DataStore({ fields: Field });
	/* dsCboFilterUnitKerjaSP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 250, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target: 'viCboUnitKerja',
				// param: "kdunit=" + gstrListUnitKerja
				param: ''
			} 
		}
	);		 */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboFilterUnitKerjaSP3DForm.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboFilterUnitKerjaSP3DForm.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboFilterUnitKerjaSP3DForm.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
	var cboFilterUnitKerjaSP3DForm = new Ext.form.ComboBox
	(
		{
			id: 'cboFilterUnitKerjaSP3DForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: 'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja',			
			align: 'right',
			anchor: '100%',
			store: dsCboFilterUnitKerjaSP3DForm,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCboFilterUnitKerjaSP3DForm = b.data.kd_unit ;
					RefreshDataGListSP3DForm();
				} 
			}
		}
	);	
	
	return cboFilterUnitKerjaSP3DForm;
}

function RefreshDataGListSP3DForm_lama()
{
	var app=0;
	if( Ext.getCmp('chkFilterApprovedSP3DForm').getValue() == true){
		app=1;
	}
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPPD/LoadPPD",
		params: {
			no_sp3d 		: Ext.getCmp('txtFilterNoSP3DForm').getValue(),
			kd_unit_kerja 	: Ext.getCmp('cboFilterUnitKerjaSP3DForm').getValue(),
			approved		: app,
			tgl_awal		:  Ext.getCmp('dtpFilterTgl1SP3DForm').getValue(),
			tgl_akhir		:  Ext.getCmp('dtpFilterTgl2SP3DForm').getValue(),
			tahap_approve	:  Ext.getCmp('txtFilterThpappForm').getValue()
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGListSP3DForm.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGListSP3DForm.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGListSP3DForm.add(recs);
				grdListSP3DForm.getView().refresh();
				console.log(dsGListSP3DForm.data);
			} else {
				ShowPesanError('Gagal menampilkan data PPD!', 'Error');
			};
		},
		callback:function(){
			var gridListPPD=Ext.getCmp('grdListSP3DForm').getStore().data.items;
				for(var i=0,iLen=gridListPPD.length; i<iLen;i++){
					console.log(gridListPPD[i].data.app_sp3d);
					if(gridListPPD[i].data.app_sp3d == 1){
						gridListPPD[i].data.app_sp3d_temp = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grdListSP3DForm').getStore().data.items);
			Ext.getCmp('grdListSP3DForm').getView().refresh();
			
		}
	});
	
	return dsGListSP3DForm;
}

function RefreshDataGListSP3DForm()
{
	var criteria = GetCriteriaGridUtama();
	console.log(criteria);
	dsGListSP3DForm.removeAll();
	dsGListSP3DForm.load({
		params:{
			Skip: 0,
			Take: countGListSP3DForm,
			Sort: '',
			Sortdir: 'ASC',
			target: 'vi_viewdata_ppd',
			param: criteria
		},
		callback:function(){
			var gridListPPD=Ext.getCmp('grdListSP3DForm').getStore().data.items;
				for(var i=0,iLen=gridListPPD.length; i<iLen;i++){
					console.log(gridListPPD[i].data.app_sp3d);
					if(gridListPPD[i].data.app_sp3d == 1){
						gridListPPD[i].data.app_sp3d_temp = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grdListSP3DForm').getStore().data.items);
			Ext.getCmp('grdListSP3DForm').getView().refresh();
		}
	});
    return dsGListSP3DForm; 
}

function GetCriteriaGridUtama(){
	var criteria = '';
	var criteria_approved = '';
	var criteria_no_sp3d = '';
	var criteria_tahap_approve = '';
	
	var app=0;
	if( Ext.getCmp('chkFilterApprovedSP3DForm').getValue() == true){
		app=1;
	}
	
	criteria_approved = " and a.app_sp3d ='"+ app +"' ";
	
	if (Ext.getCmp('txtFilterNoSP3DForm').getValue() != ''){
		criteria_no_sp3d = " and a.no_sp3d_rkat ='"+ Ext.getCmp('txtFilterNoSP3DForm').getValue() +"' ";
	}

	if (Ext.getCmp('txtFilterThpappForm').getValue() != ''){
		criteria_tahap_approve = " a.urut_app ='"+ Ext.getCmp('txtFilterThpappForm').getValue()+"' ";
	}else{
		criteria_tahap_approve = "nol";
	}
	
	
	if (Ext.getCmp('cboFilterUnitKerjaSP3DForm').getValue() == '000' || Ext.getCmp('cboFilterUnitKerjaSP3DForm').getValue() == 000){
		criteria = "  a.tgl_sp3d_rkat between '" + Ext.get('dtpFilterTgl1SP3DForm').getValue()+ "' and '" + Ext.get('dtpFilterTgl2SP3DForm').getValue()+ "' "+criteria_no_sp3d+"  "+criteria_approved+" || "+criteria_tahap_approve+" ";
	}else{
		criteria = "  a.tgl_sp3d_rkat between '" + Ext.get('dtpFilterTgl1SP3DForm').getValue()+ "' and '" + Ext.get('dtpFilterTgl2SP3DForm').getValue()+ "' "+criteria_no_sp3d+" and a.kd_unit_kerja ='"+Ext.getCmp('cboFilterUnitKerjaSP3DForm').getValue()+"'  "+criteria_approved+" || "+criteria_tahap_approve+" ";
		
	}
	
	return criteria; 
}

function getCriteriaGListSP3DForm()
{
	var strKriteria = "";
	
	if(Ext.getCmp('txtFilterNoSP3DForm').getValue() != "" || Ext.getCmp('txtFilterNoSP3DForm').getValue() != undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "no=" + Ext.getCmp('txtFilterNoSP3DForm').getValue();
	}
	//alert(Ext.getCmp('cboFilterUnitKerjaSP3DForm').getValue() +' '+ selectCboFilterUnitKerjaSP3DForm +' ' )
	
	if(selectCboFilterUnitKerjaSP3DForm !== undefined)
	{
	
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectCboFilterUnitKerjaSP3DForm;
	}else
	{
	 if(strKriteria != "")
		{
			strKriteria += "&";
		}
	// strKriteria += "unit=" +  gstrListUnitKerja;
	}
	
	if(Ext.getCmp('chkFilterApprovedSP3DForm').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "approved=1";
	}else{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "approved=0";
	}
	
	if(Ext.getCmp('dtpFilterTgl1SP3DForm').getValue() != "" && Ext.getCmp('dtpFilterTgl2SP3DForm').getValue() != "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		// strKriteria += "tglawal=" + ShowDate(Ext.getCmp('dtpFilterTgl1SP3DForm').getValue());
		// strKriteria += "&tglakhir=" + ShowDate(Ext.getCmp('dtpFilterTgl2SP3DForm').getValue());		
	}

	if(Ext.getCmp('chkFilterApproved1SP3DForm').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "Level1=1";
	}
	// else{
	// 	if(strKriteria != "")
	// 	{
	// 		strKriteria += "&";
	// 	}
	// 	strKriteria += "Level1=0";
	// }

	if(Ext.getCmp('chkFilterApproved2SP3DForm').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "Level2=1";
	}
	// else{
	// 	if(strKriteria != "")
	// 	{
	// 		strKriteria += "&";
	// 	}
	// 	strKriteria += "Level2=0";
	// }

	if(Ext.getCmp('chkFilterApproved3SP3DForm').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "Level3=1";
	}
	// else{
	// 	if(strKriteria != "")
	// 	{
	// 		strKriteria += "&";
	// 	}
	// 	strKriteria += "Level3=0";
	// }

	if(Ext.getCmp('chkFilterApproved4SP3DForm').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "Level4=1";
	}
	// else{
	// 	if(strKriteria != "")
	// 	{
	// 		strKriteria += "&";
	// 	}
	// 	strKriteria += "Level4=0";
	// }
	if(Ext.getCmp('chkFilterApproved5SP3DForm').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "Level5=1";
	}

	if(Ext.getCmp('txtFilterThpappForm').getValue() != "" || Ext.getCmp('txtFilterThpappForm').getValue() != undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "urutapp=" + Ext.getCmp('txtFilterThpappForm').getValue();
	}
	return strKriteria;
}

function showWinFormEntrySP3DForm(rowdata)
{
	console.log(rowdata);
	var pnlTotDetailJumEntrySP3DForm = new Ext.Panel
	(
		{
			id: 'pnlTotDetailJumEntrySP3DForm',
			layout: 'hbox',
			border: false,	
			region: 'south',
			height: 60,
			bodyStyle: 'padding: 10px 18px 0 10px;border-top: solid 1px #DFE8F6;',
			defaults:
			{
				flex: 1
			},
			items:
			[
				{
					xtype: 'panel',
					layout: 'form',
					border: false,
					width: 400,
					defaults: { anchor: '100%', readOnly: true },
					hidden:true,
					items:
					[
						{ 
							xtype: 'checkboxgroup',
							id: 'cgApprovedEntrySP3DForm',					
							fieldLabel: 'Telah disahkan oleh',
							itemCls: 'x-check-group-alt',	
							disabled: true,
							readOnly: true,							
							columns: 4,
							items: 
							[
								{boxLabel: 'Level 1', id: 'chkAppKajurEntrySP3DForm', NAME: 'chkAppKajurEntrySP3DForm'},
								{boxLabel: 'Level 2', id: 'chkAppDekanEntrySP3DForm', NAME: 'chkAppDekanEntrySP3DForm'},
								{boxLabel: 'Level 3', id: 'chkAppBAKUEntrySP3DForm', NAME: 'chkAppBAKUEntrySP3DForm'},
								{boxLabel: 'Level 4', id: 'chkAppWR2EntrySP3DForm', NAME: 'chkAppWR2EntrySP3DForm'},
								{boxLabel: 'Level 5', id: 'chkAppYayasanEntrySP3DForm', NAME: 'chkAppYayasanEntrySP3DForm'}
							]
						}
						// ,
						// {
							// xtype: 'textfield',
							// id: 'txtApproverEntrySP3D',
							// fieldLabel: 'Disahkan oleh'
						// },
						// {
							// xtype: 'textfield',
							// id: 'txtJabatanEntrySP3D',
							// fieldLabel: 'Jabatan'
						// }
					]
				},
				{
					xtype: 'displayfield',
					id: 'dspTotDetailJumEntrySP3DForm',
					value: 'Total Biaya Rp. ',
					style: 'padding-left: 50px;font-weight: bold;font-size: 14px;'					
				},
				{
					xtype: 'textfield',
					id: 'txtTotDetailJumEntrySP3DForm',
					readOnly: true,
					width: 198,
					style: 'font-weight: bold;text-align: right;'
				}
			]
		}
	);
	
	winFormEntrySP3DForm = new Ext.Window
	(
		{
			id: 'winFormEntrySP3DForm',
			title: 'PPD',
			closeAction: 'destroy',
			width: 800,
			height: 500,//600,
			resizable: false,
			layout: 'border',
			iconCls: 'Pencairan',
			modal: true,
			items: 
			[
				getFormHeaderSP3DForm(rowdata),
				getGridDetailEntrySP3DForm(),
				pnlTotDetailJumEntrySP3DForm
			],
			listeners:
			{
				'beforedestroy': function()
				{ 
					RefreshDataGListSP3DForm(getCriteriaGListSP3DForm()); 
					rowselectGListSP3DForm = undefined;
				}
			}
		}
	);
	
	winFormEntrySP3DForm.show();
	
	if(rowdata !== undefined)
	{
		InitDataEntrySP3DForm(rowdata);
	}
	else
	{
		AddNewEntrySP3DForm();
	}
}

function getFormHeaderSP3DForm(rowdata)
{
	console.log(rowdata);
	/*
	//SEMENTARA DIKOMENTAR TERLEBIH DAHULU
	//UNTUK APA CONDITIONAL INI?
	if (strKdUser == gstrHakUnapp)
	{ 

	*/
		var pnlFormEntrySP3DForm = new Ext.FormPanel
		(
			{
				id: 'pnlFormEntrySP3DForm',
				border: false,
				region: 'north',
				height: 210,//280,			
				items:
				[
					{
						xtype: 'fieldset',
						layout: 'form',
						style: 'margin: 5px;',					
						defaults: { labelWidth: 120 },
						items:
						[
							{
								xtype: 'compositefield',
								fieldLabel: 'Nomor',
								defaults: { anchor: '100%', flex: 1 },
								items: 
								[
									{
										xtype: 'textfield',
										id: 'txtNoEntrySP3DForm',
										disabled: true,
										width: 120
									},
									// {
									// 	xtype: 'textfield',
									// 	id: 'txtaliasNoEntrySP3DForm',
									// 	disabled: true,
									// 	width: 120,
									// 	hidden: true
									// },
									{ xtype: 'label', cls: 'left-label', width: 80, text: 'Tanggal : ' },
									{
										xtype: 'datefield',
										id: 'dtpTglEntrySP3DForm',
										format: 'd/M/Y',
										readOnly: true,
										width: 120,
										value: now.format('d/M/Y')
									},
									{
										xtype: 'checkbox',
										id: 'chkApprovedEntrySP3DForm',
										disabled: true,
										hidden: true,
										readOnly: true,
										boxLabel: 'Approved'
									}
								]
							},
							mCboUnitKerjaEntrySP3DForm(),
							{
								xtype: 'textarea',
								id: 'txtaKetEntrySP3DForm',
								autoCreate: {tag: 'input', type: 'text', size: '40', autocomplete: 'off', maxlength: '350'},
								//enforceMaxLength : true,
								fieldLabel: 'Keterangan',
								anchor: '100%',
								height: 40
							},
							{
								xtype: 'compositefield',
								fieldLabel: 'Program (KUPPA)',
								defaults: { anchor: '100%', flex: 1 },
								hidden:true,
								items: 
								[
									{
										xtype: 'textfield',
										id: 'txtRKATEntrySP3DForm',
										readOnly: true,
										width: 60,
										// hidden: true
									},
									{
										xtype: 'textfield',
										id: 'txtJnsRKATEntrySP3DForm',
										readOnly: true,
										width: 60,
										hidden: true
									},
									{
										xtype: 'textfield',
										id: 'txtNoProgEntrySP3DForm',
										readOnly: true,
										hidden:true,
										width: 60
									},								
									{
										xtype: 'textfield',
										id: 'txtNmProgEntrySP3DForm',
										readOnly: true,
										hidden:true,
									},
									{ xtype: 'label', cls: 'left-label', forId: '', text: 'Prioritas : ', width: 80 },
									{
										xtype: 'numberfield',
										id: 'numPriorEntrySP3DForm',
										readOnly: true,
										hidden:true,
										style: 'text-align: right',
										width: 30
									}
								]
							},
							{
								xtype: 'textarea',
								id: 'txtaKegEntrySP3DForm',
								fieldLabel: 'Kegiatan',
								readOnly: false,
								height: 40,
								anchor: '100%',
								hidden:true
							},
							{
								xtype: 'textfield',
								id: 'txtJumBiayaEntrySP3DForm',
								readOnly: true,
								anchor: '100%',
								fieldLabel: 'Jumlah',
								style: 'text-align: right',
							},
							{
							xtype: 'compositefield',
							fieldLabel: 'Path file :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compPathfileSP3DForm',
							id: 'compPathfileSP3DForm',
							items: 
							[
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',
									width: 480,
									readOnly:true,
									name: 'txtPathfileSP3DForm',
									id: 'txtPathfileSP3DForm'
								},
								{
									xtype:'button',
									text:'Upload',
									tooltip: 'Upload',
									id:'btnuplfileSP3DForm',
									name:'btnuplfileSP3DForm',
									handler:function()
									{
										FormLookupUploadfile(2,'uploads')
									}
								},
								{
									xtype:'button',
									text:'View',
									tooltip: 'View',
									id:'btnvwfileSP3DForm',
									name:'btnvwfileSP3DForm',
									handler:function()
									{
										ViewFileSP3DForm()
									}
								}				
							]
						}
							//rdJalur
						]
					}
				],
				tbar:
				[
					{
						xtype: 'button',
						id: 'btnAddEntrySP3DForm',
						iconCls: 'add',
						text: 'Tambah',
						handler: function(){ AddNewEntrySP3DForm(); }
					},
					{
						xtype: 'button',
						id: 'btnSaveEntrySP3DForm',
						iconCls: 'save',
						text: 'Simpan',
						handler: function()
						{
							
							Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(true);
							Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(true);
							SaveDataEntrySP3DForm(false);
						}
					},
					{
						xtype: 'button',
						id: 'btnSaveExitEntrySP3DForm',
						iconCls: 'saveexit',
						text: 'Simpan & Keluar',
						handler: function()
						{
							
							Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(true);
							Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(true);
							SaveDataEntrySP3DForm(true);
						}
					},
					{
						xtype: 'button',
						id: 'btnDelEntrySP3DForm',
						iconCls: 'remove',
						text: 'Hapus',
						handler: function()
						{
							
							HapusEntryEntrySP3DForm()
						}
					},
					{ xtype: 'tbseparator' },
					{
						xtype: 'button',
						id: 'btnLookupRKATEntrySP3DForm',
						iconCls: 'find',
						text: 'Lookup RAB',
						handler: function()
						{						
							
							if(selectCboUnitKerjaEntrySP3DForm !== undefined && Ext.getCmp('dtpTglEntrySP3DForm').getValue() !== "")
							{ 
									var obj = 
									{
										RKAT: 'txtRKATEntrySP3DForm',
										JNSRKAT: 'txtJnsRKATEntrySP3DForm',
										NOPROG: 'txtNoProgEntrySP3DForm',
										NMPROG: 'txtNmProgEntrySP3DForm',
										PRIOR: 'numPriorEntrySP3DForm',
										KEG: 'txtaKegEntrySP3DForm',
										JUM: 'txtJumBiayaEntrySP3DForm',
										THNANGG: TahunAngg
									};
									var criteria = "tahun=" + thn;
									criteria += "&unit=" + selectCboUnitKerjaEntrySP3DForm;
									criteria += "&jnsrkat=1";
		
									FormLookupRKATAll_Unit(obj, criteria,selectCboUnitKerjaEntrySP3DForm, thn);
								
							}
							else
							{
								ShowPesanWarning("Unit Kerja atau tanggal belum dipilih.", "Lookup RAB");
							} 
						}
					},
					{ xtype: 'tbseparator' },
					{
						xtype: 'button',
						id: 'btnApproveEntrySP3DForm',
						iconCls: 'approve',
						text: 'Approve',
						handler: function()
						{
							if(ValidasiAppEntrySP3DForm() == 1)
							{
								stris_app=1
								ApproveEntrySP3DForm()
							}
						}
					},
					{
						xtype: 'button',
						id: 'btnUnApproveEntrySP3DForm',
						iconCls: 'remove',
						text: 'UnApprove',
						handler: function()
						{
							stris_app=1;
							UnApproveEntrySP3DForm()
						}
					},
					{
						xtype: 'button',
						id: 'btnBatalEntrySP3DForm',
						iconCls: 'remove',
						text: 'Batal PPD',
						handler: function()
						{
							stris_app=1;
							BatalEntrySP3DForm()
						}
					},
					{
						xtype: 'button',
						id: 'btnViewEntrySP3DForm',
						iconCls: 'find',
						text: 'Info Approved',
						handler: function()
						{
							FormLookup_viewUserApp(
								Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
								selectCboUnitKerjaEntrySP3DForm,
								Ext.getCmp('txtNoEntrySP3DForm').getValue(), 
								Ext.getCmp('dtpTglEntrySP3DForm').getValue()
							);
						}
					},
					{ xtype: 'tbfill'},
					{
						xtype: 'button',
						id: 'btnPrintEntrySP3DForm',
						iconCls: 'print',
						text: 'Cetak',
						handler: function()
						{
							if(Validasi_SP3DForm2() == 1)
							{
								CetakPPD();
							}
						}
					}
				]
			}
		);
	/* 
	//SEMENTARA DIKOMENTAR TERLEBIH DAHULU
	}
	
	else
	{
		var pnlFormEntrySP3DForm = new Ext.FormPanel
		(
			{
				id: 'pnlFormEntrySP3DForm',
				border: false,
				region: 'north',
				height: 210,			
				items:
				[
					{
						xtype: 'fieldset',
						layout: 'form',
						style: 'margin: 5px;',					
						defaults: { labelWidth: 120 },
						items:
						[
							{
								xtype: 'compositefield',
								fieldLabel: 'Nomor',
								defaults: { anchor: '100%', flex: 1 },
								items: 
								[
									{
										xtype: 'textfield',
										id: 'txtNoEntrySP3DForm',
										disabled: true,
										width: 120
									},
									
									{ xtype: 'label', cls: 'left-label', width: 80, text: 'Tanggal : ' },
									{
										xtype: 'datefield',
										id: 'dtpTglEntrySP3DForm',
										format: 'd/M/Y',
										readOnly: true,
										width: 120,
										value: now.format('d/M/Y')
									},
									{
										xtype: 'checkbox',
										id: 'chkApprovedEntrySP3DForm',
										disabled: true,
										hidden: true,
										readOnly: true,
										boxLabel: 'Approved'
									}
								]
							},
							mCboUnitKerjaEntrySP3DForm(),
							{
								xtype: 'textarea',
								id: 'txtaKetEntrySP3DForm',
								autoCreate: {tag: 'input', type: 'text', size: '40', autocomplete: 'off', maxlength: '1023'},
								fieldLabel: 'Keterangan',
								anchor: '100%',
								height: 40
							},
							{
								xtype: 'compositefield',
								fieldLabel: 'Program (KUPPA)',
								defaults: { anchor: '100%', flex: 1 },
								hidden: true,
								items: 
								[
									{
										xtype: 'textfield',
										id: 'txtRKATEntrySP3DForm',
										readOnly: true,
										width: 60,
										hidden: true
									},
									{
										xtype: 'textfield',
										id: 'txtJnsRKATEntrySP3DForm',
										readOnly: true,
										width: 60,
										hidden: true
									},
									{
										xtype: 'textfield',
										id: 'txtNoProgEntrySP3DForm',
										readOnly: true,
										width: 60
									},								
									{
										xtype: 'textfield',
										id: 'txtNmProgEntrySP3DForm',
										readOnly: true
									},
									{ xtype: 'label', cls: 'left-label', forId: '', text: 'Prioritas : ', width: 80 },
									{
										xtype: 'numberfield',
										id: 'numPriorEntrySP3DForm',
										readOnly: true,
										style: 'text-align: right',
										width: 30
									}
								]
							},
							{
								xtype: 'textarea',
								id: 'txtaKegEntrySP3DForm',
								fieldLabel: 'Kegiatan',
								readOnly: false,
								height: 40,
								anchor: '100%',
								hidden:true
							},
							{
								xtype: 'textfield',
								id: 'txtJumBiayaEntrySP3DForm',
								readOnly: true,
								anchor: '100%',
								fieldLabel: 'Jumlah',
								style: 'text-align: right',
							},
							{
							xtype: 'compositefield',
							fieldLabel: 'Path file :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compPathfileSP3DForm',
							id: 'compPathfileSP3DForm',
							items: 
							[
								{
									xtype: 'textfield',
									flex: 1,
									fieldLabel: '',
									width: 480,
									readOnly:true,
									name: 'txtPathfileSP3DForm',
									id: 'txtPathfileSP3DForm'
								},
								{
									xtype:'button',
									text:'Upload',
									tooltip: 'Upload',
									id:'btnuplfileSP3DForm',
									name:'btnuplfileSP3DForm',
									handler:function()
									{
										FormLookupUploadfile(2,'uploads')
									}
								},
								{
									xtype:'button',
									text:'View',
									tooltip: 'View',
									id:'btnvwfileSP3DForm',
									name:'btnvwfileSP3DForm',
									handler:function()
									{
										ViewFileSP3DForm()
									}
								}				
							]
						}
							//rdJalur
						]
					}
				],
				tbar:
				[
					{
						xtype: 'button',
						id: 'btnAddEntrySP3DForm',
						iconCls: 'add',
						text: 'Tambah',
						handler: function(){ AddNewEntrySP3DForm(); }
					},
					{
						xtype: 'button',
						id: 'btnSaveEntrySP3DForm',
						iconCls: 'save',
						text: 'Simpan',
						handler: function()
						{
							
							Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(true);
							Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(true);
							SaveDataEntrySP3DForm(false);
						}
					},
					{
						xtype: 'button',
						id: 'btnSaveExitEntrySP3DForm',
						iconCls: 'saveexit',
						text: 'Simpan & Keluar',
						handler: function()
						{
							
							Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(true);
							Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(true);
							SaveDataEntrySP3DForm(true);
						}
					},
					{
						xtype: 'button',
						id: 'btnDelEntrySP3DForm',
						iconCls: 'remove',
						text: 'Hapus',
						handler: function()
						{

							HapusEntryEntrySP3DForm()
						}
					},
					{ xtype: 'tbseparator' },
					{
						xtype: 'button',
						id: 'btnLookupRKATEntrySP3DForm',
						iconCls: 'find',
						text: 'Lookup '+gstrrkat,
						handler: function()
						{						
							if(selectCboUnitKerjaEntrySP3DForm !== undefined && Ext.getCmp('dtpTglEntrySP3DForm').getValue() !== "")
							{
								// if(dsGridDetailEntrySP3DForm.getCount() == 0)
								// {
									var obj = 
									{
										RKAT: 'txtRKATEntrySP3DForm',
										JNSRKAT: 'txtJnsRKATEntrySP3DForm',
										NOPROG: 'txtNoProgEntrySP3DForm',
										NMPROG: 'txtNmProgEntrySP3DForm',
										PRIOR: 'numPriorEntrySP3DForm',
										KEG: 'txtaKegEntrySP3DForm',
										JUM: 'txtJumBiayaEntrySP3DForm',
										THNANGG: TahunAngg
									};
									var criteria = "tahun=" + thn;
									criteria += "&unit=" + selectCboUnitKerjaEntrySP3DForm;
									criteria += "&jnsrkat=1";
		
									FormLookupRKATAll_Unit(obj, criteria,selectCboUnitKerjaEntrySP3DForm, thn);
								// }else
								// {
								// 	ShowPesanWarning(gstrSp3d+" tidak boleh lebih dari satu", "Lookup "+gstrSp3d);		
								// }

							}
							else
							{
								ShowPesanWarning(gstrSatker+" atau tanggal belum dipilih.", "Lookup "+gstrSp3d);
							}
						}
					},
					{ xtype: 'tbseparator' },
					{
						xtype: 'button',
						id: 'btnApproveEntrySP3DForm',
						iconCls: 'approve',
						text: 'Approve',
						handler: function()
						{
							if(ValidasiAppEntrySP3DForm() == 1)
							{
								stris_app=1;
								ApproveEntrySP3DForm()
							}
						}
					},
					{
						xtype: 'button',
						id: 'btnUnApproveEntrySP3DForm',
						iconCls: 'remove',
						text: 'UnApprove',
						hidden: true,
						handler: function()
						{
							stris_app=1;
							UnApproveEntrySP3DForm()
						}
					},
					{
						xtype: 'button',
						id: 'btnBatalEntrySP3DForm',
						iconCls: 'remove',
						text: 'Batal '+gstrSp3d,
						handler: function()
						{
							stris_app=1;
							BatalEntrySP3DForm()
						}
					},
					{
						xtype: 'button',
						id: 'btnViewEntrySP3DForm',
						iconCls: 'find',
						text: 'Info Approved',
						handler: function()
						{
							FormLookup_viewUserApp(rowdata.NO_SP3D);
						}
					},
					{ xtype: 'tbfill'},
					{
						xtype: 'button',
						id: 'btnPrintEntrySP3DForm',
						iconCls: 'print',
						text: 'Cetak',
						handler: function()
						{
							if(Validasi_SP3DForm2() == 1)
							{
								var criteria = GetCriteria_SP3DForm2();
								ShowReport('', '011502', criteria);		
								
								
							}
						}
					}
				]
			}
		);
	} */
	return pnlFormEntrySP3DForm;
}



function mCboUnitKerjaEntrySP3DForm()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsCboUnitKerjaEntrySP3DForm = new WebApp.DataStore({ fields: Field });
	/* dsCboUnitKerjaEntrySP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 250, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target: 'viCboUnitKerja',
				// param: "kdunit=" + gstrListUnitKerja
				param: ''
			} 
		}
	);		 */

  var cboUnitKerjaEntrySP3DForm = new Ext.form.ComboBox
	(
		{
			id: 'cboUnitKerjaEntrySP3DForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: 'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja',			
			align: 'right',
			anchor: '100%',
			store: dsCboUnitKerjaEntrySP3DForm,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCboUnitKerjaEntrySP3DForm = b.data.kd_unit ;
				} 
			}
		}
	);	
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaLap",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaEntrySP3DForm.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaEntrySP3DForm.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaEntrySP3DForm.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
	return cboUnitKerjaEntrySP3DForm;
}

function AddNewEntrySP3DForm()
{
	vAddNewDataEntrySP3DForm = true;
	Ext.getCmp('txtNoEntrySP3DForm').setValue('');
	// Ext.getCmp('txtaliasNoEntrySP3DForm').setValue('');
	Ext.getCmp('dtpTglEntrySP3DForm').setValue(now.format('d/M/Y'));
	Ext.getCmp('chkApprovedEntrySP3DForm').setValue(false);	
	Ext.getCmp('txtaKetEntrySP3DForm').setValue('');
	Ext.getCmp('txtRKATEntrySP3DForm').setValue('');
	Ext.getCmp('txtJnsRKATEntrySP3DForm').setValue('');
	Ext.getCmp('txtNoProgEntrySP3DForm').setValue('');
	Ext.getCmp('txtNmProgEntrySP3DForm').setValue('');
	Ext.getCmp('numPriorEntrySP3DForm').setValue('');
	Ext.getCmp('txtaKegEntrySP3DForm').setValue('');
	Ext.getCmp('txtJumBiayaEntrySP3DForm').setValue(0);
	Ext.getCmp('txtTotDetailJumEntrySP3DForm').setValue(0);
	Ext.getCmp('cboUnitKerjaEntrySP3DForm').setValue('');
	// Ext.getCmp('cboUnitKerjaEntrySP3DForm').reset();
	Ext.getCmp('chkAppKajurEntrySP3DForm').setValue(false);
	Ext.getCmp('chkAppDekanEntrySP3DForm').setValue(false);
	Ext.getCmp('chkAppBAKUEntrySP3DForm').setValue(false);
	Ext.getCmp('chkAppWR2EntrySP3DForm').setValue(false);
	Ext.getCmp('chkAppYayasanEntrySP3DForm').setValue(false);
	Ext.getCmp('txtPathfileSP3DForm').setValue('');
	//Ext.get('txtPathfileSP3DForm').dom.value = '';
	//Ext.getCmp('rdJalurKBS').setValue("1");
	//Ext.getCmp('txtApproverEntrySP3D').setValue('');
	//Ext.getCmp('txtJabatanEntrySP3D').setValue('');
	selectCboUnitKerjaEntrySP3DForm = undefined;
	selectCboUnitKerjaEntrySP3DFormTempEdit = '';
	 jnsrkatk='0';
	 jnsrkatr='0';
	 jnsrkato='0';

	dsGridDetailEntrySP3DForm.removeAll();

	Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(false);
	Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(false);
	stris_app=0;
}

function InitDataEntrySP3DForm(rowdata)
{	
	console.log(rowdata);
	vAddNewDataEntrySP3DForm = false;	
	Ext.getCmp('txtNoEntrySP3DForm').setValue(rowdata.no_sp3d);
	Ext.getCmp('dtpTglEntrySP3DForm').setValue(ShowDate(rowdata.tgl_sp3d));	
	dtTglSP3DAsal = ShowDate(rowdata.tgl_sp3d);
	TahunAngg = rowdata.tahun_anggaran_ta
	Ext.getCmp('txtaKetEntrySP3DForm').setValue(rowdata.keterangan);
	Ext.getCmp('txtRKATEntrySP3DForm').setValue(rowdata.rkat);
	Ext.getCmp('txtJnsRKATEntrySP3DForm').setValue(rowdata.kd_jns_rkat_jrka);
	Ext.getCmp('txtNoProgEntrySP3DForm').setValue(rowdata.no_program_prog);
	Ext.getCmp('txtaKegEntrySP3DForm').setValue(rowdata.kegiatan);
	Ext.getCmp('txtNmProgEntrySP3DForm').setValue(rowdata.nama_program_prog);
	Ext.getCmp('numPriorEntrySP3DForm').setValue(rowdata.prioritas);
	//JML_ANGGARAN	
	Ext.getCmp('txtJumBiayaEntrySP3DForm').setValue(formatCurrencyDec(rowdata.jumlah));
	Ext.getCmp('cboUnitKerjaEntrySP3DForm').setValue(rowdata.unitkerja);
	// Ext.getCmp('cboUnitKerjaEntrySP3DForm').setReadOnly(true);
	selectCboUnitKerjaEntrySP3DForm = rowdata.kd_unit_kerja;
	selectCboUnitKerjaEntrySP3DFormTempEdit = rowdata.kd_unit_kerja;
	
	// RefreshDataGridDetailKorEntrySP3DForm(getCriteriaGridDetailEntrySP3DForm());
	RefreshDataGridDetailKorEntrySP3DForm(
		rowdata.tahun_anggaran_ta,
		rowdata.kd_unit_kerja,
		rowdata.no_sp3d,
		rowdata.tgl_sp3d,
		rowdata.prioritas
	);

	// if(rowdata.RKAT == "Pengembangan")
	// {
		
		// RefreshDataGridDetail1EntrySP3DForm(getCriteriaGridDetailEntrySP3DForm());
	// }
	// else if (rowdata.RKAT == "Rutin")
	// {
		// RefreshDataGridDetail2EntrySP3DForm(getCriteriaGridDetailEntrySP3DForm());
	// }else
	// {
		// RefreshDataGridDetail3EntrySP3DForm(getCriteriaGridDetailEntrySP3DForm());
	// }
	Ext.getCmp('chkAppKajurEntrySP3DForm').setValue(rowdata.app_level_1);
	Ext.getCmp('chkAppDekanEntrySP3DForm').setValue(rowdata.app_level_2);
	Ext.getCmp('chkAppBAKUEntrySP3DForm').setValue(rowdata.app_level_3);
	Ext.getCmp('chkAppWR2EntrySP3DForm').setValue(rowdata.app_level_4);
	Ext.getCmp('chkAppYayasanEntrySP3DForm').setValue(rowdata.app_level_5);
	
	/*if (rowdata.IS_JALUR==1)
	{
		Ext.getCmp('rdJalurKBS').setValue(true);
		Ext.getCmp('rdJalurBPK').setValue(false);
	}
	else
	{
		Ext.getCmp('rdJalurKBS').setValue(false);
		Ext.getCmp('rdJalurBPK').setValue(true);
	}*/
	Ext.get('txtPathfileSP3DForm').dom.value = rowdata.filepath;
	//Ext.getCmp('txtPathfileSP3DForm').setValue(rowdata.FILEPATH);
	CheckDetailApprovedEntrySP3DForm();
	
	if (rowdata.app_sp3d == 1)
	{
		stris_app=1;	
	}else
	{
		stris_app=0;	
	}

	Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(false);
	Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(false);
	
	
	//set active button approve
	Ext.Ajax.request
	(
		{
			
			url: baseURL + "index.php/anggaran_module/functionPPD/cekappuser",
			params:{
				tahun_anggaran				:TahunAngg,
				no_sp3d						:Ext.get('txtNoEntrySP3DForm').getValue(),
				kd_unit_kerja				:selectCboUnitKerjaEntrySP3DForm,
				tgl_sp3d					:Ext.getCmp('dtpTglEntrySP3DForm').getValue()
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(cst.app == 't'){
						Ext.getCmp('btnApproveEntrySP3DForm').setDisabled(true);
						Ext.getCmp('btnDelRowDetailEntrySP3DForm').setDisabled(true);
					}
				}
				else 
				{
					
				}
			}
		}
	)
	
}

function HapusEntryEntrySP3DForm()
{	
	Ext.Msg.show
	(
		{
			title:'Hapus Data',
			 msg: "Akan menghapus data?" ,
			buttons: Ext.MessageBox.YESNO,
			width:300,
			fn: function (btn)
			{					
				if (btn =='yes')
				{
					Ext.Ajax.request
					(
						{
							url: baseURL + "index.php/anggaran_module/functionPPD/HapusPPD",
							params: getParamSaveEntrySP3DForm(),
							success: function(o)
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true)
								{
									ShowPesanInfo('Data berhasil dihapus','Hapus Data');
									AddNewEntrySP3DForm();
									RefreshDataGListSP3DForm();
								}
								else
								{
									ShowPesanError(cst.pesan ,'Hapus Data');
								}
							}
						}
					)//end Ajax.request
				} // end if btn yes
			}// end fn
		}
	)//end msg.show
    
}

function SaveDataEntrySP3DForm(isSaveExit)
{	
	if(ValidasiSaveEntrySP3DForm() == 1)
	{
		CalcTotalDetailEntrySP3DForm();
		
		Ext.Ajax.request
		(
			{
				
				url: baseURL + "index.php/anggaran_module/functionPPD/SavePPD",
				params:getParamSaveEntrySP3DForm(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// ShowPesanInfo('Data berhasil di simpan','Simpan Data');
						Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(false);
						Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(false);														

						Ext.get('txtNoEntrySP3DForm').dom.value=cst.no_sp3d;
						RefreshDataGListSP3DForm();
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Data berhasil di simpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(isSaveExit == true){
											winFormEntrySP3DForm.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);
					}
					else 
					{
						ShowPesanError('Data tidak berhasil di simpan','Simpan Data');
					}
				}
			}
		)
		
		
		
		
		
		/* if(vAddNewDataEntrySP3DForm)
		{
			
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSaveEntrySP3DForm(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(false);
						Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(false);
						if (cst.success === true) 
						{
							ShowPesanInfo_SP3DFormAll('Data berhasil disimpan.', 'Simpan Data');
							Ext.getCmp('txtNoEntrySP3DForm').setValue(cst.nosp3d);
							
							RefreshDataGridDetailKorEntrySP3DForm(getCriteriaGridDetailEntrySP3DForm());

							if(isSaveExit)
							{ 
								winFormEntrySP3DForm.close(); 
							}
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil disimpan, data tersebut sudah ada', 'Simpan Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil disimpan, ' + cst.pesan, 'Simpan Data');
						}
					}
				}
			);
		}
		else
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params: getParamSaveEntrySP3DForm(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(false);
						Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(false);							
						if (cst.success === true) 
						{
							ShowPesanInfo_SP3DFormAll('Data berhasil di-edit.', 'Edit Data');
							
							RefreshDataGridDetailKorEntrySP3DForm(getCriteriaGridDetailEntrySP3DForm());

							if(isSaveExit){ winFormEntrySP3DForm.close(); }
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di-edit, data tersebut belum ada', 'Edit Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di-edit, ' + cst.pesan, 'Edit Data');
						}
					}
				}
			);
		} */
	}
	else
	{
		Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(false);
		Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(false);							
	}
}

function ApproveEntrySP3DForm() 
{
	Ext.Msg.show
	(
		{
			title: 'Approve',
			msg: 'Apakah Transaksi Ini Akan DiApprove ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) 
			{
				if (btn == 'yes') 
				{
					Ext.Ajax.request(
					{ 
						url: baseURL + "index.php/anggaran_module/functionPPD/approvePPD",
						params: getParamAppEntrySP3DForm(),
						// params: getParamAppEntrySP3DForm(),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								Ext.getCmp('btnSaveEntrySP3DForm').setDisabled(true);
								Ext.getCmp('btnSaveExitEntrySP3DForm').setDisabled(true);
								Ext.getCmp('btnApproveEntrySP3DForm').setDisabled(true);
								Ext.getCmp('btnDelRowDetailEntrySP3DForm').setDisabled(true);
								ShowPesanInfo('Data PPD berhasil diapprove','Approve Data');
                                userSp3D= cst.pesan;
							}
							else
							{
								if(cst.pesan != ''){
									ShowPesanWarning(cst.pesan,'Approve Data');
								}else{
									ShowPesanWarning('Data PPD gagal diapprove!','Approve Data');
								}
							}
							RefreshDataGListSP3DForm();
						}
					})    
				}
			},
			icon: Ext.MessageBox.QUESTION
		}
	);
};

function BatalEntrySP3DForm() 
{
	Ext.Msg.show
	(
		{
			title: 'Batal ',
			msg: 'Apakah Transaksi Ini Akan Dibatalkan ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) 
			{
				if (btn == 'yes') 
				{							
					Ext.Ajax.request
					({	
						url: baseURL + "index.php/anggaran_module/functionPPD/BatalPPD",
						params: getParamBatalEntrySP3DForm(),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);							
							if (cst.success === true) 
							{
								ShowPesanInfo_SP3DFormAll('PPD berhasil dibatalkan.','Batalkan PPD');
								AddNewEntrySP3DForm()
                                userSp3D= cst.pesan;
								RefreshDataGListSP3DForm();
							}
							else
							{
								if(cst.pesan!= ''){
									ShowPesanWarning(cst.pesan, 'Batalkan PPD');
								}else{
									ShowPesanWarning('PPD tidak berhasil dibatalkan!', 'Batalkan PPD');
								}
							}
						}
					})    
				}
			},
			icon: Ext.MessageBox.QUESTION
		}
	);
};

function ApproveGridEntrySP3DForm(rowData) 
{
	Ext.Msg.show
	(
		{
			title: 'Approve',
			msg: 'Apakah Transaksi Ini Akan DiApprove ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) 
			{
				if (btn == 'yes') 
				{							
					Ext.Ajax.request
					({ url: "./Datapool.mvc/UpdateDataObj",
						params: getGridParamAppEntrySP3DForm(rowData),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);							
							if (cst.success === true) 
							{
								ShowPesanInfo_SP3DFormAll('Data berhasil diapprove','Approve Data');								
                                userSp3D= cst.pesan;	
										
							}
							else
							{
								if (intLvlApproval == '4')
								{
									ShowPesanInfo_SP3DFormAll('Data gagal diapprove / Belum di Approve BKU','Approve Data'); 
								}
								else
								{
									ShowPesanInfo_SP3DFormAll('Data gagal diapprove ','Approve Data'); 
								}
							}
							RefreshDataGListSP3DForm(getCriteriaGListSP3DForm()); 
						}
					})    
				}
			},
			icon: Ext.MessageBox.QUESTION
		}
	);
};

function UnApproveEntrySP3DForm() 
{
	Ext.Msg.show
	(
		{
			title: 'Batal ',
			msg: 'Apakah Transaksi Ini Akan UnApprove ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) 
			{
				if (btn == 'yes') 
				{							
					Ext.Ajax.request({ 
						url: baseURL + "index.php/anggaran_module/functionPPD/unapprovePPD",
						params: getParamUnAppEntrySP3DForm(),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);							
							if (cst.success === true) 
							{
								ShowPesanInfo('Data PPD berhasil diUnApprove!','UnApprove PPD');
								Ext.getCmp('btnApproveEntrySP3DForm').setDisabled(false);
								Ext.getCmp('btnUnApproveEntrySP3DForm').setDisabled(true);
								Ext.getCmp('btnDelRowDetailEntrySP3DForm').setDisabled(false);
								// AddNewEntrySP3DForm()
                                userSp3D= cst.pesan;
							}
							else
							{
								if(cst.pesan != ''){
									ShowPesanWarning(cst.pesan,'Unapprove Data');
								}else{
									ShowPesanWarning('Data PPD gagal diapprove!','Unapprove Data');
								}
							}
							
							RefreshDataGListSP3DForm();
						}
					})    
				}
			},
			icon: Ext.MessageBox.QUESTION
		}
	);
};

function ValidasiSaveEntrySP3DForm()
{
	var valid = 1;
	if(Ext.getCmp('dtpTglEntrySP3DForm').getValue() == "")
	{
		ShowPesanWarning("Tanggal "+gstrSp3d+" belum diisi.", "Simpan Data");
		valid -= 1;
	}
	
	if(selectCboUnitKerjaEntrySP3DForm === undefined)
	{
		ShowPesanWarning(gstrSatker + " belum dipilih..", "Simpan Data");
		valid -= 1;
	}

	if(selectCboUnitKerjaEntrySP3DFormTempEdit != "")
	{
		if(selectCboUnitKerjaEntrySP3DFormTempEdit != selectCboUnitKerjaEntrySP3DForm)
		{
			ShowPesanWarning(gstrSatker & " tidak diperbolehkan untuk dirubah..", "Simpan Data");
			valid -= 1;
		}
	}
	// if(Ext.getCmp('numPriorEntrySP3DForm').getValue() == '' || Ext.getCmp('txtaKegEntrySP3DForm').getValue() == '')
	// {
	// 	// alert(Ext.getCmp('numPriorEntrySP3DForm').getValue() +','+Ext.getCmp('txtaKegEntrySP3DForm').getValue());
	// 	ShowPesanWarning("Data "+gstrSp3d+" belum dipilih.", "Simpan Data");
	// 	valid -= 1;
	// }
	
	if(dsGridDetailEntrySP3DForm.getCount() <= 0)
	{
		ShowPesanWarning("Transaksi harus memiliki detail", "Simpan Data");
		valid -= 1;
	}
	
	 var selisih=Ext.num(0);
	 //selisih = parseFloat(getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()))- parseFloat(getNumber(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()))
	 selisih = parseFloat(getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()))- parseFloat(getAmount_SP3DFormAll(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()))
	
	if(selisih < 0 )
	{
		ShowPesanWarning(" Jumlah melebihi anggaran", "Simpan Data");
		valid -= 1;
	}

	/*var cek_total_jumlah=Ext.num(0);
	var cek_sisa_jumlah=Ext.num(0);
	cek_total_jumlah = parseFloat(getNumber(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue())) + jumlah_RKAT_history_get
	cek_sisa_jumlah = getNumber(jumlah_RKAT_get - jumlah_RKAT_history_get)

	if(cek_total_jumlah > jumlah_RKAT_get)
	{
		ShowPesanWarning(" Jumlah melebihi anggaran, anggaran tersisa : " + cek_sisa_jumlah, "Simpan Data");
		valid -= 1;
	}*/

	
	if (Ext.getCmp('txtaKetEntrySP3DForm').getValue().length>350){
		ShowPesanWarning("Keterangan Tidak boleh lebih dari 350 Karakter","Simpan PPD");
		valid-=1;
	}

	/*if (ValidDetailEntrySP3DForm(valid)==1)
		{
			valid = 1;
		}
		else
		{
			valid = 0;
		}*/

		
	return valid;
}
function ValidDetailEntrySP3DForm(valid) 
{ 
    for (var i = 0; i < dsGridDetailEntrySP3DForm.getCount(); i++) 
	{		
		if (CekRowGridBrgGandaEntrySP3DForm(dsGridDetailEntrySP3DForm.data.items[i].data.ACCOUNT) == 0)
		{
			valid=0
			ShowPesanWarning('Account '+ dsGridDetailEntrySP3DForm.data.items[i].data.ACCOUNT +' Sudah ada ','Simpan data');
		}

    }
    
    return valid;
};

function CekRowGridBrgGandaEntrySP3DForm(accounts)
{
	var x=2;
	for (var i = 0; i < dsGridDetailEntrySP3DForm.getCount(); i++) 
	{
		if (accounts == dsGridDetailEntrySP3DForm.data.items[i].data.ACCOUNT)
		{
			x -= 1	
		}
	}
	// if (x > 1)
	// {  
	// 	x=1
	// }

	return x;
};

function ValidasiAppEntrySP3DForm()
{
	var valid = 1;
	
	// if((Ext.getCmp('txtNoEntrySP3DForm').getValue() == "") && (Ext.getCmp('txtaliasNoEntrySP3DForm').getValue == ""))
	if(Ext.getCmp('txtNoEntrySP3DForm').getValue() == "")
	{
		ShowPesanWarning("No "+gstrSp3d+" belum diisi.", "Simpan Data");
		valid = 0;
	}
	return valid;
}

function getParamSaveEntrySP3DForm()
{
	var jalurTransaksi;
	
	
    var params = 
	{
		tahun_anggaran				: TahunAngg,
		no_sp3d						:Ext.get('txtNoEntrySP3DForm').getValue(),
		kd_unit_kerja				:selectCboUnitKerjaEntrySP3DForm,
		tgl_sp3d					:Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
		jumlah						:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		is_app						:stris_app, //app_sp3d
		prioritas					:Ext.getCmp('numPriorEntrySP3DForm').getValue(), //diisi saat pilih akun RAB
		keterangan					:Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		jumlah_sp3d_rkatr			:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
	};
	
	params['jumlah_list']			=	dsGridDetailEntrySP3DForm.getCount();
	console.log(dsGridDetailEntrySP3DForm.data);
	for(var i = 0 ; i < dsGridDetailEntrySP3DForm.getCount();i++)
	{
		params['tahun_anggaran_ta-'+i]		=	dsGridDetailEntrySP3DForm.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja-'+i]			=	dsGridDetailEntrySP3DForm.data.items[i].data.kd_unit_kerja
		params['prioritas-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.prioritas
		params['urut-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.urut
		params['account-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.account
		params['deskripsi-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.deskripsi
		params['value-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.value
		params['posted-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.posted
		
	}
	
	return params;
}

function getParamAppEntrySP3DForm()
{
	var jalurTransaksi;
	
	
    var params = 
	{
		tahun_anggaran				: TahunAngg,
		no_sp3d						:Ext.get('txtNoEntrySP3DForm').getValue(),
		kd_unit_kerja				:selectCboUnitKerjaEntrySP3DForm,
		tgl_sp3d					:Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
		jumlah						:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		is_app						:1, //app_sp3d
		prioritas					:Ext.getCmp('numPriorEntrySP3DForm').getValue(), //diisi saat pilih akun RAB
		keterangan					:Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		jumlah_sp3d_rkatr			:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		tgl_app						:now.format('d/M/Y')
	};
	
	params['jumlah_list']			=	dsGridDetailEntrySP3DForm.getCount();
	console.log(dsGridDetailEntrySP3DForm.data);
	for(var i = 0 ; i < dsGridDetailEntrySP3DForm.getCount();i++)
	{
		params['tahun_anggaran_ta-'+i]		=	dsGridDetailEntrySP3DForm.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja-'+i]			=	dsGridDetailEntrySP3DForm.data.items[i].data.kd_unit_kerja
		params['prioritas-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.prioritas
		params['urut-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.urut
		params['account-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.account
		params['deskripsi-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.deskripsi
		params['value-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.value
		params['posted-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.posted
		
	}
	
	return params;
}
function getParamDeldetailSP3DForm()
{
	var params = 
	{
		Table: 'viSP3D',
		IS_DETAIL:1,	
		rkat: dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.JENIS_RKAT,//Ext.getCmp('txtRKATEntrySP3DForm').getValue(),
		tahun_anggaran: dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.TAHUN_ANGGARAN_TA, //TahunAngg,
		unit: selectCboUnitKerjaEntrySP3DForm,
		kd_user:userSp3D,
		nosp3d: Ext.getCmp('txtNoEntrySP3DForm').getValue(),
		tglsp3d: ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()),
		NO_PROGRAM_PROG: dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.NO_PROGRAM_PROG,//Ext.getCmp('txtNoProgEntrySP3DForm').getValue(),
		PRIORITAS_SP3D: dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.PRIORITAS,//Ext.getCmp('numPriorEntrySP3DForm').getValue(),
		URUT:dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.LINE,
		ACCOUNT:dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.ACCOUNT,
		FilePath:Ext.get('txtPathfileSP3DForm').getValue(),
		//VALUE:dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.VALUE,
		VALUE_TMP:dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.VALUE_TMP,
		kategori:REFFID_SP3D_SP3DForm

	};	
	return params;
}

/* function getParamAppEntrySP3DForm()
{
	
	var params = 
	{
		Table: 'viSP3D',
		nosp3d: Ext.getCmp('txtNoEntrySP3DForm').getValue(),
		tglsp3d: ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()),
		tglsp3dasal: dtTglSP3DAsal === undefined ? ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()) : ShowDate(dtTglSP3DAsal),
		unit: selectCboUnitKerjaEntrySP3DForm,
		ket: Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		rkat: Ext.getCmp('txtRKATEntrySP3DForm').getValue(),
		jnsrkat: Ext.getCmp('txtJnsRKATEntrySP3DForm').getValue(),
		noprog: Ext.getCmp('txtNoProgEntrySP3DForm').getValue(),
		jum: getAmount_SP3DFormAll(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()),//getNumber(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()),
		tahun_anggaran: TahunAngg,
		prior: Ext.getCmp('numPriorEntrySP3DForm').getValue(),
		kd_user:userSp3D,
		FilePath:Ext.get('txtPathfileSP3DForm').getValue(),
		is_app:stris_app,//1,
		kategori:REFFID_SP3D_SP3DForm
	};
	
	return params;
} */

function getParamBatalEntrySP3DForm()
{
  
	  var params = 
	{
		tahun_anggaran				: TahunAngg,
		no_sp3d						:Ext.get('txtNoEntrySP3DForm').getValue(),
		kd_unit_kerja				:selectCboUnitKerjaEntrySP3DForm,
		tgl_sp3d					:Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
		jumlah						:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		is_app						:0, //app_sp3d
		prioritas					:Ext.getCmp('numPriorEntrySP3DForm').getValue(), //diisi saat pilih akun RAB
		keterangan					:Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		jumlah_sp3d_rkatr			:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		tgl_app						:now.format('d/M/Y')
	};
	
	params['jumlah_list']			=	dsGridDetailEntrySP3DForm.getCount();
	console.log(dsGridDetailEntrySP3DForm.data);
	for(var i = 0 ; i < dsGridDetailEntrySP3DForm.getCount();i++)
	{
		params['tahun_anggaran_ta-'+i]		=	dsGridDetailEntrySP3DForm.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja-'+i]			=	dsGridDetailEntrySP3DForm.data.items[i].data.kd_unit_kerja
		params['prioritas-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.prioritas
		params['urut-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.urut
		params['account-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.account
		params['deskripsi-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.deskripsi
		params['value-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.value
		params['posted-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.posted
		
	}
	
	return params;
	
	/* var params = 
	{
		Table: 'viSP3D',
		nosp3d: Ext.getCmp('txtNoEntrySP3DForm').getValue(),
		tglsp3d: ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()),
		tglsp3dasal: dtTglSP3DAsal === undefined ? ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()) : ShowDate(dtTglSP3DAsal),
		unit: selectCboUnitKerjaEntrySP3DForm,
		ket: Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		rkat: Ext.getCmp('txtRKATEntrySP3DForm').getValue(),
		jnsrkat: Ext.getCmp('txtJnsRKATEntrySP3DForm').getValue(),
		noprog: Ext.getCmp('txtNoProgEntrySP3DForm').getValue(),
		jum: getAmount_SP3DFormAll(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()),//getNumber(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()),
		prior: Ext.getCmp('numPriorEntrySP3DForm').getValue(),
		tahun_anggaran: TahunAngg,
		kd_user:userSp3D,
		FilePath:Ext.get('txtPathfileSP3DForm').getValue(),
		is_app:stris_app,//1,
		is_unapp:1,
		kategori:REFFID_SP3D_SP3DForm
	};
	
	return params; */
}

function getParamUnAppEntrySP3DForm()
{
	
	var jalurTransaksi;
	
	
    var params = 
	{
		tahun_anggaran				: TahunAngg,
		no_sp3d						:Ext.get('txtNoEntrySP3DForm').getValue(),
		kd_unit_kerja				:selectCboUnitKerjaEntrySP3DForm,
		tgl_sp3d					:Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
		jumlah						:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		is_app						:0, //app_sp3d
		prioritas					:Ext.getCmp('numPriorEntrySP3DForm').getValue(), //diisi saat pilih akun RAB
		keterangan					:Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		jumlah_sp3d_rkatr			:getAmount_SP3DFormAll(Ext.getCmp('txtJumBiayaEntrySP3DForm').getValue()),
		tgl_app						:now.format('d/M/Y')
	};
	
	params['jumlah_list']			=	dsGridDetailEntrySP3DForm.getCount();
	console.log(dsGridDetailEntrySP3DForm.data);
	for(var i = 0 ; i < dsGridDetailEntrySP3DForm.getCount();i++)
	{
		params['tahun_anggaran_ta-'+i]		=	dsGridDetailEntrySP3DForm.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja-'+i]			=	dsGridDetailEntrySP3DForm.data.items[i].data.kd_unit_kerja
		params['prioritas-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.prioritas
		params['urut-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.urut
		params['account-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.account
		params['deskripsi-'+i]				=	dsGridDetailEntrySP3DForm.data.items[i].data.deskripsi
		params['value-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.value
		params['posted-'+i]					=	dsGridDetailEntrySP3DForm.data.items[i].data.posted
		
	}
	
	return params;
	
	/* var params =
	{
		Table: 'viSP3D',
		nosp3d: Ext.getCmp('txtNoEntrySP3DForm').getValue(),
		tglsp3d: ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()),
		tglsp3dasal: dtTglSP3DAsal === undefined ? ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()) : ShowDate(dtTglSP3DAsal),
		unit: selectCboUnitKerjaEntrySP3DForm,
		ket: Ext.getCmp('txtaKetEntrySP3DForm').getValue(),
		rkat: Ext.getCmp('txtRKATEntrySP3DForm').getValue(),
		jnsrkat: Ext.getCmp('txtJnsRKATEntrySP3DForm').getValue(),
		noprog: Ext.getCmp('txtNoProgEntrySP3DForm').getValue(),
		jum: getAmount_SP3DFormAll(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()),
		tahun_anggaran: TahunAngg,
		prior: Ext.getCmp('numPriorEntrySP3DForm').getValue(),
		kd_user:userSp3D,
		FilePath:Ext.get('txtPathfileSP3DForm').getValue(),
		is_app:stris_app,//1,
		is_unapp:2,
		kategori:REFFID_SP3D_SP3DForm
	};
	
	return params; */
}

function getGridParamAppEntrySP3DForm(rowData)
{
 
	var params = 
	{
		Table: 'viSP3D',
		nosp3d:  rowData.NO_SP3D,
		tglsp3d: ShowDate(rowData.TGL_SP3D),
		tglsp3dasal: ShowDate(rowData.TGL_SP3D),
		unit: rowData.KD_UNIT_KERJA,
		ket: rowData.KETERANGAN,
		rkat: rowData.RKAT,
		jnsrkat: rowData.KD_JNS_RKAT_JRKA,
		noprog: rowData.NO_PROGRAM_PROG,
		jum: getAmount_SP3DFormAll(rowData.JUMLAH),//getNumber(rowData.JUMLAH),
		prior: rowData.PRIORITAS,
		kd_user:userSp3D,
		FilePath:Ext.get('txtPathfileSP3DForm').getValue(),
		is_app:stris_app//1
	};
	
	return params;
}

function getArrDetailEntrySP3DForm()
{
	var x = "";
	var y = "";
	var	z = "@@##$$@@";
	
	if(dsGridDetailEntrySP3DForm.getCount() > 0)
	{
		for(var i=0; i<dsGridDetailEntrySP3DForm.getCount(); i++)
		{
			y = "col_line=" + dsGridDetailEntrySP3DForm.data.items[i].data.LINE;
			y += z + "col_thn=" + dsGridDetailEntrySP3DForm.data.items[i].data.TAHUN_ANGGARAN_TA;
			y += z + "col_unit=" + dsGridDetailEntrySP3DForm.data.items[i].data.KD_UNIT_KERJA;
			y += z + "col_jnsrkat=" + dsGridDetailEntrySP3DForm.data.items[i].data.KD_JNS_RKAT_JRKA;
			y += z + "col_noprog=" + dsGridDetailEntrySP3DForm.data.items[i].data.NO_PROGRAM_PROG;
			y += z + "col_prior=" + dsGridDetailEntrySP3DForm.data.items[i].data.PRIORITAS;
			y += z + "col_nosp3d=" + dsGridDetailEntrySP3DForm.data.items[i].data.NO_SP3D_RKAT;
			y += z + "col_tglsp3d=" + ShowDate(dsGridDetailEntrySP3DForm.data.items[i].data.TGL_SP3D_RKAT);
			y += z + "col_akun=" + dsGridDetailEntrySP3DForm.data.items[i].data.ACCOUNT;
			y += z + "col_desc=" + dsGridDetailEntrySP3DForm.data.items[i].data.DESKRIPSI;
			y += z + "col_value=" + dsGridDetailEntrySP3DForm.data.items[i].data.VALUE;
			y += z + "col_posted=" + dsGridDetailEntrySP3DForm.data.items[i].data.POSTED;
			y += z + "col_value_tmp=" + dsGridDetailEntrySP3DForm.data.items[i].data.VALUE_TMP;
			y += z + "col_jenis_rkat=" + dsGridDetailEntrySP3DForm.data.items[i].data.JENIS_RKAT;
			
			if(i == dsGridDetailEntrySP3DForm.getCount() - 1)
			{
				x += y;
			}
			else
			{
				x += y + "##[[]]##";
			}
		}
	}
	
	return x;
}

function CheckDetailApprovedEntrySP3DForm()
{
	var x = 0;
	if(dsGridDetailEntrySP3DForm.getCount() > 0)
	{
		for(var i=0;i<dsGridDetailEntrySP3DForm.getCOunt();i++)
		{
			x += 1;
		}
	}
	
	if(x === dsGridDetailEntrySP3DForm.getCount())
	{
		Ext.getCmp('chkApprovedEntrySP3DForm').setValue(true);	
	}
	else
	{
		Ext.getCmp('chkApprovedEntrySP3DForm').setValue(false);	
	}
}

function getGridDetailEntrySP3DForm()
{
	
	var Fields = [
		// 'TAHUN_ANGGARAN_TA',
		// 'KD_UNIT_KERJA',
		// 'KD_JNS_RKAT_JRKA',
		// 'NO_PROGRAM_PROG',
		// 'PRIORITAS',
		// 'NO_SP3D',
		// 'TGL_SP3D',
		// 'LINE',
		// 'ACCOUNT',
		// 'NAME',
		// 'DESKRIPSI',
		// 'VALUE',
		// 'POSTED',
		// 'DESKRIPSI',
		// 'DESKRIPSI_RKAT',
		// 'VALUE_TMP',
		// 'JENIS_RKAT'
		'line','tahun_anggaran_ta','kd_unit_kerja','kd_jns_rkat_jrka','prioritas','no_program_prog','no_sp3d_rkat','tgl_sp3d_rkat',
		'account','name','deskripsi','value','posted','deskripsi_rkat','value_tmp','jenis_rkat','kegiatan','nama_program_prog'
	];
	dsGridDetailEntrySP3DForm = new WebApp.DataStore({ fields: Fields });	
	
	grdDetailEntrySP3DForm = new Ext.grid.EditorGridPanel(
		{
			id: 'grdDetailEntrySP3DForm',
			border: false,
			region: 'center',
			anchor: '100% 80%',
			store: dsGridDetailEntrySP3DForm,
			autoScroll: true,
			// viewConfig: { forceFit: true },
			sm: new Ext.grid.CellSelectionModel(
				{
					singleSelect: true,
					listeners:
					{
						/* 'rowselect': function(sm, row, rec)
						{
							rowselectGridDetailEntrySP3DForm = dsGridDetailEntrySP3DForm.getAt(row);
							CurrentRowGridDetailEntrySP3DForm.row = row;
							CurrentRowGridDetailEntrySP3DForm.data = rowselectGridDetailEntrySP3DForm.data;
						} */
						
						cellselect: function(sm, row, rec)
						{
							rowselectGridDetailEntrySP3DForm = dsGridDetailEntrySP3DForm.getAt(row);
							CurrentRowGridDetailEntrySP3DForm.row = row;
							CurrentRowGridDetailEntrySP3DForm.data = rowselectGridDetailEntrySP3DForm.data;
						}
					}
				}
			),
			cm: new Ext.grid.ColumnModel(
				[
					new Ext.grid.RowNumberer(),
					{
						xtype: 'gridcolumn',
						id: 'col_noakun_gldetail',
						dataIndex: 'account',
						header: 'No. Akun',
						width: 140
						/*,
						editor: new Ext.form.TextField(
							{
								id: 'fcol_noakun_detsp3dform',
								enableKeyEvents: true,
								listeners:
								{
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() === 13)
										{
											alert(this.getValue());
										}
									}
								}
							}
						)*/
					},
					{
						xtype: 'gridcolumn',
						id: 'col_nmakun_gldetail',
						dataIndex: 'name',
						width: 200,						
						header: 'Nama Akun'/*,
						editor: new Ext.form.TextField(
							{
								id: 'fcol_nmakun_detsp3dform',
								enableKeyEvents: true,
								listeners:
								{
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() === 13)
										{
											alert(this.getValue());
										}
									}
								}
							}
						)*/
					},
					{
						xtype: 'gridcolumn',
						id: 'col_des_gldetailRKAT',
						dataIndex: 'deskripsi_rkat',
						width: 200,
						header: 'Deskripsi(RAB)'
					},					
					{
						xtype: 'gridcolumn',
						id: 'col_des_gldetail',
						dataIndex: 'deskripsi',
						width: 200,
						header: 'Deskripsi(PPD)',
						editor: new Ext.form.TextField(
							{
								id: 'fcol_des_detsp3dform'								
							}
						)
					},
					{
						xtype: 'gridcolumn',
						id: 'col_ket_gldetail',
						dataIndex: 'kegiatan',
						header: 'Kegiatan',
						width:200,
						editor: new Ext.form.TextField(
							{
								id: 'fcol_ket_detsp3dform'								
							}
						)
					}, 
					{
						xtype: 'gridcolumn',
						id: 'col_NoProg_gldetail',
						dataIndex: 'no_program_prog',
						header: 'No Program',
						width: 70
					},
					{
						xtype: 'gridcolumn',
						id: 'col_NamaProg_gldetail',
						dataIndex: 'nama_program_prog',
						header: 'Nama Program',
						width: 100
					},
					{
						xtype: 'gridcolumn',
						id: 'col_Prioritas_gldetail',
						dataIndex: 'prioritas',
						header: 'Prioritas',
						width: 70
					},
					{
						xtype: 'gridcolumn',
						id: 'col_jum_gldetail',
						dataIndex: 'value',
						header: 'Jumlah',
						width: 100,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;text-align: right;'>" + formatCurrencyDec(record.data.value) + "</div>";
							return str;
						},
						editor: new Ext.form.TextField(
							{
								id: 'fcol_jum_detsp3dform',
								style: 'text-align: right;',
								enableKeyEvents: true,
								baseChars: '0123456789.',
								listeners:
								{
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() === 13)
										{
											CalcTotalDetailEntrySP3DForm();
										}
									},
									'blur' : function(){
										CalcTotalDetailEntrySP3DForm();
									}
								}
							}
						)
					},
					{
						xtype: 'gridcolumn',
						id: 'col_jum_tmp_gldetail',
						dataIndex: 'value_tmp',
						header: 'Jumlah Tmp',
						width: 100,
						hidden: true, // tes SDY
					},
					{
						xtype: 'gridcolumn',
						id: 'col_jenis_rkat_gldetail',
						dataIndex: 'jenis_rkat',
						header: 'JENIS_RKAT',
						width: 100,
						hidden: true, 
					}
				]
			),
			tbar:
			[
				/* {
					xtype: 'button',
					id: 'btnAddRowDetailEntrySP3DForm',
					iconCls: 'add',
					text: 'Tambah Baris',
					handler: function(){ TambahBarisDetailEntrySP3DForm(); }
				}, */
				{
					xtype: 'button',
					id: 'btnDelRowDetailEntrySP3DForm',
					iconCls: 'remove',
					text: 'Hapus Baris',
					handler: function(){ HapusBarisDetailEntrySP3DForm(); }
				}
			],
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load",
						function()
						{
							CalcTotalDetailEntrySP3DForm()
						}
					);
					
					this.store.on("datachanged",
						function()
						{
							CalcTotalDetailEntrySP3DForm()
						}
					);
				}
			}
		}
	);
	
	return grdDetailEntrySP3DForm;
} 

function RefreshDataGridDetailKorEntrySP3DForm( tahun_anggaran_ta, kd_unit_kerja, no_sp3d, tgl_sp3d, prioritas)
{
	/* dsGridDetailEntrySP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viListDetailSP3DKOR',
				param: criteria
			} 
		}
	);	
	return dsGridDetailEntrySP3DForm; */
	
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPPD/LoadDetailPPD",
		params: {
			tahun_anggaran_ta	: tahun_anggaran_ta,
			kd_unit_kerja		: kd_unit_kerja,
			no_sp3d				: no_sp3d,
			tgl_sp3d			: tgl_sp3d,
			prioritas			: prioritas
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data detail PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGridDetailEntrySP3DForm.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGridDetailEntrySP3DForm.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGridDetailEntrySP3DForm.add(recs);
				CalcTotalDetailEntrySP3DForm();
			} else {
				ShowPesanError('Gagal menampilkan data detail PPD!', 'Error');
			};
		}
	});
}

function RefreshDataGridDetail1EntrySP3DForm(criteria)
{
	dsGridDetailEntrySP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viListDetailSP3DK',
				param: criteria
			} 
		}
	);	
	return dsGridDetailEntrySP3DForm;
}

function RefreshDataGridDetail3EntrySP3DForm(criteria)
{
	dsGridDetailEntrySP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viewListDetailSP3DO',
				param: criteria
			} 
		}
	);
	
	return dsGridDetailEntrySP3DForm;
}

function RefreshDataGridDetail2EntrySP3DForm(criteria)
{
	dsGridDetailEntrySP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viListDetailSP3DR',
				param: criteria
			} 
		}
	);
	
	return dsGridDetailEntrySP3DForm;
}

function getCriteriaGridDetailEntrySP3DForm()
{
	var strKriteria = "";
	
	if(Ext.getCmp('txtNoEntrySP3DForm').getValue() !== "")
	{
	   
		if(strKriteria !== "")
		{
			// strKriteria += " and SRKD.NO_SP3D_RKAT='" + Ext.getCmp('txtNoEntrySP3DForm').getValue()  + "'";
			strKriteria += " and X.NO_SP3D_RKAT='" + Ext.getCmp('txtNoEntrySP3DForm').getValue()  + "'";
		}
		else
		{
			// strKriteria += " Where SRKD.NO_SP3D_RKAT='" + Ext.getCmp('txtNoEntrySP3DForm').getValue()  + "'";
			strKriteria += " Where X.NO_SP3D_RKAT='" + Ext.getCmp('txtNoEntrySP3DForm').getValue()  + "'";
		}
	}	
	if(Ext.getCmp('dtpTglEntrySP3DForm').getValue() !== "")
	{
		if(strKriteria !== "")
		{
			// strKriteria += " and SRKD.TGL_SP3D_RKAT='" + ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue())  + "'";
			strKriteria += " and X.TGL_SP3D_RKAT='" + ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue())  + "'";
		}
		else
		{
			// strKriteria += " Where SRKD.TGL_SP3D_RKAT='" + ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue())  + "'";
			strKriteria += " Where X.TGL_SP3D_RKAT='" + ShowDate(Ext.getCmp('dtpTglEntrySP3DForm').getValue()) + "'";
		}		
	}		
	return strKriteria;
}

function CalcTotalDetailEntrySP3DForm()
{
	var total = 0;
	
	if(dsGridDetailEntrySP3DForm.getCount() > 0)
	{
		for(var i=0;i<dsGridDetailEntrySP3DForm.getCount();i++)
		{
			// total += getNumber(dsGridDetailEntrySP3DForm.data.items[i].data.VALUE);
			total += parseFloat(dsGridDetailEntrySP3DForm.data.items[i].data.value);
			//total += dsGridDetailEntrySP3DForm.data.items[i].data.VALUE; // nilai gabung
		}
	}	
	Ext.getCmp('txtTotDetailJumEntrySP3DForm').setValue(formatCurrencyDec(total));
	//tambahan.........................
	Ext.getCmp('txtJumBiayaEntrySP3DForm').setValue(formatCurrencyDec(total));
	console.log(total);
	
}

function TambahBarisDetailEntrySP3DForm()
{
	var x = true;
	if(dsGridDetailEntrySP3DForm.getCount() > 0)
	{		
		if(dsGridDetailEntrySP3DForm.data.items[dsGridDetailEntrySP3DForm.getCount() - 1].data.ACCOUNT == "")
		{
			x = false;
		}
	}
	
	if(x)
	{
		var p = new mRecordDetailEntrySP3DForm(
			{
				tahun_anggaran_ta: '',
				kd_unit_kerja: '',
				kd_jns_rkat_jrka: '',
				no_program_prog: '',
				prioritas: '',
				no_sp3d: '',
				tgl_sp3d_rkat: '',
				line: 0,
				account: '',
				name: '',
				deskripsi: '',
				value: 0,
				posted: 0,
				jenis_rkat: 0
			}
		);
		
		dsGridDetailEntrySP3DForm.insert(dsGridDetailEntrySP3DForm.getCount(), p);
		rowselectGridDetailEntrySP3DForm = dsGridDetailEntrySP3DForm.getAt(dsGridDetailEntrySP3DForm.getCount());
		CurrentRowGridDetailEntrySP3DForm.row = dsGridDetailEntrySP3DForm.getCount();
		CurrentRowGridDetailEntrySP3DForm.data = rowselectGridDetailEntrySP3DForm
	}
}

function HapusBarisDetailEntrySP3DForm()
{		

	var line = grdDetailEntrySP3DForm.getSelectionModel().selection.cell[0];
	var o = dsGridDetailEntrySP3DForm.getRange()[line].data;
	console.log(o);
	Ext.Msg.confirm('Warning', 'Apakah data detail PPD  ini akan dihapus?', function(button){
		if (button == 'yes'){
			if( dsGridDetailEntrySP3DForm.getRange()[line].data.prioritas !== ''  ){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionPPD/HapusDetailPPD",
						params:{
							tahun_anggaran_ta			:o.tahun_anggaran_ta,
							kd_unit_kerja				:o.kd_unit_kerja,
							no_sp3d_rkat				:o.no_sp3d_rkat,
							tgl_sp3d_rkat				:o.tgl_sp3d_rkat,
							prioritas_sp3d_rkatr_trans	:o.prioritas,
							urut						:o.urut,
							account						:o.account,
							total						:getAmount_SP3DFormAll(Ext.getCmp('txtTotDetailJumEntrySP3DForm').getValue()),
							value						:o.value
							
						} ,
						failure: function(o)
						{
							ShowPesanError('Error menghapus detail  PPD!', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsGridDetailEntrySP3DForm.removeAt(line);
								grdDetailEntrySP3DForm.getView().refresh();
								CalcTotalDetailEntrySP3DForm();
								ShowPesanInfo('Data detail PPD berhasil dihapus','Information');
							}
							else 
							{
								ShowPesanError('Gagal menghapus data detail PPD!', 'Error');
							};
						}
					}
					
				)
			}else{
				dsGridDetailEntrySP3DForm.removeAt(line);
				grdDetailEntrySP3DForm.getView().refresh();
				CalcTotalDetailEntrySP3DForm();
				ShowPesanInfo('Data detail PPD berhasil dihapus','Information');
			}
			
		} 
		
	});
	/* var fn_HapusDetail = function(btn)
	{
		
		if(btn == "yes")
		{
			if(dsGridDetailEntrySP3DForm.data.items[CurrentRowGridDetailEntrySP3DForm.row].data.value_tmp == 0)
			{
				dsGridDetailEntrySP3DForm.removeAt(CurrentRowGridDetailEntrySP3DForm.row);
				ShowPesanInfo_SP3DFormAll("Baris ke-" + (CurrentRowGridDetailEntrySP3DForm.row + 1) + " berhasil dihapus!", "Hapus Baris");
				CalcTotalDetailEntrySP3DForm();
			}
			else
			{			
				Ext.Ajax.request
				(
					{
						url: "./Datapool.mvc/DeleteDataObj",
						params:  getParamDeldetailSP3DForm(),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsGridDetailEntrySP3DForm.removeAt(CurrentRowGridDetailEntrySP3DForm.row);
								ShowPesanInfo("Baris ke-" + (CurrentRowGridDetailEntrySP3DForm.row + 1) + " berhasil dihapus!", "Hapus Baris");
								CalcTotalDetailEntrySP3DForm();
								ishapusdetail=1;
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning('Data tidak berhasil dihapus, data tersebut belum ada','Hapus Data');
							}
							else 
							{
								ShowPesanError('Data tidak berhasil dihapus','Hapus Data');
							};
						}
					}
				);
			}
		}
	}
	if(dsGridDetailEntrySP3DForm !== undefined || dsGridDetailEntrySP3DForm.getCount() > 1)
	{
		var msg = "Hapus baris ke-" + (CurrentRowGridDetailEntrySP3DForm.row + 1) + " "
		ShowPesanConfirm_SP3DFormAll(msg, "Hapus Baris", fn_HapusDetail);
	} */	
}

function loadDetailRKATEntrySP3DForm(criteria)
{	
	/* var Fields = 
	[
		'tahun_anggaran_ta',
		'kd_unit_kerja',
		'kd_jns_rkat_jrka',
		'no_program_prog',
		'prioritas',
		'line',
		'account',
		'name',
		'jumlah',
		'deskripsi',
		'nama_program_prog',
		'kegiatan',
		'jenis_rkat',
	];
	var dsTmp = new WebApp.DataStore({ fields: Fields });
	var target ="";
	if (Ext.getCmp('txtRKATEntrySP3DForm').getValue() == "Rutin")
	{ 
	target ='viDetailRKATSP3DR' ;
	}
	else if 
	(Ext.getCmp('txtRKATEntrySP3DForm').getValue() == "Non Rutin" || Ext.getCmp('txtRKATEntrySP3DForm').getValue() == "Pengembangan")//update asalnya 'Pengembangan'
	{
		target = 'viDetailRKATSP3DK';
	}else 
	{
		target ='viewDetailRKATSP3DO'
	};
	dsTmp.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: target,
			    param: criteria
			}
		}
	);
	dsTmp.on
	("load",
		function()
		{ 							
			insertToGridDetailEntrySP3DForm(dsTmp); 
		}
	); */
	insertToGridDetailEntrySP3DForm(dsTmp); 
}	

function insertToGridDetailEntrySP3DForm(ds)
{
	console.log(ds.data);
	
	if(dsGridDetailEntrySP3DForm.getCount > 0){ dsGridDetailEntrySP3DForm.removeAll(); }

	// if(ds.getCount() > 0)
	// {
		// for(var i=0;i<ds.getCount();i++)
		// {	
			var p = new mRecordDetailEntrySP3DForm
			(
				{
					// --LINE: 0,//ds.data.items[i].data.LINE,
					// line: ds.data.line,
					tahun_anggaran_ta	: ds.data.tahun_anggaran_ta,
					kd_unit_kerja		: ds.data.kd_unit_kerja,
					kd_jns_rkat_jrka	: ds.data.kd_jns_rkat_jrka,
					no_program_prog		: ds.data.no_program_prog,
					nama_program_prog	: ds.data.nama_program_prog,
					kegiatan			: ds.data.kegiatan,
					prioritas			: ds.data.prioritas,
					no_sp3d_rkat		: Ext.getCmp('txtNoEntrySP3DForm').getValue(),
					tgl_sp3d_rkat		: Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
					account				: ds.data.account,
					name				: ds.data.name,
					deskripsi			: ds.data.deskripsi_rkatrdet,
					value				: ds.data.jmlh_rkatrdet_sisa,
					deskripsi_rkat		: ds.data.deskripsi_rkatrdet,	
					value_tmp			: 0,
					posted				: ds.data.disahkan_rka,
					urut				: ds.data.urut_rkatrdet,
					jenis_rkat			: StrJnsRKATEntrySP3DForm
				}
			);			
			console.log(p);
			dsGridDetailEntrySP3DForm.insert(dsGridDetailEntrySP3DForm.getCount(), p);			
			Ext.getCmp('txtJumBiayaEntrySP3DForm').setValue(formatCurrencyDec(ds.data.jumlah));
		// }
		
		CalcTotalDetailEntrySP3DForm();
	// }
}

function getCriteriaDetailRKATEntrySP3DForm()
{
	var strKriteria = "";
	
	if(Ext.getCmp('dtpTglEntrySP3DForm').getValue() !== "")
	{
		var tahun = (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear() < 1900 ? 1900 + (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear() : (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear();
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + tahun;
	}
	
	if(selectCboUnitKerjaEntrySP3DForm !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectCboUnitKerjaEntrySP3DForm;
	}
	
	if(Ext.getCmp('txtJnsRKATEntrySP3DForm').getValue() !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "jnsrkat=" + Ext.getCmp('txtJnsRKATEntrySP3DForm').getValue();
	}
	
	if(Ext.getCmp('txtNoProgEntrySP3DForm').getValue() !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "noprog=" + Ext.getCmp('txtNoProgEntrySP3DForm').getValue();
	}
	
	if(Ext.getCmp('numPriorEntrySP3DForm').getValue() !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "prior=" + Ext.getCmp('numPriorEntrySP3DForm').getValue();
	}
	
	if(akun !== "" || akun !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "akun=" + akun;
	}
	
	if(jumlah !== "" || jumlah !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "jumlah=" + jumlah;
	}
	if(line !== "" || line !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "line=" + line;
	}
	
	if(dsGridDetailEntrySP3DForm.getCount() > 0)
	{
		var acc = "";
		for(var i=0;i<dsGridDetailEntrySP3DForm.getCount();i++)
		{
			acc += "'" + dsGridDetailEntrySP3DForm.data.items[i].data.ACCOUNT + "'";
			if(i<dsGridDetailEntrySP3DForm.getCount() - 1){ acc+= ","; }
		}
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "notin=" + acc;
	}
	
	return strKriteria;
}

/* Cetak DPD */
function GetCriteria_SP3DForm2()
{
	var strKriteria = '';
	
	if (Ext.getCmp('txtNoEntrySP3DForm').getValue() != '') 
	{
		strKriteria = " Where NO_SP3D_RKAT ='" + Ext.getCmp('txtNoEntrySP3DForm').getValue() + "'";
	};
	
	if (Ext.getCmp('cboUnitKerjaEntrySP3DForm').getValue() != '') 
	{
		strKriteria += " and KD_UNIT_KERJA ='" + selectCboUnitKerjaEntrySP3DForm + "'";
	};
	
	strKriteria += '##' + Ext.getCmp('txtNoEntrySP3DForm').getValue() + "'";
	strKriteria += '##' + Ext.getCmp('txtaKetEntrySP3DForm').getValue() + "'";
	return strKriteria;
};

function Validasi_SP3DForm2()
{
	var x=1;
	
	if((Ext.getCmp('txtNoEntrySP3DForm').getValue() == '') || 
	(Ext.getCmp('cboUnitKerjaEntrySP3DForm').getValue() == ''))
	{
		if(Ext.getCmp('txtNoEntrySP3DForm').getValue() == '')
		{
			ShowPesanWarning_SP3DForm2('No. '+gstrSp3d+' belum di isi','Laporan '+gstrSp3d);
			x=0;
		}
		else if(Ext.getCmp('cboUnitKerjaEntrySP3DForm').getValue() == '' )
		{
			ShowPesanWarning_SP3DForm2(gstrSatker + ' belum di isi','Laporan '+gstrSp3d);
			x=0;
		}
	};
	return x;
};

function ShowPesanWarning_SP3DForm2(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfo_SP3DFormAll(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}
function ShowPesanConfirm_SP3DFormAll(str,modul,fncallback)
{
	Ext.MessageBox.show
	(
		{
		   zSeed: 9000,
		   width: 'auto',
		   title: modul,
		   msg: str,
		   buttons: Ext.MessageBox.YESNO,
		   fn: fncallback,		   
		   icon: Ext.MessageBox.QUESTION
	   }
   );
}

function ViewFileSP3DForm()
{
new Ext.Window({
    height: 500,
    width: 500,
    bodyCfg: {
        tag: 'iframe',
        //src: 'bills/bill123.pdf',
		src: Ext.get('txtPathfileSP3DForm').getValue(),
        style: 'border: 0 none'
    },
    modal: true
}).show();  
}

function getAmount_SP3DFormAll(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount //.replace(',','.')
};
// FORM Lookup
var winFormLookupRKATAll_Unit;
var dsGLLookupRKATAll_Unit;
var selectedrowGLLookupRKATAll_Unit;
var mCancel=true;

function FormLookupRKATAll_Unit(objForm, criteria, unit,tahunsp3d)
{
	winFormLookupRKATAll_Unit = new Ext.Window
	(
		{
		    id: 'winFormLookupRKATAll_Unit',
		    title: 'Lookup RAB',
		    closeAction: 'destroy',
		    closable: true,
		    width: 800,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookupRKATAll_Unit(objForm, criteria,unit,tahunsp3d)],
			listeners:
			{
				'beforeclose': function()
				{
					if (mCancel != true)
					{
						// loadDetailRKATEntrySP3DForm(getCriteriaDetailRKATEntrySP3DForm());
					}
				},
				activate: function()
				{
					mCancel=true;
				}
			}
		}
	);
	
	winFormLookupRKATAll_Unit.show();
}

function getItemFormLookupRKATAll_Unit(objForm, criteria,unit,tahunsp3d)
{
	var pnlButtonLookupRKATUnit = new Ext.Panel
	(
		{
			id: 'pnlButtonLookupRKATUnit',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			items:
			[
				{
					xtype: 'button',
					width: 70,
					text: 'Ok',
					handler: function()
					{
						if(selectedrowGLLookupRKATAll_Unit !== undefined)
						{
							insertToGridDetailEntrySP3DForm(selectedrowGLLookupRKATAll_Unit);
							mCancel=false;
							console.log(selectedrowGLLookupRKATAll_Unit.data);
							Ext.getCmp(objForm.RKAT).setValue(selectedrowGLLookupRKATAll_Unit.data.rkat);
							//Ext.getCmp(objForm.RKAT).setValue('test');
							Ext.getCmp(objForm.JNSRKAT).setValue(selectedrowGLLookupRKATAll_Unit.data.kd_jns_rkat_jrka);
							Ext.getCmp(objForm.NOPROG).setValue(selectedrowGLLookupRKATAll_Unit.data.no_program_prog);
							Ext.getCmp(objForm.NMPROG).setValue(selectedrowGLLookupRKATAll_Unit.data.nama_program_prog);
							Ext.getCmp(objForm.PRIOR).setValue(selectedrowGLLookupRKATAll_Unit.data.prioritas);
							Ext.getCmp(objForm.KEG).setValue(selectedrowGLLookupRKATAll_Unit.data.kegiatan);
							// Ext.getCmp(objForm.JUM).setValue(formatCurrency(selectedrowGLLookupRKATAll_Unit.data.total));
							objForm.THNANGG = selectedrowGLLookupRKATAll_Unit.data.tahun_anggaran_ta;
							//GetJumlahRKATHistorySP3D(selectedrowGLLookupRKATAll_Unit.data.TAHUN_ANGGARAN_TA,selectedrowGLLookupRKATAll_Unit.data.KD_UNIT_KERJA,selectedrowGLLookupRKATAll_Unit.data.ACCOUNT,selectedrowGLLookupRKATAll_Unit.data.PRIORITAS,selectedrowGLLookupRKATAll_Unit.data.LINE);
							akun = selectedrowGLLookupRKATAll_Unit.data.account;
							jumlah = selectedrowGLLookupRKATAll_Unit.data.jumlah;
							line = selectedrowGLLookupRKATAll_Unit.data.line;

							//alert(selectedrowGLLookupRKATAll_Unit.data.RKAT+' '+selectedrowGLLookupRKATAll_Unit.data.NO_PROGRAM_PROG);

							if (selectedrowGLLookupRKATAll_Unit.data.no_program_prog == '')
							{
								jnsrkatr='1'
								StrJnsRKATEntrySP3DForm = '1'
							}else if (selectedrowGLLookupRKATAll_Unit.data.no_program_prog != '')
							{
								jnsrkatk='2'
								StrJnsRKATEntrySP3DForm = '2'
							}else
							{
								jnsrkato='3'
								StrJnsRKATEntrySP3DForm = '3'
							}
							 //alert(selectedrowGLLookupRKATAll_Unit.data.NO_PROGRAM_PROG+':'+jnsrkatk+'-'+jnsrkatr+'-'+jnsrkato);
							winFormLookupRKATAll_Unit.close();
						}
						else
						{
							ShowPesanInfo('Lookup RAPB','RAPB belum dipilih.');
						}
					}
				},
				{
					xtype: 'button',
					width: 70,
					text: 'Cancel',
					handler: function()
					{
						selectedrowGLLookupRKATAll_Unit = undefined;
						winFormLookupRKATAll_Unit.close();
					}
				}
			]
		}
	);
	
	var frmListLookupRKATUnit = new Ext.Panel
	(
		{
			id: 'frmListLookupRKATUnit',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListKUPPA(objForm),
				pnlButtonLookupRKATUnit
			],
			tbar:
			{
				xtype: 'toolbar',
				items: 
				[
					{ xtype: 'tbtext', text: 'Tahun : ', cls: 'left-label', width: 40 },
					mCombo_TahunAnggaran(100,'cboThnLookupRKATUnit'),
					{
						xtype: 'spacer',
						width: 30
					},
					{ xtype: 'tbtext', text: 'RAB : ', cls: 'left-label', width: 40 },
						new Ext.form.ComboBox(
						{
							id: 'cboRKATLookupRKATUnit',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender: true,
							mode: 'local',
							displayField: 'value',
							valueField: 'key',
							width:100,	
							store: new Ext.data.ArrayStore
							(
								{
									id: 'dscboRKATLookupRKATUnit',
									fields:['key','value'],
									data:[
										['','Semua'],
										['Rutin','Rutin'],
										['Non Rutin','Non Rutin']//,//['Pengembangan','Pengembangan'],
										//['Other Pengembangan','Other Pengembangan'],
										//['Other Rutin','Other Rutin']
									]
								}
							)
						}
						),
					{
						xtype: 'spacer',
						width: 30
					},
					{ xtype: 'tbtext', text: 'No. Akun / Deskripsi: ', cls: 'left-label', width: 110 },
					{
						xtype: 'textfield',
						id: 'numAkunLookupRKATUnit',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									RefreshDataLookupRKATUnit(Ext.getCmp('cboRKATLookupRKATUnit').getValue(), Ext.getCmp('numAkunLookupRKATUnit').getValue());		//dh						
								} 						
							}
						}
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupRKATUnit',
						iconCls: 'refresh',
						text:'Tampilkan',
						handler: function()
						{
							RefreshDataLookupRKATUnit(Ext.getCmp('cboRKATLookupRKATUnit').getValue(), Ext.getCmp('numAkunLookupRKATUnit').getValue());
						}
					}
				]
			}
		}
	);
	
	return frmListLookupRKATUnit;
}

function mCombo_TahunAnggaran(lebar,NamaCbo) 
{
	// var Field = ['TAHUN_ANGGARAN_TA', 'CLOSED_TA', 'TMP_TAHUN'];
	// dsListTahun_TahunAnggaran = new WebApp.DataStore({ fields: Field });
	// RefreshComboTahun_TahunAnggaran();
	var currYear = parseInt(now.format('Y'));
	var combo_TahunAnggaran = new Ext.form.ComboBox
	(
		{
		    id: NamaCbo,
		    name: NamaCbo,			
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			align:'Right',
			disabled:true,
			// anchor:'100%',//'70%',			
			width: lebar,
			// store: dsListTahun_TahunAnggaran,
			// valueField: 'TAHUN_ANGGARAN_TA',
			// displayField: 'TMP_TAHUN',
			// value:gstrTahunAngg +'-'+(Ext.num(gstrTahunAngg)+1),
			value:'',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1 +'/' + (Ext.num(currYear) + 2)], 
						[currYear,currYear +'/' + (Ext.num(currYear) + 1)]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText'	,
			value: Ext.num(now.format('Y')),
			listeners:
			{
				'select': function(a,b,c)
				{
				    // SelectThn_TahunAnggaran = b.data.TAHUN_ANGGARAN_TA;
				}
			}
		}
	);
	
	return combo_TahunAnggaran;
}

function RefreshComboTahun_TahunAnggaran()
{
	dsListTahun_TahunAnggaran.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_KOMPONEN',
				Sortdir: 'ASC', 
				target:'vicomboKomponen',
				param: ''
			} 
		}
	);
}
function getGridListKUPPA(objForm)
{
	var _fields = 
	[
		'tahun_anggaran_ta', 'kd_unit_kerja', 'kd_jns_rkat_jrka', 'no_program_prog',
		'prioritas', 'kegiatan', 'rkat', 'nama_program_prog','unitkerja', 
		'jmlh_rkatrdet','account', 'deskripsi_rkatrdet','approved','total','name','line','jmlh_rkatrdet_sisa'
	];
	dsGLLookupRKATAll_Unit = new WebApp.DataStore({ fields: _fields });
	
	RefreshDataLookupRKATUnit(); 
	
	gridListLookupRKATUnit = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookupRKATUnit',
			store: dsGLLookupRKATAll_Unit,
			anchor: '100% 90%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			autoScroll: true,
			border: false,
			viewConfig :
			{
				forceFit: true
			},
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							selectedrowGLLookupRKATAll_Unit = dsGLLookupRKATAll_Unit.getAt(row);
						}
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{ 
						id: 'colRKATGLLookupRKATUnit',
						header: 'RAB',
						dataIndex: 'rkat',
						width: 50,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},
					{ 
						id: 'colNoAkunGLLookupRKATUnit',
						header: 'No. Akun',
						dataIndex: 'account',
						width: 90,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},
					{ 
						id: 'colNmAkunGLLookupRKATUnit',
						header: 'Nama Akun',
						dataIndex: 'name',
						width: 100,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},
					{
						id: 'colNUraianProgGLLookupRKATUnit',
						header: 'Deskripsi',
						dataIndex: 'deskripsi_rkatrdet',
						width: 100,
						hidden: false,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{
						id: 'colNUraianProgGLLookupRKATUnit',
						header: 'Program',
						dataIndex: 'nama_program_prog',
						width: 100,
						hidden: true,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colJumGLLookupRKATUnit',
						header: 'Jumlah (Rp.)',
						dataIndex: 'jmlh_rkatrdet_sisa',
						width: 80,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
							return str;
						}
					}
				]
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					if(selectedrowGLLookupRKATAll_Unit !== undefined)
					{
						insertToGridDetailEntrySP3DForm(selectedrowGLLookupRKATAll_Unit);
						mCancel=false;
						console.log(selectedrowGLLookupRKATAll_Unit.data);
						Ext.getCmp(objForm.RKAT).setValue(selectedrowGLLookupRKATAll_Unit.data.rkat);
						//Ext.getCmp(objForm.RKAT).setValue('test');
						Ext.getCmp(objForm.JNSRKAT).setValue(selectedrowGLLookupRKATAll_Unit.data.kd_jns_rkat_jrka);
						Ext.getCmp(objForm.NOPROG).setValue(selectedrowGLLookupRKATAll_Unit.data.no_program_prog);
						Ext.getCmp(objForm.NMPROG).setValue(selectedrowGLLookupRKATAll_Unit.data.nama_program_prog);
						Ext.getCmp(objForm.PRIOR).setValue(selectedrowGLLookupRKATAll_Unit.data.prioritas);
						Ext.getCmp(objForm.KEG).setValue(selectedrowGLLookupRKATAll_Unit.data.kegiatan);
						// Ext.getCmp(objForm.JUM).setValue(formatCurrency(selectedrowGLLookupRKATAll_Unit.data.total));
						objForm.THNANGG = selectedrowGLLookupRKATAll_Unit.data.tahun_anggaran_ta;
						//GetJumlahRKATHistorySP3D(selectedrowGLLookupRKATAll_Unit.data.TAHUN_ANGGARAN_TA,selectedrowGLLookupRKATAll_Unit.data.KD_UNIT_KERJA,selectedrowGLLookupRKATAll_Unit.data.ACCOUNT,selectedrowGLLookupRKATAll_Unit.data.PRIORITAS,selectedrowGLLookupRKATAll_Unit.data.LINE);
						akun = selectedrowGLLookupRKATAll_Unit.data.account;
						jumlah = selectedrowGLLookupRKATAll_Unit.data.jumlah;
						line = selectedrowGLLookupRKATAll_Unit.data.line;

						//alert(selectedrowGLLookupRKATAll_Unit.data.RKAT+' '+selectedrowGLLookupRKATAll_Unit.data.NO_PROGRAM_PROG);

						if (selectedrowGLLookupRKATAll_Unit.data.no_program_prog == '')
						{
							jnsrkatr='1'
							StrJnsRKATEntrySP3DForm = '1'
						}else if (selectedrowGLLookupRKATAll_Unit.data.no_program_prog != '')
						{
							jnsrkatk='2'
							StrJnsRKATEntrySP3DForm = '2'
						}else
						{
							jnsrkato='3'
							StrJnsRKATEntrySP3DForm = '3'
						}
						 //alert(selectedrowGLLookupRKATAll_Unit.data.NO_PROGRAM_PROG+':'+jnsrkatk+'-'+jnsrkatr+'-'+jnsrkato);
						winFormLookupRKATAll_Unit.close();
					}
					
				}
			}
		}
	);
	return gridListLookupRKATUnit;
}

function RefreshDataLookupRKATUnit(jenis_rab,no_akun_des)
{
	/* dsGLLookupRKATAll_Unit.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viewLookupRKAT_All',
			    param: criteria
			}
		}
	);
	return dsGLLookupRKATAll_Unit; */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPPD/LoadRAB",
		params: {
			tanggal			: Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
			kd_unit_kerja	: selectCboUnitKerjaEntrySP3DForm,
			jenis_rab		: jenis_rab,
			no_akun_des		: no_akun_des
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLLookupRKATAll_Unit.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLLookupRKATAll_Unit.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLLookupRKATAll_Unit.add(recs);
				gridListLookupRKATUnit.getView().refresh();
			
			} else {
				ShowPesanError('Gagal menampilkan data RAB', 'Error');
			};
		}
	});
	
}

function getCriteriaLookupRKATUnit(unit)
{
	var strKriteria = "";
	if(Ext.getCmp('cboRKATLookupRKATUnit').getValue() !== "")
	{
		strKriteria += "rkat=" + Ext.getCmp('cboRKATLookupRKATUnit').getValue();
	}
	if(Ext.getCmp('numAkunLookupRKATUnit').getValue() !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "akun=" + Ext.getCmp('numAkunLookupRKATUnit').getValue(); 
	}
	if(unit !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + unit;
	}
	if (SelectThn_TahunAnggaran != undefined)
	{	
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		// strKriteria += "tahun=" + ((Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear() < 1900 ? 1900 + (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear() : (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear());
		strKriteria += "thn=" + SelectThn_TahunAnggaran//Ext.getCmp('cboThnLookupRKATUnit').getValue();
	}else
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "thn=" + gstrTahunAngg
	}
	
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + '1' //Pengeluaran Penting!!
	
	return strKriteria;
}

// form lookup 2
var winFormLookupRKATAll_viewUserApp;
var dsGLLookupRKATAll_viewUserApp;
var selectedrowGLLookupRKATAll_viewUserApp;
var mCancel=true;
var str_Nosp3d;
var dsGridViewAppSP3DForm;


function FormLookup_viewUserApp(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d)
{
	winFormLookupRKATAll_viewUserApp = new Ext.Window
	(
		{
		    id: 'winFormLookupRKATAll_viewUserApp',
		    title: 'Info Approved',
		    closeAction: 'destroy',
		    closable: true,
		    width: 320,
		    height: 280,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'Pencairan',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookupRKATAll_viewUserApp(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d)],
			listeners:
			{
				'beforeclose': function()
				{
					if (mCancel != true)
					{
						loadDetailRKATEntrySP3DForm(getCriteriaDetailRKATEntrySP3DForm());
					}
				},
				activate: function()
				{
					mCancel=true;
				}
			}
		}
	);
	
	winFormLookupRKATAll_viewUserApp.show();
}

function getItemFormLookupRKATAll_viewUserApp(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d)
{
	var pnlButtonLookupRKATUnit = new Ext.Panel
	(
		{
			id: 'pnlButtonLookupRKATUnit',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			// // style: { 'margin-left': '4px', 'margin-top': '3px' },
			// anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'bottom'
			},
			items:
			[
				{
					xtype: 'button',
					width: 70,
					text: 'Ok',
					handler: function()
					{
						winFormLookupRKATAll_viewUserApp.close();						
					}
				}
				// ,
				// {
				// 	xtype: 'button',
				// 	width: 70,
				// 	text: 'Cancel',
				// 	handler: function()
				// 	{
				// 		selectedrowGLLookupRKATAll_viewUserApp = undefined;
				// 		winFormLookupRKATAll_viewUserApp.close();
				// 	}
				// }
			]
		}
	);
	
	var frmListLookupRKATUnit = new Ext.Panel
	(
		{
			id: 'frmListLookupRKATUnit',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridViewAppSP3DForm(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d),
				pnlButtonLookupRKATUnit
			],
			tbar:
			{
				xtype: 'toolbar',
				items: 
				[
					
				]
			}
		}
	);
	
	return frmListLookupRKATUnit;
}

function RefreshDataGridViewAppSP3DForm(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d)
{
	/* dsGridViewAppSP3DForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 10, 
				Sort: '',
				Sortdir: 'ASC', 
				target: 'ViewUserLvlApp',
				param: "where NO_TRANSAKSI='" + nosp3d + "'"
			} 
		}
	);	
	return dsGridViewAppSP3DForm; */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPPD/getInfoApproved",
		params: {
			tahun_anggaran_ta 	:	tahun_anggaran_ta,
			kd_unit_kerja		: 	kd_unit_kerja,
			no_sp3d_rkat		:	no_sp3d,
			tgl_sp3d_rkat		:	tgl_sp3d
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data approve PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGridViewAppSP3DForm.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGridViewAppSP3DForm.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGridViewAppSP3DForm.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data approved!', 'Error');
			};
		}
	});
}

function getGridViewAppSP3DForm(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d)
{
	
	var Fields = [
		'no_transaksi','kd_user','user_names','tgl_approve','jumlah','user_level','group_name'
	];
	dsGridViewAppSP3DForm = new WebApp.DataStore({ fields: Fields });
	
	var GridViewAppSP3DForm = new Ext.grid.EditorGridPanel(
		{
			id: 'GridViewAppSP3DForm',
			border: false,
			region: 'center',
			anchor: '100%',
			// width:330,
			height:200,
			store: dsGridViewAppSP3DForm,
			autoScroll: true,
			// viewConfig: { forceFit: true },
			sm: new Ext.grid.RowSelectionModel(
				{
					singleSelect: true,
					listeners:
					{
						'rowselect': function(sm, row, rec)
						{
							
						}
					}
				}
			),
			cm: new Ext.grid.ColumnModel(
				[
					// new Ext.grid.RowNumberer(),
					{
						xtype: 'gridcolumn',
						id: 'COL_USER_LEVEL',
						dataIndex: 'user_level',
						width: 100,
						header: 'Tahap Approved'
					}
					,
					{
						xtype: 'gridcolumn',
						id: 'COL_TGL_APPROVE',
						dataIndex: 'tgl_approve',
						header: 'Tanggal',
						width: 200,
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.tgl_approve);
						}
					}
				]
			),
			listeners:
			{
				'afterrender': function()
				{
					RefreshDataGridViewAppSP3DForm(tahun_anggaran_ta,kd_unit_kerja,no_sp3d, tgl_sp3d);
				}
			}
		}
	);
	
	return GridViewAppSP3DForm;
} 

//form lookup upload file
// <reference path="../../ext-base.js" />
// <reference path="../../ext-all.js" />

var rowSelectedLook_vupload;
var strpathfile;
var FormWindow;
var nAsal;
var strDirektori;
var LFormLPJ=1;
var LFormDPD=2;
var LFormPM=3;
var vWinFormEntryUploadfile;

function FormLookupUploadfile(Asal,Direktori) 
{
    vWinFormEntryUploadfile = new Ext.Window
	(			
		{
			id: 'formLookupUploadfile',
			title: 'Upload File',
			closeAction: 'destroy',
			width: 450,
			height: 120,
			border: false,			
			frame: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',			
			modal: true,                                   
			items: 
			[
				GetpanelUploadFile()
			],		
			listeners:
			{ 
				activate: function()
				{ 
					nAsal=Asal;
					strDirektori=Direktori;
				} 
			}
		}
	);	
    vWinFormEntryUploadfile.show();
};

function GetpanelUploadFile()
{
    var vWinFormUploadfile = new Ext.FormPanel
	({
		//id: 'FormUpldSeminar',
		layout: 'form',
		width: 400,
		height: 100,
		frame: true,
		border: false,
		fileUpload: true,		
		anchor: '100% 100%',
		items:
		[
			{
				xtype: 'fileuploadfield',
				emptyText: 'Path file upload',
				fieldLabel: 'Path file',
				frame: false,
				border: false,
				name: 'file',
				anchor: '100%',
				buttonText: 'browse'
			},
			{
				xtype: 'hidden',
				name: 'direktori',
				id: 'direktori',
				value: 'uploader'
			}
		],
		buttons:
		[
			{
				text: 'Upload',
				handler: function() 
				{
					Ext.get('direktori').dom.value = strDirektori;					  
					if (vWinFormUploadfile.getForm().isValid()) 
					{
						vWinFormUploadfile.getForm().submit
						({
							url: "./home.mvc/Upload",
							waitMsg: 'Uploading your file...',								
							success: function(form, o) //(3)
							{	
								strpathfile = o.result.namafile;					        
								Ext.Msg.show
								({
									title: 'Result',
									msg: o.result.result,
									buttons: Ext.Msg.OK,
									icon: Ext.Msg.INFO
								});
							},								
							failure: function(form, o) //(4)
							{
								Ext.Msg.show
								({
									title: 'Result',
									msg: o.result.error,
									buttons: Ext.Msg.OK,
									icon: Ext.Msg.ERROR
								});
							}
						});
					}
				}
			},
			{
				text: 'OK',
				handler: function() 
				{
					var pathupload;
					pathupload = './' + strDirektori + '/' + strpathfile;
					if (nAsal == LFormLPJ)
					{
						Ext.get('txtPathfile_LPJ').dom.value = pathupload;
					}
					else if (nAsal == LFormDPD)
					{
						Ext.get('txtPathfileSP3DForm').dom.value = pathupload;
					}
					else if (nAsal == LFormPM)
					{
						Ext.get('txtPathfile_viImportDataBNI').dom.value = strpathfile;
						///ProsesImport_viImportDataBNI(strpathfile);
					};
					//vWinFormEntryUploadfile = Ext.getCmp('FormLookupUploadfile')
					vWinFormEntryUploadfile.close();
				}
			}
		]
	});     
	return vWinFormUploadfile;
};

function ShowPesanWarning(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfo(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanError(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function getNumber(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};

function CetakPPD(){
	var params={
		tahun_anggaran				:TahunAngg,
		no_sp3d						:Ext.get('txtNoEntrySP3DForm').getValue(),
		kd_unit_kerja				:selectCboUnitKerjaEntrySP3DForm,
		tgl_sp3d					:Ext.getCmp('dtpTglEntrySP3DForm').getValue(),
		keterangan					:Ext.getCmp('txtaKetEntrySP3DForm').getValue()
	} 
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/anggaran_module/functionPPD/CetakPPD");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();		
}
///---------------------------------------------------------------------------------------///