var dsGListApprovalRKAT;
var rowselectGListApprovalRKAT;
var selectCboUnitKerjaFilterApprovalRKAT;
var selectCboMaksDataFilterApprovalRKAT = 50;
var winEntryApprovalRKAT;
var rowselectGListApprovalRKAT;
var now = new Date();
var selectCboUnitKerjaFormApprovalRKAT;
var AddNewAprrovalRKAT;
var isApproved;
var JnsAnggaran;
var jumlah_ApprovalRKAT;
var grdListApprovalRKAT;

CurrentPage.page = getPanelApprovalRKAT(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelApprovalRKAT(mod_id)
{
	var Fields = [
		'kd_unit_kerja',
		'tahun_anggaran_ta',
		'nama_unit',
		'plafond_plaf',
		'sisa',
		'jumlah',
		'sapproved',
		'kd_jns_plafond',
		'jns_plafond',
		'ket',
		'tahun_anggaran_ta_tmp',
		'disahkan_rka',
		'disahkan_rka_tmp'
	];
	dsGListApprovalRKAT = new WebApp.DataStore({ fields: Fields });
	
	var chkApprovedApprovalRKAT = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprovedApprovalRKAT',
			header: 'App',
			align: 'center',			
			dataIndex: 'disahkan_rka_tmp',
			disabled: true,
			readOnly:true,
			width: 30			
		}
	);
	
	grdListApprovalRKAT = new Ext.grid.EditorGridPanel(
		{
			id: 'grdListApprovalRKAT',
			xtype: 'editorgrid',
			store: dsGListApprovalRKAT,
			anchor: '100% 100%',
			columnLines: true,
			border: false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						'rowselect': function(sm, row, rec) 
						{
							rowselectGListApprovalRKAT = dsGListApprovalRKAT.getAt(row);
						}
					}
				}
			),
			//cm: new Ext.grid.ColumnModel
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'col_thnangg_approvalrkat',
						header: 'Periode',
						dataIndex: 'tahun_anggaran_ta',
						sortable: true,			
						width : 100,
						filter: {}
					},
					{
						id: 'col_unit_approvalrkat',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit',
						sortable: true,			
						width :495,
						filter: {}
					},
					{
						id: 'col_plafond_approvalrkat',
						header: 'Plafond',
						dataIndex: 'plafond_plaf',
						sortable: true,			
						width :150,
						renderer: function(v, params, record)
						{
							return "<div style='text-align: right;'>" + formatCurrency(record.data.plafond_plaf) + "</div>";
						},
						filter: {}
					},
					{
						id: 'col_Jenis_approvalrkat',
						header: 'Jenis Anggaran',
						dataIndex: 'jns_plafond',
						sortable: true,			
						width :136,
						filter: {}
					}
					,chkApprovedApprovalRKAT					
				]
			),	
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowselectGListApprovalRKAT = dsGListApprovalRKAT.getAt(ridx);
					 if(rowselectGListApprovalRKAT !== undefined)
					{
						showWinEntryApprovalRKAT(rowselectGListApprovalRKAT.data);
					}
					else
					{
						ShowPesanWarning("Data belum dipilih.");
					}
				}
				// End Function # --------------
			},
			tbar: 
			[
				{
					id:'btnEditApprovalRKAT',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{
						
						 if(rowselectGListApprovalRKAT !== undefined)
						{
							showWinEntryApprovalRKAT(rowselectGListApprovalRKAT.data);
						}
						else
						{
							ShowPesanWarning("Data belum dipilih.");
						}
					}
				},
				{
					id:'btnAppAllApprovalRKAT',
					text: 'Approve All',
					tooltip: 'Approve All',
					iconCls: 'approve',
					handler: function(sm, row, rec) 
					{
						AllApprovalRKAT()
					}
				},
				{
					id:'btnUnAppAllApprovalRKAT',
					text: 'UnApprove All',
					tooltip: 'UnApprove All',
					iconCls: 'RemoveRowAset',
					handler: function(sm, row, rec) 
					{
						AllUnApprovalRKAT()
					}
				}
			],
			viewConfig: { forceFit: false },
			//plugins: chkApprovedApprovalRKAT,
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsGListApprovalRKAT,
					pageSize: selectCboMaksDataFilterApprovalRKAT,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			)
		}
	);
	
	var pnlListApprovalRKAT = new Ext.Panel(
		{
			id: mod_id,
			closable:true,
			layout: 'form',
			region: 'center',
			// title: 'Pengesahan '+gstrrkat,          			 		
			title: 'Pengesahan RAB',          			 		
			iconCls: 'Rencana',
			items: [grdListApprovalRKAT],
			tbar:
			[
				{ xtype: 'tbseparator' },
				// { xtype: 'tbtext', text: gstrSatker +' : ', cls: 'tb-label' },
				{ xtype: 'tbtext', text: 'Unit Kerja' +' : ', cls: 'tb-label' },
				mCboUnitKerjaFilterApprovalRKAT(),	
				{ xtype: 'tbtext', text: 'Periode : ', cls: 'tb-label' },
				{ xtype: 'tbseparator' },
				mComboTahunFilterApprovalRKAT(),
				// { xtype: 'tbseparator' },
				// { xtype: 'tbtext', text: 'Maks. Data : ', cls: 'tb-label' },
				// mComboMaksDataFilterApprovalRKAT(),	
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApprovalRKAT',
					boxLabel: 'Approved',
					listeners: 
					{
						check: function()
						{
						   RefreshDataGListApprovalRKAT();
						}
					}
				},				
				{ xtype: 'tbfill' },
				{
					xtype: 'button',
					id: 'btnRefreshGListApprovalRKAT',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(){
						RefreshDataGListApprovalRKAT(true); 
					}
				}
			],
			listeners:
			{
				'afterrender': function(){ RefreshDataGListApprovalRKAT(); }
			}
		}
	);
	
	return pnlListApprovalRKAT;
}

function AllApprovalRKAT()
{	
	Ext.Msg.show
	(
		{
			title:'Approve All',
			msg: "Akan melakukan Approve All tahun " + Ext.getCmp('cboTahunFilterApprovalRKAT').getValue() + "? " ,
			buttons: Ext.MessageBox.YESNO,
			width:300,
			fn: function (btn)
			{					
				if (btn =='yes')
				{
					isApproved=true;
					ProsesAllApprovalRKAT();
					RefreshDataGListApprovalRKAT(true); 
				} // end if btn yes
			}// end fn
		}
	)//end msg.show
 }

 function AllUnApprovalRKAT()
{	
	Ext.Msg.show
	(
		{
			title:'UnApprove All',
			msg: "Akan melakukan UnApprove All tahun " + Ext.getCmp('cboTahunFilterApprovalRKAT').getValue() + "? " ,
			buttons: Ext.MessageBox.YESNO,
			width:300,
			fn: function (btn)
			{					
				if (btn =='yes')
				{
					isApproved=false;
					ProsesAllApprovalRKAT();
					RefreshDataGListApprovalRKAT(true); 
				} // end if btn yes
			}// end fn
		}
	)//end msg.show
 }

 
function ProsesAllApprovalRKAT()
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran_module/functionPengesahanRAB/ApproveRAB",
			params: {
				tahun_anggaran_ta		: Ext.getCmp('cboTahunFilterApprovalRKAT').getValue(),
				is_all					:true,
				approve					: isApproved,
				kd_unit_kerja			: '',
				kd_jns_plafond			: ''
			},
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);		
				if (cst.success == true) 
				{
					ShowPesanInfo('Data berhasil diproses','Approve/Unapprove All');									
				}
				else 
				{
					ShowPesanError('Data tidak berhasil diproses','Approve/Unapprove All');
				}
				
				RefreshDataGListApprovalRKAT(false);
			}
		}
	);	
}

function mComboTahunFilterApprovalRKAT()
{
	var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThnFilterApprovalRKAT = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThnFilterApprovalRKAT.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThnFilterApprovalRKAT.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThnFilterApprovalRKAT.add(recs);
			} 
		}
	});
	
	var currYear = parseInt(now.format('Y'));
	var cboTahunFilterApprovalRKAT = new Ext.form.ComboBox
	(
		{
			id:'cboTahunFilterApprovalRKAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',			
			width: 100,
			store: dsThnFilterApprovalRKAT,
			// store: new Ext.data.ArrayStore
			// (
				// {
					// id: 0,
					// fields: 
					// [
						// 'Id',
						// 'displayText'
					// ],
					// data: 
					// [						
						// [currYear + 1,currYear + 1 ], 
						// [currYear,currYear ]
					// ]
				// }
			// ),
			// valueField: 'TAHUN_ANGGARAN_TA',
			// displayField: 'TMP_TAHUN'
			// value: gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1),
			// displayField: 'displayText'	,
			// valueField: 'Id',
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			value: Ext.num(now.format('Y')),
			 listeners:
			{
			    'select': function(a, b, c) 
				{
					RefreshDataGListApprovalRKAT();
			    }
			}
		}
	);	
	
	return cboTahunFilterApprovalRKAT;
}

function mCboUnitKerjaFilterApprovalRKAT() 
{
    var Fields = ['kd_unit', 'nama_unit'];
    var dsCboUnitKerjaFilterApprovalRKAT = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaFilterApprovalRKAT.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaFilterApprovalRKAT.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaFilterApprovalRKAT.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
    var cboUnitKerjaFilterApprovalRKAT = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaFilterApprovalRKAT',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+ gstrSatker +'...',
		    emptyText: 'Pilih Unit Kerja...',
		    // fieldLabel: 'Unit/Sub-'+gstrSatker,
		    fieldLabel: 'Unit/Sub-',
		    align: 'right',
		    width: 200,
		    store: dsCboUnitKerjaFilterApprovalRKAT,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectCboUnitKerjaFilterApprovalRKAT = b.data.kd_unit;
					RefreshDataGListApprovalRKAT();
			    }
			}
		}
	);	
	
    return cboUnitKerjaFilterApprovalRKAT;
}


function RefreshDataGListApprovalRKAT_lama(filter)
{
	/* dsGListApprovalRKAT.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCboMaksDataFilterApprovalRKAT, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viPengesahan',
				param: criteria
			} 
		}
	);
	return dsGListApprovalRKAT; */
	
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPengesahanRAB/getPlafond",
		params: {
			filter				:filter,
			kd_unit				:Ext.getCmp('cboUnitKerjaFilterApprovalRKAT').getValue(),
			thn_anggaran		:Ext.getCmp('cboTahunFilterApprovalRKAT').getValue(),
			approve				:Ext.getCmp('chkFilterApprovalRKAT').getValue()
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan daftar RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsGListApprovalRKAT.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGListApprovalRKAT.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGListApprovalRKAT.add(recs);
				grdListApprovalRKAT.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan daftar RAB!', 'Error');
			};
		},
		callback:function(){
			var gridLisRAB=Ext.getCmp('grdListApprovalRKAT').getStore().data.items;
				for(var i=0,iLen=gridLisRAB.length; i<iLen;i++){
					if(gridLisRAB[i].data.disahkan_rka == 't'){
						gridLisRAB[i].data.disahkan_rka_tmp = true;
					} 
				}
			Ext.getCmp('grdListApprovalRKAT').getView().refresh();
			
		}
	});
}

function RefreshDataGListApprovalRKAT (){
	var criteria = GetCriteriaGridUtama();
	dsGListApprovalRKAT.removeAll();
	dsGListApprovalRKAT.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCboMaksDataFilterApprovalRKAT, 
				Sort: '', 
				Sortdir: '', 
				target: 'vi_viewdata_pengesahan_rab',
				param: criteria
			} ,
			callback:function(){
				var gridLisRAB=Ext.getCmp('grdListApprovalRKAT').getStore().data.items;
					for(var i=0,iLen=gridLisRAB.length; i<iLen;i++){
						if(gridLisRAB[i].data.disahkan_rka == 't'){
							gridLisRAB[i].data.disahkan_rka_tmp = true;
						} 
					}
				Ext.getCmp('grdListApprovalRKAT').getView().refresh();
				
			}
		}
	);
	return dsGListApprovalRKAT; 
}

function GetCriteriaGridUtama(){
	var criteria = '';
	if (Ext.getCmp('cboUnitKerjaFilterApprovalRKAT').getValue() == '000' || Ext.getCmp('cboUnitKerjaFilterApprovalRKAT').getValue() == 000){
		criteria = "SEMUA~WHERE  a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunFilterApprovalRKAT').getValue() + " '  and d.disahkan_rka ='" +  Ext.getCmp('chkFilterApprovalRKAT').getValue()+ "' ";
	}else{
		criteria = "  WHERE a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunFilterApprovalRKAT').getValue() + " '  and d.disahkan_rka ='" +  Ext.getCmp('chkFilterApprovalRKAT').getValue()+ "' and a.kd_unit_kerja = '"+ Ext.getCmp('cboUnitKerjaFilterApprovalRKAT').getValue()+"'";
	}
	
	return criteria;
}

function getCriteriaGListApprovalRKAT()
{
	var strKriteria = "";
	
	if(Ext.getCmp('cboTahunFilterApprovalRKAT').getValue() !== "")
	{
		strKriteria += "tahun=" + Ext.getCmp('cboTahunFilterApprovalRKAT').getValue();
	}
	
	if(selectCboUnitKerjaFilterApprovalRKAT !== undefined)
	{
		if(strKriteria !== "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectCboUnitKerjaFilterApprovalRKAT;
	}
	
	if(Ext.getCmp('chkFilterApprovalRKAT').getValue() == true)
	{
		if(strKriteria !== "")
		{
			strKriteria += "&";
		}
		strKriteria += "approve=" + 1;
	}else{
		if(strKriteria !== "")
		{
			strKriteria += "&";
		}
		strKriteria += "approve=" + 0;
	}

	return strKriteria;
}

function showWinEntryApprovalRKAT(rowdata)
{	
    winEntryApprovalRKAT = new Ext.Window
	(
		{  
			id: 'winEntryApprovalRKAT',
			title: 'Pengesahan RAB ',
			closeAction: 'destroy',
			width: 640,
			height: 173,			
			resizable:false,
			plain: true,
			layout: 'border',
			iconCls: 'Rencana',
			modal: true,
			anchor: '100%',
			items: 
			[
				getFormHeaderApprovalRKAT()				
			],
			listeners:
			{
				'activate': function(){},
				'deactivate': function()
				{
					rowselectGListApprovalRKAT = undefined;
					RefreshDataGListApprovalRKAT(getCriteriaGListApprovalRKAT()); 
				},
				'beforedestroy': function()
				{
					rowselectGListApprovalRKAT = undefined;
					RefreshDataGListApprovalRKAT(getCriteriaGListApprovalRKAT()); 
				}
			}
		}
	);
 
	winEntryApprovalRKAT.show();
	
	if(rowdata !== undefined) {
		initDataApprovalRKAT(rowdata); 
	}
}

function getFormHeaderApprovalRKAT()
{
	var pnlFormHeaderApprovalRKAT =  new Ext.FormPanel(
		{
			id: 'pnlFormHeaderApprovalRKAT',
			border: false,
			region: 'center',
			height: 158,
			items:
			[
				{
					xtype: 'fieldset',
					style: 'margin: 5px;',
					layout: 'form',
					items:
					[
						{
							xtype: 'compositefield',
							// fieldLabel: gstrSatker,
							fieldLabel: '',
							defaults: { anchor: '100%', flex: 1 },
							items:
							[
								mCboUnitKerjaFormApprovalRKAT(),
								{ xtype: 'label', cls: 'left-label', forId: 'cboUnitKerjaFormApprovalRKAT', text: 'Tahun Anggaran : ', width: 120 },
								mComboTahunFormApprovalRKAT(),
								{
									xtype: 'textfield',
									id: 'txtTahunFormApprovalRKAT',
									fieldLabel: '',
									width:70,
									readOnly:true
								},
								{
									xtype: 'checkbox',
									id: 'chkApprovedFormApprovalRKAT',
									boxLabel: 'Approved',
									cls: 'aligned-radio',
									width: 100,
									readOnly: true,
									disabled: true
								}
							]
						},
						{
							xtype: 'textfield',
							id: 'txtPlafondApprovalRKAT',
							fieldLabel: 'Plafond',
							style: 'text-align: right;font-weight: bold;',
						},
						{
							xtype: 'textfield',
							id: 'txtStatusApprovalRKAT',
							fieldLabel: 'Status',
							style: 'color: #FF0000; font: bold 120% "Verdana";'
						}
					]
				}
			],
			tbar:
			[
				
				{ xtype: 'tbseparator' },
				{
					id:'btnApproveApprovalRKAT',
					text: 'Approve',
					iconCls: 'approve',
					handler: function() 
					{ 
						isApproved = true;
						SaveDataApprovalRKAT();
					}
				},
				{
					id:'btnUnapproveApprovalRKAT',
					text: 'Unapprove',
					iconCls: 'RemoveRowAset',
					handler: function() 
					{ 
						isApproved = false;
						SaveDataApprovalRKAT();
					}
				},
				{ xtype: 'tbfill' },
				{
					id:'btnPrintApprovalRKAT',
					text: 'Cetak',
					iconCls: 'print',					
					hidden:true,
					handler: function() 
					{ 

					}
				}
			]
		}
	);
	
	return pnlFormHeaderApprovalRKAT;
}

function mComboTahunFormApprovalRKAT()
{
	var Field = ['TAHUN_ANGGARAN_TA','TMP_TAHUN'];
	var dsThnFormApprovalRKAT = new WebApp.DataStore({ fields: Field });
	dsThnFormApprovalRKAT.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA', 
				Sortdir: 'ASC', 
				target: 'viCboThnAngg',
				param: 2
			} 
		}
	);
	
	var cboTahunFormApprovalRKAT = new Ext.form.ComboBox
	(
		{
			id:'cboTahunFormApprovalRKAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',			
			width: 60,
			store: dsThnFormApprovalRKAT,
			valueField: 'TAHUN_ANGGARAN_TA',
			// value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1),
			value:'',
			displayField: 'TMP_TAHUN',
			readOnly: true,
			hidden:true,
		}
	);	
	
	return cboTahunFormApprovalRKAT;
}

function mCboUnitKerjaFormApprovalRKAT() 
{
    var Fields = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA', 'UNITKERJA'];
    var dsCboUnitKerjaFormApprovalRKAT = new WebApp.DataStore({ fields: Fields });
	dsCboUnitKerjaFormApprovalRKAT.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    // param: "kdunit=" + gstrListUnitKerja
			    param: ''
			}
		}
	);
    
    var cboUnitKerjaFormApprovalRKAT = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaFormApprovalRKAT',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+gstrSatker+'...',
		    emptyText: 'Pilih...',
		    // fieldLabel: 'Unit/Sub-'+gstrSatker,
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    store: dsCboUnitKerjaFormApprovalRKAT,
		    valueField: 'KD_UNIT_KERJA',
		    displayField: 'UNITKERJA',
			readOnly: true,
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectCboUnitKerjaFormApprovalRKAT = b.data.KD_UNIT_KERJA;
			    }
			}
		}
	);	
	
    return cboUnitKerjaFormApprovalRKAT;
}

function initDataApprovalRKAT(rowdata)
{
	AddNewAprrovalRKAT = false;
	Ext.getCmp('cboUnitKerjaFormApprovalRKAT').setValue(rowdata.nama_unit);
	selectCboUnitKerjaFormApprovalRKAT = rowdata.kd_unit_kerja;
	Ext.getCmp('cboTahunFormApprovalRKAT').setValue(rowdata.tahun_anggaran_ta);
	Ext.getCmp('txtTahunFormApprovalRKAT').setValue(rowdata.tahun_anggaran_ta);
	Ext.getCmp('txtPlafondApprovalRKAT').setValue(formatCurrency(rowdata.plafond_plaf));
	
	console.log(rowdata.disahkan_rka);
	if(rowdata.disahkan_rka == 't'){
		Ext.getCmp('chkApprovedFormApprovalRKAT').setValue(true);
	}else{
		Ext.getCmp('chkApprovedFormApprovalRKAT').setValue(false);
	}
	
	isApproved = rowdata.disahkan_rka;
	jumlah_ApprovalRKAT = rowdata.jumlah;
	Ext.get('txtStatusApprovalRKAT').dom.value = rowdata.sapproved;
	JnsAnggaran = rowdata.kd_jns_plafond;
	if(isApproved == 't' && isApproved != "")
	{
		Ext.getCmp('btnApproveApprovalRKAT').setDisabled(true);
		Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(false);
	}
	else
	{
		Ext.getCmp('btnApproveApprovalRKAT').setDisabled(false);
		Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(true);
	}
}

function AddNewDataApprovalRKAT(rowdata)
{
	AddNewAprrovalRKAT = true;
	Ext.getCmp('cboUnitKerjaFormApprovalRKAT').reset();
	selectCboUnitKerjaFormApprovalRKAT = undefined;
	Ext.getCmp('cboTahunFormApprovalRKAT').setValue('');
	Ext.getCmp('txtPlafondApprovalRKAT').setValue(0);
	Ext.getCmp('chkApprovedFormApprovalRKAT').setValue(false);	
	Ext.get('txtStatusApprovalRKAT').dom.value = '';	
	jumlah_ApprovalRKAT='0';
	JnsAnggaran = '';
	isApproved = false;
	Ext.getCmp('btnApproveApprovalRKAT').setDisabled(false);
	Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(true);
	Ext.getCmp('btnDelApprovalRKAT').setDisabled(false);
}

function SaveDataApprovalRKAT()
{
	
	if (ValidasiEntryApprovalRKAT('Simpan Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionPengesahanRAB/ApproveRAB",
				params:  {
					tahun_anggaran_ta 	:Ext.getCmp('cboTahunFormApprovalRKAT').getValue(),
					kd_unit_kerja 		:selectCboUnitKerjaFormApprovalRKAT,
					kd_jns_plafond 		:JnsAnggaran,
					approve				:isApproved,
					is_all				:false
				}, 
				success: function(o) 
				{				
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						
						if(isApproved)
						{
							ShowPesanInfo('Data berhasil di Approve','Approve');
							Ext.getCmp('btnApproveApprovalRKAT').setDisabled(true);
							Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(false);
							Ext.getCmp('chkApprovedFormApprovalRKAT').setValue(true);
						}
						else
						{
							ShowPesanInfo('Data berhasil di Unapprove','Approve');
							Ext.getCmp('btnApproveApprovalRKAT').setDisabled(false);
							Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(true);
							Ext.getCmp('chkApprovedFormApprovalRKAT').setValue(false);
						}
						RefreshDataGListApprovalRKAT();
					
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarning('Data tidak berhasil di Approve','Edit Data');
					}
					else 
					{
						ShowPesanError('Data tidak berhasil di Approve ','Approve');
					}										
				}
			}
	)
		
		
		
		
		/* if (AddNewAprrovalRKAT == true) 
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamApprovalRKAT(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success == true) 
						{
							if(isApproved)
							{
								Ext.getCmp('btnApproveApprovalRKAT').setDisabled(true);
								Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(false);
							}
							else
							{
								Ext.getCmp('btnApproveApprovalRKAT').setDisabled(false);
								Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(true);
							}
							ShowPesanInfo('Data berhasil di simpan','Simpan Data');								
							AddNewAprrovalRKAT = false;
						}
						else if (cst == '{"pesan":0,"success":false}' )
						{
							ShowPesanWarning('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			);
		}
		else
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params: getParamApprovalRKAT(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success == true) 
						{
							if(isApproved)
							{
								Ext.getCmp('btnApproveApprovalRKAT').setDisabled(true);
								Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(false);
								//Ext.getCmp('btnDelApprovalRKAT').setDisabled(true);
							}
							else
							{
								Ext.getCmp('btnApproveApprovalRKAT').setDisabled(false);
								Ext.getCmp('btnUnapproveApprovalRKAT').setDisabled(true);
								//Ext.getCmp('btnDelApprovalRKAT').setDisabled(false);
							}
							ShowPesanInfo('Data berhasil di edit','Edit Data');
							AddNewAprrovalRKAT = false;
						}
						else if (cst.success === false && cst.pesan == 0)
						{
							ShowPesanWarning('Data tidak berhasil di edit, data tersebut tidak ada','Edit Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			);
		} */
	}
}

function ValidasiEntryApprovalRKAT()
{
	var valid = 1;

	if(selectCboUnitKerjaFormApprovalRKAT === undefined)
	{
		ShowPesanWarning(gstrSatker+' belum dipilih', 'Pengesahan '+gstrrkat);
		valid -= 1;
	}
	
	if(Ext.getCmp('cboTahunFormApprovalRKAT').getValue() == "")
	{
		ShowPesanWarning('Tahun anggaran belum dipilih','Pengesahan '+gstrrkat);
		valid -= 1;
	}
	
	return valid;
}


function DeleteDataApprovalRKAT()
{
	var fn_DeleteData = function(btn)
	{
		if(btn == "yes")
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/DeleteDataObj",
					params: getParamApprovalRKAT(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success == true) 
						{
							ShowPesanInfo('Data berhasil di hapus','Hapus Data');
							AddNewDataApprovalRKAT
						}
						else if (cst.success == false && cst.pesan == 0)
						{
							ShowPesanWarning('Data tidak berhasil di hapus, data tersebut tidak ada','Hapus Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di hapus','Hapus Data');
						}
					}
				}
			);
		}
	}
	
	ShowPesanConfirm("Yakin hapus data "+gstrrkat+" ini?", "Hapus Data", fn_DeleteData);
}

function ShowPesanWarning(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfo(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanError(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};