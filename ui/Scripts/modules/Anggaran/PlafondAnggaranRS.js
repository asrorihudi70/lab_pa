var CurrentPaguGNRL = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentTR_PaguGNRL = 
{
    data: Object,
    details:Array, 
    row: 0
};

var cellSelectedDet_PaguGNRL;
var mRecordPaguGNRL = Ext.data.Record.create
(
	[
	   {name: 'TAHUN_ANGGARAN_TA', mapping:'TAHUN_ANGGARAN_TA'},	   
	   {name: 'CLOSED_TA', mapping:'CLOSED_TA'},	   
	   {name: 'KD_UNIT_KERJA', mapping:'KD_UNIT_KERJA'},	   
	   {name: 'NAMA_UNIT_KERJA', mapping:'NAMA_UNIT_KERJA'},	   
	   {name: 'UNITKERJA', mapping:'UNITKERJA'},	   
	   {name: 'PLAFOND_PLAF', mapping:'PLAFOND_PLAF'}  
	]
);

var dsDTLTRList_PaguGNRL;
var dsPaguGNRLList;
var dsDTLPaguGNRLList;
var dsDTLPaguGNRLListTemp;
var cellSelectedPaguGNRLDet;
var fldDetail;
var AddNewPaguGNRL = false;
var now = new Date();
var selectCountPaguGNRL=50;
var PaguGNRLLookUps;
var rowSelectedPaguGNRL;
var dsUnitKerjaPaguGNRL;
var dsUnitKerjaPaguGNRLEntry;
//var selectUnitKerjaPaguGNRL = gstrListUnitKerja;
var strUnitKerjaPaguGNRL = 'Semua';
//var selectUnitKerjaPaguGNRLEntry = gstrListUnitKerja;

var dsThAnggPaguGNRLFilter;
var dsThAnggPaguGNRLEntry;
var grListPaguGNRL;
var mKD_UNIT_KERJA_TMP;
var mTAHUN_ANGGARAN_TA_TMP;
var gridDTLTR_PaguGNRL;
var mod_name="Plafond Anggaran RS";
var baru =true;
CurrentPage.page=getPanelPaguGNRL(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelPaguGNRL(mod_id)
{
    var Field =
	[
		'tahun_anggaran_ta', 'jumlah_plafond', 
		'jumlah_terpakai', 'app_plafond_general','app_plafond_general_tmp','tahun_anggaran_ta_tmp'
	];
    dsPaguGNRLList = new WebApp.DataStore({ fields: Field });
	
	var chkApproved_paguGNRL = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApproved_paguGNRL ',
			header: 'Approved',
			align: 'center',			
			dataIndex: 'app_plafond_general_tmp',
			disabled: true,
			width: 12
			
		}
	);
	RefreshDataPaguGNRL();
    grListPaguGNRL = new Ext.grid.EditorGridPanel
	(
		{
			id:'grListPaguGNRL',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsPaguGNRLList,
			columnLines:true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedPaguGNRL = dsPaguGNRLList.getAt(row);
						}
					}
				}
			),
			//cm: new Ext.grid.ColumnModel
			colModel: new Ext.grid.ColumnModel
			(	
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colPeriode',
						header: "Periode",
						dataIndex: 'tahun_anggaran_ta',
						sortable: true,
						width: 70,
						filter: {}
					}, 
					{
						id: 'colPaguGNRL',
						header: "Plafond (Rp)",
						dataIndex: 'jumlah_plafond',
						align: 'right',
						sortable: true,
						width: 30,
						renderer: function(v, params, record) 
						{
							if(v !== undefined)
							{
								return formatCurrency(record.data.jumlah_plafond);
							}
							else
							{
								return 0;
							}
						},
						filter: {}
					},
					chkApproved_paguGNRL	
				]
			),
			tbar: 
			[
				{
					id:'btnEditDataPaguGNRL',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{
						if (rowSelectedPaguGNRL != undefined) 
						{
				            PaguGNRLLookUp(rowSelectedPaguGNRL.data);
				        }
				        else 
						{
				            PaguGNRLLookUp();
				        }
					}
				},' ','-'
			],
            listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedPaguGNRL = dsPaguGNRLList.getAt(ridx);
					if (rowSelectedPaguGNRL != undefined)
					{
						PaguGNRLLookUp(rowSelectedPaguGNRL.data);
					}
					else
					{
						PaguGNRLLookUp();
					}
				}
				// End Function # --------------
			},
			/* bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsPaguGNRLList,
					pageSize: selectCountPaguGNRL,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			) */
			bbar : bbar_paging(mod_name, selectCountPaguGNRL, dsPaguGNRLList),
			viewConfig: 
			{
                forceFit: true
            },
			
        }
	);
	
	
    var FormPaguGNRL = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: 'Plafond RS',
			margins:'0 5 5 0',
			bodyStyle: 'padding:15px',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'PaguGNRL',
			items: [grListPaguGNRL],
			tbar: 
			[
				' ','->',
				{
					xtype: 'button',
					id:'btnRefreshPaguGNRL',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(sm, row, rec) 
					{
						RefreshDataPaguGNRLFilter(false);
					}
				}
			]
		}
	);

	RefreshDataPaguGNRLFilter(true);	
    return FormPaguGNRL ;

};



function mComboTahunPaguGNRLEntry() 
{
	var currYear = parseInt(now.format('Y'));
	
	var TahunPaguGNRLEntry = new Ext.form.ComboBox
	(
		{
			id:'TahunPaguGNRLEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 4,currYear + 4 ], 
						[currYear + 3,currYear + 3 ], 
						[currYear + 2,currYear + 2 ], 
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear ]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value: Ext.num(now.format('Y'))
		}
	);
	
	return TahunPaguGNRLEntry;
};


function PaguGNRLLookUp(rowdata) 
{
	var mWidth=580;
    PaguGNRLLookUps = new Ext.Window
	(
		{  
			id: 'PaguGNRLLookUps',
			title: 'Plafond RS',
			closeAction: 'destroy',
			width: mWidth,
			height: 360,
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'PaguGNRL',
			modal: true,
			anchor: '100%',
			items: getFormEntryPaguGNRL(mWidth,rowdata),
			listeners: 
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 					
				    this.activate();
				},
				deactivate:function()
				{
					rowSelectedPaguGNRL=undefined;
					RefreshDataPaguGNRLFilter(true);
				}
			}
		}
	); 
	PaguGNRLLookUps.show();
	if (rowdata == undefined) 
	{
        PaguGNRLAddNew(rowdata);
    }
    else 
	{
        PaguGNRLInit(rowdata);		
    }
};



function getFormEntryPaguGNRL(mWidth,rowdata) 
{

    var pnlPaguGNRL = new Ext.FormPanel    
	(
		{    
			id: 'PanelPaguGNRL',
			fileUpload: true,
			region: 'center',
			layout: 'column',
			height: 110,
			anchor: '100%',
			bodyStyle: 'padding:5px 5px 5px 5px',
			border: false,
			items: 
			[
				{
					layout: 'form',
					width:mWidth-27,
					anchor: '100%',
					bodyStyle: 'padding:5px 5px 5px 5px',
					autoHeight: true,
					items: 
					[	
						mComboTahunPaguGNRLEntry(),
						{
							xtype: 'textfield',
							fieldLabel: 'Anggaran',
							id:'txtAnggaranPaguGNRL',
							name:'txtAnggaranPaguGNRL',				
							width: 160,
							style: 
							{
								'text-align':'right'
							},
							listeners:
							{
								'blur': function()
								{
									this.setValue(formatCurrency(this.getValue()));
								},
								'focus': function()
								{
									this.setValue(getNumber(this.getValue()));
								}
							}
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Anggaran',
							id:'txt_app_plafond_general',
							name:'txt_app_plafond_general',				
							width: 160,
							hidden:true
						},
						
					]
				}
					
			],
			tbar:
			[
				{
					id:'btnTambahPaguGNRL',
					text: 'Tambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{
					     PaguGNRLAddNew(rowdata);	 
					}
				},
				'-', 
				{
					id:'btnSimpanPaguGNRL',
					text: 'Simpan',
					tooltip: 'Simpan Data ',
					iconCls: 'save',
					handler: function() 
					{ 
						PaguGNRLSave(false,rowdata); 
					}
				},'-',
				{
					id:'btnSimpanClosePaguGNRL',
					text: 'Simpan & Keluar',
					tooltip: 'Simpan Data dan Keluar',
					iconCls: 'saveexit',
					handler: function() 
					{ 
						// var x = PaguGNRLSave(true);
						PaguGNRLSave(true,rowdata);
						// RefreshDataPaguGNRLFilter(false);
						// if (x===undefined)
						// {
							// PaguGNRLLookUps.close();
						// };
					}
				},
				'-', 
				{
					id:'btnHapusPaguGNRL',
					text: 'Hapus',
					tooltip: 'Hapus',
					iconCls: 'remove',
					handler: function() 
					{ 
						HapusDetailPaguGNRL(rowdata);						
					}
				}, 
				'-', 
				{
					xtype: 'button',
					id: 'btnApproveEntryPaguGNRL',
					iconCls: 'approve',
					text: 'Approve',
					handler: function()
					{
						FormApprove(getAmount_PaguGNRL(Ext.get('txtTotalPaguGNRL').getValue()),'9',Ext.get('TahunPaguGNRLEntry').getValue())
					}
				},
				{
					xtype: 'button',
					id: 'btnunApproveEntryPaguGNRL',
					iconCls: 'remove',
					text: 'Unapprove',
					handler: function()
					{
						Unapprove_PaguGNRL()
					}
				},
				'->','-', 
				{
					id:'btnCetakPaguGNRL',
					text: 'Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',
					hidden: true,
					handler: function() 
					{ 
						var x=GetStrCriteriaCetak();
						if ( x != '')
						{
							ShowReport('', '231036', x); 
						};
	
					}
				}
			]
		}
	);
	
	var TotalPaguGNRL = new Ext.Panel
	(
		{
			id:'TotalPaguGNRL',
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '7px',
				'margin-left': '325px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: 'Total ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPaguGNRL',
					name:'txtTotalPaguGNRL',
					readOnly:true,					
					width: 160,
					listeners:
					{
						'change': function()
						{
							this.setValue(formatCurrency(this.getValue()));
						}
					}
				}
			]
		}
	);
	
	var GDtabDetail_PaguGNRL = new Ext.TabPanel
	(
		{
			region: 'center',
			id:'tabTabelDetail_PaguGNRL',
			name:'tabTabelDetail_PaguGNRL',
			activeTab: 0,			
			anchor: '100% 55%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: GetDTLTRGrid_PaguGNRL()			
		}
	);
		
	var FormPaguGNRL = new Ext.Panel   
	(
		{
			id: 'FormPaguGNRL',
			region: 'center',
			layout: 'form',
			title: '',
			// bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			// items: [pnlPaguGNRL,TotalPaguGNRL]
			items: [pnlPaguGNRL,GDtabDetail_PaguGNRL,TotalPaguGNRL]
        }
	);
                
     return FormPaguGNRL
};

function Unapprove_PaguGNRL()
{
	if (AddNewPaguGNRL === false)
	{
	Ext.Msg.show
		(
			{
			   title:'Unapprove PaguGNRL',
			   msg: 'Apakah data Pagu ini akan diunapprove pada tahun : '+ ' ' + Ext.get('TahunPaguGNRLEntry').dom.value + '? ',
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/anggaran_module/functionPlafondRS/unapprovePlafondRS",
								// params:  getParamAPPPaguGNRL(TglApprove, NoteApprove), 
								params:  {
									thn_anggaran :Ext.getCmp('TahunPaguGNRLEntry').getValue()
								}, 
								success: function(o) 
								{				
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										ShowPesanInfo('Data berhasil di Unapprove','Unapprove');
										//RefreshDataFilter_kbs(false);								
										Ext.getCmp('btnSimpanPaguGNRL').setDisabled(false);
										Ext.getCmp('btnSimpanClosePaguGNRL').setDisabled(false);
										Ext.getCmp('btnHapusPaguGNRL').setDisabled(false);
										Ext.getCmp('btnApproveEntryPaguGNRL').setDisabled(false);
									}
									else if (cst.success === false && cst.pesan === 0 )
									{
										ShowPesanWarning('Data tidak berhasil di Unapprove','Unapprove');
									}
									else 
									{
										ShowPesanError('Data tidak berhasil di Unapprove ','Unapprove');
									}										
								}
							}
						)
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}else
	{
		ShowPesanWarningPaguGNRL('Data tidak berhasil di Unpprove, data belum disimpan' ,'Edit Data');	
	}
};


function Approve_PaguGNRL(TglApprove, NoteApprove) 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran_module/functionPlafondRS/approvePlafondRS",
			// params:  getParamAPPPaguGNRL(TglApprove, NoteApprove), 
			params:  {
				thn_anggaran :Ext.getCmp('TahunPaguGNRLEntry').getValue()
			}, 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfo('Data berhasil di Approve','Approve');
					//RefreshDataFilter_kbs(false);								
					Ext.getCmp('btnSimpanPaguGNRL').setDisabled(true);
					Ext.getCmp('btnSimpanClosePaguGNRL').setDisabled(true);
					Ext.getCmp('btnHapusPaguGNRL').setDisabled(true);
					Ext.getCmp('btnApproveEntryPaguGNRL').setDisabled(true);
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning('Data tidak berhasil di Approve','Edit Data');
				}
				else 
				{
					ShowPesanError('Data tidak berhasil di Approve ','Approve');
				}										
			}
		}
	)
}


function getAmount_PaguGNRL(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};


function GetDTLTRGrid_PaguGNRL() 
{
	// grid untuk detail transaksi
	var fldDetail = 
	[
	'tahun_anggaran_ta', 'kd_komponen', 'jumlah_persen', 
	'jumlah_rp', 'komponen', 'tahun_anggaran_ta_tmp', 
	'kd_komponen_tmp']
	
	dsDTLTRList_PaguGNRL = new WebApp.DataStore({ fields: fldDetail })
	// dsTmpVerSP3D= new WebApp.DataStore({ fields: fldDetail })
	gridDTLTR_PaguGNRL = new Ext.grid.EditorGridPanel
	(
		{
			id:'gridDTLTR_PaguGNRL',
			title: 'Detail Plafond',
			stripeRows: true,
			store: dsDTLTRList_PaguGNRL,
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 99%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_PaguGNRL =dsDTLTRList_PaguGNRL.getAt(row);
							CurrentTR_PaguGNRL.row = row;
							//CurrentTRGL.data = cellSelectedGLDet;
						}
					}
				}
			),
			cm: TRDetailColumModel_PaguGNRL()
			, viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{	
							CalcTotal_PaguGNRL(); 
						} 
					);
					this.store.on("datachanged", function()
						{							
							CalcTotal_PaguGNRL();
						}
					);
				}
			}
		}
	);
			
	return gridDTLTR_PaguGNRL;
};

function TRDetailColumModel_PaguGNRL() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'kd_komponen',
				name: 'kd_komponen',
				header: "kd_komponen",
				dataIndex: 'kd_komponen',
				sortable: false,
				anchor: '30%',				
				width: 150,
				hidden:true
			},	
			{
				id: 'KOMPONEN',
				name: 'KOMPONEN',
				header: "Plafond",
				dataIndex: 'komponen',
				sortable: false,
				anchor: '30%',				
				width: 150
			},		 
			{
				id: 'JUMLAH_PERSEN',	
				name: 'JUMLAH_PERSEN',
				header: "Jumlah (%)",
				anchor: '10%',
				dataIndex: 'jumlah_persen',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jumlah_persen);
				},										
				editor: new Ext.form.NumberField
				(
					{
						id:'fieldJUMLAH_PERSEN',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									var total = getNumber(Ext.get('txtAnggaranPaguGNRL').getValue()) * (getNumber(this.getValue())/100)
									Ext.getCmp('fieldJUMLAH_RP').setValue(total);
									dsDTLTRList_PaguGNRL.data.items[CurrentTR_PaguGNRL.row].data.jumlah_rp = total;
									
									CalcTotal_PaguGNRL(CurrentTR_PaguGNRL.row);							
								}
							},
							'blur': function()
							{
									var total = getNumber(Ext.get('txtAnggaranPaguGNRL').getValue()) * (getNumber(this.getValue())/100)
									Ext.getCmp('fieldJUMLAH_RP').setValue(total);
									dsDTLTRList_PaguGNRL.data.items[CurrentTR_PaguGNRL.row].data.jumlah_rp = total;
								CalcTotal_PaguGNRL(CurrentTR_PaguGNRL.row);
							},
							'change': function()
							{
								var total = getNumber(Ext.get('txtAnggaranPaguGNRL').getValue()) * (getNumber(this.getValue())/100)
									Ext.getCmp('fieldJUMLAH_RP').setValue(total);
									dsDTLTRList_PaguGNRL.data.items[CurrentTR_PaguGNRL.row].data.jumlah_rp = total;
								CalcTotal_PaguGNRL(CurrentTR_PaguGNRL.row);
							}
						}
					}
				)
			},
			{
				id: 'JUMLAH_RP',				
				name: 'JUMLAH_RP',
				header: "Jumlah (Rp.)",
				anchor: '10%',
				dataIndex: 'jumlah_rp',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jumlah_rp);
				},											
				editor: new Ext.form.NumberField
				(
					{
						id:'fieldJUMLAH_RP',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey': function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									var totalPersen = (getNumber(this.getValue())/getNumber(Ext.get('txtAnggaranPaguGNRL').getValue())) * 100
									Ext.getCmp('fieldJUMLAH_PERSEN').setValue(Math.round(totalPersen));
									dsDTLTRList_PaguGNRL.data.items[CurrentTR_PaguGNRL.row].data.jumlah_persen = Math.round(totalPersen);
									console.log('aa');
									CalcTotal_PaguGNRL(CurrentTR_PaguGNRL.row);				
								} 
							},
							'blur': function()
							{
								var totalPersen = (getNumber(this.getValue())/getNumber(Ext.get('txtAnggaranPaguGNRL').getValue())) * 100
									Ext.getCmp('fieldJUMLAH_PERSEN').setValue(Math.round(totalPersen));
									dsDTLTRList_PaguGNRL.data.items[CurrentTR_PaguGNRL.row].data.jumlah_persen = Math.round(totalPersen);

								CalcTotal_PaguGNRL(CurrentTR_PaguGNRL.row);
							},
							'change': function()
							{
								var totalPersen = (getNumber(this.getValue())/getNumber(Ext.get('txtAnggaranPaguGNRL').getValue())) * 100
									Ext.getCmp('fieldJUMLAH_PERSEN').setValue(Math.round(totalPersen));
									dsDTLTRList_PaguGNRL.data.items[CurrentTR_PaguGNRL.row].data.jumlah_persen = Math.round(totalPersen);

								CalcTotal_PaguGNRL(CurrentTR_PaguGNRL.row);
							}
						}
					}
				)
			}
		]
	)
};


function CalcTotal_PaguGNRL(idx)
{
    var total=Ext.num(0);		
	
	if(dsDTLTRList_PaguGNRL.getCount() > 0)
	{
		for(var i = 0;i < dsDTLTRList_PaguGNRL.getCount();i++)
		{
			total = total + parseFloat(dsDTLTRList_PaguGNRL.data.items[i].data.jumlah_rp);
			console.log(parseFloat(dsDTLTRList_PaguGNRL.data.items[i].data.jumlah_rp));
		}
	}
	console.log(total);
	Ext.get('txtTotalPaguGNRL').dom.value = formatCurrency(total);	

	
};

function GetStrCriteriaCetak()
{
	var x='';	
	if (Ext.get('TahunPaguGNRLEntry').dom.value != '' && Ext.get('comboUnitKerjaPaguGNRLEntry').dom.value != '' && selectUnitKerjaPaguGNRLEntry !=undefined)
	{
		x = Ext.get('TahunPaguGNRLEntry').dom.value + "##@@##";
		x +=  selectUnitKerjaPaguGNRLEntry ;		
	};
	return x;
};

function ShowPesanWarningPaguGNRL(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfoPaguGNRL(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanErrorPaguGNRL(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function PaguGNRLSave(mBol,rowdata) 
{
	
	if (ValidasiEntryPaguGNRL('Simpan Data') === 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionPlafondRS/savePlafondRS",
				params: getParamSavePlafondRS(rowdata),
				failure: function(o)
				{
					ShowPesanError('Error menyimpan plafond anggaran !', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// RefreshDataPaguGNRL();
						// ShowPesanInfo('Plafond Rumah Sakit Berhasil Disimpan','Information');
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Plafond Rumah Sakit Berhasil Disimpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(mBol == true){
											PaguGNRLLookUps.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);
						
					}
					else 
					{
						ShowPesanError('Plafond anggaran gagal disimpan!', 'Error');
					};
				}
			}
			
		)
	}
	
};

function PaguGNRLDelete(rowdata) 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran_module/functionPlafondRS/hapusPlafond",
			params: getParamSavePlafondRS(rowdata),
			failure: function(o)
			{
				ShowPesanError('Error hapus plafond anggaran !', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfo('Plafond Rumah Sakit Berhasil Dihapus','Information');
					RefreshDataPaguGNRLFilter(false);
					PaguGNRLAddNew();
					dsDTLTRList_PaguGNRL.removeAll();
				}
				else 
				{
					ShowPesanError('Plafond anggaran gagal dihapus!', 'Error');
				};
			}
		}
		
	)
};

function HapusDetailPaguGNRL(rowdata)
{
	console.log(rowdata);
	if (ValidasiEntryPaguGNRL('Hapus Data') == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus PaguGNRL',
			   // msg: 'Apakah data Pagu ini akan dihapus pada tahun : '+ ' ' + Ext.get('TahunPaguGNRLEntry').dom.value + '? ',
			   msg: 'Apakah data Pagu ini akan dihapus pada tahun : '+ ' ' + rowdata.tahun_anggaran_ta + '? ',
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						PaguGNRLDelete(rowdata);
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
};

function GetAngkaPaguGNRL(str)
{
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};

function ValidasiEntryPaguGNRL(modul)
{
	var x = 1;

	if (Ext.getCmp('txtTotalPaguGNRL').getValue() != Ext.getCmp('txtAnggaranPaguGNRL').getValue())
	{
		ShowPesanErrorPaguGNRL('Total anggaran salah,proses tidak dapat dilanjutkan',modul);
		x=0;
	}
	if (Ext.getCmp('txtTotalPaguGNRL').getValue() == 0)
	{
		ShowPesanErrorPaguGNRL('Total anggaran masih 0',modul);
		x=0;
	}
	
	// if (Ext.get('TahunPaguGNRLEntry').getValue() === '' || Ext.get('comboUnitKerjaPaguGNRLEntry').getValue() === '')
	// {
		// if (Ext.get('TahunPaguGNRLEntry').getValue() == '')
		// {
			// ShowPesanWarningPaguGNRL('Tahun belum di isi',modul);
			// x=0;
		// }
		// else if(Ext.get('comboUnitKerjaPaguGNRLEntry').getValue() == '')
		// {
			// ShowPesanWarningPaguGNRL(gstrSatker+' belum di isi',modul);
			// x=0;
		// }
	// }
	return x;
};
///-----------------------------------------------------------------------------------------///

function RefreshDataPaguGNRL_lama()
{	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondRS/getPlafondGeneral",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data plafond !', 'Error');
		},	
		success: function(o) 
		{   
			dsPaguGNRLList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsPaguGNRLList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsPaguGNRLList.add(recs);
				grListPaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data plafond', 'Error');
			};
		},
		callback:function(){
			var gridListAnggaran=Ext.getCmp('grListPaguGNRL').getStore().data.items;
				for(var i=0,iLen=gridListAnggaran.length; i<iLen;i++){
					if(gridListAnggaran[i].data.app_plafond_general == 1){
						gridListAnggaran[i].data.app_plafond_general_tmp = true;
					} 
				}
			Ext.getCmp('grListPaguGNRL').getView().refresh();
			
		}
	});
	
};

function RefreshDataPaguGNRL(criteria)
{	
	dsPaguGNRLList.load({
		params:{
			Skip: 0,
			Take: selectCountPaguGNRL,
			Sort: 'tahun_anggaran_ta',
			Sortdir: 'ASC',
			target: 'vi_viewdataplafondrs',
			param: criteria
		},
		callback:function(){
			var gridListAnggaran=Ext.getCmp('grListPaguGNRL').getStore().data.items;
				for(var i=0,iLen=gridListAnggaran.length; i<iLen;i++){
					if(gridListAnggaran[i].data.app_plafond_general == 1){
						gridListAnggaran[i].data.app_plafond_general_tmp = true;
					} 
				}
			Ext.getCmp('grListPaguGNRL').getView().refresh();
			
		}
	});
    return dsPaguGNRLList;
	
};

function RefreshDataPaguGNRLFilter(mBol) 
{   
	var strPaguGNRL='';
	
    if (strPaguGNRL != undefined) 
    {  
		RefreshDataPaguGNRL();
    }
	else
	{
		RefreshDataPaguGNRL();
	};
};


function getNumberPaguGNRL(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};



function PaguGNRLInit(rowdata) 
{
	var StrKriteria;
	baru = false;
	if(rowdata.app_plafond_general == 1 ){
		Ext.getCmp('btnSimpanPaguGNRL').setDisabled(true);
		Ext.getCmp('btnSimpanClosePaguGNRL').setDisabled(true);
		Ext.getCmp('btnHapusPaguGNRL').setDisabled(true);
		Ext.getCmp('btnApproveEntryPaguGNRL').setDisabled(true);
	}else{
		Ext.getCmp('btnSimpanPaguGNRL').setDisabled(false);
		Ext.getCmp('btnSimpanClosePaguGNRL').setDisabled(false);
		Ext.getCmp('btnHapusPaguGNRL').setDisabled(false);
		Ext.getCmp('btnApproveEntryPaguGNRL').setDisabled(false);
	}
	
    AddNewPaguGNRL = false;	
	Ext.getCmp('TahunPaguGNRLEntry').setValue(rowdata.tahun_anggaran_ta);	
	Ext.getCmp('txt_app_plafond_general').setValue(rowdata.app_plafond_general);	
	Ext.get('txtAnggaranPaguGNRL').dom.value = formatCurrency(rowdata.jumlah_plafond);
	mTAHUN_ANGGARAN_TA_TMP = rowdata.tahun_anggaran_ta_tmp;	
	RefreshDataDetail_PaguGNRL(rowdata.tahun_anggaran_ta)
};

function PaguGNRLAddNew(rowdata) 
{	
    AddNewPaguGNRL = true;
	baru = true;
    Ext.getCmp('TahunPaguGNRLEntry').reset();
	Ext.get('txtAnggaranPaguGNRL').dom.value = '0';
	Ext.get('txtTotalPaguGNRL').dom.value = '0';
	mTAHUN_ANGGARAN_TA_TMP = "";	
	RefreshDataKomponen_PaguGNRL();
	rowdata = undefined;
	console.log(rowdata);
	Ext.getCmp('btnSimpanPaguGNRL').setDisabled(false);
	Ext.getCmp('btnSimpanClosePaguGNRL').setDisabled(false);
	Ext.getCmp('btnHapusPaguGNRL').setDisabled(false);
	Ext.getCmp('btnApproveEntryPaguGNRL').setDisabled(false);
};

function RefreshDataKomponen_PaguGNRL()
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondRS/getJnsKomponenPlafond",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan komponen plafond!', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_PaguGNRL.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_PaguGNRL.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_PaguGNRL.add(recs);
				gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan komponen plafond', 'Error');
			};
		}
	});
};

function RefreshDataDetail_PaguGNRL(thn_anggaran)
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondRS/getPlafondKomponen",
		params: {
			thn_anggaran:thn_anggaran
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan komponen plafond!', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_PaguGNRL.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_PaguGNRL.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_PaguGNRL.add(recs);
				gridDTLTR_PaguGNRL.getView().refresh();
				CalcTotal_PaguGNRL();
			} else {
				ShowPesanError('Gagal menampilkan komponen plafond', 'Error');
			};
		}
	});
	
};

function GetParamPaguGNRLInputPaguGNRL()
{
	var x='';	
	if (Ext.get('TahunPaguGNRLEntry').dom.value != '' && Ext.get('comboUnitKerjaPaguGNRLEntry').dom.value != '' && selectUnitKerjaPaguGNRLEntry !=undefined)
	{
		x =" where tahun= " + Ext.get('TahunPaguGNRLEntry').dom.value ;
		x += " and kd_unit_kerja=~" + selectUnitKerjaPaguGNRLEntry + "~";		
	};
	return x;
};


function getParamSavePlafondRS(rowdata)
{
	console.log(baru);
	var tmp_thn_anggaran='';
	if(baru == true){
		tmp_thn_anggaran = Ext.getCmp('TahunPaguGNRLEntry').getValue();
	}else{
		if(rowdata == undefined){
			tmp_thn_anggaran = Ext.getCmp('TahunPaguGNRLEntry').getValue();
		}else{
			tmp_thn_anggaran = rowdata.tahun_anggaran_ta;
		}
	}
	
	
	
    var params =
	{
		thn_anggaran			:Ext.getCmp('TahunPaguGNRLEntry').getValue(),
		jml_total_plafond		:getNumberPaguGNRL(Ext.getCmp('txtAnggaranPaguGNRL').getValue()),
		is_approve				:Ext.getCmp('txt_app_plafond_general').getValue(),
		
		//INIT PARAMETER
		param_thn_anggaran		:tmp_thn_anggaran
	};
	
	params['jumlah_list']=dsDTLTRList_PaguGNRL.getCount();
	for(var i = 0 ; i < dsDTLTRList_PaguGNRL.getCount();i++)
	{
		params['kd_komponen-'+i]=dsDTLTRList_PaguGNRL.data.items[i].data.kd_komponen
		params['jumlah_persen-'+i]=dsDTLTRList_PaguGNRL.data.items[i].data.jumlah_persen
		params['jumlah_rp-'+i]=dsDTLTRList_PaguGNRL.data.items[i].data.jumlah_rp
		
	}
	
    return params
}

/*** FUNCTION GENERAL ***/

function ShowPesanWarning(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function ShowPesanError(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfo(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:300
		}
	);
};

function getNumberPaguGNRL(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};

/* form lookup approve*/
var rowSelectedLookApprove;
var vWinFormEntryApprove;
var now = new Date();
var vPesan;
var vCekPeriode;

function FormApprove(jumlah,type,nomor,Keterangan,tgl) {

    vWinFormEntryApprove = new Ext.Window
	(
		{
		    id: 'FormApprove',
		    title: 'Approve',
		    closeAction: 'hide',
		    closable: false,
		    width: 400,
		    height: 200,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'approve',
		    modal: true,
		    items:
			[
				ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl)
			],
		    listeners:
				{
				    activate: function()
				    
				    {  }
				}
		}
	);
    vWinFormEntryApprove.show();
};


function ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl) 
{	
	if (tgl==undefined)
	{		
		tgl=now;
	}
	
	var PnlLapApprove = new Ext.Panel
	(
		{ 
			id: 'PnlLapApprove',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemApprove_Nomor(nomor),
				getItemApprove_Tgl(tgl),
				getItemApprove_Jml(jumlah),
				getItemApprove_Notes(Keterangan),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApprove',
							handler: function() 
							{
								if (ValidasiEntryNomorApp('Approve') == 1 )
								{										
									if (type==='0')
									{
										PenerimaanApprove(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue());
										RefreshDataPenerimaanFilter(false);
									};
									if (type==='2')
									{
										Approve_PenerimaanMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='3')
									{
										Approve_PenerimaanNonMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='4')
									{
										Approve_KasKeluar(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='5')
									{
										Approve_KasKeluarkecil(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='6')
									{
										Approve_kbs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='7')
									{										
										Approve_RekapSP3D(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};		
									if (type==='8')
									{
										Approve_LPJ(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};									
									if (type==='9')
									{
										Approve_PaguGNRL(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};	
									if (type==='10')
									{
										Approve_viKembaliBDATM(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='11')
									{
										// Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue())
										Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='12')
									{
										Approve_OpenApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='13')
									{
										Approve_AdjustArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='14')
									{
										Approve_AdjustApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									vWinFormEntryApprove.close();	
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApprove',
							handler: function() 
							{
								vWinFormEntryApprove.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApprove
};

function getItemApprove_Nomor(nomor)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				readOnly:true,
				border:false,				
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'No.Reff. ',
						name: 'txtNomorApp',
						id: 'txtNomorApp',						
						value:nomor,
						readOnly:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Notes(Keterangan)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Catatan ',
						name: 'txtCatatanApp',
						id: 'txtCatatanApp',
						value:Keterangan,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Tgl(tgl)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',					
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggalApp',
                        name: 'dtpTanggalApp',
                        format: 'd/M/Y',						
						value:tgl,
                        anchor: '50%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Jml(jumlah)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[			
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Jumlah ',
						name: 'txtJumlah',
						id: 'txtJumlah',
						anchor:'95%',
						value:jumlah,
						readOnly:true
					}
				]
			}
		]
	}
	return items;
};

function ValidasiEntryNomorApp(modul)
{
	var x = 1;
	if (Ext.get('txtNomorApp').getValue() == '' )
	{
		if (Ext.get('txtNomorApp').getValue() == '')
		{
			ShowPesanWarningApprove('Nomor belum di isi',modul);
			x=0;
		}		
	}
	return x;
};

function ShowPesanWarningApprove(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};