var CurrentPagu = 
{
    data: Object,
    details:Array, 
    row: 0
};

var mRecordPagu = Ext.data.Record.create
(
	[
	   {name: 'tahun_anggaran_ta', mapping:'tahun_anggaran_ta'},	   
	   {name: 'closed_ta', mapping:'closed_ta'},	   
	   {name: 'kd_unit_kerja', mapping:'kd_unit_kerja'},	   
	   {name: 'nama_unit_kerja', mapping:'nama_unit_kerja'},	   
	   {name: 'unitkerja', mapping:'unitkerja'},	   
	   {name: 'plafond_plaf', mapping:'plafond_plaf'},  
	   {name: 'kd_jns_plafond', mapping:'kd_jns_plafond'}, 
	   {name: 'jns_plafond', mapping:'jns_plafond'} 
	]
);

var dsPaguList;
var dsDTLPaguList;
var dsDTLPaguListTemp;
var cellSelectedPaguDet;
var fldDetail;
var AddNewPagu = false;
var now = new Date();
var selectCountPagu=50;
var PaguLookUps;
var rowSelectedPagu;
var dsUnitKerjaPagu;
var dsUnitKerjaPaguEntry;
var selectJnsPlafond_pagu;
var dsJnsPlafond_pagu;
var grListPagu;
var mod_name_unit_kerja='Plafond Unit Kerja';
//var selectUnitKerjaPagu = gstrListUnitKerja;
var strUnitKerjaPagu = 'Semua';
//var selectUnitKerjaPaguEntry = gstrListUnitKerja;

var dsThAnggPaguFilter;
var dsThAnggPaguEntry;

var mKD_UNIT_KERJA_TMP;
var mTAHUN_ANGGARAN_TA_TMP;
var mKD_JNS_PLAFOND_TMP;

CurrentPage.page=getPanelPagu(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelPagu(mod_id)
{
    var Field =
	[
		'tahun_anggaran_ta',
		'closed_ta',
		'kd_unit_kerja',
		'nama_unit',
		'unitkerja',
		'plafond_plaf',
		'tahun_anggaran_ta_tmp',
		'kd_unit_kerja_tmp',
		'kd_jns_plafond',
		'kd_jns_plafond_tmp',
		'jns_plafond',
		'app_plafond_general_tmp',
		'app_plafond_general'
	];
    dsPaguList = new WebApp.DataStore({ fields: Field });
	
	var chkApprovedPaguList = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprovedPaguList',
			header: 'App',
			align: 'center',			
			dataIndex: 'app_plafond_general_tmp',
			disabled: true,
			width: 20,
			editable:false ,
			// renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				// console.log(value);
				// var cssPrefix = Ext.baseCSSPrefix,
					// cls = cssPrefix + 'grid-checkcolumn';


				// if (this.disabled || value == 'false') {
					// metaData.tdCls += ' ' + this.disabledCls;
				// }
				// if (value) {
					// cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';
				// }

				// return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
			// },
		}
	); 
	
    grListPagu = new Ext.grid.EditorGridPanel
	(
		{
			id:'grListPagu',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsPaguList,
			columnLines:true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedPagu = dsPaguList.getAt(row);
						}
					}
				}
			),
			colModel: new Ext.grid.ColumnModel
			//cm: new Ext.grid.ColumnModel
			(	
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colPeriode',
						header: "Periode",
						dataIndex: 'tahun_anggaran_ta',
						sortable: true,
						width: 35,
						hidden:true,
						filter: {}
					}, 
					{
						id: 'colPeriodeTmp',
						header: "Periode",
						dataIndex: 'tahun_anggaran_ta',
						sortable: true,
						width: 35,
						filter: {}
					}, 
					{
						id: 'colUnitKerjaPagu',
						// header: gstrSatker,
						header: 'Unit Kerja',
						dataIndex: 'nama_unit',
						sortable: true,
						width: 210,
						filter: {}
					}, 
					{
						id: 'colPagu',
						header: "Plafond (Rp)",
						dataIndex: 'plafond_plaf',
						align: 'right',
						sortable: true,
						width: 60,
						renderer: function(v, params, record) 
						{
							if(v !== undefined)
							{
								return formatCurrency(record.data.plafond_plaf);
							}
							else
							{
								return 0;
							}
						},
						filter: {}
					},
					{
						id: 'colJnsPlatfonPagu',
						header: "Jenis Plafond",
						dataIndex: 'jns_plafond',
						sortable: true,
						width: 100,
						filter: {}
					},
					chkApprovedPaguList,
				]
			),
			tbar: 
			[
				{
					id:'btnEditDataPagu',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{
						if (rowSelectedPagu != undefined) 
						{
				            PaguLookUp(rowSelectedPagu.data);
				        }
				        else 
						{
				            PaguLookUp();
				        }
					}
				},' ','-'
			],
			 listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedPagu = dsPaguList.getAt(ridx);
					if (rowSelectedPagu != undefined) 
					{
						PaguLookUp(rowSelectedPagu.data);
					}
					else 
					{
						PaguLookUp();
					}
				}
				// End Function # --------------
			},
			bbar : bbar_paging(mod_name_unit_kerja, selectCountPagu, dsPaguList),
            viewConfig: 
			{
                forceFit: true
            },
			/* bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsPaguList,
					pageSize: selectCountPagu,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			) */
        }
	);
	
	
    var FormPagu = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: 'Plafond Unit Kerja',
			margins:'0 5 5 0',
			bodyStyle: 'padding:15px',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'Pagu',
			items: [grListPagu],
			tbar: 
			[
				//'Unit/Sub '+gstrSatker+' : ', ' ',mComboUnitKerjaPagu(),
				'Unit Kerja '+' : ', ' ',mComboUnitKerjaPagu(),
				' ','-','Periode : ', ' ',mComboTahunPaguFilter(),
				' ','->',
				{
					xtype: 'button',
					id:'btnRefreshPagu',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(sm, row, rec) 
					{
						RefreshDataPaguFilter();
					}
				}
			]
		}
	);

	// RefreshDataPaguFilter();
	RefreshDataPaguFilter();
    return FormPagu ;

};

function mComboUnitKerjaPagu() 
{
    var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
    dsUnitKerjaPagu = new WebApp.DataStore({ fields: Field });

    var comboUnitKerjaPagu = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaPagu',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+gstrSatker+'...',
		    emptyText: 'Pilih Unit Kerja...',
		    // fieldLabel: gstrSatker,
		    fieldLabel: 'gstrSatker',
		    align: 'right',
		    width: 180,
		    store: dsUnitKerjaPagu,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaPagu = b.data.kd_unit;
					RefreshDataPaguFilter();
			    }
			}
		}
	);


	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerjaPagu.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerjaPagu.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerjaPagu.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
    return comboUnitKerjaPagu;
};




function mComboTahunPaguFilter()
{
	var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThAnggPaguFilter = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThAnggPaguFilter.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThAnggPaguFilter.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThAnggPaguFilter.add(recs);
			} 
		}
	});
	var currYear = parseInt(now.format('Y'));
	var TahunPaguFilter = new Ext.form.ComboBox
	(
		{
			id:'TahunPaguFilter',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',
			value: '' ,
			width:100,
			store: dsThAnggPaguFilter,
			/* store: new Ext.data.ArrayStore
						(
							{
								id: 0,
								fields: 
								[
									'Id',
									'displayText'
								],
								data: 
								[						
									[currYear + 1,currYear + 1 ], 
									[currYear,currYear ]
								]
							}
						), */
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			value: Ext.num(now.format('Y')),
			listeners:
			{
			    'select': function(a, b, c) 
				{
					RefreshDataPaguFilter();
			    }
			}
		}
	);

	return TahunPaguFilter;
};


function mComboTahunPaguEntry() 
{
	//var currYear = gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1)//parseInt(now.format('Y'));
	var currYear = parseInt(now.format('Y'));	
	
	var TahunPaguEntry = new Ext.form.ComboBox
	(
		{
			id:'TahunPaguEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 2,currYear + 2 ], 
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value: gstrTahunAngg+'/'+(Ext.num(gstrTahunAngg)+1),
			value: Ext.num(now.format('Y'))
		}
	);
	
	return TahunPaguEntry;
};

function mComboUnitKerjaPAGUEntry()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerjaPaguEntry = new WebApp.DataStore({ fields: Field });

	LoadDataUnitKerjaFilterPaguEntry();
   var comboUnitKerjaPaguEntry = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerjaPaguEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			// emptyText:'Pilih '+gstrSatker+'...',
			emptyText:'Pilih...',
			// fieldLabel: gstrSatker+' ',			
			fieldLabel: 'Unit Kerja',			
			align:'Right',
			width: 250,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			store:dsUnitKerjaPaguEntry,
			listeners:  
			{
				'select': function(a,b,c)
				{
				    selectUnitKerjaPaguEntry = b.data.kd_unit;
				} 
			}
		}
	);
	
	return comboUnitKerjaPaguEntry;
};

function LoadDataUnitKerjaFilterPaguEntry()
{
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerjaPaguEntry.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerjaPaguEntry.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerjaPaguEntry.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
	
};

function mComboJnsPlafond_pagu()
{
	var Field = ['kd_komponen', 'komponen'];
	dsJnsPlafond_pagu = new WebApp.DataStore({ fields: Field });

	LoadDataJnsPlafond_pagu();
	var comboJnsPlafond_pagu = new Ext.form.ComboBox
	(
		{
			id:'comboJnsPlafond_pagu',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Plafond...',
			fieldLabel: 'Jenis Plafond ',			
			align:'Right',
			width: 200,
			valueField: 'kd_komponen',
			displayField: 'komponen',
			store:dsJnsPlafond_pagu,
			listeners:  
			{
				'select': function(a,b,c)
				{
				    selectJnsPlafond_pagu = b.data.kd_komponen;
				} 
			}
		}
	);
	
	return comboJnsPlafond_pagu;
};

function LoadDataJnsPlafond_pagu()
{
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getJnsPlafond",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data jenis plafond !', 'Error');
		},	
		success: function(o) 
		{   
			dsJnsPlafond_pagu.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsJnsPlafond_pagu.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsJnsPlafond_pagu.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data jenis plafond', 'Error');
			};
		}
	});

};

function PaguLookUp(rowdata) 
{
	var mWidth=580;
    PaguLookUps = new Ext.Window
	(
		{  
			id: 'PaguLookUps',
			title: 'Plafond Unit Kerja',
			closeAction: 'destroy',
			width: mWidth,
			height: 250,
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Pagu',
			modal: true,
			anchor: '100%',
			items: getFormEntryPagu(mWidth,rowdata),
			listeners: 
			{
				activate: function() 
				{
				},
				afterShow: function() 
				{ 
				    this.activate();
				},
				deactivate:function()
				{
					rowSelectedPagu=undefined;
					grListPagu.getSelectionModel().clearSelections();
					//RefreshDataPaguFilter(true);
				},
				close: function (){
					
				},
			}
		}
	);
 
	PaguLookUps.show();
	 if (rowdata == undefined) 
	{
        PaguAddNew();
    }
    else 
	{
        PaguInit(rowdata);
		console.log(rowdata);
    }
};



function getFormEntryPagu(mWidth,rowdata) 
{

    var pnlPagu = new Ext.FormPanel    
	(
		{    
			id: 'PanelPagu',
			fileUpload: true,
			region: 'center',
			layout: 'column',
			height: 180,
			anchor: '100%',
			bodyStyle: 'padding:10px 10px 10px 10px',
			border: false,
			items: 
			[
				{
					layout: 'form',
					width:mWidth-34,
					anchor: '100%',
					bodyStyle: 'padding:10px 10px 10px 10px',
					autoHeight: true,
					labelAlign: 'right',
					labelWidth:180,
					items: 
					[	
						mComboTahunPaguEntry(),
						mComboUnitKerjaPAGUEntry(),
						{
							xtype: 'textfield',
							fieldLabel: 'Plafond (Rp.)',
							labelWidth: 180,
							style: 
							{
								'font-weight': 'bold',
								'text-align':'right'
							},
							id: 'txtPlafon',
							enableKeyEvents: true,
							listeners:
							{
								'keyup': function()
								{
									Ext.getCmp('txtTotalPagu').setValue(formatCurrency(this.getValue()));
								},
								'blur': function()
								{
									this.setValue(formatCurrency(this.getValue()));
								},
								'focus': function()
								{
									this.setValue(getNumber(this.getValue()));
								}
							}
						},
						mComboJnsPlafond_pagu()
					]
				}
					
			],
			tbar:
			[
				{
					id:'btnTambahPagu',
					text: 'Tambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{
					     PaguAddNew();	 
					}
				},
				'-', 
				{
					id:'btnSimpanPagu',
					text: 'Simpan',
					tooltip: 'Simpan Data ',
					iconCls: 'save',
					handler: function() 
					{ 
						PaguSave(false,rowdata); 
					}
				},'-',
				{
					id:'btnSimpanClosePagu',
					text: 'Simpan & Keluar',
					tooltip: 'Simpan Data dan Keluar',
					iconCls: 'saveexit',
					handler: function() 
					{ 
						// Ext.getCmp('btnSimpanPagu').setDisabled(true);
						// Ext.getCmp('btnSimpanClosePagu').setDisabled(true);														
						// var x = PaguSave(true);
						PaguSave(true,rowdata);
						//RefreshDataPaguFilter(false);
						// if (x===undefined)
						// {
							// PaguLookUps.close();
						// };
					}
				},
				'-', 
				{
					id:'btnHapusPagu',
					text: 'Hapus',
					tooltip: 'Hapus',
					iconCls: 'remove',
					handler: function() 
					{ 
						HapusDetailPagu(rowdata);						
					}
				}, 
				'-', 
				
			]
		}
	);
	
	var TotalPagu = new Ext.Panel
	(
		{
			id:'TotalPagu',
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '0px',
				'margin-left': '325px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					fieldLabel: 'Total ',
					style: 
					{
						'font-weight': 'bold',
						'text-align':'right'
					},
					id:'txtTotalPagu',
					name:'txtTotalPagu',
					readOnly:true,					
					width: 160,
					listeners:
					{
						'change': function()
						{
							this.setValue(formatCurrency(this.getValue()));
						}
					}
				}
			]
		}
	);
	
			
	var FormPagu = new Ext.Panel   
	(
		{
			id: 'FormPagu',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			items: [pnlPagu,TotalPagu]
        }
	);
                
     return FormPagu
};

function GetStrCriteriaCetak()
{
	var x='';	
	if (Ext.get('TahunPaguEntry').dom.value != '' && Ext.get('comboUnitKerjaPaguEntry').dom.value != '' && selectUnitKerjaPaguEntry !=undefined)
	{
	
	x = selectUnitKerjaPaguEntry +" "+ "##";
	x+= Ext.get('TahunPaguEntry').dom.value +" "+ "##";
	x+= selectJnsPlafond_pagu + "##";

	};
	// x  += " "+ "##"
	x += 1 + "##"
	return x;
};

function ShowPesanWarningPagu(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfoPagu(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanErrorPagu(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function PaguSave(mBol,rowdata) 
{
	if (ValidasiEntryPagu('Simpan Data') === 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/savePlafondUnitKerja",
				params:getParamInputPagu(rowdata),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// ShowPesanInfoPagu('Data berhasil di simpan','Simpan Data');
						mKD_UNIT_KERJA_TMP = selectUnitKerjaPaguEntry;
						mTAHUN_ANGGARAN_TA_TMP = Ext.get('TahunPaguEntry').getValue();
						mKD_JNS_PLAFOND_TMP= selectJnsPlafond_pagu;
						RefreshDataPaguFilter();
						Ext.getCmp('btnSimpanPagu').setDisabled(true);
						Ext.getCmp('btnSimpanClosePagu').setDisabled(true);
						
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Data berhasil di simpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(mBol == true){
											PaguLookUps.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);
					}
					else if (cst.success === false )
					{
						ShowPesanWarningPagu('Data tidak berhasil di simpan, '+ cst.pesan,'Simpan Data');
					}
					else 
					{
						ShowPesanErrorPagu('Data tidak berhasil di simpan','Simpan Data');
					}
				}
			}
		)
	}
	else
	{
		Ext.getCmp('btnSimpanPagu').setDisabled(false);
		Ext.getCmp('btnSimpanClosePagu').setDisabled(false);														
		if(mBol === true)
		{
			return false;
		};
	};
};

function PaguDelete(rowdata) 
{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/hapusPlafondUnitKerja",
				// params:  getParamInputPaguDelete(), 
				params:  getParamInputPagu(rowdata), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoPagu('Data berhasil di hapus','Hapus Data');
						RefreshDataPaguFilter();
						PaguAddNew();
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarningPagu('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else if (cst.success === false && cst.pesan === 999 )
					{
						ShowPesanWarningPagu('Data tidak berhasil di hapus, data tersebut telah dipakai','Hapus Data');
					}
					else 
					{
						ShowPesanErrorPagu('Data tidak berhasil di hapus, Error : ' + cst.pesan,'Hapus Data');
					}
				}
			}
		)
};

function HapusDetailPagu(rowdata)
{
	console.log(rowdata);
	if (ValidasiEntryPagu('Hapus Data') == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Pagu',
			   msg: 'Apakah data pagu ini akan dihapus ?' + ' ' + 'Pagu pada Tahun : '+ ' ' + rowdata.tahun_anggaran_ta + ', '+' Unit Kerja'+' : '+ ' ' + rowdata.nama_unit,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						PaguDelete(rowdata);
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
};

function GetAngkaPagu(str)
{
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};

function ValidasiEntryPagu(modul)
{
	var x = 1;
	// else if (Ext.get('TahunPaguEntry').getValue() === '' || Ext.get('comboUnitKerjaPaguEntry').getValue() === '' )
	// {
	if (Ext.get('TahunPaguEntry').getValue() == '' )
	{
		ShowPesanWarningPagu('Tahun belum di isi',modul);
		x=0;
	}
	// else if(selectUnitKerjaPaguEntry == undefined)
	// {
		// ShowPesanWarningPagu(gstrSatker+' belum di isi',modul);
		// x=0;
	// }
	else if(selectJnsPlafond_pagu == undefined)
	{
		ShowPesanWarningPagu('Jenis Plafond belum di isi',modul);
		x=0;
	}
	// }
	return x;
};
//-----------------------------------------------------------------------------------------///


function RefreshDataPaguFilter()
{	
	var criteria = GetCriteriaGridUtama();
	dsPaguList.removeAll();
	dsPaguList.load({
		params:{
			Skip: 0,
			Take: selectCountPagu,
			Sort: '',
			Sortdir: 'ASC',
			target: 'vi_viewdataplafond_unitkerja',
			param: criteria
		},
		callback:function(){
			var gridListAnggaranUnitKerja=Ext.getCmp('grListPagu').getStore().data.items;
				for(var i=0,iLen=gridListAnggaranUnitKerja.length; i<iLen;i++){
					if(gridListAnggaranUnitKerja[i].data.app_plafond_general == 1){
						gridListAnggaranUnitKerja[i].data.app_plafond_general_tmp = true;
						// Ext.getCmp('chkApprovedPaguList').setDisabled(true);
					} 
				}
			Ext.getCmp('grListPagu').getView().refresh();
		}
	});
    return dsPaguList;
	
};

function GetCriteriaGridUtama(){
	var criteria = '';
	if (Ext.getCmp('comboUnitKerjaPagu').getValue() == '000' || Ext.getCmp('comboUnitKerjaPagu').getValue() == 000){
		criteria = "SEMUA~"+Ext.getCmp('TahunPaguFilter').getValue();
	}else{
		criteria = "  WHERE a.kd_unit_kerja= '" + Ext.getCmp('comboUnitKerjaPagu').getValue() +  "' and a.tahun_anggaran_ta= '" + Ext.getCmp('TahunPaguFilter').getValue() + " '";
	}
	
	return criteria;
}

function RefreshDataPaguFilter_lama() 
{   
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getPlafond",
		params: {
			// filter:true,
			kd_unit				:Ext.getCmp('comboUnitKerjaPagu').getValue(),
			thn_anggaran		:Ext.getCmp('TahunPaguFilter').getValue()
		},
		failure: function(o)
		{
			ShowPesanErrorPagu('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsPaguList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsPaguList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsPaguList.add(recs);
				console.log(dsPaguList);
				grListPagu.getView().refresh();
			} else {
				ShowPesanErrorPagu('Gagal menampilkan data plafond unit kerja', 'Error');
			};
		},
		callback:function(){
			var gridListAnggaranUnitKerja=Ext.getCmp('grListPagu').getStore().data.items;
				for(var i=0,iLen=gridListAnggaranUnitKerja.length; i<iLen;i++){
					if(gridListAnggaranUnitKerja[i].data.app_plafond_general == 1){
						gridListAnggaranUnitKerja[i].data.app_plafond_general_tmp = true;
						// Ext.getCmp('chkApprovedPaguList').setDisabled(true);
					} 
				}
			Ext.getCmp('grListPagu').getView().refresh();
		}
	});
};


function getNumberPagu(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};

function getParamInputPagu(rowdata) 
{
	var tmp_thn_anggaran 	= '';
	var tmp_kd_unit_kerja 	= '';
	var tmp_kd_jns_plafond 	= '';
	
	if(rowdata == undefined){
		tmp_thn_anggaran 	= Ext.getCmp('TahunPaguEntry').getValue();
		tmp_kd_unit_kerja 	= selectUnitKerjaPaguEntry;
		tmp_kd_jns_plafond 	= selectJnsPlafond_pagu;
	}else{
		tmp_thn_anggaran 	= rowdata.tahun_anggaran_ta;
		tmp_kd_unit_kerja 	= rowdata.kd_unit_kerja;
		tmp_kd_jns_plafond 	= rowdata.kd_jns_plafond;
	}
	
	var params =
	{
		thn_anggaran			:Ext.getCmp('TahunPaguEntry').getValue(),
		plaf					:getNumberPaguGNRL(Ext.getCmp('txtPlafon').getValue()),
		kd_unit_kerja			:selectUnitKerjaPaguEntry,
		kd_jns_plafond			:selectJnsPlafond_pagu,
		
		param_thn_anggaran 		: tmp_thn_anggaran,
		param_kd_unit_kerja 	: tmp_kd_unit_kerja,
		param_kd_jns_plafond 	: tmp_kd_jns_plafond
	};

   
return params; 
};

function getNumberPaguGNRL(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};

function getParamInputPaguDelete() 
{
    var params = 
	{
		thn_anggaran:mTAHUN_ANGGARAN_TA_TMP,
		kd_unit:selectUnitKerjaPaguEntry,
		kd_jns_plafond:selectJnsPlafond_pagu
    };
    return params;
};

function PaguInit(rowdata) 
{
	console.log(rowdata);
	console.log(rowdata.tahun_anggaran_ta);
    AddNewPagu = false;
	// Ext.getCmp('TahunPaguEntry').setValue(rowdata.TAHUN_ANGGARAN_TA);
	Ext.getCmp('TahunPaguEntry').setValue(rowdata.tahun_anggaran_ta);
	Ext.getCmp('comboUnitKerjaPaguEntry').setValue(rowdata.nama_unit);
	Ext.getCmp('comboJnsPlafond_pagu').setValue(rowdata.jns_plafond);
	selectJnsPlafond_pagu = rowdata.kd_jns_plafond;
	selectUnitKerjaPaguEntry = rowdata.kd_unit_kerja ;
	console.log(selectUnitKerjaPaguEntry);
	Ext.getCmp('txtTotalPagu').setValue(formatCurrency(rowdata.plafond_plaf));
	Ext.getCmp('txtPlafon').setValue(formatCurrency(rowdata.plafond_plaf));
	
	mTAHUN_ANGGARAN_TA_TMP = rowdata.tahun_anggaran_ta;
	mKD_UNIT_KERJA_TMP = rowdata.kd_unit_kerja;
	mKD_JNS_PLAFOND_TMP= rowdata.kd_jns_plafond;
	
	if (rowdata.app_plafond_general == 1){
		Ext.getCmp('btnSimpanPagu').setDisabled(true);
		Ext.getCmp('btnSimpanClosePagu').setDisabled(true);
		Ext.getCmp('btnHapusPagu').setDisabled(true);
	}else{
		Ext.getCmp('btnSimpanPagu').setDisabled(false);
		Ext.getCmp('btnSimpanClosePagu').setDisabled(false);
		Ext.getCmp('btnHapusPagu').setDisabled(false);
	}
};

function PaguAddNew() 
{	
    AddNewPagu = true;
    Ext.getCmp('TahunPaguEntry').reset();
    Ext.getCmp('comboUnitKerjaPaguEntry').reset();
    Ext.getCmp('comboJnsPlafond_pagu').reset();
    Ext.getCmp('txtTotalPagu').setValue(0); 
    Ext.getCmp('txtPlafon').setValue(0); 
	mTAHUN_ANGGARAN_TA_TMP = "";
	mKD_UNIT_KERJA_TMP = "";		
	mKD_JNS_PLAFOND_TMP="";
	selectUnitKerjaPaguEntry=undefined;	
	selectJnsPlafond_pagu=undefined;

	Ext.getCmp('btnSimpanPagu').setDisabled(false);
	Ext.getCmp('btnSimpanClosePagu').setDisabled(false);
	Ext.getCmp('btnHapusPagu').setDisabled(false);	
};

function GetParamPaguInputPagu()
{
	var x='';	
	if (Ext.get('TahunPaguEntry').dom.value != '' && Ext.get('comboUnitKerjaPaguEntry').dom.value != '' && selectUnitKerjaPaguEntry !=undefined)
	{
		x =" where tahun= " + Ext.get('TahunPaguEntry').dom.value ;
		x += " and kd_unit_kerja=~" + selectUnitKerjaPaguEntry + "~";		
	};
	return x;
};

function CalcTotalPaguDetail(idx)
{
	Ext.getCmp('txtTotalPagu').setValue(formatCurrency());	
};



