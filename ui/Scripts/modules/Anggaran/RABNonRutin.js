var CurrentGLRKAPengembangan = 
{
	data: Object,
    details: Array,
    row: 0
};

var CurrentGLDaftarRKAPengembangan = 
{
	data: Object,
    details: Array,
    row: 0
};

var CurrentGLDetailRKAPengembangan = 
{
	data: Object,
    details: Array,
    row: 0
};

var mRecordGLDetailRKAPengembangan = Ext.data.Record.create
(
	[
		{ name: 'LINE', mapping: 'LINE' },
		{ name: 'TAHUN_ANGGARAN_TA', mapping: 'TAHUN_ANGGARAN_TA' },
		{ name: 'KD_UNIT_KERJA', mapping: 'KD_UNIT_KERJA' },
		{ name: 'NO_PROGRAM_PROG', mapping: 'NO_PROGRAM_PROG' },
		{ name: 'PRIORITAS', mapping: 'PRIORITAS' },
		{ name: 'KD_JNS_RKAT_JRKA', mapping: 'KD_JNS_RKAT_JRKA' },
		{ name: 'Account', mapping: 'Account' },
		{ name: 'KD_SATUAN_SAT', mapping: 'KD_SATUAN_SAT' },
		{ name: 'SATUAN_SAT', mapping: 'SATUAN_SAT' },
		{ name: 'Name', mapping: 'Name' },
		{ name: 'QTY', mapping: 'QTY' },
		{ name: 'BIAYA_SATUAN', mapping: 'BIAYA_SATUAN' },
		{ name: 'JUMLAH', mapping: 'JUMLAH' },
		{ name: 'M1', mapping: 'M1' },
		{ name: 'M2', mapping: 'M2' },
		{ name: 'M3', mapping: 'M3' },
		{ name: 'M4', mapping: 'M4' },
		{ name: 'M5', mapping: 'M5' },
		{ name: 'M6', mapping: 'M6' },
		{ name: 'M7', mapping: 'M7' },
		{ name: 'M8', mapping: 'M8' },
		{ name: 'M9', mapping: 'M9' },
		{ name: 'M10', mapping: 'M10' },
		{ name: 'M11', mapping: 'M11' },
		{ name: 'M12', mapping: 'M12' },
		{ name: 'DESKRIPSI_RKATKDET', mapping: 'DESKRIPSI_RKATKDET' }
	]
);

var CurrentGLDetailPusatRKAPengembangan = 
{
	data: Object,
    details: Array,
    row: 0
};

var dsTRRKAPengembanganList;
var rowSelectedRKAPengembangan;
var dsCboUnitKerjaRKAPengembanganFilter;
// var selectUnitKerjaRKAPengembanganFilter = gstrListUnitKerja;
var selectUnitKerjaRKAPengembanganFilter = '';
var now = new Date();
var winFormEntryRKAPengembangan;
var dsCboUnitKerjaTabPL1RKAPengembangan;
// var selectUnitKerjaTabPL1RKAPengembangan = gstrListUnitKerja;
var selectUnitKerjaTabPL1RKAPengembangan = '';
var dsCboUnitKerjaTabPN1RKAPengembangan;
// var selectUnitKerjaTabPN1RKAPengembangan = gstrListUnitKerja;
var selectUnitKerjaTabPN1RKAPengembangan = '';
var dsGLDaftarRKAPengembanganList;
var rowSelectedDaftarRKAPengembangan;
var dsGLDetailRKAPengembanganList;
var rowSelectedDetailRKAPengembangan;
var dsGLPusatRKAPengembanganList;
var selectCountRKAPengembangan = 50;
var dsThAnggPaguRKATPL1;
var dsThAnggPaguRKATPN1;
var dsThAnggRKAT1Filter;
var vAddNewRKAPengembangan;
var dsGLDetailPusatRKAPengembanganList;
var strProgAsal;
var intPriorAsal;
var disahkan;
var dblBeforeEditTotDetailRKAT1 = 0;
var grListTRRKAPengembangan;
var tabActive;
var tabDetActive;
var gridListDaftarRKAPengembangan;
var jumlah;
var danaRKAPengembangan;
var gridListDetailRKAPengembangan ;

// var namaform_RKATPengembangan=gstrrkat+' Non Rutin'   
var namaform_RKATPengembangan='RAB Non Rutin'   
CurrentPage.page = getPanelRKAPengembangan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelRKAPengembangan(mod_id) 
{
    var Fields = 
	[
		'kd_unit_kerja',
		'unitkerja','nama_unit',
		'tahun_anggaran_ta',
		'plafondawal','sisaplaf','jumlah_rkatk','jumlah_rkatk','disahkan_rka','tahun_anggaran_ta_tmp'
	];
    dsTRRKAPengembanganList = new WebApp.DataStore({ fields: Fields });	
	
	var chkApprovedRKAPengembanganList = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprovedRKAPengembanganList',
			header: 'Approve',
			align: 'center',			
			dataIndex: 'disahkan_rka',
			disabled: true,
			width: 15
		}
	);
	
	grListTRRKAPengembangan = new Ext.grid.EditorGridPanel
	(
		{
			id:'grListTRRKAPengembangan',
			stripeRows: false,
			columnLines: true,
			border: false,
			store: dsTRRKAPengembanganList,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedRKAPengembangan = dsTRRKAPengembanganList.getAt(row);
							CurrentGLRKAPengembangan.row = row;
							CurrentGLRKAPengembangan.data = rowSelectedRKAPengembangan.data;
						}
					}
				}
			),
			viewConfig: 
			{ 
				stripeRows: true,
				forceFit: true
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colThnAnggGLRKAPengembangan',
						header: 'Tahun Anggaran',
						dataIndex: 'tahun_anggaran_ta',
						sortable: true,
						width: 20,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}
					},
					{
						id: 'colUnitKerjaGLRKAPengembangan',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit',
						sortable: true,						
						width : 100,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}
					},
					chkApprovedRKAPengembanganList				
				]
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedRKAPengembangan = dsTRRKAPengembanganList.getAt(ridx);
					if (rowSelectedRKAPengembangan != undefined) 
					{
						showFormEntryRKAPengembangan(rowSelectedRKAPengembangan.data);
					}
					else 
					{
						showFormEntryRKAPengembangan();
					}	
				}
				// End Function # --------------
			},
			tbar: 
			[
				{
					id: 'btnEditRKAPengembangan',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{
						if (rowSelectedRKAPengembangan != undefined) 
						{
				            showFormEntryRKAPengembangan(rowSelectedRKAPengembangan.data);
				        }
				        else 
						{
							showFormEntryRKAPengembangan();
				        }						
					}
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRRKAPengembanganList,
					pageSize: selectCountRKAPengembangan,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			)
		}
	);

	var FormTRRKAPengembangan = new Ext.Panel
	(
		{
			id: mod_id,
			closable: true,
			region: 'center',
			layout: 'form',
			title: namaform_RKATPengembangan,
			border: false,           
			shadhow: true,
			iconCls: 'RKAT',
			margins: '0 5 5 0',
			items: [grListTRRKAPengembangan],
			tbar: 
			[
				'Unit Kerja'+': ', ' ',mCboUnitKerjaRKAPengembanganFilter(),
				' ','-','Tahun Anggaran : ', ' ',mComboTahunRKAPengembanganFilter(),
				' ','-',
				{
					xtype: 'checkbox',
					id: 'chkFilterRKAPengembanganFilter',
					boxLabel: 'Approved',
					listeners: 
					{
						check: function()
						{
						   RefreshDataGLRKAPengembangan();
						}
					}
				},
				'->',
				{
					id:'btnRefreshRKAPengembangan',
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(sm, row, rec) 
					{
						RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
					}
				}
			],
			listeners:
			{
				'afterrender': function()
				{					
					RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
				}
			}
		}
	);
   	
	return FormTRRKAPengembangan;
};

function mCboUnitKerjaRKAPengembanganFilter() 
{
     var Fields = ['kd_unit', 'nama_unit'];
    dsCboUnitKerjaRKAPengembanganFilter = new WebApp.DataStore({ fields: Fields });
    
    var cboUnitKerjaRKAPengembanganFilter = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaRKAPengembanganFilter',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja' ,
		    align: 'right',
		    width: 200,
		    store: dsCboUnitKerjaRKAPengembanganFilter,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaRKAPengembanganFilter = b.data.kd_unit;
					RefreshDataGLRKAPengembangan();
			    }
			}
		}
	);

	/* dsCboUnitKerjaRKAPengembanganFilter.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    // param: "kdunit=" + gstrListUnitKerja
			    param: ''
			}
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaRKAPengembanganFilter.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaRKAPengembanganFilter.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaRKAPengembanganFilter.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
    return cboUnitKerjaRKAPengembanganFilter;
};

function mComboTahunRKAPengembanganFilter()
{
	var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThAnggRKAT1Filter = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThAnggRKAT1Filter.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThAnggRKAT1Filter.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThAnggRKAT1Filter.add(recs);
			} 
		}
	});
	
	var currYear = parseInt(now.format('Y'));
	var cboTahunRKAPengembanganFilter = new Ext.form.ComboBox
	(
		{
			id:'cboTahunRKAPengembanganFilter',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:100,
			store: dsThAnggRKAT1Filter,
			/* store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear]
					]
				}
			), */
			// valueField: 'TAHUN_ANGGARAN_TA',
			// value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1),
			// value:'',
			// displayField: 'TMP_TAHUN'
			// valueField: 'Id',
			// displayField: 'displayText'	,
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			value: Ext.num(now.format('Y')),
			listeners:
			{
			    'select': function(a, b, c) 
				{
					RefreshDataGLRKAPengembangan();
			    }
			}
		}
	);	
	
	return cboTahunRKAPengembanganFilter;
};

function showFormEntryRKAPengembangan(rowdata)
{
	var lebar = 800;	
	winFormEntryRKAPengembangan = new Ext.Window
	(
		{
			id: 'winFormEntryRKAPengembangan',
			title: namaform_RKATPengembangan,
			layout: 'border',
			modal: true,
			resizable: false,
			width: lebar,
			height: 600,
			iconCls: 'SetupKUPPA',
			items: [getItemsFormEntryRKAPengembangan(lebar,rowdata)],
			listeners:
			{
				'deactivate': function()
				{
					rowSelectedRKAPengembangan = undefined;
					rowSelectedDaftarRKAPengembangan = undefined;
					rowSelectedDetailRKAPengembangan = undefined;
					vAddNewRKAPengembangan = undefined;
					
					RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
				}
			}
		}
	);	
	winFormEntryRKAPengembangan.show();
	
	if(rowdata !== undefined)
	{
		disahkan = false;
		initFormEntryRKAPengembangan(rowdata);		
	}
	else
	{
		AddNewRKAPengembangan(tabActive);
	}
}

function getItemsFormEntryRKAPengembangan(lebar,rowdata)
{
	var pnlContainerRKAPengembangan = new Ext.Panel
	(
		{
			id: 'pnlContainerRKAPengembangan',
			region: 'center',
			layout: 'border',
			border: false,
			width: lebar,
			items:
			[
				getTabPanelFormEntryHeaderRKAPengembangan(rowdata),
				getTabPanelFormEntryDetailRKAPengembangan()
			]
		}
	);
	
	return pnlContainerRKAPengembangan;
}

function getTabPanelFormEntryHeaderRKAPengembangan(rowdata)
{
	var tabPL1FormEntryHeaderRKAPengembangan = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabPL1FormEntryHeaderRKAPengembangan',
            title: 'Pengeluaran (PL-1)',
            items: 
			[
                {
					xtype: 'fieldset',
					style: 'margin: 5px',
					items: 
					[						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Unit Kerja',
							items: 
							[										
								mCboUnitKerjaTabPL1RKAPengembangan(rowdata),
								{
									xtype: 'displayfield',
									value: 'Tahun Anggaran: ',
									width: 100
								},
								mComboTahunEntryPL1RKAPengembangan(),
								{
									xtype: 'displayfield',
									value: 'Revisi: ',
									width: 30
								},
								{
									xtype: 'checkbox',
									id: 'txtCheckPL1RKAPengembangan',
									readOnly:true
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Program',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtNoProgAliasPL1RKAPengembangan',
									width: 80,
									readOnly: true,
									style: 'text-align: right'
								},
								{
									xtype: 'textfield',
									id: 'txtNoProgramPL1RKAPengembangan',
									width: 80,
									// readOnly: true,
									// visible: false,
									hidden: true,
									style: 'text-align: right'
								},
								{
									xtype: 'textfield',									
									id: 'txtNmProgramPL1RKAPengembangan',
									enableKeyEvents: true,
									listeners:
									{										
										'specialkey': function()
										{
											if(Ext.EventObject.getKey() === 13)
											{
												if(selectUnitKerjaTabPL1RKAPengembangan !== undefined && Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() !== "")
												{
													// FormLookupKUPPASingle('txtNoProgramPL1RKAPengembangan','txtNmProgramPL1RKAPengembangan',getCriteriaLookupProgUK());
													FormLookupKUPPASingle('txtNoProgramPL1RKAPengembangan','txtNmProgramPL1RKAPengembangan',getCriteriaLookupProgUK(),'txtNoProgAliasPL1RKAPengembangan');
												}
												else
												{
													ShowPesanWarning("Unit Kerja dan Tahun Anggaran belum dipilih",namaform_RKATPengembangan);
												}
											}
										}
									}
								},
								{
									xtype: 'displayfield',
									value: 'Prioritas: ',
									width: 60
								},
								{
									xtype: 'textfield',
									id: 'txtPrioritasPL1RKAPengembangan',
									width: 50,
									style: 'text-align: right',
									readOnly:true,
									emptyText: 'Auto'
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Kegiatan',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaKegiatanPL1RKAPengembangan',
									height: 40
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							fieldLabel: 'Latar Belakang',
							defaults: { flex: 1 },
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaLatarPL1RKAPengembangan',
									height: 40
								},
								{
									xtype: 'displayfield',									
									value: 'Rasional: ',
									width: 100,									
								},
								{
									xtype: 'textarea',									
									id: 'txtaRasionalPL1RKAPengembangan',
									height: 40
								}
							]
						},					
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Tujuan',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaTujuanPL1RKAPengembangan',
									height: 40
								},
								{
									xtype: 'displayfield',									
									value: 'Mekanisme: ',
									width: 100
								},
								{
									xtype: 'textarea',									
									id: 'txtaMekanismePL1RKAPengembangan',
									height: 40
								}
							]
						},					
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Target Indikator',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaTargetPL1RKAPengembangan',
									height: 40
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Plafond (Rp.)',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtJumPlafondPL1RKAPengembangan',									
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Non Rutin: ',
									width: 90
								},
								{
									xtype: 'textfield',									
									id: 'txtPengembanganPL1RKAPengembangan',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Rutin: ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtRutinPL1RKAPengembangan',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Sisa (Rp.): ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtSisaPlafondPL1RKAPengembangan',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Status RAB',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtaStatusPL1_RKAPengembangan',
									readOnly: true,
									style: 'color: #FF0000; font: bold 120% "Verdana";'
								}
							]
						}
					]
				}
			],
			listeners:
			{
				'activate': function()
				{
					tabActive = "PL1";
					// if(vAddNewRKAPengembangan != undefined && !vAddNewRKAPengembangan)
					// { 
						//RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan()+"&KD_JNS_RKAT_JRKA=1");
						// RefreshDataGLDaftarPL1(getCriteriaGListDaftarPL1()); 
					// }
					RefreshDataGLDaftarPL1(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),selectUnitKerjaTabPL1RKAPengembangan);
					Ext.getCmp('tabFormEntryDetailRKAPengembangan').setActiveTab(
						Ext.getCmp('tabDaftarFormEntryDetailRKAPengembangan')
					);
					Ext.getCmp('dspTotBiayaDaftarRKAPengembangan').setValue("Total Biaya Rp.");					
				}
			}
		}
	);
	
	var tabPN1FormEntryHeaderRKAPengembangan = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabPN1FormEntryHeaderRKAPengembangan',
            title: 'Penerimaan (PN-1)',
            items: 
			[
                {
					xtype: 'fieldset',
					style: 'margin: 5px',					
					items: 
					[						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Unit Kerja',
							labelWidth: 120,
							items: 
							[
								mCboUnitKerjaTabPN1RKAPengembangan(),
								{
									xtype: 'displayfield',
									value: 'Tahun Anggaran: ',
									width: 100
								},
								mComboTahunEntryPN1RKAPengembangan(),
								{
									xtype: 'displayfield',
									value: 'Revisi: ',
									width: 30
								},
								{
									xtype: 'checkbox',
									id: 'txtCheckPN1RKAPengembangan',
									readOnly:true									
								}
								
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Program',
							labelWidth: 120,
							items: [
								{
									xtype: 'textfield',
									id: 'txtNoProgramPN1RKAPengembangan',
									width: 80,
									style: 'text-align: right',
									readOnly: true
								},
								{
									xtype: 'textfield',									
									id: 'txtNmProgramPN1RKAPengembangan',
									enableKeyEvents: true,
									listeners:
									{										
										'specialkey': function()
										{
											if(Ext.EventObject.getKey() === 13)
											{
												if(selectUnitKerjaTabPN1RKAPengembangan !== undefined && Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue() !== "")
												{
													// FormLookupKUPPASingle('txtNoProgramPN1RKAPengembangan','txtNmProgramPN1RKAPengembangan',getCriteriaLookupProgUK());
													FormLookupKUPPASingle('txtNoProgramPN1RKAPengembangan','txtNmProgramPN1RKAPengembangan',getCriteriaLookupProgUK(),'txtNoProgAliasPL1RKAPengembangan');
												}
												else
												{
													ShowPesanWarning("Unit Kerja dan Tahun Anggaran belum dipilih",namaform_RKATPengembangan);
												}
											}
										}
									}
								},
								{
									xtype: 'displayfield',
									value: 'Prioritas: ',
									width: 60
								},
								{
									xtype: 'textfield',
									id: 'txtPrioritasPN1RKAPengembangan',
									width: 50,
									readOnly:true,
									style: 'text-align: right'									
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Kegiatan',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaKegiatanPN1RKAPengembangan',
									height: 40
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							fieldLabel: 'Latar Belakang',
							defaults: { flex: 1 },
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaLatarPN1RKAPengembangan',
									height: 40
								},
								{
									xtype: 'displayfield',									
									value: 'Rasional: ',
									width: 100,									
								},
								{
									xtype: 'textarea',									
									id: 'txtaRasionalPN1RKAPengembangan',
									height: 40
								}
							]
						},					
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Tujuan',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaTujuanPN1RKAPengembangan',
									height: 40
								},
								{
									xtype: 'displayfield',									
									value: 'Mekanisme: ',
									width: 100
								},
								{
									xtype: 'textarea',									
									id: 'txtaMekanismePN1RKAPengembangan',
									height: 40
								}
							]
						},					
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Target Indikator',
							items: [
								{
									xtype: 'textarea',
									id: 'txtaTargetPN1RKAPengembangan',
									height: 40
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Plafond (Rp.)',
							hidden: true,
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtJumPlafondPN1RKAPengembangan',									
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Pengembangan: ',
									width: 90
								},
								{
									xtype: 'textfield',									
									id: 'txtPengembanganPN1RKAPengembangan',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Rutin: ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtRutinPN1RKAPengembangan',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Sisa (Rp.): ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtSisaPlafondPN1RKAPengembangan',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Status RAB',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtaStatusPN1_RKAPengembangan',
									readOnly: true,
									style: 'color: #FF0000; font: bold 120% "Verdana";'
								}
							]
						}
					]
				}
			],
			listeners:
			{
				'activate': function()
				{
					tabActive = "PN1";
					// if(vAddNewRKAPengembangan !== undefined && !vAddNewRKAPengembangan)
					// { 
						//RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan()+"&KD_JNS_RKAT_JRKA=2");
						RefreshDataGLDaftarPN1(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),selectUnitKerjaTabPN1RKAPengembangan); 
					// }
					Ext.getCmp('tabFormEntryDetailRKAPengembangan').setActiveTab(
						Ext.getCmp('tabDaftarFormEntryDetailRKAPengembangan')
					);
					Ext.getCmp('dspTotBiayaDaftarRKAPengembangan').setValue("Total Penerimaan Rp.");
				}
			}
		}
	);
	
	var tabFormEntryHeaderRKAPengembangan = new Ext.TabPanel
	(
		{
			id: 'tabFormEntryHeaderRKAPengembangan',			
			region: 'north',
			border: false,
			activeTab: 0,
			items: [tabPL1FormEntryHeaderRKAPengembangan,tabPN1FormEntryHeaderRKAPengembangan],
			tbar:
			{
				xtype: 'toolbar',				
				items: 
				[
					{
						xtype: 'button',
						id:'btnadd_RKATPengembangan',
						iconCls: 'add',
						text: 'Tambah',
						handler: function()
						{
							AddNewRKAPengembangan(tabActive);
						}
					},
					{
						xtype: 'button',
						id:'btnsave_RKATPengembangan',
						iconCls: 'save',
						text: 'Simpan',
						handler: function()
						{
							// Ext.getCmp('btnsave_RKATPengembangan').setDisabled(true);
							// Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(true);		
							if(tabActive == "PL1")
							{
								SaveDataPL1RKATPengembangan(false);
							}							
							else
							{
								SaveDataPN1RKATPengembangan(false);
							}
						}
					},
					{
						xtype: 'button',
						id:'btnsaveexit_RKATPengembangan',
						iconCls: 'saveexit',
						text: 'Simpan & Keluar',
						handler: function()
						{
							Ext.getCmp('btnsave_RKATPengembangan').setDisabled(true);
							Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(true);		

							if(tabActive == "PL1")
							{
								SaveDataPL1RKATPengembangan(true);
							}							
							else
							{
								SaveDataPN1RKATPengembangan(true);
							}
						}
					},
					{
						xtype: 'button',
						id:'btnremove_RKATPengembangan',
						iconCls: 'remove',
						text: 'Hapus',
						handler: function()
						{
							if(tabActive == "PL1")
							{
								HapusDataPL1RKATPengembangan();
							}							
							else
							{
								HapusDataPN1RKATPengembangan();
							}
						}
					},
					{ xtype: 'tbseparator' },					
					{
						xtype: 'button',
						id: 'btnLookopRKAPengembangan',
						iconCls: 'find',
						text: 'Look Up',
						handler: function()
						{	
							if(tabActive == "PL1")
							{
								//SEMENTARA DIKOMENTAR DULU
								if(selectUnitKerjaTabPL1RKAPengembangan !== undefined && Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() !== "")
								{	
									FormLookupKUPPASingle('txtNoProgramPL1RKAPengembangan','txtNmProgramPL1RKAPengembangan',getCriteriaLookupProgUK(),'txtNoProgAliasPL1RKAPengembangan');
								}
								else
								{
									ShowPesanWarning("Unit Kerja dan Tahun Anggaran belum dipilih",namaform_RKATPengembangan);
								}		
							}
							else
							{
								console.log(selectUnitKerjaTabPN1RKAPengembangan, Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue());
								if(selectUnitKerjaTabPN1RKAPengembangan !== undefined && Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue() !== "")
								{	
									FormLookupKUPPASingle('txtNoProgramPN1RKAPengembangan','txtNmProgramPN1RKAPengembangan',getCriteriaLookupProgUK(),'txtNoProgAliasPL1RKAPengembangan');
								}
								else
								{
									ShowPesanWarning("Unit Kerja dan Tahun Anggaran belum dipilih",namaform_RKATPengembangan);
								}
							}
						}
					},
					{
						xtype: 'tbfill'
					}/*,
					{
						xtype: 'button',
						iconCls: 'print',
						text: 'Cetak'
					}*/
				]
			}	
		}
	);
	
	return tabFormEntryHeaderRKAPengembangan;
}

function getCriteriaLookupProgUK()
{
	var strKriteria = "";
		
	if(tabActive == "PL1")
	{
		if(selectUnitKerjaTabPL1RKAPengembangan !== undefined)
		{
			strKriteria += "unit=" + selectUnitKerjaTabPL1RKAPengembangan;
		}
		
		if(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() != "")
		{
			if(strKriteria != "")
			{
				strKriteria += "&";
			}
			strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue();
		}
	}
	else if(tabActive == "PN1")
		{
			if(selectUnitKerjaTabPN1RKAPengembangan !== undefined)
			{
				strKriteria += "unit=" + selectUnitKerjaTabPN1RKAPengembangan;
			}
			
			if(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue() != "")
			{
				if(strKriteria != "")
				{
					strKriteria += "&";
				}
				strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue();
			}
		}
	
	return strKriteria;
}

function mComboTahunEntryPL1RKAPengembangan()
{
	/* var Field = ['TAHUN_ANGGARAN_TA','TMP_TAHUN'];
	dsThAnggPaguRKATPL1 = new WebApp.DataStore({ fields: Field });
	
	dsThAnggPaguRKATPL1.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA', 
				Sortdir: 'ASC', 
				target:'viCboThnAngg',
				param: 0
			} 
		}
	); */
	
	var currYear = parseInt(now.format('Y'));	
	var cboTahunEntryPL1RKAPengembangan = new Ext.form.ComboBox
	(
		{
			id:'cboTahunEntryPL1RKAPengembangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:100,
			// store: dsThAnggPaguRKATPL1,
			// valueField: 'TAHUN_ANGGARAN_TA',
			// displayField: 'TMP_TAHUN',
			// value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1),
			// value:'',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value: gstrTahunAngg+'/'+(Ext.num(gstrTahunAngg)+1),
			value: Ext.num(now.format('Y')),
			listeners:
			{
				'select': function(){ GetPlafondRKAT1(); }
			}
		}
	);	
	
	return cboTahunEntryPL1RKAPengembangan;
};

function mCboUnitKerjaTabPL1RKAPengembangan(rowdata) 
{
     var Field = ['kd_unit', 'nama_unit'];
    dsCboUnitKerjaTabPL1RKAPengembangan = new WebApp.DataStore({ fields: Field });
    
    var cboUnitKerjaTabPL1RKAPengembangan = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaTabPL1RKAPengembangan',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    width: 250,
		    store: dsCboUnitKerjaTabPL1RKAPengembangan,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaTabPL1RKAPengembangan = b.data.kd_unit;
					GetPlafondRKAT1(rowdata);
			    }
			}
		}
	);

	/* dsCboUnitKerjaTabPL1RKAPengembangan.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    //param: "kdunit=" + gstrListUnitKerja
			    param: ''
			}
		}
	); */
	
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaTabPL1RKAPengembangan.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaTabPL1RKAPengembangan.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaTabPL1RKAPengembangan.add(recs);
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
    return cboUnitKerjaTabPL1RKAPengembangan;
};

function mComboTahunEntryPN1RKAPengembangan()
{
	var Field = ['TAHUN_ANGGARAN_TA','TMP_TAHUN'];
	dsThAnggPaguRKATPN1 = new WebApp.DataStore({ fields: Field });
	var currYear = parseInt(now.format('Y'));
	var cboTahunEntryPN1RKAPengembangan = new Ext.form.ComboBox
	(
		{
			id:'cboTahunEntryPN1RKAPengembangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:100,
			store: dsThAnggPaguRKATPN1,
			/* valueField: 'TAHUN_ANGGARAN_TA',
			displayField: 'TMP_TAHUN', */
			//value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1)
			// value:''
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value: gstrTahunAngg+'/'+(Ext.num(gstrTahunAngg)+1),
			value: Ext.num(now.format('Y')),
			listeners:
			{
				'select': function(){ 
					// GetPlafondRKAT1(); 
				}
			}
		}
	);

	/* dsThAnggPaguRKATPN1.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA', 
				Sortdir: 'ASC', 
				target:'viCboThnAngg',
				param: 0
			} 
		}
	); */
	
	return cboTahunEntryPN1RKAPengembangan;
};

function mCboUnitKerjaTabPN1RKAPengembangan() 
{
     var Field = ['kd_unit', 'nama_unit'];
	dsCboUnitKerjaTabPN1RKAPengembangan = new WebApp.DataStore({ fields: Field });
    
    var cboUnitKerjaTabPN1RKAPengembangan = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaTabPN1RKAPengembangan',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    width: 250,
		    store: dsCboUnitKerjaTabPN1RKAPengembangan,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaTabPN1RKAPengembangan = b.data.kd_unit;
					//REFRESH DAFTAR KEGIATAN PENERIMAAN
					RefreshDataGLDaftarPN1(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),selectUnitKerjaTabPL1RKAPengembangan);
					
				}
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaTabPN1RKAPengembangan.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaTabPN1RKAPengembangan.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaTabPN1RKAPengembangan.add(recs);
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
    return cboUnitKerjaTabPN1RKAPengembangan;
};

function getTabPanelFormEntryDetailRKAPengembangan()
{
	var pnlTotBiayaKegDaftarRKAPengembangan = new Ext.Panel
	(
		{
			id: 'pnlTotBiayaKegDaftarRKAPengembangan',
			layout: 'hbox',
			border: false,			
			bodyStyle: 'padding: 14px 18px 0 350px;border-top: solid 1px #DFE8F6;',
			defaults:
			{
				flex: 1
			},
			items:
			[
				{
					xtype: 'displayfield',
					id: 'dspTotBiayaDaftarRKAPengembangan',
					value: 'Total Biaya Rp. ',
					style: 'padding-left: 50px;font-weight: bold;font-size: 14px;'					
				},
				{
					xtype: 'textfield',
					id: 'txtTotBiayaKegDaftarRKAPengembangan',
					readOnly: true,
					width: 198,
					style: 'font-weight: bold;text-align: right'
				}
			]
		}
	);
	
	var tabDaftarFormEntryDetailRKAPengembangan = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabDaftarFormEntryDetailRKAPengembangan',
            title: 'Daftar ',
			autoScroll: true,			
            items: 
			[
               getGridListDaftarRKAPengembangan(),
			   pnlTotBiayaKegDaftarRKAPengembangan
			],
			listeners:
			{
				'activate': function()
				{
					tabDetActive = 0;					
				}
			}
		}
	);
	
	var pnlTotBiayaKegDetailRKAPengembangan = new Ext.Panel
	(
		{
			id: 'pnlTotBiayaKegDetailRKAPengembangan',
			layout: 'hbox',
			border: false,			
			bodyStyle: 'padding: 2px 18px 0 350px;border-top: solid 1px #DFE8F6;',
			defaults:
			{
				flex: 1
			},
			items:
			[
				{
					xtype: 'displayfield',
					id: 'dspTotBiayaDetailRKAPengembangan',
					value: 'Total Jumlah Rp. ',
					style: 'padding-left: 50px;font-weight: bold;font-size: 14px;'					
				},
				{
					xtype: 'textfield',
					id: 'txtTotBiayaKegDetailRKAPengembangan',
					readOnly: true,
					width: 198,
					style: 'font-weight: bold;text-align: right'
				}
			]
		}
	);
	
	var tabDetailFormEntryDetailRKAPengembangan = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabDetailFormEntryDetailRKAPengembangan',
            title: 'Detail ',
			autoScroll: false,
            items: 
			[
				getGridListDetailRKAPengembangan(),
				pnlTotBiayaKegDetailRKAPengembangan
			],
			tbar:
			{
				xtype: 'toolbar',				
				items: 
				[
					{
						xtype: 'button',
						id:'btndetailadd_RKATPengembangan',
						iconCls: 'add',
						text: 'Tambah Baris',
						handler: function()
						{
							TambahBarisGLDetailRKAPengembangan(tabActive);
						}
					},					
					{
						xtype: 'button',
						id:'btndetailhapus_RKATPengembangan',
						iconCls: 'remove',
						text: 'Hapus Baris',
						handler: function()
						{
							HapusBarisDetailRKAPengembangan();
							
						}
					}	
				]
			},
			listeners:
			{
				'activate': function()
				{
					tabDetActive = 1;
					console.log(rowSelectedDaftarRKAPengembangan);
					if(rowSelectedDaftarRKAPengembangan !== undefined)
					{
						Ext.getCmp('btndetailadd_RKATPengembangan').setDisabled(false);
						Ext.getCmp('btndetailhapus_RKATPengembangan').setDisabled(false);
						if(tabActive == "PL1")
						{
							RefreshDataGLDetailPL1(
								Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
								selectUnitKerjaTabPL1RKAPengembangan,
								1,//pengeluaran
								Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').getValue(),
								Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue()
							);
						}
						else
						{
							RefreshDataGLDetailPN1(
								Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
								selectUnitKerjaTabPN1RKAPengembangan,
								2,//penerimaan
								Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(),
								Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue()
							);
						}
					}
					else
					{
						
						Ext.getCmp('btndetailadd_RKATPengembangan').setDisabled(true);
						Ext.getCmp('btndetailhapus_RKATPengembangan').setDisabled(true);
						alert("Harap Pilih Daftar RAB Terlebih Dahulu!")
						// Ext.getCmp('tabFormEntryDetailRKAPengembangan').setActiveTab
						// (
							// Ext.getCmp('tabDaftarFormEntryDetailRKAPengembangan')
						// );
					}
					gridListDaftarRKAPengembangan.getSelectionModel().clearSelections();
					rowSelectedDaftarRKAPengembangan=undefined;
				}
			}
		}
	);
	
	/*var tabDetailPusatFormEntryDetailRKAPengembangan = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabDetailPusatFormEntryDetailRKAPengembangan',
            title: 'Tambahan',
			autoScroll: true,
            items: 
			[
				//getGridListDetailPusatRKAPengembangan()
			],
			tbar:
			{
				xtype: 'toolbar',				
				items: 
				[
					{
						xtype: 'button',
						iconCls: 'add',
						text: 'Tambah Baris',
						handler: function()
						{
							
						}
					},					
					{
						xtype: 'button',
						iconCls: 'remove',
						text: 'Hapus Baris'
					}	
				]
			},
			listeners:
			{
				'activate': function()
				{
					tabDetActive = 2;
				}
			}
		}
	);*/
	
	var tabFormEntryDetailRKAPengembangan = new Ext.TabPanel
	(
		{
			id: 'tabFormEntryDetailRKAPengembangan',			
			region: 'center',
			border: false,
			activeTab: 0,
			items: 
			[
				tabDaftarFormEntryDetailRKAPengembangan,
				tabDetailFormEntryDetailRKAPengembangan/*,
				tabDetailPusatFormEntryDetailRKAPengembangan*/
			]
		}
	);
	
	return tabFormEntryDetailRKAPengembangan;
}

function getGridListDaftarRKAPengembangan()
{
	var Field = [		
		'kd_unit_kerja',
		'tahun_anggaran_ta',
		'kd_jns_rkat_jrka',
		'no_program_prog',
		'prioritas',
		'nama_program_prog',
		'kegiatan_rkatk',
		'tujuan_rkatk',
		'rasional_rkatk',
		'latarblkg_rkatk',
		'mekanisme_rkatk',
		'target_ind_rkatk',
		'unitkerja',
		'parent',
		'plafondawal',
		'jumlah',
		'disahkan_rka','jml_rkat','is_revisi',
		'alias_noprogram'
	];
    dsGLDaftarRKAPengembanganList = new WebApp.DataStore({ fields: Field });
	
	gridListDaftarRKAPengembangan = new Ext.grid.EditorGridPanel
	(
		{
			id: 'gridListDaftarRKAPengembangan',
			stripeRows: false,
			columnLines:true,
			border:false,
			store: dsGLDaftarRKAPengembanganList,			
			// anchor: '100% 85%',
			anchor: '100% 80%',
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedDaftarRKAPengembangan = dsGLDaftarRKAPengembanganList.getAt(row);
							CurrentGLDaftarRKAPengembangan.row = row;
							CurrentGLDaftarRKAPengembangan.data = rowSelectedDaftarRKAPengembangan.data;	

							initFormEntryFromDaftarRKAPengembangan(rowSelectedDaftarRKAPengembangan.data);							
						}
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),					
					{
						id: 'col_noprog_glist_daftarrkat1',
						header: "No. Program",
						dataIndex: 'no_program_prog',
						// dataIndex: 'ALIAS_NOPROGRAM',						
						sortable: true,						
						width: 100,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},
					{
						id: 'col_nmprog_glist_daftarrkat1',
						header: "Program",
						dataIndex: 'nama_program_prog',
						sortable: true,
						flex: 1,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},					
					{
						id: 'col_keg_glist_daftarrkat1',
						header: "Kegiatan",
						dataIndex: 'kegiatan_rkatk',
						sortable: true,
						flex: 1,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},
					{
						id: 'col_jum_glist_daftarrkat1',
						header: "Jumlah (Rp.)",
						dataIndex: 'jumlah',
						sortable: true,	
						width: 115,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						}
					}
				]
			),	
			viewConfig: { forceFit: true },
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							getTotalBiayaTabDaftarRKAT1();						
							//totalSisadaftar_RKATPengembangan();
							//CalcPlafondRKATPengembangan(); 
						} 
					);
					this.store.on("datachanged", function()
						{
							getTotalBiayaTabDaftarRKAT1();
							//totalSisadaftar_RKATPengembangan();
							//CalcPlafondRKATPengembangan(); 						
						}
					);
				}
			}
		}
	);
	
	return gridListDaftarRKAPengembangan;
}

function getGridListDetailRKAPengembangan()
{
	var Fields = [
		'urut_rkatkdet',
		'tahun_anggaran_ta',
		'kd_unit_kerja',
		'no_program_prog',
		'prioritas',
		'kd_jns_rkat_jrka',
		'account',
		'kd_satuan_sat',
		'satuan_sat',
		'name',
		'kuantitas_rkatkdet',
		'biaya_satuan_rkatkdet',
		'jmlh_rkatkdet',
		'm1_rkatkdet',
		'm2_rkatkdet',
		'm3_rkatkdet',
		'm4_rkatkdet',
		'm5_rkatkdet',
		'm6_rkatkdet',
		'm7_rkatkdet',
		'm8_rkatkdet',
		'm9_rkatkdet',
		'm10_rkatkdet',
		'm11_rkatkdet',
		'm12_rkatkdet',
		'deskripsi_rkatkdet',
		'account_temp'
	];
    dsGLDetailRKAPengembanganList = new WebApp.DataStore({ fields: Fields });
	
	gridListDetailRKAPengembangan = new Ext.grid.EditorGridPanel
	(
		{
			id: 'gridListDetailRKAPengembangan',
			stripeRows: false,
			columnLines:true,
			border:false,
			autoScroll: false,
			anchor: '100% 80%',
			store: dsGLDetailRKAPengembanganList,									
			/* sm: new Ext.grid.RowSelectionModel
			(
				{					
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedDetailRKAPengembangan = dsGLDetailRKAPengembanganList.getAt(row);
							CurrentGLDetailRKAPengembangan.row = row;
							CurrentGLDetailRKAPengembangan.data = rowSelectedDetailRKAPengembangan.data;
							danaRKAPengembangan = rowSelectedDetailRKAPengembangan.data.JUMLAH;
						}
					}
				}
			), */
			selModel: new Ext.grid.CellSelectionModel({
				singleSelect: true,
				listeners:{
					cellselect: function(sm, row, rec){
							rowSelectedDetailRKAPengembangan = dsGLDetailRKAPengembanganList.getAt(row);
							CurrentGLDetailRKAPengembangan.row = row;
							CurrentGLDetailRKAPengembangan.data = rowSelectedDetailRKAPengembangan.data;
							danaRKAPengembangan = rowSelectedDetailRKAPengembangan.data.jumlah;
					}
				}
			}),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: "urut_rkatkdet",
						dataIndex: 'urut_rkatkdet',
						sortable: true,
						width: 55,
						hidden:true
					},
					{
						id: 'col_noakun_glist_detailrkat1',
						header: "No. Akun",
						dataIndex: 'account',
						sortable: true,						
						width : 70,
						editor: new Ext.form.TextField(
							{
								id: 'fcol_noakun_glist_detailrkat1',								
								enableKeyEvents : true,
								listeners: 
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() == 13) 
										{										
											var unit = "";
											if(tabActive == "PL1")
											{
												unit = selectUnitKerjaTabPL1RKAPengembangan;
												GetFormLookupAccountRKATPengembangan("account like '" + this.getValue() +"%' AND groups= 5");
											}
											else
											{
												unit = selectUnitKerjaTabPN1RKAPengembangan;
												GetFormLookupAccountRKATPengembangan("account like '" + this.getValue() +"%' AND groups= 4");
											}											
											//GetFormLookupAccountRKATPengembangan("kdunit=" + unit  + "&noakun=" + this.getValue());											
											// GetFormLookupAccountRKATRutin("account like '" + this.getValue() +"%' AND groups= 5 ");
										} 
									}
								}
							}
						)
					},
					{
						id: 'col_nmakun_glist_detailrkat1',
						header: "Nama Akun & Uraian",
						dataIndex: 'name',
						sortable: true,
						width : 240,
						editor: new Ext.form.TextField(
							{
								id:'fcol_nmakun_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents : true,
								listeners: 
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() == 13) 
										{											
											var unit = "";
											if(tabActive == "PL1")
											{
												unit = selectUnitKerjaTabPL1RKAPengembangan;
												GetFormLookupAccountRKATPengembangan("upper(name) like '" + this.getValue().toUpperCase() +"%' AND groups= 5");										
											}
											else
											{
												unit = selectUnitKerjaTabPN1RKAPengembangan;
												GetFormLookupAccountRKATPengembangan("upper(name) like '" + this.getValue().toUpperCase() +"%' AND groups= 4");										
											}
										} 
									}
								}
							}
						)
					},
					{
						id: 'col_deskripsi_glist_detailrkat1',
						header: "Deskripsi",
						dataIndex: 'deskripsi_rkatkdet',
						sortable: true,
						width : 240,
						editor: new Ext.form.TextField(
							{
								id:'fcol_deskripsi_glist_detailrkat1',
								allowBlank: true,
								listeners: 
								{
									/* 'blur': function()
									{
										var qty = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
										var satuan = parseFloat(this.getValue());
										var total = qty * satuan;
										Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
										dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;
									} */
								}
							}
						)
					},
					{
						id: 'col_qty_glist_detailrkat1',
						header: "Kuantitas",
						dataIndex: 'kuantitas_rkatkdet',
						sortable: true,	
						align: 'right',
						width : 55,
						editor: new Ext.form.NumberField(
							{
								id:'fcol_qty_glist_detailrkat1',
								allowBlank: true,
								listeners: 
								{
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9)
										{
											var qty = parseFloat(this.getValue()); //parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
											var satuan = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.biaya_satuan_rkatkdet);//parseFloat(this.getValue());
											var total = parseFloat(qty * satuan);
											Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);										
											console.log(qty);
											console.log(satuan);
											dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.jmlh_rkatkdet = total;		
											// alert(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH);
											getTotalBiayaTabDetailRKAT1();
											getTotalPengembangan_RKATPengembangan(false);
										}
									},
									/* 'blur': function()
									{
										var qty = parseFloat(this.getValue()); //parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
										var satuan = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.BIAYA_SATUAN);//parseFloat(this.getValue());
										var total = qty * satuan;
										Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
										dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;
										getTotalBiayaTabDetailRKAT1();
										getTotalPengembangan_RKATPengembangan(false);
									},
									'change':function(field,newValue,oldValue )
									{
									if(Ext.EventObject.getKey() != 13)
									{								  
										if (newValue != oldValue)
										{											
											// var qty = parseFloat(this.getValue()); //parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
											// var satuan = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.BIAYA_SATUAN);//parseFloat(this.getValue());
											// var total = qty * satuan;
											// Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
											// dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;
											// getTotalBiayaTabDetailRKAT1();
											// getTotalPengembangan_RKATPengembangan(false);
										
										}
									}
									} */
								}
							}
						)
					},
					{
						id: 'col_sat_glist_detailrkat1',
						header: "Satuan",
						dataIndex: 'satuan_sat',
						sortable: true,
						width: 55,
						editor: mComboFieldEntrySatDetailRKAT1()
					},
					{
						id: 'col_biayasat_glist_detailrkat1',
						header: "Biaya/Satuan (Rp.)",
						dataIndex: 'biaya_satuan_rkatkdet',
						sortable: true,								
						width : 100,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								id:'fcol_biayasat_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{
									// 'specialkey': function()
									// {
										// if(Ext.EventObject.getKey() == 13)
										// {
											// //var qty = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
											// //var satuan = parseFloat(this.getValue());
											// //var total = qty * satuan;
											// //Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
											// //dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;		
											// //getTotalBiayaTabDetailRKAT1();
											// //getTotalPengembangan_RKATPengembangan(false);
										// }
									// },
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9)
										{
											var qty =  parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.kuantitas_rkatkdet);
											var satuan = parseFloat(this.getValue());//parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.BIAYA_SATUAN);//parseFloat(this.getValue());
											var total = parseFloat(qty * satuan);
											console.log(total);
											Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
											dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.jmlh_rkatkdet = total;		
											getTotalBiayaTabDetailRKAT1();
											getTotalPengembangan_RKATPengembangan(false);
										}
									}
									// ,
									// 'blur': function()
									// {
										// // var qty = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
										// // var satuan = parseFloat(this.getValue());
										// // var total = qty * satuan;
										// // Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);										
										// // getTotalBiayaTabDetailRKAT1();
										// // getTotalPengembangan_RKATPengembangan(false);
										// // getTotalPengembangan_RKATPengembangan()
										// // dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;
										// var qty =  parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
										// var satuan = parseFloat(this.getValue());//parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.BIAYA_SATUAN);//parseFloat(this.getValue());
										// var total = qty * satuan;
										// Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
										// dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;		
										// getTotalBiayaTabDetailRKAT1();
										// getTotalPengembangan_RKATPengembangan(false);
									// },
									// 'change':function(field,newValue,oldValue )
									// {
									// if(Ext.EventObject.getKey() != 13)
										// {
											// if (newValue != oldValue)
											// {
												// // var qty = parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
												// // var satuan = parseFloat(this.getValue());
												// // var total = qty * satuan;
												// // Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);												
												// // getTotalBiayaTabDetailRKAT1();
												// // getTotalPengembangan_RKATPengembangan(false);
												// // dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;
												// var qty =  parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.QTY);
												// var satuan = parseFloat(this.getValue());//parseFloat(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.BIAYA_SATUAN);//parseFloat(this.getValue());
												// var total = qty * satuan;
												// Ext.getCmp('fcol_jum_glistpengembangan_detailrkat').setValue(total);
												// dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.JUMLAH = total;		
												// getTotalBiayaTabDetailRKAT1();
												// getTotalPengembangan_RKATPengembangan(false);
											// }
									  // }
									// }
								}
							}
						)
					},
					{
						id: 'col_jum_glist_detailrkat1',
						header: "Jumlah (Rp.)",
						dataIndex: 'jmlh_rkatkdet',						
						width : 80,
						sortable: true,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						}
						,
						editor: new Ext.form.NumberField(
							{
								id:'fcol_jum_glistpengembangan_detailrkat',
								// allowBlank: true,
								readOnly: true,
								'blur': function()
								{
								//	getTotalBiayaTabDetailRKAT1();
									//getSisaDetail_RKATPengembangan();
									//getTotalPengembangan_RKATPengembangan(false);
								}
							}
						)						
					},
					{
						// id: 'col_jan_glist_detailrkat1',
						header: "Jan (Rp.)",
						dataIndex: 'm1_rkatkdet',
						sortable: true,						
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_jan_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
										
									}
								}
							}
						)
					},
					{
						// id: 'col_feb_glist_detailrkat1',
						header: "Feb (Rp.)",
						dataIndex: 'm2_rkatkdet',
						sortable: true,						
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_feb_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
										
									}
								}								
							}
						)
					},
					{
						// id: 'col_mar_glist_detailrkat1',
						header: "Mar (Rp.)",
						dataIndex: 'm3_rkatkdet',
						sortable: true,							
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_mar_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_apr_glist_detailrkat1',
						header: "Apr (Rp.)",
						dataIndex: 'm4_rkatkdet',
						sortable: true,	
						align: 'right',
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_apr_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{

									}
								}
							}
						)
					},
					{
						// id: 'col_mei_glist_detailrkat1',
						header: "Mei (Rp.)",
						dataIndex: 'm5_rkatkdet',
						sortable: true,							
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_mei_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_jun_glist_detailrkat1',
						header: "Jun (Rp.)",
						dataIndex: 'm6_rkatkdet',
						sortable: true,							
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_jun_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_jul_glist_detailrkat1',
						header: "Jul (Rp.)",
						dataIndex: 'm7_rkatkdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_jul_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}								
							}
						)
					},
					{
						// id: 'col_ags_glist_detailrkat1',
						header: "Ags (Rp.)",
						dataIndex: 'm8_rkatkdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_ags_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}								
							}
						)
					},
					{
						// id: 'col_sep_glist_detailrkat1',
						header: "Sep (Rp.)",
						dataIndex: 'm9_rkatkdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_sep_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_okt_glist_detailrkat1',
						header: "Okt (Rp.)",
						dataIndex: 'm10_rkatkdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_okt_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
					
									}
								}
							}
						)
					},
					{
						// id: 'col_nov_glist_detailrkat1',
						header: "Nov (Rp.)",
						dataIndex: 'm11_rkatkdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_nov_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_des_glist_detailrkat1',
						header: "Des (Rp.)",
						dataIndex: 'm12_rkatkdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_des_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
										
									}
								}								
							}
						)
					}
					
				]
			),
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							getTotalBiayaTabDetailRKAT1();
							//getTotalPengembangan_RKATPengembangan(false);
							//totalSisadaftar_RKATPengembangan();
						} 
					);
					this.store.on("datachanged", function()
						{
							getTotalBiayaTabDetailRKAT1();
							//getTotalPengembangan_RKATPengembangan();
							//totalSisadaftar_RKATPengembangan();					
						}
					);
				}
			}
		}
	);
	
	return gridListDetailRKAPengembangan;
}

function mComboFieldEntrySatDetailRKAT1()
{
	var Field = ['kd_satuan_sat','satuan_sat'];
	var dsCboFieldSatuanDetailRKAT1 = new WebApp.DataStore({ fields: Field });

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_satuan",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data satuan !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboFieldSatuanDetailRKAT1.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboFieldSatuanDetailRKAT1.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboFieldSatuanDetailRKAT1.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data satuan', 'Error');
			};
		}
	});
	
	var cboFieldSatuanDetailRKAT1 = new Ext.form.ComboBox
	(
		{
			id:'cboFieldSatuanDetailRKAT1',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',	
			width:60,
			store: dsCboFieldSatuanDetailRKAT1,
			valueField: 'satuan_sat',
			displayField: 'satuan_sat',
			listeners:
			{
				'select': function(a,b,c)
				{
					dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.kd_satuan_sat = b.data.kd_satuan_sat;					
					// dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.SATUAN_SAT = b.data.SATUAN_SAT;					
				}
			}
		}
	);
	return cboFieldSatuanDetailRKAT1;
};

/** DATA */
function RefreshDataGLRKAPengembangan_lama(filter)
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRABNonRutin/get_acc_rkat",
		params: {
			kd_unit_kerja 		: Ext.getCmp('cboUnitKerjaRKAPengembanganFilter').getValue(),
			tahun_anggaran_ta 	: Ext.getCmp('cboTahunRKAPengembanganFilter').getValue(),
			disahkan_rka		: Ext.getCmp('chkFilterRKAPengembanganFilter').getValue(),
			filter				: filter
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRKAPengembanganList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsTRRKAPengembanganList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsTRRKAPengembanganList.add(recs);
				grListTRRKAPengembangan.getView().refresh();
				console.log(dsTRRKAPengembanganList.data);
			} else {
				ShowPesanError('Gagal menampilkan data RAB', 'Error');
			};
		},
		callback:function(){
			var gridListRAB=Ext.getCmp('grListTRRKAPengembangan').getStore().data.items;
				for(var i=0,iLen=gridListRAB.length; i<iLen;i++){
					if(gridListRAB[i].data.disahkan_rka_int == 1){
						gridListRAB[i].data.disahkan_rka = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grListTRRKAPengembangan').getStore().data.items);
			Ext.getCmp('grListTRRKAPengembangan').getView().refresh();
			
		}
	});
	
	
};

function RefreshDataGLRKAPengembangan (){
	var criteria = GetCriteriaGridUtama();
	dsTRRKAPengembanganList.removeAll();
	dsTRRKAPengembanganList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKAPengembangan, 
				Sort: '', 
				Sortdir: '', 
				target: 'vi_viewdata_rab_non_rutin',
				param: criteria
			} ,
			callback:function(){
				var gridListRAB=Ext.getCmp('grListTRRKAPengembangan').getStore().data.items;
					for(var i=0,iLen=gridListRAB.length; i<iLen;i++){
						if(gridListRAB[i].data.disahkan_rka_int == 1){
							gridListRAB[i].data.disahkan_rka = true;
							console.log('true');
						} 
					}
					console.log(Ext.getCmp('grListTRRKAPengembangan').getStore().data.items);
				Ext.getCmp('grListTRRKAPengembangan').getView().refresh();
			}
		}
	);
	return dsTRRKAPengembanganList; 
}

function GetCriteriaGridUtama(){
	var criteria = '';
	if (Ext.getCmp('cboUnitKerjaRKAPengembanganFilter').getValue() == '000' || Ext.getCmp('cboUnitKerjaRKAPengembanganFilter').getValue() == 000){
		// criteria = "  WHERE a.kd_jns_plafond = 2 and a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunRKAPengembanganFilter').getValue() + " '  and a.disahkan_rka ='" +  Ext.getCmp('chkFilterRKAPengembanganFilter').getValue()+ "' ";
		criteria = "SEMUA~WHERE a.kd_jns_plafond = 2 and a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunRKAPengembanganFilter').getValue() + " '  and a.disahkan_rka ='" +  Ext.getCmp('chkFilterRKAPengembanganFilter').getValue()+ "' ";
	}else{
		criteria = "  WHERE a.kd_jns_plafond = 2 and a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunRKAPengembanganFilter').getValue() + " '  and a.disahkan_rka ='" +  Ext.getCmp('chkFilterRKAPengembanganFilter').getValue()+ "' and a.kd_unit_kerja = '"+ Ext.getCmp('cboUnitKerjaRKAPengembanganFilter').getValue()+"'";
	}
	
	return criteria;
}

function getCriteriaGListRKAPengembangan()
{
	var strKriteria = "";
	
	if(selectUnitKerjaRKAPengembanganFilter !== undefined)
	{
		strKriteria += "unit=" + selectUnitKerjaRKAPengembanganFilter;
	}
	
	if(Ext.getCmp('cboTahunRKAPengembanganFilter').getValue() != "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboTahunRKAPengembanganFilter').getValue();
	}
	
	if(Ext.getCmp('chkFilterRKAPengembanganFilter').getValue() == true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "approve=1";
	}else{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "approve=0";
	}
	
	//strKriteria += "&KD_JNS_RKAT_JRKA=1"
	return strKriteria;
}

function getCriteriaGListDaftarPL1()
{
	var strKriteria = "";
	
	if(selectUnitKerjaTabPL1RKAPengembangan !== undefined)
	{
		strKriteria += "unit=" + selectUnitKerjaTabPL1RKAPengembangan;
	}
	
	if(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue())
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue();
	}
		
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 1;
	return strKriteria;
}

function RefreshDataGLDaftarPL1(thn_anggaran,kd_unit_kerja)
{	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRABNonRutin/get_acc_rkatk",
		params: {
			thn_anggaran : thn_anggaran,
			kd_unit_kerja: kd_unit_kerja,
			kd_jns_rkat_jrka :1, //pengeluaran
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data Daftar RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLDaftarRKAPengembanganList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDaftarRKAPengembanganList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDaftarRKAPengembanganList.add(recs);
				grListTRRKAPengembangan.getView().refresh();
				getTotalBiayaTabDaftarRKAT1(); //fungsi menghitung total kegiatan
				// getTotalBiayaTabDetailRKAT2(); //fungsi menghitung total kegiatan
				
			} else {
				ShowPesanError('Gagal menampilkan data  Daftar RAB', 'Error');
			};
		}
	});
	/* dsGLDaftarRKAPengembanganList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKAPengembangan, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viGListDaftarPL1',
				param: criteria
			} 
		}
	);
	return dsGLDaftarRKAPengembanganList; */
};

function getCriteriaGListDetailPL1()
{
	var strKriteria = "";
	
	if(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() !== "")
	{ 
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(); 
	}
	
	if(selectUnitKerjaTabPL1RKAPengembangan !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectUnitKerjaTabPL1RKAPengembangan; 
	}
	
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 1; 
	
	if(Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue() !== "")
	{ 
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "noprog=" + Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(); 
	}
	
	if(Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue() !== "")
	{ 
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "prior=" + Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue();
	}
	
	return strKriteria;
}

function RefreshDataGLDetailPL1(tahun_anggaran_ta,kd_unit_kerja,kd_jns_rkat_jrka,no_program_prog,prioritas_rkatk)
{		
		Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRABNonRutin/get_acc_rkatk_det",
		params: {
			tahun_anggaran_ta	:	tahun_anggaran_ta,
			kd_unit_kerja		:	kd_unit_kerja,
			kd_jns_rkat_jrka	:	kd_jns_rkat_jrka,
			no_program_prog		:	no_program_prog,
			prioritas_rkatk		:	prioritas_rkatk
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB detail !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLDetailRKAPengembanganList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDetailRKAPengembanganList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDetailRKAPengembanganList.add(recs);
				gridListDetailRKAPengembangan.getView().refresh();
				getTotalBiayaTabDetailRKAT1();
			} else {
				ShowPesanError('Gagal menampilkan data RAB detail', 'Error');
			};
		}
	});
	/* dsGLDetailRKAPengembanganList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKAPengembangan, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viGListDetailPL1',
				param: criteria
			} 
		}
	);
	return dsGLDetailRKAPengembanganList; */
	
	
};

function getCriteriaGListDaftarPN1(fromGrid, rowdata)
{
	var strKriteria = "";
	
	if(selectUnitKerjaTabPN1RKAPengembangan !== undefined)
	{
		strKriteria += "unit=" + selectUnitKerjaTabPN1RKAPengembangan;
	}
	
	if(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue())
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue();
	}
		
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 2;
	
	return strKriteria;
}

function RefreshDataGLDaftarPN1(thn_anggaran,kd_unit_kerja)
{	
	dsGLDaftarRKAPengembanganList.removeAll();
	dsGLDetailRKAPengembanganList.removeAll();
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRABNonRutin/get_acc_rkatk",
		params: {
			thn_anggaran 		: thn_anggaran,
			kd_unit_kerja		: kd_unit_kerja,
			kd_jns_rkat_jrka 	:2, //penerimaan
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data Daftar RAB Penerimaan!', 'Error');
		},	 
		success: function(o) 
		{   
			dsGLDaftarRKAPengembanganList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDaftarRKAPengembanganList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDaftarRKAPengembanganList.add(recs);
				gridListDaftarRKAPengembangan.getView().refresh();
				getTotalBiayaTabDaftarRKAT1(); //fungsi menghitung total kegiatan
				//console.log(dsGLDaftarRKARutinList);
			} else {
				ShowPesanError('Gagal menampilkan data  Daftar RAB', 'Error');
			};
		}
	});
	/* dsGLDaftarRKAPengembanganList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKAPengembangan, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viGListDaftarPN1',
				param: criteria
			} 
		}
	);
	return dsGLDaftarRKAPengembanganList; */
};

function getCriteriaGListDetailPN1()
{
	var strKriteria = "";
	
	if(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue() !== "")
	{ 
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(); 
	}
	
	if(selectUnitKerjaTabPN1RKAPengembangan !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectUnitKerjaTabPN1RKAPengembangan; 
	}
	
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 2; 
	
	if(Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue() !== "")
	{ 
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "noprog=" + Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(); 
	}
	
	if(Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue() !== "")
	{ 
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "prior=" + Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue();
	}
	
	return strKriteria;
}

function RefreshDataGLDetailPN1(tahun_anggaran_ta,kd_unit_kerja,kd_jns_rkat_jrka,no_program_prog,prioritas_rkatk)
{	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRABNonRutin/get_acc_rkatk_det",
		params: {
			tahun_anggaran_ta	:	tahun_anggaran_ta,
			kd_unit_kerja		:	kd_unit_kerja,
			kd_jns_rkat_jrka	:	kd_jns_rkat_jrka,
			no_program_prog		:	no_program_prog,
			prioritas_rkatk		:	prioritas_rkatk
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB Penerimaan detail !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLDetailRKAPengembanganList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDetailRKAPengembanganList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDetailRKAPengembanganList.add(recs);
				gridListDetailRKAPengembangan.getView().refresh();
				getTotalBiayaTabDetailRKAT1();
			} else {
				ShowPesanError('Gagal menampilkan data RAB Penerimaan detail', 'Error');
			};
		}
	});
	/* dsGLDetailRKAPengembanganList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKAPengembangan, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viGListDetailPN1',
				param: criteria
			} 
		}
	);
	return dsGLDetailRKAPengembanganList; */
};

/** DATA */
function TambahBarisGLDetailRKAPengembangan()
{
	var x=true;
	if (dsGLDetailRKAPengembanganList.getCount() > 0)
	{		
		if(dsGLDetailRKAPengembanganList.data.items[dsGLDetailRKAPengembanganList.getCount()-1].data.NO_AKUN_AK==='')
		{
			x=false;
		}
	}
	
	if (x === true)
	{
		if(tabActive == "PL1")
		{
			var p = new mRecordGLDetailRKAPengembangan
			(
				{
					urut_rkatkdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPL1RKAPengembangan,
					no_program_prog: Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
					kd_jns_rkat_jrka: 1,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatkdet: 1,
					biaya_satuan_rkatkdet: 0,
					jmlh_rkatkdet: 0,
					m1_rkatkdet: 0,
					m2_rkatkdet: 0,
					m3_rkatkdet: 0,
					m4_rkatkdet: 0,
					m5_rkatkdet: 0,
					m6_rkatkdet: 0,
					m7_rkatkdet: 0,
					m8_rkatkdet: 0,
					m9_rkatkdet: 0,
					m10_rkatkdet: 0,
					m11_rkatkdet: 0,
					m12_rkatkdet: 0,
					deskripsi_rkatkdet:''
				}
			);
		}
		else
		{
			var p = new mRecordGLDetailRKAPengembangan
			(
				{
					urut_rkatkdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPL1RKAPengembangan,
					no_program_prog: Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
					kd_jns_rkat_jrka: 2,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatkdet: 1,
					biaya_satuan_rkatkdet: 0,
					jmlh_rkatkdet: 0,
					m1_rkatkdet: 0,
					m2_rkatkdet: 0,
					m3_rkatkdet: 0,
					m4_rkatkdet: 0,
					m5_rkatkdet: 0,
					m6_rkatkdet: 0,
					m7_rkatkdet: 0,
					m8_rkatkdet: 0,
					m9_rkatkdet: 0,
					m10_rkatkdet: 0,
					m11_rkatkdet: 0,
					m12_rkatkdet: 0,
					deskripsi_rkatkdet:''
				}
			);
		}
		dsGLDetailRKAPengembanganList.insert(dsGLDetailRKAPengembanganList.getCount(), p);
		rowSelectedDetailRKAPengembangan = dsGLDetailRKAPengembanganList.getAt(CurrentGLDetailRKAPengembangan.row);
	}
};

function AddNewRKAPengembangan(tab)
{	
console.log(dsGLDaftarRKAPengembanganList);
	if(tab == 'PL1')
	{			
		Ext.getCmp('txtPrioritasPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtNoProgramPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtNmProgramPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaKegiatanPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaLatarPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaRasionalPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaTujuanPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaMekanismePL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaTargetPL1RKAPengembangan').setValue('');
		//Tambahan reza
		Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setValue('');
		Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setReadOnly(false);
		selectUnitKerjaTabPL1RKAPengembangan = ''
		Ext.getCmp('txtJumPlafondPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtPengembanganPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtRutinPL1RKAPengembangan').setValue('');
		Ext.getCmp('txtaStatusPL1_RKAPengembangan').setValue('');
		Ext.getCmp('txtTotBiayaKegDaftarRKAPengembangan').setValue('');
		// Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setValue(gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1));
		Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setReadOnly(false);
		dsGLDaftarRKAPengembanganList.removeAll();
		//end Tambahan
		intPriorAsal='';
		strProgAsal='';
		disahkan = false;
		
	}
	else if(tab == 'PN1')
	{
		Ext.getCmp('txtPrioritasPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtNoProgramPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtNmProgramPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaKegiatanPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaLatarPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaRasionalPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaTujuanPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaMekanismePN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaTargetPN1RKAPengembangan').setValue('');
		//Ext.getCmp('txtaStatus_RKAPengembangan').setValue('');
		//Tambahan reza
		Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setValue('');
		Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setReadOnly(false);
		selectUnitKerjaTabPN1RKAPengembangan = ''
		Ext.getCmp('txtJumPlafondPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtPengembanganPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtRutinPN1RKAPengembangan').setValue('');
		Ext.getCmp('txtaStatusPN1_RKAPengembangan').setValue('');
		Ext.getCmp('txtTotBiayaKegDaftarRKAPengembangan').setValue('');
		// Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setValue(gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1));
		Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setReadOnly(false);
		dsGLDaftarRKAPengembanganList.removeAll();
		//end Tambahan
		intPriorAsal='';
		strProgAsal='';
		disahkan = false;		
		
	}
	vAddNewRKAPengembangan = true;
}

function initFormEntryRKAPengembangan(rowdata)
{
	vAddNewRKAPengembangan = false;
	tabActive = "PL1";
	//PENGELUARAN
	Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setValue(rowdata.nama_unit);
	Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setReadOnly(true);
	selectUnitKerjaTabPL1RKAPengembangan = rowdata.kd_unit_kerja;
	Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setValue(rowdata.tahun_anggaran_ta);
	Ext.get('cboTahunEntryPL1RKAPengembangan').dom.value=rowdata.tahun_anggaran_ta;
	Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setReadOnly(true);
	Ext.getCmp('txtJumPlafondPL1RKAPengembangan').setValue(formatCurrency(rowdata.plafondawal));
	Ext.getCmp('txtPengembanganPL1RKAPengembangan').setValue(formatCurrency(rowdata.jumlah_rkatk));
	// Ext.getCmp('txtRutinPL1RKAPengembangan').setValue(formatCurrency(rowdata.jumlah_rkatk));
	Ext.getCmp('txtRutinPL1RKAPengembangan').setValue(0);
	
	var a = rowdata.jumlah_rkatk;
	var b = rowdata.jumlah_rkatk;
	var c = rowdata.plafondawal;
	/*var d = a + b;
	var e = c - d; */
	var e = c - a;
	console.log(c);
	console.log(a);
	Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue(formatCurrency(e));
	
	RefreshDataGLDaftarPL1(rowdata.tahun_anggaran_ta, rowdata.kd_unit_kerja);
	
	//PENERIMAAN
	Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setValue(rowdata.nama_unit);
	Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setReadOnly(true);
	selectUnitKerjaTabPN1RKAPengembangan = rowdata.kd_unit_kerja;
	Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setValue(rowdata.tahun_anggaran_ta);
	Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setReadOnly(true);
	Ext.getCmp('txtJumPlafondPN1RKAPengembangan').setValue(formatCurrency(rowdata.plafondawal));
	Ext.getCmp('txtPengembanganPN1RKAPengembangan').setValue(formatCurrency(rowdata.jumlah_rkatk));
	Ext.getCmp('txtRutinPN1RKAPengembangan').setValue(formatCurrency(rowdata.jumlah_rkatk));
	
	var a = rowdata.jumlah_rkatk;
	var b = rowdata.jumlah_rkatk;
	var c = rowdata.plafondawal;
	var d = a + b;
	var e = c - d;
	Ext.getCmp('txtSisaPlafondPN1RKAPengembangan').setValue(formatCurrency(e));

	disahkan = rowdata.disahkan_rka;
	if(disahkan == 1 && disahkan != "")
	{
		//tambahan ades untuk proses revisi/alih anggaran............
		// if (gUSER_REVISI_RKAT == false) 
		// {
			Ext.getCmp('btnadd_RKATPengembangan').setDisabled(true);
			Ext.getCmp('btnsave_RKATPengembangan').setDisabled(true);
			Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(true);
			Ext.getCmp('btnremove_RKATPengembangan').setDisabled(true);
			Ext.getCmp('btndetailadd_RKATPengembangan').setDisabled(true);
			Ext.getCmp('btndetailhapus_RKATPengembangan').setDisabled(true);
			Ext.getCmp('btnLookopRKAPengembangan').setDisabled(true);
		// }
		//end tambahan ades untuk proses revisi/alih anggaran............
		
		Ext.getCmp('txtaStatusPL1_RKAPengembangan').setValue(" Telah Disahkan ");
		Ext.getCmp('txtaStatusPN1_RKAPengembangan').setValue(" Telah Disahkan ");
	}
	else
	{
		//tambahan ades untuk proses revisi/alih anggaran............
		// if (gUSER_REVISI_RKAT == false) 
		// {
			Ext.getCmp('btnadd_RKATPengembangan').setDisabled(false);
			Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
			Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);
			Ext.getCmp('btnremove_RKATPengembangan').setDisabled(false);
			Ext.getCmp('btndetailadd_RKATPengembangan').setDisabled(false);
			Ext.getCmp('btndetailhapus_RKATPengembangan').setDisabled(false);
			Ext.getCmp('btnLookopRKAPengembangan').setDisabled(false);
		// }
		//end tambahan ades untuk proses revisi/alih anggaran............
		
		Ext.getCmp('txtaStatusPL1_RKAPengembangan').setValue(" Belum Disahkan ");
		Ext.getCmp('txtaStatusPN1_RKAPengembangan').setValue(" Belum Disahkan ");
	}
}

function initFormEntryFromDaftarRKAPengembangan(rowdata)
{
	vAddNewRKAPengembangan = false;
	tabActive = (rowdata.kd_jns_rkat_jrka == 1) ? "PL1" : "PN1";
	
	console.log(rowdata);
	if(tabActive == "PL1")	
	{		
		// TAB PENGELUARAN			
		Ext.getCmp('tabFormEntryHeaderRKAPengembangan').setActiveTab(Ext.getCmp('tabPL1FormEntryHeaderRKAPengembangan'));
		Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setValue(rowdata.nama_unit);
		Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setReadOnly(true);
		selectUnitKerjaTabPL1RKAPengembangan = rowdata.kd_unit_kerja;
		Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setValue(rowdata.tahun_anggaran_ta);
		Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setReadOnly(true);
		Ext.getCmp('txtPrioritasPL1RKAPengembangan').setValue(rowdata.prioritas_rkatk);
		intPriorAsal = rowdata.prioritas_rkatk;
		Ext.getCmp('txtNoProgramPL1RKAPengembangan').setValue(rowdata.no_program_prog);	
		Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').setValue(rowdata.no_program_prog);	
		strProgAsal = rowdata.no_program_prog;
		Ext.getCmp('txtNmProgramPL1RKAPengembangan').setValue(rowdata.nama_program_prog);
		Ext.getCmp('txtaKegiatanPL1RKAPengembangan').setValue(rowdata.kegiatan_rkatk);
		Ext.getCmp('txtaLatarPL1RKAPengembangan').setValue(rowdata.latarblkg_rkatk);
		Ext.getCmp('txtaRasionalPL1RKAPengembangan').setValue(rowdata.rasional_rkatk);
		Ext.getCmp('txtaTujuanPL1RKAPengembangan').setValue(rowdata.tujuan_rkatk);
		Ext.getCmp('txtaMekanismePL1RKAPengembangan').setValue(rowdata.mekanisme_rkatk);
		Ext.getCmp('txtaTargetPL1RKAPengembangan').setValue(rowdata.target_ind_rkatk);
		Ext.getCmp('txtCheckPL1RKAPengembangan').setValue(rowdata.is_revisi);
		jumlah = rowdata.jumlah;
	}
	else
	{
		// TAB PENERIMAAN		
		Ext.getCmp('tabFormEntryHeaderRKAPengembangan').setActiveTab(Ext.getCmp('tabPN1FormEntryHeaderRKAPengembangan'));
		Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setValue(rowdata.nama_unit);
		Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setReadOnly(true);
		selectUnitKerjaTabPN1RKAPengembangan = rowdata.kd_unit_kerja;
		Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setValue(rowdata.tahun_anggaran_ta);
		Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setReadOnly(true);
		Ext.getCmp('txtPrioritasPN1RKAPengembangan').setValue(rowdata.prioritas_rkatk);
		intPriorAsal = rowdata.prioritas_rkatk;		
		Ext.getCmp('txtNoProgramPN1RKAPengembangan').setValue(rowdata.no_program_prog);	
		strProgAsal = rowdata.no_program_prog;
		Ext.getCmp('txtNmProgramPN1RKAPengembangan').setValue(rowdata.nama_program_prog);
		Ext.getCmp('txtaKegiatanPN1RKAPengembangan').setValue(rowdata.kegiatan_rkatk);
		Ext.getCmp('txtaLatarPN1RKAPengembangan').setValue(rowdata.latarblkg_rkatk);
		Ext.getCmp('txtaRasionalPN1RKAPengembangan').setValue(rowdata.rasional_rkatk);
		Ext.getCmp('txtaTujuanPN1RKAPengembangan').setValue(rowdata.tujuan_rkatk);
		Ext.getCmp('txtaMekanismePN1RKAPengembangan').setValue(rowdata.mekanisme_rkatk);
		Ext.getCmp('txtaTargetPN1RKAPengembangan').setValue(rowdata.target_ind_rkatk);
		Ext.getCmp('txtCheckPN1RKAPengembangan').setValue(rowdata.is_revisi);
	}	
}

function GetPlafondRKAT1(rowdata)
{
	
	 Ext.Ajax.request
	 (
		{
            url: baseURL + "index.php/anggaran_module/functionRABNonRutin/getPlafondByUnitKerja",
            params: 
			{
                /* UserID: 'Admin',
                ModuleID: 'ProsesGetPlafondUK',
				Params:	getParamProsesGetPlafondRKAT1() */
				
				thn_anggaran	:Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
				kd_unit_kerja 	:Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').getValue(),
				kd_jns_plafond	:2 //non rutin
            },
            success: function(o) 
			{
                var cst = Ext.decode(o.responseText);				
				if (cst.success === true) 
				{
					Ext.getCmp('txtJumPlafondPL1RKAPengembangan').setValue(formatCurrency(cst.Jumlah));
					Ext.getCmp('txtRutinPL1RKAPengembangan').setValue(0);
					var sisa = cst.Jumlah - Ext.getCmp('txtPengembanganPL1RKAPengembangan').getValue();
					Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue(formatCurrency(sisa));
					Ext.getCmp('txtPengembanganPL1RKAPengembangan').setValue(0);
					RefreshDataGLDaftarPL1(rowdata.tahun_anggaran_ta, rowdata.kd_unit_kerja);
				}
				else
				{
					Ext.getCmp('txtJumPlafondPL1RKAPengembangan').setValue(0);
					Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue(0);
				};
            }

        }
	);
}

function getParamProsesGetPlafondRKAT1()
{
	var param = "";
	if(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() != "")
	{
		param += "tahun=" + Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue()
	}	
	
	if(selectUnitKerjaTabPL1RKAPengembangan !== undefined)
	{
		if(param != "")
		{
			param += "&";
		}
		param += "unit=" + selectUnitKerjaTabPL1RKAPengembangan;
	}
	if(param != "")
	{
		param += "&";
	}
	param += "kdJns=" + 1; //kode Non Rutin di tabel ACC_JNS_PLAFOND
	
	
	return param;
}

function GetFormLookupAccountRKATPengembangan(str)
{
	if (vAddNewRKAPengembangan === true)
	{	

		if(tabActive == "PL1")
		{
			var p = new mRecordGLDetailRKAPengembangan
			(
				{
					urut_rkatkdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPL1RKAPengembangan,
					no_program_prog: Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
					kd_jns_rkat_jrka: 1,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatkdet: 1,
					biaya_satuan_rkatkdet: 0,
					jmlh_rkatkdet: 0,
					m1_rkatkdet: 0,
					m2_rkatkdet: 0,
					m3_rkatkdet: 0,
					m4_rkatkdet: 0,
					m5_rkatkdet: 0,
					m6_rkatkdet: 0,
					m7_rkatkdet: 0,
					m8_rkatkdet: 0,
					m9_rkatkdet: 0,
					m10_rkatkdet: 0,
					m11_rkatkdet: 0,
					m12_rkatkdet: 0,
					deskripsi_rkatkdet:''
				}
			);
		}
		else
		{
	
			var p = new mRecordGLDetailRKAPengembangan
			(
				{
					urut_rkatkdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPL1RKAPengembangan,
					no_program_prog: Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
					kd_jns_rkat_jrka: 2,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatkdet: 1,
					biaya_satuan_rkatkdet: 0,
					jmlh_rkatkdet: 0,
					m1_rkatkdet: 0,
					m2_rkatkdet: 0,
					m3_rkatkdet: 0,
					m4_rkatkdet: 0,
					m5_rkatkdet: 0,
					m6_rkatkdet: 0,
					m7_rkatkdet: 0,
					m8_rkatkdet: 0,
					m9_rkatkdet: 0,
					m10_rkatkdet: 0,
					m11_rkatkdet: 0,
					m12_rkatkdet: 0,
					deskripsi_rkatkdet:''
				}
			);
		}
		FormLookupAccountRKAT(str,dsGLDetailRKAPengembanganList,p,true,'',false);
	}
	else
	{					
	
		var p = new mRecordGLDetailRKAPengembangan
		(
			{					
				urut_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.urut_rkatkdet,
				tahun_anggaran_ta: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.tahun_anggaran_ta,
				kd_unit_kerja: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.kd_unit_kerja,
				no_program_prog: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.no_program_prog,
				prioritas: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.prioritas,
				kd_jns_rkat_jrka: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.kd_jns_rkat_jrka,
				account: '',
				kd_satuan_sat: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.kd_satuan_sat,
				satuan_sat: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.satuan_sat,
				name: '',
				kuantitas_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.kuantitas_rkatkdet,
				biaya_satuan_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.biaya_satuan_rkatkdet,
				jmlh_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.jmlh_rkatkdet,
				m1_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m1_rkatkdet,
				m2_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m2_rkatkdet,
				m3_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m3_rkatkdet,
				m4_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m4_rkatkdet,
				m5_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m5_rkatkdet,
				m6_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m6_rkatkdet,
				m7_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m7_rkatkdet,
				m8_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m8_rkatkdet,
				m9_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m9_rkatkdet,
				m10_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m10_rkatkdet,
				m11_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m11_rkatkdet,
				m12_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.m12_rkatkdet,
				deskripsi_rkatkdet: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.deskripsi_rkatkdet
			
			}
		);
		FormLookupAccountRKAT(str,dsGLDetailRKAPengembanganList,p,false,CurrentGLDetailRKAPengembangan.row,false);
	}
};

function SaveDataPL1RKATPengembangan(isSaveExit)
{
	
	if(ValidasiSaveDataPL1RKATPengembangan() == 1)
	{
		// if(vAddNewRKAPengembangan === true)
		// {
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/anggaran_module/functionRABNonRutin/saveRAB",
					params: getParamSavePL1RKATPengembangan(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							// ShowPesanInfo('Data RAB Berhasil Disimpan','Information');
							RefreshDataGLRKAPengembangan();
							Ext.getCmp('txtPrioritasPL1RKAPengembangan').setValue(cst.prioritas);
							
							if(cst.status_rab == 't' && cst.status_rab != ""){
								Ext.getCmp('txtaStatusPL1_RKAPengembangan').setValue(" Telah Disahkan ");
							}else{
								Ext.getCmp('txtaStatusPL1_RKAPengembangan').setValue(" Belum Disahkan ");
							}	
							
							RefreshDataGLDaftarPL1(cst.tahun_anggaran_ta,cst.kd_unit_kerja);
							RefreshDataGLDetailPL1(
								Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
								selectUnitKerjaTabPL1RKAPengembangan,
								1,//pengeluaran
								Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').getValue(),
								Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue()
							);	

							Ext.getCmp('txtPrioritasPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtNoProgramPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtNmProgramPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaKegiatanPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaLatarPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaRasionalPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaTujuanPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaMekanismePL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaTargetPL1RKAPengembangan').setValue('');
							Ext.Msg.show
							(
								{
								   title:'Information',
								   msg: 'Data RAB berhasil di simpan',
								   buttons: Ext.MessageBox.OK,
								   fn: function (btn) 
								   {			
									   if (btn =='ok') 
										{
											if(isSaveExit == true){
												winFormEntryRKAPengembangan.close();
											}
											
										} 
								   },
								   icon: Ext.MessageBox.INFO
								}
							);	
						}
						else 
						{
							ShowPesanError('RAB gagal disimpan!', 'Error');
						};
						
						/* var cst = Ext.decode(o.responseText);
						// Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
						// Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);		

						if (cst.success === true) 
						{
							ShowPesanInfo('Data berhasil disimpan.', 'Simpan Data');

							// RefreshDataGLDaftarPL1(getCriteriaGListDaftarPL1());
							// RefreshDataGLDetailPL1(getCriteriaGListDetailPL1());

							if(!isSaveExit)
							{ 
								vAddNewRKAPengembangan = false; 
								Ext.getCmp('cboUnitKerjaTabPL1RKAPengembangan').setReadOnly(true);
								Ext.getCmp('cboTahunEntryPL1RKAPengembangan').setReadOnly(true);
							}
							else { winFormEntryRKAPengembangan.close(); }
							RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di simpan, data tersebut sudah ada', 'Simpan Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
						} */
					}
				}
			);
		// }
		/* else
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSavePL1RKATPengembangan(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
						Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);		

						if (cst.success === true) 
						{
							ShowPesanInfo('Data berhasil di-edit.', 'Edit Data');

							RefreshDataGLDaftarPL1(getCriteriaGListDaftarPL1());
							RefreshDataGLDetailPL1(getCriteriaGListDetailPL1());
							if(isSaveExit)
							{ 
								winFormEntryRKAPengembangan.close(); 
							}
							RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di-edit, data tersebut sudah ada', 'Edit Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di-edit, Error : ' + cst.pesan, 'Edit Data');
						}
					}
				}
			);
		} */
	}
	else
	{
		Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
		Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);		
	}
}

function SaveDataPN1RKATPengembangan(isSaveExit)
{
	
	if(ValidasiSaveDataPN1RKATPengembangan() == 1)
	{
		
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionRABNonRutin/saveRAB",
				params: getParamSavePN1RKATPengembangan(),
				failure: function(o)
				{
					ShowPesanError('Error menyimpan RAB !', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// ShowPesanInfo('Data RAB Berhasil Disimpan','Information');
						RefreshDataGLRKAPengembangan();
						Ext.getCmp('txtPrioritasPN1RKAPengembangan').setValue(cst.prioritas);
						if(cst.status_rab == 't' && cst.status_rab != ""){
							Ext.getCmp('txtaStatusPN1_RKAPengembangan').setValue(" Telah Disahkan ");
						}else{
							Ext.getCmp('txtaStatusPN1_RKAPengembangan').setValue(" Belum Disahkan ");
						}	
						RefreshDataGLDaftarPN1(cst.tahun_anggaran_ta,cst.kd_unit_kerja);
						RefreshDataGLDetailPN1(
							Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
							selectUnitKerjaTabPN1RKAPengembangan,
							2,//penerimaan
							Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(),
							Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue()
						);	
						
						Ext.getCmp('txtPrioritasPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtNoProgramPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtNmProgramPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtaKegiatanPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtaLatarPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtaRasionalPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtaTujuanPN1RKAPengembangan').setValue('');
						Ext.getCmp('txtaMekanismePN1RKAPengembangan').setValue('');
						Ext.getCmp('txtaTargetPN1RKAPengembangan').setValue('');
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Data RAB berhasil di simpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(isSaveExit == true){
											winFormEntryRKAPengembangan.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);	
						
					}
					else 
					{
						ShowPesanError('RAB gagal disimpan!', 'Error');
					};
				}
			}
			
		)
	
		// if(vAddNewRKAPengembangan === true)
		// {
			// Ext.Ajax.request
			// (
				// {
					// url: "./Datapool.mvc/CreateDataObj",
					// params: getParamSavePN1RKATPengembangan(),
					// success: function(o) 
					// {
						// var cst = Ext.decode(o.responseText);
						// Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
						// Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);		

						// if (cst.success === true) 
						// {
							// ShowPesanInfo('Data berhasil disimpan.', 'Simpan Data');
							
							// RefreshDataGLDaftarPN1(getCriteriaGListDaftarPN1());
							// RefreshDataGLDetailPN1(getCriteriaGListDetailPN1());
							
							// if(!isSaveExit)
							// { 
								// vAddNewRKAPengembangan = false; 
								// Ext.getCmp('cboUnitKerjaTabPN1RKAPengembangan').setReadOnly(true);
								// Ext.getCmp('cboTahunEntryPN1RKAPengembangan').setReadOnly(true);
							// }
							// else { winFormEntryRKAPengembangan.close(); }
							
							// RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
						// }
						// else if (cst.success === false && cst.pesan === 0) 
						// {
							// ShowPesanWarning('Data tidak berhasil di simpan, data tersebut sudah ada', 'Simpan Data');
						// }
						// else 
						// {
							// ShowPesanError('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
						// }
					// }
				// }
			// );
		// }
		// else
		// {
			// Ext.Ajax.request
			// (
				// {
					// url: "./Datapool.mvc/CreateDataObj",
					// params: getParamSavePN1RKATPengembangan(),
					// success: function(o) 
					// {
						// var cst = Ext.decode(o.responseText);
						// Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
						// Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);		

						// if (cst.success === true) 
						// {
							// ShowPesanInfo('Data berhasil di-edit.', 'Edit Data');
							
							// RefreshDataGLDaftarPN1(getCriteriaGListDaftarPN1());
							// RefreshDataGLDetailPN1(getCriteriaGListDetailPN1());
							// if(isSaveExit){ winFormEntryRKAPengembangan.close(); }
							// RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
						// }
						// else if (cst.success === false && cst.pesan === 0) 
						// {
							// ShowPesanWarning('Data tidak berhasil di-edit, data tersebut sudah ada', 'Edit Data');
						// }
						// else 
						// {
							// ShowPesanError('Data tidak berhasil di-edit, Error : ' + cst.pesan, 'Edit Data');
						// }
					// }
				// }
			// );
		// }
	}
	else
	{
		Ext.getCmp('btnsave_RKATPengembangan').setDisabled(false);
		Ext.getCmp('btnsaveexit_RKATPengembangan').setDisabled(false);		
	}
}

function ValidasiSaveDataPL1RKATPengembangan()
{
	var valid = 1;

	if(tabDetActive == 1)
	{
		if(dsGLDetailRKAPengembanganList.getCount() <=0)
		{
			ShowPesanWarning("Transaksi harus memiliki detail.", "Simpan Data");
			valid = 0;
		}
		
		var d = getNumber(Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').getValue());
		if(d >= 0 || d == undefined)
		{
			valid = 1;
		}else{
			ShowPesanWarning("Jumlah plafond telah habis atau melebihi plafond yg ada.", "Simpan Data");
			valid = 0;
		}
		
	}else{
		var d = getNumber(Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').getValue());
		if(d >= 0 || d == undefined)
		{
			valid = 1;
		}else{
			ShowPesanWarning("Jumlah plafond telah habis atau melebihi plafond yg ada.", "Simpan Data");
			valid = 0;
		}
	}
	
	//validasi Detail
	if(dsGLDetailRKAPengembanganList.getCount() > 0)
	{
		var m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,jumlah,total_m
		for(var i=0; i<dsGLDetailRKAPengembanganList.getCount(); i++)
		{
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m1_rkatkdet == "") { m1 = 0}
			else{m1 =parseFloat( dsGLDetailRKAPengembanganList.data.items[i].data.m1_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m2_rkatkdet == "") { m2 = 0}
			else{m2 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m2_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m3_rkatkdet == "") { m3 = 0}
			else{m3 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m3_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m4_rkatkdet == "") { m4 = 0}
			else{m4 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m4_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m5_rkatkdet == "") { m5 = 0}
			else{m5 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m5_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m6_rkatkdet == "") { m6 = 0}
			else{m6 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m6_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m7_rkatkdet == "") { m7 = 0}
			else{m7 =parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m7_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m8_rkatkdet == "") { m8 = 0}
			else{m8 =parseFloat( dsGLDetailRKAPengembanganList.data.items[i].data.m8_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m9_rkatkdet == "") { m9 = 0}
			else{m9 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m9_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m10_rkatkdet == "") { m10 = 0}
			else{m10 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m10_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m11_rkatkdet == "") { m11 = 0}
			else{m11 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m11_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m12_rkatkdet == "") { m12 = 0}
			else{m12 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m12_rkatkdet);}
			jumlah = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.jmlh_rkatkdet);
			
			total_m =parseFloat( m1+m2+m3+m4+m5+m6+m7+m8+m9+m10+m11+m12);
			console.log(total_m);
			
			if (total_m != jumlah)
			{
				// ShowPesanWarning("anggaran per bulan melebihi jumlah anggaran " + formatCurrency(JUMLAH)+ " Baris " + (i+1), "Simpan Data");
				ShowPesanWarning("Total anggaran per bulan tidak sesuai, total anggaran per bulan : " + formatCurrency(total_m)+ " Baris " + (i+1), "Simpan Data");
				valid = 0;
			}
			// alert("pl" + dsGLDetailRKAPengembanganList.data.items[i].data.KD_SATUAN_SAT)
			if( dsGLDetailRKAPengembanganList.data.items[i].data.kd_satuan_sat == "")
			{
				ShowPesanWarning("Satuan belum di isi", "Simpan Data");
				valid = 0;
			}
		}
	}
	
	
	//validSisa_RKATPengembangan();
	if(selectUnitKerjaTabPL1RKAPengembangan === undefined || selectUnitKerjaTabPL1RKAPengembangan=='')
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid -= 1;
		// valid = 0;	
	}else if(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid -= 1;
		// valid = 0;	
	}else if(Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue() === "" || Ext.getCmp('txtNmProgramPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Program belum dipilih.", "Simpan Data");
		valid = 0;	
	}else if(Ext.getCmp('txtaKegiatanPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid = 0;				
	}
	
	/*if(Ext.getCmp('txtaKegiatanPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid -= 1;		
	}
	*/
	// validasi saving detail
	
	return valid;
}

function ValidasiSaveDataPN1RKATPengembangan()
{
	var valid = 1;

	// validasi saving detail
	if(tabDetActive == 1)
	{
		if(dsGLDetailRKAPengembanganList.getCount() <=0)
		{
			ShowPesanWarning("Transaksi harus memiliki detail.", "Simpan Data");
			valid = 0;
		}
	}
	
	//validasi Detail
	if(dsGLDetailRKAPengembanganList.getCount() > 0)
	{
		var m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,jumlah,total_m
		for(var i=0; i<dsGLDetailRKAPengembanganList.getCount(); i++)
		{/* 
			M1 = dsGLDetailRKAPengembanganList.data.items[i].data.M1;
			M2 = dsGLDetailRKAPengembanganList.data.items[i].data.M2;
			M3 = dsGLDetailRKAPengembanganList.data.items[i].data.M3;
			M4 = dsGLDetailRKAPengembanganList.data.items[i].data.M4;
			M5 = dsGLDetailRKAPengembanganList.data.items[i].data.M5;
			M6 = dsGLDetailRKAPengembanganList.data.items[i].data.M6;
			M7 = dsGLDetailRKAPengembanganList.data.items[i].data.M7;
			M8 = dsGLDetailRKAPengembanganList.data.items[i].data.M8;
			M9 = dsGLDetailRKAPengembanganList.data.items[i].data.M9;
			M10 = dsGLDetailRKAPengembanganList.data.items[i].data.M10;
			M11 = dsGLDetailRKAPengembanganList.data.items[i].data.M11;
			M12 = dsGLDetailRKAPengembanganList.data.items[i].data.M12;
			JUMLAH = dsGLDetailRKAPengembanganList.data.items[i].data.JUMLAH; */
			
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m1_rkatkdet == "") { m1 = 0}
			else{m1 =parseFloat( dsGLDetailRKAPengembanganList.data.items[i].data.m1_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m2_rkatkdet == "") { m2 = 0}
			else{m2 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m2_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m3_rkatkdet == "") { m3 = 0}
			else{m3 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m3_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m4_rkatkdet == "") { m4 = 0}
			else{m4 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m4_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m5_rkatkdet == "") { m5 = 0}
			else{m5 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m5_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m6_rkatkdet == "") { m6 = 0}
			else{m6 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m6_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m7_rkatkdet == "") { m7 = 0}
			else{m7 =parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m7_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m8_rkatkdet == "") { m8 = 0}
			else{m8 =parseFloat( dsGLDetailRKAPengembanganList.data.items[i].data.m8_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m9_rkatkdet == "") { m9 = 0}
			else{m9 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m9_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m10_rkatkdet == "") { m10 = 0}
			else{m10 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m10_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m11_rkatkdet == "") { m11 = 0}
			else{m11 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m11_rkatkdet);}
			if (dsGLDetailRKAPengembanganList.data.items[i].data.m12_rkatkdet == "") { m12 = 0}
			else{m12 = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.m12_rkatkdet);}
			jumlah = parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.jmlh_rkatkdet);
			
			total_m =parseFloat( m1+m2+m3+m4+m5+m6+m7+m8+m9+m10+m11+m12);
			
			total_m = (m1+m2)+(m3+m4)+(m5+m6)+(m7+m8)+(m9+m10)+(m11+m12);
			if (total_m > jumlah){
				ShowPesanWarning("anggaran per bulan melebihi jumlah anggaran " + formatCurrency(jumlah) + " Baris " + (i+1), "Simpan Data");
				valid = 0;
			}
			// alert("pl" + dsGLDetailRKAPengembanganList.data.items[i].data.KD_SATUAN_SAT)
			if( dsGLDetailRKAPengembanganList.data.items[i].data.kd_satuan_sat == ""){
				ShowPesanWarning("Satuan belum di isi", "Simpan Data");
				valid = 0;
			}
		}
	}

	if(selectUnitKerjaTabPN1RKAPengembangan === undefined)
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid = 0;
	}else if(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid = 0;
	}else if(Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue() === "" || Ext.getCmp('txtNmProgramPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Program belum dipilih.", "Simpan Data");
		valid = 0;	
	}else if(Ext.getCmp('txtaKegiatanPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid = 0;				
	}
	

	
	
	return valid;
}

function getParamSavePL1RKATPengembangan()
{
	var revisidata;
	if(Ext.getCmp('txtCheckPL1RKAPengembangan').getValue() == true){
		revisidata = 1;
	}else{
		revisidata = 0;
	}
	
	if( disahkan == '' || disahkan == undefined ){
		disahkan = 'f';
	}
	
	var params = {
		
		/* ACC_RKAT */
		thn_anggaran 		:	Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
		kd_unit_kerja 		:	selectUnitKerjaTabPL1RKAPengembangan,
		disahkan_rka		:	disahkan,
		jumlah_rkatk		:	getNumber(Ext.getCmp('txtPengembanganPL1RKAPengembangan').getValue()),
	
		/* ACC_RKATK */
		noprog				: 	Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
		kd_jns_rkat_jrka	:	1, //pengeluaran
		prioritas			:	Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
		kegiatan_rkatk		:	Ext.getCmp('txtaKegiatanPL1RKAPengembangan').getValue(),
		latar				: 	Ext.getCmp('txtaLatarPL1RKAPengembangan').getValue(),
		rasional			: 	Ext.getCmp('txtaRasionalPL1RKAPengembangan').getValue(),
		tujuan				: 	Ext.getCmp('txtaTujuanPL1RKAPengembangan').getValue(),
		mekanisme			: 	Ext.getCmp('txtaMekanismePL1RKAPengembangan').getValue(),
		target				: 	Ext.getCmp('txtaTargetPL1RKAPengembangan').getValue(),
		revisi				: 	revisidata,
		
	};
	
	/* ACC_RKATK_DET */
	params['jumlah_baris']=dsGLDetailRKAPengembanganList.getCount();
	for(var i = 0 ; i < dsGLDetailRKAPengembanganList.getCount();i++)
	{
		
		params['urut_rkatkdet-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.urut_rkatkdet
		params['kd_satuan_sat-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.kd_satuan_sat
		params['account-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.account
		params['qty-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.kuantitas_rkatkdet
		params['biaya_satuan-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.biaya_satuan_rkatkdet
		params['jumlah-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.jmlh_rkatkdet
		params['m1-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m1_rkatkdet
		params['m2-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m2_rkatkdet
		params['m3-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m3_rkatkdet
		params['m4-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m4_rkatkdet
		params['m5-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m5_rkatkdet
		params['m6-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m6_rkatkdet
		params['m7-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m7_rkatkdet
		params['m8-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m8_rkatkdet
		params['m9-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m9_rkatkdet
		params['m10-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m10_rkatkdet
		params['m11-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m11_rkatkdet
		params['m12-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m12_rkatkdet
		params['deskripsi_rkatkdet-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.deskripsi_rkatkdet
		
	}
	
	/* var params = {
		Table: 'viRKAT1',
		rkat: 'PL1',
		isDetail: (tabDetActive == 1) ? true : false,
		unit: selectUnitKerjaTabPL1RKAPengembangan,
		tahun: Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
		jnsrkat: 1,
		prior: Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
		priorasal: (intPriorAsal !== undefined) ? intPriorAsal : Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
		noprog: Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
		progasal: (strProgAsal !== undefined) ? strProgAsal : Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
		keg: Ext.getCmp('txtaKegiatanPL1RKAPengembangan').getValue(),
		latar: Ext.getCmp('txtaLatarPL1RKAPengembangan').getValue(),
		rasional: Ext.getCmp('txtaRasionalPL1RKAPengembangan').getValue(),
		tujuan: Ext.getCmp('txtaTujuanPL1RKAPengembangan').getValue(),
		approve: disahkan,
		mekanisme: Ext.getCmp('txtaMekanismePL1RKAPengembangan').getValue(),
		indikator: Ext.getCmp('txtaTargetPL1RKAPengembangan').getValue(),
		//jumlah:Ext.get('txtTotBiayaKegDaftarRKAPengembangan').getValue(),
		sisaRKATPengembangan:Ext.get('txtPengembanganPL1RKAPengembangan').getValue(),
		sisarkatkutin:Ext.get('txtRutinPL1RKAPengembangan').getValue(),
		jumlahAnggaran:Ext.get('txtJumPlafondPL1RKAPengembangan').getValue(),
		revisi:revisidata,
		lists: getListDetailRKATPengembangan(),
		nrow: dsGLDetailRKAPengembanganList.getCount(),
		nfield: 25
	}; */
	return params;
}

function getParamSavePN1RKATPengembangan()
{
	var revisidata;
	if(Ext.getCmp('txtCheckPN1RKAPengembangan').getValue() == true){
		revisidata = 1;
	}else{
		revisidata = 0;
	}
	
	if( disahkan == '' || disahkan == undefined ){
		disahkan = 'f';
	}
	
		var params = {
		/* ACC_RKAT */
		thn_anggaran 		:	Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
		kd_unit_kerja 		:	selectUnitKerjaTabPN1RKAPengembangan,
		disahkan_rka		:	disahkan,
		jumlah_rkatk		:	'',
		
		/* ACC_rkatk */
		noprog				: 	Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(),
		kd_jns_rkat_jrka	:	2, //penerimaan
		prioritas			:	Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue(),
		kegiatan_rkatk		:	Ext.getCmp('txtaKegiatanPN1RKAPengembangan').getValue(),
		latar				: 	Ext.getCmp('txtaLatarPN1RKAPengembangan').getValue(),
		rasional			: 	Ext.getCmp('txtaRasionalPN1RKAPengembangan').getValue(),
		tujuan				: 	Ext.getCmp('txtaTujuanPN1RKAPengembangan').getValue(),
		mekanisme			: 	Ext.getCmp('txtaMekanismePN1RKAPengembangan').getValue(),
		target				: 	Ext.getCmp('txtaTargetPN1RKAPengembangan').getValue(),
		revisi				: 	revisidata,
		
	};
	
	/* ACC_rkatk_DET */
	params['jumlah_baris']=dsGLDetailRKAPengembanganList.getCount();
	for(var i = 0 ; i < dsGLDetailRKAPengembanganList.getCount();i++)
	{
		
		params['urut_rkatkdet-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.urut_rkatkdet
		params['kd_satuan_sat-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.kd_satuan_sat
		params['account-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.account
		params['qty-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.kuantitas_rkatkdet
		params['biaya_satuan-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.biaya_satuan_rkatkdet
		params['jumlah-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.jmlh_rkatkdet
		params['m1-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m1_rkatkdet
		params['m2-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m2_rkatkdet
		params['m3-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m3_rkatkdet
		params['m4-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m4_rkatkdet
		params['m5-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m5_rkatkdet
		params['m6-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m6_rkatkdet
		params['m7-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m7_rkatkdet
		params['m8-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m8_rkatkdet
		params['m9-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m9_rkatkdet
		params['m10-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m10_rkatkdet
		params['m11-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m11_rkatkdet
		params['m12-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.m12_rkatkdet
		params['deskripsi_rkatkdet-'+i]=dsGLDetailRKAPengembanganList.data.items[i].data.deskripsi_rkatkdet
		
	}
	/* var params = {
		Table: 'viRKAT1',
		rkat: 'PN1',
		isDetail: (tabDetActive == 1) ? true : false,
		unit: selectUnitKerjaTabPN1RKAPengembangan,
		tahun: Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
		jnsrkat: 2,
		prior: Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue(),
		priorasal: intPriorAsal,		
		noprog: Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(),
		progasal: strProgAsal,
		keg: Ext.getCmp('txtaKegiatanPN1RKAPengembangan').getValue(),
		latar: Ext.getCmp('txtaLatarPN1RKAPengembangan').getValue(),
		rasional: Ext.getCmp('txtaRasionalPN1RKAPengembangan').getValue(),
		mekanisme: Ext.getCmp('txtaMekanismePN1RKAPengembangan').getValue(),
		indikator: Ext.getCmp('txtaTargetPN1RKAPengembangan').getValue(),
		tujuan: Ext.getCmp('txtaTujuanPN1RKAPengembangan').getValue(),
		approve: disahkan,
		sisaRKATPengembangan:Ext.get('txtTotBiayaKegDaftarRKAPengembangan').getValue(),
		sisarkatkutin:Ext.get('txtRutinPN1RKAPengembangan').getValue(),
		jumlahAnggaran:Ext.get('txtJumPlafondPN1RKAPengembangan').getValue(),
		revisi:revisidata,
		lists: getListDetailRKATPengembangan(),
		nrow: dsGLDetailRKAPengembanganList.getCount(),
		nfield: 25
	}; */
	
	return params;
}

function getListDetailRKATPengembangan()
{
	var x = "";
	var y = "";
	var	z = "@@##$$@@";
	
	if(dsGLDetailRKAPengembanganList.getCount() > 0)
	{
		for(var i=0; i<dsGLDetailRKAPengembanganList.getCount(); i++)
		{
			y = "col_line=" + dsGLDetailRKAPengembanganList.data.items[i].data.LINE;
			y += z + "col_unit=" + dsGLDetailRKAPengembanganList.data.items[i].data.KD_UNIT_KERJA;
			y += z + "col_tahun=" + dsGLDetailRKAPengembanganList.data.items[i].data.TAHUN_ANGGARAN_TA;
			y += z + "col_noprog=" + dsGLDetailRKAPengembanganList.data.items[i].data.NO_PROGRAM_PROG;
			y += z + "col_prior=" + dsGLDetailRKAPengembanganList.data.items[i].data.PRIORITAS;
			y += z + "col_jnsrkat=" + dsGLDetailRKAPengembanganList.data.items[i].data.KD_JNS_RKAT_JRKA;
			y += z + "col_satuan=" + dsGLDetailRKAPengembanganList.data.items[i].data.KD_SATUAN_SAT;
			y += z + "col_akun=" + dsGLDetailRKAPengembanganList.data.items[i].data.ACCOUNT;
			y += z + "col_qty=" + dsGLDetailRKAPengembanganList.data.items[i].data.QTY;
			y += z + "col_biaya=" + dsGLDetailRKAPengembanganList.data.items[i].data.BIAYA_SATUAN;
			y += z + "col_jumlah=" + dsGLDetailRKAPengembanganList.data.items[i].data.JUMLAH;
			y += z + "col_m1=" + dsGLDetailRKAPengembanganList.data.items[i].data.M1;
			y += z + "col_m2=" + dsGLDetailRKAPengembanganList.data.items[i].data.M2;
			y += z + "col_m3=" + dsGLDetailRKAPengembanganList.data.items[i].data.M3;
			y += z + "col_m4=" + dsGLDetailRKAPengembanganList.data.items[i].data.M4;
			y += z + "col_m5=" + dsGLDetailRKAPengembanganList.data.items[i].data.M5;
			y += z + "col_m6=" + dsGLDetailRKAPengembanganList.data.items[i].data.M6;
			y += z + "col_m7=" + dsGLDetailRKAPengembanganList.data.items[i].data.M7;
			y += z + "col_m8=" + dsGLDetailRKAPengembanganList.data.items[i].data.M8;
			y += z + "col_m9=" + dsGLDetailRKAPengembanganList.data.items[i].data.M9;
			y += z + "col_m10=" + dsGLDetailRKAPengembanganList.data.items[i].data.M10;
			y += z + "col_m11=" + dsGLDetailRKAPengembanganList.data.items[i].data.M11;
			y += z + "col_m12=" + dsGLDetailRKAPengembanganList.data.items[i].data.M12;
			y += z + "col_deskripsi=" + dsGLDetailRKAPengembanganList.data.items[i].data.DESKRIPSI_RKATKDET;
			y += z + "col_acountemp=" + dsGLDetailRKAPengembanganList.data.items[i].data.ACCOUNT_TEMP;
			
			if(i == dsGLDetailRKAPengembanganList.getCount() - 1)
			{
				x += y;
			}
			else
			{
				x += y + "##[[]]##";
			}
		}
	}
	
	return x;
}

function HapusBarisDetailRKAPengembangan()
{
	var line = gridListDetailRKAPengembangan.getSelectionModel().selection.cell[0];
	var o = dsGLDetailRKAPengembanganList.getRange()[line].data;
	console.log(dsGLDetailRKAPengembanganList.getRange()[line].data.urut_rkatkdet);
	console.log(o);
	var prioritas_rkatk='';
	if(o.kd_jns_rkat_jrka == 1){
		prioritas_rkatk = Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue();
	}else{
		prioritas_rkatk = Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue();
	}
	Ext.Msg.confirm('Warning', 'Apakah data detail RAB  ini akan dihapus?', function(button){
		if (button == 'yes'){
			if( dsGLDetailRKAPengembanganList.getRange()[line].data.urut_rkatkdet !== ''){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRABNonRutin/hapusDetailRAB",
						params:{
							
							tahun_anggaran_ta	:o.tahun_anggaran_ta,
							kd_unit_kerja		:o.kd_unit_kerja,
							kd_jns_rkat_jrka	:o.kd_jns_rkat_jrka,
							prioritas_rkatk		:prioritas_rkatk,
							urut_rkatkdet		:o.urut_rkatkdet,
							jml_rkatk_det		:o.jmlh_rkatkdet,
							no_program_prog		:o.no_program_prog
						} ,
						failure: function(o)
						{
							ShowPesanError('Error menghapus detail RAB!', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsGLDetailRKAPengembanganList.removeAt(line);
								gridListDetailRKAPengembangan.getView().refresh();
								ShowPesanInfo('Data detail RAB berhasil dihapus','Information');
								RefreshDataGLDaftarPL1(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),selectUnitKerjaTabPL1RKAPengembangan); 
								RefreshDataGLDetailPL1(
									Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
									selectUnitKerjaTabPL1RKAPengembangan,
									1,//pengeluaran
									Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').getValue(),
									Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue()
								);	
								// getTotalBiayaTabDetailRKAT1();
							}
							else 
							{
								ShowPesanError('Gagal menghapus detail RAB!', 'Error');
							};
						}
					}
					
				)
			}else{
				dsGLDetailRKAPengembanganList.removeAt(line);
				gridListDetailRKAPengembangan.getView().refresh();
				ShowPesanInfo('Data detail RAB berhasil dihapus','Information');
				getTotalBiayaTabDetailRKAT1();
				RefreshDataGLDaftarPL1(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),selectUnitKerjaTabPL2RKARutin); 
			}
		} 
		
	});
	
	
	/* var fn_HapusBarisDetail = function(button)
	{
		if(button == "yes")
		{
			getTotalPengembangan_RKATPengembangan(true);
			if(dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.LINE === 0)
			{
				dsGLDetailRKAPengembanganList.removeAt(CurrentGLDetailRKAPengembangan.row);				
				var msg = "Hapus baris ke-" + (CurrentGLDetailRKAPengembangan.row + 1) + " berhasil dihapus.";
				ShowPesanInfo(msg,"Hapus Baris");
			}
			else
			{
				Ext.Ajax.request
				(
					{
						url: "./Datapool.mvc/DeleteDataObj",
						params:  getParamHapusDetailRKATPengembangan(), 
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsGLDetailRKAPengembanganList.removeAt(CurrentGLDetailRKAPengembangan.row);								
								var msg = "Hapus baris ke-" + (CurrentGLDetailRKAPengembangan.row + 1) + " berhasil dihapus.";
								ShowPesanInfo(msg, "Hapus Baris");
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning('Data tidak berhasil dihapus, data tersebut belum ada','Hapus Data');
							}
							else 
							{
								ShowPesanError('Data tidak berhasil dihapus','Hapus Data');
							};
						}
					}
				);
			}
			if(tabActive == "PL1")
			{
				// RefreshDataGLDaftarPL1(getCriteriaGListDaftarPL1());
				// RefreshDataGLDetailPL1(getCriteriaGListDetailPL1());
			}
			else
			{
				// RefreshDataGLDaftarPN1(getCriteriaGListDaftarPN1());
				// RefreshDataGLDetailPN1(getCriteriaGListDetailPN1());
			}
		}
	}
	
	if(dsGLDetailRKAPengembanganList !== undefined || dsGLDetailRKAPengembanganList.getCount() > 1)
	{
		var msg = "Hapus baris ke-" + (CurrentGLDetailRKAPengembangan.row + 1) + " dengan no account: " + CurrentGLDetailRKAPengembangan.data.ACCOUNT;
		ShowPesanConfirm(msg, "Hapus Baris", fn_HapusBarisDetail);
	}
	else if(dsGLDetailRKAPengembanganList.getCount() == 1)
	{
		var msg = "Daftar detail hanya tersisa 1, hapus seluruh daftar?";
		ShowPesanConfirm(msg, "Hapus Baris", function(button)
			{
				if(button == "yes")
				{
					if(tabActive == "PL1")
					{
						HapusDataPL1RKATPengembangan();						
					}
					else
					{
						HapusDataPN1RKATPengembangan();
					}
				}
			}
		);
	} */
}

function getParamHapusDetailRKATPengembangan()
{
	var params = {
		Table: 'viRKAT1',
		isRow: true,
		line: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.LINE,
		unit: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.KD_UNIT_KERJA,
		tahun: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.TAHUN_ANGGARAN_TA,
		noprog: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.NO_PROGRAM_PROG,
		prior: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.PRIORITAS,
		jnsrkat: (tabActive == "PL1") ? 1 : 2,
		sisaRKATPengembangan:Ext.get('txtPengembanganPL1RKAPengembangan').getValue(),
		akun: dsGLDetailRKAPengembanganList.data.items[CurrentGLDetailRKAPengembangan.row].data.ACCOUNT
	};
	
	return params;
}

function HapusDataPL1RKATPengembangan()
{
	var jumlah= rowSelectedDaftarRKAPengembangan.data.jumlah;
	Ext.Msg.confirm('Warning', 'Apakah data pengeluaran pada RAB  ini akan dihapus?', function(button){
		if (button == 'yes'){
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/anggaran_module/functionRABNonRutin/hapusRAB",
					params:{
						
						tahun_anggaran_ta	:Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
						kd_unit_kerja		:selectUnitKerjaTabPL1RKAPengembangan,
						kd_jns_rkat_jrka	:1,
						prioritas_rkatk		:Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
						no_program_prog		:Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
						jumlah				: jumlah
					} ,
					failure: function(o)
					{
						ShowPesanError('Error menghapus detail RAB!', 'Error');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfo('Data  berhasil dihapus','Information');
							//refresh grid daftar
							RefreshDataGLDaftarPL1(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),selectUnitKerjaTabPL1RKAPengembangan);
							Ext.getCmp('txtPrioritasPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtNoProgramPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtNoProgAliasPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtNmProgramPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaKegiatanPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaLatarPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaRasionalPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaTujuanPL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaMekanismePL1RKAPengembangan').setValue('');
							Ext.getCmp('txtaTargetPL1RKAPengembangan').setValue('');
						}
						else 
						{
							ShowPesanError('Gagal menghapus detail RAB!', 'Error');
						};
					}
				}
				
			)
		} 
		
	});
	/* var fn_HapusDataPL1 = function(button)
	{
		if(button == "yes")
		{
			if(ValidasiDeleteDataPL1RKATPengembangan() === 1)
			{
				Ext.Ajax.request
				(
					{
						url: "./Datapool.mvc/DeleteDataObj",
						params: getParamDeletePL1RKATPengembangan(),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success) 
							{					
								ShowPesanInfo('Data berhasil dihapus','Hapus Data');
								AddNewRKAPengembangan(tabActive);
								RefreshDataGLDaftarPL1(getCriteriaGListDaftarPL1());
								RefreshDataGLDetailPL1(getCriteriaGListDetailPL1());
								
								RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
							}
							else if (cst.success && (cst.pesan === 0))
							{
								ShowPesanWarning('Data tidak berhasil dihapus, data tersebut belum ada','Hapus Data');
							}
							else 
							{
								ShowPesanError('Data tidak berhasil dihapus','Hapus Data');
							}
						}
					}
				);
			}
		}
	}
		
	ShowPesanConfirm("Hapus data RAB?", "Hapus Data", fn_HapusDataPL1); */
}

function getParamDeletePL1RKATPengembangan()
{
	var params = {
		Table: 'viRKAT1',
		isrow: false,
		unit: selectUnitKerjaTabPL1RKAPengembangan,
		tahun: Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue(),
		jnsrkat: 1, // 1 untuk pengeluaran
		prior: Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue(),
		noprog: Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue(),
		keg: Ext.getCmp('txtaKegiatanPL1RKAPengembangan').getValue(),
		sisaRKATPengembangan:Ext.get('txtPengembanganPL1RKAPengembangan').getValue(),
		lists: getListDetailRKATPengembangan(),
		nrow: dsGLDetailRKAPengembanganList.getCount(),
		nfield: 25
	};
	
	return params;
}

function ValidasiDeleteDataPL1RKATPengembangan()
{
	var valid = 1;
	
	if(selectUnitKerjaTabPL1RKAPengembangan === undefined)
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('cboTahunEntryPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtPrioritasPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Prioritas belum ditentukan.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtNoProgramPL1RKAPengembangan').getValue() === "" || Ext.getCmp('txtNmProgramPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Program belum dipilih.", "Simpan Data");
		valid -= 1;	
	}
	
	if(Ext.getCmp('txtaKegiatanPL1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid -= 1;		
	}
	
	return valid;
}

function HapusDataPN1RKATPengembangan()
{
	if(ValidasiDeleteDataPN1RKATPengembangan() === 1)
	{
		Ext.Msg.confirm('Warning', 'Apakah data penerimaan pada RAB  ini akan dihapus?', function(button){
			if (button == 'yes'){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRABNonRutin/hapusRAB",
						params:{
							
							tahun_anggaran_ta	:Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
							kd_unit_kerja		:selectUnitKerjaTabPN1RKAPengembangan,
							kd_jns_rkat_jrka	:2,
							prioritas_rkatk		:Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue(),
							no_program_prog		:Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(),
							jumlah 				:0
						} ,
						failure: function(o)
						{
							ShowPesanError('Error menghapus detail RAB!', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								ShowPesanInfo('Data  berhasil dihapus','Information');
								RefreshDataGLDaftarPN1(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),selectUnitKerjaTabPN1RKAPengembangan);
								Ext.getCmp('txtPrioritasPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtNoProgramPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtNmProgramPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtaKegiatanPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtaLatarPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtaRasionalPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtaTujuanPN1RKAPengembangan').setValue('');
								Ext.getCmp('txtaMekanismePN1RKAPengembangan').setValue('');
								Ext.getCmp('txtaTargetPN1RKAPengembangan').setValue('');
							}
							else 
							{
								ShowPesanError('Gagal menghapus detail RAB!', 'Error');
							};
						}
					}
					
				)
			} 
			
		});
		/* Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params: getParamDeletePN1RKATPengembangan(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success) 
					{					
						ShowPesanInfo('Data berhasil dihapus','Hapus Data');
						AddNewRKAPengembangan(tabActive);
						RefreshDataGLDaftarPN1(getCriteriaGListDaftarPN1());
						RefreshDataGLDetailPN1(getCriteriaGListDetailPN1());
						
						RefreshDataGLRKAPengembangan(getCriteriaGListRKAPengembangan());
					}
					else if (cst.success && (cst.pesan === 0))
					{
						ShowPesanWarning('Data tidak berhasil dihapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError('Data tidak berhasil dihapus','Hapus Data');
					}
				}
			}
		); */
	}
}

function getParamDeletePN1RKATPengembangan()
{
	var params = {
		Table: 'viRKAT1',
		isrow: false,
		unit: selectUnitKerjaTabPN1RKAPengembangan,
		tahun: Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue(),
		jnsrkat: 2, // 2 untuk penerimaan
		prior: Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue(),
		noprog: Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue(),
		keg: Ext.getCmp('txtaKegiatanPN1RKAPengembangan').getValue(),
		lists: getListDetailRKATPengembangan(),
		nrow: dsGLDetailRKAPengembanganList.getCount(),
		nfield: 24
	};
	
	return params;
}

function ValidasiDeleteDataPN1RKATPengembangan()
{
	var valid = 1;
	if(selectUnitKerjaTabPN1RKAPengembangan === undefined)
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('cboTahunEntryPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtPrioritasPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Prioritas belum ditentukan.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtNoProgramPN1RKAPengembangan').getValue() === "" || Ext.getCmp('txtNmProgramPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Program belum dipilih.", "Simpan Data");
		valid -= 1;	
	}
	
	if(Ext.getCmp('txtaKegiatanPN1RKAPengembangan').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid -= 1;		
	}
	
	return valid;
}
/* AKUMULASI */
/* function validSisa_RKATPengembangan(){

	//var b = getTotalBiayaTabDetailRKAT1();
	var c = getNumber(Ext.getCmp('txtJumPlafondPL1RKAPengembangan').getValue()) - getNumber(Ext.getCmp('txtTotBiayaKegDaftarRKAPengembangan').getValue()); // jumlah entry detail baru
	if(c < 0)
	{
		ShowPesanWarning("Jumlah detail melebihi sisa plafond yang ada.", "Simpan Data");
		valid = 0;
	}
	return valid;
} */

/* function validSisa_RKATPengembangan(){
	
	var b = getTotalBiayaTabDetailRKAT1();
	var c = b - getNumber(Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').getValue());
	if(c < 0)
	{
		ShowPesanWarning("Jumlah detail melebihi sisa plafond yang ada.", "Simpan Data");
		valid = 0;
	}
}
 */
 
//kalkulasi effect blur di detail
function getTotalPengembangan_RKATPengembangan(nilaiDelete)
{
	
	var plafon = getNumber(Ext.get('txtJumPlafondPL1RKAPengembangan').getValue());
	//var detPengembangan1 = Ext.get('txtTotBiayaKegDetailRKAPengembangan').getValue();
	var detPengembangan2 = getNumber(Ext.get('txtTotBiayaKegDetailRKAPengembangan').getValue());
	var rutin = getNumber(Ext.get('txtRutinPL1RKAPengembangan').getValue());
	var daf = getNumber(Ext.get('txtTotBiayaKegDaftarRKAPengembangan').getValue());
	
	if(nilaiDelete == false){
		//set value anggaran		
		var totalPengembangan = jumlah - detPengembangan2;
		var anggaranPengembangan = daf - totalPengembangan;
		Ext.get('txtPengembanganPL1RKAPengembangan').dom.value = formatCurrency(anggaranPengembangan);
		
		var total = anggaranPengembangan + rutin;
		var sisaAnggaran = plafon - total;
		Ext.get('txtSisaPlafondPL1RKAPengembangan').dom.value = formatCurrency(sisaAnggaran);
	}else{
		//set value anggaran
		var anggaranPengembangan = daf - danaRKAPengembangan;
		Ext.get('txtPengembanganPL1RKAPengembangan').dom.value = formatCurrency(anggaranPengembangan);
		
		var total = anggaranPengembangan + rutin;
		var sisaAnggaran = plafon - total;
		Ext.get('txtSisaPlafondPL1RKAPengembangan').dom.value = formatCurrency(sisaAnggaran);
	}
	
}

//Looping kalkulasi
function getTotalBiayaTabDaftarRKAT1() // Total Daftar
{
	var total = 0;
	if(dsGLDaftarRKAPengembanganList.getCount() > 0)
	{
		console.log(dsGLDaftarRKAPengembanganList.data);
		for(var i = 0;i < dsGLDaftarRKAPengembanganList.getCount();i++)
		{
			total = total + parseFloat(dsGLDaftarRKAPengembanganList.data.items[i].data.jumlah);
			
		}
	}
	
	console.log(total);
	Ext.getCmp('txtTotBiayaKegDaftarRKAPengembangan').setValue(formatCurrency(total));
	Ext.getCmp('txtPengembanganPL1RKAPengembangan').setValue(formatCurrency(total));

}

function getTotalBiayaTabDetailRKAT1() //Total Detail
{
	var total = 0;
	if(dsGLDetailRKAPengembanganList.getCount() > 0)
	{
		for(var i = 0;i < dsGLDetailRKAPengembanganList.getCount();i++)
		{
			total = total + parseFloat(dsGLDetailRKAPengembanganList.data.items[i].data.jmlh_rkatkdet);
			console.log(total);
		}
	}
			console.log(total);
	
	Ext.getCmp('txtTotBiayaKegDetailRKAPengembangan').setValue(formatCurrency(total));
}

function getSisaDaftar_RKATPengembangan() // Total Daftar
{
	var total = 0;
	if(dsGLDaftarRKAPengembanganList.getCount() > 0)
	{
		for(var i = 0;i < dsGLDaftarRKAPengembanganList.getCount();i++)
		{
			total = total + parseFloat(dsGLDaftarRKAPengembanganList.data.items[i].data.jumlah);
		}
	}
	
	var c = getNumber(Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').getValue())- total;
	Ext.getCmp('txtSisaPlafondPL1RKAPengembangan').setValue(formatCurrency(c))
}

// FORM LOOKUP

var winFormLookupKUPPASingle;
var dsGLLookupKUPPASingle;
var selectedrowGLLookupKUPPASingle;

function FormLookupKUPPASingle(txtNoProg_id,txtNmProg_id,criteria,txtaliasNoProg_id)
{	
	winFormLookupKUPPASingle = new Ext.Window
	(		
		{
		    id: 'winFormLookupKUPPASingle',
		    title: 'Lookup',
		    closeAction: 'destroy',
		    closable: true,
		    width: 600,
		    height: 450,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookupKUPPASingle(txtNoProg_id,txtNmProg_id,criteria,txtaliasNoProg_id)]			
		}
	);
	
	winFormLookupKUPPASingle.show();
}

function getItemFormLookupKUPPASingle(txtNoProg_id,txtNmProg_id,criteria,txtaliasNoProg_id)
{
	var frmListLookupKUPPASingle = new Ext.Panel
	(
		{
			id: 'frmListLookupKUPPASingle',	    
			autoScroll: true,
			items: 
			[
				getGridListKUPPA(criteria,txtNoProg_id,txtNmProg_id,txtaliasNoProg_id),				
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '4px', 'margin-top': '3px' },
				    anchor: '96.5%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[
						{
						    xtype: 'button',
							width: 70,
						    text: 'Ok',
							handler: function()
							{
								if(selectedrowGLLookupKUPPASingle !== undefined)
								{									
									
									Ext.getCmp(txtNoProg_id).setValue(selectedrowGLLookupKUPPASingle.data.no_program_prog);
									Ext.getCmp(txtNmProg_id).setValue(selectedrowGLLookupKUPPASingle.data.nama_program_prog);		
									//Ext.getCmp(txtaliasNoProg_id).setValue(selectedrowGLLookupKUPPASingle.data.ALIAS_NOPROGRAM);
									Ext.getCmp(txtaliasNoProg_id).setValue(selectedrowGLLookupKUPPASingle.data.no_program_prog);
									winFormLookupKUPPASingle.close();									
								}									
								else
								{
									Ext.Msg.alert('Lookup','Program belum dipilih.');
								}
							}
						},
						{
						    xtype: 'button',
							width: 70,
						    text: 'Cancel',
							handler: function()
							{
								selectedrowGLLookupKUPPASingle = undefined;
								winFormLookupKUPPASingle.close();
							}
						}
					]
				}
			],
			tbar:
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'tbtext',
						text: 'Program: '
					},
					{
						xtype: 'textfield',
						id: 'txtNmKUPPAFilterLookupKUPPASingle'					
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupKUPPASingle',
						iconCls: 'refresh',
						text: 'Tampilkan',
						handler: function()
						{
							if(Ext.getCmp('txtNmKUPPAFilterLookupKUPPASingle').getValue() != ""){
								RefreshDataLookupKUPPASingle(getCriteriaLookupKUPPASingle(criteria));
							}else{
								RefreshDataLookupKUPPASingle(criteria);
							}
						}
					}
				]
			}
		}
	);
	
	return frmListLookupKUPPASingle;
}

function getGridListKUPPA(criteria,txtNoProg_id,txtNmProg_id,txtaliasNoProg_id)
{
	var mCriteria = '';
	var _fields = ['no_program_prog','nama_program_prog','alias_noprogram'];
	dsGLLookupKUPPASingle = new WebApp.DataStore({ fields: _fields });
	
	/*
	if(criteria.indexOf("##@@##") != -1)
	{
		mCriteria = criteria;
		}
	else
	{
		
	}*/
	RefreshDataLookupKUPPASingle(criteria);
	
	var gridListLookupKUPPASingle = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookupKUPPASingle',
			store: dsGLLookupKUPPASingle,
			height:353,
			anchor: '100% 90%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			autoScroll: true,
			border: false,
			viewConfig : 
			{
				forceFit: true
			},		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							selectedrowGLLookupKUPPASingle = dsGLLookupKUPPASingle.getAt(row);
						}						
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),					
					{ 
						id: 'colNoKUPPAGLLookupKUPPASingle',
						header: 'No. Program',
						// dataIndex: 'NOPROGRAM',
						dataIndex: 'no_program_prog',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colNmKUPPAGLLookupKUPPASingle',
						header: 'Program',
						dataIndex: 'nama_program_prog',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					}
				]
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					if(selectedrowGLLookupKUPPASingle !== undefined)
					{									
						
						Ext.getCmp(txtNoProg_id).setValue(selectedrowGLLookupKUPPASingle.data.no_program_prog);
						Ext.getCmp(txtNmProg_id).setValue(selectedrowGLLookupKUPPASingle.data.nama_program_prog);		
						//Ext.getCmp(txtaliasNoProg_id).setValue(selectedrowGLLookupKUPPASingle.data.ALIAS_NOPROGRAM);
						Ext.getCmp(txtaliasNoProg_id).setValue(selectedrowGLLookupKUPPASingle.data.no_program_prog);
						winFormLookupKUPPASingle.close();									
					}									
					else
					{
						Ext.Msg.alert('Lookup','Program belum dipilih.');
					}
					
				}
				// End Function # --------------
			},
		}
	);
		
	return gridListLookupKUPPASingle;
}

function getCriteriaLookupKUPPASingle(criteria)
{
	var strKriteria = '';
	
	if(Ext.getCmp('txtNmKUPPAFilterLookupKUPPASingle').getValue() != "")
	{
		strKriteria += "nmprog=" + Ext.getCmp('txtNmKUPPAFilterLookupKUPPASingle').getValue();
		strKriteria += "&" + criteria;
	}
	return strKriteria;
}

function RefreshDataLookupKUPPASingle(criteria)
{	

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRABNonRutin/getProgram",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data program!', 'Error');
		},	
		success: function(o) 
		{   
			dsGLLookupKUPPASingle.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLLookupKUPPASingle.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLLookupKUPPASingle.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data program', 'Error');
			};
		}
	});
	/* dsGLLookupKUPPASingle.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viGListEntryProgUK',
			    param: criteria
			}
		}
	);
	
	return dsGLLookupKUPPASingle; */
}

// form lookup rkat/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookAcc;
var vWinFormEntry;


function FormLookupAccountRKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormROLookup',
			title: 'Lookup Akun',
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpRKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
			],
			listeners:
				{ 
					activate: function()
					{ },
					afterrender: function() {
						RefreshDataLookUpRKAT(criteria);
					}
				}
		}
	);

    vWinFormEntry.show();
};

function GetPanelLookUpRKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
{
	var FormLookUp = new Ext.Panel    // from transaksi jurnal
	(
		{
			id: 'FormLookUp',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpFERKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'11px','margin-top':'2px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items:
					[
						{
							xtype:'button',
							text:'Ok',
							width:70,
							style:{'margin-left':'3px','margin-top':'2px'},
							hideLabel:true,
							id: 'btnOkAcc',
							handler:function()
							{
								if (p != undefined && rowSelectedLookAcc != undefined)
								{
									if (mBolAddNew === true || mBolAddNew === undefined)
									{
										
										p.data.account=rowSelectedLookAcc.data.account;
										p.data.name=rowSelectedLookAcc.data.name;
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
										
									}
									else
									{
									
										if (dsStore != undefined)
										{
											p.data.account=rowSelectedLookAcc.data.account;
											p.data.name=rowSelectedLookAcc.data.name;
											
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}
									}
								}
								vWinFormEntry.close();
							}
						},
						{
								xtype:'button',
								text:'Cancel',
								width:70,
								style:{'margin-left':'5px','margin-top':'2px'},
								hideLabel:true,
								id: 'btnCancelAcc',
								handler:function() 
								{
									vWinFormEntry.close();
								}
						}
					]
				}
			]
        }
	);
	
	return FormLookUp;
};

//---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpFERKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{  
	var fldDetail = ['account','name'];
	dsLookAccList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookAccFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookAccList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookAcc = dsLookAccList.getAt(row);
						}
					}
			}
				),
			cm: fnGridLookAccColumnModelRKAT(),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					if (p != undefined && rowSelectedLookAcc != undefined)
					{
						if (mBolAddNew === true || mBolAddNew === undefined)
						{
							
							p.data.account=rowSelectedLookAcc.data.account;
							p.data.name=rowSelectedLookAcc.data.name;
							if (mBolLookup === true)
							{
							   dsStore.insert(dsStore.getCount(), p);
							}
							else
							{
							   dsStore.removeAt(dsStore.getCount()-1);
							   dsStore.insert(dsStore.getCount(), p);
							}
							
						}
						else
						{
						
							if (dsStore != undefined)
							{
								p.data.account=rowSelectedLookAcc.data.account;
								p.data.name=rowSelectedLookAcc.data.name;
								
								dsStore.removeAt(idx);
								dsStore.insert(idx, p);
							}
						}
					}
					vWinFormEntry.close();
					
				}
				// End Function # --------------
			},
			tbar:
			[
				{ 
					xtype: 'buttongroup',
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		            items: 
		            [
		            	{ 
							xtype: 'tbtext', 
							text: 'No. Akun', 
							style: {'text-align':'left'},
							width: 60
						},
						{
							xtype: 'tbtext', text: ':', style: {'text-align':'right'}, width: 10
						},	
						{
							xtype: 'textfield',
							name: 'txtNoAkunLookUpRKAT',
							id: 'txtNoAkunLookUpRKAT',
							width: 50,
							style: {'text-align':'left'},
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										var criteria_lp='';
										if(Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != '' && Ext.getCmp('txtNamaLookUpRKAT').getValue() == '' || Ext.getCmp('txtNamaLookUpRKAT').getValue() == undefined){
											criteria_lp = criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == '' || Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == undefined){
											criteria_lp =  criteria+ "and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != ''){
											criteria_lp =  criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%' and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'" ;
										}
										RefreshDataLookUpRKAT(criteria_lp);
									} 						
								}
							}
						},
						{ 
							xtype: 'tbspacer',
							width: 20,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Nama Akun', 
							style: {'text-align':'left'},
							width: 70
						},
						{
							xtype: 'tbtext', text: ':', style: {'text-align':'right'}, width: 10
						},
						{
							xtype: 'textfield',
							name: 'txtNamaLookUpRKAT',
							id: 'txtNamaLookUpRKAT',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										var criteria_lp='';
										if(Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != '' && Ext.getCmp('txtNamaLookUpRKAT').getValue() == '' || Ext.getCmp('txtNamaLookUpRKAT').getValue() == undefined){
											criteria_lp = criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == '' || Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == undefined){
											criteria_lp =  criteria+ "and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != ''){
											criteria_lp =  criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%' and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'" ;
										}
										RefreshDataLookUpRKAT(criteria_lp);
									} 						
								}
							}
						},
						{ 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{
							id: 'BtnRefreshLookUpRKAT',
						    text: 'Tampilkan',
						    tooltip: 'Tampilkan',
						    iconCls: 'Refresh',
						    style: {'text-align':'right'},
						    handler: function(sm, row, rec) {
						      var criteria_lp='';
								if(Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != '' && Ext.getCmp('txtNamaLookUpRKAT').getValue() == '' || Ext.getCmp('txtNamaLookUpRKAT').getValue() == undefined){
									criteria_lp = criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%'";
								}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == '' || Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == undefined){
									criteria_lp =  criteria+ "and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'";
								}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != ''){
									criteria_lp =  criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%' and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'" ;
								}
								RefreshDataLookUpRKAT(criteria_lp);
							}
						}
					]
				}
			],
			viewConfig: { forceFit: true }
		});
	
	return vGridLookAccFormEntry;
};

function RefreshDataLookUpRKAT(criteria)
{
	/* if (criteria != '')
	{
		if (Ext.get('txtNoAkunLookUpRKAT').getValue() != '')  {
			criteria = "noakun=" + Ext.get('txtNoAkunLookUpRKAT').getValue();
		};
		if (Ext.get('txtNamaLookUpRKAT').getValue() != '') {
			if (criteria != '')
			{
				criteria += "&nmakun=" + Ext.get('txtNamaLookUpRKAT').getValue();	
			}
			else
			{	
				criteria += "nmakun=" + Ext.get('txtNamaLookUpRKAT').getValue();	
			}
			
	    };
		if (criteria == undefined)
	    {
	    	criteria=''
	    }
	}
 */
	/* dsLookAccList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viLookupAccount',
				param: criteria
			} 
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/getAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data account !', 'Error');
		},	
		success: function(o) 
		{   
			dsLookAccList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsLookAccList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsLookAccList.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data account', 'Error');
			};
		}
	});
}

function fnGridLookAccColumnModelRKAT() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colAccLook',
				header: "No Akun",
				dataIndex: 'account',
				width: 70
			},
			{ 
				id: 'colAccNameLook',
				header: "Nama Akun",
				dataIndex: 'name',
				width: 300
            }
		]
	)
};

function ShowPesanWarning(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfo(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanError(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function getNumber(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};
///---------------------------------------------------------------------------------------///