var CurrentGLRKARutin = 
{
	data: Object,
    details: Array,
    row: 0
};

var CurrentGLDaftarRKARutin = 
{
	data: Object,
    details: Array,
    row: 0
};

var CurrentGLDetailRKARutin = 
{
	data: Object,
    details: Array,
    row: 0
};

var mRecordGLDetailRKARutin = Ext.data.Record.create
(
	[
		{ name: 'urut_rkatrdet', mapping: 'urut_rkatrdet' },
		{ name: 'tahun_anggaran_ta', mapping: 'tahun_anggaran_ta' },
		{ name: 'kd_unit_kerja', mapping: 'kd_unit_kerja' },		
		{ name: 'prioritas', mapping: 'prioritas' },
		{ name: 'kd_jns_rkat_jrka', mapping: 'kd_jns_rkat_jrka' },
		{ name: 'account', mapping: 'account' },
		{ name: 'kd_satuan_sat', mapping: 'kd_satuan_sat' },
		{ name: 'satuan_sat', mapping: 'satuan_sat' },
		{ name: 'name', mapping: 'name' },
		{ name: 'kuantitas_rkatrdet', mapping: 'kuantitas_rkatrdet' },
		{ name: 'biaya_satuan_rkatrdet', mapping: 'biaya_satuan_rkatrdet' },
		{ name: 'jmlh_rkatrdet', mapping: 'jmlh_rkatrdet' },
		{ name: 'm1_rkatrdet', mapping: 'm1_rkatrdet' },
		{ name: 'm2_rkatrdet', mapping: 'm2_rkatrdet' },
		{ name: 'm3_rkatrdet', mapping: 'm3_rkatrdet' },
		{ name: 'm4_rkatrdet', mapping: 'm4_rkatrdet' },
		{ name: 'm5_rkatrdet', mapping: 'm5_rkatrdet' },
		{ name: 'm6_rkatrdet', mapping: 'm6_rkatrdet' },
		{ name: 'm7_rkatrdet', mapping: 'm7_rkatrdet' },
		{ name: 'm8_rkatrdet', mapping: 'm8_rkatrdet' },
		{ name: 'm9_rkatrdet', mapping: 'm9_rkatrdet' },
		{ name: 'm10_rkatrdet', mapping: 'm10_rkatrdet' },
		{ name: 'm11_rkatrdet', mapping: 'm11_rkatrdet' },
		{ name: 'm12_rkatrdet', mapping: 'm12_rkatrdet' },
		{ name: 'deskripsi_rkatrdet', mapping: 'deskripsi_rkatrdet' }
	]
);

var CurrentGLDetailPusatRKARutin = 
{
	data: Object,
    details: Array,
    row: 0
};

var dsTRRKARutinList;
var rowSelectedRKARutin;
var dsCboUnitKerjaRKARutinFilter;
// var selectUnitKerjaRKARutinFilter = gstrListUnitKerja;
var selectUnitKerjaRKARutinFilter ='';
var now = new Date();
var winFormEntryRKARutin;
var dsCboUnitKerjaTabPL2RKARutin;
// var selectUnitKerjaTabPL2RKARutin = gstrListUnitKerja;
var selectUnitKerjaTabPL2RKARutin = '';
var dsCboUnitKerjaTabPN2RKARutin;
// var selectUnitKerjaTabPN2RKARutin = gstrListUnitKerja;
var selectUnitKerjaTabPN2RKARutin = '';
var dsGLDaftarRKARutinList;
var rowSelectedDaftarRKARutin;
var dsGLDetailRKARutinList;
var rowSelectedDetailRKARutin;
var dsGLPusatRKARutinList;
var selectCountRKARutin = 250;
var dsThAnggPaguRKATPL2;
var dsThAnggPaguRKATPN2;
var dsThAnggRKAT1Filter;
var vAddNewRKARutin;
var disahkan;
var dsGLDetailPusatRKARutinList;
var dblBeforeEditTotDetailRKAT2;
var gridListDaftarRKARutin;
var  gridListDetailRKARutin;
var tabActive = 'PL2';
var tabDetActive;
var jumlah;
var danaRKARutin;
var grListTRRKARutin;
CurrentPage.page = getPanelRKARutin(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelRKARutin(mod_id) 
{
    var Fields = 
	[
		'kd_unit_kerja',
		'unitkerja',
		'nama_unit',
		'tahun_anggaran_ta',
		'plafondawal',
		'disahkan_rka','disahkan_rka_int','jumlah','jumlah_rkatk','jumlah_rkatr','tahun_anggaran_ta_tmp','is_revisi'
	];
    dsTRRKARutinList = new WebApp.DataStore({ fields: Fields });	
	
	 var chkApprovedRKARutinList = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprovedRKARutinList',
			header: 'Approve',
			align: 'center',			
			dataIndex: 'disahkan_rka',
			disabled: true,
			width: 15
		}
	); 
	
	grListTRRKARutin = new Ext.grid.EditorGridPanel
	(
		{
			id:'grListTRRKARutin',
			stripeRows: false,
			columnLines: true,
			border: false,
			store: dsTRRKARutinList,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{						
						rowselect: function(sm, row, rec) 
						{
							rowSelectedRKARutin = dsTRRKARutinList.getAt(row);
							CurrentGLRKARutin.row = row;
							CurrentGLRKARutin.data = rowSelectedRKARutin.data;
						}					
					}
				}
			),
			viewConfig: 
			{ 
				stripeRows: true,
				forceFit: true
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colThnAnggGLRKARutin',
						header: 'Tahun Anggaran',
						dataIndex: 'tahun_anggaran_ta',
						sortable: true,
						width: 20,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}
					},
					{
						id: 'colUnitKerjaGLRKARutin',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit',
						sortable: true,						
						width : 100,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}
					},
					chkApprovedRKARutinList
				]
			),
			tbar: 
			[
				{
					id: 'btnEditRKARutin',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{
						
						if (rowSelectedRKARutin != undefined) 
						{
				            showFormEntryRKARutin(rowSelectedRKARutin.data);
				        }
				        else 
						{
							showFormEntryRKARutin();
				        }						
					}
				}
			],
			 listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedRKARutin = dsTRRKARutinList.getAt(ridx);
					if (rowSelectedRKARutin != undefined) 
					{
						showFormEntryRKARutin(rowSelectedRKARutin.data);
					}
					else 
					{
						showFormEntryRKARutin();
					}	
				}
				// End Function # --------------
			},
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRRKARutinList,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			)
		}
	);

	var FormTRRKARutin = new Ext.Panel
	(
		{
			id: mod_id,
			closable: true,
			region: 'center',
			layout: 'form',
			title: 'RAB Rutin',          
			border: false,           
			shadhow: true,
			iconCls: 'RKAT',
			margins: '0 5 5 0',
			items: [grListTRRKARutin],
			tbar: 
			[
				'Unit Kerja'+': ', ' ',mCboUnitKerjaRKARutinFilter(),
				' ','-','Tahun Anggaran : ', ' ',mComboTahunRKARutinFilter(),
				' ','-',
				{
					xtype: 'checkbox',
					id: 'chkFilterRKARutinFilter',
					boxLabel: 'Approved',
					listeners: 
					{
						check: function()
						{
						   RefreshDataGLRKARutin();
						}
					}
				},
				'->',
				{
					id:'btnRefreshRKARutin',
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(sm, row, rec) 
					{
						RefreshDataGLRKARutin();
					}
				}
			],
			listeners:
			{
				'afterrender': function()
				{
					RefreshDataGLRKARutin();
				}
			}
		}
	);
   	
	return FormTRRKARutin;
};

function mCboUnitKerjaRKARutinFilter() 
{
    var Fields = ['kd_unit', 'nama_unit'];
    dsCboUnitKerjaRKARutinFilter = new WebApp.DataStore({ fields: Fields });
    
    var cboUnitKerjaRKARutinFilter = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaRKARutinFilter',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja ',
		    align: 'right',
		    width: 200,
		    store: dsCboUnitKerjaRKARutinFilter,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaRKARutinFilter = b.data.kd_unit;
					RefreshDataGLRKARutin();
			    }
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaRKARutinFilter.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaRKARutinFilter.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaRKARutinFilter.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
    return cboUnitKerjaRKARutinFilter;
};

function mComboTahunRKARutinFilter()
{
	var Field = ['tahun_anggaran_ta', 'closed_ta'];
	dsThAnggRKAT1Filter = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getThnAnggaran",
		params: {
			text:''
		},
		failure: function(o)
		{
			//ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsThAnggRKAT1Filter.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsThAnggRKAT1Filter.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsThAnggRKAT1Filter.add(recs);
			} 
		}
	});
	
	var currYear = parseInt(now.format('Y'));
	var cboTahunRKARutinFilter = new Ext.form.ComboBox
	(
		{
			id:'cboTahunRKARutinFilter',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:100,
			store: dsThAnggRKAT1Filter,
			// valueField: 'TAHUN_ANGGARAN_TA',
			// value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1),
			// value:'',
			// displayField: 'TMP_TAHUN'
			/* store: new Ext.data.ArrayStore
						(
							{
								id: 0,
								fields: 
								[
									'Id',
									'displayText'
								],
								data: 
								[						
									[currYear + 1,currYear + 1], 
									[currYear,currYear]
								]
							}
						),
			valueField: 'Id',
			displayField: 'displayText'	,
			value: Ext.num(now.format('Y')), */
			valueField: 'tahun_anggaran_ta',
			displayField: 'tahun_anggaran_ta'	,
			value: Ext.num(now.format('Y')),
			listeners:
			{
			    'select': function(a, b, c) 
				{
			       RefreshDataGLRKARutin();
			    }
			}
		}
	);	
	
	return cboTahunRKARutinFilter;
};

function showFormEntryRKARutin(rowdata)
{
	
	var lebar = 800;	
	winFormEntryRKARutin = new Ext.Window
	(
		{
			id: 'winFormEntryRKARutin',
			title: 'RAB Rutin',
			layout: 'border',
			modal: true,
			resizable: false,
			width: lebar,
			height: 560,
			iconCls: 'SetupKUPPA',
			items: [getItemsFormEntryRKARutin(lebar,rowdata)],
			listeners:
			{
				'deactivate': function()
				{
					rowSelectedRKARutin = undefined;
					rowSelectedDaftarRKARutin = undefined;
					rowSelectedDetailRKARutin = undefined;
					grListTRRKARutin.getSelectionModel().clearSelections();
					// vAddNewRKARutin = undefined;
					
					//RefreshDataGLRKARutin(getCriteriaGListRKARutin());
				},
				close: function (){
					Ext.getCmp('cboTahunEntryPL2RKARutin').setValue('');
					selectUnitKerjaTabPL2RKARutin = '';
				},
			}
		}
	);	
	winFormEntryRKARutin.show();
	if(rowdata !== undefined)
	{
		disahkan = false;
		initFormEntryRKARutin(rowdata);		
	}
	else
	{
		AddNewRKARutin(tabActive);
		
	}
}

function getItemsFormEntryRKARutin(lebar,rowdata)
{
	var pnlContainerRKARutin = new Ext.Panel
	(
		{
			id: 'pnlContainerRKARutin',
			region: 'center',
			layout: 'border',
			border: false,
			width: lebar,
			items:
			[
				getTabPanelFormEntryHeaderRKARutin(rowdata),
				getTabPanelFormEntryDetailRKARutin()
			]
		}
	);
	
	return pnlContainerRKARutin;
}

function getTabPanelFormEntryHeaderRKARutin(rowdata)
{
	// console.log(rowdata);
	var tabPL2FormEntryHeaderRKARutin = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabPL2FormEntryHeaderRKARutin',
            title: 'Pengeluaran (PL-2)',
            items: 
			[
                {
					xtype: 'fieldset',
					style: 'margin: 5px',
					items: 
					[						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Unit Kerja',
							items: 
							[										
								mCboUnitKerjaTabPL2RKARutin(),
								{
									xtype: 'displayfield',
									value: 'Tahun Anggaran: ',
									width: 100
								},
								mComboTahunEntryPL2RKARutin(),
								{
									xtype: 'displayfield',									
									value: 'Prioritas: ',
									width: 60
								},
								{
									xtype: 'textfield',
									id: 'txtPrioritasPL2RKARutin',									
									style: 'text-align: right',
									readOnly: true,
									emptyText: 'Auto-generate'
								}
							]
						},						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Kegiatan',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaKegiatanPL2RKARutin',
									height: 40
								}
							]
						},						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Plafond (Rp.)',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtJumPlafondPL2RKARutin',									
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},{
									xtype: 'displayfield',
									value: 'Non Rutin: ',
									width: 100
								},
								{
									xtype: 'textfield',									
									id: 'txtPengembanganPL2RKARutin',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Rutin: ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtRutinPL2RKARutin',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Sisa (Rp.): ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtSisaPlafondPL2RKARutin',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Status RAB',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtaStatusPL2_RKARutin',
									readOnly: true,
									style: 'color: #FF0000; font: bold 120% "Verdana";'
								},
								{
									xtype: 'displayfield',
									value: 'Revisi: ',
									width: 40
								},
								{
									xtype: 'checkbox',
									id: 'txtCheckPL2_RKARutin',
									readOnly:true
								}
							]
						}
					]
				}
			],
			listeners:
			{
				'activate': function()
				{
					
					tabActive = "PL2";
					console.log(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),selectUnitKerjaTabPL2RKARutin);
					RefreshDataGLDaftarPL2(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),selectUnitKerjaTabPL2RKARutin); 
					Ext.getCmp('tabFormEntryDetailRKARutin').setActiveTab(
						Ext.getCmp('tabDaftarFormEntryDetailRKARutin')
					);
					Ext.getCmp('dspTotBiayaDaftarRKARutin').setValue("Total Biaya Rp.");
				}
			}
		}
	);
	
	var tabPN2FormEntryHeaderRKARutin = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabPN2FormEntryHeaderRKARutin',
            title: 'Penerimaan (PN-2)',
            items: 
			[
                {
					xtype: 'fieldset',
					style: 'margin: 5px',					
					items: 
					[						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Unit Kerja',
							labelWidth: 120,
							items: 
							[
								mCboUnitKerjaTabPN2RKARutin(),
								{
									xtype: 'displayfield',
									value: 'Tahun Anggaran: ',
									width: 100
								},
								mComboTahunEntryPN2RKARutin(),
								{
									xtype: 'displayfield',
									value: 'Prioritas: ',
									width: 60
								},
								{
									xtype: 'textfield',
									id: 'txtPrioritasPN2RKARutin',									
									style: 'text-align: right',
									readOnly: true,
									emptyText: 'Auto-generate'
								}
							]
						},						
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Kegiatan',
							items: 
							[
								{
									xtype: 'textarea',
									id: 'txtaKegiatanPN2RKARutin',
									height: 40
								}
							]
						},
						{
							xtype: 'compositefield',
							anchor: '100%',
							defaults: { flex: 1 },
							fieldLabel: 'Plafond (Rp.)',
							hidden: true,
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtJumPlafondPN2RKARutin',									
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},{
									xtype: 'displayfield',
									value: 'Non Rutin: ',
									width: 100
								},
								{
									xtype: 'textfield',									
									id: 'txtPengembanganPN2RKARutin',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Rutin: ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtRutinPN2RKARutin',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								},
								{
									xtype: 'displayfield',
									value: 'Sisa (Rp.): ',
									width: 60
								},
								{
									xtype: 'textfield',									
									id: 'txtSisaPlafondPN2RKARutin',
									style: 'text-align: right;font-weight: bold;',
									readOnly: true
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Status RAB',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtaStatusPN2_RKARutin',
									readOnly: true,
									style: 'color: #FF0000; font: bold 120% "Verdana";'
								},
								{
									xtype: 'displayfield',
									value: 'Revisi: ',
									width: 40
								},
								{
									xtype: 'checkbox',
									id: 'txtCheckPN2_RKARutin',
									readOnly:true
								}
							]
						}						
					]
				}
			],
			listeners:
			{
				'activate': function()
				{
					
					tabActive = "PN2";
					RefreshDataGLDaftarPN2(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),selectUnitKerjaTabPN2RKARutin); 
					Ext.getCmp('tabFormEntryDetailRKARutin').setActiveTab(
						Ext.getCmp('tabDaftarFormEntryDetailRKARutin')
					);
					Ext.getCmp('dspTotBiayaDaftarRKARutin').setValue("Total Penerimaan Rp.");
				}
			}
		}
	);
	
	var tabFormEntryHeaderRKARutin = new Ext.TabPanel
	(
		{
			id: 'tabFormEntryHeaderRKARutin',			
			region: 'north',
			border: false,
			activeTab: 0,
			items: [tabPL2FormEntryHeaderRKARutin,tabPN2FormEntryHeaderRKARutin],
			tbar:
			{
				xtype: 'toolbar',				
				items: 
				[
					{
						xtype: 'button',
						iconCls: 'add',
						id:'btnadd_RKATRutin',
						text: 'Tambah',
						handler: function()
						{
							AddNewRKARutin(tabActive);
						}
					},
					{
						xtype: 'button',
						iconCls: 'save',
						id:'btnsave_RKATRutin',
						text: 'Simpan',
						handler: function()
						{
							/* Ext.getCmp('btnsave_RKATRutin').setDisabled(true);
							Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(true);	 */
							if(tabActive == "PL2")
							{
								SaveDataPL2RKATRutin(false,rowdata);
							}							
							else
							{
								SaveDataPN2RKATRutin(false,rowdata);
							}
						}
					},
					{
						xtype: 'button',
						iconCls: 'saveexit',
						id:'btnsaveexit_RKATRutin',
						text: 'Simpan & Keluar',
						handler: function()
						{
							Ext.getCmp('btnsave_RKATRutin').setDisabled(true);
							Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(true);		
							if(tabActive == "PL2")
							{
								SaveDataPL2RKATRutin(true,rowdata);
							}							
							else
							{
								SaveDataPN2RKATRutin(true,rowdata);
							}
						}
					},
					{
						xtype: 'button',
						iconCls: 'remove',
						id:'btnremove_RKATRutin',
						text: 'Hapus',
						handler: function()
						{
							if(tabActive == "PL2")
							{
								HapusDataPL2RKATRutin();
							}							
							else
							{
								HapusDataPN2RKATRutin();
							}
						}
					},
					{ xtype: 'tbseparator' },
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						iconCls: 'print',
						text: 'Cetak',
						hidden:true
					}
				]
			}	
		}
	);
	
	return tabFormEntryHeaderRKARutin;
}

function getCriteriaLookupProgUK()
{
	var strKriteria = "";
		
	if(tabActive == "PL2")
	{
		if(selectUnitKerjaTabPL2RKARutin != undefined)
		{
			strKriteria += "unit=" + selectUnitKerjaTabPL2RKARutin;
		}
		
		if(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue() != "")
		{
			if(strKriteria != "")
			{
				strKriteria += "&";
			}
			strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPL2RKARutin').getValue();
		}
	}
	else if(tabActive == "PN2")
		{
			if(selectUnitKerjaTabPN2RKARutin != undefined)
			{
				strKriteria += "unit=" + selectUnitKerjaTabPN2RKARutin;
			}
			
			if(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue() != "")
			{
				if(strKriteria != "")
				{
					strKriteria += "&";
				}
				strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPN2RKARutin').getValue();
			}
		}
	
	return strKriteria;
}

function mComboTahunEntryPL2RKARutin()
{
	/* var Field = ['TAHUN_ANGGARAN_TA','TMP_TAHUN'];
	dsThAnggPaguRKATPL2 = new WebApp.DataStore({ fields: Field });
	
	dsThAnggPaguRKATPL2.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA', 
				Sortdir: 'ASC', 
				target:'viCboThnAngg',
				param: 0
			} 
		}
	); */
	var currYear = parseInt(now.format('Y'));	
	var cboTahunEntryPL2RKARutin = new Ext.form.ComboBox
	(
		{
			id:'cboTahunEntryPL2RKARutin',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:90,
			/* store: dsThAnggPaguRKATPL2,
			valueField: 'TAHUN_ANGGARAN_TA',
			displayField: 'TMP_TAHUN',
			// value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1),
			value:'',
			listeners:
			{
				'select': function(){ GetPlafondRKAT2(); }
			} */
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear ]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value: gstrTahunAngg+'/'+(Ext.num(gstrTahunAngg)+1),
			value: Ext.num(now.format('Y'))
		}
	);	
	
	return cboTahunEntryPL2RKARutin;
};

function mCboUnitKerjaTabPL2RKARutin() 
{
    var Field = ['kd_unit', 'nama_unit'];
    dsCboUnitKerjaTabPL2RKARutin = new WebApp.DataStore({ fields: Field });
    
    var cboUnitKerjaTabPL2RKARutin = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaTabPL2RKARutin',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    width: 250,
		    store: dsCboUnitKerjaTabPL2RKARutin,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaTabPL2RKARutin = b.data.kd_unit;
					
					 GetPlafondRKAT2(1); // 1 pengeluaran || JIKA KETIKA MEMILIH UNIT KERJA, DATA RAB MUNCUL
			    }
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaTabPL2RKARutin.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaTabPL2RKARutin.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaTabPL2RKARutin.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
    return cboUnitKerjaTabPL2RKARutin;
};

function mComboTahunEntryPN2RKARutin()
{
	var Field = ['TAHUN_ANGGARAN_TA','TMP_TAHUN'];
	dsThAnggPaguRKATPN2 = new WebApp.DataStore({ fields: Field });
	
	var currYear = parseInt(now.format('Y'));	
	var cboTahunEntryPN2RKARutin = new Ext.form.ComboBox
	(
		{
			id:'cboTahunEntryPN2RKARutin',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:90,
			/* store: dsThAnggPaguRKATPN2,
			valueField: 'TAHUN_ANGGARAN_TA',
			displayField: 'TMP_TAHUN', */
			// value:gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1)
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1 ], 
						[currYear,currYear ]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value: Ext.num(now.format('Y'))
		}
	);

	/* dsThAnggPaguRKATPN2.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA', 
				Sortdir: 'ASC', 
				target:'viCboThnAngg',
				param: 0
			} 
		}
	); */
	
	return cboTahunEntryPN2RKARutin;
};

function mCboUnitKerjaTabPN2RKARutin() 
{
    var Field = ['kd_unit', 'nama_unit'];
    dsCboUnitKerjaTabPN2RKARutin = new WebApp.DataStore({ fields: Field });
    
    var cboUnitKerjaTabPN2RKARutin = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaTabPN2RKARutin',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja' ,
		    align: 'right',
		    width: 250,
		    store: dsCboUnitKerjaTabPN2RKARutin,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaTabPN2RKARutin = b.data.kd_unit;
					GetPlafondRKAT2(2);//2 penerimaan
			    }
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaTabPN2RKARutin.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaTabPN2RKARutin.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaTabPN2RKARutin.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
    return cboUnitKerjaTabPN2RKARutin;
};

function getTabPanelFormEntryDetailRKARutin()
{
	var pnlTotBiayaKegDaftarRKARutin = new Ext.Panel
	(
		{
			id: 'pnlTotBiayaKegDaftarRKARutin',
			layout: 'hbox',
			border: false,			
			bodyStyle: 'padding: 5px 18px 0 350px;border-top: solid 1px #DFE8F6;',
			defaults:
			{
				flex: 1
			},
			items:
			[
				{
					xtype: 'displayfield',
					id: 'dspTotBiayaDaftarRKARutin',
					value: 'Total Biaya Rp. ',
					style: 'padding-left: 50px;font-weight: bold;font-size: 14px;'					
				},
				{
					xtype: 'textfield',
					id: 'txtTotBiayaKegDaftarRKARutin',
					readOnly: true,
					width: 198,
					style: 'font-weight: bold;text-align: right'
				}
			]
		}
	);
	
	var tabDaftarFormEntryDetailRKARutin = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabDaftarFormEntryDetailRKARutin',
            title: 'Daftar ',
			autoScroll: true,			
            items: 
			[
               getGridListDaftarRKARutin(),
			   pnlTotBiayaKegDaftarRKARutin
			],
			listeners:
			{
				'activate': function()
				{
					tabDetActive = 0;					
				}
			}
		}
	);
	
	var pnlTotBiayaKegDetailRKARutin = new Ext.Panel
	(
		{
			id: 'pnlTotBiayaKegDetailRKARutin',
			layout: 'hbox',
			border: false,			
			bodyStyle: 'padding: 4px 18px 0 350px;border-top: solid 1px #DFE8F6;',
			defaults:
			{
				flex: 1
			},
			items:
			[
				{
					xtype: 'displayfield',
					id: 'dspTotBiayaDetailRKARutin',
					value: 'Total Jumlah Rp. ',
					style: 'padding-left: 50px;font-weight: bold;font-size: 14px;'					
				},
				{
					xtype: 'textfield',
					id: 'txtTotBiayaKegDetailRKARutin',
					readOnly: true,
					width: 198,
					style: 'font-weight: bold;text-align: right'
				}
			]
		}
	);
	
	var tabDetailFormEntryDetailRKARutin = new Ext.FormPanel
	(
		{
			border: false,
			id: 'tabDetailFormEntryDetailRKARutin',
            title: 'Detail ',
			autoScroll: false,
            items: 
			[
				getGridListDetailRKARutin(),
				pnlTotBiayaKegDetailRKARutin
			],
			tbar:
			{
				xtype: 'toolbar',				
				items: 
				[
					{
						xtype: 'button',
						iconCls: 'add',
						id:'btndetailadd_RKATRutin',
						text: 'Tambah Baris',
						handler: function()
						{
							TambahBarisGLDetailRKARutin(tabActive);
						}
					},					
					{
						xtype: 'button',
						iconCls: 'remove',
						id:'btndetailhapus_RKATRutin',
						text: 'Hapus Baris',
						handler: function()
						{
							HapusBarisDetailRKARutin();
						}
					}	
				]
			},
			listeners:
			{
				'activate': function()
				{
					tabDetActive = 1;
					if(rowSelectedDaftarRKARutin !== undefined)
					{
						Ext.getCmp('btndetailadd_RKATRutin').setDisabled(false);
						Ext.getCmp('btndetailhapus_RKATRutin').setDisabled(false);
						if(tabActive == "PL2")
						{
							RefreshDataGLDetailPL2(
								Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
								selectUnitKerjaTabPL2RKARutin,
								1,//pengeluaran
								Ext.getCmp('txtPrioritasPL2RKARutin').getValue()
							);
						}
						else
						{
							// RefreshDataGLDetailPN2(getCriteriaGListDetailPN2());
							RefreshDataGLDetailPN2(
								Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
								selectUnitKerjaTabPN2RKARutin,
								2,//penerimaan
								Ext.getCmp('txtPrioritasPN2RKARutin').getValue()
							);
						}
					}
					else
					{
						Ext.getCmp('btndetailadd_RKATRutin').setDisabled(true);
						Ext.getCmp('btndetailhapus_RKATRutin').setDisabled(true);
						dsGLDetailRKARutinList.removeAll();
						alert('Pilih daftar RAB terlebih dahulu!');
						// ShowPesanInfo('Pilih daftar RAB terlebih dahulu!','Information');
						// Ext.getCmp('tabFormEntryDetailRKARutin').setActiveTab(
							// Ext.getCmp('tabDaftarFormEntryDetailRKARutin')
						// );
					}
					
					gridListDaftarRKARutin.getSelectionModel().clearSelections();
					rowSelectedDaftarRKARutin=undefined;
				}
			}
		}
	);
	
	var tabFormEntryDetailRKARutin = new Ext.TabPanel
	(
		{
			id: 'tabFormEntryDetailRKARutin',			
			region: 'center',
			border: false,
			activeTab: 0,
			items: 
			[
				tabDaftarFormEntryDetailRKARutin,
				tabDetailFormEntryDetailRKARutin
			]
		}
	);
	
	return tabFormEntryDetailRKARutin;
}

function getGridListDaftarRKARutin()
{
	var Field = [		
		'kd_unit_kerja',
		'tahun_anggaran_ta',
		'kd_jns_rkat_jrka',
		//'no_program_prog',
		'prioritas_rkatr',
		'nama_program_prog',
		'kegiatan',
		'tujuan',
		'rasional',
		'latarbelakang',
		'mekanisme',
		'indikator',
		'nama_unit',
		'parent',
		'plafondawal',
		'jumlah',
		'is_revisi'
	];
    dsGLDaftarRKARutinList = new WebApp.DataStore({ fields: Field });
	
	gridListDaftarRKARutin = new Ext.grid.EditorGridPanel
	(
		{
			id: 'gridListDaftarRKARutin',
			stripeRows: true,
			columnLines:true,
			border:false,
			store: dsGLDaftarRKARutinList,			
			// anchor: '100% 85%',
			anchor: '100% 90%',
			frame:true,
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedDaftarRKARutin = dsGLDaftarRKARutinList.getAt(row);
							CurrentGLDaftarRKARutin.row = row;
							CurrentGLDaftarRKARutin.data = rowSelectedDaftarRKARutin.data;	

							initFormEntryFromDaftarRKARutin(rowSelectedDaftarRKARutin.data);							
						}
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),										
					{
						id: 'col_keg_glist_daftarrkat1',
						header: "Kegiatan",
						dataIndex: 'kegiatan_rkatr',
						sortable: true,
						flex: 1,
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}
					},
					{
						id: 'col_jum_glist_daftarrkat1',
						header: "Jumlah (Rp.)",
						dataIndex: 'jumlah',
						sortable: true,	
						width: 115,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						}
					}
				]
			),	
			viewConfig: { forceFit: true },
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							getTotalBiayaTabDaftarRKAT2(); 							
							//CalcPlafondRKATRutin(); 
						} 
					);
					this.store.on("datachanged", function()
						{
							getTotalBiayaTabDaftarRKAT2(); 							
							//CalcPlafondRKATRutin(); 						
						}
					);
				}
			}
		}
	);
	
	return gridListDaftarRKARutin;
}

function getGridListDetailRKARutin()
{
	var Fields = [
		'urut_rkatrdet',
		'tahun_anggaran_ta',
		'kd_unit_kerja',		
		'prioritas',
		'kd_jns_rkat_jrka',
		'account',
		'kd_satuan_sat',
		'satuan_sat',
		'name',
		'kuantitas_rkatrdet',
		'biaya_satuan_rkatrdet',
		'jmlh_rkatrdet',
		'm1_rkatrdet',
		'm2_rkatrdet',
		'm3_rkatrdet',
		'm4_rkatrdet',
		'm5_rkatrdet',
		'm6_rkatrdet',
		'm7_rkatrdet',
		'm8_rkatrdet',
		'm9_rkatrdet',
		'm10_rkatrdet',
		'm11_rkatrdet',
		'm12_rkatrdet',
		'deskripsi_rkatrdet',
		'account_temp'
	];
    dsGLDetailRKARutinList = new WebApp.DataStore({ fields: Fields });
	
	gridListDetailRKARutin = new Ext.grid.EditorGridPanel
	(
		{
			id: 'gridListDetailRKARutin',
			stripeRows: false,
			columnLines:true,
			border:false,
			autoScroll: true,
			anchor: '100% 90%',
			store: dsGLDetailRKARutinList,									
			/* sm: new Ext.grid.RowSelectionModel
			(
				{					
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedDetailRKARutin = dsGLDetailRKARutinList.getAt(row);
							CurrentGLDetailRKARutin.row = row;
							CurrentGLDetailRKARutin.data = rowSelectedDetailRKARutin.data;
							
							danaRKARutin = rowSelectedDetailRKARutin.data.jumlah;
						}
					}
				}
			), */
			selModel: new Ext.grid.CellSelectionModel({
				singleSelect: true,
				listeners:{
					cellselect: function(sm, row, rec){
						rowSelectedDetailRKARutin = dsGLDetailRKARutinList.getAt(row);
							CurrentGLDetailRKARutin.row = row;
							CurrentGLDetailRKARutin.data = rowSelectedDetailRKARutin.data;
							
							danaRKARutin = rowSelectedDetailRKARutin.data.jumlah;
					}
				}
			}),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: "urut_rkatrdet",
						dataIndex: 'urut_rkatrdet',
						sortable: true,
						width: 55,
						hidden:true
					},
					{
						// id: 'col_noakun_glist_detailrkat1',
						header: "No. Akun",
						dataIndex: 'account',
						sortable: true,						
						width : 70,
						editor: new Ext.form.TextField(
							{
								id: 'fcol_noakun_glist_detailrkat1',								
								enableKeyEvents : true,
								listeners: 
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() == 13) 
										{
											var unit = "";
											if(tabActive == "PL2")
											{
												unit = selectUnitKerjaTabPL2RKARutin;
												GetFormLookupAccountRKATRutin("account like '" + this.getValue() +"%'   AND groups in (1,5) ");
											}
											else
											{
												unit = selectUnitKerjaTabPN2RKARutin;
												GetFormLookupAccountRKATRutin("account like '" + this.getValue() +"%'  AND groups= 4 ");
											}
											// GetFormLookupAccountRKATRutin("kdunit=" + unit + "&noakun=" + this.getValue());
											
										} 
									}
								}
							}
						)
					},
					{
						// id: 'col_nmakun_glist_detailrkat1',
						header: "Nama Akun & Uraian",
						dataIndex: 'name',
						sortable: true,
						width : 240,
						editor: new Ext.form.TextField(
							{
								id:'fcol_nmakun_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents : true,
								listeners: 
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() == 13) 
										{
											var unit = "";
											if(tabActive == "PL2")
											{
												unit = selectUnitKerjaTabPL2RKARutin;
											}
											else
											{
												unit = selectUnitKerjaTabPN2RKARutin;
											}
											// GetFormLookupAccountRKATRutin("kdunit=" + unit + "&nmakun=" + this.getValue());										
											GetFormLookupAccountRKATRutin("upper(name) like '" + this.getValue().toUpperCase() +"%'");
										} 
									}
								}
							}
						)
					},
					{
						// id: 'col_deskripsi_glist_detailrkat1',
						header: "Deskripsi",
						dataIndex: 'deskripsi_rkatrdet',
						sortable: true,
						width : 240,
						editor: new Ext.form.TextField(
							{
								id:'fcol_deskripsi_glist_detailrkat1',
								allowBlank: true,
								listeners: 
								{
									/* 'blur': function()
									{
										var qty = parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.QTY);
										var satuan = parseFloat(this.getValue());
										var total = qty * satuan;
										Ext.getCmp('fcol_jum_glist_detailrkat1').setValue(total);
										dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.JUMLAH = total;
									} */
								}
							}
						)
					},
					{
						// id: 'col_qty_glist_detailrkat1',
						header: "Kuantitas",
						dataIndex: 'kuantitas_rkatrdet',
						sortable: true,	
						align: 'right',
						width : 100,
						editor: new Ext.form.NumberField(
							{
								id:'fcol_qty_glist_detailrkat1',
								allowBlank: true,
								listeners: 
								{	
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9)
										{											
											var qty = parseFloat(this.getValue()); //parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.QTY);
											var satuan = parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.biaya_satuan_rkatrdet); //parseFloat(this.getValue());
											var total = qty * satuan;		
											console.log(total);
											Ext.getCmp('fcol_jum_glist_detailrkat1').setValue(total);
											dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.jmlh_rkatrdet = total;
											getTotalBiayaTabDetailRKAT2();
											getTotalRutin_RKARutin(false);									
											
										}
									},
									/* 'blur': function()
									{
										var qty = parseFloat(this.getValue()); //parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.QTY);
										var satuan = parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.biaya_satuan_rkatrdet); //parseFloat(this.getValue());
										var total = qty * satuan;										
										Ext.getCmp('fcol_jum_glist_detailrkat1').setValue(total);
										dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.jmlh_rkatrdet = total;
										getTotalBiayaTabDetailRKAT2();
										getTotalRutin_RKARutin(false);									
									},	
									'change':function(field,newValue,oldValue )
									{
										if(Ext.EventObject.getKey() != 13)
										{
										  
											if (newValue != oldValue)
											{
												// var qty = parseFloat(this.getValue()); //parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.QTY);
												// var satuan = parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.BIAYA_SATUAN); //parseFloat(this.getValue());
												// var total = qty * satuan;
												// Ext.getCmp('fcol_jum_glist_detailrkat1').setValue(total);
												// dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.JUMLAH = total;
											}
										}
									} */
								}
							}
						)
					},
					{
						// id: 'col_sat_glist_detailrkat1',
						header: "Satuan",
						dataIndex: 'satuan_sat',
						sortable: true,
						width: 55,
						editor: mComboFieldEntrySatDetailRKAT1()
					},
					{
						// id: 'col_biayasat_glist_detailrkat1',
						header: "Biaya/Satuan (Rp.)",
						dataIndex: 'biaya_satuan_rkatrdet',
						sortable: true,								
						width : 120,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								id:'fcol_biayasat_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9)
										{											
											var qty = parseFloat(dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.kuantitas_rkatrdet);
											var satuan = parseFloat(this.getValue());
											var total = parseFloat(qty * satuan);
											console.log(total);
											Ext.getCmp('fcol_jum_glist_detailrkat1').setValue(total);											
											dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.jmlh_rkatrdet = total;										
											getTotalBiayaTabDetailRKAT2();
											getTotalRutin_RKARutin(false);											
										
										}
									},
									
								}
							}
						)
					},
					{
						// id: 'col_jum_glist_detailrkat1',
						header: "Jumlah (Rp.)",
						dataIndex: 'jmlh_rkatrdet',						
						width : 90,
						sortable: true,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								id:'fcol_jum_glist_detailrkat1',
								//allowBlank: true,
								readOnly: true,
								'blur': function()
								{
									
									//getTotalBiayaTabDetailRKAT2();
									//getTotalRutin_RKARutin(false);
								}
							}
						)						
					}, 
					{
						// id: 'col_jan_glist_detailrkat1',
						header: "Jan (Rp.)",
						dataIndex: 'm1_rkatrdet',
						sortable: true,						
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_jan_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
										
									}
								}
							}
						)
					},
					{
						// id: 'col_feb_glist_detailrkat1',
						header: "Feb (Rp.)",
						dataIndex: 'm2_rkatrdet',
						sortable: true,						
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_feb_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
										
									}
								}								
							}
						)
					},
					{
						// id: 'col_mar_glist_detailrkat1',
						header: "Mar (Rp.)",
						dataIndex: 'm3_rkatrdet',
						sortable: true,							
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_mar_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_apr_glist_detailrkat1',
						header: "Apr (Rp.)",
						dataIndex: 'm4_rkatrdet',
						sortable: true,	
						align: 'right',
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_apr_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{

									}
								}
							}
						)
					},
					{
						// id: 'col_mei_glist_detailrkat1',
						header: "Mei (Rp.)",
						dataIndex: 'm5_rkatrdet',
						sortable: true,							
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_mei_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_jun_glist_detailrkat1',
						header: "Jun (Rp.)",
						dataIndex: 'm6_rkatrdet',
						sortable: true,							
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_jun_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_jul_glist_detailrkat1',
						header: "Jul (Rp.)",
						dataIndex: 'm7_rkatrdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_jul_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}								
							}
						)
					},
					{
						// id: 'col_ags_glist_detailrkat1',
						header: "Ags (Rp.)",
						dataIndex: 'm8_rkatrdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_ags_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}								
							}
						)
					},
					{
						// id: 'col_sep_glist_detailrkat1',
						header: "Sep (Rp.)",
						dataIndex: 'm9_rkatrdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_sep_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_okt_glist_detailrkat1',
						header: "Okt (Rp.)",
						dataIndex: 'm10_rkatrdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_okt_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
					
									}
								}
							}
						)
					},
					{
						// id: 'col_nov_glist_detailrkat1',
						header: "Nov (Rp.)",
						dataIndex: 'm11_rkatrdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_nov_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
																				
									}
								}
							}
						)
					},
					{
						// id: 'col_des_glist_detailrkat1',
						header: "Des (Rp.)",
						dataIndex: 'm12_rkatrdet',						
						sortable: true,
						width : 80,
						align: 'right',
						renderer: function(value, cell)
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + formatCurrency(value) + "</div>";
							return str;
						},
						editor: new Ext.form.NumberField(
							{
								// id:'fcol_des_glist_detailrkat1',
								allowBlank: true,
								enableKeyEvents: true,
								listeners:
								{									
									'blur': function()
									{
										
									}
								}								
							}
						)
					}
				]
			),
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							getTotalBiayaTabDetailRKAT2(); 							
						//	CalcPlafondRKATRutin(); 
						} 
					);
					this.store.on("datachanged", function()
						{
							getTotalBiayaTabDetailRKAT2(); 							
							//CalcPlafondRKATRutin(); 						
						}
					);
				}
			}
		}
	);
	
	return gridListDetailRKARutin;
}

function mComboFieldEntrySatDetailRKAT1()
{
	var Field = ['kd_satuan_sat','satuan_sat'];
	var dsCboFieldSatuanDetailRKAT1 = new WebApp.DataStore({ fields: Field });

	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_satuan",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data satuan !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboFieldSatuanDetailRKAT1.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboFieldSatuanDetailRKAT1.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboFieldSatuanDetailRKAT1.add(recs);
			} else {
				ShowPesanError('Gagal menampilkan data satuan', 'Error');
			};
		}
	});
	var cboFieldSatuanDetailRKAT1 = new Ext.form.ComboBox
	(
		{
			id:'cboFieldSatuanDetailRKAT1',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',	
			width:60,
			store: dsCboFieldSatuanDetailRKAT1,
			valueField: 'satuan_sat',
			displayField: 'satuan_sat',
			listeners:
			{
				'select': function(a,b,c)
				{
					dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.kd_satuan_sat = b.data.kd_satuan_sat;					
					//dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.SATUAN_SAT = b.data.SATUAN_SAT;					
				}
			}
		}
	);
	
	return cboFieldSatuanDetailRKAT1;
};

/** DATA */
function RefreshDataGLRKARutin_lama(filter)
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_rkat",
		params: {
			kd_unit_kerja 		: Ext.getCmp('cboUnitKerjaRKARutinFilter').getValue(),
			tahun_anggaran_ta 	: Ext.getCmp('cboTahunRKARutinFilter').getValue(),
			disahkan_rka		: Ext.getCmp('chkFilterRKARutinFilter').getValue(),
			filter				: filter
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRKARutinList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsTRRKARutinList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsTRRKARutinList.add(recs);
				grListTRRKARutin.getView().refresh();
				console.log(dsTRRKARutinList.data);
			} else {
				ShowPesanError('Gagal menampilkan data RAB', 'Error');
			};
		},
		callback:function(){
			var gridListRAB=Ext.getCmp('grListTRRKARutin').getStore().data.items;
				for(var i=0,iLen=gridListRAB.length; i<iLen;i++){
					if(gridListRAB[i].data.disahkan_rka_int == 1){
						gridListRAB[i].data.disahkan_rka = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grListTRRKARutin').getStore().data.items);
			Ext.getCmp('grListTRRKARutin').getView().refresh();
			
		}
	});
};

function RefreshDataGLRKARutin()
{	
	var criteria = GetCriteriaGridUtama();
	dsTRRKARutinList.removeAll();
	dsTRRKARutinList.load({
		params:{
			Skip: 0,
			Take: selectCountRKARutin,
			Sort: '',
			Sortdir: 'ASC',
			target: 'vi_viewdata_rab_rutin',
			param: criteria
		},
		callback:function(){
			var gridListRAB=Ext.getCmp('grListTRRKARutin').getStore().data.items;
				for(var i=0,iLen=gridListRAB.length; i<iLen;i++){
					if(gridListRAB[i].data.disahkan_rka_int == 1){
						gridListRAB[i].data.disahkan_rka = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grListTRRKARutin').getStore().data.items);
			Ext.getCmp('grListTRRKARutin').getView().refresh();
			
		}
	});
    return dsTRRKARutinList;
};


function GetCriteriaGridUtama(){
	var criteria = '';
	if (Ext.getCmp('cboUnitKerjaRKARutinFilter').getValue() == '000' || Ext.getCmp('cboUnitKerjaRKARutinFilter').getValue() == 000){
		// criteria = "  WHERE a.kd_jns_plafond = 1 and a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunRKARutinFilter').getValue() + " '  and a.disahkan_rka ='" +  Ext.getCmp('chkFilterRKARutinFilter').getValue()+ "' ";
		criteria = "SEMUA~WHERE a.kd_jns_plafond = 1 and a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunRKARutinFilter').getValue() + " '  and a.disahkan_rka ='" +  Ext.getCmp('chkFilterRKARutinFilter').getValue()+ "' ";
		
	}else{
		criteria = "  WHERE a.kd_jns_plafond = 1 and a.tahun_anggaran_ta= '" + Ext.getCmp('cboTahunRKARutinFilter').getValue() + " '  and a.disahkan_rka ='" +  Ext.getCmp('chkFilterRKARutinFilter').getValue()+ "' and a.kd_unit_kerja = '"+ Ext.getCmp('cboUnitKerjaRKARutinFilter').getValue()+"'";
	}
	
	return criteria;
}

function getCriteriaGListRKARutin()
{
	var strKriteria = "";
	
	if(selectUnitKerjaRKARutinFilter !== undefined)
	{
		strKriteria += "unit=" + selectUnitKerjaRKARutinFilter;
	}
	
	if(Ext.getCmp('cboTahunRKARutinFilter').getValue() != "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboTahunRKARutinFilter').getValue();
	}
	
	if(Ext.getCmp('chkFilterRKARutinFilter').getValue() == true)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "approve=1";
	}else{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "approve=0";
	}
	return strKriteria;
}

function getCriteriaGListDaftarPL2()
{
	var strKriteria = "";
	
	if(selectUnitKerjaTabPL2RKARutin !== undefined)
	{
		strKriteria += "unit=" + selectUnitKerjaTabPL2RKARutin;
	}
	
	if(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue())
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPL2RKARutin').getValue();
	}
		
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 1;
	
	if(Ext.getCmp('txtPrioritasPL2RKARutin').getValue())
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "prior=" + Ext.getCmp('txtPrioritasPL2RKARutin').getValue();
	}
	
	return strKriteria;
}

function RefreshDataGLDaftarPL2(thn_anggaran,kd_unit_kerja)
{
	console.log('get daftar kegiatan pengeluaran');
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_rkatr",
		params: {
			thn_anggaran : thn_anggaran,
			kd_unit_kerja: kd_unit_kerja,
			kd_jns_rkat_jrka :1, //pengeluaran
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data Daftar RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLDaftarRKARutinList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDaftarRKARutinList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDaftarRKARutinList.add(recs);
				gridListDaftarRKARutin.getView().refresh();
				getTotalBiayaTabDaftarRKAT2(); //fungsi menghitung total kegiatan
				getTotalBiayaTabDetailRKAT2(); //fungsi menghitung total kegiatan
				
			} else {
				ShowPesanError('Gagal menampilkan data  Daftar RAB', 'Error');
			};
		}
	});
	/* dsGLDaftarRKARutinList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKARutin, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viGListDaftarPL2',
				param: criteria
			} 
		}
	);
	return dsGLDaftarRKARutinList; */
};

function getCriteriaGListDetailPL2()
{
	var strKriteria = "";
	
	if(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue() !== "")
	{ 
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(); 
	}
	
	if(selectUnitKerjaTabPL2RKARutin !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectUnitKerjaTabPL2RKARutin; 
	}
	
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 1; 
	
	if(Ext.getCmp('txtPrioritasPL2RKARutin').getValue() !== "")
	{ 
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "prior=" + Ext.getCmp('txtPrioritasPL2RKARutin').getValue();
	}
	
	return strKriteria;
}

function RefreshDataGLDetailPL2(tahun_anggaran_ta,kd_unit_kerja,kd_jns_rkat_jrka,prioritas_rkatr)
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_rkatr_det",
		params: {
			tahun_anggaran_ta	:	tahun_anggaran_ta,
			kd_unit_kerja		:	kd_unit_kerja,
			kd_jns_rkat_jrka	:	kd_jns_rkat_jrka,
			prioritas_rkatr		:	prioritas_rkatr
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB detail !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLDetailRKARutinList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDetailRKARutinList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDetailRKARutinList.add(recs);
				gridListDetailRKARutin.getView().refresh();
				getTotalBiayaTabDetailRKAT2();
			} else {
				ShowPesanError('Gagal menampilkan data RAB detail', 'Error');
			};
		}
	});

};

function getCriteriaGListDaftarPN2(fromGrid, rowdata)
{
	var strKriteria = "";
	
	if(selectUnitKerjaTabPN2RKARutin !== undefined)
	{
		strKriteria += "unit=" + selectUnitKerjaTabPN2RKARutin;
	}
	
	if(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue())
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPN2RKARutin').getValue();
	}
		
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 2;
	
	return strKriteria;
}

function RefreshDataGLDaftarPN2(thn_anggaran,kd_unit_kerja)
{		
	console.log('get daftar kegiatan penerimaan');
	console.log(thn_anggaran,kd_unit_kerja);
	dsGLDaftarRKARutinList.removeAll();
	dsGLDetailRKARutinList.removeAll();
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_rkatr",
		params: {
			thn_anggaran 		: thn_anggaran,
			kd_unit_kerja		: kd_unit_kerja,
			kd_jns_rkat_jrka 	:2, //pengeluaran
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data Daftar RAB Penerimaan!', 'Error');
		},	 
		success: function(o) 
		{   
			dsGLDaftarRKARutinList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDaftarRKARutinList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDaftarRKARutinList.add(recs);
				gridListDaftarRKARutin.getView().refresh();
				getTotalBiayaTabDaftarRKAT2(); //fungsi menghitung total kegiatan
				console.log(dsGLDaftarRKARutinList);
			} else {
				ShowPesanError('Gagal menampilkan data  Daftar RAB', 'Error');
			};
		}
	});
};

function getCriteriaGListDetailPN2()
{
	var strKriteria = "";
	
	if(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue() !== "")
	{ 
		strKriteria += "tahun=" + Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(); 
	}
	
	if(selectUnitKerjaTabPN2RKARutin !== undefined)
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "unit=" + selectUnitKerjaTabPN2RKARutin; 
	}
	
	if(strKriteria != "")
	{
		strKriteria += "&";
	}
	strKriteria += "jnsrkat=" + 2; 
	
	if(Ext.getCmp('txtPrioritasPN2RKARutin').getValue() !== "")
	{ 
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "prior=" + Ext.getCmp('txtPrioritasPN2RKARutin').getValue();
	}
	
	return strKriteria;
}

function RefreshDataGLDetailPN2(tahun_anggaran_ta,kd_unit_kerja,kd_jns_rkat_jrka,prioritas_rkatr)
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_rkatr_det",
		params: {
			tahun_anggaran_ta	:	tahun_anggaran_ta,
			kd_unit_kerja		:	kd_unit_kerja,
			kd_jns_rkat_jrka	:	kd_jns_rkat_jrka,
			prioritas_rkatr		:	prioritas_rkatr
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data RAB Penerimaan detail !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLDetailRKARutinList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLDetailRKARutinList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLDetailRKARutinList.add(recs);
				gridListDetailRKARutin.getView().refresh();
				getTotalBiayaTabDetailRKAT2();
			} else {
				ShowPesanError('Gagal menampilkan data RAB Penerimaan detail', 'Error');
			};
		}
	});


	/* dsGLDetailRKARutinList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountRKARutin, 
				Sort: '', 
				Sortdir: 'ASC', 
				target: 'viGListDetailPN2',
				param: criteria
			} 
		}
	);
	return dsGLDetailRKARutinList; */
};
/** DATA */

function TambahBarisGLDetailRKARutin()
{
	var x=true;
	if (dsGLDetailRKARutinList.getCount() > 0)
	{		
		if(dsGLDetailRKARutinList.data.items[dsGLDetailRKARutinList.getCount()-1].data.no_akun_ak==='')
		{
			x=false;
		}
	}
	
	if (x === true)
	{
		if(tabActive == "PL2")
		{
			var p = new mRecordGLDetailRKARutin
			(
				{
					urut_rkatrdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPL2RKARutin,
					//NO_PROGRAM_PROG: Ext.getCmp('txtNoProgramPL2RKARutin').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPL2RKARutin').getValue(),
					kd_jns_rkat_jrka: 1,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatrdet: 1,
					biaya_satuan_rkatrdet: 0,
					jmlh_rkatrdet: 0,
					m1_rkatrdet: 0,
					m2_rkatrdet: 0,
					m3_rkatrdet: 0,
					m4_rkatrdet: 0,
					m5_rkatrdet: 0,
					m6_rkatrdet: 0,
					m7_rkatrdet: 0,
					m8_rkatrdet: 0,
					m9_rkatrdet: 0,
					m10_rkatrdet: 0,
					m11_rkatrdet: 0,
					m12_rkatrdet: 0,
					deskripsi_rkatrdet:''
				}
			);
		}
		else
		{
			var p = new mRecordGLDetailRKARutin
			(
				{
					urut_rkatrdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPN2RKARutin,
					//NO_PROGRAM_PROG: Ext.getCmp('txtNoProgramPN2RKARutin').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPN2RKARutin').getValue(),
					kd_jns_rkat_jrka: 2,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatrdet: 1,
					biaya_satuan_rkatrdet: 0,
					jmlh_rkatrdet: 0,
					m1_rkatrdet: 0,
					m2_rkatrdet: 0,
					m3_rkatrdet: 0,
					m4_rkatrdet: 0,
					m5_rkatrdet: 0,
					m6_rkatrdet: 0,
					m7_rkatrdet: 0,
					m8_rkatrdet: 0,
					m9_rkatrdet: 0,
					m10_rkatrdet: 0,
					m11_rkatrdet: 0,
					m12_rkatrdet: 0,
					deskripsi_rkatrdet:''
				}
			);
		}
		dsGLDetailRKARutinList.insert(dsGLDetailRKARutinList.getCount(), p);
		rowSelectedDetailRKARutin = dsGLDetailRKARutinList.getAt(CurrentGLDetailRKARutin.row);
	}
	console.log(p);
};

function AddNewRKARutin(tab)
{	
	// Ext.getCmp('cboTahunEntryPL2RKARutin').setValue('');
	// selectUnitKerjaTabPL2RKARutin='';
	if(tab == 'PL2')
	{	
		selectUnitKerjaTabPL2RKARutin='';
		dsGLDaftarRKARutinList.removeAll();
		dsGLDetailRKARutinList.removeAll();
		Ext.getCmp('txtPrioritasPL2RKARutin').setValue('');
		Ext.getCmp('txtaKegiatanPL2RKARutin').setValue('');
		//Tambahan reza
		Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setValue('');
		Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setReadOnly(false);
		//selectUnitKerjaTabPL2RKARutin = '';
		// Ext.get('cboTahunEntryPL2RKARutin').dom.value=gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1);
		Ext.getCmp('cboTahunEntryPL2RKARutin').setReadOnly(false);
		Ext.getCmp('txtJumPlafondPL2RKARutin').setValue('');
		Ext.getCmp('txtPengembanganPL2RKARutin').setValue('');
		Ext.getCmp('txtRutinPL2RKARutin').setValue('');
		Ext.getCmp('txtaStatusPL2_RKARutin').setValue('');
		Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue('');
		
		getTotalBiayaTabDaftarRKAT2();
		getTotalBiayaTabDetailRKAT2();
		
		//end tambahan

		disahkan = false;
	}
	else if(tab == 'PN2')
	{
		selectUnitKerjaTabPN2RKARutin='';
		Ext.getCmp('txtPrioritasPN2RKARutin').setValue('');
		Ext.getCmp('txtaKegiatanPN2RKARutin').setValue('');
		disahkan = false;
		//Tambahan reza
		Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setValue('');
		Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setReadOnly(false);
		//selectUnitKerjaTabPN2RKARutin = '';
		// Ext.get('cboTahunEntryPN2RKARutin').dom.value=gstrTahunAngg +'/'+(Ext.num(gstrTahunAngg)+1);
		Ext.getCmp('cboTahunEntryPN2RKARutin').setReadOnly(false);
		Ext.getCmp('txtJumPlafondPN2RKARutin').setValue('');
		Ext.getCmp('txtPengembanganPN2RKARutin').setValue('');
		Ext.getCmp('txtRutinPN2RKARutin').setValue('');
		Ext.getCmp('txtaStatusPN2_RKARutin').setValue('');
		Ext.getCmp('txtSisaPlafondPN2RKARutin').setValue('');
		
		//end tambahan
	}	
	dsGLDaftarRKARutinList.removeAll();
	dsGLDetailRKARutinList.removeAll();
	vAddNewRKARutin = true;
}

function initFormEntryRKARutin(rowdata)
{
	console.log(rowdata);
	vAddNewRKARutin = false;
	
	Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setReadOnly(true);
	Ext.getCmp('cboTahunEntryPL2RKARutin').setReadOnly(true);
	Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setReadOnly(true);
	Ext.getCmp('cboTahunEntryPN2RKARutin').setReadOnly(true);
	
	tabActive = "PL2";
	// TAB PENGELUARAN	
	Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setValue(rowdata.nama_unit);
	
	selectUnitKerjaTabPL2RKARutin = rowdata.kd_unit_kerja;
	Ext.getCmp('cboTahunEntryPL2RKARutin').setValue(rowdata.tahun_anggaran_ta);
	Ext.get('cboTahunEntryPL2RKARutin').dom.value=rowdata.tahun_anggaran_ta;
	
	Ext.getCmp('txtJumPlafondPL2RKARutin').setValue(formatCurrency(rowdata.plafondawal));
	Ext.getCmp('txtPengembanganPL2RKARutin').setValue(formatCurrency(rowdata.jumlah_rkatk));
	//Ext.getCmp('txtRutinPL2RKARutin').setValue(formatCurrency(rowdata.jumlah_rkatr));
	disahkan = rowdata.disahkan_rka;
	getTotalBiayaTabDaftarRKAT2(); 		
	
	if(rowdata.is_revisi == 1 ){
		Ext.getCmp("txtCheckPL2_RKARutin").setValue(true);  
	}else{
		Ext.getCmp("txtCheckPL2_RKARutin").setValue(false);  
	}
	var a = rowdata.jumlah_rkatk;
	var b = rowdata.jumlah_rkatr;
	var c = rowdata.plafondawal;
	var d = a + b;
	// var e = c - d;
	var e = c- b;
	console.log(e);
	//Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(formatCurrency(e));
	
	// RefreshDataGLDaftarPL2(getCriteriaGListDaftarPL2());
	console.log(rowdata.tahun_anggaran_ta,rowdata.kd_unit_kerja);
	RefreshDataGLDaftarPL2(rowdata.tahun_anggaran_ta,rowdata.kd_unit_kerja);
	
	// TAB PENERIMAAN
	Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setValue(rowdata.nama_unit);
	Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setReadOnly(true);
	selectUnitKerjaTabPN2RKARutin = rowdata.kd_unit_kerja;
	Ext.getCmp('cboTahunEntryPN2RKARutin').setValue(rowdata.tahun_anggaran_ta);
	Ext.getCmp('cboTahunEntryPN2RKARutin').setReadOnly(true);
	Ext.getCmp('txtJumPlafondPN2RKARutin').setValue(formatCurrency(rowdata.plafondawal));
	Ext.getCmp('txtPengembanganPN2RKARutin').setValue(formatCurrency(rowdata.jumlah_rkatk));
	Ext.getCmp('txtRutinPN2RKARutin').setValue(formatCurrency(rowdata.jumlah_rkatr));
	disahkan = rowdata.disahkan_rka;
	
	var a = rowdata.jumlah_rkatk;
	var b = rowdata.jumlah_rkatr;
	var c = rowdata.plafondawal;
	var d = a + b;
	var e = c - d;
	Ext.getCmp('txtSisaPlafondPN2RKARutin').setValue(formatCurrency(e));
	
	disahkan = rowdata.disahkan_rka;
	console.log(rowdata.disahkan_rka);
	if(disahkan == true && disahkan != "")
	{
		//tambahan ades untuk proses revisi/alih anggaran............
		// if (gUSER_REVISI_RKAT == false) 
		// {
			Ext.getCmp('btnadd_RKATRutin').setDisabled(true);
			Ext.getCmp('btnsave_RKATRutin').setDisabled(true);
			Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(true);
			Ext.getCmp('btnremove_RKATRutin').setDisabled(true);
			Ext.getCmp('btndetailadd_RKATRutin').setDisabled(true);
			Ext.getCmp('btndetailhapus_RKATRutin').setDisabled(true);
			
		// }				
		//end tambahan ades untuk proses revisi/alih anggaran............
		
		Ext.getCmp('txtaStatusPL2_RKARutin').setValue(" Telah Disahkan ");
		Ext.getCmp('txtaStatusPN2_RKARutin').setValue(" Telah Disahkan ");
	}
	else
	{
		//tambahan ades untuk proses revisi/alih anggaran............
		// if (gUSER_REVISI_RKAT == false) 
		// {	
			// Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setReadOnly(false);
			// Ext.getCmp('cboTahunEntryPL2RKARutin').setReadOnly(false);
			Ext.getCmp('btnadd_RKATRutin').setDisabled(false);
			Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
			Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);
			Ext.getCmp('btnremove_RKATRutin').setDisabled(false);
			Ext.getCmp('btndetailadd_RKATRutin').setDisabled(false);
			Ext.getCmp('btndetailhapus_RKATRutin').setDisabled(false);
		// }				
		//end tambahan ades untuk proses revisi/alih anggaran............
		
		Ext.getCmp('txtaStatusPL2_RKARutin').setValue(" Belum Disahkan ");
		Ext.getCmp('txtaStatusPN2_RKARutin').setValue(" Belum Disahkan ");
	}	
	
}

function initFormEntryFromDaftarRKARutin(rowdata)
{

	vAddNewRKARutin = false;
	tabActive = (rowdata.kd_jns_rkat_jrka == 1) ? "PL2" : "PN2";
	if(tabActive == "PL2")	
	{		
		// TAB PENGELUARAN			
		Ext.getCmp('tabFormEntryHeaderRKARutin').setActiveTab(Ext.getCmp('tabPL2FormEntryHeaderRKARutin'));
		Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setValue(rowdata.nama_unit);
		
		// Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setReadOnly(true);
		selectUnitKerjaTabPL2RKARutin = rowdata.kd_unit_kerja;
		Ext.getCmp('cboTahunEntryPL2RKARutin').setValue(rowdata.tahun_anggaran_ta);
		// Ext.getCmp('cboTahunEntryPL2RKARutin').setReadOnly(true);
		Ext.getCmp('txtPrioritasPL2RKARutin').setValue(rowdata.prioritas_rkatr);
		Ext.getCmp('txtaKegiatanPL2RKARutin').setValue(rowdata.kegiatan_rkatr);
		Ext.getCmp('txtCheckPL2_RKARutin').setValue(rowdata.is_revisi);
		jumlah = rowdata.jumlah
		RefreshDataGLDetailPL2(rowdata.tahun_anggaran_ta,rowdata.kd_unit_kerja,rowdata.kd_jns_rkat_jrka,rowdata.prioritas_rkatr);
		
	}
	else
	{
		// TAB PENERIMAAN		
		Ext.getCmp('tabFormEntryHeaderRKARutin').setActiveTab(Ext.getCmp('tabPN2FormEntryHeaderRKARutin'));
		Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setValue(rowdata.nama_unit);
		// Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setReadOnly(true);
		selectUnitKerjaTabPN2RKARutin = rowdata.kd_unit_kerja;
		Ext.getCmp('cboTahunEntryPN2RKARutin').setValue(rowdata.tahun_anggaran_ta);
		// Ext.getCmp('cboTahunEntryPN2RKARutin').setReadOnly(true);
		Ext.getCmp('txtPrioritasPN2RKARutin').setValue(rowdata.prioritas_rkatr);
		Ext.getCmp('txtaKegiatanPN2RKARutin').setValue(rowdata.kegiatan_rkatr);
		Ext.getCmp('txtCheckPN2_RKARutin').setValue(rowdata.is_revisi);
		RefreshDataGLDetailPN2(rowdata.tahun_anggaran_ta,rowdata.kd_unit_kerja,rowdata.kd_jns_rkat_jrka,rowdata.prioritas_rkatr);
		
	}	
}

function GetPlafondRKAT2(kd_jenis)
{
	if(kd_jenis == 1){
		 Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/anggaran_module/functionRAB/getPlafondByUnitKerja",
				params: 
				{
				  thn_anggaran		:Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
				  kd_unit_kerja 	:Ext.getCmp('cboUnitKerjaTabPL2RKARutin').getValue(),
				  kd_jns_plafond	:1 //rutin
				},
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);

					if (cst.success === true) 
					{
						Ext.getCmp('txtJumPlafondPL2RKARutin').setValue(formatCurrency(cst.Jumlah));
						Ext.getCmp('txtPengembanganPL2RKARutin').setValue(0);
						var sisa = cst.Jumlah - Ext.getCmp('txtRutinPL2RKARutin').getValue();
						Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(formatCurrency(sisa));
						RefreshDataGLDaftarPL2(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),Ext.getCmp('cboUnitKerjaTabPL2RKARutin').getValue());
		
					}
					else
					{
						Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(0);
					};
				}

			}
		);
	}else{
		 Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/anggaran_module/functionRAB/getPlafondByUnitKerja",
				params: 
				{
				  thn_anggaran		:Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
				  kd_unit_kerja 	:Ext.getCmp('cboUnitKerjaTabPN2RKARutin').getValue(),
				  kd_jns_plafond	:1 //rutin
				},
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);

					if (cst.success === true) 
					{
						// Ext.getCmp('txtJumPlafondPN2RKARutin').setValue(formatCurrency(cst.Jumlah));
						// Ext.getCmp('txtPengembanganPN2RKARutin').setValue(0);
						// var sisa = cst.Jumlah - Ext.getCmp('txtRutinPN2RKARutin').getValue();
						// Ext.getCmp('txtSisaPlafondPN2RKARutin').setValue(formatCurrency(sisa));
						RefreshDataGLDaftarPN2(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),Ext.getCmp('cboUnitKerjaTabPN2RKARutin').getValue());
		
					}
					else
					{
						Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(0);
					};
				}

			}
		);
	}
	
}

/* function getParamProsesGetPlafondRKAT2()
{
	var param = "";
	if(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue() != "")
	{
		param += "tahun=" + Ext.getCmp('cboTahunEntryPL2RKARutin').getValue()
	}	
	
	if(selectUnitKerjaTabPL2RKARutin !== undefined)
	{
		if(param != "")
		{
			param += "&";
		}
		param += "unit=" + selectUnitKerjaTabPL2RKARutin;
	}

	if(param != "")
	{
		param += "&";
	}
	param += "kdJns=" + 2; //kode Non Rutin di tabel ACC_JNS_PLAFOND	
	return param;
} */

function GetFormLookupAccountRKATRutin(str)
{
	if (vAddNewRKARutin === true)
	{
		if(tabActive == "PL2")
		{
			var p = new mRecordGLDetailRKARutin
			(
				{
					urut_rkatrdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPL2RKARutin,
					//NO_PROGRAM_PROG: Ext.getCmp('txtNoProgramPL2RKARutin').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPL2RKARutin').getValue(),
					kd_jns_rkat_jrka: 1,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatrdet: 1,
					biaya_satuan_rkatrdet: 0,
					jmlh_rkatrdet: 0,
					m1_rkatrdet: 0,
					m2_rkatrdet: 0,
					m3_rkatrdet: 0,
					m4_rkatrdet: 0,
					m5_rkatrdet: 0,
					m6_rkatrdet: 0,
					m7_rkatrdet: 0,
					m8_rkatrdet: 0,
					m9_rkatrdet: 0,
					m10_rkatrdet: 0,
					m11_rkatrdet: 0,
					m12_rkatrdet: 0,
					deskripsi_rkatrdet:''
				}
			);
		}
		else
		{
			var p = new mRecordGLDetailRKARutin
			(
				{
					urut_rkatrdet: '',
					tahun_anggaran_ta: Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
					kd_unit_kerja: selectUnitKerjaTabPN2RKARutin,
					//NO_PROGRAM_PROG: Ext.getCmp('txtNoProgramPN2RKARutin').getValue(),
					prioritas: Ext.getCmp('txtPrioritasPN2RKARutin').getValue(),
					kd_jns_rkat_jrka: 2,
					account: '',
					kd_satuan_sat: '',
					satuan_sat: '',
					name: '',
					kuantitas_rkatrdet: 1,
					biaya_satuan_rkatrdet: 0,
					jmlh_rkatrdet: 0,
					m1_rkatrdet: 0,
					m2_rkatrdet: 0,
					m3_rkatrdet: 0,
					m4_rkatrdet: 0,
					m5_rkatrdet: 0,
					m6_rkatrdet: 0,
					m7_rkatrdet: 0,
					m8_rkatrdet: 0,
					m9_rkatrdet: 0,
					m10_rkatrdet: 0,
					m11_rkatrdet: 0,
					m12_rkatrdet: 0,
					deskripsi_rkatrdet:''
				}
			);
		}
		FormLookupAccountRKAT(str,dsGLDetailRKARutinList,p,true,'',false);
	}
	else
	{
		var p = new mRecordGLDetailRKARutin
		(
			{					
				urut_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.urut_rkatrdet,
				tahun_anggaran_ta: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.tahun_anggaran_ta,
				kd_unit_kerja: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.kd_unit_kerja,
				//NO_PROGRAM_PROG: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.NO_PROGRAM_PROG,
				prioritas: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.prioritas,
				kd_jns_rkat_jrka: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.kd_jns_rkat_jrka,
				account: '',
				kd_satuan_sat: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.kd_satuan_sat,
				satuan_sat: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.satuan_sat,
				name: '',
				kuantitas_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.kuantitas_rkatrdet,
				biaya_satuan_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.biaya_satuan_rkatrdet,
				jmlh_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.jmlh_rkatrdet,
				m1_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m1_rkatrdet,
				m2_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m2_rkatrdet,
				m3_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m3_rkatrdet,
				m4_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m4_rkatrdet,
				m5_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m5_rkatrdet,
				m6_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m6_rkatrdet,
				m7_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m7_rkatrdet,
				m8_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m8_rkatrdet,
				m9_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m9_rkatrdet,
				m10_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m10_rkatrdet,
				m11_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m11_rkatrdet,
				m12_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.m12_rkatrdet,
				deskripsi_rkatrdet: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.deskripsi_rkatrdet
			}
		);
		FormLookupAccountRKAT(str,dsGLDetailRKARutinList,p,false,CurrentGLDetailRKARutin.row,false);
	}
};

function getTotalBiayaTabDaftarRKAT2()
{
	
	var total = 0;
	if(dsGLDaftarRKARutinList.getCount() > 0)
	{
		for(var i = 0;i < dsGLDaftarRKARutinList.getCount();i++)
		{
			
			if(isNaN(parseFloat(dsGLDaftarRKARutinList.data.items[i].data.jumlah))) {
				total = total + 0;
			}else{
				total = total + parseFloat(dsGLDaftarRKARutinList.data.items[i].data.jumlah);
			}
			
		}
	}
	
	/* INFORMASI TOTAL YANG DIBAWAH */
	Ext.getCmp('txtTotBiayaKegDaftarRKARutin').setValue(formatCurrency(total));	
	
	/* INFORMASI TOTAL RUTIN YANG DIATAS */
	Ext.getCmp('txtRutinPL2RKARutin').setValue(formatCurrency(total));
	
	/* INFORMASI SISA YANG DIATAS */
	var sisa = getNumber(Ext.getCmp('txtJumPlafondPL2RKARutin').getValue()) -  getNumber(Ext.getCmp('txtRutinPL2RKARutin').getValue());
	
	Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(formatCurrency(sisa));
}

function getTotalBiayaTabDetailRKAT2()
{
	var total = 0;
	if(dsGLDetailRKARutinList.getCount() > 0)
	{
		for(var i = 0;i < dsGLDetailRKARutinList.getCount();i++)
		{
			total = total + parseFloat(dsGLDetailRKARutinList.data.items[i].data.jmlh_rkatrdet);
		}
	}
	Ext.getCmp('txtTotBiayaKegDetailRKARutin').setValue(formatCurrency(total));
	/* Ext.getCmp('txtRutinPL2RKARutin').setValue(formatCurrency(total));
	
	var sisa = Ext.getCmp('txtJumPlafondPL2RKARutin').getValue() - Ext.getCmp('txtRutinPL2RKARutin').getValue();
	Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(formatCurrency(sisa)); */
}

function SaveDataPL2RKATRutin(isSaveExit,rowdata)
{
	if(ValidasiSaveDataPL2RKATRutin() == 1)
	{
		
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionRAB/saveRAB",
				params: getParamSavePL2RKATRutin(rowdata),
				failure: function(o)
				{
					ShowPesanError('Error menyimpan RAB !', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// ShowPesanInfo('Data RAB Berhasil Disimpan','Information');
						
						RefreshDataGLRKARutin();
						Ext.getCmp('txtPrioritasPL2RKARutin').setValue(cst.prioritas);
						
						if(cst.status_rab == 't' && cst.status_rab != ""){
							Ext.getCmp('txtaStatusPL2_RKARutin').setValue(" Telah Disahkan ");
						}else{
							Ext.getCmp('txtaStatusPL2_RKARutin').setValue(" Belum Disahkan ");
						}	
						
						RefreshDataGLDaftarPL2(cst.tahun_anggaran_ta,cst.kd_unit_kerja);
						RefreshDataGLDetailPL2(
							Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
							selectUnitKerjaTabPL2RKARutin,
							1,//pengeluaran
							Ext.getCmp('txtPrioritasPL2RKARutin').getValue()
						);	
						// Ext.getCmp('txtaKegiatanPL2RKARutin').setValue('');
						// Ext.getCmp('txtPrioritasPL2RKARutin').setValue('');
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Data RAB berhasil di simpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(isSaveExit == true){
											winFormEntryRKARutin.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);
						
					}
					else 
					{
						ShowPesanError('RAB gagal disimpan!', 'Error');
					};
				}
			}
			
		)
		/* if(vAddNewRKARutin === true)
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSavePL2RKATRutin(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
						Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);		

						if (cst.success === true) 
						{
							ShowPesanInfo('Data berhasil disimpan.', 'Simpan Data');

							RefreshDataGLDaftarPL2(getCriteriaGListDaftarPL2());
							RefreshDataGLDetailPL2(getCriteriaGListDetailPL2());
							
							if(isSaveExit != true)
							{ 
								vAddNewRKARutin = false; 
								Ext.getCmp('cboUnitKerjaTabPL2RKARutin').setReadOnly(true);
								Ext.getCmp('cboTahunEntryPL2RKARutin').setReadOnly(true);
							}
							else{ winFormEntryRKARutin.close(); }							
							
							RefreshDataGLRKARutin(getCriteriaGListRKARutin());
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di simpan, data tersebut sudah ada', 'Simpan Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
						}
					}
				}
			);
		}
		else
		{
			// alert(vAddNewRKARutin);
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSavePL2RKATRutin(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
						Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);		

						if (cst.success === true) 
						{
							ShowPesanInfo('Data berhasil di-edit.', 'Edit Data');
							
							RefreshDataGLDaftarPL2(getCriteriaGListDaftarPL2());
							RefreshDataGLDetailPL2(getCriteriaGListDetailPL2());
							if(isSaveExit){ winFormEntryRKARutin.close(); }
							
							RefreshDataGLRKARutin(getCriteriaGListRKARutin());
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di-edit, data tersebut sudah ada', 'Edit Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di-edit, Error : ' + cst.pesan, 'Edit Data');
						}
					}
				}
			);
		} */
	}
	else
	{
		Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
		Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);		
	}
}

function SaveDataPN2RKATRutin(isSaveExit)
{
	if(ValidasiSaveDataPN2RKATRutin() == 1)
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionRAB/saveRAB",
				params: getParamSavePN2RKATRutin(),
				failure: function(o)
				{
					ShowPesanError('Error menyimpan RAB !', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// ShowPesanInfo('Data RAB Berhasil Disimpan','Information');
						RefreshDataGLRKARutin();
						Ext.getCmp('txtPrioritasPN2RKARutin').setValue(cst.prioritas);
						if(cst.status_rab == 't' && cst.status_rab != ""){
							Ext.getCmp('txtaStatusPN2_RKARutin').setValue(" Telah Disahkan ");
						}else{
							Ext.getCmp('txtaStatusPN2_RKARutin').setValue(" Belum Disahkan ");
						}	
						RefreshDataGLDaftarPN2(cst.tahun_anggaran_ta,cst.kd_unit_kerja);
						RefreshDataGLDetailPN2(
							Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
							selectUnitKerjaTabPN2RKARutin,
							2,//penerimaan
							Ext.getCmp('txtPrioritasPN2RKARutin').getValue()
						);	
						
						Ext.getCmp('txtaKegiatanPN2RKARutin').setValue('');
						Ext.getCmp('txtPrioritasPN2RKARutin').setValue('');
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Data RAB berhasil di simpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(isSaveExit == true){
											winFormEntryRKARutin.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);
						
					}
					else 
					{
						ShowPesanError('RAB gagal disimpan!', 'Error');
					};
				}
			}
			
		)
	
		/* if(vAddNewRKARutin === true)
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSavePN2RKATRutin(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
						Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);		

						if (cst.success === true) 
						{
							ShowPesanInfo('Data berhasil disimpan.', 'Simpan Data');
							
							RefreshDataGLDaftarPN2(getCriteriaGListDaftarPN2());
							RefreshDataGLDetailPN2(getCriteriaGListDetailPN2());
							
							if(!isSaveExit)
							{ 
								vAddNewRKARutin = false; 
								Ext.getCmp('cboUnitKerjaTabPN2RKARutin').setReadOnly(true);
								Ext.getCmp('cboTahunEntryPN2RKARutin').setReadOnly(true);
							}
							else{ winFormEntryRKARutin.close(); }
							
							RefreshDataGLRKARutin(getCriteriaGListRKARutin());
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di simpan, data tersebut sudah ada', 'Simpan Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
						}
					}
				}
			);
		}
		else
		{
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: getParamSavePN2RKATRutin(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
						Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);		

						if (cst.success === true) 
						{
							ShowPesanInfo('Data berhasil di-edit.', 'Edit Data');
							
							RefreshDataGLDaftarPN2(getCriteriaGListDaftarPN2());
							RefreshDataGLDetailPN2(getCriteriaGListDetailPN2());
							if(isSaveExit){ winFormEntryRKARutin.close(); }
							RefreshDataGLRKARutin(getCriteriaGListRKARutin());
						}
						else if (cst.success === false && cst.pesan === 0) 
						{
							ShowPesanWarning('Data tidak berhasil di-edit, data tersebut sudah ada', 'Edit Data');
						}
						else 
						{
							ShowPesanError('Data tidak berhasil di-edit, Error : ' + cst.pesan, 'Edit Data');
						}
					}
				}
			);
		} */
	}
	else
	{
		Ext.getCmp('btnsave_RKATRutin').setDisabled(false);
		Ext.getCmp('btnsaveexit_RKATRutin').setDisabled(false);		
	}
}

function ValidasiSaveDataPL2RKATRutin()
{
	var valid = 1;
	// if(selectUnitKerjaTabPL2RKARutin === undefined)
	if(selectUnitKerjaTabPL2RKARutin === undefined || selectUnitKerjaTabPL2RKARutin == '')
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		// valid -= 1;
		valid=0;
	}else if(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		// valid -= 1;
		valid=0;
	}else if(Ext.getCmp('txtaKegiatanPL2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		// valid -= 1;	
		valid=0;	
	}

		// validasi saving detail
	else if(tabDetActive == 1)
	{
		if(dsGLDetailRKARutinList.getCount() <=0)
		{
			ShowPesanWarning("Transaksi harus memiliki detail.", "Simpan Data");
			valid -= 1;
		}
		
		var d = getNumber(Ext.getCmp('txtSisaPlafondPL2RKARutin').getValue());
		//alert(Ext.getCmp('txtSisaPlafondPL2RKARutin').getValue());
		//alert(getNumber(Ext.getCmp('txtSisaPlafondPL2RKARutin').getValue()));
		if(d >= 0 || d == undefined)
		{
			valid = 1;
		}else{
			ShowPesanWarning("Jumlah plafond telah habis atau melebihi plafond yg ada.", "Simpan Data");
			valid = 0;
		}
	}else{
		var d = getNumber(Ext.getCmp('txtSisaPlafondPL2RKARutin').getValue());
		if(d >= 0 || d == undefined)
		{
			valid = 1;
		}else{
			ShowPesanWarning("Jumlah plafond telah habis atau melebihi plafond yg ada.", "Simpan Data");
			valid = 0;
		}
	}
	
	//validasi Detail
	if(dsGLDetailRKARutinList.getCount() > 0)
	{
		var m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,jumlah,total_m
		for(var i=0; i<dsGLDetailRKARutinList.getCount(); i++)
		{
			//M1 = dsGLDetailRKARutinList.data.items[i].data.M1;
			//M2 = dsGLDetailRKARutinList.data.items[i].data.M2;
			//M3 = dsGLDetailRKARutinList.data.items[i].data.M3;
			//M4 = dsGLDetailRKARutinList.data.items[i].data.M4;
			//M5 = dsGLDetailRKARutinList.data.items[i].data.M5;
			//M6 = dsGLDetailRKARutinList.data.items[i].data.M6;
			//M7 = dsGLDetailRKARutinList.data.items[i].data.M7;
			//M8 = dsGLDetailRKARutinList.data.items[i].data.M8;
			//M9 = dsGLDetailRKARutinList.data.items[i].data.M9;
			//M10 = dsGLDetailRKARutinList.data.items[i].data.M10;
			//M11 = dsGLDetailRKARutinList.data.items[i].data.M11;
			//M12 = dsGLDetailRKARutinList.data.items[i].data.M12;
			
			if (dsGLDetailRKARutinList.data.items[i].data.m1_rkatrdet == "") { m1 = 0}
			else{m1 =parseFloat( dsGLDetailRKARutinList.data.items[i].data.m1_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m2_rkatrdet == "") { m2 = 0}
			else{m2 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m2_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m3_rkatrdet == "") { m3 = 0}
			else{m3 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m3_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m4_rkatrdet == "") { m4 = 0}
			else{m4 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m4_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m5_rkatrdet == "") { m5 = 0}
			else{m5 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m5_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m6_rkatrdet == "") { m6 = 0}
			else{m6 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m6_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m7_rkatrdet == "") { m7 = 0}
			else{m7 =parseFloat(dsGLDetailRKARutinList.data.items[i].data.m7_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m8_rkatrdet == "") { m8 = 0}
			else{m8 =parseFloat( dsGLDetailRKARutinList.data.items[i].data.m8_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m9_rkatrdet == "") { m9 = 0}
			else{m9 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m9_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m10_rkatrdet == "") { m10 = 0}
			else{m10 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m10_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m11_rkatrdet == "") { m11 = 0}
			else{m11 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m11_rkatrdet);}
			if (dsGLDetailRKARutinList.data.items[i].data.m12_rkatrdet == "") { m12 = 0}
			else{m12 = parseFloat(dsGLDetailRKARutinList.data.items[i].data.m12_rkatrdet);}
			jumlah = parseFloat(dsGLDetailRKARutinList.data.items[i].data.jmlh_rkatrdet);
			
			total_m =parseFloat( m1+m2+m3+m4+m5+m6+m7+m8+m9+m10+m11+m12);
			console.log(total_m);
			if (total_m != jumlah)
			{			
				// ShowPesanWarning("anggaran per bulan melebihi jumlah anggaran " + formatCurrency(JUMLAH)  + " baris : " + (i+1), "Simpan Data");
				ShowPesanWarning("Total anggaran per bulan tidak sesuai, total anggaran per bulan : " + formatCurrency(total_m)+ " Baris " + (i+1), "Simpan Data");
				valid = 0;
			}
			if( dsGLDetailRKARutinList.data.items[i].data.kd_satuan_sat === ""){
				ShowPesanWarning("Satuan belum di isi", "Simpan Data");
				valid = 0;
			}
		}
	}
	

	
	return valid;
}

function ValidasiSaveDataPN2RKATRutin()
{
	var valid = 1;

	if(selectUnitKerjaTabPN2RKARutin === undefined)
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid -= 1;
	}else if(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	/* if(Ext.getCmp('txtPrioritasPN2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Prioritas belum ditentukan.", "Simpan Data");
		valid -= 1;
	} */
	
	else if(Ext.getCmp('txtaKegiatanPN2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid -= 1;		
	}
	// validasi saving detail
	else if(tabDetActive == 1)
	{
		if(dsGLDetailRKARutinList.getCount() <=0)
		{
			ShowPesanWarning("Transaksi harus memiliki detail.", "Simpan Data");
			valid -= 1;
		}
	}
	
	//validasi Detail
	else if(dsGLDetailRKARutinList.getCount() > 0)
	{
		var M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12,JUMLAH,TOTAL_M
		for(var i=0; i<dsGLDetailRKARutinList.getCount(); i++)
		{
			M1 = dsGLDetailRKARutinList.data.items[i].data.M1;
			M2 = dsGLDetailRKARutinList.data.items[i].data.M2;
			M3 = dsGLDetailRKARutinList.data.items[i].data.M3;
			M4 = dsGLDetailRKARutinList.data.items[i].data.M4;
			M5 = dsGLDetailRKARutinList.data.items[i].data.M5;
			M6 = dsGLDetailRKARutinList.data.items[i].data.M6;
			M7 = dsGLDetailRKARutinList.data.items[i].data.M7;
			M8 = dsGLDetailRKARutinList.data.items[i].data.M8;
			M9 = dsGLDetailRKARutinList.data.items[i].data.M9;
			M10 = dsGLDetailRKARutinList.data.items[i].data.M10;
			M11 = dsGLDetailRKARutinList.data.items[i].data.M11;
			M12 = dsGLDetailRKARutinList.data.items[i].data.M12;
			JUMLAH = dsGLDetailRKARutinList.data.items[i].data.JUMLAH;
			
			TOTAL_M = (M1+M2)+(M3+M4)+(M5+M6)+(M7+M8)+(M9+M10)+(M11+M12);
			
			if (TOTAL_M > JUMLAH){
				ShowPesanWarning("anggaran per bulan melebihi jumlah anggaran " + formatCurrency(JUMLAH), "Simpan Data");
				valid = 0;
			}
			if( dsGLDetailRKARutinList.data.items[i].data.KD_SATUAN_SAT == ""){
				ShowPesanWarning("Satuan belum di isi", "Simpan Data");
				valid = 0;
			}
		}
	}
	
	
	
	
	
	return valid;
}

function getParamSavePL2RKATRutin(rowdata)
{
	
	var tmp_thn_anggaran 	= '';
	var tmp_kd_unit_kerja 	= '';
	var revisidata;
	
	if(rowdata == undefined){
		tmp_thn_anggaran 	= Ext.getCmp('cboTahunEntryPL2RKARutin').getValue();
		tmp_kd_unit_kerja 	= selectUnitKerjaTabPL2RKARutin;
	}else{
		tmp_thn_anggaran 	= rowdata.tahun_anggaran_ta;
		tmp_kd_unit_kerja 	= rowdata.kd_unit_kerja;
	}
	
	
	if(Ext.getCmp('txtCheckPL2_RKARutin').getValue() == true){
		revisidata = 1;
	}else{
		revisidata = 0;
	}
	
	if( disahkan == '' || disahkan == undefined ){
		disahkan = 'f';
	}
	
	var params = {
		/* ACC_RKAT */
		thn_anggaran 		:	Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
		kd_unit_kerja 		:	selectUnitKerjaTabPL2RKARutin,
		disahkan_rka		:	disahkan,
		jumlah_rkatr		:	getNumber(Ext.getCmp('txtRutinPL2RKARutin').getValue()),
		
		/* ACC_RKATR */
		kd_jns_rkat_jrka	:	1, //pengeluaran
		kegiatan_rkatr		:	Ext.getCmp('txtaKegiatanPL2RKARutin').getValue(),
		revisi				: 	revisidata,
		prioritas			:	Ext.getCmp('txtPrioritasPL2RKARutin').getValue(),
		
		param_thn_anggaran 		: tmp_thn_anggaran,
		param_kd_unit_kerja 	: tmp_kd_unit_kerja,
	};
	
	/* ACC_RKATR_DET */
	params['jumlah_baris']=dsGLDetailRKARutinList.getCount();
	for(var i = 0 ; i < dsGLDetailRKARutinList.getCount();i++)
	{
		
		params['urut_rkatrdet-'+i]=dsGLDetailRKARutinList.data.items[i].data.urut_rkatrdet
		params['kd_satuan_sat-'+i]=dsGLDetailRKARutinList.data.items[i].data.kd_satuan_sat
		params['account-'+i]=dsGLDetailRKARutinList.data.items[i].data.account
		params['qty-'+i]=dsGLDetailRKARutinList.data.items[i].data.kuantitas_rkatrdet
		params['biaya_satuan-'+i]=dsGLDetailRKARutinList.data.items[i].data.biaya_satuan_rkatrdet
		params['jumlah-'+i]=dsGLDetailRKARutinList.data.items[i].data.jmlh_rkatrdet
		params['m1-'+i]=dsGLDetailRKARutinList.data.items[i].data.m1_rkatrdet
		params['m2-'+i]=dsGLDetailRKARutinList.data.items[i].data.m2_rkatrdet
		params['m3-'+i]=dsGLDetailRKARutinList.data.items[i].data.m3_rkatrdet
		params['m4-'+i]=dsGLDetailRKARutinList.data.items[i].data.m4_rkatrdet
		params['m5-'+i]=dsGLDetailRKARutinList.data.items[i].data.m5_rkatrdet
		params['m6-'+i]=dsGLDetailRKARutinList.data.items[i].data.m6_rkatrdet
		params['m7-'+i]=dsGLDetailRKARutinList.data.items[i].data.m7_rkatrdet
		params['m8-'+i]=dsGLDetailRKARutinList.data.items[i].data.m8_rkatrdet
		params['m9-'+i]=dsGLDetailRKARutinList.data.items[i].data.m9_rkatrdet
		params['m10-'+i]=dsGLDetailRKARutinList.data.items[i].data.m10_rkatrdet
		params['m11-'+i]=dsGLDetailRKARutinList.data.items[i].data.m11_rkatrdet
		params['m12-'+i]=dsGLDetailRKARutinList.data.items[i].data.m12_rkatrdet
		params['deskripsi_rkatrdet-'+i]=dsGLDetailRKARutinList.data.items[i].data.deskripsi_rkatrdet
		
	}
	return params;
}

function getParamSavePN2RKATRutin()
{
	
	var revisidata;
	if(Ext.getCmp('txtCheckPN2_RKARutin').getValue() == true){
		revisidata = 1;
	}else{
		revisidata = 0;
	}
	
	if( disahkan == '' || disahkan == undefined ){
		disahkan = 'f';
	}
	
	var params = {
		/* ACC_RKAT */
		thn_anggaran 		:	Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
		kd_unit_kerja 		:	selectUnitKerjaTabPN2RKARutin,
		disahkan_rka		:	disahkan,
		jumlah_rkatr		:	'',
		
		/* ACC_RKATR */
		kd_jns_rkat_jrka	:	2, //pengeluaran
		kegiatan_rkatr		:	Ext.getCmp('txtaKegiatanPN2RKARutin').getValue(),
		revisi				: 	revisidata,
		prioritas			:	Ext.getCmp('txtPrioritasPN2RKARutin').getValue()
		
	};
	
	/* ACC_RKATR_DET */
	params['jumlah_baris']=dsGLDetailRKARutinList.getCount();
	for(var i = 0 ; i < dsGLDetailRKARutinList.getCount();i++)
	{
		
		params['urut_rkatrdet-'+i]=dsGLDetailRKARutinList.data.items[i].data.urut_rkatrdet
		params['kd_satuan_sat-'+i]=dsGLDetailRKARutinList.data.items[i].data.kd_satuan_sat
		params['account-'+i]=dsGLDetailRKARutinList.data.items[i].data.account
		params['qty-'+i]=dsGLDetailRKARutinList.data.items[i].data.kuantitas_rkatrdet
		params['biaya_satuan-'+i]=dsGLDetailRKARutinList.data.items[i].data.biaya_satuan_rkatrdet
		params['jumlah-'+i]=dsGLDetailRKARutinList.data.items[i].data.jmlh_rkatrdet
		params['m1-'+i]=dsGLDetailRKARutinList.data.items[i].data.m1_rkatrdet
		params['m2-'+i]=dsGLDetailRKARutinList.data.items[i].data.m2_rkatrdet
		params['m3-'+i]=dsGLDetailRKARutinList.data.items[i].data.m3_rkatrdet
		params['m4-'+i]=dsGLDetailRKARutinList.data.items[i].data.m4_rkatrdet
		params['m5-'+i]=dsGLDetailRKARutinList.data.items[i].data.m5_rkatrdet
		params['m6-'+i]=dsGLDetailRKARutinList.data.items[i].data.m6_rkatrdet
		params['m7-'+i]=dsGLDetailRKARutinList.data.items[i].data.m7_rkatrdet
		params['m8-'+i]=dsGLDetailRKARutinList.data.items[i].data.m8_rkatrdet
		params['m9-'+i]=dsGLDetailRKARutinList.data.items[i].data.m9_rkatrdet
		params['m10-'+i]=dsGLDetailRKARutinList.data.items[i].data.m10_rkatrdet
		params['m11-'+i]=dsGLDetailRKARutinList.data.items[i].data.m11_rkatrdet
		params['m12-'+i]=dsGLDetailRKARutinList.data.items[i].data.m12_rkatrdet
		params['deskripsi_rkatrdet-'+i]=dsGLDetailRKARutinList.data.items[i].data.deskripsi_rkatrdet
		
	}
	/* var revisidata;
	if(Ext.getCmp('txtCheckPN2_RKARutin').getValue() == true){
		revisidata = 1;
	}else{
		revisidata = 0;
	}
	var params = {
		Table: 'viRKAT2',
		rkat: 'PN2',
		isDetail: (tabDetActive == 1) ? true : false,
		unit: selectUnitKerjaTabPN2RKARutin,
		tahun: Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
		jnsrkat: 2, // 2 untuk penerimaan
		prior: Ext.getCmp('txtPrioritasPN2RKARutin').getValue(),
		//jumlah: Ext.getCmp('txtTotBiayaKegDetailRKARutin').getValue(),
		sisaRKATRutin: Ext.getCmp('txtTotBiayaKegDaftarRKARutin').getValue(),
		sisaRKATPengembangan: Ext.getCmp('txtPengembanganPL2RKARutin').getValue(),
		jumlahAnggaran: Ext.get('txtJumPlafondPN2RKARutin').getValue(),
		approve: disahkan,
		keg: Ext.getCmp('txtaKegiatanPN2RKARutin').getValue(),
		revisi: revisidata,
		lists: getListDetailRKATRutin(),
		nrow: dsGLDetailRKARutinList.getCount(),
		nfield: 24,//23,
	}; */
	
	return params;
}

function getListDetailRKATRutin()
{
	
	/* var x = "";
	var y = "";
	var	z = "@@##$$@@";
	
	if(dsGLDetailRKARutinList.getCount() > 0)
	{
		for(var i=0; i<dsGLDetailRKARutinList.getCount(); i++)
		{
			y = "col_line=" + dsGLDetailRKARutinList.data.items[i].data.LINE;
			y += z + "col_unit=" + dsGLDetailRKARutinList.data.items[i].data.KD_UNIT_KERJA;
			y += z + "col_tahun=" + dsGLDetailRKARutinList.data.items[i].data.TAHUN_ANGGARAN_TA;
			//y += z + "col_noprog=" + dsGLDetailRKARutinList.data.items[i].data.NO_PROGRAM_PROG;
			y += z + "col_prior=" + dsGLDetailRKARutinList.data.items[i].data.PRIORITAS;
			y += z + "col_jnsrkat=" + dsGLDetailRKARutinList.data.items[i].data.KD_JNS_RKAT_JRKA;
			y += z + "col_satuan=" + dsGLDetailRKARutinList.data.items[i].data.KD_SATUAN_SAT;
			y += z + "col_akun=" + dsGLDetailRKARutinList.data.items[i].data.ACCOUNT;
			y += z + "col_qty=" + dsGLDetailRKARutinList.data.items[i].data.QTY;
			y += z + "col_biaya=" + dsGLDetailRKARutinList.data.items[i].data.BIAYA_SATUAN;
			y += z + "col_jumlah=" + dsGLDetailRKARutinList.data.items[i].data.JUMLAH;
			y += z + "col_m1=" + dsGLDetailRKARutinList.data.items[i].data.M1;
			y += z + "col_m2=" + dsGLDetailRKARutinList.data.items[i].data.M2;
			y += z + "col_m3=" + dsGLDetailRKARutinList.data.items[i].data.M3;
			y += z + "col_m4=" + dsGLDetailRKARutinList.data.items[i].data.M4;
			y += z + "col_m5=" + dsGLDetailRKARutinList.data.items[i].data.M5;
			y += z + "col_m6=" + dsGLDetailRKARutinList.data.items[i].data.M6;
			y += z + "col_m7=" + dsGLDetailRKARutinList.data.items[i].data.M7;
			y += z + "col_m8=" + dsGLDetailRKARutinList.data.items[i].data.M8;
			y += z + "col_m9=" + dsGLDetailRKARutinList.data.items[i].data.M9;
			y += z + "col_m10=" + dsGLDetailRKARutinList.data.items[i].data.M10;
			y += z + "col_m11=" + dsGLDetailRKARutinList.data.items[i].data.M11;
			y += z + "col_m12=" + dsGLDetailRKARutinList.data.items[i].data.M12;
			y += z + "col_deskripsi=" + dsGLDetailRKARutinList.data.items[i].data.DESKRIPSI_RKATRDET;
			y += z + "col_acountemp=" + dsGLDetailRKARutinList.data.items[i].data.ACCOUNT_TEMP;
			
			if(i == dsGLDetailRKARutinList.getCount() - 1)
			{
				x += y;
			}
			else
			{
				x += y + "##[[]]##";
			}
		}
	}
	
	return x; */
}

function HapusBarisDetailRKARutin()
{
	var line = gridListDetailRKARutin.getSelectionModel().selection.cell[0];
	var o = dsGLDetailRKARutinList.getRange()[line].data;
	console.log(dsGLDetailRKARutinList.getRange()[line].data.urut_rkatrdet);
	console.log(o);
	
	var prioritas_rkatr='';
	if(o.kd_jns_rkat_jrka == 1){
		prioritas_rkatr = Ext.getCmp('txtPrioritasPL2RKARutin').getValue();
	}else{
		prioritas_rkatr = Ext.getCmp('txtPrioritasPN2RKARutin').getValue();
	}
	Ext.Msg.confirm('Warning', 'Apakah data detail RAB  ini akan dihapus?', function(button){
		if (button == 'yes'){
			if( dsGLDetailRKARutinList.getRange()[line].data.urut_rkatrdet !== ''){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRAB/hapusDetailRAB",
						params:{
							
							tahun_anggaran_ta	:o.tahun_anggaran_ta,
							kd_unit_kerja		:o.kd_unit_kerja,
							kd_jns_rkat_jrka	:o.kd_jns_rkat_jrka,
							prioritas_rkatr		:prioritas_rkatr,
							urut_rkatrdet		:o.urut_rkatrdet,
							jml_rkatr_det		:o.jmlh_rkatrdet
						} ,
						failure: function(o)
						{
							ShowPesanError('Error menghapus detail RAB!', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsGLDetailRKARutinList.removeAt(line);
								gridListDetailRKARutin.getView().refresh();
								ShowPesanInfo('Data detail RAB berhasil dihapus','Information');
								getTotalBiayaTabDetailRKAT2();
								RefreshDataGLDaftarPL2(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),selectUnitKerjaTabPL2RKARutin); 
							}
							else 
							{
								ShowPesanError('Gagal menghapus detail RAB!', 'Error');
							};
						}
					}
					
				)
			}else{
				dsGLDetailRKARutinList.removeAt(line);
				gridListDetailRKARutin.getView().refresh();
				ShowPesanInfo('Data detail RAB berhasil dihapus','Information');
				getTotalBiayaTabDetailRKAT2();
				RefreshDataGLDaftarPL2(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),selectUnitKerjaTabPL2RKARutin); 
			}
		} 
		
	});
	
}

function getParamHapusDetailRKATRutin()
{
	var params = {
		Table: 'viRKAT2',
		isRow: true,
		approve: disahkan,
		line: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.LINE,
		unit: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.KD_UNIT_KERJA,
		tahun: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.TAHUN_ANGGARAN_TA,
		//noprog: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.NO_PROGRAM_PROG,
		prior: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.PRIORITAS,
		jnsrkat: (tabActive == "PL2") ? 1 : 2,
		akun: dsGLDetailRKARutinList.data.items[CurrentGLDetailRKARutin.row].data.ACCOUNT,
		sisaRKATRutin: Ext.getCmp('txtRutinPL2RKARutin').getValue()
	};

	return params;
}

function HapusDataPL2RKATRutin()
{
	var jumlah = rowSelectedDaftarRKARutin.data.jumlah;
	console.log(jumlah);
	Ext.Msg.confirm('Warning', 'Apakah data  RAB  ini akan dihapus?', function(button){
		if (button == 'yes'){
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/anggaran_module/functionRAB/hapusRAB",
					params:{
						
						tahun_anggaran_ta	: Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
						kd_unit_kerja		: selectUnitKerjaTabPL2RKARutin,
						kd_jns_rkat_jrka	: 1, //pengeluaran
						prioritas_rkatr		: Ext.getCmp('txtPrioritasPL2RKARutin').getValue(),
						jumlah				: jumlah
					} ,
					failure: function(o)
					{
						ShowPesanError('Error menghapus  RAB!', 'Error');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							RefreshDataGLRKARutin();
							// AddNewRKARutin('PL2');
							Ext.getCmp('txtaKegiatanPL2RKARutin').setValue('');
							Ext.getCmp('txtPrioritasPL2RKARutin').setValue('');
							RefreshDataGLDaftarPL2(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),selectUnitKerjaTabPL2RKARutin);
							ShowPesanInfo('Data  RAB berhasil dihapus','Information');
							
						}
						else 
						{
							ShowPesanError('Gagal menghapus  RAB!', 'Error');
						};
					}
				}
				
			)
		} 
		
	});
}

function getParamDeletePL2RKATRutin()
{
	var params = {
		Table: 'viRKAT2',
		isrow: false,
		approve: disahkan,
		unit: selectUnitKerjaTabPL2RKARutin,
		tahun: Ext.getCmp('cboTahunEntryPL2RKARutin').getValue(),
		jnsrkat: 1, // 1 untuk pengeluaran
		prior: Ext.getCmp('txtPrioritasPL2RKARutin').getValue(),
		//noprog: Ext.getCmp('txtNoProgramPL2RKARutin').getValue(),
		keg: Ext.getCmp('txtaKegiatanPL2RKARutin').getValue(),
		lists: getListDetailRKATRutin(),
		nrow: dsGLDetailRKARutinList.getCount(),
		nfield: 23
	};
	
	return params;
}

function ValidasiDeleteDataPL2RKATRutin()
{
	var valid = 1;
	
	if(selectUnitKerjaTabPL2RKARutin === undefined)
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('cboTahunEntryPL2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtPrioritasPL2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Prioritas belum ditentukan.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtaKegiatanPL2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid -= 1;		
	}
	
	return valid;
}

function HapusDataPN2RKATRutin()
{
	if(ValidasiDeleteDataPN2RKATRutin() === 1)
	{
		
		Ext.Msg.confirm('Warning', 'Apakah data  RAB  ini akan dihapus?', function(button){
			if (button == 'yes'){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRAB/hapusRAB",
						params:{
							
							tahun_anggaran_ta	: Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
							kd_unit_kerja		: selectUnitKerjaTabPN2RKARutin,
							kd_jns_rkat_jrka	: 2, //pengeluaran
							prioritas_rkatr		: Ext.getCmp('txtPrioritasPN2RKARutin').getValue(),
							jumlah				: 0
						} ,
						failure: function(o)
						{
							ShowPesanError('Error menghapus  RAB!', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								RefreshDataGLRKARutin();
								// AddNewRKARutin('PL2');
								RefreshDataGLDaftarPN2(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),selectUnitKerjaTabPN2RKARutin);
								ShowPesanInfo('Data  RAB berhasil dihapus','Information');
								Ext.getCmp('txtaKegiatanPN2RKARutin').setValue('');
								Ext.getCmp('txtPrioritasPN2RKARutin').setValue('');
							}
							else 
							{
								ShowPesanError('Gagal menghapus  RAB!', 'Error');
							};
						}
					}
				)
			} 
		});
		/* Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params: getParamDeletePN2RKATRutin(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success) 
					{					
						ShowPesanInfo('Data berhasil dihapus','Hapus Data');
						AddNewRKARutin(tabActive);						
						RefreshDataGLDaftarPN2(getCriteriaGListDaftarPN2());
						RefreshDataGLDetailPN2(getCriteriaGListDetailPN2());
						
						// RefreshDataGLRKARutin(getCriteriaGListRKARutin());
					}
					else if (cst.success && (cst.pesan === 0))
					{
						ShowPesanWarning('Data tidak berhasil dihapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError('Data tidak berhasil dihapus','Hapus Data');
					}
				}
			}
		); */
	}
}

function getParamDeletePN2RKATRutin()
{
	var params = {
		Table: 'viRKAT2',
		isrow: false,
		approve: disahkan,
		unit: selectUnitKerjaTabPN2RKARutin,
		tahun: Ext.getCmp('cboTahunEntryPN2RKARutin').getValue(),
		jnsrkat: 2, // 2 untuk penerimaan
		prior: Ext.getCmp('txtPrioritasPN2RKARutin').getValue(),
		//noprog: Ext.getCmp('txtNoProgramPN2RKARutin').getValue(),
		keg: Ext.getCmp('txtaKegiatanPN2RKARutin').getValue(),
		lists: getListDetailRKATRutin(),
		nrow: dsGLDetailRKARutinList.getCount(),
		nfield: 23
	};
	
	return params;
}

function ValidasiDeleteDataPN2RKATRutin()
{
	var valid = 1;
	
	if(selectUnitKerjaTabPN2RKARutin === undefined)
	{
		ShowPesanWarning(gstrSatker+" belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('cboTahunEntryPN2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Tahun anggaran belum dipilih.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtPrioritasPN2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Prioritas belum ditentukan.", "Simpan Data");
		valid -= 1;
	}
	
	if(Ext.getCmp('txtaKegiatanPN2RKARutin').getValue() === "")
	{
		ShowPesanWarning("Kegiatan belum ditentukan.", "Simpan Data");
		valid -= 1;		
	}
	
	return valid;
}

/* AKUMULASI */
/* function totalSisadaftar_RKATRutin(){
	
	var b = getTotalBiayaTabDetailRKAT2();
	var c = getNumber(Ext.getCmp('txtJumPlafondPL2RKARutin').getValue()) - getNumber(Ext.getCmp('txtTotBiayaKegDaftarRKARutin').getValue()); // jumlah entry detail baru
	if(c < 0)
	{
		ShowPesanWarning("Jumlah detail melebihi sisa plafond yang ada.", "Simpan Data");
		valid = 0;
	}
	return valid;
}

function totalSisaDetail_RKATRutin(){
	
	var b = getTotalBiayaTabDetailRKAT2();
	var c = b - getNumber(Ext.getCmp('txtSisaPlafondPL2RKARutin').getValue());
	if(c < 0)
	{
		ShowPesanWarning("Jumlah detail melebihi sisa plafond yang ada.", "Simpan Data");
		valid = 0;
	}
	return valid;
} */

/* function getTotalBiayaTabDaftarRKAT2() // Total Daftar
{
	var total = 0;
	if(dsGLDaftarRKARutinList.getCount() > 0)
	{
		dsGLDaftarRKARutinList
		for(var i = 0;i < dsGLDaftarRKARutinList.getCount();i++)
		{
			total = total + parseFloat(dsGLDaftarRKARutinList.data.items[i].data.jumlah);
			console.log(parseFloat(dsGLDaftarRKARutinList.data.items[i].data.jumlah));
		}
	}
	Ext.getCmp('txtTotBiayaKegDaftarRKARutin').setValue(formatCurrency(total));	
	
} */

/* function getTotalBiayaTabDetailRKAT2() //Total Detail
{
	var total = 0;
	if(dsGLDetailRKARutinList.getCount() > 0)
	{
		for(var i = 0;i < dsGLDetailRKARutinList.getCount();i++)
		{
			total = total + parseFloat(dsGLDetailRKARutinList.data.items[i].data.jmlh_rkatrdet);
		}
	}
	Ext.getCmp('txtTotBiayaKegDetailRKARutin').setValue(formatCurrency(total));
} */

//kalkulasi effect blur di detail
function getTotalRutin_RKARutin(nilaiDelete){
	
	/* GET NILAI RUTIN */
	var plafon = getNumber(Ext.get('txtJumPlafondPL2RKARutin').getValue());
	// var rutin = getNumber(Ext.get('txtRutinPL2RKARutin').getValue()) + getNumber(Ext.get('txtTotBiayaKegDetailRKARutin').getValue()); 
	var rutin = getNumber(Ext.get('txtTotBiayaKegDetailRKARutin').getValue()); 
	
	var sisa = plafon - rutin;
	
	Ext.getCmp('txtRutinPL2RKARutin').setValue(formatCurrency(rutin));
	Ext.getCmp('txtSisaPlafondPL2RKARutin').setValue(formatCurrency(sisa));
	
	//Rutin
	/* var plafon = getNumber(Ext.get('txtJumPlafondPL2RKARutin').getValue());
	var det2 = getNumber(Ext.get('txtTotBiayaKegDetailRKARutin').getValue());
	var pengembangan = getNumber(Ext.get('txtPengembanganPL2RKARutin').getValue());
	var daf = getNumber(Ext.get('txtTotBiayaKegDaftarRKARutin').getValue());
	
	if(nilaiDelete == false){
		//set value anggaran
		
		var totalRutin = jumlah - det2;
		var anggaranRutin = daf - totalRutin;
		//Ext.get('txtRutinPL2RKARutin').dom.value = formatCurrency(anggaranRutin); 
		Ext.get('txtRutinPL2RKARutin').dom.value = formatCurrency(det2);
		
		///var total = anggaranRutin + pengembangan; 
		var total = det2;
		var sisaAnggaran = plafon - total;
		Ext.get('txtSisaPlafondPL2RKARutin').dom.value = formatCurrency(sisaAnggaran);
	}else{
		
		//set value anggaran
		//var totalRutin = jumlah - det2;
		var anggaranRutin = daf - danaRKARutin;
		Ext.get('txtRutinPL2RKARutin').dom.value = formatCurrency(anggaranRutin);
		
		var total = anggaranRutin + pengembangan;
		var sisaAnggaran = plafon - total;
		Ext.get('txtSisaPlafondPL2RKARutin').dom.value = formatCurrency(sisaAnggaran);
	} */
	
}

//lookup rkat
/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookAcc;
var vWinFormEntry;


function FormLookupAccountRKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormROLookup',
			title: 'Lookup Akun',
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpRKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
			],
			listeners:
				{ 
					activate: function()
					{ },
					afterrender: function() {
						RefreshDataLookUpRKAT(criteria);
					}
				}
		}
	);

    vWinFormEntry.show();
};

function GetPanelLookUpRKAT(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
{
	var FormLookUp = new Ext.Panel    // from transaksi jurnal
	(
		{
			id: 'FormLookUp',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpFERKAT(p,criteria,mBolAddNew,dsStore,idx,mBolLookup),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'11px','margin-top':'2px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items:
					[
						{
							xtype:'button',
							text:'Ok',
							width:70,
							style:{'margin-left':'3px','margin-top':'2px'},
							hideLabel:true,
							id: 'btnOkAcc',
							handler:function()
							{
								console.log(p);
								if (p != undefined && rowSelectedLookAcc != undefined)
								{
									if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.account=rowSelectedLookAcc.data.account;
										p.data.name=rowSelectedLookAcc.data.name;
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
										
									}
									else
									{
										if (dsStore != undefined)
										{
											p.data.account=rowSelectedLookAcc.data.account;
											p.data.name=rowSelectedLookAcc.data.name;
											
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}
									}
								}
								vWinFormEntry.close();
							}
						},
						{
								xtype:'button',
								text:'Cancel',
								width:70,
								style:{'margin-left':'5px','margin-top':'2px'},
								hideLabel:true,
								id: 'btnCancelAcc',
								handler:function() 
								{
									vWinFormEntry.close();
								}
						}
					]
				}
			]
        }
	);
	
	return FormLookUp;
};

//---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpFERKAT(p,criteria,mBolAddNew,dsStore,idx,mBolLookup) 
{  
	console.log(criteria);
	var fldDetail = ['account','name'];
	dsLookAccList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookAccFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookAccList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookAcc = dsLookAccList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookAccColumnModelRKAT(),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					if (p != undefined && rowSelectedLookAcc != undefined)
					{
						if (mBolAddNew === true || mBolAddNew === undefined)
						{
							p.data.account=rowSelectedLookAcc.data.account;
							p.data.name=rowSelectedLookAcc.data.name;
							if (mBolLookup === true)
							{
							   dsStore.insert(dsStore.getCount(), p);
							}
							else
							{
							   dsStore.removeAt(dsStore.getCount()-1);
							   dsStore.insert(dsStore.getCount(), p);
							}
							
						}
						else
						{
							if (dsStore != undefined)
							{
								p.data.account=rowSelectedLookAcc.data.account;
								p.data.name=rowSelectedLookAcc.data.name;
								
								dsStore.removeAt(idx);
								dsStore.insert(idx, p);
							}
						}
					}
					vWinFormEntry.close();
					
				}
				// End Function # --------------
			},
			tbar:
			[
				{ 
					xtype: 'buttongroup',
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		            items: 
		            [
		            	{ 
							xtype: 'tbtext', 
							text: 'No. Akun', 
							style: {'text-align':'left'},
							width: 60
						},
						{
							xtype: 'tbtext', text: ':', style: {'text-align':'right'}, width: 10
						},	
						{
							xtype: 'textfield',
							name: 'txtNoAkunLookUpRKAT',
							id: 'txtNoAkunLookUpRKAT',
							width: 50,
							style: {'text-align':'left'},
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										var criteria_lp='';
										if(Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != '' && Ext.getCmp('txtNamaLookUpRKAT').getValue() == '' || Ext.getCmp('txtNamaLookUpRKAT').getValue() == undefined){
											criteria_lp = criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == '' || Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == undefined){
											criteria_lp = criteria+  "and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != ''){
											criteria_lp = criteria+  "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%' and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'" ;
										}
										RefreshDataLookUpRKAT(criteria_lp);
									} 						
								}
							}
						},
						{ 
							xtype: 'tbspacer',
							width: 20,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Nama Akun', 
							style: {'text-align':'left'},
							width: 70
						},
						{
							xtype: 'tbtext', text: ':', style: {'text-align':'right'}, width: 10
						},
						{
							xtype: 'textfield',
							name: 'txtNamaLookUpRKAT',
							id: 'txtNamaLookUpRKAT',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										var criteria_lp='';
										if(Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != '' && Ext.getCmp('txtNamaLookUpRKAT').getValue() == '' || Ext.getCmp('txtNamaLookUpRKAT').getValue() == undefined){
											criteria_lp = criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == '' || Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == undefined){
											criteria_lp = criteria+  "and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'";
										}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != ''){
											criteria_lp = criteria+  "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%' and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'" ;
										}
										RefreshDataLookUpRKAT(criteria_lp);
									} 						
								}
							}
						},
						{ 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{
							id: 'BtnRefreshLookUpRKAT',
						    text: 'Tampilkan',
						    tooltip: 'Tampilkan',
						    iconCls: 'Refresh',
						    style: {'text-align':'right'},
						    handler: function(sm, row, rec) {
								var criteria_lp='';
								if(Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != '' && Ext.getCmp('txtNamaLookUpRKAT').getValue() == '' || Ext.getCmp('txtNamaLookUpRKAT').getValue() == undefined){
									criteria_lp = criteria+ "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%'";
								}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == '' || Ext.getCmp('txtNoAkunLookUpRKAT').getValue() == undefined){
									criteria_lp = criteria+  "and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'";
								}else if(Ext.getCmp('txtNamaLookUpRKAT').getValue() != '' && Ext.getCmp('txtNoAkunLookUpRKAT').getValue() != ''){
									criteria_lp = criteria+  "and account like '" + Ext.getCmp('txtNoAkunLookUpRKAT').getValue() +"%' and upper(name) like '" + Ext.getCmp('txtNamaLookUpRKAT').getValue().toUpperCase() +"%'" ;
								}
								RefreshDataLookUpRKAT(criteria_lp);
							}
						}
					]
				}
			],
			viewConfig: { forceFit: true }
		});
	
	return vGridLookAccFormEntry;
};

function RefreshDataLookUpRKAT(criteria)
{
	console.log(criteria);
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/getAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			ShowPesanError('Error menampilkan data account !', 'Error');
		},	
		success: function(o) 
		{   
			dsLookAccList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsLookAccList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsLookAccList.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError('Gagal menampilkan data account', 'Error');
			};
		}
	});
	
	
}

function fnGridLookAccColumnModelRKAT() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colAccLook',
				header: "No Akun",
				dataIndex: 'account',
				width: 70
			},
			{ 
				id: 'colAccNameLook',
				header: "Nama Akun",
				dataIndex: 'name',
				width: 300
            }
		]
	)
};

function ShowPesanWarning(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfo(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanError(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};
///---------------------------------------------------------------------------------------///