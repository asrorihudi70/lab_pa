var dsGListRekapSP3D;
var rowselectGListRekapSP3D;
var countGListRekapSP3D = 500;
var selectcount_RekapSP3D=50;
var nowRekapSP3D = new Date();
var winFormEntryRekapSP3D;
var selectAktivaLancar_REKAP = 'NULL';
var dsGridDetailEntryRekapSP3D;
var rowselectGridDetailEntryRekapSP3D;
var strNO_RKP_SP3D_TMP; 
var userRekapSp3D;
var nosp3dRekapSp3D;
// userRekapSp3D= strKdUser;
userRekapSp3D= '';
var now = new Date();
var grdDetailEntryRekapSP3D;
var mRecord = Ext.data.Record.create
(
	[	  	  
		{ name: 'tgl_rkp_sp3d', mapping: 'tgl_rkp_sp3d' },
		{ name: 'no_rkp_sp3d', mapping: 'no_rkp_sp3d' },
		{ name: 'tahun_anggaran_ta', mapping: 'tahun_anggaran_ta' }, 
		{ name: 'kd_unit_kerja', mapping: 'kd_unit_kerja' },
		{ name: 'no_sp3d_rkat', mapping: 'no_sp3d_rkat' }, 
		{ name: 'tmpno_sp3d_rkat', mapping: 'tmpno_sp3d_rkat' },		
		{ name: 'tgl_sp3d_rkat', mapping: 'tgl_sp3d_rkat' },
		{ name: 'acc_sp3d', mapping: 'acc_sp3d' }, 
		{ name: 'used_sp3d', mapping: 'used_sp3d' },
		{ name: 'rkat_sp3d', mapping: 'rkat_sp3d' },
		{ name: 'jumlah', mapping: 'jumlah' },
		{ name: 'nama_unit_kerja', mapping: 'nama_unit_kerja' },
	]
);

var CurrentRowGridDetailEntryRekapSP3D = 
{
	data: Object,
	details: Array,
	row: 0
}
var AddNewRekapSP3D;
var BlnIsDetailRekapSP3D;
var grdListRekapSP3D ;
// var thn = gstrTahunAngg;
var thn = '';
var REFFID_RSP3D_RekapSP3D='02'; //KODE REKAP SP3D
CurrentPage.page = getPnlEntryRekapSP3D(CurrentPage.id);

mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPnlEntryRekapSP3D(mod_id)
{
	var Fields = 
	[
		'no_rkp_sp3d',
		// 'aliasno_rkp_sp3d',
		'tgl_rkp_sp3d',
		'ket_rkp_sp3d',
		'app_rkp_sp3d',
		'jumlah'
	];
	dsGListRekapSP3D = new WebApp.DataStore
	({ 
		fields: Fields 
	});

	// RefreshDataGListRekapSP3D('');

	var chkApprovedRekapSP3D = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprovedRekapSP3D',
			header: 'App',
			align: 'center',			
			dataIndex: 'app_rkp_sp3d_tmp',
			disabled: true,
			width: 30
		}
	);
	
	grdListRekapSP3D = new Ext.grid.EditorGridPanel
	(
		{
			id:'grdListRekapSP3D',
			xtype: 'editorgrid',
			title: '',
			store: dsGListRekapSP3D,
			autoScroll: true,
			columnLines: true,
			border: false,
			anchor: '100% 100%',
			id: 'grdListRekapSP3D',
			width: 720,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
           (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowselectGListRekapSP3D = undefined;
                            rowselectGListRekapSP3D = dsGListRekapSP3D.getAt(row);
                        }
                    }
                }
            ),			
			//cm: new Ext.grid.ColumnModel(
			colModel: new Ext.grid.ColumnModel(
				[
					new Ext.grid.RowNumberer(),
					{
						xtype: 'gridcolumn',
						header: 'Nomor Rekap',
						dataIndex: 'no_rkp_sp3d',
						width: 80,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.no_rkp_sp3d + "</div>";
							return str;
						},
						filter: {}
					},
					// {
					// 	xtype: 'gridcolumn',
					// 	header: 'Nomor Rekap',
					// 	hidden:true,
					// 	dataIndex: 'AliasNO_RKP_SP3D',
					// 	width: 80,
					// 	renderer: function(v, params, record)
					// 	{
					// 		var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.AliasNO_RKP_SP3D + "</div>";
					// 		return str;
					// 	},
					// 	filter: {}
					// },
					{
						xtype: 'gridcolumn',
						header: 'Tanggal Rekap',
						dataIndex: 'tgl_rkp_sp3d',
						width: 80,
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.tgl_rkp_sp3d);
						},
						filter: {}
					},
					{
						xtype: 'gridcolumn',
						header: 'Keterangan',
						dataIndex: 'ket_rkp_sp3d',
						width: 80,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;'>" + record.data.ket_rkp_sp3d + "</div>";
							return str;
						},
						filter: {}
					},
					{
						xtype: 'gridcolumn',
						header: 'Jumlah',
						dataIndex: 'jumlah',
						width: 80,
						renderer: function(v, params, record)
						{
							var str = "<div style='white-space:normal; padding: 2px;text-align: right;'>" + formatCurrencyDec(record.data.jumlah) + "</div>";
							return str;
						},
						filter: {}
					},
					chkApprovedRekapSP3D
				]
			),
			
			tbar:
			{
				xtype: 'toolbar',
	            id: 'toolbar_RekapSP3D',
	            items: 
				[

					{
						xtype: 'button',
						id: 'btnEditRekapSP3D',
						iconCls: 'Edit_Tr',
						text: 'Edit Data',
						handler: function()
						{
							if (rowselectGListRekapSP3D != undefined)
							{							
								showWinFormEntryRekapSP3D(rowselectGListRekapSP3D.data);
							}
							else
							{
								showWinFormEntryRekapSP3D();
							}
							
						}
					},' ','-'
					,
					{
						xtype: 'checkbox',
						id: 'chkWithTgl_RekapSP3D',					
						hideLabel:true,					
						checked: true,
						handler: function() 
						{
							if (this.getValue()===true)
							{
								Ext.get('dtpFilterTgl1RekapSP3D').dom.disabled=false;
								Ext.get('dtpFilterTgl2RekapSP3D').dom.disabled=false;
								Ext.get('dtpFilterTgl1RekapSP3D').dom.readOnly=true;	
								Ext.get('dtpFilterTgl2RekapSP3D').dom.readOnly=true;
							}
							else
							{
								Ext.get('dtpFilterTgl1RekapSP3D').dom.disabled=true;
								Ext.get('dtpFilterTgl2RekapSP3D').dom.disabled=true;							
							};
						}
					}
					, ' ','-','Tanggal : ', ' ',
					{
						xtype: 'datefield',
						fieldLabel: 'Dari Tanggal : ',
						id: 'dtpFilterTgl1RekapSP3D',
						format: 'd/M/Y',
						value:nowRekapSP3D.format('d/M/Y'),
						width:100,
						onInit: function() { },
						listeners: 
						{
							'specialkey' : function()
							{

								if (Ext.EventObject.getKey() === 13) 
								{
									RefreshDataGListRekapSP3D();
								}
							}
						}
					}, ' ', ' s/d ',' ', {
						xtype: 'datefield',
						fieldLabel: 'Sd /',
						id: 'dtpFilterTgl2RekapSP3D',
						format: 'd/M/Y',
						value:nowRekapSP3D.format('d/M/Y'),
						width:100,
						listeners: 
						{
							'specialkey' : function()
							{

								if (Ext.EventObject.getKey() === 13) 
								{
									RefreshDataGListRekapSP3D();
								}
							}
						}
					}				
				]
			},
			
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsGListRekapSP3D,
					pageSize: selectcount_RekapSP3D,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;",
					// beforePageText : 'Page',
					// afterPageText : ''	
				}
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowselectGListRekapSP3D = dsGListRekapSP3D.getAt(ridx);
					if (rowselectGListRekapSP3D != undefined)
					{							
						showWinFormEntryRekapSP3D(rowselectGListRekapSP3D.data);
					}
					else
					{
						showWinFormEntryRekapSP3D();
					}
				},
				'afterrender': function(){ 
					RefreshDataGListRekapSP3D(); 
				}
				// End Function # --------------
			},
			
			viewConfig: { forceFit: true }
		}
	)
	
	var pnlRekapSP3D = new Ext.Panel(
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: 'Rekap PPD',
			border: false,  
			closable: true,
			iconCls: 'Pencairan',
			margins: '0 5 5 0',
			items: [grdListRekapSP3D],
			tbar:
			[
				{ xtype: 'tbtext', text: 'Nomor : ', cls: 'left-label', width: 45 },
				{
					xtype: 'textfield',
					id: 'txtFilterNoRekapSP3D',
					width: 120,
					listeners: 
					{
						'specialkey' : function()
						{

							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataGListRekapSP3D();
							}
						}
					}
				},
				// { xtype: 'tbseparator' },
				// { xtype: 'tbtext', text: 'Tanggal dari : ', cls: 'left-label', width: 75 },
				// {
					// xtype: 'datefield',
					// id: 'dtpFilterTgl1RekapSP3D',
					// format: 'd/M/Y',
					// value: nowRekapSP3D.format('d/M/Y')
				// },
				// { xtype: 'tbtext', text: 's/d', style: 'text-align: center;', width: 35 },
				// {
					// xtype: 'datefield',
					// id: 'dtpFilterTgl2RekapSP3D',
					// format: 'd/M/Y',
					// value: nowRekapSP3D.format('d/M/Y')
				// },
				{ xtype: 'tbseparator' },
				 ' ',
					// 'Maks.Data : ', ' ',mComboMaksData_RekapSP3D(),'-',
				{
					xtype: 'checkbox',
					id: 'chkFilterApprovedRekapSP3D',
					boxLabel: 'Approved',
					
					listeners: 
					{
						check: function()
						{
						   RefreshDataGListRekapSP3D();
						}
					}
				},
				{ xtype: 'tbfill' },
				{
					xtype: 'button',
					id: 'btnRefreshRekapSP3D',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function()
					{
						RefreshDataGListRekapSP3D();
					}
				}
			]
		}
	);
	
	return pnlRekapSP3D;
}

function RefreshDataGListRekapSP3D_lama(criteria)
{		
	
	/* dsGListRekapSP3D.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectcount_RekapSP3D, 
				Sort: 'NO_RKP_SP3D', 
				Sortdir: 'ASC', 
				target: 'viviewRekapSP3D',
				param: criteria
			} 
		}
	);
	
	return dsGListRekapSP3D; */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRekapPPD/LoadRekapPPD",
		params: {
			no_rkp_sp3d : Ext.getCmp('txtFilterNoRekapSP3D').getValue(),
			approve 	: Ext.getCmp('chkFilterApprovedRekapSP3D').getValue(),
			filter_tgl 	: Ext.getCmp('chkWithTgl_RekapSP3D').getValue(),
			tglawal 	: Ext.getCmp('dtpFilterTgl1RekapSP3D').getValue(),
			tglakhir 	: Ext.getCmp('dtpFilterTgl2RekapSP3D').getValue(),
			
		},
		failure: function(o)
		{
			ShowPesanErrorRekapSP3D('Error menampilkan data Rekap PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGListRekapSP3D.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGListRekapSP3D.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGListRekapSP3D.add(recs);
				console.log(dsGListRekapSP3D);
			} else {
				ShowPesanErrorRekapSP3D('Gagal menampilkan data  Rekap  PPD', 'Error');
			};
		},
		callback:function(){
			var grdListRSP3D=Ext.getCmp('grdListRekapSP3D').getStore().data.items;
				for(var i=0,iLen=grdListRSP3D.length; i<iLen;i++){
					console.log(grdListRSP3D[i].data.app_sp3d);
					if(grdListRSP3D[i].data.app_rkp_sp3d == 't'){
						grdListRSP3D[i].data.app_rkp_sp3d_tmp = true;
						console.log('true');
					} 
				}
			Ext.getCmp('grdListRekapSP3D').getView().refresh();
			
		}
	});
}

function RefreshDataGListRekapSP3D()
{		
	var criteria = GetCriteriaGridUtama();
	dsGListRekapSP3D.removeAll();
	dsGListRekapSP3D.load({
		params:{
			Skip: 0,
			Take: selectcount_RekapSP3D,
			Sort: '',
			Sortdir: 'ASC',
			target: 'vi_viewdata_rekap_ppd',
			param: criteria
		},
		callback:function(){
			var grdListRSP3D=Ext.getCmp('grdListRekapSP3D').getStore().data.items;
				for(var i=0,iLen=grdListRSP3D.length; i<iLen;i++){
					
					if(grdListRSP3D[i].data.app_rkp_sp3d == 't'){
						grdListRSP3D[i].data.app_rkp_sp3d_tmp = true;
						
					} 
				}
			Ext.getCmp('grdListRekapSP3D').getView().refresh();
		}
	}); 
}

function GetCriteriaGridUtama(){
	var criteria = '';
	var criteria_approved = '';
	var criteria_no_rkp_sp3d = '';
	var criteria_tgl = '';
	
	criteria_approved = " app_rkp_sp3d ='"+ Ext.getCmp('chkFilterApprovedRekapSP3D').getValue() +"' ";
	
	if (Ext.getCmp('txtFilterNoRekapSP3D').getValue() != ''){
		criteria_no_rkp_sp3d = " and no_rkp_sp3d ='"+ Ext.getCmp('txtFilterNoRekapSP3D').getValue() +"' ";
	}

	if (Ext.getCmp('chkWithTgl_RekapSP3D').getValue() == true){
		criteria_tgl = " and tgl_rkp_sp3d between '"+ Ext.get('dtpFilterTgl1RekapSP3D').getValue() +"'  and '"+ Ext.get('dtpFilterTgl2RekapSP3D').getValue() +"' ";
	}

	criteria = " "+criteria_approved+" "+criteria_tgl+" "+criteria_no_rkp_sp3d+" ";
	
	return criteria; 
}

// function mComboMaksData_RekapSP3D()
// {
//   var cboMaksDataRekapSP3D = new Ext.form.ComboBox
// 	(
// 		{
// 			id:'cboMaksDataRekapSP3D',
// 			typeAhead: true,
// 			triggerAction: 'all',
// 			lazyRender:true,
// 			mode: 'local',
// 			emptyText:'',
// 			fieldLabel: 'Maks.Data ',			
// 			width:50,
// 			store: new Ext.data.ArrayStore
// 			(
// 				{
// 					id: 0,
// 					fields: 
// 					[
// 						'Id',
// 						'displayText'
// 					],
// 				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
// 				}
// 			),
// 			valueField: 'Id',
// 			displayField: 'displayText',
// 			value:selectcount_RekapSP3D,
// 			listeners:  
// 			{
// 				'select': function(a,b,c)
// 				{   
// 					selectcount_RekapSP3D=b.data.displayText ;
// 					RefreshDataGListRekapSP3D(false);
// 				} 
// 			}
// 		}
// 	);
// 	return cboMaksDataRekapSP3D;
// };
function getCriteriaGListRekapSP3D()
{
	var strKriteria = "";
	
	if(Ext.getCmp('txtFilterNoRekapSP3D').getValue() != "" && Ext.getCmp('txtFilterNoRekapSP3D').getValue() != undefined)
	{
		strKriteria = " where NO_RKP_SP3D like '%" + Ext.getCmp('txtFilterNoRekapSP3D').getValue() + "%' ";
	}
	
	if(Ext.getCmp('chkFilterApprovedRekapSP3D').getValue() === true)
	{
		if(strKriteria != "")
		{
			strKriteria += " and APP_RKP_SP3D='1' ";
		}
		else
		{
			strKriteria = " Where APP_RKP_SP3D='1' ";
		}
		
	}else{
		if(strKriteria != "")
		{
			strKriteria += " and APP_RKP_SP3D='0' ";
		}
		else
		{
			strKriteria = " Where APP_RKP_SP3D='0' ";
		}
	}
	
	if(Ext.getCmp('dtpFilterTgl1RekapSP3D').getValue() != "" && Ext.getCmp('dtpFilterTgl2RekapSP3D').getValue() != "")
	{
	 if ( Ext.get('chkWithTgl_RekapSP3D').dom.checked == true)
	 {
		if(strKriteria != "")
		{			
			strKriteria += " and (TGL_RKP_SP3D between '" + ShowDate(Ext.getCmp('dtpFilterTgl1RekapSP3D').getValue()) + "' ";
			strKriteria += "   and '" + ShowDate(Ext.getCmp('dtpFilterTgl2RekapSP3D').getValue()) + "') ";				
		}
		else
		{
			strKriteria = " Where (TGL_RKP_SP3D between '" + ShowDate(Ext.getCmp('dtpFilterTgl1RekapSP3D').getValue()) + "' "
			strKriteria += "   and '" + ShowDate(Ext.getCmp('dtpFilterTgl2RekapSP3D').getValue()) + "') "						
		}
		}
	}
	strKriteria += " ORDER BY NO_RKP_SP3D ASC"
	return strKriteria;
}

function showWinFormEntryRekapSP3D(rowdata)
{
	winFormEntryRekapSP3D = new Ext.Window(
		{
			id: 'winFormEntryRekapSP3D',
			title: 'Rekap PPD',
			closeAction: 'destroy',
			width: 640,
			height: 480,//480, 			
			resizable: false,
			layout: 'form',//'border',
			iconCls: 'Pencairan',
			// bodyStyle: 'padding:5px 5px 5px 5px;',
			modal: true,
			items: 
			[
				getFormHeaderRekapSP3D(),
				getGridDetailEntryRekapSP3D(),
				{
					id: 'pnlTotBiayaKegDaftarRKAPengembangan',
					layout: 'hbox',
					border: false,			
					bodyStyle: 'padding: 14px 18px 0 320px;border-top: solid 1px #DFE8F6;',
					defaults:
					{
						flex: 1
					},
					items:
					[
						{
							xtype: 'displayfield',
							id: 'dspTotRekapSP3D',
							value: 'Total Rp. ',
							style: 'padding-left: 0px;font-weight: bold;font-size: 14px;'					
						},
						{
							xtype: 'textfield',
							id: 'txtTotRekapSP3D',
							readOnly: true,
							width: 198,
							style: 'font-weight: bold;text-align: right'
						}
					]
				}
			],
			listeners:
			{
				'close': function()
				{ 
					
					
				}
			}
		}
	);
	
	winFormEntryRekapSP3D.show();
	if (rowdata == undefined)
	{		
		// AddNewEntryRekapSP3D
		AddNewEntryRekapSP3D();
	}
	else
	{
		// datainitRekapSP3D(rowdata)
		InitDataRekapSP3D(rowdata)
	}

}

function getFormHeaderRekapSP3D()
{
	var pnlFormEntryRekapSP3D = new Ext.FormPanel(
		{
			id: 'pnlFormEntryRekapSP3D',
			border: false,
			region: 'north',
			height: 140,//180,			
			items:
			[
				{
					xtype: 'fieldset',
					layout: 'form',
					style: 'margin: 5px;',					
					defaults: { labelWidth: 120 },
					items:
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nomor',
							defaults: { anchor: '100%', flex: 1 },
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtNoEntryRekapSP3D',
									readOnly:true,
									width: 180
								},
								{ xtype: 'label', cls: 'left-label', width: 80, text: 'Tanggal : ' },
								{
									xtype: 'datefield',
									id: 'dtpTglEntryRekapSP3D',
									format: 'd/M/Y',
									value: nowRekapSP3D.format('d/M/Y')
									// value: nowRekapSP3D
								},
								{
									xtype: 'checkbox',
									id: 'chkApprovedEntryRekapSP3D',
									disabled: true,
									readOnly: true,
									boxLabel: 'Approved'
								}
							]
						},						
						{
							xtype: 'textarea',
							id: 'txtaKetEntryRekapSP3D',
							fieldLabel: 'Keterangan',
							autoCreate: {tag: 'input', type: 'text', size: '', autocomplete: 'off', maxlength: '100'},					
							anchor: '100%',
							height: 40
						},
						{
							// columnWidth:0.98,
							columnWidth:.5,
							layout: 'form',
							border:false,
							labelWidth:100,
							items: 
							[
								mcomboAktivaLancar_Rekap(),					
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Info Saldo Akun',
							defaults: { anchor: '100%', flex: 1 },
							hidden:true,
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtSaldoAkun',
									readOnly:true,
									width: 150
								},
								{ xtype: 'label', cls: 'left-label', width: 150, text: 'Info Saldo Rekap : ' },
								{
									xtype: 'textfield',
									id: 'txtSaldoRekap',
									readOnly:true,
									width: 100
								}
							]
						}
					]
				}
			],
			tbar:
			[
				{
					xtype: 'button',
					id: 'btnAddEntryRekapSP3D',
					iconCls: 'add',
					text: 'Tambah',
					handler: function()
					{ 
						AddNewEntryRekapSP3D(); 
					}
				},
				{
					xtype: 'button',
					id: 'btnSaveEntryRekapSP3D',
					iconCls: 'save',
					text: 'Simpan',
					handler: function()
					{
						Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(true);
						Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(true);							
						SaveDataEntryRekapSP3D(false);
					}
				},
				{
					xtype: 'button',
					id: 'btnSaveExitEntryRekapSP3D',
					iconCls: 'saveexit',
					text: 'Simpan & Keluar',
					handler: function()
					{
						Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(true);
						Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(true);							
						SaveDataEntryRekapSP3D(true);
						winFormEntryRekapSP3D.close();
					}
				},
				{
					xtype: 'button',
					id: 'btnDelEntryRekapSP3D',
					iconCls: 'remove',
					text: 'Hapus',
					handler: function()
					{		
						BlnIsDetailRekapSP3D = false;
						HapusEntryRekapSP3D();
					}
				},
				{ xtype: 'tbseparator' },
				{
					xtype: 'button',
					id: 'btnLookupRKATEntryRekapSP3D',
					iconCls: 'find',
					text: 'Lookup PPD',
					handler: function()
					{					
						var p = new mRecord
						(
							{
								tgl_rkp_sp3d: '' , 
								no_rkp_sp3d: '' , 
								tahun_anggaran_ta: '' , 
								kd_unit_kerja: '' ,
								no_sp3d_rkat: '' ,
								tmpno_sp3d_rkat: '',
								// tgl_sp3d_rkat: showdate(nowrekapsp3d) , 
								tgl_sp3d_rkat: '' , 
								acc_sp3d: '' , 
								used_sp3d: '' , 
								rkat_sp3d: '' , 
								jumlah: '' ,
								nama_unit_kerja: '' 			
							}
						);						
						//var criteria = " AND S.TAHUN_ANGGARAN_TA = " + thn
						var nosp3d= getRownosp3dGridRekapsp3d();
						var criteria = "tahun=" + thn 
						var	criteriaGrd = "&" +"nosp3din= " + nosp3d
						FormLookupSP3DUnit(p,criteria,dsGridDetailEntryRekapSP3D,criteriaGrd);
					}
				},
				{ xtype: 'tbseparator' },
				{
					xtype: 'button',
					id: 'btnApproveEntryRekapSP3D',
					iconCls: 'approve',
					text: 'Approve',
					handler: function()
					{
						if (ValidasiAppEntryRekapSP3D('Simpan Data',false) == 1 )
						{
							FormApprove(Ext.get('txtSaldoRekap').getValue(),'7',Ext.get('txtNoEntryRekapSP3D').getValue(),
							Ext.get('txtaKetEntryRekapSP3D').getValue(),Ext.get('dtpTglEntryRekapSP3D').getValue())
							
							RefreshDataGListRekapSP3D(getCriteriaGListRekapSP3D());
						}
					}
				},
				// {
					// xtype: 'button',
					// id: 'btnUnApproveEntrySP3DForm',
					// iconCls: 'remove',
					// text: 'Batal Rekap '+gstrSp3d,
					// handler: function()
					// {
						// UnApprove_RekapSP3D()
					// }
				// },
				{ xtype: 'tbfill'},
				{
					xtype: 'button',
					id: 'btnPrintEntryRekapSP3D',
					iconCls: 'print',
					text: 'Cetak',
					handler: function(){
						if(Validasi_RekapSP3DForm() == 1)
						{
							CetakRekapPPD();
						}
					}
				}
			]
		}
	);	
	return pnlFormEntryRekapSP3D;
}

function mcomboAktivaLancar_Rekap()
{
	var Field = ['Account','Name','Groups','AKUN'];
	dsAktivaLancarREKAP = new WebApp.DataStore({ fields: Field });
	dsAktivaLancarREKAP.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viewCboaktivaFilter',
			    param: 'REKAP'
			}
		}
	);
	
 var comboAktivaLancar_REKAP = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_REKAP',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Kas / Bank ',			
			align:'Right',
			anchor:'95%',
			store: dsAktivaLancarREKAP,
			valueField: 'Account',
			displayField: 'AKUN',
			hidden:true,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_REKAP=b.data.Account ;
					ProsesCekTotalRekap(b.data.Account,true)
				} 
			}
		}
	);
	
	return comboAktivaLancar_REKAP;
} ;

function getAmount_rekap(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount
};


function DatarefreshdetailRekapSP3D(no_rkp_sp3d,tgl_rkp_sp3d)
{		
    /* dsGridDetailEntryRekapSP3D.load
    (
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: "NO_RKP_SP3D",
				Sortdir: "ASC",
				target:"viviewDetailRekapSP3D",
				param: rowdataparam
			}
		}
    );
	// CalctotalRekapSP3D();
    return dsGridDetailEntryRekapSP3D; */
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRekapPPD/LoadDetailRekapPPD",
		params: {
			no_rkp_sp3d			: no_rkp_sp3d,
			tgl_rkp_sp3d		: tgl_rkp_sp3d
		},
		failure: function(o)
		{
			ShowPesanErrorRekapSP3D('Error menampilkan data detail Rekap PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGridDetailEntryRekapSP3D.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGridDetailEntryRekapSP3D.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGridDetailEntryRekapSP3D.add(recs);
				CalctotalRekapSP3D();
			} else {
				ShowPesanErrorRekapSP3D('Gagal menampilkan data detail Rekap PPD!', 'Error');
			};
		}
	});
	
}


function TambahBarisDetailEntryRekapSP3D()
{	
	var p = new mRecord
	(
		{
			tgl_rkp_sp3d: '' , 
			no_rkp_sp3d: '' , 
			tahun_anggaran_ta: '' , 
			kd_unit_kerja: '' ,
			no_sp3d_rkat: '' ,
			tmpno_sp3d_rkat: '', 
			tgl_sp3d_rkat: showdate(nowrekapsp3d) , 
			acc_sp3d: '' , 
			used_sp3d: '' , 
			rkat_sp3d: '' , 
			jumlah: '' ,
			nama_unit_kerja: '' 			
		}
	);
	dsGridDetailEntryRekapSP3D.insert(dsGridDetailEntryRekapSP3D.getCount(), p);	 	
};

function HapusBarisDetailEntryRekapSP3D(nBaris)
{		

	var line = grdDetailEntryRekapSP3D.getSelectionModel().selection.cell[0];
	var o = dsGridDetailEntryRekapSP3D.getRange()[line].data;
	console.log(o);
	Ext.Msg.confirm('Warning', 'Apakah data detail Rekap PPD  ini akan dihapus?', function(button){
		if (button == 'yes'){
			if( dsGridDetailEntryRekapSP3D.getRange()[line].data.no_rkp_sp3d !== ''  ){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRekapPPD/HapusDetailRekapPPD",
						params:{
							tgl_rkp_sp3d 		: o.tgl_rkp_sp3d,
							no_rkp_sp3d 		: o.no_rkp_sp3d,
							tahun_anggaran_ta 	: o.tahun_anggaran_ta,
							kd_unit_kerja 		: o.kd_unit_kerja,
							no_sp3d_rkat 		: o.no_sp3d_rkat,
							tgl_sp3d_rkat 		: o.tgl_sp3d_rkat,
							jumlah_awal			: getAmount_rekap(Ext.getCmp('txtTotRekapSP3D').getValue()),
							jumlah_hapus		: o.jumlah
						} ,
						failure: function(o)
						{
							ShowPesanErrorRekapSP3D('Error menghapus detail Rekap PPD!', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsGridDetailEntryRekapSP3D.removeAt(line);
								grdDetailEntryRekapSP3D.getView().refresh();
								DatarefreshdetailRekapSP3D(cst.no_rkp_sp3d,cst.tgl_rkp_sp3d);
								RefreshDataGListRekapSP3D();
								ShowPesanInfoRekapSP3D('Data detail Rekap PPD berhasil dihapus','Information');
							}
							else 
							{
								ShowPesanErrorRekapSP3D('Gagal menghapus data detail Rekap PPD!', 'Error');
							};
						}
					}
					
				)
			}else{
				dsGridDetailEntryRekapSP3D.removeAt(line);
				grdDetailEntryRekapSP3D.getView().refresh();
				CalctotalRekapSP3D();
				ShowPesanInfoRekapSP3D('Data detail PPD berhasil dihapus','Information');
			}
			
		} 
		
	});
	/* if (rowselectGridDetailEntryRekapSP3D.data.NO_SP3D_RKAT_TMP != undefined ) 
	{
		Ext.Msg.show
		(
			{
				title: 'Hapus Baris',
				msg: 'Apakah baris ini akan dihapus ?',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) 
				{
					if (btn == 'yes') 
					{							
						dsGridDetailEntryRekapSP3D.removeAt(CurrentRowGridDetailEntryRekapSP3D.row);
						CalctotalRekapSP3D();
						DataDeletebarisRekapSP3D()
						rowselectGridDetailEntryRekapSP3D = undefined;
						RefreshDataGListRekapSP3D(getCriteriaGListRekapSP3D());						
					}
				},
				icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else 
	{		
		Ext.Msg.show
		(
			{
				title: 'Hapus Baris',
				msg: 'Apakah baris ini akan dihapus ?',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) 
				{
					if (btn == 'yes') 
					{							
						dsGridDetailEntryRekapSP3D.removeAt(CurrentRowGridDetailEntryRekapSP3D.row);
						rowselectGridDetailEntryRekapSP3D = undefined;
						CalctotalRekapSP3D();
					}
				},
				icon: Ext.MessageBox.QUESTION
			}
		);
	} */
}

function getGridDetailEntryRekapSP3D()
{
	var Fields = 
	[
	'tgl_rkp_sp3d', 'no_rkp_sp3d','tmpno_sp3d_rkat', 'tahun_anggaran_ta', 'kd_unit_kerja',
	'no_sp3d_rkat', 'tgl_sp3d_rkat', 'acc_sp3d', 'used_sp3d', 'rkat_sp3d', 'jumlah',
	'nama_unit_kerja', 'no_rkp_sp3d_tmp', 'no_sp3d_rkat_tmp','jalur'
	];
	
	dsGridDetailEntryRekapSP3D = new WebApp.DataStore({ fields: Fields });	
	
	grdDetailEntryRekapSP3D = new Ext.grid.EditorGridPanel(
		{
			id: 'grdDetailEntryRekapSP3D',
			border: false,
			region: 'center',
			anchor: '100% 60%',
			store: dsGridDetailEntryRekapSP3D,
			// bodyStyle: 'padding:5px 5px 5px 5px;',
			autoScroll: true,
			viewConfig: { forceFit: true },
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						/* 'rowselect': function(sm, row, rec)
						{
							rowselectGridDetailEntryRekapSP3D = dsGridDetailEntryRekapSP3D.getAt(row);
							CurrentRowGridDetailEntryRekapSP3D.row = row;
							CurrentRowGridDetailEntryRekapSP3D.data = rowselectGridDetailEntryRekapSP3D.data;
						} */
						cellselect: function(sm, row, rec)
						{
							rowselectGridDetailEntryRekapSP3D = dsGridDetailEntryRekapSP3D.getAt(row);
							CurrentRowGridDetailEntryRekapSP3D.row = row;
							CurrentRowGridDetailEntryRekapSP3D.data = rowselectGridDetailEntryRekapSP3D.data;
						}
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						xtype: 'gridcolumn',
						id: 'col_nosp3d_gldetail',
						dataIndex: 'no_sp3d_rkat',
						header: 'Nomor PPD',
						width: 100
					},
					{
						xtype: 'gridcolumn',
						id: 'col_TMPnosp3d_gldetail',
						dataIndex: 'tmpno_sp3d_rkat',
						hidden: true,
						header: 'Nomor PPD',
						width: 100
					},
					{
						xtype: 'gridcolumn',
						id: 'col_tglsp3d_gldetail',
						dataIndex: 'tgl_sp3d_rkat',
						header: 'Tanggal PPD',
						width: 70
					},
					{
						xtype: 'gridcolumn',
						id: 'col_unit_gldetail',
						dataIndex: 'nama_unit_kerja',
						header: 'Unit Kerja',
						width: 150
					},
					/*{
						xtype: 'gridcolumn',
						id: 'col_Jalur_gldetail',
						dataIndex: 'JALUR',
						header: 'Jalur Anggaran',
						width: 70
					},*/					
					{
						xtype: 'gridcolumn',
						id: 'col_jum_gldetail',
						dataIndex: 'jumlah',
						header: 'Jumlah',
						renderer:function(v, params, record) 
						{
						 var str = "<div style='white-space:normal; padding: 2px;text-align: right;'>" + formatCurrencyDec(record.data.jumlah) + "</div>";
							return str;
							
						}
						// ,
						// listeners: 
						// { 
							// 'blur' : function()
							// {
								// CalctotalRekapSP3D();
							// }
						// }
					}
				]
			),
			tbar:
			[
				{
					xtype: 'button',
					id: 'btnAddRowDetailEntryRekapSP3D',
					iconCls: 'add',
					text: 'Tambah Baris',
					handler: function()
					{
						//TambahBarisDetailEntryRekapSP3D(); 
						
						//kata asdom ini salah...katanya sih gitu
						var p = new mRecord
						(
							{
								tgl_rkp_sp3d: '' , 
								no_rkp_sp3d: '' , 
								tahun_anggaran_ta: '' , 
								kd_unit_kerja: '' ,
								no_sp3d_rkat: '' ,
								tmpno_sp3d_rkat: '',
								// tgl_sp3d_rkat: showdate(nowrekapsp3d) , 
								tgl_sp3d_rkat: '', 
								acc_sp3d: '' , 
								used_sp3d: '' , 
								rkat_sp3d: '' , 
								jumlah: '' ,
								nama_unit_kerja: '' 			
							}
						);						
						//var criteria = " AND S.TAHUN_ANGGARAN_TA = " + thn
						var nosp3d= getRownosp3dGridRekapsp3d();
						var criteria = "tahun=" + thn 
						var	criteriaGrd = "&" +"nosp3din= " + nosp3d
						FormLookupSP3DUnit(p,criteria,dsGridDetailEntryRekapSP3D,criteriaGrd);

					}
				},
				{
					xtype: 'button',
					id: 'btnDelRowDetailEntryRekapSP3D',
					iconCls: 'remove',
					text: 'Hapus Baris',
					handler: function()
					{ 		
						BlnIsDetailRekapSP3D = true;
						HapusBarisDetailEntryRekapSP3D(CurrentRowGridDetailEntryRekapSP3D.row); 
					}
				}
			],
			listeners:
			{
				'afterrender': function()
				{
					
				}
			}
		}
	);
	
	return grdDetailEntryRekapSP3D;
}

function HapusEntryRekapSP3D()
{	
    if (ValidasiEntryRekapSP3D('Hapus Data',true) == 1 )
    {
        Ext.Msg.show
        (
			{
				title:'Hapus Data',
				 msg: "Akan menghapus data?" ,
				buttons: Ext.MessageBox.YESNO,
				width:300,
				fn: function (btn)
				{					
					if (btn =='yes')
					{
						Ext.Ajax.request
						(
						{
							url: baseURL + "index.php/anggaran_module/functionRekapPPD/HapusRekapPPD",
							params: dataparamRekapSP3D(),
							success: function(o)
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true)
								{
									ShowPesanInfoRekapSP3D('Data berhasil dihapus','Hapus Data');
									RefreshDataGListRekapSP3D();
									AddNewEntryRekapSP3D()
								}
								else if (cst.success === false )
								{
									ShowPesanWarningRekapSP3D('Data tidak berhasil dihapus !' ,'Hapus Data');
								}
								else
								{
									ShowPesanErrorRekapSP3D('Data tidak berhasil dihapus !' ,'Hapus Data');
								}
							}
						}
						)//end Ajax.request
					} // end if btn yes
				}// end fn
			}
        )//end msg.show
    }
}

function DataDeletebarisRekapSP3D() 
{
    Ext.Ajax.request
	({ url: "./Datapool.mvc/DeleteDataObj",
		params: dataparamRekapSP3D(),
		success: function(o) 
		{
			var cst = o.responseText;
			if (cst == '{"success":true}') 
			{
				ShowPesanWarningRekapSP3D('Data berhasil dihapus','Hapus Data');                
			}
			else
			{ 
				ShowPesanErrorRekapSP3D('Data gagal dihapus','Hapus Data'); 
			}
		}
	})       
};

function Approve_RekapSP3D(TglApprove, NoteApprove) 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran_module/functionRekapPPD/ApproveRekapPPD",
			params:  getParamAPPRekapSP3D(TglApprove, NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoRekapSP3D('Data berhasil di Approve','Approve');
					Ext.getCmp('chkApprovedEntryRekapSP3D').setValue(true);	
					RefreshDataGListRekapSP3D();
					Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(true);
					Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(true);
					Ext.getCmp('btnDelEntryRekapSP3D').setDisabled(true);
					Ext.getCmp('btnLookupRKATEntryRekapSP3D').setDisabled(true);
					Ext.getCmp('btnApproveEntryRekapSP3D').setDisabled(true);
					Ext.getCmp('btnAddRowDetailEntryRekapSP3D').setDisabled(true);
					Ext.getCmp('btnDelRowDetailEntryRekapSP3D').setDisabled(true);
				}
				else if (cst.success === false )
				{
					ShowPesanWarningRekapSP3D('Data tidak berhasil di Approve','Edit Data');
				}
				else 
				{
					ShowPesanErrorRekapSP3D('Data tidak berhasil di Approve!','Approve');
				}										
			}
		}
	) 
}

function UnApprove_RekapSP3D() 
{
	Ext.Msg.show
	(
		{
			title: 'Batal ',
			msg: 'Apakah Transaksi Ini Akan Dibatalkan ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) 
			{
				if (btn == 'yes') 
				{							
					Ext.Ajax.request
					({ url: "./Datapool.mvc/UpdateDataObj",
						params: getParamUnAppRekapSP3D(),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);							
							if (cst.success === true) 
							{
								ShowPesanInfoRekapSP3D('Data berhasil dibatalkan','Batalkan Rekap '+gstrSp3d);
								InitDataRekapSP3D(rowdata);
								userRekapSp3D = cst.pesan;
								Ext.getCmp('chkApprovedEntryRekapSP3D').setValue(false);	
							}
							else
							{
								ShowPesanInfoRekapSP3D(cst.pesan, 'Batalkan Rekap '+gstrSp3d);
							}
						}
					})    
				}
			},
			icon: Ext.MessageBox.QUESTION
		}
	);
};

function SaveDataEntryRekapSP3D(mBol)
{
    if (ValidasiEntryRekapSP3D('Simpan Data',false) == 1 )
    {
		
		Ext.Ajax.request
		(
			{
				
				url: baseURL + "index.php/anggaran_module/functionRekapPPD/SaveRekapPPD",
				params:dataparamRekapSP3D(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoRekapSP3D('Data berhasil di simpan','Simpan Data');
						if(mBol === false)
						{
							Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(false);
							Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(false);
							Ext.getCmp('txtNoEntryRekapSP3D').setValue(cst.no_rkp_sp3d);
						};
						RefreshDataGListRekapSP3D();
						DatarefreshdetailRekapSP3D(cst.no_rkp_sp3d,cst.tgl_rkp_sp3d);
					}
					else 
					{
						ShowPesanErrorRekapSP3D('Data tidak berhasil di simpan','Simpan Data');
					}
				}
			}
		)
		
        /* if (AddNewRekapSP3D == true)
        {
            Ext.Ajax.request
            (
				{
					url: "./Datapool.mvc/CreateDataObj",
					params: dataparamRekapSP3D(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						var StrKriteria;
						if (cst.success === true)
						{
							ShowPesanInfoRekapSP3D('Data berhasil di simpan','Simpan Data');	
							if(mBol === false)
							{
								Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(false);
								Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(false);
								Ext.get('txtNoEntryRekapSP3D').dom.value=cst.ID_SETUP
							};
							RefreshDataGListRekapSP3D(getCriteriaGListRekapSP3D());
							StrKriteria = " Where SD.NO_RKP_SP3D ='" + Ext.get('txtNoEntryRekapSP3D').dom.value + "' "
							DatarefreshdetailRekapSP3D(StrKriteria)
							Ext.getCmp('btnPrintEntryRekapSP3D').setDisabled(false);
							AddNewRekapSP3D = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRekapSP3D('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanErrorRekapSP3D('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
        }
        else
        {
            Ext.Ajax.request
            (
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params: dataparamRekapSP3D(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(false);
						Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(false);														
						if (cst.success === true)
						{
							ShowPesanInfoRekapSP3D('Data berhasil disimpan','Edit Data');
							RefreshDataGListRekapSP3D(getCriteriaGListRekapSP3D());
							StrKriteria = " Where SD.NO_RKP_SP3D ='" + Ext.get('txtNoEntryRekapSP3D').dom.value + "' "
							DatarefreshdetailRekapSP3D(StrKriteria)
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRekapSP3D('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
						}
						else
						{
							ShowPesanErrorRekapSP3D('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
						}
					}
				}
            )
        } */
    }
    else
    {
		Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(false);
		Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(false);														
        if(mBol === true)
        {
            return false;
        }
    }
}

function CalctotalRekapSP3D() 
{	
    var totalNilaiRekapSP3D = 0;	
	
	for (var i = 0; i < dsGridDetailEntryRekapSP3D.getCount(); i++) 
	{
        var jumlah2_RekapSP3D = 0;
        jumlah2_RekapSP3D = parseFloat(dsGridDetailEntryRekapSP3D.data.items[i].data.jumlah)		
		totalNilaiRekapSP3D = parseFloat(totalNilaiRekapSP3D) + parseFloat(jumlah2_RekapSP3D)
    }
	Ext.get('txtSaldoRekap').dom.value= formatCurrencyDec(totalNilaiRekapSP3D);
	Ext.get('txtTotRekapSP3D').dom.value= formatCurrencyDec(totalNilaiRekapSP3D);
};

function dataparamRekapSP3D()
{	
	 var params = 
	{
		no_rkp_sp3d : Ext.getCmp('txtNoEntryRekapSP3D').getValue(),//Ext.get('txtNoEntryRekapSP3D').getValue(), 
		tgl_rkp_sp3d : Ext.get('dtpTglEntryRekapSP3D').getValue(), 
		ket_rkp_sp3d : Ext.get('txtaKetEntryRekapSP3D').getValue(), 
		app_rkp_sp3d : Ext.getCmp('chkApprovedEntryRekapSP3D').getValue(),
		jumlah :getAmount_rekap(Ext.get('txtSaldoRekap').dom.value),
		
	};
	
	params['jumlah_list']			=	dsGridDetailEntryRekapSP3D.getCount();
	console.log(dsGridDetailEntryRekapSP3D.data);
	for(var i = 0 ; i < dsGridDetailEntryRekapSP3D.getCount();i++)
	{
		params['tgl_rkp_sp3d-'+i]		=	dsGridDetailEntryRekapSP3D.data.items[i].data.tgl_rkp_sp3d
		params['no_rkp_sp3d-'+i]		=	dsGridDetailEntryRekapSP3D.data.items[i].data.no_rkp_sp3d
		params['tahun_anggaran_ta-'+i]	=	dsGridDetailEntryRekapSP3D.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja-'+i]		=	dsGridDetailEntryRekapSP3D.data.items[i].data.kd_unit_kerja
		params['no_sp3d_rkat-'+i]		=	dsGridDetailEntryRekapSP3D.data.items[i].data.no_sp3d_rkat
		params['tgl_sp3d_rkat-'+i]		=	dsGridDetailEntryRekapSP3D.data.items[i].data.tgl_sp3d_rkat
		params['acc_sp3d-'+i]			=	dsGridDetailEntryRekapSP3D.data.items[i].data.acc_sp3d
		params['used_sp3d-'+i]			=	dsGridDetailEntryRekapSP3D.data.items[i].data.used_sp3d
		params['rkat_sp3d-'+i]			=	dsGridDetailEntryRekapSP3D.data.items[i].data.rkat_sp3d
		params['jumlah-'+i]				=	dsGridDetailEntryRekapSP3D.data.items[i].data.jumlah
		params['no_sp3d_rkat_tmp-'+i]	=	dsGridDetailEntryRekapSP3D.data.items[i].data.no_sp3d_rkat_tmp
		
	}
	
	/* var nJmlRecordKiriman=11;
	if (BlnIsDetailRekapSP3D==true)
	{
		var params =
		{
			Table: 'viACC_RKP_SP3D',
			NO_RKP_SP3D : Ext.getCmp('txtNoEntryRekapSP3D').getValue(),//Ext.get('txtNoEntryRekapSP3D').getValue(), 
			NO_RKP_SP3D_TMP : strNO_RKP_SP3D_TMP,
			TGL_RKP_SP3D : Ext.get('dtpTglEntryRekapSP3D').getValue(), 
			KET_RKP_SP3D : Ext.get('txtaKetEntryRekapSP3D').getValue(), 
			APP_RKP_SP3D : Ext.getCmp('chkApprovedEntryRekapSP3D').getValue(),
			ACCOUNT_REKAP: selectAktivaLancar_REKAP,
			JUMLAH :getAmount_rekap(Ext.get('txtSaldoRekap').dom.value),
			Is_Detail:BlnIsDetailRekapSP3D,
			NO_SP3D_RKAT: CurrentRowGridDetailEntryRekapSP3D.data.NO_SP3D_RKAT_TMP,
			kategori:REFFID_RSP3D_RekapSP3D,
			List: getArrDetailRekapSP3D(),
			JmlField: nJmlRecordKiriman,
			JmlList:dsGridDetailEntryRekapSP3D.getCount()
		}
	}
	else
	{
		var params =
		{
			Table: 'viACC_RKP_SP3D',
			NO_RKP_SP3D : Ext.getCmp('txtNoEntryRekapSP3D').getValue(),//Ext.get('txtNoEntryRekapSP3D').getValue(), 
			NO_RKP_SP3D_TMP : strNO_RKP_SP3D_TMP,
			TGL_RKP_SP3D : Ext.get('dtpTglEntryRekapSP3D').getValue(), 
			KET_RKP_SP3D : Ext.get('txtaKetEntryRekapSP3D').getValue(), 
			APP_RKP_SP3D : Ext.getCmp('chkApprovedEntryRekapSP3D').getValue(),
			JUMLAH :getAmount_rekap(Ext.get('txtSaldoRekap').dom.value),
			ACCOUNT_REKAP:selectAktivaLancar_REKAP,
			Is_Detail:BlnIsDetailRekapSP3D,
			kategori:REFFID_RSP3D_RekapSP3D,
			List: getArrDetailRekapSP3D(),
			JmlField: nJmlRecordKiriman,
			JmlList:dsGridDetailEntryRekapSP3D.getCount()
		}	
	} */
    return params
}

function getParamAPPRekapSP3D(TglApprove, NoteApprove) 
{
	var params = 
	{
		no_rkp_sp3d : Ext.getCmp('txtNoEntryRekapSP3D').getValue(),//Ext.get('txtNoEntryRekapSP3D').getValue(),
		no_rkp_sp3d_tmp : strNO_RKP_SP3D_TMP,
		tgl_rkp_sp3d : Ext.get('dtpTglEntryRekapSP3D').getValue(), 
		ket_rkp_sp3d : Ext.get('txtaKetEntryRekapSP3D').getValue(), 
		app_rkp_sp3d : Ext.getCmp('chkApprovedEntryRekapSP3D').getValue(),
		jumlah :Ext.get('txtSaldoRekap').dom.value,
		is_approve:1,
		gl_date: TglApprove,
		gl_date_asal: TglApprove,
		journal_code:"GL",		
		notes:NoteApprove,
		nilairp:getAmount_rekap(Ext.get('txtSaldoRekap').dom.value),
		reference:Ext.get('txtNoEntryRekapSP3D').getValue(),
		kategori:REFFID_RSP3D_RekapSP3D,
		/* List: getArrDetailRekapSP3D(),
		JmlField:11,
		JmlList:dsGridDetailEntryRekapSP3D.getCount()	 */	
    };
	return params
};

function getParamUnAppRekapSP3D()
{
	var params = 
	{
		Table:'viACC_RKP_SP3D',
		NO_RKP_SP3D : Ext.getCmp('txtNoEntryRekapSP3D').getValue(),//Ext.get('txtNoEntryRekapSP3D').getValue(),
		NO_RKP_SP3D_TMP : strNO_RKP_SP3D_TMP,
		Reference:Ext.get('txtNoEntryRekapSP3D').getValue(),
		kd_user: userRekapSp3D,
		IS_APPROVE:1,
		IS_UNAPPROVE:1,
		kategori:REFFID_RSP3D_RekapSP3D
	};
	
	return params;
}

function getArrDetailRekapSP3D() 
{
    var x = '';
    for (var i = 0; i < dsGridDetailEntryRekapSP3D.getCount(); i++) 
	{
        var y = '';
        var z = '@@##$$@@';       
		y += 'TGL_RKP_SP3D=' + ShowDate(dsGridDetailEntryRekapSP3D.data.items[i].data.TGL_RKP_SP3D) // 1
		y += z + 'NO_RKP_SP3D=' + dsGridDetailEntryRekapSP3D.data.items[i].data.NO_RKP_SP3D // 2
		y += z + 'TAHUN_ANGGARAN_TA=' + dsGridDetailEntryRekapSP3D.data.items[i].data.TAHUN_ANGGARAN_TA // 3
		y += z + 'KD_UNIT_KERJA=' + dsGridDetailEntryRekapSP3D.data.items[i].data.KD_UNIT_KERJA // 4
		y += z + 'NO_SP3D_RKAT=' + dsGridDetailEntryRekapSP3D.data.items[i].data.NO_SP3D_RKAT // 5
		y += z + 'TGL_SP3D_RKAT=' + ShowDate(dsGridDetailEntryRekapSP3D.data.items[i].data.TGL_SP3D_RKAT) // 6
		y += z + 'ACC_SP3D=' + dsGridDetailEntryRekapSP3D.data.items[i].data.ACC_SP3D // 7
		y += z + 'USED_SP3D=' + dsGridDetailEntryRekapSP3D.data.items[i].data.USED_SP3D // 8
		y += z + 'RKAT_SP3D=' + dsGridDetailEntryRekapSP3D.data.items[i].data.RKAT_SP3D // 9
		y += z + 'JUMLAH=' + dsGridDetailEntryRekapSP3D.data.items[i].data.JUMLAH // 10
		y += z + 'NO_RKP_SP3D_TMP=' + dsGridDetailEntryRekapSP3D.data.items[i].data.NO_SP3D_RKAT_TMP // 11
        if (i === (dsGridDetailEntryRekapSP3D.getCount() - 1)) 
		{
            x += y
        }
        else {
            x += y + '##[[]]##'
        }
    }
    return x;
};

function ValidasiEntryRekapSP3D(modul,mBolHapus)
{
    var x = 1;
    
	 if (dsGridDetailEntryRekapSP3D.getCount() <= 0) 
	 {
			 ShowPesanWarningRekapSP3D("Detail  belum terisi",modul);
			  x=0;
	 }
	 if ( Ext.get('txtaKetEntryRekapSP3D').getValue() ==='') 
	 {
			 ShowPesanWarningRekapSP3D("Keterangan belum terisi",modul);
			  x=0;
	 }
	 // if (CekRowGridDetailRekapSP3D(nosp3d) === 0 )
	 // {
		// x=0;
	 // };
	 
  //    if (( Ext.getCmp('comboAktivaLancar_REKAP').getValue() =='')  && selectAktivaLancar_REKAP == undefined)
	 // {
		// 	 ShowPesanWarningRekapSP3D("Akun Belum terisi",modul);
		// 	  x=0;
	 // }	
	
	
	// if (Ext.get('txtnamaRekapSP3D').getValue() === '')
	// {
		// x=0;
		// if (mBolHapus === false)
		// {
			// ShowPesanWarningRekapSP3D("Nama belum terisi",modul);
		// }
	// }
    return x;
}

function ValidasiAppEntryRekapSP3D(modul,mBolHapus)
{
    var x = 1;
	if (Ext.get('txtNoEntryRekapSP3D').getValue() === '') 
	{
		x=0;
		if (mBolHapus === false)
		{
			ShowPesanWarningRekapSP3D("Nomor belum terisi",modul);
		}
	}
    return x;
}

function AddNewEntryRekapSP3D()
{
	AddNewRekapSP3D = true;
	Ext.get('txtNoEntryRekapSP3D').dom.value="";
	strNO_RKP_SP3D_TMP="";
	// Ext.get('dtpTglEntryRekapSP3D').dom.value=ShowDate(nowRekapSP3D);
	Ext.get('dtpTglEntryRekapSP3D').dom.value=nowRekapSP3D.format('d/M/Y');
	Ext.get('txtaKetEntryRekapSP3D').dom.value="";
	Ext.get('txtSaldoRekap').dom.value = "";
	Ext.get('txtTotRekapSP3D').dom.value = "";
	Ext.getCmp('chkApprovedEntryRekapSP3D').setValue(false)		
	dsGridDetailEntryRekapSP3D.removeAll();
	
	Ext.getCmp('chkApprovedEntryRekapSP3D').setValue(false)
	Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(false);		
	Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(false);		
	Ext.getCmp('btnDelEntryRekapSP3D').setDisabled(false);		
	Ext.getCmp('btnLookupRKATEntryRekapSP3D').setDisabled(false);		
	Ext.getCmp('btnApproveEntryRekapSP3D').setDisabled(false);
	Ext.getCmp('btnAddRowDetailEntryRekapSP3D').setDisabled(false);		
	Ext.getCmp('btnDelRowDetailEntryRekapSP3D').setDisabled(false);	
	Ext.getCmp('btnPrintEntryRekapSP3D').setDisabled(false);	
}

function InitDataRekapSP3D(rowdata)
{
	
	DatarefreshdetailRekapSP3D(rowdata.no_rkp_sp3d,rowdata.tgl_rkp_sp3d)
	
	AddNewRekapSP3D = false;
	Ext.get('txtNoEntryRekapSP3D').dom.value=rowdata.no_rkp_sp3d;
	strNO_RKP_SP3D_TMP=rowdata.no_rkp_sp3d;
	Ext.get('dtpTglEntryRekapSP3D').dom.value=ShowDate(rowdata.tgl_rkp_sp3d);
	Ext.get('txtaKetEntryRekapSP3D').dom.value=rowdata.ket_rkp_sp3d;
	
	selectAktivaLancar_REKAP = rowdata.account_rekap;
	//Ext.getCmp('comboAktivaLancar_REKAP').dom.value = rowdata.NAME;	
	Ext.get('comboAktivaLancar_REKAP').dom.value = rowdata.name;	
	  Ext.getCmp('comboAktivaLancar_REKAP').setValue(rowdata.account_rekap);
	if (rowdata.app_rkp_sp3d=='f')
	{
		Ext.getCmp('chkApprovedEntryRekapSP3D').setValue(false)
		Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(false);		
		Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(false);		
		Ext.getCmp('btnDelEntryRekapSP3D').setDisabled(false);		
		Ext.getCmp('btnLookupRKATEntryRekapSP3D').setDisabled(false);		
		Ext.getCmp('btnApproveEntryRekapSP3D').setDisabled(false);		
		Ext.getCmp('btnAddRowDetailEntryRekapSP3D').setDisabled(false);		
		Ext.getCmp('btnDelRowDetailEntryRekapSP3D').setDisabled(false);		
	}	
	else
	{
		Ext.getCmp('chkApprovedEntryRekapSP3D').setValue(true)	
		Ext.getCmp('btnSaveEntryRekapSP3D').setDisabled(true);
		Ext.getCmp('btnSaveExitEntryRekapSP3D').setDisabled(true);
		Ext.getCmp('btnDelEntryRekapSP3D').setDisabled(true);
		Ext.getCmp('btnLookupRKATEntryRekapSP3D').setDisabled(true);
		Ext.getCmp('btnApproveEntryRekapSP3D').setDisabled(true);
		Ext.getCmp('btnAddRowDetailEntryRekapSP3D').setDisabled(true);
		Ext.getCmp('btnDelRowDetailEntryRekapSP3D').setDisabled(true);
	}
	Ext.get('txtSaldoRekap').dom.value = formatCurrencyDec(rowdata.jumlah);	
	Ext.get('txtTotRekapSP3D').dom.value = formatCurrencyDec(rowdata.jumlah);	
	Ext.getCmp('btnPrintEntryRekapSP3D').setDisabled(false);	
	
}

/* Cetak gstrSp3d */
function GetCriteria_RekapSP3DForm()
{
	var strKriteria = '';
	
	if (ValidasiEntryRekapSP3D('Simpan Data',false) == 1 )
    {
			 
		if (Ext.getCmp('txtNoEntryRekapSP3D').getValue() != '') 
		{
			strKriteria = " Where ARKPSP3D.NO_RKP_SP3D ='" + Ext.getCmp('txtNoEntryRekapSP3D').getValue() + "'";
		};
		
		strKriteria =  1;
		strKriteria +=  "##"+Ext.get('dtpTglEntryRekapSP3D').getValue() ; 
		strKriteria += "##"+ Ext.getCmp('txtNoEntryRekapSP3D').getValue();
		strKriteria += "##"+ selectAktivaLancar_REKAP ;
		
		strKriteria += ""+ "##"
		+ 1 + "##"
	}
	return strKriteria;
};

function Validasi_RekapSP3DForm()
{
	var x=1;
	
	if((Ext.getCmp('txtNoEntryRekapSP3D').getValue() == ''))
	{
		/*if(Ext.getCmp('txtNoEntryRekapSP3D').getValue() == '')
		{
			ShowPesanWarning_SP3DForm2('No. '+gstrSp3d+' belum di isi','Laporan '+gstrSp3d);
			x=0;
		}
		 else if(Ext.getCmp('cboUnitKerjaEntrySP3DForm').getValue() == '' )
		{
			ShowPesanWarning_SP3DForm2(gstrSatker+' belum di isi','Laporan '+gstrSp3d);
			x=0;
		} */
	};
	return x;
};

function ShowPesanWarningRekapSP3D(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanErrorRekapSP3D(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfoRekapSP3D(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}
function GetParamProsesCekTotalRekap(param)
{
	var x = ''
	x=param;
	x += "##"+Ext.get('dtpTglEntryRekapSP3D').getValue()
	
	
	return x;
};
function ProsesCekTotalRekap(param,view)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'clsCekTotalAkun',
				Params:	GetParamProsesCekTotalRekap(param)			
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 Ext.get('txtSaldoAkun').dom.value   = formatCurrencyDec(cst.jumlah);
				}
				else
				{
				
				ShowPesanInfoRekapSP3D	('Akun Tidak Memiliki Saldo','SALDO');
				}
			}

		}
		);
		
		// Ext.Ajax.request
	 // (
		// {
			// url: "./Module.mvc/ExecProc",
			// params: 
			// {
				// UserID: 'Admin',
				// ModuleID: 'ClsCekTotalRekap',
				// Params:	GetParamProsesCekTotalRekap(param)			
			// },
			// success: function(o) 
			// {
			// var cst = Ext.decode(o.responseText);
				// if (cst.success === true )
				// {
				 // Ext.get('txtSaldoRekap').dom.value   = formatCurrencyDec(cst.jumlah);
				// }
				// else
				// {
				
				// ShowPesanInfoRekapSP3D('Saldo Masih 0','SALDO Rekap');
				// }
			// }

		// }
		// );
	
};
// function CekRowGridDetailRekapSP3D(nosp3d)
// {
// 	var x=1;
// 	if (dsGridDetailEntryRekapSP3D != undefined || dsGridDetailEntryRekapSP3D!='')
// 	{
// 		for (var i = 0; i < dsGridDetailEntryRekapSP3D.getCount(); i++) 
// 		{
// 			alert(i);
// 			if (CekRowGridBrgGandaRekapSP3D(nosp3d) == 0)
// 			{
// 				x=0
// 				ShowPesanWarningRekapSP3D('No '+ gstrSp3d + dsGridDetailEntryRekapSP3D.data.items[i].data.NO_SP3D_RKAT +' ganda ', 'Rekap ' + gstrSp3d);
// 			}
// 		}
// 	}
// 	else
// 	{	
// 		x=0;
// 	};
// 	return x;
// };

// function CekRowGridnosp3dGandaRekapSP3D(nosp3d)
// {
// 	var x='';
// 	for (var i = 0; i < dsGridDetailEntryRekapSP3D.getCount(); i++) 
// 	{
// 		if (nosp3d == dsGridDetailEntryRekapSP3D.data.items[i].data.NO_SP3D_RKAT)
// 		{
// 			x += 1	
// 		}
// 	}
// 	if (x > 1)
// 	{  
// 		x=0
// 	}

// 	return x;
// };

function getRownosp3dGridRekapsp3d()
{
	var x='';
	if (dsGridDetailEntryRekapSP3D.getCount() == 0)
	{
		x=''
	}else
	{
		for (var i = 0; i < dsGridDetailEntryRekapSP3D.getCount(); i++) 
		{
			if (x == '')
			{ 
			}else
			{
				x += ","
			}
			x +=  "'" + dsGridDetailEntryRekapSP3D.data.items[i].data.NO_SP3D_RKAT + "'"
		}
		if (x > 1)
		{  
			x=0
		}
	}
	return x;
};

//lookup PPD
var winFormLookupSP3DUnit;
var dsGLLookupSP3DUnit;
var selectedrowGLLookupSP3DUnit;
var mCancel=true;
 
function FormLookupSP3DUnit(objForm, criteria, ds,criteriaGrd)
{	
	winFormLookupSP3DUnit = new Ext.Window
	(
		{
		    id: 'winFormLookupSP3DUnit',
		    title: 'Lookup PPD',
		    closeAction: 'destroy',
		    closable: true,
		    width: 800,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookupSP3DUnit(objForm, criteria,ds,criteriaGrd)],
		}
	);
	
	winFormLookupSP3DUnit.show();
}

function getItemFormLookupSP3DUnit(objForm, criteria,ds,criteriaGrd)
{
	var pnlButtonLookupSP3DUnit = new Ext.Panel
	(
		{
			id: 'pnlButtonLookupSP3DUnit',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			items:
			[
				{
					xtype: 'button',
					width: 70,
					text: 'Ok',
					handler: function()
					{
						var total=0;
						for(var i = 0;i < dsGLLookupSP3DUnit.getCount();i++)
						{
							if(dsGLLookupSP3DUnit.data.items[i].data.selected === true)
							{
								// if (ValidLookupRekapSP3D(dsGLLookupSP3DUnit.data.items[i].data.NO_SP3D_RKAT) == 1) //
								// { //
									var p = new mRecord
									(
										{
											tgl_rkp_sp3d: '' , 
											no_rkp_sp3d: '' , 
											tahun_anggaran_ta: dsGLLookupSP3DUnit.data.items[i].data.tahun_anggaran_ta ,  
											kd_unit_kerja: dsGLLookupSP3DUnit.data.items[i].data.kd_unit_kerja , 
											no_sp3d_rkat: dsGLLookupSP3DUnit.data.items[i].data.no_sp3d_rkat , 
											tmpno_sp3d_rkat: dsGLLookupSP3DUnit.data.items[i].data.tmpno_sp3d_rkat,
											tgl_sp3d_rkat: ShowDate(dsGLLookupSP3DUnit.data.items[i].data.tgl_sp3d_rkat) , 
											acc_sp3d: '' , 
											used_sp3d: '' , 
											rkat_sp3d: '' , 
											jumlah: dsGLLookupSP3DUnit.data.items[i].data.jumlah,
											nama_unit_kerja: dsGLLookupSP3DUnit.data.items[i].data.nama_unit_kerja,																									
											jalur: dsGLLookupSP3DUnit.data.items[i].data.jalur																									
										}
									);	
									console.log(p);
									ds.insert(ds.getCount(), p);
									total = total + parseFloat(dsGLLookupSP3DUnit.data.items[i].data.jumlah);
									console.log(total);
								// }else //
								// { //
								// }; //
							}									
						}								
						CalctotalRekapSP3D(); 
						winFormLookupSP3DUnit.close();							
					}
				},
				{
					xtype: 'button',
					width: 70,
					text: 'Cancel',
					handler: function()
					{
						selectedrowGLLookupSP3DUnit = undefined;
						winFormLookupSP3DUnit.close();
					}
				}
			]
		}
	);
	
	var frmListLookupSP3DUnit = new Ext.Panel
	(
		{
			id: 'frmListLookupSP3DUnit',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListLookupSP3DUnit(criteria,criteriaGrd,objForm,ds),
				pnlButtonLookupSP3DUnit
			],
			tbar:
			[
				{
					xtype: 'tbtext', text: 'Tahun : ', cls: 'left-label', width: 45
				},
					mCombo_TahunAnggaran(150,'cboThnLookupSP3DUnit'),
				{
					xtype: 'spacer',
					width: 30
				},
				{ 
					xtype: 'tbtext', text: 'No. PPD : ', cls: 'left-label', width: 60 
				},
				{
					xtype:'textfield',
					name: 'txtNoSP3Dlookup',
					id: 'txtNoSP3Dlookup',
					width:250,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								
								RefreshDataLookupSP3DUnit(Ext.getCmp('cboThnLookupSP3DUnit').getValue(),Ext.getCmp('txtNoSP3Dlookup').getValue());							
							} 						
						}
					}
				},
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					id: 'btnRefreshGLLookupSP3DUnit',
					iconCls: 'refresh',
					handler: function()
					{
							RefreshDataLookupSP3DUnit(Ext.getCmp('cboThnLookupSP3DUnit').getValue(),Ext.getCmp('txtNoSP3Dlookup').getValue());	
					}
				}
			]
		}
	);
	
	return frmListLookupSP3DUnit;
}

function getGridListLookupSP3DUnit(criteria,criteriaGrd,objForm,ds)
{
	var _fields = 
	[
		'selected','tahun_anggaran_ta', 'kd_unit_kerja', 'no_sp3d_rkat', 'tmpno_sp3d_rkat', 'tgl_sp3d_rkat',
		'jumlah', 'jenis_sp3d', 'nama_unit_kerja', 'app_sp3d',
		'tahun_anggaran_ta_tmp','kd_unit_kerja_tmp','no_sp3d_rkat_tmp', 'tgl_sp3d_rkat_tmp',
		'jalur'
	];
	dsGLLookupSP3DUnit = new WebApp.DataStore({ fields: _fields });

	
	
	chkSelectedLookupSP3DUnit = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelectedLookupSP3DUnit',
			header: '',
			align: 'center',			
			dataIndex: 'selected',
			width: 20
		}
	);
	var gridListLookupSP3DUnit = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookupSP3DUnit',
			store: dsGLLookupSP3DUnit,
			height:353,
			anchor: '100% 90%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			autoScroll: true,
			border: false,
			viewConfig : 
			{
				forceFit: true
			},				
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: false,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							selectedrowGLLookupSP3DUnit = dsGLLookupSP3DUnit.getAt(row);
						}						
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					chkSelectedLookupSP3DUnit,
					{ 
						id: 'colNoSP3DGLLookupSP3DUnit',
						header: 'No. PPD',
						dataIndex: 'no_sp3d_rkat',
						width: 100,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},filter: {}						
					},
					{ 
						id: 'colTmpNoSP3DGLLookupSP3DUnit',
						header: 'No. PPD',
						dataIndex: 'tmpno_sp3d_rkat',
						hidden: true,
						width: 100,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colTglSP3DLookupSP3DUnit',
						header: 'Tanggal',
						dataIndex: 'tgl_sp3d_rkat',						
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + ShowDate(value) + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colUnitLookupSP3DUnit',
						header: 'Satuan Kerja',
						dataIndex: 'nama_unit_kerja',						
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colJalurLookupSP3DUnit',
						header: 'Jalur Anggaran',
						dataIndex: 'jalur',						
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colJumLookupSP3DUnit',
						header: 'Jumlah',
						dataIndex: 'jumlah',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;text-align: right;'>" + formatCurrencyDec(value) + "</div>";
							return str;
						}						
					},
					{
						id: 'spacer_',
						header: '',
						dataIndex: 'spacer_',
						width: 10,
					}
				]
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					var total=0;
						for(var i = 0;i < dsGLLookupSP3DUnit.getCount();i++)
						{
							if(dsGLLookupSP3DUnit.data.items[i].data.selected === true)
							{
								// if (ValidLookupRekapSP3D(dsGLLookupSP3DUnit.data.items[i].data.NO_SP3D_RKAT) == 1) //
								// { //
									var p = new mRecord
									(
										{
											tgl_rkp_sp3d: '' , 
											no_rkp_sp3d: '' , 
											tahun_anggaran_ta: dsGLLookupSP3DUnit.data.items[i].data.tahun_anggaran_ta ,  
											kd_unit_kerja: dsGLLookupSP3DUnit.data.items[i].data.kd_unit_kerja , 
											no_sp3d_rkat: dsGLLookupSP3DUnit.data.items[i].data.no_sp3d_rkat , 
											tmpno_sp3d_rkat: dsGLLookupSP3DUnit.data.items[i].data.tmpno_sp3d_rkat,
											tgl_sp3d_rkat: ShowDate(dsGLLookupSP3DUnit.data.items[i].data.tgl_sp3d_rkat) , 
											acc_sp3d: '' , 
											used_sp3d: '' , 
											rkat_sp3d: '' , 
											jumlah: dsGLLookupSP3DUnit.data.items[i].data.jumlah,
											nama_unit_kerja: dsGLLookupSP3DUnit.data.items[i].data.nama_unit_kerja,																									
											jalur: dsGLLookupSP3DUnit.data.items[i].data.jalur																									
										}
									);	
									console.log(p);
									ds.insert(ds.getCount(), p);
									total = total + parseFloat(dsGLLookupSP3DUnit.data.items[i].data.jumlah);
									console.log(total);
								// }else //
								// { //
								// }; //
							}									
						}								
						CalctotalRekapSP3D(); 
						winFormLookupSP3DUnit.close();	
					
				},
				'afterrender': function(){ 
				console.log();
					RefreshDataLookupSP3DUnit(Ext.getCmp('cboThnLookupSP3DUnit').getValue(),'');
				}
				// End Function # --------------
			},
			plugins: chkSelectedLookupSP3DUnit
		}
	);
	
	return gridListLookupSP3DUnit;
}

function getCriteriaLookupSP3DUnit()
{
	var strKriteria = '';
	// if (Ext.get('txtNoSP3Dlookup').getValue() != "" && Ext.get('txtNoSP3Dlookup').getValue() != undefined)
	// {
	// 	strKriteria = " Where  S.NO_SP3D_RKAT like'%" + Ext.getCmp('txtNoSP3Dlookup').getValue() + "%' "
	// }
	// if (strKriteria != "")
	// {
	// 	strKriteria += " and S.TAHUN_ANGGARAN_TA = " + Ext.get('cboThnLookupSP3DUnit').dom.value();
	// }else
	// {
	// 	strKriteria = " Where S.TAHUN_ANGGARAN_TA = " + Ext.get('cboThnLookupSP3DUnit').dom.value();
	// }

	if(Ext.getCmp('txtNoSP3Dlookup').getValue() !== "")
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "nosp3d=" + Ext.getCmp('txtNoSP3Dlookup').getValue();
	}
	if ( (Ext.getCmp('cboThnLookupSP3DUnit').getValue() != undefined) ||  (Ext.getCmp('cboThnLookupSP3DUnit').getValue()=''))
	{	
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		// strKriteria += "tahun=" + ((Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear() < 1900 ? 1900 + (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear() : (Ext.getCmp('dtpTglEntrySP3DForm').getValue()).getYear());
		strKriteria += "tahun=" +  Ext.getCmp('cboThnLookupSP3DUnit').getValue();//Ext.getCmp('cboThnLookupRKATUnit').getValue();
	}else
	{
		if(strKriteria != "")
		{
			strKriteria += "&";
		}
		strKriteria += "tahun=" + Ext.getCmp('cboThnLookupSP3DUnit').getValue();
	};

	return strKriteria;
}

function CetakRekapPPD(){
	var params={
		no_rkp_sp3d : Ext.getCmp('txtNoEntryRekapSP3D').getValue(),//Ext.get('txtNoEntryRekapSP3D').getValue(),
		tgl_rkp_sp3d : Ext.get('dtpTglEntryRekapSP3D').getValue()
	} 
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/anggaran_module/functionRekapPPD/CetakRekapPPD");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();		
}
function RefreshDataLookupSP3DUnit(tahun_anggaran_ta,no_sp3d)
{
	/* dsGLLookupSP3DUnit.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'NO_SP3D_RKAT',
			    Sortdir: 'asc',
			    target: 'viViewSP3D',
			    param: criteria + criteriaGrd
			}
		}
	);	
	return dsGLLookupSP3DUnit; */
	// console.log(Ext.getCmp('cboThnLookupSP3DUnit').getValue());
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRekapPPD/LoadPPD",
		params: {
			no_sp3d: no_sp3d,
			tahun_anggaran_ta : tahun_anggaran_ta
		},
		failure: function(o)
		{
			ShowPesanErrorRekapSP3D('Error menampilkan data PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLLookupSP3DUnit.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLLookupSP3DUnit.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLLookupSP3DUnit.add(recs);
			} else {
				ShowPesanErrorRekapSP3D('Gagal menampilkan data PPD', 'Error');
			};
		}
	});
}

var SelectThn_TahunAnggaran;
var SelectKomponen_Komponen2;
function RefreshComboTahun_TahunAnggaran()
{
	dsListTahun_TahunAnggaran.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA',
				Sortdir: 'ASC', 
				target:'viCboThnAnggReal',
				param: ''
			} 
		}
	);
}

function mCombo_TahunAnggaran(lebar,NamaCbo) 
{
	var Field = ['TAHUN_ANGGARAN_TA', 'CLOSED_TA', 'TMP_TAHUN'];
	dsListTahun_TahunAnggaran = new WebApp.DataStore({ fields: Field });
	RefreshComboTahun_TahunAnggaran();
	var currYear = parseInt(now.format('Y'));
	var combo_TahunAnggaran = new Ext.form.ComboBox
	(
		{
		    id: NamaCbo,
		    name: NamaCbo,			
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			align:'Right',
			disabled:true,
			// anchor:'100%',//'70%',			
			width: lebar,
			// store: dsListTahun_TahunAnggaran,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1], 
						[currYear,currYear ]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value:gstrTahunAngg +'-'+(Ext.num(gstrTahunAngg)+1),
			value:Ext.num(now.format('Y')),
			listeners:
			{
				'select': function(a,b,c)
				{
				    SelectThn_TahunAnggaran = b.data.displayText;
				}
			}
		}
	);
	
	return combo_TahunAnggaran;
}

function RefreshComboTahun_TahunAnggaran()
{
	dsListTahun_TahunAnggaran.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_KOMPONEN',
				Sortdir: 'ASC', 
				target:'vicomboKomponen',
				param: ''
			} 
		}
	);
}

function mCombo_Komponen2(lebar,NamaCbo) 
{
	var Field = ['KD_KOMPONEN', 'NAMA_KOMPONEN'];
	dsListTahun_TahunAnggaran = new WebApp.DataStore({ fields: Field });
	RefreshComboTahun_TahunAnggaran();
	var combo_TahunAnggaran = new Ext.form.ComboBox
	(
		{
		    id: NamaCbo,
		    name: NamaCbo,			
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			align:'Right',
			anchor:'100%',//'70%',
			width: lebar,
			store: dsListTahun_TahunAnggaran,
			valueField: 'KD_KOMPONEN',
			displayField: 'NAMA_KOMPONEN',
			value:SelectKomponen_Komponen2,
			listeners:
			{
				'select': function(a,b,c)
				{
				    SelectKomponen_Komponen2 = b.data.KD_KOMPONEN;
				}
			}
		}
	);
	
	return combo_TahunAnggaran;
}

var rowSelectedLookApprove;
var vWinFormEntryApprove;
var now = new Date();
var vPesan;
var vCekPeriode;

function FormApprove(jumlah,type,nomor,Keterangan,tgl) {

    vWinFormEntryApprove = new Ext.Window
	(
		{
		    id: 'FormApprove',
		    title: 'Approve',
		    closeAction: 'hide',
		    closable: false,
		    width: 400,
		    height: 200,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'approve',
		    modal: true,
		    items:
			[
				ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl)
			],
		    listeners:
				{
				    activate: function()
				    
				    {  }
				}
		}
	);
    vWinFormEntryApprove.show();
};


function ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl) 
{	
	if (tgl==undefined)
	{		
		tgl=now;
	}
	
	var PnlLapApprove = new Ext.Panel
	(
		{ 
			id: 'PnlLapApprove',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemApprove_Nomor(nomor),
				getItemApprove_Tgl(tgl),
				getItemApprove_Jml(jumlah),
				getItemApprove_Notes(Keterangan),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApprove',
							handler: function() 
							{
								if (ValidasiEntryNomorApp('Approve') == 1 )
								{										
									if (type==='0')
									{
										PenerimaanApprove(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue());
										RefreshDataPenerimaanFilter(false);
									};
									if (type==='2')
									{
										Approve_PenerimaanMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='3')
									{
										Approve_PenerimaanNonMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='4')
									{
										Approve_KasKeluar(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='5')
									{
										Approve_KasKeluarkecil(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='6')
									{
										Approve_kbs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='7')
									{										
										Approve_RekapSP3D(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};		
									if (type==='8')
									{
										Approve_LPJ(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};									
									if (type==='9')
									{
										Approve_PaguGNRL(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};	
									if (type==='10')
									{
										Approve_viKembaliBDATM(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='11')
									{
										// Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue())
										Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='12')
									{
										Approve_OpenApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='13')
									{
										Approve_AdjustArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='14')
									{
										Approve_AdjustApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									vWinFormEntryApprove.close();	
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApprove',
							handler: function() 
							{
								vWinFormEntryApprove.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApprove
};

function getItemApprove_Nomor(nomor)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				readOnly:true,
				border:false,				
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'No.Reff. ',
						name: 'txtNomorApp',
						id: 'txtNomorApp',						
						value:nomor,
						readOnly:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Notes(Keterangan)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Catatan ',
						name: 'txtCatatanApp',
						id: 'txtCatatanApp',
						value:Keterangan,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Tgl(tgl)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',					
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggalApp',
                        name: 'dtpTanggalApp',
                        format: 'd/M/Y',						
						value:tgl,
                        anchor: '50%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Jml(jumlah)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[			
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Jumlah ',
						name: 'txtJumlah',
						id: 'txtJumlah',
						anchor:'95%',
						value:jumlah,
						readOnly:true
					}
				]
			}
		]
	}
	return items;
};

function ValidasiEntryNomorApp(modul)
{
	var x = 1;
	if (Ext.get('txtNomorApp').getValue() == '' )
	{
		if (Ext.get('txtNomorApp').getValue() == '')
		{
			ShowPesanWarningApprove('Nomor belum di isi',modul);
			x=0;
		}		
	}
	return x;
};

function ShowPesanWarningApprove(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};
