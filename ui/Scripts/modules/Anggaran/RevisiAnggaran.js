var CurrentRevAnggaran = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentTR_RevAnggaran = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentTRtujuan_RevAnggaran = 
{
    data: Object,
    details:Array, 
    row: 0
};

var cellSelectedDet_RevAnggaran;
var gridDTLTR_RevAnggaran;
var mRecordRevAnggaran = Ext.data.Record.create
(
	[
		{name: 'no_revisi', mapping:'no_revisi'},
		{name: 'tahun_anggaran_ta', mapping:'tahun_anggaran_ta'},
		{name: 'kd_unit_kerja', mapping:'kd_unit_kerja'},
		{name: 'kd_jns_rkat_jrka',mapping:'kd_jns_rkat_jrka'},
		{name: 'kd_jns_other', mapping: 'kd_jns_other'},
		{name: 'prioritas', mapping: 'prioritas'},
		{name: 'kd_satuan_sat', mapping:'kd_satuan_sat'},
		{name: 'account', mapping:'account'},
		{name: 'urut', mapping:'urut'},
		{name: 'isr_unit_tujuan', mapping:'isr_unit_tujuan'},
		{name: 'kuantitas', mapping:'kuantitas'},
		{name: 'jml_asal', mapping:'jml_asal'},
		{name: 'jml_tlh_digunakan', mapping: 'jml_tlh_digunakan'},
		{name: 'jml_revisi', mapping:'jml_revisi'},
		{name: 'jml_selisih',  mapping:'jml_selisih'},
		{name: 'm1',  mapping: 'm1'},
		{name: 'm2',  mapping: 'm2'},
		{name: 'm3',  mapping: 'm3'},
		{name: 'm4',  mapping: 'm4'},
		{name: 'm5',  mapping: 'm5'},
		{name: 'm6',  mapping: 'm6'},
		{name: 'm7',  mapping: 'm7'},
		{name: 'm8',  mapping: 'm8'},
		{name: 'm9',  mapping: 'm9'},
		{name: 'm10',  mapping: 'm10'},
		{name: 'm11',  mapping:	'm11'},
		{name: 'm12', mapping:	'm12'},
		{name: 'no_program_prog', mapping: 'no_program_prog'},
		{name: 'satuan_sat',  mapping:  'satuan_sat'},
		{name: 'name',  mapping: 'name'},
		{name: 'nama_program' , mapping: 'nama_program'},
		{name: 'deskripsi',  mapping: 'deskripsi'},
		{name: 'jml_selisih_min',  mapping: 'jml_selisih_min'}
	]
);
var dsLookAccList_RKATRevAnggaran;
var dsDTLTRList_RevAnggaran;
var dsRevAnggaranList;
var dsDTLRevAnggaranList;
var dsDTLRevAnggaranListTemp;
var cellSelectedRevAnggaranDet;
var fldDetail;
var AddNewRevAnggaran = false;
var now_RevAnggaran = new Date();
var selectCountRevAnggaran=50;
var RevAnggaranLookUps;
var rowSelectedRevAnggaran;
var dsUnitKerjaRevAnggaran;
var dsUnitKerjaRevAnggaranEntry;
// var selectUnitKerjaRevAnggaran = gstrListUnitKerja;
var selectUnitKerjaRevAnggaran = '';
var strUnitKerjaRevAnggaran = 'Semua';
// var selectUnitKerjaRevAnggaranEntry = gstrListUnitKerja;
var selectUnitKerjaRevAnggaranEntry = '';

var dsThAnggRevAnggaranFilter;
var dsThAnggRevAnggaranEntry;
var grListRevAnggaran ;
var BlnAppRevAnggaran;
var gridDTLTR_RevAnggaran ;
var gridDTLTR_RevAnggaran_Tujuan ;
var mKD_UNIT_KERJA_TMP;
var mTAHUN_ANGGARAN_TA_TMP;

CurrentPage.page=getPanelRevAnggaran(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelRevAnggaran(mod_id)
{
    var Field =
	[
		'no_revisi', 'kd_unit_kerja', 'kd_unit_kerja_tujuan', 'type_kd_rkat','type_kd_rkat_tujuan',
		'tgl_revisi', 'unit_asal', 'unit_tujuan',
		'rkat_asal', 'rkat_tujuan','kd_user','app_revang_tmp','app_revang'
	];
    dsRevAnggaranList = new WebApp.DataStore({ fields: Field });
	
	var chkApproved_RevAnggaran = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApproved_RevAnggaran ',
			header: 'App',
			align: 'center',			
			dataIndex: 'app_revang_tmp',
			disabled: true,
			width: 50,
			onMouseDown: function(e, t)
			{
			   if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1)
			    {
			  
					e.stopEvent();
					var index = this.grid.getView().findRowIndex(t);
					var record = this.grid.store.getAt(index);
					record.set(this.dataIndex, !record.data[this.dataIndex]);
					rowData = record.data;//save row records. will be used in the ajax request
					if (record.data.APP_REVANG)
					{
						ApproveClick_RevAnggaran(rowData)
					}
				}		 		
			}
		}
	);
	
    grListRevAnggaran = new Ext.grid.EditorGridPanel
	(
		{
			id:'grListRevAnggaran',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsRevAnggaranList,
			columnLines:true,
			border:false,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec) 
						{
							rowSelectedRevAnggaran = dsRevAnggaranList.getAt(row);
						}
					}
				}
			),
			plugins: [new Ext.ux.grid.FilterRow()],chkApproved_RevAnggaran,
			//cm: new Ext.grid.ColumnModel
			colModel: new Ext.grid.ColumnModel
			(	
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoRevisi',
						header: "No. Revisi",
						dataIndex: 'no_revisi',
						sortable: true,
						width: 150,
						filter: {}
					}, 
					{
						id: 'colTgl',
						header: "Tanggal",
						dataIndex: 'tgl_revisi',
						sortable: true,
						width: 150,
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.tgl_revisi);
						},
						filter: {}
					}, 
					{
						id: 'colUnitAsal',
						header: "Unit Asal",
						dataIndex: 'unit_asal',
						sortable: true,
						width: 200,
						filter: {}
					}, 
					{
						id: 'colUnitTujuan',
						header: "Unit Tujuan",
						dataIndex: 'unit_tujuan',
						sortable: true,
						width: 200,
						filter: {}
					}, 
					{
						id: 'colRKATASAL',
						// header: gstrrkat+" Asal",
						header: "RAB Asal",
						dataIndex: 'rkat_asal',
						sortable: true,
						width: 100,
						filter: {}
					}, 
					{
						id: 'colRKATtujuan',
						header: "RAB Tujuan",
						dataIndex: 'rkat_tujuan',
						sortable: true,
						width: 100,
						filter: {}
					},
					chkApproved_RevAnggaran	
				]
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedRevAnggaran = dsRevAnggaranList.getAt(ridx);
					if (rowSelectedRevAnggaran != undefined) 
						{
				            RevAnggaranLookUp(rowSelectedRevAnggaran.data);
				        }
				        else 
						{
				            RevAnggaranLookUp();
				        }
				}
				// End Function # --------------
			},
			tbar: 
			[
				{
					id:'btnEditDataRevAnggaran',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{
						if (rowSelectedRevAnggaran != undefined) 
						{
				            RevAnggaranLookUp(rowSelectedRevAnggaran.data);
				        }
				        else 
						{
				            RevAnggaranLookUp();
				        }
					}
				},' ','-'
			],
            // viewConfig: 
			// {
                // forceFit: true
            // },
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsRevAnggaranList,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			)
        }
	);
	
	
    var FormRevAnggaran = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: 'Revisi Anggaran',
			margins:'0 5 5 0',
			bodyStyle: 'padding:15px',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'RevAnggaran',
			items: [grListRevAnggaran],
			tbar: 
			[
				// 'Unit/Sub Satuan kerja : ', ' ',mComboUnitKerjaRevAnggaran(),
				// ' ','-','Periode : ', ' ',mComboTahunRevAnggaranFilter(),				
				// ' ', '-',
				'Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpFilterTgl1RevAnggaran',
					format: 'd/M/Y',
					value:now_RevAnggaran.format('d/M/Y'),
					width:100,
					onInit: function() { },
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataRevAnggaran();
							} 						
						}
					}
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpFilterTgl2RevAnggaran',
					format: 'd/M/Y',
					value:now_RevAnggaran.format('d/M/Y'),
					width:100,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataRevAnggaran();
							} 						
						}
					}
				},
				'Unit Kerja'+' : ', ' ',mCboUnitKerjaRevAnggaranFilter(),
				'Maks.Data : ', ' ',mComboMaksDataRevAnggaran(),
				' ','->',
				{
					id:'btnRefreshRevAnggaran',
					tooltip: 'Pilih transaksi',
					iconCls: 'refresh',
					text: 'Tampilkan',
					handler: function(sm, row, rec) 
					{
						RefreshDataRevAnggaran();
					}
				}
			],
			listeners:
			{
				'afterrender': function()
				{
					RefreshDataRevAnggaran();
				}
			}
		}
	);

    return FormRevAnggaran ;
	

};

function ApproveClick_RevAnggaran(rowData)
{	
	Ext.Msg.show
	(
		{
			title: 'Approve',
			msg: 'Apakah Transaksi Ini Akan DiApprove ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function(btn) 
			{
				if (btn == 'yes') 
				{							
					BlnAppRevAnggaran=1;
					Ext.Ajax.request
					({ 
						url: "./Datapool.mvc/UpdateDataObj",
						params: getParamClickAppRevAnggaran(rowData),
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);							
							if (cst.success === true) 
							{
								ShowPesanInfoRevAnggaranRevAnggaran('Data berhasil diapprove','Approve Data');				                      						
							}
							else
							{											
								ShowPesanErrorRevAnggaran('Data gagal diapprove ','Approve Data'); 
							}
							RefreshDataRevAnggaranFilter(false); 
						}
					})    
				}
			},
			icon: Ext.MessageBox.QUESTION
		}
	);
};


function mCboJenisRKATasal_RevAnggaran() 
{
    var Fields = ['kd_komponen', 'komponen'];
    dsCboJenisRKATasal_RevAnggaran = new WebApp.DataStore({ fields: Fields });
    
    var CboJenisRKATasal_RevAnggaran = new Ext.form.ComboBox
	(
		{
		    id: 'CboJenisRKATasal_RevAnggaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Jenis RAB...',
		    fieldLabel: 'Jenis RAB',
		    align: 'right',
		    width: 200,
		    store: dsCboJenisRKATasal_RevAnggaran,
		    valueField: 'kd_komponen',
		    displayField: 'komponen',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaRKAOtherFilter = b.data.KD_UNIT_KERJA;
			    }
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getJnsPlafond",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data jenis RAB !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboJenisRKATasal_RevAnggaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboJenisRKATasal_RevAnggaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboJenisRKATasal_RevAnggaran.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data jenis RAB', 'Error');
			};
		}
	});
	
	
    return CboJenisRKATasal_RevAnggaran;
};

function mCboJenisRKATtujuan_RevAnggaran() 
{
    var Fields = ['kd_komponen', 'komponen'];
    dsCboJenisRKATtujuan_RevAnggaran = new WebApp.DataStore({ fields: Fields });
    
    var CboJenisRKATtujuan_RevAnggaran = new Ext.form.ComboBox
	(
		{
		    id: 'CboJenisRKATtujuan_RevAnggaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Jenis RAB...',
		    fieldLabel: 'Jenis RAB ',
		    align: 'right',
		    width: 200,
		    store: dsCboJenisRKATtujuan_RevAnggaran,
		    valueField: 'kd_komponen',
		    displayField: 'komponen',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			    }
			}
		}
	);

	
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getJnsPlafond",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data jenis plafond !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboJenisRKATtujuan_RevAnggaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboJenisRKATtujuan_RevAnggaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboJenisRKATtujuan_RevAnggaran.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data jenis plafond', 'Error');
			};
		}
	});
    return CboJenisRKATtujuan_RevAnggaran;
};

function mCboUnitKerjaRevAnggaranFilter() 
{
     var Fields = ['kd_unit', 'nama_unit', 'unitkerja'];
    dsCboUnitKerjaRevAnggaranFilter = new WebApp.DataStore({ fields: Fields });
    
    var cboUnitKerjaRevAnggaranFilter = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaRevAnggaranFilter',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    width: 200,
		    store: dsCboUnitKerjaRevAnggaranFilter,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        // selectUnitKerjaRKAOtherFilter = b.data.KD_UNIT_KERJA;
					RefreshDataRevAnggaran();
			    }
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaRevAnggaranFilter.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaRevAnggaranFilter.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaRevAnggaranFilter.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
    return cboUnitKerjaRevAnggaranFilter;
};

function mCboUnitKerjaRevAnggaranAsal() 
{
    var Fields = ['kd_unit', 'nama_unit', 'unitkerja'];
    dsCboUnitKerjaRevAnggaranAsal = new WebApp.DataStore({ fields: Fields });
    
    var cboUnitKerjaRevAnggaranAsal = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaRevAnggaranAsal',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+gstrSatker+'...',
		    emptyText: 'Pilih Unit Kerja ...',
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    width: 200,
		    store: dsCboUnitKerjaRevAnggaranAsal,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaRKAOtherAsal =  b.data.kd_unit;
			    }
			}
		}
	);

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaLap",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaRevAnggaranAsal.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaRevAnggaranAsal.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaRevAnggaranAsal.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
	
    return cboUnitKerjaRevAnggaranAsal;
};

function mCboUnitKerjaRevAnggaranTujuan() 
{
    var Fields =['kd_unit', 'nama_unit', 'unitkerja'];
    dsCboUnitKerjaRevAnggaranTujuan = new WebApp.DataStore({ fields: Fields });
    
    var cboUnitKerjaRevAnggaranTujuan = new Ext.form.ComboBox
	(
		{
		    id: 'cboUnitKerjaRevAnggaranTujuan',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Unit Kerja...',
		    fieldLabel: 'Unit Kerja',
		    align: 'right',
		    width: 200,
		    store: dsCboUnitKerjaRevAnggaranTujuan,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaRKAOtherTujuan = b.data.kd_unit;
			    }
			}
		}
	);


	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaLap",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboUnitKerjaRevAnggaranTujuan.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboUnitKerjaRevAnggaranTujuan.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboUnitKerjaRevAnggaranTujuan.add(recs);
			
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
    return cboUnitKerjaRevAnggaranTujuan;
};


function mComboUnitKerjaRevAnggaran() 
{
    var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA', 'UNITKERJA'];
    dsUnitKerjaRevAnggaran = new WebApp.DataStore({ fields: Field });

    var comboUnitKerjaRevAnggaran = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaRevAnggaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih '+gstrSatker+'...',
		    fieldLabel: gstrSatker,
		    align: 'right',
		    width: 200,
		    store: dsUnitKerjaRevAnggaran,
		    valueField: 'KD_UNIT_KERJA',
		    displayField: 'UNITKERJA',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectUnitKerjaRevAnggaran = b.data.KD_UNIT_KERJA;
			    }
			}
		}
	);

	dsUnitKerjaRevAnggaran.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    param: "kdunit=" + gstrListUnitKerja
			}
		}
	);
	
    return comboUnitKerjaRevAnggaran;
};

function LoadDataUnitKerjaFilterRevAnggaran()
{
	dsUnitKerjaRevAnggaran.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target:'viCboUnitKerja',
				param: "kdunit=" + gstrListUnitKerja
			} 
		}
	);		
	
	return dsUnitKerjaRevAnggaran;
};



function mComboTahunRevAnggaranFilter()
{
	var Field = ['TAHUN_ANGGARAN_TA'];
	dsThAnggRevAnggaranFilter = new WebApp.DataStore({ fields: Field });

	var TahunRevAnggaranFilter = new Ext.form.ComboBox
	(
		{
			id:'TahunRevAnggaranFilter',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',
			value: now.format('Y'),
			width:60,
			store: dsThAnggRevAnggaranFilter,
			valueField: 'TAHUN_ANGGARAN_TA',
			displayField: 'TAHUN_ANGGARAN_TA'			
		}
	);

	dsThAnggRevAnggaranFilter.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'TAHUN_ANGGARAN_TA', 
				Sortdir: 'ASC', 
				target:'viCboThnAngg',
				param: 2
			} 
		}
	);

	return TahunRevAnggaranFilter;
};


function mComboTahunRevAnggaranEntry() 
{
	var currYear = parseInt(now.format('Y'));
	
	var TahunRevAnggaranEntry = new Ext.form.ComboBox
	(
		{
			id:'TahunRevAnggaranEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Periode ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[1, currYear + 1], 
						[2, currYear]
					]
				}
			),
			valueField: 'displayText',
			displayField: 'displayText',
			value: now.format('Y')
		}
	);
	
	return TahunRevAnggaranEntry;
};

function mComboUnitKerjaRevAnggaranEntry()
{
	var Field = ['KD_UNIT_KERJA', 'NAMA_UNIT_KERJA','UNITKERJA'];
	dsUnitKerjaRevAnggaranEntry = new WebApp.DataStore({ fields: Field });

   LoadDataUnitKerjaFilterRevAnggaranEntry();
   var comboUnitKerjaRevAnggaranEntry = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerjaRevAnggaranEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih '+gstrSatker+'...',
			fieldLabel: gstrSatker,			
			align:'Right',
			width: 250,
			valueField: 'KD_UNIT_KERJA',
			displayField: 'UNITKERJA',
			store:dsUnitKerjaRevAnggaranEntry,
			listeners:  
			{
				'select': function(a,b,c)
				{
				    selectUnitKerjaRevAnggaranEntry = b.data.KD_UNIT_KERJA;
				} 
			}
		}
	);
	LoadDataUnitKerjaFilterRevAnggaranEntry();
	
	return comboUnitKerjaRevAnggaranEntry;
};

function LoadDataUnitKerjaFilterRevAnggaranEntry()
{
	dsUnitKerjaRevAnggaranEntry.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'KD_UNIT_KERJA', 
				Sortdir: 'ASC', 
				target:'viCboUnitKerja',
				param: "kdunit=" + gstrListUnitKerja
			} 
		}
	);		
	
	return dsUnitKerjaRevAnggaranEntry;
};

function RevAnggaranLookUp(rowdata) 
{
	console.log(rowdata);
	var mWidth=850;
    RevAnggaranLookUps = new Ext.Window
	(
		{  
			id: 'RevAnggaranLookUps',
			title: 'Revisi Anggaran',
			closeAction: 'destroy',
			width: mWidth,
			height: 600,
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'RevAnggaran',
			modal: true,
			anchor: '100%',
			items: getFormEntryRevAnggaran(mWidth),
			listeners: 
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 					
				    this.activate();
				},
				deactivate:function()
				{
					rowSelectedRevAnggaran=undefined;
					RefreshDataRevAnggaranFilter(true);
				}
			}
		}
	); 
	RevAnggaranLookUps.show();
	if (rowdata == undefined) 
	{
        RevAnggaranAddNew();
    }
    else 
	{
        RevAnggaranInit(rowdata);		
    }
};



function getFormEntryRevAnggaran(mWidth) 
{

    var pnlRevAnggaran_asal = new Ext.FormPanel    
	(
		{    
			id: 'PanelRevAnggaran',
			fileUpload: true,
			region: 'center',
			layout: 'column',
			height: 130,
			anchor: '100%',
			bodyStyle: 'padding:5px 5px 5px 5px',
			border: false,
			items: 
			[
				{
					layout: 'form',
					width:mWidth-27,
					anchor: '100%',
					bodyStyle: 'padding:5px 5px 5px 5px',
					autoHeight: true,
					items: 
					[	
						{
							xtype: 'compositefield',
							fieldLabel: 'No Revisi',
							items:
							[						
								{
									xtype: 'textfield',									
									id:'txtAnggaranRevNoRevisi',
									name:'txtAnggaranRevNoRevisi',	
									readOnly:true,
									width: 160							
								},
								{
									xtype: 'displayfield',
									id:'dspfldtgl_RevAnggaran',
									name:'dspfldtgl_RevAnggaran',							
									value:'Tanggal'							
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal ',
									id: 'dtpTanggal_RevAnggaran',
									name: 'dtpTanggal_RevAnggaran',
									format: 'd/M/Y',
									value:now_RevAnggaran,
									anchor: '70%'
								}
							]
						},
						mCboUnitKerjaRevAnggaranAsal(),											
						{
							xtype: 'compositefield',
							fieldLabel: 'Jenis RAB',
							items:
							[				
								mCboJenisRKATasal_RevAnggaran()
								// ,
								// {
									// xtype: 'button',
									// text: 'Load',								
									// id: 'btnloadasal_RevAnggaran',
									// handler: function()
									// {
										// RefreshDtlTombolLoadasal_RevAnggaran();
									// }
								// }																
							]
						}
					]
				}
					
			],
			tbar:
			[
				{
					id:'btnTambahRevAnggaran',
					text: 'Tambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{
					    RevAnggaranAddNew();	 
					    Ext.getCmp('btnSimpanRevAnggaran').setDisabled(false);
						Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(false);
						Ext.getCmp('btnHapusRevAnggaran').setDisabled(false);
						Ext.getCmp('btnApproveEntryRevAnggaran').setDisabled(false);
						Ext.getCmp('btnAddRowDetailEntryAsal_RevAnggaran').setDisabled(false);
						Ext.getCmp('btnDelRowDetailEntryAsal_RevAnggaran').setDisabled(false);
						Ext.getCmp('btnAddRowDetailEntryTujuan_RevAnggaran').setDisabled(false);
						Ext.getCmp('btnDelRowDetailEntryTujuan_RevAnggaran').setDisabled(false);
					}
				},
				'-', 
				{
					id:'btnSimpanRevAnggaran',
					text: 'Simpan',
					tooltip: 'Simpan Data ',
					iconCls: 'save',
					handler: function() 
					{
						BlnAppRevAnggaran=0;
						Ext.getCmp('btnSimpanRevAnggaran').setDisabled(true);
						Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(true);
						RevAnggaranSave(false); 
					}
				},'-',
				{
					id:'btnSimpanCloseRevAnggaran',
					text: 'Simpan & Keluar',
					tooltip: 'Simpan Data dan Keluar',
					iconCls: 'saveexit',
					handler: function() 
					{ 
						Ext.getCmp('btnSimpanRevAnggaran').setDisabled(true);
						Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(true);														
						BlnAppRevAnggaran=0;
						// var x = RevAnggaranSave(true);
						RevAnggaranSave(true);
						RefreshDataRevAnggaranFilter(false);
						/* if (x===undefined)
						{
							RevAnggaranLookUps.close();
						}; */
					}
				},
				'-', 
				{
					id:'btnHapusRevAnggaran',
					text: 'Hapus',
					tooltip: 'Hapus',
					iconCls: 'remove',
					handler: function() 
					{ 
						HapusRevAnggaran();						
					}
				}, 
				'-', 
				{
					xtype: 'button',
					id: 'btnApproveEntryRevAnggaran',
					iconCls: 'approve',
					text: 'Approve',
					handler: function()
					{
						BlnAppRevAnggaran=1;
						ApproveRevAng();
					}
				},
				'->','-', 
				{
					id:'btnCetakRevAnggaran',
					text: 'Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					hidden: true,
					handler: function() 
					{ 
						var x=GetStrCriteriaCetak();
						if ( x != '')
						{
							ShowReport('', '231036', x); 
						};
	
					}
				}
			]
		}
	);
	
	 var pnlRevAnggaran_tujuan = new Ext.FormPanel    
	(
		{    
			id: 'pnlRevAnggaran_tujuan',
			fileUpload: true,
			region: 'center',
			layout: 'column',
			height: 70,
			anchor: '100%',
			bodyStyle: 'padding:5px 5px 5px 5px',
			border: false,
			items: 
			[
				{
					layout: 'form',
					width:mWidth-27,
					anchor: '100%',
					bodyStyle: 'padding:5px 5px 5px 5px',
					autoHeight: true,
					items: 
					[	
						
						mCboUnitKerjaRevAnggaranTujuan(),											
						{
							xtype: 'compositefield',
							fieldLabel: 'Jenis RAB',
							items:
							[				
								mCboJenisRKATtujuan_RevAnggaran()
								// ,
								// {
									// xtype: 'button',
									// text: 'Load',								
									// id: 'btnloadtujuan_RevAnggaran',
									// handler: function()
									// {
										// RefreshDtlTombolLoadTujuan_RevAnggaran();	
									// }
								// }																
							]
						}
					]
				}
					
			],
		}
	);
	
	var GDtabDetailasal_RevAnggaran = new Ext.TabPanel
	(
		{
			region: 'center',
			id:'tabTabelDetail_RevAnggaran',
			name:'tabTabelDetail_RevAnggaran',
			activeTab: 0,			
			anchor: '100% 30%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: GetDTLTRGrid_RevAnggaran()			
		}
	);
	
	var GDtabDetailtujuan_RevAnggaran = new Ext.TabPanel
	(
		{
			region: 'center',
			id:'GDtabDetailtujuan_RevAnggaran',
			name:'GDtabDetailtujuan_RevAnggaran',
			activeTab: 0,			
			anchor: '100% 35%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: GetDTLTRGridTujuan_RevAnggaran()			
		}
	);
	
	var FormRevAnggaran = new Ext.Panel   
	(
		{
			id: 'FormRevAnggaran',
			region: 'center',
			layout: 'form',
			title: '',
			// bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,			
			items: [pnlRevAnggaran_asal,GDtabDetailasal_RevAnggaran,pnlRevAnggaran_tujuan,GDtabDetailtujuan_RevAnggaran]
        }
	);
                
     return FormRevAnggaran
};

function Approve_RevAnggaran(TglApprove, NoteApprove) 
{
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/CreateDataObj",
			params:  getParamAPPRevAnggaran(TglApprove, NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoRevAnggaranRevAnggaran('Data berhasil di Approve','Approve');
					//RefreshDataFilter_kbs(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningRevAnggaran('Data tidak berhasil di Approve, ' + cst.pesan,'Edit Data');
				}
				else 
				{
					ShowPesanErrorRevAnggaran('Data tidak berhasil di Approve, '  + cst.pesan,'Approve');
				}										
			}
		}
	)
}

function getParamAPPRevAnggaran(TglApprove, NoteApprove) 
{
	var params = 
	{
		Table:'viACC_REVISI_ANGGARAN',
		IS_APPROVE:1,
		thn:Ext.get('TahunRevAnggaranEntry').getValue(),
		GL_Date: TglApprove,
		GL_Date_Asal: TglApprove,
		Journal_Code:"GL",		
		Notes:NoteApprove,
		nilairp:getAmount_RevAnggaran(Ext.get('txtTotalRevAnggaran').dom.value),
		Reference:Ext.get('TahunRevAnggaranEntry').getValue()	
    };
	return params
};

//Unapprove
function UnApprove_RevAnggaran() 
{
	if (ValidasiEntryRevAnggaran('Simpan Data') === 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/UnApproveRevisiAnggaran",
				params:getParamInputRevAnggaran(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoRevAnggaranRevAnggaran('Revisi anggaran berhasil diunapprove!','Approve Revisi Anggaran');
						/* 
							Ext.getCmp('btnSimpanRevAnggaran').setDisabled(false);
							Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(false);
						*/
						Ext.get('txtAnggaranRevNoRevisi').dom.value=cst.no_revisi;
						RefreshDataRevAnggaran();
						RefreshDtlasal_RevAnggaran(false);
						RefreshDtlasalTujuan_RevAnggaran(false);
						AddNewRevAnggaran = false;
					}
					else 
					{
						ShowPesanErrorRevAnggaran('Error, Revisi anggaran tidak berhasil diapprove!','Approve Revisi Anggaran');
					}
				}
			}
		)
	}
	/* Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/CreateDataObj",
			params:  getParamUnAPPRevAnggaran(), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoRevAnggaranRevAnggaran('Data berhasil di Approve','Approve');
					//RefreshDataFilter_kbs(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningRevAnggaran('Data tidak berhasil di Approve, ' + cst.pesan,'Edit Data');
				}
				else 
				{
					ShowPesanErrorRevAnggaran('Data tidak berhasil di Approve, '  + cst.pesan,'Approve');
				}										
			}
		}
	) */
}

function getParamUnAPPRevAnggaran() 
{
	var params = 
	{
		Table:'viACC_REVISI_ANGGARAN',
		IS_APPROVE:2,
		thn :Ext.get('TahunRevAnggaranEntry').getValue()
    };
	return params
};

function getAmount_RevAnggaran(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};


function GetDTLTRGrid_RevAnggaran() 
{
	// grid untuk detail transaksi
	var fldDetail = 
	[	
		'no_revisi', 'tahun_anggaran_ta', 'kd_unit_kerja', 'kd_jns_rkat_jrka',
		'kd_jns_other', 'prioritas_rkatr', 'kd_satuan_sat', 'account', 'urut_rkatrdet',
		'isr_unit_tujuan', 'kuantitas', 'jml_asal', 'jml_tlh_digunakan', 'jml_revisi',
		'jml_selisih', 'm1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11', 'm12','no_program_prog',
		'satuan_sat', 'name', 'nama_program' ,'deskripsi_rkatrdet','jml_selisih_min',
		'no_revisi_tmp', 'tahun_anggaran_ta_tmp', 'kd_unit_kerja_tmp', 
		'no_program_prog_tmp', 'kd_jns_rkat_jrka_tmp', 
		'prioritas_tmp','urut_tmp','isr_unit_tujuan_tmp'
	]
	
	dsDTLTRList_RevAnggaran = new WebApp.DataStore({ fields: fldDetail })
	gridDTLTR_RevAnggaran = new Ext.grid.EditorGridPanel
	(
		{
			id:'gridDTLTR_RevAnggaran',
			title: 'Detail Anggaran Asal',
			stripeRows: true,
			store: dsDTLTRList_RevAnggaran,
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 99%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_RevAnggaran =dsDTLTRList_RevAnggaran.getAt(row);
							CurrentTR_RevAnggaran.row = row;
						}
					}
				}
			),
			cm: TRDetailColumModel_RevAnggaran(), 
			tbar:
			[
				{
					xtype: 'button',
					id: 'btnAddRowDetailEntryAsal_RevAnggaran',
					iconCls: 'add',
					text: 'Tambah Baris',
					handler: function()
					{
						TambahBarisRevAngAsal_RevAnggaran()
					}
				}
				,
				{
					xtype: 'button',
					id: 'btnDelRowDetailEntryAsal_RevAnggaran',
					iconCls: 'remove',
					text: 'Hapus Baris',
					handler: function()
					{ 		
						HapusBarisDetailAnggaranAsal();
					}
				}
			],
			// viewConfig: 
			// {
				// forceFit: true
			// },
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{	
							// CalcTotal_RevAnggaran(); 
						} 
					);
					this.store.on("datachanged", function()
						{							
							// CalcTotal_RevAnggaran();
						}
					);
				}
			}
		}
	);
			
	return gridDTLTR_RevAnggaran;
};

function TambahBarisRevAngAsal_RevAnggaran()
{	
	var p = new mRecordRevAnggaran
	(
		{
			no_revisi: '', 
			tahun_anggaran_ta: '', 
			kd_unit_kerja: '', 
			kd_jns_rkat_jrka: '',
			kd_jns_other: '', 
			prioritas_rkatr: '', 
			kd_satuan_sat: '', 
			satuan_sat: '', 
			account: '', 
			urut_rkatrdet: '',
			isr_unit_tujuan: '', 
			kuantitas: '', 
			jml_asal: '', 
			jml_tlh_digunakan: '', 
			jml_revisi: '',
			jml_selisih: '', 
			no_program_prog: '',
			deskripsi_rkatrdet: '',
			jml_selisih_min: ''
		}
	);
	dsDTLTRList_RevAnggaran.insert(dsDTLTRList_RevAnggaran.getCount(), p);	 	
};


function TambahBarisRevAngTujuan_RevAnggaran()
{	
	var p = new mRecordRevAnggaran
	(
		{
			no_revisi: '', 
			tahun_anggaran_ta: '', 
			kd_unit_kerja: '', 
			kd_jns_rkat_jrka: '',
			kd_jns_other: '', 
			prioritas_rkatr: '', 
			kd_satuan_sat: '', 
			account: '', 
			urut_rkatrdet: '',
			isr_unit_tujuan: '', 
			kuantitas: '', 
			jml_asal: '', 
			jml_tlh_digunakan: '', 
			jml_revisi: '',
			jml_selisih: '', 
			no_program_prog: '',
			deskripsi: '',
			jml_selisih_min: ''
		}
	);
	dsDTLTRListTujuan_RevAnggaran.insert(dsDTLTRListTujuan_RevAnggaran.getCount(), p);	 	
};

function GetDTLTRGridTujuan_RevAnggaran() 
{
	// grid untuk detail transaksi
	var fldDetail = 
	[	
		'no_revisi', 'tahun_anggaran_ta', 'kd_unit_kerja', 'kd_jns_rkat_jrka',
		'kd_jns_other', 'prioritas_rkatr', 'kd_satuan_sat', 'account', 'urut_rkatrdet',
		'isr_unit_tujuan', 'kuantitas', 'jml_asal', 'jml_tlh_digunakan', 'jml_revisi',
		'jml_selisih', 'm1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11', 'm12','no_program_prog',
		'satuan_sat', 'name', 'nama_program' ,'deskripsi','jml_selisih_min',
		'no_revisi_tmp', 'tahun_anggaran_ta_tmp', 'kd_unit_kerja_tmp', 
		'no_program_prog_tmp', 'kd_jns_rkat_jrka_tmp', 
		'prioritas_tmp','urut_tmp','isr_unit_tujuan_tmp'	
	]
	
	dsDTLTRListTujuan_RevAnggaran = new WebApp.DataStore({ fields: fldDetail })
	gridDTLTR_RevAnggaran_Tujuan = new Ext.grid.EditorGridPanel
	(
		{
			id:'gridDTLTR_RevAnggaran_Tujuan',
			title: 'Detail Anggaran Tujuan',
			stripeRows: true,
			store: dsDTLTRListTujuan_RevAnggaran,
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 99%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_RevAnggaran =dsDTLTRList_RevAnggaran.getAt(row);
							CurrentTRtujuan_RevAnggaran.row = row;
						}
					}
				}
			),
			cm: TRDetailColumModelTujuan_RevAnggaran(), 
			tbar:
			[
				{
					xtype: 'button',
					id: 'btnAddRowDetailEntryTujuan_RevAnggaran',
					iconCls: 'add',
					text: 'Tambah Baris',
					handler: function()
					{
						TambahBarisRevAngTujuan_RevAnggaran();
					}
				},
				{
					xtype: 'button',
					id: 'btnDelRowDetailEntryTujuan_RevAnggaran',
					iconCls: 'remove',
					text: 'Hapus Baris',
					handler: function()
					{ 		
						HapusBarisDetailAnggaranTujuan();
					}
				}
			],
			// viewConfig: 
			// {
				// forceFit: true
			// },
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{	
							// CalcTotal_RevAnggaran(); 
						} 
					);
					this.store.on("datachanged", function()
						{							
							// CalcTotal_RevAnggaran();
						}
					);
				}
			}
		}
	);
			
	return gridDTLTR_RevAnggaran_Tujuan;
};

function TRDetailColumModel_RevAnggaran() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'ColAccount_asal',
				name: 'ColAccount_asal',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '30%',				
				width: 100,
				editor: new Ext.form.TextField
				(
					{
						id:'fColaccount_asal',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									// var strKriteria =" where tahun_anggaran_ta='" + gstrTahunAngg +"' "//ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "' "
									var strKriteria =" where tahun_anggaran_ta='" + '' +"' "//ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "' "
										strKriteria +=" and  kd_unit_kerja='" + Ext.getCmp('cboUnitKerjaRevAnggaranAsal').getValue() + "' "
										strKriteria +=" and  kd_jenis_rkat='" + Ext.getCmp('CboJenisRKATasal_RevAnggaran').getValue()  + "' "
										strKriteria +=" and  account like '%" + this.getValue()  + "%' "
									
									var asal= true;
									
									var p = new mRecordRevAnggaran
									(
										{
											no_revisi: '', 
											tahun_anggaran_ta: '', 
											kd_unit_kerja: '', 
											kd_jns_rkat_jrka: '',
											kd_jns_other: '', 
											prioritas_rkatr: '', 
											kd_satuan_sat: '', 
											account: '', 
											urut_rkatrdet: '',
											isr_unit_tujuan: '', 
											kuantitas: '', 
											jml_asal: '', 
											jml_tlh_digunakan: '', 
											jml_revisi: '',
											jml_selisih: '', 
											no_program_prog: '',
											deskripsi: '',
											jml_selisih_min: '',
											satuan_sat:'',
											
											no_revisi_tmp : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.no_revisi_tmp, 
											tahun_anggaran_ta_tmp : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.tahun_anggaran_ta_tmp, 
											kd_unit_kerja_tmp  : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.kd_unit_kerja_tmp, 
											no_program_prog_tmp  : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.no_program_prog_tmp, 
											kd_jns_rkat_jrka_tmp  : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.kd_jns_rkat_jrka_tmp, 
											prioritas_tmp  : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.prioritas_tmp,
											urut_tmp  : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.urut_tmp,
											isr_unit_tujuan_tmp  : dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.isr_unit_tujuan_tmp	
										}
									);
									FormLookUpMain_RKATRevAnggaran(strKriteria,dsDTLTRList_RevAnggaran,p,false,CurrentTR_RevAnggaran.row,true,asal);
									
								}
							}
						}
					}
				)
			},		 
			{
				id: 'ColNMAccount_asal',
				name: 'ColNMAccount_asal',
				header: "Nama Account",
				dataIndex: 'name',
				sortable: false,
				anchor: '30%',				
				width: 200
			},		 			
			{
				id: 'Coldeskripsi_asal',
				name: 'Coldeskripsi_asal',
				header: "Deskripsi",
				dataIndex: 'deskripsi_rkatrdet',
				sortable: false,
				anchor: '30%',				
				width: 200
			},
			{
				id: 'Colkuantitas_asal',
				name: 'Colkuantitas_asal',
				header: "Kuantitas",
				dataIndex: 'kuantitas',
				sortable: false,		
				width: 100,
				align:'right',
				editor: new Ext.form.NumberField({
					id:'fcol_qty_as',
					allowBlank: true,
					listeners: 
					{	
						'specialkey': function()
						{
							if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9)
							{											
								
							}
						},
						'blur': function()
						{
																
						},	
						'change':function(field,newValue,oldValue )
						{
							if(Ext.EventObject.getKey() != 13)
							{
							  
								
							}
						}
					}
				})
			},
			{
				id: 'Colsatuan_asal',
				name: 'Colsatuan_asal',
				header: "Satuan",
				dataIndex: 'satuan_sat',
				sortable: false,			
				width: 55,
				editor:mComboFieldEntrySatDetailAnggaranAsal()
			},
			{
				id: 'Colprogram_asal',
				name: 'Colprogram_asal',
				header: "Program",
				dataIndex: 'nama_program',
				sortable: false,
				anchor: '30%',				
				width: 200
			},			
			{
				id: 'ColJmlAngg_asal',	
				name: 'ColJmlAngg_asal',
				header: "Jml Anggaran",
				width: 90,
				dataIndex: 'jml_asal',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_asal);
				}
			},			
			{
				id: 'ColJmldigunakan_asal',	
				name: 'ColJmldigunakan_asal',
				header: "Telah Digunakan",
				width: 100,
				dataIndex: 'jml_tlh_digunakan',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_tlh_digunakan);
				}
			},	
			{
				id: 'ColJmlSelRev_asal',	
				name: 'ColJmlSelRev_asal',
				header: "Jml Selisih(+)",
				width: 90,
				dataIndex: 'jml_selisih',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_selisih);
				}
			
			},
			{
				id: 'ColJmlSelRevMin_asal',	
				name: 'ColJmlSelRevMin_asal',
				header: "Jml Selisih(-)",
				width: 90,
				dataIndex: 'jml_selisih_min',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_selisih_min);
				},
				editor: new Ext.form.NumberField
				(
					{
						id:'fColJmlSelisihmin_asal',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9) 
								{
									//validasi sisa
									var sisa_cek = parseFloat(dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.jml_asal - dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.jml_tlh_digunakan);
									console.log(sisa_cek);
									if(sisa_cek < getNumber(this.getValue())){
										ShowPesanWarningRevAnggaran('Nilai jumlah selisih melebihi sisa anggaran!','Warning');
									}else{
										var total = getNumber(sisa_cek) - getNumber(this.getValue())									
										dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.jml_revisi = total;	
									}
									
								}
							}
						}
					}
				)

			},
			{
				id: 'ColJmlRev_asal',	
				name: 'ColJmlRev_asal',
				header: 'Jml. Revisi',
				width: 150,
				dataIndex: 'jml_revisi',
				align: 'right',
				// style:
				// {	
					// 'text-align':'right'					
				// },
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_revisi);
				}
			}			
		]
	)
};

function mComboFieldEntrySatDetailAnggaranAsal(){
	var Field = ['kd_satuan_sat','satuan_sat'];
	var dsCboFieldSatuanDetailAS = new WebApp.DataStore({ fields: Field });

	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_satuan",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data satuan !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboFieldSatuanDetailAS.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboFieldSatuanDetailAS.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboFieldSatuanDetailAS.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data satuan', 'Error');
			};
		}
	});
	var cboFieldSatuanDetailAS = new Ext.form.ComboBox
	(
		{
			id:'cboFieldSatuanDetailAS',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',	
			width:60,
			store: dsCboFieldSatuanDetailAS,
			valueField: 'satuan_sat',
			displayField: 'satuan_sat',
			listeners:
			{
				'select': function(a,b,c)
				{
					dsDTLTRList_RevAnggaran.data.items[CurrentTR_RevAnggaran.row].data.kd_satuan_sat = b.data.kd_satuan_sat;					
					
				}
			}
		}
	);
	
	return cboFieldSatuanDetailAS;
}
function TRDetailColumModelTujuan_RevAnggaran() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'ColAccount_tujuan',
				name: 'ColAccount_tujuan',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '30%',				
				width: 100,
				editor: new Ext.form.TextField
				(
					{
						id:'fColaccount_tujuan',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13) 
								{
									// var strKriteria =" where TAHUN_ANGGARAN_TA='" + gstrTahunAngg +"' " //ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "' "
									var strKriteria =" where tahun_anggaran_ta='" + '' +"' " //ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "' "
										strKriteria +=" and  kd_unit_kerja='" + Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').getValue() + "' "
										strKriteria +=" and  kd_jenis_rkat='" + Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').getValue()  + "' "
										strKriteria +=" and  account like '%" + this.getValue()  + "%' "
									
									var asal = false;
									var p = new mRecordRevAnggaran
									(
										{
											no_revisi: '', 
											tahun_anggaran_ta: '', 
											kd_unit_kerja: '', 
											kd_jns_rkat_jrka: '',
											kd_jns_other: '', 
											prioritas_rkatr: '', 
											kd_satuan_sat: '', 
											account: '', 
											urut_rkatrdet: '',
											isr_unit_tujuan: '', 
											kuantitas: '', 
											jml_asal: '', 
											jml_tlh_digunakan: '', 
											jml_revisi: '',
											jml_selisih: '', 
											no_program_prog: '',
											deskripsi_rkatrdet: '',
											jml_selisih_min: '',
											
											no_revisi_tmp : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.no_revisi_tmp, 
											tahun_anggaran_ta_tmp : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.tahun_anggaran_ta_tmp, 
											kd_unit_kerja_tmp  : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.kd_unit_kerja_tmp, 
											no_program_prog_tmp  : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.no_program_prog_tmp, 
											kd_jns_rkat_jrka_tmp  : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.kd_jns_rkat_jrka_tmp, 
											prioritas_tmp  : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.prioritas_tmp,
											urut_tmp  : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.urut_tmp,
											isr_unit_tujuan_tmp  : dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.isr_unit_tujuan_tmp	
										}
									);
									FormLookUpMain_RKATRevAnggaran(strKriteria,dsDTLTRListTujuan_RevAnggaran,p,false,CurrentTRtujuan_RevAnggaran.row,true,asal);
								}
							}
						}
					}
				)
			},		 
			{
				id: 'ColNMAccount_tujuan',
				name: 'ColNMAccount_tujuan',
				header: "Nama Account",
				dataIndex: 'name',
				sortable: false,
				anchor: '30%',				
				width: 200
			},		 			
			{
				id: 'Coldeskripsi_tujuan',
				name: 'Coldeskripsi_tujuan',
				header: "Deskripsi",
				dataIndex: 'deskripsi_rkatrdet',
				sortable: false,
				anchor: '30%',				
				width: 200
			},
			{
				id: 'Colkuantitas_tujuan',
				name: 'Colkuantitas_tujuan',
				header: "Kuantitas",
				dataIndex: 'kuantitas',
				align: 'right',
				width: 100,
				editor: new Ext.form.NumberField({
					id:'fcol_qty_at',
					allowBlank: true,
					listeners: 
					{	
						'specialkey': function()
						{
							if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9)
							{											
								
							}
						},
						'blur': function()
						{
																
						},	
						'change':function(field,newValue,oldValue )
						{
							if(Ext.EventObject.getKey() != 13)
							{
							  
								
							}
						}
					}
				})
			},
			{
				id: 'Colsatuan_tujuan',
				name: 'Colsatuan_tujuan',
				header: "Satuan",
				dataIndex: 'satuan_sat',
				sortable: false,
				anchor: '30%',				
				width: 55,
				editor : mComboFieldEntrySatDetailAnggaranTujuan()
			},
			{
				id: 'Colprogram_tujuan',
				name: 'Colprogram_tujuan',
				header: "Program",
				dataIndex: 'nama_program',
				sortable: false,
				anchor: '30%',				
				width: 200
			},			
			{
				id: 'ColJmlAngg_tujuan',	
				name: 'ColJmlAngg_tujuan',
				header: "Jml Anggaran",
				width: 90,
				dataIndex: 'jml_asal',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_asal);
				}
			},			
			{
				id: 'ColJmldigunakan_tujuan',	
				name: 'ColJmldigunakan_tujuan',
				header: "Telah Digunakan",
				width: 100,
				dataIndex: 'jml_tlh_digunakan',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_tlh_digunakan);
				}
			},	
			
			{
				id: 'ColJmlSelRev_tujuan',	
				name: 'ColJmlSelRev_tujuan',
				header: "Jml Selisih(+)",
				width: 90,
				dataIndex: 'jml_selisih',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_selisih);
				},
				editor: new Ext.form.NumberField
				(
					{
						id:'fColJmlSelisih_tujuan',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9) 
								{									
									var total = getNumber(dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.jml_asal) + getNumber(this.getValue())														
									dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.jml_revisi = total;	
								}
							}
						}
					}
				)

			},
			{
				id: 'ColJmlSelRevMin_tujuan',	
				name: 'ColJmlSelRevMin_tujuan',
				header: "Jml Selisih(-)",
				width: 90,
				dataIndex: 'jml_selisih_min',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_selisih_min);
				}
			

			},			
			{
				id: 'ColJmlRev_tujuan',	
				name: 'ColJmlRev_tujuan',
				header: 'Jml. Revisi',
				width: 150,
				dataIndex: 'jml_revisi',
				align: 'right',
				// style:
				// {	
					// 'text-align':'right'					
				// },
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_revisi);
				}
			}			
		]
	)
};

function mComboFieldEntrySatDetailAnggaranTujuan()
{
	var Field = ['kd_satuan_sat','satuan_sat'];
	var dsCboFieldSatuanDetailAT = new WebApp.DataStore({ fields: Field });

	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRAB/get_acc_satuan",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data satuan !', 'Error');
		},	
		success: function(o) 
		{   
			dsCboFieldSatuanDetailAT.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsCboFieldSatuanDetailAT.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsCboFieldSatuanDetailAT.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data satuan', 'Error');
			};
		}
	});
	var cboFieldSatuanDetailAT = new Ext.form.ComboBox
	(
		{
			id:'cboFieldSatuanDetailAT',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',	
			width:60,
			store: dsCboFieldSatuanDetailAT,
			valueField: 'satuan_sat',
			displayField: 'satuan_sat',
			listeners:
			{
				'select': function(a,b,c)
				{
					dsDTLTRListTujuan_RevAnggaran.data.items[CurrentTRtujuan_RevAnggaran.row].data.kd_satuan_sat = b.data.kd_satuan_sat;					
					
				}
			}
		}
	);
	
	return cboFieldSatuanDetailAT;
};
function CalcTotal_RevAnggaran(idx)
{
    var total=Ext.num(0);		
	for (var i = 0; i < dsDTLTRList_RevAnggaran.getCount(); i++) 
	{
		if (i === idx)
		{
			if (Ext.get('fieldJUMLAH_RP') != null )
			{
				if (Ext.num(Ext.get('fieldJUMLAH_RP').dom.value) != null)
				{
					total += Ext.num(Ext.get('fieldJUMLAH_RP').dom.value);
				}
			}
		}
		else
		{
			var jumlah2_RevAnggaran = 0;
			jumlah2_RevAnggaran = dsDTLTRList_RevAnggaran.data.items[i].data.JUMLAH_RP		
			total = total + jumlah2_RevAnggaran
		}
    }		
	Ext.get('txtTotalRevAnggaran').dom.value = formatCurrency(total);	
	
};


function GetStrCriteriaCetak()
{
	var x='';
	if (ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) != '')
	//(Ext.get('TahunRevAnggaranEntry').getValue() != '' && Ext.get('comboUnitKerjaRevAnggaranEntry').dom.value != '' && selectUnitKerjaRevAnggaranEntry !=undefined)
	{
		x = ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "##@@##";
		x +=  selectUnitKerjaRevAnggaranEntry ;		
	};
	return x;
};

function ShowPesanWarningRevAnggaran(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanInfoRevAnggaranRevAnggaran(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function ShowPesanErrorRevAnggaran(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function RevAnggaranSave(mBol) 
{
	if (ValidasiEntryRevAnggaran('Simpan Data') === 1 )
	{
		
		Ext.Ajax.request
		(
			{
				
				url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/SaveRevisiAnggaran",
				params:getParamInputRevAnggaran(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						// ShowPesanInfoRevAnggaranRevAnggaran('Data berhasil di simpan','Simpan Data');
						Ext.getCmp('btnSimpanRevAnggaran').setDisabled(false);
						Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(false);														

						Ext.get('txtAnggaranRevNoRevisi').dom.value=cst.no_revisi;
						RefreshDataRevAnggaran();
						RefreshDtlasal_RevAnggaran(false);
						RefreshDtlasalTujuan_RevAnggaran(false);
						AddNewRevAnggaran = false;
						
						Ext.Msg.show
						(
							{
							   title:'Information',
							   msg: 'Data berhasil di simpan',
							   buttons: Ext.MessageBox.OK,
							   fn: function (btn) 
							   {			
								   if (btn =='ok') 
									{
										if(mBol == true){
											RevAnggaranLookUps.close();
										}
										
									} 
							   },
							   icon: Ext.MessageBox.INFO
							}
						);
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarningRevAnggaran('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
					}
					else 
					{
						ShowPesanErrorRevAnggaran('Data tidak berhasil di simpan, Error : ' + cst.pesan ,'Simpan Data');
					}
				}
			}
		)
	}
	else
	{
		Ext.getCmp('btnSimpanRevAnggaran').setDisabled(false);
		Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(false);														

		if(mBol === true)
		{
			return false;
		};
	};
};

function ApproveRevAng() 
{
	if (ValidasiEntryRevAnggaran('Simpan Data') === 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/ApproveRevisiAnggaran",
				params:getParamInputRevAnggaran(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoRevAnggaranRevAnggaran('Revisi anggaran berhasil diapprove!','Approve Revisi Anggaran');
						Ext.getCmp('btnSimpanRevAnggaran').setDisabled(false);
						Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(false);														
						Ext.getCmp('btnApproveEntryRevAnggaran').setDisabled(true);
						Ext.get('txtAnggaranRevNoRevisi').dom.value=cst.no_revisi;
						RefreshDataRevAnggaran();
						RefreshDtlasal_RevAnggaran(false);
						RefreshDtlasalTujuan_RevAnggaran(false);
						AddNewRevAnggaran = false;
					}
					else 
					{
						ShowPesanErrorRevAnggaran('Error, Revisi anggaran tidak berhasil diapprove!','Approve Revisi Anggaran');
					}
				}
			}
		)
	}
};


function HapusRevAnggaran()
{
	if (ValidasiEntryRevAnggaran('Hapus Data') == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus RevAnggaran',
			   msg: 'Apakah data revisi anggaran nomor : '+ ' ' + Ext.get('txtAnggaranRevNoRevisi').dom.value + ' akan dihapus? ',
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						RevAnggaranDelete();
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
};

function RevAnggaranDelete() 
{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/HapusRevisiAnggaran",
				params: { 
					no_revisi:Ext.get('txtAnggaranRevNoRevisi').getValue()
				}, 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoRevAnggaranRevAnggaran('Data berhasil di hapus','Hapus Data');
						RefreshDataRevAnggaran();
						RevAnggaranAddNew();
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarningRevAnggaran('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else if (cst.success === false && cst.pesan === 999 )
					{
						ShowPesanWarningRevAnggaran('Data tidak berhasil di hapus, data tersebut telah dipakai','Hapus Data');
					}
					else 
					{
						ShowPesanErrorRevAnggaran('Data tidak berhasil di hapus, Error : ' + cst.pesan,'Hapus Data');
					}
				}
			}
		)
}; 

function GetAngkaRevAnggaran(str)
{
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};

function ValidasiEntryRevAnggaran(modul)
{
	var x = 1;
	if (Ext.get('cboUnitKerjaRevAnggaranAsal').getValue() == '')
	{
		ShowPesanWarningRevAnggaran(gstrSatker+' Asal Belum Terisi',modul);
		x=0;
	}
	if (Ext.get('CboJenisRKATasal_RevAnggaran').getValue() == '')
	{
		ShowPesanWarningRevAnggaran('Jenis '+gstrrkat+' Asal Belum Terisi',modul);
		x=0;
	}
	if (Ext.get('cboUnitKerjaRevAnggaranTujuan').getValue() == '')
	{
		ShowPesanWarningRevAnggaran(gstrSatker+' Tujuan Belum Terisi',modul);
		x=0;
	}
	if (Ext.get('CboJenisRKATtujuan_RevAnggaran').getValue() == '')
	{
		ShowPesanWarningRevAnggaran('Jenis '+gstrrkat+' Tujuan Belum Terisi',modul);
		x=0;
	}
	
	if (dsDTLTRList_RevAnggaran.getCount()>0)
	{
		if (ValidDetailRevAnggaran(x)==1)
		{x = 1;}
		else
		{x = 0;}
	}
	else
	{
		ShowPesanWarningRevAnggaran('Data Detail Belum diisi', modul);
		x = 0;	
	}
	if (dsDTLTRListTujuan_RevAnggaran.getCount()==0)
	{
		ShowPesanWarningRevAnggaran('Data Detail Belum diisi', modul);
		x = 0;	
	}
	
	return x;
};
///-----------------------------------------------------------------------------------------///


function ValidDetailRevAnggaran(x) 
{    
	var jmlRevAsal=0;
	var jmlRevTujuan=0;
	
    for (var i = 0; i < dsDTLTRList_RevAnggaran.getCount(); i++) 
	{		
        if ((dsDTLTRList_RevAnggaran.data.items[i].data.jml_revisi < dsDTLTRList_RevAnggaran.data.items[i].data.jml_tlh_digunakan) 
			&& dsDTLTRList_RevAnggaran.data.items[i].data.jml_revisi != 0)
		{
            ShowPesanWarningRevAnggaran('Nilai revisi melebihi batas nilai yang telah digunakan','Warning');
            x = 0;		
		}		
    }
	
	for (var i = 0; i < dsDTLTRList_RevAnggaran.getCount(); i++) 
	{
		jmlRevAsal += parseFloat(dsDTLTRList_RevAnggaran.data.items[i].data.jml_selisih_min)
	}
	
	for (var i = 0; i < dsDTLTRListTujuan_RevAnggaran.getCount(); i++) 
	{
		jmlRevTujuan += parseFloat(dsDTLTRListTujuan_RevAnggaran.data.items[i].data.jml_selisih)
	}
	
	console.log(jmlRevAsal,jmlRevTujuan);
	if (parseFloat(jmlRevTujuan) != parseFloat(jmlRevAsal))
	{
		ShowPesanWarningRevAnggaran('Nilai total revisi anggaran asal dan tujuan berbeda');
		x = 0;	
	}
    return x;
};

function RefreshDataRevAnggaran_lama()
{		
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/LoadRevisiAnggaran",
		params: {
			tgl_awal		: Ext.getCmp('dtpFilterTgl1RevAnggaran').getValue(),
			tgl_akhir		: Ext.getCmp('dtpFilterTgl2RevAnggaran').getValue(),
			kd_unit_kerja	: Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue(),
			limit			: Ext.getCmp('cboMaksDataRevAnggaran').getValue()
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data Revisi Anggaran !', 'Error');
		},	
		success: function(o) 
		{   
			dsRevAnggaranList.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsRevAnggaranList.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsRevAnggaranList.add(recs);
				grListRevAnggaran.getView().refresh();
			
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data Revisi Anggaran', 'Error');
			};
		},
		callback:function(){
			var gridListRAB=Ext.getCmp('grListRevAnggaran').getStore().data.items;
				for(var i=0,iLen=gridListRAB.length; i<iLen;i++){
					if(gridListRAB[i].data.app_revang == 1){
						gridListRAB[i].data.app_revang_tmp = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grListRevAnggaran').getStore().data.items);
			Ext.getCmp('grListRevAnggaran').getView().refresh();
			
		}
	});
	
};

function RefreshDataRevAnggaran()
{		
	var criteria = GetCriteriaGridUtama();
	dsRevAnggaranList.removeAll();
	dsRevAnggaranList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: Ext.getCmp('cboMaksDataRevAnggaran').getValue(), 
				Sort: '', 
				Sortdir: '', 
				target: 'vi_viewdata_revisi_anggaran',
				param: criteria
			} ,
			callback:function(){
				var gridListRAB=Ext.getCmp('grListRevAnggaran').getStore().data.items;
				for(var i=0,iLen=gridListRAB.length; i<iLen;i++){
					if(gridListRAB[i].data.app_revang == 1){
						gridListRAB[i].data.app_revang_tmp = true;
						console.log('true');
					} 
				}
				console.log(Ext.getCmp('grListRevAnggaran').getStore().data.items);
				Ext.getCmp('grListRevAnggaran').getView().refresh();
				
			}
		}
	);
	return dsRevAnggaranList; 
	
};

function GetCriteriaGridUtama(){
	var criteria = '';
	if (Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue() == '000' || Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue() == 000){
		criteria = "SEMUA~WHERE a.tgl_revisi between '" + Ext.get('dtpFilterTgl1RevAnggaran').getValue() + " '  and '" + Ext.get('dtpFilterTgl2RevAnggaran').getValue() + " ' ";
	}else{
		criteria = "  WHERE a.tgl_revisi between '" + Ext.get('dtpFilterTgl1RevAnggaran').getValue() + " '  and '" + Ext.get('dtpFilterTgl2RevAnggaran').getValue() + " ' and ( a.kd_unit_kerja = '" + Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue() + "' or a.kd_unit_kerja_tujuan = '" + Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue() + "') ";
	}
	
	return criteria;
}


function RefreshDataRevAnggaranFilter(mBol) 
{   
	var strRevAnggaran='';
	
	strRevAnggaran = " Where RA.TGL_REVISI >='" + FormatDateReport(Ext.getCmp('dtpFilterTgl1RevAnggaran').getValue())  + "' ";
	strRevAnggaran += " and  RA.TGL_REVISI <='" + FormatDateReport(Ext.getCmp('dtpFilterTgl2RevAnggaran').getValue())  + "' ";

	if (Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue() != '') 
	{
		strRevAnggaran += " AND RA.KD_UNIT_KERJA ='" + Ext.getCmp('cboUnitKerjaRevAnggaranFilter').getValue()  + "' ";
	};
	
    if (strRevAnggaran != undefined) 
    {  
		dsRevAnggaranList.load
		(
			{ 
				params:  
				{   
					Skip:0, 
					Take:selectCountRevAnggaran, 
					Sort:'Tahun', 
					Sortdir:'ASC', 
					target: 'VIviewACC_REVISI_ANGGARAN',
					param: strRevAnggaran
				}			
			}
		);        
    }
	else
	{
		RefreshDataRevAnggaran();
	};
};

function mComboMaksDataRevAnggaran()
{
  var cboMaksDataRevAnggaran = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataRevAnggaran',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountRevAnggaran,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountRevAnggaran=b.data.displayText ;
					RefreshDataRevAnggaran();
				} 
			}
		}
	);
	return cboMaksDataRevAnggaran;
};

function getNumberRevAnggaran(dblNilai)
{
    var dblAmount;
	if (dblNilai==='')
	{
		dblNilai=0;
	};
    dblAmount = dblNilai;
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return Ext.num(dblAmount)
};

function getParamClickAppRevAnggaran(rowData) 
{
    var params = 
	{
		Table:'viACC_REVISI_ANGGARAN',
		NO_REVISI:rowData.NO_REVISI,
		IS_APPROVE:BlnAppRevAnggaran
    };
    return params;
};

function getParamInputRevAnggaran() 
{
    var params = 
	{
		no_revisi					:Ext.get('txtAnggaranRevNoRevisi').getValue(),
		kd_unit_kerja				:Ext.getCmp('cboUnitKerjaRevAnggaranAsal').getValue(),
		kd_unit_kerja_tujuan		:Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').getValue(),
		type_kd_rkat				:Ext.getCmp('CboJenisRKATasal_RevAnggaran').getValue(),
		type_kd_rkat_tujuan			:Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').getValue(),
		tgl_revisi					:Ext.get('dtpTanggal_RevAnggaran').getValue(),
		is_approve					:BlnAppRevAnggaran
    };
	
	params['jumlah_list_asal']			=	dsDTLTRList_RevAnggaran.getCount();
	console.log(dsDTLTRList_RevAnggaran.data);
	for(var i = 0 ; i < dsDTLTRList_RevAnggaran.getCount();i++)
	{
		params['tahun_anggaran_ta-'+i]		=	dsDTLTRList_RevAnggaran.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja-'+i]			=	dsDTLTRList_RevAnggaran.data.items[i].data.kd_unit_kerja
		params['kd_jns_rkat_jrka-'+i]		=	dsDTLTRList_RevAnggaran.data.items[i].data.kd_jns_rkat_jrka
		params['kd_jns_other-'+i]			=	dsDTLTRList_RevAnggaran.data.items[i].data.kd_jns_other
		params['prioritas-'+i]				=	dsDTLTRList_RevAnggaran.data.items[i].data.prioritas_rkatr
		params['kd_satuan_sat-'+i]			=	dsDTLTRList_RevAnggaran.data.items[i].data.kd_satuan_sat
		params['account-'+i]				=	dsDTLTRList_RevAnggaran.data.items[i].data.account
		params['urut-'+i]					=	dsDTLTRList_RevAnggaran.data.items[i].data.urut_rkatrdet
		params['kuantitas-'+i]				=	dsDTLTRList_RevAnggaran.data.items[i].data.kuantitas
		params['jml_revisi-'+i]				=	dsDTLTRList_RevAnggaran.data.items[i].data.jml_revisi
		params['jml_selisih-'+i]			=	dsDTLTRList_RevAnggaran.data.items[i].data.jml_selisih
		params['jml_asal-'+i]				=	dsDTLTRList_RevAnggaran.data.items[i].data.jml_asal
		/* params['m1-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m1
		params['m2-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m2
		params['m3-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m3
		params['m4-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m4
		params['m5-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m5
		params['m6-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m6
		params['m7-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m7
		params['m8-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m8
		params['m9-'+i]						=	dsDTLTRList_RevAnggaran.data.items[i].data.m9
		params['m10-'+i]					=	dsDTLTRList_RevAnggaran.data.items[i].data.m10
		params['m11-'+i]					=	dsDTLTRList_RevAnggaran.data.items[i].data.m11
		params['m12-'+i]					=	dsDTLTRList_RevAnggaran.data.items[i].data.m12 */
		params['no_program_prog-'+i]		=	dsDTLTRList_RevAnggaran.data.items[i].data.no_program_prog
		params['deskripsi_rkatrdet-'+i]		=	dsDTLTRList_RevAnggaran.data.items[i].data.deskripsi_rkatrdet
		params['jml_selisih_min-'+i]		=	dsDTLTRList_RevAnggaran.data.items[i].data.jml_selisih_min //asal
		
	}
    
	
	params['jumlah_list_tujuan']			=	dsDTLTRListTujuan_RevAnggaran.getCount();
	for(var i = 0 ; i < dsDTLTRListTujuan_RevAnggaran.getCount();i++)
	{
		params['tahun_anggaran_ta_tujuan-'+i]		=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.tahun_anggaran_ta
		params['kd_unit_kerja_tujuan-'+i]			=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.kd_unit_kerja
		params['kd_jns_rkat_jrka_tujuan-'+i]		=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.kd_jns_rkat_jrka
		params['kd_jns_other_tujuan-'+i]			=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.kd_jns_other
		params['prioritas_tujuan-'+i]				=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.prioritas_rkatr
		params['kd_satuan_sat_tujuan-'+i]			=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.kd_satuan_sat
		params['account_tujuan-'+i]					=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.account
		params['urut_tujuan-'+i]					=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.urut_rkatrdet
		params['kuantitas_tujuan-'+i]				=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.kuantitas
		params['jml_revisi_tujuan-'+i]				=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.jml_revisi
		params['jml_selisih_tujuan-'+i]				=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.jml_selisih
		params['jml_asal_tujuan-'+i]				=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.jml_asal
	/* 	params['m1_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m1
		params['m2_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m2
		params['m3_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m3
		params['m4_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m4
		params['m5_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m5
		params['m6_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m6
		params['m7_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m7
		params['m8_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m8
		params['m9_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m9
		params['m10_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m10
		params['m11_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m11
		params['m12_tujuan-'+i]						=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.m12 
	*/
		params['no_program_prog_tujuan-'+i]			=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.no_program_prog
		params['deskripsi_rkatrdet_tujuan-'+i]		=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.deskripsi_rkatrdet
		params['jml_selisih_min_tujuan-'+i]			=	dsDTLTRListTujuan_RevAnggaran.data.items[i].data.jml_selisih_min
		
	}
	return params;
};




function RevAnggaranInit(rowdata) 
{
	var StrKriteria;
	console.log(rowdata);
	Ext.getCmp('txtAnggaranRevNoRevisi').setValue(rowdata.no_revisi);		
	Ext.getCmp('cboUnitKerjaRevAnggaranAsal').setValue(rowdata.kd_unit_kerja);	
	Ext.getCmp('CboJenisRKATasal_RevAnggaran').setValue(rowdata.type_kd_rkat);
	Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').setValue(rowdata.kd_unit_kerja_tujuan);
	Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').setValue(rowdata.type_kd_rkat_tujuan);	

	Ext.get('dtpTanggal_RevAnggaran').dom.value = ShowDate(rowdata.tgl_revisi);
	Ext.get('cboUnitKerjaRevAnggaranAsal').dom.value = rowdata.unit_asal;
	Ext.get('CboJenisRKATasal_RevAnggaran').dom.value = rowdata.rkat_asal;
	Ext.get('cboUnitKerjaRevAnggaranTujuan').dom.value = rowdata.unit_tujuan;
	Ext.get('CboJenisRKATtujuan_RevAnggaran').dom.value = rowdata.rkat_tujuan;
	
	
	if(Ext.getCmp('txtAnggaranRevNoRevisi').getValue() != undefined){
		Ext.getCmp('txtAnggaranRevNoRevisi').setReadOnly(true);
		Ext.getCmp('cboUnitKerjaRevAnggaranAsal').setReadOnly(true);
		Ext.getCmp('CboJenisRKATasal_RevAnggaran').setReadOnly(true);
		Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').setReadOnly(true);
		Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').setReadOnly(true);
	}
    AddNewRevAnggaran = false;	
	// RefreshDataDetail_RevAnggaran(StrKriteria)
	RefreshDtlasalTujuan_RevAnggaran();
	RefreshDtlasal_RevAnggaran();
	
	if(rowdata.app_revang == 1){
		Ext.getCmp('btnSimpanRevAnggaran').setDisabled(true);
		Ext.getCmp('btnSimpanCloseRevAnggaran').setDisabled(true);
		Ext.getCmp('btnHapusRevAnggaran').setDisabled(true);
		Ext.getCmp('btnApproveEntryRevAnggaran').setDisabled(true);
		Ext.getCmp('btnAddRowDetailEntryAsal_RevAnggaran').setDisabled(true);
		Ext.getCmp('btnDelRowDetailEntryAsal_RevAnggaran').setDisabled(true);
		Ext.getCmp('btnAddRowDetailEntryTujuan_RevAnggaran').setDisabled(true);
		Ext.getCmp('btnDelRowDetailEntryTujuan_RevAnggaran').setDisabled(true);
	}
};

function RevAnggaranAddNew() 
{	
    AddNewRevAnggaran = true;	
	Ext.getCmp('txtAnggaranRevNoRevisi').setValue("");	
	// Ext.get('dtpTanggal_RevAnggaran').dom.value = ShowDate(now_RevAnggaran);
	Ext.getCmp('cboUnitKerjaRevAnggaranAsal').setValue("");	
	Ext.getCmp('CboJenisRKATasal_RevAnggaran').setValue("");
	Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').setValue("");
	Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').setValue("");	
	// RefreshDtlasal_RevAnggaran();
	dsDTLTRList_RevAnggaran.removeAll();
	dsDTLTRListTujuan_RevAnggaran.removeAll();
};

function RefreshDtlasal_RevAnggaran()
{		
	/* var strKriteria =" WHERE NO_REVISI='" + Ext.get('txtAnggaranRevNoRevisi').getValue() + "' and isr_unit_tujuan=0 "
	dsDTLTRList_RevAnggaran.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'MAK',
				Sortdir: 'ASC',
				target:'Viviewdetailrevisianggaran',
				param: strKriteria
			}
		}
	);
	// alert(strKriteria)
	return dsDTLTRList_RevAnggaran; */
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/LoadAnggaranDetail",
		params: {
			no_revisi		: Ext.get('txtAnggaranRevNoRevisi').getValue()	,
			isr_unit_tujuan	: 0
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data detail anggaran asal !', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_RevAnggaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_RevAnggaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_RevAnggaran.add(recs);
				gridDTLTR_RevAnggaran.getView().refresh();
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data detail anggaran asal', 'Error');
			};
		}
	});
};

function RefreshDtlTombolLoadasal_RevAnggaran()
{		
	var strKriteria =" where TAHUN_ANGGARAN_TA='" + ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "' "
		strKriteria +=" and  KD_UNIT_KERJA='" + Ext.getCmp('cboUnitKerjaRevAnggaranAsal').getValue() + "' "
		strKriteria +=" and  KD_JENIS_RKAT='" + Ext.getCmp('CboJenisRKATasal_RevAnggaran').getValue()  + "' "
		
	dsDTLTRList_RevAnggaran.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'MAK',
				Sortdir: 'ASC',
				target:'ViviewdetailRKATrevisianggaran',
				param: strKriteria
			}
		}
	);
	return dsDTLTRList_RevAnggaran;
};

function RefreshDtlTombolLoadTujuan_RevAnggaran()
{		
	var strKriteria =" where TAHUN_ANGGARAN_TA='" + ShowYear(Ext.get('dtpTanggal_RevAnggaran').getValue()) + "' "
		strKriteria +=" and  KD_UNIT_KERJA='" + Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').getValue() + "' "
		strKriteria +=" and  KD_JENIS_RKAT='" + Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').getValue()  + "' "

	dsDTLTRListTujuan_RevAnggaran.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'MAK',
				Sortdir: 'ASC',
				target:'ViviewdetailRKATrevisianggaran',
				param: strKriteria
			}
		}
	);
	return dsDTLTRListTujuan_RevAnggaran;
};


function RefreshDtlasalTujuan_RevAnggaran()
{		
	/* var strKriteria =" WHERE NO_REVISI='" + Ext.get('txtAnggaranRevNoRevisi').getValue() + "' and isr_unit_tujuan=1 "
	dsDTLTRListTujuan_RevAnggaran.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'MAK',
				Sortdir: 'ASC',
				target:'Viviewdetailrevisianggaran',
				param: strKriteria
			}
		}
	);
	// alert(strKriteria)
	return dsDTLTRListTujuan_RevAnggaran; */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/LoadAnggaranDetail",
		params: {
			no_revisi		: Ext.get('txtAnggaranRevNoRevisi').getValue()	,
			isr_unit_tujuan	: 1
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data detail anggaran tujuan !', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRListTujuan_RevAnggaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRListTujuan_RevAnggaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRListTujuan_RevAnggaran.add(recs);
				gridDTLTR_RevAnggaran_Tujuan.getView().refresh();
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data detail anggaran tujuan', 'Error');
			};
		}
	});
	
};


// function RefreshDataDetail_RevAnggaran(strKriteria)
// {		
	// dsDTLTRList_RevAnggaran.load
	// (
		// { 
			// params: 
			// { 	
				// Skip: 0,
				// Take: 50,
				// Sort: 'MAK',
				// Sortdir: 'ASC',
				// target:'VIviewACC_PLAFOND_KOMPONEN',
				// param: strKriteria
			// }
		// }
	// );
	// return dsDTLTRList_RevAnggaran;
// };

function GetParamRevAnggaranInputRevAnggaran()
{
	var x='';	
	if (Ext.get('TahunRevAnggaranEntry').dom.value != '' && Ext.get('comboUnitKerjaRevAnggaranEntry').dom.value != '' && selectUnitKerjaRevAnggaranEntry !=undefined)
	{
		x =" where tahun= " + Ext.get('TahunRevAnggaranEntry').dom.value ;
		x += " and kd_unit_kerja=~" + selectUnitKerjaRevAnggaranEntry + "~";		
	};
	return x;
};

// function CalcTotalRevAnggaranDetail(idx)
// {
	// Ext.getCmp('txtTotalRevAnggaran').setValue(formatCurrency());	
// };

//lookup 
/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookAcc;
var vWinFormEntry;


function FormLookUpMain_RKATRevAnggaran(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,asal) 
{	
	
	var kd_unit_kerja ='';
	var kd_jns_plafond ='';
	if( asal == true)
	{
		kd_unit_kerja 		= Ext.getCmp('cboUnitKerjaRevAnggaranAsal').getValue(),
		kd_jns_plafond 		= Ext.getCmp('CboJenisRKATasal_RevAnggaran').getValue()
	}else{
		kd_unit_kerja 		= Ext.getCmp('cboUnitKerjaRevAnggaranTujuan').getValue(),
		kd_jns_plafond 		= Ext.getCmp('CboJenisRKATtujuan_RevAnggaran').getValue()
	}
	
	
    vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormROLookup',
			title: 'Lookup Anggaran',
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUp_RKATRevAnggaran(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,kd_unit_kerja,kd_jns_plafond)
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
	
	if( kd_unit_kerja == '' || kd_jns_plafond== '')
	{
		ShowPesanWarningRevAnggaran('Unit Kerja atau Jenis RAB belum dipilih!','Warning');
	}else{
		 vWinFormEntry.show();
	}
	
   
};

function GetPanelLookUp_RKATRevAnggaran(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,kd_unit_kerja,kd_jns_plafond)
{
	console.log(kd_unit_kerja,kd_jns_plafond);
	var FormLookUp_RKATRevAnggaran = new Ext.Panel    // from transaksi jurnal
	(
		{
			id: 'FormLookUp_RKATRevAnggaran',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUp_RKATRevAnggaran(criteria,kd_unit_kerja,kd_jns_plafond,dsStore,p,mBolAddNew,idx,mBolLookup),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'11px','margin-top':'2px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items:
					[
						{
							xtype:'button',
							text:'Ok',
							width:70,
							style:{'margin-left':'3px','margin-top':'2px'},
							hideLabel:true,
							id: 'btnOkAcc_RKATRevAnggaran',
							handler:function()
							{
								if (p != undefined && rowSelectedLookAcc != undefined)
								{
									if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.account=rowSelectedLookAcc.data.account;
										p.data.name=rowSelectedLookAcc.data.name;
										p.data.deskripsi_rkatrdet=rowSelectedLookAcc.data.deskripsi_rkatrdet;
										p.data.nama_program=rowSelectedLookAcc.data.nama_program;
										p.data.jml_asal=rowSelectedLookAcc.data.jml_asal;
										
										p.data.jml_tlh_digunakan=rowSelectedLookAcc.data.jml_tlh_digunakan;
										
										p.data.tahun_anggaran_ta=rowSelectedLookAcc.data.tahun_anggaran_ta;
										p.data.kd_unit_kerja=rowSelectedLookAcc.data.kd_unit_kerja;
										p.data.no_program_prog=rowSelectedLookAcc.data.no_program_prog;
										p.data.kd_jns_rkat_jrka=rowSelectedLookAcc.data.kd_jns_rkat_jrka;
										p.data.prioritas_rkatr=rowSelectedLookAcc.data.prioritas_rkatr;
										p.data.urut_rkatrdet = rowSelectedLookAcc.data.urut_rkatrdet;
										p.data.kuantitas=rowSelectedLookAcc.data.kuantitas_rkatrdet;
										p.data.kd_satuan_sat=rowSelectedLookAcc.data.kd_satuan_sat;
										p.data.satuan_sat=rowSelectedLookAcc.data.satuan_sat;
										p.data.kd_jns_other=rowSelectedLookAcc.data.kd_jns_other;
										console.log();
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
										
									}
									else
									{									    
										if (dsStore != undefined)
										{											
											p.data.account=rowSelectedLookAcc.data.account;
											p.data.name=rowSelectedLookAcc.data.name;
											p.data.deskripsi_rkatrdet=rowSelectedLookAcc.data.deskripsi_rkatrdet;
											p.data.nama_program=rowSelectedLookAcc.data.nama_program;
											p.data.jml_asal=rowSelectedLookAcc.data.jml_asal;
											
											p.data.jml_tlh_digunakan=rowSelectedLookAcc.data.jml_tlh_digunakan;
											
											p.data.tahun_anggaran_ta=rowSelectedLookAcc.data.tahun_anggaran_ta;
											p.data.kd_unit_kerja=rowSelectedLookAcc.data.kd_unit_kerja;
											p.data.no_program_prog=rowSelectedLookAcc.data.no_program_prog;
											p.data.kd_jns_rkat_jrka=rowSelectedLookAcc.data.kd_jns_rkat_jrka;
											p.data.prioritas_rkatr=rowSelectedLookAcc.data.prioritas_rkatr;
											p.data.urut_rkatrdet = rowSelectedLookAcc.data.urut_rkatrdet;
											p.data.kuantitas=rowSelectedLookAcc.data.kuantitas_rkatrdet;
											p.data.kd_satuan_sat=rowSelectedLookAcc.data.kd_satuan_sat;
											p.data.satuan_sat=rowSelectedLookAcc.data.satuan_sat;
											p.data.kd_jns_other=rowSelectedLookAcc.data.kd_jns_other;
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}
									}
								}
								vWinFormEntry.close();
								console.log(p.data);
							}
						},
						{
								xtype:'button',
								text:'Cancel',
								width:70,
								style:{'margin-left':'5px','margin-top':'2px'},
								hideLabel:true,
								id: 'btnCancelAcc_RKATRevAnggaran',
								handler:function() 
								{
									vWinFormEntry.close();
								}
						}
					]
				}
			]
        }
	);
	
	return FormLookUp_RKATRevAnggaran;
};

//---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUp_RKATRevAnggaran(criteria,kd_unit_kerja,kd_jns_plafond,dsStore,p,mBolAddNew,idx,mBolLookup) 
{  
	var fldDetail = 
	[	
		'no_revisi', 'tahun_anggaran_ta','kd_jns_plafond', 'kd_unit_kerja', 'kd_jns_rkat_jrka',
		'kd_jns_other', 'prioritas', 'kd_satuan_sat', 'account', 'urut',
		'isr_unit_tujuan', 'kuantitas_rkatrdet', 'jml_asal', 'jml_tlh_digunakan', 'jml_revisi',
		'jml_selisih', 'm1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9', 'm10', 'm11', 'm12','no_program_prog',
		'satuan_sat', 'name', 'nama_program' ,'deskripsi_rkatrdet','jml_selisih_min' 
	];
	
	dsLookAccList_RKATRevAnggaran = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookFormEntry_RKATRevAnggaran = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookFormEntry_RKATRevAnggaran',
			title: '',
			stripeRows: true,
			store: dsLookAccList_RKATRevAnggaran,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookAcc = dsLookAccList_RKATRevAnggaran.getAt(row);
						}
					}
			}
				),
			cm: fnGridLookCM_RKATRevAnggaran(),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					if (p != undefined && rowSelectedLookAcc != undefined)
					{
						if (mBolAddNew === true || mBolAddNew === undefined)
						{
							p.data.account=rowSelectedLookAcc.data.account;
							p.data.name=rowSelectedLookAcc.data.name;
							p.data.deskripsi_rkatrdet=rowSelectedLookAcc.data.deskripsi_rkatrdet;
							p.data.nama_program=rowSelectedLookAcc.data.nama_program;
							p.data.jml_asal=rowSelectedLookAcc.data.jml_asal;
							
							p.data.jml_tlh_digunakan=rowSelectedLookAcc.data.jml_tlh_digunakan;
							
							p.data.tahun_anggaran_ta=rowSelectedLookAcc.data.tahun_anggaran_ta;
							p.data.kd_unit_kerja=rowSelectedLookAcc.data.kd_unit_kerja;
							p.data.no_program_prog=rowSelectedLookAcc.data.no_program_prog;
							p.data.kd_jns_rkat_jrka=rowSelectedLookAcc.data.kd_jns_rkat_jrka;
							p.data.prioritas_rkatr=rowSelectedLookAcc.data.prioritas_rkatr;
							p.data.urut_rkatrdet = rowSelectedLookAcc.data.urut_rkatrdet;
							p.data.kuantitas=rowSelectedLookAcc.data.kuantitas_rkatrdet;
							p.data.kd_satuan_sat=rowSelectedLookAcc.data.kd_satuan_sat;
							p.data.satuan_sat=rowSelectedLookAcc.data.satuan_sat;
							p.data.kd_jns_other=rowSelectedLookAcc.data.kd_jns_other;
							console.log();
							if (mBolLookup === true)
							{
							   dsStore.insert(dsStore.getCount(), p);
							}
							else
							{
							   dsStore.removeAt(dsStore.getCount()-1);
							   dsStore.insert(dsStore.getCount(), p);
							}
							
						}
						else
						{									    
							if (dsStore != undefined)
							{											
								p.data.account=rowSelectedLookAcc.data.account;
								p.data.name=rowSelectedLookAcc.data.name;
								p.data.deskripsi_rkatrdet=rowSelectedLookAcc.data.deskripsi_rkatrdet;
								p.data.nama_program=rowSelectedLookAcc.data.nama_program;
								p.data.jml_asal=rowSelectedLookAcc.data.jml_asal;
								
								p.data.jml_tlh_digunakan=rowSelectedLookAcc.data.jml_tlh_digunakan;
								
								p.data.tahun_anggaran_ta=rowSelectedLookAcc.data.tahun_anggaran_ta;
								p.data.kd_unit_kerja=rowSelectedLookAcc.data.kd_unit_kerja;
								p.data.no_program_prog=rowSelectedLookAcc.data.no_program_prog;
								p.data.kd_jns_rkat_jrka=rowSelectedLookAcc.data.kd_jns_rkat_jrka;
								p.data.prioritas_rkatr=rowSelectedLookAcc.data.prioritas_rkatr;
								p.data.urut_rkatrdet = rowSelectedLookAcc.data.urut_rkatrdet;
								p.data.kuantitas=rowSelectedLookAcc.data.kuantitas_rkatrdet;
								p.data.kd_satuan_sat=rowSelectedLookAcc.data.kd_satuan_sat;
								p.data.satuan_sat=rowSelectedLookAcc.data.satuan_sat;
								p.data.kd_jns_other=rowSelectedLookAcc.data.kd_jns_other;
								dsStore.removeAt(idx);
								dsStore.insert(idx, p);
							}
						}
					}
					vWinFormEntry.close();
				}
				// End Function # --------------
			},
			// ,
			// viewConfig: { forceFit: true }
		});
		
	LoadAkunRABAsal(kd_unit_kerja,kd_jns_plafond); // memanggil function untuk mengisi lookup akun RAB
	
	return vGridLookFormEntry_RKATRevAnggaran;
};

function LoadAkunRABAsal(kd_unit_kerja,kd_jns_plafond)
{

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/LoadAkunRABAsal",
		params: {
			tahun_anggaran_ta 	:	Ext.getCmp('dtpTanggal_RevAnggaran').getValue(),
			kd_unit_kerja		:	kd_unit_kerja,
			kd_jns_plafond		:   kd_jns_plafond
			
		},
		failure: function(o)
		{
			ShowPesanErrorRevAnggaran('Error menampilkan data akun !', 'Error');
		},	
		success: function(o) 
		{   
			dsLookAccList_RKATRevAnggaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsLookAccList_RKATRevAnggaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsLookAccList_RKATRevAnggaran.add(recs);
			} else {
				ShowPesanErrorRevAnggaran('Gagal menampilkan data akun', 'Error');
			};
			
			console.log(dsLookAccList_RKATRevAnggaran);
		}
	});

	

};

function HapusBarisDetailAnggaranAsal()
{
	var line = gridDTLTR_RevAnggaran.getSelectionModel().selection.cell[0];
	var o = dsDTLTRList_RevAnggaran.getRange()[line].data;
	console.log(o);
	Ext.Msg.confirm('Warning', 'Apakah data detail anggaran asal  ini akan dihapus?', function(button){
		if (button == 'yes'){
			if( o.no_revisi !== '' && o.isr_unit_tujuan !== '' ){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/HapusDetailAnggaran",
						params:{
							no_revisi			:o.no_revisi,
							tahun_anggaran_ta	:o.tahun_anggaran_ta,
							kd_unit_kerja		:o.kd_unit_kerja,
							kd_jns_rkat_jrka	:o.kd_jns_rkat_jrka,
							prioritas_rkatr		:o.prioritas_rkatr,
							urut_rkatrdet		:o.urut_rkatrdet,
							isr_unit_tujuan		:o.isr_unit_tujuan
						} ,
						failure: function(o)
						{
							ShowPesanErrorRevAnggaran('Error menghapus detail  anggaran asal !', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsDTLTRList_RevAnggaran.removeAt(line);
								gridDTLTR_RevAnggaran.getView().refresh();
								ShowPesanInfoRevAnggaranRevAnggaran('Data detail anggaran asal berhasil dihapus','Information');
								ValidDetailRevAnggaran();
							}
							else 
							{
								ShowPesanErrorRevAnggaran('Gagal menghapus data detail anggaran asal!', 'Error');
							};
						}
					}
					
				)
			}else{
				dsDTLTRList_RevAnggaran.removeAt(line);
				gridDTLTR_RevAnggaran.getView().refresh();
				ShowPesanInfoRevAnggaranRevAnggaran('Data detail anggaran asal berhasil dihapus','Information');
				 
			}
		} 
		
	});
	
}


function HapusBarisDetailAnggaranTujuan()
{
	var line = gridDTLTR_RevAnggaran_Tujuan.getSelectionModel().selection.cell[0];
	var o = dsDTLTRListTujuan_RevAnggaran.getRange()[line].data;
	console.log(o);
	Ext.Msg.confirm('Warning', 'Apakah data detail anggaran asal  ini akan dihapus?', function(button){
		if (button == 'yes'){
			// if( dsDTLTRListTujuan_RevAnggaran.getRange()[line].data.account !== '' && dsDTLTRListTujuan_RevAnggaran.getRange()[line].data.name !== '' && dsDTLTRListTujuan_RevAnggaran.getRange()[line].data.jml_asal !== 0){
			if( o.no_revisi !== '' && o.isr_unit_tujuan !== ''){
				Ext.Ajax.request
				(
					{
						url: baseURL + "index.php/anggaran_module/functionRevisiAnggaran/HapusDetailAnggaran",
						params:{
							no_revisi			:o.no_revisi,
							tahun_anggaran_ta	:o.tahun_anggaran_ta,
							kd_unit_kerja		:o.kd_unit_kerja,
							kd_jns_rkat_jrka	:o.kd_jns_rkat_jrka,
							prioritas_rkatr		:o.prioritas_rkatr,
							urut_rkatrdet		:o.urut_rkatrdet,
							isr_unit_tujuan		:o.isr_unit_tujuan
						} ,
						failure: function(o)
						{
							ShowPesanErrorRevAnggaran('Error menghapus detail  anggaran tujuan !', 'Error');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) 
							{
								dsDTLTRListTujuan_RevAnggaran.removeAt(line);
								gridDTLTR_RevAnggaran_Tujuan.getView().refresh();
								ShowPesanInfoRevAnggaranRevAnggaran('Data detail anggaran tujuan berhasil dihapus','Information');
								ValidDetailRevAnggaran();
							}
							else 
							{
								ShowPesanErrorRevAnggaran('Gagal menghapus data detail anggaran tujuan!', 'Error');
							};
						}
					}
					
				)
			}else{
				dsDTLTRListTujuan_RevAnggaran.removeAt(line);
				gridDTLTR_RevAnggaran_Tujuan.getView().refresh();
				ShowPesanInfoRevAnggaranRevAnggaran('Data detail anggaran asal berhasil dihapus','Information');
				 
			}
		} 
		
	});
	
}
function fnGridLookCM_RKATRevAnggaran() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),			
			{
				id: 'ColAccount_RKATRevAnggaran',
				name: 'ColAccount_RKATRevAnggaran',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '30%',				
				width: 100
			},		 
			{
				id: 'ColNMAccount_RKATRevAnggaran',
				name: 'ColNMAccount_RKATRevAnggaran',
				header: "Nama Account",
				dataIndex: 'name',
				sortable: false,
				anchor: '30%',				
				width: 200
			},		 			
			{
				id: 'Coldeskripsi_RKATRevAnggaran',
				name: 'Coldeskripsi_RKATRevAnggaran',
				header: "Deskripsi",
				dataIndex: 'deskripsi_rkatrdet',
				sortable: false,
				anchor: '30%',				
				width: 200
			},
			{
				id: 'Colprogram_RKATRevAnggaran',
				name: 'Colprogram_RKATRevAnggaran',
				header: "Program",
				dataIndex: 'nama_program',
				sortable: false,
				anchor: '30%',				
				width: 200
			},			
			{
				id: 'ColJmlAngg_RKATRevAnggaran',	
				name: 'ColJmlAngg_RKATRevAnggaran',
				header: "Jml Anggaran",
				width: 90,
				dataIndex: 'jml_asal',
				align: 'right',
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.jml_asal);
				}
			}	
		]
	)
};
///---------------------------------------------------------------------------------------///