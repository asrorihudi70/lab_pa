var dataSource_viMasterObat;
var selectCount_viMasterObat=50;
var NamaForm_viMasterObat="Master Obat dan Alat Kesehatan";
var selectCountStatusPostingMasterObat='Semua';
var mod_name_viMasterObat="viMasterObat";
var now_viMasterObat= new Date();
var addNew_viMasterObat;
var rowSelected_viMasterObat;
var setLookUps_viMasterObat;
var tanggal = now_viMasterObat.format("d/M/Y");
var jam = now_viMasterObat.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var cboJenisTerapi_MasterObat;
var ds_JenisTerapi_MasterObat;


var CurrentData_viMasterObat =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viMasterObat(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


var MasterObat={};
MasterObat.form={};
MasterObat.func={};
MasterObat.vars={};
MasterObat.func.parent=MasterObat;
MasterObat.form.ArrayStore={};
MasterObat.form.ComboBox={};
MasterObat.form.DataStore={};
MasterObat.form.Record={};
MasterObat.form.Form={};
MasterObat.form.Grid={};
MasterObat.form.Panel={};
MasterObat.form.TextField={};
MasterObat.form.Button={};

function dataGrid_viMasterObat(mod_id_viMasterObat){	
    // Field kiriman dari Project Net.
    var FieldMaster_viMasterObat = 
	[
		'KD_PRD','KD_SATUAN','SATUAN','NAMA_OBAT','KET_OBAT','KD_SAT_BESAR','KETERANGAN','KD_JNS_OBT',
		'NAMA_JENIS','GENERIC','KD_SUB_JNS','SUB_JENIS','APT_KD_GOLONGAN','APT_GOLONGAN',
		'FRACTIONS','MG','DPHO','KD_PABRIK','PABRIK','AKTIF','KD_KATAGORI','FORMULARIUM','KD_JNS_TERAPI',
		'JENIS_TERAPI','GENERIK'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viMasterObat = new WebApp.DataStore
	({
        fields: FieldMaster_viMasterObat
    });
    refreshMasterObat();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viMasterObat = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viMasterObat,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viMasterObat = undefined;
							rowSelected_viMasterObat = dataSource_viMasterObat.getAt(row);
							CurrentData_viMasterObat
							CurrentData_viMasterObat.row = row;
							CurrentData_viMasterObat.data = rowSelected_viMasterObat.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viMasterObat = dataSource_viMasterObat.getAt(ridx);
					if (rowSelected_viMasterObat != undefined)
					{
						setLookUp_viMasterObat(rowSelected_viMasterObat.data);
					}
					else
					{
						setLookUp_viMasterObat();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKdPrd_viMasterObat',
						header: 'Kode Obat',
						dataIndex: 'KD_PRD',
						sortable: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						id: 'colNama_viMasterObat',
						header:'Nama Obat',
						dataIndex: 'NAMA_OBAT',						
						width: 50,
						sortable: true,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						id: 'colSatBesar_viMasterObat',
						header: 'Satuan Besar',
						dataIndex: 'KETERANGAN',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 25
					},
					//-------------- ## --------------
					{
						id: 'colStuanKecil_viMasterObat',
						header: 'Satuan Kecil',
						dataIndex: 'SATUAN',
						sortable: true,
						width: 25
					},
					//-------------- ## --------------
					{
						id: 'colGolongan_viMasterObat',
						header: 'Golongan',
						dataIndex: 'APT_GOLONGAN',
						sortable: true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colFraction_viMasterObat',
						header: 'Fraction',
						dataIndex: 'FRACTIONS',
						sortable: true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colSubJenis_viMasterObat',
						header: 'Jenis',
						dataIndex: 'SUB_JENIS',
						sortable: true,
						width: 30
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viMasterObat',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viMasterObat',
						handler: function(sm, row, rec)
						{
							setLookUp_viMasterObat();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viMasterObat',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viMasterObat != undefined)
							{
								setLookUp_viMasterObat(rowSelected_viMasterObat.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viMasterObat, selectCount_viMasterObat, dataSource_viMasterObat),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianMasterObat = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 30,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'Nama / Kode Obat'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNamaMasterObat',
							name: 'TxtFilterGridNamaMasterObat',
							emptyText: 'Nama/Kode obat',
							width: 400,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariMasterObat();
										refreshMasterObat(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 508,
							y: 0,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viMasterObat',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariMasterObat();
								refreshMasterObat(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viMasterObat = new Ext.Panel
    (
		{
			title: NamaForm_viMasterObat,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viMasterObat,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianMasterObat,
					GridDataView_viMasterObat],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viMasterObat,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viMasterObat;
    //-------------- # End form filter # --------------
}

function refreshMasterObat(kriteria)
{
    dataSource_viMasterObat.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewMasterObat',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viMasterObat;
}

function setLookUp_viMasterObat(rowdata){
    var lebar = 985;
    setLookUps_viMasterObat = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viMasterObat, 
        closeAction: 'destroy',        
        width: 650,
		constrain:true,
       // height: 240,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'capsule',
        modal: true,		
        items: getFormItemEntry_viMasterObat(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viMasterObat=undefined;
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
	
	loadDataComboJenisTerapi_MasterObat();
    setLookUps_viMasterObat.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viMasterObat(rowdata);
    }
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpan_viMasterObat').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnHapusObat_viMasterObat').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viMasterObat.close();
				}
			}
		]
	});
}

function getFormItemEntry_viMasterObat(lebar,rowdata){
    var pnlFormDataBasic_viMasterObat = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[getItemPanelInputBiodata_viMasterObat(lebar)],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viMasterObat',
						handler: function(){
							dataaddnew_viMasterObat();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viMasterObat',
						handler: function()
						{
							loadMask.show();
							datasave_viMasterObat();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						hidden:true,
						id: 'btnSimpanExit_viMasterObat',
						handler: function()
						{
							loadMask.show();
							datasave_viMasterObat();
							refreshMasterObat();
							setLookUps_viMasterObat.close();
						}
					},
					{
						xtype: 'tbseparator',hidden:true,
					},
					{
						xtype: 'button',
						text: 'Remove',
						iconCls: 'remove',
						id: 'btnHapusObat_viMasterObat',
						handler: function()
						{
							loadMask.show();
							hapusobat_viMasterObat();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viMasterObat;
}

function getItemPanelInputBiodata_viMasterObat(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		//height:150,
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:100,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Kode Obat ',
						name: 'txtKdPrd_MasterObatL',
						id: 'txtKdPrd_MasterObatL',
						readOnly:true,
						tabIndex:0,
						width: 100
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Nama Obat ',
						name: 'txtNamaObat_MasterObatL',
						id	: 'txtNamaObat_MasterObatL',
						tabIndex:1,
						width: 180
					},
					ComboJenisObatMasterObat(),
					ComboSubJenisObatMasterObat(),
					cbo_JenisTerapi_MasterObat(),
					{
						xtype: 'textfield',
						fieldLabel: 'Generic ',
						name: 'txtGeneric_MasterObatL',
						id	: 'txtGeneric_MasterObatL',
						tabIndex:5,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Jenis ',
						name: 'txttmpJenisObat_MasterObatL',
						id	: 'txttmpJenisObat_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Sub Jenis ',
						name: 'txttmpSubJenisObat_MasterObatL',
						id	: 'txttmpSubJenisObat_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Jenis Terapi',
						name: 'txttmpJenisTerapi_MasterObatL',
						id	: 'txttmpJenisTerapi_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
				]
			},
			
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:100,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					ComboGolonganMasterObat(),
					ComboSatBesarMasterObatLookup(),
					ComboSatuanKecilMasterObat(),
					{
						xtype: 'numberfield',
						fieldLabel: 'Fraction',
						name: 'txtFraction_MasterObatL',
						id: 'txtFraction_MasterObatL',
						tabIndex:9,
						width: 180
					},
					ComboPabrikMasterObat(),
					//ComboKategoriMasterObat(),
					{
						layout: 'column',
						bodyStyle:'padding-top: 5px',
						border: false,
						items:
						[
							
							{
								x:0,
								y:0,
								xtype: 'checkbox',
								boxLabel: 'Aktif',
								id: 'cbAktifMaterObat',
								name: 'cbAktifMaterObat',
								width: 70,
								checked: true,
								hidden: true,
								handler:function(a,b) 
								{
									if(a.checked==true){
										
									}else{
										
									}
								}
							},
							{
								x:50,
								y:0,
								xtype: 'checkbox',
								boxLabel: 'Generic',
								id: 'cbGenericMaterObat',
								name: 'cbGenericMaterObat',
								width: 70,
								checked: false,
								handler:function(a,b) 
								{
									if(a.checked==true){
										
									}else{
										
									}
								}
							},
							{
								x:50,
								y:0,
								xtype: 'checkbox',
								boxLabel: 'Formularium',
								id: 'cbformulariumMaterObat',
								name: 'cbformulariumMaterObat',
								width: 85,
								checked: false,
								handler:function(a,b) 
								{
									if(a.checked==true){
										
									}else{
										
									}
								}
							},
							{
								x:50,
								y:0,
								xtype: 'checkbox',
								boxLabel: 'DPHO',
								id: 'cbDPHOMaterObat',
								name: 'cbDPHOMaterObat',
								width: 60,
								checked: false,
								handler:function(a,b) 
								{
									if(a.checked==true){
										
									}else{
										
									}
								}
							}
						]
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Golongan ',
						name: 'txttmpGolongan_MasterObatL',
						id	: 'txttmpGolongan_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Satuan Besar ',
						name: 'txttmpSatbesar_MasterObatL',
						id	: 'txttmpSatbesar_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Satuan kecil ',
						name: 'txttmpSatkecil_MasterObatL',
						id	: 'txttmpSatkecil_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Kode Pabrik ',
						name: 'txttmpKdPabrik_MasterObatL',
						id	: 'txttmpKdPabrik_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Katagori ',
						name: 'txttmpKdkatagori_MasterObatL',
						id	: 'txttmpKdkatagori_MasterObatL',
						tabIndex:1,
						hidden:true,
						width: 180
					}
				]
			}
		
		]
	};
    return items;
};

function ComboJenisObatMasterObat()
{
	var Field_Jenis = ['KD_JNS_OBT', 'NAMA_JENIS'];
	ds_Jenis = new WebApp.DataStore({fields: Field_Jenis});
    ds_Jenis.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViComboJenisObat',
					param: ''
				}
		}
	);
    var comboJenisObatMasterObat = new Ext.form.ComboBox
	(
		{
			id:'cboJenisObatMasterObat',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			emptyText:'Jenis Obat',
			width: 180,
			tabIndex:2,
			store: ds_Jenis,
			fieldLabel: 'Jenis ',
			valueField: 'KD_JNS_OBT',
			displayField: 'NAMA_JENIS',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{	
					Ext.getCmp('txttmpJenisObat_MasterObatL').setValue(b.data.KD_JNS_OBT)
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return comboJenisObatMasterObat;
};

function ComboSubJenisObatMasterObat()
{
    var Field_Dokter = ['KD_SUB_JNS', 'SUB_JENIS'];
    ds_subjenis = new WebApp.DataStore({fields: Field_Dokter});
    ds_subjenis.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_SUB_JNS',
					Sortdir: 'ASC',
					target: 'ViComboSubJenisObat',
					param: ''
				}
		}
	);
	
    var cbo_SubJenisObatMasterObat = new Ext.form.ComboBox
    (
        {
            flex: 1,
			id: 'cbo_SubJenisObatMasterObat',
			valueField: 'KD_SUB_JNS',
            displayField: 'SUB_JENIS',
			emptyText:'Sub Jenis',
			store: ds_subjenis,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:180,
			fieldLabel:'Sub Jenis',
			tabIndex:3,
			listeners:
			{ 
				
				'select': function(a,b,c)
				{
					//selectSetDokter= ;
					Ext.getCmp('txttmpSubJenisObat_MasterObatL').setValue(b.data.KD_SUB_JNS);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_SubJenisObatMasterObat;
};

function ComboGolonganMasterObat(){
    var Field_Vendor = ['APT_KD_GOLONGAN', 'APT_GOLONGAN'];
    
	ds_golongan = new WebApp.DataStore({fields: Field_Vendor});
    ds_golongan.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'APT_KD_GOLONGAN',
	        Sortdir: 'ASC',
	        target: 'ViComboGolongan',
	        param: ""
        }
    });
    var cbo_GolonganMasterObat = new Ext.form.ComboBox({
			
            flex: 1,
			id: 'cbo_GolonganMasterObat',
            fieldLabel: 'Golongan',
			valueField: 'APT_KD_GOLONGAN',
            displayField: 'APT_GOLONGAN',
			emptyText:'Golongan',
			store: ds_golongan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:180,
			tabIndex:6,
			listeners:{ 
				'select': function(a,b,c){
					Ext.getCmp('txttmpGolongan_MasterObatL').setValue(b.data.APT_KD_GOLONGAN);
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						
					} 						
				}
			}
        }
    )    
    return cbo_GolonganMasterObat;
}


function ComboSatBesarMasterObatLookup()
{
    var Field_Vendor = ['KD_SAT_BESAR', 'KETERANGAN'];
    
	ds_satbesar = new WebApp.DataStore({fields: Field_Vendor});
    ds_satbesar.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_SAT_BESAR',
					Sortdir: 'ASC',
					target: 'ViComboSatBesar',
					param: ""
				}
		}
	);
	
    var cbo_SatBesarMaterObat = new Ext.form.ComboBox
    (
        {
			id: 'cbo_SatBesarMaterObat',
            fieldLabel: 'Satuan besar',
			valueField: 'KD_SAT_BESAR',
            displayField: 'KETERANGAN',
			emptyText:'Satuan besar',
			store: ds_satbesar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:180,
			tabIndex:7,
			listeners:
			{ 
			
				'select': function(a,b,c)
				{
					Ext.getCmp('txttmpSatbesar_MasterObatL').setValue(b.data.KD_SAT_BESAR);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						/* tmpkriteria = getCriteriaCariApotekResepRWJ();
						refeshRespApotekRWJ(tmpkriteria); */
					} 						
				}
			}
        }
    )    
    return cbo_SatBesarMaterObat;
}

function ComboSatuanKecilMasterObat() 
{
	var Field = ['KD_SATUAN','SATUAN'];

    ds_satkecil = new WebApp.DataStore({ fields: Field });
    ds_satkecil.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, 
				Sort: 'kd_satuan',
			    Sortdir: 'ASC',
			    target: 'ViComboSatKecil',
				param: ""
			  
			}
		}
	);
	
    var cboSatuanKecilMaterObat = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboSatuanKecilMaterObat',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Satuan Kecil',
			fieldLabel: 'Satuan Kecil',
		    align: 'Right',
		    width:180,
			tabIndex:8,
		    store: ds_satkecil,
		    valueField: 'KD_SATUAN',
		    displayField: 'SATUAN',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					Ext.getCmp('txttmpSatkecil_MasterObatL').setValue(b.data.KD_SATUAN);				
				}
				  

			}
		}
	);
	
    return cboSatuanKecilMaterObat;
};

function ComboPabrikMasterObat() 
{
	var Field = ['KD_PABRIK','PABRIK'];

    ds_pabrik = new WebApp.DataStore({ fields: Field });
    ds_pabrik.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, 
				Sort: 'KD_PABRIK',
			    Sortdir: 'ASC',
			    target: 'ViComboPabrik',
				param: ""
			  
			}
		}
	);
	
    var cboPabrikMaterObat = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboPabrikMaterObat',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pabrik',
			fieldLabel: 'Pabrik',
		    align: 'Right',
		    width:180,
			tabIndex:10,
		    store: ds_pabrik,
		    valueField: 'KD_PABRIK',
		    displayField: 'PABRIK',
		    listeners:
			{
				
			    'select': function(a, b, c) 
				{
					Ext.getCmp('txttmpKdPabrik_MasterObatL').setValue(b.data.KD_PABRIK);
				}
				  

			}
		}
	);
	
    return cboPabrikMaterObat;
};

function ComboKategoriMasterObat() 
{
	var cboKategoriMaterObat = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboKategoriMaterObat',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Kategori',
			fieldLabel: 'Kategori',
		    align: 'Right',
		    width:150,
			tabIndex:9,
			Hidden:true,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
								'Id',
								'displayText'
							],
					data: [['AV', 'AV'],['BE', 'BE'], ['CN', 'CN']]
					}
			),
			valueField: 'Id',
			displayField: 'displayText',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					Ext.getCmp('txttmpKdkatagori_MasterObatL').setValue(b.data.Id); 
				}
				  

			}
		}
	);
	
    return cboKategoriMaterObat;
};

function loadDataComboJenisTerapi_MasterObat(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionMasterObat/getJenisTerapi",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			ds_JenisTerapi_MasterObat.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_JenisTerapi_MasterObat.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_JenisTerapi_MasterObat.add(recs);
				console.log(o);
			}
		}
	});
}


function cbo_JenisTerapi_MasterObat()
{
	var Field = ['KD_JNS_TERAPI','JENIS_TERAPI'];
    ds_JenisTerapi_MasterObat = new WebApp.DataStore({fields: Field});
    cboJenisTerapi_MasterObat = new Ext.form.ComboBox
	(
            {
                id:'cboJenisTerapi_MasterObat',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Jenis terapi',
                fieldLabel: 'Jenis terapi ',
                width:180,
                store: ds_JenisTerapi_MasterObat,
                valueField: 'KD_JNS_TERAPI',
                displayField: 'JENIS_TERAPI',
                value:'',
				tabIndex:4,
                listeners:
                {
					'select': function(a,b,c)
					{
						Ext.getCmp('txttmpJenisTerapi_MasterObatL').setValue(b.data.KD_JNS_TERAPI);
					}
                }
            }
	);
	return cboJenisTerapi_MasterObat;
};

function dataaddnew_viMasterObat(){
	Ext.getCmp('txtKdPrd_MasterObatL').setValue('');
	Ext.getCmp('txtNamaObat_MasterObatL').setValue('');
	// Ext.getCmp('cboJenisObatMasterObat').setValue('');
	// Ext.getCmp('cbo_SubJenisObatMasterObat').setValue('');
	// Ext.getCmp('cbo_GolonganMasterObat').setValue('');
	// Ext.getCmp('cbo_SatBesarMaterObat').setValue('');
	// Ext.getCmp('cboSatuanKecilMaterObat').setValue('');
	// Ext.getCmp('txtFraction_MasterObatL').setValue('');
	// Ext.getCmp('cboPabrikMaterObat').setValue('');
	// //Ext.getCmp('cboKategoriMaterObat').setValue('');
	// Ext.getCmp('cbAktifMaterObat').setValue(true);
	// Ext.getCmp('cbGenericMaterObat').setValue(false);
	// Ext.getCmp('cbDPHOMaterObat').setValue(false);
	
	
	// Ext.getCmp('txttmpJenisObat_MasterObatL').setValue('');
	// Ext.getCmp('txttmpSubJenisObat_MasterObatL').setValue('');
	// Ext.getCmp('txttmpJenisTerapi_MasterObatL').setValue('');
	// Ext.getCmp('txttmpGolongan_MasterObatL').setValue('');
	// Ext.getCmp('txttmpSatbesar_MasterObatL').setValue('');
	// Ext.getCmp('txttmpSatkecil_MasterObatL').setValue('');
	// Ext.getCmp('txttmpKdPabrik_MasterObatL').setValue('');
	// Ext.getCmp('txttmpKdkatagori_MasterObatL').setValue('');
}

function datainit_viMasterObat(rowdata)
{
	loadDataComboJenisTerapi_MasterObat();
	//alert(rowdata.AKTIF);
	if(rowdata.AKTIF == 't'){
		Ext.getCmp('cbAktifMaterObat').setValue(true);
	} else{
		Ext.getCmp('cbAktifMaterObat').setValue(false);
	}
	if(rowdata.GENERIC ==1){
		Ext.getCmp('cbGenericMaterObat').setValue(true);
	}else{
		Ext.getCmp('cbGenericMaterObat').setValue(false);
	}
	if(rowdata.DPHO == 1){
		Ext.getCmp('cbDPHOMaterObat').setValue(true);
	}else{
		Ext.getCmp('cbDPHOMaterObat').setValue(false);
	}
	if(rowdata.FORMULARIUM == 1){
		Ext.getCmp('cbformulariumMaterObat').setValue(true);
	}else{
		Ext.getCmp('cbformulariumMaterObat').setValue(false);
	}

	Ext.getCmp('txtKdPrd_MasterObatL').setValue(rowdata.KD_PRD);
	Ext.getCmp('txtNamaObat_MasterObatL').setValue(rowdata.NAMA_OBAT);
	Ext.getCmp('cboJenisObatMasterObat').setValue(rowdata.NAMA_JENIS);
	Ext.getCmp('cbo_SubJenisObatMasterObat').setValue(rowdata.SUB_JENIS);
	Ext.getCmp('cboJenisTerapi_MasterObat').setValue(rowdata.JENIS_TERAPI);
	Ext.getCmp('cbo_GolonganMasterObat').setValue(rowdata.APT_GOLONGAN);
	Ext.getCmp('cbo_SatBesarMaterObat').setValue(rowdata.KETERANGAN);
	Ext.getCmp('cboSatuanKecilMaterObat').setValue(rowdata.SATUAN);
	Ext.getCmp('txtFraction_MasterObatL').setValue(rowdata.FRACTIONS);
	Ext.getCmp('txtGeneric_MasterObatL').setValue(rowdata.GENERIK);
	Ext.getCmp('cboPabrikMaterObat').setValue(rowdata.PABRIK);
	//Ext.getCmp('cboKategoriMaterObat').setValue(rowdata.KD_KATAGORI);
	
	Ext.getCmp('txttmpJenisObat_MasterObatL').setValue(rowdata.KD_JNS_OBT);
	Ext.getCmp('txttmpJenisTerapi_MasterObatL').setValue(rowdata.KD_JNS_TERAPI);
	Ext.getCmp('txttmpSubJenisObat_MasterObatL').setValue(rowdata.KD_SUB_JNS);
	Ext.getCmp('txttmpJenisTerapi_MasterObatL').setValue(rowdata.KD_JNS_TERAPI);
	Ext.getCmp('txttmpGolongan_MasterObatL').setValue(rowdata.APT_KD_GOLONGAN);
	Ext.getCmp('txttmpSatbesar_MasterObatL').setValue(rowdata.KD_SAT_BESAR);
	Ext.getCmp('txttmpSatkecil_MasterObatL').setValue(rowdata.KD_SATUAN);
	Ext.getCmp('txttmpKdPabrik_MasterObatL').setValue(rowdata.KD_PABRIK);
	Ext.getCmp('txttmpKdkatagori_MasterObatL').setValue(rowdata.KD_KATAGORI);
	
};

function datasave_viMasterObat(){
	if (ValidasiEntryMasterObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionMasterObat/save",
				params: getParamMasterObat(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorMasterObat('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoMasterObat(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdPrd_MasterObatL').dom.value=cst.kdprd;
						refreshMasterObat();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorMasterObat('Gagal Menyimpan Data ini', 'Error');
						refreshMasterObat();
					};
				}
			}
			
		)
	}
}



function getParamMasterObat() 
{
	var	params =
	{
		KdPrd:Ext.getCmp('txtKdPrd_MasterObatL').getValue(),
		
		//edit obat
		NamaObat:Ext.getCmp('txtNamaObat_MasterObatL').getValue(),
		KdJenisObat:Ext.getCmp('txttmpJenisObat_MasterObatL').getValue(),
		KdSubJenis:Ext.getCmp('txttmpSubJenisObat_MasterObatL').getValue(),
		KdJnsTerapi:Ext.getCmp('txttmpJenisTerapi_MasterObatL').getValue(),
		KdGolongan:Ext.getCmp('txttmpGolongan_MasterObatL').getValue(),
		KdSatuanBesar:Ext.getCmp('txttmpSatbesar_MasterObatL').getValue(),
		KdSatuanKecil:Ext.getCmp('txttmpSatkecil_MasterObatL').getValue(),
		Generik:Ext.getCmp('txtGeneric_MasterObatL').getValue(),
		Fraction:Ext.getCmp('txtFraction_MasterObatL').getValue(),
		KdPabrik:Ext.getCmp('txttmpKdPabrik_MasterObatL').getValue(),
		Kategori:Ext.getCmp('txttmpKdkatagori_MasterObatL').getValue(),
		Aktif:Ext.getCmp('cbAktifMaterObat').getValue(),
		Generic:Ext.getCmp('cbGenericMaterObat').getValue(),
		DPHO:Ext.getCmp('cbDPHOMaterObat').getValue(),
		Formularium:Ext.getCmp('cbformulariumMaterObat').getValue(),
	}
   
    return params
};

function hapusobat_viMasterObat(){
	if (Ext.getCmp('txtKdPrd_MasterObatL').getValue() != "")
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionMasterObat/hapus",
				params: { kd_prd:Ext.getCmp('txtKdPrd_MasterObatL').getValue() },
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorMasterObat('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoMasterObat('Obat berhasil diHapus','Information');
						refreshMasterObat();
						setLookUps_viMasterObat.close();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorMasterObat(cst.pesan, 'Error');
						refreshMasterObat();
					};
				}
			}
			
		)
	} else{
		loadMask.hide();
		ShowPesanErrorMasterObat('Pilih obat yang akan diHapus', 'Error');
	}
}


function getCriteriaCariMasterObat()//^^^
{
	var strKriteria = "";

	if (Ext.get('TxtFilterGridNamaMasterObat').getValue() != "" && Ext.get('TxtFilterGridNamaMasterObat').getValue()!=='Nama/Kode obat')
	{
		strKriteria = " upper(o.nama_obat) " + "LIKE upper('" + Ext.get('TxtFilterGridNamaMasterObat').getValue() +"%') or o.kd_prd" + " LIKE '" + Ext.get('TxtFilterGridNamaMasterObat').getValue() +"%'";
	}
		strKriteria= strKriteria + "order by o.kd_prd limit 20"
	return strKriteria;
}


function ValidasiEntryMasterObat(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtNamaObat_MasterObatL').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Nama obat masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboJenisObatMasterObat').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Jenis obat masih kosong', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('cbo_SubJenisObatMasterObat').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Sub jenis masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cbo_GolonganMasterObat').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Golongan masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cbo_SatBesarMaterObat').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Satuan besar masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cbo_SatBesarMaterObat').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Satuan besar masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboSatuanKecilMaterObat').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Satuan kecil masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtFraction_MasterObatL').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningMasterObat('Fraction masih kosong', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningMasterObat(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorMasterObat(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:300
		}
	);
};


function ShowPesanInfoMasterObat(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};