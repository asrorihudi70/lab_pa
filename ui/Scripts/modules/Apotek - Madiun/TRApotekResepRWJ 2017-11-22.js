// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekResepRWJ.js
*	Menu 			: APOTEK
*	Model id 		: 
*	Keterangan 		: Resep RWJ dan IGD
*	Di buat tanggal : 04 Juni 2015
*	Oleh 			: M
*	Edit			: AGUNG
*/

// Deklarasi Variabel pada Apotek Perencanaan # --------------
var cbopasienorder_printer;
var tampung_ceklis_obat;
var jml_tampung_ceklis_obat;
var tampung_waktu_dosis_obat;
var jenis_obat='';
var jenis_etiket='';
var qty_default=1;
var dsGridListKunjungan_ResepRWJ;
function TRApotekResepRWJ(){
var cbopasienorder_mng_apotek;
var dspasienorder_mng_apotek;
var AptResepRWJ={};
var GridWaktuDosisObatColumnModel;
AptResepRWJ.form={};
AptResepRWJ.func={};
AptResepRWJ.vars={};
AptResepRWJ.func.parent=AptResepRWJ;
AptResepRWJ.form.DataStore={};
AptResepRWJ.form.ComboBox={};
AptResepRWJ.form.ArrayStore={};
AptResepRWJ.form.Record={};
AptResepRWJ.form.Form={};
AptResepRWJ.form.Grid={};
AptResepRWJ.form.Panel={};
AptResepRWJ.form.TextField={};
AptResepRWJ.form.Button={};
var statusRacikan_ResepRWJ=0;
var dsDataGrdJab_viApotekResepRWJ= new Ext.data.ArrayStore({
		id: 0,
        fields: ['kd_prd','kd_satuan','nama_obat','jml','disc','kd_satuan','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik','dosis','jasa','no_out','no_urut'],
		data: []
   });
	
	
var win_printer_resep_rwi;
var dsprinter;
AptResepRWJ.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_jual','harga_beli','kd_pabrik',
			'markup','tuslah','adm_racik','hargaaslicito','nilai_cito','milik'],
	data: []
});

AptResepRWJ.form.ArrayStore.namapasien	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 
			'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir','urut_masuk','customer','nama_dokter','telepon','no_sjp',
			'kd_pay','payment','jenis_pay','payment_type'],
	data: []
});
AptResepRWJ.form.ArrayStore.kodepasien	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 
			'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir','urut_masuk','customer','nama_dokter','telepon','no_sjp',
			'kd_pay','payment','jenis_pay','payment_type'],
	data: []
});


// var dsGridJamDosisObat_ResepRWJ;
var KdForm=7; // kd_form RESEP RWJ & IGD
var dataSource_viApotekResepRWJ;
var selectCount_viApotekResepRWJ=50;
var NamaForm_viApotekResepRWJ="Resep Rawat Jalan / Gawat Darurat";
var selectCountStatusPostingApotekResepRWJ='Semua';
var mod_name_viApotekResepRWJ="viApotekResepRWJ";
var now_viApotekResepRWJ= new Date();
var addNew_viApotekResepRWJ;
var rowSelected_viApotekResepRWJ;
var rowSelectedOrderManajemen_viApotekResepRWJ;
var setLookUp_bayarResepRWJ;
var setLookUps_viApotekResepRWJ;
var mNoKunjungan_viApotekResepRWJ='';
var selectSetUnit;
var selectSetUnitLookup;
var selectSetPilihankelompokPasien;
var selectSetDokter;
var tanggal = now_viApotekResepRWJ.format("d/M/Y");
var tanggal_resep_sebenarnya = now_viApotekResepRWJ.format("Y-m-d");
var tanggallabel = now_viApotekResepRWJ.format("d/M/Y");
var tanggalcekbulan = now_viApotekResepRWJ.format("Y-m-d");
var jam = now_viApotekResepRWJ.format("H/i/s");
var cellSelecteddeskripsiRWJ;
var tampungshiftsekarang;
var tmpNoOut=0;
var tmpTglOut='';
var tmpkriteria;
var kd_pasien_obbt;
var kd_unit_obbt;
var tgl_masuk_obbt;
var urut_masuk_obbt;
var gridDTLTRHistoryApotekRWJ;
var kd='';
var setLookUpApotek_TransferResepRWJ;
var GridDataViewOrderManagement_viApotekResepRWJ;
var dataSourceGridOrder_viApotekResepRWJ;
var CurrentGridOrder;
var CurrentIdMrResep='';
var detailorder=true;
var ordermanajemen=false;
var cbo_Unit;
var UnitFarAktif_ResepRWJ;
var ckboxRacikan;
var PrintBill;

var CellSelected_viApotekResepRWJ;
var currentKdPrdRacik_ResepRWJ;
var currentNamaObatRacik_ResepRWJ;
var currentHargaRacik_ResepRWJ;
var currentJumlah_ResepRWJ;
var curentIndexsSelection_ResepRWJ;
var cboKodePasienResepRWJ;
var gridPanelFormulasiCito_ResepRWJ;
var currentHargaJualObat;
var currentCitoNamaObat;
var currentCitoKdPrd;
var currentCitoTarifLama;
var currentCitoTarifBaru=0;;
var cellSelectedGridCito_ResepRWJ;
var CurrentDataGridCito_ResepRWJ;
var ds_jenis_etiket;
var ds_jam_etiket;
var ds_waktu_etiket;
var kd_jenis_etiket;
var dsGridTakaranDosisObat_ResepRWJ;
var ds_cara_minum_etiket;
var ds_KeteranganObatLuar_etiket;
var currentRowSelectionPencarianObatResepRWJ;
var currentRowSelectionListKunjunganResepRWJ;
var GridListPencarianObatColumnModel;
var PencarianLookupResep; // Variabel pembeda getdata u/ pencarian nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
var FocusExitResepRWJ=false;
var dsGridJamDosisObat_ResepRWJ;

var CurrentHistoryRWJ =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekResepRWJ =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataOrderManajemen_viApotekResepRWJ=
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekResepRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Apotek Perencanaan # --------------

// Start Project Apotek Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/


function dataGrid_viApotekResepRWJ(mod_id_viApotekResepRWJ)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekResepRWJ = 
	[
		'STATUS_POSTING','NO_RESEP','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI','TGL_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','JASA', 'ADMPRHS','ADMRESEP','CUSTOMER','JENIS_PASIEN','TGL_MASUK',
		'URUT_MASUK','TELEPON','CATATANDR','NO_SJP','KD_PAY','JENIS_PAY','PAYMENT','PAYMENT_TYPE'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSource_viApotekResepRWJ = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekResepRWJ
    });
    refreshRespApotekRWJ();
	total_pasien_order_mng_obtrwj();
	total_pasien_dilayani_order_mng_obtrwj();
	viewGridOrderAll_RASEPRWJ();
	loadDataKodePasienResepRWJ();
	getUnitFar_ResepRWJ();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekResepRWJ = new Ext.grid.EditorGridPanel
    (
		{
			/* xtype: 'editorgrid',
			title: '', */
			store: dataSource_viApotekResepRWJ,
			title: 'Daftar Resep Telah Dibuat',
			autoScroll: true,
			columnLines: true,
			border: true, //false,
			anchor: '100% 51.1%',
			//width: '500',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekResepRWJ = undefined;
							rowSelected_viApotekResepRWJ = dataSource_viApotekResepRWJ.getAt(row);
							CurrentData_viApotekResepRWJ
							CurrentData_viApotekResepRWJ.row = row;
							CurrentData_viApotekResepRWJ.data = rowSelected_viApotekResepRWJ.data;
							if (rowSelected_viApotekResepRWJ.data.STATUS_POSTING==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekResepRWJ').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekResepRWJ').disable();
							}
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					Ext.Ajax.request(
						{
							   
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							 params: {
								
								command: '0'
							
							},
							failure: function(o)
							{
								 var cst = Ext.decode(o.responseText);
								
							},	    
							success: function(o) {
							var cst = Ext.decode(o.responseText);
				
							tampungshiftsekarang=cst.shift
							}
					
					});
					rowSelected_viApotekResepRWJ = dataSource_viApotekResepRWJ.getAt(ridx);
					if (rowSelected_viApotekResepRWJ != undefined)
					{
						
						setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
					}
					else
					{
						setLookUp_viApotekResepRWJ();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						align		: 'center',
						dataIndex	: 'STATUS_POSTING',
						id			: 'colStatusPosting_viApotekResepRWJ',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoout_viApotekResepRWJ',
						header: 'No. Tr',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 20,
						// hidden:true
					},
					{
						id: 'colTgl_viApotekResepRWJ',
						header:'Tgl Resep',
						dataIndex: 'TGL_OUT',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						// format: 'd/M/Y',
						//filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OUT);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekResepRWJ',
						header: 'No. Resep',
						dataIndex: 'NO_RESEP',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					
					{
						id: 'colNoMedrec_viApotekResepRWJ',
						header: 'No Medrec',
						dataIndex: 'KD_PASIENAPT',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colNamaPasien_viApotekResepRWJ',
						header: 'Nama',
						dataIndex: 'NMPASIEN',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colPoliklinik_viApotekResepRWJ',
						header: 'Poliklinik',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						id: 'colNoout_viApotekResepRWJ',
						header: 'No Out',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekResepRWJ',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Resep [F1]',
						iconCls: 'Edit_Tr',
						tooltip: 'Tambah Data',
						id: 'btnTambah_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}
							});
							cekPeriodeBulan();
							Ext.getCmp('txtTmpStatusPost').setValue(0);
							// AptResepRWJ.form.ComboBox.kodePasien.focus(true,10);// .focus();
							 Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').focus(true,10);
						}
					},
					{
						xtype: 'button',
						text: 'Edit Resep',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request({
									   
									url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
									 params: {
										
										command: '0'
									
									},
									failure: function(o)
									{
										 var cst = Ext.decode(o.responseText);
										
									},	    
									success: function(o) {
										var cst = Ext.decode(o.responseText);
							
										tampungshiftsekarang=cst.shift
									}
							
							})
							
							if (rowSelected_viApotekResepRWJ != undefined)
							{
								setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
							}
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							
							var datanya=rowSelected_viApotekResepRWJ.data;
							if (datanya===undefined){
								ShowPesanWarningResepRWJ('Belum ada data yang dipilih','Resep RWJ');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_rwj = combo;
															if (variablebatalhistori_rwj != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxResep",
																		 params: {
																			
																			noout: datanya.NO_OUT,
																			tglout: datanya.TGL_OUT,
																			kdcustomer:datanya.KD_CUSTOMER,
																			namacustomer:datanya.CUSTOMER,
																			kdunit:datanya.KD_UNIT,
																			jenis:'RESEP',
																			apaini:"reseprwj",
																			alasan: variablebatalhistori_rwj
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanErrorResepRWJ('Data transaksi tidak dapat dihapus','Resep RWJ');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				tmpkriteria = getCriteriaCariApotekResepRWJ();
																				refreshRespApotekRWJ(tmpkriteria);
																				ShowPesanInfoResepRWJ('Data transaksi Berhasil dihapus','Resep RWJ');
																				Ext.getCmp('btnHapusTrx_viApotekResepRWJ').disable();
																			}
																			else
																			{
																				ShowPesanErrorResepRWJ('Hapus gagal dilakukan. '+ cst.pesan,'Error');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningResepRWJ('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													});
											/*  */
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							}
							 
							/* 
							
							if (rowSelected_viApotekResepRWJ != undefined)
							{
								setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
							} */
						}
					},
					
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viApotekResepRWJ, selectCount_viApotekResepRWJ, dataSource_viApotekResepRWJ),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var FieldOrderManajemen_viApotekResepRWJ = 
	[
		'kd_pasien', 'nama', 'kd_unit', 'nama_unit','kd_dokter','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSourceGridOrder_viApotekResepRWJ = new WebApp.DataStore
	({
        fields: FieldOrderManajemen_viApotekResepRWJ
    });
	
	GridDataViewOrderManagement_viApotekResepRWJ = new Ext.grid.EditorGridPanel
    (
		{
			title:'Order Obat Poli',
			store: dataSourceGridOrder_viApotekResepRWJ,
			autoScroll: true,
			columnLines: true,
			border: true, //false,
			anchor:'100% 30%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedOrderManajemen_viApotekResepRWJ = undefined;
							rowSelectedOrderManajemen_viApotekResepRWJ = dataSourceGridOrder_viApotekResepRWJ.getAt(row);
							CurrentDataOrderManajemen_viApotekResepRWJ
							CurrentDataOrderManajemen_viApotekResepRWJ.row = row;
							CurrentDataOrderManajemen_viApotekResepRWJ.data = rowSelectedOrderManajemen_viApotekResepRWJ.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedOrderManajemen_viApotekResepRWJ = dataSourceGridOrder_viApotekResepRWJ.getAt(ridx);
					if (rowSelectedOrderManajemen_viApotekResepRWJ != undefined)
					{
						CurrentGridOrder=CurrentDataOrderManajemen_viApotekResepRWJ.data;
						console.log(CurrentGridOrder);
						ordermanajemen=true;
						CurrentIdMrResep=CurrentGridOrder.id_mrresep;
						//console.log(mod_id_printer(0));
						
						Ext.Ajax.request(
						{
							   
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							 params: {
								
								command: '0'
							
							},
							failure: function(o)
							{
								 var cst = Ext.decode(o.responseText);
								
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
					
								tampungshiftsekarang=cst.shift
							}
						});
						
						cekPeriodeBulan(detailorder,CurrentGridOrder);
						
						
						
					}
					else
					{
						//setLookUp_viApotekResepRWJ();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No Medrec',
						dataIndex: 'kd_pasien',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama',
						sortable: false,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Poliklinik',
						dataIndex: 'nama_unit',
						sortable: false,
						width: 40
					},
					//-------------- ## --------------
					{
						header:'Tgl Order',
						dataIndex: 'tgl_order',						
						width: 30,
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_order);
						}
					},
					{
						header: 'Status',
						dataIndex: 'order_mng',
						sortable: false,
						width: 40
					},
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbarOrderManajemen_viApotekResepRWJ',
				items: 
				[
					{
						xtype: 'label',
						text: 'Order obat poli : ',
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtcounttr_apt_rwj',
						id: 'txtcounttr_apt_rwj',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						},
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:10},
					{
						xtype: 'label',
						text: 'Order obat telah dilayani : ',
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtcounttrDilayani_apt_rwj',
						id: 'txtcounttrDilayani_apt_rwj',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						},
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:10},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						disabled:true,
						id: 'btnRefreshOrder_viApotekResepRWJ',
						handler: function() 
						{
							viewGridOrderAll_RASEPRWJ();
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							ordermanajemen=false;
						}
					},
					{xtype: 'tbspacer',height: 3, width:7},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'label',
						text: 'Cari berdasarkan nama dan tanggal : '
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtNamaOrder_viApotekResepRWJ',
						id: 'txtNamaOrder_viApotekResepRWJ',
						width: 120,
						disabled:true,
						listeners: 
						{ 
							'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										viewGridOrderAll_RASEPRWJ(Ext.getCmp('txtNamaOrder_viApotekResepRWJ').getValue(),Ext.getCmp('dfTglOrderApotekResepRWJ').getValue());
									} 						
								}
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						xtype: 'datefield',
						id: 'dfTglOrderApotekResepRWJ',
						format: 'd/M/Y',
						width: 100,
						tabIndex:3,
						disabled:true,
						value:now_viApotekResepRWJ,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									viewGridOrderAll_RASEPRWJ(Ext.getCmp('txtNamaOrder_viApotekResepRWJ').getValue(),Ext.getCmp('dfTglOrderApotekResepRWJ').getValue());
								} 						
							}
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						xtype: 'label',
						text: '*) Enter untuk mencari'
					},
					{xtype: 'tbspacer',height: 3, width:580},
					
					
					//-------------- ## --------------
					
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianApotekResepRWJ = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekResepRWJ',
							name: 'TxtFilterGridDataView_RoNumber_viApotekResepRWJ',
							emptyText: 'No. Resep',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Kode/Nama Pasien'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtKdNamaPasien',
							name: 'txtKdNamaPasien',
							emptyText: 'Kode/Nama Pasien',
							width: 160,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Poliklinik'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						ComboUnitApotekResepRWJ(),	
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Resep'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalApotekResepRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirApotekResepRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekResepRWJ(),
						{
							x: 540,
							y: 30,
							xtype: 'label',
							text: 'Jumlah Data'
						},
						{
							x: 630,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboJumlahDataApotekResepRWJ(),
						{
							x: 310,
							y: 65,
							xtype: 'label',
							text: '*) Tekan enter untuk mencari resep'
						},
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari Resep',
							iconCls: 'refresh',
							tooltip: 'Cari',
							hidden : true,
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_viApotekResepRWJ',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariApotekResepRWJ();
								refreshRespApotekRWJ(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]			
	})
	
	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekResepRWJ = new Ext.Panel
    (
		{
			title: NamaForm_viApotekResepRWJ,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekResepRWJ,
			region: 'center',
			layout: 'form', 
			closable: true,   
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianApotekResepRWJ,
					GridDataView_viApotekResepRWJ,
					GridDataViewOrderManagement_viApotekResepRWJ],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekResepRWJ,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
	
    return FrmFilterGridDataView_viApotekResepRWJ;
	
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viApotekResepRWJ # --------------

function refreshRespApotekRWJ(kriteria)
{
    dataSource_viApotekResepRWJ.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewResepApotekRWJ',
                    param : kriteria 
                }			
            }
        );   
    return dataSource_viApotekResepRWJ;
}

/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/
function mComboJumlahDataApotekResepRWJ(){
	var limit='';
	 var cboJumlahDataApotekResepRWJ = new Ext.form.ComboBox
	(
		{
			id:'cboJumlahDataApotekResepRWJ',
			x: 650,
			y: 30,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 50,
			editable: false,
			emptyText:'',
			fieldLabel: 'JENIS',
			tabIndex:5,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
								'Id',
								'displayText'
							],
					data: [[1, 5],[2,10], [3, 20],[4,25],[5,50],[6,100]]
					}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:1,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkriteria = getCriteriaCariApotekResepRWJ();
					limit=tmpkriteria+" limit " + Ext.get('cboJumlahDataApotekResepRWJ').getValue()+" ";
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						// alert(limit);
						refreshRespApotekRWJ(limit);
					} 						
				}
			}
		}
	);
	return cboJumlahDataApotekResepRWJ;
}
	
function setLookUp_viApotekResepRWJ(rowdata){
    var lebar = 885;
    setLookUps_viApotekResepRWJ = new Ext.Window({
        id: 'SetLookUps_viApotekResepRWJ',
		name: 'SetLookUps_viApotekResepRWJ',
        title: NamaForm_viApotekResepRWJ, 
        closeAction: 'destroy',        
        width: 1010,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'resep',
        modal: true,		
        items: getFormItemEntry_viApotekResepRWJ(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				shortcuts();
				Ext.Ajax.request({
									   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							AptResepRWJ.form.Panel.shift.update(cst.shift);
						}
					
					});
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
                //datarefresh_viApotekResepRWJ();
				mNoKunjungan_viApotekResepRWJ = '';
				//ordermanajemen=false;
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
	
	dsDataGrdJab_viApotekResepRWJ.loadData([],false);
    setLookUps_viApotekResepRWJ.show();

    if (rowdata == undefined){
		Ext.getCmp('btnAddObat').enable();
		getDefaultCustomer();
		// AptResepRWJ.form.ComboBox.kodePasien.focus(true,10);
		 Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').focus(true,10);
		
    }else{
		ViewDetailPembayaranObat(rowdata.NO_OUT,rowdata.TGL_OUT);
        datainit_viApotekResepRWJ(rowdata);
    }
	shortcuts();
	
}

function getDefaultCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getDefaultCustomer",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				Ext.getCmp('txtTmpKdCustomer').setValue(cst.kd_customer);
				Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(cst.customer);
				
			}
		}
		
	)
	
}

function getPaymentPasienLangsung(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getPaymentPasienLangsung",
			params: {
				kd_customer:Ext.getCmp('txtTmpKdCustomer').getValue(),
				
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				Ext.getCmp('txtTmpJenisPay').setValue(cst.jenis_pay)
				AptResepRWJ.vars.payment_type = cst.deskripsi;
				AptResepRWJ.vars.payment = cst.uraian;
				Ext.getCmp('txtTmpKdPay').setValue(cst.kd_pay);
				
			}
		}
	)
}

function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'f8',
				fn:function(){
					Ext.getCmp('btnAdd_viApotekResepRWJ').el.dom.click();
				}
			},
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpan_viApotekResepRWJ').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDelete_viApotekResepRWJ').el.dom.click();
				}
			},
			{
				key:'f9',
				fn:function(){
					Ext.getCmp('btnbayar_viApotekResepRWJ').el.dom.click();
				}
			},
			{
				key:'f10',
				fn:function(){
					Ext.getCmp('btnTransfer_viApotekResepRWJ').el.dom.click();
				}
			},
			{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnunposting_viApotekResepRWJ').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					// Ext.getCmp('btnPrintBillResepRWJ').el.dom.click();
					if (Ext.getCmp('txtTmpStatusPost').getValue() == 1){
						Ext.getCmp('btnPrintBillResepRWJ').handler();	
					}else{
						ShowPesanWarningResepRWJ('Pembayaran belum dilakukan, tidak dapat cetak bill!','Warning');
					}
				}
			},
			{
				key:'f11',
				fn:function(){
					// Ext.getCmp('btnPrintKwitansiResepRWJ').handler();
					Ext.Ajax.request({   
						url: baseURL + "index.php/apotek/functionAPOTEK/getjenispembayaran",
						 params: {
							no_out:Ext.getCmp('txtTmpNoout').getValue(),
							tgl_out:Ext.getCmp('txtTmpTglout').getValue()
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							if(cst.jenis_pay == 'TU'){
								Ext.getCmp('btnPrintKwitansiResepRWJ').handler();
							}else{
								Ext.Msg.show({
									title: 'Warning',
									msg: 'Pembayaran bukan TUNAI, kwitansi tidak dapat dicetak!' ,
									buttons: Ext.MessageBox.OK,
									fn: function (btn) {
										
									}
								});
							}
						}	
					
					});
				}
			},
			{
				key:'ctrl+o',
				fn:function(){
					// Ext.getCmp('btnPrintLabelDosisObatResepRWJ').el.dom.click();
					Ext.getCmp('btnPrintLabelDosisObatResepRWJ').handler();
				}
			},
			{
				key:'enter',
				fn:function(){
					setLookUps_viApotekResepRWJ.close();
				}
			}
		]
	});
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

/**
*	Function : getFormItemEntry_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viApotekResepRWJ(lebar,rowdata)
{
    var pnlFormDataBasic_viApotekResepRWJ = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			listeners: {
				afterShow: function()
				{
					Ext.Ajax.request({
									   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							AptResepRWJ.form.Panel.shift.update(cst.shift);
						}
					
					});
				}
			},
			items:
			[
				getItemPanelInputBiodata_viApotekResepRWJ(lebar),
				//-------------- ## -------------- 	
				getItemGridTransaksi_viApotekResepRWJ(lebar),
				//-------------- ## --------------
				getItemGridHistoryBayar_viApotekResepRWJ(lebar),
				
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viApotekResepRWJ',
					id: 'compChkPosted_viApotekResepRWJ',
					items: 
					[
						/* {
							columnWidth	: .033,
							layout		: 'form',
							style		: {'margin-top':'-1px'},
							anchor		: '100% 8.0001%',
							border		: false,
							fieldLabel  : 'Transfered',
							html		: ''
						}, */
						AptResepRWJ.form.Panel.a=new Ext.Panel ({
						    region: 'north',
						    border: false,
						    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
						}),
						{
							columnWidth	: .08,
							layout		: 'form',
							anchor		: '100% 8.0001%',
							style		: {'margin-top':'1px'},
							border		: false,
							html		: " Status Posting"
						},
						//-------------------##------------------------
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Tuslah :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'310px'}							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtTuslahEditData_viApotekResepRWJ',
		                    name: 'txtTuslahEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'310px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 80,								
							value: 'Adm Racik:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'300px'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmRacikEditData_viApotekResepRWJ',
		                    name: 'txtAdmRacikEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'300px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true
		                },						
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Total:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'330px'}
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridJmlEditData_viApotekResepRWJ',
		                    name: 'txtDRGridJmlEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'330px'},
		                    width: 110,
		                    value: 0,
		                    readOnly: true
		                },						
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkUpdateHB_viApotekResepRWJ',
					id: 'compChkUpdateHB_viApotekResepRWJ',
					items: 
					[
						{
							xtype: 'displayfield',				
							width: 55,								
							value: 'Tanggal :',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 100,								
							value: tanggallabel,
							format:'d/M/Y',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Adm :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'250px'}					
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmEditData_viApotekResepRWJ',
		                    name: 'txtAdmEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'250px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Disc :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'270px'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtDiscEditData_viApotekResepRWJ',
		                    name: 'txtDiscEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'270px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true
		                },	
						{
							xtype: 'displayfield',				
							width: 90,								
							value: 'Grand Total :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'260px','font-weight':'bold'}
							
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtTotalEditData_viApotekResepRWJ',
		                    name: 'txtTotalEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'260px'},
		                    width: 110,
		                    value: 0,
		                    readOnly: true
		                }	
						/* {
							xtype:'button',
							text:'Acc. Approval',
							width:70,
							//style:{'margin-left':'190px','margin-top':'7px'},
							style:{'text-align':'right','margin-left':'380px'},
							hideLabel:true,
							id: 'btnAccApp_viApotekResepRWJ',
							handler:function()
							{
							}   
						}, */
						
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkHB_viApotekResepRWJ',
					id: 'compChkHB_viApotekResepRWJ',
					items: 
					[
						
						{
							xtype: 'displayfield',				
							width: 80,								
							value: 'Current Shift :',
							fieldLabel: 'Label',
							style:{'text-align':'left'}						
						},
						AptResepRWJ.form.Panel.shift=new Ext.Panel ({
							region: 'north',
							border: false
						}),							
						{
							xtype: 'displayfield',				
							width: 95,								
							value: 'Adm Perusahaan:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'385px'}						
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmPerusahaanEditData_viApotekResepRWJ',
		                    name: 'txtAdmPerusahaanEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'485px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true
		                }
					]
				}
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Baru [F8]',
						iconCls: 'add',
						hidden:false,
						id: 'btnAdd_viApotekResepRWJ',
						handler: function(){
							dataaddnew_viApotekResepRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Dilayani [CTRL+S]',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viApotekResepRWJ',
						handler: function()
						{
							datasave_viApotekResepRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Dilayani & Tutup',
						iconCls: 'saveexit',
						disabled:true,
						hidden:true,
						id: 'btnSimpanExit_viApotekResepRWJ',
						handler: function()
						{
							/* var x = datasave_viApotekResepRWJ(addNew_viApotekResepRWJ);
							refreshRespApotekRWJ();
							if (x===undefined)
							{
								setLookUps_viApotekResepRWJ.close();
							} */
							datasave_viApotekResepRWJ();
							refreshRespApotekRWJ();
							setLookUps_viApotekResepRWJ.close();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator',
						hidden:true,
					},
					{
						xtype: 'button',
						text: 'Bayar [F9]',
						id:'btnbayar_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_bayarResepRWJ();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Transfer [F10]',
						disabled:true,
						id:'btnTransfer_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_TransferResepRWJ();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Unposting [F6]',
						id:'btnunposting_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler:function()
						{
							if(Ext.getCmp('cbNonResep').getValue() == true){
								unPostingRESEPRWJ();
							} else{								
								cekTransfer(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
							}
						}  
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus Bayar',
						id:'btnDeleteHistory_viApotekResepRWJ',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayarList.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan dihapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionAPOTEK/deleteHistoryResepRWJ",
												params: getParamDeleteHistoryResepRWJ(),
												failure: function(o)
												{
													ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoResepRWJ('Penghapusan berhasil','Information');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
														gridDTLTRHistoryApotekRWJ.getView().refresh();
														Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
														Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
														if(Ext.getCmp('cbNonResep').getValue() == true){															
															Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
														} else{
															Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
														}
													}
													else 
													{
														ShowPesanErrorResepRWJ('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
														gridDTLTRHistoryApotekRWJ.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else{
								ShowPesanErrorResepRWJ('Belum melakukan pembayaran','Error');
							}
						}  
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viResepRWJ',
						disabled:true,
						handler:function()
						{															
							
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill [F12]',
								id: 'btnPrintBillResepRWJ',
								handler: function()
								{
									PrintBill='true';
									// panelnew_window_printer();
									printbill_resep_rwj();	
									Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(false,10);
								}
							},
							{
								xtype: 'button',
								text: 'Print Kwitansi [F11]',
								id: 'btnPrintKwitansiResepRWJ',
								handler: function()
								{
									PrintBill='false';
									// panelPrintKwitansi_resepRWJ();
									Ext.Ajax.request({   
										url: baseURL + "index.php/apotek/functionAPOTEK/getjenispembayaran",
										 params: {
											no_out:Ext.getCmp('txtTmpNoout').getValue(),
											tgl_out:Ext.getCmp('txtTmpTglout').getValue()
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {
											var cst = Ext.decode(o.responseText);
											if(cst.jenis_pay == 'TU'){
												panelPrintKwitansi_resepRWJ();
											}else{
												Ext.Msg.show({
													title: 'Warning',
													msg: 'Pembayaran bukan TUNAI, kwitansi tidak dapat dicetak!' ,
													buttons: Ext.MessageBox.OK,
													fn: function (btn) {
														
													}
												});
											}
										}	
									
									});
									
								}
							},
							{
								xtype: 'button',
								text: 'Print Label Dosis Obat [CTRL+O]',
								id: 'btnPrintLabelDosisObatResepRWJ',
								handler: function()
								{
									panelPrintLabelDosisObat_resepRWJ();
									
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					},
					{
					xtype: 'label',
					text: 'Order Obat Poli : ' 
					},
					mComboorder(),
					{
						xtype: 'button',
						text: 'close order',
						id:'statusservice_apt',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							updatestatus_permintaan()
							load_data_pasienorder();
							//AptResepRWJ.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt').disable();
						}
					}
				]
			}
		}
    )

    return pnlFormDataBasic_viApotekResepRWJ;
}
// End Function getFormItemEntry_viApotekResepRWJ # --------------

/**
*	Function : getItemPanelInputBiodata_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/


function getItemPanelInputBiodata_viApotekResepRWJ(lebar) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:5px 5px 5px 5px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[		

			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 500,
				height: 115,
				anchor: '100% 100%',
				items:
				[
					{
						x:0,
						y:0,
						xtype: 'label',
						text:'No. Tr'
					},
					{
						x:55,
						y:0,
						xtype: 'label',
						text:':'
					},
					{
						x:65,
						y:0,
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpNoout',
						id: 'txtTmpNoout',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						x:0,
						y:30,
						xtype: 'label',
						text:'No. Resep'
					},
					{
						x:55,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:65,
						y:30,
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWJ_viApotekResepRWJ',
						id: 'txtNoResepRWJ_viApotekResepRWJ',
						// emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						x:0,
						y:60,
						xtype: 'label',
						text:'Jml Resep'
					},
					{
						x:55,
						y:60,
						xtype: 'label',
						text:':'
					},
					{
						x:65,
						y:60,
						xtype: 'numberfield',
						width : 150,	
						name: 'txtCatatanResepRWJ_viResepRWJ',
						id: 'txtCatatanResepRWJ_viResepRWJ',
						listeners:{
							specialkey:function(){
								if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
									// var records = new Array();
									// records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
									// dsDataGrdJab_viApotekResepRWJ.add(records);
									// var row =dsDataGrdJab_viApotekResepRWJ.getCount()-1;
									// AptResepRWJ.form.Grid.a.startEditing(row, 3);	
									Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').focus();
								}
							}
							
						}
						
					},	
					{
						x:0,
						y:90,
						xtype: 'label',
						text:'Tgl. Resep'
					},
					{
						x:55,
						y:90,
						xtype: 'label',
						text:':'
					},
					{
						x:65,
						y:90,
						xtype: 'datefield',
						id:'dfTglResepSebenarnyaResepRWJ',
						width : 130,	
						format: 'd/M/Y',
						value: now_viApotekResepRWJ
					},
					{
						x:210,
						y:90,
						xtype: 'label',
						text:'*) Jika resep di input bukan pada tanggal di buatnya resep, isi Tanggal Resep sesuai dengan tanggal dibuatnya resep',
						width : 350,
						style : {
							color : 'darkblue',
							'font-size':'10px'
						}
					},
					//-----------------------------------------------------------------------------------
					{
						x:230,
						y:0,
						xtype: 'label',
						text:'Pasien'
					},
					{
						x:295,
						y:0,
						xtype: 'label',
						text:':'
					},
					{
						x:305,
						y:0,
						xtype: 'textfield',
						flex: 1,
						width : 100,	
						name: 'txtKdPasienResepRWJ_viApotekResepRWJ',
						id: 'txtKdPasienResepRWJ_viApotekResepRWJ',
						// emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									getPasienbyKodeNama(Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue(),'');
								};
							}
						}
					},
					{
						x:410,
						y:0,
						xtype: 'textfield',
						flex: 1,
						width : 180,	
						name: 'txtNamaPasienResepRWJ_viApotekResepRWJ',
						id: 'txtNamaPasienResepRWJ_viApotekResepRWJ',
						// emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									getPasienbyKodeNama('',Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue());
								};
							}
						}
					},
					/* AptResepRWJ.form.ComboBox.kodePasien = new Nci.form.Combobox.autoComplete({
						store	: AptResepRWJ.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.nama_unit);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.customer);
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdPay').setValue(b.data.kd_pay);
							Ext.getCmp('txtTmpJenisPay').setValue(b.data.jenis_pay);
							Ext.getCmp('txtNoTlp_viResepRWJ').setValue(b.data.telepon);
							Ext.getCmp('txtNoSep_viResepRWJ').setValue(b.data.no_sjp);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							AptResepRWJ.vars.urut_masuk=b.data.urut_masuk;
							AptResepRWJ.vars.payment=b.data.payment;
							AptResepRWJ.vars.payment_type=b.data.payment_type;
							Ext.getCmp('btnAddObat').enable();
							ordermanajemen=false;
							
							// var records = new Array();
							// records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
							// dsDataGrdJab_viApotekResepRWJ.add(records);
							// row=dsDataGrdJab_viApotekResepRWJ.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
							// AptResepRWJ.form.Grid.a.startEditing(row, 4);	
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(true,10);
						},
						width	: 100,
						x:305,
						y:0,
						insert	: function(o){
							return {
								kd_pasien       : o.kd_pasien,
								nama			: o.nama,
								nama_dokter		: o.nama_dokter,
								kd_unit			: o.kd_unit,
								kd_dokter		: o.kd_dokter,
								kd_customer		: o.kd_customer,
								kd_kasir		: o.kd_kasir,
								no_transaksi	: o.no_transaksi,
								tgl_transaksi	: o.tgl_transaksi,
								urut_masuk		: o.urut_masuk,
								customer		: o.customer,
								nama_unit		: o.nama_unit,
								telepon			: o.telepon,
								no_sjp			: o.no_sjp,
								kd_pay			:o.kd_pay,
								payment			:o.payment,
								jenis_pay		:o.jenis_pay,
								payment_type	:o.payment_type,
								text			:  '<table style="font-size: 11px;"><tr><td width="90" align="left">'+o.kd_pasien+'</td><td width="180" align="left">'+o.nama+'</td><td width="100" align="left">'+ShowDate(o.tgl_transaksi)+'</td><td width="150" align="left">'+o.nama_unit+'</td></tr></table>'
							}
						},
						param:function(){
							return {
								tgl_lookup : Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue()
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 420,
						emptyText: 'Kode Pasien'
					}),	 */
					
					/* AptResepRWJ.form.ComboBox.namaPasien = new Nci.form.Combobox.autoComplete({
						store	: AptResepRWJ.form.ArrayStore.namapasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							AptResepRWJ.form.ComboBox.kodePasien.setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.nama_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.customer);
							Ext.getCmp('txtNoTlp_viResepRWJ').setValue(b.data.telepon);
							Ext.getCmp('txtNoSep_viResepRWJ').setValue(b.data.no_sjp);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							AptResepRWJ.vars.urut_masuk=b.data.urut_masuk;
							AptResepRWJ.vars.payment=b.data.payment;
							AptResepRWJ.vars.payment_type=b.data.payment_type;
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							Ext.getCmp('txtTmpKdPay').setValue(b.data.kd_pay);
							Ext.getCmp('txtTmpJenisPay').setValue(b.data.jenis_pay);
							Ext.getCmp('btnAddObat').enable();
							ordermanajemen=false;
							
							// var records = new Array();
							// records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
							// dsDataGrdJab_viApotekResepRWJ.add(records);
							// row=dsDataGrdJab_viApotekResepRWJ.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
							// AptResepRWJ.form.Grid.a.startEditing(row, 4);	
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(true,10);
							
						},
						x:410,
						y:0,
						width	: 180,
						insert	: function(o){
							return {
								kd_pasien       : o.kd_pasien,
								nama			: o.nama,
								nama_dokter		: o.nama_dokter,
								kd_unit			: o.kd_unit,
								kd_dokter		: o.kd_dokter,
								kd_customer		: o.kd_customer,
								kd_kasir		: o.kd_kasir,
								no_transaksi	: o.no_transaksi,
								tgl_transaksi	: o.tgl_transaksi,
								urut_masuk		: o.urut_masuk,
								customer		: o.customer,
								nama_unit		: o.nama_unit,
								telepon			: o.telepon,
								no_sjp			: o.no_sjp,
								kd_pay			:o.kd_pay,
								payment			:o.payment,
								jenis_pay		:o.jenis_pay,
								payment_type	:o.payment_type,
								text			:  '<table style="font-size: 11px;"><tr><td width="90" align="left">'+o.kd_pasien+'</td><td width="180" align="left">'+o.nama+'</td><td width="100" align="left">'+ShowDate(o.tgl_transaksi)+'</td><td width="150" align="left">'+o.nama_unit+'</td></tr></table>'
							}
						},
						param:function(){
							return {
								tgl_lookup : Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue()
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEK/getPasienResepRWJ",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 420,
						emptyText: 'Nama Pasien'
					}), */
					
					{
						x:230,
						y:30,
						xtype: 'label',
						text:'Jenis Pasien'
					},
					{
						x:295,
						y:30,
						xtype: 'label',
						text:':'
					},
					ComboPilihanKelompokPasienApotekResepRWJ(),
					{
						x:230,
						y:60,
						xtype: 'label',
						text:'No. SEP'
					},
					{
						x:295,
						y:60,
						xtype: 'label',
						text:':'
					},
					{
						x:305,
						y:60,
						xtype: 'textfield',
						width : 135,	
						readOnly:true,
						name: 'txtNoSep_viResepRWJ',
						id: 'txtNoSep_viResepRWJ',
						listeners:{
							specialkey:function(){
								if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
								}
							}
							
						}
						
					},	
					{
						x:445,
						y:60,
						xtype: 'label',
						text:'No. Tlp :'
					},
					// {
						// x:315,
						// y:60,
						// xtype: 'label',
						// text:':'
					// },
					{
						x:490,
						y:60,
						xtype: 'textfield',
						width : 100,	
						name: 'txtNoTlp_viResepRWJ',
						id: 'txtNoTlp_viResepRWJ',
						readOnly:true,
						listeners:{
							specialkey:function(){
								if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
								}
							}
							
						}
						
					},	
					//-----------------------------------------------------------------------------------
					{
						x:600,
						y:0,
						xtype: 'label',
						text:'Nama Unit'
					},
					{
						x:700,
						y:0,
						xtype: 'label',
						text:':'
					},
					ComboUnitApotekResepRWJLookup(),
					{
						x:600,
						y:30,
						xtype: 'label',
						text:'Dokter'
					},
					{
						x:700,
						y:30,
						xtype: 'label',
						text:':'
					},
					ComboDokterApotekResepRWJ(),
					{
						x:600,
						y:60,
						xtype: 'checkbox',
						boxLabel: 'Resep langsung',
						id: 'cbNonResep',
						name: 'cbNonResep',
						width: 100,
						checked: false,
						handler:function(a,b) 
						{
							if(a.checked==true){
								getPaymentPasienLangsung();
								Ext.getCmp('txtNamaPasienNon').enable();
								Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
								Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
								Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
								Ext.getCmp('btnAddObat').enable();
								// Ext.getCmp('txtTmpKdCustomer').setValue('0000000001')
								// AptResepRWJ.form.ComboBox.kodePasien.disable();
								 Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').disable();
								// AptResepRWJ.form.ComboBox.namaPasien.disable();
								Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').disable();
								
							}else{
								Ext.getCmp('txtNamaPasienNon').disable();
								// AptResepRWJ.form.ComboBox.kodePasien.enable();
								 Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').enable();
								// AptResepRWJ.form.ComboBox.namaPasien.enable();
								Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').enable();
								
							}
						}
					},
					{
						x:710,
						y:60,
						xtype: 'textfield',
						width : 170,	
						name: 'txtNamaPasienNon',
						id: 'txtNamaPasienNon',
						emptyText: 'Pasien Resep Langsung',
						displayField:'Nama Pasien',
						disabled:true,
						listeners:{
							specialkey:function(){
								if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
									if(Ext.getCmp('txtTmpKdCustomer').getValue() == ''){
										ShowPesanErrorResepRWJ('Jenis pasien tidak boleh kosong!','Error');
									} else{
										var records = new Array();
										records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
										dsDataGrdJab_viApotekResepRWJ.add(records);
										var row =dsDataGrdJab_viApotekResepRWJ.getCount()-1;
										AptResepRWJ.form.Grid.a.startEditing(row, 4);	
									}
								}
							}
							
						}
						
					},	
					//-----------------------------------------------------------------------------------
					{
						x:890,
						y:60,
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						//hidden:true,
						id: 'btn1/2resep_viApotekResepRWJ',
						handler:function()
						{
							hitungSetengahResep();
						}   
					},					
					{
						x:900,
						y:30,
						xtype:'button',
						text:'Racikan',
						width:70,
						hideLabel:true,
						disabled:true,
						hidden:true,
						id: 'btnRacikan_viApotekResepRWJ',
						handler:function()
						{
							formulaRacikanResepRWJ();
						}   
					},
					//--------HIDDEN---------------
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdUnit',
						id: 'txtTmpKdUnit',
						emptyText: 'kode unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdDokter',
						id: 'txtTmpKdDokter',
						emptyText: 'kode dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdCustomer',
						id: 'txtTmpKdCustomer',
						emptyText: 'kode customer',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpJmlItem',
						id: 'txtTmpJmlItem',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTglResepSebenarnya',
						id: 'txtTmpTglResepSebenarnya',
						emptyText: 'Tgl resep sebenarnya',
						hidden:true,
						value: tanggal_resep_sebenarnya
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTglout',
						id: 'txtTmpTglout',
						emptyText: 'tgl out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPost',
						id: 'txtTmpStatusPost',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpSisaAngsuran',
						id: 'txtTmpSisaAngsuran',
						emptyText: 'sisa angsuran',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpJenisPay',
						id: 'txtTmpJenisPay',
						emptyText: 'Jenis pay',
						hidden:true
					},{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdPay',
						id: 'txtTmpKdPay',
						emptyText: 'KD pay',
						hidden:true
					}
				]
			}
			// {
                // xtype: 'compositefield',
                // fieldLabel: 'No. Tr ',
                // anchor: '100%',
                // width: 250,
                // items: 
                // [                    
					// {
					
					// {
						// xtype: 'displayfield',
						// flex: 1,
						// width: 100,
						// name: '',
						// value: 'Pasien :',
						// fieldLabel: 'Label'
					// },	
					// 
					
						
					// {
						// xtype: 'displayfield',
						// flex: 1,
						// width: 100,
						// name: '',
						// value: 'Nama Unit :',
						// fieldLabel: 'Label'
					// },
					// 
                    // //-------------- ## --------------  
					
                // ]
            // },
            // //-------------- ## --------------  
			// {
                // xtype: 'compositefield',
                // fieldLabel: 'No. Resep ',
                // anchor: '100%',
                // width: 250,
				// labelWidth:50,
                // items: 
                // [                    
					
					// //-------------- ## --------------  
					// {
						// xtype: 'displayfield',
						// flex: 1,
						// width: 100,
						// name: '',
						// value: 'Jenis Pasien :',
						// fieldLabel: 'Label'
					// },
					// 
					// {
						// xtype: 'displayfield',
						// flex: 1,
						// width: 100,
						// name: 'Dokter',
						// value: '',
						// fieldLabel: 'Label'
					// },
					// 
					
                // ]
            // },
			// {
				// xtype: 'compositefield',
				// fieldLabel: 'Catatan ',
				// anchor: '100%',
				// //labelSeparator: '',
				// width: 199,
				// items: 
				// [
					
					// {
						// xtype: 'displayfield',
						// flex: 1,
						// width: 5,
						// name: '',
						// value: '',
						// fieldLabel: 'Label'
					// },
					
					// {
						// xtype: 'displayfield',
						// flex: 1,
						// width: 60,
						// name: '',
						// value: '',
						// fieldLabel: 'Label'
					// },	
					
					
					
				// ]
			// },
			// // {
				// // xtype: 'compositefield',
				// // fieldLabel: 'Poliklinik',
				// // anchor: '100%',
				// // //labelSeparator: '',
				// // width: 199,
				// // items: 
				// // [
				
				// // ]
			// // },
			
			// {
				// xtype: 'compositefield',
				// fieldLabel: 'HIDDEN',
				// hidden:true,
				// anchor: '100%',
				// //labelSeparator: '',
				// width: 199,
				// items: 
				// [
					
				// ]
			// }
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viApotekResepRWJ # --------------


function getPasienbyKodeNama(kd_pasien,nama){
	var tgl_lookup = Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue();
	var url ='';
	
	Ext.Ajax.request
	(
		{
			url :baseURL + "index.php/apotek/functionAPOTEK/getKodeNamaPasienResepRWJ",
			params: {kd_pasien:kd_pasien,nama:nama,tgl_lookup : tgl_lookup},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				console.log(cst.count);
				if (cst.count == 1){
					Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setValue(cst.listData[0].kd_pasien);
					Ext.getCmp('txtTmpKdDokter').setValue(cst.listData[0].kd_dokter);
					Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(cst.listData[0].kd_dokter);
					Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue(cst.listData[0].nama);
					Ext.getCmp('cbo_UnitResepRWJLookup').setValue(cst.listData[0].nama_unit);
					Ext.getCmp('txtTmpKdUnit').setValue(cst.listData[0].kd_unit);
					Ext.getCmp('txtTmpKdDokter').setValue(cst.listData[0].kd_dokter);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(cst.listData[0].customer);
					Ext.getCmp('txtTmpKdCustomer').setValue(cst.listData[0].kd_customer);
					Ext.getCmp('txtTmpKdPay').setValue(cst.listData[0].kd_pay);
					Ext.getCmp('txtTmpJenisPay').setValue(cst.listData[0].jenis_pay);
					Ext.getCmp('txtNoTlp_viResepRWJ').setValue(cst.listData[0].telepon);
					Ext.getCmp('txtNoSep_viResepRWJ').setValue(cst.listData[0].no_sjp);
					AptResepRWJ.vars.no_transaksi=cst.listData[0].no_transaksi;
					AptResepRWJ.vars.tgl_transaksi=cst.listData[0].tgl_transaksi;
					AptResepRWJ.vars.kd_kasir=cst.listData[0].kd_kasir;
					AptResepRWJ.vars.urut_masuk=cst.listData[0].urut_masuk;
					AptResepRWJ.vars.payment=cst.listData[0].payment;
					AptResepRWJ.vars.payment_type=cst.listData[0].payment_type;
					Ext.getCmp('btnAddObat').enable();
					ordermanajemen=false;
					
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
					Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(true,10);
				
				}else if (cst.count > 1){
					LookUpKunjungan_resepRWJ(kd_pasien,nama,tgl_lookup);
				}else if( cst.count == 0){
					Ext.Msg.show({
						title: 'Perhatian',
						//msg: 'Kriteria huruf pencarian obat minimal 3 huruf!',
						msg: 'Data pasien tidak ditemukan!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').focus(true,10);
							}
						}
					});
				}
				
			}
		}
	)
}

function LookUpKunjungan_resepRWJ(kd_pasien,nama,tgl_lookup)
{
	
	WindowLookUpKunjungan_ResepRWJ = new Ext.Window
    (
        {
            id: 'idWindowLookUpKunjungan_ResepRWJ',
            title: 'Daftar Kunjungan',
            width:620,
            height: 235,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				gridListkunjunganResepRWJ() // grid kunjungan pasien
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					/* if(FocusExitResepRWJ == false){
						var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
						AptResepRWJ.form.Grid.a.startEditing(line, 4);	
					} */
				}
			}
        }
    );

    WindowLookUpKunjungan_ResepRWJ.show();
	getListKunjungan_ResepRWJ(kd_pasien,nama,tgl_lookup); 
};


function gridListkunjunganResepRWJ(){
	var fldDetail = ['kd_pasien', 'nama','alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi', 'tgl_transaksi', 'kd_unit', 'kd_dokter', 
						'kd_customer', 'kd_kasir','k.urut_masuk','telepon', 'customer','no_sjp','kd_pay','payment','jenis_pay','payment_type' ];
	dsGridListKunjungan_ResepRWJ = new WebApp.DataStore({ fields: fldDetail });
	
    GridListKunjunganColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'kd_pasien',
			header			: 'No. Medrec',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama',
			header			: 'Nama',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'alamat',
			header			: 'Alamat',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_unit',
			header			: 'Unit',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'tgl_transaksi',
			header			: 'Tanggal Kunjungan',
			width			: 70,
			menuDisabled	: true,
        },
	]);
	
	
	GridListKunjungan_ResepRWJ= new Ext.grid.EditorGridPanel({
		id			: 'GridListKunjungan_ResepRWJ',
		stripeRows	: true,
		width		: 610,
		height		: 195,
        store		: dsGridListKunjungan_ResepRWJ,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListKunjunganColumnModel,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionListKunjunganResepRWJ = undefined;
					currentRowSelectionListKunjunganResepRWJ = dsGridListKunjungan_ResepRWJ.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				// trcellCurrentTindakan_KasirRWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				alert()
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_pasien);
					Ext.getCmp('txtTmpKdDokter').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_dokter);
					Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_dokter);
					Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.nama);
					Ext.getCmp('cbo_UnitResepRWJLookup').setValue(currentRowSelectionListKunjunganResepRWJ.data.nama_unit);
					Ext.getCmp('txtTmpKdUnit').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_unit);
					Ext.getCmp('txtTmpKdDokter').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_dokter);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.customer);
					Ext.getCmp('txtTmpKdCustomer').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_customer);
					Ext.getCmp('txtTmpKdPay').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_pay);
					Ext.getCmp('txtTmpJenisPay').setValue(currentRowSelectionListKunjunganResepRWJ.data.jenis_pay);
					Ext.getCmp('txtNoTlp_viResepRWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.telepon);
					Ext.getCmp('txtNoSep_viResepRWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.no_sjp);
					AptResepRWJ.vars.no_transaksi=currentRowSelectionListKunjunganResepRWJ.data.no_transaksi;
					AptResepRWJ.vars.tgl_transaksi=currentRowSelectionListKunjunganResepRWJ.data.tgl_transaksi;
					AptResepRWJ.vars.kd_kasir=currentRowSelectionListKunjunganResepRWJ.data.kd_kasir;
					AptResepRWJ.vars.urut_masuk=currentRowSelectionListKunjunganResepRWJ.data.urut_masuk;
					AptResepRWJ.vars.payment=currentRowSelectionListKunjunganResepRWJ.data.payment;
					AptResepRWJ.vars.payment_type=currentRowSelectionListKunjunganResepRWJ.data.payment_type;
					Ext.getCmp('btnAddObat').enable();
					ordermanajemen=false;
					
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
					Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(true,10);
					WindowLookUpKunjungan_ResepRWJ.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListKunjungan_ResepRWJ;
}

function getListKunjungan_ResepRWJ(kd_pasien,nama,tgl_lookup){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEK/getKodeNamaPasienResepRWJ",
		params: {
			kd_pasien:kd_pasien,
			nama:nama,
			tgl_lookup:tgl_lookup
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWJ('Error menampilkan list kunjungan. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListKunjungan_ResepRWJ.removeAll();
			var cst = Ext.decode(o.responseText);
			var recs=[],
				recType=dsGridListKunjungan_ResepRWJ.recordType;
			for(var i=0; i<cst.listData.length; i++){
				recs.push(new recType(cst.listData[i]));						
			}
			dsGridListKunjungan_ResepRWJ.add(recs);
			GridListKunjungan_ResepRWJ.getView().refresh();
			GridListKunjungan_ResepRWJ.getSelectionModel().selectRow(0);
			GridListKunjungan_ResepRWJ.getView().focusRow(0);
		}
	});
}
/**
*	Function : getItemGridTransaksi_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viApotekResepRWJ(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 235,//255,//300, 
		tbar:
		[
			{
				text	: 'Tambah Obat',
				id		: 'btnAddObat',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					if(Ext.getCmp('txtTmpKdCustomer').getValue() == ''){
						ShowPesanErrorResepRWJ('Jenis pasien tidak boleh kosong!','Error');
					} else{
						
						if(Ext.getCmp('txtTmpStatusPost').getValue() != 1){
							var records = new Array();
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							var kd_customer=Ext.getCmp('txtTmpKdCustomer').getValue();
							if(kd_customer ==='' ||kd_customer ==='Kelompok Pasien'){
								
							}else{
								getAdm(kd_customer);
							}
							
							setTimeout(
							function(){ 
								records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
								dsDataGrdJab_viApotekResepRWJ.add(records);
								Ext.getCmp('btnRacikan_viApotekResepRWJ').disable();
								var row =dsDataGrdJab_viApotekResepRWJ.getCount()-1;
								AptResepRWJ.form.Grid.a.startEditing(row, 4);
							}, 200);
						}
						
					}
					
				}
			},
			//-------------- ## --------------
			{
				xtype:'tbseparator'
			},
			//-------------- ## --------------
			{
				xtype: 'button',
				text: 'Hapus',
				disabled:true,
				iconCls: 'remove',
				id: 'btnDelete_viApotekResepRWJ',
				handler: function()
				{
					var line = AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
					if(dsDataGrdJab_viApotekResepRWJ.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEK/hapusBarisGridResepRWJ",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viApotekResepRWJ.removeAt(line);
													AptResepRWJ.form.Grid.a.getView().refresh();
													hasilJumlah();
													Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
													Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
													
												}
												else 
												{
													ShowPesanErrorResepRWJ('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viApotekResepRWJ.removeAt(line);
									AptResepRWJ.form.Grid.a.getView().refresh();
									hasilJumlah();
									// Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
									// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorResepRWJ('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
				}
			}	
		],
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewEdit_viApotekResepRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viApotekResepRWJ # --------------

/**
*	Function : gridDataViewEdit_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viApotekResepRWJ(){
	
    AptResepRWJ.form.Grid.a = new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekResepRWJ,
        height: 180,//220,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					CellSelected_viApotekResepRWJ = dsDataGrdJab_viApotekResepRWJ.getAt(row);
					
					currentKdPrdRacik_ResepRWJ=CellSelected_viApotekResepRWJ.data.kd_prd;
					currentNamaObatRacik_ResepRWJ=CellSelected_viApotekResepRWJ.data.nama_obat;
					currentHargaRacik_ResepRWJ=CellSelected_viApotekResepRWJ.data.harga_jual;
					currentJumlah_ResepRWJ=CellSelected_viApotekResepRWJ.data.jml;
					currentHargaJualObat=CellSelected_viApotekResepRWJ.data.harga_jual;
					currentCitoNamaObat=CellSelected_viApotekResepRWJ.data.nama_obat;
					currentCitoKdPrd=CellSelected_viApotekResepRWJ.data.kd_prd;
					curentIndexsSelection_ResepRWJ= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
                }
            }
        }),
        stripeRows: true,
			//-------------- ## --------------
		cm: new Ext.grid.ColumnModel([
			
			new Ext.grid.RowNumberer(),	
			//-------------- ## --------------
			// chkSelected_viApotekResepRWJ,
			//-------------- ## --------------
			{
				dataIndex: 'kd_milik',
				header: 'M',
				// hidden: true,
				width: 30
			},
			{
				dataIndex: 'cito',
				header: 'C',
				width: 50,
				align:'center',
				editor: new Ext.form.ComboBox ( {
					id				: 'gridcboCito_ResepRWJ',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					width			: 50,
					anchor			: '95%',
					value			: 'Tidak',
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	:['Id','displayText'],
						data	: [[0, 'Tidak'],[1, 'Ya']]
					}),
					valueField	: 'displayText',
					displayField: 'displayText',
					listeners	: {
						select	: function(a,b,c){
							var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
							if(b.data.Id == 1) {
								if(o.harga_jual=='' ||o.harga_jual==undefined){
									ShowPesanWarningResepRWJ("Obat yang akan di Cito masih kosong!","Warning");
								} else{
									currentHargaJualObat=o.harga_jual;
									formulacitoResepRWJ();
								}
							} else{
								var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
								var o = dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
								o.harga_jual=o.hargaaslicito;
								o.nilai_cito=0;
								o.cito="Tidak";
								hasilJumlah();
								AptResepRWJ.form.Grid.a.startEditing(line,4);	
							}
						}
					}
				})
			},
			//-------------- ## --------------
			{			
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
								//if(a.getValue().length < 3){
								if(a.getValue().length < 1){
									if(a.getValue().length != 0){
										Ext.Msg.show({
											title: 'Perhatian',
											//msg: 'Kriteria huruf pencarian obat minimal 3 huruf!',
											msg: 'Kriteria Pencarian Obat Tidak Boleh Kosong!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													AptResepRWJ.form.Grid.a.startEditing(line, 4);
												}
											}
										});
									}
									
								} else{		
									PencarianLookupResep = true;// Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
									FocusExitResepRWJ = false;
									LookUpSearchListGetObat_resepRWJ(a.getValue());
								}
							}
						}
					}
				})
			},
			/* {			
				dataIndex: 'nama_obat',
				//id			: Nci.getId(),
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: AptResepRWJ.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
						console.log(dsDataGrdJab_viApotekResepRWJ.getRange()[line].data);
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.cito="Tidak";
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_prd=b.data.kd_prd;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_jual=b.data.harga_jual;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.markup=b.data.markup;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.adm_racik=b.data.adm_racik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jasa=b.data.jasa;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out=b.data.no_out;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_urut=b.data.no_urut;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.tgl_out=b.data.tgl_out;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.milik=b.data.milik;
						// dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.min_stok=b.data.min_stok;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.hargaaslicito=b.data.hargaaslicito;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.nilai_cito=0;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=0;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.racik="Tidak";
						currentHargaJualObat=b.data.harga_jual;
					
						AptResepRWJ.form.Grid.a.getView().refresh();
						
						AptResepRWJ.form.Grid.a.startEditing(line, 6);	
						// alert(b.data.jml_stok_apt)
						
						if(b.data.jml_stok_apt <= 10){
							Ext.Msg.show({
									title: 'Information',
									msg: 'Stok obat hampir habis, jumlah stok tersedia adalah '+b.data.jml_stok_apt,
									buttons: Ext.MessageBox.OK,
									fn: function (btn) {
										if (btn == 'ok')
										{
											AptResepRWJ.form.Grid.a.startEditing(line, 6);
										}
									}
							});
						}
						//Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(b.data.adm_racik);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							kd_satuan		: o.kd_satuan,
							fractions		: o.fractions,
							harga_jual		: o.harga_jual,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							tuslah			: o.tuslah,
							adm_racik		: o.adm_racik,
							markup			: o.markup,
							jasa			: o.jasa,
							no_out			: o.no_out,
							no_urut			: o.no_urut,
							tgl_out			: o.tgl_out,
							kd_milik		: o.kd_milik,
							milik			: o.milik,
							jml_stok_apt	: o.jml_stok_apt,
							hargaaslicito	: o.hargaaslicito,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_prd+'</td><td width="250">'+o.nama_obat+'</td><td width="60">'+o.kd_satuan+'</td><td width="40" align="right">'+o.jml_stok_apt+'</td><td width="80" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td><td width="10" align="right"> </td><td width="50"  align="left">'+o.milik+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
							}
					},
					url		: baseURL + "index.php/apotek/functionAPOTEK/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 450
				})
			}, */
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				width: 60
					
			},
			//-------------- ## --------------
			{
				dataIndex: 'racik',
				header: 'Racikan',
				width: 70,
				align:'center',
				editor: new Ext.form.ComboBox ( {
					id				: 'gridcboRacik_ResepRWJ',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					width			: 50,
					anchor			: '95%',
					value			: 1,
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	:['Id','displayText'],
						data	: [[0, 'Tidak'],[1, 'Ya']]
					}),
					valueField	: 'displayText',
					displayField: 'displayText',
					value		: '',
					listeners	: {
						select	: function(a,b,c){
							if(b.data.Id == 1) {
								Ext.getCmp('btnRacikan_viApotekResepRWJ').enable();
								formulaRacikanResepRWJ();
								statusRacikan_ResepRWJ=b.data.Id;
							} else{
								statusRacikan_ResepRWJ=b.data.Id;
								var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
								Ext.getCmp('btnRacikan_viApotekResepRWJ').disable();
								AptResepRWJ.form.Grid.a.startEditing(line, 9);	
							}
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								if(statusRacikan_ResepRWJ == 0){
									statusRacikan_ResepRWJ=0;
									var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
									AptResepRWJ.form.Grid.a.startEditing(line, 9);
								} 
							}
						}
					}
				})
			},{
				dataIndex: 'min_stok',
				header: 'min stok',
				sortable: true,
				xtype:'numbercolumn',
				hidden: true,
				align:'right',
				width: 85
			},
			//-------------- ## --------------
			{
				dataIndex: 'harga_jual',
				header: 'Harga Sat',
				xtype:'numbercolumn',
				sortable: true,
				align:'right',
				format : '0,000',
				width: 85/* ,
				renderer: 
				function(v, params, record) {
						return parseInt(record.data.harga_jual);
				} */
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml',
				header: 'Qty',
				sortable: true,
				width: 50,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.index;
								if(a.getValue()==''){
									// ShowPesanWarningResepRWJ('Qty obat belum di isi', 'Warning');
									Ext.Msg.show({
										title: 'Perhatian',
										msg: 'Qty obat belum di isi!',
										buttons: Ext.MessageBox.OK,
										fn: function (btn) {
											if (btn == 'ok')
											{
												var thisrow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
												AptResepRWJ.form.Grid.a.startEditing(thisrow, 9);
											}
										}
									});
								}else{
									statusRacikan_ResepRWJ=0;
									var o=dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
									var tmp_jml_stok=0;
									var tmp_min_stok=0;
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEK/getStokObat",
										params: {kd_prd: o.kd_prd, kd_milik :o.kd_milik},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											var cst = Ext.decode(o.responseText);
											if (cst.success==true)
											{
												tmp_jml_stok = cst.jml_stok;
												tmp_min_stok = cst.min_stok;
												console.log( parseFloat(tmp_jml_stok));
												console.log( parseFloat(tmp_min_stok));
												if ( parseFloat(tmp_min_stok) == parseFloat(tmp_jml_stok)){
													Ext.Msg.show({
														title: 'Perhatian',
														msg: 'Sisa stok sudah mencapai atau melebihi minimum stok!',
														buttons: Ext.MessageBox.OK,
														fn: function (btn) {
															if (btn == 'ok')
															{
																o.jml=a.getValue();
																hasilJumlah();
																var thisrow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
																AptResepRWJ.form.Grid.a.startEditing(thisrow, 9);
															}
														}
													});
												} else if(parseFloat(a.getValue()) > parseFloat(tmp_jml_stok)){
													Ext.Msg.show({
														title: 'Perhatian',
														msg: 'Qty melebihi sisa stok tersedia,  Stok hanya tersedia ' +cst.jml_stok+ '!',
														buttons: Ext.MessageBox.OK,
														fn: function (btn) {
															if (btn == 'ok')
															{
																o.jml=tmp_jml_stok;
																hasilJumlah();
																var thisrow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
																AptResepRWJ.form.Grid.a.startEditing(thisrow, 9);
															}
														}
													});
												} else{
													o.jml=a.getValue();
													hasilJumlah();
												
													var records = new Array();
													records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
													dsDataGrdJab_viApotekResepRWJ.add(records);
													var nextRow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
													AptResepRWJ.form.Grid.a.startEditing(nextRow, 4);
												}
												Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
											}
										}
									})
									
									
									
									
									// AptResepRWJ.form.Grid.a.startEditing(line, 12);	
								}
								
							}
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'disc',
				header: 'Diskon',
				sortable: true,
				xtype:'numbercolumn',
				width: 70,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=a.getValue();
							hasilJumlah();
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'jumlah',
				header: 'Sub total',
				sortable: true,
				xtype:'numbercolumn',
				width: 110,
				format : '0,000',
				align:'right'
			},
			//-------------- ## --------------
			{
				dataIndex: 'dosis',
				header: 'Dosis',
				width: 150,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								statusRacikan_ResepRWJ=0;
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
								dsDataGrdJab_viApotekResepRWJ.add(records);
								var nextRow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
								AptResepRWJ.form.Grid.a.startEditing(nextRow, 4);
							}
						}
					}
				})
			},
			//-------------- ## --------------
			//-------------- HIDDEN --------------
			{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'Kode Pabrik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'markup',
				header: 'Markup',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'adm_racik',
				header: 'Adm Racik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jasa',
				header: 'Jasa Tuslah',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'stok tersedia',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'hargaaslicito',
				header: 'hargaaslicito',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'nilai_cito',
				header: 'nilai_cito',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'milik',
				header: 'milik',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
        //plugins:chkSelected_viApotekResepRWJ,
		viewConfig:{
			forceFit: true
		}
    });    
    return AptResepRWJ.form.Grid.a;
}
// End Function gridDataViewEdit_viApotekResepRWJ # --------------

function hasilJumlah(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.jml != undefined){
			//if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				/* if(o.racik){
					console.log(admRacik);
					if(isNaN(admRacik)){
						admRacik=0;
					} else {
						admRacik += parseFloat(o.adm_racik) * parseFloat(o.racik);
						console.log(o.adm_racik+' loih');
					}
				} */
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya' ){
							
							if(o.adm_racik == undefined || o.adm_racik == null || o.adm_racik == 'null' || o.adm_racik == 0){
								admRacik += 0;
							} else{
								admRacik += parseFloat(o.adm_racik);
								
							}
							
						} else{
							admRacik += 0;
						}
						
					}
				} 
				
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			/* } else{
				// ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				// o.jml=o.jml_stok_apt;
				Ext.Msg.show({
					title: 'Perhatian',
					msg: 'Jumlah obat melebihi stok yang tersedia',
					buttons: Ext.MessageBox.OK,
					fn: function (btn) {
						if (btn == 'ok')
						{
							o.jml=o.jml_stok_apt;
							var nextRow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
							AptResepRWJ.form.Grid.a.startEditing(nextRow, 9);
							// AptResepRWJ.form.Grid.a.getView().refresh();
							// var records = new Array();
							// records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
							// dsDataGrdJab_viApotekResepRWJ.add(records);
							// var nextRow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
							// AptResepRWJ.form.Grid.a.startEditing(nextRow, 4);
						}
					}
				});
			} */
			totqty +=parseFloat(o.jml);
		}
		

	}
	admRacik = 0;
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	//alert(toFormat(total));
	total=aptpembulatan(total);

	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(toFormat(totdisc));
	// admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	admprs = 0;
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	console.log(admprs);
	console.log(admRacik);
	
	
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	AptResepRWJ.form.Grid.a.getView().refresh();
}

function hasilJumlahLoad(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		
		if(o.jml != undefined){
			/* console.log(parseFloat(o.jml))
			console.log(parseFloat(o.jml_stok_apt)); */
			
			// if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				// jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				// totdisc += parseFloat(o.disc);
				// o.jumlah =jumlahGrid;
				// total +=jumlahGrid;
				// /* console.log("masuk if -"+o.jml)
				// console.log("masuk if -"+o.jml_stok_apt); */
				
			// } else {
				// ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				// o.jml=o.jml_stok_apt;
				// /* console.log(parseFloat("masuk else -"+o.jml))
				// console.log(parseFloat("masuk else -"+o.jml_stok_apt)); */
				
			// }
			jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			totdisc += parseFloat(o.disc);
			o.jumlah =jumlahGrid;
			total +=jumlahGrid;
			totqty +=parseFloat(o.jml);
		} 
		
		
	}
	
	
	//console.log("new ---");
	admRacik = 0;
	// admRacik=parseFloat(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue())// * parseFloat(o.racik);
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.get('txtDiscEditData_viApotekResepRWJ').dom.value=toFormat(totdisc);
	// admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	admprs = 0;
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.get('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.get('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	/* console.log(admprs);
	console.log(admRacik); */
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	AptResepRWJ.form.Grid.a.getView().refresh();
}



/**
*	Function : getItemGridHistoryBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form Histori pembayaran
*/
function getItemGridHistoryBayar_viApotekResepRWJ(lebar) 
{
    var items =
	{
		title: 'Histori Bayar', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		autoScroll:true,
		height: 100,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewHistoryBayar_viApotekResepRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridHistoryBayar_viApotekResepRWJ # --------------

/**
*	Function : gridDataViewHistoryBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian histori pembayaran
*/
function gridDataViewHistoryBayar_viApotekResepRWJ() 
{

    var fldDetail = ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
	
    dsTRDetailHistoryBayarList = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLTRHistoryApotekRWJ = new Ext.grid.EditorGridPanel
    (
        {
			//title: 'History Bayar',
            store: dsTRDetailHistoryBayarList,
            border: false,
            columnLines: true,
           //frame: false,
			//anchor: '100% 25%',
			height: 72,
			
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
							if(Ext.getCmp('txtTmpStatusPost').getValue() == 0){
								Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').enable();
							}
                            /* cellSelecteddeskripsiRWJ = dsTRDetailHistoryBayarList.getAt(row);
                            CurrentHistoryRWJ.row = row;
                            CurrentHistoryRWJ.data = cellSelecteddeskripsiRWJ; */
                        }
                    }
                }
            ),
			stripeRows: true,
			autoScroll:true,
			columns: /* new Ext.grid.ColumnModel
				( */
					[
					   //new Ext.grid.RowNumberer(),
						{
							id: 'colStatPost',
							header: 'Status Posting',
							dataIndex: 'TUTUP',
							width:100,
							hidden:true
						},
						{
							id: 'colKdPsien',
							header: 'Kode Pasien',
							dataIndex: 'KD_PASIENAPT',
							width:100,
							hidden:true
						},
						{
							id: 'colNoOut',
							header: 'No out',
							dataIndex: 'NO_OUT',
							width:100,
							hidden:true
						},
						{
							id: 'coleurutmasuk',
							header: 'Urut Bayar',
							dataIndex: 'URUT',
							align :'center',
							width:90
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Resep',
							dataIndex: 'TGL_OUT',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_OUT);

							} 
						},
						{
							id: 'colePembayaran',
							header: 'Pembayaran',
							dataIndex: 'URAIAN',
							align :'center',
							width:100,
							hidden:false
							
						},
						{
							id: 'colTGlBayar',
							header: 'Tanggal Bayar',
							dataIndex: 'TGL_BAYAR',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_BAYAR);

							} 
						},
						{
							id: 'colJumlah',
							header: 'Total Bayar',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JUMLAH',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JUMLAH);

							}
							
						},
						{
							id: 'colJumlahAngsuran',
							header: 'Jumlah Angsuran',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JML_TERIMA_UANG',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JML_TERIMA_UANG);

							}
							
						},
						{
							id: 'colSisa',
							header: 'Sisa',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'SISA',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.SISA);

							}
							
						},
						{
							id: 'colKDPAY',
							header: 'KD_PAY',
							dataIndex: 'KD_PAY',
							align :'center',
							width:100,
							hidden:true
							
						},
					],
					viewConfig: {forceFit: true}
	 
		}
    );
    return gridDTLTRHistoryApotekRWJ;
};

function TRHistoryColumModelApotekRWJ() 
{
	//'TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','JUMLAH','JML_TERIMA_UANG'
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colStatPost',
                header: 'Status Posting',
                dataIndex: 'TUTUP',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'colKdPsien',
                header: 'Kode Pasien',
                dataIndex: 'KD_PASIENAPT',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'colNoOut',
                header: 'No out',
                dataIndex: 'NO_OUT',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT'
                
            },
			{
                id: 'colTGlout',
                header: 'Tanggal Resep',
                dataIndex: 'TGL_OUT',
				menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                } 
            },
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'URAIAN',
				width:150,
				hidden:false
                
            },
			{
                id: 'colTGlout',
                header: 'Tanggal Bayar',
                dataIndex: 'TGL_BAYAR',
				menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                } 
            },
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'JUMLAH',
				hidden:false/* ,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.JUMLAH);

                } */
                
            },
			{
                id: 'colJumlah',
                header: 'Jumlah Angsuran',
				width:150,
				align :'right',
                dataIndex: 'JML_TERIMA_UANG',
				hidden:false/* ,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.JML_TERIMA_UANG);

                } */
                
            }/* ,
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME' 
            } */
			

        ]
    )
};
//----------------------------End GetDTLTRHistoryGrid------------------ 




function mComboStatusPostingApotekResepRWJ()
{
  var cboStatusPostingApotekResepRWJ = new Ext.form.ComboBox
	(
		{
			id:'cboStatusPostingApotekResepRWJ',
			x: 410,
			y: 30,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 110,
			editable: false,
			emptyText:'',
			fieldLabel: 'JENIS',
			tabIndex:5,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
								'Id',
								'displayText'
							],
					data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
					}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusPostingApotekResepRWJ,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusPostingApotekResepRWJ=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWJ();
					refreshRespApotekRWJ(tmpkriteria);

				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekResepRWJ();
						refreshRespApotekRWJ(tmpkriteria);
					} 						
				}
			}
		}
	);
	return cboStatusPostingApotekResepRWJ;
};

function ComboUnitApotekResepRWJ()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='2'"
				}
		}
	);
	
    cbo_Unit = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_Unit',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:160,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnit=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWJ();
					refreshRespApotekRWJ(tmpkriteria);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekResepRWJ();
						refreshRespApotekRWJ(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_Unit;
}


var selectSetPilihan;

/* function loadDataComboUnitFar_LapNilaiPersediaanDetail(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/lap_nilaipersediaan/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbo_Unit.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_unit.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_unit.add(recs);
				
			}
		}
	});
} */

function ComboUnitApotekResepRWJLookup()
{
    var cbo_UnitResepRWJLookup = new Ext.form.ComboBox
    (
        {
			x:710,
			y:0,
            flex: 1,
			id: 'cbo_UnitResepRWJLookup',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:170,
			tabIndex:2,
			// readOnly:true,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnitLookup=b.data.displayText ;
					Ext.getCmp('txtTmpKdUnit').setValue(b.data.KD_UNIT);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						Ext.getCmp('cbo_DokterApotekResepRWJ').focus(true,20);
					} 						
				}
			}
        }
    )    
    return cbo_UnitResepRWJLookup;
}

function ComboPilihanKelompokPasienApotekResepRWJ()
{
	var Field_Customer = ['KD_CUSTOMER', 'CUSTOMER'];
	ds_kelpas = new WebApp.DataStore({fields: Field_Customer});
    ds_kelpas.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboKelPasApotek',
					param: "status='t' order by customer"
				}
		}
	);
    var cboPilihankelompokPasienAptResepRWJ = new Ext.form.ComboBox
	(
		{
			x:305,
			y:30,
			id:'cboPilihankelompokPasienAptResepRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			// readOnly:true,
			emptyText:'Kelompok Pasien',
			width: 285,
			store: ds_kelpas,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihankelompokPasien=b.data.displayText;
					Ext.getCmp('txtTmpKdCustomer').setValue(b.data.KD_CUSTOMER);
				},
				'specialkey': function(){
					if (Ext.EventObject.getKey() === 13){
						Ext.getCmp('cbo_UnitResepRWJLookup').focus();
					}
				}
			}
		}
	);
	return cboPilihankelompokPasienAptResepRWJ;
};

function ComboDokterApotekResepRWJ()
{
    var Field_Dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_Dokter});
    ds_dokter.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_dokter',
					Sortdir: 'ASC',
					target: 'ComboDokterApotek',
					param: ''
				}
		}
	);
	
    var cbo_DokterApotekResepRWJ = new Ext.form.ComboBox
    (
        {
			x:710,
			y:30,
            flex: 1,
			id: 'cbo_DokterApotekResepRWJ',
			valueField: 'KD_DOKTER',
            displayField: 'NAMA',
			emptyText:'Dokter',
			store: ds_dokter,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			// readOnly:true,
			width:170,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetDokter=b.data.displayText ;
					Ext.getCmp('txtTmpKdDokter').setValue(b.data.KD_DOKTER);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						var records = new Array();
						records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
						dsDataGrdJab_viApotekResepRWJ.add(records);
						row=dsDataGrdJab_viApotekResepRWJ.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
						AptResepRWJ.form.Grid.a.startEditing(row, 4);	
					} 						
				}
			}
        }
    )    
    return cbo_DokterApotekResepRWJ;
};

function mComboJenisByrResepRWJ() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrResepRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
					Ext.getCmp('txtTmpJenisPay').setValue(b.data.JENIS_PAY);
				}
				  

			}
		}
	);
	
    return cboJenisByr;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			width:225,
			value: 'TUNAI',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						Ext.getCmp('txtTmpKdPay').setValue(b.data.KD_PAY);
						//getTotalDetailProduk();
					
			   
				}
				  

			}
		}
	);

    return cboPembayaran;
};
function ViewDetailPembayaranObat(no_out,tgl_out) 
{	
    var strKriteriaRWJ='';
    strKriteriaRWJ = "a.no_out = '" + no_out + "'" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayarList.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWJ
		}
	}); 
	return dsTRDetailHistoryBayarList;
	
    
};

/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_bayarResepRWJ(rowdata)
{
    var lebar = 450;
	setLookUpApotek_bayarResepRWJ = new Ext.Window
    (
    {
        id: 'setLookUpApotek_bayarResepRWJ',
		name: 'setLookUpApotek_bayarResepRWJ',
        title: 'Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 230,
        resizable:false,
		emptyText:'Pilih Jenis Pembayaran...',
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ getItemPanelBiodataPembayaran_viApotekResepRWJ(lebar,rowdata),
				 getItemPanelBiodataUang_viApotekResepRWJ(lebar,rowdata)
			   ],//1
        listeners:
        {
            activate: function()
            {
				// shortcut_bayar();
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
            },
			close:function(){
				shortcut.remove('lookupbayar');
			}
        }
    }
    );

    setLookUpApotek_bayarResepRWJ.show();
	initLookupBayarResepRWJ();
	// shortcut_bayar();
}

function shortcut_bayar(){
	shortcut.set({
		code:'lookupbayar',
		list:[
			{
				key:'enter',
				fn:function(){
					setLookUpApotek_bayarResepRWJ.close();
				}
			}
		]
	});
}

function initLookupBayarResepRWJ(){
	var stmpnoOut=0;
	var stmptgl;
	if(Ext.getCmp('cbNonResep').getValue() == true){
		Ext.getCmp('txtNamaPasien_Pembayaran').setValue(Ext.get('txtNamaPasienNon').getValue());
	} else {
		Ext.getCmp('txtNamaPasien_Pembayaran').setValue(Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue());
	}
	
	Ext.getCmp('txtkdPasien_Pembayaran').setValue( Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('txtNoResepRWJ_Pembayaran').setValue(Ext.get('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('dftanggalResepRWJ_Pembayaran').setValue(Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue());
	stmpnoOut=Ext.getCmp('txtTmpNoout').getValue();
	stmptgl=Ext.getCmp('txtTmpTglout').getValue();
	
	if(Ext.getCmp('txtTmpNoout').getValue() == 'No out' || Ext.getCmp('txtTmpNoout').getValue() == ''){
		Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
	
	// if(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue() == 0 || Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue() ==''){
		// ShowPesanWarningResepRWJ('Total bayar 0, pembayaran tidak dapat dilakukan!','WARNING');
	// }
	
	loaddatastorePembayaran(Ext.getCmp('txtTmpJenisPay').getValue());
	Ext.getCmp('cboJenisByrResepRWJ').setValue(AptResepRWJ.vars.payment_type);
	Ext.getCmp('cboPembayaran').setValue(AptResepRWJ.vars.payment);
	Ext.getCmp('txtBayarResepRWJ_Pembayaran').focus(true,10);
	
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWJ_Pembayaran',
						id: 'txtNoResepRWJ_Pembayaran',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalResepRWJ_Pembayaran',
						id: 'dftanggalResepRWJ_Pembayaran',
						// readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_Pembayaran',
						id: 'txtkdPasien_Pembayaran',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_Pembayaran',
						id: 'txtNamaPasien_Pembayaran',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrResepRWJ(),
					mComboPembayaran()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalResepRWJ_Pembayaran',
						id: 'txtTotalResepRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						flex: 1,
						width : 150,	
						//readOnly: true,
						style:{'text-align':'right'},
						name: 'txtBayarResepRWJ_Pembayaran',
						id: 'txtBayarResepRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranResepRWJ();
								};
							}
						}
					},
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnBayar_viApotekResepRWJL',
						handler:function()
						{
							pembayaranResepRWJ();
						}   
					}
				]
            }
		]
	};
    return items;
};


/**
*	Function : setLookUpBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_TransferResepRWJ(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_TransferResepRWJ = new Ext.Window
    (
    {
        id: 'setLookUpApotek_transferResepRWJ',
		name: 'setLookUpApotek_transferResepRWJ',
        title: 'Transfer Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 260,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			getItemPanelBiodataTransfer_viApotekResepRWJ(),
			getItemPanelTotalBayar_ApotekResepRWJ()
		],//1
		fbar:[
			{
				xtype:'button',
				text:'Transfer',
				width:70,
				hideLabel:true,
				id: 'btnTransfer_viApotekResepRWJL',
				handler:function()
				{
					transferResepRWJ();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalTransfer_viApotekResepRWJL',
				handler:function()
				{
					setLookUpApotek_TransferResepRWJ.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
            }
        }
    }
    );

    setLookUpApotek_TransferResepRWJ.show();
	stmpnoOut=Ext.getCmp('txtTmpNoout').getValue();
	stmptgl=Ext.getCmp('txtTmpTglout').getValue();
	getSisaAngsuran(stmpnoOut,stmptgl);
	if(Ext.getCmp('cbNonResep').getValue === true){
		Ext.getCmp('txtNamaPasienTransfer_ResepRWJ').setValue(Ext.get('txtNamaPasienNon').getValue());
	} else {
		Ext.getCmp('txtNamaPasienTransfer_ResepRWJ').setValue(Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue());
	}
	Ext.getCmp('txtNoTransaksiTransfer_ResepRWJ').setValue(AptResepRWJ.vars.no_transaksi);
	Ext.getCmp('dftanggalTransaksi_ResepRWJ').setValue(ShowDate(AptResepRWJ.vars.tgl_transaksi));
	
	Ext.getCmp('txtkdPasienTransfer_ResepRWJ').setValue( Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('txtNoResepTransfer_ResepRWJ').setValue(Ext.get('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('dftanggalTransfer_ResepRWJ').setValue(Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue());
	
	Ext.getCmp('txtTotalBayarTransfer_ResepRWJ').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtTotalTransfer_ResepRWJ').setValue(toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()));
	Ext.getCmp('txtTotalTransfer_ResepRWJ').focus(false,10);
	
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataTransfer_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Transaksi ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoTransaksiTransfer_ResepRWJ',
						id: 'txtNoTransaksiTransfer_ResepRWJ',
						emptyText: 'No Transaksi',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tgl. Masuk :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransaksi_ResepRWJ',
						id: 'dftanggalTransaksi_ResepRWJ',
						readOnly:true
					}
				
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepTransfer_ResepRWJ',
						id: 'txtNoResepTransfer_ResepRWJ',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tgl. Transfer :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransfer_ResepRWJ',
						id: 'dftanggalTransfer_ResepRWJ',
						// readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasienTransfer_ResepRWJ',
						id: 'txtkdPasienTransfer_ResepRWJ',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasienTransfer_ResepRWJ',
						id: 'txtNamaPasienTransfer_ResepRWJ',
						readOnly:true
					}
					
                ]
            }
					
		]
	};
    return items;
};



function getItemPanelTotalBayar_ApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[	
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Alasan transfer:',
						fieldLabel: 'Label'
					},
					mComboalasantransfer_RESEPRWJ(),
					
                ]
            },		
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total biaya:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalBayarTransfer_ResepRWJ',
						id: 'txtTotalBayarTransfer_ResepRWJ',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWJ();
								};
							}
						}
					}
					
                ]
            },		
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total transfer :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalTransfer_ResepRWJ',
						id: 'txtTotalTransfer_ResepRWJ',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWJ();
								};
							}
						}
					},
					
                ]
            },
		]
	};
    return items;
};


function mComboalasantransfer_RESEPRWJ() 
{
	var Field = ['KD_ALASAN','ALASAN'];

    var dsalasantransfer_RESEPRWJ = new WebApp.DataStore({ fields: Field });
    dsalasantransfer_RESEPRWJ.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'kd_alasan',
			    Sortdir: 'ASC',
			    target: 'ComboAlasanTransfer',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboalasantransfer_RESEPRWJ = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasantransfer_RESEPRWJ',
		    typeAhead: false,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			width: 130,
			editable:false,
		    anchor:'100%',
		    store: dsalasantransfer_RESEPRWJ,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
			value: 'Pembayaran Disatukan',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{	
			    }

			}
		}
	);
	
    return cboalasantransfer_RESEPRWJ;
};

function datainit_viApotekResepRWJ(rowdata)
{
	dataisi=1;
	Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue(rowdata.NO_RESEP);
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(rowdata.CUSTOMER);
	console.log(rowdata.CUSTOMER);
	Ext.getCmp('txtNoTlp_viResepRWJ').setValue(rowdata.TELEPON);
	Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').setValue(rowdata.CATATANDR);
	Ext.getCmp('txtNoSep_viResepRWJ').setValue(rowdata.NO_SJP);
	Ext.getCmp('dfTglResepSebenarnyaResepRWJ').setValue(ShowDate(rowdata.TGL_OUT));
	if(rowdata.ADMPRHS ==0.00){
		Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(rowdata.ADMPRHS);
	}
	if(rowdata.ADMRESEP == null){
		Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(rowdata.ADMRESEP);
	}
	if(rowdata.JASA == null){
		Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(rowdata.JASA);
	}
	
	Ext.getCmp('cbopasienorder_mng_apotek').disable();
		
	Ext.getCmp('txtTmpKdCustomer').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokter').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnit').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNoout').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTglout').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpSisaAngsuran').setValue(rowdata.SISA);
	Ext.getCmp('txtTmpStatusPost').setValue(rowdata.STATUS_POSTING);
	Ext.getCmp('txtTmpJenisPay').setValue(rowdata.JENIS_PAY);
	Ext.getCmp('txtTmpKdPay').setValue(rowdata.KD_PAY);
	Ext.getCmp('txtTmpTglResepSebenarnya').setValue(rowdata.TGL_RESEP);
	
	AptResepRWJ.vars.no_transaksi=rowdata.APT_NO_TRANSAKSI;
	AptResepRWJ.vars.tgl_transaksi=rowdata.TGL_TRANSAKSI;
	AptResepRWJ.vars.kd_kasir=rowdata.APT_KD_KASIR;
	AptResepRWJ.vars.urut_masuk=rowdata.URUT_MASUK;
	
	AptResepRWJ.vars.payment_type=rowdata.PAYMENT_TYPE;
	AptResepRWJ.vars.payment=rowdata.PAYMENT;
	
	
	kd_pasien_obbt=rowdata.KD_PASIENAPT;
	kd_unit_obbt=rowdata.KD_UNIT;
	tgl_masuk_obbt=rowdata.TGL_MASUK;
	urut_masuk_obbt=rowdata.URUT_MASUK;
	
	if(rowdata.KD_PASIENAPT === '' || rowdata.KD_PASIENAPT === undefined){
		Ext.getCmp('txtNamaPasienNon').setValue(rowdata.NMPASIEN);
		Ext.getCmp('cbNonResep').setValue(true);
		 Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').disable();
		Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').disable();
	} else {
		Ext.getCmp('txtNamaPasienNon').disable();
		 Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setValue(rowdata.KD_PASIENAPT);
		Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue(rowdata.NMPASIEN);
	}
	
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekResepRWJ').enable();
		Ext.getCmp('btnPrint_viResepRWJ').enable();
		Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
		Ext.getCmp('btnAdd_viApotekResepRWJ').enable();
		Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
		Ext.getCmp('btnAddObat').disable();
		nonaktiv(true);
		AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
			
	} else{
		Ext.getCmp('btnAdd_viApotekResepRWJ').enable();
		Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
		Ext.getCmp('btnPrint_viResepRWJ').disable();
		Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
		Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
		Ext.getCmp('btnAddObat').enable();
		nonaktiv(false);
		if(Ext.getCmp('cbNonResep').getValue() == true){			
			Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
		} else{
			Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
		}
		AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
	
	getSeCoba(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	
	Ext.Ajax.request({
									   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			AptResepRWJ.form.Panel.shift.update(cst.shift);
		}
	
	});
	
};

function viewDetailGridOrder(CurrentGridOrder){
	Ext.Ajax.request(
	{
		   
		url: baseURL + "index.php/apotek/functionAPOTEK/cekDilayani",
		params: {
			id_mrresep:CurrentGridOrder.id_mrresep
		},
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)  {
				if(CurrentGridOrder.order_mng == 'Dilayani'){
				
					ShowPesanInfoResepRWJ('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
					setLookUps_viApotekResepRWJ.close();
				} else{
					Ext.getCmp('btnAddObat').enable();
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
					Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
					Ext.getCmp('statusservice_apt').enable();
					Ext.getCmp('cbopasienorder_mng_apotek').enable();
				}
				
			} else{
				if(CurrentGridOrder.order_mng == 'Dilayani'){
					
					ShowPesanInfoResepRWJ('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
					setLookUps_viApotekResepRWJ.close();
				} else{
					ShowPesanWarningResepRWJ('Resep pasien ini sudah dibuat. Pembayaran belum dilakukan harap lakukan pembayaran terlebih dahulu!','Warning');
					Ext.getCmp('btnAddObat').disable();
					Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
					Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
					Ext.getCmp('statusservice_apt').disable();
					Ext.getCmp('cbopasienorder_mng_apotek').disable();
				}
				
			}
		}
	});
	
	kd_pasien_obbt =CurrentGridOrder.kd_pasien;
	kd_unit_obbt=CurrentGridOrder.kd_unit;
	tgl_masuk_obbt =CurrentGridOrder.tgl_masuk;
	urut_masuk_obbt=CurrentGridOrder.urut_masuk;
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(CurrentGridOrder.kd_dokter);
	Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setValue(CurrentGridOrder.kd_pasien);
	Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue(CurrentGridOrder.nama);
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue(CurrentGridOrder.kd_unit);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(CurrentGridOrder.customer);
	AptResepRWJ.vars.no_transaksi=CurrentGridOrder.no_transaksi;
	AptResepRWJ.vars.tgl_transaksi=CurrentGridOrder.tgl_transaksi;
	AptResepRWJ.vars.kd_kasir=CurrentGridOrder.kd_kasir;
	AptResepRWJ.vars.urut_masuk=CurrentGridOrder.urut_masuk;
	Ext.getCmp('txtTmpKdCustomer').setValue(CurrentGridOrder.kd_customer);
	Ext.getCmp('txtTmpKdDokter').setValue(CurrentGridOrder.kd_dokter);
	Ext.getCmp('txtTmpKdUnit').setValue(CurrentGridOrder.kd_unit);
	dataGridObatApotekReseppoli(CurrentGridOrder.id_mrresep,CurrentGridOrder.kd_customer);
	
	console.log(AptResepRWJ.vars.urut_masuk)
	
	//hasilJumlahLoad();
	//getAdm(Ext.getCmp('txtTmpKdCustomer').getValue());
}

function getAdm(kd_customer){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getAdm",
			params: {kd_customer:kd_customer},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(toFormat(cst.tuslah));
					hasilJumlahLoad();
				}
				else 
				{
					Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(toFormat(cst.tuslah));
					hasilJumlahLoad();
					
				};
			}
		}
		
	)
	
}

function unPostingRESEPRWJ(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulanPOST",
		params: {tgl:Ext.getCmp('txtTmpTglout').getValue()},
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
					if (button == 'yes'){
						Ext.Ajax.request ( {
							url: baseURL + "index.php/apotek/functionAPOTEK/unpostingResepRWJ",
							params: getParamUnpostingResepRWJ(),
							failure: function(o)
							{
								ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
							},	
							success: function(o) 
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) 
								{
									ShowPesanInfoResepRWJ('UnPosting Berhasil','Information');
									Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
									Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
									Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
									Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
									Ext.getCmp('btnPrint_viResepRWJ').disable();
									Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
									Ext.getCmp('btnAddObat').enable();
									Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').enable();
									Ext.getCmp('txtTmpStatusPost').setValue(0);
									AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
									refreshRespApotekRWJ();
									nonaktiv(false);
									// unposting_mrobatRWJ();
								}
								else 
								{
									ShowPesanErrorResepRWJ('Gagal melakukan unPosting. '+ cst.pesan, 'Error');
								};
							}
						})
					}
				});
			} else{
				if(cst.pesan=='Periode sudah ditutup'){
					ShowPesanErrorResepRWJ('Periode sudah diTutup, tidak dapat melakukan transaksi','Error');
				} else{
					ShowPesanErrorResepRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
				}
				
			}
		}
	});
}

function unposting_mrobatRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/unposting_mrobat",
			params: getParamUnPostingMrObat(),
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Error unPosting MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}


function updatestatus_permintaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/update_obat_mng",
			params: {
				kd_pasien: kd_pasien_obbt,
				kd_unit:   kd_unit_obbt,
				tgl_masuk: tgl_masuk_obbt,
				urut_masuk: urut_masuk_obbt,
      		},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
	
}


function getSeCoba(no_out,tgl_out,admracik){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/sess",
			params: {query:"no_out = " + no_out},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd=cst.session;
					dataGridObatApotekResepRWJ(no_out,tgl_out,admracik,kd);
				}
			}
		}
	);
}


function cekTransfer(no_out,tgl_out){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/cekTransfer",
			params: {
				no_out: no_out,
				tgl_out: tgl_out
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					unPostingRESEPRWJ();
				} else{
					ShowPesanWarningResepRWJ(cst.pesan,"Warning");
				}
			}
		}
	);
}

function dataGridObatApotekResepRWJ(no_out,tgl_out,admracik,kd){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/readGridObat",
			params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' and b.kd_unit_far='"+kd+"' "},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
					// Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(admracik);
					Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(0);
					hasilJumlahLoad();
				}
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function dataGridObatApotekReseppoli(id_mrresep,cuss){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getobatdetail_frompoli",
			params: {query:id_mrresep,
					 cus :cuss
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{    //dsDataGrdJab_viApotekResepRWJ.store.removeAll();
				dsDataGrdJab_viApotekResepRWJ.loadData([],false);
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
					
					getAdm(Ext.getCmp('txtTmpKdCustomer').getValue());
					
				} 
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}



function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(cst.sisa ==''){
						ShowPesanInfoResepRWJ('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(toFormat(cst.sisa));
						Ext.getCmp('txtBayarResepRWJ_Pembayaran').setValue(cst.sisa);
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
				};
			}
		}
		
	)
}

function datasave_viApotekResepRWJ(){
	if (ValidasiEntryResepRWJ(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/saveResepRWJ",
				params: getParamResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoResepRWJ('Resep berhasil dilayani','Information');
						Ext.get('txtNoResepRWJ_viApotekResepRWJ').dom.value=cst.noresep;
						Ext.get('txtTmpNoout').dom.value=cst.noout;
						Ext.get('txtTmpTglout').dom.value=cst.tgl;
						Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
						Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
						if(Ext.getCmp('cbNonResep').getValue() == true){
							Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
						} else{
							Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();	
						}
						refreshRespApotekRWJ();
						getGridDetailObatApotekResepRWJ(cst.noout,cst.tgl);
						
						if(mBol === false)
						{
							
						};
					}
					else 
					{
						ShowPesanErrorResepRWJ('Resep gagal dilayani', 'Error');
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}

function cekPeriodeBulan(detailorder,CurrentGridOrder){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/cekBulan",
			params: {tgl:tanggalcekbulan},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(detailorder==true){
						setLookUp_viApotekResepRWJ();
						viewDetailGridOrder(CurrentGridOrder)
					} else{
						setLookUp_viApotekResepRWJ();
					}
					
				} else{
					if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
						ShowPesanErrorResepRWJ('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
					} else{
						ShowPesanErrorResepRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
					}
					
				}
			}
		}
	);
}

function pembayaranResepRWJ(){
	if(ValidasiBayarResepRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/bayarSaveResepRWJ",
				params: getParamBayarResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						refreshRespApotekRWJ();
						if(toInteger(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue())){
							ShowPesanInfoResepRWJ('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue(cst.noresep);
							Ext.getCmp('txtTmpNoout').setValue(cst.noout);
							Ext.getCmp('txtTmpTglout').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekResepRWJ').enable();
							Ext.getCmp('btnPrint_viResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
							Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
							Ext.getCmp('btnAddObat').disable();
							Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
							Ext.getCmp('txtTmpStatusPost').setValue(1);
							AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ViewDetailPembayaranObat(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							setLookUpApotek_bayarResepRWJ.close();
							nonaktiv(true);
							
							// if(ordermanajemen==true){
								// updatestatus_permintaan();
								// load_data_pasienorder();
							// }
							
							// if(Ext.getCmp('cbNonResep').getValue() == false){
							
								// insert_mrobatRWJ(); 
							// }
							
							
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							viewGridOrderAll_RASEPRWJ();
						} else {
							ShowPesanInfoResepRWJ('Berhasil melakukan pembayaran','Information');
							setLookUpApotek_bayarResepRWJ.close();
							Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
							Ext.getCmp('btnPrint_viResepRWJ').disable();
							ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							nonaktiv(true);
							
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							viewGridOrderAll_RASEPRWJ();
						}
					} 
					else 
					{
						// ShowPesanErrorResepRWJ('Gagal melakukan pembayaran. ' + cst.pesan, 'Error');
						Ext.Msg.show({
							title: 'Error',
							msg: 'Gagal melakukan pembayaran. ' + cst.pesan,
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									Ext.getCmp('txtTotalTransfer_ResepRWJ').focus(false,10);
								}
							}
						});
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}


function transferResepRWJ(){
	if(ValidasiTransferResepRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/saveTransfer",
				params: getParamTransferResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
							refreshRespApotekRWJ();
						
							ShowPesanInfoResepRWJ('Transfer berhasil dilakukan','Information');
							Ext.getCmp('btnunposting_viApotekResepRWJ').enable();
							Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
							Ext.getCmp('btnPrint_viResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
							setLookUpApotek_TransferResepRWJ.close();
							Ext.getCmp('txtTmpStatusPost').setValue(1);
							ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							nonaktiv(true);
							AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							/* if(ordermanajemen==true){
								updatestatus_permintaan();
								load_data_pasienorder();
							}
							if(Ext.getCmp('cbNonResep').getValue() == false){
							
								insert_mrobatRWJ();
							} */
							
							//AptResepRWJ.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt').disable();
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							viewGridOrderAll_RASEPRWJ();
					}
					else 
					{
						// ShowPesanErrorResepRWJ('Gagal melakukan transfer. '+ cst.pesan, 'Error');
						Ext.Msg.show({
							title: 'Error',
							msg: 'Gagal melakukan transfer. '+ cst.pesan,
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									Ext.getCmp('txtTotalTransfer_ResepRWJ').focus(false,10);
								}
							}
						});
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}

function insert_mrobatRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/save_mrobat",
			params: getParamInsertMrObat(),
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Error simpan MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}

function total_pasien_order_mng_obtrwj()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/countpasienmr_resep",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr_apt_rwj').setValue(cst.countpas);
			console.log(cst);
		}
	});
};

function total_pasien_dilayani_order_mng_obtrwj()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/countpasienmrdilayani_resep",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttrDilayani_apt_rwj').setValue(cst.countpas);
			console.log(cst);
		}
	});
};


function viewGridOrderAll_RASEPRWJ(nama,tgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/vieworderall",
			params: {
				nama:nama,
				tgl:tgl
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{   
				//dataSourceGridOrder_viApotekResepRWJ.loadData([],false);
				dataSourceGridOrder_viApotekResepRWJ.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dataSourceGridOrder_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dataSourceGridOrder_viApotekResepRWJ.add(recs);
					
					
					GridDataViewOrderManagement_viApotekResepRWJ.getView().refresh();
			
				} 
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	);
}


function load_data_pasienorder(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/getPasienorder_mng",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng_apotek.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dspasienorder_mng_apotek.add(recs);
				console.log(o);
			}
		}
	});
}

function load_data_printer(param)
{
	var kriteriaPrint;
	if(PrintBill == 'true'){
		kriteriaPrint='apt_printer_bill_'+UnitFarAktif_ResepRWJ;
	} else{
		kriteriaPrint='apt_printer_kwitansi_'+UnitFarAktif_ResepRWJ;
	}

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/group_printer",
		params:{
			kriteria: kriteriaPrint
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_printer.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboorder()
{ 
 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dspasienorder_mng_apotek = new WebApp.DataStore({ fields: Field });
	
	load_data_pasienorder();
	cbopasienorder_mng_apotek= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_apotek',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			disabled		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  '',
			align: 'Right',
			width: 170,
			store: dspasienorder_mng_apotek,
			valueField: 'display',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
							ordermanajemen=true;
				            kd_pasien_obbt =b.data.kd_pasien;
							kd_unit_obbt=b.data.kd_unit;
							tgl_masuk_obbt =b.data.tgl_masuk;
							urut_masuk_obbt=b.data.urut_masuk;
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setValue(b.data.kd_pasien);
							Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.kd_customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							dataGridObatApotekReseppoli(b.data.id_mrresep,b.data.kd_customer);
							
							Ext.getCmp('btnAddObat').enable();
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('statusservice_apt').enable();
							hasilJumlahLoad();
							getAdm(Ext.getCmp('txtTmpKdCustomer').getValue());
							
							CurrentIdMrResep=b.data.id_mrresep;
							
					},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder($this1.lastQuery);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    },
				
			}
		}
	);return cbopasienorder_mng_apotek;
};

function mCombo_printer_resep_rwj()
{ 
	var Field = ['alamat_printer'];

    dsprinter = new WebApp.DataStore({ fields: Field });
	
	load_data_printer();
	cbopasienorder_printer= new Ext.form.ComboBox
	(
		{
			id	: 'cbopasienorder_printer',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText	: 'Pilih Printer',
			fieldLabel	:  'Daftar Printer',
			align: 'Right',
			anchor: '100%',
			store	: dsprinter,
			valueField: 'alamat_printer',
			displayField: 'alamat_printer',
			listeners:
			{
								
			}
		}
	);
	return cbopasienorder_printer;
};


function printbill_resep_rwj()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorResepRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function printkwitansi_resep_rwj()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/cetakKwitansi/save",
			params: {
				Table: 'cetakKwitansi',
				kd_pasien:Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWJ').getValue(),
				pembayar:Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWJ').getValue(),
				jumlah_bayar:toInteger(Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWJ').getValue()),
				nama:Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue(),
				keterangan_bayar:Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ').getValue(),
				no_resep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
				no_out:Ext.getCmp('txtTmpNoout').getValue(),
				tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
				// printer:Ext.getCmp('cbopasienorder_printer').getValue(),
				kd_form:KdForm
			},
			failure: function(o)
			{	
				ShowPesanErrorResepRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoResepRWJ('Kwitansi sedang dicetak!','Information');
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamResepRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	// if(Ext.get('txtTmpKdCustomer').getValue()=='Perseorangan'){
			// dCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		// }else if(Ext.get('txtTmpKdCustomer').getValue()=='Perusahaan'){
			// KdCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		// }else {
			// KdCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		// }
	
	if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		// tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
		tmpNamaPasien =  Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue() === '' || Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue()=='No Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		KdPasien:Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue(),
		NmPasien:tmpNamaPasien,		
		KdUnit: Ext.getCmp('txtTmpKdUnit').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokter').getValue(),
		NonResep:tmpNonResep,
		NoResepAsal:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNoout').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTglout').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPost').getValue(),
		Tanggal:Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue(),
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtDiscEditData_viApotekResepRWJ').getValue()),
		AdmRacikAll:0,//toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()),
		Adm:0,//toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
		Admprsh:0,//toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
		Kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue(),
		NoTransaksiAsal:AptResepRWJ.vars.no_transaksi,
		KdKasirAsal:AptResepRWJ.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Catatandr:Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		TglResep:Ext.getCmp('txtTmpTglResepSebenarnya').getValue(),
		Posting:0,
		IdMrResep:CurrentIdMrResep //id_mrresep untuk update status resep = sedang dilayani
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		if(dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik == 'Ya'){
			params['racik-'+i]=1;
		} else{
			params['racik-'+i]=0;
		}
		
		if(dsDataGrdJab_viApotekResepRWJ.data.items[i].data.cito == 'Ya'){
			params['cito-'+i]=1;
			params['hargaaslicito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.hargaaslicito;
			params['nilai_cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nilai_cito;
		} else{
			params['cito-'+i]=0;
			params['hargaaslicito-'+i]=0;
			params['nilai_cito-'+i]=0;
		}
		
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_milik
	}
	
    return params
};

function getParamBayarResepRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	// if(Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue()=='Perseorangan'){
		// KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
	// }else if(Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue()=='Perusahaan'){
		// KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
	// }else {
		// KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
	// }
	
	if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		// tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
		tmpNamaPasien =  Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	}
	var ubah=0;
	if(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue() === '' || Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue()=='No Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoresep='';
	if(Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue() === '' || Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue() === 'No Resep' ){
		LangsungPost = 1;
		tmpNoresep='';
	}else {
		LangsungPost = 0;
		tmpNoresep = Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue();
	}
	
	var kd_pay;
	if(Ext.getCmp('cboPembayaran').getValue() == 'TUNAI'){
		kd_pay='TU';
	} else{
		kd_pay=Ext.getCmp('cboPembayaran').getValue();
	}
	
	

	var params =
	{
		NmPasien:tmpNamaPasien,		
		KdUnit: Ext.getCmp('txtTmpKdUnit').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokter').getValue(),
		NonResep:tmpNonResep,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtDiscEditData_viApotekResepRWJ').getValue()),
		AdmRacikAll:0,//toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()),
		Adm:0,//toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
		Admprsh:0,//toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
		Kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue(),
		NoTransaksiAsal:AptResepRWJ.vars.no_transaksi,
		KdKasirAsal:AptResepRWJ.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPay:Ext.getCmp('txtTmpKdPay').getValue(),
		JumlahTotal:toInteger(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue()),
		NoResep:tmpNoresep,
		TanggalBayar:Ext.getCmp('dftanggalResepRWJ_Pembayaran').getValue(),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Posting:LangsungPost,
		Shift: tampungshiftsekarang,
		Tanggal:Ext.getCmp('txtTmpTglResepSebenarnya').getValue(),
		Ubah:ubah
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_milik
	}
	
    return params
};

function getParamTransferResepRWJ(){
	var params =
	{
		Kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue(),
		NoTransaksi:AptResepRWJ.vars.no_transaksi,
		TglTransaksi:AptResepRWJ.vars.tgl_transaksi,
		KdKasir:AptResepRWJ.vars.kd_kasir,
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		KdUnitAsal:Ext.getCmp('txtTmpKdUnit').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasienTransfer_ResepRWJ').getValue(),
		
		JumlahTotal:toInteger(Ext.getCmp('txtTotalTransfer_ResepRWJ').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtTotalTransfer_ResepRWJ').getValue()),
		TanggalBayar:Ext.getCmp('dftanggalTransfer_ResepRWJ').getValue(),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Shift: tampungshiftsekarang,
		Tanggal:tanggal
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_milik
	}
	
    return params
}

function getParamInsertMrObat(){
	var params =
	{
		// kd_pasien: AptResepRWJ.form.ComboBox.kodePasien.getValue(),//kd_pasien_obbt,
		kd_pasien: Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue(),//kd_pasien_obbt,
		kd_unit:   Ext.getCmp('txtTmpKdUnit').getValue(),//kd_unit_obbt,
		tgl_masuk: AptResepRWJ.vars.tgl_transaksi,
		urut_masuk: AptResepRWJ.vars.urut_masuk,
		tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
		no_out:Ext.getCmp('txtTmpNoout').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
	}
	
	return params
}

function getParamUnpostingResepRWJ() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_milik
	}
	
    return params
}

function getParamUnPostingMrObat(){
	var params =
	{
		kd_pasien: kd_pasien_obbt,
		kd_unit:   kd_unit_obbt,
		tgl_masuk: tgl_masuk_obbt,
		urut_masuk: urut_masuk_obbt,
		tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
		no_out:Ext.getCmp('txtTmpNoout').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
	}
	
	return params
}

function getParamDeleteHistoryResepRWJ() 
{
	var o = dsTRDetailHistoryBayarList.getRange()[gridDTLTRHistoryApotekRWJ.getSelectionModel().selection.cell[0]].data;
	
	var params =
	{
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		urut:o.URUT,
		kd_pay:o.KD_PAY
	}
	
	
	
    return params
}


function datacetakbill(){
	var tmpnama;
	var tmppoli;
	var tmpdokter;
	var tmpkode;
	
	if(Ext.getCmp('cbNonResep').getValue() === true){
		tmpnama=Ext.getCmp('txtNamaPasienNon').getValue();
		tmpkode='-';
	} else{
		// tmpnama=AptResepRWJ.form.ComboBox.namaPasien.getValue();
		tmpnama=Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue();
		// tmpkode=AptResepRWJ.form.ComboBox.kodePasien.getValue();
		tmpkode=Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue();
	}
	
	if(Ext.get('cbo_UnitResepRWJLookup').getValue() == '' || Ext.get('cbo_UnitResepRWJLookup').getValue()=='Poliklinik'){
		tmppoli='-';
	} else{
		tmppoli=Ext.get('cbo_UnitResepRWJLookup').getValue();
	}
	
	if(Ext.get('cbo_DokterApotekResepRWJ').getValue() == '' || Ext.get('cbo_DokterApotekResepRWJ').getValue()=='Dokter'){
		tmpdokter='-';
	} else{
		tmpdokter=Ext.get('cbo_DokterApotekResepRWJ').getValue();
	}
	
	var params =
	{
		Table: 'billprintingreseprwj',
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		KdPasien:tmpkode,
		NamaPasien:tmpnama,
		JenisPasien:Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue(),
		Poli:tmppoli,
		Dokter:tmpdokter,
		NoSEP:Ext.getCmp('txtNoSep_viResepRWJ').getValue(),
		AdmRacik:0,//Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue(),
		Adm:Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue(),
		Shift:tampungshiftsekarang,
		Total:toInteger(Ext.get('txtTotalEditData_viApotekResepRWJ').getValue()),
		Tot:toInteger(Ext.get('txtTotalEditData_viApotekResepRWJ').getValue()),
		// printer:Ext.getCmp('cbopasienorder_printer').getValue(),
		kd_form:KdForm
		
	}
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat;
		params['harga_sat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual;
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml;
	}
	
    return params
}



function ValidasiEntryResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbNonResep').getValue() === false){
		// if(AptResepRWJ.form.ComboBox.namaPasien.getValue() === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0 || AptResepRWJ.form.ComboBox.kodePasien.getValue() === ''){
		if(Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ') === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0 || Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue() === ''){
			if(Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ') === ''){
				ShowPesanWarningResepRWJ('Nama Pasien belum di isi!', 'Warning');
				x = 0;
			} else if(dsDataGrdJab_viApotekResepRWJ.getCount() == 0){
				ShowPesanWarningResepRWJ('Daftar resep obat belum di isi!', 'Warning');
				x = 0;
			} else {
				ShowPesanWarningResepRWJ('Kode Pasien belum di isi!', 'Warning');
				x = 0;
			}
		}
	} else {
		if(Ext.getCmp('txtNamaPasienNon').getValue() === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0){
			if(Ext.getCmp('txtNamaPasienNon').getValue() === '' ){
				ShowPesanWarningResepRWJ('Nama', 'Warning');
				x = 0;
			} else {
				ShowPesanWarningResepRWJ('Daftar resep obat belum di isi!', 'Warning');
				x = 0;
			}
		}
	}
	
	if(Ext.getCmp('cbo_UnitResepRWJLookup').getValue() == ''){
		ShowPesanWarningResepRWJ('Poliklinik belum di isi!', 'Warning');
		x = 0;
	}	
	
	if(Ext.getCmp('cbo_DokterApotekResepRWJ').getValue() == ''){
		ShowPesanWarningResepRWJ('Dokter belum di isi!', 'Warning');
		x = 0;
	}
	
	if((Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === 0) ||
		(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue() === 0)){
		if(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === 0){
			ShowPesanWarningResepRWJ('Jumlah harga total obat kosong, harap periksa kelengkapan pengisian obat!', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningResepRWJ('Jumlah total bayar kosong, harap periksa kelengkapan pengisian obat!', 'Warning');
			x = 0;
		}
	}
	
	if(Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').getValue() < 0){
		ShowPesanWarningResepRWJ('Jumlah resep tidak boleh kurang dari 1!.', 'Warning');
		x = 0;
	} 
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.kd_prd == undefined || o.kd_prd == ""){
			// ShowPesanWarningResepRWJ('Obat masih kosong, harap isi baris kosong atau hapus untuk melanjutkan!', 'Warning');
			// x = 0;
			dsDataGrdJab_viApotekResepRWJ.removeAt(i);
			AptResepRWJ.form.Grid.a.getView().refresh(); 
		} else{
			if(o.jml == undefined || o.jumlah == undefined){
				if(o.jml == undefined){
					ShowPesanWarningResepRWJ('Jumlah qty belum di isi, qty tidak boleh kosong!', 'Warning');
					x = 0;
				}else if(o.jumlah == undefined){
					ShowPesanWarningResepRWJ('Jumlah total obat kosong, harap periksa qty dan kelengkapan pengisian obat!', 'Warning');
					x = 0;
				}
			}
			/* if(parseFloat(o.jml) > parseFloat(o.jml_stok_apt)){
				ShowPesanWarningResepRWJ('Jumlah qty melebihi stok!', 'Warning');
				o.jml=o.jml_stok_apt;
				x = 0;
			} */
		}
		
		for(var j=0; j<dsDataGrdJab_viApotekResepRWJ.getCount() ; j++){
			var p=dsDataGrdJab_viApotekResepRWJ.getRange()[j].data;
			var kd_prd_milik1 = o.kd_prd+''+o.kd_milik;
			var kd_prd_milik2 = p.kd_prd+''+p.kd_milik;
			console.log(kd_prd_milik1);
			console.log(kd_prd_milik2);
			// if(i != j && o.kd_prd == p.kd_prd){
			if(i != j && kd_prd_milik1 == kd_prd_milik2){
				ShowPesanWarningResepRWJ('Item obat '+ p.nama_obat +' untuk kepemilikan obat : '+ p.milik +' sudah diInput. Proses tidak dapat dilajutkan!', 'Warning');
				x = 0;
				break;
			}
			
			// if(i != j && o.kd_milik != p.kd_milik){
				// ShowPesanWarningResepRWJ('Tidak boleh ada kepemilikan obat yang berbeda dalam satu resep!', 'Warning');
				// x = 0;
				// break;
			// }
		}
	}
	
	return x;
};

function ValidasiBayarResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrResepRWJ').getValue() === '' ){
		ShowPesanWarningResepRWJ('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaran').getValue() === '' || Ext.getCmp('cboPembayaran').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningResepRWJ('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue() === ''){
		ShowPesanWarningResepRWJ('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue() === '' || Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue() ==0){
		ShowPesanWarningResepRWJ('Total bayar 0, pembayaran tidak dapat dilakukan!', 'Warning');
		x = 0;
	}
	
	
	return x;
};

function ValidasiTransferResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtNoTransaksiTransfer_ResepRWJ').getValue() === '' || Ext.getCmp('txtNoTransaksiTransfer_ResepRWJ').getValue() === 'No Transaksi'){
		ShowPesanWarningResepRWJ('No transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransaksi_ResepRWJ').getValue() === '' ){
		ShowPesanWarningResepRWJ('Tanggal transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtNoResepTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('No. resep tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('Tanggal tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtkdPasienTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('Kode pasien tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtTotalTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('Jumlah transfer tidak boleh kosong', 'Warning');
		x = 0;
	}
	
	return x;
};

function getCriteriaCariApotekResepRWJ()//^^^
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue()!=='No. Resep')
            {
                strKriteria = " o.no_resep  " + "LIKE upper('%" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasien').getValue() != "" && Ext.get('txtKdNamaPasien').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasien').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekResepRWJ').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekResepRWJ').getValue() != "" && Ext.get('dfTglAkhirApotekResepRWJ').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWJ').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWJ').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_Unit').getValue() != "" && Ext.get('cbo_Unit').getValue() != 'Poliklinik')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_Unit').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_Unit').getValue() + "'";
			    }
                
            }
			
	 strKriteria = strKriteria +" and left(o.kd_unit,1)<>'1' and o.returapt=0 and o.kd_unit_far='" + UnitFarAktif_ResepRWJ +"' order by o.tgl_out "
	 return strKriteria;
}

function dataaddnew_viApotekResepRWJ() 
{
	getDefaultCustomer();
	nonaktiv(false);
	Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue('');
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue('');
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue('');
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue('');
	Ext.getCmp('txtNamaPasienNon').setValue('');
	// AptResepRWJ.form.ComboBox.kodePasien.setValue('');
	Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setValue('');
	// AptResepRWJ.form.ComboBox.namaPasien.setValue('');
	Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue('');
	dsDataGrdJab_viApotekResepRWJ.loadData([],false);
	Ext.getCmp('cbopasienorder_mng_apotek').enable();
	Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').setValue('');
	Ext.getCmp('txtNoSep_viResepRWJ').setValue('');
	Ext.getCmp('txtNoTlp_viResepRWJ').setValue('');
	
	AptResepRWJ.vars.no_transaksi="";
	AptResepRWJ.vars.tgl_transaksi="";
	AptResepRWJ.vars.kd_kasir="";
	AptResepRWJ.vars.urut_masuk="";
	AptResepRWJ.vars.payment_type="";
	AptResepRWJ.vars.payment="";
	FocusExitResepRWJ = "";
	PencarianLookupResep = "";
	currentRowSelectionPencarianObatResepRWJ = "";
	Ext.getCmp('txtTmpKdCustomer').setValue("");
	Ext.getCmp('txtTmpKdDokter').setValue("");
	Ext.getCmp('txtTmpKdUnit').setValue("");
	// AptResepRWJ.form.ComboBox.namaPasien.setValue("");
	Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue("");
	Ext.getCmp('txtTmpNoout').setValue("");
	Ext.getCmp('txtTmpTglout').setValue("");
	Ext.getCmp('txtTmpTglResepSebenarnya').setValue(tanggal_resep_sebenarnya);
	Ext.getCmp('txtTmpStatusPost').setValue("");
	Ext.getCmp('txtTmpJmlItem').setValue("");
	Ext.getCmp('txtTmpSisaAngsuran').setValue("");
	Ext.getCmp('txtTmpKdPay').setValue("");
	Ext.getCmp('txtTmpJenisPay').setValue("");
	kd_pasien_obbt="";
	kd_unit_obbt="";
	tgl_masuk_obbt="";
	urut_masuk_obbt="";
	Ext.getCmp('txtNamaPasienNon').setValue("");
	Ext.getCmp('txtNamaPasienNon').disable();
	// AptResepRWJ.form.ComboBox.kodePasien.enable();
	Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').enable();
	// AptResepRWJ.form.ComboBox.namaPasien.enable();
	Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').enable();
	Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
	Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
	Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
	Ext.getCmp('btnPrint_viResepRWJ').disable();
	Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
	Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
	Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
	Ext.getCmp('btnAddObat').enable();
	Ext.getCmp('cbNonResep').setValue(false);
	
	dsTRDetailHistoryBayarList.removeAll();
	AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').focus();
	
}

function loadDataKodePasienResepRWJ(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
		params: {
			text:param,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboKodePasienResepRWJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKodePasien_ResepRWJ.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKodePasien_ResepRWJ.add(recs);
				console.log(o);
			}
		}
	});
};

function mComboKodePasienResepRWJ(){
	var Field = ['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 
				'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir','urut_masuk','customer','nama_dokter','text'];
	dsKodePasien_ResepRWJ = new WebApp.DataStore({fields: Field});
	loadDataKodePasienResepRWJ();
	cboKodePasienResepRWJ = new Ext.form.ComboBox
	(
		{
			x:325,
			y:0,
			id: 'cboKodePasienResepRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hideTrigger		: true,
			store: dsKodePasien_ResepRWJ,
			valueField: 'kd_pasien',
			displayField: 'text',
			emptyText: 'No. Medrec',
			width:100,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
					Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
					// AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
					Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setValue(b.data.nama);
					Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.nama_unit);
					Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.customer);
					Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
					Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
					Ext.getCmp('txtNoTlp_viResepRWJ').setValue(b.data.telepon);
					Ext.getCmp('txtNoSep_viResepRWJ').setValue(b.data.no_sjp);
					AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
					AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
					AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
					AptResepRWJ.vars.urut_masuk=b.data.urut_masuk;
					Ext.getCmp('btnAddObat').enable();
					ordermanajemen=false;
					
					var records = new Array();
					records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
					dsDataGrdJab_viApotekResepRWJ.add(records);
					row=dsDataGrdJab_viApotekResepRWJ.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					AptResepRWJ.form.Grid.a.startEditing(row, 4);	
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboKodePasienResepRWJ.lastQuery != '' ){
								var value="";
								
								if (value!=cboKodePasienResepRWJ.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
										params: {
											text:cboKodePasienResepRWJ.lastQuery,
										},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboKodePasienResepRWJ.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsKodePasien_ResepRWJ.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsKodePasien_ResepRWJ.add(recs);
											}
											a.expand();
											if(dsKodePasien_ResepRWJ.onShowList != undefined)
												dsKodePasien_ResepRWJ.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboKodePasienResepRWJ.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
		}
	)
	return cboKodePasienResepRWJ;
};


function ShowPesanWarningResepRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function ShowPesanErrorResepRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoResepRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:300
		}
	);
};


function panelnew_window_printer()
{
    win_printer_resep_rwi = new Ext.Window
    (
        {
            id: 'win_printer_resep_rwi',
            title: 'Printer',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_printer_ResepRWJ()],
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkPrintAll_ResepRWJ',
					handler: function()
					{		
						if (Ext.getCmp('cbopasienorder_printer').getValue()===""){
							ShowPesanWarningResepRWJ('Pilih dulu print sebelum cetak', 'Warning');
						}else{
							if(PrintBill == 'true'){
								printbill_resep_rwj();	
								win_printer_resep_rwi.close();
							} else{
								printkwitansi_resep_rwj();
								win_printer_resep_rwi.close();
								panelWindowPrintKwitansi_ResepRWJ.close();
							}
							
						} 
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasswordDulu',
					handler: function()
					{
						win_printer_resep_rwi.close();
					}
				} 
			]

        }
    );

    win_printer_resep_rwi.show();
};

function Itempanel_printer_ResepRWJ()
{
    var panel_printer_ResepRWJ = new Ext.Panel
    (
        {
            id: 'panel_printer_ResepRWJ',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                mCombo_printer_resep_rwj(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return panel_printer_ResepRWJ;
};

function panelPrintKwitansi_resepRWJ()
{
    panelWindowPrintKwitansi_ResepRWJ = new Ext.Window
    (
        {
            id: 'panelWindowPrintKwitansi_ResepRWJ',
            title: 'Print Kwitansi',
            width:440,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_PrintKwitansiResepRWJ()],
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkPrintKwitansi_ResepRWJ',
					handler: function()
					{
						PrintBill='false';
						// panelnew_window_printer();
						printkwitansi_resep_rwj();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelPrintKwitansi_ResepRWJ',
					handler: function()
					{
						panelWindowPrintKwitansi_ResepRWJ.close();
					}
				} 
			]

        }
    );

    panelWindowPrintKwitansi_ResepRWJ.show();
	getDataPrintKwitansiResepRWJ();
};

function Itempanel_PrintKwitansiResepRWJ()
{
    var items=
    (
        {
            id: 'panelItemPrintKwitansiRWJ',
			layout:'form',
			border: true,
			bodyStyle:'padding: 5px',
			height: 215,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 5px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 10,
							xtype: 'textfield',
							id: 'txtKd_pasienPrintKwitansi_viApotekResepRWJ',
							name: 'txtKd_pasienPrintKwitansi_viApotekResepRWJ',
							width: 80,
							readOnly: true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pembayar'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 40,
							xtype: 'textfield',
							id: 'txtNamaPembayarPrintKwitansi_viApotekResepRWJ',
							name: 'txtNamaPembayarPrintKwitansi_viApotekResepRWJ',
							width: 180,
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Untuk Pembayaran'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 70,
							xtype: 'textarea',
							id: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ',
							name: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ',
							width: 250,
							height: 62,
						},
						{
							x: 10,
							y: 140,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 140,
							y: 140,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 140,
							xtype: 'textfield',
							id: 'txtNoResepPrintKwitansi_viApotekResepRWJ',
							name: 'txtNoResepPrintKwitansi_viApotekResepRWJ',
							width: 150,
							listeners:{
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										Ext.getCmp('btnOkPrintKwitansi_ResepRWJ').el.dom.click();
										panelWindowPrintKwitansi_ResepRWJ.close();	
										Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(false,10);
									}
								}
							}
						},
						{
							x: 10,
							y: 170,
							xtype: 'label',
							text: 'Jumlah Bayar (Rp)',
							style:{'font-weight':'bold'},
						},
						{
							x: 140,
							y: 170,
							xtype: 'label',
							text: ':',
							style:{'font-weight':'bold'},
						},
						{
							x: 150,
							y: 170,
							xtype: 'textfield',
							id: 'txtJumlahBayarPrintKwitansi_viApotekResepRWJ',
							name: 'txtJumlahBayarPrintKwitansi_viApotekResepRWJ',
							width: 150,
							style:{'text-align':'right','font-weight':'bold'},
							readOnly:true
						},
					]
				}
				
			]	
        }
    );

    return items;
};

function panelPrintLabelDosisObat_resepRWJ()
{
	
	panelWindowPrintLabelDosisObat_ResepRWJ = new Ext.Window
    (
        {
            id: 'pnlWindowPrintLabelDosisObat_ResepRWJ',
            title: 'List Label Obat',
            width:600,
            height: 550,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				gridListObatResepRWJ(),
				Itempanel_PrintJenisEtiket(),
				gridWaktuDosisObatResepRWJ(),
				Itempanel_PrintLabelDosisObatResepRWJ()
			],
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkPrintLabelDosisObat_ResepRWJ',
					handler: function()
					{
						// CetakLabelObatApotekResepRWJ(function(hasil){
							// var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
							// WinPrint.document.write(hasil);
							// WinPrint.document.close();
							// WinPrint.focus();
							// WinPrint.print();
							// WinPrint.close();
						// });
						cetak_label_obat();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelPrintLabelDosisObat_ResepRWJ',
					handler: function()
					{
						panelWindowPrintLabelDosisObat_ResepRWJ.close();
					}
				} 
			],
			listeners:
			{             
				activate: function()
				{
					
						/*------------------ UDD ---------------------------*/
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_TanggalListLabelObat').hide();
						
						/*------------------ TABLET ---------------------------*/
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
						/*------------------ OBAT LUAR ---------------------------*/
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
						
						
						
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').focus(false,10);
				}
			}
        }
    );

    panelWindowPrintLabelDosisObat_ResepRWJ.show();
	getListObatDosisObat_ResepRWJ();
};

function Itempanel_PrintJenisEtiket(){
	 var items=
    (
        {
            id: 'Itempanel_PrintJenisEtiket',
			layout:'form',
			border: true,
			bodyStyle:'padding: 2px',
			height: 50,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 2px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							id:'lblJenisEtiketListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Jenis Etiket'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						mComboJenisEtiket(),
					]
				}
				
			]	
        }
    );

    return items;
}
function CetakLabelObatApotekResepRWJ(callback){
	// if (ValidasiEntryResepRWJ(nmHeaderSimpanData,false) == 1 )
	// {
		Ext.Ajax.request
		(
			{
				// url: baseURL + "index.php/apotek/functionAPOTEK/CetakLabelObatApotekResepRWJ",
				url: baseURL + "index.php/apotek/functionAPOTEK/CetakLabelObatApotekResepRWJ",
				params: getParamCetakLabelObatResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					//var cst = Ext.decode(o.responseText);
					callback(o.responseText);
					//if (cst.success === true) 
					//{
						// callback();
						// ShowPesanInfoResepRWJ('Resep berhasil dilayani','Information');
						// Ext.get('txtNoResepRWJ_viApotekResepRWJ').dom.value=cst.noresep;
						// Ext.get('txtTmpNoout').dom.value=cst.noout;
						// Ext.get('txtTmpTglout').dom.value=cst.tgl;
						// Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
						// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
						// Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
						// Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
						// refreshRespApotekRWJ();
						
						// if(mBol === false)
						// {
							
						// };
					// }
					// else 
					// {
						// ShowPesanErrorResepRWJ('Resep gagal dilayani', 'Error');
						// refreshRespApotekRWJ();
					// };
				}
			}
			
		)
	// }
}

function getParamCetakLabelObatResepRWJ(){
	var tmp_aturan_minum='';
	var tmp_keterangan='';
	var tmp_catatan1='';
	var tmp_catatan2='';
	var tmp_catatan3='';
	var tmp_catatan4='';
	var tmp_jenis_hari='';
	var tmp_jam='';
	var tmp_tgl_udd='';
	
	if(jenis_etiket == 2 || jenis_etiket == 3 || jenis_etiket == 5){
		tmp_aturan_minum = Ext.get('cboAturanMinumEtiket').getValue();
		//obat tablet
		if(jenis_etiket == 2 || jenis_etiket == 5){
			tmp_catatan1 = Ext.getCmp('txtCatatanListLabelObat').getValue();
			if(jenis_etiket == 5){
				tmp_aturan_minum = Ext.get('txtCaraPakaiListLabelObat').getValue();
			}
		}else{
		//obat sirup
			tmp_catatan1 = Ext.getCmp('txtCatatanListLabelObat_SIRUP').getValue();
			tmp_keterangan = Ext.getCmp('txtKeteranganListLabelObat').getValue();
		}
	}else{
		//obat luar
		if(jenis_etiket == 4){
			tmp_aturan_minum = Ext.get('txtCaraPakaiListLabelObat').getValue();
			tmp_keterangan = Ext.get('cboKeteranganObatLuarEtiket').getValue();
			tmp_catatan1 = Ext.getCmp('txtCatatanListLabelObat').getValue();
			if(Ext.getCmp('cboKeteranganObatLuarEtiket').getValue() == 6){
				tmp_aturan_minum = Ext.get('txtCaraPakaiListLabelObatTAPERINGDOSE').getValue();
				tmp_catatan2 = Ext.getCmp('txtCatatanListLabelObat2').getValue();
				tmp_catatan3 = Ext.getCmp('txtCatatanListLabelObat3').getValue();
				tmp_catatan4 = Ext.getCmp('txtCatatanListLabelObat4').getValue();
			} 
			
		}
		
		if(jenis_etiket == 1){
			tmp_jenis_hari = Ext.get('cboJenisHari').getValue();
			tmp_jam = Ext.get('cboJam').getValue();
			tmp_tgl_udd = Ext.get('dfTglListLabelObat').getValue();
		}
		
	}
	
	// var KdPasien = AptResepRWJ.form.ComboBox.kodePasien.getValue();
	var KdPasien = Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue();
	var NamaPasien ='';
	if (KdPasien == undefined || KdPasien == ''){
		NamaPasien =  Ext.getCmp('txtNamaPasienNon').getValue();
	}  
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		KdPasien:KdPasien,
		NamaPasien:NamaPasien,
		Jenis_obat:jenis_obat,
		Jenis_etiket:jenis_etiket,
		Aturan_minum:tmp_aturan_minum,
		Keterangan:tmp_keterangan,
		Catatan:tmp_catatan1,
		Catatan2:tmp_catatan2,
		Catatan3:tmp_catatan3,
		Catatan4:tmp_catatan4,
		JenisHari:tmp_jenis_hari,
		Jam:tmp_jam,
		TglUDD:tmp_tgl_udd
	} 
	/* params['jml_tampung_ceklis_obat']=jml_tampung_ceklis_obat;
	for(var i = 0 ; i < jml_tampung_ceklis_obat ; i++)
	{
		params['kd_prd-'+i]=tampung_ceklis_obat['kd_prd-'+i]
		
	}
	 */
	//==================== PARAM TAMPUNG LIST OBAT ETIKET JIKA MENGGUNAKAN EVENT KEYBOARD =====================
	
	var jml_grid_obat = dsGridListDosisObat_ResepRWJ.getCount();
	var i_tmp=0;
	for(var i = 0 ; i < jml_grid_obat ; i++)
	{
		if(dsGridListDosisObat_ResepRWJ.data.items[i].data.pilih == 'V'){
			params['kd_prd-'+i_tmp] = dsGridListDosisObat_ResepRWJ.data.items[i].data.kd_prd;
			params['nama_obat-'+i_tmp] = dsGridListDosisObat_ResepRWJ.data.items[i].data.nama_obat.slice(0, 18);
			params['qty_obat-'+i_tmp] = dsGridListDosisObat_ResepRWJ.data.items[i].data.jml_out;
			i_tmp++;
		}
	} 
	params['jml_tampung_ceklis_obat']=i_tmp;
	
	// tampung_waktu_dosis_obat
	params['jumlah_dosis_obat']=dsGridWaktuDosisObat_ResepRWJ.getCount();
	for(var i = 0 ; i < dsGridWaktuDosisObat_ResepRWJ.getCount() ; i++)
	{
		params['kd_waktu-'+i]=dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.kd_waktu;
		params['waktu-'+i]=dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.waktu;
		params['jam-'+i]=dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.jam;
		params['qty-'+i]=dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.qty;
		
		if(dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.jenis_takaran == '' || dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.jenis_takaran == undefined){
			params['jenis_takaran-'+i]=jenis_obat;
		}else{
			params['jenis_takaran-'+i]=dsGridWaktuDosisObat_ResepRWJ.data.items[i].data.jenis_takaran;
		}
		
	}
	return params;
}

function cetak_label_obat(){
	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/CetakLabelObat",
			params: getParamCetakLabelObatResepRWJ(),
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				GridListDosisObat_ResepRWJ.getView().refresh();
				GridListDosisObat_ResepRWJ.startEditing(0, 1);
			}
		}
		
	)
}
function gridListObatResepRWJ(){
	var fldDetail = ['kd_prd','nama_obat','jml_out'];
	dsGridListDosisObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail });

	/* var sm = new Ext.grid.CheckboxSelectionModel({
				dataIndex:'check',
				listeners: {
					selectionchange: function(sm, selected, opts) {
						var SelectedCheckbox=GridListDosisObat_ResepRWJ.getSelectionModel();
						//alert(SelectedCheckbox.selections.length);
						tampung_ceklis_obat={};
						jml_tampung_ceklis_obat = SelectedCheckbox.selections.length;
						if(SelectedCheckbox.selections.length >0){
							for(i=0;i<SelectedCheckbox.selections.length;i++){
								tampung_ceklis_obat['kd_prd-'+i]=( SelectedCheckbox.selections.items[i].data.kd_prd)
								Ext.getCmp('cboJenisEtiket').focus(true, 20);
							}
						}else{
							// dataSource_kamar.removeAll();
						}
					}
				}
			});    
	GridListDosisObat_ResepRWJ = new Ext.grid.GridPanel({
	id				 : 'GridListDosisObat_ResepRWJ',
	store            : dsGridListDosisObat_ResepRWJ,
	autoScroll       : true,
	columnLines      : true,
	border           : true,
	width			 : 589,
	height           : 130,
	stripeRows       : true,
	title            : '',
	anchor           : '100% 100%',
	plugins          : [new Ext.ux.grid.FilterRow()],
	columns         :
						[
							new Ext.grid.RowNumberer(),
							sm,
							{
								dataIndex		: 'kd_prd',
								header			: 'Kode',
								width			: 100,
								menuDisabled	: true,
							},
							{
								dataIndex		: 'nama_obat',
								header			: 'Nama Obat',
								width			: 200,
								menuDisabled	: true,
							},
							{
								dataIndex		: 'jml_out',
								header			: 'Qty',
								align			: 'center',
								width			: 30,
								menuDisabled	: true,
							},
						],
	sm: sm,	
	viewConfig: 
	{
		forceFit: true
	}
	}); */
	/* ============================= PEMILIHAN OBAT MENGGUNAKAN EVENT KEYBOARD =================================== */
	GridListDosisObat_ResepRWJ = new Ext.grid.EditorGridPanel({
		id				 : 'GridListDosisObat_ResepRWJ',
		store            : dsGridListDosisObat_ResepRWJ,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		width			 : 589,
		height           : 130,
		stripeRows       : true,
		title            : '',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		selModel		 : new Ext.grid.CellSelectionModel
							// Tanda aktif saat salah satu baris dipilih # --------------
							({
								singleSelect: true,
								listeners:
								{
									cellselect: function(sm, row, rec)
									{}
								}
							}),
		colModel         :new Ext.grid.ColumnModel(
							[
								new Ext.grid.RowNumberer(),
								// sm, 
								{
									dataIndex		: 'pilih',
									header			: 'Pilih',
									width			: 20,
									menuDisabled	: true,
									align			: 'center',
									editor			: new Ext.form.TextField({
										allowBlank: false,
										enableKeyEvents:true,
										maxLength: 1,
										readOnly:true,
										listeners:{
												keyDown: function(a,b,c){
													if(b.getKey()==32){
														var line	= GridListDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
														var o = dsGridListDosisObat_ResepRWJ.getRange()[line].data;
														console.log(o.pilih);
														if(o.pilih == undefined || o.pilih == ''){
															o.pilih = 'V';
														}else{
															o.pilih = '';
														}
														GridListDosisObat_ResepRWJ.getView().refresh();
														if (line+1 == dsGridListDosisObat_ResepRWJ.getCount()){
															
															GridListDosisObat_ResepRWJ.startEditing(0, 1);
														}else{
															GridListDosisObat_ResepRWJ.startEditing(line+1, 1);
														}
													}
													
													if(b.getKey()==13){
														Ext.getCmp('cboJenisEtiket').focus(true, 100);
													} 
												}
										}
									})
								},
								{
									dataIndex		: 'kd_prd',
									header			: 'Kode',
									width			: 100,
									menuDisabled	: true,
								},
								{
									dataIndex		: 'nama_obat',
									header			: 'Nama Obat',
									width			: 200,
									menuDisabled	: true,
								},
								{
									dataIndex		: 'jml_out',
									header			: 'Qty',
									align			: 'center',
									width			: 30,
									menuDisabled	: true,
								},
							]),
		// sm: sm,	
		listeners	: {
			'keydown' : function(e){
					if(e.getKey() == 13){
						var line	= GridListDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
						GridListDosisObat_ResepRWJ.startEditing(line, 1);
					}
			},
			'cellclick': function(grd, rowIndex, colIndex, e) {
				console.log(grd);
				console.log(rowIndex);
				console.log(colIndex);
				var line	= GridListDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
				var o = dsGridListDosisObat_ResepRWJ.getRange()[line].data;
				console.log(o.pilih);
				if(o.pilih == undefined || o.pilih == ''){
					o.pilih = 'V';
				}else{
					o.pilih = '';
				}
				GridListDosisObat_ResepRWJ.getView().refresh();
				GridListDosisObat_ResepRWJ.startEditing(line, 1);
		    }
		},
		viewConfig: 
		{
			forceFit: true
		}
	});
	return GridListDosisObat_ResepRWJ;
}

function gridWaktuDosisObatResepRWJ(){
	var fldDetail = ['kd_waktu','waktu','jam','qty','jenis_takaran'];
	var fldDetail2 = ['kd_jam','jam'];
	
	// getAturanObat();
	//getJamDosis();
    dsGridWaktuDosisObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail });
	dsGridJamDosisObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail2 });	
	var fldDetail3 = ['kd_jenis_takaran','jenis_takaran'];
	dsGridTakaranDosisObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail3 });
			
    GridWaktuDosisObatColumnModel =  new Ext.grid.ColumnModel([
		{
			dataIndex		: 'kd_waktu',
			header			: 'Waktu',
			width			: 40,
			menuDisabled	: true,
			hidden 			: true
        },
		{
			dataIndex		: 'waktu',
			header			: 'Waktu',
			width			: 40,
			menuDisabled	: true,
			editor			: new Ext.form.TextField({
								allowBlank: false,
								listeners:{
									specialkey: function(){
										if(Ext.EventObject.getKey() == 13){
											// alert("a");
												// var line	= GridWaktuDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
												// GridWaktuDosisObat_ResepRWJ.startEditing(line, 2);
											
										}
									}
								}
							})
        },
		{
			dataIndex		: 'jam',
			header			: 'Jam',
			width			: 40,
			menuDisabled	: true,
			editor: new Ext.form.ComboBox 
			({
				id				: 'gridcbojam_etiket',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				width			: 50,
				anchor			: '95%',
				store			: dsGridJamDosisObat_ResepRWJ,
				valueField		: 'jam',
				displayField	: 'jam',
				listeners		: {
					select	: function(a,b,c){
						
					},
					specialkey: function(){
						if(Ext.EventObject.getKey() == 13){
							// alert("a");
								// var line	= GridWaktuDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
								// GridWaktuDosisObat_ResepRWJ.startEditing(line, 3);
						}
					}
				}
			})
        },
		{
			dataIndex		: 'qty',
			header			: 'Qty',
			align			: 'right',
			width			: 30,
			menuDisabled	: true,
			editor			: new Ext.form.TextField({
								allowBlank: false,
								enableKeyEvents:true,
								listeners:{
									keyDown: function(a,b,c){
										if(b.getKey()==13){
											if (GridWaktuDosisObat_ResepRWJ.getSelectionModel().selection.cell[0] == 3){
												if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3){
													Ext.getCmp('cboAturanMinumEtiket').focus(true, 20);
												}else if(kd_jenis_etiket == 5){
													Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
												}
											}else{
												var line	= GridWaktuDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
												GridWaktuDosisObat_ResepRWJ.startEditing(line+1, 3);
											}
										} 
									}
								}
			})
        },
		{
			dataIndex		: 'jenis_takaran',
			header			: 'Jns.Takaran',
			width			: 80,
			menuDisabled	: true,
			editor			: new Ext.form.ComboBox 
							({
								id				: 'gridcbojenis_takaran_etiket',
								typeAhead		: true,
								triggerAction	: 'all',
								lazyRender		: true,
								mode			: 'local',
								selectOnFocus	: true,
								forceSelection	: true,
								width			: 50,
								anchor			: '95%',
								store			: dsGridTakaranDosisObat_ResepRWJ,
								valueField		: 'jenis_takaran',
								displayField	: 'jenis_takaran',
								value			: jenis_obat,
								listeners		: {
									select	: function(a,b,c){
										
									},
									'specialkey' : function()
									{
										if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
											if (GridWaktuDosisObat_ResepRWJ.getSelectionModel().selection.cell[0] == 3){
												if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3){
													Ext.getCmp('cboAturanMinumEtiket').focus(true, 20);
												}else if(kd_jenis_etiket == 5){
													Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
												}
											}else{
												// var line	= GridWaktuDosisObat_ResepRWJ.getSelectionModel().selection.cell[0];
												// GridWaktuDosisObat_ResepRWJ.startEditing(line+1, 1);
											}
										}			
									}
								}
							}),
			// renderer:function(){
				// Ext.getCmp('gridcbojenis_takaran_etiket').setValue(jenis_obat);
				// return jenis_obat;
				
			// }
			
        }
        ]
		
    )
	
	
	GridWaktuDosisObat_ResepRWJ= new Ext.grid.EditorGridPanel({
		id			: 'GridWaktuDosisObat_ResepRWJ',
		stripeRows	: true,
		width		: 589,
		height		: 110,
        store		: dsGridWaktuDosisObat_ResepRWJ,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridWaktuDosisObatColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				trcellCurrentTindakan_KasirRWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
	return GridWaktuDosisObat_ResepRWJ;
}

function Itempanel_PrintLabelDosisObatResepRWJ()
{
    var items=
    (
        {
            id: 'panelItemPrintLabelObatRWJ',
			layout:'form',
			border: true,
			bodyStyle:'padding: 3px',
			height: 200,
			items:
			
			[
			
				{
					layout: 'absolute',
					bodyStyle: 'padding: 3px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':',
							id:'TD_awal'
						},
						/*-------------- 2 ----------------*/
						{
							id:'lblCaraMinumListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Diminum'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':',
							id:'TD_lblCaraMinumListLabelObat'
						},
						mComboCaraMinum(),
						
						{
							id:'lblJenisHariListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Jenis Hari'
						},
						mComboJenisHari(),
						
						{
							id:'lblKeteranganObatLuarListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Keterangan'
						},
						mComboKeteranganObatLuar(),
						/*-------------- 3 ----------------*/
						{
							id:'lblJamListLabelObat',
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Jam'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						mComboJam(),
						{
							id:'lblCatatanListLabelObat',
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Catatan'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							id:'txtCatatanListLabelObat',
							x: 160,
							y: 40,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										 if(kd_jenis_etiket == 2){
											 Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWJ').el.dom.click();
										 }else if(kd_jenis_etiket == 4){
											
											if(Ext.getCmp('cboKeteranganObatLuarEtiket').getValue()== 6){
												Ext.getCmp('txtCatatanListLabelObat2').focus(false, 20);
												
											}else{
												Ext.getCmp('txtCaraPakaiListLabelObat').focus(false, 20);
											}
										 }else{ 
											Ext.getCmp('txtCaraPakaiListLabelObat').focus(false, 20);
										 }
									}		
								}
							}
						},
						
						{
							id:'lblKeteranganListLabelObat',
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Keterangan'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							id:'txtKeteranganListLabelObat',
							x: 160,
							y: 40,
							width: 350,
							xtype: 'textfield',
							value:'KOCOK DAHULU SEBELUM DIMINUM',
							listeners:{
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										if(kd_jenis_etiket == 3){
											Ext.getCmp('txtCatatanListLabelObat_SIRUP').focus(false, 20);
										}else{
											Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWJ').el.dom.click();
										}
									}		
								}
							}
						},
						/*-------------- 4 ----------------*/
						{
							id:'lblTanggalListLabelObat',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							id:'TD_TanggalListLabelObat',
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 160,
							y: 70,
							xtype: 'datefield',
							id: 'dfTglListLabelObat',
							format: 'd/M/Y',
							width: 100,
							tabIndex:3,
							disabled:false,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWJ').el.dom.click();
									}		
								}
							}
						},
						{
							id:'lblCaraPakaiListLabelObat',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Cara Pakai'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':',
							id:'TD_CaraPakaiListLabelObat',
						},
						{
							id:'txtCaraPakaiListLabelObat',
							x: 160,
							y: 70,
							width: 350,
							xtype: 'textfield',
							listeners:{
								'specialkey' : function()
									{
										 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
											Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWJ').el.dom.click();
											
										}	
									}
								
							}
						},
						/**************CATATAN SYRUP ***********/
						{
							id:'lblCatatanListLabelObat_SIRUP',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Catatan'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':',
							id:'TD_CatatanListLabelObat_SIRUP'
						},
						{
							id:'txtCatatanListLabelObat_SIRUP',
							x: 160,
							y: 70,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										 if(kd_jenis_etiket == 3){
											 Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWJ').el.dom.click();
										 }
									}		
								}
							}
						},
						/********************CATATAN TAPERING DOSE**********************/
						{
							id:'lblCatatanListLabelObat2',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Catatan ke-2'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':',
							id : 'TDCatatanListLabelObat2'
						},
						{
							id:'txtCatatanListLabelObat2',
							x: 160,
							y: 70,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										  Ext.getCmp('txtCatatanListLabelObat3').focus(false, 20);
									}	
									
								}
							}
						},
						{
							id:'lblCatatanListLabelObat3',
							x: 10,
							y: 100,
							xtype: 'label',
							text: 'Catatan ke-3'
						},
						{
							x: 140,
							y: 100,
							xtype: 'label',
							text: ':',
							id : 'TDCatatanListLabelObat3'
						},
						{
							id:'txtCatatanListLabelObat3',
							x: 160,
							y: 100,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										  Ext.getCmp('txtCatatanListLabelObat4').focus(false, 20);
									}	
								}
							}
						},
						{
							id:'lblCatatanListLabelObat4',
							x: 10,
							y: 130,
							xtype: 'label',
							text: 'Catatan ke-4'
						},
						{
							x: 140,
							y: 130,
							xtype: 'label',
							text: ':',
							id : 'TDCatatanListLabelObat4'
						},
						{
							id:'txtCatatanListLabelObat4',
							x: 160,
							y: 130,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
									  Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').focus(false, 20);
									}	
								}
							}
						},
						/********************CARA PAKAI OBAT LUAR TAPERING DOSE**********************/
						{
							id:'lblCaraPakaiListLabelObatTAPERINGDOSE',
							x: 10,
							y: 160,
							xtype: 'label',
							text: 'Cara Pakai'
						},
						{
							x: 140,
							y: 160,
							xtype: 'label',
							text: ':',
							id:'TDCaraPakaiListLabelObatTAPERINGDOSE',
						},
						{
							id:'txtCaraPakaiListLabelObatTAPERINGDOSE',
							x: 160,
							y: 160,
							width: 350,
							xtype: 'textfield',
							listeners:{
								'specialkey' : function()
								{
									if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWJ').el.dom.click();
									}
								}
							}
						},
					]
				}
				
			]	
        }
    );

    return items;
};

function getDataPrintKwitansiResepRWJ(){
	if(Ext.getCmp('cbNonResep').getValue() == false){
		// Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWJ').setValue(AptResepRWJ.form.ComboBox.namaPasien.getValue());		
		Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').getValue());		
	} else{
		Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtNamaPasienNon').getValue());		
	}
	Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('txtNoResepPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getTemplateKwitansi",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(Ext.getCmp('cbNonResep').getValue() == false){						
						Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ').setValue(cst.template +" di "+cst.nm_unit_far);
					} else{
						Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ').setValue(cst.template  +" di "+cst.nm_unit_far);
					}
					 
					Ext.getCmp('txtNoResepPrintKwitansi_viApotekResepRWJ').focus();// .focus();
						
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWJ('Gagal membaca template kwitansi', 'Error');
				};
			}
		}
		
	)
}

function getUnitFar_ResepRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getUnitFar",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					UnitFarAktif_ResepRWJ = cst.kd_unit_far;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWJ('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

function hitungSetengahResep(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var qtygrid=0;
		var subJumlah=0;
		var sisa=0;
		
		
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.jml != undefined || o.jml != ""){
			// Jika qty tidak melebihi stok tersedia
			// if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				// Cek qty ganjil atau tidak
				if((parseInt(o.jml) % 2) == 1){
					sisa = parseInt(o.jml) - 1; //Dikurangi 1 supaya genap
					qtygrid = parseInt(sisa) / 2; //Setelah dikurang lalu di bagi 2
					qtygrid = qtygrid + 1; //Setelah dibagi 2 lalu di tambahkan 1 dari pengurangan menjadi genap sebelumnya
					o.jml = qtygrid;
				} else{
					qtygrid = parseInt(o.jml) / 2;
					o.jml = qtygrid;
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya'){
							if(o.adm_racik != undefined || o.adm_racik != null || o.adm_racik != 0){
								admRacik += parseFloat(o.adm_racik) * 1;
								console.log(admRacik);
							} else{
								admRacik +=0;
								console.log(admRacik);
							}
						} else{
							admRacik +=0;
						}
						
					}
				}
				
				jumlahGrid = ((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			// }else {
				// ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				// o.jml=o.jml_stok_apt;
			// }
			totqty +=parseFloat(o.jml);
			
		}
	}
	//admRacik=parseFloat(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue())// * parseFloat(o.racik);
	admRacik = 0;
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.get('txtDiscEditData_viApotekResepRWJ').dom.value=toFormat(totdisc);
	// admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	admprs = 0;
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.get('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.get('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	AptResepRWJ.form.Grid.a.getView().refresh();
	Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
}

function formulaRacikanResepRWJ(){
	var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
	statusRacikan_ResepRWJ=0;
    setLookUpRacikan_ResepRWJ = new Ext.Window
    ({
        id: 'setLookUpRacikan_ResepRWJ',
		name: 'setLookUpRacikan_ResepRWJ',
        title: 'Formulasi Racikan Obat', 
        closeAction: 'destroy',        
        width: 523,
        height: 260,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
		PanelFormulasiRacikan_ResepRWJ()
			/* getItemPanelBiodataTransfer_viApotekResepRWJ(),
			getItemPanelTotalBayar_ApotekResepRWJ() */
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKRacikan_viApotekResepRWJL',
				handler:function()
				{
					setHasilFormula_ResepRWJ();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalRacikan_viApotekResepRWJL',
				handler:function()
				{
					setLookUpRacikan_ResepRWJ.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
             //   rowSelected_viApotekResepRWJ=undefined;
            }
        }
    });

    setLookUpRacikan_ResepRWJ.show();
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWJ').focus(true,10);
	Ext.getCmp('txtkodeObatRacikan_viApotekResepRWJ').setValue(currentKdPrdRacik_ResepRWJ);
	Ext.getCmp('txtNamaObatRacikan_viApotekResepRWJ').setValue(currentNamaObatRacik_ResepRWJ);
	Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWJ').setValue(currentHargaRacik_ResepRWJ);
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').setValue(currentJumlah_ResepRWJ);
}

function PanelFormulasiRacikan_ResepRWJ(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 195,
		items:
		[
			/* {
				layout: 'column',
				border: false,
				items:
				[ */
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 10px ',
						border: true,
						width: 495,
						height: 40,
						anchor: '100% 23%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nama obat'
							},
							{
								x: 70,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 80,
								y: 10,
								xtype: 'textfield',
								id: 'txtkodeObatRacikan_viApotekResepRWJ',
								name: 'txtkodeObatRacikan_viApotekResepRWJ',
								width: 70,
								readOnly: true
							},
							{
								x: 155,
								y: 10,
								xtype: 'textfield',
								id: 'txtNamaObatRacikan_viApotekResepRWJ',
								name: 'txtNamaObatRacikan_viApotekResepRWJ',
								width: 160,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Harga satuan'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtaHargaObatRacikan_viApotekResepRWJ',
								name: 'txtaHargaObatRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 495,
						height: 1,
						items:
						[
							
							{
								xtype: 'label',
								text: ''
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: true,
						width: 500,
						height: 50,
						anchor: '100% 72%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Dosis persediaan obat farmasi'
							},
							{
								x: 170,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 10,
								xtype: 'numberfield',
								id: 'txtPersediaanObatRacikan_viApotekResepRWJ',
								name: 'txtPersediaanObatRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWJ').focus();
										}
									}
								}
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Dosis resep / permintaan dokter'
							},
							{
								x: 170,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 40,
								xtype: 'numberfield',
								id: 'txtDosisPermintaanDokterRacikan_viApotekResepRWJ',
								name: 'txtDosisPermintaanDokterRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').focus();
										}
									}
								}
							},
							
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Quantity resep racikan'
							},
							{
								x: 170,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 70,
								xtype: 'numberfield',
								id: 'txtQtyResepRacikanRacikan_viApotekResepRWJ',
								name: 'txtQtyResepRacikanRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											HitungRacikanObat_ResepRWJ();
										}
									}
								}
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Quantity obat yang dikeluarkan'
							},
							{
								x: 170,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 100,
								xtype: 'numberfield',
								id: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ',
								name: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Sub Total'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtSubTOtalRacikan_viApotekResepRWJ',
								name: 'txtSubTOtalRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 260,
								y: 75,
								xtype: 'label',
								text: '*) Enter untuk menghitung'
							},
						]
					}
			/* 	]
			}  */
							
		]		
	};
        return items;
}

function HitungRacikanObat_ResepRWJ(){
	var totqty=0;
	var totqtyawal=0;
	var totharga=0;
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWJ').getValue();
	
	totqtyawal = (parseFloat(Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWJ').getValue()) / parseFloat(Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWJ').getValue())) * parseFloat(Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').getValue());
	totqty = Math.ceil(totqtyawal).toFixed(0);
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').setValue(totqty);
	totharga = parseFloat(Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').getValue()) * parseFloat(Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWJ').getValue());
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWJ').setValue(totharga);
	setHasilFormula_ResepRWJ();
	
	
}

function setHasilFormula_ResepRWJ(){
	var o = dsDataGrdJab_viApotekResepRWJ.getRange()[curentIndexsSelection_ResepRWJ].data;
	
	o.jml = Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').getValue();
	o.jumlah = Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWJ').getValue();
	AptResepRWJ.form.Grid.a.getView().refresh();
	AptResepRWJ.form.Grid.a.startEditing(curentIndexsSelection_ResepRWJ, 10);
	setLookUpRacikan_ResepRWJ.close();
	hasilJumlah();
	statusRacikan_ResepRWJ=0;
	var records = new Array();
	records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
	dsDataGrdJab_viApotekResepRWJ.add(records);
	var nextRow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
	AptResepRWJ.form.Grid.a.startEditing(nextRow, 4);
}

function formulacitoResepRWJ(){
	var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpCito_ResepRWJ = new Ext.Window
    ({
        id: 'setLookUpCito_ResepRWJ',
		name: 'setLookUpCito_ResepRWJ',
        title: 'Formulasi Racikan Obat', 
        closeAction: 'destroy',        
        width: 543,
        height: 290,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			PanelFormulasiCito_ResepRWJ()
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKCito_viApotekResepRWJL',
				handler:function()
				{
					var line = AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
					o.harga_jual=currentCitoTarifBaru;
					o.nilai_cito=Ext.getCmp('txtaPersentaseCito_viApotekResepRWJ').getValue();
					o.hargaaslicito=Ext.getCmp('txtaHargaBeliCito_viApotekResepRWJ').getValue();
					hasilJumlah();
					setLookUpCito_ResepRWJ.close();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalRacikan_viApotekResepRWJL',
				handler:function()
				{
					setLookUpCito_ResepRWJ.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();

            },
            deactivate: function()
            {
				// shortcut.remove('lookupcito');
            },
			close: function (){
				// shortcut.remove('lookupcito');
			},
        }
    });

    setLookUpCito_ResepRWJ.show();
	Ext.getCmp('txtProdukCito_viApotekResepRWJ').setValue(currentCitoNamaObat);
	Ext.getCmp('txtaHargaBeliCito_viApotekResepRWJ').setValue(currentHargaJualObat);
	Ext.getCmp('txtaPersentaseCito_viApotekResepRWJ').setValue("50");
	// viewGridCito_ResepRWJ();
	// shortcut.set({
		// code:'lookupcito',
		// list:[
			// {
				// key:'ctrl+d',
				// fn:function(){
					// Ext.getCmp('btnDeleteComponent_ResepRWJ').el.dom.click();
				// }
			// },
			// {
				// key:'esc',
				// fn:function(){
					// setLookUpCito_ResepRWJ.close();
				// }
			// }
		// ]
	// });
}

function PanelFormulasiCito_ResepRWJ(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 225,
		items:
		[
			{
				
				layout: 'absolute',
				bodyStyle: 'padding: 10px ',
				border: true,
				width: 525,
				height: 110,
				anchor: '100% 40%',
				items:
				[
					
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Produk'
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					{
						x: 120,
						y: 10,
						xtype: 'textfield',
						id: 'txtProdukCito_viApotekResepRWJ',
						name: 'txtProdukCito_viApotekResepRWJ',
						width: 240,
						readOnly: true
					},
					{
						x: 368,
						y: 10,
						xtype: 'label',
						text: 'Harga beli'
					},
					{
						x: 420,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					{
						x: 430,
						y: 10,
						xtype: 'numberfield',
						id: 'txtaHargaBeliCito_viApotekResepRWJ',
						name: 'txtaHargaBeliCito_viApotekResepRWJ',
						style:{'text-align':'right'},
						width: 75,
						readOnly: true
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Persentasi kenaikan'
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ':'
					},
					{
						x: 120,
						y: 40,
						xtype: 'numberfield',
						id: 'txtaPersentaseCito_viApotekResepRWJ',
						name: 'txtaPersentaseCito_viApotekResepRWJ',
						style:{'text-align':'right'},
						width: 75,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									hitungpersentasecito_ResepRWJ();
									// if(currentCitoTarifLama==undefined || currentCitoTarifLama==''){
										// ShowPesanWarningResepRWJ("Pilih komponent yang akan di cito!","Warning");
									// } else{										
										// var line	= gridPanelFormulasiCito_ResepRWJ.getSelectionModel().selection.cell[0];
										// var o = dsDataGridFormulasiCito_ResepRWJ.getRange()[line].data;										
										// o.tarif_baru=parseFloat(o.tarif_lama) + ((parseFloat(o.tarif_lama)*parseFloat(Ext.getCmp('txtaPersentaseCito_viApotekResepRWJ').getValue()))/100);
										// currentCitoTarifBaru=o.tarif_baru;
										// gridPanelFormulasiCito_ResepRWJ.getView().refresh();
									// }
								}
							}
						}
					},
					{
						x: 200,
						y: 45,
						xtype: 'label',
						text: '%'
					},{
						x: 120,
						y: 60,
						xtype: 'label',
						style:{'font-size':'9px'},
						text: '*) enter untuk hitung'
					},
				]
			},
			gridFormulasiCito_ResepRWJ()
							
		]		
	};
        return items;
}
function gridFormulasiCito_ResepRWJ(){
    var FieldFormulasiCito_ResepRWJ = ['kd_prd'];
    dsDataGridFormulasiCito_ResepRWJ= new WebApp.DataStore({
        fields: FieldFormulasiCito_ResepRWJ
    });
	var Fieldcomponent = ['kd_component','component','tarif_lama','tarif_baru'];
	dsgridcombocomponentcito_ResepRWJ = new WebApp.DataStore({ fields: Fieldcomponent });
	
	loaddatagridcombocito_ResepRWJ();
    
    gridPanelFormulasiCito_ResepRWJ =new Ext.grid.EditorGridPanel({
        store: dsDataGridFormulasiCito_ResepRWJ,
        height: 130,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					cellSelectedGridCito_ResepRWJ = undefined;
					cellSelectedGridCito_ResepRWJ = dsDataGridFormulasiCito_ResepRWJ.getAt(row);
					console.log(cellSelectedGridCito_ResepRWJ.data)
					currentCitoTarifLama=cellSelectedGridCito_ResepRWJ.data.tarif_lama;
                },
            }
        }),
		tbar:
		[
			{
				text	: 'Tambah component',
				id		: 'btnAddComponent_ResepRWJ',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGridFormulasiCito_ResepRWJ.recordType());
					dsDataGridFormulasiCito_ResepRWJ.add(records);
					var row =dsDataGridFormulasiCito_ResepRWJ.getCount()-1;
					gridPanelFormulasiCito_ResepRWJ.startEditing(row, 2);
				}
			},
			{
				text	: 'Delete',
				id		: 'btnDeleteComponent_ResepRWJ',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var line = gridPanelFormulasiCito_ResepRWJ.getSelectionModel().selection.cell[0];
					dsDataGridFormulasiCito_ResepRWJ.removeAt(line);
					gridPanelFormulasiCito_ResepRWJ.getView().refresh();
				}
			}
		],
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{			
				dataIndex: 'kd_component',
				header: 'kd_component',
				hidden: true,
				width: 50
			},
			// {			
				// dataIndex: 'component',
				// header: 'Komponent',
				// sortable: true,
				// width: 150
			// },
			{
				header: 'Komponent',
				dataIndex: 'component',
				hideable:false,
				menuDisabled: true,
				width: 90,
				editor		: gridcbocomponentcito_ResepRWJ= new Ext.form.ComboBox({
						id				: 'gridcbocomponentcito_ResepRWJ',
						typeAhead		: true,
						triggerAction	: 'all',
						lazyRender		: true,
						mode			: 'local',
						emptyText		: '',
						fieldLabel		: ' ',
						align			: 'Right',
						width			: 200,
						store			: dsgridcombocomponentcito_ResepRWJ,
						valueField		: 'component',
						displayField	: 'component',
						listeners		:
						{
							select	: function(a,b,c){
								var line	= gridPanelFormulasiCito_ResepRWJ.getSelectionModel().selection.cell[0];
								dsDataGridFormulasiCito_ResepRWJ.getRange()[line].data.component=b.data.component;
								dsDataGridFormulasiCito_ResepRWJ.getRange()[line].data.kd_component=b.data.kd_component;
								dsDataGridFormulasiCito_ResepRWJ.getRange()[line].data.tarif_lama=b.data.tarif_lama;
								dsDataGridFormulasiCito_ResepRWJ.getRange()[line].data.tarif_baru=b.data.tarif_baru;
								gridPanelFormulasiCito_ResepRWJ.getView().refresh();
								
								var records = new Array();
								records.push(new dsDataGridFormulasiCito_ResepRWJ.recordType());
								dsDataGridFormulasiCito_ResepRWJ.add(records);
								var row =dsDataGridFormulasiCito_ResepRWJ.getCount()-1;
								gridPanelFormulasiCito_ResepRWJ.startEditing(row, 2);
							},
							keyUp: function(a,b,c){
								$this1=this;
								if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
									clearTimeout(this.time);

									this.time=setTimeout(function(){
										if($this1.lastQuery != '' ){
											var value=$this1.lastQuery;
											var param={};
											param['text']=$this1.lastQuery;
											loaddatagridcombocito_ResepRWJ(param);
										}
									},1000);
								}
							}
						}
					}
				)
			},
			{
				dataIndex: 'tarif_lama',
				header: 'Tarif Lama',
				xtype: 'numbercolumn',
				sortable: true,
				width: 50,
				align:'right'
			},
			{
				dataIndex: 'tarif_baru',
				header: 'Tarif Baru',
				xtype: 'numbercolumn',
				sortable: true,
				width: 50,
				align:'right'
			},
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return gridPanelFormulasiCito_ResepRWJ;
}
function viewGridCito_ResepRWJ(){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEK/viewkomponentcito",
		params: {
			kd_unit:Ext.getCmp('txtTmpKdUnit').getValue(),
			tarif:currentHargaJualObat
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsDataGridFormulasiCito_ResepRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDataGridFormulasiCito_ResepRWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDataGridFormulasiCito_ResepRWJ.add(recs);
				gridPanelFormulasiCito_ResepRWJ.getView().refresh();
			} else {
				ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
			};
		}
	});
}
function loaddatagridcombocito_ResepRWJ(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/viewkomponentcito",
		params: {
			kd_unit:Ext.getCmp('txtTmpKdUnit').getValue(),
			tarif:currentHargaJualObat
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbocomponentcito_ResepRWJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombocomponentcito_ResepRWJ.recordType;
				var o=cst['ListDataObj'][i];

				recs.push(new recType(o));
				dsgridcombocomponentcito_ResepRWJ.add(recs);
				//console.log(o);
			}
		}
	});
}

function hitungpersentasecito_ResepRWJ(){
	for(var i=0; i < dsDataGridFormulasiCito_ResepRWJ.getCount() ; i++){
		var tarifbaru=0;
		var o=dsDataGridFormulasiCito_ResepRWJ.getRange()[i].data;
		if(o.component == '' || o.component == undefined){
			dsDataGridFormulasiCito_ResepRWJ.removeAt(i);
		} else{
			if(o.tarif_lama != undefined || o.tarif_lama != ''){
				o.tarif_baru=parseFloat(o.tarif_lama) + ((parseFloat(o.tarif_lama)*parseFloat(Ext.getCmp('txtaPersentaseCito_viApotekResepRWJ').getValue()))/100);
				currentCitoTarifBaru += o.tarif_baru;
			} else{
				ShowPesanWarningResepRWJ("Tarif masih kosong!","Warning");
			}
		}
	}
	gridPanelFormulasiCito_ResepRWJ.getView().refresh(); 
}

function getListObatDosisObat_ResepRWJ(){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEK/getListDosisObat",
		params: {
			no_out:Ext.getCmp('txtTmpNoout').getValue(),
			tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWJ('Error list dosis obat. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListDosisObat_ResepRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGridListDosisObat_ResepRWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGridListDosisObat_ResepRWJ.add(recs);
				GridListDosisObat_ResepRWJ.getView().refresh();
				GridListDosisObat_ResepRWJ.startEditing(0, 1);
			} else {
				ShowPesanErrorResepRWJ('Gagal membaca data list dosis obat', 'Error');
			};
		}
	});
}

function mComboJenisEtiket()
{
	var Field = ['id_etiket','jenis_etiket'];
	ds_jenis_etiket = new WebApp.DataStore({ fields: Field });
	getJenisEtiket();
	
	var fldDetail3 = ['kd_jenis_takaran','jenis_takaran'];
	dsGridTakaranDosisObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail3 });
	console.log(dsGridTakaranDosisObat_ResepRWJ);
	
	var cboJenisEtiket = new Ext.form.ComboBox
	(
		{
			id:'cboJenisEtiket',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_jenis_etiket,
			valueField: 'id_etiket',
			displayField: 'jenis_etiket',
			value:'Etiket Tablet',
			listeners:
			{
				'select': function(a,b,c)
				{
					kd_jenis_etiket = b.data.id_etiket;
					jenis_etiket =kd_jenis_etiket;
					dsGridTakaranDosisObat_ResepRWJ.removeAll();
					// dsGridWaktuDosisObat_ResepRWJ.removeAll();
					getTakaran(kd_jenis_etiket);
					getAturanObat(kd_jenis_etiket);
					GridWaktuDosisObat_ResepRWJ.getView().refresh();
					// alert ("a");
					//alert(b.data.id_etiket);
					if(b.data.id_etiket == 1 || b.data.id_etiket == 4 ){
						// GridWaktuDosisObat_ResepRWJ.hide();
						GridWaktuDosisObat_ResepRWJ.setDisabled(true);
					}else{
						GridWaktuDosisObat_ResepRWJ.setDisabled(false);
					}
					
					
					/*----------- Etiket UDD -------------*/
					if(kd_jenis_etiket == 1){
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('lblCatatanListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat').hide(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						Ext.getCmp('TD_TanggalListLabelObat').show();
						Ext.getCmp('TD_lblCaraMinumListLabelObat').show();
						Ext.getCmp('lblJenisHariListLabelObat').show();
						Ext.getCmp('cboJenisHari').show();
						Ext.getCmp('lblJamListLabelObat').show();
						Ext.getCmp('cboJam').show();
						Ext.getCmp('lblTanggalListLabelObat').show();
						Ext.getCmp('dfTglListLabelObat').show();
						jenis_obat='';
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
						
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
					}
					
					/*----------- Etiket TABLET -------------*/
					if(kd_jenis_etiket == 2){
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('lblCatatanListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat').hide(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
					
						Ext.getCmp('TD_lblCaraMinumListLabelObat').show();
						Ext.getCmp('TD_TanggalListLabelObat').hide();
						Ext.getCmp('lblCaraMinumListLabelObat').show();
						Ext.getCmp('cboAturanMinumEtiket').show();
						Ext.getCmp('lblCatatanListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat').show();
						jenis_obat='tab';
						Ext.getCmp('gridcbojenis_takaran_etiket').setValue(jenis_obat);
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
						
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
					}
					
						/*----------- Etiket Sirup --------------*/
					if(kd_jenis_etiket == 3){
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('lblCatatanListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat').hide(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').hide();
						Ext.getCmp('TD_TanggalListLabelObat').hide();
						Ext.getCmp('lblCaraMinumListLabelObat').show();
						Ext.getCmp('cboAturanMinumEtiket').show();
						Ext.getCmp('lblKeteranganListLabelObat').show();
						Ext.getCmp('txtKeteranganListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').show();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').show();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').show();
						jenis_obat='Sendok Teh';
						
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
					}
						/*----------- Etiket Obat Luar -------------*/
					if(kd_jenis_etiket == 4){
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').show();
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').show();
						Ext.getCmp('cboKeteranganObatLuarEtiket').show();
						Ext.getCmp('lblCatatanListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat').show(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('txtCaraPakaiListLabelObat').setValue('');
						jenis_obat='';
						
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
						
						
					}
						/*----------- Etiket Racikan -------------*/
					if(kd_jenis_etiket == 5){
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').show();
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						Ext.getCmp('TD_awal').hide();
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('TD_lblCaraMinumListLabelObat').hide();
						Ext.getCmp('lblCatatanListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat').show(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').setValue('');
						jenis_obat='tab';
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
						
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
					}
					
					//etiket udd
					if(kd_jenis_etiket == 1){
						Ext.getCmp('cboJenisHari').focus(true, 20);
					}
					//etiket tablet
					if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3 || kd_jenis_etiket == 5){
						setTimeout(
							function(){ 
								GridWaktuDosisObat_ResepRWJ.startEditing(0, 3); 
							}, 500); 
					}
					if(kd_jenis_etiket == 4){
						Ext.getCmp('cboKeteranganObatLuarEtiket').focus(true, 20);
					}
				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						//etiket udd
						if(kd_jenis_etiket == 1){
							Ext.getCmp('cboJenisHari').focus(true, 20);
						}
						//etiket tablet
						if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3 || kd_jenis_etiket == 5){
							setTimeout(
								function(){ 
									GridWaktuDosisObat_ResepRWJ.startEditing(0, 3); 
								}, 500); 
						}
						if(kd_jenis_etiket == 4){
							Ext.getCmp('cboKeteranganObatLuarEtiket').focus(true, 20);
						}
					}			
				}
			}
		}
	);
	return cboJenisEtiket;
};
function mComboJenisHari()
{
	var Field = ['kd_waktu','waktu'];

	ds_waktu_etiket = new WebApp.DataStore({ fields: Field });
	getWaktuEtiket();
	var cboJenisHari = new Ext.form.ComboBox
		(
			{
				id:'cboJenisHari',
				x: 160,
				y: 10,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				width: 350,
				editable: false,
				emptyText:'',
				//fieldLabel: 'JENIS',
				tabIndex:5,
				store: ds_waktu_etiket ,
				valueField: 'kd_waktu',
				displayField: 'waktu',
				value:'Pagi',
				listeners:
				{
					'select': function(a,b,c)
					{
						if(b.data.kd_waktu == 1){
							Ext.getCmp('cboJam').setValue("07.00");
						}else if(b.data.kd_waktu == 2){
							Ext.getCmp('cboJam').setValue("13.00");
						}else if(b.data.kd_waktu == 3){
							Ext.getCmp('cboJam').setValue("16.00");
						}else{
							Ext.getCmp('cboJam').setValue("24.00");
						}
						getJamEtiket(b.data.kd_waktu);

					},
					'specialkey' : function()
					{
						if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
							Ext.getCmp('cboJam').focus(true, 20);
						}					
					}
				}
			}
		);
		return cboJenisHari;
};
function mComboJam()
{
	var Field = ['kd_jam','jam'];

	ds_jam_etiket = new WebApp.DataStore({ fields: Field });
	
  var cboJam = new Ext.form.ComboBox
	(
		{
			id:'cboJam',
			x: 160,
			y: 40,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_jam_etiket,
			valueField: 'kd_jam',
			displayField: 'jam',
			value:'07.00',
			listeners:
			{
				'select': function(a,b,c)
				{
					

				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
							Ext.getCmp('dfTglListLabelObat').focus(true, 20);
					}			
				}
			}
		}
	);
	return cboJam;
};

function getJenisEtiket(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getJenisEtiket",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_jenis_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_jenis_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function getWaktuEtiket(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getWaktuEtiket",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_waktu_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_waktu_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function getJamEtiket(kd_waktu){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getJamEtiket",
		params: {kd_waktu:kd_waktu},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			 ds_jam_etiket.removeAll();
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_jam_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_jam_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function  getJamAturanObat(kd_waktu){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getJamEtiket",
		params: {kd_waktu:kd_waktu},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   dsGridJamDosisObat_ResepRWJ.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				dsGridJamDosisObat_ResepRWJ.add(recs);
				console.log(o);
			}
		}
	});
}

function getAturanObat(kd_jenis_etiket){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getAturanObat",
		params: {kd_jenis_etiket:kd_jenis_etiket},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			dsGridWaktuDosisObat_ResepRWJ.removeAll();
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   dsGridWaktuDosisObat_ResepRWJ.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				dsGridWaktuDosisObat_ResepRWJ.add(recs);
				getJamAturanObat(o.kd_waktu);
				dsGridJamDosisObat_ResepRWJ.removeAll();
			}
		}
	});
}
function mComboCaraMinum(){
	var Field = ['kd_aturan_minum','aturan_minum'];

	ds_cara_minum_etiket = new WebApp.DataStore({ fields: Field });
	getAturanMinumEtiket();
	
  var cboAturanMinumEtiket= new Ext.form.ComboBox
	(
		{
			id:'cboAturanMinumEtiket',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'Sebelum Makan',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_cara_minum_etiket,
			valueField: 'kd_aturan_minum',
			displayField: 'aturan_minum',
			value:'Sebelum Makan',
			listeners:
			{
				'select': function(a,b,c)
				{
					

				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						if(kd_jenis_etiket == 2){
							Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
						}else if(kd_jenis_etiket == 3){
							Ext.getCmp('txtKeteranganListLabelObat').focus(true, 20);
						}
						
					}			
				}
			}
		}
	);
	return cboAturanMinumEtiket;
}

function getAturanMinumEtiket(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getAturanMinumEtiket",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_cara_minum_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_cara_minum_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboKeteranganObatLuar(){
	var Field = ['kd_ket','ket_obat_luar'];

	ds_KeteranganObatLuar_etiket = new WebApp.DataStore({ fields: Field });
	getKeteranganObatLuar();
	
  var cboKeteranganObatLuarEtiket= new Ext.form.ComboBox
	(
		{
			id:'cboKeteranganObatLuarEtiket',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_KeteranganObatLuar_etiket,
			valueField: 'kd_ket',
			displayField: 'ket_obat_luar',
			value:'Obat Luar',
			listeners:
			{
				'select': function(a,b,c)
				{
					if(b.data.kd_ket == 6){
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblCatatanListLabelObat2').show();
						Ext.getCmp('TDCatatanListLabelObat2').show();
						Ext.getCmp('txtCatatanListLabelObat2').show();
						
						Ext.getCmp('lblCatatanListLabelObat3').show();
						Ext.getCmp('TDCatatanListLabelObat3').show();
						Ext.getCmp('txtCatatanListLabelObat3').show();
						
						Ext.getCmp('lblCatatanListLabelObat4').show();
						Ext.getCmp('TDCatatanListLabelObat4').show();
						Ext.getCmp('txtCatatanListLabelObat4').show();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').show();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').show();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').show();
					}else{
						Ext.getCmp('lblCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').show(); 
						Ext.getCmp('lblCatatanListLabelObat2').hide();
						Ext.getCmp('TDCatatanListLabelObat2').hide();
						Ext.getCmp('txtCatatanListLabelObat2').hide();
						
						Ext.getCmp('lblCatatanListLabelObat3').hide();
						Ext.getCmp('TDCatatanListLabelObat3').hide();
						Ext.getCmp('txtCatatanListLabelObat3').hide();
						
						Ext.getCmp('lblCatatanListLabelObat4').hide();
						Ext.getCmp('TDCatatanListLabelObat4').hide();
						Ext.getCmp('txtCatatanListLabelObat4').hide();
						
						Ext.getCmp('lblCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('TDCaraPakaiListLabelObatTAPERINGDOSE').hide();
						Ext.getCmp('txtCaraPakaiListLabelObatTAPERINGDOSE').hide();
					}
				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
					}	
				}
			}
		}
	);
	return cboKeteranganObatLuarEtiket;
}

function getKeteranganObatLuar(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getKeteranganObatLuar",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_KeteranganObatLuar_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_KeteranganObatLuar_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function getTakaran(kd_jenis_etiket){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getTakaran",
		params: {kd_jenis_etiket:kd_jenis_etiket},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			Ext.getCmp('gridcbojenis_takaran_etiket').getStore().removeAll();
			// dsGridTakaranDosisObat_ResepRWJ.removeAll();
			// console.log(dsGridTakaranDosisObat_ResepRWJ.getRange());
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   Ext.getCmp('gridcbojenis_takaran_etiket').getStore().recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				Ext.getCmp('gridcbojenis_takaran_etiket').getStore().add(recs);
				//alert('a');
				
			}
				console.log(dsGridTakaranDosisObat_ResepRWJ.getRange());
				GridWaktuDosisObat_ResepRWJ.getView().refresh();
				// Ext.getCmp('gridcbojenis_takaran_etiket').getView().refresh();
		}
	});
} 

function getGridDetailObatApotekResepRWJ(no_out,tgl_out){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/readGridDetailObat",
			params: {
				no_out:no_out,
				tgl_out:tgl_out
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					dsDataGrdJab_viApotekResepRWJ.removeAll();
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
					// Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(admracik);
					hasilJumlahLoad();
				}
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function nonaktiv(parms){
	Ext.getCmp('txtCatatanResepRWJ_viResepRWJ').setReadOnly(parms);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setReadOnly(parms);
	Ext.getCmp('cbo_UnitResepRWJLookup').setReadOnly(parms);
	Ext.getCmp('cbo_DokterApotekResepRWJ').setReadOnly(parms);
	Ext.getCmp('txtNamaPasienNon').setReadOnly(parms);
	// AptResepRWJ.form.ComboBox.kodePasien.setReadOnly(parms);
	Ext.getCmp('txtKdPasienResepRWJ_viApotekResepRWJ').setReadOnly(parms);
	// AptResepRWJ.form.ComboBox.namaPasien.setReadOnly(parms);
	Ext.getCmp('txtNamaPasienResepRWJ_viApotekResepRWJ').setReadOnly(parms);
	// Ext.getCmp('btnAddObat').disable(parms);
	if(parms == true){
		Ext.getCmp('cbNonResep').disable();
		AptResepRWJ.form.Grid.a.disable();
	} else{
		Ext.getCmp('cbNonResep').enable();
		AptResepRWJ.form.Grid.a.enable();
	}
	
}


function LookUpSearchListGetObat_resepRWJ(nama_obat)
{
	
	WindowLookUpSearchListGetObat_ResepRWJ = new Ext.Window
    (
        {
            id: 'pnlLookUpSearchListGetObat_ResepRWJ',
            title: 'List Pencarian Obat',
            width:620,
            height: 250,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				ItempanelListPencarianObatKepemilikan_ResepRWJ(nama_obat),
				gridListObatPencarianResepRWJ()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					if(FocusExitResepRWJ == false){
						var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
						AptResepRWJ.form.Grid.a.startEditing(line, 4);	
					}
				}
			}
        }
    );

    WindowLookUpSearchListGetObat_ResepRWJ.show();
	getListObatSearch_ResepRWJ(nama_obat,'');
};

function gridListObatPencarianResepRWJ(){
	var fldDetail = ['kd_prd','nama_obat','satuan','sub_jenis','jml_stok_apt','harga_jual','milik'];
	dsGridListPencarianObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail });
	
    GridListPencarianObatColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'kd_prd',
			header			: 'Kode',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_obat',
			header			: 'Nama Obat',
			width			: 180,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'satuan',
			header			: 'Satuan',
			width			: 60,
			menuDisabled	: true,
        },{
			dataIndex		: 'sub_jenis',
			header			: 'Sub Jenis',
			width			: 100,
			menuDisabled	: true,
        },{
			dataIndex		: 'jml_stok_apt',
			header			: 'Stok',
			width			: 50,
			align			: 'right',
			menuDisabled	: true,
        },
		{
			dataIndex		: 'harga_jual',
			header			: 'Harga',
			xtype			: 'numbercolumn',
			align			: 'right',
			format 			: '0,000',
			width			: 60,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'milik',
			header			: 'Kepemilikan',
			width			: 80,
			menuDisabled	: true,
        }
	]);
	
	
	GridListPencarianObat_ResepRWJ= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianObat_ResepRWJ',
		stripeRows	: true,
		width		: 610,
		height		: 170,
        store		: dsGridListPencarianObat_ResepRWJ,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListPencarianObatColumnModel,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionPencarianObatResepRWJ = undefined;
					currentRowSelectionPencarianObatResepRWJ = dsGridListPencarianObat_ResepRWJ.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				// trcellCurrentTindakan_KasirRWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				alert()
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
					console.log(dsDataGrdJab_viApotekResepRWJ.getRange()[line].data);
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.cito="Tidak";
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.nama_obat=currentRowSelectionPencarianObatResepRWJ.data.nama_obat;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_prd=currentRowSelectionPencarianObatResepRWJ.data.kd_prd;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_satuan=currentRowSelectionPencarianObatResepRWJ.data.kd_satuan;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.fractions=currentRowSelectionPencarianObatResepRWJ.data.fractions;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_jual=currentRowSelectionPencarianObatResepRWJ.data.harga_jual;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_beli=currentRowSelectionPencarianObatResepRWJ.data.harga_beli;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_pabrik=currentRowSelectionPencarianObatResepRWJ.data.kd_pabrik;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.markup=currentRowSelectionPencarianObatResepRWJ.data.markup;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.adm_racik=currentRowSelectionPencarianObatResepRWJ.data.adm_racik;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jasa=currentRowSelectionPencarianObatResepRWJ.data.jasa;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out=currentRowSelectionPencarianObatResepRWJ.data.no_out;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_urut=currentRowSelectionPencarianObatResepRWJ.data.no_urut;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.tgl_out=currentRowSelectionPencarianObatResepRWJ.data.tgl_out;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_milik=currentRowSelectionPencarianObatResepRWJ.data.kd_milik;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jml_stok_apt=currentRowSelectionPencarianObatResepRWJ.data.jml_stok_apt;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.milik=currentRowSelectionPencarianObatResepRWJ.data.milik;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.hargaaslicito=currentRowSelectionPencarianObatResepRWJ.data.hargaaslicito;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.nilai_cito=0;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=0;
					dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.racik="Tidak";
					currentHargaJualObat=currentRowSelectionPencarianObatResepRWJ.data.harga_jual;
				
					AptResepRWJ.form.Grid.a.getView().refresh();
					
					AptResepRWJ.form.Grid.a.startEditing(line, 6);	
					
					if(currentRowSelectionPencarianObatResepRWJ.data.jml_stok_apt <= 10){
						Ext.Msg.show({
								title: 'Information',
								msg: 'Stok obat hampir habis, jumlah stok tersedia adalah '+currentRowSelectionPencarianObatResepRWJ.data.jml_stok_apt,
								buttons: Ext.MessageBox.OK,
								fn: function (btn) {
									if (btn == 'ok')
									{
										AptResepRWJ.form.Grid.a.startEditing(line, 6);
									}
								}
						});
					}
					FocusExitResepRWJ = true;
					WindowLookUpSearchListGetObat_ResepRWJ.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListPencarianObat_ResepRWJ;
}

function ItempanelListPencarianObatKepemilikan_ResepRWJ(nama_obat){
	 var items=
    (
        {
            id: 'panelListPencarianObatKepemilikan_ResepRWJ',
			layout:'form',
			border: true,
			bodyStyle:'padding: 2px',
			height: 50,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 2px ',
					border: true,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Kepemilikan'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						ComboKepemilikanPencarianObat_ResepRWJ(nama_obat),
					]
				}
				
			]	
        }
    );

    return items;
}
function ComboKepemilikanPencarianObat_ResepRWJ(nama_obat)
{
	var Field = ['kd_milik','milik'];
	ds_kepemilikan_pencarianobat_reseprwj = new WebApp.DataStore({ fields: Field });
	getKepemilikanPencarianObat();
	
	var cboKepemilikanPencarianObat = new Ext.form.ComboBox
	(
		{
			id:'cbKepemilikanPencarianObat',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			// editable: false,
			tabIndex:5,
			store: ds_kepemilikan_pencarianobat_reseprwj,
			valueField: 'kd_milik',
			displayField: 'milik',
			value:'SEMUA KEPEMILIKAN',
			listeners:
			{
				'select': function(a,b,c)
				{
					PencarianLookupResep = false; // Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
					getListObatSearch_ResepRWJ(nama_obat,b.data.kd_milik);
				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
					}			
				}
			}
		}
	);
	return cboKepemilikanPencarianObat;
};

function getListObatSearch_ResepRWJ(nama_obat,kd_milik){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEK/getListObat",
		params: {
			nama_obat:nama_obat,
			kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue(),
			kd_milik:kd_milik
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWJ('Error menampilkan pencarian obat. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListPencarianObat_ResepRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					if(PencarianLookupResep == true){						
						Ext.Msg.show({
							title: 'Information',
							msg: 'Tidak ada obat yang sesuai atau kriteria obat kurang!',
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
									AptResepRWJ.form.Grid.a.startEditing(line, 4);
									WindowLookUpSearchListGetObat_ResepRWJ.close();
								}
							}
						});
					}
				} else{
					var recs=[],
						recType=dsGridListPencarianObat_ResepRWJ.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListPencarianObat_ResepRWJ.add(recs);
					GridListPencarianObat_ResepRWJ.getView().refresh();
					GridListPencarianObat_ResepRWJ.getSelectionModel().selectRow(0);
					GridListPencarianObat_ResepRWJ.getView().focusRow(0);
				}
				
			} else {
				ShowPesanErrorResepRWJ('Gagal membaca data list pencarian obat', 'Error');
			};
		}
	});
}

function getKepemilikanPencarianObat(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getKepemilikan",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_kepemilikan_pencarianobat_reseprwj.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_kepemilikan_pencarianobat_reseprwj.add(recs);
			}
		}
	});
}

}
TRApotekResepRWJ();

shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambah_viApotekResepRWJ').el.dom.click();
			}
		},
	]
})


