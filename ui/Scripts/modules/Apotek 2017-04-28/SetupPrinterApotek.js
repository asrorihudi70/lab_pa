var dataSource_viSetupPrinterFarmasi;
var selectCount_viSetupPrinterFarmasi=50;
var NamaForm_viSetupPrinterFarmasi="Setup Printer Farmasi";
var mod_name_viSetupPrinterFarmasi="Setup Printer Farmasi";
var now_viSetupPrinterFarmasi= new Date();
var rowSelected_viSetupPrinterFarmasi;
var setLookUps_viSetupPrinterFarmasi;
var tanggal = now_viSetupPrinterFarmasi.format("d/M/Y");
var jam = now_viSetupPrinterFarmasi.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupPrinterFarmasi;
var cboPrinter_SetupPrinterFarmasi;
var cboUnitFar_SetupPrinterFarmasi;

var CurrentData_viSetupPrinterFarmasi =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupPrinterFarmasi={};
SetupPrinterFarmasi.form={};
SetupPrinterFarmasi.func={};
SetupPrinterFarmasi.vars={};
SetupPrinterFarmasi.func.parent=SetupPrinterFarmasi;
SetupPrinterFarmasi.form.ArrayStore={};
SetupPrinterFarmasi.form.ComboBox={};
SetupPrinterFarmasi.form.DataStore={};
SetupPrinterFarmasi.form.Record={};
SetupPrinterFarmasi.form.Form={};
SetupPrinterFarmasi.form.Grid={};
SetupPrinterFarmasi.form.Panel={};
SetupPrinterFarmasi.form.TextField={};
SetupPrinterFarmasi.form.Button={};

SetupPrinterFarmasi.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik'],
	data: []
});

CurrentPage.page = dataGrid_viSetupPrinterFarmasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupPrinterFarmasi(mod_id_viSetupPrinterFarmasi){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupPrinterFarmasi = 
	[
		'kd_milik', 'milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupPrinterFarmasi = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupPrinterFarmasi
    });
	
    dataGriSetupPrinterFarmasi();
	loaddatacomboprinter_setupPrinterFarmasi();
	loaddatacomboUnitFar_setupPrinterFarmasi();
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupPrinterFarmasi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupPrinterFarmasi,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupPrinterFarmasi = undefined;
							rowSelected_viSetupPrinterFarmasi = dataSource_viSetupPrinterFarmasi.getAt(row);
							CurrentData_viSetupPrinterFarmasi
							CurrentData_viSetupPrinterFarmasi.row = row;
							CurrentData_viSetupPrinterFarmasi.data = rowSelected_viSetupPrinterFarmasi.data;
							//DataInitSetupPrinterFarmasi(rowSelected_viSetupPrinterFarmasi.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupPrinterFarmasi = dataSource_viSetupPrinterFarmasi.getAt(ridx);
					if (rowSelected_viSetupPrinterFarmasi != undefined)
					{
						//DataInitSetupPrinterFarmasi(rowSelected_viSetupPrinterFarmasi.data);
						//setLookUp_viSetupPrinterFarmasi(rowSelected_viSetupPrinterFarmasi.data);
					}
					else
					{
						//setLookUp_viSetupPrinterFarmasi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Milik
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Group',
						dataIndex: 'groups',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Unit Farmasi',
						dataIndex: 'nm_unit_far',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Printer',
						dataIndex: 'alamat_printer',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_unit_far',
						dataIndex: 'kd_unit_far',
						hideable:false,
						hidden: true,
						width: 90
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupPrinterFarmasi',
				items: 
				[
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupPrinterFarmasi, selectCount_viSetupPrinterFarmasi, dataSource_viSetupPrinterFarmasi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupPrinterFarmasi = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputPrinter()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupPrinterFarmasi = new Ext.Panel
    (
		{
			title: NamaForm_viSetupPrinterFarmasi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupPrinterFarmasi,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupPrinterFarmasi,
					GridDataView_viSetupPrinterFarmasi],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupPrinterFarmasi',
						handler: function(){
							AddNewSetupPrinterFarmasi();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupPrinterFarmasi',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupPrinterFarmasi();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupPrinterFarmasi',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupPrinterFarmasi();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPrinterFarmasi',
						handler: function()
						{
							dataSource_viSetupPrinterFarmasi.removeAll();
							dataGriSetupPrinterFarmasi();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupPrinterFarmasi;
    //-------------- # End form filter # --------------
}

function PanelInputPrinter(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 85,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Group'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							cboGroups_setupPrinterFarmasi(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Printer'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboPrinter_SetupPrinterFarmasi(),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Unit Farmasi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboUnitFar_SetupPrinterFarmasi()
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function cboGroups_setupPrinterFarmasi()
{
  var cboGroups_setupPrinterFarmasi = new Ext.form.ComboBox
	(
		{
                    id:'cboGroups_setupPrinterFarmasi',
                    x: 130,
                    y: 0,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 200,
                    emptyText:'',
					tabIndex:5,
					editable: false,
                    store: new Ext.data.ArrayStore
                    (
						{
								id: 0,
								fields:
								[
									'Id',
									'displayText'
								],
						data: [[1, 'apt_printer_bill'],[2, 'apt_printer_kwitansi']]
						}
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    listeners:
                    {
						'select': function(a,b,c)
						{
							/* selectCountStatusPostingApotekReturRWJ=b.data.displayText;
							tmpkriteria = getCriteriaCariApotekReturRWJ();
							refreshReturApotekRWJ(tmpkriteria); */

						}
                    }
		}
	);
	return cboGroups_setupPrinterFarmasi;
};

function comboPrinter_SetupPrinterFarmasi()
{
 var Field = ['name'];
    dscomboprinter_SetupPrinterFarmasi = new WebApp.DataStore({ fields: Field });
	loaddatacomboprinter_setupPrinterFarmasi();
	cboPrinter_SetupPrinterFarmasi= new Ext.form.ComboBox
	(
		{
			id				: 'cboPrinter_SetupPrinterFarmasi',
			x				: 130,
			y				: 30,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscomboprinter_SetupPrinterFarmasi,
			valueField		: 'name',
			displayField	: 'name',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacomboprinter_setupPrinterFarmasi(param);
							}
						},1000);
					}
				}
			}
		}
	);return cboPrinter_SetupPrinterFarmasi;
};

function comboUnitFar_SetupPrinterFarmasi()
{
 var Field = ['kd_unit_far', 'nm_unit_far'];
    dscomboUnitFar_SetupPrinterFarmasi = new WebApp.DataStore({ fields: Field });
	loaddatacomboUnitFar_setupPrinterFarmasi();
	cboUnitFar_SetupPrinterFarmasi= new Ext.form.ComboBox
	(
		{
			id				: 'cboUnitFar_SetupPrinterFarmasi',
			x				: 130,
			y				: 60,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscomboUnitFar_SetupPrinterFarmasi,
			valueField		: 'kd_unit_far',
			displayField	: 'nm_unit_far',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacomboUnitFar_setupPrinterFarmasi(param);
							}
						},1000);
					}
				}
			}
		}
	);return cboUnitFar_SetupPrinterFarmasi;
};

function loaddatacomboprinter_setupPrinterFarmasi(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupPrinterFarmasi/getComboPrinter",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cboPrinter_SetupPrinterFarmasi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscomboprinter_SetupPrinterFarmasi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscomboprinter_SetupPrinterFarmasi.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatacomboUnitFar_setupPrinterFarmasi(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupPrinterFarmasi/getComboUnitFar",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cboUnitFar_SetupPrinterFarmasi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscomboUnitFar_SetupPrinterFarmasi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscomboUnitFar_SetupPrinterFarmasi.add(recs);
				//console.log(o);
			}
		}
	});
}

function dataGriSetupPrinterFarmasi(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupPrinterFarmasi/getItemGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupPrinterFarmasi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupPrinterFarmasi.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupPrinterFarmasi.add(recs);
					
					
					
					GridDataView_viSetupPrinterFarmasi.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupPrinterFarmasi('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupPrinterFarmasi(){
	if (ValidasiSaveSetupPrinterFarmasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupPrinterFarmasi/save",
				params: getParamSaveSetupPrinterFarmasi(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupPrinterFarmasi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupPrinterFarmasi('Berhasil menyimpan data ini','Information');
						dataSource_viSetupPrinterFarmasi.removeAll();
						dataGriSetupPrinterFarmasi();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupPrinterFarmasi('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupPrinterFarmasi.removeAll();
						dataGriSetupPrinterFarmasi();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupPrinterFarmasi(){
	if (ValidasiDeleteSetupPrinterFarmasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupPrinterFarmasi/delete",
				params: getParamDeleteSetupPrinterFarmasi(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupPrinterFarmasi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupPrinterFarmasi('Berhasil menghapus data ini','Information');
						AddNewSetupPrinterFarmasi()
						dataSource_viSetupPrinterFarmasi.removeAll();
						dataGriSetupPrinterFarmasi();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupPrinterFarmasi('Gagal menghapus data ini', 'Error');
						dataSource_viSetupPrinterFarmasi.removeAll();
						dataGriSetupPrinterFarmasi();
					};
				}
			}
			
		)
	}
}

function AddNewSetupPrinterFarmasi(){
	Ext.getCmp('cboGroups_setupPrinterFarmasi').setValue('');
	Ext.getCmp('cboPrinter_SetupPrinterFarmasi').setValue('');
	Ext.getCmp('cboUnitFar_SetupPrinterFarmasi').setValue('');
};

function DataInitSetupPrinterFarmasi(rowdata){
	Ext.getCmp('cboGroups_setupPrinterFarmasi').setValue(rowdata.groups.substring(0, 4));
	Ext.getCmp('cboPrinter_SetupPrinterFarmasi').setValue(rowdata.alamat_printer);
	Ext.getCmp('cboUnitFar_SetupPrinterFarmasi').setValue(rowdata.kd_unit_far);
};

function getParamSaveSetupPrinterFarmasi(){
	var	params =
	{
		groups:Ext.get('cboGroups_setupPrinterFarmasi').getValue(),
		alamat_printer:Ext.getCmp('cboPrinter_SetupPrinterFarmasi').getValue(),
		kd_unit_far:Ext.getCmp('cboUnitFar_SetupPrinterFarmasi').getValue(),
	}
   
    return params
};

function getParamDeleteSetupPrinterFarmasi(){
	var line = GridDataView_viSetupPrinterFarmasi.getSelectionModel().selection.cell[0];
	var o = dataSource_viSetupPrinterFarmasi.getRange()[line].data;
	var	params =
	{
		groups:o.groups,
		alamat_printer:o.alamat_printer,
		kd_unit_far:o.kd_unit_far,
	}
   
    return params
};

function ValidasiSaveSetupPrinterFarmasi(modul,mBolHapus){
	var x = 1;
	if(Ext.get('cboGroups_setupPrinterFarmasi').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningSetupPrinterFarmasi('Group masih kosong', 'Warning');
		x = 0;
	} 
	
	if(Ext.get('cboPrinter_SetupPrinterFarmasi').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningSetupPrinterFarmasi('Printer belum di pilih', 'Warning');
		x = 0;
	} 
	
	if(Ext.get('cboUnitFar_SetupPrinterFarmasi').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningSetupPrinterFarmasi('Unit far masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};

function ValidasiDeleteSetupPrinterFarmasi(modul,mBolHapus){
	var line = GridDataView_viSetupPrinterFarmasi.getSelectionModel().selection.cell[0];
	var o = dataSource_viSetupPrinterFarmasi.getRange()[line].data;
	var x = 1;
	if(o.groups === '' || o.groups === undefined){
		loadMask.hide();
		ShowPesanWarningSetupPrinterFarmasi('Pilih data yang akan dihapus!', 'Warning');
		x = 0;
	} 
	
	if(o.alamat_printer === '' || o.alamat_printer === undefined){
		loadMask.hide();
		ShowPesanWarningSetupPrinterFarmasi('Pilih data yang akan dihapus!', 'Warning');
		x = 0;
	} 
	
	return x;
};



function ShowPesanWarningSetupPrinterFarmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupPrinterFarmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupPrinterFarmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};