var dataSource_viSetupTarif;
var NamaForm_viSetupTarif="Setup Tarif";
var mod_name_viSetupTarif="Setup Tarif";
var now_viSetupTarif= new Date();
var rowSelected_viSetupTarif;

var GridDataView_viSetupTarif;
var dscombogolongan_setupTarif;
var cbogolongan_setupTarif;
var cbojenistarif_setupTarif;
var gridcbojenistarif_setupTarif;
var gridcbogolongan_setupTarif;
var dsgridcombogolongan_setupTarif;
var dsgridcombojenistarif_setupTarif;
var cGol;
var cJenis;
var cUnit;
var dGol;
var dJenis;
var dUnit;
var dUrut;
var dKdUnitFar;


var CurrentData_viSetupTarif =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupTarif={};
SetupTarif.form={};
SetupTarif.func={};
SetupTarif.vars={};
SetupTarif.func.parent=SetupTarif;
SetupTarif.form.ArrayStore={};
SetupTarif.form.ComboBox={};
SetupTarif.form.DataStore={};
SetupTarif.form.Record={};
SetupTarif.form.Form={};
SetupTarif.form.Grid={};
SetupTarif.form.Panel={};
SetupTarif.form.TextField={};
SetupTarif.form.Button={};

SetupTarif.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik'],
	data: []
});

CurrentPage.page = dataGrid_viSetupTarif(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupTarif(mod_id_viSetupTarif){	
	
    var FieldMaster_viSetupTarif =  [  ];
    dataSource_viSetupTarif = new WebApp.DataStore ({ fields: FieldMaster_viSetupTarif });
	
	var Fieldgol = ['kd_gol','ket_gol'];
    dsgridcombogolongan_setupTarif = new WebApp.DataStore({ fields: Fieldgol });
	
	var Fieldjenis = ['kd_jenis','ket_jenis'];
	dsgridcombojenistarif_setupTarif = new WebApp.DataStore({ fields: Fieldjenis });
	
    loaddatacombogolongan_setupTarif();
	loaddatacombojenistarif_setupTarif();
	loaddatagridcombogolongan_setupTarif();
	loaddatagridcombojenistarif_setupTarif();
	
	dataGriSetupTarif();
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupTarif = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupTarif,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupTarif = undefined;
							rowSelected_viSetupTarif = dataSource_viSetupTarif.getAt(row);
							CurrentData_viSetupTarif
							CurrentData_viSetupTarif.row = row;
							CurrentData_viSetupTarif.data = rowSelected_viSetupTarif.data;
							Ext.getCmp('btnDelete_viSetupTarif').enable();
							Ext.getCmp('btnSimpan_viSetupTarif').enable();
							
							dGol = CurrentData_viSetupTarif.data.kd_gol;
							dJenis = CurrentData_viSetupTarif.data.kd_jenis;
							dUnit = CurrentData_viSetupTarif.data.kd_unit_tarif;
							dUrut = CurrentData_viSetupTarif.data.urut;
							dKdUnitFar= CurrentData_viSetupTarif.data.kd_unit_far;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupTarif = dataSource_viSetupTarif.getAt(ridx);
					if (rowSelected_viSetupTarif != undefined)
					{
						
					}
					else
					{
						//setLookUp_viSetupTarif();
					}
				},
				render: function(){
					AddNewSetupTarif(true);
					Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
					Ext.getCmp('btnSimpan_viSetupTarif').disable();
					Ext.getCmp('btnDelete_viSetupTarif').disable();
					
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Tarif
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Golongan',
						width		: 130,
						menuDisabled: true,
						dataIndex	: 'ket_gol',
						editor		: gridcbogolongan_setupTarif= new Ext.form.ComboBox({
							id				: 'gridcbogolongan_setupTarif',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							emptyText		: '',
							fieldLabel		: ' ',
							align			: 'Right',
							width			: 200,
							store			: dsgridcombogolongan_setupTarif,
							valueField		: 'ket_gol',
							displayField	: 'ket_gol',
							listeners		:
							{
								select	: function(a,b,c){
									var line	= GridDataView_viSetupTarif.getSelectionModel().selection.cell[0];
									dataSource_viSetupTarif.getRange()[line].data.kd_gol=b.data.kd_gol;
								},
								keyUp: function(a,b,c){
									$this1=this;
									if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
										clearTimeout(this.time);

										this.time=setTimeout(function(){
											if($this1.lastQuery != '' ){
												var value=$this1.lastQuery;
												var param={};
												param['text']=$this1.lastQuery;
												loaddatagridcombogolongan_setupTarif(param);
											}
										},1000);
									}
								}
							}
						})
					},
					//-------------- ## --------------
					{
						header: 'Jenis Tarif',
						dataIndex: 'ket_jenis',
						hideable:false,
						menuDisabled: true,
						width: 90,
						editor		: gridcbojenistarif_setupTarif= new Ext.form.ComboBox({
								id				: 'gridcbojenistarif_setupTarif',
								typeAhead		: true,
								triggerAction	: 'all',
								lazyRender		: true,
								mode			: 'local',
								emptyText		: '',
								fieldLabel		: ' ',
								align			: 'Right',
								width			: 200,
								store			: dsgridcombojenistarif_setupTarif,
								valueField		: 'ket_jenis',
								displayField	: 'ket_jenis',
								listeners		:
								{
									select	: function(a,b,c){
										var line	= GridDataView_viSetupTarif.getSelectionModel().selection.cell[0];
										dataSource_viSetupTarif.getRange()[line].data.kd_jenis=b.data.kd_jenis;
									},
									keyUp: function(a,b,c){
										$this1=this;
										if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
											clearTimeout(this.time);

											this.time=setTimeout(function(){
												if($this1.lastQuery != '' ){
													var value=$this1.lastQuery;
													var param={};
													param['text']=$this1.lastQuery;
													loaddatagridcombojenistarif_setupTarif(param);
												}
											},1000);
										}
									}
								}
							}
						)
					},
					//-------------- ## --------------
					{
						header: 'Unit',
						dataIndex: 'unit',
						hideable:false,
						menuDisabled: true,
						width: 90,
						editor: new Ext.form.ComboBox ( {
							id				: 'gridcboUnit_setupTarif',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							selectOnFocus	: true,
							forceSelection	: true,
							anchor			: '95%',
							value			: 1,
							store			: new Ext.data.ArrayStore({
								id		: 0,
								fields	:['Id','displayText'],
								data	: [[0, 'Rawat Jalan / UGD'],[1, 'Rawat Inap'],[2, 'Non Resep']]
							}),
							valueField	: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners	: {}
						})
					},
					//-------------- ## --------------
					{
						header: 'Jumlah',
						dataIndex: 'jumlah',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
						editor: new Ext.form.NumberField({
						})
					},
					//-------------- ## --------------
					{
						header: 'Faktor',
						dataIndex: 'faktor',
						hideable:false,
						menuDisabled: true,
						width: 90,
						editor: new Ext.form.ComboBox ( {
							id				: 'gridcboFaktor_setupTarif',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							selectOnFocus	: true,
							forceSelection	: true,
							width			: 50,
							anchor			: '95%',
							value			: 1,
							store			: new Ext.data.ArrayStore({
								id		: 0,
								fields	:['Id','displayText'],
								data	: [[0, 'Penjumlah'],[1, 'Pengali']]
							}),
							valueField	: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners	: {}
						})
					},
					//-------------- ## --------------
					{
						header: 'kd_gol',
						dataIndex: 'kd_gol',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_jenis',
						dataIndex: 'kd_jenis',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_unit_far',
						dataIndex: 'kd_unit_far',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'urut',
						dataIndex: 'urut',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupTarif',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowTarif_viSetupTarif',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupTarif.recordType());
							dataSource_viSetupTarif.add(records);
							DataClearSetupTarif();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupTarif',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupTarif();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupTarif',
						handler: function()
						{
							if((dGol == '' || dGol == undefined) && (dJenis == '' || dJenis == undefined) && (dUnit == '' || dUnit == undefined) && 
								(dUrut == '' || dUrut == undefined) && (dKdUnitFar == '' || dKdUnitFar == undefined)){
								ShowPesanWarningSetupTarif('Pilih data yang akan di hapus', 'Warning');
							} else{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viSetupTarif();
									}
								});
							}
							
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupTarif',
						handler: function()
						{
							dataSource_viSetupTarif.removeAll();
							DataClearSetupTarif();
							dataGriSetupTarif();
							AddNewSetupTarif(true);
							Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
							Ext.getCmp('btnSimpan_viSetupTarif').disable();
							Ext.getCmp('btnDelete_viSetupTarif').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupTarif = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelPencarianTarif_setupTarif()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupTarif = new Ext.Panel
    (
		{
			title: NamaForm_viSetupTarif,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupTarif,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupTarif,
					GridDataView_viSetupTarif],
			tbar:[
			{
				xtype: 'button',
				text: 'Add New',
				iconCls: 'add',
				id: 'btnAddNewTarif_viSetupTarif',
				handler: function(){
					GridDataView_viSetupTarif.store.removeAll();
					var records = new Array();
					records.push(new dataSource_viSetupTarif.recordType());
					dataSource_viSetupTarif.add(records);
					AddNewSetupTarif(false);
					Ext.getCmp('btnAddRowTarif_viSetupTarif').enable();
					Ext.getCmp('btnSimpan_viSetupTarif').enable();
					Ext.getCmp('btnDelete_viSetupTarif').enable();
				}
			},
			
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupTarif;
    //-------------- # End form filter # --------------
}

function PanelPencarianTarif_setupTarif(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 125,
		title:"Pencarian",
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 500,
						height: 125,
						anchor: '100% 100%',
						items:
						[
							
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Golongan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboGolongan_SetupTarif(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Jenis Tarif'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboJenisTarif_SetupTarif(),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Unit'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboUnit_setupTarif(),
							{
								x: 350,
								y: 60,
								xtype: 'button',
								text: 'Cari',
								iconCls: 'find',
								width: 70,
								id: 'btnCari_viSetupTarif',
								handler: function()
								{
									cGol = Ext.getCmp('cbogolongan_setupTarif').getValue();
									cJenis = Ext.getCmp('cbojenistarif_setupTarif').getValue();
									cUnit = Ext.getCmp('cboUnit_setupTarif').getValue();
									dataSource_viSetupTarif.removeAll();
									dataGriSetupTarif(cGol,cJenis,cUnit);
									AddNewSetupTarif(true);
									Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
									Ext.getCmp('btnSimpan_viSetupTarif').enable();
									Ext.getCmp('btnDelete_viSetupTarif').enable();
								}
							},
						]
					}
				]
			} 
							
		]		
	};
        return items;
}

function comboGolongan_SetupTarif()
{
 var Field = ['kd_gol','ket_gol'];
    dscombogolongan_setupTarif = new WebApp.DataStore({ fields: Field });
	loaddatacombogolongan_setupTarif();
	cbogolongan_setupTarif= new Ext.form.ComboBox
	(
		{
			id				: 'cbogolongan_setupTarif',
			x				: 130,
			y				: 0,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscombogolongan_setupTarif,
			valueField		: 'kd_gol',
			displayField	: 'ket_gol',
			emptyText			: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacombogolongan_setupTarif(param);
							}
						},1000);
					}
				}
			}
		}
	);return cbogolongan_setupTarif;
};

function comboJenisTarif_SetupTarif()
{
	var Field = ['kd_jenis','ket_jenis'];
    dscombojenistarif_setupTarif = new WebApp.DataStore({ fields: Field });
	loaddatacombojenistarif_setupTarif();
	cbojenistarif_setupTarif= new Ext.form.ComboBox
	(
		{
			id				: 'cbojenistarif_setupTarif',
			x				: 130,
			y				: 30,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscombojenistarif_setupTarif,
			valueField		: 'kd_jenis',
			displayField	: 'ket_jenis',
			emptyText			: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacombojenistarif_setupTarif(param);
							}
						},1000);
					}
				}
			}
		}
	);return cbojenistarif_setupTarif;
};

function comboUnit_setupTarif()
{
    var cboUnit_setupTarif = new Ext.form.ComboBox
	(
		{
			id: 'cboUnit_setupTarif',
			x: 130,
			y: 60,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			tabIndex: 3,
			selectOnFocus: true,
			forceSelection	: true,
			width: 200,
			emptyText			: 'SEMUA',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
					data: [[0, 'Rawat Jalan / UGD'], [1, 'Rawat Inap'], [2, 'Non Resep']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:
			{
				'select': function (a, b, c)
				{
					
				},
				'render': function (c)
				{
					
				}
			}
		}
	);
    return cboUnit_setupTarif;
};
//------------------end---------------------------------------------------------

function dataGriSetupTarif(gol,jenis,unit){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarif/getItemGrid",
			params: {
				gol : gol,
				jenis : jenis,
				unit : unit
			},
			failure: function(o)
			{
				loadMask.hide()
				ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupTarif.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupTarif.add(recs);
					
					
					
					GridDataView_viSetupTarif.getView().refresh();
				}
				else 
				{
					loadMask.hide()
					ShowPesanErrorSetupTarif('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupTarif(){
	if (ValidasiSaveSetupTarif(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupTarif/save",
				params: getParamSaveSetupTarif(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTarif('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupTarif.removeAll();
						dataGriSetupTarif();
						Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
						Ext.getCmp('btnSimpan_viSetupTarif').disable();
						Ext.getCmp('btnDelete_viSetupTarif').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTarif('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupTarif.removeAll();
						dataGriSetupTarif();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupTarif(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarif/delete",
			params: getParamDeleteSetupTarif(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupTarif('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupTarif.removeAll();
					dataGriSetupTarif();
					
					Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
					Ext.getCmp('btnSimpan_viSetupTarif').disable();
					Ext.getCmp('btnDelete_viSetupTarif').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupTarif('Gagal menghapus data ini', 'Error');
					dataSource_viSetupTarif.removeAll();
					dataGriSetupTarif();
				};
			}
		}
		
	)
}

function loaddatacombogolongan_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboGolongan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbogolongan_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscombogolongan_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscombogolongan_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatagridcombogolongan_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboGolongan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbogolongan_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombogolongan_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombogolongan_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatacombojenistarif_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboJenisTarif",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbojenistarif_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscombojenistarif_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscombojenistarif_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatagridcombojenistarif_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboJenisTarif",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbojenistarif_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombojenistarif_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombojenistarif_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function AddNewSetupTarif(set){
	Ext.getCmp('gridcbogolongan_setupTarif').setReadOnly(set);
	Ext.getCmp('gridcbojenistarif_setupTarif').setReadOnly(set);
	Ext.getCmp('gridcboUnit_setupTarif').setReadOnly(set);
	
};

function ValidasiButton(set){
	
};

function DataClearSetupTarif(){
	Ext.getCmp('cbogolongan_setupTarif').setValue('');
	Ext.getCmp('cbojenistarif_setupTarif').setValue('');
	Ext.getCmp('cboUnit_setupTarif').setValue('');
};

function getParamSaveSetupTarif(){
	var	params =
	{
		
	}
	params['jml']=dataSource_viSetupTarif.getCount();
	for(var i = 0 ; i < dataSource_viSetupTarif.getCount();i++)
	{
		params['kd_gol-'+i]=dataSource_viSetupTarif.data.items[i].data.kd_gol;
		params['kd_jenis-'+i]=dataSource_viSetupTarif.data.items[i].data.kd_jenis
		params['unit-'+i]=dataSource_viSetupTarif.data.items[i].data.unit
		params['jumlah-'+i]=dataSource_viSetupTarif.data.items[i].data.jumlah
		params['faktor-'+i]=dataSource_viSetupTarif.data.items[i].data.faktor	
		params['urut-'+i]=dataSource_viSetupTarif.data.items[i].data.urut
		params['kd_unit_far-'+i]=dataSource_viSetupTarif.data.items[i].data.kd_unit_far		
	}
   
    return params
};

function getParamDeleteSetupTarif(){
	var	params =
	{
		kd_gol : dGol,
		kd_jenis : dJenis,
		kd_unit_tarif : dUnit,
		kd_unit_far : dKdUnitFar,
		urut : dUrut
	}
   
    return params
};

function ValidasiSaveSetupTarif(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupTarif.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupTarif('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupTarif.getCount() ; i++){
			var o=dataSource_viSetupTarif.getRange()[i].data;
			if(o.ket_gol == undefined || o.ket_gol == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Golongan tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.ket_jenis == undefined || o.ket_jenis == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Jenis tarif tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.unit == undefined || o.unit == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Unit tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.jumlah == undefined || o.jumlah == 0){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Jumlah tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.faktor == undefined || o.faktor == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Faktor tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningSetupTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};