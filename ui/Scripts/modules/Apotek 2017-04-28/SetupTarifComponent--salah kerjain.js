var dataSource_viSetupTarifComponent;
var selectCount_viSetupTarifComponent=50;
var NamaForm_viSetupTarifComponent="Setup Tarif Component";
var mod_name_viSetupTarifComponent="Setup Tarif Component";
var now_viSetupTarifComponent= new Date();
var rowSelected_viSetupTarifComponent;
var setLookUps_viSetupTarifComponent;
var tanggal = now_viSetupTarifComponent.format("d/M/Y");
var jam = now_viSetupTarifComponent.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupTarifComponent;


var CurrentData_viSetupTarifComponent =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupTarifComponent={};
SetupTarifComponent.form={};
SetupTarifComponent.func={};
SetupTarifComponent.vars={};
SetupTarifComponent.func.parent=SetupTarifComponent;
SetupTarifComponent.form.ArrayStore={};
SetupTarifComponent.form.ComboBox={};
SetupTarifComponent.form.DataStore={};
SetupTarifComponent.form.Record={};
SetupTarifComponent.form.Form={};
SetupTarifComponent.form.Grid={};
SetupTarifComponent.form.Panel={};
SetupTarifComponent.form.TextField={};
SetupTarifComponent.form.Button={};

SetupTarifComponent.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_component', 'component', 'kd_jenis'],
	data: []
});

CurrentPage.page = dataGrid_viSetupTarifComponent(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupTarifComponent(mod_id_viSetupTarifComponent){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupTarifComponent = 
	[
		'kd_component', 'component', 'kd_jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupTarifComponent = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupTarifComponent
    });
    dataGriSetupTarifComponent();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupTarifComponent = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupTarifComponent,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupTarifComponent = undefined;
							rowSelected_viSetupTarifComponent = dataSource_viSetupTarifComponent.getAt(row);
							CurrentData_viSetupTarifComponent
							CurrentData_viSetupTarifComponent.row = row;
							CurrentData_viSetupTarifComponent.data = rowSelected_viSetupTarifComponent.data;
							//DataInitSetupTarifComponent(rowSelected_viSetupTarifComponent.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupTarifComponent = dataSource_viSetupTarifComponent.getAt(ridx);
					if (rowSelected_viSetupTarifComponent != undefined)
					{
						DataInitSetupTarifComponent(rowSelected_viSetupTarifComponent.data);
						//setLookUp_viSetupTarifComponent(rowSelected_viSetupTarifComponent.data);
					}
					else
					{
						//setLookUp_viSetupTarifComponent();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Milik
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viSetupTarifComponent',
						header: 'Kode komponen',
						dataIndex: 'kd_component',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colComponent_viSetupTarifComponent',
						header: 'Komponen',
						dataIndex: 'component',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colJenisComponent_viSetupTarifComponent',
						header: 'Jenis Komponen',
						dataIndex: 'kd_jenis',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupTarifComponent',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupTarifComponent',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupTarifComponent != undefined)
							{
								DataInitSetupTarifComponent(rowSelected_viSetupTarifComponent.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupTarifComponent, selectCount_viSetupTarifComponent, dataSource_viSetupTarifComponent),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupTarifComponent = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputMilik()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupTarifComponent = new Ext.Panel
    (
		{
			title: NamaForm_viSetupTarifComponent,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupTarifComponent,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupTarifComponent,
					GridDataView_viSetupTarifComponent],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupTarifComponent',
						handler: function(){
							AddNewSetupTarifComponent();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupTarifComponent',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupTarifComponent();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupTarifComponent',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupTarifComponent();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupTarifComponent',
						handler: function()
						{
							dataSource_viSetupTarifComponent.removeAll();
							dataGriSetupTarifComponent();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupTarifComponent;
    //-------------- # End form filter # --------------
}

function PanelInputMilik(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode komponen'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'numberfield',
								id: 'txtKdComponent_SetupTarifComponent',
								name: 'txtKdComponent_SetupTarifComponent',
								width: 100,
								readOnly: false,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Komponen'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupTarifComponent.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: SetupTarifComponent.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdComponent_SetupTarifComponent').setValue(b.data.kd_milik);
									
									GridDataView_viSetupTarifComponent.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupTarifComponent.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupTarifComponent.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupTarifComponent.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_component      	: o.kd_component,
										component 			: o.component,
										kd_jenis			: o.kd_jenis,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_component+'</td><td width="200">'+o.component+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupTarifComponent/getTarifComponentGrid",
								valueField: 'component',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Komponen'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							ComboJenisComponent()
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupTarifComponent(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifComponent/getTarifComponentGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupTarifComponent.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupTarifComponent.add(recs);
					
					
					
					GridDataView_viSetupTarifComponent.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupTarifComponent('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupTarifComponent(){
	if (ValidasiSaveSetupTarifComponent(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupTarifComponent/save",
				params: getParamSaveSetupTarifComponent(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTarifComponent('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdComponent_SetupTarifComponent').setValue(cst.kdcomponent);
						dataSource_viSetupTarifComponent.removeAll();
						dataGriSetupTarifComponent();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTarifComponent('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupTarifComponent.removeAll();
						dataGriSetupTarifComponent();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupTarifComponent(){
	if (ValidasiSaveSetupTarifComponent(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupTarifComponent/delete",
				params: getParamDeleteSetupTarifComponent(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTarifComponent('Berhasil menghapus data ini','Information');
						AddNewSetupTarifComponent()
						dataSource_viSetupTarifComponent.removeAll();
						dataGriSetupTarifComponent();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTarifComponent('Gagal menghapus data ini', 'Error');
						dataSource_viSetupTarifComponent.removeAll();
						dataGriSetupTarifComponent();
					};
				}
			}
			
		)
	}
}

function ComboJenisComponent()
{
	var Field_Jenis = ['KD_JENIS', 'JENIS'];
	ds_Jenis = new WebApp.DataStore({fields: Field_Jenis});
    ds_Jenis.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViComboJenisComponent',
					param: ''
				}
		}
	);
    var comboJenisComponentSetupTarifComponent = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 60,
			id:'comboJenisComponentSetupTarifComponent',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			emptyText:'Jenis Komponen',
			width: 180,
			tabIndex:2,
			store: ds_Jenis,
			fieldLabel: 'Jenis ',
			valueField: 'KD_JENIS',
			displayField: 'JENIS',
			listeners:
			{
				'select': function(a,b,c)
				{
					//Ext.getCmp('txttmpJenisObat_MasterObatL').setValue(b.data.valueField)
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return comboJenisComponentSetupTarifComponent;
};

function AddNewSetupTarifComponent(){
	Ext.getCmp('txtKdComponent_SetupTarifComponent').setValue('');
	SetupTarifComponent.vars.nama.setValue('');
	Ext.getCmp('comboJenisComponentSetupTarifComponent').setValue('');
};

function DataInitSetupTarifComponent(rowdata){
	Ext.getCmp('txtKdComponent_SetupTarifComponent').setValue(rowdata.kd_component);
	SetupTarifComponent.vars.nama.setValue(rowdata.component);
	Ext.getCmp('comboJenisComponentSetupTarifComponent').setValue(rowdata.kd_jenis);
};

function getParamSaveSetupTarifComponent(){
	var	params =
	{
		KdComponent:Ext.getCmp('txtKdComponent_SetupTarifComponent').getValue(),
		Component:SetupTarifComponent.vars.nama.getValue(),
		KdJenis:Ext.getCmp('comboJenisComponentSetupTarifComponent').getValue()
	}
   
    return params
};

function getParamDeleteSetupTarifComponent(){
	var	params =
	{
		KdComponent:Ext.getCmp('txtKdComponent_SetupTarifComponent').getValue()
	}
   
    return params
};

function ValidasiSaveSetupTarifComponent(modul,mBolHapus){
	var x = 1;
	if(SetupTarifComponent.vars.nama.getValue() === '' || Ext.getCmp('comboJenisComponentSetupTarifComponent').getValue() === ''){
		if(SetupTarifComponent.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupTarifComponent('Komponen masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningSetupTarifComponent('Jenis komponen belum dipilih', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningSetupTarifComponent(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupTarifComponent(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupTarifComponent(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};