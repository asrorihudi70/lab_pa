var dataSource_viApotekResepRWI;
var selectCount_viApotekResepRWI=50;
var NamaForm_viApotekResepRWI="Resep Rawat Inap ";
var selectCountStatusPostingApotekResepRWI='Semua';
var mod_name_viApotekResepRWI="viApotekResepRWI";
var now_viApotekResepRWI= new Date();
var addNew_viApotekResepRWI;
var rowSelected_viApotekResepRWI;
var setLookUps_viApotekResepRWI;
var mNoKunjungan_viApotekResepRWI='';
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viApotekResepRWI.format("d/M/Y");
var jam = now_viApotekResepRWI.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var gridDTLTRHistoryApotekRWI;
var cbopasienorder_mng_apotek;
var dsDataGrdJab_viApotekResepRWI;
var kd_pasien_obbt_rwi;
var kd_unit_obbt_rwi;
var tgl_masuk_obbt_rwi;
var urut_masuk_obbt_rwi; 
var CurrentHistoryRWI =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekResepRWI =
{
	data: Object,
	details: Array,
	row: 0
};
var dspasienorder_mng_apotek;
CurrentPage.page = dataGrid_viApotekResepRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var ResepRWI={};
ResepRWI.form={};
ResepRWI.func={};
ResepRWI.vars={};
ResepRWI.func.parent=ResepRWI;
ResepRWI.form.ArrayStore={};
ResepRWI.form.ComboBox={};
ResepRWI.form.DataStore={};
ResepRWI.form.Record={};
ResepRWI.form.Form={};
ResepRWI.form.Grid={};
ResepRWI.form.Panel={};
ResepRWI.form.TextField={};
ResepRWI.form.Button={};


ResepRWI.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'no_kamar','nama_kamar', 'kd_dokter', 'kd_customer'
				],
		data: []
	});

ResepRWI.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'no_kamar','nama_kamar', 'kd_dokter', 'kd_customer'
				],
		data: []
	});

ResepRWI.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik'],
	data: []
});

function dataGrid_viApotekResepRWI(mod_id_viApotekResepRWI){	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekResepRWI = 
	[
		 'STATUS_POSTING','NO_RESEP','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','NO_KAMAR','NAMA_KAMAR','CUSTOMER','JENIS_PASIEN'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viApotekResepRWI = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekResepRWI
    });
    refreshRespApotekRWI();
	total_pasien_order_mng_obtrwj();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekResepRWI = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viApotekResepRWI,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekResepRWI = undefined;
							rowSelected_viApotekResepRWI = dataSource_viApotekResepRWI.getAt(row);
							CurrentData_viApotekResepRWI
							CurrentData_viApotekResepRWI.row = row;
							CurrentData_viApotekResepRWI.data = rowSelected_viApotekResepRWI.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viApotekResepRWI = dataSource_viApotekResepRWI.getAt(ridx);
					if (rowSelected_viApotekResepRWI != undefined)
					{
						setLookUp_viApotekResepRWI(rowSelected_viApotekResepRWI.data);
					}
					else
					{
						setLookUp_viApotekResepRWI();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'STATUS_POSTING',
						id			: 'colStatusPosting_viApotekResepRWI',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoMedrec_viApotekResepRWI',
						header: 'No. Resep',
						dataIndex: 'NO_RESEP',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						id: 'colTgl_viApotekResepRWI',
						header:'Tgl Resep',
						dataIndex: 'TGL_OUT',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						// format: 'd/M/Y',
						//filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OUT);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekResepRWI',
						header: 'No Medrec',
						dataIndex: 'KD_PASIENAPT',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colNamaPasien_viApotekResepRWI',
						header: 'Nama Pasien',
						dataIndex: 'NMPASIEN',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colUnit_viApotekResepRWI',
						header: 'Ruangan',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						id: 'colNoout_viApotekResepRWJ',
						header: 'No Out',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekResepRWI',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift;
									ResepRWI.form.Panel.shift.update(cst.shift);
								}	
							
							});
							Ext.Ajax.request
							(
								{
									url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulan",
									params: {a:"no_out"},
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											setLookUp_viApotekResepRWI();
										} else{
											if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
												ShowPesanErrorResepRWI('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
											} else{
												ShowPesanErrorResepRWI('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
											}
											
										}
									}
								}
							);
								
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekResepRWI',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}	
							
							});
							if (rowSelected_viApotekResepRWI != undefined)
							{
								setLookUp_viApotekResepRWI(rowSelected_viApotekResepRWI.data)
							}
							
						}
					},{xtype: 'tbspacer',height: 3, width:580},
					{
					xtype: 'label',
					text: 'Order Obat dari rawat Inap : ' 
					},{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						fieldLabel: 'No. Medrec',
						name: 'txtcounttr_apt_rwj',
						id: 'txtcounttr_apt_rwj',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						}
				  }
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viApotekResepRWI, selectCount_viApotekResepRWI, dataSource_viApotekResepRWI),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianApotekResepRWI = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekResepRWI',
							name: 'TxtFilterGridDataView_RoNumber_viApotekResepRWI',
							emptyText: 'No. Resep',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Kode/Nama Pasien'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtKdNamaPasienResepRWI',
							name: 'txtKdNamaPasienResepRWI',
							emptyText: 'Kode/Nama Pasien',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Ruangan'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						ComboUnitApotekResepRWI(),	
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Resep'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalApotekResepRWI',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viApotekResepRWI,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirApotekResepRWI',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viApotekResepRWI,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekResepRWI(),
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viApotekResepRWI',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariApotekResepRWI();
								refreshRespApotekRWI(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
						/*				
						//-------------- ## --------------
						
						//-------------- ## --------------
				]
			} */
		
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekResepRWI = new Ext.Panel
    (
		{
			title: NamaForm_viApotekResepRWI,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekResepRWI,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianApotekResepRWI,
					GridDataView_viApotekResepRWI],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekResepRWI,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viApotekResepRWI;
    //-------------- # End form filter # --------------
}


function total_pasien_order_mng_obtrwj()
{
Ext.Ajax.request(
{
url: baseURL + "index.php/apotek/functionAPOTEK/countpasienmr_resep_rwi",
 params: {
	command: '0',
	// parameter untuk url yang dituju (fungsi didalam controller)
},
failure: function(o)
{
	 var cst = Ext.decode(o.responseText);
	
},	    
success: function(o) {
	 var cst = Ext.decode(o.responseText);
	
	 Ext.getCmp('txtcounttr_apt_rwj').setValue(cst.countpas);
	 console.log(cst);
	
}
});
};

function refreshRespApotekRWI(kriteria)
{
    dataSource_viApotekResepRWI.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewResepApotekRWI',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viApotekResepRWI;
}

function setLookUp_viApotekResepRWI(rowdata){
    var lebar = 985;
    setLookUps_viApotekResepRWI = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viApotekResepRWI, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekResepRWI(lebar,rowdata),
        listeners:{
            activate: function(){
				Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
									ResepRWI.form.Panel.shift.update(cst.shift);
								}	
							
							});
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viApotekResepRWI=undefined;
                //datarefresh_viApotekResepRWI();
				mNoKunjungan_viApotekResepRWI = '';
            }
        }
    });

    setLookUps_viApotekResepRWI.show();
	//dsDataGrdJab_viApotekResepRWI.loadData([],false);
	ViewDetailPembayaranObatRWI(rowdata.NO_OUT,rowdata.TGL_OUT);
    if (rowdata == undefined){
        // dataaddnew_viApotekResepRWI();
		// Ext.getCmp('btnDelete_viApotekResepRWI').disable();	
    }
    else
    {
        datainit_viApotekResepRWI(rowdata);
    }
}

function getFormItemEntry_viApotekResepRWI(lebar,rowdata){
    var pnlFormDataBasic_viApotekResepRWI = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputBiodata_viApotekResepRWI(lebar),
				getItemGridTransaksi_viApotekResepRWI(lebar),
				getItemGridHistoryBayar_viApotekResepRWI(lebar),
				{
					layout	: 'form',
					bodyStyle: 'margin-top: 5px;',
					border: false,
					items	: [
						{
							xtype: 'compositefield',
							fieldLabel: ' ',
							labelSeparator: '',
							items: [
									ResepRWI.form.Panel.a=new Ext.Panel ({
									region: 'north',
									border: false,
									html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
								}),
								{
									xtype: 'displayfield',				
									width: 80,								
									value: 'Paid Status'
									//style:{'font-weight':'bold'}		
								},{
									xtype: 'displayfield',				
									width: 150,								
									value: 'Tuslah + Embalase :',
									style:{'text-align':'right','margin-left':'184px'}							
								},{
						            xtype: 'textfield',
						            id: 'txtTuslahEmbalaseL',
									style:{'text-align':'right','margin-left':'184px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 100,								
									value: 'Adm Racik :',
									style:{'text-align':'right','margin-left':'154px'}
								},{
						            xtype: 'textfield',
						            id: 'txtAdmRacikResepRWIL',
									style:{'text-align':'right','margin-left':'154px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 50,								
									value: 'Total :',
									style:{'text-align':'right','margin-left':'184px'}
								},{
						            xtype: 'textfield',
						            id: 'txtJumlahTotalResepRWIL',
									style:{'text-align':'right','margin-left':'184px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        }												
						    ]
						},{
							xtype: 'compositefield',
							items: [
								{ 
									xtype: 'displayfield',				
									width: 80,								
									value: 'Tanggal :',
									style:{'text-align':'right'}
								},{ 
									xtype: 'displayfield',
									id		: '',
									width: 100,								
									value: tanggal,
									format:'d/M/Y'
								},{
									xtype: 'displayfield',				
									width: 50,								
									value: 'Adm :',
									style:{'text-align':'right','margin-left':'202px'}
									
								},{
						            xtype: 'numberfield',
						            id: 'txtAdmResepRWIL',
									style:{'text-align':'right','margin-left':'202px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 100,								
									value: 'Disc :',
									style:{'text-align':'right','margin-left':'172px'}
									
								},{
						            xtype: 'numberfield',
						            id: 'txtTotalDiscResepRWIL',
									style:{'text-align':'right','margin-left':'172px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 90,								
									value: 'Grand Total :',
									style:{'text-align':'right','margin-left':'162px','font-weight':'bold'}
									
								},{
						            xtype: 'textfield',
						            id: 'txtTotalBayarResepRWIL',
									style:{'text-align':'right','margin-left':'162px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        }
						    ]
						},{
							xtype: 'compositefield',
							items: [
								{ 
									xtype: 'displayfield',				
									width: 80,								
									value: 'Current Shift :',
									style:{'text-align':'right'}
								},ResepRWI.form.Panel.shift=new Ext.Panel ({
									region: 'north',
									border: false
								}),{
									xtype: 'displayfield',				
									width: 150,								
									value: 'Adm Perusahaan :',
									style:{'text-align':'right','margin-left':'292px'}
									
								},{
						            xtype: 'textfield',
						            id: 'txtAdmPrshResepRWIL',
									style:{'text-align':'right','margin-left':'447px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        }
						    ]
						}
		     	   ]
				}
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						hidden:true,
						id: 'btnAdd_viApotekResepRWI',
						handler: function(){
							dataaddnew_viApotekResepRWI();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viApotekResepRWI',
						handler: function()
						{
							datasave_viApotekResepRWI();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viApotekResepRWI',
						handler: function()
						{
							/* var x = datasave_viApotekResepRWI(addNew_viApotekResepRWI);
							datarefresh_viApotekResepRWI();
							if (x===undefined)
							{
								setLookUps_viApotekResepRWI.close();
							} */
							datasave_viApotekResepRWI();
							refreshRespApotekRWI();
							setLookUps_viApotekResepRWI.close();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Paid',
						id: 'btnBayar_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_bayarResepRWI();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Unposting',
						id:'btnunposting_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						handler:function()
						{
							Ext.Ajax.request
							(
								{
									url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulan",
									params: {a:"no_out"},
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
												if (button == 'yes'){
													Ext.Ajax.request
													(
														{
															url: baseURL + "index.php/apotek/functionAPOTEKrwi/unpostingResepRWI",
															params: getParamUnpostingResepRWI(),
															failure: function(o)
															{
																ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
															},	
															success: function(o) 
															{
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) 
																{
																	ShowPesanInfoResepRWI('UnPosting Berhasil','Information');
																	Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
																	Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
																	Ext.getCmp('btnunposting_viApotekResepRWI').disable();
																	Ext.getCmp('btnBayar_viApotekResepRWI').disable();
																	Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
																	Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
																	Ext.getCmp('btnDelete_viApotekResepRWI').disable();
																	Ext.getCmp('btnPrint_viResepRWI').disable();
																	Ext.getCmp('btnAddObatRWI').enable();
																	Ext.getCmp('btnDeleteHistory_viApotekResepRWI').enable();
																	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue('0');
																	ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
																	refreshRespApotekRWI();
																}
																else 
																{
																	ShowPesanErrorResepRWI('Gagal melakukan unPosting', 'Error');
																};
															}
														}
														
													)
												}
											});
										} else{
											if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
												ShowPesanErrorResepRWI('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
											} else{
												ShowPesanErrorResepRWI('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
											}
											
										}
									}
								}
							);
						}  
					},
					{
						xtype: 'button',
						text: 'Delete Paid',
						id:'btnDeleteHistory_viApotekResepRWI',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayar.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan di hapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionAPOTEKrwi/deleteHistoryResepRWJ",
												params: getParamDeleteHistoryResepRWI(),
												failure: function(o)
												{
													ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoResepRWI('Penghapusan berhasil','Information');
														ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
														gridDTLTRHistoryApotekRWI.getView().refresh();
														Ext.getCmp('btnBayar_viApotekResepRWI').enable();
														Ext.getCmp('btnDelete_viApotekResepRWI').enable();
													}
													else 
													{
														ShowPesanErrorResepRWI('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
														gridDTLTRHistoryApotekRWI.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else{
								ShowPesanErrorResepRWI('Belum melakukan pembayaran','Error');
							}
						}  
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viResepRWI',
						disabled:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillResepRWI',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					},
					{
					xtype: 'label',
					text: 'Order Obat dari Rawat Inap : ' 
					},
					
					
					mComboorder(),
					{
						xtype: 'button',
						text: 'close order',
						id:'statusservice_apt_RWI',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							updatestatus_permintaan()
							load_data_pasienorder();
							ResepRWI.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt_RWI').disable();
						}
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viApotekResepRWI;
}

function updatestatus_permintaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/update_obat_mng_rwi",
			params: {
			kd_pasien: kd_pasien_obbt_rwi,
			kd_unit:   kd_unit_obbt_rwi,
			tgl_masuk: tgl_masuk_obbt_rwi,
			urut_masuk: urut_masuk_obbt_rwi,
      		},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				ShowPesanInfoResepRWI('data obat sudah ditutup','order')
			}
		}
		
	)
	
}

function mComboorder()
{ 
 
 var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_msauk','tgl_masuk'];

    dspasienorder_mng_apotek = new WebApp.DataStore({ fields: Field });
	
	load_data_pasienorder();
	cbopasienorder_mng_apotek= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_apotek',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 190,
			store: dspasienorder_mng_apotek,
			valueField: 'display',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
				
				            kd_pasien_obbt_rwi =b.data.kd_pasien;
							kd_unit_obbt_rwi=b.data.kd_unit;
							tgl_masuk_obbt_rwi =b.data.tgl_masuk;
							urut_masuk_obbt_rwi=b.data.urut_masuk; 
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
							Ext.getCmp('txtKdPasienApotekResepRWIL').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							Ext.getCmp('statusservice_apt_RWI').enable();
							dataGridObatApotekResep_penatarwi(b.data.id_mrresep,b.data.kd_customer);
							},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder($this1.lastQuery);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng_apotek;
};
function dataGridObatApotekResep_penatarwi(id_mrresep,cuss){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getobatdetail_frompoli",
			params: {query:id_mrresep,
					 cus :cuss
			},
			failure: function(o)
			{
				//ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{    ResepRWI.form.Grid.a.store.removeAll();
				//dsDataGrdJab_viApotekResepRWI.loadData([],false);
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWI.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWI.add(recs);
					
					ResepRWI.form.Grid.a.getView().refresh();
					
				
				} 
				else 
				{
					ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function load_data_pasienorder(param)
{

Ext.Ajax.request(
{
url: baseURL + "index.php/apotek/functionAPOTEK/getPasienorder_mng_rwi",
 params:{
	     command: param
		} ,
failure: function(o)
{
	 var cst = Ext.decode(o.responseText);
	
},	    
success: function(o) {
cbopasienorder_mng_apotek.store.removeAll();
	var cst = Ext.decode(o.responseText);

for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
	var recs    = [],recType = dspasienorder_mng_apotek.recordType;
	var o=cst['listData'][i];
	
	recs.push(new recType(o));
    dspasienorder_mng_apotek.add(recs);
console.log(o);
}}
});
}

function getItemPanelInputBiodata_viApotekResepRWI(lebar) {
    var items ={
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No Resep',
                width: 730,
                items:
				[                    
					{
						xtype: 'textfield',
						id: 'txtNoResepApotekResepRWIL',
						readOnly: true,
						emptyText: 'Nomor Resep',
						width: 130
					},
					{
						xtype: 'displayfield',
						width: 45
					},
					{
						xtype: 'displayfield',
						width: 100,
						value: 'Jenis Pasien :'
					},
					ComboPilihanKelompokPasienApotekResepRWI(),
					{
						xtype: 'displayfield',
						width: 25
					},
					{
						xtype: 'displayfield',
						width: 50,
						value: 'Kamar :'
					},
					{
						xtype: 'textfield',
						id: 'txtNoKamarApotekResepRWIL',
						readOnly	: true,
						width: 50,
						emptyText: 'No kamar'
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtKamarApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kamar'
					}
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				width: 199,
				items: 
				[
					
					{
						xtype: 'textfield',
						id: 'txtKdPasienApotekResepRWIL',
						readOnly	: true,
						emptyText: 'No medrec',
						width: 130
					},
					/* ResepRWI.form.ComboBox.kodepasien= new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekRes
							epRWI').setValue(b.data.kd_dokter);
							ResepRWI.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							
						},
						width	: 130,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>',
								nama	 			:o.nama
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'No medrec'
					}), */
					{
						xtype: 'displayfield',
						width: 45
					},
					{
						xtype: 'displayfield',
						width: 100,
						value: 'Dokter :'
					},
					ComboDokterApotekResepRWI()
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Nama Pasien',
				anchor: '100%',
				width: 199,
				items: 
				[
					ResepRWI.form.ComboBox.namaPasien= new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.pasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
							Ext.getCmp('txtKdPasienApotekResepRWIL').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							
						},
						width	: 180,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>',
								nama	 			:o.nama
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getPasienResepRWI",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'Nama Pasien'
					}),
					{
						xtype: 'displayfield',
						width: 100,
						value: 'Nama Unit :'
					},
					ComboUnitResepRWILookup(),
					{
						xtype: 'displayfield',
						width: 25
					},
					{
						xtype: 'button',
						width : 70,	
						text	: '1/2 Resep',
						id: Nci.getId()
					},
					//HIDDEN
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdCustomerApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kode Pasien',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdDokterApotekResepRWIL',
						readOnly: true,
						emptyText: 'kode Dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdUnitApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kode Unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpNooutResepRWIL',
						id: 'txtTmpNooutResepRWIL',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTgloutResepRWIL',
						id: 'txtTmpTgloutResepRWIL',
						emptyText: 'tgl out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPostResepRWIL',
						id: 'txtTmpStatusPostResepRWIL',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpSisaAngsuranResepRWIL',
						id: 'txtTmpSisaAngsuranResepRWIL',
						emptyText: 'sisa angsuran',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTotQtyResepRWIL',
						id: 'txtTmpTotQtyResepRWIL',
						emptyText: 'total qty',
						hidden:true
					}
				]
			}
		]
	};
    return items;
};

function getItemGridTransaksi_viApotekResepRWI(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 235,//300, 
	    tbar:
		[
			{
				text	: 'Add Product',
				id		: 'btnAddObatRWI',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
					dsDataGrdJab_viApotekResepRWI.add(records);
					var kd_customer=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
					if(kd_customer ==='' ||kd_customer ==='Kelompok Pasien'){
						
					}else{
						getAdm(kd_customer);
					}
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viApotekResepRWI',
				handler: function()
				{
					var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
					if(dsDataGrdJab_viApotekResepRWI.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEKrwi/hapusBarisGridResepRWI",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viApotekResepRWI.removeAt(line);
													ResepRWI.form.Grid.a.getView().refresh();
													hasilJumlahResepRWI();
													Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
													Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
													Ext.getCmp('btnBayar_viApotekResepRWI').disable();
													hasilJumlahResepRWI();
													
												}
												else 
												{
													ShowPesanErrorResepRWI('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viApotekResepRWI.removeAt(line);
									ResepRWI.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorResepRWI('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viApotekResepRWI()
				]	
			}
		]
	};
    return items;
};

function setLookUp_bayarResepRWI(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_bayarResepRWI = new Ext.Window
    (
		{
			id: 'setLookUpApotek_bayarResepRWI',
			name: 'setLookUpApotek_bayarResepRWI',
			title: 'Pembayaran Resep Rawat Inap', 
			closeAction: 'destroy',        
			width: 523,
			height: 230,
			resizable:false,
			emptyText:'Pilih Jenis Pembayaran...',
			autoScroll: false,
			border: true,
			constrain : true,    
			iconCls: 'Studi_Lanjut',
			modal: true,		
			items: [ getItemPanelBiodataPembayaran_viApotekResepRWI(lebar,rowdata),
					 getItemPanelBiodataUang_viApotekResepRWI(lebar,rowdata)
				   ],//1
			listeners:
			{
				activate: function()
				{
					
				},
				afterShow: function()
				{
					this.activate();
					
					// ;
				},
				deactivate: function()
				{
					//rowSelected_viApotekResepRWI=undefined;
				}
			}
		}
    );

    setLookUpApotek_bayarResepRWI.show();
	
	Ext.getCmp('txtNamaPasien_PembayaranRWI').setValue(ResepRWI.form.ComboBox.namaPasien.getValue());
	
	Ext.getCmp('txtkdPasien_PembayaranRWI').setValue(Ext.getCmp('txtKdPasienApotekResepRWIL').getValue());
	Ext.getCmp('txtNoResepRWI_Pembayaran').setValue(Ext.get('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('dftanggalResepRWI_Pembayaran').setValue(now_viApotekResepRWI);
	stmpnoOut=Ext.getCmp('txtTmpNooutResepRWIL').getValue();
	stmptgl=Ext.getCmp('txtTmpTgloutResepRWIL').getValue();
	if(Ext.getCmp('txtTmpNooutResepRWIL').getValue() == 'No out' || Ext.getCmp('txtTmpNooutResepRWIL').getValue() == ''){
		Ext.getCmp('txtTotalResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
		Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWI_Pembayaran',
						id: 'txtNoResepRWI_Pembayaran',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalResepRWI_Pembayaran',
						id: 'dftanggalResepRWI_Pembayaran',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_PembayaranRWI',
						id: 'txtkdPasien_PembayaranRWI',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_PembayaranRWI',
						id: 'txtNamaPasien_PembayaranRWI',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrResepRWI(),
					mComboPembayaranRWI()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalResepRWI_Pembayaran',
						id: 'txtTotalResepRWI_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						width : 150,	
						//readOnly: true,
						style:{'text-align':'right'},
						name: 'txtBayarResepRWI_Pembayaran',
						id: 'txtBayarResepRWI_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranResepRWI();
								};
							}
						}
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btn1/2resep_viApotekResepRWI',
						handler:function()
						{
						}   
					}
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnBayar_ResepRWILookupBayar',
						handler:function()
						{
							pembayaranResepRWI();
						}   
					}
				]
            }
		]
	};
    return items;
};

function getItemGridHistoryBayar_viApotekResepRWI(lebar) 
{
    var items =
	{
		title: 'Paid History', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
	    //bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 95,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewHistoryBayar_viApotekResepRWI()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewHistoryBayar_viApotekResepRWI() 
{

    var fldDetail = ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
	
    dsTRDetailHistoryBayar = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLTRHistoryApotekRWI = new Ext.grid.EditorGridPanel
    (
        {
            store: dsTRDetailHistoryBayar,
            border: false,
            columnLines: true,
            height: 72,
			autoScroll:true,
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            //cellSelecteddeskripsiRWI = dsTRDetailHistoryBayarList.getAt(row);
                            //CurrentHistoryRWI.row = row;
                            //CurrentHistoryRWI.data = cellSelecteddeskripsiRWI;
                        }
                    }
                }
            ),
            stripeRows: true,
            columns:[
            			{
							id: 'colStatPost',
							header: 'Status Posting',
							dataIndex: 'TUTUP',
							width:100,
							hidden:true
						},
						{
							id: 'colKdPsien',
							header: 'Kode Pasien',
							dataIndex: 'KD_PASIENAPT',
							width:100,
							hidden:true
						},
						{
							id: 'colNoOut',
							header: 'No out',
							dataIndex: 'NO_OUT',
							width:100,
							hidden:true
						},
						{
							id: 'coleurutmasuk',
							header: 'Urut Bayar',
							dataIndex: 'URUT',
							align :'center',
							width:90
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Resep',
							dataIndex: 'TGL_OUT',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_OUT);
				
							} 
						},
						{
							id: 'colePembayaran',
							header: 'Pembayaran',
							dataIndex: 'URAIAN',
							align :'center',
							width:120,
							hidden:false
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Bayar',
							dataIndex: 'TGL_BAYAR',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_BAYAR);
				
							} 
						},
						{
							id: 'colJumlah',
							header: 'Total Bayar',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JUMLAH',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JUMLAH);
				
							}
							
						},
						{
							id: 'colJumlah',
							header: 'Jumlah Angsuran',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JML_TERIMA_UANG',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JML_TERIMA_UANG);
				
							}
							
						},
						{
							id: 'colJumlah',
							header: 'Sisa',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'SISA',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.SISA);
				
							}
							
						}
            ],
			viewConfig: {forceFit: true}
            //cm: TRHistoryColumModelApotekRWI(),
			
	 
		}
    );
    return gridDTLTRHistoryApotekRWI;
};

function TRHistoryColumModelApotekRWI() 
{
    return new Ext.grid.ColumnModel
    (
        [
          	{
				id: 'colStatPost',
				header: 'Status Posting',
				dataIndex: 'TUTUP',
				width:100,
				hidden:true
			},
			{
				id: 'colKdPsien',
				header: 'Kode Pasien',
				dataIndex: 'KD_PASIENAPT',
				width:100,
				hidden:true
			},
			{
				id: 'colNoOut',
				header: 'No out',
				dataIndex: 'NO_OUT',
				width:100,
				hidden:true
			},
			{
				id: 'coleurutmasuk',
				header: 'Urut Bayar',
				dataIndex: 'URUT',
				align :'center',
				width:90
				
			},
			{
				id: 'colTGlout',
				header: 'Tanggal Resep',
				dataIndex: 'TGL_OUT',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.TGL_BAYAR);
	
				} 
			},
			{
				id: 'colePembayaran',
				header: 'Pembayaran',
				dataIndex: 'URAIAN',
				align :'center',
				width:100,
				hidden:false
				
			},
			{
				id: 'colTGlout',
				header: 'Tanggal Bayar',
				dataIndex: 'TGL_BAYAR',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.TGL_BAYAR);
	
				} 
			},
			{
				id: 'colJumlah',
				header: 'Total Bayar',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JUMLAH',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.JUMLAH);
	
				}
				
			},
			{
				id: 'colJumlah',
				header: 'Jumlah Angsuran',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JML_TERIMA_UANG',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.JML_TERIMA_UANG);
	
				}
				
			},
			{
				id: 'colJumlah',
				header: 'Sisa',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'SISA',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.SISA);
	
				}
				
			}
			

        ]
    )
};

var a={};
function gridDataViewEdit_viApotekResepRWI(){
    chkSelected_viApotekResepRWI = new Ext.grid.CheckColumn({
		id: Nci.getId(),
		header: 'C',
		align: 'center',						
		dataIndex: 'cito',			
		width: 20
	});
	
    var FieldGrdKasir_viApotekResepRWI = [];
    dsDataGrdJab_viApotekResepRWI= new WebApp.DataStore({
        fields: FieldGrdKasir_viApotekResepRWI
    });
    
    ResepRWI.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekResepRWI,
        height: 210,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			chkSelected_viApotekResepRWI,
			{
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: ResepRWI.form.ArrayStore.a,
					select	: function(a,b,c){
						//'harga_beli','kd_pabrik','markup','tuslah','adm_racik'
						var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_prd=b.data.kd_prd;
						//dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_jual=b.data.harga_jual;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.markup=b.data.markup;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.adm_racik=b.data.adm_racik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jasa=b.data.jasa;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out=b.data.no_out;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_urut=b.data.no_urut;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.tgl_out=b.data.tgl_out;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=0;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik=0;
						
						ResepRWI.form.Grid.a.getView().refresh();
						//Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(b.data.adm_racik);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							kd_satuan		: o.kd_satuan,
							fractions		: o.fractions,
							harga_jual		: o.harga_jual,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							tuslah			: o.tuslah,
							adm_racik		: o.adm_racik,
							markup			: o.markup,
							jasa			: o.jasa,
							no_out			: o.no_out,
							no_urut			: o.no_urut,
							tgl_out			: o.tgl_out,
							kd_milik		: o.kd_milik,
							jml_stok_apt	: o.jml_stok_apt,
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="100" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue()
							}
					},
					url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 380
				})
			},
			{
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				width: 60
			},{
				dataIndex: 'racik',
				header: 'Racikan',
				width: 70,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik=a.getValue();
							hasilJumlahResepRWI();
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},{
				dataIndex: 'harga_jual',
				header: 'Harga Sat',
				sortable: true,
				xtype:'numbercolumn',
				align:'right',
				width: 85
			},{
				dataIndex: 'jml',
				header: 'Qty',
				sortable: true,
				xtype:'numbercolumn',
				width: 45,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()==''){
								ShowPesanWarningResepRWI('Qty obat belum di isi', 'Warning');
							}else{
								dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jml=a.getValue();
								hasilJumlahResepRWI();
							}
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			{
				dataIndex: 'disc',
				header: 'Diskon',
				sortable: true,
				xtype:'numbercolumn',
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=a.getValue();
							hasilJumlahResepRWI();
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},	
			{
				dataIndex: 'jumlah',
				header: 'Sub Total',
				sortable: true,
				width: 100,
				xtype:'numbercolumn',
				align:'right'
				/*renderer: function(v, params, record) {
				}	*/
			},
			{
				dataIndex: 'dosis',
				header: 'Dosis',
				width: 150,
				editor: new Ext.form.TextField({
					allowBlank: false
				})
			},
			//-------------- HIDDEN --------------
			{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'Kode Pabrik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'markup',
				header: 'Markup',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'adm_racik',
				header: 'Adm Racik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jasa',
				header: 'Jasa Tuslah',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_milik',
				header: 'kd milik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'stok tersedia',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
        plugins:chkSelected_viApotekResepRWI,
		viewConfig:{
			forceFit: true
		}
    });
    return ResepRWI.form.Grid.a;
}

function hasilJumlahResepRWI(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			if(o.jml <= o.jml_stok_apt){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				if(o.racik){
					if(isNaN(admRacik)){
						admRacik=0;
					} else {
						admRacik += parseFloat(o.adm_racik) * parseFloat(o.racik);
					}
				}
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=o.jml;
		}

	}
	
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(toFormat(total));
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	admprs=total*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall += parseFloat(total) + parseFloat(admRacik) + parseFloat(Ext.get('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.get('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));

	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}

function hasilJumlahResepRWILoad(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			if(o.jml <= o.jml_stok_apt){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=o.jml;
		}

	}
	admRacik = parseFloat(Ext.getCmp('txtAdmRacikResepRWIL').getValue())
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(toFormat(total));
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	admprs=total*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall += parseFloat(total) + parseFloat(admRacik) + parseFloat(Ext.getCmp('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));
	console.log(totalall);
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}


function ComboPilihanKelompokPasienApotekResepRWI()
{
	var Field_Customer = ['KD_CUSTOMER', 'JENIS_PASIEN'];
	ds_kelpas = new WebApp.DataStore({fields: Field_Customer});
    ds_kelpas.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboKelPasApotek',
					param: ''
				}
		}
	);
    var cboPilihankelompokPasienAptResepRWI = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
			id:'cboPilihankelompokPasienAptResepRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			readOnly:true,
			emptyText:'Kelompok Pasien',
			width: 150,
			store: ds_kelpas,
			valueField: 'KD_CUSTOMER',
			displayField: 'JENIS_PASIEN',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return cboPilihankelompokPasienAptResepRWI;
};

function ComboDokterApotekResepRWI()
{
    var Field_Dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_Dokter});
    ds_dokter.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_dokter',
					Sortdir: 'ASC',
					target: 'ComboDokterApotek',
					param: ''
				}
		}
	);
	
    var cbo_DokterApotekResepRWI = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_DokterApotekResepRWI',
			valueField: 'KD_DOKTER',
            displayField: 'NAMA',
			emptyText:'Dokter',
			store: ds_dokter,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			readOnly:true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetDokter=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_DokterApotekResepRWI;
};

function mComboStatusPostingApotekResepRWI(){
  var cboStatusPostingApotekResepRWI = new Ext.form.ComboBox({
        id:'cboStatusPostingApotekResepRWI',
        x: 410,
        y: 30,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 110,
        emptyText:'',
        fieldLabel: 'JENIS',
		tabIndex:5,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:selectCountStatusPostingApotekResepRWI,
        listeners:{
			'select': function(a,b,c){
				selectCountStatusPostingApotekResepRWI=b.data.displayText ;
				tmpkriteria = getCriteriaCariApotekResepRWI();
				refreshRespApotekRWI(tmpkriteria);
			}
        }
	});
	return cboStatusPostingApotekResepRWI;
};

function ComboUnitApotekResepRWI(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100' ORDER BY nama_unit"
        }
    });
    var cbo_UnitRWI = new Ext.form.ComboBox({
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_UnitResepRWI',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariApotekResepRWI();
						refreshRespApotekRWI(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitRWI;
}

function viCombo_VendorLookup(){
    var cbo_UnitRWILookup = new Ext.form.ComboBox({
        flex: 1,
		id: 'cbo_UnitRWILookup',
        fieldLabel: 'Vendor',
		valueField: 'KD_VENDOR',
        displayField: 'VENDOR',
		emptyText:'PBF',
		store: ds_unit,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
		width:190,
		tabIndex:2,
		listeners:{ 
			'select': function(a,b,c){
				selectSetUnit=b.data.displayText ;
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13) {
					
				} 						
			}
		}
    })  ;  
    return cbo_UnitRWILookup;
}

function ComboUnitResepRWILookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100' ORDER BY nama_unit"
				}
		}
	);
	
    var cbo_Unit = new Ext.form.ComboBox
    (
        {
			id: 'cbo_UnitResepRWIL',
            fieldLabel: 'Unit',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
			readOnly: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					//selectSetUnit=b.data.valueField;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						/* tmpkriteria = getCriteriaCariApotekResepRWJ();
						refeshRespApotekRWJ(tmpkriteria); */
					} 						
				}
			}
        }
    )    
    return cbo_Unit;
}

function mComboJenisByrResepRWI() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrResepRWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
					Ext.getCmp('cboPembayaranRWI').enable();
					/* 
					loaddatastorePembayaran(b.data.JENIS_PAY);
					tampungtypedata=b.data.TYPE_DATA;
					jenispay=b.data.JENIS_PAY;
					showCols(Ext.getCmp('gridDTLTRKasirrwj'));
					hideCols(Ext.getCmp('gridDTLTRKasirrwj'));
					getTotalDetailProduk();
					Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';

					*/
			   
				}
				  

			}
		}
	);
	
    return cboJenisByr;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaranRWI()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaranRWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			disabled:true,
			width:225,
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				}
				  

			}
		}
	);

    return cboPembayaran;
};

function ViewDetailPembayaranObatRWI(no_out,tgl_out) 
{	
    var strKriteriaRWI='';
    strKriteriaRWI = "a.no_out = " + no_out + "" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayar.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWI
		}
	}); 
	return dsTRDetailHistoryBayar;
	
    
};


function datainit_viApotekResepRWI(rowdata)
{
    //AddNewPenJasRad = false;
	//console.log(rowdata);
	dataisi=1;
	Ext.getCmp('txtNoResepApotekResepRWIL').setValue(rowdata.NO_RESEP);
	Ext.getCmp('cbo_UnitResepRWIL').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(rowdata.JENIS_PASIEN);
	Ext.getCmp('txtKdPasienApotekResepRWIL').setValue(rowdata.KD_PASIENAPT);
	ResepRWI.form.ComboBox.namaPasien.setValue(rowdata.NMPASIEN);
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(rowdata.NO_KAMAR);
	Ext.getCmp('txtKamarApotekResepRWIL').setValue(rowdata.NAMA_KAMAR);
	
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNooutResepRWIL').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTgloutResepRWIL').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpSisaAngsuranResepRWIL').setValue(rowdata.SISA);
	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(rowdata.STATUS_POSTING);
	
	if(rowdata.ADMPRHS ==null){
		Ext.getCmp('txtAdmPrshResepRWIL').setValue(0);
	}else{
		Ext.getCmp('txtAdmPrshResepRWIL').setValue(rowdata.ADMPRHS);
	}
	if(rowdata.ADMRESEP == null){
		Ext.getCmp('txtAdmResepRWIL').setValue(0);
	}else{
		Ext.getCmp('txtAdmResepRWIL').setValue(rowdata.ADMRESEP);
	}
	if(rowdata.JASA == null){
		Ext.getCmp('txtTuslahEmbalaseL').setValue(0);
	}else{
		Ext.getCmp('txtTuslahEmbalaseL').setValue(rowdata.JASA);
	}
	
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekResepRWI').enable();
		Ext.getCmp('btnPrint_viResepRWI').enable();
		Ext.getCmp('btnBayar_viApotekResepRWI').disable();
		Ext.getCmp('btnAdd_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
		Ext.getCmp('btnDelete_viApotekResepRWI').disable();
		Ext.getCmp('btnAddObatRWI').disable();
		ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
			
	} else{
		Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
		Ext.getCmp('btnunposting_viApotekResepRWI').disable();
		Ext.getCmp('btnPrint_viResepRWI').disable();
		Ext.getCmp('btnBayar_viApotekResepRWI').enable();
		Ext.getCmp('btnDelete_viApotekResepRWI').enable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWI').enable();
		Ext.getCmp('btnAddObatRWI').enable();
		ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
	//dataGridObatApotekResepRWJ(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	getSeCoba(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	Ext.Ajax.request(
	{
	   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			ResepRWI.form.Panel.shift.update(cst.shift);
		}
	
	});
	
	//Ext.getCmp('tshift').setValue(tampungshiftsekarang)
};


function getAdm(kd_customer){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getAdm",
			params: {kd_customer:kd_customer},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmResepRWIL').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(cst.tuslah));
				}
				else 
				{
					Ext.getCmp('txtAdmResepRWIL').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(cst.tuslah));
					//ShowPesanErrorResepRWI('Gagal mengambil tuslah dan adm', 'Error');
				};
			}
		}
		
	)
	
}
function getSeCoba(no_out,tgl_out,admracik){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/sess",
			params: {query:"no_out = " + no_out},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd=cst.session;
					dataGridObatApotekResepRWI(no_out,tgl_out,admracik,kd);
				}
			}
		}
	);
}

function pembayaranResepRWI(){
	if(ValidasiBayarResepRWI(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/bayarSaveResepRWI",
				params: getParamBayarResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						
						if(toInteger(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalResepRWI_Pembayaran').getValue())){
							ShowPesanInfoResepRWI('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('btnBayar_ResepRWILookupBayar').disable();
							Ext.getCmp('txtNoResepApotekResepRWIL').setValue(cst.noresep);
							Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
							Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekResepRWI').enable();
							Ext.getCmp('btnPrint_viResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnAdd_viApotekResepRWI').disable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
							Ext.getCmp('btnDelete_viApotekResepRWI').disable();
							Ext.getCmp('btnAddObatRWI').disable();
							//Ext.getCmp('txtBayarResepRWI_Pembayaran').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
							ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ViewDetailPembayaranObatRWI(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekRWI.getView().refresh();
							refreshRespApotekRWI();
							setLookUpApotek_bayarResepRWI.close();
							
							
						} else {
							refreshRespApotekRWI();
							ShowPesanInfoResepRWI('Berhasil melakukan pembayaran','Information');
							setLookUpApotek_bayarResepRWI.close();
							Ext.getCmp('btnBayar_viApotekResepRWI').enable();
							Ext.getCmp('btnunposting_viApotekResepRWI').disable();
							Ext.getCmp('btnPrint_viResepRWI').disable();
							ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
							gridDTLTRHistoryApotekRWI.getView().refresh();
						}
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal melakukan pembayaran', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function dataGridObatApotekResepRWI(no_out,tgl_out,admracik,kd){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/viewdetailobat/read",
			params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' and s.kd_unit_far='"+kd+"' "},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWI.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdJab_viApotekResepRWI.add(recs);
					
					ResepRWI.form.Grid.a.getView().refresh();
					Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admracik));
					hasilJumlahResepRWILoad();
					
				}
				else 
				{
					ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//console.log(cst);
				if (cst.success === true) 
				{
					console.log(cst.sisa);
					if(cst.sisa ==''){
						ShowPesanInfoResepRWI('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						Ext.getCmp('txtTotalResepRWI_Pembayaran').setValue(toFormat(cst.sisa));
						Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(cst.sisa);
						
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
					Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
					//ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
					alert();
				};
			}
		}
		
	)
}


function datasave_viApotekResepRWI(){
	if (ValidasiEntryResepRWI(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveResepRWI",
				params: getParamResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoResepRWI(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtNoResepApotekResepRWIL').dom.value=cst.noresep;
						Ext.get('txtTmpNooutResepRWIL').dom.value=cst.noout;
						Ext.get('txtTmpTgloutResepRWIL').dom.value=cst.tgl;
						Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
						refreshRespApotekRWI();
						Ext.getCmp('btnBayar_viApotekResepRWI').enable();
						if(mBol === false)
						{
							
						};
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorResepRWI('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWI('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamResepRWI() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perseorangan'){
			KdCust='0000000001';
	}else if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}else {
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}
	
	/* if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	} */
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='Nomor Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		KdPasien:Ext.getCmp('txtKdPasienApotekResepRWIL').getValue(),
		NmPasien:ResepRWI.form.ComboBox.namaPasien.getValue(),		
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),
		//NonResep:tmpNonResep,
		NoResepAsal:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPostResepRWIL').getValue(),
		Tanggal:tanggal,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmResepRWIL').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPrshResepRWIL').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:ResepRWI.vars.no_transaksi,
		KdKasirAsal:ResepRWI.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
		NoKamar:Ext.getCmp('txtNoKamarApotekResepRWIL').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Posting:0
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.cito;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
		
	}
	
    return params
};

function getParamBayarResepRWI() 
{
	
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perseorangan'){
			KdCust='0000000001';
	}else if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}else {
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='Nomor Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoresep='';
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === 'No Resep' ){
		LangsungPost = 1;
		tmpNoresep='';
	}else {
		LangsungPost = 0;
		tmpNoresep = Ext.getCmp('txtNoResepRWI_Pembayaran').getValue();
	}

	var params =
	{
		NmPasien:ResepRWI.form.ComboBox.namaPasien.getValue(),
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),
		//NonResep:tmpNonResep,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmResepRWIL').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPrshResepRWIL').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:ResepRWI.vars.no_transaksi,
		KdKasirAsal:ResepRWI.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasien_PembayaranRWI').getValue(),
		KdPay:Ext.getCmp('cboPembayaranRWI').getValue(),
		JumlahTotal:toInteger(Ext.getCmp('txtTotalResepRWI_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue()),
		NoResep:tmpNoresep,
		TanggalBayar:Ext.getCmp('dftanggalResepRWI_Pembayaran').getValue(),
		NoKamar:Ext.getCmp('txtNoKamarApotekResepRWIL').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Posting:LangsungPost,
		Shift: tampungshiftsekarang,
		Tanggal:tanggal,
		Ubah:ubah
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
	}
	
    return params
};

function getParamUnpostingResepRWI() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
	}
	
    return params
}

function getParamDeleteHistoryResepRWI() 
{
	var urut=dsTRDetailHistoryBayar.getRange()[gridDTLTRHistoryApotekRWI.getSelectionModel().selection.cell[0]].data.URUT;
	
	var params =
	{
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		urut:urut
	}
	
	
	
    return params
}


function datacetakbill(){
	var params =
	{
		Table: 'billprintingreseprwi',
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienApotekResepRWIL').getValue(),
		NamaPasien:ResepRWI.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptResepRWI').getValue(),
		Kelas:Ext.get('cbo_UnitResepRWIL').getValue(),
		Dokter:Ext.get('cbo_DokterApotekResepRWI').getValue(),
		Total:Ext.get('txtTotalBayarResepRWIL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarResepRWIL').getValue())
		
	}
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariApotekResepRWI()//^^^
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue()!=='No. Resep')
            {
                strKriteria = " o.no_resep " + "LIKE upper('%'" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasienResepRWI').getValue() != "" && Ext.get('txtKdNamaPasienResepRWI').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasienResepRWI').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " o.kd_pasienapt " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " lower(o.nmpasien) " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " lower(o.nmpasien) " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekResepRWI').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekResepRWI').getValue() != "" && Ext.get('dfTglAkhirApotekResepRWI').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWI').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWI').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWI').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWI').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_UnitResepRWI').getValue() != "" && Ext.get('cbo_UnitResepRWI').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_UnitResepRWI').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_UnitResepRWI').getValue() + "'";
			    }
                
            }
	
		strKriteria= strKriteria + " and u.parent='100' and o.returapt=0 order by o.tgl_out limit 20"
	 return strKriteria;
}


function ValidasiEntryResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(ResepRWI.form.ComboBox.namaPasien.getValue() === '' || dsDataGrdJab_viApotekResepRWI.getCount() === 0 || Ext.getCmp('txtKdPasienApotekResepRWIL').getValue() === ''){
		if(ResepRWI.form.ComboBox.namaPasien.getValue() === ''){
			ShowPesanWarningResepRWI('Nama Pasien', 'Warning');
			x = 0;
		} else if(dsDataGrdJab_viApotekResepRWI.getCount() == 0){
			ShowPesanWarningResepRWI('Daftar resep obat belum di isi', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningResepRWI('Kode Pasien', 'Warning');
			x = 0;
		} 
	}
	
	if((Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === '0' || Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === 0) ||
		(Ext.getCmp('txtTotalBayarResepRWIL').getValue() === '0' || Ext.getCmp('txtTotalBayarResepRWIL').getValue() === 0)){
		if(Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === '0' || Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === 0){
			ShowPesanWarningResepRWI('Total harga obat kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningResepRWI('Total bayar kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		}
	}
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml == undefined || o.jumlah == undefined){
			if(o.jml == undefined){
				ShowPesanWarningResepRWI('Qty belum di isi, qty tidak boleh kosong', 'Warning');
				x = 0;
			}else if(o.jumlah == undefined){
				ShowPesanWarningResepRWI('Total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
				x = 0;
			}
		}
		if(o.jml > o.jml_stok_apt){
			ShowPesanWarningResepRWI('Qty melebihi stok', 'Warning');
			o.jml=o.jml_stok_apt;
			x = 0;
		}
	}
	
	return x;
};

function ValidasiBayarResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrResepRWI').getValue() === '' || Ext.getCmp('cboJenisByrResepRWI').getValue() === 'TUNAI'){
		ShowPesanWarningResepRWI('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaranRWI').getValue() === '' || Ext.getCmp('cboPembayaranRWI').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningResepRWI('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue() === ''){
		ShowPesanWarningResepRWI('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};


function ShowPesanWarningResepRWI(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorResepRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoResepRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};