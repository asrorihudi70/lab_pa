var dataSource_viTutupShift;
var selectCount_viTutupShift=50;
var NamaForm_viTutupShift="Tutup Shift Apotek";
var selectCountStatusPostingTutupShift='Semua';
var mod_name_viTutupShift="viTutupShift";
var now_viTutupShift= new Date();
var addNew_viTutupShift;
var rowSelected_viTutupShift;
var setLookUps_viTutupShift;
var tanggal = now_viTutupShift.format("d/M/Y");
var jam = now_viTutupShift.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var DataGridJenisObatRetur;
var DataGridJenisObatResep;


var CurrentData_viTutupShift =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = data_viTutupShift(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var TutupShift={};
TutupShift.form={};
TutupShift.func={};
TutupShift.vars={};
TutupShift.func.parent=TutupShift;
TutupShift.form.ArrayStore={};
TutupShift.form.ComboBox={};
TutupShift.form.DataStore={};
TutupShift.form.Record={};
TutupShift.form.Form={};
TutupShift.form.Grid={};
TutupShift.form.Panel={};
TutupShift.form.TextField={};
TutupShift.form.Button={};

TutupShift.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','nama_obat','kd_milik', 'kd_unit_far', 'batch'],
	data: []
});

function data_viTutupShift(mod_id_viTutupShift){
current_viTutupShift();	
   	var panelTutupShift = new Ext.FormPanel({
        labelAlign: 'top',
        title: '',
        bodyStyle:'padding:5px 5px 0',
		border: false,
        items: [
		{
			layout: 'hbox',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					title:'Tutup Shift',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 300,
					height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Shift Ke'
						},
						{
							x: 120,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 10,
							xtype: 'textfield',
							id: 'TxtShiftSekarang',
							name: 'TxtShiftSekarang',
							readOnly: true,
							emptyText: '',
							width: 150,
							tabIndex:1
						},
						//----------------------------------------
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Shift Selanjutnya'
						},
						{
							x: 120,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 40,
							xtype: 'textfield',
							id: 'TxtShiftSelanjutnya',
							name: 'TxtShiftSelanjutnya',
							readOnly: true,
							emptyText: '',
							width: 150,
							tabIndex:2
						},
						//----------------------------------------
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Tanggal Shift'
						},
						{
							x: 120,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 70,
							xtype: 'datefield',
							id: 'dfShiftSekarang',
							name: 'dfShiftSekarang',
							format: 'd/M/Y',
							value:now_viTutupShift,
							width: 150,
							tabIndex:3
						},
						
						//----------------------------------------
						{
							x: 180,
							y: 110,
							xtype: 'button',
							text: 'Ok',
							style:{paddingLeft:'30px'},
							width:130,
							id: 'BtnOk_viTutupShift',
							handler: function() 
							{	
								loadMask.show();
								tutupShift_viTutupShift()
							}                        
						}
					]
				},
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 200,
					anchor: '100% 100%'
				},
				{
					layout: 'absolute',
					title:'Ketentuan tutup shift',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 400,
					height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							xtype: 'textarea',
							name: 'txtKetentuanTutupShift',
							id: 'txtKetentuanTutupShift',
							width : 398,
							height : 175,
							tabIndex:3,
							readOnly: true,
							value:'Sebelum shift di tutup, pastikan semua transaksi sudah diTutup / diPosting.'
						}
					]
				},
				{
					layout: 'absolute',
					//title:'Ketentuan tutup shift',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 130,
					height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							x: 0,
							y: 176,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							style:{paddingLeft:'30px'},
							width:130,
							id: 'BtnRefresh_viTutupShift',
							handler: function() 
							{	
								loadMask.show();
								cekTransaksi_viTutupShift();	
							}                        
						}
					]
				}
			]
		},
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 50,
					height: 20,
					anchor: '100% 100%'
				}
			]
		},
		{
			layout: 'hbox',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					title:'Transaksi Resep yang belum ditutup',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 465,
					height: 280,
					anchor: '100% 100%',
					items:
					[
						gridTransaksiResep_viTutupShift()
					]
				},
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 260,
					anchor: '100% 100%'
				},
				{
					layout: 'absolute',
					title:'Transaksi Retur yang belum ditutup',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 465,
					height: 280,
					anchor: '100% 100%',
					items:
					[
						gridTransaksiRetur_viTutupShift()
					]
				}
			]
		}
		
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viTutupShift = new Ext.Panel
    (
		{
			title: NamaForm_viTutupShift,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viTutupShift,
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ panelTutupShift]
       }
    )
    return FrmFilterGridDataView_viTutupShift;
    //-------------- # End form filter # --------------
}

function gridTransaksiResep_viTutupShift(){
    var FieldGrdTransaksi_viTutupShift = ['no_resep','kd_pasienapt', 'nmpasien','nama_unit'];
    
	dsDataGrdTransaksiResep_viTutupShift= new WebApp.DataStore({
		fields: FieldGrdTransaksi_viTutupShift
    });   
   
    DataGridJenisObatResep =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dsDataGrdTransaksiResep_viTutupShift,
        height: 250,
		width:500,
        columnLines: true,
		border: false,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				//rowSelected_viSetupObatAlkes = dsDataGrdTransaksiResep_viTutupShift.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_resep',
				header: 'No Resep',
				sortable: true,
				width: 40
			},
			{
				dataIndex: 'kd_pasienapt',
				header: 'No Medrec',
				width: 35
			},
			{
				dataIndex: 'nmpasien',
				header: 'Nama',
				width: 75
			},
			{
				dataIndex: 'nama_unit',
				header: 'Unit',
				width: 60
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridJenisObatResep;
}

function gridTransaksiRetur_viTutupShift(){
    var FieldGrdTransaksiRetur_viTutupShift = ['no_resep','kd_pasienapt', 'nmpasien','nama_unit'];
    
	dsDataGrdTransaksiRetur_viTutupShift= new WebApp.DataStore({
		fields: FieldGrdTransaksiRetur_viTutupShift
    });   
   
    DataGridJenisObatRetur =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dsDataGrdTransaksiRetur_viTutupShift,
        height: 250,
		width:500,
        columnLines: true,
		border: false,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				//rowSelected_viSetupObatAlkes = dsDataGrdTransaksiRetur_viTutupShift.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_resep',
				header: 'No Resep',
				sortable: true,
				width: 40
			},
			{
				dataIndex: 'kd_pasienapt',
				header: 'No Medrec',
				width: 35
			},
			{
				dataIndex: 'nmpasien',
				header: 'Nama',
				width: 75
			},
			{
				dataIndex: 'nama_unit',
				header: 'Unit',
				width: 60
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridJenisObatRetur;
}

function refreshTransaksiTutupShift(kriteria)
{
    dsDataGrdTransaksi_viTutupShift.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridJenisObat',
					param: kriteria
				}
		}
	);    
    return dataSource_viTutupShift;
}

function current_viTutupShift(){
	Ext.Ajax.request(
	{
	   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			
			Ext.getCmp('TxtShiftSekarang').setValue(cst.shift)
			if(cst.shift == '1' || cst.shift == 1){
				Ext.getCmp('TxtShiftSelanjutnya').setValue(2)
			} else if(cst.shift == '2' || cst.shift == 2){
				Ext.getCmp('TxtShiftSelanjutnya').setValue(3)
			} else if(cst.shift == '3' || cst.shift == 3){
				Ext.getCmp('TxtShiftSelanjutnya').setValue(1)
			}
			cekTransaksi_viTutupShift();
		}
	
	});
}

function cekTransaksi_viTutupShift(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionTutupShift/cekTransaksi",
			params: getParamCekTransaksi(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorTutupShift('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					dsDataGrdTransaksiResep_viTutupShift.removeAll();
					dsDataGrdTransaksiRetur_viTutupShift.removeAll();
					//-----------------------Retur--------------------------------------------------
					var recs=[],
						recType=dsDataGrdTransaksiRetur_viTutupShift.recordType;
						
					for(var i=0; i<cst.ListDataObj.qRetur.length; i++){
						
						recs.push(new recType(cst.ListDataObj.qRetur[i]));
						
					}
					dsDataGrdTransaksiRetur_viTutupShift.add(recs);
					
					//-----------------------Resep--------------------------------------------------
					var recs=[],
						recType=dsDataGrdTransaksiResep_viTutupShift.recordType;
						
					for(var i=0; i<cst.ListDataObj.qResep.length; i++){
						
						recs.push(new recType(cst.ListDataObj.qResep[i]));
						
					}
					dsDataGrdTransaksiResep_viTutupShift.add(recs);
				}
				
			}
		}
		
	)
}

function tutupShift_viTutupShift(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionTutupShift/tutupShift",
			params: getParamTutupShift(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorTutupShift('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				current_viTutupShift();	
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoTutupShift('Berhasil melakukan tutup shift','Information');
				}
				else if(cst.success === false && cst.pesan ==='Transaksi belum posting')
				{
					loadMask.hide();
					ShowPesanWarningTutupShift('Tidak dapat melakukan tutup shift, transaksi masih ada yang belum diTutup','Warning');
					
				} else if(cst.success === false && cst.pesan ==='Error'){
					loadMask.hide();
					ShowPesanErrorTutupShift('Tutup shift gagal dilakukan','Error');
				};
			}
		}
		
	)
}

function getParamTutupShift(){
	var	params =
	{
		tanggal:Ext.getCmp('dfShiftSekarang').getValue(),
		shiftKe:Ext.getCmp('TxtShiftSekarang').getValue(),
		shiftSelanjutnya:Ext.getCmp('TxtShiftSelanjutnya').getValue()
	}
   
    return params
};

function getParamCekTransaksi(){
	var	params =
	{
		tanggal:Ext.getCmp('dfShiftSekarang').getValue(),
		shiftKe:Ext.getCmp('TxtShiftSekarang').getValue(),
		shiftSelanjutnya:Ext.getCmp('TxtShiftSelanjutnya').getValue()
	}
   
    return params
};



function ShowPesanWarningTutupShift(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorTutupShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoTutupShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};