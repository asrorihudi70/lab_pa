var dataSource_viSetupTarifComponent;
var selectCount_viSetupTarifComponent=50;
var NamaForm_viSetupTarifComponent="Setup Tarif Customer";
var selectCountStatusPostingSetupTarifComponent='Semua';
var mod_name_viSetupTarifComponent="viSetupTarifComponent";
var now_viSetupTarifComponent= new Date();
var rowSelected_viSetupTarifComponent;
var setLookUps_viSetupTarifComponent;
var tanggal = now_viSetupTarifComponent.format("d/M/Y");
var jam = now_viSetupTarifComponent.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var DataGridMasterComponent;
var DataGridComponent;
var unit;


var CurrentData_viSetupTarifComponent =
{
	data: Object,
	details: Array,
	row: 0
};



var SetupTarifComponent={};
SetupTarifComponent.form={};
SetupTarifComponent.func={};
SetupTarifComponent.vars={};
SetupTarifComponent.func.parent=SetupTarifComponent;
SetupTarifComponent.form.ArrayStore={};
SetupTarifComponent.form.ComboBox={};
SetupTarifComponent.form.DataStore={};
SetupTarifComponent.form.Record={};
SetupTarifComponent.form.Form={};
SetupTarifComponent.form.Grid={};
SetupTarifComponent.form.Panel={};
SetupTarifComponent.form.TextField={};
SetupTarifComponent.form.Button={};
SetupTarifComponent.form.DropDown={
	milik:null,
	unit:null,
	poli_unit:null
};

SetupTarifComponent.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_gol', 'ket_gol'],
	data: []
});

SetupTarifComponent.form.ArrayStore.data1=new Ext.data.ArrayStore({fields:[]});
SetupTarifComponent.form.ArrayStore.data2=new Ext.data.ArrayStore({fields:[]});

CurrentPage.page = data_viSetupTarifComponent(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

shortcut.set({
	code:'main',
	list:[
		{
			key:'ctrl+s',
			fn:function(){
				Ext.getCmp('btnSimpan_viSetupTarifComponent').el.dom.click();
			}
		},
		{
			key:'ctrl+d',
			fn:function(){
				Ext.getCmp('btnDelete_viSetupTarifComponent').el.dom.click();
			}
		}
	]
})

function data_viSetupTarifComponent(mod_id_viSetupTarifComponent){
getMasterCustomer_viSetupTarifComponent();
getMilikCombo_viSetupTarifComponent();

   	var panelSetupTarifComponent = new Ext.FormPanel({
        labelAlign: 'top',
        title: 'Setup Kode Tarif',
        bodyStyle:'padding:5px 5px 0',
		border: false,
        items: [
		{
			layout: 'hbox',
			border: false,
			items:
			[
				{
					title:'',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 450,
					height: 100,
					anchor: '100% 100%',
					items:
					[
						Q().input({
							label:'Kepemilikan',
							items:[
								SetupTarifComponent.form.DropDown.milik=Q().dropdown({
									width:150
								})
							]
						}),
						Q().input({
							label:'Unit',
							items:[
								SetupTarifComponent.form.DropDown.unit=Q().dropdown({
									data:[
											{id:'1',text:'Rawat Inap'},
											{id:'2',text:'Rawat Jalan'},
											{id:'3',text:'Gawat Darurat'}
									],
									width:150,
									select: function(id){
										if(id.getValue() == '1'){
											unit='1';
											getUnitCombo_viSetupTarifComponent(unit);
										} else if(id.getValue() == '2'){
											unit='2';
											getUnitCombo_viSetupTarifComponent(unit);
										} else{
											unit='3';
											getUnitCombo_viSetupTarifComponent(unit);
										}
										
									}
								})
							]
						}),
						Q().input({
							label:'',
							items:[
								SetupTarifComponent.form.DropDown.poli_unit=Q().dropdown({
									width:180,
									select: function(id){
										SetupTarifComponent.form.ArrayStore.data1.removeAll();
										
										var kd_milik;
										kd_milik=SetupTarifComponent.form.DropDown.milik.getValue();
										Ext.getCmp('TxtTotalSetupTarifComponent').setValue('');
										getComponent_viSetupTarifComponent(id.getValue(),kd_milik);
										
										
									}
									
								})
							]
						})
						
					]
				}
			]
		},
		{
			layout: 'hbox',
			align:'stretch',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					title:'Component',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 475,
					height: 270,
					anchor: '100% 100%',
					items:
					[
						gridComponent_viSetupTarifComponent()
					]
				},
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 230,
					anchor: '100% 100%'
				},
				{
					layout: 'absolute',
					title:'Master Component',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 450,
					height: 270,
					anchor: '100% 100%',
					items:
					[
						gridMasterComponent_viSetupTarifComponent()
					]
				},{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 230,
					anchor: '100% 100%'
				},
				{
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaSetupTarifComponentApotek',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaSetupTarifComponentApotek').getValue()===true)
                                                {
                                                     DataGridMasterComponent.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    DataGridMasterComponent.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
			],
			tbar: 
			{
				xtype: 'toolbar',
				border:true,
				items: 
				[
					/* {
						xtype: 'button',
						text: 'Save Customer',
						iconCls: 'save',
						id: 'btnSimpanCustomer_viSetupTarifComponent',
						handler: function()
						{
							loadMask.show();
							dataSaveCustomer_viSetupTarifComponent();
						}
					},
					{
						xtype: 'tbseparator'
					} */
					
					
				]
			
			}
		}/* ,
		{
			layout: 'hbox',
			align:'stretch',
			border: false,
			items:
			[
				{
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 460,
					height: 10,
					anchor: '100% 100%',
					items:
					[
						
						
					]
				}
			]
		}, */
		/* ,{
			layout: 'hbox',
			border: false,
			items:
			[
				{
					title:'',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 475,
					height: 40,
					anchor: '100% 100%',
					items:
					[
						
						
					]
				}
			]
		} */
		
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viSetupTarifComponent = new Ext.Panel
    (
		{
			title: NamaForm_viSetupTarifComponent,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupTarifComponent,
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ panelSetupTarifComponent],
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupTarifComponent',
						handler: function(){
							dataAddNewTarifCustomer();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupTarifComponent',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupTarifComponent();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupTarifComponent',
						handler: function()
						{
							var line = DataGridComponent.getSelectionModel().selection.cell[0];
							var o = SetupTarifComponent.form.ArrayStore.data1.getRange()[line].data;
							Ext.Msg.confirm('Warning', 'Apakah data component ini akan dihapus?', function(button){
								if (button == 'yes'){
									if(SetupTarifComponent.form.ArrayStore.data1.getRange()[line].data.percent_compo != undefined){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionSetupTarifComponent/delete",
												params:{kd_unit:SetupTarifComponent.form.DropDown.poli_unit.getValue(),kd_milik:SetupTarifComponent.form.DropDown.milik.getValue(),kd_component:o.kd_component} ,
												failure: function(o)
												{
													ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoSetupTarifComponent('Berhasil melakukan penghapusan', 'Information');
														SetupTarifComponent.form.ArrayStore.data1.removeAt(line);
														DataGridComponent.getView().refresh();
														hasilJumlah();
													}
													else 
													{
														ShowPesanErrorSetupTarifComponent('Gagal melakukan penghapusan', 'Error');
													};
												}
											}
											
										)
									}else{
										SetupTarifComponent.form.ArrayStore.data1.removeAt(line);
										DataGridComponent.getView().refresh();
										
									}
								} 
								
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupTarifComponent',
						handler: function()
						{
							SetupTarifComponent.form.ArrayStore.data1.removeAll();
							SetupTarifComponent.form.ArrayStore.data2.removeAll();
							dataAddNewTarifCustomer();
							getMasterCustomer_viSetupTarifComponent();
							
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			
			}
       }
    )
    return FrmFilterGridDataView_viSetupTarifComponent;
    //-------------- # End form filter # --------------
}


function gridComponent_viSetupTarifComponent(){
    DataGridComponent =new Ext.grid.EditorGridPanel({
		ddGroup          : 'secondGridDDGroup',
		autoScroll       : true,
		store			 : SetupTarifComponent.form.ArrayStore.data1,
        height			 : 250,
		width			 : 470,
        columnLines		 : true,
		stripeRows       : true,
		trackMouseOver   : true,
		enableDragDrop   : true,
		border			 : false,
		flex			 :1,
		listeners : {
			afterrender : function(comp) {
				var secondGridDropTargetEl = DataGridComponent.getView().scroller.dom;
				var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'firstGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records =  ddSource.dragData.selections;
						Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						DataGridComponent.store.add(records);
						DataGridComponent.store.sort('kd_customer', 'ASC');
						return true
					}
				});
			}
		},
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_component',
				header: 'Kode',
				width: 40
			},
			{
				dataIndex: 'component',
				header: 'Component',
				width: 80
			},
			{
				dataIndex: 'jenis',
				header: 'Jenis',
				width: 80
			},
			{
				dataIndex: 'percent_compo',
				header: 'Percent',
				width: 50,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()==''){
								ShowPesanWarningSetupTarifComponent('Percent belum diisi', 'Warning');
							}else{
								SetupTarifComponent.form.ArrayStore.data1.getRange()[line].data.percent_compo=a.getValue();
								DataGridComponent.getView().refresh();
								var jum=0;
								for(var i=0; i<SetupTarifComponent.form.ArrayStore.data1.getRange().length ; i++){
									if(isNaN(parseInt(SetupTarifComponent.form.ArrayStore.data1.getRange()[i].data.percent_compo))){
										SetupTarifComponent.form.ArrayStore.data1.getRange()[i].data.percent_compo=0;
									}else{
										jum+=parseInt(SetupTarifComponent.form.ArrayStore.data1.getRange()[i].data.percent_compo);
									}
								}
								if(jum>100){
									ShowPesanWarningSetupTarifComponent('Jumlah persen tidak boleh lebih dari 100','Warning');
									SetupTarifComponent.form.ArrayStore.data1.getRange()[line].data.percent_compo=0;
									Ext.getCmp('TxtTotalSetupTarifComponent').setValue(jum-parseInt(a.getValue()));
									DataGridComponent.getView().refresh();
								} else{
									Ext.getCmp('TxtTotalSetupTarifComponent').setValue(jum);
								}
							}
						},
						focus: function(a){
							this.index=DataGridComponent.getSelectionModel().selection.cell[0]
						}
					}
				})
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		},
		fbar:[
			Q().input({
							label:'Total',
							labelAlign:'right',
							width: 205,
							items:[
							{	
								xtype: 'textfield',
								id: 'TxtTotalSetupTarifComponent',
								name: 'TxtTotalSetupTarifComponent',
								style:'text-align:right;',
								readOnly: true,
								emptyText: '',
								width: 80
							}
							]
						})
		]
    });
    return DataGridComponent;
}

function gridMasterComponent_viSetupTarifComponent(){
    DataGridMasterComponent =new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		store			 : SetupTarifComponent.form.ArrayStore.data2,
		autoScroll       : true,
        height			 : 245,
		width			 : 445,
        columnLines		 : true,
		stripeRows       : true,
		trackMouseOver   : true,
		enableDragDrop   : true,
		border			 : false,
		listeners : {
			afterrender : function(comp) {
				var secondGridDropTargetEl = DataGridMasterComponent.getView().scroller.dom;
				var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records =  ddSource.dragData.selections;
						Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						DataGridMasterComponent.store.add(records);
						DataGridMasterComponent.store.sort('kd_customer', 'ASC');
						return true
					}
				});
			}
		},
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_component',
				header: 'Kode',
				width: 40
			},
			{
				dataIndex: 'component',
				header: 'Component',
				width: 80
			},
			{
				dataIndex: 'jenis',
				header: 'Jenis',
				width: 80
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridMasterComponent;
}

function getMasterCustomer_viSetupTarifComponent(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifComponent/getMasterComponentGrid",
			params: {kdgol:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					
					for(var i=0 ; i<cst.listData.length ; i++){
						SetupTarifComponent.form.ArrayStore.data2.add(new SetupTarifComponent.form.ArrayStore.data2.recordType(cst.listData[i]));
					};
				}
				
			}
		}
		
	)
}

function getComponent_viSetupTarifComponent(kd_unit,kd_milik){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifComponent/getComponentGrid",
			params: {kd_unit:kd_unit,kd_milik:kd_milik},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					
					for(var i=0 ; i<cst.listData.length ; i++){
						SetupTarifComponent.form.ArrayStore.data1.add(new SetupTarifComponent.form.ArrayStore.data1.recordType(cst.listData[i]));
					};
					hasilJumlah();
				}
				
			}
		}
		
	)
}

function getMilikCombo_viSetupTarifComponent(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifComponent/getKepemilikanCombo",
			params: {kdgol:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
						Q(SetupTarifComponent.form.DropDown.milik).add(cst.listData);
				}
				
			}
		}
		
	)
}

function getUnitCombo_viSetupTarifComponent(unit){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifComponent/getUnitCombo",
			params: {unit:unit},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					SetupTarifComponent.form.DropDown.poli_unit.setValue('');
					Q(SetupTarifComponent.form.DropDown.poli_unit).reset();
					Q(SetupTarifComponent.form.DropDown.poli_unit).add(cst.listData);
				}
				
			}
		}
		
	)
}

function dataSave_viSetupTarifComponent(){
	if (ValidasiEntrySetupTarifComponent(nmHeaderSimpanData,false) == 1 )
	{
		
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupTarifComponent/save",
				params: getParamSaveSetupTarifComponent(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTarifComponent('Berhasil menyimpan data ini','Information');
					}else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTarifComponent('Gagal menyimpan data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupTarifComponent(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifComponent/delete",
			params: getParamDeleteSetupTarifComponent(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifComponent('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupTarifComponent('Berhasil menghapus data ini','Information');
					dataAddNewTarifCustomer();
				}else 
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifComponent('Gagal menghapus data ini', 'Error');
				};
			}
		}
		
	)
}

function dataAddNewTarifCustomer(){
	SetupTarifComponent.form.DropDown.milik.setValue('');
	SetupTarifComponent.form.DropDown.unit.setValue('');
	SetupTarifComponent.form.DropDown.poli_unit.setValue('');
	Ext.getCmp('TxtTotalSetupTarifComponent').setValue('');
	SetupTarifComponent.form.ArrayStore.data1.removeAll();
	SetupTarifComponent.form.ArrayStore.data2.removeAll();
	getMasterCustomer_viSetupTarifComponent();
}


function getParamSaveSetupTarifComponent(){
	var	params =
	{
		KdMilik:SetupTarifComponent.form.DropDown.milik.getValue(),
		KdUnit:SetupTarifComponent.form.DropDown.poli_unit.getValue()
	}
	
	params['jumlah']=SetupTarifComponent.form.ArrayStore.data1.getCount();
	for(var i = 0 ; i < SetupTarifComponent.form.ArrayStore.data1.getCount();i++)
	{
		params['kd_component-'+i]=SetupTarifComponent.form.ArrayStore.data1.data.items[i].data.kd_component;
		params['percent_compo-'+i]=SetupTarifComponent.form.ArrayStore.data1.data.items[i].data.percent_compo;
	}
	
    return params
};

function getParamDeleteSetupTarifComponent(){
	var	params =
	{
		KdGol:Ext.getCmp('TxtKdTarifSetupTarifCust').getValue()
	}
   
    return params
};

function getParamSaveCustomerSetupTarifComponent(){
	var	params =
	{
		KdGol:Ext.getCmp('TxtKdTarifSetupTarifCust').getValue()
	}
	
	
	
    return params
};

function hasilJumlah(){
	var total=0;
	for(var i=0; i<SetupTarifComponent.form.ArrayStore.data1.getCount() ; i++){
		var o=SetupTarifComponent.form.ArrayStore.data1.getRange()[i].data;
		if(o.percent_compo != undefined){
			total += parseInt(o.percent_compo);
			Ext.getCmp('TxtTotalSetupTarifComponent').setValue(total);
		}
	}
}

function ValidasiEntrySetupTarifComponent(modul,mBolHapus)
{
	var x = 1;
	if(SetupTarifComponent.form.DropDown.milik.getValue()==''){
		loadMask.hide();
		ShowPesanWarningSetupTarifComponent('Kepemilikan tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(SetupTarifComponent.form.DropDown.unit.getValue()==''){
		loadMask.hide();
		ShowPesanWarningSetupTarifComponent('Unit tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(SetupTarifComponent.form.DropDown.poli_unit.getValue()==''){
		loadMask.hide();
		ShowPesanWarningSetupTarifComponent('Sub unit tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('TxtTotalSetupTarifComponent').getValue() > 100){
		loadMask.hide();
		ShowPesanWarningSetupTarifComponent('Total percent tidak boleh lebih dari 100', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('TxtTotalSetupTarifComponent').getValue() < 100){
		loadMask.hide();
		ShowPesanWarningSetupTarifComponent('Total percent tidak boleh kurang dari 100', 'Warning');
		x = 0;
	}
	for(var i=0; i<SetupTarifComponent.form.ArrayStore.data1.getCount() ; i++){
		var o=SetupTarifComponent.form.ArrayStore.data1.getRange()[i].data;
		if(o.percent_compo == undefined || o.percent_compo == 0 || o.percent_compo == '0' ){
			if(o.percent_compo == undefined){
				loadMask.hide();
				ShowPesanWarningSetupTarifComponent('Percent tidak boleh kosong', 'Warning');
				x = 0;
			}else{
				loadMask.hide();
				ShowPesanWarningSetupTarifComponent('Percent tidak boleh 0', 'Warning');
				x = 0;
			}
			
		}
		for(var j=i+1; j<SetupTarifComponent.form.ArrayStore.data1.getCount() ; j++){
			var p=SetupTarifComponent.form.ArrayStore.data1.getRange()[j].data;
			console.log(o.kd_component);
			console.log(p.kd_component);
			if(p.kd_component == o.kd_component){
				loadMask.hide();
				ShowPesanWarningSetupTarifComponent('Component tidak boleh sama', 'Warning');
				x = 0;
			}
		}
		
	}
	return x;
}

function ShowPesanWarningSetupTarifComponent(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorSetupTarifComponent(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoSetupTarifComponent(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};