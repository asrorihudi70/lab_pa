// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekResepRWJ.js
*	Menu 			: APOTEK
*	Model id 		: 
*	Keterangan 		: Resep RWJ dan IGD
*	Di buat tanggal : 04 Juni 2015
*	Oleh 			: M
*	Edit			: AGUNG
*/

// Deklarasi Variabel pada Apotek Perencanaan # --------------
var cbopasienorder_printer;

function TRApotekResepRWJ(){
var cbopasienorder_mng_apotek;
var dspasienorder_mng_apotek;
var AptResepRWJ={};
AptResepRWJ.form={};
AptResepRWJ.func={};
AptResepRWJ.vars={};
AptResepRWJ.func.parent=AptResepRWJ;
AptResepRWJ.form.DataStore={};
AptResepRWJ.form.ComboBox={};
AptResepRWJ.form.ArrayStore={};
AptResepRWJ.form.Record={};
AptResepRWJ.form.Form={};
AptResepRWJ.form.Grid={};
AptResepRWJ.form.Panel={};
AptResepRWJ.form.TextField={};
AptResepRWJ.form.Button={};
var dsDataGrdJab_viApotekResepRWJ= new Ext.data.ArrayStore({
		id: 0,
        fields: ['kd_prd','kd_satuan','nama_obat','jml','disc','kd_satuan','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik','dosis','jasa','no_out','no_urut'],
		data: []
   });
	
	
var win_printer_resep_rwi;
var dsprinter;
AptResepRWJ.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik'],
	data: []
});

AptResepRWJ.form.ArrayStore.namapasien	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 
			'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir','urut_masuk','customer','nama_dokter'],
	data: []
});
AptResepRWJ.form.ArrayStore.kodepasien	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 
			'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir','urut_masuk','customer','nama_dokter'],
	data: []
});

var KdForm=7; // kd_form RESEP RWJ & IGD
var dataSource_viApotekResepRWJ;
var selectCount_viApotekResepRWJ=50;
var NamaForm_viApotekResepRWJ="Resep Rawat Jalan / Gawat Darurat";
var selectCountStatusPostingApotekResepRWJ='Semua';
var mod_name_viApotekResepRWJ="viApotekResepRWJ";
var now_viApotekResepRWJ= new Date();
var addNew_viApotekResepRWJ;
var rowSelected_viApotekResepRWJ;
var rowSelectedOrderManajemen_viApotekResepRWJ;
var setLookUp_bayarResepRWJ;
var setLookUps_viApotekResepRWJ;
var mNoKunjungan_viApotekResepRWJ='';
var selectSetUnit;
var selectSetUnitLookup;
var selectSetPilihankelompokPasien;
var selectSetDokter;
var tanggal = now_viApotekResepRWJ.format("d/M/Y");
var tanggallabel = now_viApotekResepRWJ.format("d/M/Y");
var tanggalcekbulan = now_viApotekResepRWJ.format("Y-m-d");
var jam = now_viApotekResepRWJ.format("H/i/s");
var cellSelecteddeskripsiRWJ;
var tampungshiftsekarang;
var tmpNoOut=0;
var tmpTglOut='';
var tmpkriteria;
var kd_pasien_obbt;
var kd_unit_obbt;
var tgl_masuk_obbt;
var urut_masuk_obbt;
var gridDTLTRHistoryApotekRWJ;
var kd='';
var setLookUpApotek_TransferResepRWJ;
var GridDataViewOrderManagement_viApotekResepRWJ;
var dataSourceGridOrder_viApotekResepRWJ;
var CurrentGridOrder;
var CurrentIdMrResep='';
var detailorder=true;
var ordermanajemen=false;
var cbo_Unit;
var integrated;
var UnitFarAktif_ResepRWJ;
var ckboxRacikan;
var PrintBill;

var CellSelected_viApotekResepRWJ;
var currentKdPrdRacik_ResepRWJ;
var currentNamaObatRacik_ResepRWJ;
var currentHargaRacik_ResepRWJ;
var currentJumlah_ResepRWJ;
var curentIndexsSelection_ResepRWJ;
var cboKodePasienResepRWJ;

var CurrentHistoryRWJ =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekResepRWJ =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataOrderManajemen_viApotekResepRWJ=
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekResepRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Apotek Perencanaan # --------------

// Start Project Apotek Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viApotekResepRWJ(mod_id_viApotekResepRWJ)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekResepRWJ = 
	[
		'STATUS_POSTING','NO_RESEP','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI','TGL_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','JASA', 'ADMPRHS','ADMRESEP','CUSTOMER','JENIS_PASIEN','TGL_MASUK','URUT_MASUK'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSource_viApotekResepRWJ = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekResepRWJ
    });
    refreshRespApotekRWJ();
	total_pasien_order_mng_obtrwj();
	total_pasien_dilayani_order_mng_obtrwj();
	viewGridOrderAll_RASEPRWJ();
	loadDataKodePasienResepRWJ();
	getUnitFar_ResepRWJ();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekResepRWJ = new Ext.grid.EditorGridPanel
    (
		{
			/* xtype: 'editorgrid',
			title: '', */
			store: dataSource_viApotekResepRWJ,
			title: 'Daftar Resep Telah Dibuat',
			autoScroll: true,
			columnLines: true,
			border: true, //false,
			anchor: '100% 51.1%',
			//width: '500',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekResepRWJ = undefined;
							rowSelected_viApotekResepRWJ = dataSource_viApotekResepRWJ.getAt(row);
							CurrentData_viApotekResepRWJ
							CurrentData_viApotekResepRWJ.row = row;
							CurrentData_viApotekResepRWJ.data = rowSelected_viApotekResepRWJ.data;
							if (rowSelected_viApotekResepRWJ.data.STATUS_POSTING==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekResepRWJ').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekResepRWJ').disable();
							}
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					Ext.Ajax.request(
						{
							   
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							 params: {
								
								command: '0'
							
							},
							failure: function(o)
							{
								 var cst = Ext.decode(o.responseText);
								
							},	    
							success: function(o) {
							var cst = Ext.decode(o.responseText);
				
							tampungshiftsekarang=cst.shift
							}
					
					});
					rowSelected_viApotekResepRWJ = dataSource_viApotekResepRWJ.getAt(ridx);
					if (rowSelected_viApotekResepRWJ != undefined)
					{
						
						setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
					}
					else
					{
						setLookUp_viApotekResepRWJ();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						align		: 'center',
						dataIndex	: 'STATUS_POSTING',
						id			: 'colStatusPosting_viApotekResepRWJ',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoMedrec_viApotekResepRWJ',
						header: 'No. Resep',
						dataIndex: 'NO_RESEP',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						id: 'colTgl_viApotekResepRWJ',
						header:'Tgl Resep',
						dataIndex: 'TGL_OUT',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						// format: 'd/M/Y',
						//filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OUT);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekResepRWJ',
						header: 'No Medrec',
						dataIndex: 'KD_PASIENAPT',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colNamaPasien_viApotekResepRWJ',
						header: 'Nama',
						dataIndex: 'NMPASIEN',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colPoliklinik_viApotekResepRWJ',
						header: 'Poliklinik',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						id: 'colNoout_viApotekResepRWJ',
						header: 'No Out',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekResepRWJ',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Resep',
						iconCls: 'Edit_Tr',
						tooltip: 'Tambah Data',
						id: 'btnTambah_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}
							});
							cekPeriodeBulan();
							
						}
					},
					{
						xtype: 'button',
						text: 'Edit Resep',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request({
									   
									url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
									 params: {
										
										command: '0'
									
									},
									failure: function(o)
									{
										 var cst = Ext.decode(o.responseText);
										
									},	    
									success: function(o) {
										var cst = Ext.decode(o.responseText);
							
										tampungshiftsekarang=cst.shift
									}
							
							})
							
							if (rowSelected_viApotekResepRWJ != undefined)
							{
								setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
							}
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							
							var datanya=rowSelected_viApotekResepRWJ.data;
							if (datanya===undefined){
								ShowPesanWarningResepRWJ('Belum ada data yang dipilih','Resep RWJ');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_rwj = combo;
															if (variablebatalhistori_rwj != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxResep",
																		 params: {
																			
																			noout: datanya.NO_OUT,
																			tglout: datanya.TGL_OUT,
																			kdcustomer:datanya.KD_CUSTOMER,
																			namacustomer:datanya.CUSTOMER,
																			kdunit:datanya.KD_UNIT,
																			jenis:'RESEP',
																			apaini:"reseprwj",
																			alasan: variablebatalhistori_rwj
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanErrorResepRWJ('Data transaksi tidak dapat dihapus','Resep RWJ');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				tmpkriteria = getCriteriaCariApotekResepRWJ();
																				refreshRespApotekRWJ(tmpkriteria);
																				ShowPesanInfoResepRWJ('Data transaksi Berhasil dihapus','Resep RWJ');
																				Ext.getCmp('btnHapusTrx_viApotekResepRWJ').disable();
																			}
																			else
																			{
																				ShowPesanErrorResepRWJ('Data transaksi tidak dapat dihapus','Resep RWJ');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningResepRWJ('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													});
											/*  */
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							}
							 
							/* 
							
							if (rowSelected_viApotekResepRWJ != undefined)
							{
								setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
							} */
						}
					},
					
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viApotekResepRWJ, selectCount_viApotekResepRWJ, dataSource_viApotekResepRWJ),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var FieldOrderManajemen_viApotekResepRWJ = 
	[
		'kd_pasien', 'nama', 'kd_unit', 'nama_unit','kd_dokter','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSourceGridOrder_viApotekResepRWJ = new WebApp.DataStore
	({
        fields: FieldOrderManajemen_viApotekResepRWJ
    });
	
	GridDataViewOrderManagement_viApotekResepRWJ = new Ext.grid.EditorGridPanel
    (
		{
			title:'Order Obat Poli',
			store: dataSourceGridOrder_viApotekResepRWJ,
			autoScroll: true,
			columnLines: true,
			border: true, //false,
			anchor:'100% 30%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedOrderManajemen_viApotekResepRWJ = undefined;
							rowSelectedOrderManajemen_viApotekResepRWJ = dataSourceGridOrder_viApotekResepRWJ.getAt(row);
							CurrentDataOrderManajemen_viApotekResepRWJ
							CurrentDataOrderManajemen_viApotekResepRWJ.row = row;
							CurrentDataOrderManajemen_viApotekResepRWJ.data = rowSelectedOrderManajemen_viApotekResepRWJ.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedOrderManajemen_viApotekResepRWJ = dataSourceGridOrder_viApotekResepRWJ.getAt(ridx);
					if (rowSelectedOrderManajemen_viApotekResepRWJ != undefined)
					{
						CurrentGridOrder=CurrentDataOrderManajemen_viApotekResepRWJ.data;
						console.log(CurrentGridOrder);
						ordermanajemen=true;
						CurrentIdMrResep=CurrentGridOrder.id_mrresep;
						//console.log(mod_id_printer(0));
						
						Ext.Ajax.request(
						{
							   
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							 params: {
								
								command: '0'
							
							},
							failure: function(o)
							{
								 var cst = Ext.decode(o.responseText);
								
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
					
								tampungshiftsekarang=cst.shift
							}
						});
						
						cekPeriodeBulan(detailorder,CurrentGridOrder);
						
						
						
					}
					else
					{
						//setLookUp_viApotekResepRWJ();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No Medrec',
						dataIndex: 'kd_pasien',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama',
						sortable: false,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Poliklinik',
						dataIndex: 'nama_unit',
						sortable: false,
						width: 40
					},
					//-------------- ## --------------
					{
						header:'Tgl Order',
						dataIndex: 'tgl_order',						
						width: 30,
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_order);
						}
					},
					{
						header: 'Status',
						dataIndex: 'order_mng',
						sortable: false,
						width: 40
					},
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbarOrderManajemen_viApotekResepRWJ',
				items: 
				[
					{
						xtype: 'label',
						text: 'Order obat poli : ',
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtcounttr_apt_rwj',
						id: 'txtcounttr_apt_rwj',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						},
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:10},
					{
						xtype: 'label',
						text: 'Order obat telah dilayani : ',
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtcounttrDilayani_apt_rwj',
						id: 'txtcounttrDilayani_apt_rwj',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						},
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:10},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						disabled:true,
						id: 'btnRefreshOrder_viApotekResepRWJ',
						handler: function() 
						{
							viewGridOrderAll_RASEPRWJ();
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							ordermanajemen=false;
						}
					},
					{xtype: 'tbspacer',height: 3, width:7},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'label',
						text: 'Cari berdasarkan nama dan tanggal : '
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtNamaOrder_viApotekResepRWJ',
						id: 'txtNamaOrder_viApotekResepRWJ',
						width: 120,
						disabled:true,
						listeners: 
						{ 
							'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										viewGridOrderAll_RASEPRWJ(Ext.getCmp('txtNamaOrder_viApotekResepRWJ').getValue(),Ext.getCmp('dfTglOrderApotekResepRWJ').getValue());
									} 						
								}
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						xtype: 'datefield',
						id: 'dfTglOrderApotekResepRWJ',
						format: 'd/M/Y',
						width: 100,
						tabIndex:3,
						disabled:true,
						value:now_viApotekResepRWJ,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									viewGridOrderAll_RASEPRWJ(Ext.getCmp('txtNamaOrder_viApotekResepRWJ').getValue(),Ext.getCmp('dfTglOrderApotekResepRWJ').getValue());
								} 						
							}
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						xtype: 'label',
						text: '*) Enter untuk mencari'
					},
					{xtype: 'tbspacer',height: 3, width:580},
					
					
					//-------------- ## --------------
					
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianApotekResepRWJ = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekResepRWJ',
							name: 'TxtFilterGridDataView_RoNumber_viApotekResepRWJ',
							emptyText: 'No. Resep',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Kode/Nama Pasien'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtKdNamaPasien',
							name: 'txtKdNamaPasien',
							emptyText: 'Kode/Nama Pasien',
							width: 160,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Poliklinik'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						ComboUnitApotekResepRWJ(),	
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Resep'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalApotekResepRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirApotekResepRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekResepRWJ(),
						{
							x: 310,
							y: 65,
							xtype: 'label',
							text: '*) Tekan enter untuk mencari resep'
						},
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari Resep',
							iconCls: 'refresh',
							tooltip: 'Cari',
							hidden : true,
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_viApotekResepRWJ',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariApotekResepRWJ();
								refreshRespApotekRWJ(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]			
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekResepRWJ = new Ext.Panel
    (
		{
			title: NamaForm_viApotekResepRWJ,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekResepRWJ,
			region: 'center',
			layout: 'form', 
			closable: true,   
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianApotekResepRWJ,
					GridDataView_viApotekResepRWJ,
					GridDataViewOrderManagement_viApotekResepRWJ],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekResepRWJ,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
	
    return FrmFilterGridDataView_viApotekResepRWJ;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viApotekResepRWJ # --------------

function refreshRespApotekRWJ(kriteria)
{
    dataSource_viApotekResepRWJ.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewResepApotekRWJ',
                    param : kriteria 
                }			
            }
        );   
    return dataSource_viApotekResepRWJ;
}

/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/
	
	
function setLookUp_viApotekResepRWJ(rowdata){
    var lebar = 885;
    setLookUps_viApotekResepRWJ = new Ext.Window({
        id: 'SetLookUps_viApotekResepRWJ',
		name: 'SetLookUps_viApotekResepRWJ',
        title: NamaForm_viApotekResepRWJ, 
        closeAction: 'destroy',        
        width: 920,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'resep',
        modal: true,		
        items: getFormItemEntry_viApotekResepRWJ(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				Ext.Ajax.request({
									   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							AptResepRWJ.form.Panel.shift.update(cst.shift);
						}
					
					});
				if(Ext.getCmp('cbIntegrasiResepRWJ').getValue() == true){
					integrated='true';
				} else{
					integrated='false';
				}
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
                //datarefresh_viApotekResepRWJ();
				mNoKunjungan_viApotekResepRWJ = '';
				//ordermanajemen=false;
            }
        }
    });
	//dsDataGrdJab_viApotekResepRWJ= new WebApp.DataStore({
        //fields: ['kd_prd','kd_satuan','nama_obat','jml','disc','kd_satuan','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik','dosis']
    //});
	dsDataGrdJab_viApotekResepRWJ.loadData([],false);
    setLookUps_viApotekResepRWJ.show();
	
	
	
    if (rowdata == undefined){
		Ext.getCmp('btnAddObat').disable();
    }else{
		ViewDetailPembayaranObat(rowdata.NO_OUT,rowdata.TGL_OUT);
        datainit_viApotekResepRWJ(rowdata);
    }
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

/**
*	Function : getFormItemEntry_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viApotekResepRWJ(lebar,rowdata)
{
    var pnlFormDataBasic_viApotekResepRWJ = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			listeners: {
				afterShow: function()
				{
					Ext.Ajax.request({
									   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							AptResepRWJ.form.Panel.shift.update(cst.shift);
						}
					
					});
				}
			},
			items:
			[
				getItemPanelInputBiodata_viApotekResepRWJ(lebar),
				//-------------- ## -------------- 	
				getItemGridTransaksi_viApotekResepRWJ(lebar),
				//-------------- ## --------------
				getItemGridHistoryBayar_viApotekResepRWJ(lebar),
				
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viApotekResepRWJ',
					id: 'compChkPosted_viApotekResepRWJ',
					items: 
					[
						/* {
							columnWidth	: .033,
							layout		: 'form',
							style		: {'margin-top':'-1px'},
							anchor		: '100% 8.0001%',
							border		: false,
							fieldLabel  : 'Transfered',
							html		: ''
						}, */
						AptResepRWJ.form.Panel.a=new Ext.Panel ({
						    region: 'north',
						    border: false,
						    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
						}),
						{
							columnWidth	: .08,
							layout		: 'form',
							anchor		: '100% 8.0001%',
							style		: {'margin-top':'1px'},
							border		: false,
							html		: " Status Posting"
						},
						//-------------------##------------------------
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Tuslah :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'278px'}							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtTuslahEditData_viApotekResepRWJ',
		                    name: 'txtTuslahEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'278px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 80,								
							value: 'Adm Racik:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'258px'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmRacikEditData_viApotekResepRWJ',
		                    name: 'txtAdmRacikEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'258px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },						
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Total:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'288px'}
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridJmlEditData_viApotekResepRWJ',
		                    name: 'txtDRGridJmlEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'288px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },						
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkUpdateHB_viApotekResepRWJ',
					id: 'compChkUpdateHB_viApotekResepRWJ',
					items: 
					[
						{
							xtype: 'displayfield',				
							width: 55,								
							value: 'Tanggal :',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 100,								
							value: tanggallabel,
							format:'d/M/Y',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Adm :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'220px'}					
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmEditData_viApotekResepRWJ',
		                    name: 'txtAdmEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'220px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Disc :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'230px'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtDiscEditData_viApotekResepRWJ',
		                    name: 'txtDiscEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'230px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },	
						{
							xtype: 'displayfield',				
							width: 90,								
							value: 'Grand Total :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'220px','font-weight':'bold'}
							
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtTotalEditData_viApotekResepRWJ',
		                    name: 'txtTotalEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'220px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }	
						/* {
							xtype:'button',
							text:'Acc. Approval',
							width:70,
							//style:{'margin-left':'190px','margin-top':'7px'},
							style:{'text-align':'right','margin-left':'380px'},
							hideLabel:true,
							id: 'btnAccApp_viApotekResepRWJ',
							handler:function()
							{
							}   
						}, */
						
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkHB_viApotekResepRWJ',
					id: 'compChkHB_viApotekResepRWJ',
					items: 
					[
						
						{
							xtype: 'displayfield',				
							width: 80,								
							value: 'Current Shift :',
							fieldLabel: 'Label',
							style:{'text-align':'left'}						
						},
						AptResepRWJ.form.Panel.shift=new Ext.Panel ({
							region: 'north',
							border: false
						}),							
						{
							xtype: 'displayfield',				
							width: 95,								
							value: 'Adm Perusahaan:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'355px'}						
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmPerusahaanEditData_viApotekResepRWJ',
		                    name: 'txtAdmPerusahaanEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'455px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }
					]
				}
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Baru',
						iconCls: 'add',
						hidden:false,
						id: 'btnAdd_viApotekResepRWJ',
						handler: function(){
							dataaddnew_viApotekResepRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Dilayani',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viApotekResepRWJ',
						handler: function()
						{
							datasave_viApotekResepRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Dilayani & Tutup',
						iconCls: 'saveexit',
						disabled:true,
						hidden:true,
						id: 'btnSimpanExit_viApotekResepRWJ',
						handler: function()
						{
							/* var x = datasave_viApotekResepRWJ(addNew_viApotekResepRWJ);
							refreshRespApotekRWJ();
							if (x===undefined)
							{
								setLookUps_viApotekResepRWJ.close();
							} */
							datasave_viApotekResepRWJ();
							refreshRespApotekRWJ();
							setLookUps_viApotekResepRWJ.close();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator',
						hidden:true,
					},
					{
						xtype: 'button',
						text: 'Bayar',
						id:'btnbayar_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_bayarResepRWJ();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Transfer',
						disabled:true,
						id:'btnTransfer_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_TransferResepRWJ();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Unposting',
						id:'btnunposting_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler:function()
						{
							cekTransfer(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
						}  
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus Bayar',
						id:'btnDeleteHistory_viApotekResepRWJ',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayarList.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan dihapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionAPOTEK/deleteHistoryResepRWJ",
												params: getParamDeleteHistoryResepRWJ(),
												failure: function(o)
												{
													ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoResepRWJ('Penghapusan berhasil','Information');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
														gridDTLTRHistoryApotekRWJ.getView().refresh();
														Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
														//Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
														Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
													}
													else 
													{
														ShowPesanErrorResepRWJ('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
														gridDTLTRHistoryApotekRWJ.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else{
								ShowPesanErrorResepRWJ('Belum melakukan pembayaran','Error');
							}
						}  
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viResepRWJ',
						disabled:true,
						handler:function()
						{															
							
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillResepRWJ',
								handler: function()
								{
									PrintBill='true';
									panelnew_window_printer();
								}
							},
							{
								xtype: 'button',
								text: 'Print Kwitansi',
								id: 'btnPrintKwitansiResepRWJ',
								handler: function()
								{
									PrintBill='false';
									panelPrintKwitansi_resepRWJ();
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					},
					{
					xtype: 'label',
					text: 'Order Obat Poli : ' 
					},
					mComboorder(),
					{
						xtype: 'button',
						text: 'close order',
						id:'statusservice_apt',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							updatestatus_permintaan()
							load_data_pasienorder();
							//AptResepRWJ.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt').disable();
						}
					}
				]
			}
		}
    )

    return pnlFormDataBasic_viApotekResepRWJ;
}
// End Function getFormItemEntry_viApotekResepRWJ # --------------

/**
*	Function : getItemPanelInputBiodata_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viApotekResepRWJ(lebar) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWJ_viApotekResepRWJ',
						id: 'txtNoResepRWJ_viApotekResepRWJ',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					
					{
						xtype: 'displayfield',
						flex: 1,
						width: 100,
						name: '',
						value: 'Pasien :',
						fieldLabel: 'Label'
					},	
					mComboKodePasienResepRWJ(),
					/* AptResepRWJ.form.ComboBox.kodePasien = new Nci.form.Combobox.autoComplete({
						store	: AptResepRWJ.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.nama_dokter);
							AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.nama_unit);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.customer);
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							AptResepRWJ.vars.urut_masuk=b.data.urut_masuk;
							Ext.getCmp('btnAddObat').enable();
							ordermanajemen=false;
							
						},
						width	: 80,
						insert	: function(o){
							return {
								kd_pasien       : o.kd_pasien,
								nama			: o.nama,
								kd_unit			: o.kd_unit,
								nama_unit		: o.nama_unit,
								nama_dokter		: o.nama_dokter,
								kd_dokter		: o.kd_dokter,
								kd_customer		: o.kd_customer,
								kd_kasir		: o.kd_kasir,
								no_transaksi	: o.no_transaksi,
								tgl_transaksi	: o.tgl_transaksi,
								urut_masuk		: o.urut_masuk,
								customer		: o.customer,
								text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>'
							}
						},
						param:function(){
							var integrated;
							if(Ext.getCmp('cbIntegrasiResepRWJ').getValue() == true){
								integrated='true';
							} else{
								integrated='false';
							}
							return {
								integrasi:integrated
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 280,
						emptyText: 'Kode Pasien'
					}),	 */
					
					AptResepRWJ.form.ComboBox.namaPasien = new Nci.form.Combobox.autoComplete({
						store	: AptResepRWJ.form.ArrayStore.namapasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.nama_dokter);
							Ext.getCmp('cboKodePasienResepRWJ').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.nama_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							AptResepRWJ.vars.urut_masuk=b.data.urut_masuk;
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObat').enable();
							ordermanajemen=false;
							
							var records = new Array();
							records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
							dsDataGrdJab_viApotekResepRWJ.add(records);
							row=dsDataGrdJab_viApotekResepRWJ.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
							AptResepRWJ.form.Grid.a.startEditing(row, 3);	
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							
						},
						width	: 180,
						insert	: function(o){
							return {
								kd_pasien       : o.kd_pasien,
								nama			: o.nama,
								nama_dokter		: o.nama_dokter,
								kd_unit			: o.kd_unit,
								kd_dokter		: o.kd_dokter,
								kd_customer		: o.kd_customer,
								kd_kasir		: o.kd_kasir,
								no_transaksi	: o.no_transaksi,
								tgl_transaksi	: o.tgl_transaksi,
								urut_masuk		: o.urut_masuk,
								customer		: o.customer,
								nama_unit		: o.nama_unit,
								text			:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>'
							}
						},
						param:function(){
							if(Ext.getCmp('cbIntegrasiResepRWJ').getValue() == true){
								integrated='true';
							} else{
								integrated='false';
							}
							return {
								integrasi:integrated
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEK/getPasienResepRWJ",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'Nama Pasien'
					}),	
                    //-------------- ## --------------  
					{
						xtype: 'checkbox',
						boxLabel: 'Integrasi',
						id: 'cbIntegrasiResepRWJ',
						name: 'cbIntegrasiResepRWJ',
						width: 80,
						checked: true,
						handler:function(a,b) 
						{
							if(a.checked==true){
								
							}else{
							}
						}
					},
					
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Poliklinik ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					ComboUnitApotekResepRWJLookup(),
					
					//-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 100,
						name: '',
						value: 'Jenis Pasien :',
						fieldLabel: 'Label'
					},
					ComboPilihanKelompokPasienApotekResepRWJ(),
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						//hidden:true,
						id: 'btn1/2resep_viApotekResepRWJ',
						handler:function()
						{
							hitungSetengahResep();
						}   
					},
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					ComboDokterApotekResepRWJ(),
					{
						xtype: 'displayfield',
						flex: 1,
						width: 5,
						name: '',
						value: '',
						fieldLabel: 'Label'
					},
					
					{
						xtype: 'displayfield',
						flex: 1,
						width: 60,
						name: '',
						value: '',
						fieldLabel: 'Label'
					},	
					
					{
						xtype: 'checkbox',
						boxLabel: 'Non Resep',
						id: 'cbNonResep',
						name: 'cbNonResep',
						width: 80,
						checked: false,
						handler:function(a,b) 
						{
							if(a.checked==true){
								Ext.getCmp('txtNamaPasienNon').enable();
								Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
								Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
								Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
								Ext.getCmp('btnAddObat').enable();
								Ext.getCmp('txtTmpKdCustomer').setValue('0000000001')
								Ext.getCmp('cboKodePasienResepRWJ').disable();
								AptResepRWJ.form.ComboBox.namaPasien.disable();
								
							}else{
								Ext.getCmp('txtNamaPasienNon').disable();
								Ext.getCmp('cboKodePasienResepRWJ').enable();
								AptResepRWJ.form.ComboBox.namaPasien.enable();
							}
						}
					},
					
					{
						xtype: 'textfield',
						width : 200,	
						name: 'txtNamaPasienNon',
						id: 'txtNamaPasienNon',
						emptyText: 'Nama Pasien Non Resep',
						displayField:'Nama Pasien',
						disabled:true,
						listeners:{
							specialkey:function(){
								if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
									var records = new Array();
									records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
									dsDataGrdJab_viApotekResepRWJ.add(records);
									var row =dsDataGrdJab_viApotekResepRWJ.getCount()-1;
									AptResepRWJ.form.Grid.a.startEditing(row, 3);	
								}
							}
							
						}
						
					},					
					{
						xtype:'button',
						text:'Racikan',
						width:70,
						hideLabel:true,
						disabled:true,
						id: 'btnRacikan_viApotekResepRWJ',
						handler:function()
						{
							formulaRacikanResepRWJ();
						}   
					},
					//--------HIDDEN---------------
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdUnit',
						id: 'txtTmpKdUnit',
						emptyText: 'kode unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdDokter',
						id: 'txtTmpKdDokter',
						emptyText: 'kode dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdCustomer',
						id: 'txtTmpKdCustomer',
						emptyText: 'kode customer',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpJmlItem',
						id: 'txtTmpJmlItem',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpNoout',
						id: 'txtTmpNoout',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTglout',
						id: 'txtTmpTglout',
						emptyText: 'tgl out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPost',
						id: 'txtTmpStatusPost',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpSisaAngsuran',
						id: 'txtTmpSisaAngsuran',
						emptyText: 'sisa angsuran',
						hidden:true
					}
				]
			}
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viApotekResepRWJ # --------------


/**
*	Function : getItemGridTransaksi_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viApotekResepRWJ(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 235,//255,//300, 
		tbar:
		[
			{
				text	: 'Tambah Obat',
				id		: 'btnAddObat',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
					records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
					dsDataGrdJab_viApotekResepRWJ.add(records);
					var kd_customer=Ext.getCmp('txtTmpKdCustomer').getValue();
					if(kd_customer ==='' ||kd_customer ==='Kelompok Pasien'){
						
					}else{
						getAdm(kd_customer);
					}
					var row =dsDataGrdJab_viApotekResepRWJ.getCount()-1;
					AptResepRWJ.form.Grid.a.startEditing(row, 3);
					Ext.getCmp('btnRacikan_viApotekResepRWJ').disable()
					
				}
			},
			//-------------- ## --------------
			{
				xtype:'tbseparator'
			},
			//-------------- ## --------------
			{
				xtype: 'button',
				text: 'Hapus',
				disabled:true,
				iconCls: 'remove',
				id: 'btnDelete_viApotekResepRWJ',
				handler: function()
				{
					var line = AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
					if(dsDataGrdJab_viApotekResepRWJ.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEK/hapusBarisGridResepRWJ",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viApotekResepRWJ.removeAt(line);
													AptResepRWJ.form.Grid.a.getView().refresh();
													hasilJumlah();
													Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
													Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
													
												}
												else 
												{
													ShowPesanErrorResepRWJ('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viApotekResepRWJ.removeAt(line);
									AptResepRWJ.form.Grid.a.getView().refresh();
									// Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
									// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorResepRWJ('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
				}
			}	
		],
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewEdit_viApotekResepRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viApotekResepRWJ # --------------

/**
*	Function : gridDataViewEdit_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viApotekResepRWJ(){
    chkSelected_viApotekResepRWJ = new Ext.grid.CheckColumn({
		id: 'chkSelected_viApotekResepRWJ',
		header: 'C',
		align: 'center',						
		dataIndex: 'cito',			
		width: 20
	});
	// chkSelectedRacikan_viApotekResepRWJ = new Ext.grid.CheckColumn({
		// id: 'chkSelectedRacikan_viApotekResepRWJ',
		// header: 'Racikan',
		// align: 'center',						
		// dataIndex: 'racik',			
		// width: 20,
		// /* listeners:{
			// checkchange: function(column, recordIndex, checked){
							  // alert("checked");
						// }//btnRacikan_viApotekResepRWJ
		// } */
		
	// });
	
    AptResepRWJ.form.Grid.a = new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekResepRWJ,
        height: 210,//220,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					CellSelected_viApotekResepRWJ = dsDataGrdJab_viApotekResepRWJ.getAt(row);
					
					currentKdPrdRacik_ResepRWJ=CellSelected_viApotekResepRWJ.data.kd_prd;
					currentNamaObatRacik_ResepRWJ=CellSelected_viApotekResepRWJ.data.nama_obat;
					currentHargaRacik_ResepRWJ=CellSelected_viApotekResepRWJ.data.harga_jual;
					currentJumlah_ResepRWJ=CellSelected_viApotekResepRWJ.data.jml;
					curentIndexsSelection_ResepRWJ= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
                }
            }
        }),
        stripeRows: true,
			//-------------- ## --------------
		cm: new Ext.grid.ColumnModel([
			
			new Ext.grid.RowNumberer(),	
			//-------------- ## --------------
			chkSelected_viApotekResepRWJ,
			//-------------- ## --------------
			{			
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_obat',
				//id			: Nci.getId(),
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: AptResepRWJ.form.ArrayStore.a,
					select	: function(a,b,c){
						//'harga_beli','kd_pabrik','markup','tuslah','adm_racik'
						var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_prd=b.data.kd_prd;
						//dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_jual=b.data.harga_jual;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.markup=b.data.markup;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.adm_racik=b.data.adm_racik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jasa=b.data.jasa;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out=b.data.no_out;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_urut=b.data.no_urut;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.tgl_out=b.data.tgl_out;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.min_stok=b.data.min_stok;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=0;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.racik="";
						AptResepRWJ.form.Grid.a.getView().refresh();
						
						AptResepRWJ.form.Grid.a.startEditing(line, 5);	
						
						
						if(b.data.jml_stok_apt <= 10){
							ShowPesanInfoResepRWJ('Stok obat hampir habis, jumlah stok tersedia adalah '+b.data.jml_stok_apt,'Information')
						}
						
						//Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(b.data.adm_racik);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							kd_satuan		: o.kd_satuan,
							fractions		: o.fractions,
							harga_jual		: o.harga_jual,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							tuslah			: o.tuslah,
							adm_racik		: o.adm_racik,
							markup			: o.markup,
							jasa			: o.jasa,
							no_out			: o.no_out,
							no_urut			: o.no_urut,
							tgl_out			: o.tgl_out,
							kd_milik		: o.kd_milik,
							jml_stok_apt	: o.jml_stok_apt,
							min_stok	: o.min_stok,
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="80" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td><td width="50">&nbsp;&nbsp;'+o.min_stok+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
							}
					},
					url		: baseURL + "index.php/apotek/functionAPOTEK/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 350
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				width: 60
					
			},
			//-------------- ## --------------
			{
				dataIndex: 'racik',
				header: 'Racikan',
				width: 70,
				align:'center',
				editor: new Ext.form.ComboBox ( {
					id				: 'gridcboRacik_ResepRWJ',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					width			: 50,
					anchor			: '95%',
					value			: 1,
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	:['Id','displayText'],
						data	: [[0, 'Tidak'],[1, 'Ya']]
					}),
					valueField	: 'displayText',
					displayField: 'displayText',
					value		: '',
					listeners	: {
						select	: function(a,b,c){
							if(b.data.Id == 1) {
								Ext.getCmp('btnRacikan_viApotekResepRWJ').enable();
								formulaRacikanResepRWJ();
							} else{
								var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
								Ext.getCmp('btnRacikan_viApotekResepRWJ').disable();
								AptResepRWJ.form.Grid.a.startEditing(line, 8);	
							}
						}
					}
				})
			},{
				dataIndex: 'min_stok',
				header: 'min stok',
				sortable: true,
				xtype:'numbercolumn',
				hidden: true,
				align:'right',
				width: 85
			},
			//-------------- ## --------------
			{
				dataIndex: 'harga_jual',
				header: 'Harga Sat',
				xtype:'numbercolumn',
				sortable: true,
				align:'right',
				width: 85/* ,
				renderer: 
				function(v, params, record) {
						return parseInt(record.data.harga_jual);
				} */
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml',
				header: 'Qty',
				sortable: true,
				width: 45,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()==''){
								ShowPesanWarningResepRWJ('Qty obat belum di isi', 'Warning');
							}else{
								var o=dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
								if ( parseFloat(a.getValue())   >= parseFloat(o.min_stok))
								{
									
									Ext.Msg.alert('Perhatian','Qty sudah mencapai atau melebihi minimum stok.');
									a.setValue(o.min_stok);
									//o.jml=o.jml_stok_apt;
									//alert(o.jml);
									
								}
								else (parseFloat(a.getValue()) < parseFloat(o.min_stok))
								{
									o.jml=a.getValue();
								}
								hasilJumlah();
								AptResepRWJ.form.Grid.a.startEditing(line, 11);	
							}
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'disc',
				header: 'Diskon',
				sortable: true,
				xtype:'numbercolumn',
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=a.getValue();
							hasilJumlah();
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'jumlah',
				header: 'Sub total',
				sortable: true,
				xtype:'numbercolumn',
				width: 110,
				align:'right'
			},
			//-------------- ## --------------
			{
				dataIndex: 'dosis',
				header: 'Dosis',
				width: 150,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
								dsDataGrdJab_viApotekResepRWJ.add(records);
								var nextRow = dsDataGrdJab_viApotekResepRWJ.getCount()-1; 
								AptResepRWJ.form.Grid.a.startEditing(nextRow, 3);
							}
						}
					}
				})
			},
			//-------------- ## --------------
			//-------------- HIDDEN --------------
			{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'Kode Pabrik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'markup',
				header: 'Markup',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'adm_racik',
				header: 'Adm Racik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jasa',
				header: 'Jasa Tuslah',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_milik',
				header: 'kd milik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'stok tersedia',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
        plugins:chkSelected_viApotekResepRWJ,
		viewConfig:{
			forceFit: true
		}
    });    
    return AptResepRWJ.form.Grid.a;
}
// End Function gridDataViewEdit_viApotekResepRWJ # --------------

function hasilJumlah(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		console.log(o);
		if(o.jml != undefined){
			if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				/* if(o.racik){
					console.log(admRacik);
					if(isNaN(admRacik)){
						admRacik=0;
					} else {
						admRacik += parseFloat(o.adm_racik) * parseFloat(o.racik);
						console.log(o.adm_racik+' loih');
					}
				} */
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya' ){
							if(o.adm_racik != undefined || o.adm_racik != null || o.adm_racik != 0){
								admRacik += parseFloat(o.adm_racik) * 1;
							} else{
								admRacik +=0;
							}
							
						} else{
							admRacik +=0;
						}
						
					}
				}
				
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=parseInt(o.jml);
		}
		

	}
	
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	//alert(toFormat(total));
	total=aptpembulatan(total);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(toFormat(totdisc));
	admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	console.log(admprs);
	console.log(admRacik);
	
	
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	AptResepRWJ.form.Grid.a.getView().refresh();
}

function hasilJumlahLoad(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		
		if(o.jml != undefined){
			/* console.log(parseFloat(o.jml))
			console.log(parseFloat(o.jml_stok_apt)); */
			
			if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
				/* console.log("masuk if -"+o.jml)
				console.log("masuk if -"+o.jml_stok_apt); */
				
			} else {
				ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
				/* console.log(parseFloat("masuk else -"+o.jml))
				console.log(parseFloat("masuk else -"+o.jml_stok_apt)); */
				
			}
			totqty +=parseInt(o.jml);
		} 
		
		
	}
	
	
	//console.log("new ---");
	admRacik=parseFloat(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue())// * parseFloat(o.racik);
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.get('txtDiscEditData_viApotekResepRWJ').dom.value=toFormat(totdisc);
	admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.get('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.get('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	/* console.log(admprs);
	console.log(admRacik); */
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	AptResepRWJ.form.Grid.a.getView().refresh();
}



/**
*	Function : getItemGridHistoryBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form Histori pembayaran
*/
function getItemGridHistoryBayar_viApotekResepRWJ(lebar) 
{
    var items =
	{
		title: 'Histori Bayar', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		autoScroll:true,
		height: 100,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewHistoryBayar_viApotekResepRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridHistoryBayar_viApotekResepRWJ # --------------

/**
*	Function : gridDataViewHistoryBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian histori pembayaran
*/
function gridDataViewHistoryBayar_viApotekResepRWJ() 
{

    var fldDetail = ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
	
    dsTRDetailHistoryBayarList = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLTRHistoryApotekRWJ = new Ext.grid.EditorGridPanel
    (
        {
			//title: 'History Bayar',
            store: dsTRDetailHistoryBayarList,
            border: false,
            columnLines: true,
           //frame: false,
			//anchor: '100% 25%',
			height: 72,
			
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            /* cellSelecteddeskripsiRWJ = dsTRDetailHistoryBayarList.getAt(row);
                            CurrentHistoryRWJ.row = row;
                            CurrentHistoryRWJ.data = cellSelecteddeskripsiRWJ; */
                        }
                    }
                }
            ),
			stripeRows: true,
			autoScroll:true,
			columns: /* new Ext.grid.ColumnModel
				( */
					[
					   //new Ext.grid.RowNumberer(),
						{
							id: 'colStatPost',
							header: 'Status Posting',
							dataIndex: 'TUTUP',
							width:100,
							hidden:true
						},
						{
							id: 'colKdPsien',
							header: 'Kode Pasien',
							dataIndex: 'KD_PASIENAPT',
							width:100,
							hidden:true
						},
						{
							id: 'colNoOut',
							header: 'No out',
							dataIndex: 'NO_OUT',
							width:100,
							hidden:true
						},
						{
							id: 'coleurutmasuk',
							header: 'Urut Bayar',
							dataIndex: 'URUT',
							align :'center',
							width:90
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Resep',
							dataIndex: 'TGL_OUT',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_OUT);

							} 
						},
						{
							id: 'colePembayaran',
							header: 'Pembayaran',
							dataIndex: 'URAIAN',
							align :'center',
							width:100,
							hidden:false
							
						},
						{
							id: 'colTGlBayar',
							header: 'Tanggal Bayar',
							dataIndex: 'TGL_BAYAR',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_BAYAR);

							} 
						},
						{
							id: 'colJumlah',
							header: 'Total Bayar',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JUMLAH',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JUMLAH);

							}
							
						},
						{
							id: 'colJumlahAngsuran',
							header: 'Jumlah Angsuran',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JML_TERIMA_UANG',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JML_TERIMA_UANG);

							}
							
						},
						{
							id: 'colSisa',
							header: 'Sisa',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'SISA',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.SISA);

							}
							
						}
					],
					viewConfig: {forceFit: true}
	 
		}
    );
    return gridDTLTRHistoryApotekRWJ;
};

function TRHistoryColumModelApotekRWJ() 
{
	//'TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','JUMLAH','JML_TERIMA_UANG'
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colStatPost',
                header: 'Status Posting',
                dataIndex: 'TUTUP',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'colKdPsien',
                header: 'Kode Pasien',
                dataIndex: 'KD_PASIENAPT',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'colNoOut',
                header: 'No out',
                dataIndex: 'NO_OUT',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT'
                
            },
			{
                id: 'colTGlout',
                header: 'Tanggal Resep',
                dataIndex: 'TGL_OUT',
				menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                } 
            },
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'URAIAN',
				width:150,
				hidden:false
                
            },
			{
                id: 'colTGlout',
                header: 'Tanggal Bayar',
                dataIndex: 'TGL_BAYAR',
				menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                } 
            },
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'JUMLAH',
				hidden:false/* ,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.JUMLAH);

                } */
                
            },
			{
                id: 'colJumlah',
                header: 'Jumlah Angsuran',
				width:150,
				align :'right',
                dataIndex: 'JML_TERIMA_UANG',
				hidden:false/* ,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.JML_TERIMA_UANG);

                } */
                
            }/* ,
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME' 
            } */
			

        ]
    )
};
//----------------------------End GetDTLTRHistoryGrid------------------ 




function mComboStatusPostingApotekResepRWJ()
{
  var cboStatusPostingApotekResepRWJ = new Ext.form.ComboBox
	(
		{
			id:'cboStatusPostingApotekResepRWJ',
			x: 410,
			y: 30,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 110,
			editable: false,
			emptyText:'',
			fieldLabel: 'JENIS',
			tabIndex:5,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
								'Id',
								'displayText'
							],
					data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
					}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusPostingApotekResepRWJ,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusPostingApotekResepRWJ=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWJ();
					refreshRespApotekRWJ(tmpkriteria);

				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekResepRWJ();
						refreshRespApotekRWJ(tmpkriteria);
					} 						
				}
			}
		}
	);
	return cboStatusPostingApotekResepRWJ;
};

function ComboUnitApotekResepRWJ()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='2'"
				}
		}
	);
	
    cbo_Unit = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_Unit',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:160,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnit=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWJ();
					refreshRespApotekRWJ(tmpkriteria);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekResepRWJ();
						refreshRespApotekRWJ(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_Unit;
}


var selectSetPilihan;

/* function loadDataComboUnitFar_LapNilaiPersediaanDetail(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/lap_nilaipersediaan/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbo_Unit.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_unit.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_unit.add(recs);
				
			}
		}
	});
} */

function ComboUnitApotekResepRWJLookup()
{
    var cbo_UnitResepRWJLookup = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_UnitResepRWJLookup',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			readOnly:true,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnitLookup=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_UnitResepRWJLookup;
}

function ComboPilihanKelompokPasienApotekResepRWJ()
{
	var Field_Customer = ['KD_CUSTOMER', 'JENIS_PASIEN'];
	ds_kelpas = new WebApp.DataStore({fields: Field_Customer});
    ds_kelpas.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboKelPasApotek',
					param: ''
				}
		}
	);
    var cboPilihankelompokPasienAptResepRWJ = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
			id:'cboPilihankelompokPasienAptResepRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			readOnly:true,
			emptyText:'Kelompok Pasien',
			width: 285,
			store: ds_kelpas,
			valueField: 'KD_CUSTOMER',
			displayField: 'JENIS_PASIEN',
			value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return cboPilihankelompokPasienAptResepRWJ;
};

function ComboDokterApotekResepRWJ()
{
    var Field_Dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_Dokter});
    ds_dokter.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_dokter',
					Sortdir: 'ASC',
					target: 'ComboDokterApotek',
					param: ''
				}
		}
	);
	
    var cbo_DokterApotekResepRWJ = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_DokterApotekResepRWJ',
			valueField: 'KD_DOKTER',
            displayField: 'NAMA',
			emptyText:'Dokter',
			store: ds_dokter,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			readOnly:true,
			width:180,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetDokter=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_DokterApotekResepRWJ;
};

function mComboJenisByrResepRWJ() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrResepRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
				}
				  

			}
		}
	);
	
    return cboJenisByr;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			width:225,
			value: 'TUNAI',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				}
				  

			}
		}
	);

    return cboPembayaran;
};
function ViewDetailPembayaranObat(no_out,tgl_out) 
{	
    var strKriteriaRWJ='';
    strKriteriaRWJ = "a.no_out = " + no_out + "" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayarList.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWJ
		}
	}); 
	return dsTRDetailHistoryBayarList;
	
    
};

/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_bayarResepRWJ(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_bayarResepRWJ = new Ext.Window
    (
    {
        id: 'setLookUpApotek_bayarResepRWJ',
		name: 'setLookUpApotek_bayarResepRWJ',
        title: 'Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 230,
        resizable:false,
		emptyText:'Pilih Jenis Pembayaran...',
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ getItemPanelBiodataPembayaran_viApotekResepRWJ(lebar,rowdata),
				 getItemPanelBiodataUang_viApotekResepRWJ(lebar,rowdata)
			   ],//1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
            }
        }
    }
    );

    setLookUpApotek_bayarResepRWJ.show();
	if(Ext.getCmp('cbNonResep').getValue === true){
		Ext.getCmp('txtNamaPasien_Pembayaran').setValue(Ext.get('txtNamaPasienNon').getValue());
	} else {
		Ext.getCmp('txtNamaPasien_Pembayaran').setValue(AptResepRWJ.form.ComboBox.namaPasien.getValue());
	}
	
	 
	
	Ext.getCmp('txtkdPasien_Pembayaran').setValue(Ext.getCmp('cboKodePasienResepRWJ').getValue());
	Ext.getCmp('txtNoResepRWJ_Pembayaran').setValue(Ext.get('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('dftanggalResepRWJ_Pembayaran').setValue(now_viApotekResepRWJ);
	stmpnoOut=Ext.getCmp('txtTmpNoout').getValue();
	stmptgl=Ext.getCmp('txtTmpTglout').getValue();
	if(Ext.getCmp('txtTmpNoout').getValue() == 'No out' || Ext.getCmp('txtTmpNoout').getValue() == ''){
		Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
	if(Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue() == 'BPJS'){
		Ext.getCmp('cboJenisByrResepRWJ').setValue('ASURANSI');
		Ext.getCmp('cboPembayaran').setValue('BPJS');
	}
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWJ_Pembayaran',
						id: 'txtNoResepRWJ_Pembayaran',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalResepRWJ_Pembayaran',
						id: 'dftanggalResepRWJ_Pembayaran',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_Pembayaran',
						id: 'txtkdPasien_Pembayaran',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_Pembayaran',
						id: 'txtNamaPasien_Pembayaran',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrResepRWJ(),
					mComboPembayaran()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalResepRWJ_Pembayaran',
						id: 'txtTotalResepRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						flex: 1,
						width : 150,	
						//readOnly: true,
						style:{'text-align':'right'},
						name: 'txtBayarResepRWJ_Pembayaran',
						id: 'txtBayarResepRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranResepRWJ();
								};
							}
						}
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						id: 'btn1/2resep_viApotekResepRWJ',
						handler:function()
						{
						}   
					}
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnBayar_viApotekResepRWJL',
						handler:function()
						{
							pembayaranResepRWJ();
						}   
					}
				]
            }
		]
	};
    return items;
};


/**
*	Function : setLookUpBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_TransferResepRWJ(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_TransferResepRWJ = new Ext.Window
    (
    {
        id: 'setLookUpApotek_transferResepRWJ',
		name: 'setLookUpApotek_transferResepRWJ',
        title: 'Transfer Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 260,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			getItemPanelBiodataTransfer_viApotekResepRWJ(),
			getItemPanelTotalBayar_ApotekResepRWJ()
		],//1
		fbar:[
			{
				xtype:'button',
				text:'Transfer',
				width:70,
				hideLabel:true,
				id: 'btnTransfer_viApotekResepRWJL',
				handler:function()
				{
					transferResepRWJ();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalTransfer_viApotekResepRWJL',
				handler:function()
				{
					setLookUpApotek_TransferResepRWJ.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
            }
        }
    }
    );

    setLookUpApotek_TransferResepRWJ.show();
	if(Ext.getCmp('cbNonResep').getValue === true){
		Ext.getCmp('txtNamaPasienTransfer_ResepRWJ').setValue(Ext.get('txtNamaPasienNon').getValue());
	} else {
		Ext.getCmp('txtNamaPasienTransfer_ResepRWJ').setValue(AptResepRWJ.form.ComboBox.namaPasien.getValue());
	}
	Ext.getCmp('txtNoTransaksiTransfer_ResepRWJ').setValue(AptResepRWJ.vars.no_transaksi);
	Ext.getCmp('dftanggalTransaksi_ResepRWJ').setValue(ShowDate(AptResepRWJ.vars.tgl_transaksi));
	
	Ext.getCmp('txtkdPasienTransfer_ResepRWJ').setValue(Ext.getCmp('cboKodePasienResepRWJ').getValue());
	Ext.getCmp('txtNoResepTransfer_ResepRWJ').setValue(Ext.get('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('dftanggalTransfer_ResepRWJ').setValue(now_viApotekResepRWJ);
	stmpnoOut=Ext.getCmp('txtTmpNoout').getValue();
	stmptgl=Ext.getCmp('txtTmpTglout').getValue();
	Ext.getCmp('txtTotalBayarTransfer_ResepRWJ').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtTotalTransfer_ResepRWJ').setValue(toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()));
	
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataTransfer_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Transaksi ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoTransaksiTransfer_ResepRWJ',
						id: 'txtNoTransaksiTransfer_ResepRWJ',
						emptyText: 'No Transaksi',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tgl Transaksi :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransaksi_ResepRWJ',
						id: 'dftanggalTransaksi_ResepRWJ',
						readOnly:true
					}
				
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepTransfer_ResepRWJ',
						id: 'txtNoResepTransfer_ResepRWJ',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransfer_ResepRWJ',
						id: 'dftanggalTransfer_ResepRWJ',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasienTransfer_ResepRWJ',
						id: 'txtkdPasienTransfer_ResepRWJ',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasienTransfer_ResepRWJ',
						id: 'txtNamaPasienTransfer_ResepRWJ',
						readOnly:true
					}
					
                ]
            }
					
		]
	};
    return items;
};



function getItemPanelTotalBayar_ApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[	
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Alasan transfer:',
						fieldLabel: 'Label'
					},
					mComboalasantransfer_RESEPRWJ(),
					
                ]
            },		
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total biaya:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalBayarTransfer_ResepRWJ',
						id: 'txtTotalBayarTransfer_ResepRWJ',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWJ();
								};
							}
						}
					}
					
                ]
            },		
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total transfer :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalTransfer_ResepRWJ',
						id: 'txtTotalTransfer_ResepRWJ',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWJ();
								};
							}
						}
					},
					
                ]
            },
		]
	};
    return items;
};


function mComboalasantransfer_RESEPRWJ() 
{
	var Field = ['KD_ALASAN','ALASAN'];

    var dsalasantransfer_RESEPRWJ = new WebApp.DataStore({ fields: Field });
    dsalasantransfer_RESEPRWJ.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'kd_alasan',
			    Sortdir: 'ASC',
			    target: 'ComboAlasanTransfer',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboalasantransfer_RESEPRWJ = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasantransfer_RESEPRWJ',
		    typeAhead: false,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			width: 130,
			editable:false,
		    anchor:'100%',
		    store: dsalasantransfer_RESEPRWJ,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
			value: 'Pembayaran Disatukan',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{	
			    }

			}
		}
	);
	
    return cboalasantransfer_RESEPRWJ;
};

function datainit_viApotekResepRWJ(rowdata)
{
	dataisi=1;
	Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue(rowdata.NO_RESEP);
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(rowdata.CUSTOMER);
	if(rowdata.ADMPRHS ==0.00){
		Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(rowdata.ADMPRHS);
	}
	if(rowdata.ADMRESEP == null){
		Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(rowdata.ADMRESEP);
	}
	if(rowdata.JASA == null){
		Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(rowdata.JASA);
	}
	
	Ext.getCmp('cbopasienorder_mng_apotek').disable();
		
	Ext.getCmp('txtTmpKdCustomer').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokter').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnit').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNoout').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTglout').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpSisaAngsuran').setValue(rowdata.SISA);
	Ext.getCmp('txtTmpStatusPost').setValue(rowdata.STATUS_POSTING);
	AptResepRWJ.vars.no_transaksi=rowdata.APT_NO_TRANSAKSI;
	
	AptResepRWJ.vars.tgl_transaksi=rowdata.TGL_TRANSAKSI;
	AptResepRWJ.vars.kd_kasir=rowdata.APT_KD_KASIR;
	AptResepRWJ.vars.urut_masuk=rowdata.URUT_MASUK;
	
	kd_pasien_obbt=rowdata.KD_PASIENAPT;
	kd_unit_obbt=rowdata.KD_UNIT;
	tgl_masuk_obbt=rowdata.TGL_MASUK;
	urut_masuk_obbt=rowdata.URUT_MASUK;
	
	if(rowdata.KD_PASIENAPT === '' || rowdata.KD_PASIENAPT === undefined){
		Ext.getCmp('txtNamaPasienNon').setValue(rowdata.NMPASIEN);
		Ext.getCmp('cbNonResep').setValue(true);
		Ext.getCmp('cboKodePasienResepRWJ').disable();
		AptResepRWJ.form.ComboBox.namaPasien.disable();
	} else {
		Ext.getCmp('txtNamaPasienNon').disable();
		Ext.getCmp('cboKodePasienResepRWJ').setValue(rowdata.KD_PASIENAPT);
		AptResepRWJ.form.ComboBox.namaPasien.setValue(rowdata.NMPASIEN);
	}
	
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekResepRWJ').enable();
		Ext.getCmp('btnPrint_viResepRWJ').enable();
		Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
		Ext.getCmp('btnAdd_viApotekResepRWJ').enable();
		Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
		Ext.getCmp('btnAddObat').disable();
		AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
			
	} else{
		Ext.getCmp('btnAdd_viApotekResepRWJ').enable();
		Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
		Ext.getCmp('btnPrint_viResepRWJ').disable();
		Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').enable();
		Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
		Ext.getCmp('btnAddObat').enable();
		//Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
		AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
	
	getSeCoba(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	
	Ext.Ajax.request({
									   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			AptResepRWJ.form.Panel.shift.update(cst.shift);
		}
	
	});
	
};

function viewDetailGridOrder(CurrentGridOrder){
	Ext.Ajax.request(
	{
		   
		url: baseURL + "index.php/apotek/functionAPOTEK/cekDilayani",
		params: {
			id_mrresep:CurrentGridOrder.id_mrresep
		},
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)  {
				if(CurrentGridOrder.order_mng == 'Dilayani'){
				
					ShowPesanInfoResepRWJ('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
					setLookUps_viApotekResepRWJ.close();
				} else{
					Ext.getCmp('btnAddObat').enable();
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
					Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
					Ext.getCmp('statusservice_apt').enable();
					Ext.getCmp('cbopasienorder_mng_apotek').enable();
				}
				
			} else{
				if(CurrentGridOrder.order_mng == 'Dilayani'){
					
					ShowPesanInfoResepRWJ('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
					setLookUps_viApotekResepRWJ.close();
				} else{
					ShowPesanWarningResepRWJ('Resep pasien ini sudah dibuat. Pembayaran belum dilakukan harap lakukan pembayaran terlebih dahulu!','Warning');
					Ext.getCmp('btnAddObat').disable();
					Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
					//Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
					Ext.getCmp('statusservice_apt').disable();
					Ext.getCmp('cbopasienorder_mng_apotek').disable();
				}
				
			}
		}
	});
	
	kd_pasien_obbt =CurrentGridOrder.kd_pasien;
	kd_unit_obbt=CurrentGridOrder.kd_unit;
	tgl_masuk_obbt =CurrentGridOrder.tgl_masuk;
	urut_masuk_obbt=CurrentGridOrder.urut_masuk;
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(CurrentGridOrder.kd_dokter);
	Ext.getCmp('cboKodePasienResepRWJ').setValue(CurrentGridOrder.kd_pasien);
	AptResepRWJ.form.ComboBox.namaPasien.setValue(CurrentGridOrder.nama);
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue(CurrentGridOrder.kd_unit);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(CurrentGridOrder.customer);
	AptResepRWJ.vars.no_transaksi=CurrentGridOrder.no_transaksi;
	AptResepRWJ.vars.tgl_transaksi=CurrentGridOrder.tgl_transaksi;
	AptResepRWJ.vars.kd_kasir=CurrentGridOrder.kd_kasir;
	AptResepRWJ.vars.urut_masuk=CurrentGridOrder.urut_masuk;
	Ext.getCmp('txtTmpKdCustomer').setValue(CurrentGridOrder.kd_customer);
	Ext.getCmp('txtTmpKdDokter').setValue(CurrentGridOrder.kd_dokter);
	Ext.getCmp('txtTmpKdUnit').setValue(CurrentGridOrder.kd_unit);
	dataGridObatApotekReseppoli(CurrentGridOrder.id_mrresep,CurrentGridOrder.kd_customer);
	
	console.log(AptResepRWJ.vars.urut_masuk)
	
	//hasilJumlahLoad();
	//getAdm(Ext.getCmp('txtTmpKdCustomer').getValue());
}

function getAdm(kd_customer){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getAdm",
			params: {kd_customer:kd_customer},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(toFormat(cst.tuslah));
					hasilJumlahLoad();
				}
				else 
				{
					Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(toFormat(cst.tuslah));
					hasilJumlahLoad();
					
				};
			}
		}
		
	)
	
}

function unPostingRESEPRWJ(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulanPOST",
		params: {tgl:Ext.getCmp('txtTmpTglout').getValue()},
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
					if (button == 'yes'){
						Ext.Ajax.request ( {
							url: baseURL + "index.php/apotek/functionAPOTEK/unpostingResepRWJ",
							params: getParamUnpostingResepRWJ(),
							failure: function(o)
							{
								ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
							},	
							success: function(o) 
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) 
								{
									ShowPesanInfoResepRWJ('UnPosting Berhasil','Information');
									Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
									Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
									Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
									Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
									Ext.getCmp('btnPrint_viResepRWJ').disable();
									Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
									Ext.getCmp('btnAddObat').enable();
									Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').enable();
									Ext.getCmp('txtTmpStatusPost').setValue('0');
									AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
									refreshRespApotekRWJ();
									unposting_mrobatRWJ();
								}
								else 
								{
									ShowPesanErrorResepRWJ('Gagal melakukan unPosting', 'Error');
								};
							}
						})
					}
				});
			} else{
				if(cst.pesan=='Periode sudah ditutup'){
					ShowPesanErrorResepRWJ('Periode sudah diTutup, tidak dapat melakukan transaksi','Error');
				} else{
					ShowPesanErrorResepRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
				}
				
			}
		}
	});
}

function unposting_mrobatRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/unposting_mrobat",
			params: getParamUnPostingMrObat(),
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Error unPosting MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}


function updatestatus_permintaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/update_obat_mng",
			params: {
				kd_pasien: kd_pasien_obbt,
				kd_unit:   kd_unit_obbt,
				tgl_masuk: tgl_masuk_obbt,
				urut_masuk: urut_masuk_obbt,
      		},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
	
}


function getSeCoba(no_out,tgl_out,admracik){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/sess",
			params: {query:"no_out = " + no_out},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd=cst.session;
					dataGridObatApotekResepRWJ(no_out,tgl_out,admracik,kd);
				}
			}
		}
	);
}


function cekTransfer(no_out,tgl_out){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/cekTransfer",
			params: {
				no_out: no_out,
				tgl_out: tgl_out
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true && cst.ada === 0) 
				{
					unPostingRESEPRWJ();
				} else if (cst.success === true && cst.ada === 1) 
				{
					unPostingRESEPRWJ();
				} else{
					ShowPesanWarningResepRWJ("Pembayaran dilakukan dengan transfer. Unposting tidak dapat dilakukan!","Warning");
				}
			}
		}
	);
}

function dataGridObatApotekResepRWJ(no_out,tgl_out,admracik,kd){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/readGridObat",
			params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' and b.kd_unit_far='"+kd+"' "},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
					Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(admracik);
					hasilJumlahLoad();
				}
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function dataGridObatApotekReseppoli(id_mrresep,cuss){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getobatdetail_frompoli",
			params: {query:id_mrresep,
					 cus :cuss
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{    //dsDataGrdJab_viApotekResepRWJ.store.removeAll();
				dsDataGrdJab_viApotekResepRWJ.loadData([],false);
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
					
					getAdm(Ext.getCmp('txtTmpKdCustomer').getValue());
					
				} 
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}



function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(cst.sisa ==''){
						ShowPesanInfoResepRWJ('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(toFormat(cst.sisa));
						Ext.getCmp('txtBayarResepRWJ_Pembayaran').setValue(cst.sisa);
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
				};
			}
		}
		
	)
}

function datasave_viApotekResepRWJ(){
	if (ValidasiEntryResepRWJ(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/saveResepRWJ",
				params: getParamResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoResepRWJ('Resep berhasil dilayani','Information');
						Ext.get('txtNoResepRWJ_viApotekResepRWJ').dom.value=cst.noresep;
						Ext.get('txtTmpNoout').dom.value=cst.noout;
						Ext.get('txtTmpTglout').dom.value=cst.tgl;
						Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
						Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
						//Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
						refreshRespApotekRWJ();
						
						if(mBol === false)
						{
							
						};
					}
					else 
					{
						ShowPesanErrorResepRWJ('Resep gagal dilayani', 'Error');
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}

function cekPeriodeBulan(detailorder,CurrentGridOrder){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/cekBulan",
			params: {tgl:tanggalcekbulan},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(detailorder==true){
						setLookUp_viApotekResepRWJ();
						viewDetailGridOrder(CurrentGridOrder)
					} else{
						setLookUp_viApotekResepRWJ();
					}
					
				} else{
					if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
						ShowPesanErrorResepRWJ('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
					} else{
						ShowPesanErrorResepRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
					}
					
				}
			}
		}
	);
}

function pembayaranResepRWJ(){
	if(ValidasiBayarResepRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/bayarSaveResepRWJ",
				params: getParamBayarResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						refreshRespApotekRWJ();
						if(toInteger(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue())){
							ShowPesanInfoResepRWJ('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue(cst.noresep);
							Ext.getCmp('txtTmpNoout').setValue(cst.noout);
							Ext.getCmp('txtTmpTglout').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
							Ext.getCmp('btnPrint_viResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
							Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
							Ext.getCmp('btnAddObat').disable();
							Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
							AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ViewDetailPembayaranObat(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							setLookUpApotek_bayarResepRWJ.close();
							
							/* if(ordermanajemen==true){
								updatestatus_permintaan();
								load_data_pasienorder();
							}
							
							if(Ext.getCmp('cbNonResep').getValue() == false){
							
								insert_mrobatRWJ(); 
							} */
							
							
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							viewGridOrderAll_RASEPRWJ();
						} else {
							ShowPesanInfoResepRWJ('Berhasil melakukan pembayaran','Information');
							setLookUpApotek_bayarResepRWJ.close();
							Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
							Ext.getCmp('btnPrint_viResepRWJ').disable();
							ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							viewGridOrderAll_RASEPRWJ();
						}
					}
					else 
					{
						ShowPesanErrorResepRWJ('Gagal melakukan pembayaran', 'Error');
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}


function transferResepRWJ(){
	if(ValidasiTransferResepRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/saveTransfer",
				params: getParamTransferResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
							refreshRespApotekRWJ();
						
							ShowPesanInfoResepRWJ('Transfer berhasil dilakukan','Information');
							Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
							Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
							Ext.getCmp('btnPrint_viResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
							setLookUpApotek_TransferResepRWJ.close();
							ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							/* if(ordermanajemen==true){
								updatestatus_permintaan();
								load_data_pasienorder();
							}
							if(Ext.getCmp('cbNonResep').getValue() == false){
							
								insert_mrobatRWJ();
							} */
							
							//AptResepRWJ.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt').disable();
							total_pasien_order_mng_obtrwj();
							total_pasien_dilayani_order_mng_obtrwj();
							viewGridOrderAll_RASEPRWJ();
					}
					else 
					{
						ShowPesanErrorResepRWJ('Gagal melakukan transfer', 'Error');
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}

function insert_mrobatRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/save_mrobat",
			params: getParamInsertMrObat(),
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Error simpan MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}

function total_pasien_order_mng_obtrwj()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/countpasienmr_resep",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr_apt_rwj').setValue(cst.countpas);
			console.log(cst);
		}
	});
};

function total_pasien_dilayani_order_mng_obtrwj()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/countpasienmrdilayani_resep",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttrDilayani_apt_rwj').setValue(cst.countpas);
			console.log(cst);
		}
	});
};


function viewGridOrderAll_RASEPRWJ(nama,tgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/vieworderall",
			params: {
				nama:nama,
				tgl:tgl
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{   
				//dataSourceGridOrder_viApotekResepRWJ.loadData([],false);
				dataSourceGridOrder_viApotekResepRWJ.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dataSourceGridOrder_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dataSourceGridOrder_viApotekResepRWJ.add(recs);
					
					
					GridDataViewOrderManagement_viApotekResepRWJ.getView().refresh();
			
				} 
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	);
}


function load_data_pasienorder(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/getPasienorder_mng",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng_apotek.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dspasienorder_mng_apotek.add(recs);
				console.log(o);
			}
		}
	});
}

function load_data_printer(param)
{
	var kriteriaPrint;
	if(PrintBill == 'true'){
		kriteriaPrint='apt_printer_bill_'+UnitFarAktif_ResepRWJ;
	} else{
		kriteriaPrint='apt_printer_kwitansi_'+UnitFarAktif_ResepRWJ;
	}

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/group_printer",
		params:{
			kriteria: kriteriaPrint
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_printer.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboorder()
{ 
 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dspasienorder_mng_apotek = new WebApp.DataStore({ fields: Field });
	
	load_data_pasienorder();
	cbopasienorder_mng_apotek= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_apotek',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			disabled		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  '',
			align: 'Right',
			width: 170,
			store: dspasienorder_mng_apotek,
			valueField: 'display',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
							ordermanajemen=true;
				            kd_pasien_obbt =b.data.kd_pasien;
							kd_unit_obbt=b.data.kd_unit;
							tgl_masuk_obbt =b.data.tgl_masuk;
							urut_masuk_obbt=b.data.urut_masuk;
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							Ext.getCmp('cboKodePasienResepRWJ').setValue(b.data.kd_pasien);
							AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.kd_customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							dataGridObatApotekReseppoli(b.data.id_mrresep,b.data.kd_customer);
							
							Ext.getCmp('btnAddObat').enable();
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('statusservice_apt').enable();
							hasilJumlahLoad();
							getAdm(Ext.getCmp('txtTmpKdCustomer').getValue());
							
							CurrentIdMrResep=b.data.id_mrresep;
							
					},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder($this1.lastQuery);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    },
				
			}
		}
	);return cbopasienorder_mng_apotek;
};

function mCombo_printer_resep_rwj()
{ 
	var Field = ['alamat_printer'];

    dsprinter = new WebApp.DataStore({ fields: Field });
	
	load_data_printer();
	cbopasienorder_printer= new Ext.form.ComboBox
	(
		{
			id	: 'cbopasienorder_printer',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText	: 'Pilih Printer',
			fieldLabel	:  'Daftar Printer',
			align: 'Right',
			anchor: '100%',
			store	: dsprinter,
			valueField: 'alamat_printer',
			displayField: 'alamat_printer',
			listeners:
			{
								
			}
		}
	);
	return cbopasienorder_printer;
};


function printbill_resep_rwj()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorResepRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function printkwitansi_resep_rwj()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/cetakKwitansi/save",
			params: {
				Table: 'cetakKwitansi',
				kd_pasien:Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWJ').getValue(),
				pembayar:Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWJ').getValue(),
				jumlah_bayar:Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWJ').getValue(),
				nama:AptResepRWJ.form.ComboBox.namaPasien.getValue(),
				keterangan_bayar:Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ').getValue(),
				no_resep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
				printer:Ext.getCmp('cbopasienorder_printer').getValue(),
				kd_form:KdForm
			},
			failure: function(o)
			{	
				ShowPesanErrorResepRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamResepRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomer').getValue()=='Perseorangan'){
			KdCust='0000000001';
		}else if(Ext.get('txtTmpKdCustomer').getValue()=='Perusahaan'){
			KdCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		}else {
			KdCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		}
	
	if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue() === '' || Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue()=='No Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		KdPasien:Ext.getCmp('cboKodePasienResepRWJ').getValue(),
		NmPasien:tmpNamaPasien,		
		KdUnit: Ext.getCmp('txtTmpKdUnit').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokter').getValue(),
		NonResep:tmpNonResep,
		NoResepAsal:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNoout').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTglout').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPost').getValue(),
		Tanggal:tanggal,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtDiscEditData_viApotekResepRWJ').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:AptResepRWJ.vars.no_transaksi,
		KdKasirAsal:AptResepRWJ.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Posting:0,
		IdMrResep:CurrentIdMrResep //id_mrresep untuk update status resep = sedang dilayani
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		if(dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik == 'Ya'){
			params['racik-'+i]=1;
		} else{
			params['racik-'+i]=0;
		}
		
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.cito;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
	}
	
    return params
};

function getParamBayarResepRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue()=='Perseorangan'){
			KdCust='0000000001';
		}else if(Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue()=='Perusahaan'){
			KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
		}else {
			KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
		}
	
	if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	}
	var ubah=0;
	if(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue() === '' || Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue()=='No Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoresep='';
	if(Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue() === '' || Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue() === 'No Resep' ){
		LangsungPost = 1;
		tmpNoresep='';
	}else {
		LangsungPost = 0;
		tmpNoresep = Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue();
	}
	
	var kd_pay;
	if(Ext.getCmp('cboPembayaran').getValue() == 'TUNAI'){
		kd_pay='TU';
	} else{
		kd_pay=Ext.getCmp('cboPembayaran').getValue();
	}
	
	

	var params =
	{
		NmPasien:tmpNamaPasien,		
		KdUnit: Ext.getCmp('txtTmpKdUnit').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokter').getValue(),
		NonResep:tmpNonResep,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtDiscEditData_viApotekResepRWJ').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:AptResepRWJ.vars.no_transaksi,
		KdKasirAsal:AptResepRWJ.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPay:kd_pay,
		JumlahTotal:toInteger(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue()),
		NoResep:tmpNoresep,
		TanggalBayar:Ext.getCmp('dftanggalResepRWJ_Pembayaran').getValue(),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Posting:LangsungPost,
		Shift: tampungshiftsekarang,
		Tanggal:tanggal,
		Ubah:ubah
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
	}
	
    return params
};

function getParamTransferResepRWJ(){
	var params =
	{
		Kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue(),
		NoTransaksi:AptResepRWJ.vars.no_transaksi,
		TglTransaksi:AptResepRWJ.vars.tgl_transaksi,
		KdKasir:AptResepRWJ.vars.kd_kasir,
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasienTransfer_ResepRWJ').getValue(),
		
		JumlahTotal:toInteger(Ext.getCmp('txtTotalTransfer_ResepRWJ').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtTotalTransfer_ResepRWJ').getValue()),
		TanggalBayar:Ext.getCmp('dftanggalTransfer_ResepRWJ').getValue(),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Shift: tampungshiftsekarang,
		Tanggal:tanggal
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
	}
	
    return params
}

function getParamInsertMrObat(){
	var params =
	{
		kd_pasien: Ext.getCmp('cboKodePasienResepRWJ').getValue(),//kd_pasien_obbt,
		kd_unit:   Ext.getCmp('txtTmpKdUnit').getValue(),//kd_unit_obbt,
		tgl_masuk: AptResepRWJ.vars.tgl_transaksi,
		urut_masuk: AptResepRWJ.vars.urut_masuk,
		tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
		no_out:Ext.getCmp('txtTmpNoout').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
	}
	
	return params
}

function getParamUnpostingResepRWJ() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
	}
	
    return params
}

function getParamUnPostingMrObat(){
	var params =
	{
		kd_pasien: kd_pasien_obbt,
		kd_unit:   kd_unit_obbt,
		tgl_masuk: tgl_masuk_obbt,
		urut_masuk: urut_masuk_obbt,
		tgl_out:Ext.getCmp('txtTmpTglout').getValue(),
		no_out:Ext.getCmp('txtTmpNoout').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
	}
	
	return params
}

function getParamDeleteHistoryResepRWJ() 
{
	var urut=dsTRDetailHistoryBayarList.getRange()[gridDTLTRHistoryApotekRWJ.getSelectionModel().selection.cell[0]].data.URUT;
	
	var params =
	{
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		urut:urut
	}
	
	
	
    return params
}


function datacetakbill(){
	var tmpnama;
	var tmppoli;
	var tmpdokter;
	var tmpkode;
	
	if(Ext.getCmp('cbNonResep').getValue() === true){
		tmpnama=Ext.getCmp('txtNamaPasienNon').getValue();
		tmpkode='-';
	} else{
		tmpnama=AptResepRWJ.form.ComboBox.namaPasien.getValue();
		tmpkode=Ext.getCmp('cboKodePasienResepRWJ').getValue();
	}
	
	if(Ext.get('cbo_UnitResepRWJLookup').getValue() == '' || Ext.get('cbo_UnitResepRWJLookup').getValue()=='Poliklinik'){
		tmppoli='-';
	} else{
		tmppoli=Ext.get('cbo_UnitResepRWJLookup').getValue();
	}
	
	if(Ext.get('cbo_DokterApotekResepRWJ').getValue() == '' || Ext.get('cbo_DokterApotekResepRWJ').getValue()=='Dokter'){
		tmpdokter='-';
	} else{
		tmpdokter=Ext.get('cbo_DokterApotekResepRWJ').getValue();
	}
	
	var params =
	{
		Table: 'billprintingreseprwj',
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		KdPasien:tmpkode,
		NamaPasien:tmpnama,
		JenisPasien:Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue(),
		Poli:tmppoli,
		Dokter:tmpdokter,
		Total:Ext.get('txtTotalEditData_viApotekResepRWJ').getValue(),
		Tot:toInteger(Ext.get('txtTotalEditData_viApotekResepRWJ').getValue()),
		printer:Ext.getCmp('cbopasienorder_printer').getValue(),
		kd_form:KdForm
		
	}
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml;
	}
	
    return params
}



function ValidasiEntryResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbNonResep').getValue() === false){
		if(AptResepRWJ.form.ComboBox.namaPasien.getValue() === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0 || Ext.getCmp('cboKodePasienResepRWJ').getValue() === ''){
			if(AptResepRWJ.form.ComboBox.namaPasien.getValue() === ''){
				ShowPesanWarningResepRWJ('Nama Pasien', 'Warning');
				x = 0;
			} else if(dsDataGrdJab_viApotekResepRWJ.getCount() == 0){
				ShowPesanWarningResepRWJ('Daftar resep obat belum di isi', 'Warning');
				x = 0;
			} else {
				ShowPesanWarningResepRWJ('Kode Pasien', 'Warning');
				x = 0;
			}
		}
	} else {
		if(Ext.getCmp('txtNamaPasienNon').getValue() === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0){
			if(Ext.getCmp('txtNamaPasienNon').getValue() === '' ){
				ShowPesanWarningResepRWJ('Nama', 'Warning');
				x = 0;
			} else {
				ShowPesanWarningResepRWJ('Daftar resep obat belum di isi', 'Warning');
				x = 0;
			}
				
		}
	}
	if((Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === 0) ||
		(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue() === 0)){
		if(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === 0){
			ShowPesanWarningResepRWJ('Jumlah harga total obat kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningResepRWJ('Jumlah total bayar kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		}
	}
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.kd_prd == undefined || o.kd_prd == ""){
			ShowPesanWarningResepRWJ('Obat masih kosong, harap isi baris kosong atau hapus untuk melanjutkan!', 'Warning');
			x = 0;
		} else{
			if(o.jml == undefined || o.jumlah == undefined){
				if(o.jml == undefined){
					ShowPesanWarningResepRWJ('Jumlah qty belum di isi, qty tidak boleh kosong', 'Warning');
					x = 0;
				}else if(o.jumlah == undefined){
					ShowPesanWarningResepRWJ('Jumlah total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
					x = 0;
				}
			}
			if(parseFloat(o.jml) > parseFloat(o.jml_stok_apt)){
				ShowPesanWarningResepRWJ('Jumlah qty melebihi stok', 'Warning');
				o.jml=o.jml_stok_apt;
				x = 0;
			}
		}
		
		for(var j=0; j<dsDataGrdJab_viApotekResepRWJ.getCount() ; j++){
			var p=dsDataGrdJab_viApotekResepRWJ.getRange()[j].data;
			if(i != j && o.kd_prd == p.kd_prd){
				ShowPesanWarningResepRWJ('Tidak boleh ada obat yang sama, periksa kembali daftar obat!', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};

function ValidasiBayarResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrResepRWJ').getValue() === '' ){
		ShowPesanWarningResepRWJ('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaran').getValue() === '' || Ext.getCmp('cboPembayaran').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningResepRWJ('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue() === ''){
		ShowPesanWarningResepRWJ('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};

function ValidasiTransferResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtNoTransaksiTransfer_ResepRWJ').getValue() === '' || Ext.getCmp('txtNoTransaksiTransfer_ResepRWJ').getValue() === 'No Transaksi'){
		ShowPesanWarningResepRWJ('No transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransaksi_ResepRWJ').getValue() === '' ){
		ShowPesanWarningResepRWJ('Tanggal transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtNoResepTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('No. resep tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('Tanggal tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtkdPasienTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('Kode pasien tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtTotalTransfer_ResepRWJ').getValue() === ''){
		ShowPesanWarningResepRWJ('Jumlah transfer tidak boleh kosong', 'Warning');
		x = 0;
	}
	
	return x;
};

function getCriteriaCariApotekResepRWJ()//^^^
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue()!=='No. Resep')
            {
                strKriteria = " o.no_resep  " + "LIKE upper('%" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasien').getValue() != "" && Ext.get('txtKdNamaPasien').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasien').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekResepRWJ').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekResepRWJ').getValue() != "" && Ext.get('dfTglAkhirApotekResepRWJ').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWJ').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWJ').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_Unit').getValue() != "" && Ext.get('cbo_Unit').getValue() != 'Poliklinik')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_Unit').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_Unit').getValue() + "'";
			    }
                
            }
			
	 strKriteria = strKriteria +" and left(o.kd_unit,1)<>'1' and o.returapt=0 and o.kd_unit_far='" + UnitFarAktif_ResepRWJ +"' order by o.tgl_out limit 20"
	 return strKriteria;
}

function dataaddnew_viApotekResepRWJ() 
{
	Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue('');
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue('');
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue('');
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue('');
	Ext.getCmp('txtNamaPasienNon').setValue('');
	Ext.getCmp('cboKodePasienResepRWJ').setValue('');
	AptResepRWJ.form.ComboBox.namaPasien.setValue('');
	dsDataGrdJab_viApotekResepRWJ.loadData([],false);
	Ext.getCmp('cbopasienorder_mng_apotek').enable();
	Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(0);
	
	AptResepRWJ.vars.no_transaksi="";
	AptResepRWJ.vars.tgl_transaksi="";
	AptResepRWJ.vars.kd_kasir="";
	AptResepRWJ.vars.urut_masuk="";
	Ext.getCmp('txtTmpKdCustomer').setValue("");
	Ext.getCmp('txtTmpKdDokter').setValue("");
	Ext.getCmp('txtTmpKdUnit').setValue("");
	AptResepRWJ.form.ComboBox.namaPasien.setValue("");
	Ext.getCmp('txtTmpNoout').setValue("");
	Ext.getCmp('txtTmpTglout').setValue("");
	Ext.getCmp('txtTmpStatusPost').setValue("");
	Ext.getCmp('txtTmpJmlItem').setValue("");
	Ext.getCmp('txtTmpSisaAngsuran').setValue("");
	kd_pasien_obbt="";
	kd_unit_obbt="";
	tgl_masuk_obbt="";
	urut_masuk_obbt="";
	Ext.getCmp('txtNamaPasienNon').setValue("");
	Ext.getCmp('txtNamaPasienNon').disable();
	Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
	Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
	Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
	Ext.getCmp('btnPrint_viResepRWJ').disable();
	Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
	Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
	Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
	Ext.getCmp('btnAddObat').disable();
	dsTRDetailHistoryBayarList.removeAll();
	AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	AptResepRWJ.form.ComboBox.namaPasien.focus();
	
}

function loadDataKodePasienResepRWJ(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
		params: {
			text:param,
			integrasi:integrated
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboKodePasienResepRWJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKodePasien_ResepRWJ.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKodePasien_ResepRWJ.add(recs);
				console.log(o);
			}
		}
	});
};

function mComboKodePasienResepRWJ(){
	var Field = ['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 
				'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir','urut_masuk','customer','nama_dokter'];
	dsKodePasien_ResepRWJ = new WebApp.DataStore({fields: Field});
	loadDataKodePasienResepRWJ();
	cboKodePasienResepRWJ = new Ext.form.ComboBox
	(
		{
			id: 'cboKodePasienResepRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hideTrigger		: true,
			store: dsKodePasien_ResepRWJ,
			valueField: 'kd_pasien',
			displayField: 'kd_pasien',
			emptyText: 'No. Medrec',
			width:100,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
					Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.nama_dokter);
					AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
					Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.nama_unit);
					Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.customer);
					Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
					AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
					AptResepRWJ.vars.tgl_transaksi=b.data.tgl_transaksi;
					AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
					AptResepRWJ.vars.urut_masuk=b.data.urut_masuk;
					Ext.getCmp('btnAddObat').enable();
					ordermanajemen=false;
					
					var records = new Array();
					records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
					dsDataGrdJab_viApotekResepRWJ.add(records);
					row=dsDataGrdJab_viApotekResepRWJ.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					AptResepRWJ.form.Grid.a.startEditing(row, 3);	
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboKodePasienResepRWJ.lastQuery != '' ){
								var value="";
								
								if (value!=cboKodePasienResepRWJ.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
										params: {
											text:cboKodePasienResepRWJ.lastQuery,
											integrasi:integrated
										},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboKodePasienResepRWJ.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsKodePasien_ResepRWJ.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsKodePasien_ResepRWJ.add(recs);
											}
											a.expand();
											if(dsKodePasien_ResepRWJ.onShowList != undefined)
												dsKodePasien_ResepRWJ.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboKodePasienResepRWJ.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
		}
	)
	return cboKodePasienResepRWJ;
};


function ShowPesanWarningResepRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function ShowPesanErrorResepRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoResepRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:300
		}
	);
};


function panelnew_window_printer()
{
    win_printer_resep_rwi = new Ext.Window
    (
        {
            id: 'win_printer_resep_rwi',
            title: 'Printer',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_printer_ResepRWJ()],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkPrintAll_ResepRWJ',
					handler: function()
					{		
						if (Ext.getCmp('cbopasienorder_printer').getValue()===""){
							ShowPesanWarningResepRWJ('Pilih dulu print sebelum cetak', 'Warning');
						}else{
							if(PrintBill == 'true'){
								printbill_resep_rwj();	
								win_printer_resep_rwi.close();
							} else{
								printkwitansi_resep_rwj();
								win_printer_resep_rwi.close();
								panelWindowPrintKwitansi_ResepRWJ.close();
							}
							
						} 
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasswordDulu',
					handler: function()
					{
						win_printer_resep_rwi.close();
					}
				} 
			]

        }
    );

    win_printer_resep_rwi.show();
};

function Itempanel_printer_ResepRWJ()
{
    var panel_printer_ResepRWJ = new Ext.Panel
    (
        {
            id: 'panel_printer_ResepRWJ',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                mCombo_printer_resep_rwj(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return panel_printer_ResepRWJ;
};

function panelPrintKwitansi_resepRWJ()
{
    panelWindowPrintKwitansi_ResepRWJ = new Ext.Window
    (
        {
            id: 'panelWindowPrintKwitansi_ResepRWJ',
            title: 'Print Kwitansi',
           // closeAction: 'destroy',
            width:440,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
           // layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_PrintKwitansiResepRWJ()],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkPrintKwitansi_ResepRWJ',
					handler: function()
					{
						PrintBill='false';
						panelnew_window_printer();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelPrintKwitansi_ResepRWJ',
					handler: function()
					{
						panelWindowPrintKwitansi_ResepRWJ.close();
					}
				} 
			]

        }
    );

    panelWindowPrintKwitansi_ResepRWJ.show();
	getDataPrintKwitansiResepRWJ();
};

function Itempanel_PrintKwitansiResepRWJ()
{
    var items=
    (
        {
            id: 'panelItemPrintKwitansiRWJ',
			layout:'form',
			border: true,
			bodyStyle:'padding: 5px',
			height: 215,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 5px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 10,
							xtype: 'textfield',
							id: 'txtKd_pasienPrintKwitansi_viApotekResepRWJ',
							name: 'txtKd_pasienPrintKwitansi_viApotekResepRWJ',
							width: 80,
							readOnly: true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pembayar'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 40,
							xtype: 'textfield',
							id: 'txtNamaPembayarPrintKwitansi_viApotekResepRWJ',
							name: 'txtNamaPembayarPrintKwitansi_viApotekResepRWJ',
							width: 180,
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Untuk Pembayaran'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 70,
							xtype: 'textarea',
							id: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ',
							name: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ',
							width: 250,
							height: 62,
						},
						{
							x: 10,
							y: 140,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 140,
							y: 140,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 140,
							xtype: 'textfield',
							id: 'txtNoResepPrintKwitansi_viApotekResepRWJ',
							name: 'txtNoResepPrintKwitansi_viApotekResepRWJ',
							width: 150,
						},
						{
							x: 10,
							y: 170,
							xtype: 'label',
							text: 'Jumlah Bayar (Rp)',
							style:{'font-weight':'bold'},
						},
						{
							x: 140,
							y: 170,
							xtype: 'label',
							text: ':',
							style:{'font-weight':'bold'},
						},
						{
							x: 150,
							y: 170,
							xtype: 'textfield',
							id: 'txtJumlahBayarPrintKwitansi_viApotekResepRWJ',
							name: 'txtJumlahBayarPrintKwitansi_viApotekResepRWJ',
							width: 150,
							style:{'text-align':'right','font-weight':'bold'},
							readOnly:true
						},
					]
				}
				
			]	
        }
    );

    return items;
};

function getDataPrintKwitansiResepRWJ(){
	Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('cboKodePasienResepRWJ').getValue());
	Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWJ').setValue(AptResepRWJ.form.ComboBox.namaPasien.getValue());
	Ext.getCmp('txtNoResepPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWJ').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getTemplateKwitansi",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWJ').setValue(cst.template +" "+ AptResepRWJ.form.ComboBox.namaPasien.getValue());
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWJ('Gagal membaca template kwitansi', 'Error');
				};
			}
		}
		
	)
}

function getUnitFar_ResepRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getUnitFar",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					UnitFarAktif_ResepRWJ = cst.kd_unit_far;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWJ('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

function hitungSetengahResep(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var qtygrid=0;
		var subJumlah=0;
		var sisa=0;
		
		
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.jml != undefined || o.jml != ""){
			// Jika qty tidak melebihi stok tersedia
			if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				// Cek qty ganjil atau tidak
				if((parseInt(o.jml) % 2) == 1){
					sisa = parseInt(o.jml) - 1; //Dikurangi 1 supaya genap
					qtygrid = parseInt(sisa) / 2; //Setelah dikurang lalu di bagi 2
					qtygrid = qtygrid + 1; //Setelah dibagi 2 lalu di tambahkan 1 dari pengurangan menjadi genap sebelumnya
					o.jml = qtygrid;
				} else{
					qtygrid = parseInt(o.jml) / 2;
					o.jml = qtygrid;
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya'){
							if(o.adm_racik != undefined || o.adm_racik != null || o.adm_racik != 0){
								admRacik += parseFloat(o.adm_racik) * 1;
								console.log(admRacik);
							} else{
								admRacik +=0;
								console.log(admRacik);
							}
						} else{
							admRacik +=0;
						}
						
					}
				}
				
				jumlahGrid = ((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			}else {
				ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=parseInt(o.jml);
			
		}
	}
	//admRacik=parseFloat(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue())// * parseFloat(o.racik);
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.get('txtDiscEditData_viApotekResepRWJ').dom.value=toFormat(totdisc);
	admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.get('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.get('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	AptResepRWJ.form.Grid.a.getView().refresh();
}

function formulaRacikanResepRWJ(){
	var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpRacikan_ResepRWJ = new Ext.Window
    ({
        id: 'setLookUpRacikan_ResepRWJ',
		name: 'setLookUpRacikan_ResepRWJ',
        title: 'Formulasi Racikan Obat', 
        closeAction: 'destroy',        
        width: 523,
        height: 260,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
		PanelFormulasiRacikan_ResepRWJ()
			/* getItemPanelBiodataTransfer_viApotekResepRWJ(),
			getItemPanelTotalBayar_ApotekResepRWJ() */
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKRacikan_viApotekResepRWJL',
				handler:function()
				{
					setHasilFormula_ResepRWJ()
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalRacikan_viApotekResepRWJL',
				handler:function()
				{
					setLookUpRacikan_ResepRWJ.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
             //   rowSelected_viApotekResepRWJ=undefined;
            }
        }
    });

    setLookUpRacikan_ResepRWJ.show();
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWJ').focus();
	Ext.getCmp('txtkodeObatRacikan_viApotekResepRWJ').setValue(currentKdPrdRacik_ResepRWJ);
	Ext.getCmp('txtNamaObatRacikan_viApotekResepRWJ').setValue(currentNamaObatRacik_ResepRWJ);
	Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWJ').setValue(currentHargaRacik_ResepRWJ);
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').setValue(currentJumlah_ResepRWJ);
}

function PanelFormulasiRacikan_ResepRWJ(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 195,
		items:
		[
			/* {
				layout: 'column',
				border: false,
				items:
				[ */
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 10px ',
						border: true,
						width: 495,
						height: 40,
						anchor: '100% 23%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nama obat'
							},
							{
								x: 70,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 80,
								y: 10,
								xtype: 'textfield',
								id: 'txtkodeObatRacikan_viApotekResepRWJ',
								name: 'txtkodeObatRacikan_viApotekResepRWJ',
								width: 70,
								readOnly: true
							},
							{
								x: 155,
								y: 10,
								xtype: 'textfield',
								id: 'txtNamaObatRacikan_viApotekResepRWJ',
								name: 'txtNamaObatRacikan_viApotekResepRWJ',
								width: 160,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Harga satuan'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtaHargaObatRacikan_viApotekResepRWJ',
								name: 'txtaHargaObatRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 495,
						height: 1,
						items:
						[
							
							{
								xtype: 'label',
								text: ''
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: true,
						width: 500,
						height: 50,
						anchor: '100% 72%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Dosis persediaan obat farmasi'
							},
							{
								x: 170,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 10,
								xtype: 'numberfield',
								id: 'txtPersediaanObatRacikan_viApotekResepRWJ',
								name: 'txtPersediaanObatRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Dosis resep / permintaan dokter'
							},
							{
								x: 170,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 40,
								xtype: 'numberfield',
								id: 'txtDosisPermintaanDokterRacikan_viApotekResepRWJ',
								name: 'txtDosisPermintaanDokterRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75
							},
							
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Quantity resep racikan'
							},
							{
								x: 170,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 70,
								xtype: 'numberfield',
								id: 'txtQtyResepRacikanRacikan_viApotekResepRWJ',
								name: 'txtQtyResepRacikanRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											HitungRacikanObat_ResepRWJ();
										}
									}
								}
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Quantity obat yang dikeluarkan'
							},
							{
								x: 170,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 100,
								xtype: 'numberfield',
								id: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ',
								name: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Sub Total'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtSubTOtalRacikan_viApotekResepRWJ',
								name: 'txtSubTOtalRacikan_viApotekResepRWJ',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 260,
								y: 75,
								xtype: 'label',
								text: '*) Enter untuk menghitung'
							},
						]
					}
			/* 	]
			}  */
							
		]		
	};
        return items;
}

function HitungRacikanObat_ResepRWJ(){
	var totqty=0;
	var totharga=0;
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').getValue();
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWJ').getValue();
	
	totqty = (parseFloat(Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWJ').getValue()) / parseFloat(Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWJ').getValue())) * parseFloat(Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWJ').getValue());
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').setValue(totqty);
	totharga = parseFloat(Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').getValue()) * parseFloat(Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWJ').getValue());
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWJ').setValue(totharga);
}

function setHasilFormula_ResepRWJ(){
	var o = dsDataGrdJab_viApotekResepRWJ.getRange()[curentIndexsSelection_ResepRWJ].data;
	
	o.jml = Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWJ').getValue();
	o.jumlah = Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWJ').getValue();
	AptResepRWJ.form.Grid.a.getView().refresh();
	AptResepRWJ.form.Grid.a.startEditing(curentIndexsSelection_ResepRWJ, 10);
	setLookUpRacikan_ResepRWJ.close();
	hasilJumlah();
}



}
TRApotekResepRWJ();