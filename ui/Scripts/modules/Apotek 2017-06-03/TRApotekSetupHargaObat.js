/**
 *  _________________LAST EDITOR____________________
 * | No.| Name Editor			| Date Editor		|
 * |====|=======================|===================|
 * |_1._|_Asep_Kamaludin________|_03-07-2015________|
 * |_2._|_Malinda_______________|_17-06-2016________|
 * =================@copyright NCI 2015=============
 */
var setupHargaObat={
	/*
	 * variable
	 */
	vars:{
		title:'Setup Obat',
		selectData:null,
		shortcut:false
	},
	/*
	 * Component
	 */
	ArrayStore:{
		gridMain:new Ext.data.ArrayStore({fields:[]})
	},
	Grid:{
		main:null	
	},
	NumberField:{
		min_stok:null,
		harga_beli:null
	},
	TextField:{
		srchCode:null,
		kd_prd:null,
		nama_obat:null,
		fraction:null
	},
	Window:{
		input:null
	},
	Dropdown:{
		milik:null
	},
	/*
	 * Function
	 */
	getData:function(){
		var $this=this;
		
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/functionApotekSetupHargaObat/getData",
			success: function(r){
				loadMask.hide();
				if(r.result=='SUCCESS'){
					Q($this.Dropdown.milik).add(r.data.milik);
				}else{
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	setShortcut:function(){
		var $this=this;
		$(window).unbind().keydown(function(event) {
    		console.log(event.keyCode);
    		if($this.vars.shortcut==true){
			   	if(event.ctrlKey &&  event.which==83){
    				$this.save();
    				event.preventDefault();
					return false;
    			}
    		}
		});
	},
	getParams:function(){
		var $this=this;
		var a=[];
		a.push({name: 'kd_milik',value:$this.Dropdown.milik.getValue()});
		a.push({name: 'kd_prd',value:$this.TextField.kd_prd.getValue()});
		if($this.NumberField.min_stok.getValue()!=''){
			a.push({name: 'minstok',value:$this.NumberField.min_stok.getValue()});
		}else{
			Ext.Msg.alert('Gagal','Harap Isi Min Stok.');
			return;
		}
		if($this.NumberField.harga_beli.getValue()!=''){
			a.push({name: 'harga_beli',value:$this.NumberField.harga_beli.getValue()});
		}else{
			Ext.Msg.alert('Gagal','Harap Isi Harga Beli.');
			return;
		}
		return a;
	},
	save:function(callback){
		var $this=this;
		if($this.getParams() != undefined){
			loadMask.show();
			$.ajax({
				url:baseURL + "index.php/apotek/functionApotekSetupHargaObat/save",
				dataType:'JSON',
				type: 'POST',
				data:$this.getParams(),
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						Ext.Msg.alert('Informasi','Data Berhasil Disimpan.');
						if(callback != undefined){
							callback();
						}
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
		}
	},
	showInput:function(){
		var $this=this;
		$this.Window.input = new Ext.Window({
	        title: $this.vars.title, 
	        closeAction: 'destroy',        
	        width: 300,
	        height: 250,
	        resizable:false,
			autoScroll: false,
			layout:'fit',
	        border: false,
	        constrainHeader : true,    
	        iconCls: 'Studi_Lanjut',
	        modal: true,		
        	items:[
        		new Ext.FormPanel({
        			bodyStyle:'padding: 10px',
        			tbar:[
	        			{
	        				xtype:'button',
	        				text:'Save',
	        				iconCls:'save',
							id:'BtnAPTSaveSetupHargaObat',
	        				handler:function(){
	        					$this.save();
	        				}
	        			},{
	        				xtype:'button',
	        				text:'Save & Close',
	        				iconCls:'save',
							hidden:true,
	        				handler:function(){
	        					$this.save(function(){
	        						$this.Window.input.close();
	        					});
	        				}
	        			}
        			],
        			items:[
						{
	        				layout:'column',
	        				border:false,
	        				bodyStyle:'margin-bottom: 4px',
	        				items:[
		        				{
		        					xtype:'displayfield',
		        					value:'Milik : ',
		        					width: 100
		        				},
		        				$this.Dropdown.milik=Q().dropdown({
									width: 90,
									emptyText: 'Semua'
								})
	        				]
	        			},
	        			{
	        				layout:'column',
	        				border:false,
	        				bodyStyle:'margin-bottom: 4px',
	        				items:[
		        				{
		        					xtype:'displayfield',
		        					value:'Kode Obat : ',
		        					width: 100
		        				},
		        				$this.TextField.kd_prd=new Ext.form.TextField({
		        					disabled: true,
		        					style:{'padding':'2px'}
		        				})
	        				]
	        			},{
	        				layout:'column',
	        				bodyStyle:'margin-bottom: 4px',
	        				border:false,
	        				items:[
		        				{
		        					xtype:'displayfield',
		        					value:'Nama Obat : ',
		        					width: 100
		        				},
		        				$this.TextField.nama_obat=new Ext.form.TextField({
		        					disabled: true,
		        					style:{'padding':'2px'}
		        				})
	        				]
	        			},{
	        				layout:'column',
	        				bodyStyle:'margin-bottom: 4px',
	        				border:false,
	        				items:[
		        				{
		        					xtype:'displayfield',
		        					value:'Fractions : ',
		        					width: 100
		        				},
		        				$this.TextField.fraction=new Ext.form.TextField({
		        					disabled: true,
		        					style:{'text-align':'right','padding':'2px'}
		        				})
	        				]
	        			},{
	        				layout:'column',
	        				bodyStyle:'margin-bottom: 4px',
	        				border:false,
	        				items:[
		        				{
		        					xtype:'displayfield',
		        					value:'Min Stok : ',
		        					width: 100
		        				},
		        				$this.NumberField.min_stok=new Ext.form.NumberField({
		        					style:{'text-align':'right','padding':'2px'},
		        					listeners:{ 
										'specialkey' : function(){
											if (Ext.EventObject.getKey() === 13){
												$this.save();							
											} 						
										}
									}
		        				})
	        				]
	        			},{
	        				layout:'column',
	        				bodyStyle:'margin-bottom: 4px',
	        				border:false,
	        				items:[
		        				{
		        					xtype:'displayfield',
		        					value:'Harga Beli : ',
		        					width: 100
		        				},
		        				$this.NumberField.harga_beli=new Ext.form.NumberField({
		        					style:{'text-align':'right','padding':'2px'},
		        					listeners:{ 
										'specialkey' : function(){
											if (Ext.EventObject.getKey() === 13){
												$this.save();							
											} 						
										}
									}
		        				})
	        				]
	        			}
        			]
        		})
        	],
        	listeners:{
	            activate: function(){
					$this.vars.shortcut=true;
					$this.setShortcut();
	            },
	            afterShow: function(){
	                this.activate();
	            },
	            deactivate: function(){
	            },
	            hide:function(){
	            	$this.vars.shortcut=false;
	            },
				close: function (){
					// shortcut.remove('lookup');
				},
        	}
    	});
    	loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/functionApotekSetupHargaObat/initUpdate",
			data:{'kd_prd':$this.vars.selectData},
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.Window.input.show();
					$this.Dropdown.milik.setValue(r.data.kd_milik);
					$this.Dropdown.milik.setRawValue(r.data.milik);
					$this.TextField.kd_prd.setValue(r.data.kd_prd);
					$this.TextField.nama_obat.setValue(r.data.nama_obat);
					$this.TextField.fraction.setValue(r.data.fractions);
					$this.NumberField.min_stok.setValue(r.data.minstok);
					$this.NumberField.harga_beli.setValue(r.data.harga_beli);
					$this.getData();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
		// shortcut.set({
			// code:'lookup',
			// list:[
				// {
					// key:'ctrl+s',
					// fn:function(){
						// Ext.getCmp('BtnAPTSaveSetupHargaObat').el.dom.click();
					// }
				// },
				// {
					// key:'esc',
					// fn:function(){
						// $this.Window.input.close();
					// }
				// }
			// ]
		// });
	},
	refresh:function(){
		var $this=this;
		loadMask.show();
		var a=[];
		a.push({name: 'kdobat',value:$this.TextField.srchCode.getValue()});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/functionApotekSetupHargaObat/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.gridMain.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						$this.ArrayStore.gridMain.add(new $this.ArrayStore.gridMain.recordType(r.listData[i]))
					}
					$this.vars.selectData=null;
					$this.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	main:function(mod_id){
		var $this=this;
		$this.Grid.main = new Ext.grid.EditorGridPanel({
			title: '',
			store: $this.ArrayStore.gridMain,
			autoScroll: true,
			columnLines: true,
			border:false,
			flex:1,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners:{
					rowselect: function(sm, row, rec){
						$this.vars.selectData=$this.ArrayStore.gridMain.getAt(row).data.kd_prd;
					}
				}
			}),
			listeners:{
				rowdblclick: function (sm, ridx, cidx){
					$this.showInput();
				}
			},
			colModel: new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
				{
					header: 'Milik',
					dataIndex: 'milik',
					sortable: true,
					width: 30
				},
				{
					header: 'Kode',
					dataIndex: 'kd_prd',
					sortable: true,
					width: 35
				},{
					header:'Nama Obat',
					dataIndex: 'nama_obat',						
					flex: 1,
					sortable: true,
					hideable:false,
                    menuDisabled:true
				},{
					header: 'Sat B',
					dataIndex: 'satuan_besar',
					hideable:false,
					width: 30
				},{
					header: 'Sat K',
					dataIndex: 'satuan_kecil',
					sortable: true,
					width: 50
				},{
					header: 'Golongan',
					dataIndex: 'golongan',
					sortable: true,
					width: 40
				},{
					header: 'Fraction',
					dataIndex: 'fraction',
					sortable: true,
					width: 40
				},{
					header: 'Jenis',
					dataIndex: 'jenis',
					sortable: true,
					width: 40
				},{
					header: 'Harga Beli',
					dataIndex: 'harga_beli',
					sortable: true,
					align:'right',
					width: 40,
					renderer: function(v, params, record)
					{
						return formatCurrency(record.data.harga_beli);

					}
				},{
					header: 'Min Stok',
					dataIndex: 'minstok',
					sortable: true,
					align:'right',
					width: 40
				}
			]),
			tbar:{
				xtype: 'toolbar',
				items: [
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						handler: function(sm, row, rec){
							if($this.vars.selectData != null){
								$this.showInput();
							}
						}
					}
				]
			},
			viewConfig: {
				forceFit: true
			}
		});
		var pencarianApotekResepRWI = new Ext.FormPanel({
	        labelAlign: 'top',
	        title: '',
	        height: 35,
	        bodyStyle:'padding:5px 5px 0',
	        items: [
				{
					layout: 'column',
					border: false,
					items:[
						{
							xtype:'displayfield',
							value:'Nama/Kode Obat : ',
							width: 150
						},$this.TextField.srchCode=new Ext.form.TextField({
							width:100,
							listeners:{ 
								'specialkey' : function(){
									if (Ext.EventObject.getKey() === 13){
										$this.refresh();							
									} 						
								}
							}
						}),
						new Ext.Button({
							text:'Search',
							iconCls: 'refresh',
							handler:function(){
								$this.refresh();
							}
						})
					]
				}
			]	
		});
   		var mainPanel = new Ext.Panel({
			title: $this.vars.title,
			iconCls: 'Studi_Lanjut',
			id: mod_id,
			layout:{
				type:'vbox',
				align:'stretch'
			},
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [pencarianApotekResepRWI,$this.Grid.main]
       	});
    	return mainPanel;
	},
	init:function(){
		var $this=this;
		CurrentPage.page = $this.main(CurrentPage.id);
		mainPage.add(CurrentPage.page);
		mainPage.setActiveTab(CurrentPage.id);
		$this.refresh();
	}
}
setupHargaObat.init();