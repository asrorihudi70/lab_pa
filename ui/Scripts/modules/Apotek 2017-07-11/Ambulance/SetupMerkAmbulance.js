var NamaForm_viSetupMerkMobilAmbulanceTest="Setup Merk Ambulance";
var mod_name_viSetupMerkMobilAmbulanceTest="Setup Merk Ambulance";
var selectCount_viSetupMerkMobilAmbulance=0;
var now_viSetupMerkMobilAmbulanceTest= new Date();
var tanggal = now_viSetupMerkMobilAmbulanceTest.format("d/M/Y");
var jam = now_viSetupMerkMobilAmbulanceTest.format("H/i/s");

var dataSource_viSetupMerkAmbulance;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupMerkMobilAmbulanceTest;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viSetupMerkAmbulance;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;
var dataGridMerkMobilAmbulanceRefresh;
var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;

var CurrentData_viSetupMerkMobilAmbulanceTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupMerkMobilAmbulanceTest={};
SetupMerkMobilAmbulanceTest.form={};
SetupMerkMobilAmbulanceTest.func={};
SetupMerkMobilAmbulanceTest.vars={};
SetupMerkMobilAmbulanceTest.func.parent=SetupMerkMobilAmbulanceTest;
SetupMerkMobilAmbulanceTest.form.ArrayStore={};
SetupMerkMobilAmbulanceTest.form.ComboBox={};
SetupMerkMobilAmbulanceTest.form.DataStore={};
SetupMerkMobilAmbulanceTest.form.Record={};
SetupMerkMobilAmbulanceTest.form.Form={};
SetupMerkMobilAmbulanceTest.form.Grid={};
SetupMerkMobilAmbulanceTest.form.Panel={};
SetupMerkMobilAmbulanceTest.form.TextField={};
SetupMerkMobilAmbulanceTest.form.Button={};

SetupMerkMobilAmbulanceTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupMerkMobilAmbulanceTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd'],
	data: []
});

CurrentPage.page = dataGrid_viSetupMerkMobilAmbulanceTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupMerkMobilAmbulanceTest(mod_id_viSetupMerkMobilAmbulanceTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupMerkMobilAmbulanceTest = 
	[
		'kd_milik','merk'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupMerkAmbulance = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupMerkMobilAmbulanceTest
    });
    //dataGridMerkMobilAmbulanceRefresh();
	dataGridItemHasil_SetupMerkAmbulance();
//	dataGridRefHasil();
	//dataGridHasilDialisa(0);
	//loadDataComboItemHasil();
	var FieldItem=['kd_item', 'item_hd'];
	dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viSetupMerkAmbulance = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupMerkAmbulance,
			autoScroll: true,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			height:540,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupMerkMobilAmbulanceTest = undefined;
							rowSelected_viSetupMerkMobilAmbulanceTest = dataSource_viSetupMerkAmbulance.getAt(row);
							CurrentData_viSetupMerkMobilAmbulanceTest
							CurrentData_viSetupMerkMobilAmbulanceTest.row = row;
							CurrentData_viSetupMerkMobilAmbulanceTest.data = rowSelected_viSetupMerkMobilAmbulanceTest.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupMerkMobilAmbulanceTest = dataSource_viSetupMerkAmbulance.getAt(ridx);
					if (rowSelected_viSetupMerkMobilAmbulanceTest != undefined)
					{
						DataInitSetupMerkMobilAmbulanceTest(rowSelected_viSetupMerkMobilAmbulanceTest.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viKodeMerkAmbulance',
						header: 'Kode',
						dataIndex: 'kd_milik', //harus sama dengan nama kolom didatabase
						hideable:false,
						menuDisabled: true,
						width: 10
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viMerk',
						header: 'Merk',
						dataIndex: 'merk',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMerkAmbulance',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupMerkAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupMerkMobilAmbulanceTest != undefined)
							{
								DataInitSetupMerkMobilAmbulanceTest(rowSelected_viSetupMerkMobilAmbulanceTest.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMerkMobilAmbulanceTest, selectCount_viSetupMerkMobilAmbulance, dataSource_viSetupMerkAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GridDataView_viSetupReferensiHasil = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupReferensiHasil,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:250,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupReferensiHasil = undefined;
							rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(row);
							CurrentData_viSetupReferensiHasil
							CurrentData_viSetupReferensiHasil.row = row;
							CurrentData_viSetupReferensiHasil.data = rowSelected_viSetupReferensiHasil.data;
							//DataInitSetupMerkMobilAmbulanceTest(rowSelected_viSetupMerkMobilAmbulanceTest.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(ridx);
					if (rowSelected_viSetupReferensiHasil != undefined)
					{
						DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
						//setLookUp_viSetupMerkMobilAmbulanceTest(rowSelected_viSetupMerkMobilAmbulanceTest.data);
					}
					else
					{
						//setLookUp_viSetupMerkMobilAmbulanceTest();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNo_ref_viSetupReferensiHasil',
						header: 'No. Ref',
						dataIndex: 'no_ref',
						hideable:false,
						menuDisabled: true,
						width: 10 
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupReferensiHasil',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colDeskripsi_viSetupReferensiHasil',
						header: 'Deskripsi',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupReferensiHasil',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupReferensiHasil',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupReferensiHasil != undefined)
							{
								DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMerkMobilAmbulanceTest, selectCount_viSetupMerkMobilAmbulance, dataSource_viSetupMerkAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
								
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKd_Item_viSetupItemDialisa',
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 5
						
					},
					//-------------- ## --------------
					{
						id		: 'col_Item_viSetupItemDialisa',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 30,
						editor:new Nci.form.Combobox.autoComplete({
							
							store	: SetupMerkMobilAmbulanceTest.form.ArrayStore.b,
							select	: function(a,b,c){
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemDialisa.data.items[line].data.kd_item=b.data.kd_item;
								dataSource_viSetupItemDialisa.data.items[line].data.item_hd=b.data.item_hd;
								GridDataView_viSetupItemDialisa.getView().refresh();
							},
							insert	: function(o){
								return {
									kd_item        	: o.kd_item,
									item_hd 		: o.item_hd,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_item+'</td><td width="150">'+o.item_hd+'</td></tr></table>'
								}
							},
							url		: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/getItemHasilTest",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemDialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowItemDialisa',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupItemDialisa.recordType());
							dataSource_viSetupItemDialisa.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanItemDialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_ItemDialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Row',
						iconCls: 'remove',
						id: 'btnDeleteItemDialisa',
						handler: function()
						{
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
							dataSource_viSetupItemDialisa.removeAt(line);
							GridDataView_viSetupItemDialisa.getView().refresh();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshItemDialisa',
						handler: function()
						{
							dataSource_viSetupItemDialisa.removeAll();
							dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
							Ext.getCmp('btnAddRowItemDialisa').disable();
							Ext.getCmp('btnSimpanItemDialisa').disable();
							Ext.getCmp('btnDeleteItemDialisa').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMerkMobilAmbulanceTest, selectCount_viSetupMerkMobilAmbulance, dataSource_viSetupMerkAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelInputSetupMerkMobilAmbulance = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:false,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupItemMerkAmbulance()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupMerkMobilAmbulance',
						handler: function(){
							AddNewSetupMerkAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupMerkMobilAmbulance',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupMerkMobilAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupMerkMobilAmbulance',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupMerkMobilAmbulance();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupMerkMobilAmbulance',
						handler: function()
						{
							dataSource_viSetupMerkAmbulance.removeAll();
							dataGridItemHasil_SetupMerkAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					}
				]
			}
	})
	var FormMerk = new Ext.Panel
	(
		{
			id: 'mod_id_panel_grid_cari_merk_mobil_ambulance',
			closable:true,
			region: 'center',
			layout: 'form',
			//title: 'Data Supir Ambulance',
			//margins:'0 5 5 0',
			//bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			iconCls: 'Pagu',
			items: [GridDataView_viSetupMerkAmbulance],
			tbar: 
			[
				'Nama Merk  ',
				'',
							{
								xtype: 'textfield',
								id: 'txtNamaMerk_cari',
								name: 'txtNamaMerk_cari',
								width: 150,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											RefreshDataMerkFilter();	
										} 						
									},
									blur: function(a){
										
									}
								}
							},
			
							'','','',
				{
					xtype: 'button',
					id:'btnRefreshFasilitasFilter',
					tooltip: 'Tampilkan',
					iconCls: 'find',
					align :'right',
					text: 'Tampilkan',
					handler: function(sm, row, rec) {
						RefreshDataMerkFilter();
					}

				}
			]
		}
	);
	
	//LAYAR FORM UTAMA 
	var FrmData_viSetupMerkMobilAmbulanceTest = new Ext.Panel
    (
		{
			title: 'Setup Merk',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupMerkMobilAmbulanceTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
			title: '',
			height: 750,
				items:[
					PanelInputSetupMerkMobilAmbulance,
					//GridDataView_viSetupMerkAmbulance
					FormMerk
				]
		}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupMerkMobilAmbulanceTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupItemMerkAmbulance(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 60,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdMerkAmbulance',
								name: 'txtKdMerkAmbulance',
								width: 100,
								allowBlank: false,
								readOnly: true,
								disable:true,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: ' 	Merk'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtnama_Merk_ambulance',
								name: 'txtnama_Merk_ambulance',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							}
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 130,
        y: 60,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};


function dataGridItemHasil_SetupMerkAmbulance(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/getGridMerkAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupMerkAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupMerkAmbulance.add(recs);
					GridDataView_viSetupMerkAmbulance.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupMerkMobilAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupMerkAmbulance(){
	Ext.getCmp('txtKdMerkAmbulance').setValue('');
	Ext.getCmp('txtnama_Merk_ambulance').setValue('');
};

function dataSave_viSetupMerkMobilAmbulance(){
	if (ValidasiSaveSetupMerkAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/save",
				params: getParamSaveSetupMerkAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						Ext.get('txtKdMerkAmbulance').dom.value=cst.kode;
						loadMask.hide();
						ShowPesanInfoSetupMerkMobilAmbulance('Berhasil menyimpan data ini','Information')
						dataSource_viSetupMerkAmbulance.removeAll();
						dataGridItemHasil_SetupMerkAmbulance();
//						loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMerkMobilAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupMerkAmbulance.removeAll();
						//dataGridItemHasil_SetupMerkAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamSaveSetupMerkAmbulance(){
	var	params =
	{
		kd_milik:Ext.getCmp('txtKdMerkAmbulance').getValue(),
		merk:Ext.getCmp('txtnama_Merk_ambulance').getValue(),		
	}
    return params
};

function ValidasiSaveSetupMerkAmbulance(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtnama_Merk_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupMerkMobilAmbulance('Merk masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function ValidasiSaveSetupMerkAmbulance_delete(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtnama_Merk_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupMerkMobilAmbulance('Gagal dihapus tidak ada data/data kosong', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningSetupMerkMobilAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupMerkMobilAmbulance(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupMerkMobilAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viSetupMerkMobilAmbulance(){
	if (ValidasiSaveSetupMerkAmbulance_delete(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/delete",
				params: getParamDeleteSetupMerkAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{

						loadMask.hide();
						ShowPesanInfoSetupMerkMobilAmbulance('Berhasil menghapus data ini','Information');
						AddNewSetupMerkAmbulance()
						dataSource_viSetupMerkAmbulance.removeAll();
						dataGridItemHasil_SetupMerkAmbulance();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMerkMobilAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupMerkAmbulance.removeAll();
						dataGridItemHasil_SetupMerkAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupMerkAmbulance(){
	var	params =
	{
		kd_milik:Ext.getCmp('txtKdMerkAmbulance').getValue()
	}
   
    return params
};

function DataInitSetupMerkMobilAmbulanceTest(rowdata){
	Ext.getCmp('txtKdMerkAmbulance').setValue(rowdata.kd_milik);
	Ext.getCmp('txtnama_Merk_ambulance').setValue(rowdata.merk);
};

/*function loadDataComboItemHasil(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/getItemHasil",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboItemHasil.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_ItemHasil.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_ItemHasil.add(recs);
				console.log(o);
			}
		}
	});
}*/



function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMerkMobilAmbulance('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMerkMobilAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupMerkMobilAmbulance('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMerkMobilAmbulance('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMerkMobilAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupMerkMobilAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMerkMobilAmbulance('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMerkMobilAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupMerkMobilAmbulance('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningSetupMerkMobilAmbulance('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupMerkMobilAmbulanceTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupMerkMobilAmbulance('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupMerkMobilAmbulance('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};

function RefreshDataMerkFilter(){
	Ext.Ajax.request({
			url: baseURL + "index.php/ambulance/functionSetupMerkAmbulance/getGridMerkAmbulance_cari",
			params: {tipe:Ext.getCmp('txtNamaMerk_cari').getValue()},
			failure: function(o){
				ShowPesanErrorSetupMerkMobilAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dataSource_viSetupMerkAmbulance.removeAll();
					var recs=[],
						recType=dataSource_viSetupMerkAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupMerkAmbulance.add(recs);
					GridDataView_viSetupMerkAmbulance.getView().refresh();
				}
				else {
					ShowPesanErrorSetupMerkMobilAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGridMerkMobilAmbulanceRefresh(criteria){
	dataSource_viSetupMerkAmbulance.load({
		params:{
			Skip: 0,
			Take: selectCount_viSetupMerkMobilAmbulance,
			Sort: 'kd_milik',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataMerkMobilAmbulance',
			param: criteria

		}
	});
    return dataSource_viSetupMerkAmbulance;
	
}	

