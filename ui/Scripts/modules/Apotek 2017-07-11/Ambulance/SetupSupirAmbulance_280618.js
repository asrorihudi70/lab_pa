var NamaForm_viSetupHasilTest="Setup Supir Ambulance";
var mod_name_viSetupHasilTest="Setup Supir Ambulance";
var selectCount_viSetupHasilTest=50;
var now_viSetupHasilTest= new Date();
var tanggal = now_viSetupHasilTest.format("d/M/Y");
var jam = now_viSetupHasilTest.format("H/i/s");
var tmp_kd_kelurahan;
var dataSource_viSetupSupirAmbulance;
var dataSource_viSetupSupirAmbulance;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupHasilTest;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viSetupHasilTest;
var GridDataView_viSetupSupirAmbulance;
var GridDataView_viSetupItemDialisa;
var dsPropinsiSetupSupirAmbulance;
var dsKabupatenSetupSupirAmbulance;
var dsKecamatanSetupSupirAmbulance;
var now = new Date();

var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;

var CurrentData_viSetupHasilTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupHasilTest={};
SetupHasilTest.form={};
SetupHasilTest.func={};
SetupHasilTest.vars={};
SetupHasilTest.func.parent=SetupHasilTest;
SetupHasilTest.form.ArrayStore={};
SetupHasilTest.form.ComboBox={};
SetupHasilTest.form.DataStore={};
SetupHasilTest.form.Record={};
SetupHasilTest.form.Form={};
SetupHasilTest.form.Grid={};
SetupHasilTest.form.Panel={};
SetupHasilTest.form.TextField={};
SetupHasilTest.form.Button={};

SetupHasilTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupHasilTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd'],
	data: []
});

CurrentPage.page = dataGrid_viSetupHasilTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupHasilTest(mod_id_viSetupHasilTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupHasilTest = 
	[
		'kd_item','item_hd','type_item','type_item_des'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupSupirAmbulance = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupHasilTest
    });
	dataGridSupirAmbulance();
	var FieldItem=['kd_item', 'item_hd'];
	dataSource_viSetupSupirAmbulance = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viSetupHasilTest = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupSupirAmbulance,
			autoScroll: false,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			height:570,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupHasilTest = undefined;
							rowSelected_viSetupHasilTest = dataSource_viSetupSupirAmbulance.getAt(row);
							CurrentData_viSetupHasilTest
							CurrentData_viSetupHasilTest.row = row;
							CurrentData_viSetupHasilTest.data = rowSelected_viSetupHasilTest.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupHasilTest = dataSource_viSetupSupirAmbulance.getAt(ridx);
					if (rowSelected_viSetupHasilTest != undefined)
					{
						DataInitSetupHasilTest(rowSelected_viSetupHasilTest.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKodeSupir',
						header: 'Kode',
						dataIndex: 'kd_supir', //harus sama dengan nama kolom didatabase
						hideable:false,
						menuDisabled: true,
						hidden:false,
						width: 10
						
					},
					//-------------- ## --------------
					{
						id: 'colNamaSupir',
						header: 'Nama Supir',
						dataIndex: 'nama_supir',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					
					{
						id: 'colTglLahirSupirAmbulance',
						header: 'Tanggal Lahir',
						dataIndex: 'tgl_lahir',
						hidden:false,
						menuDisabled: true,
						width: 40
					},

					{
						id: 'colTglBerlakuSupirAmbulance',
						header: 'Tgl Berlaku SIM',
						dataIndex: 'batas_tgl_berlaku_sim',
						hidden:false,
						menuDisabled: true,
						width: 40
					},
					//-------------- ## --------------
					{
						id: 'colAlamatSupirAmbulance',
						header: 'Alamat',
						dataIndex: 'alamat',
						hidden:false,
						menuDisabled: true,
						width: 40
					},
					
					
					{
						id: 'colProvinsiSupirAmbulance',
						header: 'Provinsi',
						dataIndex: 'propinsi',
						hidden:false,
						menuDisabled: true,
						width: 40
					},
					{
						id: 'colkabKotSupirAmbulance',
						header: 'Kabupaten/Kota',
						dataIndex: 'kabupaten',
						hidden:false,
						menuDisabled: true,
						width: 40
					},
					{
						id: 'colLKecamatanSupirAmbulance',
						header: 'Kecamatan',
						dataIndex: 'kecamatan',
						hidden:false,
						menuDisabled: true,
						width: 40
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupDokterAmbulance',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupDokterAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupHasilTest != undefined)
							{
								DataInitSetupHasilTest(rowSelected_viSetupHasilTest.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupHasilTest, selectCount_viSetupHasilTest, dataSource_viSetupSupirAmbulance),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	

	
	var PanelInputSetupHasil = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupSupirAmbulance()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupHasil',
						handler: function(){
							AddNewSetupHasil();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupHasil',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupSupirAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupHasil',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupSupirAmbulance();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupHasil',
						handler: function()
						{
							dataSource_viSetupSupirAmbulance.removeAll();
							dataGridSupirAmbulance();
						}
					},
					{
						xtype: 'tbseparator'
					}
				]
			}
	})
	
	
	
	
	
	/*var FrmHasilTest= new Ext.Panel(
	{
        layout: {
        type: 'accordion',
        titleCollapse: true,
        multi: true,
        fill: false,
        animate: false, 
		id: 'accordionNavigationContainerHemodialisa',
        flex: 1
        },

        
		width: 1000,
		height: 1500,
		defaults: {
		},
		layoutConfig: {
			titleCollapse: false,
			animate: true,
			activeOnTop: false
		},
		items: [{
			title: 'Supir Ambulance',
			height: 780,
			items:[
				PanelInputSetupHasil,
				GridDataView_viSetupHasilTest
			]
		}]
	}
	);
	*/
	//LAYAR FORM UTAMA 
	var FrmData_viSetupHasilTest = new Ext.Panel
    (
		{
			title: 'Setup Supir Ambulance',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupHasilTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
				title: '',
				height: 780,
				items:[
					PanelInputSetupHasil,
					GridDataView_viSetupHasilTest
				]
			}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupHasilTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupSupirAmbulance(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
                    xtype: 'fieldset',
                    title: 'Inputan',
                    autoHeight: true,
                    bodyStyle: 'margin-left: 10px',
                    width: '830px',
                    // each item will be a checkbox
                    items: 
                    [
                     {
						columnWidth:.98,
						layout: 'absolute',
						border: false,
						// width: 500,
						height: 180,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 5,
								xtype: 'label',
								text: 'kode Supir'
							},
							{
								x: 120,
								y: 5,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 5,
								xtype: 'textfield',
								id: 'txtKdSupirAmbulance',
								name: 'txtKdSupirAmbulance',
								width: 100,
								disabled:true,
								allowBlank: false,
								readOnly: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 35,
								xtype: 'label',
								text: 'Nama Supir'
							},
							{
								x: 120,
								y: 35,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 35,
								xtype: 'textfield',
								id: 'txtNamaSupirAmbulance',
								name: 'txtNamaSupirAmbulance',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 65,
								xtype: 'label',
								text:  'Tanggal Lahir'
							},
							{
								x: 120,
								y: 65,
								xtype: 'label',
								text: ':'
							},

							{
                                xtype: 'datefield',
                                x: 130,
								y: 65,
                                id: 'tgl_lahir_supir_ambulance',
                                name: 'tgl_lahir_supir_ambulance',
                                format: 'd/M/Y',
                                readOnly: false,
                                value: now
                               
                            },

							{
								x: 250,
								y: 65,
								xtype: 'label',
								text:  'No.Sim'
							},
							{
								x: 310,
								y: 65,
								xtype: 'label',
								text: ':'
							},
							{
								x: 320,
								y: 65,
								xtype: 'numberfield',
								id: 'txtno_sim_supir',
								name: 'txtno_sim_supir',
								width: 200,
								allowBlank: false,
								allowDecimals: false,
					            allowNegative: false,
					            maxLength: 13,
					            autoCreate: {tag: 'input', type: 'text', size: '13', autocomplete: 'off', maxlength: '13'},
					           // size:1,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},

							{
								x: 540,
								y: 65,
								xtype: 'label',
								text:  'Tgl Berlaku SIM'
							},
							{
								x: 640,
								y: 65,
								xtype: 'label',
								text: ':'
							},
							{
                                xtype: 'datefield',
                                x: 660,
								y: 65,
                                id: 'txttgl_berlaku_sim_supir',
                                name: 'txttgl_berlaku_sim_supir',
                                format: 'd/M/Y',
                                readOnly: false,
                                value: now,
                               
                            },
							

							{
								x: 10,
								y: 150,
								xtype: 'label',
								text:  'Alamat'
							},

							{
								x: 120,
								y: 150,
								xtype: 'label',
								text: ':'
							},

							{
								x: 160,
								y: 105,
								xtype: 'label',
								text:  'Provinsi'
							},
							
							mComboPropinsiSetupSupirAmbulance(),

							{
								x: 280,
								y: 100,
								xtype: 'label',
								text:  'Kabupaten/Kota'
							},
							mComboKabupatenSetupSupirAmbulance(),

							{
								x: 430,
								y: 105,
								xtype: 'label',
								text:  'Kecamatan'
							},
							mComboKecamatanSetupSupirAmbulance(),

								{
								x: 570,
								y: 105,
								xtype: 'label',
								text:  'Kelurahan'
							},
							mCombokelurahan(),

							{
								x: 130,
								y: 150,
								xtype: 'textfield',
								id: 'txt_alamat_supir_ambulance',
								name: 'txt_alamat_supir_ambulance',
								width: 600,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},


						]
					}   
                    ]
            	},{
                    xtype: 'fieldset',
                    title: 'Pencarian',
                    autoHeight: true,
                    width: '830px',
                    bodyStyle: 'margin-left: 10px',
                    items: 
                    [
                     {
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 40,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 5,
								xtype: 'label',
								text: 'Nama Supir'
							},
							{
								x: 120,
								y: 5,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 5,
								xtype: 'textfield',
								id: 'txtNamaSupirAmbulance_cari',
								name: 'txtNamaSupirAmbulance_cari',
								width: 150,
								allowBlank: false,
								maxLength:3,
								tabIndex:1
							},
							{
								x: 300,
								y: 5,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 410,
								y: 5,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 5,
								xtype: 'textfield',
								id: 'txtAlamatSupirAmbulance_cari',
								name: 'txtAlamatSupirAmbulance_cari',
								width: 150,
								allowBlank: false,
								maxLength:3,
								tabIndex:1
							},
						]
					}   
                    ]
            	},

					
				]
			}
		]		
	};
        return items;
}

/*function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 200,
		y: 100,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 100,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};
*/
function mComboPropinsiSetupSupirAmbulance(){
    var Field = ['KD_PROPINSI', 'PROPINSI'];
    dsPropinsiSetupSupirAmbulance = new WebApp.DataStore({fields: Field});
    dsPropinsiSetupSupirAmbulance.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
    var cboPropinsiSetupSupirAmbulance = new Ext.form.ComboBox({
		id: 'cboPropinsiSetupSupirAmbulance',
		x: 130,
		y: 125,
		width: 130,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiSetupSupirAmbulance,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		tabIndex:3,
		enableKeyEvents:true,
		listeners:{
			'select': function (a, b, c){
				selectPropinsiSetupSupirAmbulance = b.data.KD_PROPINSI;
				loaddatastorekabupaten(b.data.KD_PROPINSI);
/*				console.log(PILIHKABUPATEN);*/
				/* Ext.getCmp('cboKabupatenSetupSupirAmbulance').setValue();
				Ext.getCmp('cboKecamatanSetupSupirAmbulance').setValue();
				Ext.getCmp('cbokelurahan').setValue(); */
				Ext.getCmp('cboKabupatenSetupSupirAmbulance').focus();
			},
			keypress:function(c,e){
				if (e.keyCode == 13){
					Ext.getCmp('cboKabupatenSetupSupirAmbulance').focus();
					selectPropinsiSetupSupirAmbulance = c.value;
					//loaddatastorekabupaten(selectPropinsiSetupSupirAmbulance);
				} else if (e.keyCode == 9){
					selectPropinsiSetupSupirAmbulance = c.value;
					Ext.getCmp('cboKabupatenSetupSupirAmbulance').focus();
					//loaddatastorekabupaten(selectPropinsiSetupSupirAmbulance);
				}
			},
			keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					//cekKunjunganPasienLaluCetakBpjs();
				}
			}

		}
	});
    return cboPropinsiSetupSupirAmbulance;
}

function mComboKabupatenSetupSupirAmbulance(){
    var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
    dsKabupatenSetupSupirAmbulance = new WebApp.DataStore({fields: Field});
    var cboKabupatenSetupSupirAmbulance = new Ext.form.ComboBox({
		id: 'cboKabupatenSetupSupirAmbulance',
		typeAhead: true,
		x: 270,
		y: 125,
		triggerAction: 'all',
		selectOnFocus: true,
		lazyRender: true,
		mode: 'local',
		width:130,
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod',
		align: 'Right',
		store: dsKabupatenSetupSupirAmbulance,
		valueField: 'KD_KABUPATEN',
		displayField: 'KABUPATEN',
		tabIndex: 6,
		enableKeyEvents:true,
		listeners:{
			'select': function (a, b, c){
				selectKabupatenSetupSupirAmbulance = b.data.KD_KABUPATEN;
				loaddatastorekecamatan(b.data.KD_KABUPATEN);
				/* Ext.getCmp('cboKecamatanSetupSupirAmbulance').setValue();
				Ext.getCmp('cbokelurahan').setValue(); */
				Ext.getCmp('cboKecamatanSetupSupirAmbulance').focus(false);
			},
			keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress:function(c,e){
				if (e.keyCode == 13){
					Ext.getCmp('cboKecamatanSetupSupirAmbulance').focus(false);
					selectKabupatenSetupSupirAmbulance = c.value;
					//loaddatastorekecamatan(selectKabupatenSetupSupirAmbulance);
				}else if (e.keyCode == 9){
					selectKabupatenSetupSupirAmbulance = c.value;
					Ext.getCmp('cboKecamatanSetupSupirAmbulance').focus(false);
					//loaddatastorekecamatan(selectKabupatenSetupSupirAmbulance);
				}
			}
		}
	});
    return cboKabupatenSetupSupirAmbulance;
}

function mComboKecamatanSetupSupirAmbulance(){
    var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
    dsKecamatanSetupSupirAmbulance = new WebApp.DataStore({fields: Field});
    var cboKecamatanSetupSupirAmbulance = new Ext.form.ComboBox({
		id: 'cboKecamatanSetupSupirAmbulance',
		x: 410,
		y: 125,
		width: 130,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 6,
		store: dsKecamatanSetupSupirAmbulance,
		valueField: 'KD_KECAMATAN',
		displayField: 'KECAMATAN',
		enableKeyEvents:true,
		listeners:{
			'select': function (a, b, c){
				selectKecamatanpasien = b.data.KD_KECAMATAN;
				loaddatastorekelurahan(b.data.KD_KECAMATAN);
				//Ext.getCmp('cbokelurahan').setValue();
				Ext.getCmp('cbokelurahan').focus(false)
			},
			keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress:function(c,e){
				if (e.keyCode == 13){
					Ext.getCmp('cbokelurahan').focus(false);
					selectKecamatanpasien = c.value;
				}else if (e.keyCode == 9){
					Ext.getCmp('cbokelurahan').focus(false);
					//loaddatastorekelurahan(c.value);
					selectKecamatanpasien = c.value;
				}
			}
		}
	});
    return cboKecamatanSetupSupirAmbulance;
}

function mCombokelurahan(){
    var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
    dsKelurahan = new WebApp.DataStore({fields: Field});
    var cbokelurahan = new Ext.form.ComboBox({
		id: 'cbokelurahan',
		x: 550,
		y: 125,
		width:130,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kelurahan...',
		fieldLabel: 'Kelurahan',
		align: 'Right',
		tabIndex: 6,
		store: dsKelurahan,
		valueField: 'KD_KELURAHAN',
		displayField: 'KELURAHAN',
		enableKeyEvents:true,
		listeners:{
			'select': function (a, b, c){
				kelurahanpasien = b.data.KD_KELURAHAN;
				tmp_kd_kelurahan = b.data.KD_KELURAHAN;
				console.log(kelurahanpasien);
				Ext.getCmp('txt_alamat_supir_ambulance').focus();
			},
			keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress:function(c,e){
				if (e.keyCode == 13) {
					Ext.getCmp('txtpos').focus();
				}else if (e.keyCode == 9){
					kelurahanpasien = c.value;
				}
			}
		}
	});
    return cbokelurahan;
}

/*function dataGridItemHasil_SetupHasilTest(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionSetupSupirAmbulance/getGridSupirAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupSupirAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				 var cst = Ext.decode(response.responseText);
	            Ext.getCmp('cboPropinsiSetupSupirAmbulance').setValue(cst.kd_propinsi);
	            Ext.getCmp('cboPropinsiKtp').setValue(cst.kd_propinsi);
	            loaddatastorekabupaten(cst.kd_propinsi);
	            loaddatastoreKtpkabupaten(cst.kd_propinsi);
	            selectPropinsiSetupSupirAmbulance  = cst.kd_propinsi;
	            selectPropinsiKtp           = cst.kd_propinsi;   
	            Ext.getCmp('cboKabupatenSetupSupirAmbulance').setValue(cst.kabupaten);
	            Ext.getCmp('cboKtpKabupaten').setValue(cst.kabupaten);
	            loaddatastorekecamatan(cst.kd_kabupaten);
	            loaddatastoreKtpkecamatan(cst.kd_kabupaten);
	            selectKabupatenSetupSupirAmbulance = cst.kd_kabupaten;
	            selectKabupatenKTP          = cst.kd_kabupaten;
	            Ext.getCmp('cboKecamatanSetupSupirAmbulance').setValue(cst.kecamatan);
	            Ext.getCmp('cboKecamatanKtp').setValue(cst.kecamatan);
	            loaddatastorekelurahan(cst.kd_kecamatan);
	            loaddatastoreKtpkelurahan(cst.kd_kecamatan);
	            selectKecamatanpasien   = cst.kd_kecamatan;
	            selectKecamatanktp      = cst.kd_kecamatan;
	            Ext.getCmp('cbokelurahan').setValue(cst.kelurahan);
	            Ext.getCmp('cbokelurahanKtp').setValue(cst.kelurahan);
	            kelurahanpasien     = cst.kd_kelurahan;
	            selectkelurahanktp  = cst.kd_kelurahan;
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupSupirAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupSupirAmbulance.add(recs);
					GridDataView_viSetupHasilTest.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupSupirAmbulance('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}*/

function AddNewSetupHasil(){
	Ext.getCmp('txtKdSupirAmbulance').setValue('');
	Ext.getCmp('txtNamaSupirAmbulance').setValue('');
	Ext.getCmp('tgl_lahir_supir_ambulance').setValue('');
	Ext.getCmp('txtno_sim_supir').setValue('');
	Ext.getCmp('txttgl_berlaku_sim_supir').setValue('');
	Ext.getCmp('cboPropinsiSetupSupirAmbulance').setValue('');
	Ext.getCmp('cboKabupatenSetupSupirAmbulance').setValue('');
	Ext.getCmp('cboKecamatanSetupSupirAmbulance').setValue('');
	Ext.getCmp('cbokelurahan').setValue('');
	Ext.getCmp('txt_alamat_supir_ambulance').setValue('');
};

function dataSave_viSetupSupirAmbulance(){
	if (ValidasiSaveSetupSupirAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupSupirAmbulance/save",
				params: getParamSaveSetupSupirAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupSupirAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						dataSource_viSetupSupirAmbulance.removeAll();
						//dataGridItemHasil_SetupHasilTest();
						dataGridSupirAmbulance();
						dataSave_viSetupSupirAmbulance
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupSupirAmbulance('Gagal menyimpan data ini', 'Error');
						dataSave_viSetupSupirAmbulance.removeAll();
						dataGridSupirAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamSaveSetupSupirAmbulance(){
	/*var kd_kelurahan_fix='';
	if(tmp_kd_kelurahan===''){
		kd_kelurahan_fix=Ext.getCmp('cbokelurahan').getValue() 
	}
	if(tmp_kd_kelurahan!=''){
		kd_kelurahan_fix=tmp_kd_kelurahan
	}
	console.log(tmp_kd_kelurahan);*/
	var	params =
	{
		kd_supir			  :Ext.getCmp('txtKdSupirAmbulance').getValue(),
		nama_supir	  		  :Ext.getCmp('txtNamaSupirAmbulance').getValue(),
		tgl_lahir 	    	  :Ext.getCmp('tgl_lahir_supir_ambulance').getValue(),
		no_sim 	 	  		  :Ext.getCmp('txtno_sim_supir').getValue(),
		batas_tgl_berlaku_sim :Ext.getCmp('txttgl_berlaku_sim_supir').getValue(),
		kd_kelurahan 		  :tmp_kd_kelurahan,
		alamat 				  :Ext.getCmp('txt_alamat_supir_ambulance').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupSupirAmbulance(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtNamaSupirAmbulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Nama masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('tgl_lahir_supir_ambulance').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Tanggal Lahir masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtno_sim_supir').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('No Sim masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txttgl_berlaku_sim_supir').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Tanggal Berlaku Sim masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cbokelurahan').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('kelurahan  belum dipilih', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupHasil(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupSupirAmbulance(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viSetupSupirAmbulance(){
	if (ValidasiSaveSetupSupirAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/ambulance/functionSetupSupirAmbulance/delete",
				params: getParamDeleteSetupSupirAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupSupirAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupHasil()
						dataSource_viSetupSupirAmbulance.removeAll();
						dataGridSupirAmbulance();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupSupirAmbulance('Gagal menghapus data ini', 'Error');
						dataSource_viSetupSupirAmbulance.removeAll();
						dataGridSupirAmbulance();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupSupirAmbulance(){
	var	params =
	{
		kd_supir:Ext.getCmp('txtKdSupirAmbulance').getValue()
	}
   
    return params
};

function DataInitSetupHasilTest(rowdata){
	console.log(rowdata.tgl_lahir);
	Ext.getCmp('txtKdSupirAmbulance').setValue(rowdata.kd_supir);
	Ext.getCmp('txtNamaSupirAmbulance').setValue(rowdata.nama_supir);
	Ext.getCmp('tgl_lahir_supir_ambulance').setValue(rowdata.tgl_lahir);
	Ext.getCmp('txtno_sim_supir').setValue(rowdata.no_sim);
	Ext.getCmp('txttgl_berlaku_sim_supir').setValue(rowdata.batas_tgl_berlaku_sim);
	Ext.getCmp('cboPropinsiSetupSupirAmbulance').setValue(rowdata.propinsi);
	Ext.getCmp('cboKabupatenSetupSupirAmbulance').setValue(rowdata.kabupaten);
	Ext.getCmp('cboKecamatanSetupSupirAmbulance').setValue(rowdata.kecamatan);
	Ext.getCmp('cbokelurahan').setValue(rowdata.kelurahan);
	tmp_kd_kelurahan=rowdata.kd_kelurahan;
	/*if(rowdata.kelurahan==='DEFAULT'){
		Ext.getCmp('cbokelurahan').setValue(tmp_kd_kelurahan);
	}*/
	//console.log(tmp_kd_kelurahan);
	Ext.getCmp('txt_alamat_supir_ambulance').setValue(rowdata.alamat);

};


function dataGridSupirAmbulance(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/ambulance/functionSetupSupirAmbulance/getGridSupirAmbulance",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupSupirAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupSupirAmbulance.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupSupirAmbulance.add(recs);
					console.log();
					GridDataView_viSetupHasilTest.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data supir', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupSupirAmbulance('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupSupirAmbulance('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningSetupHasil('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupSupirAmbulance('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupSupirAmbulance('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};

function loaddatastorekabupaten(kd_propinsi){
    dsKabupatenSetupSupirAmbulance.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'kabupaten',
			Sortdir: 'ASC',
			target: 'ViewComboKabupaten',
			param: 'kd_propinsi=' + kd_propinsi
		}
	});
}
function loaddatastorekecamatan(kd_kabupaten){
    dsKecamatanSetupSupirAmbulance.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'kecamatan',
			Sortdir: 'ASC',
			target: 'ViewComboKecamatan',
			param: 'kd_kabupaten=' + kd_kabupaten
		}
	});
}
function loaddatastorekelurahan(kd_kecamatan){
    dsKelurahan.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'kelurahan',
			Sortdir: 'ASC',
			target: 'ViewComboKelurahan',
			param: 'kd_kecamatan=' + kd_kecamatan
		}
	});
}	