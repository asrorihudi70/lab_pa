    
    function validasiJenisTr(){

    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
            notrans: 0,
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.kd_unit);
            kd_unit_konfig = "'"+cst.kd_unit+"'";
        }
    });

    var kriteria = "";
    var strkriteria = getCriteriaFilter_viDaftar();
    console.log(radiovalues);
    if (combovalues === 'Transaksi Lama'){
        Ext.getCmp('cboStatusLunas_viKasirAmb').enable();
        if(radiovalues === '1'){
            if(combovaluesunit !== ""){
                if (combovaluesunit === "All"){
                  tmpkreteriarad = "";
                }else{
                    tmpkreteriarad = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue()){
               tmpkriterianamarwi = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue()){
               tmpkriteriaalamatrwi = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue()){
               tmpkriteriamedrecrwi = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else
            {
                tmpkriteriamedrecrwi = "";
            }

            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";
            if(selectCountStatusLunasByr_viKasirRwj !== ""){
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas"){
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas"){
                    tmplunas = "and lunas='f'";
                }else{
                    tmplunas = "";
                }
            }
            // IGD
            console.log("A");
            kriteria = " kd_bagian = 7 and left(kd_unit_asal, 1) in('3') and left(kd_pasien, 2) not in ('RD') "+tmpkriteriaunittujuancrwi+" "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" "+tmplunas+" "+strkriteria+"   ORDER BY tgl_transaksi desc, no_transaksi ";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
            
        }else if (radiovalues === '2'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            
            /*if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }
*/
            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";

            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("B");
            kriteria = " kd_bagian = 7 "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"  "+tmplunas+" and left(kd_pasien, 2) not in ('RD')   and left(kd_unit_asal, 1) = '1'  "+strkriteria+" ORDER BY tgl_transaksi desc, no_transaksi ";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
        }else if (radiovalues === '3')
        {
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }

           /* if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }*/
            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";
            
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = " and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("C");
            kriteria = " kd_bagian = 7 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"  and left(kd_pasien, 2) = 'RD' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+tmplunas+"   ORDER BY tgl_transaksi desc, no_transaksi limit 5";
            tmpunit = 'ViewPenJasRadKunjunganLangsung';
            loadKasirAmb(kriteria, tmpunit);
            
        }else if (radiovalues === '4'){
        	console.log('umum');
            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";

            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                   tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("D");
            //"+tmplunas+"
            // kriteria = " kd_bagian = 7 and left(kd_unit_asal, 1) in('2') "+tmpkriteriaunittujuancrwi+" "+tmplunas+"  and left(kd_pasien, 2) not in ('RD')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "'  ORDER BY tgl_transaksi desc, no_transaksi";
            kriteria = "kd_bagian = 7 and left(kd_unit_asal, 1) in('4') and left(kd_pasien, 2) not in ('RD') "+tmpkriteriaunittujuancrwi+" "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" "+tmplunas+" "+strkriteria+"   ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
        }else
        {
            kriteria = "posting_transaksi = 'f'  and kd_bagian = 7 and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
        }
        
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viKasirAmb').disable();
        if(radiovalues === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc limit 5";
            tmpunit = 'ViewRwjPenjasRad';
            //loadKasirAmb(tmpparams, tmpunit);
            loadKasirAmb(tmpparams);
        }
        else if (radiovalues === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }

             if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
             tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1) IN ('1') ORDER BY tr.no_transaksi desc limit 5";
             tmpunit = 'ViewRwiPenjasRad';
             //loadKasirAmb(tmpparams, tmpunit);
             loadKasirAmb(tmpparams);
             
        }else if (radiovalues === '3')
        {
            PenJasRadLookUp();
            Ext.getCmp('txtnotlprad').setReadOnly(false);
        }else if (radiovalues === '4')
        {
             if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('2') ORDER BY tr.no_transaksi desc limit 5";
            tmpunit = 'ViewRwjPenjasRad';
            //loadKasirAmb(tmpparams, tmpunit);
            loadKasirAmb(tmpparams);
        }
        
    }
}
var CurrentKasirAmb ={
    data: Object,
    details: Array,
    row: 0
};

var mRecordRad = Ext.data.Record.create([
   {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
   {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
   {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
   {name: 'KD_TARIF', mapping:'KD_TARIF'},
   {name: 'HARGA', mapping:'HARGA'},
   {name: 'QTY', mapping:'QTY'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'DESC_REQ', mapping:'DESC_REQ'},
   {name: 'URUT', mapping:'URUT'}
]);
var Trkdunit2;
var CurrentHistory ={
    data: Object,
    details: Array,
    row: 0
};
var indexRow = "";
var flag_error='1';
var paneltotal_history_bayar;
var tmpno_trans_sql;
var DataMobilAMBDS;
var DataSupirAMBDS;
var tmp_kd_supir;
var tmp_kd_mobil;
//---------TAMBAH BARU 27-SEPTEMBER-2017
var selectSetGolDarah;
var selectSetJK;
var ID_JENIS_KELAMIN;
var dsRujukanAmbulance;
var tmpkd_unit = '';
var tmpkd_unit_asal = '';
var tmpkd_unit_tarif_mir = '';
var kodeunit;
var tmp_kodeunitkamar_RAD;
var tmp_kdspesial_RAD;
var tmp_nokamar_RAD;
var tmplunas;
var kodeunittransfer;
var tglTransaksiAwalPembandingTransferRAD;
var dsTRDetailHistoryList_amb;
var kodepasien;
var namapasien;
var namaunit;
var kodepay ;
var uraianpay;
var nilai;
var mRecordKasirRAD = Ext.data.Record.create([
    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
    {name: 'HARGA', mapping: 'HARGA'},
    {name: 'QTY', mapping: 'QTY'},
    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
    {name: 'URUT', mapping: 'URUT'}
]);
var GridGetTindakanTrKasirAmbulance;
var AddNewKasirRADKasir = true;
var dsTRDetailDiagnosaList;
var dsNamaRujukanAmbulance;
var AddNewDiagnosa = true;
var kdpaytransfer = 'T1';
var kdkasir;
var kdkasir_tmp;
var tanggaltransaksitampung;
var kdkasirasal_rad;
var notransaksiasal_rad;
var kdcustomeraa;
var vflag;
var tmp_NoTransaksi;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var tampungtypedata;
var jenispay;
var tapungkd_pay;
var tranfertujuan = 'Rawat Inap';
var cellSelecteddeskripsi;
var vkd_unit;
var notransaksi;
var noTransaksiPilihan;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();
var tigaharilalu = new Date().add(Date.DAY, -0);
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var asal_pasien;
var tigaharilaluNew = tigaharilalu.format("d/M/Y");

var gridDTItemTest;

var selectSetDr;
var selectSetGDarah;
var selectSetJK;
var selectSetKelPasien;
var selectSetPerseorangan;
var selectSetPerusahaan;
var selectSetAsuransi;

var radelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Posting';
var selectCountStatusLunasByr_viKasirRwj='Belum Lunas';
var selectCountJenTr_viKasirAmb='Transaksi Baru';
var dsTRKasirAmbulanceList;
var dsTRDetailKasirAmbList;
var AddNewKasirAmb = true;
var selectCountKasirAmb = 50;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksi='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var kodeUnitRad;
var dsPeroranganRadRequestEntry;

var rowSelectedKasirAmb;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var tmpparams = '';
var grListKasirAmbulance;

//VALIDASI JENIS TRANSAKSI
var combovalues = 'Transaksi Baru';
var radiovalues = '1';
var combovaluesunit = "";
var combovaluesunittujuan = "";
var ComboValuesKamar = "";

var vkode_customerRAD;
CurrentPage.page = getPanelKasirAmb(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var tmpurut;
var citopersentasi = 0;
var kd_unit_konfig = "";

var KasirAMBDataStoreMobil=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});

//membuat form

     Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/get_zusers",
        params: {
            command: '0',
        },
        failure: function (o)
        {
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            kd_unit_konfig = cst.data_user[0].kd_unit;
            // console.log(cst.data_user[0].kd_unit);
        }
    });
function msg_box_alasanbukatrans_AMB(data)
{
    var lebar = 250;
    form_msg_box_alasanbukatrans_AMB = new Ext.Window
            (
                    {
                        id: 'alasan_bukatransRAD',
                        title: 'Alasan Buka Transaksi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanbukatransRAD',
                                                    id: 'txtAlasanbukatransRAD',
                                                    emptyText: 'Alasan Buka Transaksi',
                                                    anchor: '99%',
                                                },
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Buka',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_bukatransLAb',
                                                                    handler: function ()
                                                                    {
                                                                        if (Ext.getCmp('txtAlasanbukatransRAD').getValue().trim()==='' || Ext.getCmp('txtAlasanbukatransRAD').getValue().trim==='Alasan Buka Transaksi')
                                                                        {
                                                                            ShowPesanWarningKasirAmb("Isi alasan buka transaksi !", "Perhatian");
                                                                        }else{
                                                                            Ext.Ajax.request
                                                                            (
                                                                                    {
                                                                                        url: baseURL + "index.php/main/functionKasirPenunjang/bukatransaksi",
                                                                                        params: {
                                                                                            no_transaksi : data.notrans_bukatrans,
                                                                                            kd_kasir     : data.kd_kasir_bukatrans,
                                                                                            tgl_transaksi: data.tgl_transaksi_bukatrans,
                                                                                            kd_pasien    : data.kd_pasien_bukatrans,
                                                                                            urut_masuk   : data.urut_masuk_bukatrans,
                                                                                            kd_unit      : data.kd_unit_bukatrans,
                                                                                            keterangan   : Ext.getCmp('txtAlasanbukatransRAD').getValue(),
                                                                                            kd_bagian_shift : 5
                                                                                        },
                                                                                        success: function (o)
                                                                                        {
                                                                                            //  RefreshDatahistoribayar_LAB(Kdtransaksi);
                                                                                           // RefreshDataFilterKasirLABKasir();
                                                                                            //RefreshDatahistoribayar_LAB('0');
                                                                                            var cst = Ext.decode(o.responseText);
                                                                                            if (cst.success === true)
                                                                                            {
                                                                                                ShowPesanInfoKasirAmb("Proses Buka Transaksi Berhasil", nmHeaderHapusData);
                                                                                                Ext.getCmp('btnBukaTransaksiRAD').disable();
                                                                                                validasiJenisTr();
                                                                                                
                                                                                            } else {
                                                                                                ShowPesanWarningKasirAmb(nmPesanHapusError, nmHeaderHapusData);
                                                                                            }
                                                                                            ;
                                                                                        }
                                                                                    }
                                                                            )
                                                                            form_msg_box_alasanbukatrans_AMB.close();
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_bukatransRAD',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanbukatrans_AMB.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanbukatrans_AMB.show();
}
function getPanelKasirAmb(mod_id) 
{
    var Field = ['KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
                 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT',
                 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 
                 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','KD_UNIT_ASAL','HP','SJP','NO_FOTO'];
    dsTRKasirAmbulanceList = new WebApp.DataStore({ fields: Field });
    
     var k="tr.tgl_transaksi >='"+tigaharilaluNew+"' and tr.tgl_transaksi <='"+tglGridBawah+"'    and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc limit 5";
    //---------------------EDIT DEFAULT UNIT RAD 31 01 2017
    
    refeshtrkasiramb(k);
    getcitorad();
    loadDataComboMobilAmbulance();
    loadDataComboSupirAmbulance();
    //loadDataComboRujukanAmbulance();
    //console.log(loadDataComboRujukanAmbulance());

    grListKasirAmbulance = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            id:'KasirAmb',
            store: dsTRKasirAmbulanceList,
            anchor: '100% 50%',
            columnLines: false,
            autoScroll:true,
            border: false,
            sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedKasirAmb = dsTRKasirAmbulanceList.getAt(row);
                            console.log(rowSelectedKasirAmb.data.KD_KASIR);
                            kdkasir_tmp = rowSelectedKasirAmb.data.KD_KASIR;
                            if(Ext.get('cboJenisTr_viKasirAmb').getValue()=='Transaksi Baru'){
                                Ext.getCmp('btnBatalTransaksiRad').disable();
                                Ext.getCmp('btnGantiKelompokRad').disable();
                                Ext.getCmp('btnGantiDokterRad').disable();
                                Ext.getCmp('btnBukaTransaksiRAD').disable();
                                Ext.getDom('rb_pilihan1').checked = true;
                            }
                            else
                            {
                                
                                //alert(vKdUnit);
                                     Ext.getDom('rb_pilihan1').checked = true;
                                if (rowSelectedKasirAmb.data.LUNAS==='t' || rowSelectedKasirAmb.data.LUNAS==='true' || rowSelectedKasirAmb.data.LUNAS===true){
                                    Ext.getCmp('btnGantiKelompokRad').disable();
                                    Ext.getCmp('btnGantiDokterRad').disable();
                                    if (rowSelectedKasirAmb.data.CO_STATUS==='t' || rowSelectedKasirAmb.data.CO_STATUS==='true' || rowSelectedKasirAmb.data.CO_STATUS===true){
                                        Ext.getCmp('btnBukaTransaksiRAD').enable();
                                    }else{
                                        Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    }
                                    
                                    Ext.getCmp('btnBatalTransaksiRad').disable();
                                }else{
                                    Ext.getCmp('btnGantiKelompokRad').enable();
                                    Ext.getCmp('btnGantiDokterRad').enable();
                                    Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    Ext.getCmp('btnBatalTransaksiRad').enable();
                                }
                                
                            }
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedKasirAmb = dsTRKasirAmbulanceList.getAt(ridx);
                    noTransaksiPilihan = rowSelectedKasirAmb.data.NO_TRANSAKSI;
                    if (rowSelectedKasirAmb !== undefined){
                         console.log(rowSelectedKasirAmb.data);
                        KasirAmbLookUp(rowSelectedKasirAmb.data);
                    }
                    else{
                        KasirAmbLookUp();
                        Ext.getCmp('cboDOKTER_viKasirAmb').disable();
                        Ext.getCmp('cboGDR').disable();
                        Ext.getCmp('cboJK').disable();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   new Ext.grid.RowNumberer(),
                    {
                        id: 'colReqIdViewRWJ',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglRWJViewRWJ',
                        header: 'Tgl Transaksi',
                        dataIndex: 'Tgl_transaksi',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 75,
                        renderer: function(v, params, record)
                        {
                            return ShowDate(record.data.TGL_TRANSAKSI);

                        }
                    },
                    {
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colRWJerViewRWJ'
                    },
                    {
                        header: ' Nama Pasien',
                        width: 190,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colRWJerViewRWJ'
                    },
                    {
                        id: 'colLocationViewRWJ',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 170
                    },
                    
                    /*{
                        id: 'colDeptViewRWJ',
                        header: 'Dokter',
                        dataIndex: 'DOKTER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 150
                    },*/
                    {
                        id: 'colImpactViewRWJ',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASLI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
                    {
                        header: 'Status Transaksi',
                        width: 100,
                        sortable: false,
                        hideable:true,
                        hidden:true,
                        menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtposting',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                        
                    },
                    {
                        id: 'colCoSTatusViewRWJ',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case '':
                                         metaData.css = 'StatusMerah'; // 
                                         break;
                                 case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colBukaTransaksiViewRAD',
                        header: 'Status Tutup Transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    }/*,
                    {
                        id: 'colnofotViewrad',
                        header: 'No. Foto',
                        dataIndex: 'NO_FOTO',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 100
                    }*/
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditRWJ',
                        text: 'Lookup',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec){
                            console.log(rowSelectedKasirAmb);
                            if (rowSelectedKasirAmb != undefined){
                                    KasirAmbLookUp(rowSelectedKasirAmb.data);
                            }
                            else{
                            ShowPesanWarningKasirAmb('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },
                    {
                        id: 'btnBatalTransaksiRad',
                        text: 'Batal Transaksi',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                         //     console.log(rowSelectedKasirAmb);
                            var tmpdatatransaksi = rowSelectedKasirAmb.data;
                         //   console.log(tmpdatatransaksi);
                            if (Ext.get('cboJenisTr_viKasirAmb').getValue() === 'Transaksi Lama') {
                                if (rowSelectedKasirAmb === undefined) {
                                    ShowPesanWarningKasirAmb('Silahkan Pilih Data Terlebih Dahulu');
                                }else{
                                    Ext.Msg.confirm('Warning', 'Apakah Transaksi ini akan dihapus?', function(button){
                                        if (button == 'yes'){
                                             console.log(tmpdatatransaksi);
                                            var urutmasuktransaksi;
                                            if (tmpdatatransaksi.URUT != '') {
                                                urutmasuktransaksi = tmpdatatransaksi.URUT;
                                            }else{
                                                urutmasuktransaksi = tmpdatatransaksi.URUT_MASUK;
                                            }
                                            if(rowSelectedKasirAmb != undefined) {
                                              
                                                var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan pembatalan:', function(btn, combo){
                                                 if (btn == 'ok')
                                                 {
                                                 //variablehistori=combo;
                                                 if (combo!='')
                                                 {
                                                     var params = {
                                                        kd_unit: tmpdatatransaksi.KD_UNIT,
                                                        Tglkunjungan: tmpdatatransaksi.TGL_TRANSAKSI,
                                                        Kodepasein: tmpdatatransaksi.KD_PASIEN,
                                                        urut: urutmasuktransaksi,
                                                        alasanbatal: combo
                                                    };
                                                    console.log(params);
                                                    Ext.Ajax.request
                                                            ({
                                                                url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/deletekunjungan",
                                                                params: params,
                                                                failure: function (o)
                                                                {
                                                                    loadMask.hide();
                                                                    ShowPesanWarningKasirAmb('Error proses database', 'Hapus Data');
                                                                },
                                                                success: function (o)
                                                                {
                                                                    var cst = Ext.decode(o.responseText);
                                                                    if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false)
                                                                    {
                                                                        ShowPesanWarningKasirAmb('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
                                                                    } else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true)
                                                                    {
                                                                        ShowPesanWarningKasirAmb('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
                                                                    } else if (cst.success === true)
                                                                    {
                                                                        // RefreshDatahistoribayar(datapasienvariable.kd_pasien);
                                                                        ShowPesanInfoKasirAmb('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                        validasiJenisTr();
                                                                    } else if (cst.success === false && cst.pesan === 0)
                                                                    {
                                                                        ShowPesanWarningKasirAmb('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                    } else
                                                                    {
                                                                        if (cst.kd_kasir) {
                                                                            ShowPesanWarningKasirAmb('Anda tidak diberikan hak akses untuk menghapus data kunjungan '+cst.kd_kasir, 'Hapus Data Kunjungan');
                                                                        }else{
                                                                            ShowPesanErrorKasirAmb('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                        }
                                                                    };
                                                                }})
                                                 }
                                                 else
                                                 {
                                                 ShowPesanWarningKasirAmb('Silahkan isi alasan terlebih dahaulu','Keterangan');
                                                 }
                                                 }  
                                                 }); 
                                            }
                                        }
                                    })
                                }
                            }else {

                            }
                        }
                    },
                    {
                        id: 'btnGantiKelompokRad',
                        text: 'Ganti Kelompok',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            var tmpdatatransaksi = rowSelectedKasirAmb.data;
                            panelActiveDataPasien = 0;
                            KelompokPasienLookUp_rad(tmpdatatransaksi);
                        }
                    },
                    {
                        id: 'btnGantiDokterRad',
                        text: 'Ganti Dokter',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            var tmpdatatransaksi = rowSelectedKasirAmb.data;
                            panelActiveDataPasien = 1;
                            GantiDokterPasienLookUp_rad(tmpdatatransaksi);

                        }
                    },{                             
                        id: 'btnBukaTransaksiRAD', text: 'Buka Transaksi', tooltip: 'Buka Transaksi', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            
                            var parameter_bukatrans = {
                                notrans_bukatrans       : rowSelectedKasirAmb.data.NO_TRANSAKSI,
                                kd_kasir_bukatrans      : rowSelectedKasirAmb.data.KD_KASIR,
                                kd_pasien_bukatrans     : rowSelectedKasirAmb.data.KD_PASIEN,
                                kd_unit_bukatrans       : rowSelectedKasirAmb.data.KD_UNIT,
                                tgl_transaksi_bukatrans : rowSelectedKasirAmb.data.TGL_TRANSAKSI,
                                urut_masuk_bukatrans    : rowSelectedKasirAmb.data.URUT_MASUK
                            }
                            msg_box_alasanbukatrans_AMB(parameter_bukatrans);
                        }
                    },
                ]
            }
    );
    
    
    //form depan awal dan judul tab
    var FormDepanTrKasirAmbulance = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir Ambulance',
            border: false,
            shadhow: true,
            autoScroll:false,
            width: '100%',
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                        getItemPanelTrKasirAmbulance(),
                        grListKasirAmbulance
                   ],
            listeners:
            {
                'afterrender': function()
                {
                    Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').hide();
                    Ext.getCmp('cboStatusLunas_viKasirAmb').disable();
                    Ext.getCmp('btnBatalTransaksiRad').disable();
                    Ext.getCmp('btnGantiKelompokRad').disable();
                    Ext.getCmp('btnGantiDokterRad').hide();
                    Ext.getCmp('btnBukaTransaksiRAD').disable();    
                },
                'close':function(){
                    rowSelectedKasirAmb=undefined;
                }
            }
        }
    );
    
   return FormDepanTrKasirAmbulance;

};


function mComboStatusBayar_viKasirAmb()
{
  var cboStatus_viKasirAmb = new Ext.form.ComboBox
    (
        {
                    id:'cboStatus_viKasirAmb',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusByr_viKasirRwj,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusByr_viKasirRwj=b.data.displayText ;
                            }
                    }
        }
    );
    return cboStatus_viKasirAmb;
};
function cboStatusLunas_viKasirAmb()
{
  var cboStatusLunas_viKasirAmb = new Ext.form.ComboBox
    (
        {
                    id:'cboStatusLunas_viKasirAmb',
                    x: 155,
                    y: 100,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 120,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusLunasByr_viKasirRwj,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusLunasByr_viKasirRwj=b.data.displayText ;
                            }
                    }
        }
    );
    return cboStatusLunas_viKasirAmb;
};

//COMBO JENIS TRANSAKSI
function mComboJenisTrans_viKasirAmb()
{
  var cboJenisTr_viKasirAmb = new Ext.form.ComboBox
    (
        {
                    id:'cboJenisTr_viKasirAmb',
                    x: 155,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 120,
                    emptyText:'',
                    fieldLabel: 'JENIS TRANSAKSI',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Transaksi Baru'],[2, 'Transaksi Lama']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountJenTr_viKasirAmb,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                combovalues=b.data.displayText;
                                if(b.data.Id==1){
//                                    Ext.getCmp('cbounitrads_viKasirAmb').setValue();
                                   // Ext.getCmp('cbounitrads_viKasirAmb').disable();
                                    Ext.getCmp('btnBatalTransaksiRad').disable();
                                    Ext.getCmp('btnGantiKelompokRad').disable();
                                    Ext.getCmp('btnGantiDokterRad').disable();
                                    Ext.getCmp('btnBukaTransaksiRAD').disable();
                                    tmppasienbarulama = 'Baru';
                                }else{
                          //          Ext.getCmp('cbounitrads_viKasirAmb').enable();
                           //         Ext.getCmp('cbounitrads_viKasirAmb').setValue(combovaluesunittujuan);
                                    tmppasienbarulama = 'Lama';
                                    Ext.getCmp('btnBatalTransaksiRad').enable();
                                    Ext.getCmp('btnGantiKelompokRad').enable();
                                    Ext.getCmp('btnGantiDokterRad').enable();
                                    Ext.getCmp('btnBukaTransaksiRAD').enable();
                                }
                                
                                validasiJenisTr();
                                
                            }
                    }
        }
    );
    return cboJenisTr_viKasirAmb;
};

var tmppasienbarulama = 'Baru';

function validasiJenisTr(){

    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
            notrans: 0,
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.kd_unit);
            kd_unit_konfig = "'"+cst.kd_unit+"'";
        }
    });

    var kriteria = "";
    var strkriteria = getCriteriaFilter_viDaftar();
    console.log(combovalues);
    if (combovalues === 'Transaksi Lama'){
        Ext.getCmp('cboStatusLunas_viKasirAmb').enable();
    
        if(radiovalues === 'IGD'){
            if(combovaluesunit !== ""){
                if (combovaluesunit === "All"){
                  tmpkreteriarad = "";
                }else{
                    tmpkreteriarad = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue()){
               tmpkriterianamarwi = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue()){
               tmpkriteriaalamatrwi = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')"; 
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue()){
               tmpkriteriamedrecrwi = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }

            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            // IGD
            console.log("IGD");
            kriteria = " kd_bagian = 7 and left(kd_unit_asal, 1) in('3') and left(kd_pasien, 2) not in ('RD') "+tmpkriteriaunittujuancrwi+" "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" "+tmplunas+" "+strkriteria+"   ORDER BY tgl_transaksi desc, no_transaksi ";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
            
        }else if (radiovalues === 'RWI'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";

            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("RWI");
            kriteria = " kd_bagian = 7 "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"  "+tmplunas+" and left(kd_pasien, 2) not in ('RD')  "+strkriteria+" ORDER BY tgl_transaksi desc, no_transaksi ";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
        }else if (radiovalues === 'RWJ')
        {
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }

            /*if(Ext.getCmp('cbounitrads_viPenJasRad').getValue() != null && Ext.getCmp('cbounitrads_viPenJasRad').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitrads_viPenJasRad').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            }*/
            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";
            
            if(selectCountStatusLunasByr_viKasirRwj !== "")
            {
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas")
                {
                    tmplunas = " and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            console.log("RWJ");
            kriteria = " kd_bagian = 7 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +" AND  asal_pasien IN ( '1' ) and left(kd_pasien, 2) = 'RD' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+tmplunas+"   ORDER BY tgl_transaksi desc, no_transaksi limit 5";
            tmpunit = 'ViewPenJasRadKunjunganLangsung';
            loadKasirAmb(kriteria, tmpunit);
            
        }else if (radiovalues === 'UMUM' || Ext.getDom('rb_pilihan3').checked)
        {
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue()){
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianama = "";
            }

             if (Ext.getCmp('txtalamatpasienrad').getValue()){
               tmpkriteriaalamat = " AND lower(ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue()){
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else{
                tmpkriteriamedrec = "";
            }
            tmpkriteriaunittujuancrwi = " AND kd_unit in ("+kd_unit_konfig+")";

            if(selectCountStatusLunasByr_viKasirRwj !== ""){
                if(selectCountStatusLunasByr_viKasirRwj === "Lunas"){
                   tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirRwj === "Belum Lunas"){
                    tmplunas = "and lunas='f'";
                }else{
                    tmplunas = "";
                }
            }
            console.log("UMUM");
            //"+tmplunas+"
            // kriteria = " kd_bagian = 7 and left(kd_unit_asal, 1) in('2') "+tmpkriteriaunittujuancrwi+" "+tmplunas+"  and left(kd_pasien, 2) not in ('RD')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "'  ORDER BY tgl_transaksi desc, no_transaksi";
            kriteria = " kd_bagian = 7 "+tmpkriteriaunittujuancrwi+" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +"  "+tmplunas+" and left(kd_pasien, 2) not in ('RD')   AND  asal_pasien IN ( '4' )   "+strkriteria+" ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'Umum';
            loadKasirAmb(kriteria, tmpunit);
        }else
        {
            kriteria = "posting_transaksi = 'f'  and kd_bagian = 7 and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasRad';
            loadKasirAmb(kriteria, tmpunit);
        }
        
    }else if (combovalues === 'Transaksi Baru'){
         console.log(radiovalues);
        Ext.getCmp('cboStatusLunas_viKasirAmb').disable();
        if(radiovalues === 'RWJ')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc limit 5";
            tmpunit = 'ViewRwjPenjasRad';
            //loadKasirAmb(tmpparams, tmpunit);
            refeshtrkasiramb(tmpparams);
        }
        else if (radiovalues === 'RWI')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }

             if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamat = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamat = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
             tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriaalamat +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1) IN ('1') ORDER BY tr.no_transaksi desc limit 5";
             tmpunit = 'ViewRwiPenjasRad';
            // loadKasirAmb(tmpparams, tmpunit);
            refeshtrkasiramb(tmpparams);

             
        }else if (radiovalues === 'UGD')
        {
//	            PenJasRadLookUp();
//            Ext.getCmp('txtnotlprad').setReadOnly(false);
        }else if (radiovalues === '4')
        {
             if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriarad = "";
                }else
                {
                tmpkreteriarad = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRad').getValue() + "'";
                }
            }else {
                tmpkreteriarad = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasirAmb').getValue())
            {
               tmpkriterianamarwi = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasirAmb').dom.value + "%')";
            }else{
                tmpkriterianamarwi = "";
            }

            if (Ext.getCmp('txtalamatpasienrad').getValue())
            {
               tmpkriteriaalamatrwi = " AND lower(pasien.ALAMAT) like lower('%" + Ext.get('txtalamatpasienrad').dom.value + "%')";
            }else{
                tmpkriteriaalamatrwi = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasirAmb').getValue())
            {
               tmpkriteriamedrecrwi = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasirAmb').dom.value + "'";
            }else{
                tmpkriteriamedrecrwi = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue() + "' "+ tmpkreteriarad +" "+ tmpkriterianamarwi +" "+ tmpkriteriaalamatrwi +" "+ tmpkriteriamedrecrwi +" and left(u.kd_unit,1) IN ('2') ORDER BY tr.no_transaksi desc limit 5";
            tmpunit = 'ViewRwjPenjasRad';
            //loadKasirAmb(tmpparams, tmpunit);
            refeshtrkasiramb(tmpparams);
        }
        
    }
}

//VALIDASI COMBO UNIT/POLI
function getDataCariUnitKasirAmb(kriteria)
{
    if (kriteria===undefined)
    {
        kriteria="kd_bagian=3 and parent<>'0'";
    }
    dsunit_viKasirRwj.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ViewSetupUnit',
                        param: kriteria
                    }
            }
    );
    return dsunit_viKasirRwj;
}
function mComboUnit_viKasirAmb() 
{
    
    var Field = ['KD_UNIT','NAMA_UNIT'];
    
    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    getDataCariUnitKasirAmb();
    var cboUNIT_viKasirRad = new Ext.form.ComboBox
    (
            {
                id: 'cboUNIT_viKasirRad',
                x: 155,
                y: 70,
                typeAhead: true,
                triggerAction: 'all',
                emptyText:'Poli',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 120,
                store: dsunit_viKasirRwj,
                valueField: 'KD_UNIT',
                displayField: 'NAMA_UNIT',
                value:'All',
                listeners:
                    {

                        'select': function(a, b, c) 
                            {                          
                                //RefreshDataFilterKasirAmb();
                                combovaluesunit=b.data.valueField;
                                validasiJenisTr();
                            }

                    }
            }
    );
    
    return cboUNIT_viKasirRad;
};

function mcomboKamarSpesial()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viJasRad = new WebApp.DataStore({fields: Field});

    ds_KamarSpesial_viJasRad.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialTrKasirAMBRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 155,
            y: 70,
            id: 'cboRujukanKamarSpesialTrKasirAMBRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viJasRad,
            valueField: 'no_kamar',
            displayField: 'kamar',
            Width:'120',
            listeners:
                {
                    'select': function(a, b, c)
                    {
                        ComboValuesKamar=b.data.valueField;
                        validasiJenisTr();    
                    },
                    'render': function(c)
                    {
                        
                    }


        }
        }
    )

    return cboRujukanKamarSpesialTrKasirAMBRequestEntry;
}

//LOOKUP DETAIL TRANSAKSI RADORATORIUM
function KasirAmbLookUp(rowdata) 
{
 //   loadDataComboRujukanAmbulance();
//    loadDataComboNamaRujukanAmbulance();
 //  loadDataComboMobilAmbulance();
   // loadDataComboSupirAmbulance();
    //Ext.getCmp('cboAsuransiAmb').disabled();
    //Ext.getCmp('cboPerusahaanRequestEntryAmb').disabled();
    var lebar = 940;
    FormLookUpsdetailTRrwj = new Ext.Window
    (
        {
            id: 'gridkasirAmbulance',
            title: 'Kasir Ambulance',
            closeAction: 'destroy',
            width: lebar,
            height: 550,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryKasirAmbulance(lebar,rowdata),
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){
                        Ext.getCmp('txtNamaPasienL').focus(false,100);
                        Ext.getCmp('txtNoMedrecL').setReadOnly(true);
                        Ext.getCmp('txtNamaUnit').setReadOnly(true);
                        Ext.getCmp('txtNamaPasienL').setReadOnly(false);
                        Ext.getCmp('txtAlamatL').setReadOnly(false);
                        Ext.getCmp('dtpTtlL').setReadOnly(false);
                        Ext.getCmp('txtCustomerLamaHide').hide();
                    }
                    
                    
                }
            }
        }
    );

    FormLookUpsdetailTRrwj.show();
    if (rowdata === undefined) 
    {
        KasirAmbAddNew();
    }
    else 
    {
        TRKasirAmbInit(rowdata);
    }

};
function TrKasirLookUpAMB(rowdata){
    console.log(rowdata);
    var lebar = 580;
    var FormLookUpsdetailTRKasirAmb = new Ext.Window
            (
                    {
                        id: 'gridKasirAmb',
                        title: 'Kasir Ambulance',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRKasirAMB(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRKasirAmb.show();
    /* if (rowdata == undefined)
    {
        PenjasBayarRADAddNew();
    } else
    { */
        TRPenjasBayarAMBInit(rowdata)
    //}

}
;
function mEnabledKasirRADCM(mBol)
{
   /*  Ext.get('btnLookupKasirRAD').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirAMB').dom.disabled = mBol; */
}
;
function PenjasBayarRADAddNew()
{
    AddNewKasirRADKasir = true;
    Ext.get('txtNoTransaksiKasirAMBKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTgltransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirRADKasir').dom.value = ''
    rowSelectedKasirAmb = undefined;
    dsTRDetailKasirAmbList.removeAll();
    mEnabledKasirRADCM(false);


}
;
function loaddatastorePembayaran(jenis_pay)
{
    // console.log(jenis_pay);
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}
function TRPenjasBayarAMBInit(rowdata){
    console.log(rowdata);
    AddNewKasirRADKasir = false;
    
    // console.log(rowdata);
    if (rowdata===undefined)
    {
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganL').getValue().format('d/M/Y');
        Ext.get('txtNoMedrecDetransaksi').dom.value = Ext.getCmp('txtNoMedrecL').getValue();
        Ext.get('txtNamaPasienDetransaksi').dom.value = Ext.getCmp('txtNamaPasienL').getValue();
        tanggaltransaksitampung = Ext.getCmp('dtpKunjunganL').getValue();
    }
    else{
           console.log(rowdata.TGL_TRANSAKSI); 
        tanggaltransaksitampung = rowdata.TGL_TRANSAKSI;
       // Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganL').getValue().format('d/M/Y'); //ShowDate(rowdata.TGL_TRANSAKSI);
        Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TGL_TRANSAKSI); //ShowDate(rowdata.TGL_TRANSAKSI);
        Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
        Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
    }
    
    var notransnya;
    if (Ext.getCmp('txtNoTransaksiAmbulance').getValue()==='' || Ext.getCmp('txtNoTransaksiAmbulance').getValue()===undefined){
        notransnya=rowdata.NO_TRANSAKSI;
    }
    else{
        notransnya=Ext.getCmp('txtNoTransaksiAmbulance').getValue();
    }
    console.log(asal_pasien);   
    var modul='';
    if(asal_pasien == 1){
        modul='rwj';
    } else if(asal_pasien == 2){
        modul='rwi';
    } else if(asal_pasien == 3){
        modul='ugd';
    } else{
        modul='langsung';
    }
    //disini
    Ext.get('txtNoTransaksiKasirAMBKasir').dom.value = Ext.getCmp('txtNoTransaksiAmbulance').getValue();
    RefreshDataKasirRADKasirDetail(notransnya,kd_kasir_amb);
    notransaksiasal_rad='';
    // take the displayField value 
    Ext.Ajax.request({
        url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/cekPembayaran",
        params: {
            notrans: Ext.getCmp('txtNoTransaksiKasirAMBKasir').getValue(),
            notrans1: Ext.getCmp('txtNoTransaksiAmbulance').getValue(),
            Modul:modul,
            KdKasir:34,
            KdPasien:Ext.getCmp('txtNoMedrecL').getValue()
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);

        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
            notransaksi=cst.ListDataObj[0].no_transaksi;
            vkd_unit = cst.ListDataObj[0].kd_unit;
            kodeunit=cst.ListDataObj[0].kd_unit;
            kdkasir = cst.ListDataObj[0].kd_kasir;
            notransaksiasal_rad = cst.ListDataObj[0].no_transaksi_asal;
            kdkasirasal_rad = cst.ListDataObj[0].kd_kasir_asal;
            Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
            Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
            vflag = cst.ListDataObj[0].flag;
            tapungkd_pay = cst.ListDataObj[0].kd_pay;
            vkode_customerRAD =  cst.ListDataObj[0].kd_customer;
        }

    });
    console.log(notransaksiasal_rad);
    tampungtypedata = 0;
    
    jenispay = 1;
    showCols(Ext.getCmp('gridDTItemTest'));
    hideCols(Ext.getCmp('gridDTItemTest'));
   
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });



}
;
function getItemPanelNoTransksiAMBKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirAMBKasir',
                                                id: 'txtNoTransaksiKasirAMBKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        RefreshDataKasirRADKasirDetail(b.data.JENIS_PAY)
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTItemTest'));
									    hideCols(Ext.getCmp('gridDTItemTest'));
                                        getTotalDetailProdukAMB();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function getTotalDetailProdukAMB(){
    // console.log(nilai);
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    var tampung_nilai=Ext.getCmp('txtJumlah2EditData_viKasirAMB').getValue();
    var karakter = tampung_nilai.length;
    if(karakter>=8){
         var nilai1= tampung_nilai.replace('.', '');
             nilai =nilai1.replace('.','');
    }else{
             nilai=tampung_nilai.replace('.',''); 
    }
   
    console.log(parseInt(nilai));
    for (var i = 0; i < dsTRDetailKasirAMBKasirList.getCount(); i++){
        var recordterakhir;
        //alert(TotalProduk);
        if (tampungtypedata == '0'){
            tampunggrid = parseInt(dsTRDetailKasirAMBKasirList.data.items[i].data.BAYARTR);
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirAMB').dom.value = formatCurrency(recordterakhir);
            var tampung_record=dsTRDetailKasirAMBKasirList.data.items[i].data.BAYARTR;
            console.log(tampung_record);
            if( parseInt(dsTRDetailKasirAMBKasirList.data.items[i].data.BAYARTR) > parseInt(nilai)){
                dsTRDetailKasirAMBKasirList.data.items[i].data.BAYARTR=0;
                ShowPesanWarningKasirAmb('Jumlah Tunai Yang Di input Melebihi Harga Awal!', 'Gagal');
                Ext.getCmp('txtJumlah2EditData_viKasirAMB').setValue(tampung_nilai);

            }
        }
        if (tampungtypedata == 3){
            tampunggrid = parseInt(dsTRDetailKasirAMBKasirList.data.items[i].data.PIUTANG);
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirAMB').dom.value = formatCurrency(recordterakhir);
            if( parseInt(dsTRDetailKasirAMBKasirList.data.items[i].data.PIUTANG) > parseInt(nilai)){
                 dsTRDetailKasirAMBKasirList.data.items[i].data.PIUTANG=0;
                ShowPesanWarningKasirAmb('Jumlah Puitang Yang Di input Melebihi Harga Awal!', 'Gagal');
                Ext.getCmp('txtJumlah2EditData_viKasirAMB').setValue(tampung_nilai);
            }
        }
        if (tampungtypedata == 1){
            tampunggrid = parseInt(dsTRDetailKasirAMBKasirList.data.items[i].data.DISCOUNT);
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirAMB').dom.value = formatCurrency(recordterakhir);
            if( parseInt(dsTRDetailKasirAMBKasirList.data.items[i].data.DISCOUNT) > parseInt(nilai)){
                dsTRDetailKasirAMBKasirList.data.items[i].data.DISCOUNT=0;
                ShowPesanWarningKasirAmb('Jumlah Discount Yang Di input Melebihi Harga Awal!', 'Gagal');
                flag_error='0';
                Ext.getCmp('txtJumlah2EditData_viKasirAMB').setValue(tampung_nilai);
            }
        }
    }
    
    if (Ext.getCmp('txtJumlah2EditData_viKasirAMB').getValue()==='0' || Ext.getCmp('txtJumlah2EditData_viKasirAMB').getValue()===0 ){
        Ext.getCmp('btnSimpanKasirAMB').disable();
        Ext.getCmp('btnTransferKasirAMB').disable();
    }
    else
    {
        //  Ext.getCmp('btnSimpanKasirAMB').enable();
        // Ext.getCmp('btnTransferKasirAMB').enable(); 
    }
    bayar = Ext.get('txtJumlah2EditData_viKasirAMB').getValue();
    return bayar;
};


    
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } /*else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } */else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
	console.log(grid);
    if (tampungtypedata == 3)
    {
        //grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);
        grid.columns[4].setVisible(false);

    } /*else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    }*/ else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;
function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:{
                                    'select': function (a, b, c){
                                        tapungkd_pay = b.data.KD_PAY;
                                        console.log(tapungkd_pay)
                                    },
                                  }
                    }
            );

    return cboPembayaran;
}
;
function getItemPanelmedreckasirRAD(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelInputKasirAMB(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 120,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiAMBKasir(lebar),
                                            getItemPanelmedreckasirRAD(lebar), getItemPanelUnitKasirAMB(lebar),
                                            getItemPanelTanggalBayarKasirRAD(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;

function getItemPanelTanggalBayarKasirRAD(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                        {
                            columnWidth: .40,
                            layout: 'form',
                            border: false,
                            labelWidth: 100,
                            items:
                            [
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'Tanggal Bayar',
                                            id: 'dtpTanggalBayarDetransaksiRAD',
                                            name: 'dtpTanggalBayarDetransaksiRAD',
                                            format: 'd/M/Y',
                                            //readOnly: true,
                                            value: now,
                                            anchor: '100%',
                                            enableKeyEvents:true,
                                            listeners:
                                                    {
                                                        
                                                    },
                                        }
                            ]
                        }
                            
                        ]
            }
    return items;
}

function getItemPanelUnitKasirAMB(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function RefreshDataKasirRADKasirDetail(no_transaksi,kd_kasir){
   // console.log(no_transaksi);
    var strKriteriaKasirAMB = '';
    var tmp_no_transaksi=Ext.getCmp('txtNoTransaksiKasirAMBKasir').getValue();
    var tmp_jenis_pay=Ext.getCmp('cboJenisByr').getValue();
    var tmp_kd_kasir=34;
    console.log(tmp_jenis_pay); 
//console.log(kd_kasir);
    if (kd_kasir !== undefined) {
        strKriteriaKasirAMB = "no_transaksi = '" + tmp_no_transaksi + "' and kd_kasir =  '" + tmp_kd_kasir + "' ";
    }else{
        strKriteriaKasirAMB = "no_transaksi = '" + tmp_no_transaksi + "' and kd_kasir =  '" + tmp_kd_kasir + "' ";
    }
// faisal
    //dsTRDetailKasirAMBKasirList.load
        Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getGridPembayaran",
            params: {params:strKriteriaKasirAMB,params1:tmp_jenis_pay},
            failure: function(o)
            {
                ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                    
                    var recs=[],
                        recType=dsTRDetailKasirAMBKasirList.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        recs.push(new recType(cst.listData[i]));
                    }
                    dsTRDetailKasirAMBKasirList.removeAll();
                    dsTRDetailKasirAMBKasirList.add(recs);
                   // GridDataView_viSetupHasilTest.getView().refresh();
                }
                else 
                {
                    ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
                }
            }
        }
        
    )


    return dsTRDetailKasirAMBKasirList;
}
;
function GetDTLTRKasirAMBGrid()
{
    var fldDetailAmb = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];

    dsTRDetailKasirAMBKasirList = new WebApp.DataStore({fields: fldDetailAmb})

    gridDTLTRKasirRAD = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Bayar',
                        stripeRows: true,
                        id: 'gridDTLTRKasirRAD',
                        store: dsTRDetailKasirAMBKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRawatJalanColumModelAMB(),
                        sm: new Ext.grid.CellSelectionModel({
                            singleSelect: true,
                            listeners: {
                                cellselect: function (sm, row, rec) {   
                                    indexRow = row;
                                    // console.log(row);
                                }
                            }
                        }),
                    }
            );

    return gridDTLTRKasirRAD;
}
;

function TRKasirRawatJalanColumModelAMB()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirAMB',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirAMB',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 140

                        },
                        {
                            id: 'colURUTKasirAMB',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirAMB',
                            header: 'Qty',
                            width: 30,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirAMB',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirAMB',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangAMB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukAMB();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirAMB',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiAMB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                // console.log(v);
                                // console.log(params);
                                 // tampung_tunai_awal= dsTRDetailKasirAMBKasirList.getRange()[indexRow].modified.BAYARTR;
                                // console.log(tampung_tunai_awal);
                                //console.log(dsTRDetailKasirAMBKasirList.getRange()[indexRow]);
                                // Ext.getCmp('fieldcolTunaiAMB').setValue('12304556');
                                // getTotalDetailProdukAMB();

                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirAMB',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountAMB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;

function getParamDetailTransaksiKasirAMB(){
    var tampungshiftsekarang;
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getCurrentShift",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
            console.log(tampungshiftsekarang);
        }
    });

    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: kd_unit_bayar,
                kd_pasien: Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirAMBKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: 1,/*tampungshiftsekarang,*/
                kdKasir: '34',
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProdukAMB(),
                List: getArrDetailTrKasirAMB(),
                JmlField: mRecordKasirRAD.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksiAMB(),
                Hapus: 1,
                Ubah: 0,
                TglBayar : Ext.get('dtpTanggalBayarDetransaksiRAD').dom.value,
            };
    return params
};

function GetListCountDetailTransaksiAMB()
{
    
    var x=0;
    for(var i = 0 ; i < dsTRDetailKasirAMBKasirList.getCount();i++)
    {
        if (dsTRDetailKasirAMBKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirAMBKasirList.data.items[i].data.DESKRIPSI  != '')
        {
            x += 1;
        };
    }
    return x;
    
};

function getArrDetailBayarTrKasirAMB(){
    var x = '';
    console.log( dsTRDetailHistoryList_amb.getCount());
    for (var i = 0; i < dsTRDetailHistoryList_amb.getCount(); i++)
    {
        if (dsTRDetailHistoryList_amb.data.items[i].data.KD_PRODUK != '' && dsTRDetailHistoryList_amb.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailHistoryList_amb.data.items[i].data.NO_TRANSAKSI
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.BAYAR
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.DESKRIPSI
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.KD_USER
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.SHIFT
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.TGL_BAYAR
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.URUT
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.USERNAME
            y += z + dsTRDetailHistoryList_amb.data.items[i].data.BAYAR



            if (i === (dsTRDetailHistoryList_amb.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }
console.log(x);
    return x;
};

function getArrDetailTrKasirAMB(){
    var x = '';
    console.log( dsTRDetailKasirAMBKasirList.getCount());
    for (var i = 0; i < dsTRDetailKasirAMBKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirAMBKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirAMBKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirAMBKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirAMBKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirAMBKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }
console.log(x);
    return x;
};

function ValidasiEntryCMKasirAMB(modul, mBolHapus){
    var x = 1;
    if ((Ext.get('txtNoTransaksiKasirAMBKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirAMBKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')){
        if (Ext.get('txtNoTransaksiKasirAMBKasir').getValue() == '' && mBolHapus === true){
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'){
            ShowPesanWarningKasirAmb(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == ''){
            ShowPesanWarningKasirAmb(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == ''){
            ShowPesanWarningKasirAmb('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == ''){
            ShowPesanWarningKasirAmb(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirAMBKasirList.getCount() === 0){
            ShowPesanWarningKasirAmb('Data dalam tabel kosong', modul);
            x = 0;
        }else if(modul==-'0'){
            x=0;
        }
        ;
    }
    ;
    return x;
}
;
function Datasave_KasirAMBKasir(mBol)
{
    if (ValidasiEntryCMKasirAMB(nmHeaderSimpanData, false) == 1)
    {
        if (tglTransaksiAwalPembandingTransferRAD > Ext.getCmp('dtpTanggalBayarDetransaksiRAD').getValue().format('Y-m-d')+" 00:00:00"){
            ShowPesanWarningKasirAmb('Tanggal bayar Tidak boleh kurang dari tanggal transaksi pasien', 'Gagal');
            Ext.getCmp('btnSimpanKasirAMB').enable();
            Ext.getCmp('btnTransferKasirAMB').enable();
        }else{
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/savepembayaran",
                            params: getParamDetailTransaksiKasirAMB(),
                            failure: function (o)
                            {
                                ShowPesanWarningKasirAmb('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                            },
                            success: function (o)
                            {
                                 RefreshDatahistoribayar('0');
                                RefreshDataKasirAmbDetail('0');
                                nilai=0;
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true){
                                    var sts_lunas=cst.lunas;
                                    if(sts_lunas==='t'){
                                        Ext.getCmp('btnSimpanKasirAMB').disable();
                                        Ext.getCmp('btnTransferKasirAMB').disable();
                                        Ext.getCmp('btnPembayaranKasirAmb').disable();
                                        Ext.getCmp('btnTutupTransaksiKasirAmb').disable(); 
                                    }else{
                                        Ext.getCmp('btnSimpanKasirAMB').enable();
                                        Ext.getCmp('btnTransferKasirAMB').enable();
                                         Ext.getCmp('btnPembayaranKasirAmb').disable();
                                        Ext.getCmp('btnTutupTransaksiKasirAmb').enable(); 
                                    }
                                    ShowPesanInfoKasirAmb(nmPesanSimpanSukses, nmHeaderSimpanData);
                                    
                                    Ext.getCmp('btnPrint_viDaftar').enable();
                                    RefreshDatahistoribayar(Ext.get('txtNoTransaksiKasirAMBKasir').dom.value);
                                    RefreshDataKasirRADKasirDetail(Ext.get('txtNoTransaksiKasirAMBKasir').dom.value,kd_kasir_amb);
                                    
                                    ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiKasirAMBKasir').dom.value,kd_kasir_amb);
                                    //RefreshDataKasirRADKasir();
                                   /*  if (mBol === false)
                                    {
                                        ViewGridBawahLookupKasirAmb();
                                    }
                                    ; */
                                } else
                                {
                                    ShowPesanWarningKasirAmb('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )
        }

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}

function TransferLookUp_rad(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Rad = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Rad',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_rad(lebar),
                        listeners:
                                {
                                     activate: function()
                                    {
                                        Ext.getCmp('cboalasan_transfer').setValue('Pembayaran Disatukan');                                        
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Rad.show();
    //  Transferbaru();

};
function getParamTransferRwi_dari_rad()
{

/*
       KDkasirIGD:'01',
        TrKodeTranskasi: notransaksi,
        KdUnit: kodeunit,
        Kdpay: kdpaytransfer,
        Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
        Tglasal:  ShowDate(tgltrans),
        Shift: tampungshiftsekarang,
        TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
        KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
        TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
        KDunittujuan : Trkdunit2,
        KDalasan :Ext.get(cboalasan_transfer).dom.value,
        KasirRWI:'05',
        Kdcustomer:kdcustomeraa,
        kodeunitkamar:tmp_kodeunitkamar,
        kdspesial:tmp_kdspesial,
        nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: vkode_customerRAD,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
                Tglasal: ShowDate(TglTransaksi),
                Shift: 1,/* tampungshiftsekarang,*/
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiRAD).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRAD).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transfer).dom.value,
                KasirRWI: kdkasirasal_rad,
                kodeunitkamar:tmp_kodeunitkamar_RAD,
                kdspesial:tmp_kdspesial_RAD,
                nokamar:tmp_nokamar_RAD,
                TglTransfer:Ext.get(dtpTanggaltransferRAD).dom.value


            };
    return params
}
;
function TransferData_amb(mBol)
{
    if (tglTransaksiAwalPembandingTransferRAD > Ext.getCmp('dtpTanggaltransferRAD').getValue().format('Y-m-d')+" 00:00:00"){
        ShowPesanWarningKasirAmb('Tanggal transfer Tidak boleh kurang dari tanggal transaksi pasien', 'Gagal');
    }else{
    loadMask.show();
    var UrlTransfer="";
    /* if (radiovalues === 2 || radiovalues ==='2'){
        UrlTransfer="index.php/main/functionRAD/saveTransferRWI";
    }else{ */
        UrlTransfer="index.php/ambulance/functionTrkasirAmbulance/saveTransfer";
    //}
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + UrlTransfer,
                        params: getParamTransferRwi_dari_rad(),
                        failure: function (o)
                        {

                            ShowPesanWarningKasirAmb('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            // ViewGridBawahLookupKasirAmb();
                        },
                        success: function (o)
                        {   
                        
                            loadMask.hide();
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {   
                                //TransferData_amb_SQL();
                                Ext.getCmp('btnSimpanKasirAMB').disable();
                                Ext.getCmp('btnTransferKasirAMB').disable();
                                ShowPesanInfoKasirAmb('Transfer Berhasil', 'transfer ');
                                Ext.getCmp('btnPrint_viDaftar').enable();
                                RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiAmbulance').getValue());
                                FormLookUpsdetailTRTransfer_Rad.close();
                                
                            } else
                            {
                                ShowPesanWarningKasirAmb('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )
    }

}
;

function getItemPanelButtonTransfer_rad(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_rad',
                                                handler: function ()
                                                {
                                                
                                                    TransferData_amb(false); //SIMPAN TRANSFER AMBULANCE
                                                     Ext.getCmp('btnSimpanKasirAMB').disable();
                                                     Ext.getCmp('btnTransferKasirAMB').disable();
                                                    
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_rad',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Rad.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getFormEntryTRTransfer_rad(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_RAD(lebar), getItemPanelButtonTransfer_rad(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        ViewGridBawahLookupKasirAmb();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function getTransfertujuan_rad(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiRAD',
                                id: 'txtTranfernoTransaksiRAD',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanRAD',
                                name: 'dtpTanggaltransaksiRujuanRAD',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecRAD',
                                id: 'txtTranfernomedrecRAD',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienRAD',
                                id: 'txtTranfernamapasienRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitRAD',
                                id: 'txtTranferunitRAD',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_RAD',
                                        id: 'txtTranferkelaskamar_RAD',
                                        readOnly : true,
                                        labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal transfer ',
                                id: 'dtpTanggaltransferRAD',
                                name: 'dtpTanggaltransferRAD',
                                format: 'd/M/Y',
                                //readOnly: true,
                                value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiTransferRAD(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasal',
                                                id: 'txtjumlahbiayasal',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaid',
                                                id: 'txtpaid',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranfer',
                                                id: 'txtjumlahtranfer',
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihan',
                                                id: 'txtsaldotagihan',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function mComboalasan_transfer()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({fields: Field});
    dsalasan_transfer.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transfer = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transfer',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transfer,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transfer;
};

function getItemPanelInputTransfer_RAD(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_rad(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransferRAD(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transfer()

                                        ]
                            },
                        ]
            };
    return items;
}
;
function showhide_unit(kode)
{
    if(kode==='05')
    {
    Ext.getCmp('txtTranferunitRAD').hide();
    Ext.getCmp('txtTranferkelaskamar_RAD').show();

    }else{
    Ext.getCmp('txtTranferunitRAD').show();
    Ext.getCmp('txtTranferkelaskamar_RAD').hide();
    }
}
function getFormEntryTRKasirAMB(lebar){
    var pnlTRKasirAMB = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirAMB',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKasirAMB(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotal = new Ext.Panel
            (
                    {
                        id: 'paneltotal',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirAMB',
                                                        name: 'txtJumlah2EditData_viKasirAMB',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirRAD',
                                                        name: 'txtJumlahEditData_viKasirRAD',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirRAD = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirRAD',
                        region: 'center',
                        activeTab: 0,
                        height: 350,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirAMBGrid(), paneltotal],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirAMB',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirAMBKasir(false);
                                           // FormDepan2KasirRAD.close();
                                            Ext.getCmp('btnSimpanKasirAMB').disable();
                                            Ext.getCmp('btnTransferKasirAMB').disable();
                                            //Ext.getCmp('btnHpsBrsKasirAMB').disable();
                                        }
                                    },
                                    
                                    {
                                        id: 'btnTransferKasirAMB',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
                                            //Disini
                                            TransferLookUp_rad();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal_rad,
                                                    notransaksi: Ext.getCmp('txtNoTransaksiKasirAMBKasir').getValue(),
                                                    kdkasir: kdkasirasal_rad
                                                },
                                                success: function (o)
                                                {   
                                                showhide_unit(kdkasirasal_rad);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningKasirAmb('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                         console.log(tmp);
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningKasirAmb('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Rad.close();
                                                        } else {
                                                            console.log(tmp[12]);
                                                            Ext.get(txtTranfernoTransaksiRAD).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanRAD).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecRAD).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienRAD).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitRAD).dom.value = tmp[9];
                                                            Trkdunit2 = tmp[6];
                                                            Ext.get(txtTranferkelaskamar_RAD).dom.value=tmp[9];
                                                            Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
                                                            Ext.get(txtjumlahtranfer).dom.value=tmp[12];
                                                            
                                                            tmp_kodeunitkamar_RAD=tmp[8];
                                                            tmp_kdspesial_RAD=tmp[10];
                                                            tmp_nokamar_RAD=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
                                                                    kd_kasir:kdkasir
                                                                },
                                                                success: function (o)
                                                                {
                                                                    console.log(o);
                                                                    Ext.get(txtjumlahbiayasal).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranfer).dom.value = formatCurrency(o.responseText);
                                                                    /* Ext.get(txtjumlahbiayasal).dom.value=tmp[12];
                                                                    Ext.get(txtjumlahtranfer).dom.value=tmp[12]; */
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        //disabled: true,
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnprintbillAmbAmb',
                                                            handler: function ()
                                                            {
                                                                Ext.Msg.confirm('Warning', 'Apakah anda ingin mencetak billing ?', function(button){
                                                                if (button == 'yes'){
                                                                    printbillAmbAmb();
                                                                }
                                                                })
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadRad',
                                                            handler: function ()
                                                            {
                                                                Ext.Msg.confirm('Warning', 'Apakah anda ingin mencetak kwitansi ?', function(button){
                                                                if (button == 'yes'){
                                                                    printkwitansiRadRad();
                                                                }
                                                                })
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirAMB2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirAMB2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirRAD,
                        ]
                    }
            );




    FormDepan2KasirRAD = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirRAD',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirAMB, pnlTRKasirAMB2,
                        ]
                    }
            );

    return FormDepan2KasirRAD
}
function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: Ext.get('txtNoTransaksiAmbulance').dom.value,
                KDkasir: 34
            };
    return params
}
;
function UpdateTutuptransaksi(mBol){
    Ext.Ajax.request({
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o){
                            ShowPesanInfoKasirAmb('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            validasiJenisTr();
                        },
                        success: function (o){
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                ShowPesanInfoKasirAmb('Status Transaksi Berhasil Di update', nmHeaderSimpanData);
                                 //disini yaa();
                                //RefreshDataKasirRADKasir();
                               // loadKasirAmb(kriteria,tmpunit)
                               validasiJenisTr();
                                if (mBol === false){
                                    validasiJenisTr();
                                };
                                cellSelecteddeskripsi = '';
                            } else{
                                ShowPesanInfoKasirAmb('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelecteddeskripsi = '';
                            }
                            ;
                        }
                    }
)};
function getFormEntryKasirAmbulance(lebar,data) {
    var pnlTRKasirAmb = new Ext.FormPanel({
            id: 'PanelTRKasirAmb',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding: 5px 5px 5px 5px',
            height:210,
            width: 930,
            border: true,
            items: [getItemPanelKasirAmbulance(lebar)],
			listeners:
            {
                'close':function(){
					console.log('close');
                     Ext.getDom('rb_pilihan1').checked = true;
                }
            },
            tbar:
            [
                {
                    text: 'Simpan',
                    id: 'btnSaveTransaksiKasirAmb',
                    tooltip: nmEditData,
                    iconCls: 'save',
                    handler: function (){
                                Datasave_TrKasirAmbulance();
                    }//, disabled: true
                },
                {
                        text: 'Simpan',
                        id: 'btnSimpanRAD',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
                        {
                            loadMask.show();
                            Datasave_TrKasirAmbulance(false);  
                            loadMask.hide();
                            // console.log(dsTRDetailKasirAmbList);                                  

                        }
                },  
                
                '-',
                {
                    id: 'btnPembayaranKasirAmb',
                    text: 'Pembayaran',
                    tooltip: nmEditData,
                    iconCls: 'Edit_Tr',
                    handler: function (sm, row, rec)
                    {
                        if (rowSelectedKasirAmb != undefined) {
                            TrKasirLookUpAMB(rowSelectedKasirAmb.data);
                        } else {
                            TrKasirLookUpAMB();
                            //ShowPesanWarningKasirAmb('Pilih data tabel  ', 'Pembayaran');
                        }
                    }//, disabled: true
                }, ' ', ' ', '' + ' ', '' + ' ', '' + ' ', '' + ' ', ' ', '' + '-',
                {
                    text: 'Tutup Transaksi',
                    id: 'btnTutupTransaksiKasirAmb',
                    tooltip: nmHapus,
                    iconCls: 'remove',
                    handler: function ()
                    {
                        if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
                            ShowPesanWarningKasirAmb('Pilih data tabel  ', 'Pembayaran');
                        } else {
                            UpdateTutuptransaksi();
                            Ext.getCmp('btnLookupTRKasirAmbulance').enable();
                            Ext.getCmp('btnSaveTransaksiKasirAmb').enable();
                            Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
                            Ext.getCmp('btnPembayaranKasirAmb').disable();
                        }
                    }//, disabled: true
                },

                 
                    
            ]
});

    /* var paneltotal_history_bayar = new Ext.Panel
            (
                    {
                        id: 'paneltotal_history_bayar',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirAMB_history_bayar',
                                                        name: 'txtJumlah2EditData_viKasirAMB_history_bayar',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirAMB_history_bayar',
                                                        name: 'txtJumlahEditData_viKasirAMB_history_bayar',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )*/
 var x;
 var GDtabDetailAmb = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailAmb',
        region: 'center',
        activeTab: 0,
        height:250,
        width:910,
        //anchor: '100%',
        border:true,
        plain: true,
        defaults:
        {
            autoScroll: true
        },
        items: [GridDetailItemPemeriksaan(),panel_total_history_bayar()],

            listeners:
            {   
                'tabchange' : function (panel, tab) {
                    if (x ==1)
                    {
                        Ext.getCmp('btnLookupTRKasirAmbulance').hide()
                        Ext.getCmp('btnSimpanRAD').hide()
                        Ext.getCmp('btnHpsBrsItemRad').hide()
                        x=2;
                        return x;
                    }else 
                    {   
                        Ext.getCmp('btnLookupTRKasirAmbulance').show()
                        Ext.getCmp('btnSimpanRAD').hide()
                        Ext.getCmp('btnHpsBrsItemRad').show()
                        x=1;    
                        return x;
                    }

                }
            }
        }
        
    );
    
   
   
   var pnlTRKasirAmb2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirAmb2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:490,
            //anchor: '100%',
            width: lebar,
            border: false,
            items: [    GDtabDetailAmb
            
            ]
        }
    );

    
   
   
    var FormDepanTrKasirAmbulance = new Ext.Panel
    (
        {
            id: 'FormDepanTrKasirAmbulance',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: 
            [
                pnlTRKasirAmb,pnlTRKasirAmb2  
            ]

        }
    );
    return FormDepanTrKasirAmbulance
};

function DataDeleteKasirAmbDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirAmbDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirAmb(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirAmbList.removeAt(CurrentKasirAmb.row);
                    cellSelecteddeskripsi=undefined;
                    ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
                    AddNewKasirAmb = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirAmb(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasirAmb(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirAmbDetail()
{
    var params =
    {//^^^
        Table: 'ViewDetailTransaksiKasirAmb',
        TrKodeTranskasi: CurrentKasirAmb.data.data.NO_TRANSAKSI,
        TrTglTransaksi:  CurrentKasirAmb.data.data.TGL_TRANSAKSI,
        TrKdPasien :     CurrentKasirAmb.data.data.KD_PASIEN,
        TrKdNamaPasien : Ext.get('txtNamaPasienL').getValue(),
        //TrAlamatPasien : Ext.get('txtAlamatL').getValue(),    
        TrKdUnit :       Ext.get('txtKdUnitRad').getValue(),
        TrNamaUnit :     Ext.get('txtNamaUnit').getValue(),
        Uraian :         CurrentKasirAmb.data.data.DESKRIPSI2,
        AlasanHapus :    variablehistori,
        TrHarga :        CurrentKasirAmb.data.data.HARGA,
        
        TrKdProduk :     CurrentKasirAmb.data.data.KD_PRODUK,
        RowReq: CurrentKasirAmb.data.data.URUT,
        Hapus:2
    };
    
    return params
};

function getParamDataupdateKasirAmbDetail()
{
    var params =
    {
        Table: 'ViewDetailTransaksiKasirAmb',
        TrKodeTranskasi: CurrentKasirAmb.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasirAmb.data.data.URUT,

        Qty: CurrentKasirAmb.data.data.QTY,
        Ubah:1
    };
    
    return params
};

function GridDetailItemPemeriksaan() 
{
    var fldDetailAmb = ['cito','kd_produk','deskripsi','deskripsi2','kd_unit','kd_tarif','tarif','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','kp_produk','nama_supir','mobil'];
    
    dsTRDetailKasirAmbList = new WebApp.DataStore({ fields: fldDetailAmb })
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Tindakan',
            id: 'gridDTItemTest',
            stripeRows: true,
            store: dsTRDetailKasirAmbList,
            border: false,
            columnLines: true,
            frame: false,
            width:80,
            height:100,
            //anchor: '100%',
            autoScroll:true,
            viewConfig: {forceFit: true},
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailKasirAmbList.getAt(row);
                            CurrentKasirAmb.row = row;
                            CurrentKasirAmb.data = cellSelecteddeskripsi;
                            // console.log(CurrentKasirAmb.row);
                            // console.log(CurrentKasirAmb.data);
                            // console.log(dsTRDetailKasirAmbList.data.items[0].data);
                        }
                    }
                }
            ),
            
            cm: TRKasirAmbColumModel(),
            tbar:
        [
           /* {
                text: 'Tambah Baris',
                id: 'btnTambahBarisKasirAmb',
                tooltip: nmLookup,
                iconCls: 'Edit_Tr',
                handler: function()
                {
                    TambahBarisRAD()
                    
                }
            },*/
            {
                text: 'Tambah Item Ambulance',
                id: 'btnLookupTRKasirAmbulance',
                tooltip: nmLookup,
                iconCls: 'find',
                handler: function()
                {
                    getTindakanAmbulance();
                    setLookUp_getTindakanTrKasirAmbulance();
                    
                }
            },
            {
                    id:'btnHpsBrsItemRad',
                    text: 'Hapus Baris',
                    tooltip: 'Hapus Baris',
                    disabled:false,
                    iconCls: 'RemoveRow',
                    handler: function()
                    {
                        if (dsTRDetailKasirAmbList.getCount() > 0 ) {
                            if (cellSelecteddeskripsi != undefined)
                            {
                                // console.log(cellSelecteddeskripsi.data);
                                Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
                                    if (button == 'yes'){
                                        if(CurrentKasirAmb != undefined) {
                                            var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                            console.log(line);
                                            Ext.Ajax.request
                                             (
                                                {
                                                    url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/deletedetailambulance",
                                                    params: getParamHapusDetailTransaksiAMB(),
                                                    failure: function(o)
                                                    {
                                                        ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                                                    },  
                                                    success: function(o) 
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true && cst.tmp === 'ada') 
                                                        {
                                                            ShowPesanInfoKasirAmb('Data Berhasil Di hapus', 'Sukses');
                                                            dsTRDetailKasirAmbList.removeAt(line);
                                                            gridDTItemTest.getView().refresh();
                                                            //Delete_Detail_Rad_SQL();
                                                        }else if (cst.success === true && cst.tmp === 'kosong') {
                                                            dsTRDetailKasirAmbList.removeAt(line);
                                                        }
                                                        else 
                                                        {
                                                                ShowPesanWarningKasirAmb('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                                                        };
                                                    }
                                                }
                                            )
                                        }
                                    }
                                })
                            } else {
                                ShowPesanWarningKasirAmb('Pilih record ','Hapus data');
                            }
                        }
                    }
            },
           
                    
        ],
            
        }
        
        
    );
    
    

    return gridDTItemTest;
};

function TRKasirAmbColumModel()
{
    var kd_cus_gettarif=vkode_customerRAD;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    // data gridnya
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
            {   id:'uraian_tindakan_ambulance',
                dataIndex: 'deskripsi',
                header: 'Uraian',
                sortable: true,
                menuDisabled:true,
                width: 120,
               
            },

          /*  {   
                id:'tarif',
                header: 'Tarif',
                width:50,
                align: 'right',
                menuDisabled:false,
                dataIndex: 'tarif',
                renderer: function(value, cell) {
				var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
				return str;
				}
            },*/
            { 
				id: 'tarif',
				header: 'Tarif',
				dataIndex: 'tarif',
				width: 80,
				renderer: function(value){
				var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
				return str;
				}
			},


            {   
                id:'kd_produk',
                header: 'kd_produk',
                width:30,
                align: 'right',
                menuDisabled:true,
                hidden:true,
                dataIndex: 'kd_produk',
            },  

            {   
                id:'kd_tarif',
                header: 'kd_tarif',
                width:30,
                align: 'right',
                menuDisabled:true,
                hidden:true,
                dataIndex: 'kd_tarif',
            },  

           

            {   
                id:'tgl_berlaku',
                header: 'tgl_berlaku',
                width:30,
                align: 'right',
                menuDisabled:true,
                hidden:true,
                dataIndex: 'tgl_berlaku',
            },


              

             {
                header: 'Ambulance',
                dataIndex: 'mobil',
                width:70,
                menuDisabled:true,
                hidden:false,
                editor      : new Ext.form.ComboBox({
                    id: 'cboMobilAmbulance',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
                    emptyText:'',
                 //   fieldLabel: 'Spesialisasi IGD',
                  //  align: 'Right',
                    store: DataMobilAMBDS,
                    valueField: 'mobil',
                    displayField: 'mobil',
                    tabIndex:10,
                    anchor: '99%',
                    listeners:
                    {
                        'select': function(a, b, c){
                                  tmp_kd_mobil =  b.data.kd_mobil
                                  },                                      
                    }
                }),
            },
            
           
            {
                header: 'Supir',
                width:70,
                menuDisabled:false,
                dataIndex: 'nama_supir',
                hidden: false,
                editor      : new Ext.form.ComboBox({
                    id: 'cboSupirAmbulance',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
                    emptyText:'',
                 //   fieldLabel: 'Spesialisasi IGD',
                  //  align: 'Right',
                    store: DataSupirAMBDS,
                    valueField: 'nama_supir',
                    displayField: 'nama_supir',
                    tabIndex:10,
                    anchor: '99%',
                    listeners:
                    {
                        'select': function(a, b, c){
                                  tmp_kd_supir =  b.data.kd_supir
                                  },                                        
                    }
                    //console.log(DataSupirAMBDS);
                }),


            },
            {
                header: 'dokter',
                width:50,
                menuDisabled:false,
                dataIndex: 'DR',
                hidden: true,

            }, 

        ]
    )
};

/* function TRKasirAmbColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
                menuDisabled:true,
                hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }
            ,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
               menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
                renderer: function(v, params, record) 
                            {
                            return formatCurrency(record.data.HARGA);
                            
                            }   
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                            { 
                                'specialkey' : function()
                                {
                                    
                                            Dataupdate_KasirAmb(false);

                                }
                            }
                    }
                ),
                
                
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
                hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
                
            }

        ]
    )
}; */


function RecordBaruRAD()
{
    var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
    var p = new mRecordRad
    (
        {
            'cito':'Tidak',
            'DESKRIPSI2':'',
            'KD_PRODUK':'',
            'DESKRIPSI':'', 
            'KD_TARIF':'', 
            'HARGA':'',
            'QTY':'',
            'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
            'DESC_REQ':'',
            'KD_TARIF':'',
            'URUT':''
        }
    );
    
    return p;
};
var kd_kasir_amb;
//----------MEMASUKAN DATA YG DIPILIH DARI DATA GRID KEDALAM LOOKUP EDIT DATA PASIEN
function TRKasirAmbInit(rowdata){
    console.log(rowdata);
    var urutmasuk;
    // console.log(rowdata);
    if (rowdata.URUT != '') {
        urutmasuk = rowdata.URUT;
    }else{
        urutmasuk = rowdata.URUT_MASUK
    }
    tmpurut = urutmasuk;
    AddNewKasirAmb = false;
    TmpNotransaksi=rowdata.NO_TRANSAKSI;
    KdKasirAsal=rowdata.KD_KASIR;
    TglTransaksi=rowdata.TGL_TRANSAKSI;
    tglTransaksiAwalPembandingTransferRAD=rowdata.TGL_TRANSAKSI;
    Kd_Spesial=rowdata.KD_SPESIAL;
    No_Kamar=rowdata.KAMAR;
    kodeunit=rowdata.KD_UNIT;
    kodeunittransfer=rowdata.KD_UNIT;
    if (combovalues==='Transaksi Lama' ){ 
        Ext.get('txtNoTransaksiAmbulance').dom.value = rowdata.NO_TRANSAKSI;
        Ext.get('txtnoregamb').dom.value = rowdata.NO_REGISTER;
          Ext.getCmp('dtpKunjunganL').setReadOnly(true);
        ViewGridBawahLookupKasirAmb(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
        RefreshDatahistoribayar(rowdata.NO_TRANSAKSI);
        kodeUnitRad=rowdata.KD_UNIT_ASAL;
        if (rowdata.LUNAS==='t' && rowdata.CO_STATUS==='t' ){
            console.log('a1');
            Ext.getCmp('btnPembayaranKasirAmb').disable();
            Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
            Ext.getCmp('btnSaveTransaksiKasirAmb').disable();
        }
        else if(rowdata.LUNAS==='t' && rowdata.CO_STATUS==='f' ){
              console.log('a2');
        	Ext.getCmp('btnPembayaranKasirAmb').disable();
            Ext.getCmp('btnTutupTransaksiKasirAmb').enable();
             Ext.getCmp('btnSaveTransaksiKasirAmb').enable();
        }
        else if (rowdata.LUNAS==='f' && rowdata.CO_STATUS==='f'){
              console.log('a3');
            Ext.getCmp('btnPembayaranKasirAmb').enable();
            Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
        }else{
              console.log('a4');
            Ext.getCmp('btnLookupTRKasirAmbulance').enable();
        }
    }
    else{
      	Ext.getCmp('btnHpsBrsKasirAMB').disable();
        Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
        kodeUnitRad=rowdata.KD_UNIT;
    }
    var modul='';
    if(asal_pasien == 1){
        modul='rwi';
    } else if(asal_pasien == 2){
        modul='rwg';
    } else if(asal_pasien == 3){
        modul='ugd';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/cekPembayaran",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul,
            KdKasir: rowdata.KD_KASIR,
            KdPasien: rowdata.KD_PASIEN
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            if(cst.ListDataObj=!''){
                kodepasien = rowdata.KD_PASIEN;
                namapasien = rowdata.NAMA;
                // console.log(rowdata.KD_UNIT);
                //kodeUnitRad = rowdata.KD_UNIT;
                namaunit = rowdata.nama_unit;
                kodepay = cst.ListDataObj.kd_pay;
                kdkasir= cst.ListDataObj.kd_kasir;
                uraianpay = cst.ListDataObj.cara_bayar;
                unit_asal =cst.ListDataObj.namaunitasal;
                kdcustomeraa = rowdata.kd_customer;
              //  Ext.getCmp('btnPembayaranKasirAmb').disable();
                    //Ext.getCmp('btnSaveTransaksiKasirAmb').disable();
            }
            else{
                 kodepasien = rowdata.KD_PASIEN;
                namapasien = rowdata.NAMA;
                // console.log(rowdata.KD_UNIT);
                //kodeUnitRad = rowdata.KD_UNIT;
                namaunit = rowdata.NAMA_UNIT;
                kodepay = '';
                kdkasir='';
                uraianpay = '';
                kdcustomeraa = rowdata.KD_CUSTOMER;


            }
            
        }

    });
    
    Ext.get('dtpTtlL').dom.value=ShowDate(rowdata.TGL_LAHIR);
    Ext.get('txtNoMedrecL').dom.value= rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienL').dom.value = rowdata.NAMA;
    Ext.get('txtAlamatL').dom.value = rowdata.ALAMAT;
    
    Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
            
    Ext.get('cboJK').dom.value = rowdata.JENIS_KELAMIN;
    if (rowdata.JENIS_KELAMIN === 't')
    {
        Ext.get('cboJK').dom.value= 'Laki-laki';
    }else
    {
        Ext.get('cboJK').dom.value= 'Perempuan'
    }
    Ext.getCmp('cboJK').disable();
    
    Ext.get('cboGDR').dom.value = rowdata.GOL_DARAH;
    if (rowdata.GOL_DARAH === '0')
    {
        Ext.get('cboGDR').dom.value= '-';
    }else if (rowdata.GOL_DARAH === '1')
    {
        Ext.get('cboGDR').dom.value= 'A+'
    }else if (rowdata.GOL_DARAH === '2')
    {
        Ext.get('cboGDR').dom.value= 'B+'
    }else if (rowdata.GOL_DARAH === '3')
    {
        Ext.get('cboGDR').dom.value= 'AB+'
    }else if (rowdata.GOL_DARAH === '4')
    {
        Ext.get('cboGDR').dom.value= 'O+'
    }else if (rowdata.GOL_DARAH === '5')
    {
        Ext.get('cboGDR').dom.value= 'A-'
    }else if (rowdata.GOL_DARAH === '6')
    {
        Ext.get('cboGDR').dom.value= 'B-'
    }
    Ext.getCmp('cboGDR').disable();
    
    //alert(rowdata.KD_CUSTOMER);
    Ext.get('txtKdCustomerLamaHide').dom.value= rowdata.KD_CUSTOMER;//tampung kd customer untuk transaksi selain kunjungan langsung
    vkode_customerRAD = rowdata.KD_CUSTOMER;
    //Ext.getCmp('cboKelPasienAmbulance').disable();
    
    Ext.get('cboKelPasienAmbulance').dom.value= rowdata.KELPASIEN;
     Ext.get('cboPerusahaanRequestEntryAmb').dom.value= rowdata.CUSTOMER;
    
    
    if(tmppasienbarulama == 'Baru'){
         Ext.getCmp('cboAsuransiAmb').hide();
    //    Ext.getCmp('txtDokterPengirimL').setValue(rowdata.DOKTER);
     //   Ext.getCmp('cboDOKTER_viPenJasRad').setValue('');
       // Ext.getCmp('cboUnitRad_viAmbulance').setValue('');
        Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASLI;
        tmpkd_unit_asal = rowdata.KD_UNIT;
        console.log(tmpkd_unit_asal);
        tmpkddoktertujuan = '';
        Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT;
        kd_kasir_amb = '';
  //      Ext.getCmp('cboUnitRad_viAmbulance').enable();
    //    Ext.getCmp('cboDOKTER_viPenJasRad').enable();

        Ext.getCmp('cboKelPasienAmbulance').enable();
        Ext.get('txtCustomerLamaHide').hide();
	      
         if(rowdata.KELPASIEN == 'Perseorangan'){
            console.log(rowdata.CUSTOMER);
           
            Ext.getCmp('cboPerseoranganAmb').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerseoranganAmb').show();
            Ext.getCmp('cboPerseoranganAmb').enable();

        } else if(rowdata.KELPASIEN == 'Perusahaan'){
            Ext.getCmp('cboPerusahaanRequestEntryAmb').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerusahaanRequestEntryAmb').show();
            Ext.getCmp('cboPerusahaanRequestEntryAmb').enable();
        } else{
            Ext.getCmp('cboAsuransiAmb').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboAsuransiAmb').show();
            Ext.getCmp('cboAsuransiAmb').enable();
        } 
    }else{
      Ext.getCmp('cboPerusahaanRequestEntryAmb').disable();
       console.log(rowdata);
        tmpkd_unit_asal = rowdata.KD_UNIT;
        tmpkddoktertujuan = rowdata.KD_DOKTER;
        Ext.get('txtKdUnitRad').dom.value   = rowdata.KD_UNIT_ASAL;
        Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT_ASLI;
        kd_kasir_amb = rowdata.KD_KASIR;
        Ext.getCmp('txtNamaUnit').disable();
      //  Ext.getCmp('btnPembayaranKasirAmb').disable();
       // Ext.getCmp('btnSaveTransaksiKasirAmb').enable();
       // Ext.getCmp('btnTutupTransaksiKasirAmb').enable();
       // Ext.getCmp('cboDOKTER_viPenJasRad').disable();

        Ext.getCmp('cboKelPasienAmbulance').disable();
        Ext.get('txtCustomerLamaHide').hide();
        if(rowdata.KELPASIEN == 'Perseorangan'){
            Ext.getCmp('cboPerseoranganAmb').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerseoranganAmb').show();
            Ext.getCmp('cboPerseoranganAmb').disable();
        } else if(rowdata.KELPASIEN == 'Perusahaan'){
            Ext.getCmp('cboPerusahaanRequestEntryAmb').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboPerusahaanRequestEntryAmb').show();
            Ext.getCmp('cboPerusahaanRequestEntryAmb').disable();
        } else{
            Ext.getCmp('cboAsuransiAmb').setValue(rowdata.CUSTOMER);
            Ext.getCmp('cboAsuransiAmb').show();
            Ext.getCmp('cboAsuransiAmb').disable();
        }
    }
    // getDokterPengirim(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
    Ext.getCmp('txtnotlprad').setValue(rowdata.HP);
    // Ext.getCmp('txtnoseprad').setValue(rowdata.SJP);
    
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/getcurrentshift",
         params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            tampungshiftsekarang=o.responseText
            
        }
        
    
    });
    
    //---------------------EDIT DEFAULT UNIT RAD 31 01 2017
    Ext.Ajax.request({
        url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getDefaultUnit",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            kd_unit_bayar=cst.kd_unit;
            tmpkd_unit = cst.kd_unit;
            // console.log(rowSelectedKasirAmb)
            var TrPenJasRad_kd_unit = tmpkd_unit_asal;
            if (rowSelectedKasirAmb.data.KD_UNIT_ASAL != undefined) {

                TrPenJasRad_kd_unit = rowSelectedKasirAmb.data.KD_UNIT_ASAL;
            }


            Ext.Ajax.request({
                url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getTarifMir",
                params: {
                    kd_unit:TrPenJasRad_kd_unit,
                    //kd_unit:tmpkd_unit_asal,
                    //kd_unit_asal:tmpkd_unit_asal,
                    kd_customer:vkode_customerRAD,
                    penjas:modul,
                    kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    // console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });
    //,,
    //alert(tampungshiftsekarang);
    setUsia(ShowDate(rowdata.TGL_LAHIR));
    
};


function KasirAmbAddNew() 
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    //alert('hai');
    AddNewKasirAmb = true;
    Ext.get('txtNoTransaksiAmbulance').dom.value = '';
    Ext.get('txtNoMedrecL').dom.value='';
    Ext.get('txtNamaPasienL').dom.value = '';
    Ext.get('txtKdDokter').dom.value   = undefined;
    Ext.get('txtNamaUnit').dom.value   = 'Umum';
//    Ext.getCmp('txtDokterPengirimL').setValue('DOKTER LUAR');
    Ext.get('txtKdUrutMasuk').dom.value = '';
    tglTransaksiAwalPembandingTransferRAD=nowTglTransaksiGrid.format('Y-m-d')+" 00:00:00";
    //Ext.getCmp('btnPembayaranKasirAmb').disable();
    //Ext.getCmp('btnHpsBrsKasirAMB').disable();
    Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
    Ext.getCmp('cboPerseoranganAmb').setValue('UMUM');
    Ext.getCmp('cboPerseoranganAmb').show();
    rowSelectedKasirAmb=undefined;
    dsTRDetailKasirAmbList.removeAll();
    vkode_customerRAD='0000000001';
    databaru = 1;
    Ext.get('txtNamaUnit').readOnly;
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/getDefaultUnit",
        params: {
           text:''
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
//            Ext.getCmp('cboUnitRad_viAmbulance').setValue(cst.kd_unit);
         //   getComboDokterRad();
            //Ext.getCmp('cbounitrads_viKasirAmb').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            tmpkd_unit = cst.kd_unit;
            
            Ext.Ajax.request({
                url: baseURL + "index.php/main/functionRAD/getTarifMir",
                params: {
                    kd_unit:cst.kd_unit,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:vkode_customerRAD,
                     penjas:'langsung',
                     kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    // console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });
    
    
    

};

function RefreshDataKasirAmbDetail(no_transaksi){
    console.log(no_transaksi);
    var strKriteriaRAD='';
    //strKriteriaRAD = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_unit ='34'";
    //strKriteriaRAD = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirAmbList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailRWJGridBawah',
                param: strKriteriaRAD
            }
        }
    );
    return dsTRDetailKasirAmbList;
};

//tampil grid bawah penjas rad
function ViewGridBawahLookupKasirAmb(no_transaksi,kd_kasir){
console.log(no_transaksi);
console.log(kd_kasir);   
    var strKriteriaRAD='';
    if (kd_kasir !== undefined) {
        strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }


    
    // strKriteriaRAD = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
 
   
    dsTRDetailKasirAmbList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailGridBawahTrKasirAmb',
                param: strKriteriaRAD
            }
        }
    );
    return dsTRDetailKasirAmbList;
};

//PARAMETER UNTUK SAVING
//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiRAD() 
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    /* if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
        KdCust=Ext.getCmp('cboPerseoranganRad').getValue();
    }else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
        KdCust=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
    }else {
        KdCust=Ext.getCmp('cboAsuransiRad').getValue();
    } */
    
    if(Ext.getCmp('txtNoMedrecL').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    
    var params =
    {
        //Table:'ViewDetailTransaksiPenJasRad',
        
        KdTransaksi: Ext.get('txtNoTransaksiAmbulance').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecL').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienL').getValue(),
        Ttl:Ext.getCmp('dtpTtlL').getValue(),
        Alamat:Ext.getCmp('txtAlamatL').getValue(),
        JK:Ext.getCmp('cboJK').getValue(),
        GolDarah:Ext.getCmp('cboGDR').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitRad').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
        TglTransaksiAsal:TglTransaksi,
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customerRAD,
        TmpCustoLama:vkode_customerRAD,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRad').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesRad').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPRad').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasRad(),
        JmlField: mRecordRad.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiAMB(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:52,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter    : '',
        Modul:modul,
        KdProduk:'',//sTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.KD_PRODUK
        URUT :tmpurut,
        no_reg:Ext.getCmp('txtnoregamb').getValue()
        
    };
    return params
};


function getParamKonsultasi() 
{

    var params =
    {
        
        Table:'ViewDetailTransaksiKasirAmb', //data access listnya belum dibuat
        
        //TrKodeTranskasi: Ext.get('txtNoTransaksiAmbulance').getValue(),
        KdUnitAsal : Ext.get('txtKdUnitRad').getValue(),
        KdDokterAsal : Ext.get('txtKdDokter').getValue(),
        KdUnit: selectKlinikPoli,
        KdDokter:selectDokter,
        KdPasien:Ext.get('txtNoMedrecL').getValue(),
        TglTransaksi : Ext.get('dtpKunjunganL').dom.value,
        KDCustomer :vkode_customerRAD,
    };
    return params
};

function GetListCountDetailTransaksiAMB(){
    var x=0;
    for(var i = 0 ; i < dsTRDetailKasirAmbList.getCount();i++)
    {
        if (dsTRDetailKasirAmbList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirAmbList.data.items[i].data.DESKRIPSI  != ''){
            x += 1;
        };
    }
    return x;
    
};


function getArrKasirAmb()
{
    var x='';
    var arr=[];
    for(var i = 0 ; i < dsTRDetailKasirAmbList.getCount();i++)
    {
        console.log(dsTRDetailKasirAmbList.data.items[i].data);
        if (dsTRDetailKasirAmbList.data.items[i].data.kd_produk != undefined && 
            dsTRDetailKasirAmbList.data.items[i].data.deskripsi != undefined && tmp_kd_supir!=undefined && tmp_kd_mobil!=undefined){
            var o={};
            var y='';
            var z='@@##$$@@';
            o['URUT']= dsTRDetailKasirAmbList.data.items[i].data.urut;
            o['KD_PRODUK']= dsTRDetailKasirAmbList.data.items[i].data.kd_produk;
            o['DESKRIPSI']= dsTRDetailKasirAmbList.data.items[i].data.deskripsi;
            o['KD_SUPIR']= tmp_kd_supir;
            o['KD_MOBIL']= tmp_kd_mobil;
            o['TGL_BERLAKU']= dsTRDetailKasirAmbList.data.items[i].data.tgl_berlaku;
            o['HARGA']= dsTRDetailKasirAmbList.data.items[i].data.tarif;
            o['KD_TARIF']= dsTRDetailKasirAmbList.data.items[i].data.kd_tarif;
          //  o['cito']= dsTRDetailKasirAmbList.data.items[i].data.cito;
            o['kd_unit']= '801';
            arr.push(o);
        }else{
             ShowPesanWarningKasirAmb('Lengkapi Item Ambulance','Warning');
        }
    }   
    
    return Ext.encode(arr);
};

//panel dalam lookup detail pasien
/*function getItemPanelKasirAmbulance(lebar) 
{
    //pengaturan panel data pasien
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding: 5px 5px 5px 5px',
        border:true,
        height:290,
        items:
        [
                    { //awal panel biodata pasien
                        columnWidth:.99,
                        layout: 'absolute',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        width: 100,
                        height: 290,
                        anchor: '100% 100%',
                        items:
                        [
                            //bagian pinggir kiri                            
                            {
                                x: 10,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Transaksi  '
                            },
                            {
                                x: 100,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x : 110,
                                y : 10,
                                xtype: 'textfield',
                                name: 'txtNoTransaksiAmbulance',
                                id: 'txtNoTransaksiAmbulance',
                                width: 100,
                                emptyText:nmNomorOtomatis,
                                readOnly:true

                            },
                            {
                                x: 10,
                                y: 40,
                                xtype: 'label',
                                text: 'No. Medrec  '
                            },
                            {
                                x: 100,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: 'No. Medrec',
                                name: 'txtNoMedrecL',
                                id: 'txtNoMedrecL',
                                readOnly:true,
                                width: 120,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },

                            {
                                xtype: 'textfield',
                                name: 'txt_kd_customer',
                                id: 'txt_kd_customer',
                                readOnly:true,
                                width: 120,
                                hidden:true,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },

                            {
                                xtype: 'textfield',
                                name: 'txt_kd_unit_asal',
                                id: 'txt_kd_unit_asal',
                                readOnly:true,
                                width: 120,
                                hidden:true,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },

                              {
                                xtype: 'textfield',
                                name: 'txt_tgl_kunj_asal',
                                id: 'txt_tgl_kunj_asal',
                                readOnly:true,
                                width: 120,
                                hidden:true,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },
                            {
                                x: 10,
                                y: 70,
                                xtype: 'label',
                                text: 'Unit Asal  '
                            },
                            {
                                x: 100,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 70,
                                xtype: 'textfield',
                                name: 'txtNamaUnit',
                                id: 'txtNamaUnit',
                                readOnly:true,
                                width: 120
                            },
                             
                            {
                                x: 10,
                                y: 160,
                                xtype: 'label',
                                text: 'Unit Ambulance'
                            },
                            {
                                x: 100,
                                y: 160,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboUnitAmbulance(),
                            {
                                x: 10,
                                y: 140,
                                xtype: 'label',
                                text: 'Cara Penerimaan'
                            },
                            {
                                x: 100,
                                y: 140,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 140,
                                y: 140,
                                xtype: 'radiogroup',
                                id: 'rbrujukan',
                                fieldLabel: 'Asal Pasien ',
                                width:250,
                                items: [
                                            {
                                                boxLabel: 'Datang Sendiri',
                                                name: 'rb_pemerimaan',
                                                id: 'rb_datang_sendiri',
                                                checked: true,
                                                inputValue: '1',
                                                handler: function (field, value) {
                                                    if (value === true)
                                                    {
                                                        Ext.getCmp('cboRujukankAmbulance').disable();
                                                        Ext.getCmp('lblRujukanRS').disable();  
                                                        Ext.getCmp('lblNamaRS').disable(); 
                                                        Ext.getCmp('cboNamaRujukankAmbulance').disable(); 
                                                        Ext.getCmp('lblAlamatRS').disable();   
                                                        Ext.getCmp('txtAlamatRS').disable();
                                                        Ext.getCmp('txtKotaRS').disable();
                                                        Ext.getCmp('lblKotaRS').disable();
                                                       // Ext.getCmp('cboNamaRujukankAmbulance').disable();             
                        
                                                    }
                                                }
                                            },
                                            
                                            {
                                                boxLabel: 'Rujukan',
                                                name: 'rb_pemerimaan',
                                                id: 'rb_rujukan',
                                                inputValue: '2',
                                                handler: function (field, value) {
                                                if (value === true)
                                                {
                                                        Ext.getCmp('cboRujukankAmbulance').enable();
                                                        Ext.getCmp('lblRujukanRS').enable();  
                                                        Ext.getCmp('cboNamaRujukankAmbulance').enable(); 
                                                        Ext.getCmp('lblNamaRS').enable(); 
                                                        Ext.getCmp('lblAlamatRS').enable();   
                                                        Ext.getCmp('txtAlamatRS').enable();
                                                        Ext.getCmp('txtKotaRS').enable();
                                                        Ext.getCmp('lblKotaRS').enable(); 
                                                      //  Ext.getCmp('cboNamaRujukankAmbulance').enable(); 
                                                }
                                             }
                                            },
                                            
                                        ]
                            },

                            {
                                x: 10,
                                y: 170,
                                disabled:true,
                                xtype: 'label',
                                text: 'Dari',
                                name: 'lblRujukanRS',
                                id: 'lblRujukanRS',
                            },

                            {
                                x: 100,
                                y: 170,
                                xtype: 'label',
                                text: ':'
                            },

                           mComboRujukanAmbulance(),


                            {
                                x: 280,
                                y: 170,
                                disabled:true,
                                xtype: 'label',
                                text: 'Nama',
                                name: 'lblNamaRS',
                                id: 'lblNamaRS',
                            },

                            {
                                x: 340,
                                y: 170,
                                xtype: 'label',
                                text: ':'
                            },

                            {
                                x: 370,
                                y: 220,
                                xtype: 'textfield',
                                name: 'txtNamaRS',
                                id: 'txtNamaRS',
                                disabled:true,
                                width: 180
                            },
                            mComboNamaRujukanAmbulance(),   



                            {
                                x: 10,
                                y: 200,
                                disabled:true,
                                xtype: 'label',
                                text: 'Alamat',
                                name: 'lblAlamatRS',
                                id: 'lblAlamatRS',
                            },

                            {
                                x: 100,
                                y: 200,
                                xtype: 'label',
                                text: ':'
                            },

                            {
                                x: 120,
                                y: 200,
                                xtype: 'textfield',
                                name: 'txtAlamatRS',
                                id: 'txtAlamatRS',
                                disabled:true,
                                width: 430
                            },


                             {
                                x: 560,
                                y: 200,
                                disabled:true,
                                xtype: 'label',
                                text: 'Kota',
                                name: 'lblKotaRS',
                                id: 'lblKotaRS',
                            },

                            {
                                x: 620,
                                y: 200,
                                xtype: 'label',
                                text: ':'
                            },

                            {
                                x: 630,
                                y: 200,
                                xtype: 'textfield',
                                name: 'txtKotaRS',
                                id: 'txtKotaRS',
                                disabled:true,
                                width: 120
                            },


                            {
                                x: 10,
                                y: 100,
                                xtype: 'label',
                                text: 'Dokter Ambulance'
                            },
                            {
                                x: 100,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboDOKTER(),
                            {
                                x: 360,
                                y: 130,
                                xtype: 'label',
                                text: 'No. Tlp'
                            },
                            {
                                x: 400,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 410,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtnotlprad',
                                id: 'txtnotlprad',
                                width: 150,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('cboDOKTER_viKasirAmb').focus();
                                        }
                                    }
                                }
                            },
                            //bagian tengah
                            {
                                x: 260,
                                y: 10,
                                xtype: 'label',
                                text: 'Tanggal Kunjung '
                            },
                            {
                                x: 350,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 10,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal hari ini ',
                                id: 'dtpKunjunganL',
                                name: 'dtpKunjunganL',
                                format: 'd/M/Y',
                                //readOnly : true,
                                value: now,
                                width: 110
                            },
                            {
                                x: 480,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Reg : '
                            },
                            {
                                x: 530,
                                y: 10,
                                xtype: 'textfield',
                                id: 'txtnoregamb',
                                name: 'txtnoregamb',
                                readOnly : true,
                                width: 110
                            },
                            {
                                x: 650,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Foto : '
                            },
                            {
                                x: 705,
                                y: 10,
                                xtype: 'textfield',
                                id: 'txtnofotorad',
                                name: 'txtnofotorad',
                                readOnly : true,
                                width: 110
                            },
                            {
                                x: 260,
                                y: 40,
                                xtype: 'label',
                                text: 'Nama Pasien '
                            },
                            {
                                x: 350,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtNamaPasienL',
                                id: 'txtNamaPasienL',
                                width: 200,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('dtpTtlL').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 260,
                                y: 70,
                                xtype: 'label',
                                text: 'Alamat '
                            },
                            {
                                x: 350,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 70,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtAlamatL',
                                id: 'txtAlamatL',
                                width: 395,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            if (combovalues === 'Transaksi Lama')
                                            {
                                                Ext.getCmp('txtnotlprad').focus();
                                            }
                                            else
                                            {
                                                Ext.getCmp('cboJK').focus();
                                            }
                                            
                                        }
                                    }
                                }
                            },
                            {
                                x: 360,
                                y: 100,
                                xtype: 'label',
                                text: 'Jenis Kelamin '
                            },
                            {
                                x: 445,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboJK(),
                            {
                                x: 570,
                                y: 100,
                                xtype: 'label',
                                text: 'Gol. Darah '
                            },
                            {
                                x: 640,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboGDarah(),
                            {
                                x: 570,
                                y: 40,
                                xtype: 'label',
                                text: 'Tanggal lahir '
                            },
                            {
                                x: 640,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 645,
                                y: 40,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTtlL',
                                selectOnFocus: true,
                                forceSelection: true,
                                name: 'dtpTtlL',
                                format: 'd/M/Y',
                                readOnly : true,
                                value: now,
                                width: 110,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            var tmptanggal = Ext.get('dtpTtlL').getValue();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetUmur",
                                                params: {
                                                    TanggalLahir: tmptanggal
                                                },
                                                success: function (o){
                                                    var tmphasil = o.responseText;
                                                    var tmp = tmphasil.split(' ');
                                                    if (tmp.length == 6){
                                                        Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 4){
                                                        if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else{
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 2 ){
                                                        if (tmp[1] == 'year' ){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'years' ){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'mon'  ){
                                                            Ext.getCmp('txtUmurRad').setValue('0');
                                                        }else if (tmp[1] == 'mons'  ){
                                                            Ext.getCmp('txtUmurRad').setValue('0');
                                                        }else{
                                                            Ext.getCmp('txtUmurRad').setValue('0');
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 1){
                                                        Ext.getCmp('txtUmurRad').setValue('0');
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else{
                                                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                    }   
                                                }
                                            });
                                            Ext.getCmp('txtAlamatL').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 765,
                                y: 40,
                                xtype: 'label',
                                text: 'Umur '
                            },
                            {
                                x: 795,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 805,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                name: 'txtUmurRad',
                                id: 'txtUmurRad',
                                width: 30,
                                //anchor: '95%',
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {
                                                    Ext.Ajax.request
                                                    (
                                                            {
                                                                url: baseURL + "index.php/main/functionLAB/cekUsia",
                                                                params: {umur: Ext.get('txtUmurRad').getValue()},
                                                                failure: function (o)
                                                                {
                                                                    ShowPesanErrorKasirAmb('Proses Error ! ', 'Radiologi');
                                                                },
                                                                success: function (o)
                                                                {
                                                                    var cst = Ext.decode(o.responseText);
                                                                    if (cst.success === true)
                                                                    {
                                                                        Ext.getCmp('dtpTtlL').setValue(cst.tahunumur);
                                                                    }
                                                                    else
                                                                    {
                                                                        ShowPesanErrorKasirAmb('Proses Error ! ', 'Radiologi');
                                                                    }
                                                                }
                                                            }
                                                    )
                                                }
                                            }
                                        }
                            },
                            {
                                x: 10,
                                y: 100,
                                xtype: 'label',
                                text: 'Kelompok Pasien '
                            },
                            {
                                x: 100,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mCboKelompokpasien(),
                            mComboPerseorangan(),
                            mComboPerusahaan(),
                            mComboAsuransi(),
                            {
                                x: 410,
                                y: 160,
                                xtype: 'textfield',
                                fieldLabel: 'Nama Peserta',
                                maxLength: 200,
                                name: 'txtNamaPesertaAsuransiAmb',
                                id: 'txtNamaPesertaAsuransiAmb',
                                emptyText:'Nama Peserta Asuransi',
                                width: 150
                            },
                            {
                                x: 250,
                                y: 190,
                                xtype: 'textfield',
                                fieldLabel: 'No. Askes',
                                maxLength: 200,
                                name: 'txtNoAskesAmb',
                                id: 'txtNoAskesAmb',
                                emptyText:'No Askes',
                                width: 150
                            },
                            {
                                x: 410,
                                y: 190,
                                xtype: 'textfield',
                                fieldLabel: 'No. SJP',
                                maxLength: 200,
                                name: 'txtNoSJPAmb',
                                id: 'txtNoSJPAmb',
                                emptyText:'No SJP',
                                width: 150
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtCustomerLamaHide',
                                id: 'txtCustomerLamaHide',
                                readOnly:true,
                                width: 280
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Kode customer  ',
                                name: 'txtKdCustomerLamaHide',
                                id: 'txtKdCustomerLamaHide',
                                readOnly:true,
                                hidden:true,
                                width: 280
                            },
                            
                            
                            
                            //---------------------hidden----------------------------------
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtKdDokter',
                                id: 'txtKdDokter',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Unit  ',
                                name: 'txtKdUnitRad',
                                id: 'txtKdUnitRad',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            {
                                xtype: 'textfield',
                                name: 'txtKdUrutMasuk',
                                id: 'txtKdUrutMasuk',
                                readOnly:true,
                                hidden:true,
                                anchor: '100%'
                            }
                                
                            //-------------------------------------------------------------
                            
                        ]
                    },  //akhir panel biodata pasien
            
        ]
    };
    return items;
};
*/

function getItemPanelKasirAmbulance(lebar) 
{
    //pengaturan panel data pasien
    var items =
    {
        layout: 'fit',
        // anchor: '100%',
        width: 940,
        labelAlign: 'right',
        bodyStyle: 'padding: 5px 5px 5px 5px',
        border:true,
        height:175,//pertama
        items:
        [
                    { //awal panel biodata pasien
                        // columnWidth:.99,
                        layout: 'absolute',
                       // bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        width: 700,
                        height: 50,
                      //  anchor: '100% 100%',
                        items:
                        [
                            //bagian pinggir kiri 
                             {
                                xtype: 'textfield',
                                name: 'txt_kd_unit_asal',
                                id: 'txt_kd_unit_asal',
                                readOnly:true,
                                width: 120,
                                hidden:true,
                                listeners: 
                                { 
                                    
                                }
                            },
                             {
                                xtype: 'textfield',
                                name: 'txt_kd_customer',
                                id: 'txt_kd_customer',
                                readOnly:true,
                                width: 120,
                                hidden:true,
                                listeners: 
                                { 
                                    
                                }
                            },

                             {
                                xtype: 'textfield',
                                name: 'txt_tgl_kunj_asal',
                                id: 'txt_tgl_kunj_asal',
                                readOnly:true,
                                width: 120,
                                hidden:true,
                                listeners: 
                                { 
                                    
                                }
                            },

                                
                            {
                                x: 10,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Transaksi  '
                            },
                            {
                                x: 100,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                             {
                                x : 110,
                                y : 10,
                                xtype: 'textfield',
                                name: 'txtNoTransaksiAmbulance',
                                id: 'txtNoTransaksiAmbulance',
                                width: 100,
                                emptyText:nmNomorOtomatis,
                                readOnly:true

                            },
                            {
                                x: 10,
                                y: 40,
                                xtype: 'label',
                                text: 'No. Medrec  '
                            },
                            {
                                x: 100,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: 'No. Medrec',
                                name: 'txtNoMedrecL',
                                id: 'txtNoMedrecL',
                                readOnly:true,
                                width: 120,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },
                            {
                                x: 10,
                                y: 70,
                                xtype: 'label',
                                text: 'Unit  '
                            },
                            {
                                x: 100,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 70,
                                xtype: 'textfield',
                                name: 'txtNamaUnit',
                                id: 'txtNamaUnit',
                                readOnly:true,
                                width: 120
                            },
                            {
                                x: 10,
                                y: 100,
                                xtype: 'label',
                                text: 'Kelompok Pasien'
                            },
                            {
                                x: 100,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                           /* {
                                x: 110,
                                y: 100,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtDokterPengirimL',
                                id: 'txtDokterPengirimL',
                                width: 200
                            },
                            {
                                x: 10,
                                y: 190,
                                xtype: 'label',
                                text: 'Unit Rad'
                            },
                            {
                                x: 100,
                                y: 190,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboUnitRad(),*/
                            // {
                            //     x: 360,
                            //     y: 190,
                            //     xtype: 'label',
                            //     text: 'No. SEP'
                            // },
                            // {
                            //     x: 400,
                            //     y: 190,
                            //     xtype: 'label',
                            //     text: ':'
                            // },
                            // {
                            //     x: 410,
                            //     y: 185,
                            //     xtype: 'textfield',
                            //     fieldLabel: '',
                            //     readOnly:true,
                            //     name: 'txtnoseprad',
                            //     id: 'txtnoseprad',
                            //     width: 200
                            // },
                            /*{
                                x: 10,
                                y: 130,
                                xtype: 'label',
                                text: 'Dokter Rad'
                            },
                            {
                                x: 100,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboDOKTER(),*/
                            {
                                x: 570,
                                y: 100,
                                xtype: 'label',
                                text: 'No. Tlp'
                            },
                            {
                                x: 630,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 640,
                                y: 100,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtnotlprad',
                                id: 'txtnotlprad',
                                width: 150,
                                enableKeyEvents: true,
                                autoCreate: {tag: 'input', type: 'text', size: '12', autocomplete: 'off', maxlength: '12'},
                            },
                            //bagian tengah
                            {
                                x: 250,
                                y: 10,
                                xtype: 'label',
                                text: 'Tanggal Kunjungan'
                            },
                            {
                                x: 350,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 10,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal hari ini ',
                                id: 'dtpKunjunganL',
                                name: 'dtpKunjunganL',
                                format: 'd/M/Y',
                                //readOnly : true,
                                value: now,
                                width: 110
                            },
                            {
                                x: 480,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Reg : '
                            },
                            {
                                x: 530,
                                y: 10,
                                xtype: 'textfield',
                                id: 'txtnoregamb',
                                name: 'txtnoregamb',
                                readOnly : true,
                                width: 110
                            },
                           /* {
                                x: 650,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Foto : '
                            },
                            {
                                x: 705,
                                y: 10,
                                xtype: 'textfield',
                                id: 'txtnofotorad',
                                name: 'txtnofotorad',
                                readOnly : true,
                                width: 110
                            },*/
                            {
                                x: 250,
                                y: 240,
                                xtype: 'label',
                                text: 'Total Bawah'
                            },
                            {
                                x: 250,
                                y: 40,
                                xtype: 'label',
                                text: 'Nama Pasien '
                            },
                            {
                                x: 350,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtNamaPasienL',
                                id: 'txtNamaPasienL',
                                width: 200,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('dtpTtlL').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 250,
                                y: 70,
                                xtype: 'label',
                                text: 'Alamat '
                            },
                            {
                                x: 350,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 70,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtAlamatL',
                                id: 'txtAlamatL',
                                width: 385,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            if (combovalues === 'Transaksi Lama')
                                            {
                                                Ext.getCmp('txtnotlprad').focus();
                                            }
                                            else
                                            {
                                                Ext.getCmp('cboJK').focus();
                                            }
                                            
                                        }
                                    }
                                }
                            },
                            {
                                x: 360,
                                y: 100,
                                xtype: 'label',
                                text: 'Jenis Kelamin '
                            },
                            {
                                x: 445,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboJK(),
                            {
                                x: 750,
                                y: 70,
                                xtype: 'label',
                                text: 'Gol. Darah '
                            },
                            {
                                x: 820,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboGDarah(),
                            {
                                x: 570,
                                y: 40,
                                xtype: 'label',
                                text: 'Tanggal lahir '
                            },
                            {
                                x: 640,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 645,
                                y: 40,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTtlL',
                                selectOnFocus: true,
                                forceSelection: true,
                                name: 'dtpTtlL',
                                format: 'd/M/Y',
                                readOnly : true,
                                value: now,
                                width: 110,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            var tmptanggal = Ext.get('dtpTtlL').getValue();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetUmur",
                                                params: {
                                                    TanggalLahir: tmptanggal
                                                },
                                                success: function (o){
                                                    var tmphasil = o.responseText;
                                                    var tmp = tmphasil.split(' ');
                                                    if (tmp.length == 6){
                                                        Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 4){
                                                        if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else{
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 2 ){
                                                        if (tmp[1] == 'year' ){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'years' ){
                                                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'mon'  ){
                                                            Ext.getCmp('txtUmurRad').setValue('0');
                                                        }else if (tmp[1] == 'mons'  ){
                                                            Ext.getCmp('txtUmurRad').setValue('0');
                                                        }else{
                                                            Ext.getCmp('txtUmurRad').setValue('0');
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 1){
                                                        Ext.getCmp('txtUmurRad').setValue('0');
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else{
                                                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                    }   
                                                }
                                            });
                                            Ext.getCmp('txtAlamatL').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 765,
                                y: 40,
                                xtype: 'label',
                                text: 'Umur '
                            },
                            {
                                x: 795,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 805,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                name: 'txtUmurRad',
                                id: 'txtUmurRad',
                                width: 30,
                                //anchor: '95%',
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {
                                                    Ext.Ajax.request
                                                    (
                                                            {
                                                                url: baseURL + "index.php/main/functionLAB/cekUsia",
                                                                params: {umur: Ext.get('txtUmurRad').getValue()},
                                                                failure: function (o)
                                                                {
                                                                    ShowPesanErrorPenJasRad('Proses Error ! ', 'Radiologi');
                                                                },
                                                                success: function (o)
                                                                {
                                                                    var cst = Ext.decode(o.responseText);
                                                                    if (cst.success === true)
                                                                    {
                                                                        Ext.getCmp('dtpTtlL').setValue(cst.tahunumur);
                                                                    }
                                                                    else
                                                                    {
                                                                        ShowPesanErrorPenJasRad('Proses Error ! ', 'Radiologi');
                                                                    }
                                                                }
                                                            }
                                                    )
                                                }
                                            }
                                        }
                            },
                          /*  {
                                x: 10,
                                y: 160,
                                xtype: 'label',
                                text: 'Kelompok Pasien '
                            },
                            {
                                x: 100,
                                y: 160,
                                xtype: 'label',
                                text: ':'
                            },*/
                            mCboKelompokpasien(),
                            mComboPerseorangan(),
                            mComboPerusahaan(),
                            mComboAsuransi(),
                            {
                                x: 280,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: 'Nama Peserta',
                                maxLength: 200,
                                name: 'txtNamaPesertaAsuransiAmb',
                                id: 'txtNamaPesertaAsuransiAmb',
                                emptyText:'Nama Peserta Asuransi',
                                width: 150
                            },
                            {
                                x: 450,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: 'No. Askes',
                                maxLength: 200,
                                name: 'txtNoAskesAmb',
                                id: 'txtNoAskesAmb',
                                emptyText:'No Askes',
                                width: 150
                            },
                            {
                                x: 620,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: 'No. SJP',
                                maxLength: 200,
                                name: 'txtNoSJPAmb',
                                id: 'txtNoSJPAmb',
                                emptyText:'No SJP',
                                width: 150
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtCustomerLamaHide',
                                id: 'txtCustomerLamaHide',
                                readOnly:true,
                                hidden:true,
                                width: 280
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Kode customer  ',
                                name: 'txtKdCustomerLamaHide',
                                id: 'txtKdCustomerLamaHide',
                                readOnly:true,
                                hidden:true,
                                width: 280
                            },
                            
                            
                            
                            //---------------------hidden----------------------------------
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtKdDokter',
                                id: 'txtKdDokter',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Unit  ',
                                name: 'txtKdUnitRad',
                                id: 'txtKdUnitRad',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            {
                                xtype: 'textfield',
                                name: 'txtKdUrutMasuk',
                                id: 'txtKdUrutMasuk',
                                readOnly:true,
                                hidden:true,
                                anchor: '100%'
                            }
                                
                            //-------------------------------------------------------------
                            
                        ]
                    },  //akhir panel biodata pasien
            
        ]
    };
    return items;
};
function getParamBalikanHitungUmur(tgl){
    Ext.getCmp('dtpTtlL').setValue(tgl);
}

function getItemPanelUnit(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .40,
                layout: 'form',
                labelWidth:100,
                border: false,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel:'Unit  ',
                        name: 'txtKdUnitRad',
                        id: 'txtKdUnitRad',
                        readOnly:true,
                        anchor: '99%'
                    }
                ]
            },
            {
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth:1,
                items:
                [
                    {
                        xtype: 'textfield',
                        name: 'txtNamaUnit',
                        id: 'txtNamaUnit',
                        readOnly:true,
                        anchor: '100%'
                    }
                ]
            }
        ]
    }
    return items;
};

function refeshtrkasiramb(kriteria){
    Ext.Ajax.request
    (
        {
           // url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getPasien",
            url: baseURL + "index.php/rad/viewpenjasrad/getPasien",
            params: getParamRefreshAmb(kriteria),
            failure: function(o)
            {
                ShowPesanWarningKasirAmb('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dsTRKasirAmbulanceList.removeAll();
                    var recs=[],
                    recType=dsTRKasirAmbulanceList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dsTRKasirAmbulanceList.add(recs);
                    grListKasirAmbulance.getView().refresh();

                }
                else
                {
                    ShowPesanWarningKasirAmb('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
}


function refeshtrkasiramb(kriteria)
{
    /* dsTRKasirAmbulanceList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenJasRad',
                    param : kriteria
                }           
            }
        );   
    return dsTRKasirAmbulanceList; */
    
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getPasien",
            params: getParamRefreshRad(kriteria),
            failure: function(o)
            {
                ShowPesanWarningTrKasirAMB('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dsTRKasirAmbulanceList.removeAll();
                    var recs=[],
                    recType=dsTRKasirAmbulanceList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dsTRKasirAmbulanceList.add(recs);
                 //  Ext.getDom('rb_pilihan1').checked = true;
                    grListKasirAmbulance.getView().refresh();

                }
                else
                {
                    ShowPesanWarningTrKasirAMB('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
    Ext.Ajax.request({
        url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getDefaultUnit",
        params: {
            notrans: 'no',
            Modul:'no'
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            console.log(o);
            var cst = Ext.decode(o.responseText);
           // getComboDokterRad();
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            combovaluesunittujuan=cst.kd_unit;
            
        }

    });
}


function getParamRefreshRad(krite){
	console.log(krite);
    var unit;
    if(radiovalues == 1){
        unit = 'IGD';
    } else if(radiovalues == 2){
        unit = 'RWI';
    } else if(radiovalues == 3){
        unit = 'Langsung';
    } else if (radiovalues == 3){
        unit = 'RWJ';
    }
console.log(unit);
    var params =
    {
        unit:unit,
        kriteria:krite
        /* tglAwal:
        tglAkhir:dtpTglAwalFilterKasirAmb */
        
    }
   // console.log(params);
    return params;
}

function getParamRefreshAmbLama(){
    var params =
    {
        tgl_transaksi_1:Ext.getCmp('dtpTglAwalFilterKasirAmb').getValue(),
        tgl_transaksi_2:Ext.getCmp('dtpTglAkhirFilterKasirAmb').getValue(),
        status_lunas   :Ext.getCmp('cboStatusLunas_viKasirAmb').getValue()
        
    }
    return params;
}

function loadKasirAmb(kriteria,tmpunit){
    console.log(kriteria);
     console.log(tmpunit);
    if(tmpunit==='Umum'){
         Ext.Ajax.request({
            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/read_pasien_lama_langsung/",
            params:{kriteria:kriteria},

            failure: function(o){
                ShowPesanWarningKasirAmb('Hubungi Admin', 'Error');
            },
            success: function(o){   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){ 
                    dsTRKasirAmbulanceList.removeAll();
                    var recs=[],
                    recType=dsTRKasirAmbulanceList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                     // Ext.getDom('rb_pilihan3').checked = true;
                    dsTRKasirAmbulanceList.add(recs);
                    grListKasirAmbulance.getView().refresh();

                }
                else{
                    ShowPesanWarningKasirAmb('Gagal membaca Data ini', 'Error');
                };
            }
        });

    }else{
          Ext.Ajax.request({
            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/read_pasien_lama/",
            params:{kriteria:kriteria},

            failure: function(o){
                ShowPesanWarningKasirAmb('Hubungi Admin', 'Error');
            },
            success: function(o){   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){ 
                    dsTRKasirAmbulanceList.removeAll();
                    var recs=[],
                    recType=dsTRKasirAmbulanceList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                      //Ext.getDom('rb_pilihan1').checked = true; 
                    dsTRKasirAmbulanceList.add(recs);
                    grListKasirAmbulance.getView().refresh();

                }
                else{
                    ShowPesanWarningKasirAmb('Gagal membaca Data ini', 'Error');
                };
            }
        });

    }
 
}

function Datasave_TrKasirAmbulance(mBol) {
console.log('masukan');   
     if (ValidasiEntryTrKasirAmbulance(nmHeaderSimpanData,false) == 1 )
     {
        // console.log(tglTransaksiAwalPembandingTransferRAD);
        // console.log(Ext.getCmp('dtpKunjunganL').getValue().format('Y-m-d')+" 00:00:00");
          if (tglTransaksiAwalPembandingTransferRAD > Ext.getCmp('dtpKunjunganL').getValue().format('Y-m-d')+" 00:00:00"){
             ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
            ShowPesanWarningKasirAmb('Tanggal transaksi pasien Tidak boleh kurang dari tanggal kunjungan', 'Gagal');
            
        }else{
         // loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/savedatamasterambulance",
                    params: getParamDetailTransaksiAmbulance(),
                    failure: function(o)
                    {
                        ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
                    },  
                    success: function(o) 
                    {
                        // loadMask.hide();
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) {
                             console.log(cst);
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
//                            Ext.get('txtNoTransaksiKasirAMBKasir').dom.value = notransnya;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            tmpurut = cst.urut;
                            kd_kasir_amb = cst.kdkasir;
                          	 Ext.get('txtNoTransaksiAmbulance').dom.value=cst.notrans;
                              Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                            loadMask.hide();
                            ShowPesanInfoKasirAmb("Berhasil menyimpan data ini","Information");
                          //  Ext.get('txtNoTransaksiAmbulance').dom.value=cst.notrans;
                           // Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                            ViewGridBawahLookupKasirAmb(tmpno_trans_sql,kd_kasir_amb);
                            Ext.getCmp('btnPembayaranKasirAmb').enable();
                            Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
                            Ext.getCmp('txtNamaPasienL').disable();
                            Ext.getCmp('cboKelPasienAmbulance').disable();
                            Ext.getCmp('cboPerseoranganAmb').disable();
                            Ext.getCmp('txtnoregamb').setValue(cst.no_reg);
                            if(mBol === false)
                            {
                                ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
                            };

                            if (Ext.get('cboJenisTr_viKasirAmb').getValue()!='Transaksi Baru' && radiovalues != '3')
                            {
                                validasiJenisTr();
                            }
                           // Datasave_TrKasirAmbulance_SQL();
                        }
                        else 
                        {
                                ShowPesanWarningKasirAmb('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        }
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function Datasave_TrKasirAmbulance_LangsungTambahBaris(mBol) 
{   
     if (ValidasiEntryTrKasirAmbulance(nmHeaderSimpanData,false) == 1 )
     {
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionRAD/savedetailpenyakit",
                    params: getParamDetailTransaksiAmbulance(),
                    failure: function(o)
                    {
                        ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
                    },  
                    success: function(o) 
                    {
                        loadMask.hide();
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // console.log(cst);
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            tmpurut = cst.urut;
                            kd_kasir_amb = cst.kdkasir;
                            loadMask.hide();
                            //ShowPesanInfoKasirAmb("Berhasil menyimpan data ini","Information");
                            Ext.get('txtNoTransaksiAmbulance').dom.value=cst.notrans;
                            Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                            //ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,tmpkd_kasir_sql);
                            Ext.getCmp('btnPembayaranKasirAmb').enable();
                            Ext.getCmp('btnTutupTransaksiKasirAmb').enable();
                            Ext.getCmp('txtnoregamb').setValue(cst.noreg);
                            /* if(mBol === false)
                            {
                                ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,tmpkd_kasir_sql);
                            }; */
                            if (Ext.get('cboJenisTr_viKasirAmb').getValue()!='Transaksi Baru' && radiovalues != '3')
                            {
                                validasiJenisTr();
                            }
                            
                            //Datasave_TrKasirAmbulance_SQL();
                        }
                        else 
                        {
                                ShowPesanWarningKasirAmb('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

var tmpkd_pasien_sql = '';
var tmpno_trans_sql = '';
var tmpkd_kasir_sql = '';
var tmp_tgl_sql = '';
var tmp_urut_sql = '';


function Dataupdate_KasirAmb(mBol) 
{   
    if (ValidasiEntryTrKasirAmbulance(nmHeaderSimpanData,false) == 1 )
    {
        
            Ext.Ajax.request
             (
                {
                    //url: "./Datapool.mvc/CreateDataObj",
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDataupdateKasirAmbDetail(),
                    success: function(o) 
                    {
                        ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoKasirAmb(nmPesanSimpanSukses,nmHeaderSimpanData);
                            //RefreshDataKasirAmb();
                            if(mBol === false)
                            {
                                ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value,kd_kasir_amb);
                            };
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarningKasirAmb(nmPesanSimpanGagal,nmHeaderSimpanData);
                        }
                        else if (cst.success === false && cst.pesan===1)
                        {
                            ShowPesanWarningKasirAmb(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
                        }
                        else 
                        {
                            ShowPesanErrorKasirAmb(nmPesanSimpanError,nmHeaderSimpanData);
                        };
                    }
                }
            )
        
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
};
function ValidasiEntryTrKasirAmbulance(modul,mBolHapus){
    var x = 1;
    //cboUnitRad_viAmbulance

        if (Ext.get('txtNoTransaksiAmbulance').getValue() == '' && mBolHapus === true) {
            x = 0;
        }

        else if (Ext.get('txtNoMedrecL').getValue() == '') {
            if(Ext.getCmp('txtNamaUnit').getValue()=='Umum'){
                x=1
            }else{
                  ShowPesanWarningKasirAmb('No. Medrec tidak boleh kosong!','Warning');
                  x = 0;
            }
          
        }
        else if (Ext.get('txtNamaPasienL').getValue() == '') {
            ShowPesanWarningKasirAmb('Nama tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('dtpKunjunganL').getValue() == '') {
            ShowPesanWarningKasirAmb('Tanggal kunjungan tidak boleh kosong!','Warning');
            x = 0;
        }   
        else if (Ext.get('cboKelPasienAmbulance').getValue() == '') {
            ShowPesanWarningKasirAmb('Kelompok Pasien tidak boleh kosong!','Warning');
            x = 0;
        }   
        else if (Ext.get('cboPerusahaanRequestEntryAmb').getValue() == '') {
            ShowPesanWarningKasirAmb('Perusahaan / Asuransi tidak boleh kosong!','Warning');
            x = 0;
        }   
        else if (Ext.get('cboKelPasienAmbulance').getValue() == 'Asuransi') {
            if( Ext.get('cboAsuransiAmb').getValue()==''){
                    ShowPesanWarningKasirAmb(' Asuransi tidak boleh kosong!','Warning');
                    x = 0;
            }else if( Ext.get('txtNamaPesertaAsuransiAmb').getValue()==''){
                    ShowPesanWarningKasirAmb('Peserta Asuransi tidak boleh kosong!','Warning');
                    x = 0;
            }else if( Ext.get('txtNoAskesAmb').getValue()==''){
                    ShowPesanWarningKasirAmb('No Akses Asuransi tidak boleh kosong!','Warning');
                    x = 0;
            }else if( Ext.get('txtNoSJPAmb').getValue()==''){
                    ShowPesanWarningKasirAmb('No SJP tidak boleh kosong!','Warning');
                    x = 0;
            }else{
                    x = 0;
            }
        }   
        else if (dsTRDetailKasirAmbList.getCount() === 0) {
            ShowPesanWarningKasirAmb('Daftar item Ambulance tidak boleh kosong!','Warning');
            x = 0;
        };

        
console.log(x);
    return x;
};

function ShowPesanWarningKasirAmb(str, modul) 
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width:250
        }
    );
};

function ShowPesanErrorKasirAmb(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width:250
        }
    );
};

function ShowPesanInfoKasirAmb(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width:250
        }
    );
};


function DataDeleteKasirAmb() 
{
   if (ValidasiEntryTrKasirAmbulance(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiAmbulance(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoKasirAmb(nmPesanHapusSukses,nmHeaderHapusData);
                                        //RefreshDataKasirAmb();
                                        KasirAmbAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningKasirAmb(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningKasirAmb(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorKasirAmb(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customerRAD+'~)'
            }
        }
    )
    
    // rowSelectedKasirAmb = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien',
            typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: radelisi,
                        align: 'Right',
                        anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
    //  title: 'Kelompok Pasien Lama',
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                //title: 'Kelompok Pasien Lama',
                items:
                [
                    {    
                        xtype: 'tbspacer',
                        height: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kelompok Pasien Asal',
                        //maxLength: 200,
                        name: 'txtCustomerLama',
                        id: 'txtCustomerLama',
                        labelWidth:130,
                        width: 100,
                        anchor: '95%'
                    },
                    
                    
                ]
            }
            
        ]
    }
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [
                    
                    
                    {    
                        xtype: 'tbspacer',
                        
                        height:3
                    },
                        {  

                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien',
                         editable: false,
                        //value: 'Perseorangan',
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {
                                            jeniscus='0'
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Perusahaan')
                                        {
                                            jeniscus='1';
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Asuransi')
                                        {
                                            jeniscus='2';
                                            Ext.getCmp('txtNoSJP').enable();
                                            Ext.getCmp('txtNoAskes').enable();
                                        }
                                    
                                        RefreshDatacombo(jeniscus);
                                    }

                                }
                        }, 
                        {
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                mComboKelompokpasien()
                            ]
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. SJP',
                            maxLength: 200,
                            name: 'txtNoSJP',
                            id: 'txtNoSJP',
                            width: 100,
                            anchor: '95%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Askes',
                            maxLength: 200,
                            name: 'txtNoAskes',
                            id: 'txtNoAskes',
                            width: 100,
                            anchor: '95%'
                         }
                        
                        //mComboKelompokpasien
        
        
                ]
            }
            
        ]
    }
    return items;
};

//combo jenis kelamin
function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
    (
        {
            id:'cboJK',
            x: 455,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Jenis Kelamin',
            fieldLabel: 'Jenis Kelamin ',
            editable: false,
            width:100,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetJK,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('cboGDR').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetJK=b.data.valueField ;
                },
/*              'render': function(c) {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir').focus();
                                    }, c);
                                }
 */         }
        }
    );
    return cboJK;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDR = new Ext.form.ComboBox
    (
        {
            id:'cboGDR',
            x: 835,
            y: 70,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Gol. Darah',
            fieldLabel: 'Gol. Darah ',
            width:50,
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetGDarah,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('txtnotlprad').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetGDarah=b.data.displayText ;
                },
                /*'render': function(c) {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir').focus();
                                    }, c);
                                }*/
            }
        }
    );
    return cboGDR;
};

//Combo Dokter Lookup
/*function getComboDokterRad(kdUnit)
{
    dsdokter_viKasirAmb.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '5'"
        }
    });
     dsdokter_viKasirAmb.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '"+kdUnit+"'"
        }
    }); 
    return dsdokter_viKasirAmb;
}*/
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viKasirAmb = new WebApp.DataStore({ fields: Field });
    dsdokter_viKasirAmb.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '5'"
        }
    });
    
    var cboDOKTER_viKasirAmb = new Ext.form.ComboBox
    (
            {
                id: 'cboDOKTER_viKasirAmb',
                x: 110,
                y: 100,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 200,
                emptyText:'Pilih Dokter',
                store: dsdokter_viKasirAmb,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
                //value:'All',
                editable: false,
                enableKeyEvents: true,
                
                listeners:
                {
                    'specialkey': function (){
                            if (Ext.EventObject.getKey() === 13){
                                Ext.getCmp('cboKelPasienAmbulance').focus();
                            }
                        },
                    'select': function(a,b,c)
                    {
                        tmpkddoktertujuan=b.data.KD_DOKTER ;
                    },
                }
            }
    );
    
    return cboDOKTER_viKasirAmb;
};
var tmpkddoktertujuan;

function mComboUnitAmbulance(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitrad_viKasirAmb = new WebApp.DataStore({ fields: Field });
    dsunitrad_viKasirAmb.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "parent = '5'"
        }
    });
    var cboUnitRad_viAmbulance = new Ext.form.ComboBox({
        id: 'cboUnitRad_viAmbulance',
        x: 110,
        y: 160,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 130,
        emptyText:'Pilih Unit',
        store: dsunitrad_viKasirAmb,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
        listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkd_unit = b.data.KD_UNIT;
                    if (Ext.getCmp('txtDokterPengirimL').getValue()=='DOKTER LUAR')
                    {
                        Ext.Ajax.request({
                            url: baseURL + "index.php/main/functionRAD/getTarifMir",
                            params: {
                                kd_unit:b.data.KD_UNIT,
                                 //kd_unit_asal:tmpkd_unit_asal,
                                 kd_customer:vkode_customerRAD,
                                 penjas:'langsung',
                                 kdunittujuan:b.data.KD_UNIT
                            },
                            failure: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                            },
                            success: function (o) {
                                var cst = Ext.decode(o.responseText);
                                // console.log(cst.ListDataObj);
                                tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                            }

                        });
                    }
                    
                    //getComboDokterRad(b.data.KD_UNIT);
                }
            }
    });
    return cboUnitRad_viAmbulance;
}


function mCboKelompokpasien(){  
 var cboKelPasienAmbulance = new Ext.form.ComboBox
    (
        {
            
            id:'cboKelPasienAmbulance',
            fieldLabel: 'Kelompok Pasien',
            x: 110,
            y: 100,
            mode: 'local',
            width: 150,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            emptyText: 'Pilih Kelompok Pasien...',
            width:150,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetKelPasien,
            selectOnFocus: true,
            tabIndex:22,
            listeners:
            {
                'select': function(a, b, c)
                    {
                       Combo_Select(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganAmb').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRad').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryAmb').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRad').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboAsuransiAmb').focus(false,100);                          
                       }
                    },
                'select': function(a, b, c)
                    {
                       Combo_Select(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganAmb').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryAmb').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryLab').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show();  
                             Ext.getCmp('cboAsuransiAmb').focus(false,100);
                       }
                      
                    },
                'render': function(a,b,c)
                {
                    Ext.getCmp('txtNamaPesertaAsuransiAmb').hide();
                    Ext.getCmp('txtNoAskesAmb').hide();
                    Ext.getCmp('txtNoSJPAmb').hide();
/*                    Ext.getCmp('cboPerseoranganAmb').hide();
                    Ext.getCmp('cboAsuransiAmb').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryAmb').hide();*/
                    
                }
            }

        }
    );
    return cboKelPasienAmbulance;
};

function mComboPerseorangan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganRadRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganRadRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=0'
            }
        }
    )
    var cboPerseoranganAmb = new Ext.form.ComboBox
    (
        {
            id:'cboPerseoranganAmb',
            x: 110,
            y: 130,
            editable: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Jenis',
            tabIndex:23,
            width:150,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            store:dsPeroranganRadRequestEntry,
            value:selectSetPerseorangan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customerRAD = b.data.KD_CUSTOMER;
                    selectSetPerseorangan=b.data.displayText ;
                }
            }
        }
    );
    return cboPerseoranganAmb;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                //Sort: 'DEPT_ID',
                Sort: '',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=1'
            }
        }
    )
    var cboPerusahaanRequestEntryAmb = new Ext.form.ComboBox
    (
        {
            id: 'cboPerusahaanRequestEntryAmb',
            x: 110,
            y: 130,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: 'Perusahaan',
            align: 'Right',
            width:150,
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            //anchor: '95%',
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customerRAD = b.data.KD_CUSTOMER;
                    alert(vkode_customerRAD)
                    selectSetPerusahaan=b.data.valueField ;
                }
            }
        }
    );

    return cboPerusahaanRequestEntryAmb;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
        }
    )
    var cboAsuransiAmb = new Ext.form.ComboBox
    (
        {
            id:'cboAsuransiAmb',
            x: 110,
            y: 130,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: 'Asuransi',
            align: 'Right',
            width:150,
            hidden:true,
            //anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customerRAD = b.data.KD_CUSTOMER;
                    //alert(vkode_customerRAD)
                    selectSetAsuransi=b.data.valueField ;
                }
            }
        }
    );
    return cboAsuransiAmb;
};


function Combo_Select(combo)
{
   var value = combo
  var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiAmb') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiAmb').hide()
        Ext.getCmp('txtNoAskesAmb').hide()
        Ext.getCmp('txtNoSJPAmb').hide()
        Ext.getCmp('cboPerseoranganAmb').show()
        Ext.getCmp('cboAsuransiAmb').hide()
        Ext.getCmp('cboPerusahaanRequestEntryAmb').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiAmb').hide()
        Ext.getCmp('txtNoAskesAmb').hide()
        Ext.getCmp('txtNoSJPAmb').hide()
        Ext.getCmp('cboPerseoranganAmb').hide()
        Ext.getCmp('cboAsuransiAmb').hide()
        Ext.getCmp('cboPerusahaanRequestEntryAmb').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiAmb').show()
         Ext.getCmp('txtNoAskesAmb').show()
         Ext.getCmp('txtNoSJPAmb').show()
         Ext.getCmp('cboPerseoranganAmb').hide()
         Ext.getCmp('cboAsuransiAmb').show()
         Ext.getCmp('cboPerusahaanRequestEntryAmb').hide()
         
       }
}


function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirRad').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirRad').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable(); 

Ext.getCmp('btnLookupTRKasirAmbulance').disable()
Ext.getCmp('btnSimpanRAD').disable()
Ext.getCmp('btnHpsBrsRAD').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
//Ext.getCmp('btnLookUpKonsultasi_viKasirRad').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirRad').enable();
Ext.getCmp('btngantipasien').enable();
//Ext.getCmp('btnposting').enable();    

Ext.getCmp('btnLookupTRKasirAmbulance').enable()
Ext.getCmp('btnSimpanRAD').enable()
//Ext.getCmp('btnHpsBrsRAD').enable()
//Ext.getCmp('btnHpsBrsDiagnosa').enable()
//Ext.getCmp('btnSimpanDiagnosa').enable()
//Ext.getCmp('btnLookupDiagnosa').enable()  
}

//Criteria untuk pencarian berdasarkan no medrec, nama pasien dll
function getCriteriaFilter_viDaftar()//^^^
{   
    /*
    var strKriteria = "";
    var tmptanggalawal = Ext.getCmp('dtpTglAwalFilterKasirAmb').getValue();
    var tmptanggalakhir = Ext.getCmp('dtpTglAkhirFilterKasirAmb').getValue();
    var tglawal = tmptanggalawal.getFullYear() + '/' + (tmptanggalawal.getMonth() + 1) + '/' + tmptanggalawal.getDate();
    var tglakhir =  tmptanggalakhir.getFullYear() + '/' + (tmptanggalakhir.getMonth() + 1) + '/' + tmptanggalakhir.getDate();
    if (Ext.get('dtpTglAwalFilterKasirAmb').getValue() != "")
    {
     if (strKriteria == "")
        {
            strKriteria = "and tr.tgl_transaksi >= '" + tglawal + "'" ;
        }
        else
            {
                strKriteria += " and tr.tgl_transaksi >= '" + tglawal+"'";
            }

    }
    if (Ext.get('dtpTglAkhirFilterKasirAmb').getValue() != "")
    {
    if (strKriteria == "")
        {
            strKriteria = "and tr.tgl_transaksi <= '" + tglakhir+"'" ;
        }
        else
            {
                strKriteria += " and tr.tgl_transaksi <= '" + tglakhir+"'";
            }

    }
    */
        var strKriteria = "";
        if (Ext.get('dtpTglAwalFilterKasirAmb').getValue() != "")
            {
                 if (strKriteria == "")
                    {
                        strKriteria = "and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterKasirAmb').getValue()+"'";
                        }
                
            }
            if (Ext.get('dtpTglAkhirFilterKasirAmb').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = "and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue()+"'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterKasirAmb').getValue()+"'";
                        }
                
            }   
     return strKriteria;
}

function getItemPanelTrKasirAmbulance(){
    var items =
    {
        layout:'column',
        border:true,
        items:
        [
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: '100%',
                height: 100,
                //anchor: '100% 100%',
                items:
                [
                    //-----------COMBO JENIS TRANSAKSI-----------------------------
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Transaksi'
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboJenisTrans_viKasirAmb(),
                    
                    
                    
                    //--------RADIO BUTTON PILIHAN ASAL PASIEN---------------------
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 40,
                        xtype: 'radiogroup',
                        id: 'rbasalPasien',
                        fieldLabel: 'Asal Pasien ',
                        items: [
                                    {
                                        boxLabel: 'IGD',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan1',
                                        checked: true,
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true) {
                                                Ext.getCmp('dtpTglAwalFilterKasirAmb').setValue(now);
                                                Ext.getCmp('dtpTglAkhirFilterKasirAmb').setValue(now);
//                                                getDataCariUnitPenjasRad("kd_bagian=3 and parent<>'0'");
                                                Ext.getCmp('cboUNIT_viKasirRad').show();
                                                Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').hide();
                                                //--------------TAMBAH BARU 27-SEPTEMBER-2017
//                                                Ext.getCmp('btnEditDataPasienRAD').disable();
                                                radiovalues='IGD';
                                                asal_pasien='3';
                                                validasiJenisTr();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasRad';
                                                loadKasirAmb(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWJ',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan4',
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true){
                                                Ext.getCmp('dtpTglAwalFilterKasirAmb').setValue(now);
                                                Ext.getCmp('dtpTglAkhirFilterKasirAmb').setValue(now);
                                             //   getDataCariUnitKasirAmb("kd_bagian=2 and type_unit=false");
                                                Ext.getCmp('cboUNIT_viKasirRad').show();
                                                Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').hide();
                                                //--------------TAMBAH BARU 27-SEPTEMBER-2017
                                                Ext.getCmp('btnEditDataPasienRAD').disable();
                                                radiovalues='RWJ';
                                                 asal_pasien='1';
                                                validasiJenisTr();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterKasirAmb').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasRad';
                                                loadKasirAmb(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWI',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan2',
                                        inputValue: '2',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
                                        	//Ext.getCmp('dtpTglAwalFilterKasirAmb').setValue(now);
                                            Ext.getCmp('dtpTglAwalFilterKasirAmb').setValue(now);
                                            Ext.getCmp('dtpTglAkhirFilterKasirAmb').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirRad').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialTrKasirAMBRequestEntry').show();
                                            //--------------TAMBAH BARU 27-SEPTEMBER-2017
//                                            Ext.getCmp('btnEditDataPasienRAD').disable();
                                            radiovalues='RWI';
                                             asal_pasien='2';
                                            validasiJenisTr();
                                            /* tmpparams = " tr.co_Status='0' ORDER BY tr.no_transaksi desc limit 50";
                                            tmpunit = 'ViewRwiPenjasRad';
                                            loadKasirAmb(tmpparams, tmpunit); */
                                        }
                                     }
                                    },
                                    {
                                        boxLabel: 'Umum',
                                        name: 'rb_auto',
                                        id: 'rb_pilihan3',
                                        inputValue: '3',
                                        handler: function (field, value) {
                                        if (value === true){
                                        	if(combovalues=='Transaksi Baru'){
   												 KunjunganLangungAmbulance();	
                                        	}else{
                                        		 radiovalues='UMUM';
                                           		 asal_pasien='4';
                                        	}
                                        
                                        }
                                     }
                                    },
                                ]
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Detail Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    //COMBO POLI dan KAMAR
                    mComboUnit_viKasirAmb(),
                    mcomboKamarSpesial(),
                    
                ]
            },
            //---------------JARAK 1----------------------------
             {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: '100%',
                height: 5,
             //   anchor: '50% 50%',
                items:
                [
                        
                ]
             },
            //--------------------------------------------------
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: '100%',
                height: 160,
             //   anchor: '100% 100%',
                items:
                [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecKasirAmb',
                        id: 'txtNoMedrecKasirAmb',
                        enableKeyEvents: true,
                        width:200,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecKasirAmb').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecKasirAmb').getValue())
                                             Ext.getCmp('txtNoMedrecKasirAmb').setValue(tmpgetNoMedrec);
                                             validasiJenisTr();
                                             /* var tmpkriteria = getCriteriaFilter_viDaftar();
                                             refeshtrkasiramb(tmpkriteria); */
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        validasiJenisTr();
                                                        /* tmpkriteria = getCriteriaFilter_viDaftar();
                                                        refeshtrkasiramb(tmpkriteria); */
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrecKasirAmb').setValue('')
                                            }
                                }
                            }

                        }

                    },


                    {
                        x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Transaksi',
                        name: 'txtNoTransaksiAmbulance',
                        id: 'txtNoMedrecKasirAmb',
                        enableKeyEvents: true,
                         width: 300,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                            }

                        }

                    },  

                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'txtNamaPasienKasirAmb',
                        id: 'txtNamaPasienKasirAmb',
                        width: 300,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
                                            //getCriteriaFilter_viDaftar();
                                            validasiJenisTr();
                                            /* tmpkriteria = getCriteriaFilter_viDaftar();
                                            refeshtrkasiramb(tmpkriteria); */

                                            //RefreshDataFilterKasirAmb();

                                        }                       
                                }
                        }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Alamat '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },                  
                    {   
                        x : 155,
                        y : 70,
                        xtype: 'textfield',
                        name: 'txtalamatpasienrad',
                        id: 'txtalamatpasienrad',
                        width: 500,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
                                            validasiJenisTr();
                                        }                       
                                }
                        }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Status Lunas '
                    },
                    {
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },                  
                    cboStatusLunas_viKasirAmb(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
                    {
                        x: 145,
                        y: 130,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 130,
                        width:120,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterKasirAmb',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    },
                    {
                        x: 290,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 325,
                        y: 130,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterKasirAmb',
                        format: 'd/M/Y',
                        value: now,
                        width: 120
                    },
                    {
                        x: 475,
                        y: 130,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
                            // kd_unit_konfig = '';
                            validasiJenisTr();
                        }
                    }
                    
                ]
            },
            
        //----------------------------------------------------------------  
            
          /*  {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: '100%',
                height: 1,
                anchor: '100% 100%',
                items:
                [
                    
                ]
            },*/
        //----------------------------------------------------------------    
        ]
    }
    return items;
};


function getTindakanAmbulance(){
    var kd_cus_gettarif=vkode_customerRAD;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 3){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getGridProduk",
            params: {
                     kd_unit:tmpkd_unit_tarif_mir,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:kd_cus_gettarif,
                     penjas:modul,
                     kdunittujuan:tmpkd_unit
                    },
            failure: function(o)
            {
                ShowPesanErrorKasirAmb('Error, pasien! Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                
                if (cst.success === true) 
                {
                  
                    console.log(cst);
                    var recs=[],
                        recType=dsDataGrd_getTindakanTrKasirAmbulance.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        
                        recs.push(new recType(cst.listData[i]));
                        
                    }
                    dsDataGrd_getTindakanTrKasirAmbulance.add(recs);

                   // GridGetTindakanTrKasirAmbulance.getView().refresh();
                    console.log( dsDataGrd_getTindakanTrKasirAmbulance);
                    
                    
                    for(var i = 0 ; i < dsDataGrd_getTindakanTrKasirAmbulance.getCount();i++)
                    {
                        var o= dsDataGrd_getTindakanTrKasirAmbulance.getRange()[i].data;
                        for(var je = 0 ; je < dsTRDetailKasirAmbList.getCount();je++)
                        {
                            var p= dsTRDetailKasirAmbList.getRange()[je].data;
                            if (o.kd_produk === p.kd_produk)
                            {
                                o.pilihchkprodukrad = true;
                                
                                
                            }
                        }
                    }
                   
                    GridGetTindakanTrKasirAmbulance.getView().refresh();
                   // GridGetTindakanTrKasirAmbulance.startEditing(line, 2);
                    var row =dsDataGrd_getTindakanTrKasirAmbulance.getCount();
                    GridGetTindakanTrKasirAmbulance.startEditing(row, 2);   
                }
                else 
                {
                    ShowPesanErrorKasirAmb('Gagal membaca data Produk', 'Error');
                };
            }
        }
        
    )
    
}

function setLookUp_getTindakanTrKasirAmbulance(rowdata){
    var lebar = 985;
    setLookUps_getTindakanTrKasirAmbulance = new Ext.Window({
        id: 'FormLookUpGetTindakanAmbulance',
        title: 'Daftar Item', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
        autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,        
        items: getFormItemEntry_getTindakanTrKasirAmbulance(lebar,rowdata),
        listeners:{
            activate: function(){
                
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanKasirAmb=undefined;
            }
        }
    });

    setLookUps_getTindakanTrKasirAmbulance.show();
   
}


function getFormItemEntry_getTindakanTrKasirAmbulance(lebar,rowdata){
    var pnlFormDataBasic_getTindakanTrKasirAmbulance = new Ext.FormPanel({
        title: '',
        region: 'north',
        layout: 'form',
        bodyStyle: 'padding:10px 10px 10px 10px',
        anchor: '100%',
        labelWidth: 1,
        autoWidth: true,
        width: lebar,
        border: false,
        items:[
                gridDataViewEdit_getTindakanTrKasirAmbulance()
            ],
            fileUpload: true,
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Tambahkan',
                        id:'btnTambahKanPermintaan_getTindakanTrKasirAmbulance',
                        iconCls: 'add',
                        //disabled:true,
                        handler:function()
                        {
                            /* var jumlah=0;
                            var items=[];
                            for(var i = 0 ; i < dsDataGrd_getTindakanTrKasirAmbulance.getCount();i++){
                                var o=dsDataGrd_getTindakanTrKasirAmbulance.getRange()[i].data;
                                    if(o.pilihchkprodukrad == true){
                                        jumlah += 1;
                                        
                                        // for(var x = 0; x < 100; x++){
                                            // arr[x] = [];    
                                            // for(var y = 0; y < 100; y++){ 
                                                // arr[x][y] = x*y;    
                                            // }    
                                        // }
                                        var recs=[],
                                            recType=dsTRDetailKasirAmbList.recordType;

                                        for(var i=0; i<dsDataGrd_getTindakanTrKasirAmbulance.getCount(); i++){

                                            recs.push(new recType(dsDataGrd_getTindakanTrKasirAmbulance.data.items[i].data));

                                        }
                                        dsTRDetailKasirAmbList.add(recs);

                                        gridDTItemTest.getView().refresh();
                                    }
                            }
                            console.log(items[0][0]) */
                            
                            
                            
                            
                            var params={};
                            params['jumlah']=dsDataGrd_getTindakanTrKasirAmbulance.getCount();
                            console.log(dsDataGrd_getTindakanTrKasirAmbulance.getCount());
                            dsTRDetailKasirAmbList.removeAll();
                            var recs=[],
                            recType=dsTRDetailKasirAmbList.recordType;
                            var adadata=false;
                            for(var i = 0 ; i < dsDataGrd_getTindakanTrKasirAmbulance.getCount();i++)
                            {
                          //      console.log(dsDataGrd_getTindakanTrKasirAmbulance.data.items[i].data.pilihchkprodukrad);
                                if (dsDataGrd_getTindakanTrKasirAmbulance.data.items[i].data.pilihchkprodukrad === true)
                                {
                                    console.log(dsDataGrd_getTindakanTrKasirAmbulance.data.items[i].data);
                                    recs.push(new recType(dsDataGrd_getTindakanTrKasirAmbulance.data.items[i].data));
                                    adadata=true;
                                }
                            }
                            dsTRDetailKasirAmbList.add(recs);
                            gridDTItemTest.getView().refresh(); 
                           /* var row =dsDataGrd_getTindakanTrKasirAmbulance.getCount()-1;
                            gridDTItemTest.startEditing(row, 2);  */ 

                            if (adadata===true)
                            {
                                setLookUps_getTindakanTrKasirAmbulance.close(); 
                                loadMask.show();
                                //savingnya
                              //  Datasave_TrKasirAmbulance(false); 
                                loadMask.hide();
                                 /* membuat fokus*/
                                
                            }
                            else
                            {
                                ShowPesanWarningKasirAmb('Ceklis data item ambulance  ','Ambulance');
                            }
                            
                        }
                          
                    },
                    
                    
                ]
            }//,items:
        }
    )

    return pnlFormDataBasic_getTindakanTrKasirAmbulance;
}

function RefreshDatahistoribayar(no_transaksi){
    var strKriteriaKasirAMB = '';

    strKriteriaKasirAMB = 'no_transaksi= ~' + no_transaksi + '~ and kd_kasir= ~'+34+'~';
    get_total(no_transaksi);	
    dsTRDetailHistoryList_amb.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirAMB
                                }
                    }
            );
    return dsTRDetailHistoryList_amb;
};
function get_total(no_transaksi){
	  Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/history_bayar",
                        params: {no_transaksi:no_transaksi,kd_kasir:34},
                        success: function (o){
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                               var total_bayar_kasir= formatCurrency(cst.total_bayar); 
                              	Ext.getCmp('txtJumlah2EditData_viKasirAMB_history_bayar').setValue(total_bayar_kasir);
             
                            };
                        }
                    }
            )
}

function panel_total_history_bayar(){

	 var paneltotal_history_bayar = new Ext.Panel
            (
                    {
                        id: 'paneltotal_history_bayar',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 0px -10px 0px',
                        layout: 'column',
                        frame: true,
                        title:'History Bayar',
                      //  height: 10,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [ GetDTLTRHistoryGrid(),
                                    {
                                        xtype: 'compositefield',
                                        //anchor: '100%',
                                        labelSeparator: '',
                                        border: false,
                                        style: {'margin-top': '10px' },
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '750px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirAMB_history_bayar',
                                                        name: 'txtJumlah2EditData_viKasirAMB_history_bayar',
                                                        style: {'text-align': 'right', 'margin-left': '770px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        //anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirAMB_history_bayar',
                                                        name: 'txtJumlahEditData_viKasirAMB_history_bayar',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    return paneltotal_history_bayar;
}

function GetDTLTRHistoryGrid(){

    var fldDetailAmb = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_amb = new WebApp.DataStore({fields: fldDetailAmb})


    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                       
                        stripeRows: true,
                        store: dsTRDetailHistoryList_amb,
                        border: false,
                        columnLines: true,
                        frame: false,
                       // anchor: '100% 25%',
                        height:'170',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec){
                                                            Ext.getCmp('btnHpsBrsKasirAMB').enable();
                                                            console.log(row);
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList_amb.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                        },
                                                         afterrender : function(grid, eOpts){
												            ///console.log('Sum >> ', grid.dsTRDetailHistoryList_amb.sum('BAYAR'));
												            console.log(grid.dsTRDetailHistoryList_amb().sum('NameColumn'));
												        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirAMB',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_amb.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningKasirAmb('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            /* Ext.getCmp('btnEditKasirRAD').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirRAD').disable();
                                            Ext.getCmp('btnHpsBrsKasirAMB').disable(); */
                                        },
                                        disabled: true
                                    }
                                ]
                    },//gans
                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirAMB',
                                                        name: 'txtJumlah2EditData_viKasirAMB',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },

            );

             

    return gridDTLTRHistory;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirRADKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirRAD('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }  
             });  */
            msg_box_alasanhapus_RAD();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_RAD()
{
    var lebar = 250;
    form_msg_box_alasanhapus_RAD = new Ext.Window
            (
                    {
                        id: 'alasan_hapusRad',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanHapusRAD',
                                                    id: 'txtAlasanHapusRAD',
                                                    emptyText: 'Alasan Hapus',
                                                    anchor: '99%',
                                                },
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusRad',
                                                                    handler: function ()
                                                                    {
                                                                        //alert(kdkasir);
                                                                        DataDeleteKasirRADKasirDetail();
                                                                        form_msg_box_alasanhapus_RAD.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusRad',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_RAD.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_RAD.show();
}
;

function mComboalasan_hapusRad()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusRad = new WebApp.DataStore({fields: Field});

    dsalasan_hapusRad.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusRad = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusRad',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusRad,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusRad = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusRad;
}
;
function DataDeleteKasirRADKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirAmbKasirDetail(),
                        success: function (o)
                        {
                            //  RefreshDatahistoribayar(Kdtransaksi);
                           // RefreshDataFilterKasirRADKasir();
                            //RefreshDatahistoribayar('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirAmb("Proses Hapus Pembayaran Berhasil", nmHeaderHapusData);
                                Ext.getCmp('btnPembayaranKasirAmb').enable();
                                RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiAmbulance').getValue());
                                Ext.getCmp('btnTutupTransaksiKasirAmb').disable();
                                validasiJenisTr();
                                //alert(Kdtransaksi);                    

                                //refeshKasirRADKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirAmb(nmPesanHapusGagal, nmHeaderHapusData);
                            }else  if (cst.success === false && cst.type === 6)
                            {
                                ShowPesanWarningKasirAmb("Transaksi sudah ditutup.", nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasirAmb(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirAmbKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: TglTransaksi,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: 34,
                KeTterangan: Ext.get('txtAlasanHapusRAD').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
};


function TRHistoryColumModel(){
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'Urut Bayar',
                            dataIndex: 'URUT',
                            hidden:false

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
//var a={};
function gridDataViewEdit_getTindakanTrKasirAmbulance(){
    var FieldGrdKasir_getTindakanKasirAmb = [];
    dsDataGrd_getTindakanTrKasirAmbulance= new WebApp.DataStore({
        fields: FieldGrdKasir_getTindakanKasirAmb
    });
    chkgetTindakanKasirAmb = new Ext.grid.CheckColumn
        (
            {
                
                id: 'chkgetTindakanKasirAmb',
                header: 'Pilih',
                align: 'center',
                //disabled:false,
                sortable: true,
                dataIndex: 'pilihchkprodukrad',
                anchor: '10% 100%',
                width: 30,
                listeners: 
                {
                    checkchange: function()
                    {
                        alert('hai');
                    }
                }
                /* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            alert('hai');
                            return true;
                            
                            break;
                        case 'f':
                            alert('fei');
                            return false;
                            break;
                    }
                    return false;
                } */  
        
            }
        ); 
    GridGetTindakanTrKasirAmbulance =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
        //title: 'Dafrar Pemeriksaan',
        store: dsDataGrd_getTindakanTrKasirAmbulance,
        autoScroll: true,
        columnLines: true,
        border: false,
        height: 250,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                    }
                        }
                ),
        listeners:
                {
                    // Function saat ada event double klik maka akan muncul form view
                    rowclick: function (sm, ridx, cidx)
                    {
                        
                    },
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        
                    }

                },
        /* xtype: 'editorgrid',
        store: dsDataGrd_getTindakanTrKasirAmbulance,
        height: 250,
        autoScroll: true,
        columnLines: true,
        border: false,
        plugins: [ new Ext.ux.grid.FilterRow(),chkgetTindakanKasirAmb ],
        selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowclick: function(sm, row, rec){
                    /* rowSelectedGridPasien_viGzPermintaanDiet = undefined;
                    rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
                    CurrentDataPasien_viGzPermintaanDiet
                    CurrentDataPasien_viGzPermintaanDiet.row = row;
                    CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
                    
                    dsTRDetailDietGzPermintaanDiet.removeAll();
                    getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                    
                    if(GzPermintaanDiet.vars.status_order == 'false'){
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
                        Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
                    } else{
                        Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
                    }
                    Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                     
                },
            }
        }), */
        //stripeRows: true,
        colModel:new Ext.grid.ColumnModel([
            //new Ext.grid.RowNumberer(),   
            {
                dataIndex: 'kd_klas',
                header: 'Kode Produk',
                sortable: true,
                width: 50,
                hidden:true,
                filter: {}
            },
            new Ext.grid.RowNumberer(),
            {
                dataIndex: 'deskripsi',
                header: 'Nama Item',
                width: 70,
                align:'left',
                filter: {}
            },
            {
                dataIndex: 'kd_tarif',
                header: 'Kd Tarif',
                width: 70,
                hidden : true,
                align:'center'
            },/*,{
                dataIndex: 'tgl_transaksi',
                header: 'Tgl Transaksi',
                hidden : false,
                sortable: true,
                width: 70
            },*/
            { 
				id: 'tarif',
				header: 'Tarif',
				dataIndex: 'tarif',
				width: 80,
				renderer: function(value){
				var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
				return str;
				}
			},
			{
                dataIndex: 'tgl_berlaku',
                header: 'Tgl Berlaku',
                width: 70,
                align:'left',
                hidden:true,
             //   filter: {}
            },
            /*{
                dataIndex: 'qty',
                header: 'qty',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'jumlah',
                header: 'jumlah',
                width: 70,
                hidden : true,
                align:'center'
            },*/
            chkgetTindakanKasirAmb
        ]),
        viewConfig:{
            forceFit: true
        } 
    });
    return GridGetTindakanTrKasirAmbulance;
}

function getDokterPengirim(no_transaksi,kd_kasir){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionRAD/getDokterPengirim",
            params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
            failure: function(o)
            {
                ShowPesanErrorKasirAmb('Hubungi Admin', 'Error');
            },
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
                }
                else
                {
                    ShowPesanErrorKasirAmb('Gagal membaca dokter pengirim', 'Error');
                };
            }
        }

    )
}

function printbillAmbAmb()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarAmb(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarAmb()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingAmbAJA',
                notrans_asal:notransaksiasal_rad,
                No_TRans: Ext.get('txtNoTransaksiKasirAMBKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'rad',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirAMB').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirRAD').getValue(),
                TlpPasien: Ext.get('txtnotlprad').getValue(),
                tgl_bayar: Ext.get('dtpTanggalBayarDetransaksiRAD').getValue(),
                
            };
    var urut=[];
    var kd_produk=[];
    var items=Ext.getCmp('gridDTLTRKasirRAD').getStore().data.items;
    for(var i=0,iLen=items.length; i<iLen; i++){
        if(items[i].data.TAG==true){
            urut.push(items[i].data.URUT);
            kd_produk.push(items[i].data.KD_PRODUK);
        }
    }
    paramscetakbill_vikasirDaftarRadLab['no_urut[]']=urut;
    paramscetakbill_vikasirDaftarRadLab['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadRad()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirAmb('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirAmb('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
};
function getParamDetailTransaksiAmbulance(){
    var tampungshiftsekarang;
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionRAD/getCurrentShiftRad",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovalues == 1){
        modul='igd';
    } else if(radiovalues == 2){
        modul='rwi';
    } else if(radiovalues == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    /* if(Ext.get('cboKelPasienRad').getValue()=='Perseorangan'){
        KdCust=Ext.getCmp('cboPerseoranganRad').getValue();
    }else if(Ext.get('cboKelPasienRad').getValue()=='Perusahaan'){
        KdCust=Ext.getCmp('cboPerusahaanRequestEntryRad').getValue();
    }else {
        KdCust=Ext.getCmp('cboAsuransiRad').getValue();
    } */
    
    if(Ext.getCmp('txtNoMedrecL').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    
    var params =
    {
        //Table:'ViewDetailTransaksiPenJasRad',
        
        KdTransaksi: Ext.get('txtNoTransaksiAmbulance').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecL').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienL').getValue(),
        Ttl:Ext.getCmp('dtpTtlL').getValue(),
        Alamat:Ext.getCmp('txtAlamatL').getValue(),
        JK:Ext.getCmp('cboJK').getValue(),
        GolDarah:Ext.getCmp('cboGDR').getValue(),
        KdUnit: tmpkd_unit_asal,
      //  KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:'801',
        Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
        TglTransaksiAsal:TglTransaksi,
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customerRAD,
        TmpCustoLama:vkode_customerRAD,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiAmb').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesAmb').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPAmb').getValue(),
        Shift:1,
        List:getArrKasirAmb(),
        JmlField: mRecordRad.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiAMB(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:52,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
      //  listTrDokter    : '',
        Modul:modul,
        KdProduk:'',//sTRDetailPenJasRadList.data.items[CurrentPenJasRad.row].data.KD_PRODUK
        URUT :tmpurut,
        no_reg:Ext.getCmp('txtnoregamb').getValue()
        
    };
    console.log(params);
    return params
};

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingAmbAJA',
                No_TRans: Ext.get('txtNoTransaksiKasirAMBKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'rad',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirAMB').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirRAD').getValue()
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function getParamHapusDetailTransaksiAMB(){   
    var tmpparam = cellSelecteddeskripsi.data;
    var params =
    {       
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi
    };
    console.log(params);
    return params
};



function Delete_Detail_Rad_SQL(){
    Ext.Ajax.request
     (
        {
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteRadDetail(),
            failure: function(o)
            {
                ShowPesanWarningKasirAmb('Error Hapus. Hubungi Admin!', 'Gagal');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    // ShowPesanInfoKasirAmb('Data Berhasil Di hapus', 'Sukses');
                    // dsTRDetailKasirAmbList.removeAt(line);
                    // gridDTItemTest.getView().refresh();
                    // Delete_Detail_Rad_SQL();
                }
                else 
                {
                        ShowPesanWarningKasirAmb('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                };
            }
        }
    )
}

function getParamDataDeleteRadDetail(){
    var tmpparam = cellSelecteddeskripsi.data;
    var params =
    {       
        Table: 'SQL_RAD',
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi     
    };
    return params

}
var tmpunitfotorad;
var tmpurutfotorad;
var tmptglfotorad;
function KasirAmbUpdateFotoLookUp(rowdata) 
{
    var lebar = 275;
    FormKasirAmbUpdateFotoLookUp = new Ext.Window
    (
        {
            id: 'windowKasirAmbUpdateFotoLookUp',
            title: 'Update Foto Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 190,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: [formpopupUpdateFotoRad()],
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){

                    }else{
                        // console.log(rowdata);
                        tmpunitfotorad = rowdata.KD_UNIT;
                        tmptglfotorad = rowdata.TGL_TRANSAKSI;
                        tmpurutfotorad = rowdata.URUT;
                        var tmpdate = new Date(rowdata.TGL_TRANSAKSI);
                        var realdate = tmpdate.format("d/M/Y");
                        Ext.getCmp('TxtPopupMedrecRad').setValue(rowdata.KD_PASIEN);
                        Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
                        Ext.getCmp('TxtPopupTglKunjungPasien').setValue(realdate);
                        Ext.getCmp('TxtPopupNoFotoPasien').setValue(rowdata.NO_FOTO)
                    }
                    
                    
                }
            }
        }
    );

    FormKasirAmbUpdateFotoLookUp.show();
    // if (rowdata === undefined) 
    // {
    //     // KasirAmbAddNew();
    // }
    // else 
    // {
    //     // TRKasirAmbInit(rowdata);
    // }

};

function formpopupUpdateFotoRad() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopupUpdateFotoRad',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelUpdateFotoRad()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_RAD',
                                                    // disabled: true,
                                                    handler: function ()
                                                    {
                                                        if (Ext.getCmp('TxtPopupNoFotoPasien').getValue() !== '') {
                                                            UpdateFotoRad();
                                                        }else{
                                                            ShowPesanWarningKasirAmb('No. Foto Tidak Boleh Kosong', 'Warning');
                                                        }
                                                        
                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupdatahasilrad;
}
;

function PanelUpdateFotoRad() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 450,
                height: 150,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec'
                    },
                    {
                        x: 90,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupMedrecRad',
                        id: 'TxtPopupMedrecRad',
                        disable:true,
                        width: 80
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 90,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        disable:true,
                        width: 150
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tgl. Kunjungan '
                    },
                    {
                        x: 90,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupTglKunjungPasien',
                        id: 'TxtPopupTglKunjungPasien',
                        disable:true,
                        width: 80
                    },
                    /*{
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'No. Foto '
                    },
                    {
                        x: 90,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 100,
                        xtype: 'textfield',
                        name: 'TxtPopupNoFotoPasien',
                        id: 'TxtPopupNoFotoPasien',
                        width: 80
                    },*/
                ]
            }
        ]
    };
    return items;
}
;

function UpdateFotoRad(){
    Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionRAD/updatefotorad",
                    params: getParamUpdateFotoRAD(),
                    failure: function(o)
                    {
                        ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoKasirAmb("Berhasil menyimpan data ini","Information");
                        }
                        else 
                        {
                                ShowPesanWarningKasirAmb('Data Gagal Di Simpan', 'Gagal');
                        };
                    }
                }
            )
};

function getParamUpdateFotoRAD(){
    var params =
            {
                kdpasien: Ext.getCmp('TxtPopupMedrecRad').getValue(),
                kdunit: tmpunitfotorad,
                tgl: tmptglfotorad,
                urut: tmpurutfotorad,
                nofoto: Ext.getCmp('TxtPopupNoFotoPasien').getValue()
            };
    return params
}

function TransferData_amb_SQL(){
    Ext.Ajax.request
        (
            {
                url: baseURL + "index.php/rad_sql/function_rad_sql/saveTransfer",
                params: getParamTransferRwi_dari_rad(),
                failure: function (o)
                {

                    ShowPesanWarningKasirAmb('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                    // ViewGridBawahLookupKasirAmb();
                },
                success: function (o)
                {   
                
                    
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {   
                        // TransferData_amb_SQL();
                        // Ext.getCmp('btnSimpanKasirAMB').disable();
                        // Ext.getCmp('btnTransferKasirAMB').disable();
                        // ShowPesanInfoKasirAmb('Transfer Berhasil', 'transfer ');
                        // RefreshDatahistoribayar(Ext.getCmp('txtNoTransaksiAmbulance').getValue());
                        // FormLookUpsdetailTRTransfer_Rad.close();
                        
                    } else
                    {
                        ShowPesanWarningKasirAmb('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                    }
                    ;
                }
            }
        )
}
var tmpnotransPF;
var tmptgltransPF;
var tmpuruttransPF;


function formpopupPemakaianFilmRad() {
    var Field = ['NAMA_OBAT','KD_PRODUK','HARGA','QTY','QTY_RSK','QTY_STD','MAX_QTY','JNS_BHN'];
        dtsgridpemakaianfilm = new WebApp.DataStore({ fields: Field });
        
        //  var k="tr.tgl_transaksi >='"+tigaharilaluNew+"' and tr.tgl_transaksi <='"+tglGridBawah+"'    and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc";
        
        // refeshtrkasiramb(k);

        gridpemakaianfilm = new Ext.grid.EditorGridPanel
        (
            {
                stripeRows: true,
                id:'KasirAmb',
                store: dtsgridpemakaianfilm,
                anchor: '100% 100%',
                height:'300',
                columnLines: false,
                autoScroll:true,
                border: false,
                editable:true,
                sort :false,
                sm: new Ext.grid.RowSelectionModel
                (
                    {
                        singleSelect: true,
                        listeners:
                        {
                            rowselect: function(sm, row, rec)
                            {
                                
                                
                            }
                        }
                    }
                ),
            cm: new Ext.grid.ColumnModel
                (
                    [
                       
                        {
                            id: 'colkdfilmpemakaianfilm',
                            header: 'Kd. Film',
                            dataIndex: 'KD_PRODUK',
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            width: 50
                        },
                        {
                            id: 'coluraianpemakaianfilm',
                            header: 'Uraian',
                            dataIndex: 'NAMA_OBAT',
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            width: 100,
                        },
                        {
                            id: 'coljmlpemakaianfilm',
                            header: 'Jml',
                            dataIndex: 'QTY',
                            width: 50,
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            editor:{
                                xtype:'textfield'
                            }                        
                        },
                        {
                            id: 'coljmlrskpemakaianfilm',
                            header: 'jml. Rsk',
                            dataIndex: 'QTY_RSK',
                            width: 50,
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            editor:{
                                xtype:'textfield'
                            }                          
                        },
                       
                    ]
                ),
                viewConfig: {forceFit: true},
                tbar:
                    [
                        {
                            id: 'btnEditRWJ',
                            text: 'Save & Exit',
                            tooltip: nmEditData,
                            iconCls: 'Save',
                            handler: function(sm, row, rec)
                            {
                                loadMask.show();
                                DataUpdate_PemakaianFotoKasirAmb();
                            }
                        }
                    ]
                }
        );
    var FrmTabs_formpopupPemakianFilmRad = new Ext.Panel
            (
                    {
                        id: 'FrmTabs_formpopupPemakianFilmRad',
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        // margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    PanelPemakaianFilmRad(),gridpemakaianfilm
                                ]
                    }
            );
    return FrmTabs_formpopupPemakianFilmRad;
}
;
var gridpemakaianfilm;
var dtsgridpemakaianfilm;
function PanelPemakaianFilmRad() {
        
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: true,
                width: 500,
                height: 30,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 7,
                        xtype: 'label',
                        text: 'Tindakan '
                    },
                    {
                        x: 90,
                        y: 7,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 100,
                        y: 5,
                        xtype: 'textfield',
                        name: 'TxtPopupTindakanPFRad',
                        id: 'TxtPopupTindakanPFRad',
                        editable:false,
                        width: 300
                    },
                ]
            }
        ]
    };
    return items;
}
;

function loadFilmKasirAmb(rowdata)
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionRAD/getdatafoto",
            params: getParamRefreshlistfotoRad(rowdata),
            failure: function(o)
            {
                ShowPesanWarningKasirAmb('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dtsgridpemakaianfilm.removeAll();
                    var recs=[],
                    recType=dsTRKasirAmbulanceList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dtsgridpemakaianfilm.add(recs);
                    gridpemakaianfilm.getView().refresh();

                }
                else
                {
                    ShowPesanWarningKasirAmb('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
}

function getParamRefreshlistfotoRad(rowdata){

    // var kriteria = "WHERE dfo.kd_kasir = '"+ rowdata. +"' AND dfo.no_transaksi = '4603972' AND dfo.tgl_transaksi = '2017-01-18' AND dfo.urut = 1";
    var params =
    {
        kd_kasir:'',
        notrans:rowdata.no_transaksi,
        tgltransaksi:rowdata.tgl_transaksi,
        urut:rowdata.urut       
    }
    return params;
}

function getArrfotoKasirAmb()
{
    var x='';
    var arr=[];
    for(var i = 0 ; i < dtsgridpemakaianfilm.getCount();i++)
    {
        var o={};
        var y='';
        var z='@@##$$@@';
        o['KD_PRD_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.KD_PRODUK;
        o['QTY_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.QTY;
        o['QTY_RSK_RAD_FO']= dtsgridpemakaianfilm.data.items[i].data.QTY_RSK;
        arr.push(o);
        
    }   
    
    return Ext.encode(arr);
};

function DataUpdate_PemakaianFotoKasirAmb() 
{   
    Ext.Ajax.request
     (
        {
            url: baseURL + "index.php/main/functionRAD/UpdatePemakaianFotoRad",
            params: getParamDetailPemakaianFotoRAD(),
            failure: function(o)
            {
                loadMask.hide();
                ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) 
                {
                    loadMask.hide();
                    //ShowPesanInfoKasirAmb("Berhasil menyimpan data ini","Information");
                 //   FormTrKasirAmbMobilLookUp.close();
                    // Ext.get('txtNoTransaksiAmbulance').dom.value=cst.notrans;
                    // Ext.get('txtNoMedrecL').dom.value=cst.kdPasien;
                    // ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value);
                    // Ext.getCmp('btnPembayaranKasirAmb').enable();
                    // Ext.getCmp('btnTutupTransaksiKasirAmb').enable();
                    // Ext.getCmp('txtnoregamb').setValue(cst.noreg);
                    // if(mBol === false)
                    // {
                    //     ViewGridBawahLookupKasirAmb(Ext.get('txtNoTransaksiAmbulance').dom.value);
                    // };

                    // tmpkd_pasien_sql = cst.kdPasien;
                    // tmpno_trans_sql = cst.notrans;
                    // tmpkd_kasir_sql = cst.kdkasir;
                    // tmp_tgl_sql = cst.tgl;
                    // tmp_urut_sql = cst.urut;
                    // tmpurut = cst.urut;
                   // DataUpdate_PemakaianFotoKasirAmb_SQL();
                }
                else 
                {
                    loadMask.hide();
                    ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                };
            }
        }
    )
};

function getParamDetailPemakaianFotoRAD() 
{
    
    var params =
    {        
        notrans: tmpnotransPF,
        tgltrans:tmptgltransPF,
        uruttrans:tmpuruttransPF,
        List:getArrfotoKasirAmb(),
        
    };
    return params
};

function DataUpdate_PemakaianFotoKasirAmb_SQL(){
    Ext.Ajax.request
        (
            {
                url: baseURL + "index.php/rad_sql/function_rad_sql/UpdatePemakaianFotoRad",
                params: getParamDetailPemakaianFotoRAD(),
                failure: function (o)
                {

                    ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                },
                success: function (o)
                {   
                
                    
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {   
                        
                    } else
                    {
                        ShowPesanWarningKasirAmb('Error simpan. Hubungi Admin!', 'Gagal');
                    }
                    ;
                }
            }
        )
}

function setUsia(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('txtUmurRad').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('txtUmurRad').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('txtUmurRad').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}

/*----------------------------------------------------Tambah Baris Item Test---------------------------------------------------------
 Oleh     : HDHT 
 Tanggal  : 01-februari-2017
 BANDUNG 
 */

function TambahBarisRAD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRAD();
        dsTRDetailKasirAmbList.insert(dsTRDetailKasirAmbList.getCount(), p);
    };
};
/*----------------------------------------------------Ganti Kelompok Pasien---------------------------------------------------------
 Oleh     : HDHT 
 Tanggal  : 21-februari-2017
 MADIUN 
 */

var jeniscus_rad;
var labelisi_rad;
var panelActiveDataPasien;
function KelompokPasienLookUp_rad(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_rad = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_rad(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_rad.show();
    KelompokPasienbaru_rad(rowdata);

};

function KelompokPasienbaru_rad(rowdata) 
{
    jeniscus_rad=0;
    KelompokPasienAddNew_rad = true;
    Ext.getCmp('cboKelompokpasien_rad').show()
    Ext.getCmp('txtCustomer_radLama').disable();
    Ext.get('txtCustomer_radLama').dom.value=rowdata.CUSTOMER;
    Ext.get('txtRWJNoSEP').dom.value = rowdata.SJP;
    console.log(rowdata);
    // Ext.getCmp('txtRWJNoSEP').disable();
    // Ext.getCmp('txtRWJNoAskes').disable();
    
    RefreshDatacombo_rad(jeniscus_rad,rowdata.KELPASIEN);
};

function getFormEntryTRKelompokPasien_rad(lebar,rowdata) 
{
    var pnlTRKelompokPasien_rad = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_rad',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputKelompokPasien_rad(lebar),
                    getItemPanelButtonKelompokPasien_rad(lebar,rowdata)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
    (
        {
            id: 'FormDepanKelompokPasien',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_rad 
                
            ]

        }
    );

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_rad(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [
                    getKelompokpasienlama_rad(lebar),   
                    getItemPanelNoTransksiKelompokPasien_rad(lebar) ,
                    
                ]
            }
        ]
    };
    return items;
};

function getKelompokpasienlama_rad(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [{   
                        xtype: 'tbspacer',
                        height: 2
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kelompok Pasien Asal',
                            name: 'txtCustomer_radLama',
                            id: 'txtCustomer_radLama',
                            labelWidth:130,
                            editable: false,
                            width: 100,
                            anchor: '95%'
                         }
                    ]
            }
            
        ]
    }
    return items;
};

function getItemPanelNoTransksiKelompokPasien_rad(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: true,
                items:
                [{   
                        xtype: 'tbspacer',
                        height:3
                    },{ 
                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien_rad',
                        editable: false,
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                 {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {jeniscus_rad='0'
                                            Ext.getCmp('txtRWJNoSEP').disable();
                                            Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Perusahaan')
                                        {jeniscus_rad='1';
                                            Ext.getCmp('txtRWJNoSEP').disable();
                                            Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Asuransi')
                                        {jeniscus_rad='2';
                                            Ext.getCmp('txtRWJNoSEP').enable();
                                            Ext.getCmp('txtRWJNoAskes').enable();
                                        }
                                        
                                        RefreshDatacombo_rad(jeniscus_rad);
                                    }

                                }
                        },{
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                                mComboKelompokpasien_rad()
                            ]
                        },{
                            xtype: 'textfield',
                            fieldLabel:'No SEP  ',
                            name: 'txtRWJNoSEP',
                            id: 'txtRWJNoSEP',
                            width: 100,
                            anchor: '99%'
                         }, {
                             xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtRWJNoAskes',
                            id: 'txtRWJNoAskes',
                            width: 100,
                            anchor: '99%'
                         }
                                    
                ]
            }
            
        ]
    }
    return items;
};

function mComboKelompokpasien_rad()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_RAD = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    if (jeniscus_rad===undefined || jeniscus_rad==='')
    {
        jeniscus_rad=0;
    }
    ds_customer_viPJ_RAD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_rad +'~'
            }
        }
    )
    var cboKelompokpasien_rad = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien_rad',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih...',
            fieldLabel: labelisi_rad,
            align: 'Right',
            anchor: '95%',
            store: ds_customer_viPJ_RAD,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien_rad;
};


function RefreshDatacombo_rad(jeniscus_rad,vkode_customerRAD_rad) 
{

    ds_customer_viPJ_RAD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_rad +'~ and kontraktor.kd_customer not in(~'+ vkode_customerRAD_rad+'~)'
            }
        }
    )
    
    return ds_customer_viPJ_RAD;
};

function getItemPanelButtonKelompokPasien_rad(lebar,rowdata) 
{
    var items =
    {
        layout: 'column',
        border: false,
        height:30,
        anchor:'100%',
        style:{'margin-top':'-1px'},
        items:
        [
            {
                layout: 'hBox',
                width:400,
                border: false,
                bodyStyle: 'padding:5px 0px 5px 5px',
                defaults: { margins: '3 3 3 3' },
                anchor: '90%',
                layoutConfig: 
                {
                    align: 'middle',
                    pack:'end'
                },
                items:
                [
                    {
                        xtype:'button',
                        text:'Simpan',
                        width:70,
                        style:{'margin-left':'0px','margin-top':'0px'},
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function()
                        {
                            if(panelActiveDataPasien == 0){
                                Datasave_Kelompokpasien_rad(rowdata);
                            }
                            if(panelActiveDataPasien == 1){
                                Datasave_GantiDokter_rad(rowdata);
                            }
                            
                        }
                    },
                    {
                        xtype:'button',
                        text:'Tutup',
                        width:70,
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function() 
                        {
                            if(panelActiveDataPasien == 0){
                                FormLookUpsdetailTRKelompokPasien_rad.close();
                            }
                            if(panelActiveDataPasien == 1){
                                FormLookUpsdetailTRGantiDokter_rad.close();
                            }
                            
                        }
                    }
                ]
            }
        ]
    }
    return items;
};

function Datasave_Kelompokpasien_rad(rowdata) 
{   
    if (ValidasiEntryUpdateKelompokPasien_rad(nmHeaderSimpanData,false) == 1 )
    {           
            Ext.Ajax.request
            (
                {
                    url: baseURL +  "index.php/ambulance/functionTrkasirAmbulance/UpdateGantiKelompok",   
                    params: getParamKelompokpasien_amb(rowdata),
                    failure: function(o)
                    {
                        ShowPesanWarningKasirAmb('Simpan kelompok pasien gagal', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            validasiJenisTr();
                            ShowPesanInfoKasirAmb("Mengganti kelompok pasien berhasil", "Success");
                            FormLookUpsdetailTRKelompokPasien_rad.close();
                        }else 
                        {
                            ShowPesanWarningKasirAmb('Simpan kelompok pasien gagal', 'Gagal');
                        };
                    }
                }
             )
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
    
};

function ValidasiEntryUpdateKelompokPasien_rad(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.get('kelPasien_rad').getValue() == '') || (Ext.get('kelPasien_rad').dom.value  === undefined ))
    {
        if (Ext.get('kelPasien_rad').getValue() == '' && mBolHapus === true) 
        {
            ShowPesanWarningKasirAmb(nmGetValidasiKosong('Kelompok Pasien'), modul);
            x = 0;
        }
    };
    return x;
};

function getParamKelompokpasien_amb(rowdata){
console.log(rowdata);
    var params;
    var urut;
    if (rowdata.URUT != '') {
        urut = rowdata.URUT;
    }else{
        urut = rowdata.URUT_MASUK;
    }
    if(panelActiveDataPasien == 0){
        params = {
        KDCustomer  : selectKdCustomer,
        KDNoSJP     : Ext.get('txtRWJNoSEP').getValue(),
        KDNoAskes   : Ext.get('txtRWJNoAskes').getValue(),
        KdPasien    : rowdata.KD_PASIEN,
        TglMasuk    : rowdata.TGL_TRANSAKSI,
        KdUnit      : rowdata.KD_UNIT,
        UrutMasuk   : urut,
        KdDokter    : rowdata.KD_DOKTER,
        NoTransaksi : rowdata.NO_TRANSAKSI,
        }
    }else if(panelActiveDataPasien == 1 ){
        params = {
            KdPasien    : rowdata.KD_PASIEN,
            TglMasuk    : rowdata.TGL_TRANSAKSI,
            KdUnit      : rowdata.KD_UNIT,
            UrutMasuk   : urut,
            KdDokter    : vKdDokter,
            NoTransaksi : rowdata.NO_TRANSAKSI,
            KdKasir     : rowdata.KD_KASIR,
            KdUnitDulu  : rowdata.KD_UNIT_ASAL,
        }
    }
    return params
};

/*----------------------------------------------------Ganti Dokter Radiologi---------------------------------------------------------
 Oleh     : HDHT 
 Tanggal  : 22-februari-2017
 MADIUN 
 */

function GantiDokterPasienLookUp_rad(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_rad = new Ext.Window
    (
        {
            id: 'idGantiDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_rad(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_rad.show();
    // GantiPasien_rad(rowdata);

};

function getFormEntryTRGantiDokter_rad(lebar,rowdata) 
{
    var pnlTRKelompokPasien_rad = new Ext.FormPanel
    (
        {
            id: 'PanelTRGanti',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputGantiDokter_rad(lebar,rowdata),
                    getItemPanelButtonKelompokPasien_rad(lebar,rowdata)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokter = new Ext.Panel
    (
        {
            id: 'FormDepanGantiDokter',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: true,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_rad 
                
            ]

        }
    );

    return FormDepanGantiDokter
};

function getItemPanelInputGantiDokter_rad(lebar,rowdata) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [   
                    {
                        xtype: 'textfield',
                        fieldLabel:  'Unit Asal ',
                        name: 'txtUnitAsal_DataPasien',
                        id: 'txtUnitAsal_DataPasien',
                        value:rowdata.NAMA_UNIT_ASLI,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Dokter Asal ',
                        name: 'txtDokterAsal_DataPasien',
                        id: 'txtDokterAsal_DataPasien',
                        value:rowdata.DOKTER,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },
                    mComboDokterGantiEntry()
                ]
            }
        ]
    };
    return items;
};
var dsDokterRequestEntry;
var vKdDokter;
function mComboDokterGantiEntry(){ 
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({ fields: Field });
    dsDokterRequestEntry.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '5'"
        }
    });
    var cboDokterGantiEntry = new Ext.form.ComboBox({
        id: 'cboDokterRequestEntry',
        typeAhead: true,
        triggerAction: 'all',
        name:'txtdokter',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih Dokter...',
        fieldLabel: 'Dokter Baru',
        align: 'Right',
        store: dsDokterRequestEntry,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        anchor:'100%',
        listeners:{
            'select': function(a,b,c){
                vKdDokter = b.data.KD_DOKTER;
            },
        }
    });
    return cboDokterGantiEntry;
};

function Datasave_GantiDokter_rad(rowdata) 
{   
    //console.log(Ext.get('cboDokterRequestEntry').getValue());
    if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
    {
        ShowPesanWarningKasirAmb('Dokter baru harap diisi', "Informasi");
    }else{
        Ext.Ajax.request
        (
            {
                url: baseURL +  "index.php/ambulance/functionTrkasirAmbulance/UpdateGantiDokter", 
                params: getParamKelompokpasien_amb(rowdata),
                failure: function(o)
                {
                    ShowPesanWarningKasirAmb('Simpan dokter pasien gagal', 'Gagal');
                },  
                success: function(o) 
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) 
                    {
                        panelActiveDataPasien = 1;
                        validasiJenisTr();
                        FormLookUpsdetailTRGantiDokter_rad.close();
                        ShowPesanInfoKasirAmb("Mengganti dokter pasien berhasil", "Success");

                    }else 
                    {
                        ShowPesanWarningKasirAmb('Simpan dokter pasien gagal', 'Gagal');
                    };
                }
            }
        ) 
    }
    
};

function getcitorad(){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionRAD/cekCitoRadiologi",
        params: {Modul:''},
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.ListDataObj);
            // kodepasien = rowdata.KD_PASIEN;
            // namapasien = rowdata.NAMA;
            // console.log(rowdata.KD_UNIT);
            // namaunit = rowdata.NAMA_UNIT;
            // kodepay = cst.ListDataObj[0].kd_pay;
            // uraianpay = cst.ListDataObj[0].cara_bayar;
            // kdcustomeraa = rowdata.KD_CUSTOMER;
            // cst.ListDataObj
            citopersentasi = cst.cito;
            
        }

    });
}

//------------------TAMBAH BARU 27-September-2017
function setLookUpGridDataView_viRAD(rowdata){
    var lebar = 819; // Lebar Form saat PopUp
    setLookUpsGridDataView_viRAD = new Ext.Window({
        id: 'setLookUpsGridDataView_viRAD',
        title: 'Edit Data Pasien',
        closeAction: 'destroy',
        autoScroll: true,
        width: 640,
        height: 225,
        resizable: false,
        border: false,
        plain: true,
        layout: 'fit',
        modal: true,
        items: getFormItemEntryGridDataView_viRAD(lebar, rowdata),
        listeners:{
            activate: function (){
            },
            afterShow: function (){
                this.activate();
            },
            deactivate: function (){
                
            }
        }
    });
    setLookUpsGridDataView_viRAD.show();
    datainit_viRAD(rowdata)
    
}
function getFormItemEntryGridDataView_viRAD(lebar, rowdata)
{
    var pnlFormDataWindowPopup_viRAD = new Ext.FormPanel
            (
                    {
                        title: '',
                        // region: 'center',
                        fileUpload: true,
                        margin:true,
                        // layout: 'anchor',
                        // padding: '8px',
                        bodyStyle: 'padding-top:5px;',
                        // Tombol pada tollbar Edit Data Pasien
                        tbar: {
                            xtype: 'toolbar',
                            items:
                                    [
                                        {
                                            xtype: 'button',
                                            text: 'Save',
                                            id: 'btnSimpanWindowPopup_viRAD',
                                            iconCls: 'save',
                                            handler: function (){
                                                datasave_EditDataPasienRAD(false); //1.1
                                                
                                            }
                                        },
                                        //-------------- ## --------------
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        //-------------- ## --------------
                                        {
                                            xtype: 'button',
                                            text: 'Save & Close',
                                            id: 'btnSimpanExitWindowPopup_viRAD',
                                            iconCls: 'saveexit',
                                            handler: function ()
                                            {
                                                var x = datasave_EditDataPasienRAD(false); //1.2
                                                
                                                if (x === undefined)
                                                {
                                                    setLookUpsGridDataView_viRAD.close();
                                                }
                                            }
                                        },
                                        //-------------- ## --------------
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        //-------------- ## --------------
                                    ]
                        },
                        //-------------- #items# --------------
                        items:
                                [
                                    // Isian Pada Edit Data Pasien
                                    {
                                        xtype: 'panel',
                                        title: '',
                                        layout:'form',
                                        margin:true,
                                        border:false,
                                        labelAlign: 'right',
                                        width: 610,
                                        items:
                                                [//---------------# Penampung data untuk fungsi update # ---------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        id: 'TxtTmpAgama_viRAD',
                                                        name: 'TxtTmpAgama_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        id: 'TxtTmpPekerjaan_viRAD',
                                                        name: 'TxtTmpPekerjaan_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        id: 'TxtTmpPendidikan_viRAD',
                                                        name: 'TxtTmpPendidikan_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //------------------------ end ---------------------------------------------                                
                                                    //-------------- # Pelengkap untuk kriteria ke Net. # --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'KD_CUSTOMER',
                                                        id: 'TxtWindowPopup_KD_CUSTOMER_viRAD',
                                                        name: 'TxtWindowPopup_KD_CUSTOMER_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'URUT_MASUK',
                                                        id: 'TxtWindowPopup_Urut_Masuk_viRAD',
                                                        name: 'TxtWindowPopup_Urut_Masuk_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'KD_PASIEN',
                                                        id: 'TxtWindowPopup_KD_PASIEN_viRAD',
                                                        name: 'TxtWindowPopup_KD_PASIEN_viRAD',
                                                        readOnly: true,
                                                        hidden: true
                                                    },
                                                    //-------------- # End Pelengkap untuk kriteria ke Net. # --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'No. MedRec',
                                                        id: 'TxtWindowPopup_NO_RM_viRAD',
                                                        name: 'TxtWindowPopup_NO_RM_viRAD',
                                                        emptyText: 'No. MedRec',
                                                        labelSeparator: '',
                                                        readOnly: true,
                                                        flex: 1,
                                                        width: 195
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama',
                                                        id: 'TxtWindowPopup_NAMA_viRAD',
                                                        name: 'TxtWindowPopup_NAMA_viRAD',
                                                        emptyText: 'Nama',
                                                        labelSeparator: '',
                                                        flex: 1,
                                                        anchor: '100%'
                                                    },{
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Alamat',
                                                        id: 'TxtWindowPopup_ALAMAT_viRAD',
                                                        name: 'TxtWindowPopup_ALAMAT_viRAD',
                                                        emptyText: 'Alamat',
                                                        labelSeparator: '',
                                                        flex: 1,
                                                        anchor: '100%'
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'No Tlp',
                                                        labelSeparator: '',
                                                        anchor: '100%',
                                                        width: 250,
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_No_Tlp_viRAD',
                                                                        name: 'TxtWindowPopup_No_Tlp_viRAD',
                                                                        emptyText: 'No Tlp',
                                                                        flex: 1,
                                                                        width: 195
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 17,
                                                                        height: 23
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Tgl. Lahir',
                                                                        style: {'text-align': 'right'},
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 58
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'datefield',
                                                                        fieldLabel: 'Tanggal',
                                                                        id: 'DateWindowPopup_TGL_LAHIR_viRAD',
                                                                        name: 'DateWindowPopup_TGL_LAHIR_viRAD',
                                                                        width: 198,
                                                                        format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey' : function(){
                                                                                if (Ext.EventObject.getKey() === 13){
                                                                                    var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viRAD').getValue();
                                                                                    Ext.Ajax.request({
                                                                                        url: baseURL + "index.php/main/GetUmur",
                                                                                        params: {
                                                                                            TanggalLahir: tmptanggal
                                                                                        },
                                                                                        success: function (o){
                                                                                            var tmphasil = o.responseText;
                                                                                            var tmp = tmphasil.split(' ');
                                                                                            if (tmp.length == 6){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else if(tmp.length == 4){
                                                                                                if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else if(tmp.length == 2 ){
                                                                                                if (tmp[1] == 'year' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if (tmp[1] == 'years' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                                                                                }else if (tmp[1] == 'mon'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                }else if (tmp[1] == 'mons'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                }
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else if(tmp.length == 1){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                                                                                                getParamBalikanHitungUmurPopUp(tmptanggal);
                                                                                            }else{
                                                                                                alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                            }   
                                                                                        }
                                                                                    });
                                                                                }
                                                                            },
                                                                        }
                                                                    }
                                                                    //-------------- ## --------------              
                                                                ]
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Umur',
                                                        labelSeparator: '',
                                                        anchor: '100%',
                                                        width: 50,
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_TAHUN_viRAD',
                                                                        name: 'TxtWindowPopup_TAHUN_viRAD',
                                                                        emptyText: 'Thn',
                                                                        style: {'text-align': 'right'},
                                                                        readOnly: true,
                                                                        flex: 1,
                                                                        width: 35
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Thn',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 20
                                                                    },
                                                                    //-------------- ## --------------
                                                                    /* {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_BULAN_viRAD',
                                                                        name: 'TxtWindowPopup_BULAN_viRAD',
                                                                        emptyText: 'Bln',
                                                                        style: {'text-align': 'right'},
                                                                        readOnly: true,
                                                                        flex: 1,
                                                                        width: 35
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Bln',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 20
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtWindowPopup_HARI_viRAD',
                                                                        name: 'TxtWindowPopup_HARI_viRAD',
                                                                        emptyText: 'Hari',
                                                                        style: {'text-align': 'right'},
                                                                        readOnly: true,
                                                                        flex: 1,
                                                                        width: 35
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Hari',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 20
                                                                    } */
                                                                    //-------------- ## --------------          
                                                                ]
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Jenis Kelamin',
                                                        labelSeparator: '',
                                                        anchor: '100%',
                                                        width: 250,
                                                        items:
                                                                [
                                                                    mComboJKRAD(),
                                                                    {
                                                                        xtype: 'displayfield',
                                                                        fieldLabel: 'Label',
                                                                        value: 'Golongan Darah',
                                                                        id: '',
                                                                        name: '',
                                                                        flex: 1,
                                                                        width: 90
                                                                    },
                                                                    mComboGolDarahRAD()
                                                                            //-------------- ## --------------              
                                                                ]
                                                    },
                                                    //-------------- ## --------------                      

                                                    //-------------- ## --------------
                                                ]
                                    },
                                    //-------------- ## --------------
                                    /*{
                                     xtype: 'spacer',
                                     width: 10,
                                     height: 1
                                     },*/
                                    //-------------- ## --------------
                                    
                                            //-------------- ## --------------
                                ]
                                //-------------- #items# --------------
                    }
            )
    return pnlFormDataWindowPopup_viRAD;
}

function datainit_viRAD(rowdata){
    var tmpjk;
    var tmp_data;
    addNew_viDataPasien = false;
    if (rowdata.jk === "t" || rowdata.jk === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
        Ext.get('cboJKRAD').dom.value= 'Laki-laki';
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
        Ext.get('cboJKRAD').dom.value= 'Perempuan'
    }
    ID_JENIS_KELAMIN = rowdata.jk;
    Ext.getCmp('cboGolDarahRAD').setValue(rowdata.goldarah);
    Ext.getCmp('TxtWindowPopup_ALAMAT_viRAD').setValue(rowdata.alamat);
    Ext.getCmp('TxtWindowPopup_NAMA_viRAD').setValue(rowdata.nama);
    Ext.getCmp('TxtWindowPopup_NO_RM_viRAD').setValue(rowdata.medrec);
    Ext.getCmp('TxtWindowPopup_No_Tlp_viRAD').setValue(rowdata.hp);
    Ext.get('DateWindowPopup_TGL_LAHIR_viRAD').dom.value=ShowDate(rowdata.tgl_lahir);
    setUsiaPopUp(ShowDate(rowdata.tgl_lahir));
}
function mComboGolDarahRAD()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarahRAD',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJKRAD()
        {
            
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJKRAD',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;
function setUsiaPopUp(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viRAD').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}
function datasave_EditDataPasienRAD(mBol){  
    
    if (ValidasiEntry_viDataPasienRAD('Simpan Data', true) == 1){
        Ext.Ajax.request({
            url: baseURL + "index.php/main/functionKasirPenunjang/simpanEditDataPasien",
            params: dataparam_datapasviDataPasienRAD(),
            failure: function (o){
                ShowPesanErrorKasirAmb('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
            },
            success: function (o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    ShowPesanInfoKasirAmb('Data berhasil disimpan', 'Edit Data');
                    validasiJenisTr();
                    //DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                } else if (cst.success === false && cst.pesan === 0){
                    ShowPesanErrorKasirAmb('Data tidak berhasil diupdate ', 'Edit Data');
                } else{
                    ShowPesanErrorKasirAmb('Data tidak berhasil diupdate. ', 'Edit Data');
                }
            }
        });
    } else{
        if (mBol === true){
            return false;
        }
    }
    
}
function ValidasiEntry_viDataPasienRAD(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === 'Nama' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viRAD').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === '' ||
                     (Ext.get('TxtWindowPopup_ALAMAT_viRAD').getValue() === ''))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viRAD').getValue() === ''))
        {
            ShowPesanWarningKasirAmb('Nama belum terisi', modul);
            x = 0;
        }  
    }

    return x;
}

function dataparam_datapasviDataPasienRAD(){
    var params_viDataPasienRAD ={
                //-------------- # modelist Net. # --------------
        //-------------- # textfield # --------------
        NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viRAD').getValue(),
        NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viRAD').getValue(),
        ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viRAD').getValue(),
        NO_TELP: Ext.getCmp('TxtWindowPopup_No_Tlp_viRAD').getValue(),
        //NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataPasien').getValue(),

        //-------------- # datefield # --------------
        TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viRAD').getValue(),
        //-------------- # combobox # --------------
        ID_JENIS_KELAMIN: Ext.getCmp('cboJKRAD').getValue(),
        ID_GOL_DARAH: Ext.getCmp('cboGolDarahRAD').getValue(),
        TRUESQL: false
    }
    return params_viDataPasienRAD
}
function getParamBalikanHitungUmurPopUp(tgl){
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viRAD').setValue(tgl);
}


function mComboRujukanAmbulance(){
    
      var Field = ['cara_penerimaan','penerimaan'];
      dsRujukanAmbulance = new WebApp.DataStore({fields: Field});
      
       cboRujukanAmbulance = new Ext.form.ComboBox
    (
        {
            x: 120,
            y: 170,
            id: 'cboRujukankAmbulance',
            typeAhead: true,
            disabled:true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            align: 'Right',
            store: dsRujukanAmbulance,
            valueField: 'cara_penerimaan',
            displayField: 'penerimaan',
            width:150,
            tabIndex:3,
            listeners:
            {
                'select': function (a, b, c){
                  //  tmp_kd_mobil = b.data.kd_mobil;
            },                                     
            }
                }
    );
    return cboRujukanAmbulance;
};

function loadDataComboRujukanAmbulance(){
    Ext.Ajax.request({
    url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getComboRujukan",
        params: '0',
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType =  dsRujukanAmbulance.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsRujukanAmbulance.add(recs);
            }
                console.log(dsRujukanAmbulance);
        }
    });
}


function mComboNamaRujukanAmbulance(){
    
      var Field = ['kd_rujukan','rujukan'];
      dsNamaRujukanAmbulance = new WebApp.DataStore({fields: Field});
      
       cboNamaRujukanAmbulance = new Ext.form.ComboBox
    (
        {
            x: 370,
            y: 170,
            id: 'cboNamaRujukankAmbulance',
            typeAhead: true,
            disabled:true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            align: 'Right',
            store: dsNamaRujukanAmbulance,
            valueField: 'kd_rujukan',
            displayField: 'rujukan',
            width:150,
            tabIndex:3,
            listeners:
            {
                'select': function (a, b, c){
                  //  tmp_kd_mobil = b.data.kd_mobil;
            },                                     
            }
                }
    );
    return cboNamaRujukanAmbulance;
};

/*function loadDataComboNamaRujukanAmbulance(){
    Ext.Ajax.request({
    url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getComboNamaRujukan",
        params: '0',
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType =  dsNamaRujukanAmbulance.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                dsNamaRujukanAmbulance.add(recs);
            }
                console.log(dsNamaRujukanAmbulance);
        }
    });
}*/


function setLookUp_getAmbulance(rowdata){
    var lebar = 985;
    setLookUps_getAmbulance = new Ext.Window({
        id: 'FormLookUpGetAmbulance',
        title: 'Daftar Item', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
        autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,        
        items: getFormItemEntry_getTindakanTrKasirAmbulance(lebar,rowdata),
        listeners:{
            activate: function(){
                
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanKasirAmb=undefined;
            }
        }
    });

    setLookUps_getAmbulance.show();
   
}

function setLookUp_getMobilTrKasirAmbulance(rowdata){
    var lebar = 985;
    setLookUps_getMobilTrKasirAmbulance = new Ext.Window({
        id: 'FormLookUpGetMobilAmbulance',
        title: 'Daftar Mobil Ambulance', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
        autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,        
        items: getFormItemEntry_getMobilTrKasirAmbulance(lebar,rowdata),
        listeners:{
            activate: function(){
                
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanKasirAmb=undefined;
            }
        }
    });

    setLookUps_getMobilTrKasirAmbulance.show();
   
}

function getFormItemEntry_getMobilTrKasirAmbulance(lebar,rowdata){
    var pnlFormDataBasic_getMobilTrKasirAmbulance = new Ext.FormPanel({
        title: '',
        region: 'north',
        layout: 'form',
        bodyStyle: 'padding:10px 10px 10px 10px',
        anchor: '100%',
        labelWidth: 1,
        autoWidth: true,
        width: lebar,
        border: false,
        items:[
                gridDataViewEdit_getMobilTrKasirAmbulance()
            ],
            fileUpload: true,
            tbar: {
                xtype: 'toolbar',
                items: 
                [
                    {
                        xtype: 'button',
                        text: 'Tambahkan',
                        id:'btnTambahKanPermintaan_getMobilTrKasirAmbulance',
                        iconCls: 'add',
                        //disabled:true,
                        handler:function()
                        {        
                            var params={};
                            params['jumlah']=dsDataGrd_getMobilTrKasirAmbulance.getCount();
                            console.log(dsDataGrd_getMobilTrKasirAmbulance.getCount());
                            dsTRDetailKasirAmbList.removeAll();
                            var recs=[],
                            recType=dsTRDetailKasirAmbList.recordType;
                            var adadata=false;
                            for(var i = 0 ; i < dsDataGrd_getMobilTrKasirAmbulance.getCount();i++)
                            {
                              console.log(dsDataGrd_getMobilTrKasirAmbulance);
                                if (dsDataGrd_getMobilTrKasirAmbulance.data.items[i].data.pilihchkprodukMobilAmbulance === true)
                                {
                                    console.log(dsDataGrd_getMobilTrKasirAmbulance.data.items[i].data);
                                    recs.push(new recType(dsDataGrd_getMobilTrKasirAmbulance.data.items[i].data));
                                    adadata=true;
                                }
                            }
                            dsTRDetailKasirAmbList.add(recs);
                            gridDTItemTest.getView().refresh(); 
                            var row =dsDataGrd_getMobilTrKasirAmbulance.getCount()-1;
                            gridDTItemTest.startEditing(row, 2);   
                            console.log(recs);
                            if (adadata===true)
                            {
                                setLookUps_getMobilTrKasirAmbulance.close(); 
                                loadMask.show();
                              //  Datasave_TrKasirAmbulance(false); 
                                loadMask.hide();
                            }
                            else
                            {
                                ShowPesanWarningKasirAmb('Ceklis data item ambulance  ','Ambulance');
                            }
                            
                        }
                          
                    },
                    
                    
                ]
            }//,items:
        }
    )

    return pnlFormDataBasic_getMobilTrKasirAmbulance;
}


function gridDataViewEdit_getMobilTrKasirAmbulance(){
    var FieldGrdKasir_getMobilAmbulance = [];
    dsDataGrd_getMobilTrKasirAmbulance= new WebApp.DataStore({
        fields: FieldGrdKasir_getMobilAmbulance
    });
    chkgetMobilAmbulance = new Ext.grid.CheckColumn
        (
            {
                
                id: 'chkgetMobilAmbulance',
                header: 'Pilih',
                align: 'center',
                //disabled:false,
                sortable: true,
                dataIndex: 'pilihchkprodukMobilAmbulance',
                anchor: '10% 100%',
                width: 30,
                listeners: 
                {
                    checkchange: function()
                    {
                        alert('hai');
                    }
                }
        
            }
        ); 
    GridGetMobilTrKasirAmbulance =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
        //title: 'Dafrar Pemeriksaan',
        store: dsDataGrd_getMobilTrKasirAmbulance,
        autoScroll: true,
        columnLines: true,
        border: false,
        height: 250,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                    }
                        }
                ),
        listeners:
                {
                    // Function saat ada event double klik maka akan muncul form view
                    rowclick: function (sm, ridx, cidx)
                    {
                        
                    },
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        
                    }

                },
        
        colModel:new Ext.grid.ColumnModel([
            //new Ext.grid.RowNumberer(),   
            {
                dataIndex: 'nopol',
                header: 'Nopol',
                sortable: true,
                width: 50,
                 filter: {}
            },
            {
                dataIndex: 'mobil',
                header: 'Mobil',
                width: 100,
                align:'left',
                 filter: {}
            },
            {
                dataIndex: 'kondisi',
                header: 'Kondisi',
                width: 70,
                align:'left',
                 filter: {}
            },
           
            chkgetMobilAmbulance
        ]),
        viewConfig:{
            forceFit: true
        } 
    });
    return GridGetMobilTrKasirAmbulance;
}


function getMobilAmbulace(){
    var kd_cus_gettarif=vkode_customerRAD;
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getGridMobil",
             params: {
                     kd_unit:tmpkd_unit_tarif_mir
                    },
            failure: function(o)
            {
                ShowPesanErrorKasirAmb('Error, pasien! Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                
                if (cst.success === true) 
                {
                  
                    console.log(cst);
                    var recs=[],
                        recType=dsDataGrd_getMobilTrKasirAmbulance.recordType;
                        
                    for(var i=0; i<cst.listData.length; i++){
                        
                        recs.push(new recType(cst.listData[i]));
                        
                    }
                    dsDataGrd_getMobilTrKasirAmbulance.add(recs);

                   // GridGetTindakanTrKasirAmbulance.getView().refresh();
                    console.log( dsDataGrd_getMobilTrKasirAmbulance.getCount);                    
                    for(var i = 0 ; i < dsDataGrd_getMobilTrKasirAmbulance.getCount();i++)
                    {
                        var o= dsDataGrd_getMobilTrKasirAmbulance.getRange()[i].data;
                        for(var je = 0 ; je < dsTRDetailKasirAmbList.getCount();je++)
                        {
                            var p= dsTRDetailKasirAmbList.getRange()[je].data;
                            if (o.kd_produk === p.kd_produk)
                            {
                                o.pilihchkprodukrad = true;
                                
                                
                            }
                        }
                    }
                   
                    //    GridGetTindakanTrKasirAmbulance.getView().refresh();
                   // GridGetTindakanTrKasirAmbulance.startEditing(line, 2);
                    var row =dsDataGrd_getMobilTrKasirAmbulance.getCount();
                    GridGetTindakanTrKasirAmbulance.startEditing(row, 2);   
                }
                else 
                {
                    ShowPesanErrorKasirAmb('Gagal membaca data Tindakan ini', 'Error');

                };
            }
        }
        
    )
    
}

function loadDataComboMobilAmbulance(){
    var Field = ['kd_mobil','mobil'];
    DataMobilAMBDS = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request({
    url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getGridMobil",
        params: '0',
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            //cboMobilAmbulance.store.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = DataMobilAMBDS.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                DataMobilAMBDS.add(recs);
                console.log(DataMobilAMBDS);
            }
        }
    });
}

function loadDataComboSupirAmbulance(){
    var Field = ['kd_supir','nama_supir'];
    DataSupirAMBDS = new WebApp.DataStore({fields: Field});
    Ext.Ajax.request({
    url: baseURL + "index.php/ambulance/functionTrkasirAmbulance/getSupirAmbulance",
        params: '0',
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            //cboMobilAmbulance.store.removeAll();
            var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = DataSupirAMBDS.recordType;
                var o=cst['listData'][i];
        
                recs.push(new recType(o));
                DataSupirAMBDS.add(recs);
                console.log(DataSupirAMBDS);
            }
        }
    });
}
function mComboUnitRad(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitrad_viPenJasRad = new WebApp.DataStore({ fields: Field });
    dsunitrad_viPenJasRad.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "parent = '5'"
        }
    });
    var cbounitrad_viPenJasRad = new Ext.form.ComboBox({
        id: 'cboUnitRad_viPenJasRad',
        x: 110,
        y: 190,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 130,
        emptyText:'Pilih Unit',
        store: dsunitrad_viPenJasRad,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
        listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkd_unit = b.data.KD_UNIT;
                    if (Ext.getCmp('txtDokterPengirimL').getValue()=='DOKTER LUAR')
                    {
                        Ext.Ajax.request({
                            url: baseURL + "index.php/main/functionRAD/getTarifMir",
                            params: {
                                kd_unit:b.data.KD_UNIT,
                                 //kd_unit_asal:tmpkd_unit_asal,
                                 kd_customer:vkode_customerRAD,
                                 penjas:'langsung',
                                 kdunittujuan:b.data.KD_UNIT
                            },
                            failure: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                            },
                            success: function (o) {
                                var cst = Ext.decode(o.responseText);
                                // console.log(cst.ListDataObj);
                                tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                            }

                        });
                    }
                    
                    //getComboDokterRad(b.data.KD_UNIT);
                }
            }
    });
    return cbounitrad_viPenJasRad;
}
function KunjunganLangungAmbulance(rowdata){
    var lebar = 920;
    FormLookUpsdetailTRKasirAmb = new Ext.Window
    (
        {
            id: 'gridKunjLansung',
            title: ' Lookup Pendaftaran Ambulance UMUM ',
            //	closeAction: 'destroy',
            width: lebar,
            height: 520,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true, 
            items: getFormEntryKasirAmbulance(lebar,rowdata),
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){
                        Ext.getCmp('txtNamaPasienL').focus(false,100);
                        Ext.getCmp('txtNoMedrecL').setReadOnly(true);
                        Ext.getCmp('txtNamaUnit').setValue('Umum').setReadOnly(true);
                        Ext.getCmp('txtNamaPasienL').setReadOnly(false);
                        Ext.getCmp('txtAlamatL').setReadOnly(false);
                        Ext.getCmp('dtpKunjunganL').setReadOnly(false);
                        Ext.getCmp('txtnotlprad').setReadOnly(false);
                        Ext.getCmp('cboJK').setReadOnly(false);
                        Ext.getCmp('cboKelPasienAmbulance').setReadOnly(false);
                        Ext.getCmp('cboPerseoranganAmb').setReadOnly(false);
                        Ext.getCmp('dtpTtlL').setReadOnly(false);
                        Ext.getCmp('txtUmurRad').setReadOnly(false);
                        Ext.getCmp('cboGDR').setReadOnly(false);
                        Ext.getCmp('txtnoregamb').disable();
                        //Ext.getCmp('btnTutupTransaksiKasirAmb').hidden(true);
                       
                    }
                    
                    
                },
                'close':function(){
					console.log('close');
                     Ext.getDom('rb_pilihan1').checked = true;
                }
			
				 
            }
        }
    );

    FormLookUpsdetailTRKasirAmb.show();
   if (rowdata === undefined) 
    {
        KasirAmbAddNew();
    }
    else 
    {
        TRKasirAmbInit(rowdata);
    }
    //  FormLookUpsdetailTRrwj.show();
    

};

