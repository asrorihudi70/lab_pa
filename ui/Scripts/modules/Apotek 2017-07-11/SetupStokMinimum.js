var dataSource_viSetupStokMinimum;
var NamaForm_viSetupStokMinimum="Setup Stok Minimum";
var mod_name_viSetupStokMinimum="Setup Stok Minimum";
var now_viSetupStokMinimum= new Date();
var rowSelected_viSetupStokMinimum;

var GridDataView_viSetupStokMinimum;
var gridcbounitfar_SetupStokMinimum;
var gridcbomilik_SetupStokMinimum;
var dsunitfar_SetupStokMinimum;
var dsmilik_SetupStokMinimum;
var kodenamaobat;
var dskodenama;
var currentKdPrd="";
var currentkd_milik="";
var currentstok_min="";

dskodenama = new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'kd_milik', 'milik','kd_prd','nama_obat'],
	data: []
});


var CurrentData_viSetupStokMinimum =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupStokMinimum={};
SetupStokMinimum.form={};
SetupStokMinimum.func={};
SetupStokMinimum.vars={};
SetupStokMinimum.func.parent=SetupStokMinimum;
SetupStokMinimum.form.ArrayStore={};
SetupStokMinimum.form.ComboBox={};
SetupStokMinimum.form.DataStore={};
SetupStokMinimum.form.Record={};
SetupStokMinimum.form.Form={};
SetupStokMinimum.form.Grid={};
SetupStokMinimum.form.Panel={};
SetupStokMinimum.form.TextField={};
SetupStokMinimum.form.Button={};

SetupStokMinimum.form.ArrayStore.obat= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik','kd_prd','nama_obat','min_stok'],
	data: []
});

CurrentPage.page = dataGrid_viSetupStokMinimum(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function dataGrid_viSetupStokMinimum(mod_id_viSetupStokMinimum){	
	
    var FieldMaster_viSetupStokMinimum =  [  ];
    dataSource_viSetupStokMinimum = new WebApp.DataStore ({ fields: FieldMaster_viSetupStokMinimum });
	
	var Fieldmilik = ['kd_milik','milik'];
    dsmilik_SetupStokMinimum = new WebApp.DataStore({ fields: Fieldmilik });
	
	/* var Fieldunitfar = ['kd_unit_far','nm_unit_far'];
    dsunitfar_SetupStokMinimum = new WebApp.DataStore({ fields: Fieldunitfar });
	
	
	
	loaddatacombounitfar_SetupStokMinimum();
	loaddatacombomilik_SetupStokMinimum(); */
	
	dataGriSetupStokMinimum();
	//loaddatacombounitfar_SetupStokMinimum();
	loaddatacombomilik_SetupStokMinimum();
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupStokMinimum = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupStokMinimum,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupStokMinimum = undefined;
							rowSelected_viSetupStokMinimum = dataSource_viSetupStokMinimum.getAt(row);
							CurrentData_viSetupStokMinimum
							CurrentData_viSetupStokMinimum.row = row;
							CurrentData_viSetupStokMinimum.data = rowSelected_viSetupStokMinimum.data;
							currentKdPrd = CurrentData_viSetupStokMinimum.data.kd_prd;
							currentkd_milik = CurrentData_viSetupStokMinimum.data.kd_milik;
							currentstok_min = CurrentData_viSetupStokMinimum.data.min_stok;
							console.log(currentstok_min)
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupStokMinimum = dataSource_viSetupStokMinimum.getAt(ridx);
					if (rowSelected_viSetupStokMinimum != undefined)
					{
						
					}
					else
					{
						//setLookUp_viSetupStokMinimum();
					}
				},
				render: function(){
					
				}
				// End Function # --------------
			},
			
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupTarif',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah',
						iconCls: 'add',
						disabled:true,
						id: 'btnAddGrid_viSetupStokMinimum',
						handler: function()
						{
							var records = new Array();
							records.push(new dataSource_viSetupStokMinimum.recordType());
							dataSource_viSetupStokMinimum.add(records);
							
							var line=dataSource_viSetupStokMinimum.getCount()-1;
							GridDataView_viSetupStokMinimum.startEditing(line,2);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus baris',
						iconCls: 'remove',
						disabled:true,
						id: 'btnDeleteGrid_viSetupStokMinimum',
						handler: function()
						{
							var line	=GridDataView_viSetupStokMinimum.getSelectionModel().selection.cell[0];
							var o=dataSource_viSetupStokMinimum.getRange()[line].data;
							dataSource_viSetupStokMinimum.removeAt(line);
							GridDataView_viSetupStokMinimum.getView().refresh();
							
						}
					},
				]
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Tarif
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					//-------------- ## --------------
					{
						header		: 'Kode Obat',
						width		: 80,
						menuDisabled: true,
						dataIndex	: 'kd_prd'
					},
					//-------------- ## --------------
					{
						header: 'Nama Obat',
						dataIndex: 'nama_obat',
						hideable:false,
						menuDisabled: true,
						width: 150,
						editor:new Nci.form.Combobox.autoComplete({
							store	: SetupStokMinimum.form.ArrayStore.obat,
							select	: function(a,b,c){
								
								var line	=GridDataView_viSetupStokMinimum.getSelectionModel().selection.cell[0];
								dataSource_viSetupStokMinimum.getRange()[line].data.nama_obat=b.data.nama_obat;
								dataSource_viSetupStokMinimum.getRange()[line].data.kd_prd=b.data.kd_prd;
								dataSource_viSetupStokMinimum.getRange()[line].data.kd_milik=b.data.kd_milik;
								dataSource_viSetupStokMinimum.getRange()[line].data.milik=b.data.milik;
								if(b.data.min_stok != undefined || b.data.nama_obat!=""){
									dataSource_viSetupStokMinimum.getRange()[line].data.min_stok=b.data.min_stok;
								} else{
									dataSource_viSetupStokMinimum.getRange()[line].data.min_stok=0;
								}
								
								GridDataView_viSetupStokMinimum.getView().refresh();	
								
								GridDataView_viSetupStokMinimum.startEditing(line, 3);	
							},
							insert	: function(o){
								return {
									kd_prd			: o.kd_prd,
									nama_obat		: o.nama_obat,
									min_stok		: o.min_stok,
									text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_prd+'</td><td width="250">'+o.nama_obat+'</td></tr></table>',
									milik			: o.milik,
									kd_milik		: o.kd_milik,
								}
							},
							param:function(){
								return {
								
								}
							},
							url		: baseURL + "index.php/apotek/functionSetupStokMinimum/getobat",
							valueField: 'nama_obat',
							displayField: 'text',
							listWidth: 320
						})
					},
					//-------------- ## --------------
					{
						header: 'Min Stok',
						dataIndex: 'min_stok',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
						editor: new Ext.form.NumberField({
							listeners:{
								specialkey:function(){
									if(Ext.EventObject.getKey() ==13){
										var line	=GridDataView_viSetupStokMinimum.getSelectionModel().selection.cell[0]; 
										GridDataView_viSetupStokMinimum.startEditing(line, 4);
									}
								}
							}
						}),
						
					},
					//-------------- ## --------------
					{
						header: 'Kepemilikan',
						dataIndex: 'milik',
						hideable:false,
						menuDisabled: true,
						width: 150,
						editor		: gridcbokepemilikan= new Ext.form.ComboBox({
								id				: 'gridcbokepemilikan',
								typeAhead		: true,
								triggerAction	: 'all',
								lazyRender		: true,
								mode			: 'local',
								emptyText		: '',
								fieldLabel		: ' ',
								align			: 'Right',
								width			: 200,
								store			: dsmilik_SetupStokMinimum,
								valueField		: 'milik',
								displayField	: 'milik',
								listeners		:
								{
									select	: function(a,b,c){
										var line	= GridDataView_viSetupStokMinimum.getSelectionModel().selection.cell[0];
										dataSource_viSetupStokMinimum.getRange()[line].data.kd_milik=b.data.kd_milik;
										
										var records = new Array();
										records.push(new dataSource_viSetupStokMinimum.recordType());
										dataSource_viSetupStokMinimum.add(records);
										
										var line=dataSource_viSetupStokMinimum.getCount()-1;
										GridDataView_viSetupStokMinimum.startEditing(line,2);
									},
									keyUp: function(a,b,c){
										$this1=this;
										if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
											clearTimeout(this.time);

											this.time=setTimeout(function(){
												if($this1.lastQuery != '' ){
													var value=$this1.lastQuery;
													var param={};
													param['text']=$this1.lastQuery;
													loaddatacombomilik_SetupStokMinimum(param);
												}
											},1000);
										}
									}
								}
							}
						)
					},
					//-------------- ## --------------
					{
						header: 'kd_milik',
						dataIndex: 'kd_milik',
						hideable:false,
						hidden: true,
						width: 150
					},
					//-------------- ## --------------
				]
			),
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupStokMinimum = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelPencarianTarif_SetupStokMinimum()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupStokMinimum = new Ext.Panel
    (
		{
			title: NamaForm_viSetupStokMinimum,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupStokMinimum,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupStokMinimum,
					GridDataView_viSetupStokMinimum],
			tbar:
			[
				{
					xtype: 'button',
					text: 'Baru',
					iconCls: 'add',
					id: 'btnAdd_viSetupStokMinimum',
					handler: function()
					{
						Ext.getCmp('btnAddGrid_viSetupStokMinimum').enable();
						Ext.getCmp('btnDeleteGrid_viSetupStokMinimum').enable();
						kodenamaobat.setValue("");
						dataSource_viSetupStokMinimum.removeAll()
						var records = new Array();
						records.push(new dataSource_viSetupStokMinimum.recordType());
						dataSource_viSetupStokMinimum.add(records);
						
						var line=dataSource_viSetupStokMinimum.getCount()-1;
						GridDataView_viSetupStokMinimum.startEditing(line,2);
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Save',
					iconCls: 'save',
					id: 'btnSimpan_viSetupStokMinimum',
					handler: function()
					{
						loadMask.show();
						dataSave_viSetupStokMinimum();
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Delete',
					iconCls: 'remove',
					id: 'btnDelete_viSetupStokMinimum',
					handler: function()
					{
						if(currentKdPrd == "" || currentKdPrd == undefined){
							loadMask.hide();
							ShowPesanErrorSetupStokMinimum("Pilih produk yang akan dihapus!","Error")
						} else{
							loadMask.show();
							dataDelete_viSetupStokMinimum();
						}
						
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Refresh',
					iconCls: 'refresh',
					id: 'btnRefresh_viSetupStokMinimum',
					handler: function()
					{
						kodenamaobat.setValue("");
						dataSource_viSetupStokMinimum.removeAll();
						dataGriSetupStokMinimum();
					}
				},
				{
					xtype: 'tbseparator'
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupStokMinimum;
    //-------------- # End form filter # --------------
}

function PanelPencarianTarif_SetupStokMinimum(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 35,
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 500,
						height: 50,
						anchor: '100% 100%',
						items:
						[
							
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Nama obat'
							},
							{
								x: 100,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							kodenamaobat = new Nci.form.Combobox.autoComplete({
								x: 110,
								y: 0,
								width:250,
								id		: 'txtnamaobat_setupMinimumStok',
								store	: dskodenama,
								select	: function(a,b,c){
									GridDataView_viSetupStokMinimum.getView().refresh();
								},
								onShowList:function(a){
									dataSource_viSetupStokMinimum.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupStokMinimum.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupStokMinimum.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_prd      	: o.kd_prd,
										nama_obat 		: o.nama_obat,
										min_stok		: o.min_stok,
										kd_milik		: o.kd_milik,
										kd_unit_far		: o.kd_unit_far,
										nm_unit_far		: o.nm_unit_far,
										milik			: o.milik,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="150">'+o.milik+'</td></tr></table>'
									}
								},
								param:function(){
									return {
										//kd_unit_far:Ext.getCmp('cbounitfar_SetupStokMinimum').getValue(),
										//kd_milik:Ext.getCmp('cbomilik_SetupStokMinimum').getValue(),
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupStokMinimum/getItemGrid",
								valueField: 'nama_obat',
								displayField: 'text',
								listWidth: 280
							}),
							/* {
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Unit Farmasi'
							},
							{
								x: 100,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboUnitFar_SetupStokMinimum(), */
							/* {
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Kepemilikan'
							},
							{
								x: 100,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboMilik_SetupStokMinimum() */
						]
					}
				]
			} 
							
		]		
	};
        return items;
}

function comboUnitFar_SetupStokMinimum()
{
 var Field = ['kd_unit_far','nm_unit_far'];
    dsunitfar_SetupStokMinimum = new WebApp.DataStore({ fields: Field });
	loaddatacombounitfar_SetupStokMinimum();
	cbounitfar_SetupStokMinimum= new Ext.form.ComboBox
	(
		{
			id				: 'cbounitfar_SetupStokMinimum',
			x				: 110,
			y				: 30,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dsunitfar_SetupStokMinimum,
			valueField		: 'kd_unit_far',
			displayField	: 'nm_unit_far',
			value			: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacombounitfar_SetupStokMinimum(param);
							}
						},1000);
					}
				}
			}
		}
	);return cbounitfar_SetupStokMinimum;
};
/* 
function comboMilik_SetupStokMinimum()
{
 var Field = ['kd_milik','milik'];
    dsmilik_SetupStokMinimum = new WebApp.DataStore({ fields: Field });
	loaddatacombomilik_SetupStokMinimum();
	cbomilik_SetupStokMinimum= new Ext.form.ComboBox
	(
		{
			id				: 'cbomilik_SetupStokMinimum',
			x				: 110,
			y				: 30,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dsmilik_SetupStokMinimum,
			valueField		: 'kd_milik',
			displayField	: 'milik',
			value			: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacombomilik_SetupStokMinimum(param);
							}
						},1000);
					}
				}
			}
		}
	);return cbomilik_SetupStokMinimum;
}; */

function dataGriSetupStokMinimum(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupStokMinimum/getItemGrid",
			params: {
				text:""
			},
			failure: function(o)
			{
				loadMask.hide()
				ShowPesanErrorSetupStokMinimum('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupStokMinimum.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupStokMinimum.add(recs);
					
					
					
					GridDataView_viSetupStokMinimum.getView().refresh();
				}
				else 
				{
					loadMask.hide()
					ShowPesanErrorSetupStokMinimum('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupStokMinimum(){
	if (ValidasiSaveSetupStokMinimum(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupStokMinimum/save",
				params: getParamSaveSetupStokMinimum(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupStokMinimum('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupStokMinimum('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupStokMinimum.removeAll();
						dataGriSetupStokMinimum();
						Ext.getCmp('btnAddGrid_viSetupStokMinimum').disable();
						Ext.getCmp('btnDeleteGrid_viSetupStokMinimum').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupStokMinimum('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupStokMinimum.removeAll();
						dataGriSetupStokMinimum();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupStokMinimum(){
	if(currentstok_min == undefined || currentstok_min==null){
		loadMask.hide();
		ShowPesanErrorSetupStokMinimum('Stok minimum obat ini belum tersedia, penghapusan gagal', 'Error');
	} else{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupStokMinimum/delete",
				params: getParamDeleteSetupStokMinimum(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupStokMinimum('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupStokMinimum('Berhasil menghapus data ini','Information');
						
						dataSource_viSetupStokMinimum.removeAll();
						dataGriSetupStokMinimum();
					}
					else 
					{
						loadMask.hide();
						if(cst.pesan == "tidak ada"){
							ShowPesanErrorSetupStokMinimum('Stok minimum obat ini belum tersedia, penghapusan gagal', 'Error');
						} else{
							ShowPesanErrorSetupStokMinimum('Gagal hapus data ini', 'Error');
						}
						
						dataSource_viSetupStokMinimum.removeAll();
						dataGriSetupStokMinimum();
					};
				}
			}
			
		)

	}
}

function loaddatacombomilik_SetupStokMinimum(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupStokMinimum/getComboMilik",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			dsmilik_SetupStokMinimum.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsmilik_SetupStokMinimum.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsmilik_SetupStokMinimum.add(recs);
				//console.log(o);
			}
		}
	});
}
/* 
function loaddatacombounitfar_SetupStokMinimum(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupStokMinimum/getComboUnitFar",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbokepemilikan.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsunitfar_SetupStokMinimum.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsunitfar_SetupStokMinimum.add(recs);
				//console.log(o);
			}
		}
	});
}
 */

function getParamSaveSetupStokMinimum(){
	var kd_unit_far;
	var kd_milik;
	var	params =
	{
		
	}
	params['jml']=dataSource_viSetupStokMinimum.getCount();
	for(var i = 0 ; i < dataSource_viSetupStokMinimum.getCount();i++)
	{
		params['kd_prd-'+i]=dataSource_viSetupStokMinimum.data.items[i].data.kd_prd;
		params['kd_milik-'+i]=dataSource_viSetupStokMinimum.data.items[i].data.kd_milik
		params['min_stok-'+i]=dataSource_viSetupStokMinimum.data.items[i].data.min_stok
			
	}
   
    return params
};

function getParamDeleteSetupStokMinimum(){
	var	params =
	{
		kd_prd:currentKdPrd,
		kd_milik:currentkd_milik
		
	}
    return params
};

function ValidasiSaveSetupStokMinimum(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupStokMinimum.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupStokMinimum('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupStokMinimum.getCount() ; i++){
			var o=dataSource_viSetupStokMinimum.getRange()[i].data;
			if(o.min_stok == undefined || o.min_stok == ''){
				loadMask.hide();	
				ShowPesanWarningSetupStokMinimum('Stok minimum tidak boleh kosong!', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningSetupStokMinimum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupStokMinimum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupStokMinimum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};