// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekResepRWJ.js
*	Menu 			: APOTEK
*	Model id 		: 
*	Keterangan 		: Resep RWJ dan IGD
*	Di buat tanggal : 04 Juni 2015
*	Oleh 			: M
*/

// Deklarasi Variabel pada Apotek Perencanaan # --------------
function TRApotekResepRWJ(){
var cbopasienorder_mng_apotek;
var dspasienorder_mng_apotek;
var AptResepRWJ={};
AptResepRWJ.form={};
AptResepRWJ.func={};
AptResepRWJ.vars={};
AptResepRWJ.func.parent=AptResepRWJ;
AptResepRWJ.form.DataStore={};
AptResepRWJ.form.ComboBox={};
AptResepRWJ.form.ArrayStore={};
AptResepRWJ.form.Record={};
AptResepRWJ.form.Form={};
AptResepRWJ.form.Grid={};
AptResepRWJ.form.Panel={};
AptResepRWJ.form.TextField={};
AptResepRWJ.form.Button={};
var dsDataGrdJab_viApotekResepRWJ= new Ext.data.ArrayStore({
		id: 0,
        fields: ['kd_prd','kd_satuan','nama_obat','jml','disc','kd_satuan','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik','dosis','jasa','no_out','no_urut'],
		data: []
   });
	
	


AptResepRWJ.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik'],
	data: []
});

AptResepRWJ.form.ArrayStore.namapasien	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir'],
	data: []
});
AptResepRWJ.form.ArrayStore.kodepasien	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama', 'alamat', 'nama_keluarga', 'nama_unit', 'kd_unit','no_transaksi','tgl_transaksi', 'kd_unit', 'kd_dokter', 'kd_customer','kd_kasir'],
	data: []
});

var dataSource_viApotekResepRWJ;
var selectCount_viApotekResepRWJ=50;
var NamaForm_viApotekResepRWJ="Resep Rawat Jalan ";
var selectCountStatusPostingApotekResepRWJ='Semua';
var mod_name_viApotekResepRWJ="viApotekResepRWJ";
var now_viApotekResepRWJ= new Date();
var addNew_viApotekResepRWJ;
var rowSelected_viApotekResepRWJ;
var setLookUp_bayarResepRWJ;
var setLookUps_viApotekResepRWJ;
var mNoKunjungan_viApotekResepRWJ='';
var selectSetUnit;
var selectSetUnitLookup;
var selectSetPilihankelompokPasien;
var selectSetDokter;
var tanggal = now_viApotekResepRWJ.format("d/M/Y");
var jam = now_viApotekResepRWJ.format("H/i/s");
var cellSelecteddeskripsiRWJ;
var tampungshiftsekarang;
var tmpNoOut=0;
var tmpTglOut='';
var tmpkriteria;
var kd_pasien_obbt;
var kd_unit_obbt;
var tgl_masuk_obbt;
var urut_masuk_obbt;
var gridDTLTRHistoryApotekRWJ;
var kd='';

var CurrentHistoryRWJ =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekResepRWJ =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekResepRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Apotek Perencanaan # --------------

// Start Project Apotek Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viApotekResepRWJ(mod_id_viApotekResepRWJ)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekResepRWJ = 
	[
		'STATUS_POSTING','NO_RESEP','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','JASA', 'ADMPRHS','ADMRESEP','CUSTOMER','JENIS_PASIEN'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSource_viApotekResepRWJ = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekResepRWJ
    });
    refreshRespApotekRWJ();
	total_pasien_order_mng_obtrwj();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekResepRWJ = new Ext.grid.EditorGridPanel
    (
		{
			/* xtype: 'editorgrid',
			title: '', */
			store: dataSource_viApotekResepRWJ,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekResepRWJ = undefined;
							rowSelected_viApotekResepRWJ = dataSource_viApotekResepRWJ.getAt(row);
							CurrentData_viApotekResepRWJ
							CurrentData_viApotekResepRWJ.row = row;
							CurrentData_viApotekResepRWJ.data = rowSelected_viApotekResepRWJ.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					Ext.Ajax.request(
						{
							   
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							 params: {
								
								command: '0'
							
							},
							failure: function(o)
							{
								 var cst = Ext.decode(o.responseText);
								
							},	    
							success: function(o) {
							var cst = Ext.decode(o.responseText);
				
							tampungshiftsekarang=cst.shift
							}
					
					});
					rowSelected_viApotekResepRWJ = dataSource_viApotekResepRWJ.getAt(ridx);
					if (rowSelected_viApotekResepRWJ != undefined)
					{
						
						setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
					}
					else
					{
						setLookUp_viApotekResepRWJ();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						align		: 'center',
						dataIndex	: 'STATUS_POSTING',
						id			: 'colStatusPosting_viApotekResepRWJ',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoMedrec_viApotekResepRWJ',
						header: 'No. Resep',
						dataIndex: 'NO_RESEP',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						id: 'colTgl_viApotekResepRWJ',
						header:'Tgl Resep',
						dataIndex: 'TGL_OUT',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						// format: 'd/M/Y',
						//filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OUT);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekResepRWJ',
						header: 'No Medrec',
						dataIndex: 'KD_PASIENAPT',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colNamaPasien_viApotekResepRWJ',
						header: 'Nama',
						dataIndex: 'NMPASIEN',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colPoliklinik_viApotekResepRWJ',
						header: 'Poliklinik',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						id: 'colNoout_viApotekResepRWJ',
						header: 'No Out',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekResepRWJ',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Tambah Data',
						id: 'btnTambah_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}
							});
							cekPeriodeBulan();
							
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekResepRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request({
									   
									url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
									 params: {
										
										command: '0'
									
									},
									failure: function(o)
									{
										 var cst = Ext.decode(o.responseText);
										
									},	    
									success: function(o) {
										var cst = Ext.decode(o.responseText);
							
										tampungshiftsekarang=cst.shift
									}
							
							})
							
							if (rowSelected_viApotekResepRWJ != undefined)
							{
								setLookUp_viApotekResepRWJ(rowSelected_viApotekResepRWJ.data);
							}
						}
				},{xtype: 'tbspacer',height: 3, width:580},
				{
				xtype: 'label',
				text: 'Order Obat dari poli : ' 
				},{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					fieldLabel: 'No. Medrec',
					name: 'txtcounttr_apt_rwj',
					id: 'txtcounttr_apt_rwj',
					width: 50,
					disabled:true,
					listeners: 
					{ 
						
					}
				}
					
					//-------------- ## --------------
					
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viApotekResepRWJ, selectCount_viApotekResepRWJ, dataSource_viApotekResepRWJ),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianApotekResepRWJ = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekResepRWJ',
							name: 'TxtFilterGridDataView_RoNumber_viApotekResepRWJ',
							emptyText: 'No. Resep',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Kode/Nama Pasien'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtKdNamaPasien',
							name: 'txtKdNamaPasien',
							emptyText: 'Kode/Nama Pasien',
							width: 160,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Poliklinik'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						ComboUnitApotekResepRWJ(),	
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Resep'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalApotekResepRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirApotekResepRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viApotekResepRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWJ();
										refreshRespApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekResepRWJ(),
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viApotekResepRWJ',
							handler: function() 
							{					
								/* DfltFilterBtn_viApotekResepRWJ = 1;
								DataRefresh_viApotekResepRWJ(getCriteriaFilterGridDataView_viApotekResepRWJ()); */
								tmpkriteria = getCriteriaCariApotekResepRWJ();
								refreshRespApotekRWJ(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]			
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekResepRWJ = new Ext.Panel
    (
		{
			title: NamaForm_viApotekResepRWJ,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekResepRWJ,
			region: 'center',
			layout: 'form', 
			closable: true,   
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianApotekResepRWJ,
					GridDataView_viApotekResepRWJ],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekResepRWJ,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
	
    return FrmFilterGridDataView_viApotekResepRWJ;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viApotekResepRWJ # --------------

function refreshRespApotekRWJ(kriteria)
{
    dataSource_viApotekResepRWJ.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewResepApotekRWJ',
                    param : kriteria 
                }			
            }
        );   
    return dataSource_viApotekResepRWJ;
}

/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/
function total_pasien_order_mng_obtrwj()
{
Ext.Ajax.request(
{
url: baseURL + "index.php/apotek/functionAPOTEK/countpasienmr_resep",
 params: {
	command: '0',
	// parameter untuk url yang dituju (fungsi didalam controller)
},
failure: function(o)
{
	 var cst = Ext.decode(o.responseText);
	
},	    
success: function(o) {
	 var cst = Ext.decode(o.responseText);
	
	 Ext.getCmp('txtcounttr_apt_rwj').setValue(cst.countpas);
	 console.log(cst);
	
}
});
};
function setLookUp_viApotekResepRWJ(rowdata){
    var lebar = 885;
    setLookUps_viApotekResepRWJ = new Ext.Window({
        id: 'SetLookUps_viApotekResepRWJ',
		name: 'SetLookUps_viApotekResepRWJ',
        title: NamaForm_viApotekResepRWJ, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekResepRWJ(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				Ext.Ajax.request({
									   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							AptResepRWJ.form.Panel.shift.update(cst.shift);
						}
					
					});
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
                //datarefresh_viApotekResepRWJ();
				mNoKunjungan_viApotekResepRWJ = '';
            }
        }
    });
	//dsDataGrdJab_viApotekResepRWJ= new WebApp.DataStore({
        //fields: ['kd_prd','kd_satuan','nama_obat','jml','disc','kd_satuan','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik','dosis']
    //});
	dsDataGrdJab_viApotekResepRWJ.loadData([],false);
    setLookUps_viApotekResepRWJ.show();
	
	ViewDetailPembayaranObat(rowdata.NO_OUT,rowdata.TGL_OUT);
	
    if (rowdata == undefined){
		Ext.getCmp('btnAddObat').disable();
    }else{
        datainit_viApotekResepRWJ(rowdata);
    }
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

/**
*	Function : getFormItemEntry_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viApotekResepRWJ(lebar,rowdata)
{
    var pnlFormDataBasic_viApotekResepRWJ = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			listeners: {
				afterShow: function()
				{
					Ext.Ajax.request({
									   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							AptResepRWJ.form.Panel.shift.update(cst.shift);
						}
					
					});
				}
			},
			items:
			[
				getItemPanelInputBiodata_viApotekResepRWJ(lebar),
				//-------------- ## -------------- 	
				getItemGridTransaksi_viApotekResepRWJ(lebar),
				//-------------- ## --------------
				getItemGridHistoryBayar_viApotekResepRWJ(lebar),
				
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viApotekResepRWJ',
					id: 'compChkPosted_viApotekResepRWJ',
					items: 
					[
						/* {
							columnWidth	: .033,
							layout		: 'form',
							style		: {'margin-top':'-1px'},
							anchor		: '100% 8.0001%',
							border		: false,
							fieldLabel  : 'Transfered',
							html		: ''
						}, */
						AptResepRWJ.form.Panel.a=new Ext.Panel ({
						    region: 'north',
						    border: false,
						    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
						}),
						{
							columnWidth	: .08,
							layout		: 'form',
							anchor		: '100% 8.0001%',
							style		: {'margin-top':'1px'},
							border		: false,
							html		: " Status Posting"
						},
						//-------------------##------------------------
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Tuslah :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'278px'}							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtTuslahEditData_viApotekResepRWJ',
		                    name: 'txtTuslahEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'278px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 80,								
							value: 'Adm Racik:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'258px'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmRacikEditData_viApotekResepRWJ',
		                    name: 'txtAdmRacikEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'258px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },						
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Total:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'288px'}
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridJmlEditData_viApotekResepRWJ',
		                    name: 'txtDRGridJmlEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'288px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }											
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkUpdateHB_viApotekResepRWJ',
					id: 'compChkUpdateHB_viApotekResepRWJ',
					items: 
					[
						{
							xtype: 'displayfield',				
							width: 55,								
							value: 'Tanggal :',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 100,								
							value: tanggal,
							format:'d/M/Y',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Adm :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'220px'}					
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmEditData_viApotekResepRWJ',
		                    name: 'txtAdmEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'220px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Disc :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'230px'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtDiscEditData_viApotekResepRWJ',
		                    name: 'txtDiscEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'230px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },	
						{
							xtype: 'displayfield',				
							width: 90,								
							value: 'Grand Total :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'220px','font-weight':'bold'}
							
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtTotalEditData_viApotekResepRWJ',
		                    name: 'txtTotalEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'220px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }	
						/* {
							xtype:'button',
							text:'Acc. Approval',
							width:70,
							//style:{'margin-left':'190px','margin-top':'7px'},
							style:{'text-align':'right','margin-left':'380px'},
							hideLabel:true,
							id: 'btnAccApp_viApotekResepRWJ',
							handler:function()
							{
							}   
						}, */
						
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkHB_viApotekResepRWJ',
					id: 'compChkHB_viApotekResepRWJ',
					items: 
					[
						
						{
							xtype: 'displayfield',				
							width: 80,								
							value: 'Current Shift :',
							fieldLabel: 'Label',
							style:{'text-align':'left'}						
						},
						AptResepRWJ.form.Panel.shift=new Ext.Panel ({
							region: 'north',
							border: false
						}),							
						{
							xtype: 'displayfield',				
							width: 95,								
							value: 'Adm Perusahaan:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'355px'}						
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtAdmPerusahaanEditData_viApotekResepRWJ',
		                    name: 'txtAdmPerusahaanEditData_viApotekResepRWJ',
							style:{'text-align':'right','margin-left':'455px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }
					]
				}
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						hidden:true,
						id: 'btnAdd_viApotekResepRWJ',
						handler: function(){
							dataaddnew_viApotekResepRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viApotekResepRWJ',
						handler: function()
						{
							datasave_viApotekResepRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viApotekResepRWJ',
						handler: function()
						{
							/* var x = datasave_viApotekResepRWJ(addNew_viApotekResepRWJ);
							refreshRespApotekRWJ();
							if (x===undefined)
							{
								setLookUps_viApotekResepRWJ.close();
							} */
							datasave_viApotekResepRWJ();
							refreshRespApotekRWJ();
							setLookUps_viApotekResepRWJ.close();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Paid',
						id:'btnbayar_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_bayarResepRWJ();
						}
					},
					{
						xtype:'tbseparator'
					},
					
					{
						xtype: 'button',
						text: 'Unposting',
						id:'btnunposting_viApotekResepRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler:function()
						{
							Ext.Ajax.request
							(
								{
									url: baseURL + "index.php/apotek/functionAPOTEK/cekBulan",
									params: {a:"no_out"},
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
												if (button == 'yes'){
													Ext.Ajax.request
													(
														{
															url: baseURL + "index.php/apotek/functionAPOTEK/unpostingResepRWJ",
															params: getParamUnpostingResepRWJ(),
															failure: function(o)
															{
																ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
															},	
															success: function(o) 
															{
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) 
																{
																	ShowPesanInfoResepRWJ('UnPosting Berhasil','Information');
																	Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
																	Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
																	Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
																	Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
																	Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
																	Ext.getCmp('btnPrint_viResepRWJ').disable();
																	Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
																	Ext.getCmp('btnAddObat').enable();
																	Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').enable();
																	Ext.getCmp('txtTmpStatusPost').setValue('0');
																	AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
																	refreshRespApotekRWJ();
																}
																else 
																{
																	ShowPesanErrorResepRWJ('Gagal melakukan unPosting', 'Error');
																};
															}
														}
														
													)
												}
											});
										} else{
											if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
												ShowPesanErrorResepRWJ('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
											} else{
												ShowPesanErrorResepRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
											}
											
										}
									}
								}
							);
						}  
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Paid',
						id:'btnDeleteHistory_viApotekResepRWJ',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayarList.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan dihapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionAPOTEK/deleteHistoryResepRWJ",
												params: getParamDeleteHistoryResepRWJ(),
												failure: function(o)
												{
													ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoResepRWJ('Penghapusan berhasil','Information');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
														gridDTLTRHistoryApotekRWJ.getView().refresh();
														Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
														Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
													}
													else 
													{
														ShowPesanErrorResepRWJ('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
														gridDTLTRHistoryApotekRWJ.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else{
								ShowPesanErrorResepRWJ('Belum melakukan pembayaran','Error');
							}
						}  
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viResepRWJ',
						disabled:true,
						handler:function()
						{															
							
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillResepRWJ',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					},
					{
					xtype: 'label',
					text: 'Order Obat dari Poli : ' 
					},
					
					
					mComboorder(),
					{
						xtype: 'button',
						text: 'close order',
						id:'statusservice_apt',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							updatestatus_permintaan()
							load_data_pasienorder();
							AptResepRWJ.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt').disable();
						}
					}
				]
			}
		}
    )

    return pnlFormDataBasic_viApotekResepRWJ;
}
// End Function getFormItemEntry_viApotekResepRWJ # --------------

/**
*	Function : getItemPanelInputBiodata_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viApotekResepRWJ(lebar) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWJ_viApotekResepRWJ',
						id: 'txtNoResepRWJ_viApotekResepRWJ',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					
					{
						xtype: 'displayfield',
						flex: 1,
						width: 100,
						name: '',
						value: 'Pasien :',
						fieldLabel: 'Label'
					},	
					AptResepRWJ.form.ComboBox.kodePasien = new Nci.form.Combobox.autoComplete({
						store	: AptResepRWJ.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.kd_customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('btnAddObat').enable();
							
						},
						width	: 80,
						insert	: function(o){
							return {
								kd_pasien       : o.kd_pasien,
								nama			: o.nama,
								kd_unit			: o.kd_unit,
								kd_dokter		: o.kd_dokter,
								kd_customer		: o.kd_customer,
								kd_kasir		: o.kd_kasir,
								no_transaksi	: o.no_transaksi,
								text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEK/getKodePasienResepRWJ",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 280,
						emptyText: 'Kode Pasien'
					}),	
					AptResepRWJ.form.ComboBox.namaPasien = new Nci.form.Combobox.autoComplete({
						store	: AptResepRWJ.form.ArrayStore.namapasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							AptResepRWJ.form.ComboBox.kodePasien.setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.kd_customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObat').enable();
							
						},
						width	: 200,
						insert	: function(o){
							return {
								kd_pasien       : o.kd_pasien,
								nama			: o.nama,
								kd_unit			: o.kd_unit,
								kd_dokter		: o.kd_dokter,
								kd_customer		: o.kd_customer,
								kd_kasir		: o.kd_kasir,
								no_transaksi	: o.no_transaksi,
								text			:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEK/getPasienResepRWJ",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'Nama Pasien'
					})	
                    //-------------- ## --------------  
					
					
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Poliklinik ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					ComboUnitApotekResepRWJLookup(),
					
					//-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 100,
						name: '',
						value: 'Jenis Pasien :',
						fieldLabel: 'Label'
					},
					ComboPilihanKelompokPasienApotekResepRWJ()
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					ComboDokterApotekResepRWJ(),
					{
						xtype: 'displayfield',
						flex: 1,
						width: 5,
						name: '',
						value: '',
						fieldLabel: 'Label'
					},
					
					{
						xtype: 'displayfield',
						flex: 1,
						width: 60,
						name: '',
						value: '',
						fieldLabel: 'Label'
					},	
					
					{
						xtype: 'checkbox',
						boxLabel: 'Non Resep',
						id: 'cbNonResep',
						name: 'cbNonResep',
						width: 80,
						checked: false,
						handler:function(a,b) 
						{
							if(a.checked==true){
								Ext.getCmp('txtNamaPasienNon').enable();
								Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
								Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
								Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
								Ext.getCmp('btnAddObat').enable();
								Ext.getCmp('txtTmpKdCustomer').setValue('0000000001')
								AptResepRWJ.form.ComboBox.kodePasien.disable();
								AptResepRWJ.form.ComboBox.namaPasien.disable();
							}else{
								Ext.getCmp('txtNamaPasienNon').disable();
								AptResepRWJ.form.ComboBox.kodePasien.enable();
								AptResepRWJ.form.ComboBox.namaPasien.enable();
							}
						}
					},
					
					{
						xtype: 'textfield',
						width : 200,	
						name: 'txtNamaPasienNon',
						id: 'txtNamaPasienNon',
						emptyText: 'Nama Pasien Non Resep',
						displayField:'Nama Pasien',
						disabled:true
					},					
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						id: 'btn1/2resep_viApotekResepRWJ',
						handler:function()
						{
						}   
					},
					//--------HIDDEN---------------
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdUnit',
						id: 'txtTmpKdUnit',
						emptyText: 'kode unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdDokter',
						id: 'txtTmpKdDokter',
						emptyText: 'kode dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdCustomer',
						id: 'txtTmpKdCustomer',
						emptyText: 'kode customer',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpJmlItem',
						id: 'txtTmpJmlItem',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpNoout',
						id: 'txtTmpNoout',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTglout',
						id: 'txtTmpTglout',
						emptyText: 'tgl out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPost',
						id: 'txtTmpStatusPost',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpSisaAngsuran',
						id: 'txtTmpSisaAngsuran',
						emptyText: 'sisa angsuran',
						hidden:true
					}
				]
			}
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viApotekResepRWJ # --------------


/**
*	Function : getItemGridTransaksi_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viApotekResepRWJ(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 235,//255,//300, 
		tbar:
		[
			{
				text	: 'Add Product',
				id		: 'btnAddObat',
				tooltip	: nmLookup,
				disabled:true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
					records.push(new dsDataGrdJab_viApotekResepRWJ.recordType());
					dsDataGrdJab_viApotekResepRWJ.add(records);
					var kd_customer=Ext.getCmp('txtTmpKdCustomer').getValue();
					if(kd_customer ==='' ||kd_customer ==='Kelompok Pasien'){
						
					}else{
						getAdm(kd_customer);
					    }
					
				}
			},
			//-------------- ## --------------
			{
				xtype:'tbseparator'
			},
			//-------------- ## --------------
			{
				xtype: 'button',
				text: 'Delete',
				disabled:true,
				iconCls: 'remove',
				id: 'btnDelete_viApotekResepRWJ',
				handler: function()
				{
					var line = AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWJ.getRange()[line].data;
					if(dsDataGrdJab_viApotekResepRWJ.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEK/hapusBarisGridResepRWJ",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viApotekResepRWJ.removeAt(line);
													AptResepRWJ.form.Grid.a.getView().refresh();
													hasilJumlah();
													Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
													Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
													
												}
												else 
												{
													ShowPesanErrorResepRWJ('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viApotekResepRWJ.removeAt(line);
									AptResepRWJ.form.Grid.a.getView().refresh();
									// Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
									// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorResepRWJ('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
				}
			}	
		],
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewEdit_viApotekResepRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viApotekResepRWJ # --------------

/**
*	Function : gridDataViewEdit_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viApotekResepRWJ(){
    chkSelected_viApotekResepRWJ = new Ext.grid.CheckColumn({
		id: 'chkSelected_viApotekResepRWJ',
		header: 'C',
		align: 'center',						
		dataIndex: 'cito',			
		width: 20
	});
	
    AptResepRWJ.form.Grid.a = new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekResepRWJ,
        height: 210,//220,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){

                }
            }
        }),
        stripeRows: true,
			//-------------- ## --------------
		cm: new Ext.grid.ColumnModel([
			
			new Ext.grid.RowNumberer(),	
			//-------------- ## --------------
			chkSelected_viApotekResepRWJ,
			//-------------- ## --------------
			{			
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_obat',
				//id			: Nci.getId(),
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: AptResepRWJ.form.ArrayStore.a,
					select	: function(a,b,c){
						//'harga_beli','kd_pabrik','markup','tuslah','adm_racik'
						var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_prd=b.data.kd_prd;
						//dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_jual=b.data.harga_jual;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.markup=b.data.markup;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.adm_racik=b.data.adm_racik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jasa=b.data.jasa;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_out=b.data.no_out;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.no_urut=b.data.no_urut;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.tgl_out=b.data.tgl_out;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=0;
						dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.racik=0;
						AptResepRWJ.form.Grid.a.getView().refresh();
						
						//Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(b.data.adm_racik);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							kd_satuan		: o.kd_satuan,
							fractions		: o.fractions,
							harga_jual		: o.harga_jual,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							tuslah			: o.tuslah,
							adm_racik		: o.adm_racik,
							markup			: o.markup,
							jasa			: o.jasa,
							no_out			: o.no_out,
							no_urut			: o.no_urut,
							tgl_out			: o.tgl_out,
							kd_milik		: o.kd_milik,
							jml_stok_apt	: o.jml_stok_apt,
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="80" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
							}
					},
					url		: baseURL + "index.php/apotek/functionAPOTEK/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 350
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				width: 60
					
			},
			//-------------- ## --------------
			{
				dataIndex: 'racik',
				header: 'Racikan',
				width: 70,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.racik=a.getValue();
							hasilJumlah();
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'harga_jual',
				header: 'Harga Sat',
				xtype:'numbercolumn',
				sortable: true,
				align:'right',
				width: 85/* ,
				renderer: 
				function(v, params, record) {
						return parseInt(record.data.harga_jual);
				} */
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml',
				header: 'Qty',
				sortable: true,
				width: 45,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()==''){
								ShowPesanWarningResepRWJ('Qty obat belum di isi', 'Warning');
							}else{
								dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.jml=a.getValue();
								hasilJumlah();
							}
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'disc',
				header: 'Diskon',
				sortable: true,
				xtype:'numbercolumn',
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWJ.getRange()[line].data.disc=a.getValue();
							hasilJumlah();
						},
						focus: function(a){
							this.index=AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'jumlah',
				header: 'Sub total',
				sortable: true,
				xtype:'numbercolumn',
				width: 110,
				align:'right'
			},
			//-------------- ## --------------
			{
				dataIndex: 'dosis',
				header: 'Dosis',
				width: 150,
				editor: new Ext.form.TextField({
					allowBlank: false
				})
			},
			//-------------- ## --------------
			//-------------- HIDDEN --------------
			{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'Kode Pabrik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'markup',
				header: 'Markup',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'adm_racik',
				header: 'Adm Racik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jasa',
				header: 'Jasa Tuslah',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_milik',
				header: 'kd milik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'stok tersedia',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
        plugins:chkSelected_viApotekResepRWJ,
		viewConfig:{
			forceFit: true
		}
    });    
    return AptResepRWJ.form.Grid.a;
}
// End Function gridDataViewEdit_viApotekResepRWJ # --------------

function hasilJumlah(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		console.log(o);
		if(o.jml != undefined){
			if(o.jml <= o.jml_stok_apt){
			jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
		
			if(o.racik){
				console.log(admRacik);
				if(isNaN(admRacik)){
					admRacik=0;
				} else {
					admRacik += parseFloat(o.adm_racik) * parseFloat(o.racik);
					console.log(o.adm_racik+' loih');
				}
			}
			
			totdisc += parseFloat(o.disc);
			o.jumlah =jumlahGrid;
			total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=parseInt(o.jml);
		}
		

	}
	
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	//alert(toFormat(total));
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(toFormat(total));
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(toFormat(totdisc));
	admprs=total*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=total + admRacik + parseInt(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
	console.log(admprs);
	console.log(admRacik);
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	AptResepRWJ.form.Grid.a.getView().refresh();
}

function hasilJumlahLoad(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.jml != undefined){
			if(o.jml <= o.jml_stok_apt){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=parseInt(o.jml);
		}
		
	}
	admRacik=parseFloat(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue())// * parseFloat(o.racik);
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	//alert(totqty);
	Ext.get('txtDRGridJmlEditData_viApotekResepRWJ').dom.value=toFormat(total);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admRacik));
	Ext.get('txtDiscEditData_viApotekResepRWJ').dom.value=toFormat(totdisc);
	admprs=total*parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue());
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=total + admRacik + parseInt(Ext.get('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.get('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=toFormat(totalall);
	console.log(admprs);
	console.log(admRacik);
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	AptResepRWJ.form.Grid.a.getView().refresh();
}



/**
*	Function : getItemGridHistoryBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form Histori pembayaran
*/
function getItemGridHistoryBayar_viApotekResepRWJ(lebar) 
{
    var items =
	{
		title: 'Paid History', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		autoScroll:true,
		height: 100,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewHistoryBayar_viApotekResepRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridHistoryBayar_viApotekResepRWJ # --------------

/**
*	Function : gridDataViewHistoryBayar_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan isian histori pembayaran
*/
function gridDataViewHistoryBayar_viApotekResepRWJ() 
{

    var fldDetail = ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
	
    dsTRDetailHistoryBayarList = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLTRHistoryApotekRWJ = new Ext.grid.EditorGridPanel
    (
        {
			//title: 'History Bayar',
            store: dsTRDetailHistoryBayarList,
            border: false,
            columnLines: true,
           //frame: false,
			//anchor: '100% 25%',
			height: 72,
			
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            /* cellSelecteddeskripsiRWJ = dsTRDetailHistoryBayarList.getAt(row);
                            CurrentHistoryRWJ.row = row;
                            CurrentHistoryRWJ.data = cellSelecteddeskripsiRWJ; */
                        }
                    }
                }
            ),
			stripeRows: true,
			autoScroll:true,
			columns: /* new Ext.grid.ColumnModel
				( */
					[
					   //new Ext.grid.RowNumberer(),
						{
							id: 'colStatPost',
							header: 'Status Posting',
							dataIndex: 'TUTUP',
							width:100,
							hidden:true
						},
						{
							id: 'colKdPsien',
							header: 'Kode Pasien',
							dataIndex: 'KD_PASIENAPT',
							width:100,
							hidden:true
						},
						{
							id: 'colNoOut',
							header: 'No out',
							dataIndex: 'NO_OUT',
							width:100,
							hidden:true
						},
						{
							id: 'coleurutmasuk',
							header: 'Urut Bayar',
							dataIndex: 'URUT',
							align :'center',
							width:90
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Resep',
							dataIndex: 'TGL_OUT',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_OUT);

							} 
						},
						{
							id: 'colePembayaran',
							header: 'Pembayaran',
							dataIndex: 'URAIAN',
							align :'center',
							width:100,
							hidden:false
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Bayar',
							dataIndex: 'TGL_BAYAR',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_BAYAR);

							} 
						},
						{
							id: 'colJumlah',
							header: 'Total Bayar',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JUMLAH',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JUMLAH);

							}
							
						},
						{
							id: 'colJumlah',
							header: 'Jumlah Angsuran',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JML_TERIMA_UANG',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JML_TERIMA_UANG);

							}
							
						},
						{
							id: 'colJumlah',
							header: 'Sisa',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'SISA',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.SISA);

							}
							
						}
					],
					viewConfig: {forceFit: true}
	 
		}
    );
    return gridDTLTRHistoryApotekRWJ;
};

function TRHistoryColumModelApotekRWJ() 
{
	//'TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','JUMLAH','JML_TERIMA_UANG'
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colStatPost',
                header: 'Status Posting',
                dataIndex: 'TUTUP',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'colKdPsien',
                header: 'Kode Pasien',
                dataIndex: 'KD_PASIENAPT',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'colNoOut',
                header: 'No out',
                dataIndex: 'NO_OUT',
                width:100,
				menuDisabled:true,
                hidden:true
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT'
                
            },
			{
                id: 'colTGlout',
                header: 'Tanggal Resep',
                dataIndex: 'TGL_OUT',
				menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                } 
            },
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'URAIAN',
				width:150,
				hidden:false
                
            },
			{
                id: 'colTGlout',
                header: 'Tanggal Bayar',
                dataIndex: 'TGL_BAYAR',
				menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                } 
            },
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'JUMLAH',
				hidden:false/* ,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.JUMLAH);

                } */
                
            },
			{
                id: 'colJumlah',
                header: 'Jumlah Angsuran',
				width:150,
				align :'right',
                dataIndex: 'JML_TERIMA_UANG',
				hidden:false/* ,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.JML_TERIMA_UANG);

                } */
                
            }/* ,
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME' 
            } */
			

        ]
    )
};
//----------------------------End GetDTLTRHistoryGrid------------------ 




function mComboStatusPostingApotekResepRWJ()
{
  var cboStatusPostingApotekResepRWJ = new Ext.form.ComboBox
	(
		{
			id:'cboStatusPostingApotekResepRWJ',
			x: 410,
			y: 30,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 110,
			editable: false,
			emptyText:'',
			fieldLabel: 'JENIS',
			tabIndex:5,
			store: new Ext.data.ArrayStore
			(
					{
							id: 0,
							fields:
							[
								'Id',
								'displayText'
							],
					data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
					}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusPostingApotekResepRWJ,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusPostingApotekResepRWJ=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWJ();
					refreshRespApotekRWJ(tmpkriteria);

				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekResepRWJ();
						refreshRespApotekRWJ(tmpkriteria);
					} 						
				}
			}
		}
	);
	return cboStatusPostingApotekResepRWJ;
};

function ComboUnitApotekResepRWJ()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='2' ORDER BY nama_unit"
				}
		}
	);
	
    var cbo_Unit = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_Unit',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:160,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekResepRWJ();
						refreshRespApotekRWJ(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_Unit;
}

function ComboUnitApotekResepRWJLookup()
{
    var cbo_UnitResepRWJLookup = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_UnitResepRWJLookup',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			readOnly:true,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnitLookup=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_UnitResepRWJLookup;
}

function ComboPilihanKelompokPasienApotekResepRWJ()
{
	var Field_Customer = ['KD_CUSTOMER', 'JENIS_PASIEN'];
	ds_kelpas = new WebApp.DataStore({fields: Field_Customer});
    ds_kelpas.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboKelPasApotek',
					param: ''
				}
		}
	);
    var cboPilihankelompokPasienAptResepRWJ = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
			id:'cboPilihankelompokPasienAptResepRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			readOnly:true,
			emptyText:'Kelompok Pasien',
			width: 285,
			store: ds_kelpas,
			valueField: 'KD_CUSTOMER',
			displayField: 'JENIS_PASIEN',
			value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return cboPilihankelompokPasienAptResepRWJ;
};

function ComboDokterApotekResepRWJ()
{
    var Field_Dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_Dokter});
    ds_dokter.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_dokter',
					Sortdir: 'ASC',
					target: 'ComboDokterApotek',
					param: ''
				}
		}
	);
	
    var cbo_DokterApotekResepRWJ = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_DokterApotekResepRWJ',
			valueField: 'KD_DOKTER',
            displayField: 'NAMA',
			emptyText:'Dokter',
			store: ds_dokter,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			readOnly:true,
			width:180,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetDokter=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_DokterApotekResepRWJ;
};

function mComboJenisByrResepRWJ() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrResepRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
					Ext.getCmp('cboPembayaran').enable();
					/* 
					loaddatastorePembayaran(b.data.JENIS_PAY);
					tampungtypedata=b.data.TYPE_DATA;
					jenispay=b.data.JENIS_PAY;
					showCols(Ext.getCmp('gridDTLTRKasirrwj'));
					hideCols(Ext.getCmp('gridDTLTRKasirrwj'));
					getTotalDetailProduk();
					Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';

					*/
			   
				}
				  

			}
		}
	);
	
    return cboJenisByr;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			disabled:true,
			width:225,
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				}
				  

			}
		}
	);

    return cboPembayaran;
};
function ViewDetailPembayaranObat(no_out,tgl_out) 
{	
    var strKriteriaRWJ='';
    strKriteriaRWJ = "a.no_out = " + no_out + "" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayarList.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWJ
		}
	}); 
	return dsTRDetailHistoryBayarList;
	
    
};

/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_bayarResepRWJ(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_bayarResepRWJ = new Ext.Window
    (
    {
        id: 'setLookUpApotek_bayarResepRWJ',
		name: 'setLookUpApotek_bayarResepRWJ',
        title: 'Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 230,
        resizable:false,
		emptyText:'Pilih Jenis Pembayaran...',
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ getItemPanelBiodataPembayaran_viApotekResepRWJ(lebar,rowdata),
				 getItemPanelBiodataUang_viApotekResepRWJ(lebar,rowdata)
			   ],//1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWJ=undefined;
            }
        }
    }
    );

    setLookUpApotek_bayarResepRWJ.show();
	if(Ext.getCmp('cbNonResep').getValue === true){
		Ext.getCmp('txtNamaPasien_Pembayaran').setValue(Ext.get('txtNamaPasienNon').getValue());
	} else {
		Ext.getCmp('txtNamaPasien_Pembayaran').setValue(AptResepRWJ.form.ComboBox.namaPasien.getValue());
	}
	
	Ext.getCmp('txtkdPasien_Pembayaran').setValue(AptResepRWJ.form.ComboBox.kodePasien.getValue());
	Ext.getCmp('txtNoResepRWJ_Pembayaran').setValue(Ext.get('txtNoResepRWJ_viApotekResepRWJ').getValue());
	Ext.getCmp('dftanggalResepRWJ_Pembayaran').setValue(now_viApotekResepRWJ);
	stmpnoOut=Ext.getCmp('txtTmpNoout').getValue();
	stmptgl=Ext.getCmp('txtTmpTglout').getValue();
	if(Ext.getCmp('txtTmpNoout').getValue() == 'No out' || Ext.getCmp('txtTmpNoout').getValue() == ''){
		Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWJ_Pembayaran',
						id: 'txtNoResepRWJ_Pembayaran',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalResepRWJ_Pembayaran',
						id: 'dftanggalResepRWJ_Pembayaran',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_Pembayaran',
						id: 'txtkdPasien_Pembayaran',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_Pembayaran',
						id: 'txtNamaPasien_Pembayaran',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrResepRWJ(),
					mComboPembayaran()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekResepRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalResepRWJ_Pembayaran',
						id: 'txtTotalResepRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						flex: 1,
						width : 150,	
						//readOnly: true,
						style:{'text-align':'right'},
						name: 'txtBayarResepRWJ_Pembayaran',
						id: 'txtBayarResepRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranResepRWJ();
								};
							}
						}
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						id: 'btn1/2resep_viApotekResepRWJ',
						handler:function()
						{
						}   
					}
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnBayar_viApotekResepRWJL',
						handler:function()
						{
							pembayaranResepRWJ();
						}   
					}
				]
            }
		]
	};
    return items;
};

function datainit_viApotekResepRWJ(rowdata)
{
	dataisi=1;
	Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue(rowdata.NO_RESEP);
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(rowdata.JENIS_PASIEN);
	if(rowdata.ADMPRHS ==0.00){
		Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(rowdata.ADMPRHS);
	}
	if(rowdata.ADMRESEP == null){
		Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(rowdata.ADMRESEP);
	}
	if(rowdata.JASA == null){
		Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(0);
	}else{
		Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(rowdata.JASA);
	}
	
	Ext.getCmp('cbopasienorder_mng_apotek').disable();
		
	Ext.getCmp('txtTmpKdCustomer').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokter').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnit').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNoout').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTglout').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpSisaAngsuran').setValue(rowdata.SISA);
	Ext.getCmp('txtTmpStatusPost').setValue(rowdata.STATUS_POSTING);
	
	if(rowdata.KD_PASIENAPT === '' || rowdata.KD_PASIENAPT === undefined){
		Ext.getCmp('txtNamaPasienNon').setValue(rowdata.NMPASIEN);
		Ext.getCmp('cbNonResep').setValue(true);
		AptResepRWJ.form.ComboBox.kodePasien.disable();
		AptResepRWJ.form.ComboBox.namaPasien.disable();
	} else {
		Ext.getCmp('txtNamaPasienNon').disable();
		AptResepRWJ.form.ComboBox.kodePasien.setValue(rowdata.KD_PASIENAPT);
		AptResepRWJ.form.ComboBox.namaPasien.setValue(rowdata.NMPASIEN);
	}
	
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekResepRWJ').enable();
		Ext.getCmp('btnPrint_viResepRWJ').enable();
		Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
		Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
		Ext.getCmp('btnAddObat').disable();
		AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
			
	} else{
		Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
		Ext.getCmp('btnPrint_viResepRWJ').disable();
		Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').enable();
		Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
		Ext.getCmp('btnAddObat').enable();
		AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
	getSeCoba(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	
	Ext.Ajax.request({
									   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			AptResepRWJ.form.Panel.shift.update(cst.shift);
		}
	
	});
	
};

function getAdm(kd_customer){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getAdm",
			params: {kd_customer:kd_customer},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(toFormat(cst.tuslah));
				}
				else 
				{
					Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(toFormat(cst.tuslah));
					
				};
			}
		}
		
	)
	
}


function updatestatus_permintaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/update_obat_mng",
			params: {
			kd_pasien: kd_pasien_obbt,
			kd_unit:   kd_unit_obbt,
			tgl_masuk: tgl_masuk_obbt,
			urut_masuk: urut_masuk_obbt,
      		},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
	
}


function getSeCoba(no_out,tgl_out,admracik){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/sess",
			params: {query:"no_out = " + no_out},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd=cst.session;
					dataGridObatApotekResepRWJ(no_out,tgl_out,admracik,kd);
				}
			}
		}
	);
}

function dataGridObatApotekResepRWJ(no_out,tgl_out,admracik,kd){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/viewdetailobat/read",
			params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' and s.kd_unit_far='"+kd+"' "},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
					Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admracik));
					hasilJumlahLoad();
				}
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function dataGridObatApotekReseppoli(id_mrresep,cuss){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getobatdetail_frompoli",
			params: {query:id_mrresep,
					 cus :cuss
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{    //dsDataGrdJab_viApotekResepRWJ.store.removeAll();
				dsDataGrdJab_viApotekResepRWJ.loadData([],false);
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWJ.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dsDataGrdJab_viApotekResepRWJ.add(recs);
					
					
					AptResepRWJ.form.Grid.a.getView().refresh();
				/* 	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(toFormat(admracik));
					hasilJumlahLoad();*/
				} 
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}



function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(cst.sisa ==''){
						ShowPesanInfoResepRWJ('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(toFormat(cst.sisa));
						Ext.getCmp('txtBayarResepRWJ_Pembayaran').setValue(cst.sisa);
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue());
				};
			}
		}
		
	)
}

function datasave_viApotekResepRWJ(){
	if (ValidasiEntryResepRWJ(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/saveResepRWJ",
				params: getParamResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoResepRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtNoResepRWJ_viApotekResepRWJ').dom.value=cst.noresep;
						Ext.get('txtTmpNoout').dom.value=cst.noout;
						Ext.get('txtTmpTglout').dom.value=cst.tgl;
						Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
						Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
						refreshRespApotekRWJ();
						
						if(mBol === false)
						{
							
						};
					}
					else 
					{
						ShowPesanErrorResepRWJ('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}

function cekPeriodeBulan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/cekBulan",
			params: {a:"no_out"},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					setLookUp_viApotekResepRWJ();
				} else{
					if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
						ShowPesanErrorResepRWJ('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
					} else{
						ShowPesanErrorResepRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
					}
					
				}
			}
		}
	);
}

function pembayaranResepRWJ(){
	if(ValidasiBayarResepRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/bayarSaveResepRWJ",
				params: getParamBayarResepRWJ(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						refreshRespApotekRWJ();
						if(toInteger(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue())){
							ShowPesanInfoResepRWJ('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue(cst.noresep);
							Ext.getCmp('txtTmpNoout').setValue(cst.noout);
							Ext.getCmp('txtTmpTglout').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekResepRWJ').enable();
							Ext.getCmp('btnPrint_viResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
							Ext.getCmp('btnDelete_viApotekResepRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
							Ext.getCmp('btnAddObat').disable();
							AptResepRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ViewDetailPembayaranObat(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekRWJ.getView().refresh();
							setLookUpApotek_bayarResepRWJ.close();
						} else {
							ShowPesanInfoResepRWJ('Berhasil melakukan pembayaran','Information');
							setLookUpApotek_bayarResepRWJ.close();
							Ext.getCmp('btnunposting_viApotekResepRWJ').disable();
							Ext.getCmp('btnPrint_viResepRWJ').disable();
							ViewDetailPembayaranObat(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
							gridDTLTRHistoryApotekRWJ.getView().refresh();
						}
					}
					else 
					{
						ShowPesanErrorResepRWJ('Gagal melakukan pembayaran', 'Error');
						refreshRespApotekRWJ();
					};
				}
			}
			
		)
	}
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorResepRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamResepRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomer').getValue()=='Perseorangan'){
			KdCust='0000000001';
		}else if(Ext.get('txtTmpKdCustomer').getValue()=='Perusahaan'){
			KdCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		}else {
			KdCust=Ext.getCmp('txtTmpKdCustomer').getValue();
		}
	
	if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue() === '' || Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue()=='No Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		KdPasien:AptResepRWJ.form.ComboBox.kodePasien.getValue(),
		NmPasien:tmpNamaPasien,		
		KdUnit: Ext.getCmp('txtTmpKdUnit').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokter').getValue(),
		NonResep:tmpNonResep,
		NoResepAsal:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNoout').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTglout').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPost').getValue(),
		Tanggal:tanggal,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtDiscEditData_viApotekResepRWJ').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:AptResepRWJ.vars.no_transaksi,
		KdKasirAsal:AptResepRWJ.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Posting:0
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.cito;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
	}
	
    return params
};

function getParamBayarResepRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue()=='Perseorangan'){
			KdCust='0000000001';
		}else if(Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue()=='Perusahaan'){
			KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
		}else {
			KdCust=Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').getValue();
		}
	
	if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	}
	var ubah=0;
	if(Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue() === '' || Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue()=='No Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoresep='';
	if(Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue() === '' || Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue() === 'No Resep' ){
		LangsungPost = 1;
		tmpNoresep='';
	}else {
		LangsungPost = 0;
		tmpNoresep = Ext.getCmp('txtNoResepRWJ_Pembayaran').getValue();
	}

	var params =
	{
		NmPasien:tmpNamaPasien,		
		KdUnit: Ext.getCmp('txtTmpKdUnit').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokter').getValue(),
		NonResep:tmpNonResep,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtDiscEditData_viApotekResepRWJ').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:AptResepRWJ.vars.no_transaksi,
		KdKasirAsal:AptResepRWJ.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue()),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPay:Ext.getCmp('cboPembayaran').getValue(),
		JumlahTotal:toInteger(Ext.getCmp('txtTotalResepRWJ_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue()),
		NoResep:tmpNoresep,
		TanggalBayar:Ext.getCmp('dftanggalResepRWJ_Pembayaran').getValue(),
		JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
		Posting:LangsungPost,
		Shift: tampungshiftsekarang,
		Tanggal:tanggal,
		Ubah:ubah
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
	}
	
    return params
};

function getParamUnpostingResepRWJ() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
	}
	
    return params
}

function getParamDeleteHistoryResepRWJ() 
{
	var urut=dsTRDetailHistoryBayarList.getRange()[gridDTLTRHistoryApotekRWJ.getSelectionModel().selection.cell[0]].data.URUT;
	
	var params =
	{
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		urut:urut
	}
	
	
	
    return params
}


function datacetakbill(){
	var tmpnama;
	var tmppoli;
	var tmpdokter;
	var tmpkode;
	
	if(Ext.getCmp('cbNonResep').getValue() === true){
		tmpnama=Ext.getCmp('txtNamaPasienNon').getValue();
		tmpkode='-';
	} else{
		tmpnama=AptResepRWJ.form.ComboBox.namaPasien.getValue();
		tmpkode=AptResepRWJ.form.ComboBox.kodePasien.getValue();
	}
	
	if(Ext.get('cbo_UnitResepRWJLookup').getValue() == '' || Ext.get('cbo_UnitResepRWJLookup').getValue()=='Poliklinik'){
		tmppoli='-';
	} else{
		tmppoli=Ext.get('cbo_UnitResepRWJLookup').getValue();
	}
	
	if(Ext.get('cbo_DokterApotekResepRWJ').getValue() == '' || Ext.get('cbo_DokterApotekResepRWJ').getValue()=='Dokter'){
		tmpdokter='-';
	} else{
		tmpdokter=Ext.get('cbo_DokterApotekResepRWJ').getValue();
	}
	
	var params =
	{
		Table: 'billprintingreseprwj',
		NoResep:Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNoout').getValue(),
		TglOut:Ext.getCmp('txtTmpTglout').getValue(),
		KdPasien:tmpkode,
		NamaPasien:tmpnama,
		JenisPasien:Ext.get('cboPilihankelompokPasienAptResepRWJ').getValue(),
		Poli:tmppoli,
		Dokter:tmpdokter,
		Total:Ext.get('txtTotalEditData_viApotekResepRWJ').getValue(),
		Tot:toInteger(Ext.get('txtTotalEditData_viApotekResepRWJ').getValue())
		
	}
	params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWJ.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml;
	}
	
    return params
}



function ValidasiEntryResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbNonResep').getValue() === false){
		if(AptResepRWJ.form.ComboBox.namaPasien.getValue() === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0 || AptResepRWJ.form.ComboBox.kodePasien.getValue() === ''){
			if(AptResepRWJ.form.ComboBox.namaPasien.getValue() === ''){
				ShowPesanWarningResepRWJ('Nama Pasien', 'Warning');
				x = 0;
			} else if(dsDataGrdJab_viApotekResepRWJ.getCount() == 0){
				ShowPesanWarningResepRWJ('Daftar resep obat belum di isi', 'Warning');
				x = 0;
			} else {
				ShowPesanWarningResepRWJ('Kode Pasien', 'Warning');
				x = 0;
			}
		}
	} else {
		if(Ext.getCmp('txtNamaPasienNon').getValue() === '' || dsDataGrdJab_viApotekResepRWJ.getCount() === 0){
			if(Ext.getCmp('txtNamaPasienNon').getValue() === '' ){
				ShowPesanWarningResepRWJ('Nama', 'Warning');
				x = 0;
			} else {
				ShowPesanWarningResepRWJ('Daftar resep obat belum di isi', 'Warning');
				x = 0;
			}
				
		}
	}
	if((Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === 0) ||
		(Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtTotalEditData_viApotekResepRWJ').getValue() === 0)){
		if(Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').getValue() === 0){
			ShowPesanWarningResepRWJ('Jumlah harga total obat kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningResepRWJ('Jumlah total bayar kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		}
	}
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWJ.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWJ.getRange()[i].data;
		if(o.jml == undefined || o.jumlah == undefined){
			if(o.jml == undefined){
				ShowPesanWarningResepRWJ('Jumlah qty belum di isi, qty tidak boleh kosong', 'Warning');
				x = 0;
			}else if(o.jumlah == undefined){
				ShowPesanWarningResepRWJ('Jumlah total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
				x = 0;
			}
		}
		if(o.jml > o.jml_stok_apt){
			ShowPesanWarningResepRWJ('Jumlah qty melebihi stok', 'Warning');
			o.jml=o.jml_stok_apt;
			x = 0;
		}
	}
	
	return x;
};
function ValidasiBayarResepRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrResepRWJ').getValue() === '' || Ext.getCmp('cboJenisByrResepRWJ').getValue() === 'TUNAI'){
		ShowPesanWarningResepRWJ('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaran').getValue() === '' || Ext.getCmp('cboPembayaran').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningResepRWJ('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarResepRWJ_Pembayaran').getValue() === ''){
		ShowPesanWarningResepRWJ('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};

function getCriteriaCariApotekResepRWJ()//^^^
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue()!=='No. Resep')
            {
                strKriteria = " o.no_resep  " + "LIKE upper('%" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWJ').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasien').getValue() != "" && Ext.get('txtKdNamaPasien').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasien').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasien').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekResepRWJ').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWJ').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekResepRWJ').getValue() != "" && Ext.get('dfTglAkhirApotekResepRWJ').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWJ').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWJ').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_Unit').getValue() != "" && Ext.get('cbo_Unit').getValue() != 'Poliklinik')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_Unit').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_Unit').getValue() + "'";
			    }
                
            }
			
	 strKriteria = strKriteria +" and left(o.kd_unit,1)<>'1' and o.returapt=0 order by o.tgl_out limit 20"
	 return strKriteria;
}

function dataaddnew_viApotekResepRWJ() 
{
	Ext.getCmp('txtNoResepRWJ_viApotekResepRWJ').setValue('');
	Ext.getCmp('cbo_UnitResepRWJLookup').setValue('');
	Ext.getCmp('cbo_DokterApotekResepRWJ').setValue('');
	Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue('');
	Ext.getCmp('txtNamaPasienNon').setValue('');
	AptResepRWJ.form.ComboBox.kodePasien.setValue('');
	AptResepRWJ.form.ComboBox.namaPasien.setValue('');
	dsDataGrdJab_viApotekResepRWJ.loadData([],false);
	Ext.getCmp('cbopasienorder_mng_apotek').enable();
	Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWJ').setValue(0);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(0);
}


function ShowPesanWarningResepRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorResepRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function load_data_pasienorder(param)
{

Ext.Ajax.request(
{
url: baseURL + "index.php/apotek/functionAPOTEK/getPasienorder_mng",
 params:{
	     command: param
		} ,
failure: function(o)
{
	 var cst = Ext.decode(o.responseText);
	
},	    
success: function(o) {
cbopasienorder_mng_apotek.store.removeAll();
	var cst = Ext.decode(o.responseText);

for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
	var recs    = [],recType = dspasienorder_mng_apotek.recordType;
	var o=cst['listData'][i];
	
	recs.push(new recType(o));
    dspasienorder_mng_apotek.add(recs);
console.log(o);
}}
});



}
function mComboorder()
{ 
 
 var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_msauk','tgl_masuk'];

    dspasienorder_mng_apotek = new WebApp.DataStore({ fields: Field });
	
	load_data_pasienorder();
	cbopasienorder_mng_apotek= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_apotek',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 190,
			store: dspasienorder_mng_apotek,
			valueField: 'display',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
				
				            kd_pasien_obbt =b.data.kd_pasien;
							kd_unit_obbt=b.data.kd_unit;
							tgl_masuk_obbt =b.data.tgl_masuk;
							urut_masuk_obbt=b.data.urut_masuk;
							Ext.getCmp('cbo_DokterApotekResepRWJ').setValue(b.data.kd_dokter);
							AptResepRWJ.form.ComboBox.kodePasien.setValue(b.data.kd_pasien);
							AptResepRWJ.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWJLookup').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWJ').setValue(b.data.kd_customer);
							AptResepRWJ.vars.no_transaksi=b.data.no_transaksi;
							AptResepRWJ.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomer').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokter').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnit').setValue(b.data.kd_unit);
							dataGridObatApotekReseppoli(b.data.id_mrresep,b.data.kd_customer);
							Ext.getCmp('btnAddObat').enable();
							Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
							Ext.getCmp('btnbayar_viApotekResepRWJ').disable();
							Ext.getCmp('statusservice_apt').enable();
							},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder($this1.lastQuery);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng_apotek;
};

function ShowPesanInfoResepRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};




}
TRApotekResepRWJ();