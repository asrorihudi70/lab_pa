var dataSource_viSetupMilik;
var selectCount_viSetupMilik=50;
var NamaForm_viSetupMilik="Setup Milik";
var mod_name_viSetupMilik="Setup Milik";
var now_viSetupMilik= new Date();
var rowSelected_viSetupMilik;
var setLookUps_viSetupMilik;
var tanggal = now_viSetupMilik.format("d/M/Y");
var jam = now_viSetupMilik.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupMilik;


var CurrentData_viSetupMilik =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupMilik={};
SetupMilik.form={};
SetupMilik.func={};
SetupMilik.vars={};
SetupMilik.func.parent=SetupMilik;
SetupMilik.form.ArrayStore={};
SetupMilik.form.ComboBox={};
SetupMilik.form.DataStore={};
SetupMilik.form.Record={};
SetupMilik.form.Form={};
SetupMilik.form.Grid={};
SetupMilik.form.Panel={};
SetupMilik.form.TextField={};
SetupMilik.form.Button={};

SetupMilik.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik'],
	data: []
});

CurrentPage.page = dataGrid_viSetupMilik(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

shortcut.set({
	code:'lookup',
	list:[
		{
			key:'ctrl+s',
			fn:function(){
				Ext.getCmp('btnSimpan_viSetupMilik').el.dom.click();
			}
		},{
			key:'ctrl+d',
			fn:function(){
				Ext.getCmp('btnDelete_viSetupMilik').el.dom.click();
			}
		},{
			key:'f5',
			fn:function(){
				Ext.getCmp('btnRefresh_viSetupMilik').el.dom.click();
			}
		},
	]
});

function dataGrid_viSetupMilik(mod_id_viSetupMilik){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupMilik = 
	[
		'kd_milik', 'milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupMilik = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupMilik
    });
    dataGriSetupMilik();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupMilik = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupMilik,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupMilik = undefined;
							rowSelected_viSetupMilik = dataSource_viSetupMilik.getAt(row);
							CurrentData_viSetupMilik
							CurrentData_viSetupMilik.row = row;
							CurrentData_viSetupMilik.data = rowSelected_viSetupMilik.data;
							//DataInitSetupMilik(rowSelected_viSetupMilik.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupMilik = dataSource_viSetupMilik.getAt(ridx);
					if (rowSelected_viSetupMilik != undefined)
					{
						DataInitSetupMilik(rowSelected_viSetupMilik.data);
						//setLookUp_viSetupMilik(rowSelected_viSetupMilik.data);
					}
					else
					{
						//setLookUp_viSetupMilik();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Milik
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viSetupMilik',
						header: 'Kode milik',
						dataIndex: 'kd_milik',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colMilik_viSetupMilik',
						header: 'Milik',
						dataIndex: 'milik',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMilik',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupMilik',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupMilik != undefined)
							{
								DataInitSetupMilik(rowSelected_viSetupMilik.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMilik, selectCount_viSetupMilik, dataSource_viSetupMilik),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupMilik = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputMilik()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupMilik = new Ext.Panel
    (
		{
			title: NamaForm_viSetupMilik,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupMilik,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupMilik,
					GridDataView_viSetupMilik],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupMilik',
						handler: function(){
							AddNewSetupMilik();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupMilik',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupMilik();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupMilik',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupMilik();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupMilik',
						handler: function()
						{
							dataSource_viSetupMilik.removeAll();
							dataGriSetupMilik();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupMilik;
    //-------------- # End form filter # --------------
}

function PanelInputMilik(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode milik'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdMilik_SetupMilik',
								name: 'txtKdMilik_SetupMilik',
								width: 100,
								readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Milik'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupMilik.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboNama_SetupMilik',
								store	: SetupMilik.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdMilik_SetupMilik').setValue(b.data.kd_milik);
									
									GridDataView_viSetupMilik.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupMilik.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupMilik.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupMilik.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_milik      	: o.kd_milik,
										milik 			: o.milik,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_milik+'</td><td width="200">'+o.milik+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupMilik/getMilikGrid",
								valueField: 'milik',
								displayField: 'text',
								listWidth: 280
							})
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupMilik(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupMilik/getMilikGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupMilik('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupMilik.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupMilik.add(recs);
					
					
					
					GridDataView_viSetupMilik.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupMilik('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupMilik(){
	if (ValidasiSaveSetupMilik(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupMilik/save",
				params: getParamSaveSetupMilik(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMilik('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMilik('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdMilik_SetupMilik').setValue(cst.kdmilik);
						dataSource_viSetupMilik.removeAll();
						dataGriSetupMilik();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMilik('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupMilik.removeAll();
						dataGriSetupMilik();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupMilik(){
	if (ValidasiSaveSetupMilik(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupMilik/delete",
				params: getParamDeleteSetupMilik(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMilik('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMilik('Berhasil menghapus data ini','Information');
						AddNewSetupMilik()
						dataSource_viSetupMilik.removeAll();
						dataGriSetupMilik();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMilik('Gagal menghapus data ini', 'Error');
						dataSource_viSetupMilik.removeAll();
						dataGriSetupMilik();
					};
				}
			}
			
		)
	}
}

function AddNewSetupMilik(){
	Ext.getCmp('txtKdMilik_SetupMilik').setValue('');
	SetupMilik.vars.nama.setValue('');
};

function DataInitSetupMilik(rowdata){
	Ext.getCmp('txtKdMilik_SetupMilik').setValue(rowdata.kd_milik);
	SetupMilik.vars.nama.setValue(rowdata.milik);
};

function getParamSaveSetupMilik(){
	var	params =
	{
		KdMilik:Ext.getCmp('txtKdMilik_SetupMilik').getValue(),
		Milik:SetupMilik.vars.nama.getValue()
	}
   
    return params
};

function getParamDeleteSetupMilik(){
	var	params =
	{
		KdMilik:Ext.getCmp('txtKdMilik_SetupMilik').getValue()
	}
   
    return params
};

function ValidasiSaveSetupMilik(modul,mBolHapus){
	var x = 1;
	if(SetupMilik.vars.nama.getValue() === ''){
		if(SetupMilik.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupMilik('Nama milik masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningSetupMilik(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupMilik(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupMilik(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};